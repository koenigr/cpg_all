--- 1 ---
You need to copy the four files:
CUDA 5.0.props
CUDA 5.0.targets
CUDA 5.0.xml
Nvda.Build.CudaTasks.v5.0.dll

to the folder 
Program Files (x86) [can be different on your computer]
\MSBuild\Microsoft.Cpp\v4.0\BuildCustomizations

If this doesn't work, you need to instal CUDA:
https://developer.nvidia.com/cuda-downloads

--- 2 ---
You need to download and instal the boost library:
http://www.boost.org/users/download/

Add the path of your boost installation to:
FW_GPU_NET -> properties -> Configuration Properties -> C/C++ -> General -> Additional Include Directories

Click on the text (drop down) and selece <Edit>. Now add the path to your boost installation.



echo off

rem get main class
set MAIN=%1
shift

rem build classpath for binary distribution
set LOCALCLASSPATH=
for %%i in (%ESOM_HOME%\lib\*.jar) do call %ESOM_HOME%\bin\lcp.bat %%i

rem start program
echo on
call java -cp "%LOCALCLASSPATH%" -Xmx512m %MAIN% %%*

#ifndef COLOR_MATH_h
#define COLOR_MATH_h

#include <math.h>
#include "MyMath.h"
#include <vector>
#include <atlstr.h>

/////////////////////////////////////////////////////////////////////////////

const float eps360 = 0.001f;
const float eps = 0.000001f;
const float eps2 = 2*eps;

const float CIE_e = 216.0f / 24389.0f;
const float CIE_k = 24389.0f / 27.0f;
//reference white Observer= 2�, Illuminant= D65 von http://www.easyrgb.com/math.php?MATH=M8#text8
const float D65_X = 0.95047f;
const float D65_Y = 1.0f;
const float D65_Z = 1.08883f;

class CColorMath
{
public:
   typedef std::vector<double> DoubleVector;
   typedef std::vector<bool> BoolVector;
   typedef std::vector<int> IntVector;

   //Farbfunktionen von http://semmix.pl/color/extrans
   inline double minimum(double r, double g, double b)
   {
	   double m;

	   if (r<g) m = r; else m = g;
	   if (b<m) m = b;

	   return m;
   }

   inline double maximum(double r, double g, double b)
   {
	   double m;

	   if (r>g) m = r; else m = g;
	   if (b>m) m = b;

	   return m;
   }

   inline double centre(double r, double g, double b)
   {
	   double m;
	   while (true)
	   {
		   if ((r<g) && (r<b)) { if (g<b) m = g; else m = b; break; }
		   if ((g<r) && (g<b)) { if (r<b) m = r; else m = b; break; }
		   if (r<g) m = r; else m = g; break;
	   }
	   return m;
   }

   inline bool verify(double *v)
   {
	   if (*v<-eps) return false;
	   if (*v>1+eps) return false;
	   if (*v<=eps) *v = 0; else
	   if (*v>=1-eps) *v = 1;
	   return true;
   }

   inline float minimum(float r, float g, float b)
   {
	   float m;

	   if (r<g) m = r; else m = g;
	   if (b<m) m = b;

	   return m;
   }

   inline float maximum(float r, float g, float b)
   {
	   float m;

	   if (r>g) m = r; else m = g;
	   if (b>m) m = b;

	   return m;
   }

   inline float centre(float r, float g, float b)
   {
	   float m;
	   while (true)
	   {
		   if ((r<g) && (r<b)) { if (g<b) m = g; else m = b; break; }
		   if ((g<r) && (g<b)) { if (r<b) m = r; else m = b; break; }
		   if (r<g) m = r; else m = g; break;
	   }
	   return m;
   }

   inline bool verify(float *v)
   {
	   if (*v<-eps) return false;
	   if (*v>1+eps) return false;
	   if (*v<=eps) *v = 0; else
	   if (*v>=1-eps) *v = 1;
	   return true;
   }
/*
   // http://www.easyrgb.com: HSV-->RGB

if ( S == 0 )                       //HSV from 0 to 1
{
   R = V * 255
   G = V * 255
   B = V * 255
}
else
{
   var_h = H * 6
   if ( var_h == 6 ) var_h = 0      //H must be < 1
   var_i = int( var_h )             //Or ... var_i = floor( var_h )
   var_1 = V * ( 1 - S )
   var_2 = V * ( 1 - S * ( var_h - var_i ) )
   var_3 = V * ( 1 - S * ( 1 - ( var_h - var_i ) ) )

   if      ( var_i == 0 ) { var_r = V     ; var_g = var_3 ; var_b = var_1 }
   else if ( var_i == 1 ) { var_r = var_2 ; var_g = V     ; var_b = var_1 }
   else if ( var_i == 2 ) { var_r = var_1 ; var_g = V     ; var_b = var_3 }
   else if ( var_i == 3 ) { var_r = var_1 ; var_g = var_2 ; var_b = V     }
   else if ( var_i == 4 ) { var_r = var_3 ; var_g = var_1 ; var_b = V     }
   else                   { var_r = V     ; var_g = var_1 ; var_b = var_2 }

   R = var_r * 255                  //RGB results from 0 to 255
   G = var_g * 255
   B = var_b * 255
}
*/

   inline void   HSBtoRGB(double H, double S, double V, double *R, double *G, double *B)
   {
      if ( S == 0 )                       //HSV from 0 to 1
      {
         (*R) = V;
         (*G) = V;
         (*B) = V;
      }
      else
      {
         double var_h = H / 60;
         while ( var_h >= 6 ) 
            var_h -= 6;

         int var_i = (int)floor( var_h );
         double var_1 = V * ( 1 - S );
         double var_2 = V * ( 1 - S * ( var_h - var_i ) );
         double var_3 = V * ( 1 - S * ( 1 - ( var_h - var_i ) ) );

         if      ( var_i == 0 ) { (*R) = V     ; (*G) = var_3 ; (*B) = var_1; }
         else if ( var_i == 1 ) { (*R) = var_2 ; (*G) = V     ; (*B) = var_1; }
         else if ( var_i == 2 ) { (*R) = var_1 ; (*G) = V     ; (*B) = var_3; }
         else if ( var_i == 3 ) { (*R) = var_1 ; (*G) = var_2 ; (*B) = V;     }
         else if ( var_i == 4 ) { (*R) = var_3 ; (*G) = var_1 ; (*B) = V;     }
         else                   { (*R) = V     ; (*G) = var_1 ; (*B) = var_2; }
      }
   }

   inline void   HSBtoRGB(float H, float S, float V, float *R, float *G, float *B)
   {
      if ( S == 0 )                       //HSV from 0 to 1
      {
         (*R) = V;
         (*G) = V;
         (*B) = V;
      }
      else
      {
         float var_h = H / 60;
         while ( var_h >= 6 ) 
            var_h -= 6;

         int var_i = (int)floor( var_h );
         float var_1 = V * ( 1 - S );
         float var_2 = V * ( 1 - S * ( var_h - var_i ) );
         float var_3 = V * ( 1 - S * ( 1 - ( var_h - var_i ) ) );

         if      ( var_i == 0 ) { (*R) = V     ; (*G) = var_3 ; (*B) = var_1; }
         else if ( var_i == 1 ) { (*R) = var_2 ; (*G) = V     ; (*B) = var_1; }
         else if ( var_i == 2 ) { (*R) = var_1 ; (*G) = V     ; (*B) = var_3; }
         else if ( var_i == 3 ) { (*R) = var_1 ; (*G) = var_2 ; (*B) = V;     }
         else if ( var_i == 4 ) { (*R) = var_3 ; (*G) = var_1 ; (*B) = V;     }
         else                   { (*R) = V     ; (*G) = var_1 ; (*B) = var_2; }
      }
   }

/*
   // http://www.easyrgb.com: RGB --> HSV

var_R = ( R / 255 )                     //RGB from 0 to 255
var_G = ( G / 255 )
var_B = ( B / 255 )

var_Min = min( var_R, var_G, var_B )    //Min. value of RGB
var_Max = max( var_R, var_G, var_B )    //Max. value of RGB
del_Max = var_Max - var_Min             //Delta RGB value

V = var_Max

if ( del_Max == 0 )                     //This is a gray, no chroma...
{
   H = 0                                //HSV results from 0 to 1
   S = 0
}
else                                    //Chromatic data...
{
   S = del_Max / var_Max

   del_R = ( ( ( var_Max - var_R ) / 6 ) + ( del_Max / 2 ) ) / del_Max
   del_G = ( ( ( var_Max - var_G ) / 6 ) + ( del_Max / 2 ) ) / del_Max
   del_B = ( ( ( var_Max - var_B ) / 6 ) + ( del_Max / 2 ) ) / del_Max

   if      ( var_R == var_Max ) H = del_B - del_G
   else if ( var_G == var_Max ) H = ( 1 / 3 ) + del_R - del_B
   else if ( var_B == var_Max ) H = ( 2 / 3 ) + del_G - del_R

   if ( H < 0 ) ; H += 1
   if ( H > 1 ) ; H -= 1
}

*/
   inline void   RGBtoHSB(double R, double G, double B, double *H, double *S, double *V)
   {
      double var_Min = minimum( R, G, B );    //Min. value of RGB
      double var_Max = maximum( R, G, B );    //Max. value of RGB
      double del_Max = var_Max - var_Min;     //Delta RGB value

      (*V) = var_Max;

      if ( del_Max < eps )                    //This is a gray, no chroma...
      {
         (*H) = 0;                            //HSV results from 0 to 1
         (*S) = 0;
      }
      else                                    //Chromatic data...
      {
         (*S) = del_Max / var_Max;

         double del_R = ( ( ( var_Max - R ) / 6 ) + ( del_Max / 2 ) ) / del_Max;
         double del_G = ( ( ( var_Max - G ) / 6 ) + ( del_Max / 2 ) ) / del_Max;
         double del_B = ( ( ( var_Max - B ) / 6 ) + ( del_Max / 2 ) ) / del_Max;

         if      ( R == var_Max ) (*H) = del_B - del_G;
         else if ( G == var_Max ) (*H) = ( 1.0 / 3.0 ) + del_R - del_B;
         else if ( B == var_Max ) (*H) = ( 2.0 / 3.0 ) + del_G - del_R;

         if ( (*H) < 0 ) (*H) += 1;
         if ( (*H) > 1 ) (*H) -= 1;

         (*H) *= 360;
      }
   }

   inline void   RGBtoHSB(float R, float G, float B, float *H, float *S, float *V)
   {
      float var_Min = minimum( R, G, B );    //Min. value of RGB
      float var_Max = maximum( R, G, B );    //Max. value of RGB
      float del_Max = var_Max - var_Min;     //Delta RGB value

      (*V) = var_Max;

      if ( del_Max < eps )                    //This is a gray, no chroma...
      {
         (*H) = 0;                            //HSV results from 0 to 1
         (*S) = 0;
      }
      else                                    //Chromatic data...
      {
         (*S) = del_Max / var_Max;

         float del_R = ( ( ( var_Max - R ) / 6 ) + ( del_Max / 2 ) ) / del_Max;
         float del_G = ( ( ( var_Max - G ) / 6 ) + ( del_Max / 2 ) ) / del_Max;
         float del_B = ( ( ( var_Max - B ) / 6 ) + ( del_Max / 2 ) ) / del_Max;

         if      ( R == var_Max ) (*H) = del_B - del_G;
         else if ( G == var_Max ) (*H) = ( 1.0f / 3.0f ) + del_R - del_B;
         else if ( B == var_Max ) (*H) = ( 2.0f / 3.0f ) + del_G - del_R;

         if ( (*H) < 0 ) (*H) += 1;
         if ( (*H) > 1 ) (*H) -= 1;

         (*H) *= 360;
      }
   }
/*
   inline void   HueToRGB(double m0, double m2, double H, double *R, double *G, double *B)
   {
	   double m1, F;
	   int n;

      if ((H<-360) || (H>720)) H=0;
      while (H<0)    H += 360;
	   while (H>=360) H -= 360;
	   n = (int)floor(H / 60);
	   if (H<60)  F = H; else
	   if (H<180) F = H - 120; else
	   if (H<300) F = H - 240; else F = H - 360;
	   m1 = m0 + (m2 - m0) * sqrt(fabs(F / 60));
	   switch (n)
	   {
	   case 0: *R = m2; *G = m1; *B = m0; break;
	   case 1: *R = m1; *G = m2; *B = m0; break;
	   case 2: *R = m0; *G = m2; *B = m1; break;
	   case 3: *R = m0; *G = m1; *B = m2; break;
	   case 4: *R = m1; *G = m0; *B = m2; break;
	   case 5: *R = m2; *G = m0; *B = m1; break;
	   }
   }
   inline double RGBtoHue(double R, double G, double B)
   {
	   double F, H, m0, m1, m2;

	   m2 = maximum(R,G,B);
	   m1 = centre(R,G,B);
	   m0 = minimum(R,G,B);
	   while (true)
	   {
		   if (fabs(m2 - m1) <= eps) //exist two max: m1=m2
		   {
			   if (fabs(R - G) <= eps) { H = 60; break; }
			   if (fabs(G - B) <= eps) { H = 180; break; }
			   H = 300; break;
		   }

		   //exist single man only
		   F = 60 * (m1 - m0) / (m2 - m0);
		   if (fabs(R - m2) <= eps) {	H = 0   + F * (G - B); break; }
		   if (fabs(G - m2) <= eps) {	H = 120 + F * (B - R); break; }
		   H = 240 + F * (R - G); break; 
	   }
	   if (H<0) H += 360;

	   return H;
   }
   inline void   HueToRGB(float m0, float m2, float H, float *R, float *G, float *B)
   {
	   float m1, F;
	   int n;

      if ((H<-360) || (H>720)) H=0;
      while (H<0)    H += 360;
	   while (H>=360) H -= 360;
	   n = (int)floor(H / 60);
	   if (H<60)  F = H; else
	   if (H<180) F = H - 120; else
	   if (H<300) F = H - 240; else F = H - 360;
	   m1 = m0 + (m2 - m0) * sqrt(fabs(F / 60));
	   switch (n)
	   {
	   case 0: *R = m2; *G = m1; *B = m0; break;
	   case 1: *R = m1; *G = m2; *B = m0; break;
	   case 2: *R = m0; *G = m2; *B = m1; break;
	   case 3: *R = m0; *G = m1; *B = m2; break;
	   case 4: *R = m1; *G = m0; *B = m2; break;
	   case 5: *R = m2; *G = m0; *B = m1; break;
	   }
   }
   inline float RGBtoHue(float R, float G, float B)
   {
	   float F, H, m0, m1, m2;

	   m2 = maximum(R,G,B);
	   m1 = centre(R,G,B);
	   m0 = minimum(R,G,B);
	   while (true)
	   {
		   if (fabs(m2 - m1) <= eps) //exist two max: m1=m2
		   {
			   if (fabs(R - G) <= eps) { H = 60; break; }
			   if (fabs(G - B) <= eps) { H = 180; break; }
			   H = 300; break;
		   }

		   //exist single man only
		   F = 60 * (m1 - m0) / (m2 - m0);
		   if (fabs(R - m2) <= eps) {	H = 0   + F * (G - B); break; }
		   if (fabs(G - m2) <= eps) {	H = 120 + F * (B - R); break; }
		   H = 240 + F * (R - G); break; 
	   }
	   if (H<0) H += 360;

	   return H;
   }
   inline void   HSBtoRGB(double H, double S, double V, double *R, double *G, double *B)
   {
	   double m0;
	   *R = -1; *G = -1; *B = -1; 
	   if (!verify(&V)) return;
	   if (!verify(&S)) return;
	   if (V <= eps2) { *R = 0; *G = 0; *B = 0; return; }
	   m0 = (1 - S) * V;
	   if (fabs(V - m0) <= eps2) { *R = V; *G = V; *B = V; return; }
	   HueToRGB(m0,V,H,R,G,B);
   }
   inline void   RGBtoHSB(double R, double G, double B, double *H, double *S, double *V)
   {
	   double m0, m2;

	   *H = 0; *S = -1; *V = -1;
	   if (!verify(&R)) return;
	   if (!verify(&G)) return;
	   if (!verify(&B)) return;
	   m0 = minimum(R,G,B);
	   m2 = maximum(R,G,B);
	   if (m2 <= eps2) { *S = 0; *V = 0; return; }
	   *V = m2;
      *S = 1.0 - m0 / m2;
	   if (*S <= eps2) { *S = 0; return; }
	   *H = RGBtoHue(R,G,B);
   }
   inline void   HSBtoRGB(float H, float S, float V, float *R, float *G, float *B)
   {
	   float m0;
	   *R = -1; *G = -1; *B = -1; 
	   if (!verify(&V)) return;
	   if (!verify(&S)) return;
	   if (V <= eps2) { *R = 0; *G = 0; *B = 0; return; }
	   m0 = (1 - S) * V;
	   if (fabs(V - m0) <= eps2) { *R = V; *G = V; *B = V; return; }
	   HueToRGB(m0,V,H,R,G,B);
   }
   inline void   RGBtoHSB(float R, float G, float B, float *H, float *S, float *V)
   {
	   float m0, m2;

	   *H = 0; *S = -1; *V = -1;
	   if (!verify(&R)) return;
	   if (!verify(&G)) return;
	   if (!verify(&B)) return;
	   m0 = minimum(R,G,B);
	   m2 = maximum(R,G,B);
	   if (m2 <= eps2) { *S = 0; *V = 0; return; }
	   *V = m2;
      *S = 1.0 - m0 / m2;
	   if (*S <= eps2) { *S = 0; return; }
	   *H = RGBtoHue(R,G,B);
   }
*/

   inline void HueAngle2IttenAngle(double hue, double *itten_angle)
   {
      while(hue<0)    hue += 360;
      while(hue>=360) hue -= 360;

      //rot bis gelb
      if ((hue>=0)   && (hue<60))  (*itten_angle) = 120 - hue * 2;
      //gelb bis gr�n
      if ((hue>=60)  && (hue<120)) (*itten_angle) = 360 - (hue - 60);
      //gr�n bis blau
      if ((hue>=120) && (hue<240)) (*itten_angle) = 300 - (hue - 120) / 2;
      //blau bis rot
      if ((hue>=240) && (hue<360)) (*itten_angle) = 240 - (hue - 240);

      (*itten_angle) = (*itten_angle) * DSL_PI / 180;
   }

   inline void IttenAngle2HueAngle(double itten_angle, double *hue)
   {
      itten_angle=itten_angle/DSL_PI*180;

      while(itten_angle<0)    itten_angle += 360;
      while(itten_angle>=360) itten_angle -= 360;

      //gelb bis rot
      if ((itten_angle>=0)   && (itten_angle<120))  (*hue) = 60 - itten_angle / 2;
      //rot bis blau
      if ((itten_angle>=120) && (itten_angle<240))  (*hue) = 360 - (itten_angle - 120);
      //blau bis gr�n
      if ((itten_angle>=240) && (itten_angle<300))  (*hue) = 240 - (itten_angle - 240) * 2;
      //gr�n bis gelb
      if ((itten_angle>=300) && (itten_angle<360))  (*hue) = 120 - (itten_angle - 300);
   }


   //eigene Farbfunktionene Ittenscher Farbkreis
   inline void RGB2Itten(double R, double G, double B, double *Itten, double *S, double *V)
   {
      double H;
      RGBtoHSB(R,G,B,&H,S,V);
      HueAngle2IttenAngle(H,Itten);
   }

   inline void Itten2RGB(double Itten, double S, double V, double *R, double *G, double *B)
   {
      double H;
      IttenAngle2HueAngle(Itten,&H);
      HSBtoRGB(H,S,V,R,G,B);
   }
/*
   inline void   GetIttenColors(double angle,double *r, double *g, double *b)
   {
      angle=angle/PI*180;
      while (angle<0) angle+=360;
      while (angle>360) angle-=360;

      if (angle==0)
      {
         //gelb
         (*r)=1; (*g)=1; (*b)=0;
         return;
      }
      if ((angle>0) && (angle<120))
      {
         //gelb-orange-rot
         (*r)=1; (*g)=(120.0-angle)/120.0; (*b)=0;
         return;
      }
      if (angle==120)
      {
         //rot
         (*r)=1; (*g)=0; (*b)=0;
         return;
      }
      if ((angle>120) && (angle<240))
      {
         //rot-violett-blau
         (*r)=(240.0-angle)/120.0; (*g)=0; (*b)=(angle-120.0)/120.0;
         return;
      }
      if (angle==240)
      {
         //blau
         (*r)=0; (*g)=0; (*b)=1;
         return;
      }
      if ((angle>240) && (angle<300))
      {
         //blau-gr�n
         (*r)=0; (*g)=(angle-240.0)/60.0; (*b)=(300.0-angle)/60.0;
         return;
      }
      if (angle==300)
      {
         //gr�n
         (*r)=0; (*g)=1; (*b)=0;
         return;
      }
      if ((angle>300) && (angle<360))
      {
         //gr�n-gelb
         (*r)=(angle-300.0)/60.0; (*g)=1.0; (*b)=0;
         return;
      }
   }
   inline void   RGBToIttenAngle(double r,double g, double b, double * angle)
   {
	   double m0, m1, m2;

	   m2 = maximum(r,g,b);
	   m1 = centre(r,g,b);
	   m0 = minimum(r,g,b);

      //wei� grau scharz
      if ((m0==m1) && (m1==m2))
      {
         (*angle)=0;
         return;
      }

      //Festfarben
      if (m1-m0<=eps)
      {
         if (m2-r<=eps) { (*angle)=120.0 /180.0*PI; return; } //rot
         if (m2-g<=eps) { (*angle)=300.0 /180.0*PI; return; } //gr�n
         (*angle)=240.0 /180.0*PI; //blau
         return;
      }
      if (m2-m1<=eps)
      {
         if (b-m0<=eps) { (*angle)=0.0 /180.0*PI; return; } //gelb
      }

      //gelb bis rot
      if ((m2-r<=eps) && (m1-g<=eps))
      {
         (*angle)=(1.0-(m1-m0)/(m2-m0))*120.0 /180.0*PI;
         return;
      }
      //rot bis blau
      if (g-m0<=eps)
      {
         //(*r)=(240.0-angle)/120.0; (*g)=0; (*b)=(angle-120.0)/120.0;
         r-=m0;
         b-=m0;
         //r+b=1
         if (r+b<=eps) 
         {
            (*angle)=0;
            return;
         }
         r/=(r+b);
         //b/=(r+b);

         (*angle)=(240.0-r*120.0) /180*PI;

         return;
      }
      //blau bis gr�n
      if (r-m0<=eps)
      {
         //(*r)=0; (*g)=(angle-240.0)/60.0; (*b)=(300.0-angle)/60.0;
         g-=m0;
         b-=m0;
         //g+b=1
         if (g+b<=eps) 
         {
            (*angle)=0;
            return;
         }
         g/=(g+b);
         //b/=(g+b);

         (*angle)=(g*60.0+240.0) /180*PI;

         return;
      }
      //gr�n bis gelb
      if ((m2-g<=eps) && (m1-r<=eps))
      {
         (*angle)=(300.0 + (m1-m0)/(m2-m0)*60.0) /180.0*PI;
         return;
      }

      (*angle)=0.0;
      return;
   }
*/
  
   void   ApplyColorSymetry(DoubleVector * angle,IntVector constant_angle);
   inline double GetAngleAxisDiff(double * angle,double * axis)
   {
      double d;

      d=(*angle)-(*axis);
      if (d<-DSL_PI) d+=2*DSL_PI;
      if (d>DSL_PI)  d-=2*DSL_PI;

      return d;
   }
   inline bool   ValueIn(CString s, int item)
   {
      bool erg;
      CString tmp;

      if (item<9)
         tmp.Format("#0%d",item+1);
	   else
         tmp.Format("#%d",item+1);

      erg=(s.Find(tmp,0) != -1);

      return erg;
   }

   inline void   MaximizeIttenColors(double *r, double *g, double *b)
   {
	   double m0, m1, m2;

	   m2 = maximum(*r,*g,*b);
	   m1 = centre(*r,*g,*b);
	   m0 = minimum(*r,*g,*b);

      if ((m2>0.0) && (m2<1.0))
      {
         if (fabs(m2-*r)<=eps) (*r)=1.0;
         if (fabs(m2-*g)<=eps) (*g)=1.0;
         if (fabs(m2-*b)<=eps) (*b)=1.0;

         if (fabs(m1-*r)<=eps) (*r)=m1/m2;
         if (fabs(m1-*g)<=eps) (*g)=m1/m2;
         if (fabs(m1-*b)<=eps) (*b)=m1/m2;

         if (fabs(m0-*r)<=eps) (*r)=m0/m2;
         if (fabs(m0-*g)<=eps) (*g)=m0/m2;
         if (fabs(m0-*b)<=eps) (*b)=m0/m2;
      }
   }

   
   //Farbfunktionen von http://www.brucelindbloom.com/index.html?ColorCalcHelp.html

   inline void RGBtoXYZ(double R, double G, double B, double &X, double &Y, double &Z)
   {
      double r,g,b;

      if (R<=0.04045) r = R / 12.92; else r = pow((R + 0.055) / 1.055, 2.4);
      if (G<=0.04045) g = G / 12.92; else g = pow((G + 0.055) / 1.055, 2.4);
      if (B<=0.04045) b = B / 12.92; else b = pow((B + 0.055) / 1.055, 2.4);

      // RGB Workingspace = "sRGB D65" siehe http://www.brucelindbloom.com/index.html?ColorCalcHelp.html
      X = 0.412453 * r + 0.357580 * g + 0.180423 * b;
      Y = 0.212671 * r + 0.715160 * g + 0.072169 * b;
      Z = 0.019334 * r + 0.119193 * g + 0.950227 * b;
   }

   inline void RGBtoXYZ(float R, float G, float B, float &X, float &Y, float &Z)
   {
      float r,g,b;

      if (R<=0.04045f) r = R / 12.92f; else r = pow((R + 0.055f) / 1.055f, 2.4f);
      if (G<=0.04045f) g = G / 12.92f; else g = pow((G + 0.055f) / 1.055f, 2.4f);
      if (B<=0.04045f) b = B / 12.92f; else b = pow((B + 0.055f) / 1.055f, 2.4f);

      // RGB Workingspace = "sRGB D65" siehe http://www.brucelindbloom.com/index.html?ColorCalcHelp.html
      X = 0.412453f * r + 0.357580f * g + 0.180423f * b;
      Y = 0.212671f * r + 0.715160f * g + 0.072169f * b;
      Z = 0.019334f * r + 0.119193f * g + 0.950227f * b;
   }

   inline void XYZtoxyY(double X, double Y, double Z, double &nx, double &ny, double &nY)
   {
      if (X+Y+Z != 0.0)
      {
         nx = X / (X + Y + Z);
         ny = Y / (X + Y + Z);
      }
      else
      {
         nx = 0;
         ny = 0;
      }
      nY = Y;
   }

   inline void xyYtoXYZ(double nx, double ny, double nY, double &X, double &Y, double &Z)
   {
      if (ny != 0.0)
      {
         X = (nx * nY) / ny;
         Y = nY;
         Z = (1 - nx - ny) * nY / ny;
      }
      else
         X = Y = Z = 0.0;
   }

   inline void XYZtoxyY(float X, float Y, float Z, float &nx, float &ny, float &nY)
   {
      if (X+Y+Z != 0.0)
      {
         const float fak = 1 / (X + Y + Z);
         nx = X * fak;
         ny = Y * fak;
         //nx = X / (X + Y + Z);
         //ny = Y / (X + Y + Z);
      }
      else
      {
         nx = 0;
         ny = 0;
      }
      nY = Y;
   }

   inline void xyYtoXYZ(float nx, float ny, float nY, float &X, float &Y, float &Z)
   {
      if (ny != 0.0)
      {
         const float fak = 1 / ny;
         X = (nx * nY) * fak;
         Y = nY;
         Z = (1 - nx - ny) * nY * fak;
         //X = (nx * nY) / ny;
         //Y = nY;
         //Z = (1 - nx - ny) * nY / ny;
      }
      else
         X = Y = Z = 0.0;
   }

   inline void XYZtoRGB(double X, double Y, double Z, double &R, double &G, double &B)
   {
      double r,g,b;

      // RGB Workingspace = "sRGB D65" siehe http://www.brucelindbloom.com/index.html?ColorCalcHelp.html
      r =   3.240479 * X - 1.537150 * Y - 0.498535 * Z;
      g = - 0.969256 * X + 1.875991 * Y + 0.041556 * Z;
      b =   0.055648 * X - 0.204043 * Y + 1.057311 * Z;

      if (r<=0.0031308) R = 12.92 * r; else R = 1.055 * pow(r, 1.0/2.4) - 0.055;
      if (g<=0.0031308) G = 12.92 * g; else G = 1.055 * pow(g, 1.0/2.4) - 0.055;
      if (b<=0.0031308) B = 12.92 * b; else B = 1.055 * pow(b, 1.0/2.4) - 0.055;
   }

   inline void XYZtoRGB(float X, float Y, float Z, float &R, float &G, float &B)
   {
      float r,g,b;

      // RGB Workingspace = "sRGB D65" siehe http://www.brucelindbloom.com/index.html?ColorCalcHelp.html
      r =   3.240479f * X - 1.537150f * Y - 0.498535f * Z;
      g = - 0.969256f * X + 1.875991f * Y + 0.041556f * Z;
      b =   0.055648f * X - 0.204043f * Y + 1.057311f * Z;

      if (r<=0.0031308f) R = 12.92f * r; else R = 1.055f * pow(r, 1.0f/2.4f) - 0.055f;
      if (g<=0.0031308f) G = 12.92f * g; else G = 1.055f * pow(g, 1.0f/2.4f) - 0.055f;
      if (b<=0.0031308f) B = 12.92f * b; else B = 1.055f * pow(b, 1.0f/2.4f) - 0.055f;
   }

   inline void XYZtoLab(double X, double Y, double Z, double &L, double &a, double &b)
   {
      double fx,fy,fz;

      //Observer= 2�, Illuminant= D65 von http://www.easyrgb.com/math.php?MATH=M8#text8
      X /= D65_X;
      Y /= D65_Y;
      Z /= D65_Z;

      if (X>CIE_e)   fx = pow(X, 1.0 / 3.0);  else   fx = (CIE_k * X + 16.0) / 116.0;
      if (Y>CIE_e)   fy = pow(Y, 1.0 / 3.0);  else   fy = (CIE_k * Y + 16.0) / 116.0;
      if (Z>CIE_e)   fz = pow(Z, 1.0 / 3.0);  else   fz = (CIE_k * Z + 16.0) / 116.0;

      L = 116.0 * fy -16.0;
      a = 500.0 * (fx - fy);
      b = 200.0 * (fy - fz);
   }

   inline void XYZtoLab(float X, float Y, float Z, float &L, float &a, float &b)
   {
      float fx,fy,fz;

      //Observer= 2�, Illuminant= D65 von http://www.easyrgb.com/math.php?MATH=M8#text8
      X /= (float)D65_X;
      Y /= (float)D65_Y;
      Z /= (float)D65_Z;

      if (X>CIE_e)   fx = pow(X, 1.0f / 3.0f);  else   fx = ((float)CIE_k * X + 16.0f) / 116.0f;
      if (Y>CIE_e)   fy = pow(Y, 1.0f / 3.0f);  else   fy = ((float)CIE_k * Y + 16.0f) / 116.0f;
      if (Z>CIE_e)   fz = pow(Z, 1.0f / 3.0f);  else   fz = ((float)CIE_k * Z + 16.0f) / 116.0f;

      L = 116.0f * fy -16.0f;
      a = 500.0f * (fx - fy);
      b = 200.0f * (fy - fz);
   }

   inline void LabtoXYZ(double L, double a, double b, double &X, double &Y, double &Z)
   {
      double fx,fy,fz;

      if (L > CIE_k * CIE_e)  Y = pow((L + 16.0) / 116.0 , 3.0);  else  Y = L / CIE_k;

      if (Y > CIE_e)  fy = (L + 16.0) / 116.0;  else  fy = (CIE_k * Y + 16.0) / 116.0;

      fx = a / 500.0 + fy;
      fz = fy - b / 200.0;

      if (pow(fx, 3.0) > CIE_e)  X = pow(fx, 3.0);  else  X = (116.0 * fx - 16.0) / CIE_k;
      if (pow(fz, 3.0) > CIE_e)  Z = pow(fz, 3.0);  else  Z = (116.0 * fz - 16.0) / CIE_k;

      //Observer= 2�, Illuminant= D65 von http://www.easyrgb.com/math.php?MATH=M8#text8
      X *= D65_X;
      Y *= D65_Y;
      Z *= D65_Z;
   }

   inline void RGBtoLab(double R, double G, double B, double &L, double &a, double &b)
   {
      double X,Y,Z;

      RGBtoXYZ(R,G,B,X,Y,Z);
      XYZtoLab(X,Y,Z,L,a,b);
   }

   inline void RGBtoLab(float R, float G, float B, float &L, float &a, float &b)
   {
      float X,Y,Z;

      RGBtoXYZ(R,G,B,X,Y,Z);
      XYZtoLab(X,Y,Z,L,a,b);
   }

   inline void LabtoRGB(double L, double a, double b, double &R, double &G, double &B)
   {
      double X,Y,Z;

      LabtoXYZ(L,a,b,X,Y,Z);
      XYZtoRGB(X,Y,Z,R,G,B);
   }

   //OpenCV Color Conversion
   /*
   RGB<=>YCrCb JPEG (a.k.a. YCC) (CV_BGR2YCrCb, CV_RGB2YCrCb, CV_YCrCb2BGR, CV_YCrCb2RGB)

   Y <- 0.299*R + 0.587*G + 0.114*B
   Cr <- (R-Y)*0.713 + delta
   Cb <- (B-Y)*0.564 + delta

   R <- Y + 1.403*(Cr - delta)
   G <- Y - 0.344*(Cr - delta) - 0.714*(Cb - delta)
   B <- Y + 1.773*(Cb - delta),

                 { 128 for 8-bit images,
   where delta = { 32768 for 16-bit images
                 { 0.5 for floating-point images

   Y, Cr and Cb cover the whole value range.
   */
   inline void RGBtoYCrCb(float R, float G, float B, float &Y, float &Cr, float &Cb)
   {
      const float delta = 0.5f;

      Y = 0.299f*R + 0.587f*G + 0.114f*B;
      Cr = (R-Y)*0.713f + delta;
      Cb = (B-Y)*0.564f + delta;
   }

   inline void YCrCbtoRGB(float Y, float Cr, float Cb, float &R, float &G, float &B)
   {
      const float delta = 0.5f;

      R = Y + 1.403f*(Cr - delta);
      G = Y - 0.344f*(Cr - delta) - 0.714f*(Cb - delta);
      B = Y + 1.773f*(Cb - delta);
   }
};

static CColorMath ColorMath;

#endif
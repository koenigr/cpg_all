#include "stdafx.h"
#include "DirectSunLight.h"
#include <opencv/highgui.h>


using namespace DSL;

#ifdef _DEBUG
inline void CheckOpenCLError(cl_int clerr)
{
	if (clerr == CL_SUCCESS)
		return;
	else
	{
		int stop = 0;
		switch (clerr)
		{
		case CL_INVALID_PLATFORM:              stop = 1;      assert(0); break;
		case CL_INVALID_VALUE:                 stop = 2;      assert(0); break;
		case CL_DEVICE_NOT_AVAILABLE:          stop = 3;      assert(0); break;
		case CL_DEVICE_NOT_FOUND:              stop = 4;      assert(0); break;
		case CL_OUT_OF_HOST_MEMORY:            stop = 5;      assert(0); break;
		case CL_INVALID_DEVICE_TYPE:           stop = 6;      assert(0); break;
		case CL_INVALID_PROGRAM_EXECUTABLE:    stop = 7;      assert(0); break;
		case CL_INVALID_COMMAND_QUEUE:         stop = 8;      assert(0); break;
		case CL_INVALID_KERNEL:                stop = 9;      assert(0); break;
		case CL_INVALID_CONTEXT:               stop = 10;     assert(0); break;
		case CL_INVALID_KERNEL_ARGS:           stop = 11;     assert(0); break;
		case CL_INVALID_WORK_DIMENSION:        stop = 12;     assert(0); break;
		case CL_INVALID_WORK_GROUP_SIZE:       stop = 13;     assert(0); break;
		case CL_INVALID_WORK_ITEM_SIZE:        stop = 14;     assert(0); break;
		case CL_INVALID_GLOBAL_OFFSET:         stop = 15;     assert(0); break;
		case CL_OUT_OF_RESOURCES:              stop = 16;     assert(0); break;
		case CL_MEM_OBJECT_ALLOCATION_FAILURE: stop = 17;     assert(0); break;
		case CL_INVALID_EVENT_WAIT_LIST:       stop = 18;     assert(0); break;
		case CL_INVALID_PROGRAM:               stop = 19;     assert(0); break;
		case CL_INVALID_DEVICE:                stop = 20;     assert(0); break;
		case CL_INVALID_BINARY:                stop = 21;     assert(0); break;
		case CL_INVALID_BUILD_OPTIONS:         stop = 22;     assert(0); break;
		case CL_INVALID_OPERATION:             stop = 23;     assert(0); break;
		case CL_COMPILER_NOT_AVAILABLE:        stop = 24;     assert(0); break;
		case CL_BUILD_PROGRAM_FAILURE:         stop = 25;     assert(0); break;
		case CL_INVALID_ARG_INDEX:             stop = 27;     assert(0); break; //if arg_index is not a valid argument index.
		case CL_INVALID_ARG_VALUE:             stop = 28;     assert(0); break; //if arg_value specified is NULL for an argument that is not declared with the __local qualifier or vice-versa.
		case CL_INVALID_MEM_OBJECT:            stop = 29;     assert(0); break; //for an argument declared to be a memory object when the specified arg_value is not a valid memory object.
		case CL_INVALID_SAMPLER:               stop = 30;     assert(0); break; //for an argument declared to be of type sampler_t when the specified arg_value is not a valid sampler object.
		case CL_INVALID_ARG_SIZE:              stop = 31;     assert(0); break; //if arg_size does not match the size of the data type for an argument that is not a memory object or if the argument is a memory object and arg_size != sizeof(cl_mem) or if arg_size is zero and the argument is declared with the __local qualifier or if the argument is a sampler and arg_size != sizeof(cl_sampler)
		default:                               stop = 9999;   assert(0); break;
		}
	}
}
#else
inline void CheckOpenCLError(cl_int clerr) 
{
#ifdef _DEBUG_CONSOLE
	if (clerr == CL_SUCCESS)
		return;
	else
	{
		switch (clerr)
		{
		case CL_INVALID_PLATFORM:              printf("CL_INVALID_PLATFORM\n");                   break;
		case CL_INVALID_VALUE:                 printf("CL_INVALID_VALUE\n");                      break;
		case CL_DEVICE_NOT_AVAILABLE:          printf("CL_DEVICE_NOT_AVAILABLE\n");               break;
		case CL_DEVICE_NOT_FOUND:              printf("CL_DEVICE_NOT_FOUND\n");                   break;
		case CL_OUT_OF_HOST_MEMORY:            printf("CL_OUT_OF_HOST_MEMORY\n");                 break;
		case CL_INVALID_DEVICE_TYPE:           printf("CL_INVALID_DEVICE_TYPE\n");                break;
		case CL_INVALID_PROGRAM_EXECUTABLE:    printf("CL_INVALID_PROGRAM_EXECUTABLE\n");         break;
		case CL_INVALID_COMMAND_QUEUE:         printf("CL_INVALID_COMMAND_QUEUE\n");              break;
		case CL_INVALID_KERNEL:                printf("CL_INVALID_KERNEL\n");                     break;
		case CL_INVALID_CONTEXT:               printf("CL_INVALID_CONTEXT\n");                    break;
		case CL_INVALID_KERNEL_ARGS:           printf("CL_INVALID_KERNEL_ARGS\n");                break;
		case CL_INVALID_WORK_DIMENSION:        printf("CL_INVALID_WORK_DIMENSION\n");             break;
		case CL_INVALID_WORK_GROUP_SIZE:       printf("CL_INVALID_WORK_GROUP_SIZE\n");            break;
		case CL_INVALID_WORK_ITEM_SIZE:        printf("CL_INVALID_WORK_ITEM_SIZE\n");             break;
		case CL_INVALID_GLOBAL_OFFSET:         printf("CL_INVALID_GLOBAL_OFFSET\n");              break;
		case CL_OUT_OF_RESOURCES:              printf("CL_OUT_OF_RESOURCES\n");                   break;
		case CL_MEM_OBJECT_ALLOCATION_FAILURE: printf("CL_MEM_OBJECT_ALLOCATION_FAILURE\n");      break;
		case CL_INVALID_EVENT_WAIT_LIST:       printf("CL_INVALID_EVENT_WAIT_LIST\n");            break;
		case CL_INVALID_PROGRAM:               printf("CL_INVALID_PROGRAM\n");                    break;
		case CL_INVALID_DEVICE:                printf("CL_INVALID_DEVICE\n");                     break;
		case CL_INVALID_BINARY:                printf("CL_INVALID_BINARY\n");                     break;
		case CL_INVALID_BUILD_OPTIONS:         printf("CL_INVALID_BUILD_OPTIONS\n");              break;
		case CL_INVALID_OPERATION:             printf("CL_INVALID_OPERATION\n");                  break;
		case CL_COMPILER_NOT_AVAILABLE:        printf("CL_COMPILER_NOT_AVAILABLE\n");             break;
		case CL_BUILD_PROGRAM_FAILURE:         printf("CL_BUILD_PROGRAM_FAILURE\n");              break;
		case CL_INVALID_ARG_INDEX:             printf("CL_INVALID_ARG_INDEX\n");                  break; //if arg_index is not a valid argument index.
		case CL_INVALID_ARG_VALUE:             printf("CL_INVALID_ARG_VALUE\n");                  break; //if arg_value specified is NULL for an argument that is not declared with the __local qualifier or vice-versa.
		case CL_INVALID_MEM_OBJECT:            printf("CL_INVALID_MEM_OBJECT\n");                 break; //for an argument declared to be a memory object when the specified arg_value is not a valid memory object.
		case CL_INVALID_SAMPLER:               printf("CL_INVALID_SAMPLER\n");                    break; //for an argument declared to be of type sampler_t when the specified arg_value is not a valid sampler object.
		case CL_INVALID_ARG_SIZE:              printf("CL_INVALID_ARG_SIZE\n");                   break; //if arg_size does not match the size of the data type for an argument that is not a memory object or if the argument is a memory object and arg_size != sizeof(cl_mem) or if arg_size is zero and the argument is declared with the __local qualifier or if the argument is a sampler and arg_size != sizeof(cl_sampler)
		default:                               printf("CL_UNKNOWN_ERROR %d\n", clerr);            break;
		}
	}
#endif
}
#endif


CDirectSunLight::CDirectSunLight()
	: m_pHDRImage( NULL )
	, m_pContext( NULL )
	, m_pFBO( NULL )
	, m_pReadBuffer( NULL )
	, m_pSkyCubeMap( NULL )
	, m_bGeometryChanged( true )
	, m_bTexInited( false )
	, m_pWichtung( NULL )
	, m_pErg( NULL )
	, m_pErg2( NULL )
	, m_pWichtungOpenCL( NULL )
	, m_dErg( NULL )
	, m_dErg2( NULL )
	, m_dWichtungOpenCL( NULL )
	, m_dImageBuffer( NULL )
	, m_dImage( NULL )
	, m_iCalculationMode( Leuchtdichte_kcd_per_square_meter )
	, m_vNormale( 0, 0, 1)
	, m_iCurrentPolygonID( 0 )
#ifdef _DEBUG_DEEP_OPENCL
	, m_dDebug( NULL )
	, m_pDebug( NULL )
#endif
{
}

CDirectSunLight::~CDirectSunLight()
{
	if (m_pHDRImage)
	{
		delete m_pHDRImage;
		m_pHDRImage = NULL;
	}
	if (m_pContext)
	{
		delete m_pContext;
		m_pContext = NULL;
	}
	if (m_pFBO)
	{
		delete m_pFBO;
		m_pFBO = NULL;
	}
	if (m_pReadBuffer)
	{
		free( m_pReadBuffer );
		m_pReadBuffer = NULL;
	}
	if (m_pSkyCubeMap)
	{
		free( m_pSkyCubeMap );
		m_pSkyCubeMap = NULL;
	}
	if (m_pWichtung)
	{
		free( m_pWichtung );
		m_pWichtung = NULL;
	}

	OpenCLQuit();

	OpenCL::CloseOpenCLForever();
	DeleteAllGeometry();
}

bool CDirectSunLight::SetHDRImage(const char * name)
{
	if (m_pHDRImage)
	{
		delete m_pHDRImage;
		m_pHDRImage = NULL;
	}

	m_pHDRImage = new CHDRImage();
	bool result = m_pHDRImage->LoadHDRImage(CString(name));
	if (result)
		m_pHDRImage->GenerateBlurArray();
	return result;
}

void CDirectSunLight::SetResolution(int cubeSize)
{
	if (cubeSize < 64)
		cubeSize = 64;
	cubeSize = (int)ceil(cubeSize / 64.0f) * 64;
	m_iCubeSize = cubeSize;

	//OpenCL Optimierung
	const int iMaxTextureSize = 1024;
	m_iOptiFactorX = (int)floor((float)iMaxTextureSize / (m_iCubeSize * 6)); // 2
	m_iOptiFactorY = (int)floor((float)iMaxTextureSize / m_iCubeSize);		 // 16

	if (m_iOptiFactorX < 1)
		m_iOptiFactorX = 1;
	if (m_iOptiFactorY < 1)
		m_iOptiFactorY = 1;

	bool reInit = false;
	if (m_pContext)
	{
		//delete m_pContext;
		//m_pContext = NULL;
		reInit = true;
	}
	else
	{
		m_pContext = new COpenGLContext();
		m_pContext->InitContext(cubeSize * 6 * m_iOptiFactorX, cubeSize * m_iOptiFactorY);
	}

	if (m_pFBO)
	{
		delete m_pFBO;
		m_pFBO = NULL;
	}

	CString info = m_pContext->GetInformation();

	m_pContext->BeginGLCommands();

	m_pFBO = new AS::FramebufferObject("rgba=32t depth=32", 1);
	CheckGLError();
	m_pFBO->initialize(cubeSize * 6 * m_iOptiFactorX, cubeSize * m_iOptiFactorY);
	CheckGLError();
	bool use_fbo = m_pFBO->isFBOsupported() && m_pFBO->checkFramebufferStatus();
	if (use_fbo)
	{
		m_pContext->OnCreateGL();
		//std::cout << "- using Framebuffer object" << std::endl;
	}
	else
	{
		//delete m_pFBO;
		//m_pFBO = NULL;

		exit(2);
	}

	m_pContext->EndGLCommands();

	if (m_pReadBuffer)
	{
		free( m_pReadBuffer );
		m_pReadBuffer = NULL;
	}
	m_pReadBuffer = (float *)malloc(sizeof(float) * cubeSize * 6 * m_iOptiFactorX * cubeSize * m_iOptiFactorY * 4);

	if (m_pWichtung)
	{
		free( m_pWichtung );
		m_pWichtung = NULL;
	}
	m_pWichtung = GenerateFormfactorMap(cubeSize);

	if (reInit)
	{
		m_pContext->BeginGLCommands();
		OpenCLQuit();
		OpenCLInit();
		m_pContext->EndGLCommands();
	}
}

float * CDirectSunLight::GetFBOPixel(int x, int y) const
{
	return m_pReadBuffer + (y * m_pContext->GetDX() + x) * 4;
}


void CDirectSunLight::RenderHDRISky()
{
	if (!m_bTexInited)
		assert(0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_iTexName);
	glEnable(GL_TEXTURE_2D);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glDepthMask( GL_FALSE );
	glDisable( GL_DEPTH_TEST );

	const int tex_dx = m_iCubeSize * 6;
	const int tex_dy = m_iCubeSize;
	const float dist = 99.0f;

	glBegin(GL_QUADS);

	//glColor3f(1,0,0);//rot - Westen
	glTexCoord2f(  0.0f/tex_dx,               0.0f/tex_dy);
	glVertex3f(-dist, -dist,  dist);
	glTexCoord2f(  0.0f/tex_dx,               (tex_dy-0.0f)/tex_dy);
	glVertex3f(-dist,  dist,  dist);
	glTexCoord2f(  (tex_dy-0.0f)/tex_dx,      (tex_dy-0.0f)/tex_dy);
	glVertex3f(-dist,  dist, -dist);
	glTexCoord2f(  (tex_dy-0.0f)/tex_dx,      0.0f/tex_dy);
	glVertex3f(-dist, -dist, -dist);

	//glColor3f(0,1,0);//gr�n - Norden
	glTexCoord2f(  (tex_dy+0.0f)/tex_dx,      0.0f/tex_dy);
	glVertex3f(-dist, -dist, -dist);
	glTexCoord2f(  (tex_dy+0.0f)/tex_dx,      (tex_dy-0.0f)/tex_dy);
	glVertex3f(-dist,  dist, -dist);
	glTexCoord2f(  (2.0f*tex_dy-0.0f)/tex_dx, (tex_dy-0.0f)/tex_dy);
	glVertex3f( dist,  dist, -dist);
	glTexCoord2f(  (2.0f*tex_dy-0.0f)/tex_dx, 0.0f/tex_dy);
	glVertex3f( dist, -dist, -dist);

	//glColor3f(1,1,0);//gelb - Osten
	glTexCoord2f(  (2.0f*tex_dy+0.0f)/tex_dx, 0.0f/tex_dy);
	glVertex3f( dist, -dist, -dist);
	glTexCoord2f(  (2.0f*tex_dy+0.0f)/tex_dx, (tex_dy-0.0f)/tex_dy);
	glVertex3f( dist,  dist, -dist);
	glTexCoord2f(  (3.0f*tex_dy-0.0f)/tex_dx, (tex_dy-0.0f)/tex_dy);
	glVertex3f( dist,  dist,  dist);
	glTexCoord2f(  (3.0f*tex_dy-0.0f)/tex_dx, 0.0f/tex_dy);
	glVertex3f( dist, -dist,  dist);


	//glColor3f(0,1,1);//indigo - S�den
	glTexCoord2f(  (3.0f*tex_dy+0.0f)/tex_dx, 0.0f/tex_dy);
	glVertex3f( dist, -dist,  dist);
	glTexCoord2f(  (3.0f*tex_dy+0.0f)/tex_dx, (tex_dy-0.0f)/tex_dy);
	glVertex3f( dist,  dist,  dist);
	glTexCoord2f(  (4.0f*tex_dy-0.0f)/tex_dx, (tex_dy-0.0f)/tex_dy);
	glVertex3f(-dist,  dist,  dist);
	glTexCoord2f(  (4.0f*tex_dy-0.0f)/tex_dx, 0.0f/tex_dy);
	glVertex3f(-dist, -dist,  dist);

	//glColor3f(1,0,1);//lila - unten
	glTexCoord2f(  (4.0f*tex_dy+0.0f)/tex_dx, 0.0f/tex_dy);
	glVertex3f( dist, -dist, -dist);
	glTexCoord2f(  (4.0f*tex_dy+0.0f)/tex_dx, (tex_dy-0.0f)/tex_dy);
	glVertex3f(-dist, -dist, -dist);
	glTexCoord2f(  (5.0f*tex_dy-0.0f)/tex_dx, (tex_dy-0.0f)/tex_dy);
	glVertex3f(-dist, -dist,  dist);
	glTexCoord2f(  (5.0f*tex_dy-0.0f)/tex_dx, 0.0f/tex_dy);
	glVertex3f( dist, -dist,  dist);

	//glColor3f(0,0,1);//blau - oben
	glTexCoord2f(  (5.0f*tex_dy+0.0f)/tex_dx, 0.0f/tex_dy);
	glVertex3f(-dist,  dist,  dist);
	glTexCoord2f(  (5.0f*tex_dy+0.0f)/tex_dx, (tex_dy-0.0f)/tex_dy);
	glVertex3f( dist,  dist,  dist);
	glTexCoord2f(  (6.0f*tex_dy-0.0f)/tex_dx, (tex_dy-0.0f)/tex_dy);
	glVertex3f( dist,  dist, -dist);
	glTexCoord2f(  (6.0f*tex_dy-0.0f)/tex_dx, 0.0f/tex_dy);
	glVertex3f(-dist,  dist, -dist);

	glEnd();

	glDepthMask( GL_TRUE );
	glEnable( GL_DEPTH_TEST );
	glDisable(GL_TEXTURE_2D);
}

#define PI 3.14159265

void CDirectSunLight::RenderDirectSunLight(int iOptiPosX, int iOptiPosY, Vec3d position, Vec3d direction)
{
	direction.Normalize();
	Vec3d rotAxis = Vec3d(0, 0, 1).amul(direction);
	double rotCos = Vec3d(0, 0, 1).imul(direction);
	double rotSin = rotAxis.Value();
	double rotAngle = acos(rotCos) * 180.0 / PI;
	bool toRotate = rotAxis.Normalize();
	
	for(int side = 0; side < 6; ++side)
	{
		glViewport(iOptiPosX * (m_iCubeSize * 6) + side * m_iCubeSize, iOptiPosY * m_iCubeSize, m_iCubeSize, m_iCubeSize);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(90.0, 1.0, 0.01, 100.0); // 90� Sichtfeld, 1.0 Aspektratio, 0.01 m z_near, 100.0 z-far
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		if (toRotate)
			glRotated(rotAngle, rotAxis.x, rotAxis.y, rotAxis.z);
		else if (rotCos < 0)
			glRotated(180, 0, 1, 0);

		switch (side)
		{
		case 0: // front
			break;
		case 1: // right
			glRotated(90.0, 0, -1, 0);
			break;
		case 2: // back
			glRotated(180.0, 0, -1, 0);
			break;
		case 3: // left
			glRotated(270.0, 0, -1, 0);
			break;
		case 4: // bottom
			glRotated(90.0, -1, 0, 0);
			break;
		case 5: // top
			glRotated(-90.0, -1, 0, 0);
			break;
		}

		//HDRI des Himmels rendern
		RenderHDRISky();


		//Kamera Position ber�cksichtigen
		glTranslated(-position.x, -position.z, position.y);
		//Architektur Geometrie dar�ber rendern
		if (m_bGeometryChanged)
		{
			BuildGeometryDisplayList();
			m_bGeometryChanged = false;
		}
		ShowGeometryDisplayList();
	}
}

void CDirectSunLight::CalculateDirectSunLight(Vec3d position, Vec3d direction, float &fEnergyRed, float &fEnergyGreen, float &fEnergyBlue)
{
	if ((!m_pContext) || (!m_pHDRImage))
		return;

	m_pContext->BeginGLCommands();
	// Rendern
	glClampColor(GL_CLAMP_READ_COLOR, GL_FALSE);
	glClampColor(GL_CLAMP_VERTEX_COLOR, GL_FALSE);
	glClampColor(GL_CLAMP_FRAGMENT_COLOR, GL_FALSE);
	CheckGLError();

	m_pFBO->beginCapture();

	glViewport(0, 0, m_iCubeSize * 6, m_iCubeSize);
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	CheckGLError();

	RenderDirectSunLight(0, 0, position, direction);
	m_pFBO->endCapture();

	float * pErg = (float *)malloc(sizeof(float) * 3);
	RunOpenCL(1, pErg);
	fEnergyRed		= pErg[0];
	fEnergyGreen	= pErg[1];
	fEnergyBlue		= pErg[2];
	free(pErg);

	m_pContext->EndGLCommands();
}

void CDirectSunLight::CalculateDirectSunLight(size_t iPointCount, Vec3d * pPosition, Vec3d * pDirection, float *pResult)
{
	if ((!m_pContext) || (!m_pHDRImage))
		return;

	m_pContext->BeginGLCommands();
	// Rendern
	glClampColor(GL_CLAMP_READ_COLOR, GL_FALSE);
	glClampColor(GL_CLAMP_VERTEX_COLOR, GL_FALSE);
	glClampColor(GL_CLAMP_FRAGMENT_COLOR, GL_FALSE);
	CheckGLError();

	unsigned int rows = (unsigned int)ceil((float)iPointCount / m_iOptiFactorX);
	unsigned int pages = (unsigned int)ceil((float)rows / m_iOptiFactorY);
	unsigned int counter(iPointCount);
	unsigned int doneCounter(0);
	for(unsigned int page = 0; page < pages; ++page)
	{
		unsigned int pageCounter = counter;
		if (pageCounter > m_iOptiFactorX * m_iOptiFactorY)
			pageCounter = m_iOptiFactorX * m_iOptiFactorY;
		counter -= pageCounter;

		m_pFBO->beginCapture();

		glViewport(0, 0, m_pContext->GetDX(), m_pContext->GetDY());
		glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		CheckGLError();

		// Rendern
		unsigned int iCount(0);
		for(unsigned int y = 0; y < m_iOptiFactorY; ++y)
			for(unsigned int x = 0; x < m_iOptiFactorX; ++x, ++iCount)
			{
				if (iCount < pageCounter)
					RenderDirectSunLight(x, y, pPosition[doneCounter + iCount], pDirection[doneCounter + iCount]);
			}

		m_pFBO->endCapture();

#if 0
		glBindTexture(GL_TEXTURE_RECTANGLE_ARB , m_pFBO->getColorAttachmentID(0) );
		CheckGLError();
		glGetTexImage(GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGBA, GL_FLOAT, m_pReadBuffer);
		CheckGLError();

		//opencv Test
		int cv_res_x = m_pContext->GetDX();
		int cv_res_y = m_pContext->GetDY();
		IplImage* tmpimg = cvCreateImage( cvSize(cv_res_x, cv_res_y), IPL_DEPTH_8U, 3);
		
		int x,y;
		for(y=0; y<cv_res_y; y++)
			for(x=0; x<cv_res_x; x++)
			{
				float * pRGB = GetFBOPixel(x, y);
				
				unsigned char & r = *((unsigned char*)tmpimg->imageData + ((cv_res_y-y-1) * cv_res_x + x) * 3 + 0);
				unsigned char & g = *((unsigned char*)tmpimg->imageData + ((cv_res_y-y-1) * cv_res_x + x) * 3 + 1);
				unsigned char & b = *((unsigned char*)tmpimg->imageData + ((cv_res_y-y-1) * cv_res_x + x) * 3 + 2);
				
				float fr = floor((*pRGB++) * 255.0f + 0.5f);
				float fg = floor((*pRGB++) * 255.0f + 0.5f);
				float fb = floor((*pRGB  ) * 255.0f + 0.5f);
				
				b = (fr < 0.0f) ? 0 : ((fr > 255.0f) ? 255 : (unsigned char) fr);
				g = (fg < 0.0f) ? 0 : ((fg > 255.0f) ? 255 : (unsigned char) fg);
				r = (fb < 0.0f) ? 0 : ((fb > 255.0f) ? 255 : (unsigned char) fb);
			}
		char filename[256];
		sprintf_s(filename, "bild_side_all.bmp");
		cvSaveImage(filename,tmpimg);      //tempor�re Darstellung
		cvReleaseImage( &tmpimg );
#endif

		// Auswerten
		RunOpenCL(pageCounter, pResult + (doneCounter*3));

		doneCounter += pageCounter;	
	}
	m_pContext->EndGLCommands();
}

void CDirectSunLight::PrepareHDRCubeMap(float fNorthAngle)
{
	if ((!m_pContext) || (!m_pHDRImage))
		return;

	double dHalfCubeSize = m_iCubeSize/2.0;

	if (m_pSkyCubeMap)
	{
		free( m_pSkyCubeMap );
		m_pSkyCubeMap = NULL;
	}
	m_pSkyCubeMap = (float *)malloc(sizeof(float) * 6 * m_iCubeSize * m_iCubeSize * 3);
	float fBlur = (float)m_pHDRImage->width / (m_iCubeSize * 4); // Pixel Blur

	for(int side = 0; side < 6; ++side)
	{
		for(size_t y = 0; y < m_iCubeSize; ++y)
			for(size_t x = 0; x < m_iCubeSize; ++x)
			{
				Vec3d vec(dHalfCubeSize - 0.5 - x, dHalfCubeSize, dHalfCubeSize - 0.5 - y);
				vec.Normalize();
				switch(side)
				{
				case 0: // front
					break;
				case 1: // right
					Geom3d.Rot_Z_3d(vec, sin(DSL_PI/2), cos(DSL_PI/2));
					break;
				case 2: // back
					Geom3d.Rot_Z_3d(vec, sin(DSL_PI), cos(DSL_PI));
					break;
				case 3: // left
					Geom3d.Rot_Z_3d(vec, sin(3*DSL_PI/2), cos(3*DSL_PI/2));
					break;
				case 4: // bottom
					Geom3d.Rot_X_3d(vec, sin(DSL_PI/2), cos(DSL_PI/2));
					break;
				case 5: // top
					Geom3d.Rot_X_3d(vec, sin(-DSL_PI/2), cos(-DSL_PI/2));
					break;
				}
				//Geom3d.Rot_Z_3d(vec, sin(fNorthAngle), cos(fNorthAngle));

				double angleH = Geom3d.GetXYAngle(vec) + fNorthAngle;
				double angleV = sin(vec.z);

				float px = (float)(angleH/(2*DSL_PI) * m_pHDRImage->width);
				float py = (float)((angleV/(DSL_PI/2)+1)/2 * m_pHDRImage->height);

				float r, g, b;
				m_pHDRImage->GetPixelBlur(px, py, r, g, b, fBlur);

				float *pRGB = m_pSkyCubeMap + (((y * m_iCubeSize * 6) + x + (side * m_iCubeSize)) * 3);
				float &wr = *pRGB++;
				float &wg = *pRGB++;
				float &wb = *pRGB;

				wr = r;
				wg = g;
				wb = b;
			}
	}

	//OpenGL Textur zur Grafikkarte schicken
	m_pContext->BeginGLCommands();

	if (!m_bTexInited)
	{
		glGenTextures( 1, &m_iTexName );
		m_bTexInited = true;
	}
	glActiveTexture(GL_TEXTURE0);
	glBindTexture( GL_TEXTURE_2D, m_iTexName );
/*
	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, m_iCubeSize * 6, m_iCubeSize, GL_RGB, GL_FLOAT, m_pSkyCubeMap);

	// select modulate to mix texture with color for shading
	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );

	// when texture area is small, bilinear filter the closest mipmap
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	// when texture area is large, bilinear filter the first mipmap
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

	// if wrap is true, the texture wraps over at the edges (repeat)
	//       ... false, the texture ends at the edges (clamp)
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
*/
	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);        
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);    
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);   
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, m_iCubeSize * 6, m_iCubeSize, 0, GL_RGB, GL_FLOAT, m_pSkyCubeMap);

	//OpenCLInit();

	m_pContext->EndGLCommands();

#if 0
	//opencv Test
	int cv_res_x = m_iCubeSize * 6;
	int cv_res_y = m_iCubeSize;
	IplImage* tmpimg = cvCreateImage( cvSize(cv_res_x, cv_res_y), IPL_DEPTH_8U, 3); 

	int x,y;
	//COLORREF scan;
	for(y=0; y<cv_res_y; y++)
		for(x=0; x<cv_res_x; x++)
		{
			float * pRGB = m_pSkyCubeMap + (((y * m_iCubeSize * 6) + x) * 3);

			unsigned char & r = *((unsigned char*)tmpimg->imageData + ((cv_res_y-y-1) * cv_res_x + x) * 3 + 0);
			unsigned char & g = *((unsigned char*)tmpimg->imageData + ((cv_res_y-y-1) * cv_res_x + x) * 3 + 1);
			unsigned char & b = *((unsigned char*)tmpimg->imageData + ((cv_res_y-y-1) * cv_res_x + x) * 3 + 2);

			float fr = floor((*pRGB++) * 255.0f + 0.5f);
			float fg = floor((*pRGB++) * 255.0f + 0.5f);
			float fb = floor((*pRGB  ) * 255.0f + 0.5f);

			b = (fr < 0.0f) ? 0 : ((fr > 255.0f) ? 255 : (unsigned char) fr);
			g = (fg < 0.0f) ? 0 : ((fg > 255.0f) ? 255 : (unsigned char) fg);
			r = (fb < 0.0f) ? 0 : ((fb > 255.0f) ? 255 : (unsigned char) fb);
		}

	cvSaveImage("cube.bmp",tmpimg);      //tempor�re Darstellung
	cvReleaseImage( &tmpimg );
#endif
}

void CDirectSunLight::DeleteAllGeometry()
{
	GeometryMap::iterator l,e;
	l = m_vGeometryMap.begin();
	e = m_vGeometryMap.end();
	for(; l!=e; l++)
	{
		PolygonRenderInfo * pInfo = (*l).second;
		delete pInfo;
	}
	m_vGeometryMap.clear();

	m_bGeometryChanged = true;
	m_iCurrentPolygonID = 0;
}

size_t CDirectSunLight::AddPolygon(CGeom3d::Point_3d_Vector_Vector &points, float fTransparency)
{
	CGeom3d::TriangleVector triangles;
	Geom3d.TesselatePVV(points, triangles);
	CGeom3d::Vec3d normale = Geom3d.GetNormale(points);

	size_t id = GetNewPolygonID();
	m_vGeometryMap.insert(GeometryPair(id, new PolygonRenderInfo(triangles, normale, fTransparency)));
	m_bGeometryChanged = true;
	return id;
}

size_t CDirectSunLight::AddPolygonTriangles(CGeom3d::Vec3d& normale, CGeom3d::TriangleVector &triangles, float fTransparency)
{
	size_t id = GetNewPolygonID();
	m_vGeometryMap.insert(GeometryPair(id, new PolygonRenderInfo(triangles, normale, fTransparency)));
	m_bGeometryChanged = true;
	return id;
}

void CDirectSunLight::DeletePolygon(size_t id)
{
	GeometryMap::iterator found = m_vGeometryMap.find(id);
	if (found != m_vGeometryMap.end())
	{
		PolygonRenderInfo * pInfo = (*found).second;
		delete pInfo;

		m_vGeometryMap.erase(found);
	}
	m_bGeometryChanged = true;
}

bool CDirectSunLight::MovePolygon(size_t id, CGeom3d::Point_3d_Vector_Vector &newPoints)
{
	GeometryMap::iterator found = m_vGeometryMap.find(id);
	if (found != m_vGeometryMap.end())
	{
		CGeom3d::TriangleVector triangles;
		Geom3d.TesselatePVV(newPoints, triangles);
		CGeom3d::Vec3d normale = Geom3d.GetNormale(newPoints);
		(*found).second->m_vTriangles = triangles;
		(*found).second->m_normale = normale;
		m_bGeometryChanged = true;
		return true;
	}
	else
		return false;
}

size_t CDirectSunLight::GetNewPolygonID()
{
#if 0
	size_t id(0);

	GeometryMap::const_iterator l,e;
	l = m_vGeometryMap.begin();
	e = m_vGeometryMap.end();
	for(; l!=e; l++)
		if (id == (*l).first)
			++id;
		else
			break;

	return id;
#else
	return m_iCurrentPolygonID++;
#endif
}

void CDirectSunLight::BuildGeometryDisplayList()
{
	m_vDisplayList.SetMode(GL_COMPILE);
	m_vDisplayList.Reset();
	glNewList(m_vDisplayList.GetNewList(), m_vDisplayList.GetMode());

	GeometryMap::iterator l,e;
	l = m_vGeometryMap.begin();
	e = m_vGeometryMap.end();

	glBegin(GL_TRIANGLES);
	m_vDisplayList.IncCounter(2); // begin, end

	for(; l!=e; l++)
	{
		const PolygonRenderInfo & info = *(*l).second;
		glNormal3f((GLfloat)info.m_normale.x, (GLfloat)info.m_normale.z, (GLfloat)-info.m_normale.y);
		glColor4f(0.0f, 0.0f, 0.0f, info.m_fTransparency);

		CGeom3d::TriangleVector::const_iterator l_tri, e_tri;
		l_tri = info.m_vTriangles.begin();
		e_tri = info.m_vTriangles.end();
		for(; l_tri!=e_tri; l_tri++)
		{
			const CGeom3d::Triangle_3d &tri = *l_tri;
			glVertex3f((GLfloat)tri.m_P1.x, (GLfloat)tri.m_P1.z, (GLfloat)-tri.m_P1.y);
			glVertex3f((GLfloat)tri.m_P2.x, (GLfloat)tri.m_P2.z, (GLfloat)-tri.m_P2.y);
			glVertex3f((GLfloat)tri.m_P3.x, (GLfloat)tri.m_P3.z, (GLfloat)-tri.m_P3.y);
		}

		m_vDisplayList.IncCounter(2 + info.m_vTriangles.size() * 3); // normale + color + 3 Punkte je Dreieck
		if (m_vDisplayList.GetCounter() > MAX_DISPLAYLIST_INSTRUCTIONS)
		{
			glEnd();

			glEndList();
			glNewList(m_vDisplayList.GetNewList(), m_vDisplayList.GetMode());
			m_vDisplayList.ResetCounter();

			glBegin(GL_TRIANGLES);
			m_vDisplayList.IncCounter(2); // begin, end
		}
	}
	glEnd();
	glEndList();
}

void CDirectSunLight::ShowGeometryDisplayList()
{
	m_vDisplayList.Show();
}

float * CDirectSunLight::GenerateFormfactorMap(int cube_size)
{
	float * pMap;
	int x, y, z, x2, y2;
	float value;
	double sum_check=0;
	Vec3d pixel_ecke[4];
	int cube_size_2 = (int)ceil(cube_size/2.0f);

	pMap=(float *)malloc(sizeof(float)*(long)pow((float)cube_size,2.0f));;
	for (x=0; x<cube_size_2; x++)
		for (y=0; y<cube_size_2; y++)
		{
			pixel_ecke[0].x=((double)x    /(double)cube_size)-0.5;
			pixel_ecke[0].y=((double)y    /(double)cube_size)-0.5;
			pixel_ecke[1].x=((double)(x+1)/(double)cube_size)-0.5;
			pixel_ecke[1].y=((double)y    /(double)cube_size)-0.5;
			pixel_ecke[2].x=((double)(x+1)/(double)cube_size)-0.5;
			pixel_ecke[2].y=((double)(y+1)/(double)cube_size)-0.5;
			pixel_ecke[3].x=((double)x    /(double)cube_size)-0.5;
			pixel_ecke[3].y=((double)(y+1)/(double)cube_size)-0.5;
			pixel_ecke[0].z=0.5;
			pixel_ecke[1].z=0.5;
			pixel_ecke[2].z=0.5;
			pixel_ecke[3].z=0.5;
			for (z=0; z<4; z++)
				pixel_ecke[z].Normalize();
			value=(float)(Geom3d.AreaKugelViereck_Normalized(pixel_ecke[0], 
				pixel_ecke[1], 
				pixel_ecke[2], 
				pixel_ecke[3]) / (4.0 * DSL_PI));

			x2 = cube_size - x - 1;
			y2 = cube_size - y - 1;
			pMap[x *(unsigned __int32)cube_size+y ]=value;
			pMap[x2*(unsigned __int32)cube_size+y ]=value;
			pMap[x2*(unsigned __int32)cube_size+y2]=value;
			pMap[x *(unsigned __int32)cube_size+y2]=value;
			if ((x!=x2) && (y!=y2))
				sum_check+=value*4;
			else
				if ((x==x2) && (y==y2))
					sum_check+=value;
				else
					sum_check+=value*2;
		}

		//Einheitskreis Okay ?
		sum_check*=6;
		if (fabs(sum_check-1.0)>Eps3)
			assert(0);
		sum_check = 0;

		return pMap;
}

void CDirectSunLight::OpenCLQuit()
{
	if (m_pErg)
	{
		free( m_pErg );
		m_pErg = NULL;
	}

	if (m_pErg2)
	{
		free( m_pErg2 );
		m_pErg2 = NULL;
	}

	if (m_pWichtungOpenCL)
	{
		free( m_pWichtungOpenCL );
		m_pWichtungOpenCL = NULL;
	}

	if (m_dErg)
	{
		clReleaseMemObject( *m_dErg );
		delete m_dErg;
		m_dErg = NULL;
	}

	if (m_dErg2)
	{
		clReleaseMemObject( *m_dErg2 );
		delete m_dErg2;
		m_dErg2 = NULL;
	}

	if (m_dWichtungOpenCL)
	{
		clReleaseMemObject( *m_dWichtungOpenCL );
		delete m_dWichtungOpenCL;
		m_dWichtungOpenCL = NULL;
	}

	if (m_dImageBuffer)
	{
		clReleaseMemObject( *m_dImageBuffer );
		delete m_dImageBuffer;
		m_dImageBuffer = NULL;
	}

	if (m_dImage)
	{
		clReleaseMemObject( *m_dImage );
		delete m_dImage;
		m_dImage = NULL;
	}

#ifdef _DEBUG_DEEP_OPENCL
	if (m_dDebug)
	{
		clReleaseMemObject( *m_dDebug );
		delete m_dDebug;
		m_dDebug = NULL;
	}

	if (m_pDebug)
	{
		free( m_pDebug );
		m_pDebug = NULL;
	}
#endif
}

void CDirectSunLight::OpenCLInit()
{
	OpenCL *pOpenCL = OpenCL::GetOpenCL();

	cl_int clerr = CL_SUCCESS;
	unsigned int imageDX = m_iCubeSize * 6 * m_iOptiFactorX;
	unsigned int imageDY = m_iCubeSize     * m_iOptiFactorY;
	m_iBlockDX = (unsigned int)ceil((float)imageDX / (float)pOpenCL->GetItemSizeX());
	m_iBlockDY = (unsigned int)ceil((float)imageDY / (float)pOpenCL->GetItemSizeY());
	m_iBlock2DX = m_iBlockDX / 8;
	m_iBlock2DY = m_iBlockDY / 8;

	//Datenobjekte erstellen
	m_pErg = (cl_float *)malloc(m_iBlockDX * m_iBlockDY * sizeof(cl_float) * 3);
	m_pErg2 = (cl_float *)malloc(m_iBlock2DX * m_iBlock2DY * sizeof(cl_float) * 3);
	m_pWichtungOpenCL = (cl_float *)malloc(m_iCubeSize * m_iCubeSize * sizeof(cl_float));
	memcpy(m_pWichtungOpenCL, m_pWichtung, m_iCubeSize * m_iCubeSize * sizeof(cl_float));
#ifdef _DEBUG_DEEP_OPENCL
	m_pDebug = (cl_float *)malloc(imageDX * imageDY * sizeof(cl_float) * 4);
#endif

	//Buffer erstellen
	m_dErg = new cl_mem();
	*m_dErg = clCreateBuffer(*pOpenCL->GetContext(), CL_MEM_READ_WRITE, m_iBlockDX * m_iBlockDY * sizeof(cl_float) * 3, NULL, &clerr);
	CheckOpenCLError(clerr);
	m_dErg2 = new cl_mem();
	*m_dErg2 = clCreateBuffer(*pOpenCL->GetContext(), CL_MEM_WRITE_ONLY, m_iBlock2DX * m_iBlock2DY * sizeof(cl_float) * 3, NULL, &clerr);
	CheckOpenCLError(clerr);
	m_dWichtungOpenCL = new cl_mem();
	*m_dWichtungOpenCL = clCreateBuffer(*pOpenCL->GetContext(), CL_MEM_READ_ONLY,  m_iCubeSize * m_iCubeSize * sizeof(cl_float), NULL, &clerr);
	CheckOpenCLError(clerr);
	m_dImageBuffer = new cl_mem();
	*m_dImageBuffer = clCreateBuffer(*pOpenCL->GetContext(), CL_MEM_READ_ONLY,  m_iCubeSize * 6 * m_iOptiFactorX * m_iCubeSize * m_iOptiFactorY * sizeof(cl_float) * 4, NULL, &clerr);
	CheckOpenCLError(clerr);
	m_dImage = new cl_mem();
	*m_dImage = clCreateFromGLTexture2D(*pOpenCL->GetContext(), CL_MEM_READ_ONLY, GL_TEXTURE_RECTANGLE_ARB, 0, m_pFBO->getColorAttachmentID(), &clerr);
	CheckOpenCLError(clerr);
#ifdef _DEBUG_DEEP_OPENCL
	m_dDebug = new cl_mem();
	*m_dDebug = clCreateBuffer(*pOpenCL->GetContext(), CL_MEM_WRITE_ONLY, imageDX * imageDY * sizeof(cl_float) * 4, NULL, &clerr);
	CheckOpenCLError(clerr);
#endif

	//Buffer schreiben
	clerr = clEnqueueWriteBuffer(*pOpenCL->GetQueue(), *m_dWichtungOpenCL, CL_TRUE, 0, m_iCubeSize * m_iCubeSize * sizeof(cl_float), m_pWichtungOpenCL, 0, NULL, NULL);
	CheckOpenCLError(clerr);

//       "__kernel void sum_light(__global float *pErg, __global float *pImage, __global float *pWichtung, unsigned int iDX, unsigned int iDY, __local float * pSharedImage, unsigned int iCubeSize, int iUseNormale, float fNormaleX, float fNormaleY, float fNormaleZ)\n"

	//Variablen des Kernel setzen
	unsigned int iDX(m_iCubeSize * 6 * m_iOptiFactorX), iDY(m_iCubeSize * m_iOptiFactorY);
	unsigned int iCubeSize(m_iCubeSize);
	int iUseNormale(m_iCalculationMode == Beleuchtungsstaerke_klux);
	cl_float vNormale[3];
	vNormale[0] = (cl_float)m_vNormale.x;
	vNormale[1] = (cl_float)m_vNormale.z;
	vNormale[2] = (cl_float)-m_vNormale.y;
	clerr = clSetKernelArg(*pOpenCL->GetKernel(), 0, sizeof(cl_mem), m_dErg);
	CheckOpenCLError(clerr);
	clerr = clSetKernelArg(*pOpenCL->GetKernel(), 1, sizeof(cl_mem), m_dImageBuffer);
	CheckOpenCLError(clerr);
	clerr = clSetKernelArg(*pOpenCL->GetKernel(), 2, sizeof(cl_mem), m_dWichtungOpenCL);
	CheckOpenCLError(clerr);
	clerr = clSetKernelArg(*pOpenCL->GetKernel(), 3, sizeof(unsigned int), &iDX);
	CheckOpenCLError(clerr);
	clerr = clSetKernelArg(*pOpenCL->GetKernel(), 4, sizeof(unsigned int), &iDY);
	CheckOpenCLError(clerr);
	clerr = clSetKernelArg(*pOpenCL->GetKernel(), 5, sizeof(cl_float) * pOpenCL->GetItemSizeX() * pOpenCL->GetItemSizeY() * 3, NULL);
	CheckOpenCLError(clerr);
	clerr = clSetKernelArg(*pOpenCL->GetKernel(), 6, sizeof(unsigned int), &iCubeSize);
	CheckOpenCLError(clerr);
	clerr = clSetKernelArg(*pOpenCL->GetKernel(), 7, sizeof(int), &iUseNormale);
	CheckOpenCLError(clerr);
	/*clerr = clSetKernelArg(*pOpenCL->GetKernel(), 8, sizeof(cl_float), &vNormale[0]);
	CheckOpenCLError(clerr);
	clerr = clSetKernelArg(*pOpenCL->GetKernel(), 9, sizeof(cl_float), &vNormale[1]);
	CheckOpenCLError(clerr);
	clerr = clSetKernelArg(*pOpenCL->GetKernel(), 10, sizeof(cl_float), &vNormale[2]);
	CheckOpenCLError(clerr);*/
#ifdef _DEBUG_DEEP_OPENCL
	clerr = clSetKernelArg(*pOpenCL->GetKernel(), 11, sizeof(cl_mem), m_dDebug);
	CheckOpenCLError(clerr);
#endif

//		"__kernel void sum_light2(__global float *pErg2, __global float *pErg, unsigned int iDX, unsigned int iDY, __local float * pSharedImage)\n"

	//Variablen des Kernel setzen
	clerr = clSetKernelArg(*pOpenCL->GetKernel2(), 0, sizeof(cl_mem), m_dErg2);
	CheckOpenCLError(clerr);
	clerr = clSetKernelArg(*pOpenCL->GetKernel2(), 1, sizeof(cl_mem), m_dErg);
	CheckOpenCLError(clerr);
	clerr = clSetKernelArg(*pOpenCL->GetKernel2(), 2, sizeof(unsigned int), &m_iBlockDX);
	CheckOpenCLError(clerr);
	clerr = clSetKernelArg(*pOpenCL->GetKernel2(), 3, sizeof(unsigned int), &m_iBlockDY);
	CheckOpenCLError(clerr);
	clerr = clSetKernelArg(*pOpenCL->GetKernel2(), 4, sizeof(cl_float) * pOpenCL->GetItemSizeX() * pOpenCL->GetItemSizeY() * 3, NULL);
	CheckOpenCLError(clerr);

}

void CDirectSunLight::RunOpenCL(unsigned int iNumber, float *pRGB)
{
	OpenCL *pOpenCL = OpenCL::GetOpenCL();
	cl_int clerr = CL_SUCCESS;

	unsigned int itemBlocksX = (m_iCubeSize * 6) / (pOpenCL->GetItemSizeX() * pOpenCL->GetItemSizeX());
	unsigned int itemBlocksY = (m_iCubeSize)     / (pOpenCL->GetItemSizeY() * pOpenCL->GetItemSizeY());

	unsigned int imageDX = m_iCubeSize * 6 * m_iOptiFactorX;
	unsigned int imageDY = (unsigned int)ceil((float)iNumber / m_iOptiFactorX) * m_iCubeSize;
	if (imageDY > m_iCubeSize * m_iOptiFactorY)
		imageDY = m_iCubeSize * m_iOptiFactorY;
	unsigned int local_iBlockDX = m_iBlockDX;
	unsigned int local_iBlockDY = (unsigned int)ceil((float)imageDY / pOpenCL->GetItemSizeY());
	unsigned int local_iBlock2DX = m_iBlock2DX;
	unsigned int local_iBlock2DY = (unsigned int)ceil((float)local_iBlockDY / pOpenCL->GetItemSizeY());

	//dImage "holen"
	clerr = clEnqueueAcquireGLObjects(*pOpenCL->GetQueue(), 1, m_dImage, 0, NULL, NULL);
	CheckOpenCLError(clerr);

	//Buffer kopieren
	size_t src_origin[3] = {0, 0, 0};
	size_t region[3] = {imageDX, imageDY, 1};
	clerr = clEnqueueCopyImageToBuffer(*pOpenCL->GetQueue(), *m_dImage, *m_dImageBuffer, src_origin, region, 0, 0, NULL, NULL);
	CheckOpenCLError(clerr);

	//Variablen aktualisieren
	int iUseNormale(m_iCalculationMode == Beleuchtungsstaerke_klux);
	cl_float vNormale[3];
	vNormale[0] = (cl_float)m_vNormale.x;
	vNormale[1] = (cl_float)m_vNormale.z;
	vNormale[2] = (cl_float)-m_vNormale.y;
	clerr = clSetKernelArg(*pOpenCL->GetKernel(), 7, sizeof(int), &iUseNormale);
	CheckOpenCLError(clerr);
	/*clerr = clSetKernelArg(*pOpenCL->GetKernel(), 8, sizeof(cl_float), &vNormale[0]);
	CheckOpenCLError(clerr);
	clerr = clSetKernelArg(*pOpenCL->GetKernel(), 9, sizeof(cl_float), &vNormale[1]);
	CheckOpenCLError(clerr);
	clerr = clSetKernelArg(*pOpenCL->GetKernel(), 10, sizeof(cl_float), &vNormale[2]);
	CheckOpenCLError(clerr);*/

	//Kernel starten
	cl_event event;
	size_t Total_size[2], Item_size[2];
	Total_size[0] = (size_t)(ceil((float)imageDX / pOpenCL->GetItemSizeX()) * pOpenCL->GetItemSizeX());
	Total_size[1] = (size_t)(ceil((float)imageDY / pOpenCL->GetItemSizeY()) * pOpenCL->GetItemSizeY());
	Item_size[0] = pOpenCL->GetItemSizeX();
	Item_size[1] = pOpenCL->GetItemSizeY();
	clerr = clEnqueueNDRangeKernel(*pOpenCL->GetQueue(), *pOpenCL->GetKernel(), 2, NULL, Total_size, Item_size, 0, NULL, &event);
	CheckOpenCLError(clerr);

	clerr = clWaitForEvents(1, &event);
	CheckOpenCLError(clerr);

	clerr = clReleaseEvent(event);
	CheckOpenCLError(clerr);

#ifdef _DEBUG_DEEP_OPENCL
	unsigned int imageDX2 = m_iCubeSize * 6 * m_iOptiFactorX;
	unsigned int imageDY2 = m_iCubeSize     * m_iOptiFactorY;
	//Debug Buffer lesen
	clerr = clEnqueueReadBuffer(*pOpenCL->GetQueue(), *m_dDebug, CL_TRUE, 0, imageDX2 * imageDY2 * sizeof(cl_float) * 4, m_pDebug, 0, NULL, NULL);
	CheckOpenCLError(clerr);
	float upScale = 1.0; // 50000.0f;

	//opencv Test
	int cv_res_x = imageDX2;
	int cv_res_y = imageDY2;
	IplImage* tmpimg = cvCreateImage( cvSize(cv_res_x, cv_res_y), IPL_DEPTH_8U, 3); 

	int x,y;
	//COLORREF scan;
	for(y=0; y<cv_res_y; y++)
		for(x=0; x<cv_res_x; x++)
		{
			float * pRGB = m_pDebug + (((y * cv_res_x) + x) * 4);

			unsigned char & r = *((unsigned char*)tmpimg->imageData + ((cv_res_y-y-1) * cv_res_x + x) * 3 + 0);
			unsigned char & g = *((unsigned char*)tmpimg->imageData + ((cv_res_y-y-1) * cv_res_x + x) * 3 + 1);
			unsigned char & b = *((unsigned char*)tmpimg->imageData + ((cv_res_y-y-1) * cv_res_x + x) * 3 + 2);

			float fr = floor((*pRGB++) * upScale * 255.0f + 0.5f);
			float fg = floor((*pRGB++) * upScale * 255.0f + 0.5f);
			float fb = floor((*pRGB  ) * upScale * 255.0f + 0.5f);

			b = (fr < 0.0f) ? 0 : ((fr > 255.0f) ? 255 : (unsigned char) fr);
			g = (fg < 0.0f) ? 0 : ((fg > 255.0f) ? 255 : (unsigned char) fg);
			r = (fb < 0.0f) ? 0 : ((fb > 255.0f) ? 255 : (unsigned char) fb);
		}

	cvSaveImage("debugImage.bmp",tmpimg);      //tempor�re Darstellung
	cvReleaseImage( &tmpimg );
#endif

	//Kernel2 starten
	Total_size[0] = (size_t)(ceil((float)local_iBlockDX / pOpenCL->GetItemSizeX()) * pOpenCL->GetItemSizeX());
	Total_size[1] = (size_t)(ceil((float)local_iBlockDY / pOpenCL->GetItemSizeY()) * pOpenCL->GetItemSizeY());
	Item_size[0] = pOpenCL->GetItemSizeX();
	Item_size[1] = pOpenCL->GetItemSizeY();
	clerr = clEnqueueNDRangeKernel(*pOpenCL->GetQueue(), *pOpenCL->GetKernel2(), 2, NULL, Total_size, Item_size, 0, NULL, &event);
	CheckOpenCLError(clerr);

	clerr = clWaitForEvents(1, &event);
	CheckOpenCLError(clerr);

	clerr = clReleaseEvent(event);
	CheckOpenCLError(clerr);

	//dImage wieder freigeben
	clerr = clEnqueueReleaseGLObjects(*pOpenCL->GetQueue(), 1, m_dImage, 0, NULL, NULL);
	CheckOpenCLError(clerr);

	//Buffer lesen
	clerr = clEnqueueReadBuffer(*pOpenCL->GetQueue(), *m_dErg2, CL_TRUE, 0, local_iBlock2DX * local_iBlock2DY * sizeof(cl_float) * 3, m_pErg2, 0, NULL, NULL);
	CheckOpenCLError(clerr);

	//pRGB 0 setzen
	float * pWalker = pRGB;
	for(unsigned int j = 0; j < iNumber; ++j)
	{
		(*pWalker++) = 0.0f;
		(*pWalker++) = 0.0f;
		(*pWalker++) = 0.0f;
	}

	//m_pErg2 addieren
	unsigned int max_y;
	max_y = (unsigned int)ceil((float)iNumber / m_iOptiFactorX) * itemBlocksY;
	if (max_y > m_iBlock2DY)
		max_y = m_iBlock2DY;
	float *pWalker2 = m_pErg2;
	for(unsigned int y = 0; y < max_y; ++y)
		for(unsigned int x = 0; x < m_iBlock2DX; ++x)
		{
			unsigned int id = y / itemBlocksY * m_iOptiFactorX + x / itemBlocksX;
			if (id < iNumber)
			{
				pWalker = pRGB + (id * 3);
				(*pWalker++) += (*pWalker2++);
				(*pWalker++) += (*pWalker2++);
				(*pWalker)   += (*pWalker2++);
			}
			else
				break;
		}
}

bool CDirectSunLight::StartOpenCLOnceAndForever()
{
	if (!m_pContext)
		return false;

	m_pContext->BeginGLCommands();

	OpenCLInit();

	m_pContext->EndGLCommands();

	return true;
}

void CDirectSunLight::SetNormale(const Vec3d& normale)
{
	m_vNormale = normale;
	m_vNormale.Normalize();
}

void CDirectSunLight::SetCalculationMode(CalculationMode mode)
{
	m_iCalculationMode = mode;
}
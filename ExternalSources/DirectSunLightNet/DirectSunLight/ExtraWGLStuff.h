#pragma once

#include <glew\glew.h>
#include "gl/glu.h"
#include "stdafx.h"
#include "wglext.h"

//Put this in a .h so that you can include the header in all your other .cpp
extern PFNWGLCREATEPBUFFERARBPROC		wglCreatePbufferARB;  
extern PFNWGLGETPBUFFERDCARBPROC		wglGetPbufferDCARB;  
extern PFNWGLRELEASEPBUFFERDCARBPROC	wglReleasePbufferDCARB;  
extern PFNWGLDESTROYPBUFFERARBPROC		wglDestroyPbufferARB;  

#include "stdafx.h"

//------------------------------------------------------------------------------
// File : FramebufferObject.h
//------------------------------------------------------------------------------
// Copyright (c) 2005 Gordon Wetzstein
//---------------------------------------------------------------------------
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any
// damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any
// purpose, including commercial applications, and to alter it and
// redistribute it freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you
//    must not claim that you wrote the original software. If you use
//    this software in a product, an acknowledgment in the product
//    documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such, and
//    must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source
//    distribution.
//
// -----------------------------------------------------------------------------
//
// Credits:
// Original RenderTexture code: Mark J. Harris
//	parts of the code are used from the original RenderTexture  
//
// -----------------------------------------------------------------------------

//#define _DEBUG_FBO_MESSAGES

#include <glew\glew.h>
#include "FramebufferObject.h"

// float data type definitions
#include <float.h>

// openCV stuff
#include <opencv\cv.h>
#include <opencv\highgui.h>

// openEXR stuff
//#include <IlmImf/ImfCRgbaFile.h>

namespace AS{

/*
	const GLenum buffers[] = {
		GL_COLOR_ATTACHMENT0_EXT,
		GL_COLOR_ATTACHMENT1_EXT,
		GL_COLOR_ATTACHMENT2_EXT,
		GL_COLOR_ATTACHMENT3_EXT
	};
*/
	const GLenum buffers[] = {
		GL_COLOR_ATTACHMENT0,
		GL_COLOR_ATTACHMENT1,
		GL_COLOR_ATTACHMENT2,
		GL_COLOR_ATTACHMENT3
	};
	unsigned long int FramebufferObject::s_OverallMemConsumption = 0;

	// -----------------------------------------------------------------------------

	/// all formats for 8 bit data types rgba
	int			_numInternal8AFormats = 1;
	GLenum	_internal8AColorFormats[]	= {	GL_RGBA8 };
	/// all formats for 16 bit data types rgb
	int			_numInternal8Formats = 1;
	GLenum	_internal8ColorFormats[]		= {	GL_RGB8 };

	/// all formats for 16 bit data types rgba
	int			_numInternal16AFormats = 3;
	GLenum	_internal16AColorFormats[]	= {	GL_RGBA16F_ARB,GL_FLOAT_RGBA16_NV,GL_RGBA_FLOAT16_ATI};
	/// all formats for 16 bit data types rgb
	int			_numInternal16Formats = 3;
	GLenum	_internal16ColorFormats[]	= {		GL_RGB16F_ARB,
																					GL_FLOAT_RGB16_NV,
																					GL_RGB_FLOAT16_ATI	};


	/// all formats for 32 bit data types rgba
	int			_numInternal32AFormats = 3;
	GLenum	_internal32AColorFormats[]	= {	GL_RGBA32F_ARB,
																					GL_FLOAT_RGBA32_NV,
																					GL_RGBA_FLOAT32_ATI	};
	/// all formats for 32 bit data types rgb
	int			_numInternal32Formats = 3;
	GLenum	_internal32ColorFormats[]	= {		GL_RGB32F_ARB,
																					GL_FLOAT_RGB32_NV,
																					GL_RGB_FLOAT32_ATI	};

	GLenum _framebufferStatus;

	// -----------------------------------------------------------------------------

	FramebufferObject::FramebufferObject(const char *strMode, unsigned int textures_) :

		_textureTarget(GL_TEXTURE_RECTANGLE_ARB),
		
		_colorFormat(GL_RGBA),
		_internalColorFormat(GL_RGBA),
		_colorType(GL_UNSIGNED_BYTE),

		_colorAttachmentDepth(FramebufferObject::RGBA_8),

		_depthFormat(GL_DEPTH_COMPONENT),
		 _internalDepthFormat(GL_DEPTH_COMPONENT24),
		_depthType(GL_UNSIGNED_BYTE),
		
		_wrapS(GL_CLAMP_TO_EDGE),
		_wrapT(GL_CLAMP_TO_EDGE),
		_minFilter(GL_NEAREST),
		_magFilter(GL_NEAREST),
		
		_width(512),
		_height(512),
		
		_bFBOSupported(false),
		_bInitialized(false),
		
		_bColorAttachment(false),
		_bDepthAttachment(false),
		_bColorAttachmentRenderTexture(false),
		_bDepthAttachmentRenderTexture(false),

		_bFloatColorBuffer(false),

		m_numberOfTextures(textures_),
		m_MemConsumption(0),
      m_use_mipmapping(false)
	{
#ifdef _DEBUG_FBO_MESSAGES
		std::cout << "textures: " << m_numberOfTextures << std::endl;
#endif
		parseModeString(strMode);
	}

	// -----------------------------------------------------------------------------

	FramebufferObject::~FramebufferObject() {

		if(_bInitialized) {

			if(_bColorAttachment) {
				if(_bColorAttachmentRenderTexture) 
					glDeleteTextures(m_numberOfTextures, &_colorAttachmentID[0]);
				else
					//glDeleteRenderbuffersEXT(1, &_colorAttachmentID[0]);
               glDeleteRenderbuffers(1, &_colorAttachmentID[0]);
			} 

			if(_bDepthAttachment) {
				if(_bDepthAttachmentRenderTexture) 
					glDeleteTextures(1, &_depthAttachmentID);
				else
					//glDeleteRenderbuffersEXT(1, &_depthAttachmentID);
               glDeleteRenderbuffers(1, &_depthAttachmentID);
			}

			//glDeleteFramebuffersEXT(1, &_frameBufferID);
         glDeleteFramebuffers(1, &_frameBufferID);
				
		}

		glFinish();
		s_OverallMemConsumption -= m_MemConsumption;
#ifdef _DEBUG_FBO_MESSAGES
	    cout << "### FBO: overall memory consumption: "<< s_OverallMemConsumption / (_width* _height * 3 )<< " MB" << endl;
#endif

		if(_bPassThroughProgramInitialized)
			glDeleteProgramsARB(1, &_passThroughProgram); 
	}

	// -----------------------------------------------------------------------------

	unsigned int
	FramebufferObject::getWidth() {
		return _width;
	}

	// -----------------------------------------------------------------------------

	unsigned int
	FramebufferObject::getHeight() {
		return _height;
	}

	// -----------------------------------------------------------------------------

	void						
	FramebufferObject::bind(int index) {
		glEnable(_textureTarget);
		glBindTexture(_textureTarget, _colorAttachmentID[index]);
		glTexParameteri(_textureTarget, GL_TEXTURE_WRAP_S, _wrapS);
	  glTexParameteri(_textureTarget, GL_TEXTURE_WRAP_T, _wrapT);
		glTexParameteri(_textureTarget, GL_TEXTURE_MIN_FILTER, _minFilter);
		glTexParameteri(_textureTarget, GL_TEXTURE_MAG_FILTER, _magFilter);
	  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
     if (m_use_mipmapping)
        glGenerateMipmap(_textureTarget);
	}

	// -----------------------------------------------------------------------------

	void						
	FramebufferObject::bindDepth(void) {
		glEnable(_textureTarget);
		glBindTexture(_textureTarget, _depthAttachmentID);
		glTexParameteri(_textureTarget, GL_TEXTURE_WRAP_S, _wrapS);
	  glTexParameteri(_textureTarget, GL_TEXTURE_WRAP_T, _wrapT);
		glTexParameteri(_textureTarget, GL_TEXTURE_MIN_FILTER, _minFilter);
		glTexParameteri(_textureTarget, GL_TEXTURE_MAG_FILTER, _magFilter);
	  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	}

	// -----------------------------------------------------------------------------

	void
	FramebufferObject::bindNearestNeighbor(int index) {

	//      glEnable(_textureTarget);
		glBindTexture(_textureTarget, _colorAttachmentID[index]);
		glTexParameteri(_textureTarget, GL_TEXTURE_WRAP_S, _wrapS);
	   glTexParameteri(_textureTarget, GL_TEXTURE_WRAP_T, _wrapT);
      if (_textureTarget == GL_TEXTURE_RECTANGLE_ARB)
      {
         glTexParameterf(_textureTarget, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
         glTexParameterf(_textureTarget, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      }
      else
      {
		   glTexParameteri(_textureTarget, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		   glTexParameteri(_textureTarget, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      }
	   glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
      if (m_use_mipmapping)
         glGenerateMipmap(_textureTarget);
	}

	// -----------------------------------------------------------------------------

	GLuint
	FramebufferObject::getColorAttachmentID(int index) {

		   return _colorAttachmentID[index];
	}

	// -----------------------------------------------------------------------------

	GLuint
	FramebufferObject::getDepthAttachmentID(void) {

		   return _depthAttachmentID;
	}

	// -----------------------------------------------------------------------------

	bool
	FramebufferObject::initialize(  unsigned int width, unsigned int height ) {

		   if(_bInitialized)
				   return reInitialize( width, height );

		   _width  = width;
		   _height = height;

		   GLenum err = glewInit();
		   if (GLEW_OK != err){
				fprintf(stderr, "GLEW Error: %s\n", glewGetErrorString(err));
			}
		   //if(!glewIsSupported("GL_EXT_framebuffer_object")) {
           if(!glewIsSupported("GL_ARB_framebuffer_object")) {
				   _bFBOSupported = false;
					   return false;
		   }

		   if ((_internalColorFormat != GL_RGB) && (_internalColorFormat != GL_RGBA) && (!glewIsSupported("GL_ARB_color_buffer_float"))) {
				   _bFBOSupported = false;
					   return false;
		   }

		   // create FBO
		   //glGenFramebuffersEXT(1, &_frameBufferID);
         glGenFramebuffers(1, &_frameBufferID);
         checkFramebufferStatus();
		   //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, _frameBufferID);
         glBindFramebuffer(GL_FRAMEBUFFER, _frameBufferID);

		   if(_bColorAttachment) {

				   if(_bColorAttachmentRenderTexture) {

						   // initialize texture
						  // glEnable(_textureTarget);
						   glGenTextures(m_numberOfTextures, &_colorAttachmentID[0]);

						   for (unsigned int j = 0; j < m_numberOfTextures; ++j) {
								   glBindTexture(_textureTarget, _colorAttachmentID[j]);
								   this->bindNearestNeighbor(j);

								   float * texels = new float[_width*_height*4];

								   for(unsigned int x = 0; x < _width*_height*4; ++x)
										   texels[x] = 0.0;

								   glTexImage2D(   _textureTarget,
														   0,
														   _internalColorFormat,
														   _width,
														   _height,
														   0,
														   _colorFormat,
														   _colorType,
														   texels);

                           if (m_use_mipmapping)
                              glGenerateMipmap(_textureTarget);

								   delete [] texels;

								   switch (j)  {
										   case 0:
																   // attach texture to framebuffer color buffer
																   //glFramebufferTexture2DEXT(      GL_FRAMEBUFFER_EXT,
                                                   glFramebufferTexture2D(      GL_FRAMEBUFFER,
	                                                //                                                                       GL_COLOR_ATTACHMENT0_EXT,
                                                                                                                          GL_COLOR_ATTACHMENT0,
	                                                                                                                       _textureTarget,
	                                                                                                                       _colorAttachmentID[j],
	                                                                                                                               0);
																   break;
										   case 1:
																   // attach texture to framebuffer color buffer
																   //glFramebufferTexture2DEXT(      GL_FRAMEBUFFER_EXT,
                                                   glFramebufferTexture2D(      GL_FRAMEBUFFER,
	                                                //                                                                       GL_COLOR_ATTACHMENT1_EXT,
                                                                                                                          GL_COLOR_ATTACHMENT1,
	                                                                                                                       _textureTarget,
	                                                                                                                       _colorAttachmentID[j],
	                                                                                                                               0);
																   break;
										   case 2:
																   // attach texture to framebuffer color buffer
																   //glFramebufferTexture2DEXT(      GL_FRAMEBUFFER_EXT,
                                                   glFramebufferTexture2D(      GL_FRAMEBUFFER,
	                                                //                                                                       GL_COLOR_ATTACHMENT2_EXT,
                                                                                                                          GL_COLOR_ATTACHMENT2,
	                                                                                                                       _textureTarget,
	                                                                                                                       _colorAttachmentID[j],
	                                                                                                                               0);
																   break;
										   case 3:
																   // attach texture to framebuffer color buffer
																   //glFramebufferTexture2DEXT(      GL_FRAMEBUFFER_EXT,
                                                   glFramebufferTexture2D(      GL_FRAMEBUFFER,
	                                                //                                                                       GL_COLOR_ATTACHMENT3_EXT,
                                                                                                                          GL_COLOR_ATTACHMENT3,
	                                                                                                                       _textureTarget,
	                                                                                                                       _colorAttachmentID[j],
	                                                                                                                               0);
																   break;
										   default:
	                                                                                                               // attach texture to framebuffer color buffer
																   //glFramebufferTexture2DEXT(      GL_FRAMEBUFFER_EXT,
                                                   glFramebufferTexture2D(      GL_FRAMEBUFFER,
	                                                //                                                                       GL_COLOR_ATTACHMENT0_EXT,
                                                                                                                          GL_COLOR_ATTACHMENT0,
	                                                                                                                       _textureTarget,
	                                                                                                                       _colorAttachmentID[0],
	                                                                                                                               0);
																   break;
								   }
						   }

						   checkFramebufferStatus();

				   } else {

						   //glGenRenderbuffersEXT(1, &_colorAttachmentID[0]);
                     glGenRenderbuffers(1, &_colorAttachmentID[0]);

						   // initialize color renderbuffer
						   //glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, _colorAttachmentID[0]);
                     glBindRenderbuffer(GL_RENDERBUFFER, _colorAttachmentID[0]);
						   //glRenderbufferStorageEXT(       GL_RENDERBUFFER_EXT,
                     glRenderbufferStorage(       GL_RENDERBUFFER,
																			   _internalColorFormat,
																			   _width,
																				   _height);

						   // attach renderbuffer to framebuffer color buffer
						   //glFramebufferRenderbufferEXT(   GL_FRAMEBUFFER_EXT,
                     glFramebufferRenderbuffer(   GL_FRAMEBUFFER,
	                  //                                                                     GL_COLOR_ATTACHMENT0_EXT,
                                                                                          GL_COLOR_ATTACHMENT0,
	                  //                                                                     GL_RENDERBUFFER_EXT,
                                                                                          GL_RENDERBUFFER,
	                                                                                       _colorAttachmentID[0] );

						   checkFramebufferStatus();

				   }

		   }

		   if(_bDepthAttachment) {

				   if(_bDepthAttachmentRenderTexture) {

						   // initialize depth texture
						   glGenTextures(1, &_depthAttachmentID);
						   glBindTexture(_textureTarget, _depthAttachmentID);
						   glTexImage2D(   _textureTarget,
														   0,
														   _internalDepthFormat,
														   _width,
														   _height,
														   0,
														   _depthFormat,
														   _depthType,
														   NULL);

                     glTexParameterf( _textureTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                     glTexParameterf( _textureTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                     glTexParameterf( _textureTarget, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
                     glTexParameterf( _textureTarget, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
                     glTexParameterf( _textureTarget, GL_DEPTH_TEXTURE_MODE, GL_LUMINANCE);

						   // attach texture to framebuffer color buffer
						   //glFramebufferTexture2DEXT(      GL_FRAMEBUFFER_EXT,
                     if (_internalDepthFormat != GL_DEPTH24_STENCIL8)
                        glFramebufferTexture2D(      GL_FRAMEBUFFER,
							//													   GL_DEPTH_ATTACHMENT_EXT,
                     													   GL_DEPTH_ATTACHMENT,
																				   _textureTarget,
																				   _depthAttachmentID, 0);
                     else
                        glFramebufferTexture2D(      GL_FRAMEBUFFER,
                        //													   GL_DEPTH_ATTACHMENT_EXT,
                                                               GL_DEPTH_STENCIL_ATTACHMENT,
                                                               _textureTarget,
                                                               _depthAttachmentID, 0);

						   checkFramebufferStatus();

				   } else {

						   //glGenRenderbuffersEXT(1, &_depthAttachmentID);
                     glGenRenderbuffers(1, &_depthAttachmentID);

						   // initialize depth renderbuffer
						   //glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, _depthAttachmentID);
                     glBindRenderbuffer(GL_RENDERBUFFER, _depthAttachmentID);
						   //glRenderbufferStorageEXT(       GL_RENDERBUFFER_EXT,
                     glRenderbufferStorage(       GL_RENDERBUFFER,
																				   _internalDepthFormat,
																				   _width,
																				   _height);

						   // attach renderbuffer to framebuffer depth buffer
						   //glFramebufferRenderbufferEXT(   GL_FRAMEBUFFER_EXT,
                     glFramebufferRenderbuffer(   GL_FRAMEBUFFER,
	                  //                                                                     GL_DEPTH_ATTACHMENT_EXT,
                                                                                          GL_DEPTH_ATTACHMENT,
	                  //                                                                     GL_RENDERBUFFER_EXT,
                                                                                          GL_RENDERBUFFER,
	                                                                                       _depthAttachmentID );

						   checkFramebufferStatus();

				   }

		   }

		   _bInitialized = true;
		   _bFBOSupported = true;

	   //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
      glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// mem info
		   int l_bits = 0;
		   switch(_internalColorFormat) {
				   case GL_RGB:						l_bits = 8*3;   break;
				   case GL_RGBA:					   l_bits = 8*4;   break;
				   case GL_RGB16F_ARB:				l_bits = 16*3;  break;
				   case GL_RGBA16F_ARB:				l_bits = 16*4;  break;
				   case GL_RGB32F_ARB:				l_bits = 32*3;  break;
				   case GL_RGBA32F_ARB:				l_bits = 32*4;  break;
				   default:							;
		   }
		   m_MemConsumption +=     _width * _height * l_bits * m_numberOfTextures;
		   s_OverallMemConsumption += m_MemConsumption;
#ifdef _DEBUG_FBO_MESSAGES
		   cout << "### FBO: approximated overall memory consumption: "<< s_OverallMemConsumption / (1024 * 1024 * 8)<< " MB" << endl;
#endif

		   return true;
	}

	// -----------------------------------------------------------------------------

	bool
	FramebufferObject::reInitialize(        unsigned int width, unsigned int height, const char *strMode ) {

		   if(!_bInitialized)
				   return initialize( width, height );

		   if(!_bFBOSupported)
				   return false;

		   _width  = width;
		   _height = height;

		   // ---------------------------------------------------------
		   // delete old configuration

		   //glDeleteFramebuffersEXT(1, &_frameBufferID);
         glDeleteFramebuffers(1, &_frameBufferID);

		   if(_bColorAttachment) {
				   if(_bColorAttachmentRenderTexture)
						   glDeleteTextures(1, &_colorAttachmentID[0]);
				   else
						   //glDeleteRenderbuffersEXT(1, &_colorAttachmentID[0]);
                     glDeleteRenderbuffers(1, &_colorAttachmentID[0]);
		   }

		   if(_bDepthAttachment) {
				   if(_bDepthAttachmentRenderTexture)
						   glDeleteTextures(1, &_depthAttachmentID);
				   else
						   //glDeleteRenderbuffersEXT(1, &_depthAttachmentID);
                     glDeleteRenderbuffers(1, &_depthAttachmentID);
		   }

		   // ---------------------------------------------------------

		   parseModeString(strMode);

		   // ---------------------------------------------------------
		   // create new configuration

		   //glGenFramebuffersEXT(1, &_frameBufferID);
         glGenFramebuffers(1, &_frameBufferID);
		   //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, _frameBufferID);
         glBindFramebuffer(GL_FRAMEBUFFER, _frameBufferID);

		   if(_bColorAttachment) {

				   if(_bColorAttachmentRenderTexture) {

						   // initialize texture
						//   glEnable(_textureTarget);
						   glGenTextures(m_numberOfTextures, &_colorAttachmentID[0]);

						   for (unsigned int j = 0; j < m_numberOfTextures; ++j) {
								   glBindTexture(_textureTarget, _colorAttachmentID[j]);
								   this->bindNearestNeighbor(j);

								   glTexImage2D(   _textureTarget,
														   0,
														   _internalColorFormat,
														   _width,
														   _height,
														   0,
														   _colorFormat,
														   _colorType,
														   NULL);

                           if (m_use_mipmapping)
                              glGenerateMipmap(_textureTarget);

                           switch (j)  {
										   case 0:
																   // attach texture to framebuffer color buffer
																   //glFramebufferTexture2DEXT(      GL_FRAMEBUFFER_EXT,
                                                   glFramebufferTexture2D(      GL_FRAMEBUFFER,
	                                                //                                                                       GL_COLOR_ATTACHMENT0_EXT,
                                                                                                                          GL_COLOR_ATTACHMENT0,
	                                                                                                                       _textureTarget,
	                                                                                                                       _colorAttachmentID[j],
	                                                                                                                               0);
																   break;
										   case 1:
																   // attach texture to framebuffer color buffer
																   //glFramebufferTexture2DEXT(      GL_FRAMEBUFFER_EXT,
                                                   glFramebufferTexture2D(      GL_FRAMEBUFFER,
	                                                //                                                                       GL_COLOR_ATTACHMENT1_EXT,
                                                                                                                          GL_COLOR_ATTACHMENT1,
	                                                                                                                       _textureTarget,
	                                                                                                                       _colorAttachmentID[j],
	                                                                                                                               0);
																   break;
										   case 2:
																   // attach texture to framebuffer color buffer
																   //glFramebufferTexture2DEXT(      GL_FRAMEBUFFER_EXT,
                                                   glFramebufferTexture2D(      GL_FRAMEBUFFER,
	                                                //                                                                       GL_COLOR_ATTACHMENT2_EXT,
                                                                                                                          GL_COLOR_ATTACHMENT2,
	                                                                                                                       _textureTarget,
	                                                                                                                       _colorAttachmentID[j],
	                                                                                                                               0);
																   break;
										   case 3:
																   // attach texture to framebuffer color buffer
																   //glFramebufferTexture2DEXT(      GL_FRAMEBUFFER_EXT,
                                                   glFramebufferTexture2D(      GL_FRAMEBUFFER,
	                                                //                                                                       GL_COLOR_ATTACHMENT3_EXT,
                                                                                                                          GL_COLOR_ATTACHMENT3,
	                                                                                                                       _textureTarget,
	                                                                                                                       _colorAttachmentID[j],
	                                                                                                                               0);
																   break;
										   default:
	                                                                                                               // attach texture to framebuffer color buffer
																   //glFramebufferTexture2DEXT(      GL_FRAMEBUFFER_EXT,
                                                   glFramebufferTexture2D(      GL_FRAMEBUFFER,
	                                                //                                                                       GL_COLOR_ATTACHMENT0_EXT,
                                                                                                                          GL_COLOR_ATTACHMENT0,
	                                                                                                                       _textureTarget,
	                                                                                                                       _colorAttachmentID[0],
	                                                                                                                               0);
																   break;
								   }
						   }

						   checkFramebufferStatus();

				   } else {

						   //glGenRenderbuffersEXT(1, &_colorAttachmentID[0]);
                     glGenRenderbuffers(1, &_colorAttachmentID[0]);

						   // initialize color renderbuffer
						   //glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, _colorAttachmentID[0]);
                     glBindRenderbuffer(GL_RENDERBUFFER, _colorAttachmentID[0]);
						   //glRenderbufferStorageEXT(       GL_RENDERBUFFER_EXT,
                     glRenderbufferStorage(       GL_RENDERBUFFER,
																				   _internalColorFormat,
																				   _width,
																				   _height);

						   // attach renderbuffer to framebuffer color buffer
						   //glFramebufferRenderbufferEXT(   GL_FRAMEBUFFER_EXT,
                     glFramebufferRenderbuffer(   GL_FRAMEBUFFER,
	                  //                                                                     GL_COLOR_ATTACHMENT0_EXT,
	                                                                                       GL_COLOR_ATTACHMENT0,
	                  //                                                                     GL_RENDERBUFFER_EXT,
                                                                                          GL_RENDERBUFFER,
																					   _colorAttachmentID[0] );

						   checkFramebufferStatus();

				   }

		   }

		   if(_bDepthAttachment) {

				   if(_bDepthAttachmentRenderTexture) {

						   // initialize depth texture
						   glGenTextures(1, &_depthAttachmentID);
						   glBindTexture(_textureTarget, _depthAttachmentID);
						   glTexImage2D(   _textureTarget,
																				   0,
																				   _internalDepthFormat,
																				   _width,
																				   _height,
																				   0,
																				   _depthFormat,
																				   _depthType,
																				   NULL);

						   // attach texture to framebuffer color buffer
						   //glFramebufferTexture2DEXT(      GL_FRAMEBUFFER_EXT,
                     glTexParameterf( _textureTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                     glTexParameterf( _textureTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                     glTexParameterf( _textureTarget, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
                     glTexParameterf( _textureTarget, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
                     glTexParameterf( _textureTarget, GL_DEPTH_TEXTURE_MODE, GL_LUMINANCE);

                     if (_internalDepthFormat != GL_DEPTH24_STENCIL8)
                        glFramebufferTexture2D(      GL_FRAMEBUFFER,
                                                            //													   GL_DEPTH_ATTACHMENT_EXT,
                                                            GL_DEPTH_ATTACHMENT,
                                                            _textureTarget,
                                                            _depthAttachmentID, 0);
                     else
                        glFramebufferTexture2D(      GL_FRAMEBUFFER,
                                                            //													   GL_DEPTH_ATTACHMENT_EXT,
                                                            GL_DEPTH_STENCIL_ATTACHMENT,
                                                            _textureTarget,
                                                            _depthAttachmentID, 0);

						   checkFramebufferStatus();

				   } else {

						   //glGenRenderbuffersEXT(1, &_depthAttachmentID);
                     glGenRenderbuffers(1, &_depthAttachmentID);

						   // initialize depth renderbuffer
						   //glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, _depthAttachmentID);
                     glBindRenderbuffer(GL_RENDERBUFFER, _depthAttachmentID);
						   //glRenderbufferStorageEXT(       GL_RENDERBUFFER_EXT,
                     glRenderbufferStorage(       GL_RENDERBUFFER,
																				   _internalDepthFormat,
																				   _width,
										   _height);

						   // attach renderbuffer to framebuffer depth buffer
						   //glFramebufferRenderbufferEXT(   GL_FRAMEBUFFER_EXT,
                     glFramebufferRenderbuffer(   GL_FRAMEBUFFER,
	                  //                                                                     GL_DEPTH_ATTACHMENT_EXT,
                                                                                          GL_DEPTH_ATTACHMENT,
	                  //                                                                     GL_RENDERBUFFER_EXT,
                                                                                          GL_RENDERBUFFER,
	                                                                                       _depthAttachmentID );

						   checkFramebufferStatus();

				   }

		   }

		   return true;
	}

	// -----------------------------------------------------------------------------

	void
	FramebufferObject::beginCapture() {

	   glGetIntegerv(GL_VIEWPORT, _viewport);

	   if(_bInitialized){
				   //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, _frameBufferID);
               glBindFramebuffer(GL_FRAMEBUFFER, _frameBufferID);

				   glViewport(0, 0, _width, _height);

				   for(unsigned int i = 0; i < m_numberOfTextures; ++i) {
						   glDrawBuffer(buffers[i]);
						   glClearColor(0.0,0.0,0.0,0.0);
						   glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
				   }

				   glDrawBuffers(this->m_numberOfTextures, buffers);
		   }
	}

	// -----------------------------------------------------------------------------

	void
	FramebufferObject::endCapture() {

	 if(_bInitialized) {
	//              glDrawBuffers(this->m_numberOfTextures, buffers[0]);
				   //glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
               glBindFramebuffer(GL_FRAMEBUFFER, 0);
	   glViewport(_viewport[0], _viewport[1], _viewport[2], _viewport[3]);
	 }
	}


	// -----------------------------------------------------------------------------

	void
	FramebufferObject::bind() {

	   if(_bInitialized)
			//glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, _frameBufferID);
         glBindFramebuffer(GL_FRAMEBUFFER, _frameBufferID);
	}

	// -----------------------------------------------------------------------------

	void
	FramebufferObject::unBind() {

	 if(_bInitialized) 
		//glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
      glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	// -----------------------------------------------------------------------------

	//void
	//FramebufferObject::readBackTexture(char* img, int Buffer) {
	//
	// if(_bInitialized) 
	//	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	//
	//}

	// -----------------------------------------------------------------------------

	void
	FramebufferObject::parseModeString(const char *modeString) {

		   if (!modeString || strcmp(modeString, "") == 0)
				   return;

		   int  iDepthBits         = 0;
		   bool bHasStencil        = false;
		   bool bBind2D            = false;
		   bool bBindRECT          = false;
		   bool bBindCUBE          = false;

		   _bColorAttachment       = false;
		   _bDepthAttachment       = false;

		   _bFloatColorBuffer      = false;

		   char *mode = _strdup(modeString);

		   vector<string> tokens;
		   char *buf = strtok(mode, " ");
		   while (buf != NULL)
		   {
						   tokens.push_back(buf);
						   buf = strtok(NULL, " ");
		   }


		   for (unsigned int i = 0; i < tokens.size(); i++) {

				   string token = tokens[i];
				   KeyVal kv = getKeyValuePair(token);

#ifdef _DEBUG_FBO_MESSAGES
				   cout << "Token: " << kv.first.c_str() << " = " << kv.second.c_str() << endl;
#endif

				   // ------------------------------------------------
				   // handle RGBA color attachment
				   if ( strcmp(kv.first.c_str(), "rgba") == 0) {

						   _bColorAttachment                       = true;
						   _colorFormat                                    = GL_RGBA;
						   _internalColorFormat    = GL_RGBA;
						   _colorType                                              = GL_UNSIGNED_BYTE;

						   if( strchr(kv.second.c_str(), 't') != NULL ) {
								   _bColorAttachmentRenderTexture = true;
						   } else {
								   _bColorAttachmentRenderTexture = false;
						   }

						   if( kv.second.find("16") != kv.second.npos ) {
								   _internalColorFormat    = GL_RGBA16F_ARB;
								   _colorType                              = GL_HALF_FLOAT_ARB;
	//                              _colorType                              = GL_FLOAT;
								   _bFloatColorBuffer              = true;
						   }
						   if( kv.second.find("32") != kv.second.npos ) {
								   _internalColorFormat    = GL_RGBA32F_ARB;
								   _colorType                              = GL_FLOAT;
								   _bFloatColorBuffer              = true;
						   }

				   // ------------------------------------------------
				   // ------------------------------------------------
				   // handle RGB color attachment
				   } else if ( strcmp(kv.first.c_str(), "rgb") == 0) {

						   _bColorAttachment                       = true;
						   _colorFormat                                    = GL_RGB;
						   _internalColorFormat    = GL_RGB;
						   _colorType                                              = GL_UNSIGNED_BYTE;

						   if( strchr(kv.second.c_str(), 't') != NULL ) {
								   _bColorAttachmentRenderTexture = true;
						   } else {
								   _bColorAttachmentRenderTexture = false;
						   }

						   if( kv.second.find("16") != kv.second.npos ) {
								   _internalColorFormat    = GL_RGB16F_ARB;
								   _colorType                              = GL_HALF_FLOAT_ARB;
								   _bFloatColorBuffer              = true;
						   }
						   if( kv.second.find("32") != kv.second.npos ) {
								   _internalColorFormat    = GL_RGB32F_ARB;
								   _colorType                              = GL_FLOAT;
								   _bFloatColorBuffer              = true;
						   }


				   // ------------------------------------------------
				   // ------------------------------------------------
				   // handle single component color attachments
				   } else if ( strcmp(kv.first.c_str(), "r") == 0) {

						   _bColorAttachment                       = true;
						   _colorFormat                            = GL_RGB;
						   _internalColorFormat            = GL_RGB;
						   _colorType                                      = GL_UNSIGNED_BYTE;

						   if(strchr(kv.second.c_str(), 't') != NULL ) {
								   _bColorAttachmentRenderTexture = true;
						   } else {
								   _bColorAttachmentRenderTexture = false;
						   }

						   if( kv.second.find("16") != kv.second.npos ) {
								   _internalColorFormat    = GL_FLOAT_R16_NV;
								   _colorType                              = GL_HALF_FLOAT_ARB;
								   _bFloatColorBuffer              = true;
						   }
						   if( kv.second.find("32") != kv.second.npos ) {
								   _internalColorFormat    = GL_FLOAT_R32_NV;
								   _colorType                              = GL_FLOAT;
								   _bFloatColorBuffer              = true;
						   }
				   }

				   // ------------------------------------------------
				   // handle depth attachment

				   if ( strcmp(kv.first.c_str(), "depth") == 0) {

						   _bDepthAttachment                       = true;
						   _depthFormat                                    = GL_DEPTH_COMPONENT;
						   //_depthType                                              = GL_UNSIGNED_BYTE;
                     _depthType                                              = GL_FLOAT;
						   _internalDepthFormat    = GL_DEPTH_COMPONENT24;

						   if( kv.second.find("t") != kv.second.npos ) {
								   _bDepthAttachmentRenderTexture = true;
						   } else {
								   _bDepthAttachmentRenderTexture = false;
						   }

						   if( kv.second.find("16") != kv.second.npos )
								   _internalDepthFormat    = GL_DEPTH_COMPONENT16;
						   if( kv.second.find("24") != kv.second.npos )
								   _internalDepthFormat    = GL_DEPTH_COMPONENT24;
						   if( kv.second.find("32") != kv.second.npos )
								   _internalDepthFormat    = GL_DEPTH_COMPONENT32;
                     if( kv.second.find("24_8") != kv.second.npos )
                     {
                        _depthFormat            = GL_DEPTH_STENCIL;
                        _depthType              = GL_UNSIGNED_INT_24_8;
                        _internalDepthFormat    = GL_DEPTH24_STENCIL8;
                     }
				   }

               // ------------------------------------------------
               // handle texture target type

               if ( strcmp(kv.first.c_str(), "target") == 0) {

                  _textureTarget = GL_TEXTURE_RECTANGLE_ARB;

                  if( kv.second.find("texture_2D") != kv.second.npos ) {
                     _textureTarget = GL_TEXTURE_2D;
                  } else
                  if( kv.second.find("texture_rectangle") != kv.second.npos ) {
                     _textureTarget = GL_TEXTURE_RECTANGLE_ARB;
                  }
               }

               // ------------------------------------------------
               // handle mipmapping

               if ( strcmp(kv.first.c_str(), "mipmapping") == 0) {

                  m_use_mipmapping = false;

                  if( kv.second.find("on") != kv.second.npos ) {
                     m_use_mipmapping = true;
                  } else
                     if( kv.second.find("off") != kv.second.npos ) {
                        m_use_mipmapping = false;
                     }
               }
		   }
         free( mode );
	}

	// -----------------------------------------------------------------------------

	FramebufferObject::KeyVal
	FramebufferObject::getKeyValuePair(string token) {

		   string::size_type pos = 0;
		   if ((pos = token.find("=")) != token.npos) {

						   string key = token.substr(0, pos);
						   string value = token.substr(pos+1, token.length()-pos+1);
						   return KeyVal(key, value);
		   }
		   else
						   return KeyVal(token, "");
	}

	// -----------------------------------------------------------------------------

	bool
	FramebufferObject::checkFramebufferStatus(void) {

		   //GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
         GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

		   switch(status) {

				   //case GL_FRAMEBUFFER_COMPLETE_EXT:
               case GL_FRAMEBUFFER_COMPLETE:
						   //cout << "FramebufferObject ERROR: GL_FRAMEBUFFER_COMPLETE_EXT" << endl;
						   return true;
						   break;

				   //case GL_FRAMEBUFFER_UNSUPPORTED_EXT:
				   case GL_FRAMEBUFFER_UNSUPPORTED:
						   cout << "FramebufferObject ERROR: GL_FRAMEBUFFER_UNSUPPORTED" << endl;
						   break;

				   //case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT_EXT:
               case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
						   cout << "FramebufferObject ERROR: GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT" << endl;
						   break;

				   //case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_EXT:
               case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
						   cout << "FramebufferObject ERROR: GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT" << endl;
						   break;
/*
				   //case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:
               case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
						   cout << "FramebufferObject ERROR: GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS" << endl;
						   break;

				   //case GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT:
               case GL_FRAMEBUFFER_INCOMPLETE_FORMATS:
						   cout << "FramebufferObject ERROR: GL_FRAMEBUFFER_INCOMPLETE_FORMATS" << endl;
						   break;
*/
				   //case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT:
               case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
						   cout << "FramebufferObject ERROR: GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER" << endl;
						   break;

				   //case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT:
               case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
						   cout << "FramebufferObject ERROR: GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER" << endl;
						   break;

				   //case GL_FRAMEBUFFER_BINDING_EXT:
               case GL_FRAMEBUFFER_BINDING:
						   cout << "FramebufferObject ERROR: GL_FRAMEBUFFER_BINDING" << endl;
						   break;

	//        Not supported in actual glext.h
				   //case GL_FRAMEBUFFER_STATUS_ERROR_EXT:
				   //      cout << "FramebufferObject ERROR: GL_FRAMEBUFFER_STATUS_ERROR_EXT" << endl;
				   //      break;

				   default:
						   cout << "FramebufferObject ERROR: unidentified error!" << endl;
						   break;
		   }
		   getchar();
		   return false;
	}

	void
	FramebufferObject::save(char* filename, int id){

		   IplImage *IPlimg = cvCreateImage(cvSize(_width,_height), IPL_DEPTH_8U, 3);
		   glBindTexture(GL_TEXTURE_RECTANGLE_ARB , getColorAttachmentID(id) );
		   glGetTexImage(GL_TEXTURE_RECTANGLE_ARB, 0, GL_BGR, GL_UNSIGNED_BYTE, IPlimg->imageData);
		   cvSaveImage(filename, IPlimg);
		   cvReleaseImage(&IPlimg);
	 }

	void
	FramebufferObject::show(int id){

		   IplImage *IPlimg = cvCreateImage(cvSize(_width,_height), IPL_DEPTH_8U, 4);
		   glBindTexture(GL_TEXTURE_RECTANGLE_ARB , getColorAttachmentID(id) );
		   glGetTexImage(GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGBA, GL_UNSIGNED_BYTE, IPlimg->imageData);
		   cvNamedWindow("FBOIMAGE",1);
		   cvvShowImage("FBOIMAGE",IPlimg);
		   cvReleaseImage(&IPlimg);
	 }

	void
	FramebufferObject::saveHDR(char* filename, int id, bool displaystats){
#if 0	
	//#ifdef DEBUG_SAVES_
	       int channels = 3;
	       ImfHalf *pixels = new ImfHalf[_width*_height*3];
	
	       int w_ = _width;
	       int h_ = _height;
	
	       glBindTexture(GL_TEXTURE_RECTANGLE_ARB , getColorAttachmentID(id) );
	       glGetTexImage(GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGB, GL_HALF_FLOAT_NV, pixels);
	//      glReadPixels(0,0,_width,_height,GL_BGRA,GL_HALF_FLOAT_NV,pixels);
	       ImfRgba *imageData = new ImfRgba[w_*h_];
	       int i, count;
	       float max_ = FLT_MIN;
	       float min_ = FLT_MAX;
	       float tmp = 0.0f;
	       for(i=0, count=0; i<w_*h_*3; i+=3, count++) {
	               tmp = ImfHalfToFloat(pixels[i]);
	               max_=max(max_, tmp);
	               min_=min(min_, tmp);
	               tmp = ImfHalfToFloat(pixels[i+1]);
	               max_=max(max_, tmp);
	               min_=min(min_, tmp);
	               tmp = ImfHalfToFloat(pixels[i+2]);
	               max_=max(max_, tmp);
	               min_=min(min_, tmp);
	
	               imageData[count].r = pixels[i];
	               imageData[count].g = pixels[i+1];
	               imageData[count].b = pixels[i+2];
	               imageData[count].a = 0;
	       }
	       std::cout<<filename<<" max:" << max_ << "  min:" << min_ << std::endl;
	
	       // header
	       ImfHeader *header = ImfNewHeader();
	       ImfHeaderSetDisplayWindow(header, 0, 0, w_-1, h_-1);
	       ImfHeaderSetDataWindow(header, 0, 0, w_-1, h_-1);
	       ImfHeaderSetScreenWindowWidth(header, static_cast<float>(w_) );
	       ImfHeaderSetScreenWindowCenter(header, (w_-1)/2.0f, (h_-1)/2.0f );
	       ImfHeaderSetCompression(header, IMF_PIZ_COMPRESSION);
	               // output file
	       ImfOutputFile *file = ImfOpenOutputFile(filename, header, IMF_WRITE_RGBA);
	       ImfOutputSetFrameBuffer(file, imageData, 1, w_);
	       ImfOutputWritePixels(file, h_);
	       ImfCloseOutputFile(file);
	       ImfDeleteHeader(header);
	
	       if(displaystats) {
	
	               float * fpixels  = new float[_width*_height*3];
	               glGetTexImage(GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGB, GL_FLOAT, fpixels);
	
	               int pixelcount = _width * _height;
	               int breakcond = pixelcount * 3;
	
	               GLfloat avg[4] = {0.0,0.0,0.0,0.0};
	               GLfloat maxVal[3] = {0.0f,0.0f,0.0f};
	               GLfloat minVal[3] = {FLT_MAX,FLT_MAX,FLT_MAX};
	               GLfloat avg_[4] = {0.0,0.0,0.0,0.0};
	               GLfloat max_[3] = {0.0f,0.0f,0.0f};
	               GLfloat min_[3] = {FLT_MAX,FLT_MAX,FLT_MAX};
	
	               bool useSSE = false;
	
	               int count = 0;
	               for(int j = 0; j < breakcond  ; j+=3) {
	
	                       // check if in the borders...
	                       if(fpixels[j] >= 0.0) {
	
	                               // find max value:
	                               if( (   fpixels[j] != 1.0) &&
	                                       (       fpixels[j+1] != 1.0) &&
	                                       (       fpixels[j+2] != 1.0) ) {
	                                       if(maxVal[0] < fpixels[j])
	                                               maxVal[0] = fpixels[j];
	                                       if(maxVal[1] < fpixels[j+1])
	                                               maxVal[1] = fpixels[j+1];
	                                       if(maxVal[2] < fpixels[j+2])
	                                               maxVal[2] = fpixels[j+2];
	                               }
	                               // find min value:
	                               if( (   fpixels[j] != 0.0) &&
	                                       (       fpixels[j+1] != 0.0) &&
	                                       (       fpixels[j+2] != 0.0) ) {
	                                       if(minVal[0] > fpixels[j])
	                                               minVal[0] = fpixels[j];
	                                       if(minVal[1] > fpixels[j+1])
	                                               minVal[1] = fpixels[j+1];
	                                       if(minVal[2] > fpixels[j+2])
	                                               minVal[2] = fpixels[j+2];
	                                       }
	                               // find avg value:
	                               avg[0] = avg[0] + fpixels[j];
	                               avg[1] = avg[1] + fpixels[j+1];
	                               avg[2] = avg[2] + fpixels[j+2];
	                               count++;
	                       }
	               }
	                       avg_[0] = avg[0] / static_cast<float>(count);
	                       avg_[1] = avg[1] / static_cast<float>(count);
	                       avg_[2] = avg[2] / static_cast<float>(count);
	                       min_[0] = minVal[0];
	                       min_[1] = minVal[1];
	                       min_[2] = minVal[2];
	                       max_[0] = maxVal[0];
	                       max_[1] = maxVal[1];
	                       max_[2] = maxVal[2];
	                       std::cout << " STATISTICS OF: " << filename << std::endl;
	                               std::cout << " max values -   R: " << max_[2] <<
	                                                                                       " G: " << max_[1] <<
	                                                                                       " B: " << max_[0] << std::endl;
	                               std::cout << " min values -   R: " << min_[2] <<
	                                                                                       " G: " << min_[1] <<
	                                                                                       " B: " << min_[0] << std::endl;
	                               std::cout << " avg values -   R: " << avg_[2] <<
	                                                                                       " G: " << avg_[1] <<
	                                                                                       " B: " << avg_[0] << std::endl;
	               delete [] fpixels;
	       }//if(displaystats)
	
	       delete [] imageData;
	       delete [] pixels;
	//#endif
#endif
	       }
}
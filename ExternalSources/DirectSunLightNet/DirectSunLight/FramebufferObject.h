//------------------------------------------------------------------------------
// File : FramebufferObject.h
//------------------------------------------------------------------------------
// Copyright (c) 2005 Gordon Wetzstein
//---------------------------------------------------------------------------
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any
// damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any
// purpose, including commercial applications, and to alter it and
// redistribute it freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you
//    must not claim that you wrote the original software. If you use
//    this software in a product, an acknowledgment in the product
//    documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such, and
//    must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source
//    distribution.
//
// -----------------------------------------------------------------------------
//
// Credits:
// Original RenderTexture code: Mark J. Harris
//	parts of the code are used from the original RenderTexture  
//
// -----------------------------------------------------------------------------
/*
* The pixel format for the pbuffer is controlled by the mode string passed
* into the FramebufferObject constructor. This string can have the 
*	following attributes:
*
* To specify the pixel format, use the following syntax.
*   <channels>=<bits>
* <channels> must match one of the following.
*
*	rgba=n[t]		- currently only RGBA and RGB color attachments are supported, 
*								n=8, 16 or 32; 16 and 32 are half-float / float buffers,
*								t for RenderTexture support
*
* The following other attributes are supported.
*
* depth=n[t]	- must have n-bit depth buffer, omit n for default (24 bits),
*								n can be 16, 24 or 32 whereas 16 did not work on my machine,
*								t for RenderTexture support!							
*
*	-----------------------------------------------------------------------------
*	TODO:
*	
*	- support 32 Bit color attachments
*	- support stencil buffer attachments
*	- support more color attachment formats [rg, r, g, b etc.]
*	- test ATI support
*	- float depth buffer
*	- support reinitialization
*	
*/

#pragma once 

// -----------------------------------------------------------------------------

#ifdef WIN32
#ifndef WIN64
	#include <windows.h>
#endif
#endif

#include <glew\glew.h>
#include <gl/gl.h>

#include <iostream>
#include <vector>
using namespace std;

namespace AS {
	// -----------------------------------------------------------------------------

	#define MAX_COLOR_COMPONENTS 15

	// -----------------------------------------------------------------------------

	class FramebufferObject {

	public:
			const char *_strMode;
		// ---------------------------------------------------------------------------

		enum ColorAttachmentType {
			
			RGB_8	= 0,
			RGB_16,
			RGB_32,

			RGBA_8,
			RGBA_16,
			RGBA_32,
		};

		// ---------------------------------------------------------------------------

		FramebufferObject(const char *strMode="rgb=16t depth=24", unsigned int textures_ = 1);
		~FramebufferObject();

		/// initialize the FBOs and Textures
		bool						initialize(	unsigned int width, unsigned int height );
		bool						reInitialize(	unsigned int width, unsigned int height, const char *strMode="rgb=16t depth=24t" );

		/// enable and bind FBO
		void						beginCapture();
		/// disable and "unbind" FBO
		void						endCapture();

		/// bind the color attachment with the index
		void						bind(int index = 0);
		void						bindNearestNeighbor(int index = 0);

		/// bind the depth attachment
		void						bindDepth(void);

		/// get the Texture ID of the color attachment with the index
		GLuint					getColorAttachmentID(int index = 0);

		/// get the Texture ID of the depth attachment
		GLuint					getDepthAttachmentID(void);

		/// get width of the FBO
		unsigned int		getWidth(void);
		/// get height of the FBO
		unsigned int		getHeight(void);

		/// check errors using FBO extension function
		bool						checkFramebufferStatus(void);
		void						printFramebufferStatus(void);
      bool                 isFBOsupported(void) { return _bFBOSupported; }

		/// enable Texture Target
		void						enableTextureTarget(void) { glEnable(_textureTarget); }
		/// disable Texture Target
		void						disableTextureTarget(void) { glDisable(_textureTarget); }
		ColorAttachmentType			_colorAttachmentDepth;

		/// bind FramebufferObject
		void	bind();

		/// bind normal OpenGL Framebuffer
		void	unBind();

		void	save(char* filename, int id);
		void	saveHDR(char* filename, int id, bool displaystats = false);
		void	show(int id);

	protected:

		/// color attachment depth
		//ColorAttachmentType			_colorAttachmentDepth;

		/// FBO extension supported?
		bool										_bFBOSupported;

		/// FBO initialized?
		bool										_bInitialized;

		/// texture target, default: GL_TEXTURE_RECTANGLE_NV
		GLenum									_textureTarget;

		///	use color attachment?
		bool										_bColorAttachment;
		///	use color as RenderTexture?
		bool										_bColorAttachmentRenderTexture;

		///	use depth attachment?
		bool										_bDepthAttachment;
		///	use depth as RenderTexture?
		bool										_bDepthAttachmentRenderTexture;

		/// format of the color texture, default: GL_RGBA
		GLint										_colorFormat;
		/// internal format of the depth texture, default: GL_RGBA
		GLint										_internalColorFormat;

		/// format of the depth texture, default: GL_DEPTH_COMPONENT
		GLenum									_depthFormat;
		/// internal format of the depth texture, default: GL_DEPTH_COMPONENT24
		///		other options: GL_DEPTH_COMPONENT16, GL_DEPTH_COMPONENT32
		GLenum									_internalDepthFormat;

		/// type of the color attachment, default: GL_UNSIGNED_BYTE
		GLenum									_colorType;
		/// type of the depth attachment, default: GL_UNSIGNED_BYTE
		GLenum									_depthType;

		/// wrap s parameter for color attachments, default: GL_CLAMP_TO_EDGE
		GLint										_wrapS;
		/// wrap t parameter for color attachments, default: GL_CLAMP_TO_EDGE
		GLint										_wrapT;
		
		/// min filterfor texture interpolation, default: GL_LINEAR
		GLint										_minFilter;
		/// mag filterfor texture interpolation, default: GL_LINEAR
		GLint										_magFilter;

		/// width of the FBO
		unsigned int						_width;
		/// height of the FBO
		unsigned int						_height;

		/// ID of the FBO
		GLuint									_frameBufferID;

		/// Texture IDs of the color attachment textures
		GLuint									_colorAttachmentID[MAX_COLOR_COMPONENTS];

		/// Tezture ID of the depth attachment
		GLuint									_depthAttachmentID;

		/// save viewport before setting new one to restore it later
		///	avoid glPushAttrib() and glPopAttrib()!
		GLint										_viewport[4];

		/// shader for float support
		GLuint									_passThroughProgram;
		bool										_bPassThroughProgramInitialized;
		
		/// indicates if color buffer is a float texture
		bool										_bFloatColorBuffer;

	private:

		/// parse the mode string and set configuration
		void										parseModeString(const char *modeString);

		typedef std::pair<std::string, std::string> KeyVal;
		/// get the key=value pair of a single token from the mode string
		KeyVal									getKeyValuePair(std::string token);

		/// the number of textures 
		unsigned int m_numberOfTextures;

		// informatino about the overall FBO GPU memory consumption
		static unsigned long int	s_OverallMemConsumption;
		// informatino about this FBO GPU memory consumption
		unsigned int				m_MemConsumption;
      // for _textureTarget == GL_TEXTURE_2D and mipmapping
      bool                    m_use_mipmapping;
	};
}
#include "stdafx.h"
#include "geom3d.h"
#include "geom2d.h"

using namespace DSL;

 GLenum                             geom3d_render_mode;
 bool                               geom3d_point_switch;
 unsigned __int8                    geom3d_point_counter;
 CGeom3d::Triangle_3d				geom3d_triangle;
 CGeom3d::TriangleVector			geom3d_triangleVector;

    static void CALLBACK BeginCallback(GLenum type)
    {
       geom3d_render_mode = type;
       geom3d_point_counter=0;
       geom3d_point_switch =false;
    }

    static void CALLBACK VertexCallback(GLvoid *vertex) //used by TesselatePVV
    {
       const GLdouble * pointer= (GLdouble *) vertex;
    
       switch (geom3d_point_counter)
       {
          case 0:
             geom3d_triangle.m_P1=Vec3d(pointer[0],pointer[1],pointer[2]);
             geom3d_point_counter++;
             break;
          case 1:
             geom3d_triangle.m_P2=Vec3d(pointer[0],pointer[1],pointer[2]);
             geom3d_point_counter++;
             break;
          case 2:
             geom3d_triangle.m_P3=Vec3d(pointer[0],pointer[1],pointer[2]);
             geom3d_triangleVector.push_back(geom3d_triangle);
             geom3d_point_counter++;
             break;
          case 3:
             switch(geom3d_render_mode)
             {
                case GL_TRIANGLE_FAN:
                   geom3d_triangle.m_P2=geom3d_triangle.m_P3;
                   geom3d_triangle.m_P3=Vec3d(pointer[0],pointer[1],pointer[2]);
                   geom3d_triangleVector.push_back(geom3d_triangle);
                   break;
                case GL_TRIANGLE_STRIP:
                   geom3d_point_switch = !geom3d_point_switch;
                   geom3d_triangle.m_P1=geom3d_triangle.m_P2;
                   geom3d_triangle.m_P2=geom3d_triangle.m_P3;
                   geom3d_triangle.m_P3=Vec3d(pointer[0],pointer[1],pointer[2]);
                   if (geom3d_point_switch)
                   {
                      CGeom3d::Triangle_3d sw;
                      sw.m_P1 = geom3d_triangle.m_P2;
                      sw.m_P2 = geom3d_triangle.m_P1;
                      sw.m_P3 = geom3d_triangle.m_P3;
                      geom3d_triangleVector.push_back(sw);
                   }
                   else
                      geom3d_triangleVector.push_back(geom3d_triangle);
                   break;
                case GL_TRIANGLES:
                   geom3d_triangle.m_P1=Vec3d(pointer[0],pointer[1],pointer[2]);
                   geom3d_point_counter = 1;
                   break;
                default:
                   int a=0;
                   break;
             }
             break;
       }
    }

    static void CALLBACK EndCallback()
    {
    }


   void CGeom3d::TesselatePVV( const Point_3d_Vector_Vector & points, TriangleVector & triangles )
   {
      GLUtesselator * pTess = gluNewTess();
      geom3d_triangleVector.clear();

      Vekt_3d normale = GetNormale( points );

      gluTessNormal(pTess,normale.x,normale.y,normale.z);
      gluTessCallback(pTess,GLU_TESS_BEGIN, (void (CALLBACK*)())&BeginCallback); 
      gluTessCallback(pTess,GLU_TESS_VERTEX,(void (CALLBACK*)())&VertexCallback); 
      gluTessCallback(pTess,GLU_TESS_END,   (void (CALLBACK*)())&EndCallback);
      gluTessProperty(pTess,GLU_TESS_BOUNDARY_ONLY,GL_FALSE);
      gluTessBeginPolygon(pTess, NULL); 
    
      typedef std::vector<Help> GLDV;
      GLDV tempGLdouble(1000);
      unsigned long int s=0;
    
      Point_3d_Vector_Vector::const_iterator polyC_l=points.begin(), polyC_e=points.end();
      for (; polyC_l!=polyC_e; polyC_l++)
      {
         gluTessBeginContour(pTess);
       
         const Point_3d_Vector & pv = *polyC_l;
         Point_3d_Vector::const_iterator point_l=pv.begin(), point_e=pv.end();
    
         for (; point_l!=point_e; s++, point_l++)
         {
            GLdouble coords[3];
            coords[0]=(*point_l).x;
            coords[1]=(*point_l).y;
            coords[2]=(*point_l).z;
    
            Help help;
            help.tmp[0] = coords[0];
            help.tmp[1] = coords[1];
            help.tmp[2] = coords[2];
    
            tempGLdouble[s]=help;
    
            gluTessVertex(pTess,coords,tempGLdouble[s].tmp);
         }
       
         gluTessEndContour(pTess);
      }
    
      gluTessEndPolygon(pTess);
      gluDeleteTess(pTess);

      triangles = geom3d_triangleVector;
   }
/*
CGeom3d::CGeom3d(void)
{
}

CGeom3d::~CGeom3d(void)
{
}
*/

/*
PROCEDURE Vekt_PP_3d (P1,P2:Point_3d; VAR V:Vekt_3d);
BEGIN
 V.x:= P2.x - P1.x;
 V.y:= P2.y - P1.y;
 V.z:= P2.z - P1.z;
END;
*/

/*
void CGeom3d::Vekt_PP_3d (Point_3d P1, Point_3d P2, Vekt_3d & V)
{
   V = P2 - P1;
}
*
/*
PROCEDURE Vekt_ProdV_3d (V1,V2:Vekt_3d; VAR V:Vekt_3d);
BEGIN
 V.x:= V1.y * V2.z - V1.z * V2.y;
 V.y:= V1.z * V2.x - V1.x * V2.z;
 V.z:= V1.x * V2.y - V1.y * V2.x;
END;
*/

/*
void CGeom3d::Vekt_ProdV_3d (Vekt_3d V1, Vekt_3d V2, Vekt_3d & V)
{
   V = V1.amul(V2);
}
*/
/*
PROCEDURE Vekt_ProdS_3d (V1,V2:Vekt_3d; VAR r:Real);
BEGIN
 r:= V1.x * V2.x + V1.y * V2.y + V1.z * V2.z;
END;
*/
/*
void CGeom3d::Vekt_ProdS_3d (Vekt_3d V1, Vekt_3d V2, double & r)
{
   r = V1.imul(V2);
}
*/
/*
FUNCTION Length_V_3d (V:Vekt_3d):Real;
BEGIN
 Length_V_3d:= Sqrt(V.x * V.x + V.y * V.y + V.z * V.z);
END;
*/
/*
double CGeom3d::Length_V_3d (Vekt_3d V)
{
   return V.Value();
}
*/
/*
FUNCTION  Dist_LL_3d (L1,L2:Line_3d):Real;
VAR
 V,R,N: Vekt_3d;
 LenN,LenV,Q: Real;
BEGIN
 Vekt_PP_3d (L1.p,L2.p,R);
 Vekt_ProdV_3d (L1.v,L2.v,N);
 Vekt_ProdS_3d (R,N,q);
 LenN:=Length_V_3d (N);
 Err_3d:= 1;
 IF Abs(LenN) < Eps6 THEN BEGIN { if parallel }
  Vekt_ProdS_3d (R,L1.V,q);
  LenV:= Length_V_3d (L1.V);
  IF Abs(LenV) < Eps6 THEN Err_3d:= 0 ELSE Dist_LL_3d:= q/LenV;
 END ELSE BEGIN { if not parallel }
  Dist_LL_3d:= Q/LenN;
 END;
END;
*/
/*
double CGeom3d::Dist_LL_3d (Line_3d L1, Line_3d L2)
{
   Vekt_3d V,R,N;
   double LenN,LenV,q;

   Vekt_PP_3d (L1.p,L2.p,R);
   Vekt_ProdV_3d (L1.v,L2.v,N);
   Vekt_ProdS_3d (R,N,q);
   LenN=Length_V_3d (N);
   Err_3d= 1;
   if (fabs(LenN) < Eps6) // if parallel
   { 
      Vekt_ProdS_3d (R,L1.v,q);
      LenV= Length_V_3d (L1.v);
      if (fabs(LenV) < Eps6)
      {
         Err_3d= 0; //schneiden sich auf der ganzen Strecke
         return 0.0;
      }
      else 
         return q/LenV;
   } 
   else // if not parallel 
      return q/LenN;
}
*/
/*
PROCEDURE Point_SXY_3d (S:Plane_3d; VAR P:Point_3d);
BEGIN
 IF Abs (S.c) < Eps6 THEN Err_3d:= 0
 ELSE BEGIN
  P.z:= -(S.a * P.x + S.b * P.y + S.d)/S.c;
  Err_3d:= 1;
 END;
END;
*/
/*
void CGeom3d::Point_SXY_3d (Plane_3d S, Point_3d & P)
{
   if (fabs(S.c) < Eps6)
      Err_3d= 0;
   else 
   {
      P.z= -(S.a * P.x + S.b * P.y + S.d)/S.c;
      Err_3d= 1;
   }
}
*/
/*
PROCEDURE Point_SXZ_3d (S:Plane_3d; VAR P:Point_3d);
BEGIN
 IF Abs (S.b) < Eps6 THEN Err_3d:= 0
 ELSE BEGIN
  P.y:= -(S.a * P.x + S.c * P.z + S.d)/S.b;
  Err_3d:= 1;
 END;
END;
*/
/*
void CGeom3d::Point_SXZ_3d (Plane_3d S, Point_3d & P)
{
   if (fabs(S.b) < Eps6)
      Err_3d= 0;
   else
   {
      P.y= -(S.a * P.x + S.c * P.z + S.d)/S.b;
      Err_3d= 1;
   }
}
*/
/*
PROCEDURE Point_SYZ_3d (S:Plane_3d; VAR P:Point_3d);
BEGIN
 IF Abs (S.a) < Eps6 THEN Err_3d:= 0
 ELSE BEGIN
  P.x:= -(S.b * P.y + S.c * P.z + S.d)/S.a;
  Err_3d:= 1;
 END;
END;
*/
/*
void CGeom3d::Point_SYZ_3d (Plane_3d S, Point_3d & P)
{
   if (fabs(S.a) < Eps6)
      Err_3d= 0;
   else
   {
      P.x= -(S.b * P.y + S.c * P.z + S.d)/S.a;
      Err_3d= 1;
   }
}
*/
/*
PROCEDURE Plane_PVV_3d (P:Point_3d; V1,V2:Vekt_3d; VAR S:Plane_3d);
BEGIN
 S.a:= V1.y * V2.z - V1.z * V2.y;
 S.b:= V1.z * V2.x - V1.x * V2.z;
 S.c:= V1.x * V2.y - V1.y * V2.x;
 S.d:= -P.x * S.a - P.y * S.b - P.z * S.c;
 IF (S.a = 0) AND (S.b = 0) AND (S.c = 0) AND (S.d = 0) THEN
 Syntax ('Plane_VVP');
END;
*/
/*
void CGeom3d::Plane_PVV_3d(Point_3d P, Vekt_3d V1, Vekt_3d V2, Plane_3d & S)
{
   Err_3d= 1;

   S.a= V1.y * V2.z - V1.z * V2.y;
   S.b= V1.z * V2.x - V1.x * V2.z;
   S.c= V1.x * V2.y - V1.y * V2.x;
   S.d= -P.x * S.a - P.y * S.b - P.z * S.c;
   if ((S.a == 0.0) && (S.b == 0.0) && (S.c == 0.0) && (S.d == 0.0))
      Err_3d= 0;
}
*/
/*
PROCEDURE Plane_PN_3d (P:Point_3d; N:Vekt_3d; VAR S:Plane_3d);
BEGIN
 S.a:= N.x;
 S.b:= N.y;
 S.c:= N.z;
 S.d:= -P.x * S.a - P.y * S.b - P.z * S.c;
 IF (S.a = 0) AND (S.b = 0) AND (S.c = 0) AND (S.d = 0) THEN Syntax ('Plane_NP');
END;
*/
/*
void CGeom3d::Plane_PN_3d (Point_3d P, Vekt_3d N, Plane_3d & S)
{
   Err_3d= 1;

   S.a= N.x;
   S.b= N.y;
   S.c= N.z;
   S.d= -P.x * S.a - P.y * S.b - P.z * S.c;
   if ((S.a == 0.0) && (S.b == 0.0) && (S.c == 0.0) && (S.d == 0.0))
      Err_3d= 0;
}
*/
/*
PROCEDURE Point_LL_3d (L1,L2:Line_3d; VAR P:Point_3d);
VAR
 Dist: Real;
 S: Plane_3d;
 Pp: Point_2d;
 Ll1,Ll2: Line_2d;
BEGIN
 Dist:= Dist_LL_3d (L1,L2);
 IF Abs(Dist) > Eps6 THEN Err_3d:= 0
 ELSE BEGIN
  Plane_PVV_3d (L1.p,L1.v,L2.v,S);
  IF (Abs (S.a) > Abs (S.b)) AND (Abs (S.a) > Abs (S.c)) THEN BEGIN
   LL1.x:= L1.y; LL1.y:= L1.z;  LL1.a:= L1.b; LL1.b:= L1.c;
   LL2.x:= L2.y; LL2.y:= L2.z;  LL2.a:= L2.b; LL2.b:= L2.c;
   Point_LL_2d (Ll1,Ll2,Pp);
   P.y:= Pp.x; P.z:= Pp.y;
   Point_SYZ_3d (S,P);
  END ELSE
  IF (Abs (S.b) > Abs (S.a)) AND (Abs (S.b) > Abs (S.c)) THEN BEGIN
   LL1.x:= L1.x; LL1.y:= L1.z;  LL1.a:= L1.a; LL1.b:= L1.c;
   LL2.x:= L2.x; LL2.y:= L2.z;  LL2.a:= L2.a; LL2.b:= L2.c;
   Point_LL_2d (Ll1,Ll2,Pp);
   P.x:= Pp.x; P.z:= Pp.y;
   Point_SXZ_3d (S,P);
  END ELSE
  IF (Abs (S.c) > Abs (S.a)) AND (Abs (S.c) > Abs (S.b)) THEN BEGIN
   LL1.x:= L1.x; LL1.y:= L1.y;  LL1.a:= L1.a; LL1.b:= L1.b;
   LL2.x:= L2.x; LL2.y:= L2.y;  LL2.a:= L2.a; LL2.b:= L2.b;
   Point_LL_2d (Ll1,Ll2,Pp);
   P.x:= Pp.x; P.y:= Pp.y;
   Point_SXY_3d (S,P);
  END ELSE BEGIN
   Err_3d:= 0;
   EXIT;
  END;
  Err_3d:=1;
 END;
END;
*/
/*
void CGeom3d::Point_LL_3d (Line_3d L1, Line_3d L2, Point_3d & P, double delta)
{
   double Dist;
   Plane_3d S;
   CGeom2d::Point_2d Pp;
   CGeom2d::Line_2d LL1,LL2;

   Dist= Dist_LL_3d (L1,L2);
   if (fabs(Dist) > delta)
      Err_3d= 0;
   else 
   {
      Plane_PVV_3d (L1.p,L1.v,L2.v,S);
      if ((fabs (S.a) > fabs (S.b)) && (fabs (S.a) > fabs (S.c)))
      {
         LL1.p.x= L1.p.y; LL1.p.y= L1.p.z;  LL1.v.x= L1.v.y; LL1.v.y= L1.v.z;
         LL2.p.x= L2.p.y; LL2.p.y= L2.p.z;  LL2.v.x= L2.v.y; LL2.v.y= L2.v.z;
         Geom2d.Point_LL_2d (LL1,LL2,Pp);
         P.y= Pp.x; P.z= Pp.y;
         Point_SYZ_3d (S,P);
      } else
      if ((fabs (S.b) > fabs (S.a)) && (fabs (S.b) > fabs (S.c)))  
      {
         LL1.p.x= L1.p.x; LL1.p.y= L1.p.z;  LL1.v.x= L1.v.x; LL1.v.y= L1.v.z;
         LL2.p.x= L2.p.x; LL2.p.y= L2.p.z;  LL2.v.x= L2.v.x; LL2.v.y= L2.v.z;
         Geom2d.Point_LL_2d (LL1,LL2,Pp);
         P.x= Pp.x; P.z= Pp.y;
         Point_SXZ_3d (S,P);
      } else
      if ((fabs (S.c) > fabs (S.a)) && (fabs (S.c) > fabs (S.b)))
      {
         LL1.p.x= L1.p.x; LL1.p.y= L1.p.y;  LL1.v.x= L1.v.x; LL1.v.y= L1.v.y;
         LL2.p.x= L2.p.x; LL2.p.y= L2.p.y;  LL2.v.x= L2.v.x; LL2.v.y= L2.v.y;
         Geom2d.Point_LL_2d (LL1,LL2,Pp);
         P.x= Pp.x; P.y= Pp.y;
         Point_SXY_3d (S,P);
      } else 
      {
         Err_3d= 0;
         return;
      }
      Err_3d=1;
   }
}
*/

/*
PROCEDURE Line_SS_3d (S1,S2:Plane_3d;
                      VAR L:Line_3d);
BEGIN
 L.a:= S1.b * S2.c - S1.c * S2.b;
 L.b:= S1.c * S2.a - S1.a * S2.c;
 L.c:= S1.a * S2.b - S1.b * S2.a;
 Err_3d:= 1;
 IF L.a <> 0 THEN BEGIN
  L.x:= 0;
  L.y:= (S1.c * S2.d - S2.c * S1.d)/(S1.b * S2.c - S2.b * S1.c);
  L.z:= (S1.b * S2.d - S2.b * S1.d)/(S2.b * S1.c - S1.b * S2.c);
 END ELSE IF L.b <> 0 THEN BEGIN
  L.x:= (S1.c * S2.d - S2.c * S1.d)/(S1.a * S2.c - S2.a * S1.c);
  L.y:= 0;
  L.z:= (S1.a * S2.d - S2.a * S1.d)/(S2.a * S1.c - S1.a * S2.c);
 END ELSE IF L.c <> 0 THEN BEGIN
  L.x:= (S1.b * S2.d - S2.b * S1.d)/(S1.a * S2.b - S2.a * S1.b);
  L.y:= (S1.a * S2.d - S2.a * S1.d)/(S2.a * S1.b - S1.a * S2.b);
  L.z:= 0;
 END ELSE Err_3d:= 0;
END;
*/
/*
void CGeom3d::Line_SS_3d (Plane_3d S1, Plane_3d S2, Line_3d & L)
{
   L.v.x= S1.b * S2.c - S1.c * S2.b;
   L.v.y= S1.c * S2.a - S1.a * S2.c;
   L.v.z= S1.a * S2.b - S1.b * S2.a;
   Err_3d= 1;
   if (L.v.x != 0.0)
   {
      L.p.x= 0.0;
      L.p.y= (S1.c * S2.d - S2.c * S1.d)/(S1.b * S2.c - S2.b * S1.c);
      L.p.z= (S1.b * S2.d - S2.b * S1.d)/(S2.b * S1.c - S1.b * S2.c);
   } else 
   if (L.v.y != 0.0)
   {
      L.p.x= (S1.c * S2.d - S2.c * S1.d)/(S1.a * S2.c - S2.a * S1.c);
      L.p.y= 0.0;
      L.p.z= (S1.a * S2.d - S2.a * S1.d)/(S2.a * S1.c - S1.a * S2.c);
   } else 
   if (L.v.z != 0.0)
   {
      L.p.x= (S1.b * S2.d - S2.b * S1.d)/(S1.a * S2.b - S2.a * S1.b);
      L.p.y= (S1.a * S2.d - S2.a * S1.d)/(S2.a * S1.b - S1.a * S2.b);
      L.p.z= 0.0;
   } else Err_3d= 0;
}
*/
/*
PROCEDURE Line_PP_3d (P1,P2:Point_3d;
                      VAR L:Line_3d);
BEGIN
 L.a:= P1.x - P2.x;
 L.b:= P1.y - P2.y;
 L.c:= P1.z - P2.z;
 L.x:= P1.x;
 L.y:= P1.y;
 L.z:= P1.z;
END;
*/
/*
void CGeom3d::Line_PP_3d (Point_3d P1, Point_3d P2, Line_3d & L)
{
   L.v.x= P1.x - P2.x;
   L.v.y= P1.y - P2.y;
   L.v.z= P1.z - P2.z;
   L.p.x= P1.x;
   L.p.y= P1.y;
   L.p.z= P1.z;
}
*/
//Funktion von Torsten
/*
bool CGeom3d::InLine_3d (Point_3d P1, Point_3d P2, Point_3d P, double Punktmindestabstand)
{
   Vec3d r1=P2-P1;
   Vec3d r2=P -P1;

   if (r2.Value()>Punktmindestabstand)
   {
      double d = cos(r1,r2)*r2.Value();
      double h = sin(r1,r2)*r2.Value();

      return (d>=-Punktmindestabstand) && (d<=r1.Value()+Punktmindestabstand) && (h <= Punktmindestabstand);
   }
   else
      return true;
}
*/
/*
bool CGeom3d::InLineWithoutEndPoints_3d (Point_3d P1, Point_3d P2, Point_3d P, double Punktmindestabstand)
{
   Vec3d r1=P2-P1;
   Vec3d r2=P -P1;

   if (r2.Value()>Punktmindestabstand)
   {
      double d = cos(r1,r2)*r2.Value();
      double h = sin(r1,r2)*r2.Value();

      return (d>=Punktmindestabstand) && (d<=r1.Value()-Punktmindestabstand) && (h <= Punktmindestabstand);
   }
   else
      return false;
}
*/
/*
FUNCTION  Dist_PS_3d (P:Point_3d; S:Plane_3d):Real;
VAR
 Len:Real;
BEGIN
 Len:= Sqrt(S.a * S.a + S.b * S.b + S.c * S.c);
 IF Abs (Len)< Eps6 THEN Syntax (' Dist_PS_3d ');
 S.a:= S.a/Len;
 S.b:= S.b/Len;
 S.c:= S.c/Len;
 S.d:= S.d/Len;
 Dist_PS_3d:= ( S.a * P.x + S.b * P.y + S.c * P.z + S.d) / Len;
END;
*/
/*
double CGeom3d::Dist_PS_3d (Point_3d P, Plane_3d S)
{
   double Len;

   Len= sqrt(S.a * S.a + S.b * S.b + S.c * S.c);
   if (fabs (Len)< Eps6) 
   {
      Err_3d=0;
      return 0.0;
   }
   else
   {
      S.a= S.a/Len;
      S.b= S.b/Len;
      S.c= S.c/Len;
      S.d= S.d/Len;
      Err_3d=1;
      return ( S.a * P.x + S.b * P.y + S.c * P.z + S.d) / Len;
   }
}
*/
/*
FUNCTION UpperPP_2d (P,P1,P2:Point_2d):Boolean;
VAR
 W: Real;
BEGIN
 W := (P.x - P1.x) * (P2.y - P1.y) - (P2.x - P1.x) * (P.y - P1.y);
 IF W < 0 THEN UpperPP_2d:= False ELSE UpperPP_2d:= True;
END;
*/
/*
bool CGeom3d::UpperPP_XY(Point_3d P, Point_3d P1, Point_3d P2)
{
   double W;
   W = (P.x - P1.x) * (P2.y - P1.y) - (P2.x - P1.x) * (P.y - P1.y);
   if (W < 0) 
      return false; 
   else 
      return true;
}
*/
/*
void CGeom3d::Vekt_Rectangular(Vec3d V, Vec3d &erg)
{
   double dummy;
   erg=V;

   if ((fabs(erg.x)<=fabs(erg.y)) && (fabs(erg.x)<=fabs(erg.z)))
   {
      //y und z tauschen
      dummy=erg.y;
      erg.y=erg.z;
      erg.z=-dummy;
      erg.x=0.0;
   }
   else
   if ((fabs(erg.y)<=fabs(erg.x)) && (fabs(erg.y)<=fabs(erg.z)))
   {
      //x und z tauschen
      dummy=erg.x;
      erg.x=erg.z;
      erg.z=-dummy;
      erg.y=0.0;
   }
   else
   {
      //x und y tauschen
      dummy=erg.x;
      erg.x=erg.y;
      erg.y=-dummy;
      erg.z=0.0;
   }
   erg.Normalize();
}
*/
//InPoly mit Vec3d

/*
bool CGeom3d::In_Loop_Plane_3d(Geometry::Polygon::PointVector * pPVV, Point_3d point, double rand)
{
   Vec3d schnittvektor, normale=GetNormale(*pPVV);
   Line_3d schnittlinie,linie;
   Point_3d schnittpunkt,lp1,lp2;
   Geometry::Polygon::PointVector::iterator l2,l3,e2,l2_next,l2_last;
   PointVector schnittpunkte;
   EckenSchnittpunktVector ecken_schnittpunkte;
   EckenSchnittpunktVector::iterator l_ecke, l2_ecke, e_ecke;
   ecken_schnittpunkt sort_dummy;
   PointVector::iterator sl,sl2,se;
   Plane_3d plane, sort_plane;
   double dist, dist1, dist2;
   long upper_points=0, lower_points=0, ecken_anz=0;
   bool found, up_inside, down_inside;

   Vekt_Rectangular(normale, schnittvektor);
   Line_VP_3d(schnittvektor,point,schnittlinie);
   Plane_PN_3d(point,schnittvektor,plane);
   Plane_PN_3d(point,schnittvektor.amul(normale),sort_plane);

   l2=pPVV->begin();
   e2=pPVV->end();

   for (;l2!=e2; l2++)
   {
      l3=l2+1;
      if (l3==e2)
         l3=pPVV->begin();

      lp1.x=(**l2).getX();
      lp1.y=(**l2).getY();
      lp1.z=(**l2).getZ();
      lp2.x=(**l3).getX();
      lp2.y=(**l3).getY();
      lp2.z=(**l3).getZ();
      if ((lp1-lp2).Value()<rand) continue;
      Line_PP_3d(lp1,lp2,linie);

      //sind wir auf dem Rand ?
      if (InLine_3d(lp1,lp2,point,rand))
         return true;

      Point_LL_3d(schnittlinie,linie,schnittpunkt,rand);
      if ((Err_3d==1) && (InLine_3d(lp1,lp2,schnittpunkt,rand)))
      {
         schnittpunkte.push_back(schnittpunkt);
         dist=Dist_PS_3d(schnittpunkt,plane);
         if (dist>0.0)
            upper_points++;
         else
            lower_points++;
      }
   }

   //doppelte Schnittpunkte l�schen
   sl=schnittpunkte.begin();
   for (;sl!=schnittpunkte.end(); sl++)
   {
      sl2=sl;
      sl2++;

      for (;sl2!=schnittpunkte.end();)
         if (((*sl)-(*sl2)).Value()<rand)
         {
            dist=Dist_PS_3d(*sl2,plane);
            if (dist>0.0)
               upper_points--;
            else
               lower_points--;
            sl2=schnittpunkte.erase(sl2);
         }
         else
            sl2++;
   }

   //Punkte und Schnitte auf Gleichheit checken
   sl=schnittpunkte.begin();
   for (;sl!=schnittpunkte.end(); sl++)
   {
      schnittpunkt=(*sl);
      found=false;

      l2=pPVV->begin();
      e2=pPVV->end();

      for (;l2!=e2; l2++)
      {
         lp1.x=(**l2).getX();
         lp1.y=(**l2).getY();
         lp1.z=(**l2).getZ();

         if ((schnittpunkt-lp1).Value()<rand)
         {
            l2_next=l2;
            l2_last=l2;
            l2_next++;
            if (l2_next==e2)
               l2_next=pPVV->begin();
            if (l2==pPVV->begin())
               l2_last=pPVV->end()-1;
            else
               l2_last--;
            lp1.x=(**l2_next).getX();
            lp1.y=(**l2_next).getY();
            lp1.z=(**l2_next).getZ();
            lp2.x=(**l2_last).getX();
            lp2.y=(**l2_last).getY();
            lp2.z=(**l2_last).getZ();

            dist1=Dist_PS_3d(lp1,sort_plane);
            dist2=Dist_PS_3d(lp2,sort_plane);
            if (fabs(dist1)<rand) dist1=0.0; else dist1/=fabs(dist1);
            if (fabs(dist2)<rand) dist2=0.0; else dist2/=fabs(dist2);

            if ((dist1==0.0) && (dist2==0.0))
               //gerade Ecke auf Schnittlinie
               ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, true));
            else
               if (fabs(dist1*dist2)<rand)
               {
                  //Winkel nach oben oder unten
                  if (dist1+dist2>0.0)
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,1, true));
                  else
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,-1, true));
               }
               else
                  if (dist1*dist2<-rand)
                     //Spitze
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, true));
                  else
                  {
                     //gerade Ecke senkrecht auf Schnittlinie (wird aufgenohmen wie normaler Schnittpunkt)
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, false));
                     ecken_anz--;
                  }
            found=true;
            ecken_anz++;
            break;
         }
      }
      if (!found)
         ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, false));
   }
   if (ecken_anz>1)
   {
      //gesuchten Punkt einf�gen
      ecken_schnittpunkte.push_back(ecken_schnittpunkt(point,1, false));
      //alle spitzen und geraden Schnittpunkt Ecken l�schen, sort-Distanzen ermitteln
      l_ecke=ecken_schnittpunkte.begin();
      for (; l_ecke!=ecken_schnittpunkte.end(); )
         if (((*l_ecke).direction==0) && ((*l_ecke).is_ecke))
         {
            //l�schen
            schnittpunkte.erase(find(schnittpunkte.begin(), schnittpunkte.end(), (*l_ecke).schnittpunkt));
            l_ecke=ecken_schnittpunkte.erase(l_ecke);
         }
         else
         {
            //bleibt! --> Distanz zu Sort-Referenzebene ermitteln
            (*l_ecke).dist=Dist_PS_3d((*l_ecke).schnittpunkt, plane);
            l_ecke++;
         }

      //alle Schnittpunkte sortieren
      l_ecke=ecken_schnittpunkte.begin();
      e_ecke=ecken_schnittpunkte.end();
      for (; l_ecke!=e_ecke; l_ecke++)
      {
         l2_ecke=l_ecke;
         l2_ecke++;
         for (; l2_ecke!=e_ecke; l2_ecke++)
            if ((*l_ecke).dist>(*l2_ecke).dist)
            {
               //vertauschen
               sort_dummy=(*l_ecke);
               (*l_ecke)=(*l2_ecke);
               (*l2_ecke)=sort_dummy;
            }
      }

      //in Fleisch Checks
      up_inside=false;
      down_inside=false;
      l_ecke=ecken_schnittpunkte.begin();
      for (; l_ecke!=e_ecke; l_ecke++)
      {
         if ((!(*l_ecke).is_ecke) && ((*l_ecke).direction==1)) //unser gesuchter Punkt
            return (up_inside || down_inside);
         else
         if ((!(*l_ecke).is_ecke) && ((*l_ecke).direction==0)) //kompletter Wechsel
         {
            up_inside=!up_inside;
            down_inside=!down_inside;
         }
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==1))  //Wechsel oben
            up_inside=!up_inside;
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==-1)) //Wechsel unten
            down_inside=!down_inside;
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==0))  //Fehler
            assert(0);
      }

      assert(0);
      return false;
   }
   else
   {
      ecken_schnittpunkte.clear();

      //ist point auf dem Polygon Rand ?
      sl=schnittpunkte.begin();
      se=schnittpunkte.end();
      for (;sl!=se; sl++)
         if ((point-(*sl)).Value()<rand)
            return true;

      //befindet sich point zwischen einer ungeraden Anzahl von Schnittpunkten ?
      return (schnittpunkte.size()>1) && (upper_points%2==1) && (lower_points%2==1);
   }
}
*/
//bool CGeom3d::In_Loop_Plane_3d(Geometry::Polygon::PointVector * pPVV, Geometry::Point * pPoint, double rand/*= 0.001*/)
/*
{
   Vec3d schnittvektor, normale=GetNormale(*pPVV);
   Line_3d schnittlinie,linie;
   Point_3d point,schnittpunkt,lp1,lp2;
   Geometry::Polygon::PointVector::iterator l2,l3,e2,l2_next,l2_last;
   PointVector schnittpunkte;
   EckenSchnittpunktVector ecken_schnittpunkte;
   EckenSchnittpunktVector::iterator l_ecke, l2_ecke, e_ecke;
   ecken_schnittpunkt sort_dummy;
   PointVector::iterator sl,sl2,se;
   Plane_3d plane, sort_plane;
   double dist, dist1, dist2;
   long upper_points=0, lower_points=0, ecken_anz=0;
   bool found, up_inside, down_inside;

   point.x=pPoint->getX();
   point.y=pPoint->getY();
   point.z=pPoint->getZ();
   Vekt_Rectangular(normale, schnittvektor);
   Line_VP_3d(schnittvektor,point,schnittlinie);
   Plane_PN_3d(point,schnittvektor,plane);
   Plane_PN_3d(point,schnittvektor.amul(normale),sort_plane);

   l2=pPVV->begin();
   e2=pPVV->end();

   for (;l2!=e2; l2++)
   {
      l3=l2+1;
      if (l3==e2)
         l3=pPVV->begin();

      lp1.x=(**l2).getX();
      lp1.y=(**l2).getY();
      lp1.z=(**l2).getZ();
      lp2.x=(**l3).getX();
      lp2.y=(**l3).getY();
      lp2.z=(**l3).getZ();
      if ((lp1-lp2).Value()<rand) continue;
      Line_PP_3d(lp1,lp2,linie);

      //sind wir auf dem Rand ?
      if (InLine_3d(lp1,lp2,point,rand))
         return true;

      Point_LL_3d(schnittlinie,linie,schnittpunkt,rand);
      if ((Err_3d==1) && (InLine_3d(lp1,lp2,schnittpunkt,rand)))
      {
         schnittpunkte.push_back(schnittpunkt);
         dist=Dist_PS_3d(schnittpunkt,plane);
         if (dist>0.0)
            upper_points++;
         else
            lower_points++;
      }
   }

   //doppelte Schnittpunkte l�schen
   sl=schnittpunkte.begin();
   for (;sl!=schnittpunkte.end(); sl++)
   {
      sl2=sl;
      sl2++;

      for (;sl2!=schnittpunkte.end();)
         if (((*sl)-(*sl2)).Value()<rand)
         {
            dist=Dist_PS_3d(*sl2,plane);
            if (dist>0.0)
               upper_points--;
            else
               lower_points--;
            sl2=schnittpunkte.erase(sl2);
         }
         else
            sl2++;
   }

   //Punkte und Schnitte auf Gleichheit checken
   sl=schnittpunkte.begin();
   for (;sl!=schnittpunkte.end(); sl++)
   {
      schnittpunkt=(*sl);
      found=false;

      l2=pPVV->begin();
      e2=pPVV->end();

      for (;l2!=e2; l2++)
      {
         lp1.x=(**l2).getX();
         lp1.y=(**l2).getY();
         lp1.z=(**l2).getZ();

         if ((schnittpunkt-lp1).Value()<rand)
         {
            l2_next=l2;
            l2_last=l2;
            l2_next++;
            if (l2_next==e2)
               l2_next=pPVV->begin();
            if (l2==pPVV->begin())
               l2_last=pPVV->end()-1;
            else
               l2_last--;
            lp1.x=(**l2_next).getX();
            lp1.y=(**l2_next).getY();
            lp1.z=(**l2_next).getZ();
            lp2.x=(**l2_last).getX();
            lp2.y=(**l2_last).getY();
            lp2.z=(**l2_last).getZ();

            dist1=Dist_PS_3d(lp1,sort_plane);
            dist2=Dist_PS_3d(lp2,sort_plane);
            if (fabs(dist1)<rand) dist1=0.0; else dist1/=fabs(dist1);
            if (fabs(dist2)<rand) dist2=0.0; else dist2/=fabs(dist2);

            if ((dist1==0.0) && (dist2==0.0))
               //gerade Ecke auf Schnittlinie
               ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, true));
            else
               if (fabs(dist1*dist2)<rand)
               {
                  //Winkel nach oben oder unten
                  if (dist1+dist2>0.0)
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,1, true));
                  else
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,-1, true));
               }
               else
                  if (dist1*dist2<-rand)
                     //Spitze
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, true));
                  else
                  {
                     //gerade Ecke senkrecht auf Schnittlinie (wird aufgenohmen wie normaler Schnittpunkt)
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, false));
                     ecken_anz--;
                  }
            found=true;
            ecken_anz++;
            break;
         }
      }
      if (!found)
         ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, false));
   }
   if (ecken_anz>1)
   {
      //gesuchten Punkt einf�gen
      ecken_schnittpunkte.push_back(ecken_schnittpunkt(point,1, false));
      //alle spitzen und geraden Schnittpunkt Ecken l�schen, sort-Distanzen ermitteln
      l_ecke=ecken_schnittpunkte.begin();
      for (; l_ecke!=ecken_schnittpunkte.end(); )
         if (((*l_ecke).direction==0) && ((*l_ecke).is_ecke))
         {
            //l�schen
            schnittpunkte.erase(find(schnittpunkte.begin(), schnittpunkte.end(), (*l_ecke).schnittpunkt));
            l_ecke=ecken_schnittpunkte.erase(l_ecke);
         }
         else
         {
            //bleibt! --> Distanz zu Sort-Referenzebene ermitteln
            (*l_ecke).dist=Dist_PS_3d((*l_ecke).schnittpunkt, plane);
            l_ecke++;
         }

      //alle Schnittpunkte sortieren
      l_ecke=ecken_schnittpunkte.begin();
      e_ecke=ecken_schnittpunkte.end();
      for (; l_ecke!=e_ecke; l_ecke++)
      {
         l2_ecke=l_ecke;
         l2_ecke++;
         for (; l2_ecke!=e_ecke; l2_ecke++)
            if ((*l_ecke).dist>(*l2_ecke).dist)
            {
               //vertauschen
               sort_dummy=(*l_ecke);
               (*l_ecke)=(*l2_ecke);
               (*l2_ecke)=sort_dummy;
            }
      }

      //in Fleisch Checks
      up_inside=false;
      down_inside=false;
      l_ecke=ecken_schnittpunkte.begin();
      for (; l_ecke!=e_ecke; l_ecke++)
      {
         if ((!(*l_ecke).is_ecke) && ((*l_ecke).direction==1)) //unser gesuchter Punkt
            return (up_inside || down_inside);
         else
         if ((!(*l_ecke).is_ecke) && ((*l_ecke).direction==0)) //kompletter Wechsel
         {
            up_inside=!up_inside;
            down_inside=!down_inside;
         }
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==1))  //Wechsel oben
            up_inside=!up_inside;
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==-1)) //Wechsel unten
            down_inside=!down_inside;
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==0))  //Fehler
            assert(0);
      }

      assert(0);
      return false;
   }
   else
   {
      ecken_schnittpunkte.clear();

      //ist point auf dem Polygon Rand ?
      sl=schnittpunkte.begin();
      se=schnittpunkte.end();
      for (;sl!=se; sl++)
         if ((point-(*sl)).Value()<rand)
            return true;

      //befindet sich point zwischen einer ungeraden Anzahl von Schnittpunkten ?
      return (schnittpunkte.size()>1) && (upper_points%2==1) && (lower_points%2==1);
   }
}
*/
//bool CGeom3d::In_Loop_Plane_3d(Geometry::Polygon::PointVector &PVV, Geometry::Point * pPoint, double rand/*= 0.001*/)
/*
{
   Vec3d schnittvektor, normale=GetNormale(PVV);
   Line_3d schnittlinie,linie;
   Point_3d point,schnittpunkt,lp1,lp2;
   Geometry::Polygon::PointVector::iterator l2,l3,e2,l2_next,l2_last;
   PointVector schnittpunkte;
   EckenSchnittpunktVector ecken_schnittpunkte;
   EckenSchnittpunktVector::iterator l_ecke, l2_ecke, e_ecke;
   ecken_schnittpunkt sort_dummy;
   PointVector::iterator sl,sl2,se;
   Plane_3d plane, sort_plane;
   double dist, dist1, dist2;
   long upper_points=0, lower_points=0, ecken_anz=0;
   bool found, up_inside, down_inside;

   point.x=pPoint->getX();
   point.y=pPoint->getY();
   point.z=pPoint->getZ();
   Vekt_Rectangular(normale, schnittvektor);
   Line_VP_3d(schnittvektor,point,schnittlinie);
   Plane_PN_3d(point,schnittvektor,plane);
   Plane_PN_3d(point,schnittvektor.amul(normale),sort_plane);

   l2=PVV.begin();
   e2=PVV.end();

   for (;l2!=e2; l2++)
   {
      l3=l2+1;
      if (l3==e2)
         l3=PVV.begin();

      lp1.x=(**l2).getX();
      lp1.y=(**l2).getY();
      lp1.z=(**l2).getZ();
      lp2.x=(**l3).getX();
      lp2.y=(**l3).getY();
      lp2.z=(**l3).getZ();
      if ((lp1-lp2).Value()<rand) continue;
      Line_PP_3d(lp1,lp2,linie);

      //sind wir auf dem Rand ?
      if (InLine_3d(lp1,lp2,point,rand))
         return true;

      Point_LL_3d(schnittlinie,linie,schnittpunkt,rand);
      if ((Err_3d==1) && (InLine_3d(lp1,lp2,schnittpunkt,rand)))
      {
         schnittpunkte.push_back(schnittpunkt);
         dist=Dist_PS_3d(schnittpunkt,plane);
         if (dist>0.0)
            upper_points++;
         else
            lower_points++;
      }
   }

   //doppelte Schnittpunkte l�schen
   sl=schnittpunkte.begin();
   for (;sl!=schnittpunkte.end(); sl++)
   {
      sl2=sl;
      sl2++;

      for (;sl2!=schnittpunkte.end();)
         if (((*sl)-(*sl2)).Value()<rand)
         {
            dist=Dist_PS_3d(*sl2,plane);
            if (dist>0.0)
               upper_points--;
            else
               lower_points--;
            sl2=schnittpunkte.erase(sl2);
         }
         else
            sl2++;
   }

   //Punkte und Schnitte auf Gleichheit checken
   sl=schnittpunkte.begin();
   for (;sl!=schnittpunkte.end(); sl++)
   {
      schnittpunkt=(*sl);
      found=false;

      l2=PVV.begin();
      e2=PVV.end();

      for (;l2!=e2; l2++)
      {
         lp1.x=(**l2).getX();
         lp1.y=(**l2).getY();
         lp1.z=(**l2).getZ();

         if ((schnittpunkt-lp1).Value()<rand)
         {
            l2_next=l2;
            l2_last=l2;
            l2_next++;
            if (l2_next==e2)
               l2_next=PVV.begin();
            if (l2==PVV.begin())
               l2_last=PVV.end()-1;
            else
               l2_last--;
            lp1.x=(**l2_next).getX();
            lp1.y=(**l2_next).getY();
            lp1.z=(**l2_next).getZ();
            lp2.x=(**l2_last).getX();
            lp2.y=(**l2_last).getY();
            lp2.z=(**l2_last).getZ();

            dist1=Dist_PS_3d(lp1,sort_plane);
            dist2=Dist_PS_3d(lp2,sort_plane);
            if (fabs(dist1)<rand) dist1=0.0; else dist1/=fabs(dist1);
            if (fabs(dist2)<rand) dist2=0.0; else dist2/=fabs(dist2);

            if ((dist1==0.0) && (dist2==0.0))
               //gerade Ecke auf Schnittlinie
               ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, true));
            else
               if (fabs(dist1*dist2)<rand)
               {
                  //Winkel nach oben oder unten
                  if (dist1+dist2>0.0)
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,1, true));
                  else
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,-1, true));
               }
               else
                  if (dist1*dist2<-rand)
                     //Spitze
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, true));
                  else
                  {
                     //gerade Ecke senkrecht auf Schnittlinie (wird aufgenohmen wie normaler Schnittpunkt)
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, false));
                     ecken_anz--;
                  }
            found=true;
            ecken_anz++;
            break;
         }
      }
      if (!found)
         ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, false));
   }
   if (ecken_anz>1)
   {
      //gesuchten Punkt einf�gen
      ecken_schnittpunkte.push_back(ecken_schnittpunkt(point,1, false));
      //alle spitzen und geraden Schnittpunkt Ecken l�schen, sort-Distanzen ermitteln
      l_ecke=ecken_schnittpunkte.begin();
      for (; l_ecke!=ecken_schnittpunkte.end(); )
         if (((*l_ecke).direction==0) && ((*l_ecke).is_ecke))
         {
            //l�schen
            schnittpunkte.erase(find(schnittpunkte.begin(), schnittpunkte.end(), (*l_ecke).schnittpunkt));
            l_ecke=ecken_schnittpunkte.erase(l_ecke);
         }
         else
         {
            //bleibt! --> Distanz zu Sort-Referenzebene ermitteln
            (*l_ecke).dist=Dist_PS_3d((*l_ecke).schnittpunkt, plane);
            l_ecke++;
         }

      //alle Schnittpunkte sortieren
      l_ecke=ecken_schnittpunkte.begin();
      e_ecke=ecken_schnittpunkte.end();
      for (; l_ecke!=e_ecke; l_ecke++)
      {
         l2_ecke=l_ecke;
         l2_ecke++;
         for (; l2_ecke!=e_ecke; l2_ecke++)
            if ((*l_ecke).dist>(*l2_ecke).dist)
            {
               //vertauschen
               sort_dummy=(*l_ecke);
               (*l_ecke)=(*l2_ecke);
               (*l2_ecke)=sort_dummy;
            }
      }

      //in Fleisch Checks
      up_inside=false;
      down_inside=false;
      l_ecke=ecken_schnittpunkte.begin();
      for (; l_ecke!=e_ecke; l_ecke++)
      {
         if ((!(*l_ecke).is_ecke) && ((*l_ecke).direction==1)) //unser gesuchter Punkt
            return (up_inside || down_inside);
         else
         if ((!(*l_ecke).is_ecke) && ((*l_ecke).direction==0)) //kompletter Wechsel
         {
            up_inside=!up_inside;
            down_inside=!down_inside;
         }
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==1))  //Wechsel oben
            up_inside=!up_inside;
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==-1)) //Wechsel unten
            down_inside=!down_inside;
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==0))  //Fehler
            assert(0);
      }

      assert(0);
      return false;
   }
   else
   {
      ecken_schnittpunkte.clear();

      //ist point auf dem Polygon Rand ?
      sl=schnittpunkte.begin();
      se=schnittpunkte.end();
      for (;sl!=se; sl++)
         if ((point-(*sl)).Value()<rand)
            return true;

      //befindet sich point zwischen einer ungeraden Anzahl von Schnittpunkten ?
      return (schnittpunkte.size()>1) && (upper_points%2==1) && (lower_points%2==1);
   }
}

bool CGeom3d::In_Loop_Plane_3d(Geometry::Polygon::PointVector &PVV, Point_3d check_point, double rand)
{
   Vec3d schnittvektor, normale=GetNormale(PVV);
   Line_3d schnittlinie,linie;
   Point_3d schnittpunkt,lp1,lp2;
   Geometry::Polygon::PointVector::iterator l2,l3,e2,l2_next,l2_last;
   PointVector schnittpunkte;
   EckenSchnittpunktVector ecken_schnittpunkte;
   EckenSchnittpunktVector::iterator l_ecke, l2_ecke, e_ecke;
   ecken_schnittpunkt sort_dummy;
   PointVector::iterator sl,sl2,se;
   Plane_3d plane, sort_plane;
   double dist, dist1, dist2;
   long upper_points=0, lower_points=0, ecken_anz=0;
   bool found, up_inside, down_inside;

   Vekt_Rectangular(normale, schnittvektor);
   Line_VP_3d(schnittvektor,check_point,schnittlinie);
   Plane_PN_3d(check_point,schnittvektor,plane);
   Plane_PN_3d(check_point,schnittvektor.amul(normale),sort_plane);

   l2=PVV.begin();
   e2=PVV.end();

   for (;l2!=e2; l2++)
   {
      l3=l2+1;
      if (l3==e2)
         l3=PVV.begin();

      lp1.x=(**l2).getX();
      lp1.y=(**l2).getY();
      lp1.z=(**l2).getZ();
      lp2.x=(**l3).getX();
      lp2.y=(**l3).getY();
      lp2.z=(**l3).getZ();
      if ((lp1-lp2).Value()<rand) continue;
      Line_PP_3d(lp1,lp2,linie);

      //sind wir auf dem Rand ?
      if (InLine_3d(lp1,lp2,check_point,rand))
         return true;

      Point_LL_3d(schnittlinie,linie,schnittpunkt,rand);
      if ((Err_3d==1) && (InLine_3d(lp1,lp2,schnittpunkt,rand)))
      {
         schnittpunkte.push_back(schnittpunkt);
         dist=Dist_PS_3d(schnittpunkt,plane);
         if (dist>0.0)
            upper_points++;
         else
            lower_points++;
      }
   }

   //doppelte Schnittpunkte l�schen
   sl=schnittpunkte.begin();
   for (;sl!=schnittpunkte.end(); sl++)
   {
      sl2=sl;
      sl2++;

      for (;sl2!=schnittpunkte.end();)
         if (((*sl)-(*sl2)).Value()<rand)
         {
            dist=Dist_PS_3d(*sl2,plane);
            if (dist>0.0)
               upper_points--;
            else
               lower_points--;
            sl2=schnittpunkte.erase(sl2);
         }
         else
            sl2++;
   }

   //Punkte und Schnitte auf Gleichheit checken
   sl=schnittpunkte.begin();
   for (;sl!=schnittpunkte.end(); sl++)
   {
      schnittpunkt=(*sl);
      found=false;

      l2=PVV.begin();
      e2=PVV.end();

      for (;l2!=e2; l2++)
      {
         lp1.x=(**l2).getX();
         lp1.y=(**l2).getY();
         lp1.z=(**l2).getZ();

         if ((schnittpunkt-lp1).Value()<rand)
         {
            l2_next=l2;
            l2_last=l2;
            l2_next++;
            if (l2_next==e2)
               l2_next=PVV.begin();
            if (l2==PVV.begin())
               l2_last=PVV.end()-1;
            else
               l2_last--;
            lp1.x=(**l2_next).getX();
            lp1.y=(**l2_next).getY();
            lp1.z=(**l2_next).getZ();
            lp2.x=(**l2_last).getX();
            lp2.y=(**l2_last).getY();
            lp2.z=(**l2_last).getZ();

            dist1=Dist_PS_3d(lp1,sort_plane);
            dist2=Dist_PS_3d(lp2,sort_plane);
            if (fabs(dist1)<rand) dist1=0.0; else dist1/=fabs(dist1);
            if (fabs(dist2)<rand) dist2=0.0; else dist2/=fabs(dist2);

            if ((dist1==0.0) && (dist2==0.0))
               //gerade Ecke auf Schnittlinie
               ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, true));
            else
               if (fabs(dist1*dist2)<rand)
               {
                  //Winkel nach oben oder unten
                  if (dist1+dist2>0.0)
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,1, true));
                  else
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,-1, true));
               }
               else
                  if (dist1*dist2<-rand)
                     //Spitze
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, true));
                  else
                  {
                     //gerade Ecke senkrecht auf Schnittlinie (wird aufgenohmen wie normaler Schnittpunkt)
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, false));
                     ecken_anz--;
                  }
            found=true;
            ecken_anz++;
            break;
         }
      }
      if (!found)
         ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, false));
   }
   if (ecken_anz>1)
   {
      //gesuchten Punkt einf�gen
      ecken_schnittpunkte.push_back(ecken_schnittpunkt(check_point,1, false));
      //alle spitzen und geraden Schnittpunkt Ecken l�schen, sort-Distanzen ermitteln
      l_ecke=ecken_schnittpunkte.begin();
      for (; l_ecke!=ecken_schnittpunkte.end(); )
         if (((*l_ecke).direction==0) && ((*l_ecke).is_ecke))
         {
            //l�schen
            schnittpunkte.erase(find(schnittpunkte.begin(), schnittpunkte.end(), (*l_ecke).schnittpunkt));
            l_ecke=ecken_schnittpunkte.erase(l_ecke);
         }
         else
         {
            //bleibt! --> Distanz zu Sort-Referenzebene ermitteln
            (*l_ecke).dist=Dist_PS_3d((*l_ecke).schnittpunkt, plane);
            l_ecke++;
         }

      //alle Schnittpunkte sortieren
      l_ecke=ecken_schnittpunkte.begin();
      e_ecke=ecken_schnittpunkte.end();
      for (; l_ecke!=e_ecke; l_ecke++)
      {
         l2_ecke=l_ecke;
         l2_ecke++;
         for (; l2_ecke!=e_ecke; l2_ecke++)
            if ((*l_ecke).dist>(*l2_ecke).dist)
            {
               //vertauschen
               sort_dummy=(*l_ecke);
               (*l_ecke)=(*l2_ecke);
               (*l2_ecke)=sort_dummy;
            }
      }

      //in Fleisch Checks
      up_inside=false;
      down_inside=false;
      l_ecke=ecken_schnittpunkte.begin();
      for (; l_ecke!=e_ecke; l_ecke++)
      {
         if ((!(*l_ecke).is_ecke) && ((*l_ecke).direction==1)) //unser gesuchter Punkt
            return (up_inside || down_inside);
         else
         if ((!(*l_ecke).is_ecke) && ((*l_ecke).direction==0)) //kompletter Wechsel
         {
            up_inside=!up_inside;
            down_inside=!down_inside;
         }
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==1))  //Wechsel oben
            up_inside=!up_inside;
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==-1)) //Wechsel unten
            down_inside=!down_inside;
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==0))  //Fehler
            assert(0);
      }

      assert(0);
      return false;
   }
   else
   {
      ecken_schnittpunkte.clear();

      //ist point auf dem Polygon Rand ?
      sl=schnittpunkte.begin();
      se=schnittpunkte.end();
      for (;sl!=se; sl++)
         if ((check_point-(*sl)).Value()<rand)
            return true;

      //befindet sich point zwischen einer ungeraden Anzahl von Schnittpunkten ?
      return (schnittpunkte.size()>1) && (upper_points%2==1) && (lower_points%2==1);
   }
}
*/
//bool CGeom3d::In_Loop_Plane_3d(Point_3d_Vector & PV, Point_3d * pPoint, double rand/*= 0.001*/)
/*
{
   Vec3d schnittvektor, normale=GetNormale(PV);
   Line_3d schnittlinie,linie;
   Point_3d point,schnittpunkt,lp1,lp2;
   Point_3d_Vector::iterator l2,l3,e2,l2_next,l2_last;
   PointVector schnittpunkte;
   EckenSchnittpunktVector ecken_schnittpunkte;
   EckenSchnittpunktVector::iterator l_ecke, l2_ecke, e_ecke;
   ecken_schnittpunkt sort_dummy;
   PointVector::iterator sl,sl2,se;
   Plane_3d plane, sort_plane;
   double dist, dist1, dist2;
   long upper_points=0, lower_points=0, ecken_anz=0;
   bool found, up_inside, down_inside;

   point=*pPoint;
   Vekt_Rectangular(normale, schnittvektor);
   Line_VP_3d(schnittvektor,point,schnittlinie);
   Plane_PN_3d(point,schnittvektor,plane);
   Plane_PN_3d(point,schnittvektor.amul(normale),sort_plane);

   l2=PV.begin();
   e2=PV.end();

   for (;l2!=e2; l2++)
   {
      l3=l2+1;
      if (l3==e2)
         l3=PV.begin();

      lp1=*l2;
      lp2=*l3;
      if ((lp1-lp2).Value()<rand) continue;
      Line_PP_3d(lp1,lp2,linie);

      //sind wir auf dem Rand ?
      if (InLine_3d(lp1,lp2,point,rand))
         return true;

      Point_LL_3d(schnittlinie,linie,schnittpunkt,rand);
      if ((Err_3d==1) && (InLine_3d(lp1,lp2,schnittpunkt,rand)))
      {
         schnittpunkte.push_back(schnittpunkt);
         dist=Dist_PS_3d(schnittpunkt,plane);
         if (dist>0.0)
            upper_points++;
         else
            lower_points++;
      }
   }

   //doppelte Schnittpunkte l�schen
   sl=schnittpunkte.begin();
   for (;sl!=schnittpunkte.end(); sl++)
   {
      sl2=sl;
      sl2++;

      for (;sl2!=schnittpunkte.end();)
         if (((*sl)-(*sl2)).Value()<rand)
         {
            dist=Dist_PS_3d(*sl2,plane);
            if (dist>0.0)
               upper_points--;
            else
               lower_points--;
            sl2=schnittpunkte.erase(sl2);
         }
         else
            sl2++;
   }

   //Punkte und Schnitte auf Gleichheit checken
   sl=schnittpunkte.begin();
   for (;sl!=schnittpunkte.end(); sl++)
   {
      schnittpunkt=(*sl);
      found=false;

      l2=PV.begin();
      e2=PV.end();
      for (;l2!=e2; l2++)
      {
         lp1=(*l2);

         if ((schnittpunkt-lp1).Value()<rand)
         {
            l2_next=l2;
            l2_last=l2;
            l2_next++;
            if (l2_next==e2)
               l2_next=PV.begin();
            if (l2==PV.begin())
               l2_last=PV.end()-1;
            else
               l2_last--;
            lp1=(*l2_next);
            lp2=(*l2_last);

            dist1=Dist_PS_3d(lp1,sort_plane);
            dist2=Dist_PS_3d(lp2,sort_plane);
            if (fabs(dist1)<rand) dist1=0.0; else dist1/=fabs(dist1);
            if (fabs(dist2)<rand) dist2=0.0; else dist2/=fabs(dist2);

            if ((dist1==0.0) && (dist2==0.0))
               //gerade Ecke auf Schnittlinie
               ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, true));
            else
               if (fabs(dist1*dist2)<rand)
               {
                  //Winkel nach oben oder unten
                  if (dist1+dist2>0.0)
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,1, true));
                  else
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,-1, true));
               }
               else
                  if (dist1*dist2<-rand)
                     //Spitze
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, true));
                  else
                  {
                     //gerade Ecke senkrecht auf Schnittlinie (wird aufgenohmen wie normaler Schnittpunkt)
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, false));
                     ecken_anz--;
                  }
            found=true;
            ecken_anz++;
            break;
         }
      }
      if (!found)
         ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, false));
   }
   if (ecken_anz>1)
   {
      //gesuchten Punkt einf�gen
      ecken_schnittpunkte.push_back(ecken_schnittpunkt(point,1, false));
      //alle spitzen und geraden Schnittpunkt Ecken l�schen, sort-Distanzen ermitteln
      l_ecke=ecken_schnittpunkte.begin();
      for (; l_ecke!=ecken_schnittpunkte.end(); )
         if (((*l_ecke).direction==0) && ((*l_ecke).is_ecke))
         {
            //l�schen
            schnittpunkte.erase(find(schnittpunkte.begin(), schnittpunkte.end(), (*l_ecke).schnittpunkt));
            l_ecke=ecken_schnittpunkte.erase(l_ecke);
         }
         else
         {
            //bleibt! --> Distanz zu Sort-Referenzebene ermitteln
            (*l_ecke).dist=Dist_PS_3d((*l_ecke).schnittpunkt, plane);
            l_ecke++;
         }

      //alle Schnittpunkte sortieren
      l_ecke=ecken_schnittpunkte.begin();
      e_ecke=ecken_schnittpunkte.end();
      for (; l_ecke!=e_ecke; l_ecke++)
      {
         l2_ecke=l_ecke;
         l2_ecke++;
         for (; l2_ecke!=e_ecke; l2_ecke++)
            if ((*l_ecke).dist>(*l2_ecke).dist)
            {
               //vertauschen
               sort_dummy=(*l_ecke);
               (*l_ecke)=(*l2_ecke);
               (*l2_ecke)=sort_dummy;
            }
      }

      //in Fleisch Checks
      up_inside=false;
      down_inside=false;
      l_ecke=ecken_schnittpunkte.begin();
      for (; l_ecke!=e_ecke; l_ecke++)
      {
         if ((!(*l_ecke).is_ecke) && ((*l_ecke).direction==1)) //unser gesuchter Punkt
            return (up_inside || down_inside);
         else
         if ((!(*l_ecke).is_ecke) && ((*l_ecke).direction==0)) //kompletter Wechsel
         {
            up_inside=!up_inside;
            down_inside=!down_inside;
         }
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==1))  //Wechsel oben
            up_inside=!up_inside;
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==-1)) //Wechsel unten
            down_inside=!down_inside;
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==0))  //Fehler
            assert(0);
      }

      assert(0);
      return false;
   }
   else
   {
      ecken_schnittpunkte.clear();

      //ist point auf dem Polygon Rand ?
      sl=schnittpunkte.begin();
      se=schnittpunkte.end();
      for (;sl!=se; sl++)
         if ((point-(*sl)).Value()<rand)
            return true;

      //befindet sich point zwischen einer ungeraden Anzahl von Schnittpunkten ?
      return (schnittpunkte.size()>1) && (upper_points%2==1) && (lower_points%2==1);
   }
}
*/

//bool CGeom3d::In_Poly_Plane_3d(Geometry::Polygon * pPoly, Geometry::Point * pPoint, double rand/*= 0.001*/)
/*
{
   Vec3d schnittvektor, normale=pPoly->GetNormale();
   Line_3d schnittlinie,linie;
   Point_3d point,schnittpunkt,lp1,lp2;
   Geometry::Polygon::PointVectorVector::iterator l=pPoly->points.begin(), e=pPoly->points.end();
   Geometry::Polygon::PointVector::iterator l2,l3,e2,l2_next,l2_last;
   PointVector schnittpunkte;
   EckenSchnittpunktVector ecken_schnittpunkte;
   EckenSchnittpunktVector::iterator l_ecke, l2_ecke, e_ecke;
   ecken_schnittpunkt sort_dummy;
   PointVector::iterator sl,sl2,se;
   Plane_3d plane, sort_plane;
   double dist, dist1, dist2;
   long upper_points=0, lower_points=0, ecken_anz=0;
   bool found, up_inside, down_inside;

   point.x=pPoint->getX();
   point.y=pPoint->getY();
   point.z=pPoint->getZ();
   Vekt_Rectangular(normale, schnittvektor);
   Line_VP_3d(schnittvektor,point,schnittlinie);
   Plane_PN_3d(point,schnittvektor,plane);
   Plane_PN_3d(point,schnittvektor.amul(normale),sort_plane);

   for(; l!=e; l++)
   {
      l2=(*l).begin();
      e2=(*l).end();

      for (;l2!=e2; l2++)
      {
         l3=l2+1;
         if (l3==e2)
            l3=(*l).begin();

         lp1.x=(**l2).getX();
         lp1.y=(**l2).getY();
         lp1.z=(**l2).getZ();
         lp2.x=(**l3).getX();
         lp2.y=(**l3).getY();
         lp2.z=(**l3).getZ();
         if ((lp1-lp2).Value()<rand) continue;
         Line_PP_3d(lp1,lp2,linie);

         //sind wir auf dem Rand ?
         if (InLine_3d(lp1,lp2,point,rand))
            return true;

         Point_LL_3d(schnittlinie,linie,schnittpunkt,rand);
         if ((Err_3d==1) && (InLine_3d(lp1,lp2,schnittpunkt,rand)))
         {
            schnittpunkte.push_back(schnittpunkt);
            dist=Dist_PS_3d(schnittpunkt,plane);
            if (dist>0.0)
               upper_points++;
            else
               lower_points++;
         }
      }
   }

   //doppelte Schnittpunkte l�schen
   sl=schnittpunkte.begin();
   for (;sl!=schnittpunkte.end(); sl++)
   {
      sl2=sl;
      sl2++;

      for (;sl2!=schnittpunkte.end();)
         if (((*sl)-(*sl2)).Value()<rand)
         {
            dist=Dist_PS_3d(*sl2,plane);
            if (dist>0.0)
               upper_points--;
            else
               lower_points--;
            sl2=schnittpunkte.erase(sl2);
         }
         else
            sl2++;
   }

   //Punkte und Schnitte auf Gleichheit checken
   sl=schnittpunkte.begin();
   for (;sl!=schnittpunkte.end(); sl++)
   {
      schnittpunkt=(*sl);
      found=false;

      l=pPoly->points.begin();
      e=pPoly->points.end();
      for(; l!=e; l++)
      {
         l2=(*l).begin();
         e2=(*l).end();

         for (;l2!=e2; l2++)
         {
            lp1.x=(**l2).getX();
            lp1.y=(**l2).getY();
            lp1.z=(**l2).getZ();

            if ((schnittpunkt-lp1).Value()<rand)
            {
               l2_next=l2;
               l2_last=l2;
               l2_next++;
               if (l2_next==e2)
                  l2_next=(*l).begin();
               if (l2==(*l).begin())
                  l2_last=(*l).end()-1;
               else
                  l2_last--;
               lp1.x=(**l2_next).getX();
               lp1.y=(**l2_next).getY();
               lp1.z=(**l2_next).getZ();
               lp2.x=(**l2_last).getX();
               lp2.y=(**l2_last).getY();
               lp2.z=(**l2_last).getZ();

               dist1=Dist_PS_3d(lp1,sort_plane);
               dist2=Dist_PS_3d(lp2,sort_plane);
               if (fabs(dist1)<rand) dist1=0.0; else dist1/=fabs(dist1);
               if (fabs(dist2)<rand) dist2=0.0; else dist2/=fabs(dist2);

               if ((dist1==0.0) && (dist2==0.0))
                  //gerade Ecke auf Schnittlinie
                  ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, true));
               else
                  if (fabs(dist1*dist2)<rand)
                  {
                     //Winkel nach oben oder unten
                     if (dist1+dist2>0.0)
                        ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,1, true));
                     else
                        ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,-1, true));
                  }
                  else
                     if (dist1*dist2<-rand)
                        //Spitze
                        ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, true));
                     else
                     {
                        //gerade Ecke senkrecht auf Schnittlinie (wird aufgenohmen wie normaler Schnittpunkt)
                        ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, false));
                        ecken_anz--;
                     }
               found=true;
               ecken_anz++;
               break;
            }
         }
         if (found)
            break;
      }
      if (!found)
         ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, false));
   }
   if (ecken_anz>1)
   {
      //gesuchten Punkt einf�gen
      ecken_schnittpunkte.push_back(ecken_schnittpunkt(point,1, false));
      //alle spitzen und geraden Schnittpunkt Ecken l�schen, sort-Distanzen ermitteln
      l_ecke=ecken_schnittpunkte.begin();
      for (; l_ecke!=ecken_schnittpunkte.end(); )
         if (((*l_ecke).direction==0) && ((*l_ecke).is_ecke))
         {
            //l�schen
            schnittpunkte.erase(find(schnittpunkte.begin(), schnittpunkte.end(), (*l_ecke).schnittpunkt));
            l_ecke=ecken_schnittpunkte.erase(l_ecke);
         }
         else
         {
            //bleibt! --> Distanz zu Sort-Referenzebene ermitteln
            (*l_ecke).dist=Dist_PS_3d((*l_ecke).schnittpunkt, plane);
            l_ecke++;
         }

      //alle Schnittpunkte sortieren
      l_ecke=ecken_schnittpunkte.begin();
      e_ecke=ecken_schnittpunkte.end();
      for (; l_ecke!=e_ecke; l_ecke++)
      {
         l2_ecke=l_ecke;
         l2_ecke++;
         for (; l2_ecke!=e_ecke; l2_ecke++)
            if ((*l_ecke).dist>(*l2_ecke).dist)
            {
               //vertauschen
               sort_dummy=(*l_ecke);
               (*l_ecke)=(*l2_ecke);
               (*l2_ecke)=sort_dummy;
            }
      }

      //in Fleisch Checks
      up_inside=false;
      down_inside=false;
      l_ecke=ecken_schnittpunkte.begin();
      for (; l_ecke!=e_ecke; l_ecke++)
      {
         if ((!(*l_ecke).is_ecke) && ((*l_ecke).direction==1)) //unser gesuchter Punkt
            return (up_inside || down_inside);
         else
         if ((!(*l_ecke).is_ecke) && ((*l_ecke).direction==0)) //kompletter Wechsel
         {
            up_inside=!up_inside;
            down_inside=!down_inside;
         }
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==1))  //Wechsel oben
            up_inside=!up_inside;
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==-1)) //Wechsel unten
            down_inside=!down_inside;
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==0))  //Fehler
            assert(0);
      }

      assert(0);
      return false;
   }
   else
   {
      ecken_schnittpunkte.clear();

      //ist point auf dem Polygon Rand ?
      sl=schnittpunkte.begin();
      se=schnittpunkte.end();
      for (;sl!=se; sl++)
         if ((point-(*sl)).Value()<rand)
            return true;

      //befindet sich point zwischen einer ungeraden Anzahl von Schnittpunkten ?
      return (schnittpunkte.size()>1) && (upper_points%2==1) && (lower_points%2==1);
   }
}

bool CGeom3d::In_Poly_Plane_3d(Geometry::Polygon * pPoly, Vec3d point, double rand)
{
   Vec3d schnittvektor, normale=pPoly->GetNormale();
   Line_3d schnittlinie,linie;
   Point_3d schnittpunkt,lp1,lp2;
   Geometry::Polygon::PointVectorVector::iterator l=pPoly->points.begin(), e=pPoly->points.end();
   Geometry::Polygon::PointVector::iterator l2,l3,e2,l2_next,l2_last;
   PointVector schnittpunkte;
   EckenSchnittpunktVector ecken_schnittpunkte;
   EckenSchnittpunktVector::iterator l_ecke, l2_ecke, e_ecke;
   ecken_schnittpunkt sort_dummy;
   PointVector::iterator sl,sl2,se;
   Plane_3d plane, sort_plane;
   double dist, dist1, dist2;
   long upper_points=0, lower_points=0, ecken_anz=0;
   bool found, up_inside, down_inside;

   Vekt_Rectangular(normale, schnittvektor);
   Line_VP_3d(schnittvektor,point,schnittlinie);
   Plane_PN_3d(point,schnittvektor,plane);
   Plane_PN_3d(point,schnittvektor.amul(normale),sort_plane);

   for(; l!=e; l++)
   {
      l2=(*l).begin();
      e2=(*l).end();

      for (;l2!=e2; l2++)
      {
         l3=l2+1;
         if (l3==e2)
            l3=(*l).begin();

         lp1.x=(**l2).getX();
         lp1.y=(**l2).getY();
         lp1.z=(**l2).getZ();
         lp2.x=(**l3).getX();
         lp2.y=(**l3).getY();
         lp2.z=(**l3).getZ();
         if ((lp1-lp2).Value()<rand) continue;
         Line_PP_3d(lp1,lp2,linie);

         //sind wir auf dem Rand ?
         if (InLine_3d(lp1,lp2,point,rand))
            return true;

         Point_LL_3d(schnittlinie,linie,schnittpunkt,rand);
         if ((Err_3d==1) && (InLine_3d(lp1,lp2,schnittpunkt,rand)))
         {
            schnittpunkte.push_back(schnittpunkt);
            dist=Dist_PS_3d(schnittpunkt,plane);
            if (dist>0.0)
               upper_points++;
            else
               lower_points++;
         }
      }
   }

   //doppelte Schnittpunkte l�schen
   sl=schnittpunkte.begin();
   for (;sl!=schnittpunkte.end(); sl++)
   {
      sl2=sl;
      sl2++;

      for (;sl2!=schnittpunkte.end();)
         if (((*sl)-(*sl2)).Value()<rand)
         {
            dist=Dist_PS_3d(*sl2,plane);
            if (dist>0.0)
               upper_points--;
            else
               lower_points--;
            sl2=schnittpunkte.erase(sl2);
         }
         else
            sl2++;
   }

   //Punkte und Schnitte auf Gleichheit checken
   sl=schnittpunkte.begin();
   for (;sl!=schnittpunkte.end(); sl++)
   {
      schnittpunkt=(*sl);
      found=false;

      l=pPoly->points.begin();
      e=pPoly->points.end();
      for(; l!=e; l++)
      {
         l2=(*l).begin();
         e2=(*l).end();

         for (;l2!=e2; l2++)
         {
            lp1.x=(**l2).getX();
            lp1.y=(**l2).getY();
            lp1.z=(**l2).getZ();

            if ((schnittpunkt-lp1).Value()<rand)
            {
               l2_next=l2;
               l2_last=l2;
               l2_next++;
               if (l2_next==e2)
                  l2_next=(*l).begin();
               if (l2==(*l).begin())
                  l2_last=(*l).end()-1;
               else
                  l2_last--;
               lp1.x=(**l2_next).getX();
               lp1.y=(**l2_next).getY();
               lp1.z=(**l2_next).getZ();
               lp2.x=(**l2_last).getX();
               lp2.y=(**l2_last).getY();
               lp2.z=(**l2_last).getZ();

               dist1=Dist_PS_3d(lp1,sort_plane);
               dist2=Dist_PS_3d(lp2,sort_plane);
               if (fabs(dist1)<rand) dist1=0.0; else dist1/=fabs(dist1);
               if (fabs(dist2)<rand) dist2=0.0; else dist2/=fabs(dist2);

               if ((dist1==0.0) && (dist2==0.0))
                  //gerade Ecke auf Schnittlinie
                  ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, true));
               else
                  if (fabs(dist1*dist2)<rand)
                  {
                     //Winkel nach oben oder unten
                     if (dist1+dist2>0.0)
                        ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,1, true));
                     else
                        ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,-1, true));
                  }
                  else
                     if (dist1*dist2<-rand)
                        //Spitze
                        ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, true));
                     else
                     {
                        //gerade Ecke senkrecht auf Schnittlinie (wird aufgenohmen wie normaler Schnittpunkt)
                        ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, false));
                        ecken_anz--;
                     }
               found=true;
               ecken_anz++;
               break;
            }
         }
         if (found)
            break;
      }
      if (!found)
         ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, false));
   }
   if (ecken_anz>1)
   {
      //gesuchten Punkt einf�gen
      ecken_schnittpunkte.push_back(ecken_schnittpunkt(point,1, false));
      //alle spitzen und geraden Schnittpunkt Ecken l�schen, sort-Distanzen ermitteln
      l_ecke=ecken_schnittpunkte.begin();
      for (; l_ecke!=ecken_schnittpunkte.end(); )
         if (((*l_ecke).direction==0) && ((*l_ecke).is_ecke))
         {
            //l�schen
            schnittpunkte.erase(find(schnittpunkte.begin(), schnittpunkte.end(), (*l_ecke).schnittpunkt));
            l_ecke=ecken_schnittpunkte.erase(l_ecke);
         }
         else
         {
            //bleibt! --> Distanz zu Sort-Referenzebene ermitteln
            (*l_ecke).dist=Dist_PS_3d((*l_ecke).schnittpunkt, plane);
            l_ecke++;
         }

      //alle Schnittpunkte sortieren
      l_ecke=ecken_schnittpunkte.begin();
      e_ecke=ecken_schnittpunkte.end();
      for (; l_ecke!=e_ecke; l_ecke++)
      {
         l2_ecke=l_ecke;
         l2_ecke++;
         for (; l2_ecke!=e_ecke; l2_ecke++)
            if ((*l_ecke).dist>(*l2_ecke).dist)
            {
               //vertauschen
               sort_dummy=(*l_ecke);
               (*l_ecke)=(*l2_ecke);
               (*l2_ecke)=sort_dummy;
            }
      }

      //in Fleisch Checks
      up_inside=false;
      down_inside=false;
      l_ecke=ecken_schnittpunkte.begin();
      for (; l_ecke!=e_ecke; l_ecke++)
      {
         if ((!(*l_ecke).is_ecke) && ((*l_ecke).direction==1)) //unser gesuchter Punkt
            return (up_inside || down_inside);
         else
         if ((!(*l_ecke).is_ecke) && ((*l_ecke).direction==0)) //kompletter Wechsel
         {
            up_inside=!up_inside;
            down_inside=!down_inside;
         }
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==1))  //Wechsel oben
            up_inside=!up_inside;
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==-1)) //Wechsel unten
            down_inside=!down_inside;
         else
         if (((*l_ecke).is_ecke) && ((*l_ecke).direction==0))  //Fehler
            assert(0);
      }

      assert(0);
      return false;
   }
   else
   {
      ecken_schnittpunkte.clear();

      //ist point auf dem Polygon Rand ?
      sl=schnittpunkte.begin();
      se=schnittpunkte.end();
      for (;sl!=se; sl++)
         if ((point-(*sl)).Value()<rand)
            return true;

      //befindet sich point zwischen einer ungeraden Anzahl von Schnittpunkten ?
      return (schnittpunkte.size()>1) && (upper_points%2==1) && (lower_points%2==1);
   }
}

/*
bool CGeom3d::In_Loop_3d(Point_3d_Vector & PV, Point_3d * pPoint, double rand)
{
   Vec3d normale;
   Plane_3d plane;
   double dist;

   //Check: Ist der Punkt in der Poly-Ebene ?
   if (PV.empty())
      return false;
   normale=GetNormale(PV);
   normale.Normalize();
   Plane_PN_3d(*PV.begin(),normale,plane);
   dist=fabs(Dist_PS_3d(*pPoint,plane));
   if (dist>rand)
      return false;

   //Polygon in der richtigen Ebene checken
   return In_Loop_Plane_3d(PV,pPoint,rand);
}
*/
/*
bool CGeom3d::In_Loop_3d(Geometry::Polygon::PointVector * pPVV, Point_3d point, double rand)
{
   Vec3d normale,point_plane;
   Plane_3d plane;
   double dist;

   //Check: Ist der Punkt in der Poly-Ebene ?
   if (pPVV->empty())
      return false;
   normale=GetNormale(*pPVV);
   normale.Normalize();
   point_plane.x=(**(*pPVV).begin()).getX();
   point_plane.y=(**(*pPVV).begin()).getY();
   point_plane.z=(**(*pPVV).begin()).getZ();
   Plane_PN_3d(point_plane,normale,plane);
   dist=fabs(Dist_PS_3d(point,plane));
   if (dist>rand)
      return false;

   //Polygon in der richtigen Ebene checken
   return In_Loop_Plane_3d(pPVV,point,rand);
}
*/
/*
bool CGeom3d::In_Loop_3d(Geometry::Polygon::PointVector * pPVV, Geometry::Point * pPoint, double rand)
{
   Vec3d normale, point;
   Plane_3d plane;
   double dist;

   //Check: Ist der Punkt in der Poly-Ebene ?
   if (pPVV->empty())
      return false;
   normale=GetNormale(*pPVV);
   normale.Normalize();
   point.x=(**(*pPVV).begin()).getX();
   point.y=(**(*pPVV).begin()).getY();
   point.z=(**(*pPVV).begin()).getZ();
   Plane_PN_3d(point,normale,plane);
   point.x=pPoint->getX();
   point.y=pPoint->getY();
   point.z=pPoint->getZ();
   dist=fabs(Dist_PS_3d(point,plane));
   if (dist>rand)
      return false;

   //Polygon in der richtigen Ebene checken
   return In_Loop_Plane_3d(pPVV,pPoint,rand);
}
*/
/*
bool CGeom3d::In_Loop_3d(Geometry::Polygon::PointVector & PVV, Geometry::Point *pPoint, double rand)
{
   Vec3d normale, point;
   Plane_3d plane;
   double dist;

   //Check: Ist der Punkt in der Poly-Ebene ?
   if (PVV.empty())
      return false;
   normale=GetNormale(PVV);
   normale.Normalize();
   point.x=(**PVV.begin()).getX();
   point.y=(**PVV.begin()).getY();
   point.z=(**PVV.begin()).getZ();
   Plane_PN_3d(point,normale,plane);
   point.x=pPoint->getX();
   point.y=pPoint->getY();
   point.z=pPoint->getZ();
   dist=fabs(Dist_PS_3d(point,plane));
   if (dist>rand)
      return false;

   //Polygon in der richtigen Ebene checken
   return In_Loop_Plane_3d(PVV,pPoint,rand);
}
*/
/*
bool CGeom3d::In_Loop_3d(Geometry::Polygon::PointVector & PVV, Point_3d check_point, double rand)
{
   Vec3d normale, point;
   Plane_3d plane;
   double dist;

   //Check: Ist der Punkt in der Poly-Ebene ?
   if (PVV.empty())
      return false;
   normale=GetNormale(PVV);
   normale.Normalize();
   point.x=(**PVV.begin()).getX();
   point.y=(**PVV.begin()).getY();
   point.z=(**PVV.begin()).getZ();
   Plane_PN_3d(point,normale,plane);
   dist=fabs(Dist_PS_3d(check_point,plane));
   if (dist>rand)
      return false;

   //Polygon in der richtigen Ebene checken
   return In_Loop_Plane_3d(PVV,check_point,rand);
}
*/
/*
bool CGeom3d::In_Poly_3d(Geometry::Polygon * pPoly, Geometry::Point * pPoint, double rand)
{
   Vec3d normale, point;
   Plane_3d plane;
   double dist;

   //Check: Ist der Punkt in der Poly-Ebene ?
   if (pPoly->points.empty())
      return false;
   normale=pPoly->GetNormale();
   normale.Normalize();
   point.x=(**(*pPoly->points.begin()).begin()).getX();
   point.y=(**(*pPoly->points.begin()).begin()).getY();
   point.z=(**(*pPoly->points.begin()).begin()).getZ();
   Plane_PN_3d(point,normale,plane);
   point.x=pPoint->getX();
   point.y=pPoint->getY();
   point.z=pPoint->getZ();
   dist=fabs(Dist_PS_3d(point,plane));
   if (dist>rand)
      return false;

   //Polygon in der richtigen Ebene checken
   return In_Poly_Plane_3d(pPoly,pPoint,rand);
}
*/
/*
bool CGeom3d::In_Poly_3d(Geometry::Polygon * pPoly, Vec3d vPoint, double rand)
{
   Vec3d normale, point;
   Plane_3d plane;
   double dist;

   //Check: Ist der Punkt in der Poly-Ebene ?
   if (pPoly->points.empty())
      return false;
   normale=pPoly->GetNormale();
   normale.Normalize();
   point.x=(**(*pPoly->points.begin()).begin()).getX();
   point.y=(**(*pPoly->points.begin()).begin()).getY();
   point.z=(**(*pPoly->points.begin()).begin()).getZ();
   Plane_PN_3d(point,normale,plane);
   dist=fabs(Dist_PS_3d(vPoint,plane));
   if (dist>rand)
      return false;

   //Polygon in der richtigen Ebene checken
   return In_Poly_Plane_3d(pPoly,vPoint,rand);
}
*/
/*
bool CGeom3d::InTriangle(Point_3d & p1,
                         Point_3d & p2,
                         Point_3d & p3,
                         Point_3d * pCheck)
{
   Point_3d_Vector PV;

   PV.push_back(p1);
   PV.push_back(p2);
   PV.push_back(p3);

   return In_Loop_3d(PV, pCheck);
}
*/
/*
PROCEDURE Line_VP_3d (V:Vekt_3d; P:Point_3d;
                      VAR L:Line_3d);
BEGIN
 L.a:= V.x;
 L.b:= V.y;
 L.c:= V.z;
 L.x:= P.x;
 L.y:= P.y;
 L.z:= P.z;
END;
*/
/*
void CGeom3d::Line_VP_3d(Vekt_3d V, Point_3d P, Line_3d & L)
{
   L.v.x= V.x;
   L.v.y= V.y;
   L.v.z= V.z;
   L.p.x= P.x;
   L.p.y= P.y;
   L.p.z= P.z;
}
*/

//bool CGeom3d::PolyInPoly_3d(Geometry::Polygon * pTestPoly, Geometry::Polygon * pPoly, double rand/*= 0.001*/, bool check_schwerpunkt/*=false*/)
/*
{
   Geometry::Polygon::PointVectorVector::iterator l=pTestPoly->points.begin(), e=pTestPoly->points.end();
   Geometry::Polygon::PointVector::iterator l2,l3,e2;
   Vec3d Linienmittelpunkt(0,0,0), Schwerpunkt(0,0,0),p1,p2;
   long Anzahl=0;
 
   for(; l!=e; l++)
   {
      l2=(*l).begin();
      e2=(*l).end();

      for (;l2!=e2; l2++)
      {
         if (!In_Poly_3d(pPoly,(*l2),rand))
            return false;
         
         l3=l2;
         l3++;
         if (l3==e2)
            l3=(*l).begin();

         p1=Vec3d((**l2).getX(),(**l2).getY(),(**l2).getZ());
         p2=Vec3d((**l3).getX(),(**l3).getY(),(**l3).getZ());

         Linienmittelpunkt=(p1+p2)/2;
         Schwerpunkt+=p1;
         Anzahl++;

         if (!In_Poly_3d(pPoly,Linienmittelpunkt,rand))
            return false;
      }
   }

   if (Anzahl<=0)
      return false;
   
   //es sollten konvexe Polygone sein, sodass der Schwerpunkt im dazugeh�rigen Poly liegt
   if (check_schwerpunkt)
   {
      Schwerpunkt/=Anzahl;
      if (!In_Poly_3d(pPoly,Schwerpunkt,rand))
         return false;
   }

   //Schwerpunkte der Polydreiecke checken
   Geometry::Polygon::TriangleVector triangles=pTestPoly->GetTriangles();
   Geometry::Polygon::TriangleVector::iterator tl=triangles.begin(), te=triangles.end();
   Vec3d center;
   for (; tl!=te; tl++)
   {
      center.x = ((*tl).p1.x + (*tl).p2.x + (*tl).p3.x) / 3;
      center.y = ((*tl).p1.y + (*tl).p2.y + (*tl).p3.y) / 3;
      center.z = ((*tl).p1.z + (*tl).p2.z + (*tl).p3.z) / 3;
      if (!In_Poly_3d(pPoly,center,rand))
         return false;
   }
   return true;
}
*/
//bool CGeom3d::LoopInLoop_3d(Geometry::Polygon::PointVector &TestLoop, 
//                            Geometry::Polygon::PointVector &Loop, 
//                            double rand/*= 0.001*/)
/*
{
   Geometry::Polygon::PointVector::iterator l2,l3,e2;
   Vec3d Linienmittelpunkt(0.0,0.0,0.0), p1,p2;
   long Anzahl=0;
 
   l2=TestLoop.begin();
   e2=TestLoop.end();

   for (;l2!=e2; l2++)
   {
      if (!In_Loop_3d(Loop,(*l2),rand))
         return false;
      
      l3=l2;
      l3++;
      if (l3==e2)
         l3=TestLoop.begin();

      p1=Vec3d((**l2).getX(),(**l2).getY(),(**l2).getZ());
      p2=Vec3d((**l3).getX(),(**l3).getY(),(**l3).getZ());

      Linienmittelpunkt=(p1+p2)/2;
      Anzahl++;

      if (!In_Loop_3d(Loop,Linienmittelpunkt,rand))
         return false;
   }

   if (Anzahl<=0)
      return false;

   return true;
}
*/
/*
FUNCTION  Dist_SS_3d (S1,S2:Plane_3d):Real;
VAR
 Len:Real;
BEGIN
 Len:= Sqrt(S1.a * S1.a + S1.b * S1.b + S1.c * S1.c);
 IF Abs (Len)< Eps6 THEN Syntax (' Dist_SS_3d: S1 ');
 S1.a:= S1.a/Len;
 S1.b:= S1.b/Len;
 S1.c:= S1.c/Len;
 S1.d:= S1.d/Len;
 Len:= Sqrt(S2.a * S2.a + S2.b * S2.b + S2.c * S2.c);
 IF Abs (Len) < Eps6 THEN Syntax (' Dist_SS_3d: S2 ');
 S2.a:= S2.a/Len;
 S2.b:= S2.b/Len;
 S2.c:= S2.c/Len;
 S2.d:= S2.d/Len;
 IF Abs (S1.a - S2.a) > Eps6 THEN BEGIN Dist_SS_3d:= 0; Exit; END;
 IF Abs (S1.b - S2.b) > Eps6 THEN BEGIN Dist_SS_3d:= 0; Exit; END;
 IF Abs (S1.c - S2.c) > Eps6 THEN BEGIN Dist_SS_3d:= 0; Exit; END;
 Dist_SS_3d:= S1.d - S2.d;
END;
*/
/*
double CGeom3d::Dist_SS_3d (Plane_3d S1, Plane_3d S2)
{
   double Len;

   Err_3d= 1;
   Len= sqrt(S1.a * S1.a + S1.b * S1.b + S1.c * S1.c);
   if (fabs (Len)< Eps6) { Err_3d= 0; return 0; }
   S1.a= S1.a/Len;
   S1.b= S1.b/Len;
   S1.c= S1.c/Len;
   S1.d= S1.d/Len;
   Len= sqrt(S2.a * S2.a + S2.b * S2.b + S2.c * S2.c);
   if (fabs (Len) < Eps6)  { Err_3d= 0; return 0; }
   S2.a= S2.a/Len;
   S2.b= S2.b/Len;
   S2.c= S2.c/Len;
   S2.d= S2.d/Len;
   if (fabs (S1.a - S2.a) > Eps6) { Err_3d= 0; return 0; }
   if (fabs (S1.b - S2.b) > Eps6) { Err_3d= 0; return 0; }
   if (fabs (S1.c - S2.c) > Eps6) { Err_3d= 0; return 0; }
   return (S1.d - S2.d);
}
*/
/*
PROCEDURE Point_LS_3d (L:Line_3d; S:Plane_3d; VAR P:Point_3d);
VAR
 q:Real;
 r:Real;
BEGIN
 q:= S.a * L.a + S.b * L.b + S.c * L.c;
 IF abs(q) < Eps6 THEN Err_3d:= 0
 ELSE BEGIN
  r:= (S.a * L.x + S.b * L.y + S.c * L.z + S.d)/q;
  P.x:= L.x - L.a * r;
  P.y:= L.y - L.b * r;
  P.z:= L.z - L.c * r;
  Err_3d:=1;
 END;
END;
*/
/*
void CGeom3d::Point_LS_3d (Line_3d L, Plane_3d S, Point_3d & P)
{
   double q, r;

   q = S.a * L.v.x + S.b * L.v.y + S.c * L.v.z;
   if (fabs(q) < Eps6) { Err_3d = 0; return; }
   else
   {
      r = (S.a * L.p.x + S.b * L.p.y + S.c * L.p.z + S.d)/q;
      P.x = L.p.x - L.v.x * r;
      P.y = L.p.y - L.v.y * r;
      P.z = L.p.z - L.v.z * r;
      Err_3d = 1;
   }
}
*/
/*
PROCEDURE Transl_3d (    P0:Point_3d; tx,ty,tz:Real;
                     VAR P:Point_3d);
BEGIN
 P.x:= P0.x + tx;
 P.y:= P0.y + ty;
 P.z:= P0.z + tz;
END;

PROCEDURE Rot_X_3d (     P0:Point_3d; sinwx,coswx:Real;
                     VAR P:Point_3d);
BEGIN
 P.x:= P0.x;
 P.y:= P0.y * coswx - P0.z * sinwx;
 P.z:= P0.y * sinwx + P0.z * coswx;
END;

PROCEDURE Rot_Y_3d (     P0:Point_3d; sinwy,coswy:Real;
                     VAR P:Point_3d);
BEGIN
 P.x:= P0.x * coswy + P0.z * sinwy;
 P.y:= P0.y;
 P.z:=-P0.x * sinwy + P0.z * coswy;
END;

PROCEDURE Rot_Z_3d (     P0:Point_3d; sinwz,coswz:Real;
                     VAR P:Point_3d);
BEGIN
 P.x:= P0.x * coswz - P0.y * sinwz;
 P.y:= P0.x * sinwz + P0.y * coswz;
 P.z:= P0.z;
END;
PROCEDURE Rot_L_3d (     P0:Point_3d; L:Line_3d;sinwl,coswl:Real;
                     VAR P:Point_3d);
VAR
 yz,xyz: Real;
BEGIN
 Transl_3d (P0,-L.x,-L.y,-L.z,P);
 yz:= Sqrt ( Sqr(L.b) + Sqr(L.c) );
 Rot_X_3d (P,L.b/yz,L.c/yz,P);
 xyz:= Sqrt ( Sqr(L.a) + Sqr(L.b) + Sqr(L.c) );
 Rot_Y_3d (P,-L.a/xyz,yz/xyz,P);
 Rot_Z_3d (P,sinwl,coswl,P);
 Rot_Y_3d (P,L.a/xyz,yz/xyz,P);
 Rot_X_3d (P,-L.b/yz,L.c/yz,P);
 Transl_3d (P,L.x,L.y,L.z,P);
END;
*/
/*
void CGeom3d::Rot_X_3d ( Point_3d & P, double sinwx, double coswx)
{
   Point_3d P0=P;

   P.x = P0.x;
   P.y = P0.y * coswx - P0.z * sinwx;
   P.z = P0.y * sinwx + P0.z * coswx;
}
*/
/*
void CGeom3d::Rot_Y_3d ( Point_3d & P, double sinwy, double coswy)
{
   Point_3d P0=P;

   P.x = P0.x * coswy + P0.z * sinwy;
   P.y = P0.y;
   P.z =-P0.x * sinwy + P0.z * coswy;
}
*/
/*
void CGeom3d::Rot_Z_3d ( Point_3d & P, double sinwz, double coswz)
{
   Point_3d P0=P;

   P.x = P0.x * coswz - P0.y * sinwz;
   P.y = P0.x * sinwz + P0.y * coswz;
   P.z = P0.z;
}
*/
/*
void CGeom3d::Rot_L_3d ( Point_3d & P, Line_3d L, double sinwl, double coswl)
{
   double yz,xyz;

   P = P - L.p;
   yz = sqrt ( pow(L.v.y,2) + pow(L.v.z,2) );
   Rot_X_3d (P,L.v.y/yz,L.v.z/yz);
   xyz = sqrt ( pow(L.v.x,2) + pow(L.v.y,2) + pow(L.v.z,2) );
   Rot_Y_3d (P,-L.v.x/xyz,yz/xyz);
   Rot_Z_3d (P,sinwl,coswl);
   Rot_Y_3d (P,L.v.x/xyz,yz/xyz);
   Rot_X_3d (P,-L.v.y/yz,L.v.z/yz);
   P = P + L.p;
}
*/
/*
void CGeom3d::Rot_Achse_3d ( Point_3d & P, Vekt_3d A, double sinwl, double coswl)
{
   double yz,xyz;

   yz = sqrt ( pow(A.y,2) + pow(A.z,2) );
   Rot_X_3d (P,A.y/yz,A.z/yz);
   xyz = sqrt ( pow(A.x,2) + pow(A.y,2) + pow(A.z,2) );
   Rot_Y_3d (P,-A.x/xyz,yz/xyz);
   Rot_Z_3d (P,sinwl,coswl);
   Rot_Y_3d (P,A.x/xyz,yz/xyz);
   Rot_X_3d (P,-A.y/yz,A.z/yz);
}
*/

//aus CMyBar2
/*
Vec3d CGeom3d::GetNormale(Geometry::Polygon * pPoly)
{
	Geometry::Polygon::PointVectorVector::iterator pvl = pPoly->points.begin();
	Geometry::Polygon::PointVector::iterator p1,p2,p3;
	p1=pvl->begin();
	p2=p1+1;
	p3=p2+1;
	Vec3d v1,v2,v;
	v1.x = (**p1).getX() - (**p2).getX();
	v1.y = (**p1).getY() - (**p2).getY();
	v1.z = (**p1).getZ() - (**p2).getZ();
	v2.x = (**p3).getX() - (**p2).getX();
	v2.y = (**p3).getY() - (**p2).getY();
	v2.z = (**p3).getZ() - (**p2).getZ();
	v.x = v1.y * v2.z - v1.z * v2.y;
	v.y = v1.z * v2.x - v1.x * v2.z;
	v.z = v1.x * v2.y - v1.y * v2.x;
	//Einheitsvektor bilden
	double dv = sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
	if (dv!=0)
	{
		v.x /= dv;
		v.y /= dv;
		v.z /= dv;
	}
	return v;
}
*/
/*
Vec3d CGeom3d::GetNormale(Point_3d_Vector & PV)
{
	Point_3d p1,p2,p3;
	p1=PV[0];
	p2=PV[1];
	p3=PV[2];
	Vec3d v1,v2,v;
   v1 = p1 - p2;
   v2 = p3 - p2;
   v = v1.amul(v2);
   v.Normalize();

	return v;
}
*/
/*
Vec3d CGeom3d::GetNormale(Geometry::Polygon::PointVector poly)
{
	Geometry::Polygon::PointVector::iterator p1,p2,p3;
	p1=poly.begin();
	p2=p1+1;
	p3=p2+1;
	Vec3d v1,v2,v;
	v1.x = (**p1).getX() - (**p2).getX();
	v1.y = (**p1).getY() - (**p2).getY();
	v1.z = (**p1).getZ() - (**p2).getZ();
	v2.x = (**p3).getX() - (**p2).getX();
	v2.y = (**p3).getY() - (**p2).getY();
	v2.z = (**p3).getZ() - (**p2).getZ();
	v.x = v1.y * v2.z - v1.z * v2.y;
	v.y = v1.z * v2.x - v1.x * v2.z;
	v.z = v1.x * v2.y - v1.y * v2.x;
	//Einheitsvektor bilden
	double dv = sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
	if (dv!=0)
	{
		v.x /= dv;
		v.y /= dv;
		v.z /= dv;
	}
	return v;
}
*/
/*
void CGeom3d::AreaLoopXY(Geometry::Polygon::PointVector poly, double *area)
{
   unsigned int i;
   Geometry::Point *pi,*pj;

   *area=0;
   for (i=0; i<poly.size(); i++)
   {
      pi=poly[i];
      if (i<poly.size()-1)
         pj=poly[i+1];
      else
         pj=poly[0];
      *area+=((*pi).getX() - (*pj).getX()) * ((*pi).getY() + (*pj).getY());
   }
   *area/=2;
}

void CGeom3d::AreaLoopAngle(Geometry::Polygon::PointVector poly, double *area, double *angleH, double *angleV)
{
   unsigned int i;
   Geometry::Point *pj;
   Vec3d vpi,vpj,zw;

   *area=0;

   //zur�ckdrehen
   vpj.x=(*poly[0]).getX();
   vpj.y=(*poly[0]).getY();
   vpj.z=(*poly[0]).getZ();
   zw=vpj;
   vpj.x=(zw.x*cos(-*angleH)-zw.y*sin(-*angleH));
   vpj.y=(zw.y*cos(-*angleH)+zw.x*sin(-*angleH));
   zw=vpj;
   vpj.y=(zw.y*cos(-*angleV)-zw.z*sin(-*angleV));
   //vpj.z=(zw.z*cos(-*angleV)+zw.y*sin(-*angleV));

   for (i=0; i<poly.size(); i++)
   {
      vpi=vpj;

      if (i<poly.size()-1)
         pj=poly[i+1];
      else
         pj=poly[0];

      //zur�ckdrehen
      vpj.x=(*pj).getX();
      vpj.y=(*pj).getY();
      vpj.z=(*pj).getZ();
      zw=vpj;
      vpj.x=(zw.x*cos(-*angleH)-zw.y*sin(-*angleH));
      vpj.y=(zw.y*cos(-*angleH)+zw.x*sin(-*angleH));
      zw=vpj;
      vpj.y=(zw.y*cos(-*angleV)-zw.z*sin(-*angleV));
      //vpj.z=(zw.z*cos(-*angleV)+zw.y*sin(-*angleV));

      *area+=(vpi.x - vpj.x) * (vpi.y + vpj.y);
   }
   *area/=2;
}

void CGeom3d::AreaPoly(Geometry::Polygon * pPoly, double *area)
{
   Geometry::Polygon::PointVectorVector::iterator l=pPoly->points.begin();
   Geometry::Polygon::PointVectorVector::iterator e=pPoly->points.end();
   double looparea;
   Vec3d blick=GetNormale(pPoly);
   double angleH,angleV;
   *area=0;

   //Winkelermittlung
   if (fabs(blick.y)>Eps6)
      angleH=atan(-blick.x/blick.y);
   else
   {
      if (blick.x<0) angleH=PI/2.0;
      else           angleH=3*PI/2.0;
   }
   if (blick.y<0)    angleH+=PI;

	if ((sqrt(blick.x*blick.x+blick.y*blick.y)) > Eps6)
		angleV=atan(blick.z/sqrt(blick.x*blick.x+blick.y*blick.y));
	else
	{
		if (blick.z>0)	 angleV=PI/2.0;
		else  		    angleV=-PI/2.0;
	}
   angleV+=PI/2.0;


   for (;l!=e;l++)
   {
      AreaLoopAngle(*l,&looparea,&angleH,&angleV);
      if (l==pPoly->points.begin())
         *area=fabs(looparea);
      else
         *area-=fabs(looparea);
   }
}

void CGeom3d::AreaLoop(Geometry::Polygon::PointVector poly, double *area)
{
   double looparea;
   Vec3d blick=GetNormale(poly);
   double angleH,angleV;
   *area=0;

   //Winkelermittlung
   if (fabs(blick.y)>Eps6)
      angleH=atan(-blick.x/blick.y);
   else
   {
      if (blick.x<0) angleH=PI/2.0;
      else           angleH=3*PI/2.0;
   }
   if (blick.y<0)    angleH+=PI;

	if ((sqrt(blick.x*blick.x+blick.y*blick.y)) > Eps6)
		angleV=atan(blick.z/sqrt(blick.x*blick.x+blick.y*blick.y));
	else
	{
		if (blick.z>0)	 angleV=PI/2.0;
		else  		    angleV=-PI/2.0;
	}
   angleV+=PI/2.0;

   AreaLoopAngle(poly,&looparea,&angleH,&angleV);
   *area=looparea;
}

//von Torsten aus polygon.cpp

void CGeom3d::Jacobi (const double b[3][3], double (&ov) [3], double (&v) [3][3])
{
   typedef size_t INDEX;
   static const size_t n=3;
   static const double epsilon =1.0E-13;
   static const double beta =1.0E-15;

   double a[n][n];

   for (INDEX i=0; i<n; i++)
   {
      for (INDEX j=0; j<n; j++)
      {
         v[i][j]=0;
         a[i][j]=b[i][j];
      }

      v[i][i]=1;
   }

   long unsigned int counter=0;

   while (1)
   {
      double sum=0;
      for (INDEX i=1; i<n; i++)
         for (INDEX j=0; j<i; j++)
            sum+=sqr(a[i][j]);

      if (2*sum<sqr(epsilon)||counter>500)
      {
         for (INDEX i=0; i<n; i++)
         {
            ov[i]=a[i][i];
         }
         
         break;
      }

      for (INDEX p=0; p<n-1; p++)
         for (INDEX q=p+1; q<n; q++)
         {
            if (fabs(a[q][p])<epsilon)
               continue;

            double O=(a[q][q]-a[p][p])/(2*a[q][p]);
            
            double t=1;
            if (fabs(O)>beta)
               t=1.0/(O+sgn(O)*sqrt(sqr(O)+1));

            double c=1.0/sqrt(1+sqr(t));
            double s=c*t;
            double r=s/(1.0+c);
            a[p][p]-=t*a[q][p];
            a[q][q]+=t*a[q][p];
            a[q][p]=0;

            for (INDEX j=0; j<p; j++)
            {
               double g=a[q][j]+r*a[p][j];
               double h=a[p][j]-r*a[q][j];
               a[p][j]-=s*g;
               a[q][j]+=s*h;
            }

            for (INDEX i=p+1; i<q; i++)
            {
               double g=a[q][i]+r*a[i][p];
               double h=a[i][p]-r*a[q][i];
               a[i][p]-=s*g;
               a[q][i]+=s*h;
            }

            for (INDEX i=q+1; i<n; i++)
            {
               double g=a[i][q]+r*a[i][p];
               double h=a[i][p]-r*a[i][q];
               a[i][p]-=s*g;
               a[i][q]+=s*h;
            }

            for (INDEX i=0; i<n; i++)
            {
               double g=v[i][q]+r*v[i][p];
               double h=v[i][p]-r*v[i][q];
               v[i][p]-=s*g;
               v[i][q]+=s*h;
            }
         }

      counter++;
   }
}

void CGeom3d::GetNorm (double (&a)[3][3], double &x, double &y, double &z)
{
   double v[3];
   double vec[3][3];
   
   Jacobi(a,v,vec);
   
   unsigned char i;

   const double epsilon = 1.0E-10;
   if (v[0]>v[1])
      if (v[1]>v[2])
      {
         if (v[1]<v[2]+epsilon)
         {
            x=y=z=0;
            return;
         }
         i=2;
      }
      else
      {
         if (v[1]+epsilon>v[2])
         {
            x=y=z=0;
            return;
         }
         i=1;
      }
   else
      if (v[0]>v[2])
      {
         if (v[0]<v[2]+epsilon)
         {
            x=y=z=0;
            return;
         }
         i=2;
      }
      else
      {
         if (v[0]+epsilon>v[2])
         {
            x=y=z=0;
            return;
         }
         i=0;
      }

   x=vec[0][i];
   y=vec[1][i];
   z=vec[2][i];
}


void CGeom3d::ComputeKMatrix (const size_t size, const double * A, double (&K)[3][3])
{
   for (unsigned int l1=0; l1<3; l1++)
      for (unsigned int l2=0; l2<3; l2++)
      {
         K[l1][l2]=0;
         for (unsigned l3=0; l3<size; l3++)
            K[l1][l2]+=A[l3*3+l1]*A[l3*3+l2];
      }
}

void CGeom3d::ComputePlane (const Geometry::Polygon::PointVectorVector & points, Vec3d & center, Vec3d & vec)
{
   size_t size = 0;

   Geometry::Polygon::PointVectorVector::const_iterator l1=points.begin(), e1=points.end();
   for (; l1!=e1; l1++)
      size+=(*l1).size();

   double * A  = new double [size*3];
   center = Vec3d(0,0,0);
   size_t position = 0;
   l1=points.begin();
   for (; l1!=e1; l1++)
   {
      Geometry::Polygon::PointVector::const_iterator l2=(*l1).begin(), e2=(*l1).end();
      for (; l2!=e2; l2++)
      {
         center.x+=A[position++]=(*l2)->getX();
         center.y+=A[position++]=(*l2)->getY();
         center.z+=A[position++]=(*l2)->getZ();
      }
   }

   center/=size;

   position = 0;
   for (size_t l3=0; l3<size; l3++)
   {
      A[position++]-=center.x;
      A[position++]-=center.y;
      A[position++]-=center.z;
   }

   double K[3][3];
   ComputeKMatrix(size,A,K);
   double x,y,z;
   GetNorm(K,x,y,z);
   delete A;
   vec=Vec3d(x,y,z);
}

void CGeom3d::ComputePlane (const Geometry::Polygon::PointVector & points, Vec3d & center, Vec3d & vec)
{
   size_t size = points.size();

   double * A  = new double [size*3];
   center = Vec3d(0,0,0);
   size_t position = 0;

   Geometry::Polygon::PointVector::const_iterator l2=points.begin(), e2=points.end();
   for (; l2!=e2; l2++)
   {
      center.x+=A[position++]=(*l2)->getX();
      center.y+=A[position++]=(*l2)->getY();
      center.z+=A[position++]=(*l2)->getZ();
   }

   center/=size;

   position = 0;
   for (size_t l3=0; l3<size; l3++)
   {
      A[position++]-=center.x;
      A[position++]-=center.y;
      A[position++]-=center.z;
   }

   double K[3][3];
   ComputeKMatrix(size,A,K);
   double x,y,z;
   GetNorm(K,x,y,z);
   delete A;
   vec=Vec3d(x,y,z);
}

void CGeom3d::ComputePlane (const Point_3d_Vector & points, Vec3d & center, Vec3d & vec)
{
   size_t size = points.size();

   double * A  = new double [size*3];
   center = Vec3d(0,0,0);
   size_t position = 0;

   Point_3d_Vector::const_iterator l2=points.begin(), e2=points.end();
   for (; l2!=e2; l2++)
   {
      center.x+=A[position++]=(*l2).x;
      center.y+=A[position++]=(*l2).y;
      center.z+=A[position++]=(*l2).z;
   }

   center/=size;

   position = 0;
   for (size_t l3=0; l3<size; l3++)
   {
      A[position++]-=center.x;
      A[position++]-=center.y;
      A[position++]-=center.z;
   }

   double K[3][3];
   ComputeKMatrix(size,A,K);
   double x,y,z;
   GetNorm(K,x,y,z);
   delete A;
   vec=Vec3d(x,y,z);
}
*/
/*
PROCEDURE Proj_N_3d (P0:Point_3d; S:Plane_3d; VAR P:Point_3d);
VAR
 q,r:Real;
BEGIN
 q:= S.a * S.a + S.b * S.b + S.c * S.c;
 r:= (S.a * P0.x + S.b * P0.y + S.c * P0.z + S.d)/q;
 P.x:= P0.x - S.a * r;
 P.y:= P0.y - S.b * r;
 P.z:= P0.z - S.c * r;
END;

PROCEDURE Proj_V_3d (P0:Point_3d; V:Vekt_3d; S:Plane_3d; VAR P:Point_3d);
VAR
 q,r: Real;
BEGIN
 q:= S.a * V.x + S.b * V.y + S.c * V.z;
 IF Abs(q) < Eps6 THEN Err_3d:= 0
 ELSE BEGIN
  r:= (S.a * P0.x + S.b * P0.y + S.c * P0.z + S.d) / q;
  P.x:= P0.x - V.x * r;
  P.y:= P0.y - V.y * r;
  P.z:= P0.z - V.z * r;
  Err_3d:= 1;
 END;
END;
*/

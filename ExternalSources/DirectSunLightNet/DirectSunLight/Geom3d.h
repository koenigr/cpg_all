#pragma once
#include <math.h>
#include "MyMath.h"
#include "Geom2d.h"
//#include <GeometryKernel7\GeometryKernel7.h>
//#include <GeometryKernel7\OpenGLKernel.h>
#include <float.h>
#include <vector>
#include <glew\glew.h>
#include <assert.h>

namespace DSL
{

#ifndef Eps6
#define Eps6         0.000001
#endif

#ifndef Eps3
#define Eps3         0.001
#endif

class CGeom3d
{
public:
   CGeom3d::CGeom3d(void)
   {
   };

   CGeom3d::~CGeom3d(void)
   {
   };

   //Typen
   typedef TemplateVec3d<double> Vec3d;
   typedef TemplateVec3d<long double> Vec3d_long;
   typedef std::vector<Vec3d> PointVector;

   struct Line_3d
   {
      Vec3d p;
      Vec3d v;
   };

   class Section_3d
   {
   public:
      Vec3d p1;
      Vec3d p2;

      Section_3d(void) {}
      Section_3d(Vec3d n1, Vec3d n2): p1(n1), p2(n2) {}
   };
   typedef std::vector<Section_3d> SectionVector;

   struct Plane_3d
   {
      Plane_3d()
      {
      }

      Plane_3d(double na, double nb, double nc, double nd): a(na), b(nb), c(nc), d(nd)
      {
      }

      double a,b,c,d;
      Vec3d GetNormale() const
      {
         return (Vec3d(a,b,c));
      };
   };

   struct Plane_3d_long
   {
      Plane_3d_long()
      {
      }

      Plane_3d_long(long double na, long double nb, long double nc, long double nd): a(na), b(nb), c(nc), d(nd)
      {
      }

      long double a,b,c,d;
      Vec3d_long GetNormale() const
      {
         return (Vec3d_long(a,b,c));
      };
   };

   typedef Vec3d Point_3d;
   typedef Vec3d Vekt_3d;

   typedef std::vector<Point_3d> Point_3d_Vector;
   typedef std::vector<Point_3d_Vector> Point_3d_Vector_Vector;

   struct ecken_schnittpunkt
   {
      ecken_schnittpunkt()
      {
      }

      ecken_schnittpunkt(Point_3d pos, int new_direction, bool new_is_ecke): schnittpunkt(pos), direction(new_direction), is_ecke(new_is_ecke)
      {
      }

      Point_3d schnittpunkt;
      int direction;
      bool is_ecke;
      double dist;
   };
   typedef std::vector<ecken_schnittpunkt> EckenSchnittpunktVector;

   struct Help
   {
      GLdouble tmp[3];
   };

   struct Triangle_3d
   {
	   Vec3d m_P1, m_P2, m_P3;

	   Triangle_3d()
	   {
	   }
	   Triangle_3d(Vec3d p1, Vec3d p2, Vec3d p3): m_P1(p1), m_P2(p2), m_P3(p3)
	   {
	   }
   };

   typedef std::vector<Triangle_3d> TriangleVector;

   //Variablen
   mutable int Err_3d;

   //Methoden
   //void Jacobi (const double b[3][3], double (&ov) [3], double (&v) [3][3]);
   //void GetNorm (double (&a)[3][3], double &x, double &y, double &z);
   //void ComputeKMatrix (const size_t size, const double * A, double (&K)[3][3]);
   //void ComputePlane (const Geometry::Polygon::PointVectorVector & points, Vec3d & center, Vec3d & vec);
   //void ComputePlane (const Geometry::Polygon::PointVector & points, Vec3d & center, Vec3d & vec);
   //void ComputePlane (const Point_3d_Vector & points, Vec3d & center, Vec3d & vec);

   void TesselatePVV( const Point_3d_Vector_Vector & points, TriangleVector & triangles );
/*
   inline Vec3d GetVec3d(Geometry::Point * pPoint)
   {
      return Vec3d(pPoint->getX(),
                   pPoint->getY(),
                   pPoint->getZ());
   }

   inline Vec3d_long GetVec3d_long(Geometry::Point * pPoint)
   {
      return Vec3d_long(pPoint->getX(),
                        pPoint->getY(),
                        pPoint->getZ());
   }
*/
   inline void Point_SXY_3d (Plane_3d S, Point_3d & P)
   {
      if (fabs(S.c) < Eps6)
         Err_3d= 0;
      else 
      {
         P.z= -(S.a * P.x + S.b * P.y + S.d)/S.c;
         Err_3d= 1;
      }
   }
   inline void Point_SXZ_3d (Plane_3d S, Point_3d & P)
   {
      if (fabs(S.b) < Eps6)
         Err_3d= 0;
      else
      {
         P.y= -(S.a * P.x + S.c * P.z + S.d)/S.b;
         Err_3d= 1;
      }
   }
   inline void Point_SYZ_3d (Plane_3d S, Point_3d & P)
   {
      if (fabs(S.a) < Eps6)
         Err_3d= 0;
      else
      {
         P.x= -(S.b * P.y + S.c * P.z + S.d)/S.a;
         Err_3d= 1;
      }
   }
   inline void Point_LL_3d (Line_3d L1, Line_3d L2, Point_3d & P, double delta= Eps6)
   {
      //neu
      Vekt_3d N;

      N = L1.v.amul(L2.v);

      if (N.Value()<Eps6) 
      {
         //parallel
         Err_3d = 0;
      }
      else
      {
         //nicht parallel
         Plane_3d S1, S2;
         Point_3d P1, P2;

         Plane_PVV_3d(L1.p, L1.v, N, S1);
         Plane_PVV_3d(L2.p, L2.v, N, S2);
         Point_LS_3d(L2, S1, P1);
         Point_LS_3d(L1, S2, P2);
         if  ((P1-P2).Value()<=delta)
         {
            Err_3d = 1;
            P = (P1+P2)/2.0;
         }
         else
            Err_3d = 0;
      }
      //alt und funktioniert nicht (?):
/*
      double Dist;
      Plane_3d S;
      CGeom2d::Point_2d Pp;
      CGeom2d::Line_2d LL1,LL2;

      Dist= Dist_LL_3d (L1,L2);
      if (fabs(Dist) > delta)
         Err_3d= 0;
      else 
      {
         Plane_PVV_3d (L1.p,L1.v,L2.v,S);
         if ((fabs (S.a) > fabs (S.b)) && (fabs (S.a) > fabs (S.c)))
         {
            LL1.p.x= L1.p.y; LL1.p.y= L1.p.z;  LL1.v.x= L1.v.y; LL1.v.y= L1.v.z;
            LL2.p.x= L2.p.y; LL2.p.y= L2.p.z;  LL2.v.x= L2.v.y; LL2.v.y= L2.v.z;
            Geom2d.Point_LL_2d (LL1,LL2,Pp);
            P.y= Pp.x; P.z= Pp.y;
            Point_SYZ_3d (S,P);
         } else
         if ((fabs (S.b) > fabs (S.a)) && (fabs (S.b) > fabs (S.c)))  
         {
            LL1.p.x= L1.p.x; LL1.p.y= L1.p.z;  LL1.v.x= L1.v.x; LL1.v.y= L1.v.z;
            LL2.p.x= L2.p.x; LL2.p.y= L2.p.z;  LL2.v.x= L2.v.x; LL2.v.y= L2.v.z;
            Geom2d.Point_LL_2d (LL1,LL2,Pp);
            P.x= Pp.x; P.z= Pp.y;
            Point_SXZ_3d (S,P);
         } else
         if ((fabs (S.c) > fabs (S.a)) && (fabs (S.c) > fabs (S.b)))
         {
            LL1.p.x= L1.p.x; LL1.p.y= L1.p.y;  LL1.v.x= L1.v.x; LL1.v.y= L1.v.y;
            LL2.p.x= L2.p.x; LL2.p.y= L2.p.y;  LL2.v.x= L2.v.x; LL2.v.y= L2.v.y;
            Geom2d.Point_LL_2d (LL1,LL2,Pp);
            P.x= Pp.x; P.y= Pp.y;
            Point_SXY_3d (S,P);
         } else 
         {
            Err_3d= 0;
            return;
         }
         Err_3d=1;
      }
*/
   }

   inline void Point_LS_3d (Line_3d L, Plane_3d S, Point_3d & P)
   {
      double q, r;

      q = S.a * L.v.x + S.b * L.v.y + S.c * L.v.z;
      if (fabs(q) < Eps6) { Err_3d = 0; return; }
      else
      {
         r = (S.a * L.p.x + S.b * L.p.y + S.c * L.p.z + S.d)/q;
         P.x = L.p.x - L.v.x * r;
         P.y = L.p.y - L.v.y * r;
         P.z = L.p.z - L.v.z * r;
         Err_3d = 1;
      }
   }


   inline void Vekt_PP_3d (Point_3d P1, Point_3d P2, Vekt_3d & V)  { V = P2 - P1; }
   inline void Vekt_ProdV_3d (Vekt_3d V1, Vekt_3d V2, Vekt_3d & V) { V = V1.amul(V2); }
   inline void Vekt_ProdS_3d (Vekt_3d V1, Vekt_3d V2, double & r)  { r = V1.imul(V2); }
   inline void Vekt_Rectangular(Vec3d V, Vec3d &erg)
   {
      double dummy;
      erg=V;

      if ((fabs(erg.x)<=fabs(erg.y)) && (fabs(erg.x)<=fabs(erg.z)))
      {
         //y und z tauschen
         dummy=erg.y;
         erg.y=erg.z;
         erg.z=-dummy;
         erg.x=0.0;
      }
      else
      if ((fabs(erg.y)<=fabs(erg.x)) && (fabs(erg.y)<=fabs(erg.z)))
      {
         //x und z tauschen
         dummy=erg.x;
         erg.x=erg.z;
         erg.z=-dummy;
         erg.y=0.0;
      }
      else
      {
         //x und y tauschen
         dummy=erg.x;
         erg.x=erg.y;
         erg.y=-dummy;
         erg.z=0.0;
      }
      erg.Normalize();
   }

   inline void Line_SS_3d (Plane_3d S1, Plane_3d S2, Line_3d & L)
   {
      L.v.x= S1.b * S2.c - S1.c * S2.b;
      L.v.y= S1.c * S2.a - S1.a * S2.c;
      L.v.z= S1.a * S2.b - S1.b * S2.a;
      Err_3d= 1;
      if ((fabs(L.v.x)>=fabs(L.v.y)) && (fabs(L.v.x)>=fabs(L.v.z)) && (L.v.x != 0.0))
      {
         L.p.x= 0.0;
         L.p.y= (S1.c * S2.d - S2.c * S1.d)/(S1.b * S2.c - S2.b * S1.c);
         L.p.z= (S1.b * S2.d - S2.b * S1.d)/(S2.b * S1.c - S1.b * S2.c);
      } else 
      if ((fabs(L.v.y)>=fabs(L.v.x)) && (fabs(L.v.y)>=fabs(L.v.z)) && (L.v.y != 0.0))
      {
         L.p.x= (S1.c * S2.d - S2.c * S1.d)/(S1.a * S2.c - S2.a * S1.c);
         L.p.y= 0.0;
         L.p.z= (S1.a * S2.d - S2.a * S1.d)/(S2.a * S1.c - S1.a * S2.c);
      } else 
      if ((fabs(L.v.z)>=fabs(L.v.x)) && (fabs(L.v.z)>=fabs(L.v.y)) && (L.v.z != 0.0))
      {
         L.p.x= (S1.b * S2.d - S2.b * S1.d)/(S1.a * S2.b - S2.a * S1.b);
         L.p.y= (S1.a * S2.d - S2.a * S1.d)/(S2.a * S1.b - S1.a * S2.b);
         L.p.z= 0.0;
      } else Err_3d= 0;

      if (Err_3d==1)
      {
         Plane_3d ebene_null;
         Point_3d new_point;

         L.v.Normalize();
         Plane_PN_3d(Vec3d(0.0,0.0,0.0), L.v, ebene_null);
         Point_LS_3d(L, ebene_null, new_point);
         L.p=new_point;
      }
   }
   inline void Line_PP_3d (Point_3d P1, Point_3d P2, Line_3d & L)
   {
      L.v.x= P1.x - P2.x;
      L.v.y= P1.y - P2.y;
      L.v.z= P1.z - P2.z;
      L.v.Normalize();
      L.p.x= P1.x;
      L.p.y= P1.y;
      L.p.z= P1.z;
   }
   inline void Line_VP_3d (Vekt_3d V,   Point_3d P,  Line_3d & L)
   {
      L.v.x= V.x;
      L.v.y= V.y;
      L.v.z= V.z;
      L.p.x= P.x;
      L.p.y= P.y;
      L.p.z= P.z;
   }


   inline void Plane_PVV_3d(const Point_3d &P, const Vekt_3d &V1, const Vekt_3d &V2, Plane_3d & S)
   {
      Err_3d= 1;
      double value;

      S.a= V1.y * V2.z - V1.z * V2.y;
      S.b= V1.z * V2.x - V1.x * V2.z;
      S.c= V1.x * V2.y - V1.y * V2.x;

      value=sqrt(pow(S.a,2.0) + pow(S.b,2.0) + pow(S.c,2.0));
      if (value<=Eps6)
      {
         Err_3d=0;
         return;
      }
      S.a/=value;
      S.b/=value;
      S.c/=value;

      S.d= -P.x * S.a - P.y * S.b - P.z * S.c;
      if ((S.a == 0.0) && (S.b == 0.0) && (S.c == 0.0) && (S.d == 0.0))
         Err_3d= 0;
   }
   inline void Plane_PN_3d (const Point_3d &P, const Vekt_3d &N, Plane_3d & S) const
   {
      Err_3d= 1;

      double Len = N.Value();

      S.a= N.x/Len;
      S.b= N.y/Len;
      S.c= N.z/Len;
      S.d= -P.x * S.a - P.y * S.b - P.z * S.c;
      if ((S.a == 0.0) && (S.b == 0.0) && (S.c == 0.0) && (S.d == 0.0))
         Err_3d= 0;
   }

   inline void Plane_PN_3d (const Vec3d_long &P, const Vec3d_long &N, Plane_3d_long & S) const
   {
      Err_3d= 1;

      long double Len = N.Value();

      S.a= N.x/Len;
      S.b= N.y/Len;
      S.c= N.z/Len;
      S.d= -P.x * S.a - P.y * S.b - P.z * S.c;
      if ((S.a == 0.0) && (S.b == 0.0) && (S.c == 0.0) && (S.d == 0.0))
         Err_3d= 0;
   }

   inline double Length_V_3d (Vekt_3d V) { return V.Value();  }

   inline double Dist_LL_3d (Line_3d L1, Line_3d L2)
   { 
      //neu
      Vekt_3d N;

      Err_3d = 1;
      N = L1.v.amul(L2.v);

      if (N.Value()<Eps6) 
      {
         //parallel
         Plane_3d S;
         Point_3d P1, P2;
         double erg;

         Plane_PN_3d(L1.p, L1.v, S);
         Point_LS_3d(L1, S, P1);
         Point_LS_3d(L2, S, P2);

         erg = (P1-P2).Value();
         if (erg == 0)
            //schneiden sich auf der ganzen Strecke
            Err_3d = 0;

         return erg;
      }
      else
      {
         //nicht parallel
         Plane_3d S1, S2;
         Point_3d P1, P2;

         Plane_PVV_3d(L1.p, L1.v, N, S1);
         Plane_PVV_3d(L2.p, L2.v, N, S2);
         Point_LS_3d(L2, S1, P1);
         Point_LS_3d(L1, S2, P2);
         return (P1-P2).Value();
      }

      //alt und funktioniert nicht:
   /*
      Vekt_3d V,R,N;
      double LenN,LenV,q;

      Vekt_PP_3d (L1.p,L2.p,R);
      Vekt_ProdV_3d (L1.v,L2.v,N);
      Vekt_ProdS_3d (R,N,q);
      LenN=Length_V_3d (N);
      Err_3d= 1;
      if (fabs(LenN) < Eps6) // if parallel
      { 
         Vekt_ProdS_3d (R,L1.v,q);
         LenV= Length_V_3d (L1.v);
         if (fabs(LenV) < Eps6)
         {
            Err_3d= 0; //schneiden sich auf der ganzen Strecke
            return 0.0;
         }
         else 
            return q/LenV;
      } 
      else // if not parallel 
         return q/LenN;
   */
   }
   inline double Dist_PS_3d (Point_3d P, Plane_3d S)
   {
      double Len;

      Len= sqrt(S.a * S.a + S.b * S.b + S.c * S.c);
      if (fabs (Len)< Eps6) 
      {
         Err_3d=0;
         return 0.0;
      }
      else
      {
         S.a= S.a/Len;
         S.b= S.b/Len;
         S.c= S.c/Len;
         S.d= S.d/Len;
         Err_3d=1;
         return ( S.a * P.x + S.b * P.y + S.c * P.z + S.d) / Len;
      }
   }
   inline double Dist_SS_3d (Plane_3d S1, Plane_3d S2)
   {
      double Len;

      Err_3d= 1;
      Len= sqrt(S1.a * S1.a + S1.b * S1.b + S1.c * S1.c);
      if (fabs (Len)< Eps6) { Err_3d= 0; return 0; }
      S1.a= S1.a/Len;
      S1.b= S1.b/Len;
      S1.c= S1.c/Len;
      S1.d= S1.d/Len;
      Len= sqrt(S2.a * S2.a + S2.b * S2.b + S2.c * S2.c);
      if (fabs (Len) < Eps6)  { Err_3d= 0; return 0; }
      S2.a= S2.a/Len;
      S2.b= S2.b/Len;
      S2.c= S2.c/Len;
      S2.d= S2.d/Len;
      if (fabs (S1.a - S2.a) > Eps6) { Err_3d= 0; return 0; }
      if (fabs (S1.b - S2.b) > Eps6) { Err_3d= 0; return 0; }
      if (fabs (S1.c - S2.c) > Eps6) { Err_3d= 0; return 0; }
      return (S1.d - S2.d);
   }

   inline double Dist_PL_3d (Point_3d P, Line_3d L)
   {
      Plane_3d plane;
      Point_3d cut_point;

      Plane_PN_3d(P, L.v, plane);
      Point_LS_3d(L, plane, cut_point);

      return (P-cut_point).Value();
   }
   inline void Proj_N_3d (const Point_3d &P0, const Plane_3d &S, Point_3d &P) const
   {
///*
      double q,r;
      q= S.a * S.a + S.b * S.b + S.c * S.c;
      r= (S.a * P0.x + S.b * P0.y + S.c * P0.z + S.d)/q;
      P.x= P0.x - S.a * r;
      P.y= P0.y - S.b * r;
      P.z= P0.z - S.c * r;

//*/
/*
      CGeom3d::Line_3d line;

      Line_VP_3d(S.GetNormale(), P0, line);
      Point_LS_3d(line, S, P);
*/
   }

   inline void Proj_N_3d (const Vec3d_long &P0, const Plane_3d_long &S, Vec3d_long &P) const
   {
      long double q,r;
      q= S.a * S.a + S.b * S.b + S.c * S.c;
      r= (S.a * P0.x + S.b * P0.y + S.c * P0.z + S.d)/q;
      P.x= P0.x - S.a * r;
      P.y= P0.y - S.b * r;
      P.z= P0.z - S.c * r;
   }

   inline void Proj_V_3d (Point_3d P0, Vekt_3d V, Plane_3d S, Point_3d &P)
   {
///*
      double q,r;
      q= S.a * V.x + S.b * V.y + S.c * V.z;
      if (fabs(q) < Eps6)  
         Err_3d= 0;
      else
      {
         r= (S.a * P0.x + S.b * P0.y + S.c * P0.z + S.d) / q;
         P.x= P0.x - V.x * r;
         P.y= P0.y - V.y * r;
         P.z= P0.z - V.z * r;
         Err_3d= 1;
      }
//*/
/*
      CGeom3d::Line_3d line;

      Line_VP_3d(V, P0, line);
      Point_LS_3d(line, S, P);
*/
   }

   inline void Rot_X_3d ( Point_3d & P, double sinwx, double coswx)
   {
      Point_3d P0=P;

      P.x = P0.x;
      P.y = P0.y * coswx - P0.z * sinwx;
      P.z = P0.y * sinwx + P0.z * coswx;
   }
   inline void Rot_Y_3d ( Point_3d & P, double sinwy, double coswy)
   {
      Point_3d P0=P;

      P.x = P0.x * coswy + P0.z * sinwy;
      P.y = P0.y;
      P.z =-P0.x * sinwy + P0.z * coswy;
   }
   inline void Rot_Z_3d ( Point_3d & P, double sinwz, double coswz)
   {
      Point_3d P0=P;

      P.x = P0.x * coswz - P0.y * sinwz;
      P.y = P0.x * sinwz + P0.y * coswz;
      P.z = P0.z;
   }
   inline void Rot_L_3d ( Point_3d & P, Line_3d L, double sinwl, double coswl)
   {
      double yz,xyz;

      P = P - L.p;
      yz = sqrt ( pow(L.v.y,2) + pow(L.v.z,2) );
      if (yz>0)
         Rot_X_3d (P,L.v.y/yz,L.v.z/yz);
      xyz = sqrt ( pow(L.v.x,2) + pow(L.v.y,2) + pow(L.v.z,2) );
      if (xyz>0)
         Rot_Y_3d (P,-L.v.x/xyz,yz/xyz);
      Rot_Z_3d (P,sinwl,coswl);
      if (xyz>0)
         Rot_Y_3d (P,L.v.x/xyz,yz/xyz);
      if (yz>0)
         Rot_X_3d (P,-L.v.y/yz,L.v.z/yz);
      P = P + L.p;
   }
   inline void Rot_Achse_3d ( Point_3d & P, Vekt_3d A, double sinwl, double coswl)
   {
      double yz,xyz;

      yz = sqrt ( pow(A.y,2) + pow(A.z,2) );
      if (yz>0)
         Rot_X_3d (P,A.y/yz,A.z/yz);
      xyz = sqrt ( pow(A.x,2) + pow(A.y,2) + pow(A.z,2) );
      if (xyz>0)
         Rot_Y_3d (P,-A.x/xyz,yz/xyz);
      Rot_Z_3d (P,sinwl,coswl);
      if (xyz>0)
         Rot_Y_3d (P,A.x/xyz,yz/xyz);
      if (yz>0)
         Rot_X_3d (P,-A.y/yz,A.z/yz);
   }

   inline bool InLine_3d (Point_3d P1, Point_3d P2, Point_3d P, double Punktmindestabstand=Eps3)
   {
      Vec3d r1=P2-P1;
      Vec3d r2=P -P1;

      if (r2.Value()>Punktmindestabstand)
      {
         double d = cos(r1,r2)*r2.Value();
         double h = sin(r1,r2)*r2.Value();

         return (d>=-Punktmindestabstand) && (d<=r1.Value()+Punktmindestabstand) && (h <= Punktmindestabstand);
      }
      else
         return true;
   }
   inline bool InLineWithoutEndPoints_3d (Point_3d P1, Point_3d P2, Point_3d P, double Punktmindestabstand=Eps3)
   {
      Vec3d r1=P2-P1;
      Vec3d r2=P -P1;

      if (r2.Value()>Punktmindestabstand)
      {
         double d = cos(r1,r2)*r2.Value();
         double h = sin(r1,r2)*r2.Value();

         return (d>=Punktmindestabstand) && (d<=r1.Value()-Punktmindestabstand) && (h <= Punktmindestabstand);
      }
      else
         return false;
   }
   
   inline bool UpperPP_XY(Point_3d P, Point_3d P1, Point_3d P2)
   {
      double W;
      W = (P.x - P1.x) * (P2.y - P1.y) - (P2.x - P1.x) * (P.y - P1.y);
      if (W < 0) 
         return false; 
      else 
         return true;
   }
   //bool In_Poly_Plane_3d(Geometry::Polygon * pPoly, Geometry::Point * pPoint, double rand=Eps3);
   //bool In_Poly_Plane_3d(Geometry::Polygon * pPoly, Vec3d point             , double rand=Eps3);
   //bool In_Loop_Plane_3d(Geometry::Polygon::PointVector * pPVV, Geometry::Point * pPoint, double rand=Eps3);
   //bool In_Loop_Plane_3d(Geometry::Polygon::PointVector * pPVV, Point_3d point, double rand=Eps3);
   //bool In_Loop_Plane_3d(Geometry::Polygon::PointVector & PVV, Geometry::Point * pPoint, double rand=Eps3);
   //bool In_Loop_Plane_3d(Geometry::Polygon::PointVector & PVV, Point_3d check_point, double rand=Eps3);
   //bool In_Loop_Plane_3d(Point_3d_Vector & PV, Point_3d * pPoint, double rand= Eps3);
/*
   inline bool In_Poly_3d(Geometry::Polygon * pPoly, Geometry::Point * pPoint, double rand= Eps3)
   {
      Vec3d normale, point;
      Plane_3d plane;
      double dist;

      //Check: Ist der Punkt in der Poly-Ebene ?
      const Geometry::Polygon::PointVectorVector points = pPoly->GetPoints();
      if (points.empty())
         return false;
      normale=pPoly->GetNormale();
      normale.Normalize();
      point.x=(**(*points.begin()).begin()).getX();
      point.y=(**(*points.begin()).begin()).getY();
      point.z=(**(*points.begin()).begin()).getZ();
      Plane_PN_3d(point,normale,plane);
      point.x=pPoint->getX();
      point.y=pPoint->getY();
      point.z=pPoint->getZ();
      dist=fabs(Dist_PS_3d(point,plane));
      if (dist>rand)
         return false;

      //Polygon in der richtigen Ebene checken
      return In_Poly_Plane_3d(pPoly,pPoint,rand);
   }

   inline bool In_Poly_3d(Geometry::Polygon * pPoly, Vec3d vPoint            , double rand= Eps3)
   {
      Vec3d normale, point;
      Plane_3d plane;
      double dist;

      //Check: Ist der Punkt in der Poly-Ebene ?
      const Geometry::Polygon::PointVectorVector points = pPoly->GetPoints();
      if (points.empty())
         return false;
      normale=pPoly->GetNormale();
      normale.Normalize();
      point.x=(**(*points.begin()).begin()).getX();
      point.y=(**(*points.begin()).begin()).getY();
      point.z=(**(*points.begin()).begin()).getZ();
      Plane_PN_3d(point,normale,plane);
      dist=fabs(Dist_PS_3d(vPoint,plane));
      if (dist>rand)
         return false;

      //Polygon in der richtigen Ebene checken
      return In_Poly_Plane_3d(pPoly,vPoint,rand);
   }
   inline bool In_Loop_3d(const Geometry::Polygon::PointVector * pPVV, Geometry::Point * pPoint, double rand= Eps3)
   {
      Vec3d normale, point;
      Plane_3d plane;
      double dist;

      //Check: Ist der Punkt in der Poly-Ebene ?
      if (pPVV->empty())
         return false;
      normale=GetNormale(*pPVV);
      normale.Normalize();
      point.x=(**(*pPVV).begin()).getX();
      point.y=(**(*pPVV).begin()).getY();
      point.z=(**(*pPVV).begin()).getZ();
      Plane_PN_3d(point,normale,plane);
      point.x=pPoint->getX();
      point.y=pPoint->getY();
      point.z=pPoint->getZ();
      dist=fabs(Dist_PS_3d(point,plane));
      if (dist>rand)
         return false;

      //Polygon in der richtigen Ebene checken
      return In_Loop_Plane_3d(pPVV,pPoint,rand);
   }
   inline bool In_Loop_3d(const Geometry::Polygon::PointVector * pPVV, Point_3d point, double rand= Eps3)
   {
      Vec3d normale,point_plane;
      Plane_3d plane;
      double dist;

      //Check: Ist der Punkt in der Poly-Ebene ?
      if (pPVV->empty())
         return false;
      normale=GetNormale(*pPVV);
      normale.Normalize();
      point_plane.x=(**(*pPVV).begin()).getX();
      point_plane.y=(**(*pPVV).begin()).getY();
      point_plane.z=(**(*pPVV).begin()).getZ();
      Plane_PN_3d(point_plane,normale,plane);
      dist=fabs(Dist_PS_3d(point,plane));
      if (dist>rand)
         return false;

      //Polygon in der richtigen Ebene checken
      return In_Loop_Plane_3d(pPVV,point,rand);
   }
   inline bool In_Loop_3d(const Geometry::Polygon::PointVector & PVV, Geometry::Point *pPoint, double rand= Eps3)
   {
      Vec3d normale, point;
      Plane_3d plane;
      double dist;

      //Check: Ist der Punkt in der Poly-Ebene ?
      if (PVV.empty())
         return false;
      normale=GetNormale(PVV);
      normale.Normalize();
      point.x=(**PVV.begin()).getX();
      point.y=(**PVV.begin()).getY();
      point.z=(**PVV.begin()).getZ();
      Plane_PN_3d(point,normale,plane);
      point.x=pPoint->getX();
      point.y=pPoint->getY();
      point.z=pPoint->getZ();
      dist=fabs(Dist_PS_3d(point,plane));
      if (dist>rand)
         return false;

      //Polygon in der richtigen Ebene checken
      return In_Loop_Plane_3d(PVV,pPoint,rand);
   }
   inline bool In_Loop_3d(const Geometry::Polygon::PointVector & PVV, Point_3d check_point, double rand= Eps3)
   {
      Vec3d normale, point;
      Plane_3d plane;
      double dist;

      //Check: Ist der Punkt in der Poly-Ebene ?
      if (PVV.empty())
         return false;
      normale=GetNormale(PVV);
      normale.Normalize();
      point.x=(**PVV.begin()).getX();
      point.y=(**PVV.begin()).getY();
      point.z=(**PVV.begin()).getZ();
      Plane_PN_3d(point,normale,plane);
      dist=fabs(Dist_PS_3d(check_point,plane));
      if (dist>rand)
         return false;

      //Polygon in der richtigen Ebene checken
      return In_Loop_Plane_3d(PVV,check_point,rand);
   }
*/
   inline bool In_Loop_3d(const Point_3d_Vector & PV, Point_3d * pPoint, double rand= Eps3)
   {
      Vec3d normale;
      Plane_3d plane;
      double dist;

      //Check: Ist der Punkt in der Poly-Ebene ?
      if (PV.empty())
         return false;
      normale=GetNormale(PV);
      normale.Normalize();
      Plane_PN_3d(*PV.begin(),normale,plane);
      dist=fabs(Dist_PS_3d(*pPoint,plane));
      if (dist>rand)
         return false;

      //Polygon in der richtigen Ebene checken
      return In_Loop_Plane_3d(PV,pPoint,rand);
   }
/*
   inline bool PolyInLoop_3d(const Geometry::Polygon * pTestPoly, Point_3d_Vector & PV, double rand= Eps3)
   {
      const Geometry::Polygon::PointVectorVector points = pTestPoly->GetPoints();
      Geometry::Polygon::PointVectorVector::const_iterator l_pvv = points.begin();
      Geometry::Polygon::PointVectorVector::const_iterator e_pvv = points.end();
      Geometry::Point *pPoint;
      Point_3d point;

      for(; l_pvv!=e_pvv; l_pvv++)
      {
         Geometry::Polygon::PointVector::const_iterator l_pv=(*l_pvv).begin();
         Geometry::Polygon::PointVector::const_iterator e_pv=(*l_pvv).end();
         for(; l_pv!=e_pv; l_pv++)
         {
            pPoint = *l_pv;
            point = GetVec3d(pPoint);
            if (!In_Loop_3d(PV, &point, rand))
               return false;
         }
      }
      return true;
   }
*/
   //bool PolyInPoly_3d(Geometry::Polygon * pTestPoly, Geometry::Polygon * pPoly, double rand = Eps3, bool check_schwerpunkt=false);
   //bool LoopInLoop_3d(Geometry::Polygon::PointVector &TestLoop, Geometry::Polygon::PointVector &Loop, double rand = Eps3);
   inline bool InTriangle(Point_3d & p1, Point_3d & p2, Point_3d & p3, Point_3d * pCheck)
   {
/*
      Point_3d_Vector PV;

      PV.push_back(p1);
      PV.push_back(p2);
      PV.push_back(p3);

      return In_Loop_3d(PV, pCheck);
*/
      Triangle_3d triangle;

      triangle.m_P1 = p1;
      triangle.m_P2 = p2;
      triangle.m_P3 = p3;

      return In_Triangle_3d(triangle, *pCheck);
   }
/*
   inline Vec3d GetNormale(const Geometry::Polygon * pPoly)
   {
      const Geometry::Polygon::PointVectorVector points = pPoly->GetPoints();
	   Geometry::Polygon::PointVectorVector::const_iterator pvl = points.begin();
	   Geometry::Polygon::PointVector::const_iterator p1=pvl->begin();
	   Geometry::Polygon::PointVector::const_iterator p2=p1+1;
	   Geometry::Polygon::PointVector::const_iterator p3=p2+1;
	   Vec3d v1,v2,v;
	   v1.x = (**p1).getX() - (**p2).getX();
	   v1.y = (**p1).getY() - (**p2).getY();
	   v1.z = (**p1).getZ() - (**p2).getZ();
	   v2.x = (**p3).getX() - (**p2).getX();
	   v2.y = (**p3).getY() - (**p2).getY();
	   v2.z = (**p3).getZ() - (**p2).getZ();
	   v.x = v1.y * v2.z - v1.z * v2.y;
	   v.y = v1.z * v2.x - v1.x * v2.z;
	   v.z = v1.x * v2.y - v1.y * v2.x;
	   //Einheitsvektor bilden
	   double dv = sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
	   if (dv!=0)
	   {
		   v.x /= dv;
		   v.y /= dv;
		   v.z /= dv;
	   }
      else
         v=Vec3d(0.0,0.0,0.0);

      if (v.Value()<0.999)
      {
         v=pPoly->GetNormale();
         v.Normalize();
      }

      if (v.Value()<0.999)
      {
         assert(0);
         v=pPoly->GetNormale();
         v.Normalize();
      }

	   return v;
   }

   inline Vec3d GetNormale(const Geometry::Polygon::PointVector poly)
   {
	   Geometry::Polygon::PointVector::const_iterator p1,p2,p3;
	   p1=poly.begin();
	   p2=p1+1;
	   p3=p2+1;
	   Vec3d v1,v2,v;
	   v1.x = (**p1).getX() - (**p2).getX();
	   v1.y = (**p1).getY() - (**p2).getY();
	   v1.z = (**p1).getZ() - (**p2).getZ();
	   v2.x = (**p3).getX() - (**p2).getX();
	   v2.y = (**p3).getY() - (**p2).getY();
	   v2.z = (**p3).getZ() - (**p2).getZ();
	   v.x = v1.y * v2.z - v1.z * v2.y;
	   v.y = v1.z * v2.x - v1.x * v2.z;
	   v.z = v1.x * v2.y - v1.y * v2.x;
	   //Einheitsvektor bilden
	   double dv = sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
	   if (dv!=0)
	   {
		   v.x /= dv;
		   v.y /= dv;
		   v.z /= dv;
	   }
      else
         v=Vec3d(0.0,0.0,0.0);

      if (v.Value()<0.999)
      {
         Vec3d c;
         ComputePlane(poly,c,v);
         v.Normalize();
      }

      if (v.Value()<0.999)
      {
         //assert(0);
         Err_3d = 1; //Normale kann nicht bestimmt werden
      }
      else
         Err_3d = 0;

	   return v;
   }
*/
   inline Vec3d GetNormale(const Point_3d_Vector_Vector & points)
   {
      Vec3d v,c;
      ComputePlane(points,c,v);
      v.Normalize();

      return v;
   }

   inline Vec3d GetNormale(const Point_3d_Vector & PV)
   {
	   Point_3d p1,p2,p3;
	   p1=PV[0];
	   p2=PV[1];
	   p3=PV[2];
	   Vec3d v1,v2,v;
      v1 = p1 - p2;
      v2 = p3 - p2;
      v = v1.amul(v2);
      v.Normalize();

      if (v.Value()<0.999)
      {
         Vec3d c;
         ComputePlane(PV,c,v);
         v.Normalize();
      }

      if (v.Value()<0.999)
         assert(0);

      return v;
   }
   //void AreaLoopXY   (Geometry::Polygon::PointVector poly, double *area);
   //void AreaLoopAngle(Geometry::Polygon::PointVector poly, double *area,double *angleH,double *angleV);
   //void AreaLoop     (Geometry::Polygon::PointVector poly, double *area);
   //void AreaPoly     (Geometry::Polygon * pPoly, double *area);

   inline double AreaKugelDreieck(const double alpha, const double beta, const double gamma, const double radius) const
   {
      return (alpha + beta + gamma - DSL_PI) * pow(radius,2);
   }

   inline double AreaKugelDreieck(const Point_3d &P1, const Point_3d &P2, const Point_3d &P3, const Point_3d &Mittelpunkt) const
   {
      double w1, w2, w3, radius;
      Vekt_3d v1, v2;
      Plane_3d plane;

      radius=((P1-Mittelpunkt).Value()+
              (P2-Mittelpunkt).Value()+
              (P3-Mittelpunkt).Value()) / 3.0;

      Plane_PN_3d(P1,P1-Mittelpunkt,plane);
      Proj_N_3d(P2,plane,v1);
      Proj_N_3d(P3,plane,v2);
      v1-=P1;
      v2-=P1;
      w1=acos(cos(v1,v2));

      Plane_PN_3d(P2,P2-Mittelpunkt,plane);
      Proj_N_3d(P3,plane,v1);
      Proj_N_3d(P1,plane,v2);
      v1-=P2;
      v2-=P2;
      w2=acos(cos(v1,v2));

      Plane_PN_3d(P3,P3-Mittelpunkt,plane);
      Proj_N_3d(P1,plane,v1);
      Proj_N_3d(P2,plane,v2);
      v1-=P3;
      v2-=P3;
      w3=acos(cos(v1,v2));

      return AreaKugelDreieck(w1, w2, w3, radius);
   }

   inline float AreaKugelDreieck_Normalized(const Point_3d &P1, const Point_3d &P2, const Point_3d &P3) const
   {
      double w1, w2, w3;//, radius;
      Vekt_3d v1, v2;
      Plane_3d plane;
      const double double_PI = (const double)(DSL_PI);

      //Mittelpunkt Vec3d(0,0,0)

      //radius=((P1-Mittelpunkt).Value()+
      //        (P2-Mittelpunkt).Value()+
      //        (P3-Mittelpunkt).Value()) / 3.0;
      //radius = 1.0f;

      Plane_PN_3d(P1,P1,plane);
      Proj_N_3d(P2,plane,v1);
      Proj_N_3d(P3,plane,v2);
      v1-=P1;
      v2-=P1;
      w1=acos(cos(v1,v2));

      Plane_PN_3d(P2,P2,plane);
      Proj_N_3d(P3,plane,v1);
      Proj_N_3d(P1,plane,v2);
      v1-=P2;
      v2-=P2;
      w2=acos(cos(v1,v2));

      Plane_PN_3d(P3,P3,plane);
      Proj_N_3d(P1,plane,v1);
      Proj_N_3d(P2,plane,v2);
      v1-=P3;
      v2-=P3;
      w3=acos(cos(v1,v2));

      return (float)(w1 + w2 + w3 - double_PI);// * pow(radius,2);
   }

   inline double AreaKugelViereck(const Point_3d &P1, const Point_3d &P2, const Point_3d &P3, const Point_3d &P4, const Point_3d &Mittelpunkt) const //konvex, d.h. nur Quadrat, Rechteck etc.
   {
      return AreaKugelDreieck(P1,P2,P3,Mittelpunkt)+
             AreaKugelDreieck(P1,P3,P4,Mittelpunkt);
   }

   inline float AreaKugelViereck_Normalized(const Point_3d &P1, const Point_3d &P2, const Point_3d &P3, const Point_3d &P4) const //konvex, d.h. nur Quadrat, Rechteck etc.
   {
      return AreaKugelDreieck_Normalized(P1,P2,P3)+
             AreaKugelDreieck_Normalized(P1,P3,P4);
   }

//... ab hier direker cpp-Code aus Geom3d.cpp
/*
   bool In_Loop_Plane_3d(const Geometry::Polygon::PointVector * pPVV, Point_3d point, double rand)
   {
      return In_Loop_Plane_3d(*pPVV, point, rand);
   }

   bool In_Loop_Plane_3d(const Geometry::Polygon::PointVector * pPVV, Geometry::Point * pPoint, double rand= Eps3)
   {
      return In_Loop_Plane_3d(*pPVV, GetVec3d(pPoint), rand);
   }

   bool In_Loop_Plane_3d(const Geometry::Polygon::PointVector &PVV, Geometry::Point * pPoint, double rand= Eps3)
   {
      return In_Loop_Plane_3d(PVV, GetVec3d(pPoint), rand);
   }

   bool In_Loop_Plane_3d(const Geometry::Polygon::PointVector &PVV, Point_3d check_point, double rand)
   {
      Point_3d_Vector_Vector points;
      Point_3d_Vector loop;
      Geometry::Polygon::PointVector::const_iterator l_pv, e_pv;
      Geometry::Point *pPoint;

      l_pv = PVV.begin();
      e_pv = PVV.end();
      for(; l_pv!=e_pv; l_pv++)
      {
         pPoint = *l_pv;
         loop.push_back( Vec3d(pPoint->getX(), pPoint->getY(), pPoint->getZ()) );
      }
      points.push_back( loop );

      return In_Poly_Plane_3d(points, check_point, rand);

   }
*/
   bool In_Loop_Plane_3d(const Point_3d_Vector & PV, Point_3d * pPoint, double rand= Eps3)
   {
      Point_3d_Vector_Vector points;

      points.push_back( PV );
      return In_Poly_Plane_3d( points, *pPoint, rand);
   }
/*
   bool In_Poly_Plane_3d(Geometry::Polygon * pPoly, Geometry::Point * pPoint, double rand= Eps3)
   {
      return In_Poly_Plane_3d( pPoly, GetVec3d(pPoint), rand );
   }


   bool In_Poly_Plane_3d(Geometry::Polygon * pPoly, Vec3d point, double rand= Eps3)
   {
      Geometry::OpenGLKernel *pKernel = dynamic_cast<Geometry::OpenGLKernel *>(pPoly->GetVObjectManagement());
      Vec3d  center, normale;
      Plane_3d plane;

      if (Points_Have_Changed(pPoly))
         pKernel->TesselatePolygon(pPoly);
      ComputePlane(pPoly->GetPoints(), center, normale);
      Plane_PN_3d(center, normale, plane);

      return In_Poly_Plane_3d(plane, pPoly->GetTriangles(), point, rand);
   }
*/
   bool In_Triangle_3d(const Triangle_3d & triangle, const Vec3d & point, double rand= Eps3)
   {
      double total_area, p1, p2, p3;

      if (InLine_3d(triangle.m_P1, triangle.m_P2, point, rand) ||
          InLine_3d(triangle.m_P2, triangle.m_P3, point, rand) ||
          InLine_3d(triangle.m_P3, triangle.m_P1, point, rand)) return true;

      AreaTriangle(triangle.m_P1, triangle.m_P2, triangle.m_P3, &total_area);
      AreaTriangle(triangle.m_P1, triangle.m_P2, point, &p1);
      AreaTriangle(triangle.m_P2, triangle.m_P3, point, &p2);
      AreaTriangle(triangle.m_P3, triangle.m_P1, point, &p3);

      return (fabs((p1+p2+p3)/total_area - 1.0) <= Eps3);
   }

   bool In_Triangle_WithoutBorder_3d(const Triangle_3d & triangle, const Vec3d & point, double rand= Eps3)
   {
      double total_area, p1, p2, p3;

      if (InLine_3d(triangle.m_P1, triangle.m_P2, point, rand) ||
          InLine_3d(triangle.m_P2, triangle.m_P3, point, rand) ||
          InLine_3d(triangle.m_P3, triangle.m_P1, point, rand)) return false;

      AreaTriangle(triangle.m_P1, triangle.m_P2, triangle.m_P3, &total_area);
      AreaTriangle(triangle.m_P1, triangle.m_P2, point, &p1);
      AreaTriangle(triangle.m_P2, triangle.m_P3, point, &p2);
      AreaTriangle(triangle.m_P3, triangle.m_P1, point, &p3);

      return (fabs((p1+p2+p3)/total_area - 1.0) <= Eps3);
   }

   Triangle_3d GetTriangle(const Vec3d &p1, const Vec3d &p2, const Vec3d &p3)
   {
      Triangle_3d triangle;

      triangle.m_P1 = p1;
      triangle.m_P2 = p2;
      triangle.m_P3 = p3;

      return triangle;
   }

   bool In_Poly_Plane_3d(const Point_3d_Vector_Vector & points, const Vec3d & point, double rand= Eps3)
   {
      TriangleVector triangles;
      Vec3d center, normale;
      Plane_3d plane;

      ComputePlane(points, center, normale);
      Plane_PN_3d(center, normale, plane);

      TesselatePVV(points, triangles);

      return In_Poly_Plane_3d(plane, triangles, point, rand);
   }

   bool In_Poly_Plane_3d(Plane_3d &plane, TriangleVector & triangles, const Vec3d & point, double rand= Eps3)
   {
      TriangleVector::iterator l,e;
      Line_3d line;
      Vec3d cut_point;

      Line_VP_3d(plane.GetNormale(), point, line);
      Point_LS_3d(line, plane, cut_point);

      l = triangles.begin();
      e = triangles.end();
      for(; l!=e; l++)
         if (In_Triangle_3d(*l, cut_point, rand))
            return true;

      return false;
   }
   
/*
   bool In_Poly_Plane_3d(Geometry::Polygon * pPoly, Vec3d point, double rand)
   {
      Vec3d schnittvektor, normale=pPoly->GetNormale();
      Line_3d schnittlinie,linie;
      Point_3d schnittpunkt,lp1,lp2;
      Geometry::Polygon::PointVectorVector::iterator l=pPoly->points.begin(), e=pPoly->points.end();
      Geometry::Polygon::PointVector::iterator l2,l3,e2,l2_next,l2_last;
      PointVector schnittpunkte;
      EckenSchnittpunktVector ecken_schnittpunkte;
      EckenSchnittpunktVector::iterator l_ecke, l2_ecke, e_ecke;
      ecken_schnittpunkt sort_dummy;
      PointVector::iterator sl,sl2,se;
      Plane_3d plane, sort_plane;
      double dist, dist1, dist2;
      long upper_points=0, lower_points=0, ecken_anz=0;
      bool found, up_inside, down_inside;

      Vekt_Rectangular(normale, schnittvektor);
      Line_VP_3d(schnittvektor,point,schnittlinie);
      Plane_PN_3d(point,schnittvektor,plane);
      Plane_PN_3d(point,schnittvektor.amul(normale),sort_plane);

      for(; l!=e; l++)
      {
         l2=(*l).begin();
         e2=(*l).end();

         for (;l2!=e2; l2++)
         {
            l3=l2+1;
            if (l3==e2)
               l3=(*l).begin();

            lp1.x=(**l2).getX();
            lp1.y=(**l2).getY();
            lp1.z=(**l2).getZ();
            lp2.x=(**l3).getX();
            lp2.y=(**l3).getY();
            lp2.z=(**l3).getZ();
            if ((lp1-lp2).Value()<rand) continue;
            Line_PP_3d(lp1,lp2,linie);

            //sind wir auf dem Rand ?
            if (InLine_3d(lp1,lp2,point,rand))
               return true;

            Point_LL_3d(schnittlinie,linie,schnittpunkt,rand);
            if ((Err_3d==1) && (InLine_3d(lp1,lp2,schnittpunkt,rand)))
            {
               schnittpunkte.push_back(schnittpunkt);
               dist=Dist_PS_3d(schnittpunkt,plane);
               if (dist>0.0)
                  upper_points++;
               else
                  lower_points++;
            }
         }
      }

      //doppelte Schnittpunkte l�schen
      sl=schnittpunkte.begin();
      for (;sl!=schnittpunkte.end(); sl++)
      {
         sl2=sl;
         sl2++;

         for (;sl2!=schnittpunkte.end();)
            if (((*sl)-(*sl2)).Value()<rand)
            {
               dist=Dist_PS_3d(*sl2,plane);
               if (dist>0.0)
                  upper_points--;
               else
                  lower_points--;
               sl2=schnittpunkte.erase(sl2);
            }
            else
               sl2++;
      }

      //Punkte und Schnitte auf Gleichheit checken
      sl=schnittpunkte.begin();
      for (;sl!=schnittpunkte.end(); sl++)
      {
         schnittpunkt=(*sl);
         found=false;

         l=pPoly->points.begin();
         e=pPoly->points.end();
         for(; l!=e; l++)
         {
            l2=(*l).begin();
            e2=(*l).end();

            for (;l2!=e2; l2++)
            {
               lp1.x=(**l2).getX();
               lp1.y=(**l2).getY();
               lp1.z=(**l2).getZ();

               if ((schnittpunkt-lp1).Value()<rand)
               {
                  l2_next=l2;
                  l2_last=l2;
                  l2_next++;
                  if (l2_next==e2)
                     l2_next=(*l).begin();
                  if (l2==(*l).begin())
                     l2_last=(*l).end()-1;
                  else
                     l2_last--;
                  lp1.x=(**l2_next).getX();
                  lp1.y=(**l2_next).getY();
                  lp1.z=(**l2_next).getZ();
                  lp2.x=(**l2_last).getX();
                  lp2.y=(**l2_last).getY();
                  lp2.z=(**l2_last).getZ();

                  dist1=Dist_PS_3d(lp1,sort_plane);
                  dist2=Dist_PS_3d(lp2,sort_plane);
                  if (fabs(dist1)<rand) dist1=0.0; else dist1/=fabs(dist1);
                  if (fabs(dist2)<rand) dist2=0.0; else dist2/=fabs(dist2);

                  if ((dist1==0.0) && (dist2==0.0))
                     //gerade Ecke auf Schnittlinie
                     ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, true));
                  else
                     if (fabs(dist1*dist2)<rand)
                     {
                        //Winkel nach oben oder unten
                        if (dist1+dist2>0.0)
                           ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,1, true));
                        else
                           ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,-1, true));
                     }
                     else
                        if (dist1*dist2<-rand)
                           //Spitze
                           ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, true));
                        else
                        {
                           //gerade Ecke senkrecht auf Schnittlinie (wird aufgenohmen wie normaler Schnittpunkt)
                           ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, false));
                           ecken_anz--;
                        }
                  found=true;
                  ecken_anz++;
                  break;
               }
            }
            if (found)
               break;
         }
         if (!found)
            ecken_schnittpunkte.push_back(ecken_schnittpunkt(schnittpunkt,0, false));
      }
      if (ecken_anz>1)
      {
         //gesuchten Punkt einf�gen
         ecken_schnittpunkte.push_back(ecken_schnittpunkt(point,1, false));
         //alle spitzen und geraden Schnittpunkt Ecken l�schen, sort-Distanzen ermitteln
         l_ecke=ecken_schnittpunkte.begin();
         for (; l_ecke!=ecken_schnittpunkte.end(); )
            if (((*l_ecke).direction==0) && ((*l_ecke).is_ecke))
            {
               //l�schen
               schnittpunkte.erase(find(schnittpunkte.begin(), schnittpunkte.end(), (*l_ecke).schnittpunkt));
               l_ecke=ecken_schnittpunkte.erase(l_ecke);
            }
            else
            {
               //bleibt! --> Distanz zu Sort-Referenzebene ermitteln
               (*l_ecke).dist=Dist_PS_3d((*l_ecke).schnittpunkt, plane);
               l_ecke++;
            }

         //alle Schnittpunkte sortieren
         l_ecke=ecken_schnittpunkte.begin();
         e_ecke=ecken_schnittpunkte.end();
         for (; l_ecke!=e_ecke; l_ecke++)
         {
            l2_ecke=l_ecke;
            l2_ecke++;
            for (; l2_ecke!=e_ecke; l2_ecke++)
               if ((*l_ecke).dist>(*l2_ecke).dist)
               {
                  //vertauschen
                  sort_dummy=(*l_ecke);
                  (*l_ecke)=(*l2_ecke);
                  (*l2_ecke)=sort_dummy;
               }
         }

         //in Fleisch Checks
         up_inside=false;
         down_inside=false;
         l_ecke=ecken_schnittpunkte.begin();
         for (; l_ecke!=e_ecke; l_ecke++)
         {
            if ((!(*l_ecke).is_ecke) && ((*l_ecke).direction==1)) //unser gesuchter Punkt
               return (up_inside || down_inside);
            else
            if ((!(*l_ecke).is_ecke) && ((*l_ecke).direction==0)) //kompletter Wechsel
            {
               up_inside=!up_inside;
               down_inside=!down_inside;
            }
            else
            if (((*l_ecke).is_ecke) && ((*l_ecke).direction==1))  //Wechsel oben
               up_inside=!up_inside;
            else
            if (((*l_ecke).is_ecke) && ((*l_ecke).direction==-1)) //Wechsel unten
               down_inside=!down_inside;
            else
            if (((*l_ecke).is_ecke) && ((*l_ecke).direction==0))  //Fehler
               assert(0);
         }

         assert(0);
         return false;
      }
      else
      {
         ecken_schnittpunkte.clear();

         //ist point auf dem Polygon Rand ?
         sl=schnittpunkte.begin();
         se=schnittpunkte.end();
         for (;sl!=se; sl++)
            if ((point-(*sl)).Value()<rand)
               return true;

         //befindet sich point zwischen einer ungeraden Anzahl von Schnittpunkten ?
         return (schnittpunkte.size()>1) && (upper_points%2==1) && (lower_points%2==1);
      }
   }
*/

   /*
   bool In_Loop_3d(Point_3d_Vector & PV, Point_3d * pPoint, double rand)
   {
      Vec3d normale;
      Plane_3d plane;
      double dist;

      //Check: Ist der Punkt in der Poly-Ebene ?
      if (PV.empty())
         return false;
      normale=GetNormale(PV);
      normale.Normalize();
      Plane_PN_3d(*PV.begin(),normale,plane);
      dist=fabs(Dist_PS_3d(*pPoint,plane));
      if (dist>rand)
         return false;

      //Polygon in der richtigen Ebene checken
      return In_Loop_Plane_3d(PV,pPoint,rand);
   }
   */
   /*
   bool In_Loop_3d(Geometry::Polygon::PointVector * pPVV, Point_3d point, double rand)
   {
      Vec3d normale,point_plane;
      Plane_3d plane;
      double dist;

      //Check: Ist der Punkt in der Poly-Ebene ?
      if (pPVV->empty())
         return false;
      normale=GetNormale(*pPVV);
      normale.Normalize();
      point_plane.x=(**(*pPVV).begin()).getX();
      point_plane.y=(**(*pPVV).begin()).getY();
      point_plane.z=(**(*pPVV).begin()).getZ();
      Plane_PN_3d(point_plane,normale,plane);
      dist=fabs(Dist_PS_3d(point,plane));
      if (dist>rand)
         return false;

      //Polygon in der richtigen Ebene checken
      return In_Loop_Plane_3d(pPVV,point,rand);
   }
   */
   /*
   bool In_Loop_3d(Geometry::Polygon::PointVector * pPVV, Geometry::Point * pPoint, double rand)
   {
      Vec3d normale, point;
      Plane_3d plane;
      double dist;

      //Check: Ist der Punkt in der Poly-Ebene ?
      if (pPVV->empty())
         return false;
      normale=GetNormale(*pPVV);
      normale.Normalize();
      point.x=(**(*pPVV).begin()).getX();
      point.y=(**(*pPVV).begin()).getY();
      point.z=(**(*pPVV).begin()).getZ();
      Plane_PN_3d(point,normale,plane);
      point.x=pPoint->getX();
      point.y=pPoint->getY();
      point.z=pPoint->getZ();
      dist=fabs(Dist_PS_3d(point,plane));
      if (dist>rand)
         return false;

      //Polygon in der richtigen Ebene checken
      return In_Loop_Plane_3d(pPVV,pPoint,rand);
   }
   */
   /*
   bool In_Loop_3d(Geometry::Polygon::PointVector & PVV, Geometry::Point *pPoint, double rand)
   {
      Vec3d normale, point;
      Plane_3d plane;
      double dist;

      //Check: Ist der Punkt in der Poly-Ebene ?
      if (PVV.empty())
         return false;
      normale=GetNormale(PVV);
      normale.Normalize();
      point.x=(**PVV.begin()).getX();
      point.y=(**PVV.begin()).getY();
      point.z=(**PVV.begin()).getZ();
      Plane_PN_3d(point,normale,plane);
      point.x=pPoint->getX();
      point.y=pPoint->getY();
      point.z=pPoint->getZ();
      dist=fabs(Dist_PS_3d(point,plane));
      if (dist>rand)
         return false;

      //Polygon in der richtigen Ebene checken
      return In_Loop_Plane_3d(PVV,pPoint,rand);
   }
   */
   /*
   bool In_Loop_3d(Geometry::Polygon::PointVector & PVV, Point_3d check_point, double rand)
   {
      Vec3d normale, point;
      Plane_3d plane;
      double dist;

      //Check: Ist der Punkt in der Poly-Ebene ?
      if (PVV.empty())
         return false;
      normale=GetNormale(PVV);
      normale.Normalize();
      point.x=(**PVV.begin()).getX();
      point.y=(**PVV.begin()).getY();
      point.z=(**PVV.begin()).getZ();
      Plane_PN_3d(point,normale,plane);
      dist=fabs(Dist_PS_3d(check_point,plane));
      if (dist>rand)
         return false;

      //Polygon in der richtigen Ebene checken
      return In_Loop_Plane_3d(PVV,check_point,rand);
   }
   */
   /*
   bool In_Poly_3d(Geometry::Polygon * pPoly, Geometry::Point * pPoint, double rand)
   {
      Vec3d normale, point;
      Plane_3d plane;
      double dist;

      //Check: Ist der Punkt in der Poly-Ebene ?
      if (pPoly->points.empty())
         return false;
      normale=pPoly->GetNormale();
      normale.Normalize();
      point.x=(**(*pPoly->points.begin()).begin()).getX();
      point.y=(**(*pPoly->points.begin()).begin()).getY();
      point.z=(**(*pPoly->points.begin()).begin()).getZ();
      Plane_PN_3d(point,normale,plane);
      point.x=pPoint->getX();
      point.y=pPoint->getY();
      point.z=pPoint->getZ();
      dist=fabs(Dist_PS_3d(point,plane));
      if (dist>rand)
         return false;

      //Polygon in der richtigen Ebene checken
      return In_Poly_Plane_3d(pPoly,pPoint,rand);
   }
   */
   /*
   bool In_Poly_3d(Geometry::Polygon * pPoly, Vec3d vPoint, double rand)
   {
      Vec3d normale, point;
      Plane_3d plane;
      double dist;

      //Check: Ist der Punkt in der Poly-Ebene ?
      if (pPoly->points.empty())
         return false;
      normale=pPoly->GetNormale();
      normale.Normalize();
      point.x=(**(*pPoly->points.begin()).begin()).getX();
      point.y=(**(*pPoly->points.begin()).begin()).getY();
      point.z=(**(*pPoly->points.begin()).begin()).getZ();
      Plane_PN_3d(point,normale,plane);
      dist=fabs(Dist_PS_3d(vPoint,plane));
      if (dist>rand)
         return false;

      //Polygon in der richtigen Ebene checken
      return In_Poly_Plane_3d(pPoly,vPoint,rand);
   }
   */
   /*
   bool InTriangle(Point_3d & p1,
                            Point_3d & p2,
                            Point_3d & p3,
                            Point_3d * pCheck)
   {
      Point_3d_Vector PV;

      PV.push_back(p1);
      PV.push_back(p2);
      PV.push_back(p3);

      return In_Loop_3d(PV, pCheck);
   }
   */
   /*
   PROCEDURE Line_VP_3d (V:Vekt_3d; P:Point_3d;
                         VAR L:Line_3d);
   BEGIN
    L.a:= V.x;
    L.b:= V.y;
    L.c:= V.z;
    L.x:= P.x;
    L.y:= P.y;
    L.z:= P.z;
   END;
   */
   /*
   void Line_VP_3d(Vekt_3d V, Point_3d P, Line_3d & L)
   {
      L.v.x= V.x;
      L.v.y= V.y;
      L.v.z= V.z;
      L.p.x= P.x;
      L.p.y= P.y;
      L.p.z= P.z;
   }
   */
/*
   bool PolyInPoly_3d(const Geometry::Polygon * pTestPoly, Geometry::Polygon * pPoly, double rand= Eps3, bool check_schwerpunkt=false)
   {
      const Geometry::Polygon::PointVectorVector points = pTestPoly->GetPoints();
      Geometry::Polygon::PointVectorVector::const_iterator l=points.begin(), e=points.end();
      Vec3d Linienmittelpunkt(0,0,0), Schwerpunkt(0,0,0),p1,p2;
      long Anzahl=0;
    
      for(; l!=e; l++)
      {
         Geometry::Polygon::PointVector::const_iterator l3, l2=(*l).begin();
         Geometry::Polygon::PointVector::const_iterator e2=(*l).end();

         for (;l2!=e2; l2++)
         {
            if (!In_Poly_3d(pPoly,(*l2),rand))
               return false;
            
            l3=l2;
            l3++;
            if (l3==e2)
               l3=(*l).begin();

            p1=Vec3d((**l2).getX(),(**l2).getY(),(**l2).getZ());
            p2=Vec3d((**l3).getX(),(**l3).getY(),(**l3).getZ());

            Linienmittelpunkt=(p1+p2)/2;
            Schwerpunkt+=p1;
            Anzahl++;

            if (!In_Poly_3d(pPoly,Linienmittelpunkt,rand))
               return false;
         }
      }

      if (Anzahl<=0)
         return false;
      
      //es sollten konvexe Polygone sein, sodass der Schwerpunkt im dazugeh�rigen Poly liegt
      if (check_schwerpunkt)
      {
         Schwerpunkt/=Anzahl;
         if (!In_Poly_3d(pPoly,Schwerpunkt,rand))
            return false;
      }

      //Schwerpunkte der Polydreiecke checken
      Geometry::Polygon::TriangleVector triangles=pTestPoly->GetTriangles();
      Geometry::Polygon::TriangleVector::iterator tl=triangles.begin(), te=triangles.end();
      Vec3d center;
      for (; tl!=te; tl++)
      {
         center.x = ((*tl).p1.x + (*tl).p2.x + (*tl).p3.x) / 3;
         center.y = ((*tl).p1.y + (*tl).p2.y + (*tl).p3.y) / 3;
         center.z = ((*tl).p1.z + (*tl).p2.z + (*tl).p3.z) / 3;
         if (!In_Poly_3d(pPoly,center,rand))
            return false;
      }
      return true;
   }

   bool LoopInLoop_3d(const Geometry::Polygon::PointVector &TestLoop, 
                               const Geometry::Polygon::PointVector &Loop, 
                               double rand= Eps3)
   {
      Geometry::Polygon::PointVector::const_iterator l2,l3,e2;
      Vec3d Linienmittelpunkt(0.0,0.0,0.0), p1,p2;
      long Anzahl=0;
    
      l2=TestLoop.begin();
      e2=TestLoop.end();

      for (;l2!=e2; l2++)
      {
         if (!In_Loop_3d(Loop,(*l2),rand))
            return false;
         
         l3=l2;
         l3++;
         if (l3==e2)
            l3=TestLoop.begin();

         p1=Vec3d((**l2).getX(),(**l2).getY(),(**l2).getZ());
         p2=Vec3d((**l3).getX(),(**l3).getY(),(**l3).getZ());

         Linienmittelpunkt=(p1+p2)/2;
         Anzahl++;

         if (!In_Loop_3d(Loop,Linienmittelpunkt,rand))
            return false;
      }

      if (Anzahl<=0)
         return false;

      return true;
   }

   void AreaLoopXY(Geometry::Polygon::PointVector poly, double *area)
   {
      unsigned int i;
      Geometry::Point *pi,*pj;

      *area=0;
      for (i=0; i<poly.size(); i++)
      {
         pi=poly[i];
         if (i<poly.size()-1)
            pj=poly[i+1];
         else
            pj=poly[0];
         *area+=((*pi).getX() - (*pj).getX()) * ((*pi).getY() + (*pj).getY());
      }
      *area/=2;
   }
*/
   void AreaLoopXY(Point_3d_Vector & poly, double *area)
   {
      unsigned int i;
      Vec3d pi,pj;

      *area=0;
      for (i=0; i<poly.size(); i++)
      {
         pi=poly[i];
         if (i<poly.size()-1)
            pj=poly[i+1];
         else
            pj=poly[0];
         *area+=(pi.x - pj.x) * (pi.y + pj.y);
      }
      *area/=2;
   }

/*
   void AreaLoopAngle(Geometry::Polygon::PointVector poly, double *area, double *angleH, double *angleV)
   {
      unsigned int i;
      Geometry::Point *pj;
      Vec3d vpi,vpj,zw, start_point;

      *area=0;

      //zur�ckdrehen
      vpj.x=(*poly[0]).getX();
      vpj.y=(*poly[0]).getY();
      vpj.z=(*poly[0]).getZ();
      start_point = vpj;
      vpj = vpj - start_point;
      zw=vpj;
      vpj.x=(zw.x*cos(-*angleH)-zw.y*sin(-*angleH));
      vpj.y=(zw.y*cos(-*angleH)+zw.x*sin(-*angleH));
      zw=vpj;
      vpj.y=(zw.y*cos(-*angleV)-zw.z*sin(-*angleV));
      vpj.z=(zw.z*cos(-*angleV)+zw.y*sin(-*angleV));

      for (i=0; i<poly.size(); i++)
      {
         vpi=vpj;

         if (i<poly.size()-1)
            pj=poly[i+1];
         else
            pj=poly[0];

         //zur�ckdrehen
         vpj.x=(*pj).getX();
         vpj.y=(*pj).getY();
         vpj.z=(*pj).getZ();
         vpj = vpj - start_point;
         zw=vpj;
         vpj.x=(zw.x*cos(-*angleH)-zw.y*sin(-*angleH));
         vpj.y=(zw.y*cos(-*angleH)+zw.x*sin(-*angleH));
         zw=vpj;
         vpj.y=(zw.y*cos(-*angleV)-zw.z*sin(-*angleV));
         vpj.z=(zw.z*cos(-*angleV)+zw.y*sin(-*angleV));

         *area+=(vpi.x - vpj.x) * (vpi.y + vpj.y);
      }
      *area/=2;
   }
*/
/*
   void AreaLoopAngle(Geometry::Polygon::PointVector poly, double *area, Vec3d & vx, Vec3d &vy, Vec3d &normale)
   {
      unsigned int i;
      Geometry::Point *pj;
      Vec3d vpi,vpj,start_point;

      *area=0;

      //zur�ckdrehen
      vpj.x=(*poly[0]).getX();
      vpj.y=(*poly[0]).getY();
      vpj.z=(*poly[0]).getZ();
      start_point = vpj;
      vpj = Cramer(vx, vy, normale, vpj - start_point);

      for (i=0; i<poly.size(); i++)
      {
         vpi=vpj;

         if (i<poly.size()-1)
            pj=poly[i+1];
         else
            pj=poly[0];

         //zur�ckdrehen
         vpj.x=(*pj).getX();
         vpj.y=(*pj).getY();
         vpj.z=(*pj).getZ();
         vpj = Cramer(vx, vy, normale, vpj - start_point);

         *area+=(vpi.x - vpj.x) * (vpi.y + vpj.y);
      }
      *area/=2;
   }
*/
   void AreaLoopAngle(CGeom3d::Point_3d_Vector poly, double *area, Vec3d & vx, Vec3d &vy, Vec3d &normale)
   {
      unsigned int i;
      Vec3d vpi,vpj,start_point;

      *area=0;

      //zur�ckdrehen
      vpj.x=poly[0].x;
      vpj.y=poly[0].y;
      vpj.z=poly[0].z;
      start_point = vpj;
      vpj = Cramer(vx, vy, normale, vpj - start_point);

      for (i=0; i<poly.size(); i++)
      {
         vpi=vpj;

         if (i<poly.size()-1)
            vpj=poly[i+1];
         else
            vpj=poly[0];

         //zur�ckdrehen
         vpj = Cramer(vx, vy, normale, vpj - start_point);

         *area+=(vpi.x - vpj.x) * (vpi.y + vpj.y);
      }
      *area/=2;
   }
/*
   void AreaPoly(Geometry::Polygon * pPoly, double *area)
   {
      const Geometry::Polygon::PointVectorVector points = pPoly->GetPoints();
      Geometry::Polygon::PointVectorVector::const_iterator l=points.begin();
      Geometry::Polygon::PointVectorVector::const_iterator e=points.end();
      double looparea;
      Vec3d normale, base_point;
      Vec3d vx, vy;

      ComputePlane(points, base_point, normale);
      Vekt_Rectangular(normale, vx);
      vy = vx.amul( normale );
      vx.Normalize();
      vy.Normalize();

      *area=0;
      for (;l!=e;l++)
      {
         AreaLoopAngle(*l, &looparea, vx, vy, normale);
         if (l==points.begin())
            *area=fabs(looparea);
         else
            *area-=fabs(looparea);
      }
   }
*/
/*
   void AreaPoly(Geometry::Polygon * pPoly, double *area)
   {
      Geometry::Polygon::PointVectorVector::iterator l=pPoly->points.begin();
      Geometry::Polygon::PointVectorVector::iterator e=pPoly->points.end();
      double looparea;
      Vec3d blick=GetNormale(pPoly);
      double angleH,angleV;
      *area=0;

      //Winkelermittlung
      if (fabs(blick.y)>Eps6)
         angleH=atan(-blick.x/blick.y);
      else
      {
         if (blick.x<0) angleH=DSL_PI/2.0;
         else           angleH=3*DSL_PI/2.0;
      }
      if (blick.y<0)    angleH+=DSL_PI;

	   if ((sqrt(blick.x*blick.x+blick.y*blick.y)) > Eps6)
		   angleV=atan(blick.z/sqrt(blick.x*blick.x+blick.y*blick.y));
	   else
	   {
		   if (blick.z>0)	 angleV=DSL_PI/2.0;
		   else  		    angleV=-DSL_PI/2.0;
	   }
      angleV+=DSL_PI/2.0;


      for (;l!=e;l++)
      {
         AreaLoopAngle(*l,&looparea,&angleH,&angleV);
         if (l==pPoly->points.begin())
            *area=fabs(looparea);
         else
            *area-=fabs(looparea);
      }
   }
*/
/*
   void AreaPoly(Geometry::Polygon * pPoly, float *area)
   {
      double tmp_area;
      AreaPoly(pPoly, &tmp_area);
      (*area) = (float)tmp_area;
   }

   void AreaPolySet(Geometry::Body::PolygonSet &m_poly_set, double &area)
   {
      Geometry::Body::PolygonSet::iterator l,e;
      Geometry::Polygon *pPoly;
      double part_area;

      l = m_poly_set.begin();
      e = m_poly_set.end();
      area = 0.0;

      for(; l!=e; l++)
      {
         pPoly = *l;
         AreaPoly( pPoly, &part_area );
         area += fabs(part_area);
      }
   }

   
   void AreaLoop(Geometry::Polygon::PointVector poly, double *area)
   {
      Vec3d normale=GetNormale(poly);
      Vec3d vx, vy;

      Vekt_Rectangular(normale, vx);
      vy = vx.amul( normale );
      vx.Normalize();
      vy.Normalize();

      *area=0;
      AreaLoopAngle(poly, area, vx, vy, normale);
   }
*/
   void AreaLoop(CGeom3d::Point_3d_Vector poly, double *area)
   {
      Vec3d normale=GetNormale(poly);
      Vec3d vx, vy;

      Vekt_Rectangular(normale, vx);
      vy = vx.amul( normale );
      vx.Normalize();
      vy.Normalize();

      *area=0;
      AreaLoopAngle(poly, area, vx, vy, normale);
   }
/*
   void AreaLoop(Geometry::Polygon::PointVector poly, double *area)
   {
      double looparea;
      Vec3d blick=GetNormale(poly);
      double angleH,angleV;
      *area=0;

      if (Err_3d == 1)
         //Normale kann nicht bestimmt werden
         return;

      //Winkelermittlung
      if (fabs(blick.y)>Eps6)
         angleH=atan(-blick.x/blick.y);
      else
      {
         if (blick.x<0) angleH=DSL_PI/2.0;
         else           angleH=3*DSL_PI/2.0;
      }
      if (blick.y<0)    angleH+=DSL_PI;

	   if ((sqrt(blick.x*blick.x+blick.y*blick.y)) > Eps6)
		   angleV=atan(blick.z/sqrt(blick.x*blick.x+blick.y*blick.y));
	   else
	   {
		   if (blick.z>0)	 angleV=DSL_PI/2.0;
		   else  		    angleV=-DSL_PI/2.0;
	   }
      angleV+=DSL_PI/2.0;

      AreaLoopAngle(poly,&looparea,&angleH,&angleV);
      *area=looparea;
   }
*/

   void AreaTriangle(const Vec3d &p1, const Vec3d &p2, const Vec3d &p3, double *area)
   {
      double a = (p1-p2).Value();
      double b = (p2-p3).Value();
      double c = (p3-p1).Value();
      double s = (a+b+c)/2;

      //Heronsche Fl�chenformel
      double heron = s*(s-a)*(s-b)*(s-c);
      if (heron > 0)
         (*area) = sqrt( heron );
      else
         (*area) = 0;
   }
/*   
   void AreaTriangle(Geometry::Point * pP1, Geometry::Point * pP2, Geometry::Point * pP3, double *area)
   {

      AreaTriangle( GetVec3d(pP1), GetVec3d(pP2), GetVec3d(pP3), area );
   }
   
   void GetTriangleSchwerpunkt(Geometry::Point * pP1, Geometry::Point * pP2, Geometry::Point * pP3, Vec3d & schwerpunkt)
   {
      Vec3d p1(pP1->getX(), pP1->getY(), pP1->getZ());
      Vec3d p2(pP2->getX(), pP2->getY(), pP2->getZ());
      Vec3d p3(pP3->getX(), pP3->getY(), pP3->getZ());
      Vec3d m12 = (p1 + p2) / 2;
      Vec3d m23 = (p2 + p3) / 2;
      Line_3d line1, line2;
      Line_PP_3d(p3, m12, line1);
      Line_PP_3d(p1, m23, line2);
      //Schnittpunkt zweier Seitenhalbierenden
      Point_LL_3d(line1, line2, schwerpunkt);
   }

   bool GetPolySchwerpunkt(Geometry::OpenGLKernel * pKernel, Geometry::Polygon * pPoly, Vec3d & schwerpunkt)
   {
      Geometry::Polygon::PointerTriangleVector triangles = pPoly->GetPointerTriangles();
      Geometry::Polygon::PointerTriangleVector::iterator l,e;
      double area = -1;
      double triangle_area;
      Vec3d  triangle_schwerpunkt;

      if (triangles.empty())
      {
         pKernel->TesselatePolygon(pPoly);
         triangles = pPoly->GetPointerTriangles();
      }
      if (triangles.empty())
         return false;

      l = triangles.begin();
      e = triangles.end();
      for(; l!=e; l++)
      {
         AreaTriangle((*l).pP1, (*l).pP2, (*l).pP3, &triangle_area);
         triangle_area = fabs(triangle_area);
         GetTriangleSchwerpunkt((*l).pP1, (*l).pP2, (*l).pP3, triangle_schwerpunkt);

         if (area<0)
         {
            area = triangle_area;
            schwerpunkt = triangle_schwerpunkt;
         }
         else
         {
            area += triangle_area;
            if (area>0.0)
               schwerpunkt = schwerpunkt + (triangle_schwerpunkt - schwerpunkt) * (triangle_area / area);
         }
      }

      return true;
   }
*/
   //von Torsten aus polygon.cpp

   void Jacobi (const double b[3][3], double (&ov) [3], double (&v) [3][3])
   {
      typedef size_t INDEX;
      static const size_t n=3;
      static const double epsilon =1.0E-13;
      static const double beta =1.0E-15;

      double a[n][n];

      for (INDEX i=0; i<n; i++)
      {
         for (INDEX j=0; j<n; j++)
         {
            v[i][j]=0;
            a[i][j]=b[i][j];
         }

         v[i][i]=1;
      }

      long unsigned int counter=0;

      while (1)
      {
         double sum=0;
         for (INDEX i=1; i<n; i++)
            for (INDEX j=0; j<i; j++)
               sum+=sqr(a[i][j]);

         if (2*sum<sqr(epsilon)||counter>500)
         {
            for (INDEX i=0; i<n; i++)
            {
               ov[i]=a[i][i];
            }
            
            break;
         }

         for (INDEX p=0; p<n-1; p++)
            for (INDEX q=p+1; q<n; q++)
            {
               if (fabs(a[q][p])<epsilon)
                  continue;

               double O=(a[q][q]-a[p][p])/(2*a[q][p]);
               
               double t=1;
               if (fabs(O)>beta)
                  t=1.0/(O+sgn(O)*sqrt(sqr(O)+1));

               double c=1.0/sqrt(1+sqr(t));
               double s=c*t;
               double r=s/(1.0+c);
               a[p][p]-=t*a[q][p];
               a[q][q]+=t*a[q][p];
               a[q][p]=0;

               for (INDEX j=0; j<p; j++)
               {
                  double g=a[q][j]+r*a[p][j];
                  double h=a[p][j]-r*a[q][j];
                  a[p][j]-=s*g;
                  a[q][j]+=s*h;
               }

               for (INDEX i=p+1; i<q; i++)
               {
                  double g=a[q][i]+r*a[i][p];
                  double h=a[i][p]-r*a[q][i];
                  a[i][p]-=s*g;
                  a[q][i]+=s*h;
               }

               for (INDEX i=q+1; i<n; i++)
               {
                  double g=a[i][q]+r*a[i][p];
                  double h=a[i][p]-r*a[i][q];
                  a[i][p]-=s*g;
                  a[i][q]+=s*h;
               }

               for (INDEX i=0; i<n; i++)
               {
                  double g=v[i][q]+r*v[i][p];
                  double h=v[i][p]-r*v[i][q];
                  v[i][p]-=s*g;
                  v[i][q]+=s*h;
               }
            }

         counter++;
      }
   }

   void Jacobi (const long double b[3][3], long double (&ov) [3], long double (&v) [3][3])
   {
      typedef size_t INDEX;
      static const size_t n=3;
      static const long double epsilon =1.0E-26;
      static const long double beta =1.0E-30;

      long double a[n][n];

      for (INDEX i=0; i<n; i++)
      {
         for (INDEX j=0; j<n; j++)
         {
            v[i][j]=0;
            a[i][j]=b[i][j];
         }

         v[i][i]=1;
      }

      long unsigned int counter=0;

      while (1)
      {
         long double sum=0;
         for (INDEX i=1; i<n; i++)
            for (INDEX j=0; j<i; j++)
               sum+=sqr(a[i][j]);

         if (2*sum<sqr(epsilon)||counter>5000)
         {
            for (INDEX i=0; i<n; i++)
            {
               ov[i]=a[i][i];
            }
            
            break;
         }

         for (INDEX p=0; p<n-1; p++)
            for (INDEX q=p+1; q<n; q++)
            {
               if (fabs(a[q][p])<epsilon)
                  continue;

               long double O=(a[q][q]-a[p][p])/(2*a[q][p]);
               
               long double t=1;
               if (fabs(O)>beta)
                  t=1.0/(O+sgn(O)*sqrt(sqr(O)+1));

               long double c=1.0/sqrt(1+sqr(t));
               long double s=c*t;
               long double r=s/(1.0+c);
               a[p][p]-=t*a[q][p];
               a[q][q]+=t*a[q][p];
               a[q][p]=0;

               for (INDEX j=0; j<p; j++)
               {
                  long double g=a[q][j]+r*a[p][j];
                  long double h=a[p][j]-r*a[q][j];
                  a[p][j]-=s*g;
                  a[q][j]+=s*h;
               }

               for (INDEX i=p+1; i<q; i++)
               {
                  long double g=a[q][i]+r*a[i][p];
                  long double h=a[i][p]-r*a[q][i];
                  a[i][p]-=s*g;
                  a[q][i]+=s*h;
               }

               for (INDEX i=q+1; i<n; i++)
               {
                  long double g=a[i][q]+r*a[i][p];
                  long double h=a[i][p]-r*a[i][q];
                  a[i][p]-=s*g;
                  a[i][q]+=s*h;
               }

               for (INDEX i=0; i<n; i++)
               {
                  long double g=v[i][q]+r*v[i][p];
                  long double h=v[i][p]-r*v[i][q];
                  v[i][p]-=s*g;
                  v[i][q]+=s*h;
               }
            }

         counter++;
      }
   }

   void GetNorm (double (&a)[3][3], double &x, double &y, double &z)
   {
      double v[3];
      double vec[3][3];
      
      Jacobi(a,v,vec);
      
      unsigned char i;

      const double epsilon = 1.0E-10;
      if (v[0]>v[1])
         if (v[1]>v[2])
         {
            if (v[1]<v[2]+epsilon)
            {
               x=y=z=0;
               return;
            }
            i=2;
         }
         else
         {
            if (v[1]+epsilon>v[2])
            {
               x=y=z=0;
               return;
            }
            i=1;
         }
      else
         if (v[0]>v[2])
         {
            if (v[0]<v[2]+epsilon)
            {
               x=y=z=0;
               return;
            }
            i=2;
         }
         else
         {
            if (v[0]+epsilon>v[2])
            {
               x=y=z=0;
               return;
            }
            i=0;
         }

      x=vec[0][i];
      y=vec[1][i];
      z=vec[2][i];
   }

   void GetNorm (long double (&a)[3][3], long double &x, long double &y, long double &z)
   {
      long double v[3];
      long double vec[3][3];
      
      Jacobi(a,v,vec);
      
      unsigned char i;

      const long double epsilon = 1.0E-10;
      if (v[0]>v[1])
         if (v[1]>v[2])
         {
            if (v[1]<v[2]+epsilon)
            {
               x=y=z=0;
               return;
            }
            i=2;
         }
         else
         {
            if (v[1]+epsilon>v[2])
            {
               x=y=z=0;
               return;
            }
            i=1;
         }
      else
         if (v[0]>v[2])
         {
            if (v[0]<v[2]+epsilon)
            {
               x=y=z=0;
               return;
            }
            i=2;
         }
         else
         {
            if (v[0]+epsilon>v[2])
            {
               x=y=z=0;
               return;
            }
            i=0;
         }

      x=vec[0][i];
      y=vec[1][i];
      z=vec[2][i];
   }


   void ComputeKMatrix (const size_t size, const double * A, double (&K)[3][3])
   {
      for (unsigned int l1=0; l1<3; l1++)
         for (unsigned int l2=0; l2<3; l2++)
         {
            K[l1][l2]=0;
            for (unsigned l3=0; l3<size; l3++)
               K[l1][l2]+=A[l3*3+l1]*A[l3*3+l2];
         }
   }

   void ComputeKMatrix (const size_t size, const long double * A, long double (&K)[3][3])
   {
      for (unsigned int l1=0; l1<3; l1++)
         for (unsigned int l2=0; l2<3; l2++)
         {
            K[l1][l2]=0;
            for (unsigned l3=0; l3<size; l3++)
               K[l1][l2]+=A[l3*3+l1]*A[l3*3+l2];
         }
   }

   
   void ComputePlane (const Point_3d_Vector_Vector & points, Vec3d & center, Vec3d & vec)
   {
      size_t size = 0;

      Point_3d_Vector_Vector::const_iterator l1=points.begin(), e1=points.end();
      for (; l1!=e1; l1++)
         size+=(*l1).size();

      double * A  = new double [size*3];
      center = Vec3d(0,0,0);
      size_t position = 0;
      l1=points.begin();
      for (; l1!=e1; l1++)
      {
         Point_3d_Vector::const_iterator l2=(*l1).begin(), e2=(*l1).end();
         for (; l2!=e2; l2++)
         {
            center.x+=A[position++]=(*l2).x;
            center.y+=A[position++]=(*l2).y;
            center.z+=A[position++]=(*l2).z;
         }
      }

      center/=(const double)size;

      position = 0;
      for (size_t l3=0; l3<size; l3++)
      {
         A[position++]-=center.x;
         A[position++]-=center.y;
         A[position++]-=center.z;
      }

      double K[3][3];
      ComputeKMatrix(size,A,K);
      double x,y,z;
      GetNorm(K,x,y,z);
      delete A;
      vec=Vec3d(x,y,z);

      Point_3d_Vector_Vector::const_iterator l_pvv = points.begin();
      if ((*l_pvv).size()>=3)
      {
         double area;
         Vec3d vx, vy;
         Vekt_Rectangular(vec, vx);
         vy = vx.amul( vec );
         vx.Normalize();
         vy.Normalize();

         AreaLoopAngle(*l_pvv, &area, vx, vy, vec);

         if (area<0)
            vec = vec * -1;
      }
   }
/*   
   void ComputePlane (const Geometry::Polygon::PointVectorVector & points, Vec3d & center, Vec3d & vec)
   {
      size_t size = 0;

      Geometry::Polygon::PointVectorVector::const_iterator l1=points.begin(), e1=points.end();
      for (; l1!=e1; l1++)
         size+=(*l1).size();

      double * A  = new double [size*3];
      center = Vec3d(0,0,0);
      size_t position = 0;
      l1=points.begin();
      for (; l1!=e1; l1++)
      {
         Geometry::Polygon::PointVector::const_iterator l2=(*l1).begin(), e2=(*l1).end();
         for (; l2!=e2; l2++)
         {
            center.x+=A[position++]=(*l2)->getX();
            center.y+=A[position++]=(*l2)->getY();
            center.z+=A[position++]=(*l2)->getZ();
         }
      }

      center/=(const double)size;

      position = 0;
      for (size_t l3=0; l3<size; l3++)
      {
         A[position++]-=center.x;
         A[position++]-=center.y;
         A[position++]-=center.z;
      }

      double K[3][3];
      ComputeKMatrix(size,A,K);
      double x,y,z;
      GetNorm(K,x,y,z);
      delete [] A;
      vec=Vec3d(x,y,z);

      Geometry::Polygon::PointVectorVector::const_iterator l_pvv = points.begin();
      if ((*l_pvv).size()>=3)
      {
         double area;
         Vec3d vx, vy;
         Vekt_Rectangular(vec, vx);
         vy = vx.amul( vec );
         vx.Normalize();
         vy.Normalize();

         AreaLoopAngle(*l_pvv, &area, vx, vy, vec);

         if (area<0)
            vec = vec * -1;
      }
   }

   void ComputePlane_Newell (const Geometry::Polygon::PointVectorVector & points, Vec3d & mittelpunkt, Vec3d & normale)
   {
      Vec3d_long center, vec;
      size_t size = 0;
      Geometry::Polygon::PointVectorVector::const_iterator l1=points.begin(), e1=points.end();
      center = Vec3d_long(0,0,0);
      for (; l1!=e1; l1++)
      {
         Geometry::Polygon::PointVector::const_iterator l2=(*l1).begin(), e2=(*l1).end();
         for (; l2!=e2; l2++)
         {
            center.x+=(*l2)->getX();
            center.y+=(*l2)->getY();
            center.z+=(*l2)->getZ();
            ++size;
         }
      }

      center/=(const double)size;
      vec = Vec3d_long(0,0,0);
      size = 0;
      l1=points.begin();
      for (; l1!=e1; l1++)
      {
         Vec3d_long N(0,0,0), Vn, Vn2;
         int loop_size(0);
         Geometry::Polygon::PointVector::const_iterator l2=(*l1).begin(), e2=(*l1).end(), l3;
         for (; l2!=e2; l2++)
         {
            l3 = l2;
            l3++;
            if (l3==e2)
               l3 = (*l1).begin();
            loop_size++;

            Vn  = GetVec3d_long(*l2);
            Vn2 = GetVec3d_long(*l3);
            N.x += (Vn.y - Vn2.y) * (Vn.z + Vn2.z);
            N.y += (Vn.z - Vn2.z) * (Vn.x + Vn2.x);
            N.z += (Vn.x - Vn2.x) * (Vn.y + Vn2.y);
         }
         N.Normalize();
         if (size == 0)
         {
            vec = N;
            size = loop_size;
         }
         else
         {
            //gleiche Seite?
            if ((N + vec).Value2()<1)
               //umdrehen
               N = N * -1.0;
            Vec3d_long new_vec = N * loop_size + vec * (const long double)size;
            new_vec.Normalize();
            vec = new_vec;
            size += loop_size;
         }
      }

      mittelpunkt.x = (double)center.x;
      mittelpunkt.y = (double)center.y;
      mittelpunkt.z = (double)center.z;
      normale.x = (double)vec.x;
      normale.y = (double)vec.y;
      normale.z = (double)vec.z;
   }

   void ComputePlane (const Geometry::Polygon::PointVectorVector & points, Vec3d_long & center, Vec3d_long & vec)
   {
      size_t size = 0;

      Geometry::Polygon::PointVectorVector::const_iterator l1=points.begin(), e1=points.end();
      for (; l1!=e1; l1++)
         size+=(*l1).size();

      long double * A  = new long double [size*3];
      center = Vec3d_long(0,0,0);
      size_t position = 0;
      l1=points.begin();
      for (; l1!=e1; l1++)
      {
         Geometry::Polygon::PointVector::const_iterator l2=(*l1).begin(), e2=(*l1).end();
         for (; l2!=e2; l2++)
         {
            center.x+=A[position++]=(*l2)->getX();
            center.y+=A[position++]=(*l2)->getY();
            center.z+=A[position++]=(*l2)->getZ();
         }
      }

      center/=(const double)size;

      position = 0;
      for (size_t l3=0; l3<size; l3++)
      {
         A[position++]-=center.x;
         A[position++]-=center.y;
         A[position++]-=center.z;
      }

      long double K[3][3];
      ComputeKMatrix(size,A,K);
      long double x,y,z;
      GetNorm(K,x,y,z);
      delete [] A;
      vec=Vec3d_long(x,y,z);

      Geometry::Polygon::PointVectorVector::const_iterator l_pvv = points.begin();
      if ((*l_pvv).size()>=3)
      {
         double area;
         Vec3d vx, vy;
         Vec3d vec_copy((double)vec.x, (double)vec.y, (double)vec.z);
         Vekt_Rectangular(vec_copy, vx);
         vy = vx.amul( vec_copy );
         vx.Normalize();
         vy.Normalize();

         AreaLoopAngle(*l_pvv, &area, vx, vy, vec_copy);

         if (area<0)
            vec = vec * -1;
      }
   }

   void ComputePlane (const Geometry::Polygon::PointVector & points, Vec3d & center, Vec3d & vec)
   {
      size_t size = points.size();

      double * A  = new double [size*3];
      center = Vec3d(0,0,0);
      size_t position = 0;

      Geometry::Polygon::PointVector::const_iterator l2=points.begin(), e2=points.end();
      for (; l2!=e2; l2++)
      {
         center.x+=A[position++]=(*l2)->getX();
         center.y+=A[position++]=(*l2)->getY();
         center.z+=A[position++]=(*l2)->getZ();
      }

      center/=(const double)size;

      position = 0;
      for (size_t l3=0; l3<size; l3++)
      {
         A[position++]-=center.x;
         A[position++]-=center.y;
         A[position++]-=center.z;
      }

      double K[3][3];
      ComputeKMatrix(size,A,K);
      double x,y,z;
      GetNorm(K,x,y,z);
      delete A;
      vec=Vec3d(x,y,z);

      if (points.size()>=3)
      {
         double area;
         Vec3d vx, vy;
         Vekt_Rectangular(Vec3d((double)vec.x, (double)vec.y, (double)vec.z), vx);
         vy = vx.amul( vec );
         vx.Normalize();
         vy.Normalize();

         AreaLoopAngle(points, &area, vx, vy, vec);

         if (area<0)
            vec = vec * -1;
      }
   }
*/
   void ComputePlane (const Point_3d_Vector & points, Vec3d & center, Vec3d & vec)
   {
      size_t size = points.size();

      double * A  = new double [size*3];
      center = Vec3d(0,0,0);
      size_t position = 0;

      Point_3d_Vector::const_iterator l2=points.begin(), e2=points.end();
      for (; l2!=e2; l2++)
      {
         center.x+=A[position++]=(*l2).x;
         center.y+=A[position++]=(*l2).y;
         center.z+=A[position++]=(*l2).z;
      }

      center/=(const double)size;

      position = 0;
      for (size_t l3=0; l3<size; l3++)
      {
         A[position++]-=center.x;
         A[position++]-=center.y;
         A[position++]-=center.z;
      }

      double K[3][3];
      ComputeKMatrix(size,A,K);
      double x,y,z;
      GetNorm(K,x,y,z);
      delete A;
      vec=Vec3d(x,y,z);

      if (points.size()>=3)
      {
         double area;
         Vec3d vx, vy;
         Vekt_Rectangular(Vec3d((double)vec.x, (double)vec.y, (double)vec.z), vx);
         vy = vx.amul( vec );
         vx.Normalize();
         vy.Normalize();

         AreaLoopAngle(points, &area, vx, vy, vec);

         if (area<0)
            vec = vec * -1;
      }
   }

   /*
   PROCEDURE Proj_N_3d (P0:Point_3d; S:Plane_3d; VAR P:Point_3d);
   VAR
    q,r:Real;
   BEGIN
    q:= S.a * S.a + S.b * S.b + S.c * S.c;
    r:= (S.a * P0.x + S.b * P0.y + S.c * P0.z + S.d)/q;
    P.x:= P0.x - S.a * r;
    P.y:= P0.y - S.b * r;
    P.z:= P0.z - S.c * r;
   END;

   PROCEDURE Proj_V_3d (P0:Point_3d; V:Vekt_3d; S:Plane_3d; VAR P:Point_3d);
   VAR
    q,r: Real;
   BEGIN
    q:= S.a * V.x + S.b * V.y + S.c * V.z;
    IF Abs(q) < Eps6 THEN Err_3d:= 0
    ELSE BEGIN
     r:= (S.a * P0.x + S.b * P0.y + S.c * P0.z + S.d) / q;
     P.x:= P0.x - V.x * r;
     P.y:= P0.y - V.y * r;
     P.z:= P0.z - V.z * r;
     Err_3d:= 1;
    END;
   END;
   */
/*
   void AnalysePolygonAxes(const Vec3d & vx, const Vec3d &vy, const Vec3d &normale, Geometry::Polygon * pPoly, Vec3d &base_point, Vec3d &size)
   {
      const Geometry::Polygon::PointVectorVector points = pPoly->GetPoints();
      Geometry::Polygon::PointVectorVector::const_iterator l_pvv = points.begin();
      Geometry::Polygon::PointVectorVector::const_iterator e_pvv = points.end();
      Geometry::Point *pPoint;
      bool start=true;
      Vec3d start_point, dist_point, erg_point, min_point, max_point;

      //alle Punkte durchgehen
      start_point = GetVec3d( *(*l_pvv).begin() );
      l_pvv = points.begin();
      e_pvv = points.end();
      for(; l_pvv!=e_pvv; l_pvv++)
      {
         Geometry::Polygon::PointVector::const_iterator l_pv=(*l_pvv).begin();
         Geometry::Polygon::PointVector::const_iterator e_pv=(*l_pvv).end();
         for(; l_pv!=e_pv; l_pv++)
         {
            pPoint = *l_pv;
            dist_point = Vec3d(pPoint->getX(), 
                               pPoint->getY(), 
                               pPoint->getZ());
            dist_point -= start_point;

            //Gleichungssystem l�sen
            erg_point = Cramer(vx, vy, normale, dist_point);

            if (start)
            {
               start = false;
               min_point = max_point = erg_point;
            }
            else
            {
               if (erg_point.x < min_point.x)    min_point.x = erg_point.x;
               if (erg_point.y < min_point.y)    min_point.y = erg_point.y;
               if (erg_point.z < min_point.z)    min_point.z = erg_point.z;
               if (erg_point.x > max_point.x)    max_point.x = erg_point.x;
               if (erg_point.y > max_point.y)    max_point.y = erg_point.y;
               if (erg_point.z > max_point.z)    max_point.z = erg_point.z;
            }
         }
      }

      //Werte setzen
      base_point = start_point + (vx      * min_point.x)
                               + (vy      * min_point.y)
                               + (normale * min_point.z);
      size = max_point - min_point;
   }
*/
   void AnalysePointVectorAxes(const Vec3d & vx, const Vec3d &vy, const Vec3d &normale, const Point_3d_Vector &points, Vec3d &base_point, Vec3d &size)
   {
       Point_3d_Vector::const_iterator l_pv, e_pv;
       bool start=true;
       Vec3d start_point, dist_point, erg_point, min_point, max_point;

       //alle Punkte durchgehen
       start_point = *points.begin();

       l_pv=points.begin();
       e_pv=points.end();
       for(; l_pv!=e_pv; l_pv++)
       {
           dist_point = *l_pv;
           dist_point -= start_point;

           //Gleichungssystem l�sen
           erg_point = Cramer(vx, vy, normale, dist_point);

           if (start)
           {
               start = false;
               min_point = max_point = erg_point;
           }
           else
           {
               if (erg_point.x < min_point.x)    min_point.x = erg_point.x;
               if (erg_point.y < min_point.y)    min_point.y = erg_point.y;
               if (erg_point.z < min_point.z)    min_point.z = erg_point.z;
               if (erg_point.x > max_point.x)    max_point.x = erg_point.x;
               if (erg_point.y > max_point.y)    max_point.y = erg_point.y;
               if (erg_point.z > max_point.z)    max_point.z = erg_point.z;
           }
       }

       //Werte setzen
       base_point = start_point 
           + (vx      * min_point.x)
           + (vy      * min_point.y)
           + (normale * min_point.z);
       size = max_point - min_point;
   }
/*
   void AnalysePolygon(Geometry::Polygon * pPoly, Vec3d &base_point, Vec3d & vx, Vec3d &vy, Vec3d &normale, Vec3d &size)
   {
      const Geometry::Polygon::PointVectorVector points = pPoly->GetPoints();
      Geometry::Polygon::PointVectorVector::const_iterator l_pvv = points.begin();
      Geometry::Polygon::PointVectorVector::const_iterator e_pvv = points.end();
      Geometry::Point *pPoint;
      Vec3d start_point, temp_point, dist_point, erg_point, min_point, max_point, last_point, best_vx, best_vy, best_size, best_basepoint;
      bool start=true;
      double area, best_area = -1;

      //vx bestimmen
      normale = pPoly->GetNormale();
      for(; l_pvv!=e_pvv; l_pvv++)
      {
         Geometry::Polygon::PointVector::const_iterator l_pv=(*l_pvv).begin();
         Geometry::Polygon::PointVector::const_iterator e_pv=(*l_pvv).end();
         for(; l_pv!=e_pv; l_pv++)
         {
            pPoint = *l_pv;

            if (start)
            {
               start=false;
               temp_point = start_point = Vec3d(pPoint->getX(), 
                                                pPoint->getY(), 
                                                pPoint->getZ());
            }
            else
            {
               last_point = temp_point;
               temp_point = Vec3d(pPoint->getX(), 
                                  pPoint->getY(), 
                                  pPoint->getZ());

               vx = last_point - temp_point;
               vx.Normalize();
               if (vx.Value()>0.9)
               {
                  vy = normale.amul(vx);
                  vx = vy.amul(normale);
                  vx.Normalize();
                  vy.Normalize();

                  AnalysePolygonAxes(vx, vy, normale, pPoly, base_point, size);
                  area = fabs(size.x * size.y);
                  if ((area<best_area) || (best_area<0))
                  {
                     best_vx = vx;
                     best_vy = vy;
                     best_area = area;
                     best_basepoint = base_point;
                     best_size = size;
                  }
               }
            }
         }
      }
      vx = best_vx;
      vy = best_vy;
      base_point = best_basepoint;
      size = best_size;
   }

   void GetPolygonSize(Geometry::Polygon * pPoly, Vec3d vx, Vec3d vy, Vec3d normale, Vec3d &size)
   {
      const Geometry::Polygon::PointVectorVector points = pPoly->GetPoints();
      Geometry::Polygon::PointVectorVector::const_iterator l_pvv = points.begin();
      Geometry::Polygon::PointVectorVector::const_iterator e_pvv = points.end();
      Geometry::Polygon::PointVector::const_iterator l_pv, e_pv;
      Geometry::Point *pPoint;
      Vec3d dist_point, erg_point, min_point, max_point;
      bool start=true, done=false;

      //alle Punkte durchgehen
      start=true;
      for(; l_pvv!=e_pvv; l_pvv++)
      {
         l_pv=(*l_pvv).begin();
         e_pv=(*l_pvv).end();
         for(; l_pv!=e_pv; l_pv++)
         {
            pPoint = *l_pv;
            dist_point = Vec3d(pPoint->getX(), 
                               pPoint->getY(), 
                               pPoint->getZ());

            //Gleichungssystem l�sen
            erg_point = Cramer(vx, vy, normale, dist_point);

            if (start)
            {
               start = false;
               min_point = max_point = erg_point;
            }
            else
            {
               if (erg_point.x < min_point.x)    min_point.x = erg_point.x;
               if (erg_point.y < min_point.y)    min_point.y = erg_point.y;
               if (erg_point.z < min_point.z)    min_point.z = erg_point.z;
               if (erg_point.x > max_point.x)    max_point.x = erg_point.x;
               if (erg_point.y > max_point.y)    max_point.y = erg_point.y;
               if (erg_point.z > max_point.z)    max_point.z = erg_point.z;
            }
         }
      }
      size = max_point - min_point;
   }

   void GetPolygonSize(Geometry::Polygon * pPoly, Vec3d basepoint, Vec3d vx, Vec3d vy, Vec3d normale, Vec3d &size)
   {
      //basepoint ist obsolet
      GetPolygonSize(pPoly, vx, vy, normale, size);
   }

   void Get3MostFarAwayPoints(Geometry::Polygon *pPoly, Geometry::Point **pT1, Geometry::Point **pT2, Geometry::Point **pT3)
   {
      const Geometry::Polygon::PointVectorVector points = pPoly->GetPoints();
      if (points.empty())
         assert(0); //kein g�ltiges Poly

      Geometry::Polygon::PointVectorVector::const_iterator l_pvv, e_pvv;
      Geometry::Polygon::PointVector::const_iterator l_pv, e_pv;
      Geometry::Point *pPoint;

      //erster Punkt
      (*pT1) = (Geometry::Point *)*(*points.begin()).begin();
      Vec3d P1((**pT1).getX(), (**pT1).getY(), (**pT1).getZ()), temp;
      double dist, max_dist = -1;

      //zweiter Punkt
      l_pvv = points.begin();
      e_pvv = points.end();
      for(; l_pvv!=e_pvv; l_pvv++)
      {
         l_pv = (*l_pvv).begin();
         e_pv = (*l_pvv).end();
         for(; l_pv!=e_pv; l_pv++)
         {
            pPoint = *l_pv;
            if (pPoint==(*pT1))
               continue;
            temp = Vec3d(pPoint->getX(), pPoint->getY(), pPoint->getZ());
            dist = (P1 - temp).Value2();
            if (dist>max_dist)
            {
               max_dist = dist;
               (*pT2) = pPoint;
            }
         }
      }
      Vec3d P2((**pT2).getX(), (**pT2).getY(), (**pT2).getZ());

      //erster Punkt nochmal
      max_dist = -1;
      l_pvv = points.begin();
      e_pvv = points.end();
      for(; l_pvv!=e_pvv; l_pvv++)
      {
         l_pv = (*l_pvv).begin();
         e_pv = (*l_pvv).end();
         for(; l_pv!=e_pv; l_pv++)
         {
            pPoint = *l_pv;
            if (pPoint==(*pT2))
               continue;
            temp = Vec3d(pPoint->getX(), pPoint->getY(), pPoint->getZ());
            dist = (P2 - temp).Value2();
            if (dist>max_dist)
            {
               max_dist = dist;
               (*pT1) = pPoint;
               P1 = temp;
            }
         }
      }

      //dritter Punkt
      max_dist = -1;
      l_pvv = points.begin();
      e_pvv = points.end();
      for(; l_pvv!=e_pvv; l_pvv++)
      {
         l_pv = (*l_pvv).begin();
         e_pv = (*l_pvv).end();
         for(; l_pv!=e_pv; l_pv++)
         {
            pPoint = *l_pv;
            if ((pPoint==(*pT1)) || (pPoint==(*pT2)))
               continue;
            temp = Vec3d(pPoint->getX(), pPoint->getY(), pPoint->getZ());
            AreaTriangle(*pT1, *pT2, pPoint, &dist);
            dist = fabs(dist);
            if (dist>max_dist)
            {
               max_dist = dist;
               (*pT3) = pPoint;
            }
         }
      }

      if (!(*pT1) || !(*pT2) || !(*pT3))
      {
         assert(0);
         Get3MostFarAwayPoints(pPoly, pT1, pT2, pT3);
      }
   }
   double FlatenPoly(Geometry::Polygon *pPoly, double faktor)
   {
      double delta, max_delta = 0.0;
      Vec3d base_point, normale, start_punkt, lot_punkt, end_punkt;
      CGeom3d::Plane_3d plane;
      //CGeom3d::Line_3d line;
      const Geometry::Polygon::PointVectorVector points = pPoly->GetPoints();
      Geometry::Polygon::PointVectorVector::const_iterator l_pvv = points.begin();
      Geometry::Polygon::PointVectorVector::const_iterator e_pvv = points.end();
      Geometry::Polygon::PointVector::const_iterator l_pv, e_pv;
      Geometry::Point *pPoint;

      ComputePlane(points, base_point, normale);
      Plane_PN_3d(base_point, normale, plane);

      //alle Punkte durchgehen
      for(; l_pvv!=e_pvv; l_pvv++)
      {
         l_pv=(*l_pvv).begin();
         e_pv=(*l_pvv).end();
         for(; l_pv!=e_pv; l_pv++)
         {
            pPoint = *l_pv;
            start_punkt = Vec3d(pPoint->getX(), 
                                pPoint->getY(), 
                                pPoint->getZ());
            //Line_VP_3d(Vec3d(plane.a, plane.b, plane.c), start_punkt, line);
            //Point_LS_3d(line, plane, lot_punkt);
            Proj_N_3d(start_punkt, plane, lot_punkt);
            delta = (start_punkt-lot_punkt).Value();
            if (delta>max_delta)
               max_delta = delta;
            end_punkt = start_punkt - ((start_punkt - lot_punkt) * faktor);

            pPoint->setX(end_punkt.x);
            pPoint->setY(end_punkt.y);
            pPoint->setZ(end_punkt.z);
         }
      }

      return max_delta;
   }

   double FlatenPoly_SetFast(Geometry::Polygon *pPoly, double faktor)
   {
      double delta, max_delta = 0.0;
      Vec3d base_point, normale, start_punkt, lot_punkt, end_punkt;
      CGeom3d::Plane_3d plane;
      //CGeom3d::Line_3d line;
      const Geometry::Polygon::PointVectorVector points = pPoly->GetPoints();
      Geometry::Polygon::PointVectorVector::const_iterator l_pvv = points.begin();
      Geometry::Polygon::PointVectorVector::const_iterator e_pvv = points.end();
      Geometry::Polygon::PointVector::const_iterator l_pv, e_pv;
      Geometry::Point *pPoint;

      ComputePlane(points, base_point, normale);
      Plane_PN_3d(base_point, normale, plane);

      //alle Punkte durchgehen
      for(; l_pvv!=e_pvv; l_pvv++)
      {
         l_pv=(*l_pvv).begin();
         e_pv=(*l_pvv).end();
         for(; l_pv!=e_pv; l_pv++)
         {
            pPoint = *l_pv;
            start_punkt = Vec3d(pPoint->getX(), 
                                pPoint->getY(), 
                                pPoint->getZ());
            //Line_VP_3d(Vec3d(plane.a, plane.b, plane.c), start_punkt, line);
            //Point_LS_3d(line, plane, lot_punkt);
            Proj_N_3d(start_punkt, plane, lot_punkt);
            delta = (start_punkt-lot_punkt).Value();
            if (delta>max_delta)
               max_delta = delta;
            end_punkt = start_punkt - ((start_punkt - lot_punkt) * faktor);

            pPoint->setXfast(end_punkt.x);
            pPoint->setYfast(end_punkt.y);
            pPoint->setZfast(end_punkt.z);
         }
      }

      return max_delta;
   }

   double FlatenPoly_SetFast(Geometry::Polygon *pPoly, const double faktor, const Plane_3d &plane)
   {
      double delta, max_delta = 0.0;
      Vec3d start_punkt, lot_punkt, end_punkt;
      const Geometry::Polygon::PointVectorVector points = pPoly->GetPoints();
      Geometry::Polygon::PointVectorVector::const_iterator l_pvv = points.begin();
      Geometry::Polygon::PointVectorVector::const_iterator e_pvv = points.end();
      Geometry::Polygon::PointVector::const_iterator l_pv, e_pv;
      Geometry::Point *pPoint;

      //alle Punkte durchgehen
      for(; l_pvv!=e_pvv; l_pvv++)
      {
         l_pv=(*l_pvv).begin();
         e_pv=(*l_pvv).end();
         for(; l_pv!=e_pv; l_pv++)
         {
            pPoint = *l_pv;
            start_punkt = Vec3d(pPoint->getX(), 
                                pPoint->getY(), 
                                pPoint->getZ());
            //Line_VP_3d(Vec3d(plane.a, plane.b, plane.c), start_punkt, line);
            //Point_LS_3d(line, plane, lot_punkt);
            Proj_N_3d(start_punkt, plane, lot_punkt);
            delta = (start_punkt-lot_punkt).Value();
            if (delta>max_delta)
               max_delta = delta;
            end_punkt = start_punkt - ((start_punkt - lot_punkt) * faktor);

            pPoint->setXfast(end_punkt.x);
            pPoint->setYfast(end_punkt.y);
            pPoint->setZfast(end_punkt.z);
         }
      }

      return max_delta;
   }

   double FlatenPoly_long_SetFast(Geometry::Polygon *pPoly, double faktor)
   {
      long double delta, max_delta = 0.0;
      Vec3d_long base_point, normale, start_punkt, lot_punkt, end_punkt;
      CGeom3d::Plane_3d_long plane;
      const Geometry::Polygon::PointVectorVector points = pPoly->GetPoints();
      Geometry::Polygon::PointVectorVector::const_iterator l_pvv = points.begin();
      Geometry::Polygon::PointVectorVector::const_iterator e_pvv = points.end();
      Geometry::Polygon::PointVector::const_iterator l_pv, e_pv;
      Geometry::Point *pPoint;

      ComputePlane(points, base_point, normale);
      Plane_PN_3d(base_point, normale, plane);

      //alle Punkte durchgehen
      for(; l_pvv!=e_pvv; l_pvv++)
      {
         l_pv=(*l_pvv).begin();
         e_pv=(*l_pvv).end();
         for(; l_pv!=e_pv; l_pv++)
         {
            pPoint = *l_pv;
            start_punkt = Vec3d_long(pPoint->getX(), 
                                     pPoint->getY(), 
                                     pPoint->getZ());
            //Line_VP_3d(Vec3d(plane.a, plane.b, plane.c), start_punkt, line);
            //Point_LS_3d(line, plane, lot_punkt);
            Proj_N_3d(start_punkt, plane, lot_punkt);
            delta = (start_punkt-lot_punkt).Value();
            if (delta>max_delta)
               max_delta = delta;
            end_punkt = start_punkt - ((start_punkt - lot_punkt) * faktor);

            pPoint->setXfast(end_punkt.x);
            pPoint->setYfast(end_punkt.y);
            pPoint->setZfast(end_punkt.z);
         }
      }

      return max_delta;
   }
*/
   double GetXYAngle(Vec3d point)
   {
      double angle;

      if (fabs(point.y)>Eps6)
      {
         angle=atan(-point.x/point.y);
         if (point.y<0)    angle+=DSL_PI;
      }
      else
      {
         if (point.x<0) angle=DSL_PI/2.0;
         else           angle=3*DSL_PI/2.0;
      }

      while (angle<0)   angle += 2*DSL_PI;

      return angle;
   }
/*   
   bool Are_Lines_Touching(Geometry::Point *pPoint11, Geometry::Point *pPoint12, Geometry::Point *pPoint21, Geometry::Point *pPoint22)
   {
      Vec3d p11 = GetVec3d(pPoint11);
      Vec3d p12 = GetVec3d(pPoint12);
      Vec3d p21 = GetVec3d(pPoint21);
      Vec3d p22 = GetVec3d(pPoint22);
      Vec3d normale1 = p11-p12;
      Vec3d normale2 = p21-p22;

      if (fabs(fabs(cos(normale1, normale2))-1.0f)>Eps3)
         return false;

      return (InLineWithoutEndPoints_3d(p11,p12,p21) || 
              InLineWithoutEndPoints_3d(p11,p12,p22) || 
              InLineWithoutEndPoints_3d(p21,p22,p11) || 
              InLineWithoutEndPoints_3d(p21,p22,p12) ||
              (((p11-p21).Value()<Eps6) && ((p12-p22).Value()<Eps6)) || 
              (((p12-p21).Value()<Eps6) && ((p11-p22).Value()<Eps6)));
   }

   bool Polygons_Have_Same_Normal(Geometry::Polygon * pPoly1, Geometry::Polygon * pPoly2, float delta_angle=Eps3)
   {
      Vec3d normale1, normale2;
      //m�ssen in der selben Ebene liegen
      normale1 = pPoly1->GetNormale();
      normale2 = pPoly2->GetNormale();
      if (fabs(fabs(cos(normale1, normale2))-1.0f)>delta_angle)
         return false;
      else
         return true;

   }
   
   bool Are_Polygons_Attached(Geometry::Polygon * pPoly1, Geometry::Polygon * pPoly2)
   {
      //... und m�ssen gemeinsame Kanten haben 
      const Geometry::Polygon::PointVectorVector points1 = pPoly1->GetPoints();
      const Geometry::Polygon::PointVectorVector points2 = pPoly2->GetPoints();
      Geometry::Polygon::PointVectorVector::const_iterator l_pvv, l2_pvv;
      Geometry::Polygon::PointVectorVector::const_iterator e_pvv, e2_pvv;
      Geometry::Polygon::PointVector::const_iterator l_pv, e_pv, l2_pv, e2_pv;
      Geometry::Point * pPoint11, *pPoint12, *pPoint21, *pPoint22;

      l_pvv = points1.begin();
      e_pvv = points1.end();
      for(; l_pvv!=e_pvv; l_pvv++)
      {
         pPoint12 = *(*l_pvv).rbegin();
         l_pv=(*l_pvv).begin();
         e_pv=(*l_pvv).end();
         for(; l_pv!=e_pv; l_pv++)
         {
            pPoint11 = *l_pv;

            l2_pvv = points2.begin();
            e2_pvv = points2.end();
            for(; l2_pvv!=e2_pvv; l2_pvv++)
            {
               pPoint22 = *(*l2_pvv).rbegin();
               l2_pv=(*l2_pvv).begin();
               e2_pv=(*l2_pvv).end();
               for(; l2_pv!=e2_pv; l2_pv++)
               {
                  pPoint21 = *l2_pv;
                  //Check Kanten
                  if (Are_Lines_Touching(pPoint11, pPoint12, pPoint21, pPoint22))
                     return true;

                  pPoint22 = pPoint21;
               }
            }

            pPoint12 = pPoint11;
         }
      }

      return false;
   }

   bool IsPolygonValid(Geometry::Polygon *pPoly)
   {
      const Geometry::Polygon::PointVectorVector points = pPoly->GetPoints();
      if (points.empty() || (*points.begin()).empty())
         return false;

      Vec3d normale, center;

      ComputePlane(points, center, normale);

      if ((normale.Value()<Eps6) ||
          (_isnan(normale.x)) ||
          (_isnan(normale.y)) ||
          (_isnan(normale.z)))
         return false;

      Geometry::Polygon::PointVectorVector::const_iterator l_pvv, e_pvv;
      Geometry::Polygon::PointVector::const_iterator l_pv, e_pv;
      Geometry::Point *pPoint;

      l_pvv = points.begin();
      e_pvv = points.end();
      for(; l_pvv!=e_pvv; l_pvv++)
      {
         l_pv = (*l_pvv).begin();
         e_pv = (*l_pvv).end();

         for(; l_pv!=e_pv; l_pv++)
         {
            pPoint = *l_pv;
            if (_isnan(pPoint->getX())) return false;
            if (_isnan(pPoint->getY())) return false;
            if (_isnan(pPoint->getZ())) return false;
         }
      }
      return true;
   }
   bool Points_Have_Changed(const Geometry::Polygon *pPoly)
   {
      //points und old_points vergleichen - alles kopiert aus "OpenGLDraw.cpp"

      const Geometry::Polygon::PointVectorVector points = pPoly->GetPoints();
      const Geometry::Polygon::PointVectorVector old_points = pPoly->GetOldPoints();
      if (points.size()!=old_points.size())
         return true;

      Geometry::Polygon::PointVectorVector::const_iterator pvv_l,pvv_l2,pvv_e;
      Geometry::Polygon::PointVector::const_iterator pv_l,pv_l2,pv_e;

      pvv_l=points.begin();
      pvv_l2=old_points.begin();
      pvv_e=points.end();

      for (; pvv_l!=pvv_e; pvv_l++)
      {
         if ((*pvv_l).size()!=(*pvv_l2).size())
            return true;

         pv_l=(*pvv_l).begin();
         pv_e=(*pvv_l).end();
         pv_l2=(*pvv_l2).begin();

         for (; pv_l!=pv_e; pv_l++)
         {
            if ((Geometry::Point *)(*pv_l)!=(Geometry::Point *)(*pv_l2))
               return true;

            pv_l2++;
         }

         pvv_l2++;
      }

      //triangles checken

      Geometry::Polygon::TriangleVector triangles=pPoly->GetTriangles();
      Geometry::Polygon::TriangleVector::iterator l=triangles.begin(), e=triangles.end();
      Geometry::Polygon::PointerTriangleVector pointer_triangles=pPoly->GetPointerTriangles();
      Geometry::Polygon::PointerTriangleVector::iterator l2=pointer_triangles.begin(), e2=pointer_triangles.end();

      if (triangles.size()!=pointer_triangles.size())
         assert(0);

      for (; l!=e; l++)
      {
         if (((*l).p1-Vec3d((*(*l2).pP1).getX(),
                            (*(*l2).pP1).getY(),
                            (*(*l2).pP1).getZ())).Value()>0.00001) return true;
         if (((*l).p2-Vec3d((*(*l2).pP2).getX(),
                            (*(*l2).pP2).getY(),
                            (*(*l2).pP2).getZ())).Value()>0.00001) return true;
         if (((*l).p3-Vec3d((*(*l2).pP3).getX(),
                            (*(*l2).pP3).getY(),
                            (*(*l2).pP3).getZ())).Value()>0.00001) return true;
         l2++;
      }

      return false;
   }

   void ReversePolyPVV(Geometry::Polygon *pPoly)
   {
      Geometry::Polygon::PointVectorVector pvv;
      Geometry::Polygon::PointVectorVector::const_iterator l_pvv, e_pvv;
      const Geometry::Polygon::PointVectorVector points = pPoly->GetPoints();

      l_pvv = points.begin();
      e_pvv = points.end();
      for(; l_pvv!=e_pvv; l_pvv++)
      {
         const Geometry::Polygon::PointVector &loop = *l_pvv;
         Geometry::Polygon::PointVector::const_reverse_iterator l_pv, e_pv;
         Geometry::Polygon::PointVector pv;

         l_pv = loop.rbegin();
         e_pv = loop.rend();
         for(; l_pv!=e_pv; l_pv++)
         {
            Geometry::Point * pPoint = *l_pv;
            pv.push_back( pPoint );
         }
         pvv.push_back(pv);
      }

      pPoly->SetPoints(pvv);
   }


   bool ArePolysEqual(const Geometry::Polygon *pPoly1, const Geometry::Polygon *pPoly2) const
   {
      Geometry::Body::PointSet points1, points2;

      //Poly1
      const Geometry::Polygon::PointVectorVector pvv = pPoly1->GetPoints();
      Geometry::Polygon::PointVectorVector::const_iterator l_pvv, e_pvv;
      l_pvv = pvv.begin();
      e_pvv = pvv.end();
      for(; l_pvv!=e_pvv; l_pvv++)
      {
         const Geometry::Polygon::PointVector &pv = (*l_pvv);
         Geometry::Polygon::PointVector::const_iterator l_pv, e_pv;
         l_pv = pv.begin();
         e_pv = pv.end();
         for(; l_pv!=e_pv; l_pv++)
         {
            Geometry::Point *pPoint = *l_pv;
            points1.insert(pPoint);
         }
      }
      //Poly2
      const Geometry::Polygon::PointVectorVector pvv2 = pPoly2->GetPoints();
      l_pvv = pvv2.begin();
      e_pvv = pvv2.end();
      for(; l_pvv!=e_pvv; l_pvv++)
      {
         const Geometry::Polygon::PointVector &pv = (*l_pvv);
         Geometry::Polygon::PointVector::const_iterator l_pv, e_pv;
         l_pv = pv.begin();
         e_pv = pv.end();
         for(; l_pv!=e_pv; l_pv++)
         {
            Geometry::Point *pPoint = *l_pv;
            points2.insert(pPoint);
         }
      }

      //Checken
      if (points1.size() != points2.size())
         return false;

      Geometry::Body::PointSet::const_iterator l1, l2, e1;
      l1 = points1.begin();
      e1 = points1.end();
      l2 = points2.begin();
      for(; l1!=e1; l1++, l2++)
      {
         const Geometry::Point *pPoint1 = *l1;
         const Geometry::Point *pPoint2 = *l2;
         if (pPoint1 != pPoint2)
            return false;
      }

      return true;
   }
*/
};

static CGeom3d Geom3d;

} //end DSL namespace
#include "stdafx.h"
#include "hdrimage.h"
#include <math.h>
#include "ColorMath.h"
//#include "../../Wcc/ColorMath.h"
//#include "../../TextureMagic/MfcIplImage.h"

#define  MINELEN       8       /* minimum scanline length for encoding */
#define  MAXELEN       0x7fff  /* maximum scanline length for encoding */
#define  MINRUN        4       /* minimum run length */

#define at_image(x,y,Img)     ((y)*((Img)->widthStep)+(x)*((Img)->nChannels))
#define RED                   0
#define GRE                   1
#define BLU                   2
#define ALPHA                 3
#define Eps6                  0.000001
#define Eps9                  0.000000001

//CHDRImage::CHDRImage(CWnd *pWnd)
CHDRImage::CHDRImage()
{
   height=0;
   width=0;
   size=0;
   image=NULL;
   pFloatImage = NULL;
   pRGBEOrginal = NULL;
   fPixelBlur = 0;
   iBlurArraySize = 0;
   ppBlurArray = NULL;
   //m_pWnd = pWnd;
}

CHDRImage::~CHDRImage(void)
{
   if (image!=NULL)
   {
      free(image);
	  //DeleteObject(hBitmap);
   }

   if (pFloatImage)
   {
	   free( pFloatImage );
	   pFloatImage = NULL;
   }

   if (pRGBEOrginal)
   {
	   free( pRGBEOrginal );
	   pRGBEOrginal = NULL;
   }

   if (iBlurArraySize > 0)
   {
	   for(int i = 0; i < iBlurArraySize; ++i)
		   cvReleaseImage( &ppBlurArray[i] );
	   free( ppBlurArray );
	   ppBlurArray = NULL;
   }
}

void CHDRImage::GenerateBlurArray()
{
	if (iBlurArraySize > 0)
		assert(0);

	iBlurArraySize = (int)ceil( log((float)height) / log(2.0) ) + 1;
	ppBlurArray = (IplImage **)malloc(sizeof(IplImage *) * iBlurArraySize);

	int bWidth = width;
	int bHeight = height;
	IplImage *pLast;
	for(int i = 0; i < iBlurArraySize; ++i)
	{
		ppBlurArray[i] = cvCreateImage( cvSize(bWidth, bHeight), IPL_DEPTH_32F, 3);

		if (i == 0)
		{
			for(int y = 0; y < bHeight; ++y)
				for(int x = 0; x < bWidth; ++x)
					GetPixel(x, y, (CV_IMAGE_ELEM(ppBlurArray[i], float, y, 3 * x + RED))
						         , (CV_IMAGE_ELEM(ppBlurArray[i], float, y, 3 * x + GRE))
								 , (CV_IMAGE_ELEM(ppBlurArray[i], float, y, 3 * x + BLU)));
			pLast = ppBlurArray[i];
		}
		else
		{
			cvResize(pLast, ppBlurArray[i]);
			pLast = ppBlurArray[i];
		}

		bWidth = (int)floor(bWidth / 2.0f + 0.5f);
		bHeight = (int)floor(bHeight / 2.0f + 0.5f);
		if (bWidth < 0) bWidth = 1;
		if (bHeight < 0) bHeight = 1;
	}
}

double CHDRImage::max3(pixel * color)
{
   if ((color->r>color->g) && (color->r>color->b))
      return color->r;
   else
   if ((color->g>color->r) && (color->g>color->b))
      return color->g;
   else
      return color->b;
}

void CHDRImage::Write_Line(CFile & myFile, int y)
{
  int  i, j, beg, cnt = 1;
  int  c2;
  RGBE color_rgbe;
  RGBE *line_rgbe;

  if ((width < MINELEN) || (width > MAXELEN))
  {
    for (j = 0; j < width; j++)
    {
      ColorToRGBE(&color_rgbe, (pixel*) image + (j + y * width) );
      myFile.Write(&color_rgbe, sizeof(RGBE));
    }
  }
  else
  {
    unsigned char data_byte;
    data_byte=2;
    myFile.Write(&data_byte,sizeof(unsigned char)); /* put magic header */
    myFile.Write(&data_byte,sizeof(unsigned char));
    data_byte=width>>8;
    myFile.Write(&data_byte,sizeof(unsigned char));
    data_byte=width&255;
    myFile.Write(&data_byte,sizeof(unsigned char));

    line_rgbe = (RGBE *)malloc(width * sizeof(RGBE)); //"HDR image line"

    for (j = 0; j < width; j++)                 /* convert pixels */
    {
      ColorToRGBE(line_rgbe + j, (pixel*) image + (j + y * width) );
    }
                                    /* put components seperately */
    for (i = 0; i < 4; i++)
    {
      for (j = 0; j < width; j += cnt)
      {                                   /* find next run */
        for (beg = j; beg < width; beg += cnt)
        {
          for (cnt = 1; ((cnt < 127) && (beg+cnt < width) &&
               ( *((unsigned char *)(line_rgbe+(beg+cnt))+ i) == 
                 *((unsigned char *)(line_rgbe+(beg))    + i) )); cnt++) ;
          if (cnt >= MINRUN) break;       /* long enough */
        }
        if ((beg-j > 1) && (beg-j < MINRUN))
        {
          c2 = j+1;
          while (*((unsigned char *)(line_rgbe+(c2++))+ i) == 
                 *((unsigned char *)(line_rgbe+(j))   + i))
            if (c2 == beg)
            {                             /* short run */
              data_byte=128+beg-j;
              myFile.Write(&data_byte,sizeof(unsigned char));
              data_byte=*((unsigned char*)(line_rgbe + j) + i);
              myFile.Write(&data_byte,sizeof(unsigned char));
              j = beg;
              break;
            }
        }
        while (j < beg)
        {                                 /* write out non-run */
          if ((c2 = beg-j) > 128) c2 = 128;
          data_byte=(unsigned char)c2;
          myFile.Write(&data_byte,sizeof(unsigned char));
          while (c2--)
          {
             data_byte=*((unsigned char *)(line_rgbe+(j++)) + i);
             myFile.Write(&data_byte,sizeof(unsigned char));
          }
        }
        if (cnt >= MINRUN)
        {                                 /* write out run */
          data_byte=128+cnt;
          myFile.Write(&data_byte,sizeof(unsigned char));
          data_byte=*((unsigned char *)(line_rgbe+beg) + i);
          myFile.Write(&data_byte,sizeof(unsigned char));
        }
        else cnt = 0;
      }
    }

    free(line_rgbe);
  }
}


bool CHDRImage::ExportHDRImage(CString filename)
{
   if ((height>0) && (width>0))
   {
      HANDLE hFile = CreateFile(_T(filename),
                                GENERIC_WRITE, FILE_SHARE_READ,
                                NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

      if (hFile == INVALID_HANDLE_VALUE)
         //AfxMessageBox(_T("Couldn't create the HDR-file!"));
		 return false;
      else
      {
         CFile myFile(hFile);
         char data[512];

         sprintf(data,"#?RADIANCE\n");                            myFile.Write(data, lstrlen(data));
		 sprintf(data,"#SOFTWARE= Colored Architecture\n");        myFile.Write(data, lstrlen(data));
         sprintf(data,"FORMAT=32-bit_rle_rgbe\n");               myFile.Write(data, lstrlen(data));
         sprintf(data,"\n-Y %d +X %d\n",height,width);            myFile.Write(data, lstrlen(data));

         for (int y=0; y<height; y++)
            Write_Line(myFile,y);

         myFile.Close();
      }
   }
   return true;
}

bool CHDRImage::Create(int dx, int dy, double m_faktor)
{
   height=dy;
   width=dx;
   size=width*height;
   image=calloc(size,sizeof(pixel));
   faktor=m_faktor;

   pFloatImage = (float *)malloc(sizeof(float) * dx * dy * 3);

   pRGBEOrginal = (unsigned char *)malloc(sizeof(unsigned char) * dx * dy * 4);

   //Bitmap
   mdpx = dx;
   if (mdpx%4)
      mdpx += 4-mdpx%4;
   mdpy = dy;

   bmi.bmiHeader.biSize=sizeof(BITMAPINFOHEADER); 
   bmi.bmiHeader.biWidth=mdpx; 
   bmi.bmiHeader.biHeight=mdpy; 
   bmi.bmiHeader.biPlanes=1; 
   bmi.bmiHeader.biBitCount=24; 
   bmi.bmiHeader.biCompression=BI_RGB;
   bmi.bmiHeader.biSizeImage=NULL;
   bmi.bmiHeader.biXPelsPerMeter=100;
   bmi.bmiHeader.biYPelsPerMeter=100;
   bmi.bmiHeader.biClrUsed=0;
   bmi.bmiHeader.biClrImportant=0;

   //CreateDIBitmap
   //hBitmap = CreateDIBSection(m_pWnd->GetDC()->m_hDC,&bmi,DIB_RGB_COLORS,(void**)&pBuffer,NULL,NULL);

   return true;
}

void CHDRImage::SetPixel(int x, int y, double r, double g, double b)
{
   int ir,ig,ib;
   ir=(int)(r*faktor); if (ir<0) ir=0; if (ir>255) ir=255;
   ig=(int)(g*faktor); if (ig<0) ig=0; if (ig>255) ig=255;
   ib=(int)(b*faktor); if (ib<0) ib=0; if (ib>255) ib=255;
/*
   pBuffer[(x+y*mdpx)*3]  =(unsigned char)ir;
   pBuffer[(x+y*mdpx)*3+1]=(unsigned char)ig;
   pBuffer[(x+y*mdpx)*3+2]=(unsigned char)ib;
*/
   pixel * pPixel = (pixel*) image + (x + y * width);

   pPixel->r=r;
   pPixel->g=g;
   pPixel->b=b;

   float * pFloat = pFloatImage + (x + y * width) * 3;
   *pFloat++ = (float)r;
   *pFloat++ = (float)g;
   *pFloat   = (float)b;

   unsigned char * pOrg = pRGBEOrginal + (x + y * width) * 4;
   RGBE rgbe;
   pixel pix;
   pix.r = r;
   pix.g = g;
   pix.b = b;
   ColorToRGBE(&rgbe, &pix);
   *pOrg++ = rgbe.r;
   *pOrg++ = rgbe.g;
   *pOrg++ = rgbe.b;
   *pOrg   = rgbe.e;

#if 0
   float fr, fg, fb;
   unsigned char rgbe2[4];
   rgbe2[0] = rgbe.r;
   rgbe2[1] = rgbe.g;
   rgbe2[2] = rgbe.b;
   rgbe2[3] = rgbe.e;
   Rgbe2Float(rgbe2, fr, fg, fb);
   if ((fabs(r-fr)/r > 0.05) || (fabs(g-fg)/g > 0.05) || (fabs(b-fb)/b > 0.05))
	   assert(0);
#endif
}

void CHDRImage::GetPixel(int x, int y, double &r, double &g, double &b)
{
#ifdef _DEBUG
	if ((x<0) || (y<0) || (x>=width) || (y>=height))
		assert(0);
#endif
	pixel * pPixel = (pixel*) image + (x + y * width);

	r = pPixel->r;
	g = pPixel->g;
	b = pPixel->b;
}

void CHDRImage::GetPixel(int x, int y, float &r, float &g, float &b)
{
	//repeat texture
	while (x<0) x += width;
	while (x>=width) x -= width;
	while (y<0) y += height;
	while (y>=height) y -= height;

	float * pFloat = pFloatImage + (x + y * width) * 3;

	r = *pFloat++;
	g = *pFloat++;
	b = *pFloat;
}

void CHDRImage::GetPixelLevel(int x, int y, float &r, float &g, float &b, int level)
{
	if ((level < 0) || (level >= iBlurArraySize))
		assert(0);

	//repeat texture
	while (x<0) x += width;
	while (x>=width) x -= width;
	while (y<0) y += height;
	while (y>=height) y -= height;

	float fr = CV_IMAGE_ELEM(ppBlurArray[level], float, y, x * 3 + RED);
	float fg = CV_IMAGE_ELEM(ppBlurArray[level], float, y, x * 3 + GRE);
	float fb = CV_IMAGE_ELEM(ppBlurArray[level], float, y, x * 3 + BLU);

	r = fr;
	g = fg;
	b = fb;
}

void CHDRImage::GetPixelInterpolation(float x, float y, float &r, float &g, float &b)
{
	//repeat texture
	while (x<0) x += width;
	while (x>=width) x -= width;
	while (y<0) y += height;
	while (y>=height) y -= height;

	//Bilinear Interpolation
	float r00, g00, b00;
	float r01, g01, b01;
	float r10, g10, b10;
	float r11, g11, b11;

	GetPixel((int)floor(x), (int)floor(y), r00, g00, b00);
	GetPixel((int)floor(x), (int)ceil(y),  r01, g01, b01);
	GetPixel((int)ceil(x), (int)floor(y),  r10, g10, b10);
	GetPixel((int)ceil(x), (int)ceil(y),   r11, g11, b11);

	float wx = x - floor(x);
	float wy = y - floor(y);
	
	float rx0 = (1 - wx) * r00 + wx * r10;
	float gx0 = (1 - wx) * g00 + wx * g10;
	float bx0 = (1 - wx) * b00 + wx * b10;

	float rx1 = (1 - wx) * r01 + wx * r11;
	float gx1 = (1 - wx) * g01 + wx * g11;
	float bx1 = (1 - wx) * b01 + wx * b11;

	r = (1 - wy) * rx0 + wy * rx1;
	g = (1 - wy) * gx0 + wy * gx1;
	b = (1 - wy) * bx0 + wy * bx1;
}

void CHDRImage::GetPixelBlur(float gx, float gy, float &r, float &g, float &b, float fBlur)
{
	if (iBlurArraySize == 0)
		GenerateBlurArray();

	int level = (int)ceil( log((float)fBlur + 1) / log(2.0));

	float x = (gx / width) * ppBlurArray[level]->width;
	float y = (gy / height) * ppBlurArray[level]->height;
	
	//repeat texture
	while (x<0) x += ppBlurArray[level]->width;
	while (x>=ppBlurArray[level]->width) x -= ppBlurArray[level]->width;
	while (y<0) y += ppBlurArray[level]->height;
	while (y>=ppBlurArray[level]->height) y -= ppBlurArray[level]->height;

	//Bilinear Interpolation
	float r00, g00, b00;
	float r01, g01, b01;
	float r10, g10, b10;
	float r11, g11, b11;

	GetPixelLevel((int)floor(x), (int)floor(y), r00, g00, b00, level);
	GetPixelLevel((int)floor(x), (int)ceil(y),  r01, g01, b01, level);
	GetPixelLevel((int)ceil(x), (int)floor(y),  r10, g10, b10, level);
	GetPixelLevel((int)ceil(x), (int)ceil(y),   r11, g11, b11, level);

	float wx = x - floor(x);
	float wy = y - floor(y);

	float rx0 = (1 - wx) * r00 + wx * r10;
	float gx0 = (1 - wx) * g00 + wx * g10;
	float bx0 = (1 - wx) * b00 + wx * b10;

	float rx1 = (1 - wx) * r01 + wx * r11;
	float gx1 = (1 - wx) * g01 + wx * g11;
	float bx1 = (1 - wx) * b01 + wx * b11;

	r = (1 - wy) * rx0 + wy * rx1;
	g = (1 - wy) * gx0 + wy * gx1;
	b = (1 - wy) * bx0 + wy * bx1;

/*
	r = g = b = 0;
	float rn, gn, bn;

	if (fBlur > 5.f)
	{
		GetPixelBlur(x - fBlur, y - fBlur, rn, gn, bn, fBlur * 0.3333); r += rn * 1.0f/16.0f; g += gn * 1.0f/16.0f; b += bn * 1.0f/16.0f;
		GetPixelBlur(x - fBlur, y + fBlur, rn, gn, bn, fBlur * 0.3333); r += rn * 1.0f/16.0f; g += gn * 1.0f/16.0f; b += bn * 1.0f/16.0f;
		GetPixelBlur(x + fBlur, y - fBlur, rn, gn, bn, fBlur * 0.3333); r += rn * 1.0f/16.0f; g += gn * 1.0f/16.0f; b += bn * 1.0f/16.0f;
		GetPixelBlur(x + fBlur, y + fBlur, rn, gn, bn, fBlur * 0.3333); r += rn * 1.0f/16.0f; g += gn * 1.0f/16.0f; b += bn * 1.0f/16.0f;
		GetPixelBlur(x - fBlur, y        , rn, gn, bn, fBlur * 0.3333); r += rn * 2.0f/16.0f; g += gn * 2.0f/16.0f; b += bn * 2.0f/16.0f;
		GetPixelBlur(x + fBlur, y        , rn, gn, bn, fBlur * 0.3333); r += rn * 2.0f/16.0f; g += gn * 2.0f/16.0f; b += bn * 2.0f/16.0f;
		GetPixelBlur(x        , y - fBlur, rn, gn, bn, fBlur * 0.3333); r += rn * 2.0f/16.0f; g += gn * 2.0f/16.0f; b += bn * 2.0f/16.0f;
		GetPixelBlur(x        , y + fBlur, rn, gn, bn, fBlur * 0.3333); r += rn * 2.0f/16.0f; g += gn * 2.0f/16.0f; b += bn * 2.0f/16.0f;
		GetPixelBlur(x        , y        , rn, gn, bn, fBlur * 0.3333); r += rn * 4.0f/16.0f; g += gn * 4.0f/16.0f; b += bn * 4.0f/16.0f;
	}
	else
	{
		GetPixelInterpolation(x - fBlur, y - fBlur, rn, gn, bn); r += rn * 1.0f/16.0f; g += gn * 1.0f/16.0f; b += bn * 1.0f/16.0f;
		GetPixelInterpolation(x - fBlur, y + fBlur, rn, gn, bn); r += rn * 1.0f/16.0f; g += gn * 1.0f/16.0f; b += bn * 1.0f/16.0f;
		GetPixelInterpolation(x + fBlur, y - fBlur, rn, gn, bn); r += rn * 1.0f/16.0f; g += gn * 1.0f/16.0f; b += bn * 1.0f/16.0f;
		GetPixelInterpolation(x + fBlur, y + fBlur, rn, gn, bn); r += rn * 1.0f/16.0f; g += gn * 1.0f/16.0f; b += bn * 1.0f/16.0f;
		GetPixelInterpolation(x - fBlur, y        , rn, gn, bn); r += rn * 2.0f/16.0f; g += gn * 2.0f/16.0f; b += bn * 2.0f/16.0f;
		GetPixelInterpolation(x + fBlur, y        , rn, gn, bn); r += rn * 2.0f/16.0f; g += gn * 2.0f/16.0f; b += bn * 2.0f/16.0f;
		GetPixelInterpolation(x        , y - fBlur, rn, gn, bn); r += rn * 2.0f/16.0f; g += gn * 2.0f/16.0f; b += bn * 2.0f/16.0f;
		GetPixelInterpolation(x        , y + fBlur, rn, gn, bn); r += rn * 2.0f/16.0f; g += gn * 2.0f/16.0f; b += bn * 2.0f/16.0f;
		GetPixelInterpolation(x        , y        , rn, gn, bn); r += rn * 4.0f/16.0f; g += gn * 4.0f/16.0f; b += bn * 4.0f/16.0f;
	}
*/
}

void CHDRImage::GetPixel(float x, float y, float &r, float &g, float &b)
{
	if (fPixelBlur == 0.0f)
	{
		GetPixelInterpolation(x, y, r, g, b);
	}
	else
	{
		GetPixelBlur(x, y, r, g, b, fPixelBlur);
	}
}


inline void CHDRImage::PerformLightAdaption(float &rgb_r, float &rgb_g, float &rgb_b, const float lightadaption)
{
	float X, Y, Z, nx, ny, nY;
	ColorMath.RGBtoXYZ(rgb_r, rgb_g, rgb_b, X, Y, Z);
	ColorMath.XYZtoxyY(X, Y, Z, nx, ny, nY);

	nY = nY / (lightadaption + nY);
	//nY = log10(1.0f + nY)/log10(1.0f + lightadaption) * 1.0f/log10(2.0f + 8.0f * pow( nY / lightadaption, log10( 0.85f )/log10( 0.5f ) ));

	ColorMath.xyYtoXYZ(nx, ny, nY, X, Y, Z);
	ColorMath.XYZtoRGB(X, Y, Z, rgb_r, rgb_g, rgb_b);
}


IplImage * CHDRImage::GetIplImage(const float adaption)
{
	IplImage * pImage = cvCreateImage( cvSize(width, height), IPL_DEPTH_8U, 3); 

	for(int y = 0; y<height; ++y)
		for(int x = 0; x<width; ++x)
		{
			double dr, dg, db;
			GetPixel(x, y, dr, dg, db);
			float fr((float)(dr * faktor)), fg((float)(dg * faktor)), fb((float)(db * faktor));

			unsigned char & r = ((unsigned char *)pImage->imageData)[at_image(x, y, pImage) + BLU];
			unsigned char & g = ((unsigned char *)pImage->imageData)[at_image(x, y, pImage) + GRE];
			unsigned char & b = ((unsigned char *)pImage->imageData)[at_image(x, y, pImage) + RED];

			PerformLightAdaption(fr, fg, fb, adaption);
			fr = floor(fr * 255.0f + 0.5f);
			fg = floor(fg * 255.0f + 0.5f);
			fb = floor(fb * 255.0f + 0.5f);
			if (fr < 0.0f) fr = 0.0f; if (fr > 255.0f) fr = 255.0f;
			if (fg < 0.0f) fg = 0.0f; if (fg > 255.0f) fg = 255.0f;
			if (fb < 0.0f) fb = 0.0f; if (fb > 255.0f) fb = 255.0f;

			r = (unsigned char)fr;
			g = (unsigned char)fg;
			b = (unsigned char)fb;
		}

	return pImage;
}

/*
bool CHDRImage::SaveBitmap(LPCTSTR pszFile, int dpi) 
//void CMyBitmap::SaveBitmap(HWND hwnd, LPTSTR pszFile, PBITMAPINFO pbi, HBITMAP hBMP, HDC hDC) 
{ 
   CBitmap newBitmap;
   newBitmap.DeleteObject();
   newBitmap.Attach(hBitmap);

   HANDLE hf;                  // file handle 
   BITMAPFILEHEADER hdr;       // bitmap file-header 
   DWORD dwTotal;              // total count of bytes 
   DWORD cb;                   // incremental count of bytes 
   BYTE *hp;                   // byte pointer 
   DWORD dwTmp;
   
   const BITMAPINFOHEADER * const pbih = (const PBITMAPINFOHEADER) &bmi; 
   const size_t imageSize = pbih->biWidth*pbih->biHeight*3;
   LPBYTE lpBits = (LPBYTE) GlobalAlloc(GMEM_FIXED,(DWORD)imageSize);
   newBitmap.GetBitmapBits((DWORD)imageSize,lpBits);

   if (!lpBits) 
      return false; 

   // Create the .BMP file. 
   hf = CreateFile(pszFile,GENERIC_READ|GENERIC_WRITE,(DWORD)0,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,(HANDLE)NULL); 
   if (hf == INVALID_HANDLE_VALUE) 
      return false; 

   hdr.bfType = 0x4d42;        // 0x42 = "B" 0x4d = "M" 
   // Compute the size of the entire file. 
   hdr.bfSize = (DWORD) (sizeof(BITMAPFILEHEADER) + pbih->biSize + pbih->biSizeImage); 
   hdr.bfReserved1 = 0; 
   hdr.bfReserved2 = 0; 

   // Compute the offset to the array of color indices. 
   hdr.bfOffBits = (DWORD) sizeof(BITMAPFILEHEADER) + pbih->biSize; 

   // Copy the BITMAPFILEHEADER into the .BMP file. 
   if (!WriteFile(hf, (LPVOID) &hdr, sizeof(BITMAPFILEHEADER), (LPDWORD) &dwTmp,  NULL)) 
      return false; 

   // Copy the BITMAPINFOHEADER into the file. 
   if (!WriteFile(hf, (LPVOID) pbih, sizeof(BITMAPINFOHEADER), (LPDWORD) &dwTmp, NULL))
      return false; 

   // Copy the array of color indices into the .BMP file. 
   dwTotal = cb = (DWORD)imageSize; 
   hp = lpBits; 
   if (!WriteFile(hf, (LPSTR) hp, (int) cb, (LPDWORD) &dwTmp,NULL)) 
      return false; 

   // Close the .BMP file. 
   if (!CloseHandle(hf)) 
      return false; 

   // Free memory. 
   GlobalFree((HGLOBAL)lpBits);

   return true;
}
*/

/*************************************************************************************************************************************************/
//aus Blender: radiance_hdr.c:
/*************************************************************************************************************************************************/

typedef unsigned char RGBE[4];
typedef float fCOLOR[3];
#define RED 0
#define GRN 1
#define BLU 2
#define EXP 3
#define COLXS 128
#define copy_rgbe(c1, c2) (c2[RED]=c1[RED], c2[GRN]=c1[GRN], c2[BLU]=c1[BLU], c2[EXP]=c1[EXP])
#define copy_fcol(f1, f2) (f2[RED]=f1[RED], f2[GRN]=f1[GRN], f2[BLU]=f1[BLU])

/* read routines */
static unsigned char* oldreadcolrs(RGBE *scan, unsigned char *mem, int xmax)
{
	int i, rshift = 0, len = xmax;
	while (len > 0) {
		scan[0][RED] = *mem++;
		scan[0][GRN] = *mem++;
		scan[0][BLU] = *mem++;
		scan[0][EXP] = *mem++;
		if (scan[0][RED] == 1 && scan[0][GRN] == 1 && scan[0][BLU] == 1) {
			for (i=scan[0][EXP]<<rshift;i>0;i--) {
				copy_rgbe(scan[-1], scan[0]);
				scan++;
				len--;
			}
			rshift += 8;
		}
		else {
			scan++;
			len--;
			rshift = 0;
		}
	}
	return mem;
}

static unsigned char* freadcolrs(RGBE *scan, unsigned char* mem, int xmax)
{
	int i, j, code, val;

	if ((xmax < MINELEN) | (xmax > MAXELEN)) return oldreadcolrs(scan, mem, xmax);

	i = *mem++;
	if (i != 2) return oldreadcolrs(scan, mem-1, xmax);

	scan[0][GRN] = *mem++;
	scan[0][BLU] = *mem++;

	i = *mem++;
	if (((scan[0][BLU] << 8) | i) != xmax) return NULL;

	for (i=0;i<4;i++)
		for (j=0;j<xmax;) {
			code = *mem++;
			if (code > 128) {
				code &= 127;
				val = *mem++;
				while (code--)
					scan[j++][i] = (unsigned char)val;
			}
			else
				while (code--)
					scan[j++][i] = *mem++;
		}
		return mem;
}

/* helper functions */

/* rgbe -> float color */
static void RGBE2FLOAT(RGBE rgbe, fCOLOR fcol)
{
	if (rgbe[EXP]==0) {
		fcol[RED] = fcol[GRN] = fcol[BLU] = 0;
	}
	else {
		float f = ldexp(1.0f, rgbe[EXP]-(COLXS+8));
		fcol[RED] = f*(rgbe[RED] + 0.5f);
		fcol[GRN] = f*(rgbe[GRN] + 0.5f);
		fcol[BLU] = f*(rgbe[BLU] + 0.5f);
	}
}

/* float color -> rgbe */
static void FLOAT2RGBE(fCOLOR fcol, RGBE rgbe)
{
	int e;
	float d = (fcol[RED]>fcol[GRN]) ? fcol[RED] : fcol[GRN];
	if (fcol[BLU]>d) d = fcol[BLU];
	if (d <= 1e-32f)
		rgbe[RED] = rgbe[GRN] = rgbe[BLU] = rgbe[EXP] = 0;
	else {
		d = frexp(d, &e) * 256.f / d;
		rgbe[RED] = (unsigned char)(fcol[RED] * d);
		rgbe[GRN] = (unsigned char)(fcol[GRN] * d);
		rgbe[BLU] = (unsigned char)(fcol[BLU] * d);
		rgbe[EXP] = (unsigned char)(e + COLXS);
	}
}

void CHDRImage::Rgbe2Float(unsigned char * rgbe, float &r, float &g, float &b)
{
	unsigned char hdr[4];
	fCOLOR col;

	hdr[0] = *rgbe++;
	hdr[1] = *rgbe++;
	hdr[2] = *rgbe++;
	hdr[3] = *rgbe;

	RGBE2FLOAT(hdr, col);

	r = col[0];
	g = col[1];
	b = col[2];
}


void CHDRImage::ColorToRGBE(RGBE * clr, pixel * color)
{
	fCOLOR col;
	unsigned char hdr[4];

	col[0] = (float)color->r;
	col[1] = (float)color->g;
	col[2] = (float)color->b;

	FLOAT2RGBE(col, hdr);

	clr->r = hdr[0];
	clr->g = hdr[1];
	clr->b = hdr[2];
	clr->e = hdr[3];
/*
	double  d;
	int  e;

	d = max3(color);

	if (d <= 1e-32) 
	{
		clr->r=clr->g=clr->b=clr->e=0;
		return;
	}

	d = frexp(d, &e) * 255.9999 / d;

	clr->r = (unsigned char) (color->r * d);
	clr->g = (unsigned char) (color->g * d);
	clr->b = (unsigned char) (color->b * d);
	clr->e = (unsigned char) (e + 128);
*/
}

/* ImBuf read */

int imb_is_a_hdr(void *buf)
{
	/* For recognition, Blender only loades first 32 bytes, so use #?RADIANCE id instead */
	if (strstr((char*)buf, "#?RADIANCE")) return 1;
	// if (strstr((char*)buf, "32-bit_rle_rgbe")) return 1;
	return 0;
}

void imb_loadhdr(unsigned char *mem, int size, CHDRImage * pHDR)
{
	RGBE* sline;
	fCOLOR fcol;
	double* rect_double;
	float* rect_float;
	unsigned char *rect_org;
	int found=0;
	int width=0, height=0;
	int x, y;
	//int ir, ig, ib;
	unsigned char* ptr;
	//unsigned char* rect;
	char oriY[80], oriX[80];

	if (imb_is_a_hdr((void*)mem))
	{
		/* find empty line, next line is resolution info */
		for (x=1;x<size;x++) {
			if ((mem[x-1]=='\n') && (mem[x]=='\n')) {
				found = 1;
				break;
			}
		}
		if (found) {
			sscanf((char*)&mem[x+1], "%s %d %s %d", (char*)&oriY, &height, (char*)&oriX, &width);

			/* find end of this line, data right behind it */
			ptr = (unsigned char *)strchr((char*)&mem[x+1], '\n');
			ptr++;

/*
			ibuf = IMB_allocImBuf(width, height, 32, 0, 0);

			if (ibuf==NULL) return NULL;
			ibuf->ftype = RADHDR;
			ibuf->xorig = ibuf->yorig = 0;

			if (flags & IB_test) return ibuf;
*/
			//Bild anlegen
			pHDR->Create(width, height, 1.0);

			/* read in and decode the actual data */
			sline = (RGBE*)malloc(sizeof(RGBE)*width);
			//rect = (unsigned char*)ibuf->rect;
			//rect_float = (float *)ibuf->rect_float;
			//rect = pHDR->pBuffer;
			rect_double = (double *)pHDR->image;
			rect_float = (float *)pHDR->pFloatImage;
			rect_org = (unsigned char *)pHDR->pRGBEOrginal;

			for (y=0;y<height;y++) {
				ptr = freadcolrs(sline, ptr, width);
				if (ptr==NULL) {
					printf("HDR decode error\n");
					free(sline);
					return;
				}
				for (x=0;x<width;x++) {
					/* convert to ldr */
					RGBE2FLOAT(sline[x], fcol);
/*
					*rect_float++ = fcol[RED];
					*rect_float++ = fcol[GRN];
					*rect_float++ = fcol[BLU];
					*rect_float++ = 1.0f;
*/
					*rect_double++ = fcol[RED];
					*rect_double++ = fcol[GRN];
					*rect_double++ = fcol[BLU];

					*rect_float++ = fcol[RED];
					*rect_float++ = fcol[GRN];
					*rect_float++ = fcol[BLU];

					*rect_org++ = sline[x][0];
					*rect_org++ = sline[x][1];
					*rect_org++ = sline[x][2];
					*rect_org++ = sline[x][3];

					/* Also old oldstyle for the rest of blender which is not using floats yet */
					/* very weird mapping! (ton) */
					fcol[RED] = 1.f-exp(fcol[RED]*-1.414213562f);
					fcol[GRN] = 1.f-exp(fcol[GRN]*-1.414213562f);
					fcol[BLU] = 1.f-exp(fcol[BLU]*-1.414213562f);
/*
					ir = (int)(255.f*pow(fcol[RED], 0.45454545f));
					ig = (int)(255.f*pow(fcol[GRN], 0.45454545f));
					ib = (int)(255.f*pow(fcol[BLU], 0.45454545f));
					*rect++ = (unsigned char)((ir<0) ? 0 : ((ir>255) ? 255 : ir));
					*rect++ = (unsigned char)((ig<0) ? 0 : ((ig>255) ? 255 : ig));
					*rect++ = (unsigned char)((ib<0) ? 0 : ((ib>255) ? 255 : ib));
					//*rect++ = 255;
*/
				}
			}
			free(sline);
			//if (oriY[0]=='-') IMB_flipy(ibuf); //TODO
			return;
		}
		//else printf("Data not found!\n");
	}
	//else printf("Not a valid radiance HDR file!\n");

	return;
}

bool CHDRImage::LoadHDRImage(CString filename)
{
	unsigned char *data;
	size_t size(0);

	HANDLE hFile = CreateFile(_T(filename),
		GENERIC_READ, FILE_SHARE_READ,
		NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	if (hFile == INVALID_HANDLE_VALUE)
		//AfxMessageBox(_T("Couldn't read the HDR-file!"));
		return false;
	else
	{
		CFile myFile(hFile);

		size = (size_t)myFile.GetLength();
		data = (unsigned char *)malloc(sizeof(unsigned char) * size);
		myFile.Read(data, size);

		myFile.Close();

		imb_loadhdr(data, size, this);

		free(data);
	}
	return true;
}


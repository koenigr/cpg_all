#pragma once

#include "stdafx.h"
#include <opencv\cv.h>

class CHDRImage
{
public:

   struct pixel
   {
      double r,g,b; //1.0|1.0|1.0 = wei�;  0.0|0.0|0.0 = schwarz
   };
   struct RGBE
   {
      unsigned char r,g,b,e;
   };

   //CHDRImage(CWnd *pWnd);
   CHDRImage();
   ~CHDRImage(void);


   //meins ...
   bool Create(int dx, int dy, double m_faktor);
   bool SaveBitmap(LPCTSTR pszFile, int dpi=72); 
   bool ExportHDRImage(CString filename);
   void SetPixel(int x, int y, double r, double g, double b);
   double max3(pixel * color);
   void ColorToRGBE(RGBE * clr, pixel * color);
   void Write_Line(CFile & myFile, int y);
   bool LoadHDRImage(CString filename);
   void GetPixel(int x, int y, double &r, double &g, double &b);
   void GetPixel(int x, int y, float &r, float &g, float &b);
   void GetPixel(float x, float y, float &r, float &g, float &b);
   void GetPixelInterpolation(float x, float y, float &r, float &g, float &b);
   void GetPixelBlur(float gx, float gy, float &r, float &g, float &b, float fBlur);
   void GetPixelLevel(int x, int y, float &r, float &g, float &b, int level);
   IplImage * GetIplImage(const float adaption);
   void PerformLightAdaption(float &rgb_r, float &rgb_g, float &rgb_b, const float lightadaption);
   void GenerateBlurArray();
   void Rgbe2Float(unsigned char * rgbe, float &r, float &g, float &b);

   void * image;
   BITMAPINFO bmi;
   int height, width, size;
   unsigned int mdpx,mdpy;
   //unsigned char * pBuffer;
   //HBITMAP hBitmap;
   double faktor; //bestimmt Verh�ltniss zwischen RGB und double Ausgabewerten
   float * pFloatImage;
   unsigned char *pRGBEOrginal;
   //CWnd * m_pWnd;
   
   float fPixelBlur;
   IplImage **ppBlurArray;
   int iBlurArraySize;
};

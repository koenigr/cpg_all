#include "stdafx.h"
#include "Geom3d.h"
#include "OpenGLContext.h"
#include "FramebufferObject.h"
#include "DirectSunLight.h"

namespace ISOVIST3D
{
	class CIsovist3d
	{
	public:
		struct PolygonRenderInfo
		{
			DSL::CGeom3d::TriangleVector m_vTriangles;
			DSL::CGeom3d::Vec3d m_normale;
			PolygonRenderInfo(DSL::CGeom3d::TriangleVector &triangles, DSL::CGeom3d::Vec3d &normale): m_vTriangles(triangles), m_normale( normale ) {};
		};
		typedef std::map<size_t, PolygonRenderInfo*> GeometryMap;
		typedef std::pair<size_t, PolygonRenderInfo*> GeometryPair;

	private:
		COpenGLContext * m_pContext;	// the Render Context
		AS::FramebufferObject * m_pFBO;	// the FBO
		unsigned char * m_pReadBuffer;	// the FBO read buffer
		size_t m_iCubeSize;				// render cube size
		GeometryMap m_vGeometryMap;     // id Triangles
		bool m_bGeometryChanged;        // flag: Geometrie �nderungen wurden angewendet
		DSL::DisplayListVector m_vDisplayList; // DisplayListe
		float * m_pWichtung;            // Wichtungsmap
		size_t m_iCurrentPolygonID;     // letzte verwendete PolygonID f�r GetNewPolygonID

		unsigned char * GetFBOPixel(int x, int y) const;	// reads a pixel from m_pReadBuffer (slow)
		size_t GetNewPolygonID();					// liefert neue Poly ID f�r m_vGeometryMap
		void BuildGeometryDisplayList();            // baut und zeigt m_vGeometryMap als Displayliste
		void ShowGeometryDisplayList();             // zeigt m_vGeometryMap als Displayliste
		float * GenerateFormfactorMap(int cube_size); // baut Wichtungsmap des Rendercube
		void RenderIsovist3d(int iOptiPosX, int iOptiPosY, Vec3d position);  // Berechungskern Teil 1

	public:

		CIsovist3d();
		~CIsovist3d();
		void SetResolution(int cubeSize);																			// init the OpenGL Context in this resolution
		void CalculateIsovist3d(Vec3d position);																	// calculate the isovist3d
		size_t GetFBOPixelId(int x, int y);                                                                         // returns the FBO ID at that pixel
		Vec3d GetFBOPixelRay(int x, int y);                                                                         // returns the FBO ray at that pixel

		void DeleteAllGeometry();																					// l�scht alle Geometrie
		size_t AddPolygon(DSL::CGeom3d::Point_3d_Vector_Vector &points);     										// f�gt ein Polygon hinzu und liefert seine ID
		size_t AddPolygonTriangles(DSL::CGeom3d::Vec3d& normale, DSL::CGeom3d::TriangleVector &triangles);			// f�gt ein Polygon hinzu und liefert seine ID (schneller ?)
		void DeletePolygon(size_t id);																				// l�scht ein Polygon
		bool MovePolygon(size_t id, DSL::CGeom3d::Point_3d_Vector_Vector &newPoints);								// verschiebt ein Polygon
	};
}
#ifndef _DSL_MYMATH_H
#define _DSL_MYMATH_H


#include <math.h>


#ifdef PI
#undef PI
#endif

const long double DSL_PI = 3.1415926535897932384626433832795;
//#endif


// Berechet a^2
template <class T> T sqr (const T & a)
{
   return a*a;
}


// Berechnet signum(a) (Vorzeichen von a)
template <class T> T sgn (const T & a)
{
   return a<0 ? (T)-1:(T)1;
}


template <class T> struct TemplateVec3d
{
   typedef T TYPE_VALUE;

   T x,y,z;

   bool operator == (const TemplateVec3d & b)
   {
      return x==b.x && y==b.y && z==b.z;
   }


   bool operator != (const TemplateVec3d & b)
   {
      return x!=b.x || y!=b.y || z!=b.z;
   }


   TemplateVec3d ()
   { x=0; y=0; z=0;}


   TemplateVec3d (const T & xt, const T & yt, const T & zt)
   { x=xt; y=yt; z=zt;}


   T Value2 () const
   {
	   return sqr(x)+sqr(y)+sqr(z);
   }


   T Value () const
   {
	   return sqrtl(Value2());
   }


   int IsZero () const
   { return !(x||y||z); }


   bool Normalize()
   {
      T s=Value();
      if (!s)
         return false;

      operator /=(s);
      return true;
   }


  TemplateVec3d operator + (const TemplateVec3d & b) const
  {
     return TemplateVec3d (x+b.x,y+b.y,z+b.z);
  }


  TemplateVec3d operator - (const TemplateVec3d & b) const
  {
     return TemplateVec3d (x-b.x,y-b.y,z-b.z);
  }


  TemplateVec3d operator += (const TemplateVec3d & b)
  {
     x += b.x;
     y += b.y;
	  z += b.z;

	  return TemplateVec3d (x,y,z);
  }


  TemplateVec3d operator -= (const TemplateVec3d & b)
  {
     x -= b.x;
	  y -= b.y;
	  z -= b.z;

	  return TemplateVec3d (x,y,z);
  }


  TemplateVec3d operator / (const T & b) const
  {
     return TemplateVec3d (x/b,y/b,z/b);
  }


  TemplateVec3d operator * (const T & b) const
  {
     return TemplateVec3d (x*b,y*b,z*b);
  }


  TemplateVec3d operator /= (const TemplateVec3d & b)
  {
     x /= b.x;
	  y /= b.y;
	  z /= b.z;

	  return TemplateVec3d (x,y,z);
  }


  TemplateVec3d operator /= (const T & b)
  {
     x /= b;
	  y /= b;
	  z /= b;

	  return TemplateVec3d (x,y,z);
  }

  TemplateVec3d operator *= (const TemplateVec3d & b)
  {
     x *= b.x;
	  y *= b.y;
	  z *= b.z;

	  return TemplateVec3d (x,y,z);
  }


  T imul (const TemplateVec3d & b) const
  {
     return x*b.x + y*b.y + z*b.z;
  }


  TemplateVec3d amul (const TemplateVec3d & b) const
  {
     return TemplateVec3d (y*b.z-z*b.y,z*b.x-x*b.z,x*b.y-y*b.x);
  }
};


template <class T> TemplateVec3d<T> operator * (const T & a, const TemplateVec3d<T> & b)
{
	return b*a;
}


template <class T> T imul (const TemplateVec3d<T> & a, const TemplateVec3d<T> & b)
{
   return a.imul(b);
}


template <class T> TemplateVec3d<T> amul (const TemplateVec3d<T> & a, const TemplateVec3d<T> & b)
{
   return a.amul(b);
}


template <class T> T cos (const TemplateVec3d<T> & a, const TemplateVec3d<T> & b)
{
   T erg = imul(a,b)/sqrt(a.Value2()*b.Value2());
   if (erg > (T) 1.0) erg = (T) 1.0;
   if (erg < (T)-1.0) erg = (T)-1.0;
   return erg;
}


template <class T> T sin (const TemplateVec3d<T> & a, const TemplateVec3d<T> & b)
{
   T erg = sqrt((sqr(a.x*b.y-a.y*b.x)+sqr(a.y*b.z-a.z*b.y)+sqr(a.z*b.x-a.x*b.z))/(a.Value2()*b.Value2()));
   if (erg > (T) 1.0) erg = (T) 1.0;
   if (erg < (T)-1.0) erg = (T)-1.0;
   return erg;
}


typedef TemplateVec3d<double> Vec3d;


template <class T> T det (const TemplateVec3d<TemplateVec3d<T> > & m)
{
   return m.x.x*(m.y.y*m.z.z-m.y.z*m.z.y)-m.x.y*(m.y.x*m.z.z-m.y.z*m.z.x)+m.x.z*(m.y.x*m.z.y-m.y.y*m.z.x);
}


template <class T> TemplateVec3d<T> Cramer (const TemplateVec3d<T> & v0, const TemplateVec3d<T> & v1, const TemplateVec3d<T> & v2, const TemplateVec3d<T> & t)
{
   typedef TemplateVec3d<T>          Value;
   typedef TemplateVec3d<TemplateVec3d<T> > Matrix;

   Value tmp;
   T d = det(Matrix(v0,v1,v2));

   tmp.x=det(Matrix(t,v1,v2))/d;
   tmp.y=det(Matrix(v0,t,v2))/d;
   tmp.z=det(Matrix(v0,v1,t))/d;

   return tmp;
}


#endif
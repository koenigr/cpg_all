#include "stdafx.h"
#include "OpenCL.h"
#include <assert.h>

using namespace DSL;

#ifdef _DEBUG
inline void CheckOpenCLError(cl_int clerr)
{
	if (clerr == CL_SUCCESS)
		return;
	else
	{
		int stop = 0;
		switch (clerr)
		{
		case CL_INVALID_PLATFORM:              stop = 1;      assert(0); break;
		case CL_INVALID_VALUE:                 stop = 2;      assert(0); break;
		case CL_DEVICE_NOT_AVAILABLE:          stop = 3;      assert(0); break;
		case CL_DEVICE_NOT_FOUND:              stop = 4;      assert(0); break;
		case CL_OUT_OF_HOST_MEMORY:            stop = 5;      assert(0); break;
		case CL_INVALID_DEVICE_TYPE:           stop = 6;      assert(0); break;
		case CL_INVALID_PROGRAM_EXECUTABLE:    stop = 7;      assert(0); break;
		case CL_INVALID_COMMAND_QUEUE:         stop = 8;      assert(0); break;
		case CL_INVALID_KERNEL:                stop = 9;      assert(0); break;
		case CL_INVALID_CONTEXT:               stop = 10;     assert(0); break;
		case CL_INVALID_KERNEL_ARGS:           stop = 11;     assert(0); break;
		case CL_INVALID_WORK_DIMENSION:        stop = 12;     assert(0); break;
		case CL_INVALID_WORK_GROUP_SIZE:       stop = 13;     assert(0); break;
		case CL_INVALID_WORK_ITEM_SIZE:        stop = 14;     assert(0); break;
		case CL_INVALID_GLOBAL_OFFSET:         stop = 15;     assert(0); break;
		case CL_OUT_OF_RESOURCES:              stop = 16;     assert(0); break;
		case CL_MEM_OBJECT_ALLOCATION_FAILURE: stop = 17;     assert(0); break;
		case CL_INVALID_EVENT_WAIT_LIST:       stop = 18;     assert(0); break;
		case CL_INVALID_PROGRAM:               stop = 19;     assert(0); break;
		case CL_INVALID_DEVICE:                stop = 20;     assert(0); break;
		case CL_INVALID_BINARY:                stop = 21;     assert(0); break;
		case CL_INVALID_BUILD_OPTIONS:         stop = 22;     assert(0); break;
		case CL_INVALID_OPERATION:             stop = 23;     assert(0); break;
		case CL_COMPILER_NOT_AVAILABLE:        stop = 24;     assert(0); break;
		case CL_BUILD_PROGRAM_FAILURE:         stop = 25;     assert(0); break;
		case CL_INVALID_ARG_INDEX:             stop = 27;     assert(0); break; //if arg_index is not a valid argument index.
		case CL_INVALID_ARG_VALUE:             stop = 28;     assert(0); break; //if arg_value specified is NULL for an argument that is not declared with the __local qualifier or vice-versa.
		case CL_INVALID_MEM_OBJECT:            stop = 29;     assert(0); break; //for an argument declared to be a memory object when the specified arg_value is not a valid memory object.
		case CL_INVALID_SAMPLER:               stop = 30;     assert(0); break; //for an argument declared to be of type sampler_t when the specified arg_value is not a valid sampler object.
		case CL_INVALID_ARG_SIZE:              stop = 31;     assert(0); break; //if arg_size does not match the size of the data type for an argument that is not a memory object or if the argument is a memory object and arg_size != sizeof(cl_mem) or if arg_size is zero and the argument is declared with the __local qualifier or if the argument is a sampler and arg_size != sizeof(cl_sampler)
		default:                               stop = 9999;   assert(0); break;
		}
	}
}
#else
inline void CheckOpenCLError(cl_int clerr) 
{
#ifdef _DEBUG_CONSOLE
	if (clerr == CL_SUCCESS)
		return;
	else
	{
		switch (clerr)
		{
		case CL_INVALID_PLATFORM:              printf("CL_INVALID_PLATFORM\n");                   break;
		case CL_INVALID_VALUE:                 printf("CL_INVALID_VALUE\n");                      break;
		case CL_DEVICE_NOT_AVAILABLE:          printf("CL_DEVICE_NOT_AVAILABLE\n");               break;
		case CL_DEVICE_NOT_FOUND:              printf("CL_DEVICE_NOT_FOUND\n");                   break;
		case CL_OUT_OF_HOST_MEMORY:            printf("CL_OUT_OF_HOST_MEMORY\n");                 break;
		case CL_INVALID_DEVICE_TYPE:           printf("CL_INVALID_DEVICE_TYPE\n");                break;
		case CL_INVALID_PROGRAM_EXECUTABLE:    printf("CL_INVALID_PROGRAM_EXECUTABLE\n");         break;
		case CL_INVALID_COMMAND_QUEUE:         printf("CL_INVALID_COMMAND_QUEUE\n");              break;
		case CL_INVALID_KERNEL:                printf("CL_INVALID_KERNEL\n");                     break;
		case CL_INVALID_CONTEXT:               printf("CL_INVALID_CONTEXT\n");                    break;
		case CL_INVALID_KERNEL_ARGS:           printf("CL_INVALID_KERNEL_ARGS\n");                break;
		case CL_INVALID_WORK_DIMENSION:        printf("CL_INVALID_WORK_DIMENSION\n");             break;
		case CL_INVALID_WORK_GROUP_SIZE:       printf("CL_INVALID_WORK_GROUP_SIZE\n");            break;
		case CL_INVALID_WORK_ITEM_SIZE:        printf("CL_INVALID_WORK_ITEM_SIZE\n");             break;
		case CL_INVALID_GLOBAL_OFFSET:         printf("CL_INVALID_GLOBAL_OFFSET\n");              break;
		case CL_OUT_OF_RESOURCES:              printf("CL_OUT_OF_RESOURCES\n");                   break;
		case CL_MEM_OBJECT_ALLOCATION_FAILURE: printf("CL_MEM_OBJECT_ALLOCATION_FAILURE\n");      break;
		case CL_INVALID_EVENT_WAIT_LIST:       printf("CL_INVALID_EVENT_WAIT_LIST\n");            break;
		case CL_INVALID_PROGRAM:               printf("CL_INVALID_PROGRAM\n");                    break;
		case CL_INVALID_DEVICE:                printf("CL_INVALID_DEVICE\n");                     break;
		case CL_INVALID_BINARY:                printf("CL_INVALID_BINARY\n");                     break;
		case CL_INVALID_BUILD_OPTIONS:         printf("CL_INVALID_BUILD_OPTIONS\n");              break;
		case CL_INVALID_OPERATION:             printf("CL_INVALID_OPERATION\n");                  break;
		case CL_COMPILER_NOT_AVAILABLE:        printf("CL_COMPILER_NOT_AVAILABLE\n");             break;
		case CL_BUILD_PROGRAM_FAILURE:         printf("CL_BUILD_PROGRAM_FAILURE\n");              break;
		case CL_INVALID_ARG_INDEX:             printf("CL_INVALID_ARG_INDEX\n");                  break; //if arg_index is not a valid argument index.
		case CL_INVALID_ARG_VALUE:             printf("CL_INVALID_ARG_VALUE\n");                  break; //if arg_value specified is NULL for an argument that is not declared with the __local qualifier or vice-versa.
		case CL_INVALID_MEM_OBJECT:            printf("CL_INVALID_MEM_OBJECT\n");                 break; //for an argument declared to be a memory object when the specified arg_value is not a valid memory object.
		case CL_INVALID_SAMPLER:               printf("CL_INVALID_SAMPLER\n");                    break; //for an argument declared to be of type sampler_t when the specified arg_value is not a valid sampler object.
		case CL_INVALID_ARG_SIZE:              printf("CL_INVALID_ARG_SIZE\n");                   break; //if arg_size does not match the size of the data type for an argument that is not a memory object or if the argument is a memory object and arg_size != sizeof(cl_mem) or if arg_size is zero and the argument is declared with the __local qualifier or if the argument is a sampler and arg_size != sizeof(cl_sampler)
		default:                               printf("CL_UNKNOWN_ERROR %d\n", clerr);            break;
		}
	}
#endif
}
#endif


void OpenCL::InitOpenCL()
{
#ifdef _TIME_DEBUG
	Timer TotalTimer("InitOpenCL()"), *pTimer = new Timer("OpenCL - Context und Queue erstellen");
#endif

	cl_int clerr = CL_SUCCESS;

	cl_uint num_platforms;
	cl_platform_id platform_pick;
	m_platforms = NULL;
	clerr = clGetPlatformIDs(0, NULL, &num_platforms);
	CheckOpenCLError(clerr);
	if (num_platforms > 0)
	{
		m_platforms = (cl_platform_id *)malloc(sizeof(cl_platform_id) * num_platforms);
		clerr = clGetPlatformIDs(num_platforms, m_platforms, NULL);
		CheckOpenCLError(clerr);
		platform_pick = m_platforms[GetPlatformPickId()];
	}
	else
		assert(0);

	cl_context_properties props[7];
	props[0] = (cl_context_properties)CL_GL_CONTEXT_KHR;
	props[1] = (cl_context_properties)wglGetCurrentContext();
	props[2] = (cl_context_properties)CL_WGL_HDC_KHR;
	props[3] = (cl_context_properties)wglGetCurrentDC();
	props[4] = (cl_context_properties)CL_CONTEXT_PLATFORM;   // indicates that next element is platform
	props[5] = (cl_context_properties)platform_pick;         // platform_pick is of type cl_platform_id
	props[6] = (cl_context_properties)0;                     // last element must be 0

	m_clctx = new cl_context();
	*m_clctx = clCreateContextFromType(props, CL_DEVICE_TYPE_ALL, NULL, NULL, &clerr);
	CheckOpenCLError(clerr);

	size_t paramz;
	clerr = clGetContextInfo(*m_clctx, CL_CONTEXT_DEVICES, 0, NULL, &paramz);
	CheckOpenCLError(clerr);

	cl_device_id * cldevs = (cl_device_id *)malloc(paramz);
	clerr = clGetContextInfo(*m_clctx, CL_CONTEXT_DEVICES, paramz, cldevs, NULL);
	CheckOpenCLError(clerr);

	m_clcmdq = new cl_command_queue();
	*m_clcmdq = clCreateCommandQueue(*m_clctx, cldevs[0], 0, &clerr);
	CheckOpenCLError(clerr);

	//Gr�ssen definieren
	m_itemsize_x = 8;
	m_itemsize_y = 8;

#ifdef _TIME_DEBUG
	delete pTimer;
	pTimer = new Timer("OpenCL - Programm und Kernel erstellen");
#endif

	//Programm und Kernel erstellen
	const char * source_code = 
/*
		"#pragma opencl cl_khr_byte_addressable_store\n"
		"\n"
		"__kernel void sum_light(__global float *pErg, __global float *pImage, __global float *pWichtung, unsigned int iDX, unsigned int iDY, __local float * pSharedImage)\n"
		"{\n" 
		"	unsigned int lid = get_local_id(1) * get_local_size(0) + get_local_id(0);\n"
		"	unsigned int gindex = get_global_id(1) * iDX * 3 + get_global_id(0);\n"
		"	\n"
		"	//Load the image part into pSharedImage \n"
		"	if ((get_global_id(0) < iDX) || (get_global_id(1) < iDY))\n"
		"	{\n"
		"		pSharedImage[ lid ] = pImage[ (gindex / 3) * 4 + gindex % 3 ] * pWichtung[ gindex / 3 ];\n"
		"	}\n"
		"	else\n"
		"	{\n"
		"		pSharedImage[ lid ] = 0.0;\n"
		"	}\n"
		"	\n"
		"	// sum pSharedImage \n"
		"	//const unsigned int BLOCKSIZE = get_local_size(0) * get_local_size(1); / /wird bei clBuildProgram �bergeben \n"
		"	for(unsigned int stride = 1; stride < BLOCKSIZE; stride *= 2)\n"
		"	{\n"
		"		unsigned int index = 2 * stride * 3 * (lid / 3) + (lid % 3); \n"
		"		\n"
		"		barrier(CLK_LOCAL_MEM_FENCE);\n"
		"		\n"
		"		if (index < BLOCKSIZE)\n"
		"		{\n"
		"			pSharedImage[ index ] += pSharedImage[ index + stride * 3 ];\n"
		"		}\n"
		"	}\n"
		"	\n"
		"	if (lid < 3)\n"
		"	{\n"
		"		unsigned int bid = get_group_id(1) * get_num_groups(0) + get_group_id(0);\n"
		"		pErg[ bid * 3 + lid ] = pSharedImage[ lid ];\n"
		"	}\n"
		"}\n"; 
*/
/*
		"#pragma opencl cl_khr_byte_addressable_store\n"
		"\n"
		"__kernel void sum_light(__global float *pErg, __global float *pImage, __global float *pWichtung, unsigned int iDX, unsigned int iDY, __local float * pSharedImage)\n"
		"{\n" 
		"	unsigned int lid = get_local_id(1) * get_local_size(0) + get_local_id(0);\n"
		"	unsigned int gindex = get_global_id(1) * iDX + get_global_id(0);\n"
		"	\n"
		"	//Load the image part into pSharedImage \n"
		"	float fWichtung = pWichtung[ gindex ];\n"
		"	if ((get_global_id(0) < iDX) || (get_global_id(1) < iDY))\n"
		"	{\n"
		"		pSharedImage[ lid * 3 + 0 ] = pImage[ gindex * 4 + 0 ] * fWichtung;\n"
		"		pSharedImage[ lid * 3 + 1 ] = pImage[ gindex * 4 + 1 ] * fWichtung;\n"
		"		pSharedImage[ lid * 3 + 2 ] = pImage[ gindex * 4 + 2 ] * fWichtung;\n"
		"	}\n"
		"	else\n"
		"	{\n"
		"		pSharedImage[ lid * 3 + 0 ] = 0.0;\n"
		"		pSharedImage[ lid * 3 + 1 ] = 0.0;\n"
		"		pSharedImage[ lid * 3 + 2 ] = 0.0;\n"
		"	}\n"
		"	\n"
		"	// sum pSharedImage \n"
		"	//const unsigned int BLOCKSIZE = get_local_size(0) * get_local_size(1); / /wird bei clBuildProgram �bergeben \n"
		"	for(unsigned int stride = 1; stride < BLOCKSIZE; stride *= 2)\n"
		"	{\n"
		"		unsigned int index = 2 * stride * lid;\n"
		"		//unsigned int index2 = index + stride;\n"
		"		\n"
		"		barrier(CLK_LOCAL_MEM_FENCE);\n"
		"		\n"
		"		if (index < BLOCKSIZE)\n"
		"		{\n"
		"			pSharedImage[ index * 3 + 0 ] += pSharedImage[ (index + stride) * 3 + 0 ];\n"
		"			pSharedImage[ index * 3 + 1 ] += pSharedImage[ (index + stride) * 3 + 1 ];\n"
		"			pSharedImage[ index * 3 + 2 ] += pSharedImage[ (index + stride) * 3 + 2 ];\n"
		"		}\n"
		"	}\n"
		"	\n"
		"	if (lid == 0)\n"
		"	{\n"
		"		unsigned int bid = get_group_id(1) * get_num_groups(0) + get_group_id(0);\n"
		"		pErg[ bid * 3 + 0 ] = pSharedImage[ 0 ];\n"
		"		pErg[ bid * 3 + 1 ] = pSharedImage[ 1 ];\n"
		"		pErg[ bid * 3 + 2 ] = pSharedImage[ 2 ];\n"
		"	}\n"
		"}\n"; 
*/
		"#pragma opencl cl_khr_byte_addressable_store\n"
		"\n"
#ifdef _DEBUG_DEEP_OPENCL
		"__kernel void sum_light(__global float *pErg, __global float *pImage, __global float *pWichtung, unsigned int iDX, unsigned int iDY, __local float * pSharedImage, unsigned int iCubeSize, int iUseNormale, float fNormaleX, float fNormaleY, float fNormaleZ, __global float *pDebugImage)\n"
#else
//		"__kernel void sum_light(__global float *pErg, __global float *pImage, __global float *pWichtung, unsigned int iDX, unsigned int iDY, __local float * pSharedImage, unsigned int iCubeSize, int iUseNormale, float fNormaleX, float fNormaleY, float fNormaleZ)\n"
        "__kernel void sum_light(__global float *pErg, __global float *pImage, __global float *pWichtung, unsigned int iDX, unsigned int iDY, __local float * pSharedImage, unsigned int iCubeSize, int iUseNormale)\n"
#endif
		"{\n" 
		"	unsigned int lid = get_local_id(1) * get_local_size(0) + get_local_id(0);\n"
		"	\n"
		"	//Load the image part into pSharedImage \n"
		"	if ((get_global_id(0) < iDX) || (get_global_id(1) < iDY))\n"
		"	{\n"
		"		unsigned int cx = get_global_id(0) % iCubeSize;\n"
		"		unsigned int cy = get_global_id(1) % iCubeSize;\n"
		"		float fWichtung = pWichtung[ cy * iCubeSize + cx ];\n"
		"		if (iUseNormale != 0)\n"
		"		{\n"
		"			unsigned int iImageId = (get_global_id(0) / iCubeSize) % 6;\n"
		"			//float4 faceNormal = (float4)(fNormaleX, fNormaleY, fNormaleZ, 0.0);\n"
		"			float4 pixelNormal = (float4)((((0.5 + cx) / iCubeSize - 0.5) * 2.0), (((0.5 + cy) / iCubeSize - 0.5) * 2.0), -1.0, 0.0);\n"
		"           float pixelValue = sqrt(pixelNormal.x * pixelNormal.x + pixelNormal.y * pixelNormal.y + pixelNormal.z * pixelNormal.z);\n"
		"			pixelNormal = (float4)(pixelNormal.x / pixelValue, pixelNormal.y / pixelValue, pixelNormal.z / pixelValue, 0.0);\n"
		"			switch (iImageId)\n"
		"			{\n"
		"			case 0: // front\n"
		"				break;\n"
		"			case 1: // right\n"
		"				pixelNormal = (float4)(-pixelNormal.z,  pixelNormal.y,  pixelNormal.x, 0.0);\n"
		"				break;\n"
		"			case 2: // back\n"
		"				pixelNormal = (float4)(-pixelNormal.x,  pixelNormal.y, -pixelNormal.z, 0.0);\n"
		"				break;\n"
		"			case 3: // left\n"
		"				pixelNormal = (float4)( pixelNormal.z,  pixelNormal.y, -pixelNormal.x, 0.0);\n"
		"				break;\n"
		"			case 4: // bottom\n"
		"				pixelNormal = (float4)( pixelNormal.x,  pixelNormal.z, -pixelNormal.y, 0.0);\n"
		"				break;\n"
		"			case 5: // top\n"
		"				pixelNormal = (float4)( pixelNormal.x, -pixelNormal.z,  pixelNormal.y, 0.0);\n"
		"				break;\n"
		"			}\n"
		"           //float cosinus = fabs(faceNormal.x * pixelNormal.x + faceNormal.y * pixelNormal.y + faceNormal.z * pixelNormal.z);\n"
		"			float cosinus = fabs(pixelNormal.z);\n"
		"			fWichtung *= cosinus; // cosinus between faceNormal and pixelNormal\n"
#ifdef _DEBUG_DEEP_OPENCL
		"			pDebugImage[ gindex * 4 + 0 ] = cosinus;\n" //debug
		"			pDebugImage[ gindex * 4 + 1 ] = cosinus;\n"
		"			pDebugImage[ gindex * 4 + 2 ] = cosinus;\n"
#endif
		"		}\n"
		"		unsigned int gindex = get_global_id(1) * iDX + get_global_id(0);\n"
		"		pSharedImage[ lid * 3 + 0 ] = pImage[ gindex * 4 + 0 ] * fWichtung;\n"
		"		pSharedImage[ lid * 3 + 1 ] = pImage[ gindex * 4 + 1 ] * fWichtung;\n"
		"		pSharedImage[ lid * 3 + 2 ] = pImage[ gindex * 4 + 2 ] * fWichtung;\n"
		"	}\n"
		"	else\n"
		"	{\n"
		"		pSharedImage[ lid * 3 + 0 ] = 0.0;\n"
		"		pSharedImage[ lid * 3 + 1 ] = 0.0;\n"
		"		pSharedImage[ lid * 3 + 2 ] = 0.0;\n"
		"	}\n"
		"	\n"
		"	// sum pSharedImage \n"
		"	//const unsigned int BLOCKSIZE = get_local_size(0) * get_local_size(1); / /wird bei clBuildProgram �bergeben \n"
		"	for(unsigned int stride = 1; stride < BLOCKSIZE; stride *= 2)\n"
		"	{\n"
		"		unsigned int index = 2 * stride * lid;\n"
		"		//unsigned int index2 = index + stride;\n"
		"		\n"
		"		barrier(CLK_LOCAL_MEM_FENCE);\n"
		"		\n"
		"		if (index < BLOCKSIZE)\n"
		"		{\n"
		"			pSharedImage[ index * 3 + 0 ] += pSharedImage[ (index + stride) * 3 + 0 ];\n"
		"			pSharedImage[ index * 3 + 1 ] += pSharedImage[ (index + stride) * 3 + 1 ];\n"
		"			pSharedImage[ index * 3 + 2 ] += pSharedImage[ (index + stride) * 3 + 2 ];\n"
		"		}\n"
		"	}\n"
		"	\n"
		"	if (lid == 0)\n"
		"	{\n"
		"		unsigned int bid = get_group_id(1) * get_num_groups(0) + get_group_id(0);\n"
		"		pErg[ bid * 3 + 0 ] = pSharedImage[ 0 ];\n"
		"		pErg[ bid * 3 + 1 ] = pSharedImage[ 1 ];\n"
		"		pErg[ bid * 3 + 2 ] = pSharedImage[ 2 ];\n"
		"	}\n"
		"}\n"
		"\n"
		"__kernel void sum_light2(__global float *pErg2, __global float *pErg, unsigned int iDX, unsigned int iDY, __local float * pSharedImage)\n"
		"{\n" 
		"	unsigned int lid = get_local_id(1) * get_local_size(0) + get_local_id(0);\n"
		"	\n"
		"	//Load the image part into pSharedImage \n"
		"	if ((get_global_id(0) < iDX) || (get_global_id(1) < iDY))\n"
		"	{\n"
		"		unsigned int gindex = get_global_id(1) * iDX + get_global_id(0);\n"
		"		pSharedImage[ lid * 3 + 0 ] = pErg[ gindex * 3 + 0 ];\n"
		"		pSharedImage[ lid * 3 + 1 ] = pErg[ gindex * 3 + 1 ];\n"
		"		pSharedImage[ lid * 3 + 2 ] = pErg[ gindex * 3 + 2 ];\n"
		"	}\n"
		"	else\n"
		"	{\n"
		"		pSharedImage[ lid * 3 + 0 ] = 0.0;\n"
		"		pSharedImage[ lid * 3 + 1 ] = 0.0;\n"
		"		pSharedImage[ lid * 3 + 2 ] = 0.0;\n"
		"	}\n"
		"	\n"
		"	// sum pSharedImage \n"
		"	//const unsigned int BLOCKSIZE = get_local_size(0) * get_local_size(1); / /wird bei clBuildProgram �bergeben \n"
		"	for(unsigned int stride = 1; stride < BLOCKSIZE; stride *= 2)\n"
		"	{\n"
		"		unsigned int index = 2 * stride * lid;\n"
		"		//unsigned int index2 = index + stride;\n"
		"		\n"
		"		barrier(CLK_LOCAL_MEM_FENCE);\n"
		"		\n"
		"		if (index < BLOCKSIZE)\n"
		"		{\n"
		"			pSharedImage[ index * 3 + 0 ] += pSharedImage[ (index + stride) * 3 + 0 ];\n"
		"			pSharedImage[ index * 3 + 1 ] += pSharedImage[ (index + stride) * 3 + 1 ];\n"
		"			pSharedImage[ index * 3 + 2 ] += pSharedImage[ (index + stride) * 3 + 2 ];\n"
		"		}\n"
		"	}\n"
		"	\n"
		"	if (lid == 0)\n"
		"	{\n"
		"		unsigned int bid = get_group_id(1) * get_num_groups(0) + get_group_id(0);\n"
		"		pErg2[ bid * 3 + 0 ] = pSharedImage[ 0 ];\n"
		"		pErg2[ bid * 3 + 1 ] = pSharedImage[ 1 ];\n"
		"		pErg2[ bid * 3 + 2 ] = pSharedImage[ 2 ];\n"
		"	}\n"
		"}\n"; 
	char clcompileflags[4096];
	unsigned int blocksize = m_itemsize_x * m_itemsize_y;
	sprintf_s(clcompileflags, "-DBLOCKSIZE=%d -cl-fast-relaxed-math -cl-single-precision-constant -cl-denorms-are-zero -cl-mad-enable -Werror", blocksize);

	m_clpgm = new cl_program();
	*m_clpgm = clCreateProgramWithSource(*m_clctx, 1, &source_code, NULL, &clerr);
	CheckOpenCLError(clerr);
	
	clerr = clBuildProgram(*m_clpgm, 0, NULL, clcompileflags, NULL, NULL);

	if (clerr != CL_SUCCESS) 
	{
		// Determine the size of the log
		size_t log_size;
		clGetProgramBuildInfo(*m_clpgm, cldevs[0], CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);

		// Allocate memory for the log
		char *log = (char *) malloc(log_size);

		// Get the log
		clGetProgramBuildInfo(*m_clpgm, cldevs[0], CL_PROGRAM_BUILD_LOG, log_size, log, NULL);

		// Print the log
		printf("%s\n", log);
	}

	CheckOpenCLError(clerr);

	m_clkern = new cl_kernel();
	*m_clkern = clCreateKernel(*m_clpgm, "sum_light", &clerr);
	CheckOpenCLError(clerr);

	m_clkern2 = new cl_kernel();
	*m_clkern2 = clCreateKernel(*m_clpgm, "sum_light2", &clerr);
	CheckOpenCLError(clerr);

#ifdef _TIME_DEBUG
	delete pTimer;
	pTimer = NULL;
#endif

	free( cldevs );
}

void OpenCL::QuitOpenCL()
{
#ifdef _TIME_DEBUG
	Timer *pTimer = new Timer("OpenCL - alles wieder freigeben");
#endif

	//alles wieder freigeben
	free(m_platforms);

	if (*m_clkern) clReleaseKernel(*m_clkern); 
	if (*m_clkern2) clReleaseKernel(*m_clkern2); 
	if (*m_clpgm)  clReleaseProgram(*m_clpgm);
	if (*m_clcmdq) clReleaseCommandQueue(*m_clcmdq);
	if (*m_clctx)  clReleaseContext(*m_clctx);

	delete m_clkern;
	delete m_clkern2;
	delete m_clpgm;
	delete m_clcmdq;
	delete m_clctx;

#ifdef _TIME_DEBUG
	delete pTimer;
#endif
}

CString OpenCL::GetInformation(cl_platform_id platform, cl_platform_info info)
{
	CString erg;
	size_t size;
	cl_int clerr = CL_SUCCESS;

	clerr = clGetPlatformInfo(platform, info, 0, NULL, &size);
	CheckOpenCLError(clerr);
	char * char_text = (char *)malloc(size);
	clerr = clGetPlatformInfo(platform, info, size, char_text, NULL);
	CheckOpenCLError(clerr);
	erg.Format("%s",char_text);
	free(char_text);

	return erg;
}

CString OpenCL::GetInformation(cl_device_id device, cl_device_info info)
{
	CString erg;
	size_t size;
	cl_int clerr = CL_SUCCESS;

	char * char_text;
	cl_uint * erg_uint;
	cl_uint * erg_bool;
	cl_ulong * erg_ulong;
	size_t * erg_size_t;
	cl_platform_id * erg_platform;
	cl_device_fp_config * erg_fp;
	cl_device_exec_capabilities * erg_exec;
	cl_device_mem_cache_type * erg_cache;
	cl_device_local_mem_type * erg_mem;
	cl_command_queue_properties * erg_queue;
	cl_device_type * erg_device;

	clerr = clGetDeviceInfo(device, info, 0, NULL, &size);
	if (clerr != CL_SUCCESS)
	{
		if (clerr == CL_INVALID_VALUE)
			return _T("unsupported value");
		else
			CheckOpenCLError(clerr);
	}

	switch (info)
	{
	case CL_DEVICE_ADDRESS_BITS: //cl_uint
	case CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE:
	case CL_DEVICE_MAX_CLOCK_FREQUENCY:
	case CL_DEVICE_MAX_COMPUTE_UNITS:
	case CL_DEVICE_MAX_CONSTANT_ARGS:
	case CL_DEVICE_MAX_READ_IMAGE_ARGS:
	case CL_DEVICE_MAX_SAMPLERS:
	case CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS:
	case CL_DEVICE_MAX_WRITE_IMAGE_ARGS:
	case CL_DEVICE_MEM_BASE_ADDR_ALIGN:
	case CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE:
	case CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR:
	case CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT:
	case CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT:
	case CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG:
	case CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT:
	case CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE:
	case CL_DEVICE_VENDOR_ID:
		erg_uint = (cl_uint *)malloc(size);
		clerr = clGetDeviceInfo(device, info, size, erg_uint, NULL);
		CheckOpenCLError(clerr);
		erg.Format("%d", erg_uint[0]);
		free(erg_uint);
		break;
	case CL_DEVICE_AVAILABLE: //cl_bool
	case CL_DEVICE_COMPILER_AVAILABLE:
	case CL_DEVICE_ENDIAN_LITTLE:
	case CL_DEVICE_ERROR_CORRECTION_SUPPORT:
	case CL_DEVICE_IMAGE_SUPPORT:
		erg_bool = (cl_bool *)malloc(size);
		clerr = clGetDeviceInfo(device, info, size, erg_bool, NULL);
		CheckOpenCLError(clerr);
		if (erg_bool[0])
			erg.Format("true");
		else
			erg.Format("false");
		free(erg_bool);
		break;
	case CL_DEVICE_DOUBLE_FP_CONFIG: //cl_device_fp_config
	case CL_DEVICE_HALF_FP_CONFIG:
	case CL_DEVICE_SINGLE_FP_CONFIG:
		erg_fp = (cl_device_fp_config *)malloc(size);
		clerr = clGetDeviceInfo(device, info, size, erg_fp, NULL);
		CheckOpenCLError(clerr);
		if (erg_fp[0] & CL_FP_DENORM)   erg.AppendFormat("CL_FP_DENORM");
		if (erg_fp[0] & CL_FP_INF_NAN)  
			if (erg.GetLength()>0) 
				erg.AppendFormat(", CL_FP_DENORM");
			else
				erg.AppendFormat("CL_FP_DENORM");
		if (erg_fp[0] & CL_FP_FMA)  
			if (erg.GetLength()>0) 
				erg.AppendFormat(", CL_FP_FMA");
			else
				erg.AppendFormat("CL_FP_FMA");
		if (erg_fp[0] & CL_FP_ROUND_TO_NEAREST)  
			if (erg.GetLength()>0) 
				erg.AppendFormat(", CL_FP_ROUND_TO_NEAREST");
			else
				erg.AppendFormat("CL_FP_ROUND_TO_NEAREST");
		if (erg_fp[0] & CL_FP_ROUND_TO_ZERO)  
			if (erg.GetLength()>0) 
				erg.AppendFormat(", CL_FP_ROUND_TO_ZERO");
			else
				erg.AppendFormat("CL_FP_ROUND_TO_ZERO");
		if (erg_fp[0] & CL_FP_ROUND_TO_INF)  
			if (erg.GetLength()>0) 
				erg.AppendFormat(", CL_FP_ROUND_TO_INF");
			else
				erg.AppendFormat("CL_FP_ROUND_TO_INF");
		free(erg_fp);
		break;
	case CL_DEVICE_EXECUTION_CAPABILITIES: //cl_device_exec_capabilities
		erg_exec = (cl_device_exec_capabilities *)malloc(size);
		clerr = clGetDeviceInfo(device, info, size, erg_exec, NULL);
		CheckOpenCLError(clerr);
		if (erg_exec[0] & CL_EXEC_KERNEL)   erg.AppendFormat("CL_EXEC_KERNEL");
		if (erg_exec[0] & CL_EXEC_NATIVE_KERNEL)  
			if (erg.GetLength()>0) 
				erg.AppendFormat(", CL_EXEC_NATIVE_KERNEL");
			else
				erg.AppendFormat("CL_EXEC_NATIVE_KERNEL");
		free(erg_exec);
		break;
	case CL_DEVICE_EXTENSIONS: //char
	case CL_DEVICE_NAME:
	case CL_DEVICE_PROFILE:
	case CL_DEVICE_VENDOR:
	case CL_DEVICE_VERSION:
	case CL_DRIVER_VERSION:
		char_text = (char *)malloc(size);
		clerr = clGetDeviceInfo(device, info, size, char_text, NULL);
		CheckOpenCLError(clerr);
		erg.Format("%s", char_text);
		free(char_text);
		break;
	case CL_DEVICE_GLOBAL_MEM_CACHE_SIZE: //cl_ulong
	case CL_DEVICE_GLOBAL_MEM_SIZE:
	case CL_DEVICE_LOCAL_MEM_SIZE:
	case CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE:
	case CL_DEVICE_MAX_MEM_ALLOC_SIZE:
		erg_ulong = (cl_ulong *)malloc(size);
		clerr = clGetDeviceInfo(device, info, size, erg_ulong, NULL);
		CheckOpenCLError(clerr);
		erg.Format("%ld", erg_ulong[0]);
		free(erg_ulong);
		break;
	case CL_DEVICE_GLOBAL_MEM_CACHE_TYPE: //cl_device_mem_cache_type 
		erg_cache = (cl_device_mem_cache_type *)malloc(size);
		clerr = clGetDeviceInfo(device, info, size, erg_cache, NULL);
		CheckOpenCLError(clerr);
		if (erg_cache[0] & CL_NONE)   erg.AppendFormat("CL_NONE");
		if (erg_cache[0] & CL_READ_ONLY_CACHE)  
			if (erg.GetLength()>0) 
				erg.AppendFormat(", CL_READ_ONLY_CACHE");
			else
				erg.AppendFormat("CL_READ_ONLY_CACHE");
		if (erg_cache[0] & CL_READ_WRITE_CACHE)  
			if (erg.GetLength()>0) 
				erg.AppendFormat(", CL_READ_WRITE_CACHE");
			else
				erg.AppendFormat("CL_READ_WRITE_CACHE");
		free(erg_cache);
		break;
	case CL_DEVICE_IMAGE2D_MAX_HEIGHT: //size_t
	case CL_DEVICE_IMAGE2D_MAX_WIDTH:
	case CL_DEVICE_IMAGE3D_MAX_DEPTH:
	case CL_DEVICE_IMAGE3D_MAX_HEIGHT:
	case CL_DEVICE_IMAGE3D_MAX_WIDTH:
	case CL_DEVICE_MAX_PARAMETER_SIZE:
	case CL_DEVICE_MAX_WORK_GROUP_SIZE:
	case CL_DEVICE_PROFILING_TIMER_RESOLUTION:
		erg_size_t = (size_t *)malloc(size);
		clerr = clGetDeviceInfo(device, info, size, erg_size_t, NULL);
		CheckOpenCLError(clerr);
		erg.Format("%ld", erg_size_t[0]);
		free(erg_size_t);
		break;
	case CL_DEVICE_LOCAL_MEM_TYPE: //cl_device_local_mem_type 
		erg_mem = (cl_device_local_mem_type *)malloc(size);
		clerr = clGetDeviceInfo(device, info, size, erg_mem, NULL);
		CheckOpenCLError(clerr);
		if (erg_mem[0] & CL_LOCAL)   erg.AppendFormat("CL_LOCAL");
		if (erg_mem[0] & CL_GLOBAL)  
			if (erg.GetLength()>0) 
				erg.AppendFormat(", CL_GLOBAL");
			else
				erg.AppendFormat("CL_GLOBAL");
		free(erg_mem);
		break;
	case CL_DEVICE_MAX_WORK_ITEM_SIZES: //size_t[]
		erg_size_t = (size_t *)malloc(size);
		clerr = clGetDeviceInfo(device, info, size, erg_size_t, NULL);
		CheckOpenCLError(clerr);
		erg.Format("%ld", erg_size_t[0]);
		for(unsigned int z = 1; z<size/sizeof(size_t); ++z)
			erg.AppendFormat(", %ld", erg_size_t[z]);
		free(erg_size_t);
		break;
	case CL_DEVICE_PLATFORM: //cl_platform_id
		erg_platform = (cl_platform_id *)malloc(size);
		clerr = clGetDeviceInfo(device, info, size, erg_platform, NULL);
		CheckOpenCLError(clerr);
		erg.Format("%d", erg_platform[0]);
		free(erg_platform);
		break;
	case CL_DEVICE_QUEUE_PROPERTIES: //cl_command_queue_properties 
		erg_queue = (cl_command_queue_properties *)malloc(size);
		clerr = clGetDeviceInfo(device, info, size, erg_queue, NULL);
		CheckOpenCLError(clerr);
		if (erg_queue[0] & CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE)   erg.AppendFormat("CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE");
		if (erg_queue[0] & CL_QUEUE_PROFILING_ENABLE)  
			if (erg.GetLength()>0) 
				erg.AppendFormat(", CL_QUEUE_PROFILING_ENABLE");
			else
				erg.AppendFormat("CL_QUEUE_PROFILING_ENABLE");
		free(erg_queue);
		break;
	case CL_DEVICE_TYPE: //cl_device_type 
		erg_device = (cl_device_type *)malloc(size);
		clerr = clGetDeviceInfo(device, info, size, erg_device, NULL);
		CheckOpenCLError(clerr);
		if (erg_device[0] & CL_DEVICE_TYPE_CPU)   erg.AppendFormat("CL_DEVICE_TYPE_CPU");
		if (erg_device[0] & CL_DEVICE_TYPE_GPU)  
			if (erg.GetLength()>0) 
				erg.AppendFormat(", CL_DEVICE_TYPE_GPU");
			else
				erg.AppendFormat("CL_DEVICE_TYPE_GPU");
		if (erg_device[0] & CL_DEVICE_TYPE_ACCELERATOR)  
			if (erg.GetLength()>0) 
				erg.AppendFormat(", CL_DEVICE_TYPE_ACCELERATOR");
			else
				erg.AppendFormat("CL_DEVICE_TYPE_ACCELERATOR");
		if (erg_device[0] & CL_DEVICE_TYPE_DEFAULT)  
			if (erg.GetLength()>0) 
				erg.AppendFormat(", CL_DEVICE_TYPE_DEFAULT");
			else
				erg.AppendFormat("CL_DEVICE_TYPE_DEFAULT");
		free(erg_device);
		break;
	default:
		assert(0);
		break;
	}

	return erg;
}

int OpenCL::GetPlatformPickId()
{
	int result = 0;
	cl_int clerr = CL_SUCCESS;

	cl_platform_id platform_pick;
	cl_platform_id *platforms = NULL;
	clerr = clGetPlatformIDs(0, NULL, &m_num_platforms);
	CheckOpenCLError(clerr);
	if (m_num_platforms > 0)
	{
		platforms = (cl_platform_id *)malloc(sizeof(cl_platform_id) * m_num_platforms);
		clerr = clGetPlatformIDs(m_num_platforms, platforms, NULL);
		CheckOpenCLError(clerr);
		//platform_pick = platforms[GetPlatformPickId()];
	}
	else
		assert(0);

	for(int z=0; z<(int)m_num_platforms; ++z)
	{
		CString infoText;
		platform_pick = platforms[z];

		cl_context_properties props[3];
		props[0] = (cl_context_properties)CL_CONTEXT_PLATFORM;   // indicates that next element is platform
		props[1] = (cl_context_properties)platform_pick;         // platform_pick is of type cl_platform_id
		props[2] = (cl_context_properties)0;                     // last element must be 0

		cl_context clctx = clCreateContextFromType(props, CL_DEVICE_TYPE_ALL, NULL, NULL, &clerr);
		CheckOpenCLError(clerr);

		size_t paramz;
		clerr = clGetContextInfo(clctx, CL_CONTEXT_DEVICES, 0, NULL, &paramz);
		CheckOpenCLError(clerr);

		cl_device_id * cldevs = (cl_device_id *)malloc(paramz);
		clerr = clGetContextInfo(clctx, CL_CONTEXT_DEVICES, paramz, cldevs, NULL);
		CheckOpenCLError(clerr);

		infoText = GetInformation(platform_pick, CL_PLATFORM_VENDOR);
		infoText.Trim();

		if (infoText.Find("NVIDIA") >= 0)
			result = z;
		if (infoText.Find("AMD") >= 0)
			result = z;
		if (infoText.Find("ATI") >= 0)
			result = z;

		free(cldevs);
	}
	free(platforms);

	return result;
}

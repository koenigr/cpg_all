#include "stdafx.h"
#include "OpenGLContext.h"
#include <assert.h>
#include <math.h>
//#include "ExtraWGLStuff.h"
#include "HiddenWindow.h"


// these are used to construct an equilibrated 256 color palette
static unsigned char _threeto8[8] = 
{
	0, 0111>>1, 0222>>1, 0333>>1, 0444>>1, 0555>>1, 0666>>1, 0377
};

static unsigned char _twoto8[4] = 
{
	0, 0x55, 0xaa, 0xff
};

static unsigned char _oneto8[2] = 
{
	0, 255
};

static int defaultOverride[13] = 
{
	0, 3, 24, 27, 64, 67, 88, 173, 181, 236, 247, 164, 91
};

// Windows Default Palette
static PALETTEENTRY defaultPalEntry[20] = 
{
	{ 0,   0,   0,    0 },
	{ 0x80,0,   0,    0 },
	{ 0,   0x80,0,    0 },
	{ 0x80,0x80,0,    0 },
	{ 0,   0,   0x80, 0 },
	{ 0x80,0,   0x80, 0 },
	{ 0,   0x80,0x80, 0 },
	{ 0xC0,0xC0,0xC0, 0 },

	{ 192, 220, 192,  0 },
	{ 166, 202, 240,  0 },
	{ 255, 251, 240,  0 },
	{ 160, 160, 164,  0 },

	{ 0x80,0x80,0x80, 0 },
	{ 0xFF,0,   0,    0 },
	{ 0,   0xFF,0,    0 },
	{ 0xFF,0xFF,0,    0 },
	{ 0,   0,   0xFF, 0 },
	{ 0xFF,0,   0xFF, 0 },
	{ 0,   0xFF,0xFF, 0 },
	{ 0xFF,0xFF,0xFF, 0 }
};


unsigned char COpenGLContext::ComponentFromIndex(int i, UINT nbits, UINT shift)
{
	unsigned char val;

	val = (unsigned char) (i >> shift);
	switch (nbits) 
	{
	case 1:
		val &= 0x1;
		return _oneto8[val];
	case 2:
		val &= 0x3;
		return _twoto8[val];
	case 3:
		val &= 0x7;
		return _threeto8[val];

	default:
		return 0;
	}
}

void COpenGLContext::CreateRGBPalette()
{
	PIXELFORMATDESCRIPTOR pfd;
	LOGPALETTE *pPal;
	int n, i;

	// get the initially choosen video mode
	n = ::GetPixelFormat(m_hDC);
	::DescribePixelFormat(m_hDC, n, sizeof(pfd), &pfd);

	// if is an indexed one...
	if (pfd.dwFlags & PFD_NEED_PALETTE)
	{
		// ... construct an equilibrated palette (3 red bits, 3 green bits, 2 blue bits)
		// NOTE: this code is integrally taken from MFC example Cube
		n = 1 << pfd.cColorBits;
		pPal = (PLOGPALETTE) new char[sizeof(LOGPALETTE) + n * sizeof(PALETTEENTRY)];

		ASSERT(pPal != NULL);

		pPal->palVersion = 0x300;
		pPal->palNumEntries = n;
		for (i=0; i<n; i++)
		{
			pPal->palPalEntry[i].peRed=   ComponentFromIndex (i, pfd.cRedBits,   pfd.cRedShift);
			pPal->palPalEntry[i].peGreen= ComponentFromIndex (i, pfd.cGreenBits, pfd.cGreenShift);
			pPal->palPalEntry[i].peBlue=  ComponentFromIndex (i, pfd.cBlueBits,  pfd.cBlueShift);
			pPal->palPalEntry[i].peFlags= 0;
		}

		// fix up the palette to include the default Windows palette
		if ((pfd.cColorBits == 8)                           &&
			(pfd.cRedBits   == 3) && (pfd.cRedShift   == 0) &&
			(pfd.cGreenBits == 3) && (pfd.cGreenShift == 3) &&
			(pfd.cBlueBits  == 2) && (pfd.cBlueShift  == 6)
			)
		{
			for (i = 1 ; i <= 12 ; i++)
				pPal->palPalEntry[defaultOverride[i]] = defaultPalEntry[i];
		}

		m_CurrentPalette = ::CreatePalette(pPal);
		delete pPal;

		// set the palette
		m_OldPalette=::SelectPalette(m_hDC, m_CurrentPalette, FALSE);
		::RealizePalette(m_hDC);
	}
}

void COpenGLContext::VideoMode(ColorsNumber &c, ZAccuracy &z, BOOL &dbuf)
{
	// set default videomode
	c=MILLIONS_WITH_TRANSPARENCY;
	z=ACCURATE;
	dbuf=FALSE;
}

BOOL COpenGLContext::bSetupPixelFormat()
{
   // define default desired video mode (pixel format)
   static PIXELFORMATDESCRIPTOR pfd = {
      sizeof (PIXELFORMATDESCRIPTOR),             // Size of this structure
      1,                                          // Version number
      PFD_DRAW_TO_BITMAP |                        // Flags
      PFD_DRAW_TO_WINDOW |
      PFD_SUPPORT_OPENGL
      ,
      PFD_TYPE_RGBA,                              // RGB pixel values
      24,                                         // 24-bit color
      0, 0, 0, 0, 0, 0,                           // Don't care about these
      0, 0,                                       // No alpha buffer
      0, 0, 0, 0, 0,                              // No accumulation buffer
      32,                                         // 32-bit depth buffer
      0,                                          // No stencil buffer
      0,                                          // No auxiliary buffers
      PFD_MAIN_PLANE,                             // Layer type
      0,                                          // Reserved (must be 0)
      0, 0, 0                                     // No layer masks
   };

   // let the user change some parameters if he wants
	BOOL bDoublBuf;
	ColorsNumber cnum;
	ZAccuracy zdepth;
	VideoMode(cnum,zdepth,bDoublBuf);

   //set the changes
/*
	if(bDoublBuf)
      pfd.dwFlags=PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL |PFD_DOUBLEBUFFER;
	else 
      pfd.dwFlags=PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL;
*/	
   switch(cnum)
	{
	   case INDEXED: 
         pfd.cColorBits=8;
         break;
	   case THOUSANDS: 
         pfd.cColorBits=16;
         break;
	   case MILLIONS: 
         pfd.cColorBits=24;
         break;
	   case MILLIONS_WITH_TRANSPARENCY: 
         pfd.cColorBits=32;
         break;
	};
	
   switch(zdepth)
	{
	   case NORMAL: 
         pfd.cDepthBits=16;
         break;
	   case ACCURATE: 
         pfd.cDepthBits=32;
         break;
	};

   // ask the system for such video mode
   int pixelformat;
   if ( (pixelformat = ChoosePixelFormat(m_hDC, &pfd)) == 0 )
   {
      AfxMessageBox("ChoosePixelFormat failed");
      return false;
   }

   // try to set this video mode    
	if (SetPixelFormat(m_hDC, pixelformat, &pfd) == FALSE)
   {
      // the requested video mode is not available so get a default one
      assert(0);
      pixelformat = 1;	
		if (DescribePixelFormat(m_hDC, pixelformat, sizeof(PIXELFORMATDESCRIPTOR), &pfd)==0)
		{
         // neither the requested nor the default are available: fail
			AfxMessageBox("SetPixelFormat failed (no OpenGL compatible video mode)");
			return false;
		}
   }

   return true;
}

void COpenGLContext::OnCreateGL()
{
	if (glewInit()!=GLEW_OK) 
	{
		assert(0);
		exit(1);
		return;
	}
	
	// perform hidden line/surface removal (enabling Z-Buffer)
	glEnable(GL_DEPTH_TEST);

	//glEnable(GL_ALPHA_TEST);

	// set background color to black
	glClearColor(0.0f,0.0f,0.0f,0.0f);

	// set clear Z-Buffer value
	glClearDepth(1.0f);

	glEnable(GL_BLEND);
	//	glEnable(GL_LIGHTING);
	// glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glDisable(GL_POLYGON_SMOOTH);
	// glEnable(GL_POLYGON_SMOOTH);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	// glBlendFunc(GL_ONE_MINUS_DST_ALPHA,GL_DST_ALPHA);
	glShadeModel(GL_SMOOTH);
	// glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
	// glDepthFunc(GL_LESS);
	glDepthFunc(GL_LEQUAL);
}

void COpenGLContext::setupDIB(HDC hDC, int size_x, int size_y)
{
	BITMAPINFO *bmInfo;
	BITMAPINFOHEADER *bmHeader;
	UINT usage;
	VOID *base;
	int bmiSize;
	int bitsPerPixel;

	bmiSize = sizeof(*bmInfo);
	bitsPerPixel = GetDeviceCaps(hDC, BITSPIXEL);

	switch (bitsPerPixel) {
	case 8:
		/* bmiColors is 256 WORD palette indices */
		bmiSize += (256 * sizeof(WORD)) - sizeof(RGBQUAD);
		break;
	case 16:
		/* bmiColors is 3 WORD component masks */
		bmiSize += (3 * sizeof(DWORD)) - sizeof(RGBQUAD);
		break;
	case 24:
	case 32:
	default:
		/* bmiColors not used */
		break;
	}

	bmInfo = (BITMAPINFO *) calloc(1, bmiSize);
	bmHeader = &bmInfo->bmiHeader;

	bmHeader->biSize = sizeof(*bmHeader);
	bmHeader->biWidth = size_x;
	bmHeader->biHeight = size_y;
	bmHeader->biPlanes = 1;			/* must be 1 */
	bmHeader->biBitCount = bitsPerPixel;
	bmHeader->biXPelsPerMeter = 0;
	bmHeader->biYPelsPerMeter = 0;
	bmHeader->biClrUsed = 0;			/* all are used */
	bmHeader->biClrImportant = 0;		/* all are important */

	switch (bitsPerPixel) {
	case 8:
		bmHeader->biCompression = BI_RGB;
		bmHeader->biSizeImage = 0;
		usage = DIB_PAL_COLORS;
		/* bmiColors is 256 WORD palette indices */
		{
			WORD *palIndex = (WORD *) &bmInfo->bmiColors[0];
			int i;

			for (i=0; i<256; i++) {
				palIndex[i] = i;
			}
		}
		break;
	case 16:
		bmHeader->biCompression = BI_RGB;
		bmHeader->biSizeImage = 0;
		usage = DIB_RGB_COLORS;
		/* bmiColors is 3 WORD component masks */
		{
			DWORD *compMask = (DWORD *) &bmInfo->bmiColors[0];

			compMask[0] = 0xF800;
			compMask[1] = 0x07E0;
			compMask[2] = 0x001F;
		}
		break;
	case 24:
	case 32:
	default:
		bmHeader->biCompression = BI_RGB;
		bmHeader->biSizeImage = 0;
		usage = DIB_RGB_COLORS;
		/* bmiColors not used */
		break;
	}

	m_hBitmap = CreateDIBSection(hDC, bmInfo, usage, &base, NULL, 0);

	m_render_bitmap_bits = (unsigned char *)base;
	m_render_pixel_bytes = bitsPerPixel / 8;
	m_render_scanline_size = (int)ceil(((double)size_x * m_render_pixel_bytes)/4.0) * 4;
	m_dx = size_x;
	m_dy = size_y;

	if (m_hBitmap == NULL) 
	{
/*
		(void) MessageBox(
			_T("Failed to create DIBSection."),
			_T("OpenGL application error"),
			MB_ICONERROR | MB_OK);
*/
		exit(1);
	}

	m_hOldBitmap = SelectObject(hDC, m_hBitmap);

	free(bmInfo);
}

void COpenGLContext::setupPixelFormat(HDC hDC)
{
    PIXELFORMATDESCRIPTOR pfd = {
                                 sizeof(PIXELFORMATDESCRIPTOR),	/* size of this pfd */
                                 1,				                     /* version num */
                                 PFD_SUPPORT_OPENGL,		         /* support OpenGL */
                                 0,				                     /* pixel type */
                                 0,				                     /* 8-bit color depth */
                                 0, 0, 0, 0, 0, 0,		            /* color bits (ignored) */
                                 0,				                     /* no alpha buffer */
                                 0,				                     /* alpha bits (ignored) */
                                 0,				                     /* no accumulation buffer */
                                 0, 0, 0, 0,			               /* accum bits (ignored) */
                                 32,				                  /* depth buffer */
                                 0,				                     /* no stencil buffer */
                                 0,				                     /* no auxiliary buffers */
                                 PFD_MAIN_PLANE,			         /* main layer */
                                 0,				                     /* reserved */
                                 0, 0, 0,			                  /* no layer, visible, damage masks */
    };
    int SelectedPixelFormat;
    BOOL retVal;

    pfd.cColorBits = GetDeviceCaps(hDC, BITSPIXEL);

    if (m_colorIndexMode) {
	   pfd.iPixelType = PFD_TYPE_COLORINDEX;
    } else {
	   pfd.iPixelType = PFD_TYPE_RGBA;
    }

    if (m_doubleBuffered) {
        pfd.dwFlags |= PFD_DOUBLEBUFFER;
    }

    if (m_renderToDIB) {
	   pfd.dwFlags |= PFD_DRAW_TO_BITMAP;
    } else {
	   pfd.dwFlags |= PFD_DRAW_TO_WINDOW;
    }

    SelectedPixelFormat = ChoosePixelFormat(hDC, &pfd);
    if (SelectedPixelFormat == 0) {
/*
	(void) MessageBox(
		_T("Failed to find acceptable pixel format."),
		_T("OpenGL application error"),
		MB_ICONERROR | MB_OK);
*/
	exit(3);
    }

    retVal = SetPixelFormat(hDC, SelectedPixelFormat, &pfd);
    if (retVal != TRUE) {
/*
	(void) MessageBox(
		_T("Failed to set pixel format."),
		_T("OpenGL application error"),
		MB_ICONERROR | MB_OK);
*/
	exit(4);
    }
}

void COpenGLContext::setupPalette(HDC hDC)
{
    PIXELFORMATDESCRIPTOR pfd;
    int pixelFormat = GetPixelFormat(hDC);
    int paletteSize;

    DescribePixelFormat(hDC, pixelFormat, sizeof(PIXELFORMATDESCRIPTOR), &pfd);

    /*
    ** Determine if a palette is needed and if so what size.
    */
    if (pfd.dwFlags & PFD_NEED_PALETTE) 
    {
   	paletteSize = 1 << pfd.cColorBits;
    } 
    else 
      if (pfd.iPixelType == PFD_TYPE_COLORINDEX) 
      {
	      paletteSize = 4096;
      } 
      else 
      {
         return;
      }

   //es werden keine indizierten Paletten unterstützt
   assert(0);
   return;
}



void COpenGLContext::InitContext(int dx, int dy)
{
	m_bInited = true;

/*
	wglCreatePbufferARB		= (PFNWGLCREATEPBUFFERARBPROC)		wglGetProcAddress("wglCreatePbufferARB");
	wglGetPbufferDCARB		= (PFNWGLGETPBUFFERDCARBPROC)		wglGetProcAddress("wglGetPbufferDCARB");
	wglReleasePbufferDCARB	= (PFNWGLRELEASEPBUFFERDCARBPROC)	wglGetProcAddress("wglReleasePbufferDCARB");
	wglDestroyPbufferARB	= (PFNWGLDESTROYPBUFFERARBPROC)		wglGetProcAddress("wglDestroyPbufferARB");
*/
	// OpenGL rendering context creation
	PIXELFORMATDESCRIPTOR pfd;
	int n;

	//start a window and hide it
	if (!m_pHiddenWindow)
	{
		m_pHiddenWindow = new CMyHiddenWindow();
		m_pHiddenWindow->Create();
	}


	// initialize the private member
	//m_hDC = ::GetDC(GetDesktopWindow());
	
	//m_hDC = m_pHiddenWindow->GetHDC();
	//m_hRC = m_pHiddenWindow->GetHGLRC();


	if (m_pCDC)
	{
		delete m_pCDC;
		m_pCDC = NULL;
	}
	m_hDC = m_pHiddenWindow->GetHDC();
	//m_pCDC = new CDC();
	//m_pCDC->CreateCompatibleDC(CDC::FromHandle(m_hDC));
	//m_hDC = m_pCDC->GetSafeHdc();


/*
	HDC hDeskDC = ::GetDC(::GetDesktopWindow());
	setupPixelFormat(hDeskDC);
	int pixelFormatPBuffer = ::GetPixelFormat(hDeskDC);
	m_hPB = wglCreatePbufferARB(hDeskDC, pixelFormatPBuffer, dx, dy, NULL);
	::ReleaseDC(::GetDesktopWindow(), hDeskDC);
	m_hDC = wglGetPbufferDCARB(m_hPB);
*/


	setupDIB(m_hDC, dx, dy);
	//setupPixelFormat(m_hDC);
	//setupPalette(m_hDC);
	
	if (m_hDC==NULL)
		assert(0);

	// choose the requested video mode
/*
	if (!bSetupPixelFormat())
	{
		m_bInited = false;
		return;
	}
*/
	// ask the system if the video mode is supported
	n=::GetPixelFormat(m_hDC);
	::DescribePixelFormat(m_hDC,n,sizeof(pfd),&pfd);

	// create a palette if the requested video mode has 256 colors (indexed mode)
	CreateRGBPalette();

	// link the Win Device Context with the OGL Rendering Context
	m_hRC = wglCreateContext(m_hDC);

	// specify the target DeviceContext (window) of the subsequent OGL calls
	wglMakeCurrent(m_hDC, m_hRC);

	// performs default setting of rendering mode,etc..
	OnCreateGL();

	// free the target DeviceContext (window)
	wglMakeCurrent(NULL,NULL);
}

void COpenGLContext::CloseContext()
{
	if (m_bInited)
	{
		// specify the target DeviceContext (window) of the subsequent OGL calls
		wglMakeCurrent(m_hDC, m_hRC);

		// release definitely OGL Rendering Context
		if (m_hRC!=NULL)
			::wglDeleteContext(m_hRC);
/*
		wglReleasePbufferDCARB(m_hPB, m_hDC);
		wglDestroyPbufferARB(m_hPB);
*/
		// Select our palette out of the dc
		::SelectPalette(m_hDC, m_OldPalette, FALSE);

		if (m_pCDC)
		{
			delete m_pCDC;
			m_pCDC = NULL;
		}

		if (m_pHiddenWindow)
		{
			delete m_pHiddenWindow;
			m_pHiddenWindow = NULL;
		}

		m_bInited = false;
	}
}

void COpenGLContext::BeginGLCommands()
{
	// check if we are inside a drawing session or not....
	if(!( m_hRC==wglGetCurrentContext() && m_hDC==wglGetCurrentDC() ))
	{
		// ...if not specify the target DeviceContext of the subsequent OGL calls
		wglMakeCurrent(m_hDC, m_hRC);
	};
}

void COpenGLContext::EndGLCommands()
{
	wglMakeCurrent(NULL,NULL);
}

COLORREF COpenGLContext::GetPixel(int x, int y)
{
	return (*(COLORREF *)(m_render_bitmap_bits + y * m_render_scanline_size + x * m_render_pixel_bytes));
}

const CString COpenGLContext::GetInformation(InfoField type)
{
	PIXELFORMATDESCRIPTOR pfd;
	CString str("Not Available");
	GLenum err = GL_NO_ERROR;

	// Get information about the DC's current pixel format 
	::DescribePixelFormat( m_hDC, ::GetPixelFormat(m_hDC),sizeof(PIXELFORMATDESCRIPTOR), &pfd ); 

	// specify the target DeviceContext of the subsequent OGL calls
	wglMakeCurrent(m_hDC, m_hRC);
	CheckGLError();

	switch(type)
	{

		// Derive driver information
	case ACCELERATION:
		if( 0==(INSTALLABLE_DRIVER_TYPE_MASK & pfd.dwFlags) )
			str="Fully Accelerated (ICD)"; // fully in hardware (fastest)
		else 
			if (INSTALLABLE_DRIVER_TYPE_MASK==(INSTALLABLE_DRIVER_TYPE_MASK & pfd.dwFlags) ) 
				str="Partially Accelerated (MCD)"; // partially in hardware (pretty fast, maybe..)
			else str="Not Accelerated (Software)";	// software
			break;

			// get the company name responsible for this implementation
	case VENDOR:
		str=(char*)::glGetString(GL_VENDOR);
		err = ::glGetError();
		if ( err!=GL_NO_ERROR) 
			str.Format("Not Available");// failed!
		break;

		// get the renderer name; this is specific of an hardware configuration
	case RENDERER:
		str=(char*)::glGetString(GL_RENDERER);
		err = ::glGetError();
		if ( err!=GL_NO_ERROR) 
			str.Format("Not Available");// failed!
		break;

		// get the version
	case VERSION:
		str=(char*)::glGetString(GL_VERSION);
		err = ::glGetError();
		if ( err!=GL_NO_ERROR) 
			str.Format("Not Available");// failed!
		break;

		// return a space separated list of extensions
	case EXTENSIONS:
		str=(char*)::glGetString(GL_EXTENSIONS);
		err = ::glGetError();
		if ( err!=GL_NO_ERROR) 
			str.Format("Not Available");// failed!
		break;
	};

	// free the target DeviceContext (window) and return the result
	wglMakeCurrent(NULL,NULL);
	return str;
}

CString COpenGLContext::GetInformation()
{
	CString infoText;
	infoText  = "Acceleration:\t" + GetInformation(ACCELERATION) +"\f\n";
	infoText += "Vendor:\t\t"     + GetInformation(VENDOR) +'\n';
	infoText += "Renderer:\t\t"   + GetInformation(RENDERER) +'\n';
	infoText += "Version:\t\t"    + GetInformation(VERSION) +'\n';
	infoText += "Extensions:\t"   + GetInformation(EXTENSIONS) +"\n\n";

	return infoText;
}



#pragma once

#include <Glew\glew.h>
#include "gl/glu.h"
#include "stdafx.h"
#include <assert.h>
//#include "ExtraWGLStuff.h"
#include "HiddenWindow.h"

#ifdef WIN64
	#pragma comment(lib,"glew32_x64.lib")
#else
	#pragma comment(lib,"glew32.lib")
#endif

// These are stored in proper folders in Windows SDK
#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"GLU32.lib")
// #pragma comment(lib,"GLAUX.lib")

enum InfoField {VENDOR,RENDERER,VERSION,ACCELERATION,EXTENSIONS};
enum ColorsNumber{INDEXED,THOUSANDS,MILLIONS,MILLIONS_WITH_TRANSPARENCY};
enum ZAccuracy{NORMAL,ACCURATE};

#define MAX_LISTS 20
// used to identify a MCD video driver (partial OGL acceleration)
#define INSTALLABLE_DRIVER_TYPE_MASK  (PFD_GENERIC_ACCELERATED|PFD_GENERIC_FORMAT)


#ifdef _DEBUG
inline void CheckGLError ()
{
	GLenum error = glGetError();
	if (error==GL_NO_ERROR)
		return;

	int stop=0;

	switch(error)
	{
	case GL_INVALID_ENUM      : {stop=1;assert(0);}
	case GL_INVALID_VALUE     : {stop=2;assert(0);}
	case GL_INVALID_OPERATION : {stop=3;assert(0);}
	case GL_STACK_OVERFLOW    : {stop=4;assert(0);}
	case GL_STACK_UNDERFLOW   : {stop=5;assert(0);}
	case GL_OUT_OF_MEMORY     : {stop=6;assert(0);}
	default: assert(0);
	}		
}
#else
inline void CheckGLError ()
{
}
#endif

class COpenGLContext
{
	HGLRC m_hRC;	// OpenGL Rendering Context
	HDC m_hDC;      // Handle HDC
	CDC * m_pCDC;   // CDC
	//HPBUFFERARB m_hPB; // PixelBufferObject
	int m_dx;		// Width
	int m_dy;		// Height
	bool m_bInited; // Initialisiert ?
	CMyHiddenWindow * m_pHiddenWindow; //versteckter Window Thread

	HPALETTE m_CurrentPalette; // palettes
	HPALETTE m_OldPalette;
	HGDIOBJ m_hBitmap;
	HGDIOBJ m_hOldBitmap;
	unsigned char * m_render_bitmap_bits;
	int m_render_scanline_size;
	int m_render_pixel_bytes;
	BOOL m_colorIndexMode;
	BOOL m_doubleBuffered;
	BOOL m_renderToDIB;

	BOOL bSetupPixelFormat();
	void VideoMode(ColorsNumber &c, ZAccuracy &z, BOOL &dbuf);
	void CreateRGBPalette();
	unsigned char ComponentFromIndex(int i, UINT nbits, UINT shift);
	void setupDIB(HDC hDC, int size_x, int size_y);
	void setupPixelFormat(HDC hDC);
	void setupPalette(HDC hDC);
	const CString GetInformation(InfoField type);

public:
	COpenGLContext()
		: m_bInited(false)
		, m_hRC( NULL )
		, m_colorIndexMode(FALSE)
		, m_doubleBuffered(FALSE)
		, m_renderToDIB(TRUE)
		, m_pHiddenWindow( NULL )
		, m_pCDC( NULL )
	{
	}

	~COpenGLContext()
	{
		CloseContext();
	}

	void InitContext(int dx, int dy);
	void CloseContext();
	void BeginGLCommands();
	void EndGLCommands();
	COLORREF GetPixel(int x, int y);
	int GetDX() { return m_dx; }
	int GetDY() { return m_dy; }
	virtual void OnCreateGL();
	CString GetInformation();
};
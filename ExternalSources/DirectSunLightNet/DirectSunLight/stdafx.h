// stdafx.h : Includedatei f�r Standardsystem-Includedateien
// oder h�ufig verwendete projektspezifische Includedateien,
// die nur in unregelm��igen Abst�nden ge�ndert werden.
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Selten verwendete Teile der Windows-Header nic	ht einbinden.



// TODO: Hier auf zus�tzliche Header, die das Programm erfordert, verweisen.
#include <afx.h>
#include <afxwin.h>


// ask linker to load OpenCV static libraries
#pragma comment(lib, "opencv_core2412.lib")
#pragma comment(lib, "opencv_highgui2412.lib")
#pragma comment(lib, "opencv_imgproc2412.lib")
//#pragma comment(lib, "opencv_legacy243.lib")
#pragma comment(lib, "opencv_ml2412.lib")
#pragma comment(lib, "opencv_video2412.lib")
#pragma comment(lib, "opencv_objdetect2412.lib")	
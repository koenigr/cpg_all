// Dies ist die Haupt-DLL.

#include "stdafx.h"

#include "DirectSunLightNet.h"
//#include "..\..\..\ExternalSources\PureGeom3d\PureGeom3d\OpenGLTesselator.h"

using namespace DirectSunLightNet;


bool CDirectSunLightNet::SetHDRImage(String ^ name)
{
	char* name2 = (char*)(void*)Marshal::StringToHGlobalAnsi(name); // (char *)Marshal::StringToHGlobalAnsi(name).ToPointer();//
	bool result = m_pDirectSunLight->SetHDRImage(name2);
	Marshal::FreeHGlobal(IntPtr(name2));
	return result;
}

void CDirectSunLightNet::SetResolution(int cubeSize)
{
	m_pDirectSunLight->SetResolution(cubeSize);
}

void CDirectSunLightNet::PrepareHDRCubeMap(float fNorthAngle)
{
	m_pDirectSunLight->PrepareHDRCubeMap(fNorthAngle);
}

void CDirectSunLightNet::CalculateDirectSunLight(CGeom3d::Vec_3d ^ position, CGeom3d::Vec_3d ^ direction, float %fEnergyRed, float %fEnergyGreen, float %fEnergyBlue)
{
	DSL::CGeom3d::Vec3d positionCpp;
	DSL::CGeom3d::Vec3d directionCpp;
	positionCpp.x = position->x;
	positionCpp.y = position->y;
	positionCpp.z = position->z;
	directionCpp.x = direction->x;
	directionCpp.y = direction->y;
	directionCpp.z = direction->z;
	float resultRed, resultGreen, resultBlue;
	m_pDirectSunLight->CalculateDirectSunLight(positionCpp, directionCpp, resultRed, resultGreen, resultBlue);
	fEnergyRed		= resultRed;
	fEnergyGreen	= resultGreen;
	fEnergyBlue		= resultBlue;
}

void CDirectSunLightNet::DeleteAllGeometry()
{
	m_pDirectSunLight->DeleteAllGeometry();
}

size_t CDirectSunLightNet::AddPolygon(cli::array<cli::array<CGeom3d::Vec_3d ^>^>^ points, float fTransparency)
{
#if 0
	DSL::CGeom3d::Point_3d_Vector_Vector pvv;
	for each (cli::array<CGeom3d::Vec_3d ^>^ loop in points)
	{
		DSL::CGeom3d::Point_3d_Vector pv;
		for each (CGeom3d::Vec_3d ^ point in loop)
		{
			double x = point->x;
			double y = point->y;
			double z = point->z;
			pv.push_back(DSL::CGeom3d::Vec3d(x, y, z));
		}
		pvv. push_back(pv);
	}

	return m_pDirectSunLight->AddPolygon(pvv, fTransparency);
#else
	DSL::CGeom3d::Vec3d normaleCpp;
	DSL::CGeom3d::TriangleVector trianglesCpp;

	CGeom3d::Vec_3d normale = CGeom3d::GetNormale(points);
	double nx, ny, nz;
	nx = normale.x;
	ny = normale.y;
	nz = normale.z;
	normaleCpp = DSL::CGeom3d::Vec3d(nx, ny, nz);
	cli::array<CGeom3d::Triangle^>^ triangles;
	Tess::COpenGLTesselator::Tesselate(points, triangles);

	for each(CGeom3d::Triangle ^ tri in triangles)
	{
		DSL::CGeom3d::Vec3d p1, p2, p3;
		double px, py, pz;
		px = tri->m_pP1->x;
		py = tri->m_pP1->y;
		pz = tri->m_pP1->z;
		p1 = DSL::CGeom3d::Vec3d(px, py, pz);
		px = tri->m_pP2->x;
		py = tri->m_pP2->y;
		pz = tri->m_pP2->z;
		p2 = DSL::CGeom3d::Vec3d(px, py, pz);
		px = tri->m_pP3->x;
		py = tri->m_pP3->y;
		pz = tri->m_pP3->z;
		p3 = DSL::CGeom3d::Vec3d(px, py, pz);
		trianglesCpp.push_back(DSL::CGeom3d::Triangle_3d(p1, p2, p3));
	}

	return m_pDirectSunLight->AddPolygonTriangles(normaleCpp, trianglesCpp, fTransparency);
#endif
}

void CDirectSunLightNet::DeletePolygon(size_t id)
{
	m_pDirectSunLight->DeletePolygon(id);
}

bool CDirectSunLightNet::MovePolygon(size_t id, cli::array<cli::array<CGeom3d::Vec_3d ^>^>^ newPoints)
{
	DSL::CGeom3d::Point_3d_Vector_Vector pvv;
	for each (cli::array<CGeom3d::Vec_3d ^>^ loop in newPoints)
	{
		DSL::CGeom3d::Point_3d_Vector pv;
		for each (CGeom3d::Vec_3d ^ point in loop)
		{
			double x = point->x;
			double y = point->y;
			double z = point->z;
			pv.push_back(DSL::CGeom3d::Vec3d(x, y, z));
		}
		pvv. push_back(pv);
	}

	return m_pDirectSunLight->MovePolygon(id, pvv);
}

void CDirectSunLightNet::CalculateDirectSunLight(cli::array<CGeom3d::Vec_3d ^>^ pPosition, cli::array<CGeom3d::Vec_3d ^>^ pDirection, cli::array<float> ^ % pResult)
{
	size_t iPointCount = pPosition->Length;
	DSL::CGeom3d::Vec3d * pPositionCpp = (DSL::CGeom3d::Vec3d *)malloc(sizeof(DSL::CGeom3d::Vec3d) * iPointCount);
	DSL::CGeom3d::Vec3d * pDirectionCpp = (DSL::CGeom3d::Vec3d *)malloc(sizeof(DSL::CGeom3d::Vec3d) * iPointCount);
	float * pResultCpp = (float *)malloc(sizeof(float) * 3 * iPointCount);

	for(size_t i = 0; i < iPointCount; ++i)
	{
		pPositionCpp[i].x = pPosition[i]->x;
		pPositionCpp[i].y = pPosition[i]->y;
		pPositionCpp[i].z = pPosition[i]->z;
		pDirectionCpp[i].x = pDirection[i]->x;
		pDirectionCpp[i].y = pDirection[i]->y;
		pDirectionCpp[i].z = pDirection[i]->z;
	}

	m_pDirectSunLight->CalculateDirectSunLight(iPointCount, pPositionCpp, pDirectionCpp, pResultCpp);

	pResult = gcnew cli::array<float>(iPointCount * 3);
	for(size_t i = 0; i < iPointCount * 3; ++i)
		pResult[i] = pResultCpp[i];

	free(pPositionCpp);
	free(pDirectionCpp);
	free( pResultCpp );
}

bool CDirectSunLightNet::StartOpenCLOnceAndForever()
{
	return m_pDirectSunLight->StartOpenCLOnceAndForever();
}

void  CDirectSunLightNet::SetNormale(CGeom3d::Vec_3d ^ normale)
{
	DSL::CGeom3d::Vec3d normaleCpp;
	normaleCpp.x = normale->x;
	normaleCpp.y = normale->y;
	normaleCpp.z = normale->z;
	m_pDirectSunLight->SetNormale(normaleCpp);
}

void  CDirectSunLightNet::SetCalculationMode(DirectSunLightNet::CalculationMode mode)
{
	DSL::CDirectSunLight::CalculationMode modeCpp;
	switch (mode)
	{
	case CalculationMode::luminance_kcd_per_square_meter:
		modeCpp = DSL::CDirectSunLight::Leuchtdichte_kcd_per_square_meter;
		break;
	case CalculationMode::illuminance_klux:
		modeCpp = DSL::CDirectSunLight::Beleuchtungsstaerke_klux;
		break;
	default:
		throw gcnew NotImplementedException();
		break;
	}
	m_pDirectSunLight->SetCalculationMode(modeCpp);
}

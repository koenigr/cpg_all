// DirectSunLightNet.h

#pragma once

#include "../DirectSunLight/DirectSunLight.h"
#include "Isovist3d.h"

using namespace System;
using namespace System::Runtime::InteropServices;

namespace DirectSunLightNet 
{
	public enum class CalculationMode
	{
		luminance_kcd_per_square_meter = 0,
		illuminance_klux
	};

	public ref class CDirectSunLightNet: public IDisposable
	{
		DSL::CDirectSunLight * m_pDirectSunLight;
		bool disposed;

	public:

		CDirectSunLightNet(): disposed(false)
		{
			m_pDirectSunLight = new DSL::CDirectSunLight();
		}

		~CDirectSunLightNet()
		{
			this->!CDirectSunLightNet();
			disposed = true;
		}

		!CDirectSunLightNet()
		{
			delete m_pDirectSunLight;
			m_pDirectSunLight = NULL;
		}
		
		bool SetHDRImage(String ^ name);																					// set the sky HDRI
		void SetResolution(int cubeSize);																					// init the OpenGL Context in this resolution
		void PrepareHDRCubeMap(float fNorthAngle);																			// berechnet die CubeMap vor
		void CalculateDirectSunLight(CGeom3d::Vec_3d ^ position, CGeom3d::Vec_3d ^ direction, float %fEnergyRed, float %fEnergyGreen, float %fEnergyBlue);	// calculate the energy at the given point
		void CalculateDirectSunLight(cli::array<CGeom3d::Vec_3d ^>^ pPosition, cli::array<CGeom3d::Vec_3d ^>^ pDirection, cli::array<float> ^ % pResult);					// calculate the energy at the given points
		void SetNormale(CGeom3d::Vec_3d ^ normale);                                                                         // sets the normale for lux calculation
		void SetCalculationMode(CalculationMode mode);                                                                      // sets the calculation mode (kcd/m� or klux (using normale cosinus))

		void DeleteAllGeometry();																							// l�scht alle Geometrie
		size_t AddPolygon(cli::array<cli::array<CGeom3d::Vec_3d ^>^>^ points, float fTransparency);									// f�gt ein Polygon hinzu und liefert seine ID
		void DeletePolygon(size_t id);																						// l�scht ein Polygon
		bool MovePolygon(size_t id, cli::array<cli::array<CGeom3d::Vec_3d ^>^>^ newPoints);											// verschiebt ein Polygon

		bool StartOpenCLOnceAndForever();																					// startet OpenCL
	};
}

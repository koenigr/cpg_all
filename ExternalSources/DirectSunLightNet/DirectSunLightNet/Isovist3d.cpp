// Dies ist die Haupt-DLL.

#include "stdafx.h"

#include "Isovist3d.h"
//#include "..\..\..\ExternalSources\PureGeom3d\PureGeom3d\OpenGLTesselator.h"

using namespace Isovist3d_GPU;


void CIsovist3d::SetResolution(int cubeSize)
{
	m_pIsovist3d->SetResolution(cubeSize);
}

void CIsovist3d::CalculateIsovist3d(CGeom3d::Vec_3d ^ position)
{
	DSL::CGeom3d::Vec3d positionCpp;
	positionCpp.x = position->x;
	positionCpp.y = position->y;
	positionCpp.z = position->z;
	m_pIsovist3d->CalculateIsovist3d(positionCpp);
}

void CIsovist3d::DeleteAllGeometry()
{
	m_pIsovist3d->DeleteAllGeometry();
}

size_t CIsovist3d::AddPolygon(cli::array<cli::array<CGeom3d::Vec_3d ^>^>^ points)
{
#if 0
	DSL::CGeom3d::Point_3d_Vector_Vector pvv;
	for each (cli::array<CGeom3d::Vec_3d ^>^ loop in points)
	{
		DSL::CGeom3d::Point_3d_Vector pv;
		for each (CGeom3d::Vec_3d ^ point in loop)
		{
			double x = point->x;
			double y = point->y;
			double z = point->z;
			pv.push_back(DSL::CGeom3d::Vec3d(x, y, z));
		}
		pvv. push_back(pv);
	}

	return m_pIsovist3d->AddPolygon(pvv);
#else
	DSL::CGeom3d::Vec3d normaleCpp;
	DSL::CGeom3d::TriangleVector trianglesCpp;

	CGeom3d::Vec_3d normale = CGeom3d::GetNormale(points);
	double nx, ny, nz;
	nx = normale.x;
	ny = normale.y;
	nz = normale.z;
	normaleCpp = DSL::CGeom3d::Vec3d(nx, ny, nz);
	cli::array<CGeom3d::Triangle^>^ triangles;
	Tess::COpenGLTesselator::Tesselate(points, triangles);

	for each(CGeom3d::Triangle ^ tri in triangles)
	{
		DSL::CGeom3d::Vec3d p1, p2, p3;
		double px, py, pz;
		px = tri->m_pP1->x;
		py = tri->m_pP1->y;
		pz = tri->m_pP1->z;
		p1 = DSL::CGeom3d::Vec3d(px, py, pz);
		px = tri->m_pP2->x;
		py = tri->m_pP2->y;
		pz = tri->m_pP2->z;
		p2 = DSL::CGeom3d::Vec3d(px, py, pz);
		px = tri->m_pP3->x;
		py = tri->m_pP3->y;
		pz = tri->m_pP3->z;
		p3 = DSL::CGeom3d::Vec3d(px, py, pz);
		trianglesCpp.push_back(DSL::CGeom3d::Triangle_3d(p1, p2, p3));
	}

	return m_pIsovist3d->AddPolygonTriangles(normaleCpp, trianglesCpp);
#endif
}

void CIsovist3d::DeletePolygon(size_t id)
{
	m_pIsovist3d->DeletePolygon(id);
}

bool CIsovist3d::MovePolygon(size_t id, cli::array<cli::array<CGeom3d::Vec_3d ^>^>^ newPoints)
{
	DSL::CGeom3d::Point_3d_Vector_Vector pvv;
	for each (cli::array<CGeom3d::Vec_3d ^>^ loop in newPoints)
	{
		DSL::CGeom3d::Point_3d_Vector pv;
		for each (CGeom3d::Vec_3d ^ point in loop)
		{
			double x = point->x;
			double y = point->y;
			double z = point->z;
			pv.push_back(DSL::CGeom3d::Vec3d(x, y, z));
		}
		pvv. push_back(pv);
	}

	return m_pIsovist3d->MovePolygon(id, pvv);
}

size_t CIsovist3d::GetFBOPixelId(int x, int y)
{
	return m_pIsovist3d->GetFBOPixelId(x, y);
}

CGeom3d::Vec_3d^ CIsovist3d::GetFBOPixelRay(int x, int y)
{
	Vec3d vec = m_pIsovist3d->GetFBOPixelRay(x, y);
	return gcnew CGeom3d::Vec_3d(vec.x, vec.y, vec.z);
}
// DirectSunLightNet.h

#pragma once

<<<<<<< HEAD
#include "../../DirectSunLight/DirectSunLight/Isovist3d.h"
//#include "../../../ExternalSources/PureGeom3d/PureGeom3d/Geom3d.h"
//#include "../../../ExternalSources/PureGeom3d/PureGeom3d/OpenGLTesselator.h"

=======
#include "../DirectSunLight/Isovist3d.h"
//#include "..\..\..\ExternalSources\PureGeom3d\PureGeom3d\Geom3d.h"
>>>>>>> 773d77744c78c56db032b8de0141c6686c9ca32d

using namespace System;
using namespace System::Runtime::InteropServices;

namespace Isovist3d_GPU 
{
	public ref class CIsovist3d: public IDisposable
	{
		ISOVIST3D::CIsovist3d * m_pIsovist3d;
		bool disposed;

	public:

		CIsovist3d(): disposed(false)
		{
			m_pIsovist3d = new ISOVIST3D::CIsovist3d();
		}

		~CIsovist3d()
		{
			this->!CIsovist3d();
			disposed = true;
		}

		!CIsovist3d()
		{
			if (m_pIsovist3d)
				delete m_pIsovist3d;
			m_pIsovist3d = NULL;
		}

		void SetResolution(int cubeSize);																					// init the OpenGL Context in this resolution
		void CalculateIsovist3d(CGeom3d::Vec_3d ^ position);																// calculate the isovist3d at the given point
		size_t GetFBOPixelId(int x, int y);																					// returns the FBO ID at that pixel
		CGeom3d::Vec_3d^ GetFBOPixelRay(int x, int y);                                                                      // returns the FBO ray at that pixel

		void DeleteAllGeometry();																							// l�scht alle Geometrie
		size_t AddPolygon(cli::array<cli::array<CGeom3d::Vec_3d ^>^>^ points);														// f�gt ein Polygon hinzu und liefert seine ID
		void DeletePolygon(size_t id);																						// l�scht ein Polygon
		bool MovePolygon(size_t id, cli::array<cli::array<CGeom3d::Vec_3d ^>^>^ newPoints);											// verschiebt ein Polygon
	};
}

using System;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Reflection;

namespace Testproperty
{		
	//The class to show in the propertygrid must inherit CustomClass
	//test Class with three public properties
	public class Class1:ICustomClass
	{

		int Test;
		int Test2;
		int Test3;

		PropertyList publicproperties=new PropertyList();

		public int test
		{

			get
			{
				return Test;
			}
			set
			{
				Test=value;
			}
		}

		public int test2
		{
			get
			{
				return Test2;
			}
			set
			{
				Test2=value;
			}
		}

		public int test3
		{
			get
			{
				return Test3;
			}
			set
			{
				Test3=value;
			}
		}


		//ICustomClass implementation
		public PropertyList PublicProperties
		{
			get
			{
				return publicproperties;
			}
			set
			{
				publicproperties=value;
			}
		}

        /// <summary>
        /// Adds a property of a given object. Copies the original property and adds an
        /// implementation that invokes the original propertys' get-/set-method.
        /// The name of the original property can be changed by adding a
        /// PropertyDescriptionAttribute to it. 
        /// </summary>
        /// <param name="propertyOwner"></param>
        /// <param name="property"></param>
        //public void AddProperty(Object propertyOwner, PropertyInfo property)
        //{
        //    //look if there is already a reference field for propertyOwner
        //    //if not, create it
        //    //
        //    int objectReferenceIndex = 0;
        //    if ((objectReferenceIndex = objectReferences.IndexOf(propertyOwner)) == -1)
        //        objectReferenceIndex = AddObjectReferenceField(propertyOwner);

        //    string propertyName = property.Name;

        //    //look if there is a PropertyDescriptionAttribute, if yes replace
        //    //original property name with string in the description attribute
        //    //
        //    bool descriptionFound = false;
        //    int i = 0;
        //    Object[] attributes = property.GetCustomAttributes(true);
        //    while ((i < attributes.Length) && (!descriptionFound))
        //    {
        //        if (attributes[i] is PropertyDescriptionAttribute)
        //        {
        //            PropertyDescriptionAttribute pda = attributes[i] as PropertyDescriptionAttribute;
        //            propertyName = pda.PropertyDescription;
        //            descriptionFound = true;
        //        }
        //        i++;
        //    }

        //    //define the property
        //    //
        //    PropertyBuilder pb = dpoTypeBld.DefineProperty(propertyName, property.Attributes, property.PropertyType, null);

        //    //define the get method
        //    //
        //    MethodBuilder getPropMethBld = dpoTypeBld.DefineMethod(property.GetGetMethod().Name,
        //         property.GetGetMethod().Attributes,
        //         property.GetGetMethod().CallingConvention,
        //         property.GetGetMethod().ReturnType, new Type[] { });

        //    ILGenerator getIL = getPropMethBld.GetILGenerator();

        //    MethodInfo tmp = property.GetGetMethod();

        //    getIL.Emit(OpCodes.Ldarg_0);                                    //load reference to dynamic object 
        //    getIL.Emit(OpCodes.Ldfld, objectReferenceFields[objectReferenceIndex]);                  //load reference to property owner
        //    getIL.Emit(OpCodes.Call, property.GetGetMethod());   //call contents original property
        //    getIL.Emit(OpCodes.Ret);

        //    //if there is a set method define it
        //    //
        //    if (property.GetSetMethod() != null)
        //    {
        //        MethodBuilder setPropMethBld = dpoTypeBld.DefineMethod(property.GetSetMethod().Name,
        //             property.GetSetMethod().Attributes,
        //             property.GetSetMethod().CallingConvention,
        //             property.GetSetMethod().ReturnType, new Type[] { property.GetSetMethod().GetParameters()[0].ParameterType });

        //        ILGenerator setIL = setPropMethBld.GetILGenerator();

        //        tmp = property.GetSetMethod();

        //        setIL.Emit(OpCodes.Ldarg_0);                                    //load reference to dynamic object 
        //        setIL.Emit(OpCodes.Ldfld, objectReferenceFields[objectReferenceIndex]);                  //load reference to content field
        //        setIL.Emit(OpCodes.Ldarg_1);                                    //load set method parameter
        //        setIL.Emit(OpCodes.Call, property.GetSetMethod());   //call contents original property
        //        setIL.Emit(OpCodes.Ret);

        //        pb.SetSetMethod(setPropMethBld);
        //    }

        //    pb.SetGetMethod(getPropMethBld);

        //    //add some of the properties' custom attributes to its dynamic equivalent
        //    //currently supported: BrowsableAttribute, EditorAttribute, TypeConverterAttribute
        //    //
        //    foreach (Object attrib in attributes)
        //    {
        //        Type attributeType;
        //        CustomAttributeBuilder attributeBuilder;
        //        if (attrib is BrowsableAttribute)
        //        {
        //            BrowsableAttribute ba = attrib as BrowsableAttribute;
        //            attributeType = typeof(BrowsableAttribute);
        //            ConstructorInfo constructorInfo = attributeType.GetConstructor(new Type[1] { typeof(bool) });
        //            attributeBuilder = new CustomAttributeBuilder(constructorInfo, new object[1] { ba.Browsable });
        //            pb.SetCustomAttribute(attributeBuilder);
        //        }
        //        else if (attrib is EditorAttribute)
        //        {
        //            EditorAttribute ea = attrib as EditorAttribute;
        //            attributeType = typeof(EditorAttribute);
        //            ConstructorInfo constructorInfo = attributeType.GetConstructor(new Type[2] { typeof(string), typeof(string) });
        //            attributeBuilder = new CustomAttributeBuilder(constructorInfo, new object[2] { ea.EditorTypeName, ea.EditorBaseTypeName });
        //            pb.SetCustomAttribute(attributeBuilder);
        //        }
        //        else if (attrib is TypeConverterAttribute)
        //        {
        //            TypeConverterAttribute tca = attrib as TypeConverterAttribute;
        //            attributeType = typeof(TypeConverterAttribute);
        //            ConstructorInfo constructorInfo = attributeType.GetConstructor(new Type[1] { typeof(string) });
        //            attributeBuilder = new CustomAttributeBuilder(constructorInfo, new object[1] { tca.ConverterTypeName });
        //            pb.SetCustomAttribute(attributeBuilder);
        //        }
        //        else if (attrib is CategoryAttribute)
        //        {
        //            CategoryAttribute tca = attrib as CategoryAttribute;
        //            attributeType = typeof(CategoryAttribute);
        //            ConstructorInfo constructorInfo = attributeType.GetConstructor(new Type[1] { typeof(string) });
        //            attributeBuilder = new CustomAttributeBuilder(constructorInfo, new object[1] { tca.Category });
        //            pb.SetCustomAttribute(attributeBuilder);
        //        }
        //    }
        //}
	}
}

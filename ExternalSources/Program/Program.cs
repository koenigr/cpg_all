﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace statistic4RK
{
    class Program
    {
        static void Main(string[] args)
        {
            // create sample data
            List<float[]> bitmapsDiff = new List<float[]>();
            List<float[]> bitmapsSame = new List<float[]>();
            List<float[]> bitmapsSimilar = new List<float[]>();

            int size = 9;
            bitmapsDiff.Add(new float[]{    1,1,0, 0,0,0, 1,1,0,
                                            1,1,0, 0,1,1, 0,0,0,
                                            0,0,0, 0,1,1, 1,1,0});

            bitmapsDiff.Add(new float[] {   0,0,0, 0,1,1, 1,1,0,
                                            1,1,0, 0,0,0, 1,1,0,
                                            1,1,0, 0,1,1, 0,0,0});

            bitmapsDiff.Add(new float[] {   1,1,0, 0,1,1, 0,0,0,
                                            0,0,0, 0,1,1, 1,1,0,
                                            1,1,0, 0,0,0, 1,1,0});

            bitmapsDiff.Add(new float[] {   0,1,1, 0,0,0, 0,1,1,
                                            0,1,1, 1,0,1, 0,0,0,
                                            0,0,0, 1,0,1, 0,1,1});

            bitmapsDiff.Add(new float[] {   0,0,0, 1,0,1, 0,1,1,
                                            0,1,1, 0,0,0, 0,1,1,
                                            0,1,1, 1,0,1, 0,0,0});

            bitmapsDiff.Add(new float[] {   0,1,1, 1,0,1, 0,0,0,
                                            0,0,0, 1,0,1, 0,1,1,
                                            0,1,1, 0,0,0, 0,1,1});

            bitmapsDiff.Add(new float[] {   1,0,1, 0,0,0, 1,0,1,
                                            1,0,1, 1,1,0, 0,0,0,
                                            0,0,0, 1,1,0, 1,0,1});

            bitmapsDiff.Add(new float[] {   0,0,0, 1,1,0, 1,0,1,
                                            1,0,1, 0,0,0, 1,0,1,
                                            1,0,1, 1,1,0, 0,0,0});

            bitmapsDiff.Add(new float[] {   1,0,1, 1,1,0, 0,0,0,
                                            0,0,0, 1,1,0, 1,0,1,
                                            1,0,1, 0,0,0, 1,0,1});
            bitmapsDiff.Add(new float[]{    1,1,0, 0,0,0, 1,1,0,
                                            1,1,0, 0,1,1, 0,0,0,
                                            0,0,0, 0,1,1, 1,1,0});

            bitmapsDiff.Add(new float[] {   0,0,0, 0,1,1, 1,1,0,
                                            1,1,0, 0,0,0, 1,1,0,
                                            1,1,0, 0,1,1, 0,0,0});

            bitmapsDiff.Add(new float[] {   1,1,0, 0,1,1, 0,0,0,
                                            0,0,0, 0,1,1, 1,1,0,
                                            1,1,0, 0,0,0, 1,1,0});

            bitmapsDiff.Add(new float[] {   0,1,1, 0,0,0, 0,1,1,
                                            0,1,1, 1,0,1, 0,0,0,
                                            0,0,0, 1,0,1, 0,1,1});

            bitmapsDiff.Add(new float[] {   0,0,0, 1,0,1, 0,1,1,
                                            0,1,1, 0,0,0, 0,1,1,
                                            0,1,1, 1,0,1, 0,0,0});

            bitmapsDiff.Add(new float[] {   0,1,1, 1,0,1, 0,0,0,
                                            0,0,0, 1,0,1, 0,1,1,
                                            0,1,1, 0,0,0, 0,1,1});

            bitmapsDiff.Add(new float[] {   1,0,1, 0,0,0, 1,0,1,
                                            1,0,1, 1,1,0, 0,0,0,
                                            0,0,0, 1,1,0, 1,0,1});

            bitmapsDiff.Add(new float[] {   0,0,0, 1,1,0, 1,0,1,
                                            1,0,1, 0,0,0, 1,0,1,
                                            1,0,1, 1,1,0, 0,0,0});

            bitmapsDiff.Add(new float[] {   1,0,1, 1,1,0, 0,0,0,
                                            0,0,0, 1,1,0, 1,0,1,
                                            1,0,1, 0,0,0, 1,0,1});
            bitmapsDiff.Add(new float[]{    1,1,0, 0,0,0, 1,1,0,
                                            1,1,0, 0,1,1, 0,0,0,
                                            0,0,0, 0,1,1, 1,1,0});

            bitmapsDiff.Add(new float[] {   0,0,0, 0,1,1, 1,1,0,
                                            1,1,0, 0,0,0, 1,1,0,
                                            1,1,0, 0,1,1, 0,0,0});

            bitmapsDiff.Add(new float[] {   1,1,0, 0,1,1, 0,0,0,
                                            0,0,0, 0,1,1, 1,1,0,
                                            1,1,0, 0,0,0, 1,1,0});

            bitmapsDiff.Add(new float[] {   0,1,1, 0,0,0, 0,1,1,
                                            0,1,1, 1,0,1, 0,0,0,
                                            0,0,0, 1,0,1, 0,1,1});

            bitmapsDiff.Add(new float[] {   0,0,0, 1,0,1, 0,1,1,
                                            0,1,1, 0,0,0, 0,1,1,
                                            0,1,1, 1,0,1, 0,0,0});

            bitmapsDiff.Add(new float[] {   0,1,1, 1,0,1, 0,0,0,
                                            0,0,0, 1,0,1, 0,1,1,
                                            0,1,1, 0,0,0, 0,1,1});

            bitmapsDiff.Add(new float[] {   1,0,1, 0,0,0, 1,0,1,
                                            1,0,1, 1,1,0, 0,0,0,
                                            0,0,0, 1,1,0, 1,0,1});

            bitmapsDiff.Add(new float[] {   0,0,0, 1,1,0, 1,0,1,
                                            1,0,1, 0,0,0, 1,0,1,
                                            1,0,1, 1,1,0, 0,0,0});

            bitmapsDiff.Add(new float[] {   1,0,1, 1,1,0, 0,0,0,
                                            0,0,0, 1,1,0, 1,0,1,
                                            1,0,1, 0,0,0, 1,0,1});

            //------------------------------------------------------
            //------------ all the same ----------------------------
            for (int j = 0; j < 9; j++)
            {
                bitmapsSame.Add(new float[]
                {
                    1, 1, 0,  0, 0, 0,  0, 1, 1,
                    1, 1, 0,  0, 1, 1,  0, 1, 1,
                    0, 0, 0,  0, 1, 1,  0, 0, 0
                });
            }

            //------------------------------------------------------
            //------------ very small change -----------------------
            for (int j = 0; j < 8; j++)
            {
                bitmapsSimilar.Add(new float[]
                {
                    1, 1, 0,  0, 0, 0,  0, 1, 1,
                    1, 1, 0,  0, 1, 1,  0, 1, 1,
                    0, 0, 0,  0, 1, 1,  0, 0, 0
                });
            }
            bitmapsSimilar.Add(new float[]
                {
                    1, 1, 0,  0, 0, 0,  0, 1, 1,
                    1, 1, 0,  0, 1, 1,  0, 1, 1,
                    0, 0, 0,  0, 1, 1,  1, 0, 0
                });

            //eo create sample data
            Statistics STAT = new Statistics(bitmapsDiff, size); // construct statistics
            STAT.Calc();
            Console.WriteLine("=============================================");
            Console.WriteLine("============== Different ====================");
            Console.WriteLine("RANGE: " + STAT.DELTA_RANGE);
            Console.WriteLine("avg distance: " + STAT.DELTA_AVG);
            Console.WriteLine("poisson lambda (bin index @ 10% steps): " + STAT.POISSON_LAMBDA);
            Console.WriteLine("poisson error (SIGNIGFIKANT AB 14.68): " + STAT.POISSON_ERROR);
            //STAT.PrintSmoothBMPs();
            Console.Write("\n");
            Console.Write("Deltas_HGRAM: \n");
            STAT.PrintDELTAS_HGRAM();
            STAT.PrintPOISSON_HGRAM();
            Console.Write("\n");
            Console.WriteLine("=============================================");
            Console.Write("\n");

            STAT = new Statistics(bitmapsSame, size); // construct statistics
            STAT.Calc();
            Console.WriteLine("=============================================");
            Console.WriteLine("================ Same =======================");
            Console.WriteLine("RANGE: " + STAT.DELTA_RANGE);
            Console.WriteLine("avg distance: " + STAT.DELTA_AVG);
            Console.WriteLine("poisson lambda (bin index @ 10% steps): " + STAT.POISSON_LAMBDA);
            Console.WriteLine("poisson error (SIGNIGFIKANT AB 14.68): " + STAT.POISSON_ERROR);
            //STAT.PrintSmoothBMPs();
            Console.Write("\n");
            Console.Write("Deltas_HGRAM: \n");
            STAT.PrintDELTAS_HGRAM();
            STAT.PrintPOISSON_HGRAM();
            Console.Write("\n");
            Console.WriteLine("=============================================");
            Console.Write("\n");

            STAT = new Statistics(bitmapsSimilar, size); // construct statistics
            STAT.Calc();
            Console.WriteLine("=============================================");
            Console.WriteLine("================ Similar =======================");
            Console.WriteLine("RANGE: " + STAT.DELTA_RANGE);
            Console.WriteLine("avg distance: " + STAT.DELTA_AVG);
            Console.WriteLine("poisson lambda (bin index @ 10% steps): " + STAT.POISSON_LAMBDA);
            Console.WriteLine("poisson error (SIGNIGFIKANT AB 14.68): " + STAT.POISSON_ERROR);
            //STAT.PrintSmoothBMPs();
            Console.Write("\n");
            Console.Write("Deltas_HGRAM: \n");
            STAT.PrintDELTAS_HGRAM();
            STAT.PrintPOISSON_HGRAM();
            Console.Write("\n");
            STAT.PrintDELTAS();
            Console.WriteLine("=============================================");

            Console.WriteLine("\n\nPress [enter] to close...");
            Console.ReadLine();
        }
    }

    public class Statistics
    {
        private List<float[]> bitmaps = new List<float[]>();
        private int size, length;
        private List<float[]> bitmapsSmooth = new List<float[]>();
        private float[] AVG, DELTAS, DELTAS_HGRAM, POISSON_HGRAM;
        public static float MIN_RANGE = 0.0001f;
        //private float DELTA_RANGE, DELTA_SUM, DELTA_AVG, POISSON_LAMBDA, POISSON_ERROR;

        public float DELTA_RANGE { get; set; }
        public float DELTA_SUM { get; set; }
        public float DELTA_AVG { get; set; }
        public float POISSON_LAMBDA { get; set; }
        public float POISSON_ERROR { get; set; }

        public Statistics(List<float[]> bitmaps, int size) // constructor
        {
            this.bitmaps = bitmaps;
            this.size = size;
            this.length = bitmaps.ElementAt(0).Length;
        }

        public void Calc()
        {
            CreateSmoothBMPs();
            CalcDELTAS();
            CalcHistogram();
        }

        // 1. step: apply gauss convolution kernel
        public void CreateSmoothBMPs()
        {
            foreach (float[] bmp in bitmaps)
            {
                float[] bitmapSmooth = Enumerable.Repeat(0f, length).ToArray(); // populates array with 0

                float[] GAUSS = new float[] { // 7x7 PRE-CALCULATED Gaussian filter kernel (with sigma = 0.84089642)
                0.00000067f, 0.00002292f, 0.00019117f, 0.00038771f, 0.00019117f, 0.00002292f, 0.00000067f, 
                0.00002292f, 0.00078634f, 0.00655965f, 0.01330373f, 0.00655965f, 0.00078633f, 0.00002292f, 
                0.00019117f, 0.00655965f, 0.05472157f, 0.11098164f, 0.05472157f, 0.00655965f, 0.00019117f, 
                0.00038771f, 0.01330373f, 0.11098164f, 0.22508352f, 0.11098164f, 0.01330373f, 0.00038771f, 
                0.00019117f, 0.00655965f, 0.05472157f, 0.11098164f, 0.05472157f, 0.00655965f, 0.00019117f, 
                0.00002292f, 0.00078633f, 0.00655965f, 0.01330373f, 0.00655965f, 0.00078633f, 0.00002292f, 
                0.00000067f, 0.00002292f, 0.00019117f, 0.00038771f, 0.00019117f, 0.00002292f, 0.00000067f
                };
                if (length < 200) GAUSS = new float[] { 0f, 0.1f, 0f, 0.1f, 0.6f, 0.1f, 0f, 0.1f, 0f }; // minimal testkernel
                // could be implemented via http://softwarebydefault.com/2013/06/08/calculating-gaussian-kernels/

                //smoothen bmp via one kernel per pixel
                int nG = (int)Math.Sqrt(GAUSS.Length); // kernel dimensions 
                int halfnG = nG / 2; // start at center of kernel!
                for (int i = 0; i < length; i++) // for all pixels 
                {
                    for (int gx = 0; gx < nG; gx++) // for all kernel columns
                    {
                        int x = i % size;
                        x = x + gx - halfnG;
                        for (int gy = 0; gy < nG; gy++) // for each kernel row
                        {
                            int y = i / size;
                            y = y + gy - halfnG;
                            if ((x + y * size) >= 0 && (x + y * size) < length)
                                bitmapSmooth[x + y * size] += bmp[i] * GAUSS[gx * nG + gy];
                        }
                    }
                }
                //add smooth bmp tp list
                bitmapsSmooth.Add(bitmapSmooth);
            }
        }
        public void PrintSmoothBMPs()
        {
            foreach (float[] bmp in bitmapsSmooth)
            {
                for (int i = 0; i < bmp.Length; i++)
                {
                    if (i % size == 0) Console.Write("\n");
                    Console.Write(String.Format("{0:0.00}", bmp[i]) + " ");
                }
                Console.Write("\n");
            }

        }

        // 2. step: calc AVG and DELTAS
        public void CalcDELTAS()
        {
            AVG = Enumerable.Repeat(0f, length).ToArray(); // populates array with 0
            for (int i = 0; i < length; i++)
            {
                foreach (float[] bmp in bitmapsSmooth) AVG[i] += bmp[i];
                AVG[i] /= bitmapsSmooth.Count;
            }

            DELTA_RANGE = 0;
            DELTA_SUM = 0;
            DELTAS = new float[bitmapsSmooth.Count];

            for (int i = 0; i < bitmapsSmooth.Count; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    DELTAS[i] += (float)Math.Pow(AVG[j] - bitmapsSmooth.ElementAt(i)[j], 2);
                }
                DELTAS[i] = (float)Math.Sqrt(DELTAS[i]);
                if (DELTAS[i] > DELTA_RANGE) DELTA_RANGE = DELTAS[i];
                DELTA_SUM += DELTAS[i];
            }
            DELTA_AVG = DELTA_SUM / bitmapsSmooth.Count;
        }

        // 3. step: make Histogram and calc poisson probability
        public void CalcHistogram()
        {
            int n = 10; // immer in 10% intervall
            float histogrammRange = (DELTA_RANGE < MIN_RANGE) ? MIN_RANGE : DELTA_RANGE;
            float widthInterval = histogrammRange / n;
            DELTAS_HGRAM = Enumerable.Repeat(0f, n).ToArray(); // populates array with 0

            foreach (float d in DELTAS)
            {
                for (int i = 0; i < n; i++)
                {
                    if (d > i * widthInterval && d <= (i + 1) * widthInterval)
                    {
                        DELTAS_HGRAM[i]++;
                        break;
                    }
                  }
                if (d <= 0) DELTAS_HGRAM[0]++;
                if (d > n * widthInterval) DELTAS_HGRAM[n - 1]++;
            }
            for (int i = 0; i < n; i++) DELTAS_HGRAM[i] /= DELTAS.Length; // normalizing histogram

            // calc integer for the interval the average difference is part of
            int avgBinIndex = (int)Math.Ceiling(10f * DELTA_AVG / histogrammRange) - 1;
            // this on is our lambdaestimation
            POISSON_LAMBDA = avgBinIndex;

            // evaluate by calc the difference between HGRAM and poisson
            POISSON_HGRAM = Enumerable.Repeat(0f, n).ToArray(); // populates array with 0
            double POISSON_ERROR_criticalValue = 0;
            for (int i = 0; i < n; i++)
            {
                POISSON_HGRAM[i] = (float)(Math.Pow(Math.E, POISSON_LAMBDA * (-1)) * Math.Pow(POISSON_LAMBDA, i) / Faculty(i));
                //POISSON_ERROR += (float)(Math.Pow((double)(DELTAS_HGRAM[i] - POISSON_HGRAM[i]), 2) / POISSON_HGRAM[i]);// http://de.wikipedia.org/wiki/Chi-Quadrat-Test
               // G-TEST für kleine stichproben robuster als CHI^2 test
                if (POISSON_HGRAM[i] > 0 && DELTAS_HGRAM[i]>0)
                    POISSON_ERROR_criticalValue += (float)(DELTAS_HGRAM[i] * 2 * Math.Log((DELTAS_HGRAM[i] / POISSON_HGRAM[i]), Math.E ));// http://en.wikipedia.org/wiki/G-test
            }
            // http://www.codeproject.com/Articles/432194/How-to-Calculate-the-Chi-Squared-P-Value           

            int dk = n - 1;
            POISSON_ERROR = (float)(1- chisqr(dk, POISSON_ERROR_criticalValue));
            POISSON_ERROR = (float) POISSON_ERROR_criticalValue;
        }

        private static double[] CHIQUANTILE_090 = new double[] {2.71,4.61,6.25,7.78,9.24,
                                                                10.64,12.02,13.36,14.68,15.99,
                                                                17.28,18.55,19.81,21.06,22.31,
                                                                23.54,24.77,25.99,27.2,28.41,
                                                                29.62,30.81,32.01,33.2,34.38,
                                                                35.56,36.74,37.92,39.09,40.26};

        private int Faculty(int f)
        {
            int fac = 1;
            for (int i = 1; i <= f; i++) fac *= i;
            return fac;
        }

        public void PrintDELTAS_HGRAM()
        {
            foreach (float i in DELTAS_HGRAM) Console.Write(String.Format("{0:0.0000}", i) + " ");
            Console.WriteLine("");
        }
        public void PrintPOISSON_HGRAM()
        {
            foreach (float i in POISSON_HGRAM) Console.Write(String.Format("{0:0.0000}", i) + " ");
            Console.WriteLine("");
        }
        public void PrintAVG()
        {
            for (int i = 0; i < AVG.Length; i++)
            {
                if (i % size == 0) Console.Write("\n");
                Console.Write(String.Format("{0:0.00}", AVG[i]) + " ");
            }
            Console.Write("\n");
        }
        public void PrintDELTAS()
        {
            foreach (float d in DELTAS) Console.Write(String.Format("{0:0.0000}", d) + "\n");
        }








double chisqr(int Dof, double Cv)
{
    //printf("Dof:  %i\n", Dof);
    //printf("Cv:  %f\n", Cv);
    if(Cv < 0 || Dof < 1)
    {
        return 0.0;
    }
	double K = ((double)Dof) * 0.5;
	double X = Cv * 0.5;
	if(Dof == 2)
	{
        return Math.Exp(-1.0 * X);
	}
	double PValue, Gam;
    double ln_PV;
    ln_PV = log_igf(K, X);

    Gam = approx_gamma(K);
    //Gam = lgammal(K);
    //Gam = log_gamma(K);

    ln_PV -= Gam;
    PValue = 1.0 - Math.Exp(ln_PV);

	return (double)PValue;

}


/*
	Returns the Natural Logarithm of the Incomplete Gamma Function
	
	I converted the ChiSqr to work with Logarithms, and only calculate 
	the finised Value right at the end.  This allows us much more accurate
	calculations.  One result of this is that I had to increase the Number 
	of Iterations from 200 to 1000.  Feel free to play around with this if 
	you like, but this is the only way I've gotten to work.  
	Also, to make the code easier to work it, I separated out the main loop.  
*/

static double log_igf(double S, double Z)
{
	if(Z < 0.0)
	{
		return 0.0;
	}
	double Sc, K;
    Sc = (Math.Log(Z) * S) - Z - Math.Log(S);

    K = KM(S, Z);

    return Math.Log(K) + Sc;
}

static double KM(double S, double Z)
{
	double Sum = 1.0;
	double Nom = 1.0;
	double Denom = 1.0;

	for(int I = 0; I < 1000; I++) // Loops for 1000 iterations
	{
		Nom *= Z;
		S++;
		Denom *= S;
		Sum += (Nom / Denom);
	}

    return Sum;
}


/*
	Incomplete Gamma Function

	No doubleer need as I'm now using the log_igf(), but I'll leave this here anyway.
*/

static double igf(double S, double Z)
{
	if(Z < 0.0)
	{
		return 0.0;
	}
    double Sc = ((double)(1.0) / (double) S);
	Sc *= Math.Pow(Z, S);
	Sc *= Math.Exp(-Z);  

	double Sum = 1.0;
	double Nom = 1.0;
	double Denom = 1.0;

	for(int I = 0; I < 200; I++) // 200
	{
		Nom *= Z;
		S++;
		Denom *= S;
		Sum += (Nom / Denom);
	}

	return Sum * Sc;
}

        
double gamma(double N)
{
    /*
        The constant SQRT2PI is defined as sqrt(2.0 * PI);
        For speed the constant is already defined in decimal
        form.  However, if you wish to ensure that you achieve
        maximum precision on your own machine, you can calculate
        it yourself using (sqrt(atan(1.0) * 8.0))
    */

	//const double SQRT2PI = sqrtl(atanl(1.0) * 8.0);
    const double SQRT2PI = 2.5066282746310005024157652848110452530069867406099383;

    double Z = (double)N;
    double Sc = Math.Pow((Z + A), (Z + 0.5));
	Sc *= Math.Exp(-1.0 * (Z + A));
    Sc /= Z;

	double F = 1.0;
	double Ck;
    double Sum = SQRT2PI;


	for(int K = 1; K < A; K++)
	{
	    Z++;
		Ck = Math.Pow(A - K, K - 0.5);
		Ck *= Math.Exp(A - K);
		Ck /= F;

		Sum += (Ck / Z);

		F *= (-1.0 * K);
	}

	return (double)(Sum * Sc);
}

private float A = 15;
double log_gamma(double N)
{
    /*
        The constant SQRT2PI is defined as sqrt(2.0 * PI);
        For speed the constant is already defined in decimal
        form.  However, if you wish to ensure that you achieve
        maximum precision on your own machine, you can calculate
        it yourself using (sqrt(atan(1.0) * 8.0))
    */

	//const double SQRT2PI = sqrtl(atanl(1.0) * 8.0);
    const double SQRT2PI = 2.5066282746310005024157652848110452530069867406099383;

    double Z = (double)N;
    double Sc;

    Sc = (Math.Log(Z + A) * (Z + 0.5)) - (Z + A) - Math.Log(Z);

	double F = 1.0;
	double Ck;
    double Sum = SQRT2PI;


	for(int K = 1; K < A; K++)
	{
	    Z++;
		Ck = Math.Pow(A - K, K - 0.5);
		Ck *= Math.Exp(A - K);
		Ck /= F;

		Sum += (Ck / Z);

		F *= (-1.0 * K);
	}

    return Math.Log(Sum) + Sc;
}

double approx_gamma(double Z)
{
    const double RECIP_E = 0.36787944117144232159552377016147;  // RECIP_E = (E^-1) = (1.0 / E)
    const double TWOPI = 6.283185307179586476925286766559;  // TWOPI = 2.0 * PI

    double D = 1.0 / (10.0 * Z);
    D = 1.0 / ((12 * Z) - D);
    D = (D + Z) * RECIP_E;
    D = Math.Pow(D, Z);
    D *= Math.Sqrt(TWOPI / Z);

    return D;
}

double approx_log_gamma(double N)
{
    const double LOGPIHALF = 0.24857493634706692717563414414545; // LOGPIHALF = (log10(PI) / 2.0)

    double D;

    D = 1.0 + (2.0 * N);
    D *= 4.0 * N;
    D += 1.0;
    D *= N;
    D = Math.Log10(D) * (1.0 / 6.0);
    D += N + (LOGPIHALF);
    D = (N * Math.Log(N)) - D;
    return D;

}


    }
}

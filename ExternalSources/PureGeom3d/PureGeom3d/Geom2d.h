#pragma once
#include <math.h>
#include "MyMath.h"

#ifndef Eps6
#define Eps6         0.000001
#endif

#ifndef Eps3
#define Eps3         0.001
#endif

#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

class CGeom2d
{
public:
   CGeom2d::CGeom2d(void)
   {
   };

   CGeom2d::~CGeom2d(void)
   {
   };


   //Typen
   struct Vec2d
   {
      double x;
      double y;

      Vec2d(double new_x, double new_y): x(new_x), y(new_y) {};
   };

   struct Line_2d
   {
      Vec2d p;
      Vec2d v;
   };

   typedef Vec2d Point_2d;

   //Variablen
   int Err_2d;

   //Methoden
   inline bool InLine_2d (const Point_2d P1, const Point_2d P2, const Point_2d P)
   {
      return ((min(P1.x, P2.x)<=P.x) && (P.x<=max(P1.x, P2.x)) && (min(P1.y, P2.y)<=P.y) && (P.y<=max(P1.y, P2.y)));
   }

   inline void Line_PP_2d (const Point_2d P1, const Point_2d P2, Line_2d &L)
   {
      L.v.x = P2.x - P1.x;
      L.v.y = P2.y - P1.y;
      L.p.x = P1.x;
      L.p.y = P1.y;
   }

   inline void Point_LL_2d (Line_2d L1, Line_2d L2, Point_2d & P)
   {
      double det;
      double l1vx, l1vy, l2vx, l2vy, l1d, l2d;
      double value;

      l1vx = -L1.v.y;
      l1vy = L1.v.x;
      value = sqrt(l1vx*l1vx + l1vy*l1vy);
      l1vx /= value;
      l1vy /= value;

      l2vx = -L2.v.y;
      l2vy = L2.v.x;
      value = sqrt(l2vx*l2vx + l2vy*l2vy);
      l2vx /= value;
      l2vy /= value;

      det = (l1vx * l2vy - l2vx * l1vy);
      if (det == 0.0f)
         Err_2d= 0;
      else
      {
         l1d = -L1.p.x*l1vx - L1.p.y*l1vy;
         l2d = -L2.p.x*l2vx - L2.p.y*l2vy;
         P.x = (l1vy*l2d - l2vy*l1d) / det;
         P.y = (l1vx*l2d - l2vx*l1d) / (-det);
         Err_2d= 1;
      }

/*
      Det= L1.v.x * L2.v.y - L2.v.x * L1.v.y;
      if (fabs(Det) < Eps6) 
         Err_2d= 0;
      else 
      {
         Det1= L1.v.x * L1.p.y - L1.v.y * L1.p.x;
         Det2= L2.v.x * L2.p.y - L2.v.y * L2.p.x;
         DetX= -L1.v.x * Det2 + L2.v.x * Det1;
         DetY= L2.v.y * Det1 - L1.v.y * Det2;
         P.x= DetX/Det;
         P.y= DetY/Det;
         Err_2d= 1;
      }
*/
   }
};

static CGeom2d Geom2d;
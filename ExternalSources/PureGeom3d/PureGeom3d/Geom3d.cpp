#include "Stdafx.h"
#include "Geom3d.h"
#include "OpenGLTesselator.h"

bool CGeom3d::In_Poly_Plane_3d(CGeom3d::Plane_3d ^plane, array<CGeom3d::Triangle ^>^ triangles, CGeom3d::Vec_3d ^ test_point, double rand)
{
    CGeom3d::Line_3d line;
    CGeom3d::Vec_3d cut_point;

    CGeom3d::Line_VP_3d(plane->GetNormale(), test_point, %line);
    CGeom3d::Point_LS_3d(%line, plane, %cut_point);

    for each (CGeom3d::Triangle ^ triangle in triangles)
        if (CGeom3d::In_Triangle_3d(triangle->m_pP1, 
                                    triangle->m_pP2, 
                                    triangle->m_pP3, %cut_point, rand))
            return true;

    return false;
}

bool CGeom3d::In_Poly_3d(array<array<Vec_3d ^>^> ^ points, Vec_3d ^test_point, double rand)
{
    Vec_3d normale, center;
    Plane_3d plane;
    double dist;

    //Check: Ist der Punkt in der Poly-Ebene ?
    if (points->Length == 0)
        return false;

    ComputePlane(points, %center, %normale);
    normale.Normalize();
    Plane_PN_3d(%center, %normale, %plane);
    dist=fabs(Dist_PS_3d(test_point, %plane));
    if (dist>rand)
        return false;

    //Polygon in der richtigen Ebene checken
	array<CGeom3d::Triangle ^>^ triangles;
	Tess::COpenGLTesselator::Tesselate(points, triangles);
    return In_Poly_Plane_3d(%plane, triangles, test_point, rand);
}

bool CGeom3d::Poly_In_Poly(array<array<Vec_3d ^>^> ^ testPoly, array<array<Vec_3d ^>^> ^ outerPoly, double rand)
{
	Vec_3d normale, center;
	Plane_3d plane;

	if (testPoly->Length == 0)
		return false;
	if (outerPoly->Length == 0)
		return false;

	ComputePlane(outerPoly, %center, %normale);
	normale.Normalize();
	Plane_PN_3d(%center, %normale, %plane);

	//Polygon in der richtigen Ebene checken
	array<CGeom3d::Triangle ^>^ trianglesTest;
	array<CGeom3d::Triangle ^>^ trianglesOuter;
	Tess::COpenGLTesselator::Tesselate(testPoly, trianglesTest);
	Tess::COpenGLTesselator::Tesselate(outerPoly, trianglesOuter);
	for each(Triangle ^tri in trianglesTest)
	{
		Vec_3d ^ schwerpunkt = (tri->m_pP1 + tri->m_pP2 + tri->m_pP3) / 3;
		if ((!In_Poly_Plane_3d(%plane, trianglesOuter, tri->m_pP1, rand)) ||
			(!In_Poly_Plane_3d(%plane, trianglesOuter, tri->m_pP2, rand)) ||
			(!In_Poly_Plane_3d(%plane, trianglesOuter, tri->m_pP3, rand)) ||
			(!In_Poly_Plane_3d(%plane, trianglesOuter, schwerpunkt, rand)))
			return false;
	}
	return true;
}

bool CGeom3d::In_Triangle_3d(Vec_3d ^triangle_p1, Vec_3d ^triangle_p2, Vec_3d ^triangle_p3, Vec_3d ^ test_point, double rand)
{
    double total_area, p1, p2, p3;
    
    if (InLine_3d(triangle_p1, triangle_p2, test_point, rand) ||
        InLine_3d(triangle_p2, triangle_p3, test_point, rand) ||
        InLine_3d(triangle_p3, triangle_p1, test_point, rand)) return true;

    AreaTriangle(triangle_p1, triangle_p2, triangle_p3, total_area);
    AreaTriangle(triangle_p1, triangle_p2, test_point, p1);
    AreaTriangle(triangle_p2, triangle_p3, test_point, p2);
    AreaTriangle(triangle_p3, triangle_p1, test_point, p3);

    return (fabs(((p1)+(p2)+(p3))/(total_area) - 1.0) <= Eps3);
}

void CGeom3d::AreaPoly(array<array<Vec_3d ^>^> ^ points, Double %area)
{
    double looparea;
    Vec_3d normale, base_point;
    Vec_3d vx, vy;
    bool start(true);

    ComputePlane(points, %base_point, %normale);
    Vekt_Rectangular(%normale, %vx);
    vy = vx.amul( normale );
    vx.Normalize();
    vy.Normalize();

    area = 0;
    for each(array<Vec_3d ^>^ vec3d_loop in points)
    {
        AreaLoopAngle(vec3d_loop, looparea, %vx, %vy, %normale);
        if (start)
        {
            start = false;
            area = fabs(looparea);
        }
        else
            area -= fabs(looparea);
    }
}

void CGeom3d::AreaPoly_OuterLoop(array<array<Vec_3d ^>^> ^ points, Double %area)
{
    double looparea;
    Vec_3d normale, base_point;
    Vec_3d vx, vy;
    bool start(true);

    ComputePlane(points, %base_point, %normale);
    Vekt_Rectangular(%normale, %vx);
    vy = vx.amul( normale );
    vx.Normalize();
    vy.Normalize();

    area = 0;
    for each(array<Vec_3d ^>^ vec3d_loop in points)
    {
        AreaLoopAngle(vec3d_loop, looparea, %vx, %vy, %normale);
        if (start)
        {
            start = false;
            area = fabs(looparea);
            return;
        }
        else
            area -= fabs(looparea);
    }
}

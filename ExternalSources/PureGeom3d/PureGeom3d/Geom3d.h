#pragma once

using namespace System;
using namespace System::IO;
using namespace System::Runtime::Serialization;

#include <math.h>
#include "MyMath.h"
#include "Geom2d.h"
#include <float.h>

#include <assert.h>


#ifndef Eps6
#define Eps6         0.000001
#endif


#ifndef Eps3
#define Eps3         0.001
#endif


public ref class CGeom3d
{
public:

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Static constructor. </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static CGeom3d::CGeom3d(void)
   {
   };

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Destructor. </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   CGeom3d::~CGeom3d(void)
   {
   };

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Vector 3d. </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   [Serializable]
   ref struct Vec_3d: public TVec3d, IComparable
   {
       Vec_3d(): TVec3d(0,0,0) {};
       Vec_3d(double x, double y, double z): TVec3d(x,y,z) {};
       Vec_3d(TVec3d v): TVec3d(v.x, v.y, v.z) {};
       Vec_3d operator = (TVec3d %b)
       {
           x = b.x;
           y = b.y;
           z = b.z;

           return *this;
       }

       static Vec_3d ^ operator + (Vec_3d ^a, Vec_3d ^b)
       {
           return gcnew Vec_3d(a->x + b->x, 
                               a->y + b->y, 
                               a->z + b->z);
       }

       static Vec_3d ^ operator - (Vec_3d ^a, Vec_3d ^b)
       {
           return gcnew Vec_3d(a->x - b->x, 
                               a->y - b->y, 
                               a->z - b->z);
       }

       static Vec_3d ^ operator * (Vec_3d ^a, Vec_3d ^b)
       {
           return gcnew Vec_3d(a->x * b->x, 
                               a->y * b->y, 
                               a->z * b->z);
       }

       static Vec_3d ^ operator * (Vec_3d ^a, double b)
       {
           return gcnew Vec_3d(a->x * b, 
                               a->y * b, 
                               a->z * b);
       }

       static Vec_3d ^ operator * (double b, Vec_3d ^a)
       {
           return gcnew Vec_3d(a->x * b, 
                               a->y * b, 
                               a->z * b);
       }

       static Vec_3d ^ operator / (Vec_3d ^a, double b)
       {
           return gcnew Vec_3d(a->x / b, 
                               a->y / b, 
                               a->z / b);
       }

	   ////////////////////////////////////////////////////////////////////////////////////////////////////
	   /// <summary>	Calculates the Scalar Vector Product. </summary>
	   ///
	   /// <remarks>	Christian, 05.12.2012. </remarks>
	   ///
	   /// <param name="b">	[in] The other vector. </param>
	   ///
	   /// <returns>	scalar vector product. </returns>
	   ////////////////////////////////////////////////////////////////////////////////////////////////////

	   double imul(Vec_3d ^b)
       {
           return imul(*b);
       }

       ////////////////////////////////////////////////////////////////////////////////////////////////////
       /// <summary>	Calculates the Cross Vector Product. </summary>
       ///
       /// <remarks>	Christian, 05.12.2012. </remarks>
       ///
       /// <param name="b">	[in] The other vector. </param>
       ///
       /// <returns>	cross vector product. </returns>
       ////////////////////////////////////////////////////////////////////////////////////////////////////

       Vec_3d ^amul(Vec_3d ^b)
       {
           return gcnew Vec_3d(amul(*b));
       }

       ////////////////////////////////////////////////////////////////////////////////////////////////////
       /// <summary>	Calculates the Determinant. </summary>
       ///
       /// <remarks>	Christian, 05.12.2012. </remarks>
       ///
       /// <param name="m_x">	[in] the x vector. </param>
       /// <param name="m_y">	[in] the y vector. </param>
       /// <param name="m_z">	[in] the z vector. </param>
       ///
       /// <returns>	determinant. </returns>
       ////////////////////////////////////////////////////////////////////////////////////////////////////

       static double det(Vec_3d ^m_x, Vec_3d ^m_y, Vec_3d ^m_z)
       {
           return det(*m_x, *m_y, *m_z);
       }

       ////////////////////////////////////////////////////////////////////////////////////////////////////
       /// <summary>	Cramer's rule is an explicit formula for the solution of a system of linear equations. </summary>
       ///
       /// <remarks>	Christian, 05.12.2012. </remarks>
       ///
       /// <param name="v0">	[in] first new coordinate axis (x-axis). </param>
       /// <param name="v1">	[in] second new coordinate axis (y-axis). </param>
       /// <param name="v2">	[in] third new coordinate axis (z-axis). </param>
       /// <param name="t"> 	[in] the vector which should be transformed in the new coordinate system. </param>
       ///
       /// <returns>	the new transformed vector. </returns>
       ////////////////////////////////////////////////////////////////////////////////////////////////////

       static Vec_3d ^ Cramer(Vec_3d ^v0, Vec_3d ^v1, Vec_3d^ v2, Vec_3d^ t)
       {
           return gcnew Vec_3d(Cramer(*v0, *v1, *v2, *t));
       }

       ////////////////////////////////////////////////////////////////////////////////////////////////////
       /// <summary>	Calculates the Scalar Vector Product. </summary>
       ///
       /// <remarks>	Christian, 05.12.2012. </remarks>
       ///
       /// <param name="a">	[in] first vector. </param>
       /// <param name="b">	[in] second vector. </param>
       ///
       /// <returns>	scalar product. </returns>
       ////////////////////////////////////////////////////////////////////////////////////////////////////

       static double imul(Vec_3d ^a, Vec_3d ^b)
       {
           return a->imul(b);
       }

       ////////////////////////////////////////////////////////////////////////////////////////////////////
       /// <summary>	Calculates the Cross Vector Product. </summary>
       ///
       /// <remarks>	Christian, 05.12.2012. </remarks>
       ///
       /// <param name="a">	[in] first vector. </param>
       /// <param name="b">	[in] second vector. </param>
       ///
       /// <returns>	cross product. </returns>
       ////////////////////////////////////////////////////////////////////////////////////////////////////

       static Vec_3d ^ amul(Vec_3d ^a, Vec_3d ^b)
       {
           return a->amul(b);
       }

       ////////////////////////////////////////////////////////////////////////////////////////////////////
       /// <summary>	Cosine between two vectors. </summary>
       ///
       /// <remarks>	Christian, 05.12.2012. </remarks>
       ///
       /// <param name="a">	[in] first vector. </param>
       /// <param name="b">	[in] second vector. </param>
       ///
       /// <returns>	Cosine. </returns>
       ////////////////////////////////////////////////////////////////////////////////////////////////////

       static double cos(Vec_3d ^a, Vec_3d ^b)
       {
           return ::cos(*a,*b);
       }

       ////////////////////////////////////////////////////////////////////////////////////////////////////
       /// <summary>	Sinus between two vectors. </summary>
       ///
       /// <remarks>	Christian, 05.12.2012. </remarks>
       ///
       /// <param name="a">	[in] first vector. </param>
       /// <param name="b">	[in] second vector. </param>
       ///
       /// <returns>	. </returns>
       ////////////////////////////////////////////////////////////////////////////////////////////////////

       static double sin(Vec_3d ^a, Vec_3d ^b)
       {
           return ::sin(*a,*b);
       }

	   virtual int CompareTo( System::Object ^ other )
	   {
		   Vec_3d ^pOtherPoint = dynamic_cast<Vec_3d ^>(other);
		   if ((x < pOtherPoint->x) ||
			   ((x == pOtherPoint->x) && (y < pOtherPoint->y)) ||
			   ((x == pOtherPoint->x) && (y == pOtherPoint->y) && (z < pOtherPoint->z)))
			   return -1;
		   if ((x > pOtherPoint->x) ||
			   ((x == pOtherPoint->x) && (y > pOtherPoint->y)) ||
			   ((x == pOtherPoint->x) && (y == pOtherPoint->y) && (z > pOtherPoint->z)))
			   return 1;
		   return 0;
	   };

   };

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Calculates the Determinant. </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ///
   /// <param name="m_x">	[in] the x vector. </param>
   /// <param name="m_y">	[in] the y vector. </param>
   /// <param name="m_z">	[in] the z vector. </param>
   ///
   /// <returns>	Determinant. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static double det(Vec_3d ^m_x, Vec_3d ^m_y, Vec_3d ^m_z)
   {
       return ::det(*m_x, *m_y, *m_z);
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Cramer's rule is an explicit formula for the solution of a system of linear equations. </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ///
   /// <param name="v0">	[in] first new coordinate axis (x-axis). </param>
   /// <param name="v1">	[in] second new coordinate axis (y-axis). </param>
   /// <param name="v2">	[in] third new coordinate axis (z-axis). </param>
   /// <param name="t"> 	[in] the vector which should be transformed in the new coordinate system. </param>
   ///
   /// <returns>	the new transformed vector. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////
   
   static Vec_3d ^ Cramer(Vec_3d ^v0, Vec_3d ^v1, Vec_3d^ v2, Vec_3d^ t)
   {
       return gcnew Vec_3d(::Cramer(*v0, *v1, *v2, *t));
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Line_3d - a never ending line. </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   ref struct Line_3d
   {
	   Vec_3d ^ p;   //base point
       Vec_3d ^ v;   //direction vector

      Line_3d()
      {
          p = gcnew Vec_3d();
          v = gcnew Vec_3d();
      }
   };

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Plane_3d - a plane in Normal form. </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   ref struct Plane_3d
   {
      ////////////////////////////////////////////////////////////////////////////////////////////////////
      /// <summary>	Default constructor. </summary>
      ///
      /// <remarks>	Christian, 05.12.2012. </remarks>
      ////////////////////////////////////////////////////////////////////////////////////////////////////

      Plane_3d()
      {
      }

      ////////////////////////////////////////////////////////////////////////////////////////////////////
      /// <summary>	Constructor. </summary>
      ///
      /// <remarks>	Christian, 05.12.2012. </remarks>
      ///
      /// <param name="na">	The new a. </param>
      /// <param name="nb">	The new b. </param>
      /// <param name="nc">	The new c. </param>
      /// <param name="nd">	The new d. </param>
      ////////////////////////////////////////////////////////////////////////////////////////////////////

      Plane_3d(double na, double nb, double nc, double nd): a(na), b(nb), c(nc), d(nd)
      {
      }

      double a,b,c,d;  //(a,b,c) normal vector (d) distance to (0,0,0)

      ////////////////////////////////////////////////////////////////////////////////////////////////////
      /// <summary>	Gets the normal. </summary>
      ///
      /// <remarks>	Christian, 05.12.2012. </remarks>
      ///
      /// <returns>	the normal. </returns>
      ////////////////////////////////////////////////////////////////////////////////////////////////////

      Vec_3d ^ GetNormale()
      {
         return gcnew Vec_3d(a,b,c);
      };
   };

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Triangle. </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   ref struct Triangle
   {
	   Vec_3d ^ m_pP1, ^m_pP2, ^m_pP3; //the 3 triangle corners

	   ////////////////////////////////////////////////////////////////////////////////////////////////////
	   /// <summary>	Constructor. </summary>
	   ///
	   /// <remarks>	Christian, 05.12.2012. </remarks>
	   ///
	   /// <param name="p1">	[in] the first Vec_3d. </param>
	   /// <param name="p2">	[in] the second Vec_3d. </param>
	   /// <param name="p3">	[in] the third Vec_3d. </param>
	   ////////////////////////////////////////////////////////////////////////////////////////////////////

	   Triangle(Vec_3d ^ p1, Vec_3d ^p2, Vec_3d ^p3): m_pP1(p1), m_pP2(p2), m_pP3(p3)
	   {
	   }
   };

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Gets the CGeom3d error. Usually used by cut functions to return success or fail.</summary>
   ///
   /// <value>	The error code 3D. (0 = "not cutting", 1 = "cut successful") </value>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static mutable int Err_3d;

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Project Point on Z direction on plane S. </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ///
   /// <param name="S">	[in] input Plane_3d. </param>
   /// <param name="P">	[in,out] the Vec_3d to process. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Point_SXY_3d (Plane_3d ^S, Vec_3d ^ P)
   {
      if (fabs(S->c) < Eps6)
         Err_3d= 0;
      else 
      {
         P->z= -(S->a * P->x + S->b * P->y + S->d)/S->c;
         Err_3d= 1;
      }
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Project Point on Y direction on plane S. </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ///
   /// <param name="S">	[in] input Plane_3d. </param>
   /// <param name="P">	[in,out] the Vec_3d to process. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Point_SXZ_3d (Plane_3d ^S, Vec_3d ^P)
   {
      if (fabs(S->b) < Eps6)
         Err_3d= 0;
      else
      {
         P->y= -(S->a * P->x + S->c * P->z + S->d)/S->b;
         Err_3d= 1;
      }
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Project Point on Z direction on plane S.. </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ///
   /// <param name="S">	[in] input Plane_3d. </param>
   /// <param name="P">	[in,out] the Vec_3d to process. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Point_SYZ_3d (Plane_3d ^S, Vec_3d ^ P)
   {
      if (fabs(S->a) < Eps6)
         Err_3d= 0;
      else
      {
         P->x= -(S->b * P->y + S->c * P->z + S->d)/S->a;
         Err_3d= 1;
      }
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Cut Point of two lines. </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ///
   /// <param name="L1">   	[in] the first Line_3d. </param>
   /// <param name="L2">   	[in] the second Line_3d. </param>
   /// <param name="P">	   	[out] Vec_3d cut point. </param>
   /// <param name="delta">	tolerance value. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Point_LL_3d (Line_3d ^L1, Line_3d ^L2, Vec_3d ^ P, double delta)
   {
      //neu
      Vec_3d N;

      N = *L1->v->amul(L2->v);

      if (N.Value()<Eps6) 
      {
         //parallel
         Err_3d = 0;
      }
      else
      {
         //nicht parallel
         Plane_3d S1, S2;
         Vec_3d P1, P2;

         Plane_PVV_3d(L1->p, L1->v, %N, %S1);
         Plane_PVV_3d(L2->p, L2->v, %N, %S2);
         Point_LS_3d(L2, %S1, %P1);
         Point_LS_3d(L1, %S2, %P2);
         if  ((P1-P2).Value()<=delta)
         {
            Err_3d = 1;
            (*P) = (P1+P2)/2.0;
         }
         else
            Err_3d = 0;
      }
      //alt und funktioniert nicht (?):
/*
      double Dist;
      Plane_3d S;
      CGeom2d::Point_2d Pp;
      CGeom2d::Line_2d LL1,LL2;

      Dist= Dist_LL_3d (L1,L2);
      if (fabs(Dist) > delta)
         Err_3d= 0;
      else 
      {
         Plane_PVV_3d (L1.p,L1.v,L2.v,S);
         if ((fabs (S.a) > fabs (S.b)) && (fabs (S.a) > fabs (S.c)))
         {
            LL1.p.x= L1.p.y; LL1.p.y= L1.p.z;  LL1.v.x= L1.v.y; LL1.v.y= L1.v.z;
            LL2.p.x= L2.p.y; LL2.p.y= L2.p.z;  LL2.v.x= L2.v.y; LL2.v.y= L2.v.z;
            Geom2d.Point_LL_2d (LL1,LL2,Pp);
            P.y= Pp.x; P.z= Pp.y;
            Point_SYZ_3d (S,P);
         } else
         if ((fabs (S.b) > fabs (S.a)) && (fabs (S.b) > fabs (S.c)))  
         {
            LL1.p.x= L1.p.x; LL1.p.y= L1.p.z;  LL1.v.x= L1.v.x; LL1.v.y= L1.v.z;
            LL2.p.x= L2.p.x; LL2.p.y= L2.p.z;  LL2.v.x= L2.v.x; LL2.v.y= L2.v.z;
            Geom2d.Point_LL_2d (LL1,LL2,Pp);
            P.x= Pp.x; P.z= Pp.y;
            Point_SXZ_3d (S,P);
         } else
         if ((fabs (S.c) > fabs (S.a)) && (fabs (S.c) > fabs (S.b)))
         {
            LL1.p.x= L1.p.x; LL1.p.y= L1.p.y;  LL1.v.x= L1.v.x; LL1.v.y= L1.v.y;
            LL2.p.x= L2.p.x; LL2.p.y= L2.p.y;  LL2.v.x= L2.v.x; LL2.v.y= L2.v.y;
            Geom2d.Point_LL_2d (LL1,LL2,Pp);
            P.x= Pp.x; P.y= Pp.y;
            Point_SXY_3d (S,P);
         } else 
         {
            Err_3d= 0;
            return;
         }
         Err_3d=1;
      }
*/
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Cut Point of two lines. </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ///
   /// <param name="L1">   	[in] the first Line_3d. </param>
   /// <param name="L2">   	[in] the second Line_3d. </param>
   /// <param name="P">	   	[out] Vec_3d cut point. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Point_LL_3d (Line_3d ^L1, Line_3d ^L2, Vec_3d ^ P)
   {
      Point_LL_3d(L1, L2, P, Eps6);
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Cut Point of a line and a plane (surface). </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ///
   /// <param name="L">	[in] Line. </param>
   /// <param name="S">	[in] Plane. </param>
   /// <param name="P">	[out] Vec_3d cut point. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Point_LS_3d (Line_3d ^L, Plane_3d ^S, Vec_3d ^ P)
   {
      double q, r;

      q = S->a * L->v->x + S->b * L->v->y + S->c * L->v->z;
      if (fabs(q) < Eps6) { Err_3d = 0; return; }
      else
      {
         r = (S->a * L->p->x + S->b * L->p->y + S->c * L->p->z + S->d)/q;
         P->x = L->p->x - L->v->x * r;
         P->y = L->p->y - L->v->y * r;
         P->z = L->p->z - L->v->z * r;
         Err_3d = 1;
      }
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Defines a direction vector from two points. </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ///
   /// <param name="P1">	[in] the first Vec_3d. </param>
   /// <param name="P2">	[in] the second Vec_3d. </param>
   /// <param name="V"> 	[out] direction vector. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Vekt_PP_3d (Vec_3d ^P1, Vec_3d ^P2, Vec_3d ^ V)  { (*V) = (*P2) - (*P1); }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Cross product. </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ///
   /// <param name="V1">	[in] the first Vec_3d. </param>
   /// <param name="V2">	[in] the second Vec_3d. </param>
   /// <param name="V"> 	[out] cross product. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Vekt_ProdV_3d (Vec_3d ^V1, Vec_3d ^V2, Vec_3d ^ V) { (*V) = V1->amul(*V2); }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Scalar product. </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ///
   /// <param name="V1">	[in] the first Vec_3d. </param>
   /// <param name="V2">	[in] the second Vec_3d. </param>
   /// <param name="r"> 	[out] scalar product. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Vekt_ProdS_3d (Vec_3d ^V1, Vec_3d ^V2, double ^ r)  { (*r) = V1->imul(*V2); }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Generates a rectangular vector to another vector. </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ///
   /// <param name="V">  	[in] Vec_3d to process. </param>
   /// <param name="erg">	[out] the rectangular result. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Vekt_Rectangular(Vec_3d ^V, Vec_3d ^erg)
   {
      double dummy;
      (*erg)=(*V);

      if ((fabs(erg->x)<=fabs(erg->y)) && (fabs(erg->x)<=fabs(erg->z)))
      {
         //y und z tauschen
         dummy=erg->y;
         erg->y=erg->z;
         erg->z=-dummy;
         erg->x=0.0;
      }
      else
      if ((fabs(erg->y)<=fabs(erg->x)) && (fabs(erg->y)<=fabs(erg->z)))
      {
         //x und z tauschen
         dummy=erg->x;
         erg->x=erg->z;
         erg->z=-dummy;
         erg->y=0.0;
      }
      else
      {
         //x und y tauschen
         dummy=erg->x;
         erg->x=erg->y;
         erg->y=-dummy;
         erg->z=0.0;
      }
      erg->Normalize();
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Cut Line of two planes. </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ///
   /// <param name="S1">	[in] the first Plane_3d. </param>
   /// <param name="S2">	[in] the second Plane_3d. </param>
   /// <param name="L"> 	[out] the Cut Line_3d. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Line_SS_3d (Plane_3d ^S1, Plane_3d ^S2, Line_3d ^ L)
   {
      L->v->x= S1->b * S2->c - S1->c * S2->b;
      L->v->y= S1->c * S2->a - S1->a * S2->c;
      L->v->z= S1->a * S2->b - S1->b * S2->a;
      Err_3d= 1;
      if ((fabs(L->v->x)>=fabs(L->v->y)) && (fabs(L->v->x)>=fabs(L->v->z)) && (L->v->x != 0.0))
      {
         L->p->x= 0.0;
         L->p->y= (S1->c * S2->d - S2->c * S1->d)/(S1->b * S2->c - S2->b * S1->c);
         L->p->z= (S1->b * S2->d - S2->b * S1->d)/(S2->b * S1->c - S1->b * S2->c);
      } else 
      if ((fabs(L->v->y)>=fabs(L->v->x)) && (fabs(L->v->y)>=fabs(L->v->z)) && (L->v->y != 0.0))
      {
         L->p->x= (S1->c * S2->d - S2->c * S1->d)/(S1->a * S2->c - S2->a * S1->c);
         L->p->y= 0.0;
         L->p->z= (S1->a * S2->d - S2->a * S1->d)/(S2->a * S1->c - S1->a * S2->c);
      } else 
      if ((fabs(L->v->z)>=fabs(L->v->x)) && (fabs(L->v->z)>=fabs(L->v->y)) && (L->v->z != 0.0))
      {
         L->p->x= (S1->b * S2->d - S2->b * S1->d)/(S1->a * S2->b - S2->a * S1->b);
         L->p->y= (S1->a * S2->d - S2->a * S1->d)/(S2->a * S1->b - S1->a * S2->b);
         L->p->z= 0.0;
      } else Err_3d= 0;

      if (Err_3d==1)
      {
         Plane_3d ebene_null;
         Vec_3d new_point;

         L->v->Normalize();
         Plane_PN_3d(gcnew Vec_3d(0.0,0.0,0.0), L->v, %ebene_null);
         Point_LS_3d(L, %ebene_null, %new_point);
         L->p = gcnew Vec_3d(new_point);
      }
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Line defined by two points. </summary>
   ///
   /// <remarks>	Christian, 05.12.2012. </remarks>
   ///
   /// <param name="P1">	[in] the first Vec_3d. </param>
   /// <param name="P2">	[in] the second Vec_3d. </param>
   /// <param name="L"> 	[out] the Line_3d result. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Line_PP_3d (Vec_3d ^P1, Vec_3d ^P2, Line_3d ^ L)
   {
      L->v->x= P1->x - P2->x;
      L->v->y= P1->y - P2->y;
      L->v->z= P1->z - P2->z;
      L->p->x= P1->x;
      L->p->y= P1->y;
      L->p->z= P1->z;
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Line defined by a point and a vector 3D. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="V">	[in] direction vector. </param>
   /// <param name="P">	[in] point. </param>
   /// <param name="L">	[out] the Line_3d result. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Line_VP_3d (Vec_3d ^V,   Vec_3d ^P,  Line_3d ^ L)
   {
      L->v->x= V->x;
      L->v->y= V->y;
      L->v->z= V->z;
      L->p->x= P->x;
      L->p->y= P->y;
      L->p->z= P->z;
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Plane defined by a point and two vectors. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P"> 	[in] point. </param>
   /// <param name="V1">	[in] first vector. </param>
   /// <param name="V2">	[in] second vector. </param>
   /// <param name="S"> 	[out] the Plane_3d result. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Plane_PVV_3d(const Vec_3d ^P, const Vec_3d ^V1, const Vec_3d ^V2, Plane_3d ^ S)
   {
      Err_3d= 1;
      double value;

      S->a= V1->y * V2->z - V1->z * V2->y;
      S->b= V1->z * V2->x - V1->x * V2->z;
      S->c= V1->x * V2->y - V1->y * V2->x;

      value=sqrt(pow(S->a,2.0) + pow(S->b,2.0) + pow(S->c,2.0));
      if (value<=Eps6)
      {
         Err_3d=0;
         return;
      }
      S->a/=value;
      S->b/=value;
      S->c/=value;

      S->d= -P->x * S->a - P->y * S->b - P->z * S->c;
      if ((S->a == 0.0) && (S->b == 0.0) && (S->c == 0.0) && (S->d == 0.0))
         Err_3d= 0;
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Plane defined by a point and the normal vector./ </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P">	[in] point. </param>
   /// <param name="N">	[in] normal vector. </param>
   /// <param name="S">	[out] the Plane_3d result. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Plane_PN_3d (Vec_3d ^P, Vec_3d ^N, Plane_3d ^ S)
   {
      Err_3d= 1;

      double Len = N->Value();

      S->a= N->x/Len;
      S->b= N->y/Len;
      S->c= N->z/Len;
      S->d= -P->x * S->a - P->y * S->b - P->z * S->c;
      if ((S->a == 0.0) && (S->b == 0.0) && (S->c == 0.0) && (S->d == 0.0))
         Err_3d= 0;
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Length of a vector. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="V">	[in] vector. </param>
   ///
   /// <returns>	length. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline double Length_V_3d (Vec_3d ^V) { return V->Value();  }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Distance between two lines. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="L1">	[in] first line. </param>
   /// <param name="L2">	[in] second line. </param>
   ///
   /// <returns>	distance. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline double Dist_LL_3d (Line_3d ^L1, Line_3d ^L2)
   { 
      //neu
      Vec_3d N;

      Err_3d = 1;
      N = *L1->v->amul(L2->v);

      if (N.Value()<Eps6) 
      {
         //parallel
         Plane_3d S;
         Vec_3d P1, P2;
         double erg;

         Plane_PN_3d(L1->p, L1->v, %S);
         Point_LS_3d(L1, %S, %P1);
         Point_LS_3d(L2, %S, %P2);

         erg = (P1-P2).Value();
         if (erg == 0)
            //schneiden sich auf der ganzen Strecke
            Err_3d = 0;

         return erg;
      }
      else
      {
         //nicht parallel
         Plane_3d S1, S2;
         Vec_3d P1, P2;

         Plane_PVV_3d(L1->p, L1->v, %N, %S1);
         Plane_PVV_3d(L2->p, L2->v, %N, %S2);
         Point_LS_3d(L2, %S1, %P1);
         Point_LS_3d(L1, %S2, %P2);
         return (P1-P2).Value();
      }

      //alt und funktioniert nicht:
   /*
      Vec_3d V,R,N;
      double LenN,LenV,q;

      Vekt_PP_3d (L1.p,L2.p,R);
      Vekt_ProdV_3d (L1.v,L2.v,N);
      Vekt_ProdS_3d (R,N,q);
      LenN=Length_V_3d (N);
      Err_3d= 1;
      if (fabs(LenN) < Eps6) // if parallel
      { 
         Vekt_ProdS_3d (R,L1.v,q);
         LenV= Length_V_3d (L1.v);
         if (fabs(LenV) < Eps6)
         {
            Err_3d= 0; //schneiden sich auf der ganzen Strecke
            return 0.0;
         }
         else 
            return q/LenV;
      } 
      else // if not parallel 
         return q/LenN;
   */
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Distance between point and plane. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P">	[in] point. </param>
   /// <param name="S">	[in] plane. </param>
   ///
   /// <returns>	Positive or negative distance according to on which plane side the point is. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline double Dist_PS_3d (Vec_3d ^P, Plane_3d ^S)
   {
      double Len;

      Len= sqrt(S->a * S->a + S->b * S->b + S->c * S->c);
      if (fabs (Len)< Eps6) 
      {
         Err_3d=0;
         return 0.0;
      }
      else
      {
         S->a= S->a/Len;
         S->b= S->b/Len;
         S->c= S->c/Len;
         S->d= S->d/Len;
         Err_3d=1;
         return ( S->a * P->x + S->b * P->y + S->c * P->z + S->d) / Len;
      }
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Distance between two planes. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="S1">	[in] the first Plane_3d. </param>
   /// <param name="S2">	[in] the second Plane_3d. </param>
   ///
   /// <returns>	distance. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline double Dist_SS_3d (Plane_3d ^S1, Plane_3d ^S2)
   {
      double Len;

      Err_3d= 1;
      Len= sqrt(S1->a * S1->a + S1->b * S1->b + S1->c * S1->c);
      if (fabs (Len)< Eps6) { Err_3d= 0; return 0; }
      S1->a= S1->a/Len;
      S1->b= S1->b/Len;
      S1->c= S1->c/Len;
      S1->d= S1->d/Len;
      Len= sqrt(S2->a * S2->a + S2->b * S2->b + S2->c * S2->c);
      if (fabs (Len) < Eps6)  { Err_3d= 0; return 0; }
      S2->a= S2->a/Len;
      S2->b= S2->b/Len;
      S2->c= S2->c/Len;
      S2->d= S2->d/Len;
      if (fabs (S1->a - S2->a) > Eps6) { Err_3d= 0; return 0; }
      if (fabs (S1->b - S2->b) > Eps6) { Err_3d= 0; return 0; }
      if (fabs (S1->c - S2->c) > Eps6) { Err_3d= 0; return 0; }
      return (S1->d - S2->d);
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Distance between point and line 3D. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P">	[in] point. </param>
   /// <param name="L">	[in] line. </param>
   ///
   /// <returns>	distance. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline double Dist_PL_3d (Vec_3d ^P, Line_3d ^L)
   {
	   Plane_3d plane;
	   Vec_3d cut_point;

	   Plane_PN_3d(P, L->v, %plane);
	   Point_LS_3d(L, %plane, %cut_point);

	   return ((*P)-cut_point).Value();
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Projects a point on a plane along its normal vector. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P0">	point. </param>
   /// <param name="S"> 	plane. </param>
   /// <param name="P"> 	[out] projected result point. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Proj_N_3d (const Vec_3d ^P0, const Plane_3d ^S, Vec_3d ^P)
   {
///*
      double q,r;
      q= S->a * S->a + S->b * S->b + S->c * S->c;
      r= (S->a * P0->x + S->b * P0->y + S->c * P0->z + S->d)/q;
      P->x= P0->x - S->a * r;
      P->y= P0->y - S->b * r;
      P->z= P0->z - S->c * r;

//*/
/*
      CGeom3d::Line_3d line;

      Line_VP_3d(S.GetNormale(), P0, line);
      Point_LS_3d(line, S, P);
*/
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Projects a point on a plane along a given vector. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P0">	[in] point. </param>
   /// <param name="V"> 	[in] vector. </param>
   /// <param name="S"> 	[in] plane. </param>
   /// <param name="P"> 	[out] projected result point. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Proj_V_3d (Vec_3d ^P0, Vec_3d ^V, Plane_3d ^S, Vec_3d ^P)
   {
///*
      double q,r;
      q= S->a * V->x + S->b * V->y + S->c * V->z;
      if (fabs(q) < Eps6)  
         Err_3d= 0;
      else
      {
         r= (S->a * P0->x + S->b * P0->y + S->c * P0->z + S->d) / q;
         P->x= P0->x - V->x * r;
         P->y= P0->y - V->y * r;
         P->z= P0->z - V->z * r;
         Err_3d= 1;
      }
//*/
/*
      CGeom3d::Line_3d line;

      Line_VP_3d(V, P0, line);
      Point_LS_3d(line, S, P);
*/
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Rotates a point around the X axis. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P">	   	[in,out] the point. </param>
   /// <param name="sinwx">	sinus of the rotation angle. </param>
   /// <param name="coswx">	cosinus of the rotation angle. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Rot_X_3d ( Vec_3d ^ P, double sinwx, double coswx)
   {
      Vec_3d P0=*P;

      P->x = P0.x;
      P->y = P0.y * coswx - P0.z * sinwx;
      P->z = P0.y * sinwx + P0.z * coswx;
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Rotates a point around the Y axis. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P">	   	[in,out] the point. </param>
   /// <param name="sinwy">	sinus of the rotation angle. </param>
   /// <param name="coswy">	cosinus of the rotation angle. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Rot_Y_3d ( Vec_3d ^ P, double sinwy, double coswy)
   {
      Vec_3d P0=*P;

      P->x = P0.x * coswy + P0.z * sinwy;
      P->y = P0.y;
      P->z =-P0.x * sinwy + P0.z * coswy;
   }
   
   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Rotates a point around the Z axis. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P">	   	[in,out] the point. </param>
   /// <param name="sinwz">	sinus of the rotation angle. </param>
   /// <param name="coswz">	cosinus of the rotation angle. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Rot_Z_3d ( Vec_3d ^ P, double sinwz, double coswz)
   {
      Vec_3d P0=*P;

      P->x = P0.x * coswz - P0.y * sinwz;
      P->y = P0.x * sinwz + P0.y * coswz;
      P->z = P0.z;
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Rotates a point around a line. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P">	   	[in,out] point. </param>
   /// <param name="L">	   	[in] line. </param>
   /// <param name="sinwl">	sinus of the rotation angle. </param>
   /// <param name="coswl">	cosinus of the rotation angle. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Rot_L_3d ( Vec_3d ^ P, Line_3d ^L, double sinwl, double coswl)
   {
      double yz,xyz;

      (*P) = (*P) - *L->p;
      yz = sqrt ( pow(L->v->y,2) + pow(L->v->z,2) );
      if (yz>0)
         Rot_X_3d (P,L->v->y/yz,L->v->z/yz);
      xyz = sqrt ( pow(L->v->x,2) + pow(L->v->y,2) + pow(L->v->z,2) );
      if (xyz>0)
         Rot_Y_3d (P,-L->v->x/xyz,yz/xyz);
      Rot_Z_3d (P,sinwl,coswl);
      if (xyz>0)
         Rot_Y_3d (P,L->v->x/xyz,yz/xyz);
      if (yz>0)
         Rot_X_3d (P,-L->v->y/yz,L->v->z/yz);
      (*P) = (*P) + *L->p;
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Rotates a point around an given axis. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P">	   	[in,out] point. </param>
   /// <param name="A">	   	[in] axis vector. </param>
   /// <param name="sinwl">	sinus of the rotation angle. </param>
   /// <param name="coswl">	cosinus of the rotation angle. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline void Rot_Achse_3d ( Vec_3d ^ P, Vec_3d ^A, double sinwl, double coswl)
   {
      double yz,xyz;

      yz = sqrt ( pow(A->y,2) + pow(A->z,2) );
      if (yz>0)
         Rot_X_3d (P,A->y/yz,A->z/yz);
      xyz = sqrt ( pow(A->x,2) + pow(A->y,2) + pow(A->z,2) );
      if (xyz>0)
         Rot_Y_3d (P,-A->x/xyz,yz/xyz);
      Rot_Z_3d (P,sinwl,coswl);
      if (xyz>0)
         Rot_Y_3d (P,A->x/xyz,yz/xyz);
      if (yz>0)
         Rot_X_3d (P,-A->y/yz,A->z/yz);
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Checks if a point is on a line between two points. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P1">				 	[in] the first Vec_3d. </param>
   /// <param name="P2">				 	[in] the second Vec_3d. </param>
   /// <param name="P">					 	[in] the Vec_3d ^ to process. </param>
   /// <param name="Punktmindestabstand">	The points distance tolerance. </param>
   ///
   /// <returns>	true if P is on line, else false. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline bool InLine_3d (Vec_3d ^P1, Vec_3d ^P2, Vec_3d ^P, double Punktmindestabstand)
   {
      Vec_3d r1=(*P2)-(*P1);
      Vec_3d r2=(*P) -(*P1);

      if (r2.Value()>Punktmindestabstand)
      {
         double d = cos(r1,r2)*r2.Value();
         double h = sin(r1,r2)*r2.Value();

         return (d>=-Punktmindestabstand) && (d<=r1.Value()+Punktmindestabstand) && (h <= Punktmindestabstand);
      }
      else
         return true;
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Checks if a point is on a line between two points. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P1">				 	[in] the first Vec_3d. </param>
   /// <param name="P2">				 	[in] the second Vec_3d. </param>
   /// <param name="P">					 	[in] the Vec_3d ^ to process. </param>
   ///
   /// <returns>	true if P is on line, else false. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline bool InLine_3d (Vec_3d ^P1, Vec_3d ^P2, Vec_3d ^P)
   {
       return InLine_3d(P1, P2, P, Eps3);
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Checks if a point is on a line between two points without the end points. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P1">				 	[in] the first Vec_3d. </param>
   /// <param name="P2">				 	[in] the second Vec_3d. </param>
   /// <param name="P">					 	[in] the Vec_3d ^ to process. </param>
   /// <param name="Punktmindestabstand">	The points distance tolerance. </param>
   ///
   /// <returns>	true if P is on line, else false. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline bool InLineWithoutEndPoints_3d (Vec_3d ^P1, Vec_3d ^P2, Vec_3d ^P, double Punktmindestabstand)
   {
      Vec_3d r1=(*P2)-(*P1);
      Vec_3d r2=(*P) -(*P1);

      if (r2.Value()>Punktmindestabstand)
      {
         double d = cos(r1,r2)*r2.Value();
         double h = sin(r1,r2)*r2.Value();

         return (d>=Punktmindestabstand) && (d<=r1.Value()-Punktmindestabstand) && (h <= Punktmindestabstand);
      }
      else
         return false;
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Checks if a point is on a line between two points without the end points. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P1">				 	[in] the first Vec_3d. </param>
   /// <param name="P2">				 	[in] the second Vec_3d. </param>
   /// <param name="P">					 	[in] the Vec_3d ^ to process. </param>
   ///
   /// <returns>	true if P is on line, else false. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline bool InLineWithoutEndPoints_3d (Vec_3d ^P1, Vec_3d ^P2, Vec_3d ^P)
   {
       return InLineWithoutEndPoints_3d(P1, P2, P, Eps3);
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Checks if a point is on the upper side of a line in XY-plane. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P"> 	[in] point to check. </param>
   /// <param name="P1">	[in] point 1 of the line. </param>
   /// <param name="P2">	[in] point 2 of the line. </param>
   ///
   /// <returns>	true if it's upper the line in XY, else false. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline bool UpperPP_XY(Vec_3d ^P, Vec_3d ^P1, Vec_3d ^P2)
   {
      double W;
      W = (P->x - P1->x) * (P2->y - P1->y) - (P2->x - P1->x) * (P->y - P1->y);
      if (W < 0) 
         return false; 
      else 
         return true;
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Gets a normal. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="points">	[in] the point loops. </param>
   ///
   /// <returns>	The normal. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline Vec_3d GetNormale(array<array<Vec_3d ^>^> ^ points)
   {
      Vec_3d v,c;
      ComputePlane(points,%c,%v);
      v.Normalize();

      return v;
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Gets a normal. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="PV">	[in] one point loop. </param>
   ///
   /// <returns>	The normal. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline Vec_3d GetNormale(array<Vec_3d ^> ^ PV)
   {
	   Vec_3d p1,p2,p3;
	   p1=*PV[0];
	   p2=*PV[1];
	   p3=*PV[2];
	   Vec_3d v1,v2,v;
      v1 = p1 - p2;
      v2 = p3 - p2;
      v = v1.amul(v2);
      v.Normalize();

      if (v.Value()<0.999)
      {
         Vec_3d c;
         ComputePlane(PV,%c,%v);
         v.Normalize();
      }

      if (v.Value()<0.999)
         assert(0);

      return v;
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Area of a sphere triangle. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="alpha"> 	The alpha corner angle. </param>
   /// <param name="beta">  	The beta corner angle. </param>
   /// <param name="gamma"> 	The gamma corner angle. </param>
   /// <param name="radius">	The sphere radius. </param>
   ///
   /// <returns>	area. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline double AreaSphereTriangle(const double alpha, const double beta, const double gamma, const double radius)
   {
       return (alpha + beta + gamma - PI) * pow(radius,2);
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Area of a sphere triangle. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P1">		 	[in] the first Vec_3d. </param>
   /// <param name="P2">		 	[in] the second Vec_3d. </param>
   /// <param name="P3">		 	[in] the third Vec_3d. </param>
   /// <param name="Mittelpunkt">	[in] the center of the sphere. </param>
   ///
   /// <returns>	area. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline double AreaSphereTriangle(Vec_3d ^P1, Vec_3d ^P2, Vec_3d ^P3, Vec_3d ^Mittelpunkt)
   {
       double w1, w2, w3, radius;
      Vec_3d v1, v2;
      Plane_3d plane;

      radius=(((*P1)-(*Mittelpunkt)).Value()+
              ((*P2)-(*Mittelpunkt)).Value()+
              ((*P3)-(*Mittelpunkt)).Value()) / 3.0;

      Plane_PN_3d(P1,gcnew Vec_3d((*P1)-(*Mittelpunkt)),%plane);
      Proj_N_3d(P2,%plane,%v1);
      Proj_N_3d(P3,%plane,%v2);
      v1-=(*P1);
      v2-=(*P1);
      w1=acos(cos(v1,v2));

      Plane_PN_3d(P2,gcnew Vec_3d((*P2)-(*Mittelpunkt)),%plane);
      Proj_N_3d(P3,%plane,%v1);
      Proj_N_3d(P1,%plane,%v2);
      v1-=(*P2);
      v2-=(*P2);
      w2=acos(cos(v1,v2));

      Plane_PN_3d(P3,gcnew Vec_3d((*P3)-(*Mittelpunkt)),%plane);
      Proj_N_3d(P1,%plane,%v1);
      Proj_N_3d(P2,%plane,%v2);
      v1-=(*P3);
      v2-=(*P3);
      w3=acos(cos(v1,v2));

      return AreaSphereTriangle(w1, w2, w3, radius);
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Area of a sphere triangle on a one unit sphere. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P1">		 	[in] the first Vec_3d. </param>
   /// <param name="P2">		 	[in] the second Vec_3d. </param>
   /// <param name="P3">		 	[in] the third Vec_3d. </param>
   ///
   /// <returns>	area. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline float AreaSphereTriangle_Normalized(Vec_3d ^P1, Vec_3d ^P2, Vec_3d ^P3)
   {
      double w1, w2, w3;//, radius;
      Vec_3d v1, v2;
      Plane_3d plane;
      const double double_PI = (const double)(PI);

      //Mittelpunkt Vec_3d(0,0,0)

      //radius=((P1-Mittelpunkt).Value()+
      //        (P2-Mittelpunkt).Value()+
      //        (P3-Mittelpunkt).Value()) / 3.0;
      //radius = 1.0f;

      Plane_PN_3d(P1,P1,%plane);
      Proj_N_3d(P2,%plane,%v1);
      Proj_N_3d(P3,%plane,%v2);
      v1-=(*P1);
      v2-=(*P1);
      w1=acos(cos(v1,v2));

      Plane_PN_3d(P2,P2,%plane);
      Proj_N_3d(P3,%plane,%v1);
      Proj_N_3d(P1,%plane,%v2);
      v1-=(*P2);
      v2-=(*P2);
      w2=acos(cos(v1,v2));

      Plane_PN_3d(P3,P3,%plane);
      Proj_N_3d(P1,%plane,%v1);
      Proj_N_3d(P2,%plane,%v2);
      v1-=(*P3);
      v2-=(*P3);
      w3=acos(cos(v1,v2));

      return (float)(w1 + w2 + w3 - double_PI);// * pow(radius,2);
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Area of a sphere quad. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P1">		 	[in] the first Vec_3d. </param>
   /// <param name="P2">		 	[in] the second Vec_3d. </param>
   /// <param name="P3">		 	[in] the third Vec_3d. </param>
   /// <param name="P4">		 	[in] the fourth Vec_3d. </param>
   /// <param name="Mittelpunkt">	[in] the sphere center. </param>
   ///
   /// <returns>	area. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline double AreaSphereQuad(Vec_3d ^P1, Vec_3d ^P2, Vec_3d ^P3, Vec_3d ^P4, Vec_3d ^Mittelpunkt) //konvex, d.h. nur Quadrat, Rechteck etc.
   {
       Vec_3d m1,m2;
       double area = AreaSphereTriangle(P1,P2,P3,%m1)+
                     AreaSphereTriangle(P1,P3,P4,%m2);
       (*Mittelpunkt) = (m1 + m2)/2;
       return area;
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Area of a sphere quad on a one unit radius sphere. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="P1">		 	[in] the first Vec_3d. </param>
   /// <param name="P2">		 	[in] the second Vec_3d. </param>
   /// <param name="P3">		 	[in] the third Vec_3d. </param>
   /// <param name="P4">		 	[in] the fourth Vec_3d. </param>
   ///
   /// <returns>	area. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static inline float AreaSphereQuad_Normalized(Vec_3d ^P1, Vec_3d ^P2, Vec_3d ^P3, Vec_3d ^P4) //konvex, d.h. nur Quadrat, Rechteck etc.
   {
      return AreaSphereTriangle_Normalized(P1,P2,P3)+
             AreaSphereTriangle_Normalized(P1,P3,P4);
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Area of a loop in XY-plane. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="poly">	[in] the polygon loop. </param>
   /// <param name="area">	[out] The area. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static void AreaLoopXY(array<Vec_3d ^> ^ poly, Double % area)
   {
      int i;
      Vec_3d pi,pj;

      area = 0;
      for (i=0; i<poly->Length; i++)
      {
         pi=*poly[i];
         if (i<poly->Length-1)
            pj=*poly[i+1];
         else
            pj=*poly[0];
         area+=(pi.x - pj.x) * (pi.y + pj.y);
      }
      area/=2;
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Area of a loop with given orientation in 3D (normale and x, Y axis). </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="poly">   	[in] the polygon loop. </param>
   /// <param name="area">   	[out] The area. </param>
   /// <param name="vx">	 	[in] X axis of polygon loop. </param>
   /// <param name="vy">	 	[in] Y axis of polygon loop. </param>
   /// <param name="normale">	[in] normal vector of polygon loop. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static void AreaLoopAngle(array<Vec_3d ^> ^ poly, Double %area, Vec_3d ^ vx, Vec_3d ^vy, Vec_3d ^normale)
   {
      int i;
      Vec_3d vpi,vpj,start_point;

      area = 0;

      //zur�ckdrehen
      vpj.x=poly[0]->x;
      vpj.y=poly[0]->y;
      vpj.z=poly[0]->z;
      start_point = vpj;
      vpj = ::Cramer(*vx, *vy, *normale, vpj - start_point);

      for (i=0; i<poly->Length; i++)
      {
         vpi=vpj;

         if (i<poly->Length-1)
            vpj=*poly[i+1];
         else
            vpj=*poly[0];

         //zur�ckdrehen
         vpj = ::Cramer(*vx, *vy, *normale, vpj - start_point);

         area += (vpi.x - vpj.x) * (vpi.y + vpj.y);
      }
      area /= 2;
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Area of a loop. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="poly">	[in] the polygon loop. </param>
   /// <param name="area">	[out] The area. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static void AreaLoop(array<Vec_3d ^> ^ poly, Double %area)
   {
      Vec_3d normale=GetNormale(poly);
      Vec_3d vx, vy;

      Vekt_Rectangular(%normale, %vx);
      vy = vx.amul( normale );
      vx.Normalize();
      vy.Normalize();

      AreaLoopAngle(poly, area, %vx, %vy, %normale);
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Area of a triangle. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="p1">  	[in] the first Vec_3d. </param>
   /// <param name="p2">  	[in] the second Vec_3d. </param>
   /// <param name="p3">  	[in] the third Vec_3d. </param>
   /// <param name="area">	[out] The area. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static void AreaTriangle(Vec_3d ^p1, Vec_3d ^p2, Vec_3d ^p3, Double %area)
   {
      double a = ((*p1)-(*p2)).Value();
      double b = ((*p2)-(*p3)).Value();
      double c = ((*p3)-(*p1)).Value();
      double s = (a+b+c)/2;

      //Heronsche Fl�chenformel
      area = sqrt( s*(s-a)*(s-b)*(s-c) );
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Calculates the area of a polygon. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="points">	[in] the polygon loops. </param>
   /// <param name="area">  	[out] The area. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static void AreaPoly(array<array<Vec_3d ^>^> ^ points, Double %area);

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Calculates the area of a polygons outer loop. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="points">	[in] the polygon loops. </param>
   /// <param name="area">  	[out] The area. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static void AreaPoly_OuterLoop(array<array<Vec_3d ^>^> ^ points, Double %area);

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Calculates the Jacobi matrix (the matrix of all first-order partial derivatives of a vector- or scalar-valued function with respect to another vector). </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="b">		[in] K matrix.</param>
   /// <param name="ov"> 	[out] determinants (?). </param>
   /// <param name="v"> 	[out] normal axis (?). </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static void Jacobi (double b[3][3], double (&ov) [3], double (&v) [3][3])
   {
      typedef size_t INDEX;
      static const size_t n=3;
      static double epsilon =1.0E-13;
      static double beta =1.0E-15;

      double a[n][n];

      for (INDEX i=0; i<n; i++)
      {
         for (INDEX j=0; j<n; j++)
         {
            v[i][j]=0;
            a[i][j]=b[i][j];
         }

         v[i][i]=1;
      }

      long unsigned int counter=0;

      while (1)
      {
         double sum=0;
         for (INDEX i=1; i<n; i++)
            for (INDEX j=0; j<i; j++)
               sum+=sqr(a[i][j]);

         if (2*sum<sqr(epsilon)||counter>500)
         {
            for (INDEX i=0; i<n; i++)
            {
               ov[i]=a[i][i];
            }
            
            break;
         }

         for (INDEX p=0; p<n-1; p++)
            for (INDEX q=p+1; q<n; q++)
            {
               if (fabs(a[q][p])<epsilon)
                  continue;

               double O=(a[q][q]-a[p][p])/(2*a[q][p]);
               
               double t=1;
               if (fabs(O)>beta)
                  t=1.0/(O+sgn(O)*sqrt(sqr(O)+1));

               double c=1.0/sqrt(1+sqr(t));
               double s=c*t;
               double r=s/(1.0+c);
               a[p][p]-=t*a[q][p];
               a[q][q]+=t*a[q][p];
               a[q][p]=0;

               for (INDEX j=0; j<p; j++)
               {
                  double g=a[q][j]+r*a[p][j];
                  double h=a[p][j]-r*a[q][j];
                  a[p][j]-=s*g;
                  a[q][j]+=s*h;
               }

               for (INDEX i=p+1; i<q; i++)
               {
                  double g=a[q][i]+r*a[i][p];
                  double h=a[i][p]-r*a[q][i];
                  a[i][p]-=s*g;
                  a[q][i]+=s*h;
               }

               for (INDEX i=q+1; i<n; i++)
               {
                  double g=a[i][q]+r*a[i][p];
                  double h=a[i][p]-r*a[i][q];
                  a[i][p]-=s*g;
                  a[i][q]+=s*h;
               }

               for (INDEX i=0; i<n; i++)
               {
                  double g=v[i][q]+r*v[i][p];
                  double h=v[i][p]-r*v[i][q];
                  v[i][p]-=s*g;
                  v[i][q]+=s*h;
               }
            }

         counter++;
      }
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Calculates the Jacobi matrix (the matrix of all first-order partial derivatives of a vector- or scalar-valued function with respect to another vector). </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="b">		[in] K matrix.</param>
   /// <param name="ov"> 	[out] determinants (?). </param>
   /// <param name="v"> 	[out] normal axis (?). </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static void Jacobi2 (/*double b[3][3]*/ double *b, double (&ov) [3], double (&v) [3][3])
   {
       typedef size_t INDEX;
       static const size_t n=3;
       static double epsilon =1.0E-13;
       static double beta =1.0E-15;

       double a[n][n];

       for (INDEX i=0; i<n; i++)
       {
           for (INDEX j=0; j<n; j++)
           {
               v[i][j]=0;
               a[i][j]=b[i*n + j];
           }

           v[i][i]=1;
       }

       long unsigned int counter=0;

       while (1)
       {
           double sum=0;
           for (INDEX i=1; i<n; i++)
               for (INDEX j=0; j<i; j++)
                   sum+=sqr(a[i][j]);

           if (2*sum<sqr(epsilon)||counter>500)
           {
               for (INDEX i=0; i<n; i++)
               {
                   ov[i]=a[i][i];
               }

               break;
           }

           for (INDEX p=0; p<n-1; p++)
               for (INDEX q=p+1; q<n; q++)
               {
                   if (fabs(a[q][p])<epsilon)
                       continue;

                   double O=(a[q][q]-a[p][p])/(2*a[q][p]);

                   double t=1;
                   if (fabs(O)>beta)
                       t=1.0/(O+sgn(O)*sqrt(sqr(O)+1));

                   double c=1.0/sqrt(1+sqr(t));
                   double s=c*t;
                   double r=s/(1.0+c);
                   a[p][p]-=t*a[q][p];
                   a[q][q]+=t*a[q][p];
                   a[q][p]=0;

                   for (INDEX j=0; j<p; j++)
                   {
                       double g=a[q][j]+r*a[p][j];
                       double h=a[p][j]-r*a[q][j];
                       a[p][j]-=s*g;
                       a[q][j]+=s*h;
                   }

                   for (INDEX i=p+1; i<q; i++)
                   {
                       double g=a[q][i]+r*a[i][p];
                       double h=a[i][p]-r*a[q][i];
                       a[i][p]-=s*g;
                       a[q][i]+=s*h;
                   }

                   for (INDEX i=q+1; i<n; i++)
                   {
                       double g=a[i][q]+r*a[i][p];
                       double h=a[i][p]-r*a[i][q];
                       a[i][p]-=s*g;
                       a[i][q]+=s*h;
                   }

                   for (INDEX i=0; i<n; i++)
                   {
                       double g=v[i][q]+r*v[i][p];
                       double h=v[i][p]-r*v[i][q];
                       v[i][p]-=s*g;
                       v[i][q]+=s*h;
                   }
               }

               counter++;
       }
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Gets a normal out of a K matrix. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="a">		[in] the K Matrix. </param>
   /// <param name="x">	   	[out] X of normal. </param>
   /// <param name="y">	   	[out] Y of normal. </param>
   /// <param name="z">	   	[out] Z of normal. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static void GetNorm (double (&a)[3][3], double &x, double &y, double &z)
   {
      double v[3];
      double vec[3][3];
      
      Jacobi(a,v,vec);
      
      unsigned char i;

      const double epsilon = 1.0E-10;
      if (v[0]>v[1])
         if (v[1]>v[2])
         {
            if (v[1]<v[2]+epsilon)
            {
               x=y=z=0;
               return;
            }
            i=2;
         }
         else
         {
            if (v[1]+epsilon>v[2])
            {
               x=y=z=0;
               return;
            }
            i=1;
         }
      else
         if (v[0]>v[2])
         {
            if (v[0]<v[2]+epsilon)
            {
               x=y=z=0;
               return;
            }
            i=2;
         }
         else
         {
            if (v[0]+epsilon>v[2])
            {
               x=y=z=0;
               return;
            }
            i=0;
         }

      x=vec[0][i];
      y=vec[1][i];
      z=vec[2][i];
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Gets a normal out of a K matrix. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="a">		[in] the K Matrix. </param>
   /// <param name="x">	   	[out] X of normal. </param>
   /// <param name="y">	   	[out] Y of normal. </param>
   /// <param name="z">	   	[out] Z of normal. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static void GetNorm2 (/*double (&a)[3][3]*/double *a, double &x, double &y, double &z)
   {
       double v[3];
       double vec[3][3];

       Jacobi2(a,v,vec);

       unsigned char i;

       const double epsilon = 1.0E-10;
       if (v[0]>v[1])
           if (v[1]>v[2])
           {
               if (v[1]<v[2]+epsilon)
               {
                   x=y=z=0;
                   return;
               }
               i=2;
           }
           else
           {
               if (v[1]+epsilon>v[2])
               {
                   x=y=z=0;
                   return;
               }
               i=1;
           }
       else
           if (v[0]>v[2])
           {
               if (v[0]<v[2]+epsilon)
               {
                   x=y=z=0;
                   return;
               }
               i=2;
           }
           else
           {
               if (v[0]+epsilon>v[2])
               {
                   x=y=z=0;
                   return;
               }
               i=0;
           }

           x=vec[0][i];
           y=vec[1][i];
           z=vec[2][i];
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Calculates the k matrix. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="size"> 	[in] The size. </param>
   /// <param name="A">	   	[in] The A matrix. </param>
   /// <param name="K">		[out] The K matrix. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static void ComputeKMatrix (const size_t size, const double * A, double (&K)[3][3])
   {
      for (unsigned int l1=0; l1<3; l1++)
         for (unsigned int l2=0; l2<3; l2++)
         {
            K[l1][l2]=0;
            for (unsigned l3=0; l3<size; l3++)
               K[l1][l2]+=A[l3*3+l1]*A[l3*3+l2];
         }
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Calculates the k matrix. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="size"> 	[in] The size. </param>
   /// <param name="A">	   	[in] The A matrix. </param>
   /// <param name="K">		[out] The K matrix. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static void ComputeKMatrix2 (const size_t size, const double * A, /*double (&K)[3][3]*/ double *K)
   {
       for (unsigned int l1=0; l1<3; l1++)
           for (unsigned int l2=0; l2<3; l2++)
           {
               K[l1*3 + l2]=0;
               for (unsigned l3=0; l3<size; l3++)
                   K[l1*3 + l2]+=A[l3*3+l1]*A[l3*3+l2];
           }
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Calculates a plane. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="points">	[in] the polygon loops. </param>
   /// <param name="center">	[out] the center. </param>
   /// <param name="vec">   	[out] the normal vector. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static void ComputePlane (array<array<Vec_3d ^>^> ^ points, Vec_3d ^ center, Vec_3d ^ vec)
   {
      size_t size = 0;

      for each(array<Vec_3d ^> ^ vector in points)
          size += vector->Length;

      double * A  = new double [size*3];
      (*center) = Vec_3d(0,0,0);
      size_t position = 0;

      for each(array<Vec_3d ^> ^ vector in points)
      {
         for each(Vec_3d ^ point in vector)
         {
            center->x+=A[position++]=point->x;
            center->y+=A[position++]=point->y;
            center->z+=A[position++]=point->z;
         }
      }

      (*center)/=size;

      position = 0;
      for (size_t l3=0; l3<size; l3++)
      {
         A[position++]-=center->x;
         A[position++]-=center->y;
         A[position++]-=center->z;
      }

      double K[3][3];
      ComputeKMatrix(size,A,K);
      double x,y,z;
      GetNorm(K,x,y,z);
      delete A;
      (*vec)= Vec_3d(x,y,z);
   }
   

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Calculates a plane. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="points">	[in] one polygon loop. </param>
   /// <param name="center">	[out] the center. </param>
   /// <param name="vec">   	[out] the normal vector. </param>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static void ComputePlane (array<Vec_3d ^> ^ points, Vec_3d ^ center, Vec_3d ^ vec)
   {
      size_t size = points->Length;

      double * A  = new double [size*3];
      *center = Vec_3d(0,0,0);
      size_t position = 0;

      for each(Vec_3d ^ point in points)
      {
         center->x+=A[position++]=point->x;
         center->y+=A[position++]=point->y;
         center->z+=A[position++]=point->z;
      }

      (*center)/=size;

      position = 0;
      for (size_t l3=0; l3<size; l3++)
      {
         A[position++]-=center->x;
         A[position++]-=center->y;
         A[position++]-=center->z;
      }

      double K[3][3];
      ComputeKMatrix(size,A,K);
      double x,y,z;
      GetNorm(K,x,y,z);
      delete A;
      (*vec)=Vec_3d(x,y,z);
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Gets the XY point angle to the origin. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="point">	[in] the point. </param>
   ///
   /// <returns>	The XY angle. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static double GetXYAngle(Vec_3d ^point)
   {
      double angle;

      if (fabs(point->y)>Eps6)
         angle=atan(-point->x/point->y);
      else
      {
         if (point->x<0) angle=PI/2.0;
         else           angle=3*PI/2.0;
      }
      if (point->y<0)    angle+=PI;

      while (angle<0)   angle += 2*PI;

      return angle;
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Gets the 2PI angle to the upVector. </summary>
   ///
   /// <remarks>	Christian, 16.10.2013. </remarks>
   ///
   /// <param name="v1">	[in] the vector 1. </param>
   /// <param name="v2">	[in] the vector 2. </param>
   /// <param name="upVector">	[in] the upVector. </param>
   ///
   /// <returns>	The 2PI angle. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static double GetAngle(Vec_3d^ v1, Vec_3d^ v2, Vec_3d ^upVector)
   {
	   upVector->Normalize();
	   Vec_3d vx, vy;
	   Vekt_Rectangular(upVector, %vx);
       vy = vx.amul( *upVector );
       vx.Normalize();
       vy.Normalize();

	   Vec_3d tv1 = ::Cramer(vx, vy, *upVector, *v1);
	   Vec_3d tv2 = ::Cramer(vx, vy, *upVector, *v2);
	   double result = GetXYAngle(%tv2) - GetXYAngle(%tv1);
	   while ( result < 0 )
		   result += 2*PI;
	   while ( result > 2*PI )
		   result -= 2*PI;
	   return result;
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Checks if a point is in a polygon. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="points">		[in] the polygon loops. </param>
   /// <param name="test_point">	[in] the test point. </param>
   /// <param name="rand">			the point tolerance distance. </param>
   ///
   /// <returns>	true if it's in the polygon, else false. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static bool In_Poly_3d(array<array<Vec_3d ^>^> ^ points, Vec_3d ^test_point, double rand);

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Checks if a point is in a polygon. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="points">		[in] the polygon loops. </param>
   /// <param name="test_point">	[in] the test point. </param>
   ///
   /// <returns>	true if it's in the polygon, else false. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static bool In_Poly_3d(array<array<Vec_3d ^>^> ^ points, Vec_3d ^test_point)
   {
       return In_Poly_3d(points, test_point, Eps3);
   }

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Projects the test point on the plane and tests if it is inside the given triangles. </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="plane">			[in] the plane. </param>
   /// <param name="triangles"> 	[in] the triangles. </param>
   /// <param name="test_point">	[in] the test point. </param>
   /// <param name="rand">			the point tolerance distance. </param>
   ///
   /// <returns>	true if it's in the triangles, else false. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static bool In_Poly_Plane_3d(CGeom3d::Plane_3d ^plane, array<Triangle ^>^ triangles, CGeom3d::Vec_3d ^ test_point, double rand);

   ////////////////////////////////////////////////////////////////////////////////////////////////////
   /// <summary>	Checks if a point is in a triangle./ </summary>
   ///
   /// <remarks>	Christian, 06.12.2012. </remarks>
   ///
   /// <param name="triangle_p1">	[in] the first triangle point. </param>
   /// <param name="triangle_p2">	[in] the second triangle point. </param>
   /// <param name="triangle_p3">	[in] the third triangle point. </param>
   /// <param name="test_point"> 	[in] the test point. </param>
   /// <param name="rand">		 	the point tolerance distance. </param>
   ///
   /// <returns>	true if it's in the triangle, else false. </returns>
   ////////////////////////////////////////////////////////////////////////////////////////////////////

   static bool In_Triangle_3d(Vec_3d ^triangle_p1, Vec_3d ^triangle_p2, Vec_3d ^triangle_p3, Vec_3d ^ test_point, double rand);

   static bool Poly_In_Poly(array<array<Vec_3d ^>^> ^ testPoly, array<array<Vec_3d ^>^> ^ outerPoly, double rand);

   static bool Poly_In_Poly(array<array<Vec_3d ^>^> ^ testPoly, array<array<Vec_3d ^>^> ^ outerPoly)
   {
	   return Poly_In_Poly(testPoly, outerPoly, Eps3);
   }

   static bool On_Poly_Border_3d(array<array<Vec_3d ^>^> ^ testPoly, Vec_3d ^ testPoint, double rand)
   {
	   for each(array<Vec_3d ^>^ loop in testPoly)
	   {
		   Vec_3d ^lastPoint = loop[loop->Length - 1];
		   for each ( Vec_3d ^ point in loop )
		   {
			   if (InLine_3d(lastPoint, point, testPoint, rand))
				   return true;
			   lastPoint = point;
		   }
	   }
	   return false;
   }
};

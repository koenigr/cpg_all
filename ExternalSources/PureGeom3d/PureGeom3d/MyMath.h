#pragma once


using namespace System;


#include <math.h>

const long double PI = 3.1415926535897932384626433832795;

// Berechet a^2
inline double sqr (double % a)
{
   return a*a;
}


// Berechnet signum(a) (Vorzeichen von a)
inline double sgn (double % a)
{
   return a<0 ? -1:1;
}


//template <class double> struct TVec3d
[Serializable]
public ref struct TVec3d
{
   double x,y,z;

   bool operator == (const TVec3d % b)
   {
      return x==b.x && y==b.y && z==b.z;
   }


   bool operator != (const TVec3d % b)
   {
      return x!=b.x || y!=b.y || z!=b.z;
   }

   TVec3d operator = (TVec3d %b)
   {
       x = b.x;
       y = b.y;
       z = b.z;

       return *this;
   }


   TVec3d ()
   { x=0; y=0; z=0;}

   TVec3d (TVec3d %b)
   { x = b.x; y = b.y; z = b.z; }

   TVec3d (const double % xt, const double % yt, const double % zt)
   { x=xt; y=yt; z=zt;}


   double Value2 ()
   {
	   return x*x+y*y+z*z;
   }


   double Value ()
   {
	   return sqrtl(Value2());
   }


   int IsZero ()
   { return !(x||y||z); }


   bool Normalize()
   {
      double s=Value();
      if (!s)
         return false;

      operator /=(s);
      return true;
   }


  TVec3d operator + (TVec3d % b)
  {
     return TVec3d (x+b.x,y+b.y,z+b.z);
  }


  TVec3d operator - (const TVec3d % b)
  {
     return TVec3d (x-b.x,y-b.y,z-b.z);
  }


  TVec3d operator += (const TVec3d % b)
  {
      x += b.x;
      y += b.y;
	  z += b.z;

	  return TVec3d (x,y,z);
  }


  TVec3d operator -= (const TVec3d % b)
  {
      x -= b.x;
	  y -= b.y;
	  z -= b.z;

	  return TVec3d (x,y,z);
  }


  TVec3d operator / (const double % b)
  {
     return TVec3d (x/b,y/b,z/b);
  }


  TVec3d operator * (const double % b)
  {
     return TVec3d (x*b,y*b,z*b);
  }


  TVec3d operator /= (const TVec3d % b)
  {
      x /= b.x;
	  y /= b.y;
	  z /= b.z;

	  return TVec3d (x,y,z);
  }


  TVec3d operator /= (const double % b)
  {
      x /= b;
	  y /= b;
	  z /= b;

	  return TVec3d (x,y,z);
  }


  TVec3d operator *= (const TVec3d % b)
  {
      x *= b.x;
	  y *= b.y;
	  z *= b.z;

	  return TVec3d (x,y,z);
  }


  double imul (const TVec3d % b)
  {
     return x*b.x + y*b.y + z*b.z;
  }


  TVec3d amul (const TVec3d % b)
  {
     return TVec3d (y*b.z-z*b.y,z*b.x-x*b.z,x*b.y-y*b.x);
  }

    static double det (TVec3d % m_x, TVec3d % m_y, TVec3d % m_z)
    {
       return m_x.x*(m_y.y*m_z.z-m_y.z*m_z.y)-m_x.y*(m_y.x*m_z.z-m_y.z*m_z.x)+m_x.z*(m_y.x*m_z.y-m_y.y*m_z.x);
    }


    static TVec3d Cramer (TVec3d % v0, TVec3d % v1, TVec3d % v2, TVec3d % t)
    {
       TVec3d tmp;
       double d = det(v0,v1,v2);

       tmp.x=det(t,v1,v2)/d;
       tmp.y=det(v0,t,v2)/d;
       tmp.z=det(v0,v1,t)/d;

       return tmp;
    }
};


inline TVec3d operator * (double % a, TVec3d % b)
{
	return b*a;
}


inline double imul(TVec3d % a, TVec3d % b)
{
   return a.imul(b);
}


inline TVec3d amul (TVec3d % a, TVec3d % b)
{
   return a.amul(b);
}


inline double cos (TVec3d % a, TVec3d % b)
{
   double erg = imul(a,b)/sqrt(a.Value2()*b.Value2());
   if (erg >  1.0) erg =  1.0;
   if (erg < -1.0) erg = -1.0;
   return erg;
}


inline double sin (TVec3d % a, TVec3d % b)
{
   double erg = sqrt((sqr(a.x*b.y-a.y*b.x)+sqr(a.y*b.z-a.z*b.y)+sqr(a.z*b.x-a.x*b.z))/(a.Value2()*b.Value2()));
   if (erg >  1.0) erg =  1.0;
   if (erg < -1.0) erg = -1.0;
   return erg;
}


typedef TVec3d TVec3d;


inline double det (TVec3d % m_x, TVec3d % m_y, TVec3d % m_z)
{
   return m_x.x*(m_y.y*m_z.z-m_y.z*m_z.y)-m_x.y*(m_y.x*m_z.z-m_y.z*m_z.x)+m_x.z*(m_y.x*m_z.y-m_y.y*m_z.x);
}


inline TVec3d Cramer (TVec3d % v0, TVec3d % v1, TVec3d % v2, TVec3d % t)
{
   TVec3d tmp;
   double d = det(v0,v1,v2);

   tmp.x=det(t,v1,v2)/d;
   tmp.y=det(v0,t,v2)/d;
   tmp.z=det(v0,v1,t)/d;

   return tmp;
}

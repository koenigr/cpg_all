#include "Stdafx.h"
#include <windows.h>
#include <gl\glu.h>
#include <vector>

#include "OpenGLTesselator.h"
#include "OpenGLMutex.h"

#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"GLU32.lib")
#ifdef WIN64
#pragma comment(lib,"glew32_x64.lib")
#else
#pragma comment(lib,"glew32.lib")
#endif

//using namespace OpenTK::Graphics;
using namespace Tess;

struct Help_struct
{
	GLdouble tmp[3];
};

struct baseTriangle
{	
	Help_struct p1, p2, p3;
};

typedef std::vector<baseTriangle> baseTriangleVector;

GLenum                  render_mode;
bool                    point_switch;
unsigned __int8         point_counter;
baseTriangle            triangle;
baseTriangleVector      triangleVector;


void CALLBACK BeginCallback(GLenum type)
{
	//Tesselation ohne OpenGL Ausgabe
	render_mode = type;
	//OpenGL::GL::Begin((OpenGL::BeginMode)type);

	point_counter=0;
	point_switch =false;
}


void CALLBACK EndCallback()
{
	//Tesselation ohne OpenGL Ausgabe
	//OpenGL::GL::End();
}

void CALLBACK VertexCallback(GLvoid *vertex)
{
	//Tesselation mit OpenGL Ausgabe
	const GLdouble * pointer= (GLdouble *) vertex;
	//OpenGL::GL::Vertex3(pointer[0], pointer[1], pointer[2]);

	switch (point_counter)
	{
	case 0:
		triangle.p1 = *((Help_struct *)vertex);
		point_counter++;
		break;
	case 1:
		triangle.p2 = *((Help_struct *)vertex);
		point_counter++;
		break;
	case 2:
		triangle.p3 = *((Help_struct *)vertex);
		triangleVector.push_back(triangle);
		point_counter++;
		break;
	case 3:
		switch(render_mode)
		{
		case GL_TRIANGLE_FAN:
			triangle.p2=triangle.p3;
			triangle.p3 = *((Help_struct *)vertex);
			triangleVector.push_back(triangle);
			break;
		case GL_TRIANGLE_STRIP:
			point_switch = !point_switch;
			triangle.p1=triangle.p2;
			triangle.p2=triangle.p3;
			triangle.p3 = *((Help_struct *)vertex);
			if (point_switch)
			{
				baseTriangle sw;
				sw.p1 = triangle.p2;
				sw.p2 = triangle.p1;
				sw.p3 = triangle.p3;
				triangleVector.push_back(sw);
			}
			else
			{
				triangleVector.push_back(triangle);
			}
			break;
		case GL_TRIANGLES:
			triangle.p1 = *((Help_struct *)vertex);
			point_counter = 1;
			break;
		default:
			int a=0;
			break;
		}
		break;
	}
}

COpenGLTesselator::Point ^ FindPoint(array<array<COpenGLTesselator::Point^>^>^ points, Help_struct & coords)
{
	for each(array<COpenGLTesselator::Point^>^ loop in points)
		for each(COpenGLTesselator::Point^ point in loop)
		{
			if ((fabs(coords.tmp[0] - (GLdouble)point->x) < 0.00001) &&
				(fabs(coords.tmp[1] - (GLdouble)point->z) < 0.00001) &&
				(fabs(coords.tmp[2] + (GLdouble)point->y) < 0.00001))
				return point;

		}
	return nullptr;
}

void COpenGLTesselator::Tesselate(array<array<Point^>^>^ points, array<Triangle^>^ %result)
{
	Visualisation::COpenGLMutex::EnterMutex();

	CGeom3d::Vec_3d normale, center;
	CGeom3d::ComputePlane(points, %center, %normale);

	triangleVector.clear();
	//OpenGL::GL::Normal3(normale.x,normale.z,-normale.y);

	GLUtesselator * pTess = gluNewTess();

	//Init Tesselator
	gluTessNormal(pTess, normale.x,normale.z,-normale.y);
	gluTessCallback(pTess,GLU_TESS_BEGIN,  (GLvoid (CALLBACK*)())&BeginCallback); 
	gluTessCallback(pTess,GLU_TESS_VERTEX, (GLvoid (CALLBACK*)())&VertexCallback); 
	gluTessCallback(pTess,GLU_TESS_END,    (GLvoid (CALLBACK*)())&EndCallback);
	gluTessProperty(pTess,GLU_TESS_BOUNDARY_ONLY, GL_FALSE);

	unsigned int point_number = 0, point_pos=0;
	for each(array<Point^>^ loop in points)
		point_number+=loop->GetLength(0);

	gluTessBeginPolygon(pTess, NULL); 

	typedef std::vector<Help_struct> GLDV;
	GLDV tempGLdouble(point_number);

	for each(array<Point^>^ loop in points)
	{
		gluTessBeginContour(pTess);

		for each (Point^ pPoint in loop)
		{
			GLdouble coords[3];
			coords[0] =  pPoint->x;
			coords[1] =  pPoint->z;
			coords[2] = -pPoint->y;

			Help_struct help;
			help.tmp[0] = coords[0];
			help.tmp[1] = coords[1];
			help.tmp[2] = coords[2];

			tempGLdouble[point_pos] = help;

			gluTessVertex(pTess, coords,tempGLdouble[point_pos].tmp);
			point_pos++;
		}

		gluTessEndContour(pTess);
	}

	gluTessEndPolygon(pTess);
	gluDeleteTess(pTess);

	//triangleVector umrechnen zu array<Triangle^>^ &result
	int pos(0);
	result = gcnew array<Triangle^>(triangleVector.size());
	baseTriangleVector::iterator l,e;
	l = triangleVector.begin();
	e = triangleVector.end();
	for(; l!=e; l++)
	{
		Triangle ^pNewTriangle = gcnew Triangle(FindPoint(points, (*l).p1),
			                                    FindPoint(points, (*l).p2),
												FindPoint(points, (*l).p3));
		result[pos++] = pNewTriangle;
	}

	triangleVector.clear();

	Visualisation::COpenGLMutex::LeaveMutex();
}

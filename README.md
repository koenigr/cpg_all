# README #

The main documentation will be provided on this website: [http://cplan-group.net/](http://cplan-group.net/)

### What is this repository for? ###

* Quick summary

The cpg repository contains code of the Computational Planning Group. It is a collection of libraries for various purposes. The basis are the libraries for visualization and editing of basic geometry. We use the OpenTK framework as rendering platform. The main idea is to create a repository for Planning Synthesis, which uses evolutionary many-criteria optimization (EMO) to generate design solutions. Therefore we collect evaluation algorithms in this repository to use them for the EMO. We have started with solar analyis, grapha analysis, as well as Isovist analysis. 

* Version

pre-release

### How do I get set up? ###

* Summary of set up

Please see these instructions: [http://cplan-group.net/synthesiscode/](http://cplan-group.net/synthesiscode/)

* Configuration

* Dependencies

* Database configuration

* How to run tests

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

Dr. Reinhard Koenig
[http://entwurfsforschung.de/cv/ ](http://entwurfsforschung.de/cv/ )
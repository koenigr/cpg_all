﻿using System.Windows.Forms;
using UIControls;


namespace UrbanPattern_STree_v07
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.glControl1 = new OpenTK.GLControl();
            this.btn_Reset = new System.Windows.Forms.Button();
            this.btn_Subdivide = new System.Windows.Forms.Button();
            this.pB_Lines = new System.Windows.Forms.PictureBox();
            this.lbl_MaxArea = new System.Windows.Forms.Label();
            this.tB_MaxArea = new System.Windows.Forms.TextBox();
            this.tB_Connection = new System.Windows.Forms.TextBox();
            this.lbl_Connection = new System.Windows.Forms.Label();
            this.gB_Parameter = new System.Windows.Forms.GroupBox();
            this.lbl_Offset = new System.Windows.Forms.Label();
            this.tB_Offset = new System.Windows.Forms.TextBox();
            this.gB_Building = new System.Windows.Forms.GroupBox();
            this.tB_BDepth_Max = new System.Windows.Forms.TextBox();
            this.tB_BDepth_Min = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rangeBar1 = new UIControls.RangeBar();
            this.label1 = new System.Windows.Forms.Label();
            this.tB_Density = new System.Windows.Forms.TextBox();
            this.trB_Density = new System.Windows.Forms.TrackBar();
            this.lbl_Density = new System.Windows.Forms.Label();
            this.rB_Free = new System.Windows.Forms.RadioButton();
            this.rB_Line = new System.Windows.Forms.RadioButton();
            this.rB_Block = new System.Windows.Forms.RadioButton();
            this.rB_Row = new System.Windows.Forms.RadioButton();
            this.gB_BuildingStyle = new System.Windows.Forms.GroupBox();
            this.rangeBar_BLines = new UIControls.RangeBar();
            this.tB_BRestrictionLine = new System.Windows.Forms.TextBox();
            this.tB_BFrontLine = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cB_BFLine = new System.Windows.Forms.CheckBox();
            this.cB_BRLine = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pB_Lines)).BeginInit();
            this.gB_Parameter.SuspendLayout();
            this.gB_Building.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trB_Density)).BeginInit();
            this.gB_BuildingStyle.SuspendLayout();
            this.SuspendLayout();
            // 
            // glControl1
            // 
            this.glControl1.BackColor = System.Drawing.Color.Black;
            this.glControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.glControl1.Location = new System.Drawing.Point(12, 419);
            this.glControl1.Name = "glControl1";
            this.glControl1.Size = new System.Drawing.Size(500, 400);
            this.glControl1.TabIndex = 0;
            this.glControl1.VSync = false;
            this.glControl1.Load += new System.EventHandler(this.glControl1_Load);
            this.glControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.glControl1_Paint);
            this.glControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseDown);
            this.glControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseMove);
            this.glControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseUp);
            this.glControl1.Resize += new System.EventHandler(this.glControl1_Resize);
            // 
            // btn_Reset
            // 
            this.btn_Reset.Location = new System.Drawing.Point(519, 71);
            this.btn_Reset.Name = "btn_Reset";
            this.btn_Reset.Size = new System.Drawing.Size(147, 23);
            this.btn_Reset.TabIndex = 1;
            this.btn_Reset.Text = "Reset";
            this.btn_Reset.UseVisualStyleBackColor = true;
            this.btn_Reset.Click += new System.EventHandler(this.btn_Reset_Click);
            // 
            // btn_Subdivide
            // 
            this.btn_Subdivide.Location = new System.Drawing.Point(519, 13);
            this.btn_Subdivide.Name = "btn_Subdivide";
            this.btn_Subdivide.Size = new System.Drawing.Size(147, 52);
            this.btn_Subdivide.TabIndex = 2;
            this.btn_Subdivide.Text = "Subdivide Pattern";
            this.btn_Subdivide.UseVisualStyleBackColor = true;
            this.btn_Subdivide.Click += new System.EventHandler(this.btn_Subdivide_Click);
            // 
            // pB_Lines
            // 
            this.pB_Lines.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pB_Lines.Location = new System.Drawing.Point(12, 13);
            this.pB_Lines.Name = "pB_Lines";
            this.pB_Lines.Size = new System.Drawing.Size(500, 400);
            this.pB_Lines.TabIndex = 4;
            this.pB_Lines.TabStop = false;
            this.pB_Lines.Paint += new System.Windows.Forms.PaintEventHandler(this.pB_Lines_Paint);
            // 
            // lbl_MaxArea
            // 
            this.lbl_MaxArea.AutoSize = true;
            this.lbl_MaxArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_MaxArea.Location = new System.Drawing.Point(6, 25);
            this.lbl_MaxArea.Name = "lbl_MaxArea";
            this.lbl_MaxArea.Size = new System.Drawing.Size(74, 13);
            this.lbl_MaxArea.TabIndex = 6;
            this.lbl_MaxArea.Text = "Max Lot Size: ";
            // 
            // tB_MaxArea
            // 
            this.tB_MaxArea.Location = new System.Drawing.Point(86, 21);
            this.tB_MaxArea.Name = "tB_MaxArea";
            this.tB_MaxArea.Size = new System.Drawing.Size(54, 20);
            this.tB_MaxArea.TabIndex = 7;
            this.tB_MaxArea.Text = "2000";
            this.tB_MaxArea.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tB_MaxArea.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tB_MaxArea_KeyUp);
            // 
            // tB_Connection
            // 
            this.tB_Connection.Location = new System.Drawing.Point(95, 69);
            this.tB_Connection.Name = "tB_Connection";
            this.tB_Connection.Size = new System.Drawing.Size(45, 20);
            this.tB_Connection.TabIndex = 9;
            this.tB_Connection.Text = "5";
            this.tB_Connection.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tB_Connection.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tB_Connection_KeyUp);
            // 
            // lbl_Connection
            // 
            this.lbl_Connection.AutoSize = true;
            this.lbl_Connection.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Connection.Location = new System.Drawing.Point(6, 73);
            this.lbl_Connection.Name = "lbl_Connection";
            this.lbl_Connection.Size = new System.Drawing.Size(83, 13);
            this.lbl_Connection.TabIndex = 8;
            this.lbl_Connection.Text = "Min Vertice Dist:";
            // 
            // gB_Parameter
            // 
            this.gB_Parameter.BackColor = System.Drawing.SystemColors.Control;
            this.gB_Parameter.Controls.Add(this.lbl_Offset);
            this.gB_Parameter.Controls.Add(this.tB_Offset);
            this.gB_Parameter.Controls.Add(this.lbl_MaxArea);
            this.gB_Parameter.Controls.Add(this.tB_Connection);
            this.gB_Parameter.Controls.Add(this.tB_MaxArea);
            this.gB_Parameter.Controls.Add(this.lbl_Connection);
            this.gB_Parameter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gB_Parameter.Location = new System.Drawing.Point(520, 100);
            this.gB_Parameter.Name = "gB_Parameter";
            this.gB_Parameter.Size = new System.Drawing.Size(146, 100);
            this.gB_Parameter.TabIndex = 10;
            this.gB_Parameter.TabStop = false;
            this.gB_Parameter.Text = "Subdivision Parameters";
            // 
            // lbl_Offset
            // 
            this.lbl_Offset.AutoSize = true;
            this.lbl_Offset.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Offset.Location = new System.Drawing.Point(6, 49);
            this.lbl_Offset.Name = "lbl_Offset";
            this.lbl_Offset.Size = new System.Drawing.Size(72, 13);
            this.lbl_Offset.TabIndex = 10;
            this.lbl_Offset.Text = "Street Offset: ";
            // 
            // tB_Offset
            // 
            this.tB_Offset.Location = new System.Drawing.Point(86, 45);
            this.tB_Offset.Name = "tB_Offset";
            this.tB_Offset.Size = new System.Drawing.Size(54, 20);
            this.tB_Offset.TabIndex = 11;
            this.tB_Offset.Text = "5";
            this.tB_Offset.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tB_Offset.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tB_Offset_KeyUp);
            // 
            // gB_Building
            // 
            this.gB_Building.BackColor = System.Drawing.SystemColors.Control;
            this.gB_Building.Controls.Add(this.cB_BRLine);
            this.gB_Building.Controls.Add(this.cB_BFLine);
            this.gB_Building.Controls.Add(this.tB_BRestrictionLine);
            this.gB_Building.Controls.Add(this.tB_BFrontLine);
            this.gB_Building.Controls.Add(this.label4);
            this.gB_Building.Controls.Add(this.label5);
            this.gB_Building.Controls.Add(this.label6);
            this.gB_Building.Controls.Add(this.rangeBar_BLines);
            this.gB_Building.Controls.Add(this.tB_BDepth_Max);
            this.gB_Building.Controls.Add(this.tB_BDepth_Min);
            this.gB_Building.Controls.Add(this.label3);
            this.gB_Building.Controls.Add(this.label2);
            this.gB_Building.Controls.Add(this.rangeBar1);
            this.gB_Building.Controls.Add(this.label1);
            this.gB_Building.Controls.Add(this.tB_Density);
            this.gB_Building.Controls.Add(this.trB_Density);
            this.gB_Building.Controls.Add(this.lbl_Density);
            this.gB_Building.Location = new System.Drawing.Point(520, 207);
            this.gB_Building.Name = "gB_Building";
            this.gB_Building.Size = new System.Drawing.Size(146, 297);
            this.gB_Building.TabIndex = 11;
            this.gB_Building.TabStop = false;
            this.gB_Building.Text = "Building Lot Parameters";
            // 
            // tB_BDepth_Max
            // 
            this.tB_BDepth_Max.Location = new System.Drawing.Point(112, 104);
            this.tB_BDepth_Max.Name = "tB_BDepth_Max";
            this.tB_BDepth_Max.Size = new System.Drawing.Size(28, 20);
            this.tB_BDepth_Max.TabIndex = 18;
            this.tB_BDepth_Max.Text = "15";
            this.tB_BDepth_Max.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tB_BDepth_Max.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tB_BDepth_Max_KeyUp);
            // 
            // tB_BDepth_Min
            // 
            this.tB_BDepth_Min.Location = new System.Drawing.Point(39, 104);
            this.tB_BDepth_Min.Name = "tB_BDepth_Min";
            this.tB_BDepth_Min.Size = new System.Drawing.Size(28, 20);
            this.tB_BDepth_Min.TabIndex = 12;
            this.tB_BDepth_Min.Text = "10";
            this.tB_BDepth_Min.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tB_BDepth_Min.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tB_BDepth_Min_KeyUp);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(76, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 12);
            this.label3.TabIndex = 17;
            this.label3.Text = "Max:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 12);
            this.label2.TabIndex = 16;
            this.label2.Text = "Min:";
            // 
            // rangeBar1
            // 
            this.rangeBar1.DivisionNum = 20;
            this.rangeBar1.HeightOfBar = 8;
            this.rangeBar1.HeightOfMark = 24;
            this.rangeBar1.HeightOfTick = 6;
            this.rangeBar1.InnerColor = System.Drawing.Color.DeepSkyBlue;
            this.rangeBar1.Location = new System.Drawing.Point(8, 130);
            this.rangeBar1.Name = "rangeBar1";
            this.rangeBar1.Orientation = UIControls.RangeBar.RangeBarOrientation.horizontal;
            this.rangeBar1.RangeMaximum = 10;
            this.rangeBar1.RangeMinimum = 10;
            this.rangeBar1.ScaleOrientation = UIControls.RangeBar.TopBottomOrientation.bottom;
            this.rangeBar1.Size = new System.Drawing.Size(131, 17);
            this.rangeBar1.TabIndex = 15;
            this.rangeBar1.Tag = "";
            this.rangeBar1.TotalMaximum = 20;
            this.rangeBar1.TotalMinimum = 0;
            this.rangeBar1.RangeChanged += new UIControls.RangeBar.RangeChangedEventHandler(this.OnRangeChanged);
            this.rangeBar1.RangeChanging += new UIControls.RangeBar.RangeChangedEventHandler(this.OnRangeChanging);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Building Depth:";
            // 
            // tB_Density
            // 
            this.tB_Density.Location = new System.Drawing.Point(86, 20);
            this.tB_Density.Name = "tB_Density";
            this.tB_Density.Size = new System.Drawing.Size(54, 20);
            this.tB_Density.TabIndex = 11;
            this.tB_Density.Text = "0.3";
            this.tB_Density.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tB_Density.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tB_Density_KeyUp);
            // 
            // trB_Density
            // 
            this.trB_Density.Location = new System.Drawing.Point(4, 40);
            this.trB_Density.Maximum = 100;
            this.trB_Density.Name = "trB_Density";
            this.trB_Density.Size = new System.Drawing.Size(136, 45);
            this.trB_Density.TabIndex = 10;
            this.trB_Density.TickFrequency = 10;
            this.trB_Density.Value = 30;
            this.trB_Density.MouseUp += new System.Windows.Forms.MouseEventHandler(this.trB_Density_MouseUp);
            // 
            // lbl_Density
            // 
            this.lbl_Density.AutoSize = true;
            this.lbl_Density.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Density.Location = new System.Drawing.Point(6, 24);
            this.lbl_Density.Name = "lbl_Density";
            this.lbl_Density.Size = new System.Drawing.Size(86, 13);
            this.lbl_Density.TabIndex = 8;
            this.lbl_Density.Text = "Coverage Ratio: ";
            // 
            // rB_Free
            // 
            this.rB_Free.AutoSize = true;
            this.rB_Free.Location = new System.Drawing.Point(9, 76);
            this.rB_Free.Name = "rB_Free";
            this.rB_Free.Size = new System.Drawing.Size(91, 17);
            this.rB_Free.TabIndex = 3;
            this.rB_Free.Text = "Free-Standing";
            this.rB_Free.UseVisualStyleBackColor = true;
            this.rB_Free.CheckedChanged += new System.EventHandler(this.rB_Free_CheckedChanged);
            // 
            // rB_Line
            // 
            this.rB_Line.AutoSize = true;
            this.rB_Line.Location = new System.Drawing.Point(9, 57);
            this.rB_Line.Name = "rB_Line";
            this.rB_Line.Size = new System.Drawing.Size(101, 17);
            this.rB_Line.TabIndex = 2;
            this.rB_Line.Text = "Ribbon (\"Zeile\")";
            this.rB_Line.UseVisualStyleBackColor = true;
            this.rB_Line.CheckedChanged += new System.EventHandler(this.rB_Line_CheckedChanged);
            // 
            // rB_Block
            // 
            this.rB_Block.AutoSize = true;
            this.rB_Block.Location = new System.Drawing.Point(9, 38);
            this.rB_Block.Name = "rB_Block";
            this.rB_Block.Size = new System.Drawing.Size(52, 17);
            this.rB_Block.TabIndex = 1;
            this.rB_Block.Text = "Block";
            this.rB_Block.UseVisualStyleBackColor = true;
            this.rB_Block.CheckedChanged += new System.EventHandler(this.rB_Block_CheckedChanged);
            // 
            // rB_Row
            // 
            this.rB_Row.AutoSize = true;
            this.rB_Row.Checked = true;
            this.rB_Row.Location = new System.Drawing.Point(9, 19);
            this.rB_Row.Name = "rB_Row";
            this.rB_Row.Size = new System.Drawing.Size(94, 17);
            this.rB_Row.TabIndex = 0;
            this.rB_Row.TabStop = true;
            this.rB_Row.Text = "Row (\"Reihe\")";
            this.rB_Row.UseVisualStyleBackColor = true;
            this.rB_Row.CheckedChanged += new System.EventHandler(this.rB_Row_CheckedChanged);
            // 
            // gB_BuildingStyle
            // 
            this.gB_BuildingStyle.Controls.Add(this.rB_Free);
            this.gB_BuildingStyle.Controls.Add(this.rB_Row);
            this.gB_BuildingStyle.Controls.Add(this.rB_Line);
            this.gB_BuildingStyle.Controls.Add(this.rB_Block);
            this.gB_BuildingStyle.Location = new System.Drawing.Point(520, 510);
            this.gB_BuildingStyle.Name = "gB_BuildingStyle";
            this.gB_BuildingStyle.Size = new System.Drawing.Size(146, 277);
            this.gB_BuildingStyle.TabIndex = 12;
            this.gB_BuildingStyle.TabStop = false;
            this.gB_BuildingStyle.Text = "Building Styles";
            // 
            // rangeBar_BLines
            // 
            this.rangeBar_BLines.DivisionNum = 20;
            this.rangeBar_BLines.HeightOfBar = 8;
            this.rangeBar_BLines.HeightOfMark = 24;
            this.rangeBar_BLines.HeightOfTick = 6;
            this.rangeBar_BLines.InnerColor = System.Drawing.Color.DodgerBlue;
            this.rangeBar_BLines.Location = new System.Drawing.Point(9, 253);
            this.rangeBar_BLines.Name = "rangeBar_BLines";
            this.rangeBar_BLines.Orientation = UIControls.RangeBar.RangeBarOrientation.horizontal;
            this.rangeBar_BLines.RangeMaximum = 10;
            this.rangeBar_BLines.RangeMinimum = 10;
            this.rangeBar_BLines.ScaleOrientation = UIControls.RangeBar.TopBottomOrientation.bottom;
            this.rangeBar_BLines.Size = new System.Drawing.Size(131, 17);
            this.rangeBar_BLines.TabIndex = 19;
            this.rangeBar_BLines.Tag = "";
            this.rangeBar_BLines.TotalMaximum = 20;
            this.rangeBar_BLines.TotalMinimum = 0;
            this.rangeBar_BLines.RangeChanged += new UIControls.RangeBar.RangeChangedEventHandler(this.rangeBar_BLines_RangeChanged);
            this.rangeBar_BLines.RangeChanging += new UIControls.RangeBar.RangeChangedEventHandler(this.rangeBar_BLines_RangeChanging);
            // 
            // tB_BRestrictionLine
            // 
            this.tB_BRestrictionLine.Location = new System.Drawing.Point(112, 230);
            this.tB_BRestrictionLine.Name = "tB_BRestrictionLine";
            this.tB_BRestrictionLine.Size = new System.Drawing.Size(28, 20);
            this.tB_BRestrictionLine.TabIndex = 24;
            this.tB_BRestrictionLine.Text = "0";
            this.tB_BRestrictionLine.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tB_BRestrictionLine.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tB_BRestrictionLine_KeyUp);
            // 
            // tB_BFrontLine
            // 
            this.tB_BFrontLine.Location = new System.Drawing.Point(39, 230);
            this.tB_BFrontLine.Name = "tB_BFrontLine";
            this.tB_BFrontLine.Size = new System.Drawing.Size(28, 20);
            this.tB_BFrontLine.TabIndex = 20;
            this.tB_BFrontLine.Text = "0";
            this.tB_BFrontLine.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tB_BFrontLine.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tB_BFrontLine_KeyUp);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(76, 234);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 12);
            this.label4.TabIndex = 23;
            this.label4.Text = "Rest.:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 234);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 12);
            this.label5.TabIndex = 22;
            this.label5.Text = "Front:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Building Lines:";
            // 
            // cB_BFLine
            // 
            this.cB_BFLine.AutoSize = true;
            this.cB_BFLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cB_BFLine.Location = new System.Drawing.Point(9, 182);
            this.cB_BFLine.Name = "cB_BFLine";
            this.cB_BFLine.Size = new System.Drawing.Size(91, 17);
            this.cB_BFLine.TabIndex = 25;
            this.cB_BFLine.Text = "Frontage Line";
            this.cB_BFLine.UseVisualStyleBackColor = true;
            this.cB_BFLine.CheckedChanged += new System.EventHandler(this.cB_BFLine_CheckedChanged);
            // 
            // cB_BRLine
            // 
            this.cB_BRLine.AutoSize = true;
            this.cB_BRLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cB_BRLine.Location = new System.Drawing.Point(9, 205);
            this.cB_BRLine.Name = "cB_BRLine";
            this.cB_BRLine.Size = new System.Drawing.Size(97, 17);
            this.cB_BRLine.TabIndex = 26;
            this.cB_BRLine.Text = "Restriction Line";
            this.cB_BRLine.UseVisualStyleBackColor = true;
            this.cB_BRLine.CheckedChanged += new System.EventHandler(this.cB_BRLine_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 831);
            this.Controls.Add(this.gB_BuildingStyle);
            this.Controls.Add(this.gB_Building);
            this.Controls.Add(this.gB_Parameter);
            this.Controls.Add(this.pB_Lines);
            this.Controls.Add(this.btn_Subdivide);
            this.Controls.Add(this.btn_Reset);
            this.Controls.Add(this.glControl1);
            this.Name = "Form1";
            this.Text = "Street Pattern";
            ((System.ComponentModel.ISupportInitialize)(this.pB_Lines)).EndInit();
            this.gB_Parameter.ResumeLayout(false);
            this.gB_Parameter.PerformLayout();
            this.gB_Building.ResumeLayout(false);
            this.gB_Building.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trB_Density)).EndInit();
            this.gB_BuildingStyle.ResumeLayout(false);
            this.gB_BuildingStyle.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private OpenTK.GLControl glControl1;
        private System.Windows.Forms.Button btn_Reset;
        private System.Windows.Forms.Button btn_Subdivide;
        private System.Windows.Forms.PictureBox pB_Lines;
        private System.Windows.Forms.Label lbl_MaxArea;
        private System.Windows.Forms.TextBox tB_MaxArea;
        private System.Windows.Forms.TextBox tB_Connection;
        private System.Windows.Forms.Label lbl_Connection;
        private System.Windows.Forms.GroupBox gB_Parameter;
        private System.Windows.Forms.Label lbl_Offset;
        private System.Windows.Forms.TextBox tB_Offset;
        private System.Windows.Forms.GroupBox gB_Building;
        private System.Windows.Forms.TextBox tB_Density;
        private System.Windows.Forms.TrackBar trB_Density;
        private System.Windows.Forms.Label lbl_Density;
        private System.Windows.Forms.RadioButton rB_Line;
        private System.Windows.Forms.RadioButton rB_Block;
        private System.Windows.Forms.RadioButton rB_Row;
        private System.Windows.Forms.RadioButton rB_Free;
        private System.Windows.Forms.Label label1;
        private RangeBar rangeBar1;
        private System.Windows.Forms.TextBox tB_BDepth_Max;
        private System.Windows.Forms.TextBox tB_BDepth_Min;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gB_BuildingStyle;
        private System.Windows.Forms.TextBox tB_BRestrictionLine;
        private System.Windows.Forms.TextBox tB_BFrontLine;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private RangeBar rangeBar_BLines;
        private System.Windows.Forms.CheckBox cB_BRLine;
        private System.Windows.Forms.CheckBox cB_BFLine;

    }
}


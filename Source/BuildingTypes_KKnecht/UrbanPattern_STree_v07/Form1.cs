﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ClipperLib;
using GeoObj;
using GeoObj.Extensions;
using GeoObj.Graph;
using GeoObj.Helper;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Tektosyne;
using Tektosyne.Collections;
using Tektosyne.Geometry;



namespace UrbanPattern_STree_v07
{

    public partial class Form1 : Form
    {
        # region VARIABLES

        bool _isloaded = false;
        ObjectsAllList AllObjectsList = new ObjectsAllList();
        StreetPattern StreetGenerator;

        public Subdivision Graph = new Subdivision();
        private Subdivision LayoutCopy = new Subdivision();
        //List<PointD[]> polygons = new List<PointD[]>();

        //Rectangle canvas;

        double maxArea = 2000.0d;
        double connectionDist = 5.0d;
        double streetOffset = 6.0d;
        double density = 0.3d;
        int buildingDepthMin = 10;
        int buildingDepthMax = 15;
        double buildingFrontLine = 0.0d;
        double buildingRestrictionLine = 0.0d;
        double maxBDepth = 0.0d;

        bool bFLine = false;
        bool bRLine = false;

        bool showStreets = true;
        List<Node> nodes = new List<Node>();
        Polygon[] Frontage;
        bool subdivided = false;

        // Building Styles:
        // 0 - Row
        // 1 - Block
        // 2 - Ribbon
        // 3 - Free-Standing
        int bStyle = 0;

        PointD[][] curPolys2;
        Parameter LayoutParameter;

        # region Pens
        float[] dashFLine = {4, 1, 1, 1, 1, 1};
        float[] dashRLine = {4, 4, 4, 4, 1, 4};
        Pen fLinePen = new Pen(Color.Red, 1);
        Pen rLinePen = new Pen(Color.Blue, 1);

        # endregion

        # endregion


        # region CONSTRUCTORS

        public Form1()
        {
            InitializeComponent();
            //canvas = pB_Lines.ClientRectangle;
            maxArea = Convert.ToDouble(this.tB_MaxArea.Text);
            streetOffset = Convert.ToDouble(this.tB_Offset.Text);
            buildingDepthMin = rangeBar1.RangeMinimum = Convert.ToInt32(this.tB_BDepth_Min.Text);
            buildingDepthMax = rangeBar1.RangeMaximum = Convert.ToInt32(this.tB_BDepth_Max.Text);
            buildingFrontLine = rangeBar_BLines.RangeMinimum = Convert.ToInt32(this.tB_BFrontLine.Text);
            buildingRestrictionLine = rangeBar_BLines.RangeMaximum = Convert.ToInt32(this.tB_BRestrictionLine.Text);
            LayoutParameter = new Parameter(connectionDist, maxArea, streetOffset, 0.0d, 0.0d, density, buildingDepthMin, buildingDepthMax);
            fLinePen.DashPattern = dashFLine;
            rLinePen.DashPattern = dashRLine;
        }

        # endregion

        # region METHODS

        # region INITIALIZE PATTERN

        /// <summary>
        /// Creates an initial pattern.
        /// </summary>
        private void CreateInitial()
        {
            RectD border = new RectD(glControl1.Left, glControl1.Bottom, glControl1.Width, glControl1.Height);
            StreetGenerator = new StreetPattern(4, 10, 20, 80, 100, 30, false, false, border);
            PointD[] iniPts;

            // --- the first street --- 
            iniPts = new PointD[2];
            iniPts[0] = new PointD(glControl1.Width / 2 - 25, glControl1.Height / 2);
            iniPts[1] = new PointD(glControl1.Width / 2 + 25, glControl1.Height / 2);
            Graph.AddEdge(iniPts[0], iniPts[1]);

            GrowPattern();
        }

        private void CreateInitial2()
        {
            RectD border = new RectD(glControl1.Left, glControl1.Bottom, glControl1.Width, glControl1.Height);
            PointD[] polygonNodes = new PointD[5];
            for (int i = 0; i < 1; ++i)
            {
                polygonNodes[i] = new PointD(glControl1.Left + 150 * i, 10);

            }
            polygonNodes[2] = new PointD(glControl1.Left + 150 * 2, 10 + 200);
            polygonNodes[3] = new PointD(glControl1.Left + 150 * 2 - 100, 10 + 450);
            polygonNodes[4] = new PointD(glControl1.Left + 150, 10 + 450);
            LineD[] edges = new LineD[5];
            for (int i = 0; i < polygonNodes.Length; ++i)
            {
                LineD line = new LineD(polygonNodes[i], polygonNodes[(i+1)%polygonNodes.Length]);
                edges[i] = line;
            }
            Graph = Subdivision.FromLines(edges);

            
        }


        /// <summary>
        /// Recreates the graph, i.e. the objects of the graph that shall be drawn.
        /// </summary>
        /// <param name="_curGraph">(Subdivision) current graph.</param>
        private void ReCreateGraph(Subdivision _curGraph)
        {
            // --- draw the elements of the graph ---
            AllObjectsList.ObjectsToDraw.Clear();
            AllObjectsList.ObjectsToInteract.Clear();
            AllObjectsList = new ObjectsAllList();
            LineD[] graphLines = _curGraph.ToLines();
            foreach (LineD line in graphLines)
            {
                AllObjectsList.Add(new GLine(line));
            }

            PointD[] graphPts = _curGraph.Nodes.ToArray();
            foreach (PointD point in graphPts)
            {
                AllObjectsList.Add(new GCircle(point, 2, 10));
            }   
            glControl1.Invalidate();
        }

        /// <summary>
        /// Grows the street pattern.
        /// </summary>
        private void GrowPattern()
        {
            GL.ClearColor(Color.White);
            int steps = Convert.ToInt16(50);
            for (int i = 0; i < steps; i++)
            {
                Graph = StreetGenerator.GrowStreetPattern(Graph);
            }
            CleanUpGraph();

            PointD[][] validPolys = this.Graph.ToPolygons();
            List<GPolygon> polys = new List<GPolygon>();
            List<float> areas = new List<float>();

            for (int i = 0; i < validPolys.GetLength(0); i++)
            {
                PointD[] arrPoly = validPolys[i];
                GPolygon curPoly = new GPolygon(arrPoly);

                AllObjectsList.Add(curPoly);
                polys.Add(curPoly);
                Single curArea = Convert.ToSingle(Math.Pow(curPoly.GetArea(), 0.5));
                areas.Add(curArea);
            }

            CollectionsExtend.CutMinValue(areas, Fortran.Min(areas.ToArray<Single>()));
            CollectionsExtend.Normalize(areas);
            CollectionsExtend.IntervalExtension(areas, Fortran.Max(areas.ToArray<Single>()));
            Vector4[] colors = ConvertColor.CreateFFColor(areas.ToArray());
            for (int i = 0; i < polys.Count; i++)
            {
                GPolygon curPoly = polys[i];
                curPoly.FillColor = colors[i];
                curPoly.BorderWidth = 5;
                curPoly.BorderColor = ConvertColor.CreateFFColor(Color.Black);
            }
        }

        /// <summary>
        /// Cleans up the graph, i.e. adjusts the connections.
        /// </summary>
        private void CleanUpGraph()
        {   // --- only works well for two points that are located to close to each other --- 
            Subdivision tempGraph = (Subdivision)Graph.Clone();
            SortedListEx<PointD, SubdivisionEdge> vertices = tempGraph.Vertices;

            List<PointD> toNearPts = new List<PointD>();
            for (int i = 0; i < vertices.Keys.Count; i++)
            {
                PointD curVert = vertices.Keys[i];
                for (int k = i + 1; k < vertices.Keys.Count; k++)
                {
                    PointD compareVert = vertices.Keys[k];
                    Double dist = Graph.GetDistance(curVert, compareVert);
                    if (dist < 20)
                    {
                        toNearPts.Add(curVert);
                        toNearPts.Add(compareVert);
                        PointD midPt = curVert.Move(compareVert, dist / 2);
                        List<PointD> curNeightb1 = Graph.GetNeighbors(curVert).ToList();
                        foreach (PointD conPt in curNeightb1)
                        {
                            SubdivisionEdge curEdge = Graph.FindEdge(curVert, conPt);
                            if (curEdge != null) Graph.RemoveEdge(curEdge.Key);
                        }
                        List<PointD> curNeightb2 = Graph.GetNeighbors(compareVert).ToList();
                        foreach (PointD conPt in curNeightb2)
                        {
                            SubdivisionEdge curEdge = Graph.FindEdge(compareVert, conPt);
                            if (curEdge != null) Graph.RemoveEdge(curEdge.Key);
                        }

                        LineD[] curLinesArr = Graph.ToLines();
                        List<LineD> curLinesList = curLinesArr.ToList();

                        List<PointD> curNeightb = curNeightb1;
                        curNeightb.AddRange(curNeightb2);

                        foreach (PointD conPt in curNeightb)
                        {
                            curLinesList.Add(new LineD(midPt, conPt));
                        }

                        curLinesArr = curLinesList.ToArray();
                        Graph = new Subdivision();
                        Graph = Subdivision.FromLines(curLinesArr);
                    }
                }
            }

            // --- there are some bugs in this part...? --------------------------------------
            List<LineD> graphLines = Graph.ToLines().ToList();
            List<PointD> graphPoints = Graph.Vertices.Keys.ToList();
            List<int> edgesToRemove = new List<int>();
            List<LineD> linesToAdd = new List<LineD>();
            List<PointD> vertexToDelete = new List<PointD>();

            for (int i = 0; i < graphLines.Count; i++)
            {
                bool partOfLine = false;
                LineD curLine = graphLines[i];

                foreach (PointD curPt in graphPoints)
                {
                    if (curPt == curLine.Start | curPt == curLine.End)
                        partOfLine = true;
                    if (!partOfLine)
                    {
                        PointD testPt = curLine.Intersect(curPt);
                        LineLocation locTest = curLine.Locate(testPt);
                        if (locTest == LineLocation.Between)
                        {
                            double dist = Graph.GetDistance(curPt, testPt);
                            if (dist < 20)
                            {
                                if (Graph.GetNeighbors(curPt).Count == 1)
                                    vertexToDelete.Add(curPt);
                                else
                                {
                                    SubdivisionEdge curEdge = Graph.FindEdge(curLine.Start, curLine.End);
                                    if (!(curEdge == null))
                                    {
                                        edgesToRemove.Add(curEdge.Key);
                                        linesToAdd.Add(new LineD(curLine.Start, testPt));
                                        linesToAdd.Add(new LineD(curLine.End, testPt));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            foreach (int curIdx in edgesToRemove)
            {
                bool test = Graph.RemoveEdge(curIdx);
            }
            foreach (PointD curPt in vertexToDelete)
            {
                double dist;
                SubdivisionEdge curEdge = Graph.FindNearestEdge(curPt, out dist);
                bool test = Graph.RemoveEdge(curEdge.Key);
            }

            LineD[] currLinesArr = Graph.ToLines();
            List<LineD> currLinesList = currLinesArr.ToList();
            foreach (LineD curLine in linesToAdd)
            {
                currLinesList.Add(curLine);
            }
            currLinesArr = currLinesList.ToArray();
            Graph = new Subdivision();
            Graph = Subdivision.FromLines(currLinesArr);

            ReCreateGraph(Graph);
        }

        /// <summary>
        /// Clears the graph and the objects.
        /// </summary>
        private void Clear()
        {
            GL.ClearColor(Color.White);
            AllObjectsList = new ObjectsAllList();
            Graph = new Subdivision();
            nodes.Clear();
            CreateInitial2();
            //CreateInitial();
            LayoutCopy = Graph;
            glControl1.Invalidate();
        }

        # endregion

        # region CHANGE PATTERN


        /// <summary>
        /// Creates a subdivision of the graph.
        /// </summary>
        public void CreateSubdivision()
        {
            Subdivision subdivForPolys = (Subdivision)Graph.Clone();
            List<PointD> curVertices = subdivForPolys.Vertices.Keys.ToList();
            List<PointD> polyAsList = new List<PointD>();
            List<List<PointD>> validPolys = new List<List<PointD>>();
            List<List<PointD>> problemPolys = new List<List<PointD>>();

            PointD[][] curPolys = subdivForPolys.ToPolygons();

            //this.ClipPolygonsAndAddToGraph(curPolys);

            curPolys2 = Graph.ToPolygons();

            STree tmpTree = new STree(Graph, LayoutParameter);//new STree(Graph, maxArea, connectionDist, streetOffset, density);
            PointD[][] tmpPolys;
            Polygon tmpPoly;

            tmpPolys = tmpTree.ToPolygons();//tmpTree.SlicingLayout.ToPolygons();
            nodes = tmpTree.Leaves;
            Frontage = tmpTree.BuildingBlocks;

            for (int j = 0; j < tmpPolys.GetLength(0); j++)
            {
                polyAsList = tmpPolys[j].ToList();
                validPolys.Add(polyAsList);
            }

            for (int i = 0; i < curPolys2.GetLength(0); i++)
            {
                tmpPoly = new Polygon(curPolys2[i]);

                if (tmpPoly.GetArea() < maxArea)
                {
                    polyAsList = curPolys2[i].ToList();
                    validPolys.Add(polyAsList);
                }
            }

            LayoutCopy = Graph.Clone() as Subdivision;
            Graph = tmpTree.Graph.Clone() as Subdivision;
            //Console.WriteLine("No of Edges of Graph: " + Graph.Edges.Count);

            this.ReCreateGraph(Graph);
            //Console.WriteLine("No of Edges of Graph after Recreation: " + Graph.Edges.Count);

            List<GPolygon> polys = new List<GPolygon>();
            List<float> areas = new List<float>();
            for (int i = 0; i < validPolys.Count; i++)
            {
                PointD[] arrPoly = validPolys[i].ToArray();
                GPolygon curPoly = new GPolygon(arrPoly);

                AllObjectsList.Add(curPoly);
                polys.Add(curPoly);
                Single curArea = Convert.ToSingle(Math.Pow(curPoly.GetArea(), 0.5));
                areas.Add(curArea);
            }

            CollectionsExtend.CutMinValue(areas, Fortran.Min(areas.ToArray<Single>()));
            CollectionsExtend.Normalize(areas);
            CollectionsExtend.IntervalExtension(areas, Fortran.Max(areas.ToArray<Single>()));
            Vector4[] colors = ConvertColor.CreateFFColor(areas.ToArray());
            for (int i = 0; i < polys.Count; i++)
            {
                GPolygon curPoly = polys[i];
                curPoly.FillColor = colors[i];
                curPoly.BorderWidth = 5;
                curPoly.BorderColor = ConvertColor.CreateFFColor(Color.Black);
            }

            //List<PointD[]> pol = Polygon.ToListPointDArray(validPolys);

            AllObjectsList.ObjectsToDraw.Reverse();
            subdivided = true;
        }



        # endregion

        # region GL CONTROL

        /// <summary>
        /// Loads the glcontrol.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void glControl1_Load(object sender, EventArgs e)
        {
            _isloaded = true;
            GL.ClearColor(Color.White);
            SetupViewport();
            //CreateInitial();
            CreateInitial2();
            ReCreateGraph(Graph);
            //Graph = StreetGenerator.GrowStreetPattern(Graph);
        }

        /// <summary>
        /// Resizes the glcontrol.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void glControl1_Resize(object sender, EventArgs e)
        {
            SetupViewport();
            glControl1.Invalidate();
        }

        /// <summary>
        /// Sets up the viewport.
        /// </summary>
        private void SetupViewport()
        {
            int _width = glControl1.Width;
            int _height = glControl1.Height;
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, _width, _height,0, -1, 1); // Bottom-left corner pixel has coordinate (0, 0)
            GL.Viewport(0, 0, _width, _height); // Use all of the glControl painting area
        }

        /// <summary>
        /// Paints the glcontrol.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            if (!_isloaded) return;
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            AllObjectsList.Draw();

            glControl1.SwapBuffers();
        }

        /// <summary>
        /// On mouse down.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void glControl1_MouseDown(object sender, MouseEventArgs e)
        {
            if (!_isloaded) return;
            AllObjectsList.ObjectsToInteract.MouseDown(e, glControl1);
        }

        /// <summary>
        /// On mouse move.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void glControl1_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_isloaded) return;
            Cursor tmpCursor = Cursors.Default;
            AllObjectsList.ObjectsToInteract.MouseMove(e, glControl1, ref tmpCursor);
            //this.Cursor = tmpCursor;         
        }

        /// <summary>
        /// On mouse up.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void glControl1_MouseUp(object sender, MouseEventArgs e)
        {
            if (!_isloaded) return;

            AllObjectsList.ObjectsToInteract.MouseUp(e);// --- put the selected element to the fornt
            if (AllObjectsList.ObjectsToInteract.FoundInteractObj())
            {
                AllObjectsList.ObjectsToDraw.MouseUp(e, glControl1);
            }

            glControl1.Invalidate();
        }
        # endregion

        # region INTERFACE

        # region BUTTONS

        /// <summary>
        /// On button Reset click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Reset_Click(object sender, EventArgs e)
        {
            Clear();
            subdivided = false;
            pB_Lines.Invalidate();
        }

        /// <summary>
        /// On button Subdivide click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Subdivide_Click(object sender, EventArgs e)
        {
            if (!subdivided)
            {
                ReCreateGraph(Graph);
                CreateSubdivision();
            }

            glControl1.Invalidate();
            pB_Lines.Invalidate();
        }

        # endregion

        # region PAINT

        /// <summary>
        /// Paints the lines picture box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pB_Lines_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            Graphics g = e.Graphics;

            g.FillRectangle(Brushes.White, pB_Lines.ClientRectangle);

            if (Graph != null)
            {
                Pen pen = new Pen(Brushes.LightGray);

                LineD[] Lines = Graph.ToLines();

                for (int n = 0; n < Lines.Length; n++)
                {
                    g.DrawLine(pen, new Point((int)Lines[n].Start.X, (int)Lines[n].Start.Y), new Point((int)Lines[n].End.X, (int)Lines[n].End.Y));
                }

                foreach (PointD point in Graph.Nodes)
                {
                    g.FillEllipse(Brushes.White, (float)point.X - 2.0f, (float)point.Y - 2.0f, 4.0f, 4.0f);
                    g.DrawEllipse(new Pen(Brushes.Black), (float)point.X - 2.0f, (float)point.Y - 2.0f, 4.0f, 4.0f);
                }

                Subdivision zeroEdge = LayoutCopy.Clone() as Subdivision;
           Lines = zeroEdge.ToLines();

            pen = new Pen(Brushes.Black, 3.0f);

                if (showStreets)
                {
                    for (int n = 0; n < Lines.Length; n++)
                    {
                        g.DrawLine(pen, new Point((int)Lines[n].Start.X, (int)Lines[n].Start.Y), new Point((int)Lines[n].End.X, (int)Lines[n].End.Y));
                    }

                    foreach (PointD point in zeroEdge.Nodes)
                    {
                        g.FillEllipse(Brushes.White, (float)point.X - 3.0f, (float)point.Y - 3.0f, 6.0f, 6.0f);
                        g.DrawEllipse(new Pen(Brushes.Black), (float)point.X - 3.0f, (float)point.Y - 3.0f, 6.0f, 6.0f);
                    }
                }

                foreach (Node node in nodes)
                {
                    if (node.Building != null && node.Building[bStyle] != null && node.Building[bStyle].Vertices.Length > 0)
                    {
                    
                        g.FillPolygon(Brushes.Black, PointDToPointArray(node.Building[bStyle].Vertices));

                        var verts = node.Building[bStyle].Vertices;
                       // System.Windows.Forms.MessageBox.Show(verts.Length.ToString());
                        //foreach (PointD vert in verts)
                        //{
                        //    System.Windows.Forms.MessageBox. Show(vert.X + " " + vert.Y);
                        //}
                        //break;
                        g.DrawString(string.Format("{0:F2}", node.GetDensity(bStyle)), new Font(FontFamily.GenericSansSerif, 6), Brushes.Gray, Polygon.GetCentroidAsPoint(node.Region.Vertices));
                        if (node.FrontageLines != null)
                        {
                            foreach (LineD fline in node.FrontageLines)
                            {
                                g.DrawLine(fLinePen, new Point((int)fline.Start.X, (int)fline.Start.Y), new Point((int)fline.End.X, (int)fline.End.Y));
                            }
                                
                        }
                        if (node.RestrictionLines != null)
                        {
                            foreach (LineD rline in node.RestrictionLines)
                            {
                                g.DrawLine(rLinePen, new Point((int)rline.Start.X, (int)rline.Start.Y), new Point((int)rline.End.X, (int)rline.End.Y));
                            }
                        }
                        
                        /*foreach (PointD vertice in node.Building[bStyle].Vertices)
                        {
                            g.FillEllipse(Brushes.Red, (float) vertice.X-1.0f, (float) vertice.Y- 1.0f, 2.0f, 2.0f);
                        }*/
                            
                    }
                    //if(node.Buildable != null) g.FillPolygon(Brushes.Green, this.PointDToPointArray(node.Buildable.Vertices));
                }
                if (LayoutParameter.FrontageLine != 0.0d)
                {
                    /*foreach (Polygon poly in Frontage)
                    {
                        //g.DrawPolygon(fLinePen, this.PointDToPointArray(poly.Vertices));
                    }*/
                }
                
            }
        }

        private Point[] PointDToPointArray(PointD[] points)
        {
            Point[] tmpArray = new Point[points.Length];
            for (int i = 0; i < points.Length; i++)
            {
                tmpArray[i] = new Point((int)points[i].X, (int)points[i].Y);
            }
            return tmpArray;
        }

        # endregion

        # region CHECKBOXES

        # endregion

        # region RADIOBUTTONS

        private void rB_Row_CheckedChanged(object sender, EventArgs e)
        {
            if (rB_Row.Checked)
            {
                bStyle = 0;
            }

            pB_Lines.Invalidate();
        }

        private void rB_Block_CheckedChanged(object sender, EventArgs e)
        {
            if (rB_Block.Checked)
            {
                bStyle = 1;
            }

            pB_Lines.Invalidate();
        }

        private void rB_Line_CheckedChanged(object sender, EventArgs e)
        {
            if (rB_Line.Checked)
            {
                bStyle = 2;
            }

            pB_Lines.Invalidate();
        }

        private void rB_Free_CheckedChanged(object sender, EventArgs e)
        {
            if (rB_Free.Checked)
            {
                bStyle = 3;
            }

            pB_Lines.Invalidate();
        }

        # endregion

        # region RANGEBUTTON

        /// <summary>
        /// Method called on range changed event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnRangeChanged(object sender, System.EventArgs e)
        {
            bool changed = false;
            this.buildingDepthMin = this.rangeBar1.RangeMinimum;
            if (maxBDepth > 0)
                if (buildingDepthMin > Convert.ToInt32(maxBDepth) - 1) buildingDepthMin = Convert.ToInt32(maxBDepth) - 1; changed = true;
            this.buildingDepthMax = this.rangeBar1.RangeMaximum;
            if (maxBDepth > 0)
                if (buildingDepthMax > Convert.ToInt32(maxBDepth)) buildingDepthMax = Convert.ToInt32(maxBDepth); changed = true;
            if (changed) this.rangeBar1.SelectRange(buildingDepthMin, buildingDepthMax);
            LayoutParameter.MinBuildingDepth = this.buildingDepthMin;
            LayoutParameter.MaxBuildingDepth = this.buildingDepthMax;
            this.tB_BDepth_Min.Text = this.buildingDepthMin.ToString();
            this.tB_BDepth_Max.Text = this.buildingDepthMax.ToString();

            tB_BDepth_Max.BackColor = Color.FromKnownColor(KnownColor.ControlLightLight);
            tB_BDepth_Min.BackColor = Color.FromKnownColor(KnownColor.ControlLightLight);

            this.Graph = this.LayoutCopy;
            CreateSubdivision();
            pB_Lines.Invalidate();
        }

        /// <summary>
        /// Method called on range changing event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnRangeChanging(object sender, System.EventArgs e)
        {
            this.buildingDepthMin = this.rangeBar1.RangeMinimum;
            this.buildingDepthMax = this.rangeBar1.RangeMaximum;
            this.tB_BDepth_Min.Text = this.buildingDepthMin.ToString();
            this.tB_BDepth_Max.Text = this.buildingDepthMax.ToString();

            tB_BDepth_Max.BackColor = Color.FromKnownColor(KnownColor.ControlLight);
            tB_BDepth_Min.BackColor = Color.FromKnownColor(KnownColor.ControlLight);
        }

        private void rangeBar_BLines_RangeChanged(object sender, EventArgs e)
        {
            this.buildingFrontLine = this.rangeBar_BLines.RangeMinimum;
            this.buildingRestrictionLine = this.rangeBar_BLines.RangeMaximum;
            this.UpdateBDepth();

            if (this.bFLine == true)
            {
                LayoutParameter.FrontageLine = this.buildingFrontLine;
            }
            else LayoutParameter.FrontageLine = 0.0d;
            if (this.bRLine == true)
            {
                LayoutParameter.BuildingRestrictionLine = this.buildingRestrictionLine;
            }
            else LayoutParameter.BuildingRestrictionLine = 0.0d;

            this.tB_BFrontLine.Text = this.buildingFrontLine.ToString();
            this.tB_BRestrictionLine.Text = this.buildingRestrictionLine.ToString();

            this.tB_BFrontLine.BackColor = Color.FromKnownColor(KnownColor.ControlLightLight);
            this.tB_BRestrictionLine.BackColor = Color.FromKnownColor(KnownColor.ControlLightLight);

            if (bFLine == true || bRLine == true)
            {
                this.Graph = this.LayoutCopy;
                CreateSubdivision();
                pB_Lines.Invalidate();
            }
        }

        private void rangeBar_BLines_RangeChanging(object sender, EventArgs e)
        {
            this.buildingFrontLine = this.rangeBar_BLines.RangeMinimum;
            if (this.buildingRestrictionLine < this.buildingFrontLine) this.buildingRestrictionLine = this.rangeBar_BLines.RangeMaximum = Convert.ToInt32(this.buildingFrontLine);
            else this.buildingRestrictionLine = this.rangeBar_BLines.RangeMaximum;
            this.tB_BFrontLine.Text = this.buildingFrontLine.ToString();
            this.tB_BRestrictionLine.Text = this.buildingRestrictionLine.ToString();

            this.tB_BFrontLine.BackColor = Color.FromKnownColor(KnownColor.ControlLightLight);
            this.tB_BRestrictionLine.BackColor = Color.FromKnownColor(KnownColor.ControlLightLight);
        }

        # endregion

        # region TEXTBOXES

        /// <summary>
        /// On key up max area textbox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tB_MaxArea_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                maxArea = Convert.ToDouble(this.tB_MaxArea.Text);
                LayoutParameter.MaxAreaValue = maxArea;
                this.Graph = this.LayoutCopy;
                CreateSubdivision();
                pB_Lines.Invalidate();
            }
        }

        /// <summary>
        /// On key up connection distance textbox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tB_Connection_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                connectionDist = Convert.ToDouble(this.tB_Connection.Text);
                LayoutParameter.ConnectionDistance = connectionDist;
                this.Graph = this.LayoutCopy;
                CreateSubdivision();
                pB_Lines.Invalidate();
            }
        }

        /// <summary>
        /// On key up street offset textbox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tB_Offset_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                streetOffset = Convert.ToDouble(this.tB_Offset.Text);
                LayoutParameter.StreetOffset = streetOffset;
                this.Graph = this.LayoutCopy;
                CreateSubdivision();
                pB_Lines.Invalidate();
            }
        }

        /// <summary>
        /// On key up density textbox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tB_Density_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                density = Convert.ToDouble(this.tB_Density.Text);
                LayoutParameter.Density = density;
                this.trB_Density.Value = (int)(density * 100);
                this.Graph = this.LayoutCopy;
                CreateSubdivision();
                pB_Lines.Invalidate();
            }
        }

        private void trB_Density_MouseUp(object sender, MouseEventArgs e)
        {
            density = (Convert.ToDouble(this.trB_Density.Value)) / 100.0d;
            this.tB_Density.Text = density.ToString();
            LayoutParameter.Density = density;

            this.Graph = this.LayoutCopy;
            CreateSubdivision();
            pB_Lines.Invalidate();
        }

        private void tB_BDepth_Min_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                buildingDepthMin = Convert.ToInt32(this.tB_BDepth_Min.Text);
                if (maxBDepth > 0)
                    if (buildingDepthMin > Convert.ToInt32(maxBDepth) - 1) buildingDepthMin = Convert.ToInt32(maxBDepth) - 1;
                LayoutParameter.MinBuildingDepth = buildingDepthMin;
                Console.WriteLine(buildingDepthMin.ToString());
                this.rangeBar1.SelectRange(buildingDepthMin, buildingDepthMax);
                this.Graph = this.LayoutCopy;
                CreateSubdivision();
                pB_Lines.Invalidate();
            }
        }

        private void tB_BDepth_Max_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                buildingDepthMax = Convert.ToInt32(this.tB_BDepth_Max.Text);
                if (maxBDepth > 0)
                    if (buildingDepthMax > Convert.ToInt32(maxBDepth)) buildingDepthMax = Convert.ToInt32(maxBDepth);
                LayoutParameter.MaxBuildingDepth = buildingDepthMax;
                Console.WriteLine(buildingDepthMax.ToString());
                this.rangeBar1.SelectRange(buildingDepthMin, buildingDepthMax);
                this.Graph = this.LayoutCopy;
                CreateSubdivision();
                pB_Lines.Invalidate();
            }
        }

        private void tB_BFrontLine_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                buildingFrontLine = Convert.ToDouble(this.tB_BFrontLine.Text);
                LayoutParameter.FrontageLine = buildingFrontLine;
                Console.WriteLine(buildingFrontLine.ToString());
                this.rangeBar_BLines.SelectRange(Convert.ToInt32(buildingFrontLine), Convert.ToInt32(buildingRestrictionLine));
                this.UpdateBDepth();
                if (!bFLine) return;
                this.Graph = this.LayoutCopy;
                CreateSubdivision();
                pB_Lines.Invalidate();
            }
        }

        private void tB_BRestrictionLine_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                buildingRestrictionLine = Convert.ToDouble(this.tB_BRestrictionLine.Text);
                LayoutParameter.BuildingRestrictionLine = buildingRestrictionLine;
                Console.WriteLine(buildingRestrictionLine.ToString());
                this.rangeBar_BLines.SelectRange(Convert.ToInt32(buildingFrontLine), Convert.ToInt32(buildingRestrictionLine));
                this.UpdateBDepth();
                if (!bRLine) return;
                this.Graph = this.LayoutCopy;
                CreateSubdivision();
                pB_Lines.Invalidate();
            }
        }

        # endregion


        # region CHECKBOXES

        private void cB_BFLine_CheckedChanged(object sender, EventArgs e)
        {
            if (cB_BFLine.Checked)
            {
                bFLine = true;
                this.LayoutParameter.FrontageLine = this.buildingFrontLine;
            }
            else
            {
                bFLine = false;
                this.LayoutParameter.FrontageLine = 0.0d;
            }

            this.UpdateBDepth();
            this.Graph = this.LayoutCopy;
            CreateSubdivision();
            pB_Lines.Invalidate();
        }

        private void cB_BRLine_CheckedChanged(object sender, EventArgs e)
        {
            if (cB_BRLine.Checked)
            {
                bRLine = true;
                this.LayoutParameter.BuildingRestrictionLine = this.buildingRestrictionLine;
            }
            else
            {
                bRLine = false;
                this.LayoutParameter.BuildingRestrictionLine = 0.0d;
            }

            this.UpdateBDepth();
            this.Graph = this.LayoutCopy;
            CreateSubdivision();
            pB_Lines.Invalidate();
        }

        # endregion

        private void UpdateBDepth()
        {
            bool changed = false;
            maxBDepth = LayoutParameter.BuildingRestrictionLine - LayoutParameter.FrontageLine;

            if (maxBDepth > 0)
            {
                if (maxBDepth < this.buildingDepthMin) buildingDepthMin = Convert.ToInt32(maxBDepth) - 1; LayoutParameter.MinBuildingDepth = buildingDepthMin; changed = true; this.tB_BDepth_Min.Text = this.buildingDepthMin.ToString();
                if (maxBDepth < this.buildingDepthMax) buildingDepthMax = Convert.ToInt32(maxBDepth); LayoutParameter.MaxBuildingDepth = buildingDepthMax; changed = true; this.tB_BDepth_Max.Text = this.buildingDepthMax.ToString();
                if (changed) this.rangeBar1.SelectRange(buildingDepthMin, buildingDepthMax);

                /*if (changed)
                {
                    this.rangeBar1.SelectRange(buildingDepthMin, buildingDepthMax);
                    this.Graph = this.LayoutCopy;
                    CreateSubdivision();
                    pB_Lines.Invalidate();
                }*/
            }
        }

        # endregion





        # endregion


    }
}

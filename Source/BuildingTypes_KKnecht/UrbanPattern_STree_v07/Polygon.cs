﻿// Polygon.cs
//----------------------------------
// Last modified on 04/17/2012
// by Katja Knecht
//
// Description:
// The Polygon class describes a two-dimensional polygon. A Polygon object represents e.g. regions in a sub division tree.
//
// Comments:
//
// To Dos:
// 

using System;
using System.Collections.Generic;
using ClipperLib;
using Tektosyne.Geometry;


public class Polygon
{
    # region PROPERTIES

    public static readonly Polygon Empty;

    /// <summary>
    /// Gets or sets the array of vertices of a Polygon object.
    /// </summary>
    /// <value>(PointF[]) vertices.</value>
    public PointD[] Vertices
    {
        get;
        set;
    }

    /// <summary>
    /// Gets the array of polygon edges of a Polygon object.
    /// </summary>
    /// <value>(LineF[]) edges.</value>
    public LineD[] Edges
    {
        get;
        private set;
    }

    /// <summary>
    /// Gets the bounding rectangle of the Polygon.
    /// </summary>
    public RectD Bounds
    {
        get;
        private set;
    }

    # endregion 

    # region CONSTRUCTORS

    /// <summary>
    /// Initializes a new instance of the Polygon type using the specified vertex points.
    /// </summary>
    /// <param name="_vertices">(PointD[]) vertex points.</param>
    public Polygon(PointD[] _vertices)
    {
        this.Vertices = _vertices;
        this.Edges = GeoAlgorithms.ConnectPoints(true, _vertices);
        if(_vertices.Length != 0) this.Bounds = RectD.Circumscribe(Vertices);
    }

    # endregion 

    # region METHODS

    /// <summary>
    /// Computes the area of the <b>Polygon</b>
    /// </summary>
    /// <returns>
    /// The area of the <b>Polygon</b>, as <see cref="double"/> value.
    /// </returns>
    public Double GetArea()
    {
        double area;
        area = Math.Abs(GeoAlgorithms.PolygonArea(this.Vertices));
        return area;
    }

    # region INTERSECTION AND SPLIT METHODS

    /// <summary>
    /// Finds the intersection line with a specified line.
    /// </summary>
    /// <param name="_line">(LineD) line.</param>
    /// <returns>(LineD) intersection.</returns>
    public LineD IntersectsWith(LineD _line)
    {
        PointD pt;
        PointD[] ipts = new PointD[2];
        int counter = 0;
        LineIntersection intersection = new LineIntersection();

        for (int i = 0; i < this.Edges.Length; i++)
        {
            try
            {
                intersection = this.Edges[i].Intersect(_line);
            }
            catch
            {
                Console.WriteLine("Error in method IntersectsWith Polygon. Intersection could not be calculated.");
            }
                
            if (intersection.Exists)
            {
                pt = intersection.Shared.Value;
                ipts[counter] = pt;
                counter++;
            }
            if (counter == 2) break;
        }
        return new LineD(ipts[0], ipts[1]);
    }

    /// <summary>
    /// Calculates the line intersection between the Polygon and the given line and its parallel perpendicular to the specified edge and returns the intersecting polygon with this Node's region.
    /// </summary>
    /// <param name="_perpendicular">(LineD) line perpendicular to specified edge for intersection.</param>
    /// <param name="_perpParallel">(LineD) parallel to perpendicular line for intersection.</param>
    /// <param name="_edge">(LineD) edge perpendicular to line.</param>
    /// <param name="_polygon">(PointD[]) intersecting polygon.</param>
    /// <returns>(LineD) intersection line.</returns>
    public LineD PerpendicularIntersection(LineD _perpendicular, LineD _perpParallel, LineD _edge, out PointD[] _polygon)
    {
        PointD pt; // temporary point
        PointD[] ipts = new PointD[2]; // points for intersection line
        int counter = 0;

        LineIntersection intersection = new LineIntersection();
        LineD prevEdge = new LineD(), nextEdge = new LineD();
        List<LineD> inBetweenP = new List<LineD>(), inBetweenN = new List<LineD>();
        List<PointD> tmpPointList = new List<PointD>();

        // swap lines if polygon direction is counter clockwise or offset of parallel line in other direction
        if (_edge.Intersect(_perpendicular).Shared.Value.DistanceTo(_edge.End) > _edge.Intersect(_perpParallel).Shared.Value.DistanceTo(_edge.End))
        {
            LineD tmp = _perpendicular;
            _perpendicular = _perpParallel;
            _perpParallel = tmp;
        }

        intersection = _edge.Intersect(_perpendicular);
        pt = intersection.Shared.Value;

        tmpPointList.Add(pt);

        for (int i = 0; i < this.Edges.Length; i++)
        {
            if (this.Edges[i].End == _edge.Start)
            {
                prevEdge = this.Edges[i];
            }
            else if (this.Edges[i].Start == _edge.End)
            {
                nextEdge = this.Edges[i];
            }
        }

        intersection = _edge.Intersect(_perpParallel);

        if (intersection.First != LineLocation.Between && intersection.First != LineLocation.End && intersection.First != LineLocation.Start)
        {
            tmpPointList.Add(_edge.Start);

            if (prevEdge != null)
            {
                while (prevEdge.Intersect(_perpParallel).First != LineLocation.Between && prevEdge.Intersect(_perpParallel).First != LineLocation.End && prevEdge.Intersect(_perpParallel).First != LineLocation.Start)
                {

                    double angle = prevEdge.Vector.AngleBetween(_edge.Vector);
                    if (inBetweenP.Count != 0 && inBetweenP[0] == prevEdge)
                    {
                        _polygon = new PointD[0];
                        return LineD.Empty;
                    }
                    inBetweenP.Add(prevEdge);
                    for (int i = 0; i < this.Edges.Length; i++)
                    {
                        if (this.Edges[i].End == inBetweenP[inBetweenP.Count - 1].Start)
                        {
                            tmpPointList.Add(prevEdge.Start);
                            prevEdge = this.Edges[i];
                            break;
                        }
                    }
                }

                intersection = prevEdge.Intersect(_perpParallel);
                if (intersection.Shared != null)
                {
                    pt = intersection.Shared.Value;
                    ipts[counter] = pt;
                    counter++;

                    tmpPointList.Add(pt);
                }
                else
                {
                    Console.WriteLine("no intersection");
                }
            }
            for (int i = 0; i < this.Edges.Length; i++)
            {
                if (this.Edges[i].End == prevEdge.Start)
                {
                    prevEdge = this.Edges[i];
                }
            }
        }
        else
        {
            pt = intersection.Shared.Value;
            tmpPointList.Add(pt);
        }

        if (prevEdge != null)
        {
            inBetweenP.Clear();
            while (prevEdge.Intersect(_perpParallel).First != LineLocation.Between && prevEdge.Intersect(_perpParallel).First != LineLocation.End && prevEdge.Intersect(_perpParallel).First != LineLocation.Start)
            {
                double angle = prevEdge.Vector.AngleBetween(_edge.Vector);
                inBetweenP.Add(prevEdge);
                for (int i = 0; i < this.Edges.Length; i++)
                {
                    if (this.Edges[i].End == inBetweenP[inBetweenP.Count - 1].Start)
                    {
                        prevEdge = this.Edges[i];
                        break;
                    }
                }
            }

            intersection = prevEdge.Intersect(_perpParallel);
            if (intersection.Shared != null)
            {
                pt = intersection.Shared.Value;
                ipts[counter] = pt;
                counter++;

                tmpPointList.Add(pt);
            }
            else
            {
                Console.WriteLine("no intersection");
            }
        }

        if (nextEdge != null)
        {
            int count = 0;
            while (nextEdge.Intersect(_perpendicular).First != LineLocation.Between && nextEdge.Intersect(_perpendicular).First != LineLocation.End && nextEdge.Intersect(_perpendicular).First != LineLocation.Start)
            {
                System.Console.WriteLine(count);
                inBetweenN.Add(nextEdge);
                for (int i = 0; i < this.Edges.Length; i++)
                {
                    if (this.Edges[i].Start == inBetweenN[inBetweenN.Count - 1].End)
                    {
                        nextEdge = this.Edges[i];
                        break;
                    }
                }
                count ++;
            }

            intersection = nextEdge.Intersect(_perpendicular);
            if (intersection.Shared != null)
            {
                pt = intersection.Shared.Value;
            }
            else
            {
                Console.WriteLine("no intersection");
            }
        }

        if (prevEdge != nextEdge)
        {
            inBetweenP.Clear();

            LineD tmpEdge = prevEdge;

            while (tmpEdge!= nextEdge)
            {
                inBetweenP.Add(tmpEdge);
                for (int i = 0; i < this.Edges.Length; i++)
                {
                    if (this.Edges[i].End == inBetweenP[inBetweenP.Count - 1].Start)
                    {
                        tmpEdge = this.Edges[i];
                        break;
                    }
                }
            }

            for (int i = 0; i < inBetweenP.Count; i++)
            {
                if (inBetweenP[i].Length != 0.0d)
                {
                    tmpPointList.Add(inBetweenP[i].Start);
                }
            }
        }

        tmpPointList.Add(pt);

        _polygon = tmpPointList.ToArray();

        return new LineD(ipts[0], ipts[1]);
    }

    /// <summary>
/// Calculates the line intersection between the Polygon and the given line using the specified edge parallel to the line and returns the intersection polygon.
/// </summary>
/// <param name="_line">(LineD) line for intersection.</param>
/// <param name="_parallelEdge">(LineD) edge parallel to line.</param>
/// <param name="_polygon">(PointD[]) intersection polygon.</param>
/// <returns>(LineD) intersection line.</returns>
    public LineD ParallelIntersection(LineD _line, LineD _parallelEdge, out PointD[] _polygon)
    {
        PointD pt; // temporary point
        PointD[] ipts = new PointD[2]; // points for intersection line
        int counter = 0;

        LineIntersection intersection = new LineIntersection();
        LineD prevEdge = new LineD(), nextEdge = new LineD();
        List<LineD> inBetweenP = new List<LineD>(), inBetweenN = new List<LineD>();
        List<PointD> tmpPointList = new List<PointD>();

        for (int i = 0; i < this.Edges.Length; i++)
        {
            if (this.Edges[i].End == _parallelEdge.Start)
            {
                prevEdge = this.Edges[i];
            }
            else if (this.Edges[i].Start == _parallelEdge.End)
            {
                nextEdge = this.Edges[i];
            }
        }

        tmpPointList.Add(_parallelEdge.Start);
        tmpPointList.Add(_parallelEdge.End);

        if(prevEdge != null)
        {
            while (prevEdge.Intersect(_line).First != LineLocation.Between && prevEdge.Intersect(_line).First != LineLocation.End && prevEdge.Intersect(_line).First != LineLocation.Start )
            {
                double angle = prevEdge.Vector.AngleBetween(_parallelEdge.Vector);
                if (inBetweenP.Count != 0 && inBetweenP[0] == prevEdge)
                {
                    _polygon = new PointD[0];
                    return LineD.Empty;
                }
                inBetweenP.Add(prevEdge);
                for (int i = 0; i < this.Edges.Length; i++)
                {
                    if (this.Edges[i].End == inBetweenP[inBetweenP.Count-1].Start)
                    {
                        prevEdge = this.Edges[i];
                        break;
                    }
                }
            }

            intersection = prevEdge.Intersect(_line);
            if (intersection.Shared != null)
            {
                pt = intersection.Shared.Value;
                ipts[counter] = pt;
                counter++;
            }
            else
            {
                Console.WriteLine("no intersection");
            }
        }

        if (nextEdge != null)
        {
            while (nextEdge.Intersect(_line).First != LineLocation.Between && nextEdge.Intersect(_line).First != LineLocation.End && nextEdge.Intersect(_line).First != LineLocation.Start)
            {
                if (inBetweenN.Count != 0 && inBetweenN[0] == nextEdge)
                {
                    _polygon = new PointD[0];
                    return LineD.Empty;
                }
                inBetweenN.Add(nextEdge);
                for (int i = 0; i < this.Edges.Length; i++)
                {
                    if (this.Edges[i].Start == inBetweenN[inBetweenN.Count-1].End)
                    {
                        nextEdge = this.Edges[i];
                        break;
                    }
                }
            }

            intersection = nextEdge.Intersect(_line);
            if (intersection.Shared != null)
            {
                pt = intersection.Shared.Value;
                ipts[counter] = pt;
                counter++;
            }
            else
            {
                Console.WriteLine("no intersection");
            }
        }

        for (int i = 0; i < inBetweenN.Count; i++)
        {
            if (inBetweenN[i].Length != 0.0d)
            {
                tmpPointList.Add(inBetweenN[i].End);
            }
        }

        List<PointD> tmpList = new List<PointD>() { ipts[0], ipts[1] };
        int idx = GeoAlgorithms.NearestPoint(tmpList, tmpPointList[tmpPointList.Count -1]);
        tmpPointList.Add(tmpList[idx]);
        tmpList.Remove(tmpList[idx]);
        tmpPointList.Add(tmpList[0]);

        for (int i = inBetweenP.Count - 1; i >= 0; i--)
        {
            if (inBetweenP[i].Length != 0.0d)
            {
                tmpPointList.Add(inBetweenP[i].Start);
            }
        }

        _polygon = tmpPointList.ToArray();

        return new LineD(ipts[0], ipts[1]);
    }

    /// <summary>
    /// Finds the intersection line with a specified line and returns all sections of the polygon.
    /// </summary>
    /// <param name="_line">(LineD) line.</param>
    /// <param name="_sections">(LineD[]) sections of the polygon including intersecting line.</param>
    /// <returns>(LineD) intersection.</returns>
    public LineD IntersectsWith(LineD _line, out LineD[] _sections)
    {
        PointD pt;
        PointD[] ipts = new PointD[2];
        int[] idx = new int[2];
        int counter = 0;
        LineIntersection intersection = new LineIntersection();
        List<LineD> tmpList = new List<LineD>();
        LineD tmp;

        if (_line.Length != 0 && this.Bounds.IntersectsWith(_line))
        {
            for (int i = 0; i < this.Edges.Length; i++)
            {
                try
                {
                    intersection = this.Edges[i].Intersect(_line);
                }
                catch
                {
                    Console.WriteLine("Error in method IntersectsWith. Intersection could not be calculated.");
                }

                if (intersection != null && intersection.Exists && counter < 2)
                {
                    pt = intersection.Shared.Value;
                    ipts[counter] = pt;
                    tmp = new LineD(this.Edges[i].Start, pt);
                    if (tmp.Length != 0) tmpList.Add(new LineD(this.Edges[i].Start, pt));
                    tmp = new LineD(pt, this.Edges[i].End);
                    if (tmp.Length != 0) tmpList.Add(new LineD(pt, this.Edges[i].End));
                    counter++;
                }
                else
                {
                    tmpList.Add(this.Edges[i]);
                }
            }
            LineD split = new LineD(ipts[0], ipts[1]);
            if(split.Length != 0) tmpList.Add(split);
            _sections = new LineD[tmpList.Count];

            for (int i = 0; i < _sections.Length; i++)
            {
                _sections[i] = tmpList[i];
            }
            return split;
        }
        else
        {
            _sections = new LineD[0];
            return LineD.Empty;
        }
    }

    /// <summary>
    /// Calculates the split polygons using the specified lines array.
    /// </summary>
    /// <param name="_lines">(LineD[]) lines.</param>
    /// <returns>(Polygon[]) split polygons.</returns>
    public Polygon[] CalculateSplitPolygons(LineD[] _lines)
    {
        Subdivision subd = new Subdivision();
        PointD[][] tempPolygons;
        Polygon[] polygons;

        subd = Subdivision.FromLines(_lines);
        tempPolygons = subd.ToPolygons();
        polygons = new Polygon[tempPolygons.GetLength(0)];

        try
        {
            polygons = this.RectifyPolygonSuccession(tempPolygons);
        }
        catch
        {
            for (int i = 0; i < tempPolygons.GetLength(0); i++)
            {
                polygons[i] = new Polygon(tempPolygons[i]);
            }
        }

        for (int i = 0; i < polygons.Length; i++)
        {
            tempPolygons[i] = new PointD[polygons[i].Vertices.Length];
            for (int j = 0; j < polygons[i].Vertices.Length; j++)
            {
                tempPolygons[i][j] = polygons[i].Vertices[j];
            }
        }

        return polygons;
    }

    /// <summary>
    /// Rectifies the succession of the polygons in the array. 0 - left or top polygon, 1 - right or bottom polygon.
    /// </summary>
    /// <param name="_polygons">(PointD[][]) polygons.</param>
    /// <returns>(Polygon[]) rectified polygons.</returns>
    private Polygon[] RectifyPolygonSuccession(PointD[][] _polygons)
    {
        PointD[] vertices = new PointD[2];
        Polygon[] temp = new Polygon[2];
        LineD intersection;
        int count = 0;
        for(int i = 0; i< _polygons.GetLength(0)-1; i++)
        {
            for (int j = 0; j < _polygons[i].Length; j++)
            {
                for (int k = 0; k < _polygons[i + 1].Length; k++)
                {
                    if (_polygons[i][j] == _polygons[i + 1][k])
                    {
                        vertices[count] = _polygons[i][j];
                        count++;
                    }
                    if (count > 1) break;
                }
                if (count > 1) break;
            }
            if (count > 1) break;
        }
        intersection = new LineD(vertices[0], vertices[1]);

        int n = 0;
        while (intersection.Locate(_polygons[0][n]) == LineLocation.Start || intersection.Locate(_polygons[0][n]) == LineLocation.End)
        {
            n++;
        }

        double min = -Math.PI * 3 / 4;
        double max = Math.PI / 4;

        if (_polygons.GetLength(0) > 1)
        {
            if (intersection.Angle > min && intersection.Angle < max)
            {
                if (intersection.Locate(_polygons[0][n]) == LineLocation.Left)
                {
                    temp[0] = new Polygon(_polygons[1]);
                    temp[1] = new Polygon(_polygons[0]);
                }
                else
                {
                    temp[0] = new Polygon(_polygons[0]);
                    temp[1] = new Polygon(_polygons[1]);
                }
            }
            else
            {
                if (intersection.Locate(_polygons[0][n]) == LineLocation.Right)
                {
                    temp[0] = new Polygon(_polygons[1]);
                    temp[1] = new Polygon(_polygons[0]);
                }
                else
                {
                    temp[0] = new Polygon(_polygons[0]);
                    temp[1] = new Polygon(_polygons[1]);
                }
            }
        }
        else temp = new Polygon[1] {new Polygon(_polygons[0])};
        return temp;
    }

    # endregion

    /// <summary>
    /// Moves a Polygon object to and parallel a specified line.
    /// </summary>
    /// <param name="_line">(LineD) line.</param>
    public void MoveTo(LineD _line)
    {
        PointD midptL = CalculateMidPoint(_line);
        List<PointD> midpts = new List<PointD>();

        for (int i = 0; i < this.Edges.Length; i++)
        {
            midpts.Add(CalculateMidPoint(this.Edges[i]));
        }

        int nearest = GeoAlgorithms.NearestPoint(midpts, midptL);
        this.Move(midptL, midpts[nearest]);
        this.Rotate( this.Edges[nearest], _line);
    }

    /// <summary>
    /// Rotates a Polygon object to a given line using its midpoint as origin and reference edge.
    /// </summary>
    /// <param name="_origin">(PointD) rotation origin.</param>
    /// <param name="_reference">(LineD) reference edge of Polygon.</param>
    /// <param name="_line">(LineD) end of rotation.</param>
    public void Rotate(LineD _reference, LineD _line)
    {
        LineIntersection inter = _reference.Intersect(_line);
        if (inter.Relation != LineRelation.Divergent) return;

        PointD refpt =  _reference.Start;
        PointD nearest = (refpt.DistanceTo(_line.Start) < refpt.DistanceTo(_line.End)) ? _line.Start : _line.End;
        PointD _origin = (PointD) inter.Shared;

        double angle, x, y;
        angle = _origin.AngleBetween(refpt, nearest);

        PointD[] vertices = new PointD[this.Vertices.Length];

        for (int i = 0; i < this.Vertices.Length; i++)
        {
            PointD trans = Vertices[i] - _origin;

            x = trans.X * Math.Cos(angle) -trans.Y * Math.Sin(angle);
            y = trans.X * Math.Sin(angle) + trans.Y * Math.Cos(angle);
            
            vertices[i] = new PointD(x, y) + _origin;
        }

        this.Update(vertices);
    }

    private void Update(PointD[] _vertices)
    {
        this.Vertices = _vertices;
        this.Edges = GeoAlgorithms.ConnectPoints(true, _vertices);
        if (_vertices.Length != 0) this.Bounds = RectD.Circumscribe(Vertices);
    }

    /// <summary>
    /// Moves the Polygon object to a specified destination using the given reference point.
    /// </summary>
    /// <param name="_destination">(PointD) destination.</param>
    /// <param name="_reference">(PointD) reference.</param>
    public void Move(PointD _destination, PointD _reference)
    {
        double dx = _destination.X - _reference.X;
        double dy = _destination.Y - _reference.Y;

        PointD[] vertices = new PointD[this.Vertices.Length];

        for (int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = new PointD(Vertices[i].X + dx, Vertices[i].Y + dy);
        }

        this.Update(vertices);
    }

    /// <summary>
    /// Calculates the mid point for a given line.
    /// </summary>
    /// <param name="_line">(LineD) line.</param>
    /// <returns>(PointD) mid point.</returns>
    private PointD CalculateMidPoint(LineD _line)
    {
        double x = _line.Start.X + (_line.End.X - _line.Start.X) / 2;
        double y = _line.Start.Y + (_line.End.Y - _line.Start.Y) / 2;
        PointD midpt = new PointD(x, y);
        return midpt;
    }

    # region STATIC METHODS

    /// <summary>
    /// Finds the intersection point between two lines. http://stackoverflow.com/questions/1119451/how-to-tell-if-a-line-intersects-a-polygon-in-c
    /// </summary>
    /// <param name="_line1">(LineD) line 1.</param>
    /// <param name="_line2">(LineD) line 2.</param>
    /// <returns>(PointD) intersection point.</returns>
    public static PointD FindLineIntersection(LineD _line1, LineD _line2)
    {
        double denom = ((_line1.End.X - _line1.Start.X) * (_line2.End.Y - _line2.Start.Y)) - ((_line1.End.Y - _line1.Start.Y) * (_line2.End.X - _line2.Start.X));

        //  AB & CD are parallel 
        if (denom == 0)
            return PointF.Empty;

        double numer = ((_line1.Start.Y - _line2.Start.Y) * (_line2.End.X - _line2.Start.X)) - ((_line1.Start.X - _line2.Start.X) * (_line2.End.Y - _line2.Start.Y));

        double r = numer / denom;

        double numer2 = ((_line1.Start.Y - _line2.Start.Y) * (_line1.End.X - _line1.Start.X)) - ((_line1.Start.X - _line2.Start.X) * (_line1.End.Y - _line1.Start.Y));

        double s = numer2 / denom;

        if ((r < 0 || r > 1) || (s < 0 || s > 1))
            return PointF.Empty;

        // Find intersection point
        PointD result = new PointD(_line1.Start.X + (r * (_line1.End.X - _line1.Start.X)), _line1.Start.Y + (r * (_line1.End.Y - _line1.Start.Y)));

        return result;
    }

    /// <summary>
    /// Clips two specified Polygon objects and returns their intersection.
    /// </summary>
    /// <param name="_clip">(Polygon) clip polygon.</param>
    /// <param name="_subject">(Polygon) subject polygon.</param>
    /// <returns>(Polygon) intersection polygon.</returns>
    public static Polygon ClipPolygons(Polygon _clip, Polygon _subject)
    {
        List<List<IntPoint>> clipList = new List<List<IntPoint>>();
        List<List<IntPoint>> solution = new List<List<IntPoint>>();
        List<List<IntPoint>> solution2 = new List<List<IntPoint>>();
        List<IntPoint> tmp1 = new List<IntPoint>();
        List<IntPoint> tmp2 = new List<IntPoint>();
        Clipper c = new Clipper();

        tmp2 = _subject.FromPolygonToListIntPoint();
        clipList.Add(tmp2);
        c.AddPaths(clipList, PolyType.ptSubject,true);

        tmp1 = _clip.FromPolygonToListIntPoint();
        clipList.Add(tmp1);
        c.AddPaths(clipList, PolyType.ptClip,true);
            
        c.Execute(ClipType.ctXor, solution, PolyFillType.pftPositive, PolyFillType.pftPositive);
        if (solution.Count == 0) 
            Console.WriteLine("no intersection");
        else
            Console.WriteLine("intersection");

        clipList.Clear();
        clipList.Add(tmp1);
        clipList.Add(solution[0]);
        c.Execute(ClipType.ctDifference, solution2);
        
        Polygon poly;
        if (solution2.Count > 0)
        {
            poly = Polygon.FromListIntPointToPolygon(solution2[0]);
        }
        else
        {
            poly = Polygon.Empty;
        }
        return poly;
    }

    private static Polygon FromListIntPointToPolygon(List<IntPoint> _intPTList)
    {
        Polygon poly;
        PointD[] polygon = new PointD[_intPTList.Count];

        for (int i = 0; i < _intPTList.Count; i++)
        {
            polygon[i] = new PointD((double)_intPTList[i].X, (double)_intPTList[i].Y);
        }
        poly = new Polygon(polygon);
        return poly;
    }

    private List<IntPoint> FromPolygonToListIntPoint()
    {
        List<IntPoint> tmp = new List<IntPoint>();

        for (int i = 0; i < this.Vertices.Length; i++)
        {
            tmp.Add(new IntPoint((long)this.Vertices[i].X, (long)this.Vertices[i].Y));
        }
        return tmp;
    }


    /// <summary>
    /// Offsets the given polygons by the specified offset distance.
    /// </summary>
    /// <param name="_polygons">(Polygon[]) polygons.</param>
    /// <param name="_offsetDist">(double) offset distance.</param>
    /// <returns>(Polygon[]) offset polygons.</returns>
    public static Polygon[] OffsetPolygons(Polygon[] _polygons, double _offsetDist)
    {
        List<List<IntPoint>> tmppts = new List<List<IntPoint>>();
        List<PointD[]> tmpPolys = new List<PointD[]>();

        for (int i = 0; i < _polygons.Length; i++)
        {
            List<IntPoint> tmplist = new List<IntPoint>();

            for (int j = 0; j < _polygons[i].Vertices.Length; j++)
            {
                tmplist.Add(new IntPoint((long)_polygons[i].Vertices[j].X, (long)_polygons[i].Vertices[j].Y));
            }
            //tmplist.Reverse();
            tmppts.Add(tmplist);
        }

        //ClipperOffset co = new ClipperOffset();
        //co.AddPaths(pOff, JoinType.jtMiter, EndType.etClosedPolygon);
        //co.Execute(ref offsetPoly, delta);

        List<List<IntPoint>> clippolys = Clipper.OffsetPaths(tmppts, -_offsetDist, JoinType.jtMiter, EndType_.etClosed, 5.0);
        List<PointD> tmpList = new List<PointD>();

        for (int i = 0; i < clippolys.Count; i++)
        {
            tmpList.Clear();
            for (int j = 0; j < clippolys[i].Count; j++)
            {
                PointD tmpPT = new PointD(clippolys[i][j].X, clippolys[i][j].Y);
                bool isUnique = true;
                for (int n = 0; n < tmpList.Count; n++)
                {
                    if (tmpList[n] == tmpPT)
                    {
                        isUnique = false;
                        break;
                    }
                    else if(tmpList[n].DistanceTo(tmpPT) < 1.0d)
                    {
                        isUnique = false;
                        break;
                    }
                }
                if (isUnique) tmpList.Add(tmpPT);
            }
            tmpPolys.Add(tmpList.ToArray());
        }

        Polygon[] tmp = Polygon.ToPolygonArrayFromPointDArray(tmpPolys.ToArray());
        return tmp;
    }

    /// <summary>
    /// Offsets the given polygons by the specified offset distance.
    /// </summary>
    /// <param name="_polygon">(Polygon[]) polygons.</param>
    /// <param name="_offsetDist">(double) offset distance.</param>
    /// <returns>(Polygon[]) offset polygons.</returns>
    public static Polygon OffsetPolygons(Polygon _polygon, double _offsetDist)
    {
        List<List<IntPoint>> tmppts = new List<List<IntPoint>>();
        List<PointD[]> tmpPolys = new List<PointD[]>();

        List<IntPoint> tmplist = new List<IntPoint>();
        for (int j = 0; j < _polygon.Vertices.Length; j++)
        {
            tmplist.Add(new IntPoint((long)_polygon.Vertices[j].X, (long)_polygon.Vertices[j].Y));
        }
        tmppts.Add(tmplist);

        List<List<IntPoint>> clippolys = Clipper.OffsetPaths(tmppts, -_offsetDist, JoinType.jtMiter, EndType_.etClosed, 5.0);
        List<PointD> tmpList = new List<PointD>();

        for (int i = 0; i < clippolys.Count; i++)
        {
            tmpList.Clear();
            for (int j = 0; j < clippolys[i].Count; j++)
            {
                PointD tmpPT = new PointD(clippolys[i][j].X, clippolys[i][j].Y);
                bool isUnique = true;
                for (int n = 0; n < tmpList.Count; n++)
                {
                    if (tmpList[n] == tmpPT)
                    {
                        isUnique = false;
                        break;
                    }
                    else if (tmpList[n].DistanceTo(tmpPT) < 1.0d)
                    {
                        isUnique = false;
                        break;
                    }
                }
                if (isUnique) tmpList.Add(tmpPT);
            }
            tmpPolys.Add(tmpList.ToArray());
        }

        Polygon[] tmp = Polygon.ToPolygonArrayFromPointDArray(tmpPolys.ToArray());
        if (tmp.Length != 0) return tmp[0];
        else return Polygon.Empty;
    }

    /// <summary>
    /// Gets the centroid of a Polygon using the specified vertices.
    /// </summary>
    /// <param name="_vertices">(PointD[]) vertices.</param>
    /// <returns>(PointD) centroid.</returns>
    public static PointD GetCentroidAsPointD(PointD[] _vertices)
    {
        PointD centroid = new PointD();
        double signedArea = 0.0;
        double x0 = 0.0; // Current vertex X
        double y0 = 0.0; // Current vertex Y
        double x1 = 0.0; // Next vertex X
        double y1 = 0.0; // Next vertex Y
        double a = 0.0;  // Partial signed area
        double cx = 0.0d;
        double cy = 0.0d;

        // For all vertices except last
        int i=0;
        for (i=0; i<_vertices.Length-1; ++i)
        {
            x0 = _vertices[i].X;
            y0 = _vertices[i].Y;
            x1 = _vertices[i+1].X;
            y1 = _vertices[i+1].Y;
            a = x0*y1 - x1*y0;
            signedArea += a;
            cx += (x0 + x1)*a;
            cy += (y0 + y1)*a;
        }

        // Do last vertex
        x0 = _vertices[i].X;
        y0 = _vertices[i].Y;
        x1 = _vertices[0].X;
        y1 = _vertices[0].Y;
        a = x0*y1 - x1*y0;
        signedArea += a;
        cx += (x0 + x1)*a;
        cy += (y0 + y1)*a;

        signedArea *= 0.5;
        cx /= (6*signedArea);
        cy /= (6*signedArea);

        centroid = new PointD(cx, cy);

        return centroid;
    }

    /// <summary>
    /// Gets the centroid of a Polygon using the specified vertices.
    /// </summary>
    /// <param name="_vertices">(PointD[]) vertices.</param>
    /// <returns>(Point) centroid.</returns>
    public static System.Drawing.Point GetCentroidAsPoint(PointD[] _vertices)
    {
        PointD pt = GetCentroidAsPointD(_vertices);
        System.Drawing.Point pt1 = new System.Drawing.Point((int)pt.X, (int)pt.Y);
        return pt1;
    }

    /// <summary>
    /// Converts a list of lists of polygon points into a list of point arrays.
    /// </summary>
    /// <param name="_polygons">(List List PointD) polygon points as list.</param>
    /// <returns>(List PointD[]) polygon points as array.</returns>
    public static List<PointD[]> ToListPointDArray (List<List<PointD>> _polygons)
    {
        List<PointD[]> tmpList = new List<PointD[]>();
        PointD[] tmpArray;

        for (int i = 0; i < _polygons.Count; i++)
        {
            tmpArray = new PointD[_polygons[i].Count];
            for (int j = 0; j < tmpArray.Length; j++)
            {
                tmpArray[j] = _polygons[i][j];
            }
            tmpList.Add(tmpArray);
        }

        return tmpList;
    }

    /// <summary>
    /// Converts a list of lists of polygon points into a list of point arrays.
    /// </summary>
    /// <param name="_polygons">(Polygon[]) polygon array.</param>
    /// <returns>(List PointD[]) polygon points as array.</returns>
    public static List<PointD[]> ToListPointDArray(Polygon[] _polygons)
    {
        List<PointD[]> tmpList = new List<PointD[]>();
        PointD[] tmpArray;

        for (int i = 0; i < _polygons.Length; i++)
        {
            tmpArray = _polygons[i].Vertices;
            tmpList.Add(tmpArray);
        }

        return tmpList;
    }

    /// <summary>
    /// Creates an array of Polygons from point arrays.
    /// </summary>
    /// <param name="_polygons">(PointD[][]) polygon points.</param>
    /// <returns>(Polygon[]) polygon array.</returns>
    public static Polygon[] ToPolygonArrayFromPointDArray(PointD[][] _polygons)
    {
        Polygon[] tmpArray = new Polygon[_polygons.GetLength(0)];
        PointD[] tmpPoly;

        for (int i = 0; i < _polygons.GetLength(0); i++)
        {
            tmpPoly = _polygons[i];
            tmpArray[i] = new Polygon(tmpPoly);
        }

        return tmpArray;
    }

    # endregion

    # endregion
}

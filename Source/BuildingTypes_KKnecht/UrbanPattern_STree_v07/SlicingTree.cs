﻿// SlicingTree.cs
//----------------------------------
// Last modified on 04/17/2012
// by Katja Knecht
//
// Description:
// The STree class describes a slicing tree with its nodes and leaves as well as syntax and contains all necessary methods to build and alter such a tree.
//
// Comments:
// 
//

using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using Tektosyne.Geometry;


public class STree
{
    # region PROPERTIES

    /// <summary>
    /// Gets or sets the layout parameters for the building lot as well as  building generation.
    /// </summary>
    public Parameter LayoutParameters
    {
        get;
        set;
    }

    /// <summary>
    /// Gets the list of leaves of a Tree object.
    /// </summary>
    /// <value>(List Node) leaves.</value>
    public List<Node> Leaves
    {
        get;
        private set;
    }

    /// <summary>
    /// Gets the list of nodes of a Tree object.
    /// </summary>
    /// <value>(List Node) nodes.</value>
    public List<Node> Nodes
    {
        get;
        private set;
    }

    /// <summary>
    /// Gets or sets the number of leaves of a Tree object.
    /// </summary>
    /// <value>(int) number of leaves.</value>
    public int NoOfLeaves
    {
        get;
        set;
    }

    /// <summary>
    /// Gets the list of polygons of a Tree object..
    /// </summary>
    /// <value>(List PointD[]) polygons</value>
    public List<PointD[]> Polygons
    {
        get;
        private set;
    }

    /// <summary>
    /// Gets the regions of a Tree object.
    /// </summary>
    /// <value>(Polygon[]) regions.</value>
    public Polygon[] BuildingBlocks
    {
        get;
        private set;
    }

    public Polygon[] Regions
    {
        get;
        private set;
    }

    /// <summary>
    /// Gets the root node of a Tree object.
    /// </summary>
    /// <value>(Node) root.</value>
    private Node Root
    {
        get;
        set;
    }

    /// <summary>
    /// Gets the root nodes of a Tree object.
    /// </summary>
    /// <value>(Node[]) roots.</value>
    public Node[] Roots
    {
        get;
        private set;
    }

    /// <summary>
    /// Gets the graph of the tree.
    /// </summary>
    /// <value>(Subdivision) graph.</value>
    public Subdivision Graph
    {
        get;
        private set;
    }

    /// <summary>
    /// Gets the original graph of the tree.
    /// </summary>
    /// <value>(Subdivision) original graph.</value>
    private Subdivision OriginalGraph
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets the syntax of a Tree object.
    /// </summary>
    /// <value>(String) syntax.</value>
    private String Syntax
    {
        get;
        set;
    }

    /// <summary>
    /// Gets or sets the Syntaxes of the Tree.
    /// </summary>
    /// <value>(String[]) syntaxes.</value>
    public String[] Syntaxes
    {
        get;
        set;
    }

    // Counter
    private int _leafCounter;
    private int _levelCounter;
    private int _indexCounter;

    // Operators
    private char[] functions = new char[] { 'H', 'V' };
    private char[] operators = new char[] { 'H', 'V', 'o' };
    private char terminal = 'o';

    private static double _splitRate = 0.5d;
    private Random rand = new Random(RandomNumberGenerator.Create().GetHashCode());

    # endregion


    # region CONSTRUCTORS

    /// <summary>
    /// Initializes a new instance of the STree type using the specified subdivision, max area and connection distance values.
    /// </summary>
    /// <param name="_graph">(Subdivision) graph.</param>
    /// <param name="_maxArea">(double) max. area.</param>
    /// <param name="_connectionDist">(double) connection distance.</param>
    public STree(Subdivision _graph, double _maxArea, double _connectionDist, double _offset, double _density)
    {
        this.Graph = _graph.Clone() as Subdivision;
        this.OriginalGraph = _graph.Clone() as Subdivision;
        this.LayoutParameters = new Parameter();
        this.LayoutParameters.MaxAreaValue = _maxArea;
        this.LayoutParameters.ConnectionDistance = _connectionDist;
        this.LayoutParameters.StreetOffset = _offset;
        this.LayoutParameters.Density = _density;
        this.Initialize();
    }

    /// <summary>
    /// Initializes a new instance of the STree type using the specified subdivision and max area values.
    /// </summary>
    /// <param name="_graph">(Subdivision) graph.</param>
    /// <param name="_maxArea">(double) max. area.</param>
    public STree(Subdivision _graph, double _maxArea) : this(_graph, _maxArea, 5.0d, 3.0d, 0.3d)
    {
    }

    /// <summary>
    /// Initializes a new instance of the STree type using the specified subdivision and parameters.
    /// </summary>
    /// <param name="_graph">(Subdivision) graph.</param>
    /// <param name="_param">(Parameter) layout parameter.</param>
    public STree(Subdivision _graph, Parameter _param)
    {
        this.Graph = _graph.Clone() as Subdivision;
        this.OriginalGraph = _graph.Clone() as Subdivision;
        this.LayoutParameters = _param;

        this.Initialize();
    }

    # endregion


    # region METHODS

    # region Initialization

    /// <summary>
    /// Initializes the Tree object.
    /// </summary>
    public void Initialize()
    {
        this.Leaves = new List<Node>();
        this.Nodes = new List<Node>();
        this.Polygons = new List<PointD[]>();
        Polygon[] tmpRegions = Polygon.ToPolygonArrayFromPointDArray(this.Graph.ToPolygons());

        this.Regions = Polygon.OffsetPolygons(tmpRegions, this.LayoutParameters.StreetOffset);
        if (this.LayoutParameters.FrontageLine != 0.0d) 
            this.BuildingBlocks = Polygon.OffsetPolygons(tmpRegions, this.LayoutParameters.FrontageLine+this.LayoutParameters.StreetOffset);
        else this.BuildingBlocks = this.Regions;
        this.Graph = Subdivision.FromPolygons(Polygon.ToListPointDArray(this.Regions));
        this.Roots = new Node[this.Regions.Length];

        this._leafCounter = this.NoOfLeaves;
        this._levelCounter = 0;
        this._indexCounter = 0;

        if (this.Syntaxes == null)
        {
            this.Syntaxes = new String[Regions.Length];
            for (int i = 0; i < Regions.Length; i++)
            {
                this._leafCounter = this.NoOfLeaves;
                this._levelCounter = 0;
                this._indexCounter = 0;
                this.Root = new Node(this.Regions[i]);
                if (this.BuildingBlocks.Length > i)
                    this.Root.Buildable = this.BuildingBlocks[i];
                else
                    this.Root.Buildable = Polygon.Empty;
                //CreateTreeAndSyntax uses DetermineOperation which uses this.Root
                this.Roots[i] = this.CreateTreeAndSyntax(this.Regions[i]);
                if (this.BuildingBlocks.Length > i)
                    this.Roots[i].Buildable = this.BuildingBlocks[i];
                else this.Roots[i].Buildable = Polygon.Empty;
                this.Syntaxes[i] = this.Syntax;
                this.Syntax = "";
            }
            //List<LineD> lines = new List<LineD>();
            //leaves were initialized in CreateTreeAndSyntax
            foreach (Node leaf in this.Leaves)
            {
                this.Polygons.Add(leaf.Region.Vertices);
                //foreach (LineD line in leaf.Region.Edges)
                //{
                //    lines.Add(line);
                //}
            }
            this.Graph = this.Merge(this.OriginalGraph, this.Graph);
        }
        else
        {
        }
    }

    /// <summary>
    /// Merges two given subdivision graphs.
    /// </summary>
    /// <param name="_graph1">(Subdivision) graph 1.</param>
    /// <param name="_graph2">(Subdivision) graph 2.</param>
    /// <returns>(Subdivision) merged graph.</returns>
    private Subdivision Merge(Subdivision _graph1, Subdivision _graph2)
    {
        LineD[] lines1 = _graph1.ToLines();
        LineD[] lines2 = _graph2.ToLines();

        LineD[] merged = new LineD[lines1.Length + lines2.Length];
        for (int i = 0; i < merged.Length; i++)
        {
            if (i < lines1.Length) merged[i] = lines1[i];
            else merged[i] = lines2[i - lines1.Length];
        }

        return Subdivision.FromLines(merged);
    }

    # endregion

    # region Tree, Index Sequence and Syntax Creation


    /// <summary>
    /// Creates a syntax and the respective Tree and returns the Tree's root node.
    /// </summary>
    /// <value>(Node) root node.</value>
    private Node CreateTreeAndSyntax(Polygon _region)
    {
        char operation;
        Node newNode = new Node(this._levelCounter);
        newNode.Region = _region;
        double regionArea = _region.GetArea();
        if(regionArea > this.LayoutParameters.MaxAreaValue) // Function nodes
        {
            operation = this.DetermineOperation(newNode);// this.functions[rand.Next(0, 2)];
            this.Syntax += operation + "(";
        }
        else // Leaves
        {
            operation = this.terminal;
            this.Syntax += operation;

            this._leafCounter--;
            this._levelCounter--;
        }

        newNode.Operator = operation;

        if (operation != this.terminal) // continue building the tree
        {
            Polygon[] subpolygons;
            this._indexCounter++;

            subpolygons = this.CreateSubDivision(newNode);

            if (subpolygons.Length <= 1)
            {
                newNode.Operator = terminal;
                newNode.NoOfLeaves = 1;
                this.Leaves.Add(newNode);
            }
            else
            {
                // left branch
                _levelCounter++;
                Node leftNode = new Node();
                leftNode.Parent = newNode;
                leftNode = this.CreateTreeAndSyntax(subpolygons[0]);
               leftNode.Parent = newNode;
                newNode.LeftChild = leftNode;
                if (leftNode.Operator != terminal)
                {
                    newNode.NoOfLeaves += leftNode.NoOfLeaves;
                }
                else
                {
                    newNode.NoOfLeaves += 1;
                }
                this.Syntax += ")(";

                // right branch
                if (subpolygons.Length > 1)
                {
                    _levelCounter++;
                    Node rightNode = new Node();
                    rightNode.Parent = newNode;
                    rightNode = this.CreateTreeAndSyntax(subpolygons[1]);
                    rightNode.Parent = newNode;
                    newNode.RightChild = rightNode;
                    if (rightNode.Operator != terminal)
                    {
                        newNode.NoOfLeaves += rightNode.NoOfLeaves;
                    }
                    else
                    {
                        newNode.NoOfLeaves += 1;
                    }
                    this.Syntax += ")";
                }
                _levelCounter--;
            }
            this.Nodes.Add(newNode);
        }
        else
        {
            newNode.NoOfLeaves = 1;
            this.Leaves.Add(newNode);
        }
        newNode.RootRegion = this.Root.Region;
        newNode.RootBuildable = this.Root.Buildable;
        
        if (newNode.IsLeaf())
            //newNode.DetermineBuildable();
            this.CalculateBuildings(newNode);
        return newNode;
    }

    /// <summary>
    /// Determines the operation in the specified node.
    /// </summary>
    /// <param name="_curNode">(Node) current node.</param>
    /// <returns>(char) operation.</returns>
    private char DetermineOperation(Node _curNode)
    {
        List<LineD> boundaryEdges = new List<LineD>();
        LineD subDLine;
        double min1 = -3.0d/4.0d * Math.PI;
        double max1 = -1.0d/4.0d * Math.PI;
        double min2 = 1.0d/4.0d * Math.PI;
        double max2 = 3.0d/4.0d * Math.PI;
        // it is problematic in this part because if there are no collinear relations that are found then it crashes with CheckPortion function
        for (int i = 0; i < this.Root.Region.Edges.Length; i++)
        {
            for (int j = 0; j < _curNode.Region.Edges.Length; j++)
            {
                LineIntersection intersect = this.Root.Region.Edges[i].Intersect(_curNode.Region.Edges[j]);
                if (intersect.Relation == LineRelation.Collinear)
                {
                    boundaryEdges.Add(_curNode.Region.Edges[j]);
                }
            }
        }

        subDLine = CheckProportion(boundaryEdges);

        if ((subDLine.Angle > min1 && subDLine.Angle < max1) || (subDLine.Angle > min2 && subDLine.Angle < max2))
        {
            return 'H';
        }
        else
        {
            return 'V';
        }
    }

    /// <summary>
    /// Checks the proportion of the region formed by the given boundaries and returns the line for subdivision.
    /// </summary>
    /// <param name="_boundaries">(List LineD) boundaries.</param>
    /// <returns>(LineD) line for subdivision (height or width).</returns>
    private LineD CheckProportion(List<LineD> _boundaries)
    {
        Polygon poly;
        List<PointD> points = new List<PointD>();

        for (int i = 0; i < _boundaries.Count; i++)
        {
            PointD pts = _boundaries[i].Start;
            PointD pte = _boundaries[i].End;
            bool s = false;
            bool e = false;

            if (points.Count >= 1)
            {
                for (int j = 0; j < points.Count; j++)
                {
                    if (pts == points[j]) s = true;
                    if (pte == points[j]) e = true;
                }
                if (!s) points.Add(pts);
                if (!e) points.Add(pte);
            }
            else
            {
                points.Add(pts);
                points.Add(pte);
            }
        }

        poly = new Polygon(points.ToArray());

        LineD height = new LineD(poly.Bounds.TopLeft, poly.Bounds.BottomLeft);
        LineD width = new LineD(poly.Bounds.TopLeft, poly.Bounds.TopRight);

        double proportion = (width.Length < height.Length) ? width.Length / height.Length : height.Length / width.Length;
        
        if (proportion < 0.75d) // proportion parameter?
            return (width.Length < height.Length) ? height : width;
        else
        {
            _boundaries.Sort(delegate(LineD l1, LineD l2) { return Comparer<double>.Default.Compare(l1.Length, l2.Length); });
            return _boundaries[_boundaries.Count - 1];
        }
    }

    int cnt = 0;
    /// <summary>
    /// Creates the sub division partition of a specified Node object.
    /// </summary>
    /// <param name="_region">(Node) current node.</param>
    /// <value>(Polygon[]) subpolygons.</value>
    public Polygon[] CreateSubDivision(Node _curNode)
    {
        int dir = (_curNode.Operator != 'H') ? 0 : 1;
        Polygon[] subPolygons;
        bool isAdded = false;
        _splitRate = 0.5;

        LineD div;
        double divValue = _curNode.CalculateSplitValue(_splitRate);

        if (dir == 1) // H
        {
            //LineD div = curNode.Region.Bounds.Intersect(line, curNode.SubDivision); bei beliebiger Linie einsetzen
            div = new LineD(_curNode.Region.Bounds.TopLeft.X, divValue, _curNode.Region.Bounds.BottomRight.X, divValue);
        }
        else // V
        {
            div = new LineD(divValue, _curNode.Region.Bounds.TopLeft.Y, divValue, _curNode.Region.Bounds.BottomRight.Y);
        }

        div = _curNode.Region.IntersectsWith(div);

        if (div.Length == 0.0d)
        {
            Console.WriteLine(cnt.ToString());
            if (cnt > 10)
            {
                subPolygons = new Polygon[0];
                cnt = 0;
                return subPolygons;
            }
            else
            {
                if (cnt % 2 == 0)
                {
                    _splitRate += rand.NextDouble() / 2;
                }
                else
                {
                    _splitRate -= rand.NextDouble() / 2;
                }
                cnt++;
                try
                {
                    subPolygons = this.CreateSubDivision(_curNode);
                }
                catch
                {
                    subPolygons = new Polygon[0];
                }
                _splitRate = 0.5;
            }
        }
        else
        {
            LineD[] polygonlines;
            //--------
            div = _curNode.Region.IntersectsWith(div, out polygonlines);
            subPolygons = _curNode.Region.CalculateSplitPolygons(polygonlines);
            //--------

            // start edge
            double distS, distE;
            SubdivisionEdge startEdge = Graph.FindNearestEdge(div.Start,out distS);

            PointD nearestNode;
            double distance;

            LineD sLine = new LineD(startEdge.Origin, startEdge.Destination);
            PointD sPt = sLine.Intersect(div.Start);

            nearestNode = this.Graph.GetNearestNode(sPt);

            distance = sPt.DistanceTo(nearestNode);
            LineLocation location = sLine.Locate(nearestNode);

            if (distance < this.LayoutParameters.ConnectionDistance && (location == LineLocation.Start || location == LineLocation.End)) //nearestNode.IsCollinear(sLine.Start, sLine.End)) // node on the specific edge
            {
                    if (location == LineLocation.Start)
                    {
                        sPt = startEdge.Origin;
                    }
                    else if (location == LineLocation.End)
                    {
                        sPt = startEdge.Destination;
                    }
                    else
                    {
                        sPt = nearestNode;
                    }
            }
            else
            {
                SubdivisionEdge halfEdgeS = Graph.SplitEdge(startEdge.Key);
                bool onHalfEdge = false;
                if ((halfEdgeS.ToLine()).Locate(sPt) == LineLocation.Between)
                {
                    onHalfEdge = true;
                }
                else onHalfEdge = false;

                distance = halfEdgeS.Origin.DistanceTo(sPt);
                PointD newSPt = (onHalfEdge) ? halfEdgeS.Origin.Move(halfEdgeS.Destination, distance) : halfEdgeS.Origin.Move(startEdge.Origin, distance);
                Graph.MoveVertex(Graph.FindNearestVertex(halfEdgeS.Origin), sPt);
                sPt = halfEdgeS.Origin;
            }

            // end edge
            SubdivisionEdge endEdge = Graph.FindNearestEdge(div.End, out distE);
            
            LineD eLine = new LineD(endEdge.Origin, endEdge.Destination);
            PointD ePt = eLine.Intersect(div.End);
            
            nearestNode = this.Graph.GetNearestNode(ePt);
            distance = ePt.DistanceTo(nearestNode);
            location = eLine.Locate(nearestNode);

            if (distance < this.LayoutParameters.ConnectionDistance && (location == LineLocation.Start || location == LineLocation.End) ) //nearestNode.IsCollinear(sLine.Start, sLine.End)) // node on the specific edge
            {
                if (location == LineLocation.Start)
                {
                    ePt = endEdge.Origin;
                }
                else if (location == LineLocation.End)
                {
                    ePt = endEdge.Destination;
                }
                else
                {
                    ePt = nearestNode;
                }
            }
            else
            {
                SubdivisionEdge halfEdgeE = Graph.SplitEdge(endEdge.Key);
                bool onHalfEdge = false;
                LineLocation locHalfEdge = halfEdgeE.ToLine().Locate(ePt);
                LineLocation locEndEdge = endEdge.ToLine().Locate(ePt);
                if (locHalfEdge == LineLocation.Between)
                {
                    onHalfEdge = true;
                }
                else onHalfEdge = false;

                if(onHalfEdge) distance = halfEdgeE.Origin.DistanceTo(ePt);
                else distance = endEdge.Origin.DistanceTo(ePt);
                PointD newEPt = (onHalfEdge) ? halfEdgeE.Origin.Move(halfEdgeE.Destination, distance): halfEdgeE.Origin.Move(endEdge.Origin,distance);
                Graph.MoveVertex(Graph.FindNearestVertex(halfEdgeE.Origin), ePt);
                ePt = halfEdgeE.Origin;
            }

            LineD subD = new LineD(sPt, ePt);
            _curNode.SubDivision = subD;

            if (subD.Length != 0.0d)
            {
                int a, b;
                Graph.AddEdge(sPt, ePt, out a, out b);
                if (a != -1)
                {
                    isAdded = true;
                }
                else
                {
                    isAdded = false;
                    Console.WriteLine("Subdivision could not be added.");
                }
            }
            else
            {
                isAdded = false;
                Console.WriteLine("Subdivision length is zero.");
            }

            if (isAdded)
            {
                PointD[] poly1 = this.Graph.FindEdge(subD.Start, subD.End).CyclePolygon;
                PointD[] poly2 = this.Graph.FindEdge(subD.End, subD.Start).CyclePolygon;

                subPolygons = new Polygon[2];
                subPolygons[0] = new Polygon(poly1);
                subPolygons[1] = new Polygon(poly2);
            }
            else
            {
                Console.WriteLine(cnt.ToString());
                if (cnt > 10)
                {
                    subPolygons = new Polygon[0];
                    cnt = 0;
                    return subPolygons;
                }
                cnt++;
                if (cnt % 3 == 0) _splitRate += rand.NextDouble() / 2;
                else if (cnt % 3 == 1) _splitRate -= rand.NextDouble() / 2;
                else _curNode.Operator = (_curNode.Operator == 'V') ? 'H' : 'V';
                try
                {
                    subPolygons = this.CreateSubDivision(_curNode);
                }
                catch
                {
                    subPolygons = new Polygon[0];
                }
                _splitRate = 0.5;
            }
        }

        return subPolygons;
    }

    # endregion

    public void Update(Parameter _param)
    {

    }

    # region Building Calculation

    

    /// <summary>
    /// Calculates the buildings for a specified node and adds them to the list of buildings of the Tree.
    /// </summary>
    /// <param name="_curNode">(Node) current node.</param>
    private void CalculateBuildings(Node _curNode)
    {
        List<LineD> boundaryEdges = new List<LineD>();
        List<LineD> innerEdges = new List<LineD>();

        for (int i = 0; i < _curNode.RootRegion.Edges.Length; i++)
        {
            for (int j = 0; j < _curNode.Region.Edges.Length; j++)
            {
                if (_curNode.RootRegion.Edges[i].Intersect(_curNode.Region.Edges[j]).Relation == LineRelation.Collinear)
                {
                    boundaryEdges.Add(_curNode.Region.Edges[j]);
                }
                else
                {
                    innerEdges.Add(_curNode.Region.Edges[j]);
                }
            }
        }

        //boundaryEdges.Sort(delegate(LineD l1, LineD l2) { return Comparer<double>.Default.Compare(l1.Length, l2.Length); });
        if (boundaryEdges.Count > 0)
        {
            _curNode.CalculateBuildings(boundaryEdges, this.LayoutParameters, this.Root.Buildable);
        }
    }

    # endregion

    # region Adjustments and Settings

    /// <summary>
    /// Sets the syntax of the Tree recursively.
    /// </summary>
    /// <param name="_curNode">(Node) current node.</param>
    private void SetSyntax(Node _curNode)
    {
        if (_curNode.LeftChild != null && _curNode.RightChild != null)
        {
            this.Syntax += _curNode.Operator.ToString() + '(';
            this.SetSyntax(_curNode.LeftChild);
            this.Syntax += ")(";
            this.SetSyntax(_curNode.RightChild);
            this.Syntax += ')';
        }
        else
        {
            this.Syntax += _curNode.Operator.ToString();
        }
    }

    /// <summary>
    /// Converts the Tree object into an array of polygons.
    /// </summary>
    /// <returns>(PointD[][]) tree polygons.</returns>
    public PointD[][] ToPolygons()
    {
        PointD[][] tmpArray = new PointD[this.Polygons.Count][];
        for (int i = 0; i < this.Polygons.Count; i++)
        {
            tmpArray[i] = this.Polygons[i];
        }
        return tmpArray;
    }

    # endregion

    # endregion
}

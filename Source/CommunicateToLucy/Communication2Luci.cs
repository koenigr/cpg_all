﻿//using CommunicateToLucy;
//using CollabSample.ViewModels;
//using collact.go.Communication;
//using LineService.Communication;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
//using System.Threading;

namespace CPlan.CommunicateToLuci
{
    public class SocketConnection
    {
        public StreamReader OutputStream { get; private set; }
        public BinaryWriter InputStream { get; private set; }
        public bool isBusy;

        public SocketConnection(TcpClient tcpClient, bool auth = false)
        {
            if (tcpClient != null)
            {
                NetworkStream networkStream = tcpClient.GetStream();

                OutputStream = new StreamReader(networkStream, System.Text.Encoding.UTF8);
                InputStream = new BinaryWriter(networkStream, System.Text.Encoding.UTF8);

                if (auth)
                {
                    string message = "{'action':'authenticate','username':'lukas','userpasswd':'1234'}";
                    InputStream.Write(Encoding.UTF8.GetBytes(message + "\n"));
                    InputStream.Flush();

                    string json = OutputStream.ReadLine();
                }

                isBusy = false;
            }
        }
    }

    public class ConfigSettings
    {
        public int scenarioID;
        public string mqtt_topic;
        public int objID;
    }

    public class Communication2Lucy
    {
        public List<SocketConnection> socketPool = new List<SocketConnection>();

        private string _host;
        private int _port;

        public Communication2Lucy()
        {
        }


        public bool connect(String host, int port)
        {
            _host = host;
            _port = port;

            TcpClient socketForServer;
            try
            {
                socketForServer = new TcpClient(host, port);
                SocketConnection sc = new SocketConnection(socketForServer);
                socketPool.Add(sc);
            }
            catch
            {
                Console.WriteLine(
                "Failed to connect to server at {0}:{1}", host, port);
                return false;
            }

            Console.WriteLine("******* Trying to communicate with lucy on host " + host + " and port " + port + " *****");

            return true;
        }


        private SocketConnection getFreeSocketConnection()
        {
            SocketConnection result = null;

            var query = socketPool.Where(s => s.isBusy == false);
            if (query != null && query.Any())
            {
                result = query.First();
            }
            else
            {
                TcpClient socketForServer = new TcpClient(_host, _port);
                result = new SocketConnection(socketForServer, true);

                socketPool.Add(result);
            }

            return result;
        }

        private string sendMessage(string message)
        {
            SocketConnection sc = getFreeSocketConnection();

            sc.isBusy = true;
            sc.InputStream.Write(Encoding.UTF8.GetBytes(message + "\n"));
            sc.InputStream.Flush();

            string json = sc.OutputStream.ReadLine();

            if (json == null || json == "")
                json = sc.OutputStream.ReadLine();

            sc.isBusy = false;

            return json;
        }

        public JObject sendAction2Lucy(string action)
        {
            string json = sendMessage(action);

            try
            {
                var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

                return jObj2;
            }
            catch (Exception ex)
            {
                var jObj2 = (JObject)JsonConvert.DeserializeObject("{\"" + json);

                return jObj2;
            }
        }

        public byte[] GetBinaryAttachment(JObject response)
        {
            SocketConnection sc = getFreeSocketConnection();

            sc.isBusy = true;
            {
                var count = response.SelectTokens("..streaminfo").Count();
                //                for (var i = 0; i < count; i++)
                //                {
                var lengthChars = new char[sizeof(long)];
                sc.OutputStream.Read(lengthChars, 0, lengthChars.Length);
                byte[] lengthBytes = Encoding.UTF8.GetBytes(lengthChars);
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(lengthBytes);

                long length = BitConverter.ToInt64(lengthBytes, 0), haveRead = 0;
                 var attachment = new char[length];
               length /= 8;
                

                int readPrev;

                bool b = true;

                /*
                while (haveRead < length &&
                       (readPrev = sc.OutputStream.Read(attachment, (int)haveRead, (int)(length - haveRead))) >
                       0)
                    haveRead += readPrev;
                */
                string rr="";

                int c = 0;
                int cl = 0;
                while (c<11)
                {
                    rr += sc.OutputStream.ReadLine();
                    Console.Out.WriteLine(rr.Length);
                    c++;
                }

                byte[] result = Encoding.UTF8.GetBytes(rr);

//                byte[] result = Encoding.UTF8.GetBytes(attachment);

                sc.isBusy = false;
                return result;
            }
        }


        public JObject sendAction2Lucy(string action, byte[] byteStream1, byte[] byteStream2, byte[] byteStream3)
        {
            SocketConnection sc = getFreeSocketConnection();

            sc.isBusy = true;
            
            sc.InputStream.Write(Encoding.UTF8.GetBytes(action + "\n"));
            sc.InputStream.Flush();

            long len1 = byteStream1.Length;
            byte[] arr1 = BitConverter.GetBytes(len1).Reverse().ToArray(); ;
            sc.InputStream.Write(arr1);
            sc.InputStream.Flush();
            sc.InputStream.Write(byteStream1);
            sc.InputStream.Flush();

            long len2 = byteStream2.Length;
            byte[] arr2 = BitConverter.GetBytes(len2).Reverse().ToArray(); ;
            sc.InputStream.Write(arr2);
            sc.InputStream.Write(byteStream2);
            sc.InputStream.Flush();

            long len3 = byteStream3.Length;
            byte[] arr3 = BitConverter.GetBytes(len3).Reverse().ToArray(); ;
            sc.InputStream.Write(arr3);
            sc.InputStream.Write(byteStream3);
            sc.InputStream.Flush();


            string json = sc.OutputStream.ReadLine();
            var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

            return jObj2;
        }


        public ConfigSettings sendCreateService2Lucy(string command, int scenarioID)
        {
            ConfigSettings result = null;

            string json = sendMessage(command);

            try
            {
                var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

                int serviceID = jObj2.Value<JObject>("result").Value<int>("SObjID");
                string mqtt_topic = jObj2.Value<JObject>("result").Value<String>("mqtt_topic");

                if (mqtt_topic.EndsWith("/"))
                    mqtt_topic = mqtt_topic.Substring(0, mqtt_topic.Length - 1);

                result = new ConfigSettings() { scenarioID = scenarioID, mqtt_topic = mqtt_topic, objID = serviceID };
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
            }

            return result;
        }

        public JObject sendService2Lucy(string command)
        {
            SocketConnection sc = getFreeSocketConnection();

            sc.isBusy = true;
            sc.InputStream.Write(Encoding.UTF8.GetBytes(command + "\n"));
            sc.InputStream.Flush();

            string json = sc.OutputStream.ReadLine();

            if (json == null || json == "")
                json = sc.OutputStream.ReadLine();

            var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

            if (jObj2.Value<JToken>("result") != null)
            {
                if (((Newtonsoft.Json.Linq.JProperty)(jObj2.Value<JToken>("result").First())).Name.Equals("mqtt_topic"))
                {
                    json = sc.OutputStream.ReadLine();
                    jObj2 = (JObject)JsonConvert.DeserializeObject(json);
                }

                sc.isBusy = false;

                if (jObj2.Value<JToken>("result") != null) // && ((Newtonsoft.Json.Linq.JProperty)(jObj2.Value<JToken>("result").First())).Name.Equals("mqtt"))
                    return ((Newtonsoft.Json.Linq.JObject)(jObj2.Value<JToken>("result")));
            }
            return null;
        }

        public JProperty authenticate(string user, string password)
        {
            return (JProperty)sendAction2Lucy("{'action':'authenticate','username':'" + user + "','userpasswd':'" + password + "'}").First;
        }

        public JProperty getActionsList()
        {
            return (JProperty)sendAction2Lucy("{'action':'get_list','of':['actions']}").First;
        }

        public JProperty getActionParameters(string action)
        {
            return (JProperty)sendAction2Lucy("{'action':'get_infos_about','actionname':'" + action + "'}").First;
        }

        public JProperty getServiceParameters(string service)
        {
            return (JProperty)sendAction2Lucy("{'action':'get_infos_about','servicename':'" + service + "'}").First;
        }

        public JProperty getServicesList()
        {
            return (JProperty)sendAction2Lucy("{'action':'get_list','of':['services']}").First;
        }

        private static string GetData()
        {
            //Ack from sql server
            return "ack";
        }
    }
}

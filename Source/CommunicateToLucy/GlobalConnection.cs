﻿using System;

namespace CPlan.CommunicateToLuci
{
    [Serializable]
    public static class GlobalConnection
    {
        public static string IP = "localhost";//"192.168.0.1";//"137.189.153.150";//
        public static int Port = 7654;
        public static Communication2Lucy CL = null;
    }

}

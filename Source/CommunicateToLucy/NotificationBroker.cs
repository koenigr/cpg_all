﻿using System;
using System.Collections.Generic;

namespace CPlan.CommunicateToLuci
{
    /// <summary>
    /// Delegate to call on notification triggers.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="userInfo"></param>
    public delegate void NotificationHandler(object sender, Dictionary<int, Object> userInfo);

    /// <summary>
    /// The NotificationBroker enables delegates to subscribe to notifications. They get invoked when a corresponding notification is posted.
    /// </summary>
    public class NotificationBroker
    {
        private Dictionary<String, List<NotificationHandler>> subscriptionList = new Dictionary<String, List<NotificationHandler>>();

        #region Singleton
        private static NotificationBroker _instance;

        private NotificationBroker()
        {
        }

        public static NotificationBroker Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new NotificationBroker();
                }

                return _instance;
            }
        }
        #endregion

        /// <summary>
        /// Register delegate handler to given notification name
        /// </summary>
        /// <param name="handler"></param>
        /// <param name="notificationName"></param>
        public void Register(NotificationHandler handler, String notificationName)
        {
            Preconditions.ThrowIfNull(handler);
            Preconditions.ThrowIfNullOrEmpty(notificationName);

            List<NotificationHandler> handlerList;

            if (subscriptionList.TryGetValue(notificationName, out handlerList) == false)
            {
                handlerList = new List<NotificationHandler>();
                subscriptionList.Add(notificationName, handlerList);
            }

            handlerList.Add(handler);
        }

        /// <summary>
        /// Unregister delegate handler from given notification name
        /// </summary>
        /// <param name="handler"></param>
        /// <param name="notificationName"></param>
        public void Unregister(NotificationHandler handler, String notificationName)
        {
            Preconditions.ThrowIfNull(handler);
            Preconditions.ThrowIfNullOrEmpty(notificationName);

            List<NotificationHandler> handlerList;

            if (subscriptionList.TryGetValue(notificationName, out handlerList))
            {
                handlerList.Remove(handler);
            }
        }

        /// <summary>
        /// Post a notification of given name with passed info dictionary.
        /// </summary>
        /// <param name="notificationName"></param>
        /// <param name="sender">Can be null.</param>
        /// <param name="userInfo">Can be null.</param>
        public void PostNotification(String notificationName, Object sender = null, Dictionary<int, Object> userInfo = null)
        {
            Preconditions.ThrowIfNullOrEmpty(notificationName);

            List<NotificationHandler> handlerList;

            if (subscriptionList.TryGetValue(notificationName, out handlerList))
            {
                handlerList.ForEach(o => { o.Invoke(sender, userInfo); });
            }
        }
    }
}

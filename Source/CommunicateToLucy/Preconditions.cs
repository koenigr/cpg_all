﻿using System;

namespace CPlan.CommunicateToLuci
{
    /// <summary>
    /// A collections of functions useful for precondition checking.
    /// </summary>
    public static class Preconditions
    {
        /// <summary>
        /// Throws ArgumentNullException if the target is null.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="paramName"></param>
        public static void ThrowIfNull(Object target, String paramName = null)
        {
            if (target == null)
                throw new ArgumentNullException(paramName ?? "unnamed");
        }

        /// <summary>
        /// Throws ArgumentException if the target is null or empty.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="paramName"></param>
        public static void ThrowIfNullOrEmpty(String target, String paramName = null)
        {
            if (String.IsNullOrEmpty(target))
                throw new ArgumentException(paramName ?? "unnamed");
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace CPlan.CommunicateToLucy
{
    public class ResultManager
    {
        private Dictionary<int, object> ResultList = new Dictionary<int, object> ();
        //public List<bool> ResultList = new List<bool>();
        private string NotificationName;
        private int NotificationCounter = 0;

        public int Count { get { return ResultList.Count; } }

        #region Singleton
        private static ResultManager _instance;

        private ResultManager()
        {
        }

        public static ResultManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ResultManager();
                }

                return _instance;
            }
        }
        #endregion

        public void AddResult(int index, object result)
        {
            // if you want to delete existing results ..
            if (ResultList.ContainsKey(index) == true)
                ResultList.Remove(index);

            ResultList.Add(index, result);

            if (NotificationCounter > 0)
            {
                if (NotificationCounter == GetNumResults())
                    NotificationBroker.Instance.PostNotification(NotificationName, this, ResultList);
            }
        }


        public void ResetResultList()
        {
            ResultList.Clear();
        }

        public object GetResult(int index)
        {
            object result = null;
            ResultList.TryGetValue(index, out result);
            return result;
        }

        public int GetNumResults()
        {
            return ResultList.Count();
        }

        public void RegisterNotification(int numResults, string notificationName) 
        {
            NotificationName = notificationName;
            NotificationCounter= numResults;
        }
    }
}

﻿
using System.Collections.Generic;
//using System.ComponentModel;
using System.Linq;
using uPLibrary.Networking.M2Mqtt;

namespace CPlan.CommunicateToLuci
{


    public static class ServiceManager
    {
        private static Dictionary<int, ConfigSettings> finishedRuns = new Dictionary<int, ConfigSettings>();
        private static Dictionary<int, ConfigSettings> finishedRuns2 = new Dictionary<int, ConfigSettings>();
        private static Dictionary<string, ConfigSettings> unfinishedRuns = new Dictionary<string, ConfigSettings>();
        private static Dictionary<string, ConfigSettings> unfinishedRuns2 = new Dictionary<string, ConfigSettings>();
        public static MqttClient mqttClient;
        private static List<string> mqttTopics = new List<string>();
        private static List<string> mqttTopics2 = new List<string>();

        /*
        #region Singleton
        private static ServiceManager _instance;

        private  ServiceManager()
        {
        }

        public static ServiceManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ServiceManager();
                }

                return _instance;
            }
        }
        #endregion
        */

        public static void Init(string ip)
        {
            try
            {
               // mqttClient = new MqttClient(ip);
               // mqttClient.Connect("king27");

                
                System.Guid newguid = System.Guid.NewGuid();
                string identifier = newguid.ToString("N");

                mqttClient = new MqttClient(ip);
                //mqttClient.Connect("king");
                mqttClient.Connect("CPlan_" + identifier);
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                System.Console.WriteLine(ex.StackTrace);
            }
        }

        public static void AddMqttTopic(string topic)
        {
            mqttTopics.Add(topic);
            mqttClient.Subscribe(new string[] { topic }, new byte[] { 1 });
        }

        public static void AddMqttTopic2(string topic)
        {
            mqttTopics2.Add(topic);
            mqttClient.Subscribe(new string[] { topic }, new byte[] { 1 });
        }

        public static void RemoveMqttTopic(string topic)
        {
            mqttTopics.Remove(topic);
        }

        public static void RemoveMqttTopic2(string topic)
        {
            mqttTopics2.Remove(topic);
        }

        public static void ResetTopicList()
        {
            mqttTopics.Clear();
            mqttTopics2.Clear();
        }

        public static bool CheckListEmpty()
        {
            return (!mqttTopics.Any() && !mqttTopics2.Any());
        }

        public static void AddConfig(string topic, ConfigSettings settings)
        {
            unfinishedRuns.Add(topic, settings);
        }

        public static void AddConfig2(string topic, ConfigSettings settings)
        {
            unfinishedRuns2.Add(topic, settings);
        }


        public static void ReplaceID(int id, int idNew)
        {
            ConfigSettings result = GetConfig4ID(id);
            if (result != null)
            {
                if (finishedRuns.ContainsKey(idNew))
                    finishedRuns.Remove(idNew);

                finishedRuns.Add(idNew, result);
            }
        }

        public static void ReplaceID2(int id, int idNew)
        {
            ConfigSettings result = GetConfig4ID2(id);
            if (result != null)
            {
                if (finishedRuns2.ContainsKey(idNew))
                    finishedRuns2.Remove(idNew);

                finishedRuns2.Add(idNew, result);
            }
        }

        public static void AddID(int id, string topic)
        {
            ConfigSettings config = null;
            if (unfinishedRuns.ContainsKey(topic))
                unfinishedRuns.TryGetValue(topic, out config);

            if (config != null)
            {
                if (finishedRuns.ContainsKey(id))
                    finishedRuns.Remove(id);

                finishedRuns.Add(id, config);
                unfinishedRuns.Remove(topic);
            }
        }

        public static void AddID2(int id, string topic)
        {
            ConfigSettings config = null;
            if (unfinishedRuns2.ContainsKey(topic))
                unfinishedRuns2.TryGetValue(topic, out config);

            if (config != null)
            {
                if (finishedRuns2.ContainsKey(id))
                    finishedRuns2.Remove(id);

                finishedRuns2.Add(id, config);
                unfinishedRuns2.Remove(topic);
            }
        }

        public static ConfigSettings GetConfig4ID(int id)
        {
            ConfigSettings result = null;

            if (finishedRuns.ContainsKey(id))
                finishedRuns.TryGetValue(id, out result);

            return result;
        }

        public static ConfigSettings GetConfig4ID2(int id)
        {
            ConfigSettings result = null;

            if (finishedRuns2.ContainsKey(id))
                finishedRuns2.TryGetValue(id, out result);

            return result;
        }
    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Camera.cs 
//  Copyright (C) 2014/10/18  1:01 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Zeng Wei, Christian Tonn, Sven Schneider 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using CPlan.Geometry;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace CPlan.CustomGLControl
{
    public class Camera
    {
        //############################################################################################################################################################################
        # region Private Fields

        const double Eps6 = 0.000001;      
        private Matrix4 _modelviewMatrix, _projectionMatrix;

        private CGeom3d.Vec_3d _pos;

        # endregion

        //############################################################################################################################################################################
        # region Properties
        
        public const double Fovy = Math.PI / 4;


        public CGeom3d.Vec_3d Position { get { return _pos; } private set { _pos = value;} }
        public float Alpha, Beta, Gamma;
        public float Znear, Zfar, Zoom;
        public bool Ortho { private get; set; } 
        public Color4 BackgroundColor { get; set; }
        public Color4 RasterColor { get; set; } 
        public bool DrawRaster { get ; set; } 

        //===================================================================================================================================================================
        private float AngleV { get { return Beta; } }

        //===================================================================================================================================================================
        private float AngleH { get { return Alpha; } }

        //===================================================================================================================================================================
        public Vector3d GetCameraPos { get { return _pos.ToVector3d(); } }

        //===================================================================================================================================================================
        //public Vector3d BlickVectorPtn { get { return new Vector3d(GetBlickVector()); } }

        # endregion

        //############################################################################################################################################################################
        # region Constructors
        
        //===================================================================================================================================================================
        /// <summary>
        /// Initialise a new Camera instance.
        /// </summary>
        public Camera()
        {
            Position = new CGeom3d.Vec_3d(0, 0, 1);
            Alpha = 0;
            Beta = 0;
            Gamma = 0;
            Ortho = true;
            Znear = 10;
            Zfar = -10;
            Zoom = 60;
            DrawRaster = true;
            //DrawKoordinatenachsen = true;
            BackgroundColor = Color4.DarkGray; // Color4.Black;
        }

        # endregion

        //############################################################################################################################################################################
        # region Methods

        //===================================================================================================================================================================
        private void SetPMatrix()
        {
            // Viewport auslesen
            Int32[] oldViewport = new Int32[4];
            GL.GetInteger(GetPName.Viewport, oldViewport);

            double dAspectRatio = Convert.ToDouble(oldViewport[2]) / Convert.ToDouble(oldViewport[3]);
            double dAspectRatioX = dAspectRatio;
            double dAspectRatioY = 1;

            if (!Ortho)
            {
                Matrix4d perspective = Matrix4d.CreatePerspectiveFieldOfView(Fovy, dAspectRatio, Znear, Zfar);
                GL.MatrixMode(MatrixMode.Projection);
                GL.LoadMatrix(ref perspective);
                GL.GetFloat(GetPName.ProjectionMatrix, out _projectionMatrix);
            }
            else
            {
                if (dAspectRatioX < 1.0f)
                {
                    dAspectRatioX = 1;
                    dAspectRatioY = Convert.ToDouble(oldViewport[3]) / Convert.ToDouble(oldViewport[2]);
                }

                float ratio = 1.0f;
                double scale = Position.z;
                GL.Ortho(dAspectRatioX * -ratio * scale, dAspectRatioX * ratio * scale,
                                  dAspectRatioY * -ratio * scale, dAspectRatioY * ratio * scale, Znear, Zfar);
                GL.GetFloat(GetPName.ProjectionMatrix, out _projectionMatrix);
                //、Console.WriteLine("projection view " + projectionMatrix.ToString());
            }
        }

        //===================================================================================================================================================================
        private void SetMMatrix()
        {
            if (!Ortho)
            {
                Matrix4 lookat = Matrix4.LookAt(0, 0, (float)Position.z, 0, -0.0f, 0, 0, 1, 0);
                GL.LoadMatrix(ref lookat);

                GL.Rotate(Alpha * 180 / Math.PI, 1, 0, 0);
                GL.Rotate(Beta * 180 / Math.PI, 0, 1, 0);
            }

            // allow to rotate along z-axis in both 2D and 3D
            GL.Rotate(Gamma * 180 / Math.PI, 0, 0, 1);

            GL.Translate(Position.x, Position.y, 0);

            GL.GetFloat(GetPName.ModelviewMatrix, out _modelviewMatrix);
            // Console.WriteLine("model view " + modelviewMatrix.ToString());
        }

        //===================================================================================================================================================================
        private void DrawCoordinateAxisSymbol()
        {
            if (!DrawRaster)
                return;

            float dx = 0, dy = 0, dz = 0;

            GL.LineWidth(3);
            GL.Begin(PrimitiveType.Lines);
            // red x axis
            GL.Color3(1.0f, 0.0f, 0.0f);
            GL.Vertex3(0.0f + dx, 0.0f + dy, 0.0f - dz);
            GL.Vertex3(100f + dx, 0.0f + dy, 0.0f - dz);
            // green z axis
            GL.Color3(0.0f, 1.0f, 0.0f);
            GL.Vertex3(0.0f + dx, 0.0f + dy, 0.0f - dz);
            GL.Vertex3(0.0f + dx, 0.0f + dy, 100f - dz);
            // blue y axis
            GL.Color3(0.0f, 0.0f, 1.0f);
            GL.Vertex3(0.0f + dx, 0.0f + dy, 0.0f - dz);
            GL.Vertex3(0.0f + dx, 100f + dy, 0.0f - dz);
            GL.End();
        }

        //===================================================================================================================================================================
        /// <summary>
        /// Draw coordinate raster.
        /// </summary>
        /// <param name="rasterX"></param>
        /// <param name="rasterY"></param>
        /// <param name="rasterFak"></param>
        /// <param name="rasterSize"></param>
        private void DrawBackgroundGridLines(float rasterX, float rasterY, float rasterFak, float rasterSize)
        {
            if (!DrawRaster)
                return;

            float dx = 0, dy = 0, dz = 0;

            GL.LineWidth(1);
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
            GL.Begin(PrimitiveType.Lines);
            for (float s1 = -rasterSize; s1 <= rasterSize; s1 += rasterFak)
            {
                int x = (int)Math.Floor((s1 + rasterX) / rasterFak + 0.5f);
                int y = (int)Math.Floor((s1 - rasterY) / rasterFak + 0.5f);

                if ((x%10) == 0)
                    GL.Color4(RasterColor);
                else
                {
                    Color4 tmpCol = RasterColor;
                    tmpCol.A = 0.3f;
                    GL.Color4(tmpCol);
                }
                if (Math.Abs(s1 + rasterX) < 0.000001f) // (s1+raster_x == 0)
                {
                    GL.Vertex3(s1 + rasterX + dx, -rasterSize - rasterY - dy, 0.0f + dz);
                    GL.Vertex3(s1 + rasterX + dx, -1.0f - dy, 0.0f + dz);
                    GL.Vertex3(s1 + rasterX + dx, 0.0f - dy, 0.0f + dz);
                    GL.Vertex3(s1 + rasterX + dx, rasterSize - rasterY - dy, 0.0f + dz);
                }
                else
                {
                    GL.Vertex3(s1 + rasterX + dx, -rasterSize - rasterY - dy, 0.0f + dz);
                    GL.Vertex3(s1 + rasterX + dx, rasterSize - rasterY - dy, 0.0f + dz);
                }

                if ((y%10) == 0)
                    GL.Color4(RasterColor);
                else
                {
                    Color4 tmpCol = RasterColor;
                    tmpCol.A = 0.3f;
                    GL.Color4(tmpCol);
                }
                if (Math.Abs(s1 - rasterY) < 0.000001f) // (s1-raster_y == 0)
                {
                    GL.Vertex3(-rasterSize + rasterX + dx, s1 - rasterY - dy, 0.0f + dz);
                    GL.Vertex3(0.0f + dx, s1 - rasterY - dy, 0.0f + dz);
                    GL.Vertex3(1.0f + dx, s1 - rasterY - dy, 0.0f + dz);
                    GL.Vertex3(rasterSize + rasterX + dx, s1 - rasterY - dy, 0.0f + dz);
                }
                else
                {
                    GL.Vertex3(-rasterSize + rasterX + dx, s1 - rasterY - dy, 0.0f + dz);
                    GL.Vertex3(rasterSize + rasterX + dx, s1 - rasterY - dy, 0.0f + dz);
                }
            }
            GL.End();
        }

        //===================================================================================================================================================================
        /// <summary>
        /// Draw Background.
        /// </summary>
        /// <param name="raster_x"></param>
        /// <param name="raster_y"></param>
        /// <param name="raster_size"></param>
        private void DrawCoordinatesBackgroundFields(float raster_x, float raster_y, float raster_size)
        {
            float dx = 0, dy = 0, dz = 0;
            GL.LineWidth(1);          
            GL.Begin(PrimitiveType.Quads);          
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
            GL.Color4(BackgroundColor);
            GL.Vertex3(-raster_size + raster_x + dx, raster_size - raster_y - dy, 0.0f + dz);
            GL.Vertex3(-raster_size + raster_x + dx, -raster_size - raster_y - dy, 0.0f + dz);
            GL.Vertex3(raster_size + raster_x + dx, -raster_size - raster_y - dy, 0.0f + dz);
            GL.Vertex3(raster_size + raster_x + dx, raster_size - raster_y - dy, 0.0f + dz);
            GL.End();
        }

        //===================================================================================================================================================================
        public void DrawKoordinatenkreuz()
        {
            if (!DrawRaster)
                return;
            
            Int32[] oldViewport = new Int32[4];  // Viewport auslesen
            GL.GetInteger(GetPName.Viewport, oldViewport);

            float rasterSize = 100, rasterFak = 1;
            float rasterX = 0, rasterY = 0;

            //fak sollte auf 10er Potenzen gerundet werden
            rasterFak = (float)Math.Floor(Math.Log10(Math.Abs(Position.z * 0.2) * 0.5));
            rasterFak = (float)Math.Pow(10.0, rasterFak);
            if (rasterFak < 1.0f)
                rasterFak = 1;

            rasterSize = rasterFak * 500;

            rasterX = (int)(Position.x / rasterFak) * rasterFak;
            rasterY = (int)(Position.y / rasterFak) * rasterFak;

            GL.Enable(EnableCap.Blend);
            GL.DepthMask(false);
            GL.DepthRange(-500, 500);
            GL.DepthFunc(DepthFunction.Always);

            //Perspektive
            //DrawCoordinatesBackgroundFields(raster_x, raster_y, raster_size);
            DrawBackgroundGridLines(rasterX, rasterY, rasterFak, rasterSize);

            DrawCoordinateAxisSymbol();

            GL.DepthFunc(DepthFunction.Less);
            GL.DepthMask(true);
        }

        //===================================================================================================================================================================
        public void SetOpenGLMatrixes()
        {
            GL.MatrixMode(MatrixMode.Projection);                                       // Projektionsmatrix einrichten
            GL.PushMatrix();
            GL.LoadIdentity();
            SetPMatrix();

            GL.MatrixMode(MatrixMode.Modelview);                                        // Modellmatrix vom Zeitpunkt des Renderns laden
            GL.PushMatrix();
            GL.LoadIdentity();
            SetMMatrix();
        }

        //===================================================================================================================================================================
        private Vector3d GetBlickVector()
        {
            Vector3d zw = new Vector3d();

            zw.X = 0; zw.Y = 0; zw.Z = -1;
            Vector3d blick = zw;

            float angle = AngleV;
            blick.Y = (zw.Y * Math.Cos(angle) - zw.Z * Math.Sin(angle));
            blick.Z = (zw.Z * Math.Cos(angle) + zw.Y * Math.Sin(angle));

            zw = blick;

            angle = AngleH;
            blick.X = (zw.X * Math.Cos(angle) - zw.Y * Math.Sin(angle));
            blick.Y = (zw.Y * Math.Cos(angle) + zw.X * Math.Sin(angle));

            return blick;
        }

        //===================================================================================================================================================================
        private void GetGLCoordinates(int x, int y, int width, int height, CGeom3d.Vec_3d blickVec, CGeom3d.Vec_3d vecx, CGeom3d.Vec_3d vecy)
        {
            if (blickVec == null) throw new ArgumentNullException("blickVec");
            if (vecx == null) throw new ArgumentNullException("vecx");
            if (vecy == null) throw new ArgumentNullException("vecy");
            if (Ortho) return;

            //Bug
            y = height - y;

            CGeom3d.Vec_3d lot = new CGeom3d.Vec_3d(), zw = new CGeom3d.Vec_3d();

            CGeom3d.Vec_3d blick = GetBlickVector().ToGeom3d();
            double zoom = Zoom / 180.0 * Math.PI;

            if ((Math.Abs(blick.x) > Eps6) || (Math.Abs(blick.y) > Eps6))
            {
                lot.x = 0.0;
                lot.y = 0.0;
                lot.z = 1.0;
            }
            else
            {
                double angle = AngleV;
                if (angle < 0)
                {
                    zw.x = 0.0;
                    zw.y = 1.0;
                    zw.z = 0.0;
                }
                else
                {
                    zw.x = 0.0;
                    zw.y = -1.0;
                    zw.z = 0.0;
                }
                lot = zw;
                angle = AngleH;
                lot.x = (zw.x * Math.Cos(angle) - zw.y * Math.Sin(angle));
                lot.y = (zw.y * Math.Cos(angle) + zw.x * Math.Sin(angle));
            }
            vecx = lot.amul(blick);
            vecy = vecx.amul(blick);
            if (vecx.Value() < Eps6) return;
            if (vecy.Value() < Eps6) return;
            vecx /= vecx.Value();
            vecy /= vecy.Value();

            double rx = x;
            double ry = y;

            //relativ zum Mittelpunkt des Views(nr)
            rx = (rx - ((double)width / 2.0f));
            ry = (ry - ((double)height / 2.0f));

            double brennweite = (height / 2.0) / Math.Tan(zoom / 2.0);
            double brennweiteY = Math.Sqrt(brennweite * brennweite + rx * rx);

            rx = Math.Atan(rx / brennweite);
            ry = Math.Atan(ry / brennweiteY);

            //Abweichungswinkel von der Blickrichtung
            blickVec = blick;
            CGeom3d.Rot_Achse_3d(blickVec, vecx, Math.Sin(-ry), Math.Cos(-ry));
            CGeom3d.Rot_Achse_3d(blickVec, vecy, Math.Sin(rx), Math.Cos(rx));
        }

        //===================================================================================================================================================================
        public Vector3d GetClickRay(int x, int y, int width, int height)
        {
            Vector3d start = new Vector3d();
            Vector3d blick = new Vector3d();
            Vector3d vecx = new Vector3d();
            Vector3d vecy = new Vector3d();

            GetGLCoordinates(x, y, width, height, blick.ToGeom3d(), vecx.ToGeom3d(), vecy.ToGeom3d());

            return blick;
        }

        //===================================================================================================================================================================
        public Vector3d GetClickRayPtn(int x, int y, int width, int height)
        {
            return new Vector3d(GetClickRay(x, y, width, height));
        }

        //===================================================================================================================================================================
        public Vector3d GetPointXYPtn(Vector3d screen)
        {
            return GetPointXYPtn(screen.X, screen.Y);
        }

        //===================================================================================================================================================================
        //ZW: get the click point in the camera world
        //reference: http://www.opentk.com/node/1892
        public Vector3d GetPointXYPtn(double x, double y)
        {
            int[] viewport = new int[4];

            // Get Matrix
            GL.GetInteger(GetPName.Viewport, viewport);

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadMatrix(ref _projectionMatrix);

            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadMatrix(ref _modelviewMatrix);

            if (IsZero(_projectionMatrix) || IsZero(_modelviewMatrix))
                return new Vector3d();

            Vector3d pNear = new Vector3d(x, viewport[3] - y, 0);
            Vector3d pFar = new Vector3d(x, viewport[3] - y, 1);

            Vector3d worldPNear = UnProject(pNear, _modelviewMatrix, _projectionMatrix, viewport);
            Vector3d worldPFar = UnProject(pFar, _modelviewMatrix, _projectionMatrix, viewport);

            double ratio = (worldPNear.Z - 0) / (worldPNear.Z - worldPFar.Z);
            double worldX = worldPNear.X + (worldPFar.X - worldPNear.X) * ratio;
            double worldY = worldPNear.Y + (worldPFar.Y - worldPNear.Y) * ratio;

            return new Vector3d(worldX, worldY, 0);
        }

        //===================================================================================================================================================================
        private Vector3d UnProject(Vector3d screen, Matrix4 view, Matrix4 projection, int[] viewPort)
        {
            Vector4 pos = new Vector4
            {
                X = Convert.ToSingle(1.0f*(screen.X - viewPort[0])/viewPort[2]*2.0f - 1.0f),
                Y = Convert.ToSingle(1.0f*(screen.Y - viewPort[1])/viewPort[3]*2.0f - 1.0f),
                Z = Convert.ToSingle(screen.Z*2.0f - 1.0f),
                W = 1.0f
            };

            // Map x and y from window coordinates, map to range -1 to 1 

            Matrix4 mul = Matrix4.Mult(view, projection);
            
            Matrix4 inv = Matrix4.Invert(mul);
            Vector4 pos2 = Vector4.Transform(pos, inv);
            Vector3d posOut = new Vector3d(pos2.X, pos2.Y, pos2.Z);

            return posOut / pos2.W;
        }

        //===================================================================================================================================================================
        private bool IsZero(Matrix4 m)
        {
          return (m.M11 == 0)  && (m.M12 == 0) && (m.M13 == 0) && (m.M14 == 0)
              && (m.M21 == 0) && (m.M22 == 0) && (m.M23 == 0) && (m.M24 == 0)
              && (m.M31 == 0) && (m.M32 == 0) && (m.M33 == 0) && (m.M34 == 0)
              && (m.M41 == 0) && (m.M42 == 0) && (m.M43 == 0) && (m.M44 == 0);
        }

        //===================================================================================================================================================================
        public void SetPosition(double x, double y, double z)
        {
            if (Ortho && z <= 0)
                z = 10;

            Position = new CGeom3d.Vec_3d(x, y, z);
        }

        # endregion
    }
}

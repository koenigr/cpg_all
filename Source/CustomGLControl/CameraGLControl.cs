﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using CPlan.Geometry;
using CPlan.VisualObjects;
using System.Windows.Forms;

namespace CPlan.CustomGLControl
{
    public class CameraGLControl :  GLControl         
    {
        //############################################################################################################################################################################
        # region Private Fields

        //private CameraGLControl glControl;
        //private Single scaleView = 1.0f;
        //private Single rotateView = 0.0f;
        private Vector3d posView = new Vector3d(0, 0, 0);
        private Vector3d transformCentre = new Vector3d(0, 0, 0);
        private Vector3D lastMouseLocation;
        //private bool mouseDownR = false;
        //private bool mouseDownL = false;
        //private int w;
        //private int h;
        private bool pan_rotate;
        private bool move;
        DateTime start;

        # endregion

        //############################################################################################################################################################################
        # region Properties

        public Camera camera_2D;
        public Camera camera_3D;

        public Camera Camera;

        /// <summary> If GLControl control is loaded. </summary>
        public bool Loaded { get; protected set; }
        public Color BackgroundColor { get; protected set; }
        public bool Background { get; set; }
        public String MouseMode { get; set; }
        public ObjectsAllList AllObjectsList = new ObjectsAllList();

        static int count = 0;

        # endregion


        //############################################################################################################################################################################
        # region Constructors
        //===================================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of CameraGLControl.
        /// </summary>
        /// /// <param name="curControl">A OpenTK GLControl.</param>
        /// 
        public CameraGLControl()
            : base(new GraphicsMode(32, 24, 8, 4))
        {
            InitializeComponent();

            MouseMode = "ScreenMove";

            this.MouseWheel += new System.Windows.Forms.MouseEventHandler(glControl_MouseWheel);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(glControl_MouseMove);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(glControl_MouseDown);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(glControl_MouseUp);
            this.Load += new System.EventHandler(glControl_Load);
            this.Resize += new System.EventHandler(glControl_Resize);
            //this.Paint += new System.Windows.Forms.PaintEventHandler(glControl_Paint);

            camera_2D = new Camera(this);
            camera_2D.m_beta = -(float)Math.PI / 2;
            camera_2D.m_znear = -9999.9f;
            camera_2D.m_zfar = 9999.9f;
            camera_2D.m_ortho = true;
            camera_2D.SetPosition(0, 0, 50);

            camera_3D = new Camera(this);
            camera_3D.m_alpha = -(float)Math.PI / 4;
            camera_3D.m_beta = -(float)Math.PI / 12;
            camera_3D.m_zoom = 60.0f;
            camera_3D.m_znear = 0.1f;
            camera_3D.m_zfar = 9999.9f;
            camera_3D.m_ortho = false;
            camera_3D.SetPosition(0.0f, 0.0f, 50f);

            Camera = camera_2D;

            ZoomAll();
        }

        # endregion

        //############################################################################################################################################################################
        # region Methods
        public void CameraChange()
        {
            if (Camera == camera_2D) Camera = camera_3D;
            else if (Camera == camera_3D) Camera = camera_2D;
            Invalidate();
        }

        //===================================================================================================================================================================
        private void glControl_Load(object sender, EventArgs e)
        {
            Loaded = true;
            BackgroundColor = Color.White;
            SetupViewport();
        }

        //===================================================================================================================================================================
        /// <summary>
        /// Initialize the GLControl viewport settings
        /// </summary>
        public void SetupViewport()
        {
            this.MakeCurrent();
            int w = this.Width;
            int h = this.Height;
            Invalidate();
            // the following line couses visual studio to crash when used for a custom control!
            //GL.Viewport(0, 0, w, h); // Use all of the glControl painting area
        }

        //===================================================================================================================================================================
        //private void glControl_Paint(object sender, PaintEventArgs e)
        private void CameraGLControl_Paint(object sender, PaintEventArgs e)
        {
            if (!Loaded) return;
            this.MakeCurrent();

            InitOpenGL();
            GL.ClearColor(BackgroundColor);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            //Console.WriteLine("Camera: X "+Camera.m_position.x+" Y "+Camera.m_position.y+" Z "+Camera.m_position.z);
            Camera.SetOpenGLMatrixes();
            Camera.DrawKoordinatenkreuz();

            AllObjectsList.Draw();

            //glControl.SwapBuffers();


            // XXX: framerate drops when users interact with the scene
            //TimeSpan timeDiff = DateTime.Now - start;
            //Console.WriteLine("frame rate " + 1000 / timeDiff.TotalMilliseconds);
            //start = DateTime.Now;

            //GL.MatrixMode(MatrixMode.Projection);
            //GL.PopMatrix();
            //GL.MatrixMode(MatrixMode.Modelview);
            //GL.PopMatrix();
        }

        //===================================================================================================================================================================
        private void InitOpenGL()
        {
            GL.Enable(EnableCap.DepthTest);

            // set background color 
            GL.ClearColor(BackgroundColor);

            // set clear Z-Buffer value
            GL.ClearDepth(1.0f);

            GL.Enable(EnableCap.Blend);
            GL.Enable(EnableCap.PointSmooth);
            GL.Enable(EnableCap.LineSmooth);
            GL.Disable(EnableCap.PolygonSmooth);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.ShadeModel(ShadingModel.Smooth);
            GL.DepthFunc(DepthFunction.Lequal);
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
        }

        //===================================================================================================================================================================
        private void glControl_Resize(object sender, EventArgs e)
        {
            if (!Loaded) return;
            SetupViewport();
            this.Invalidate();
        }

        //===================================================================================================================================================================
        public void DeactivateInteraction()
        {
            this.MouseMove -= glControl_MouseMove;
            this.MouseDown -= glControl_MouseDown;
            this.MouseUp -= glControl_MouseUp;
        }

        public void ActivateInteraction()
        {
            this.MouseMove += glControl_MouseMove;
            this.MouseDown += glControl_MouseDown;
            this.MouseUp += glControl_MouseUp;
        }
        //===================================================================================================================================================================
        private void glControl_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!Loaded) return;

            // ZW: if select move screen button
            if (MouseMode == "ScreenMove")
            {
                //lastMouseLocation = Camera.GetPointXYPtn(Cursor.Position.X, Cursor.Position.Y, 0);

                lastMouseLocation = new Vector3D(Cursor.Position.X, Cursor.Position.Y, 0);
                //
                // -- view control --
                //

                //ZW: left mouse for move
                if (e.Button == System.Windows.Forms.MouseButtons.Right)
                {
                    pan_rotate = false;
                    move = true;
                }

                //ZW: right mouse for pan & rotate
                if (e.Button == System.Windows.Forms.MouseButtons.Right && Camera == camera_3D)
                {
                    pan_rotate = true;
                    move = false;
                }
            }
            //ZW: building interactions
            else
            {
                // -- object interaction --
                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    //mouseDownL = true;
                    // CGeom3d.Vec_3d point_xy = Camera.GetPointXYPtn(e.X, e.Y, glControl.Width, glControl.Height);
                    Vector3D point_xy = Camera.GetPointXYPtn(e.X, e.Y, 0);

                    // this.AllObjectsList.ObjectsToDraw.Add(new GPoint(point_xy.x, point_xy.y));

                    if (point_xy == null) return;
                    Vector2D p = new Vector2D(point_xy.x, point_xy.y);
                    AllObjectsList.ObjectsToInteract.MouseDown(e, p);//, ref glControl);
                }

                pan_rotate = false;
                move = false;
            }
            this.Invalidate();

        }

        //===================================================================================================================================================================
        private void glControl_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!Loaded) return;
            //
            // -- view control --
            //

            if (lastMouseLocation == null)
                return;

            double delta_x = Cursor.Position.X - lastMouseLocation.X;
            double delta_y = Cursor.Position.Y - lastMouseLocation.Y;

            //right mouse move
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                Vector3D preWorldPos = Camera.GetPointXYPtn(lastMouseLocation);
                Vector3D curWorldPos = Camera.GetPointXYPtn(Cursor.Position.X, Cursor.Position.Y, 0);
                double delta_x_world = curWorldPos.X - preWorldPos.X;
                double delta_y_world = curWorldPos.Y - preWorldPos.Y;

                //Control key down,  zoom
                if (Control.ModifierKeys == Keys.Control)
                {
                    float scale_ratio = 0.01f;
                    if (Camera.m_position.z <= 10)
                        scale_ratio = 0.1f;

                    Camera.m_position.z *= Convert.ToSingle(1.0 - delta_y_world * scale_ratio);
                    this.Invalidate();
                }
                else if (Control.ModifierKeys == Keys.Shift && Camera == camera_3D)
                {
                    // --- 3D Camera Rotate ---
                    Camera.m_alpha += Convert.ToSingle(delta_y * 0.01f);
                    Camera.m_beta += Convert.ToSingle(delta_x * 0.01f);

                    while (Camera.m_alpha < 0) Camera.m_alpha += Convert.ToSingle(2 * Math.PI);
                    while (Camera.m_alpha > 2 * Math.PI) Camera.m_alpha -= Convert.ToSingle(2 * Math.PI);

                    while (Camera.m_beta < 0) Camera.m_beta += Convert.ToSingle(2 * Math.PI);
                    while (Camera.m_beta > 2 * Math.PI) Camera.m_beta -= Convert.ToSingle(2 * Math.PI);

                    //if (Camera.m_beta < -Math.PI / 2) Camera.m_beta = Convert.ToSingle(-Math.PI / 2);
                    //if (Camera.m_beta > Math.PI / 2) Camera.m_beta = Convert.ToSingle(Math.PI / 2);
                }
                else if (Control.ModifierKeys == Keys.Alt || (Control.ModifierKeys == Keys.Shift && Camera == camera_2D))
                {
                    // change the origin point here
                    Vector3D originPoint = new Vector3D(0, 0, 0);

                    Vector3D preDir = new Vector3D(preWorldPos.X - originPoint.X, preWorldPos.Y - originPoint.Y, 0);
                    Vector3D curDir = new Vector3D(curWorldPos.X - originPoint.X, curWorldPos.Y - originPoint.Y, 0);

                    double angle1 = Math.Atan2(curDir.Y, curDir.X);
                    double angle2 = Math.Atan2(preDir.Y, preDir.X);
                    double rotateAngle = angle2 - angle1;

                    Camera.m_gamma -= (float)rotateAngle;
                }
                else
                {
                    CGeom3d.Vec_3d newPos = Camera.GetCameraPos();
                    newPos.x += delta_x_world;
                    newPos.y += delta_y_world;
                    Camera.SetPosition(newPos.x, newPos.y, newPos.z);
                }

                lastMouseLocation.X = Cursor.Position.X;
                lastMouseLocation.Y = Cursor.Position.Y;
                lastMouseLocation.Z = 0;
                this.Invalidate();
                return;
            }

            //TODO: left button for object manipulation, not test yet
            else if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Vector3D point_xy = Camera.GetPointXYPtn(e.X, e.Y, 0);
                if (point_xy == null) return;

                Cursor tmpCursor = Cursors.Default;
                Vector2D p = new Vector2D(point_xy.x, point_xy.y);
                //AllObjectsList.ObjectsToInteract.MouseMove(e, p, ref glControl, ref tmpCursor);
                // Hier könnte man noch optimieren, indem man immer nur den Bereich neuzeichnet, in dem das Objekt bewegt wurde.
            }
           
        }

        //===================================================================================================================================================================
        private void glControl_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!Loaded) return;
            ////
            //// -- view control --
            ////
            if (pan_rotate || move)
            {
                pan_rotate = false;
                move = false;
                return;
            }

            //mouseDownR = false;
            //transformCentre.X = 0;
            //transformCentre.Y = 0;
            ////
            //// -- object interaction --
            ////
            //mouseDownL = false;
            AllObjectsList.ObjectsToInteract.MouseUp(e);// put the selected element to the fornt
            if (AllObjectsList.ObjectsToInteract.FoundInteractObj())
            {
                Vector3D point_xy = Camera.GetPointXYPtn(e.X, e.Y, 0);
                if (point_xy == null) return;
                Vector2D p = new Vector2D(point_xy.x, point_xy.y);
                AllObjectsList.ObjectsToDraw.MouseUp(e, p);//, ref glControl );
            }
            this.Invalidate();
        }

        //===================================================================================================================================================================
        private void glControl_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!Loaded) return;
            //Zoom_MouseWheel(e);
            //
            // -- view control --
            //
            //if (Camera == camera_2D)
            //{
            float scale_ratio = 0.0005f;
            if (Camera.m_position.z <= 10)
                scale_ratio = 0.001f;

            Camera.m_position.z *= Convert.ToSingle(1.0 - e.Delta * scale_ratio);
            this.Invalidate();
        }

        /// <summary>
        /// Update the glControl.
        /// </summary>
        //public void Invalidate()
        //{
        //    this.Invalidate();
        //}

        //===================================================================================================================================================================
        /// <summary>
        /// Zoom to show all objects.
        /// </summary>
        public void ZoomAll()
        {
            double zoomfak = 10.1;
            double zoomfak_perspective = 10.001;
            BoundingBox box = new BoundingBox();

            // -- find the bounding box for all objects --
            if (AllObjectsList.ObjectsToDraw.Count > 0)
            {
                foreach (GeoObject curObj in AllObjectsList.ObjectsToDraw)
                    curObj.GetBoundingBox(box);
            }
            Vector3D min = box.GetMin();
            Vector3D max = box.GetMax();

            //Console.WriteLine("In ZoomAll");

            if (box.IsValid()) //wurden überhaupt Punkte in das BoundingBox Objekt eingefügt?
            {
                if (Camera == camera_2D)
                {
                    //Console.WriteLine("box is valid and camera is 2d");
                    //Grundriss
                    CGeom3d.Vec_3d midV = (min + max) / 2;

                    double camera_z = Camera.m_position.z;
                    if (camera_z <= 0)
                        camera_z = 50;
                    else
                    {
                        camera_z = Math.Max(max.X - min.X, max.Y - min.Y) / 2;
                    }
                    Camera.SetPosition(-midV.x, -midV.y, camera_z);

                }
                else
                {
                    //Perspektive
                    CGeom3d.Vec_3d midV = (min + max) / 2;

                    double camera_z = Camera.m_position.z;
                    if (camera_z <= 0)
                        camera_z = 50;
                    else
                    {
                        // distance of the camera for covering objects in Y-dimension 
                        double z_for_y = (max.Y - min.Y) / 2 / Math.Tan(Camera.fovy / 2);

                        Int32[] oldViewport = new Int32[4];
                        GL.GetInteger(GetPName.Viewport, oldViewport);

                        double dAspectRatio = Convert.ToDouble(oldViewport[2]) / Convert.ToDouble(oldViewport[3]);

                        // distance of the camera for covering objects in Y-dimension 
                        double z_for_x = (max.X - min.X) / 2 / Math.Tan(Camera.fovy / 2) / dAspectRatio;

                        camera_z = Math.Max(z_for_x, z_for_y);
                    }
                    Camera.SetPosition(-midV.x, -midV.y, camera_z);

                    Camera.m_alpha = 0;
                    Camera.m_beta = 0;
                    Camera.m_gamma = 0;

                }

                this.Invalidate();
            }
        }

        //===================================================================================================================================================================
        private double GetDistMitte(CGeom3d.Vec_3d mitte, CGeom3d.Vec_3d blick, CGeom3d.Vec_3d p, double angle, ref CGeom3d.Vec_3d erg, double zoomfak)
        {
            //Distanz Punkt Linie
            double sa, sb, sc, sd;
            double q, r;
            Vector3D bp = new Vector3D();
            double distPL, distNP, dist;

            sa = blick.x;
            sb = blick.y;
            sc = blick.z;
            sd = -p.x * sa - p.y * sb - p.z * sc;

            q = sa * blick.x + sb * blick.y + sc * blick.z;

            r = (sa * mitte.x + sb * mitte.y + sc * mitte.z + sd) / q;

            bp.x = mitte.x - blick.x * r;
            bp.y = mitte.y - blick.y * r;
            bp.z = mitte.z - blick.z * r;

            distPL = Math.Sqrt(Math.Pow(bp.x - p.x, 2) + Math.Pow(bp.y - p.y, 2) + Math.Pow(bp.z - p.z, 2));

            distNP = distPL / Math.Tan(angle / 2.0f) * zoomfak;

            (erg).x = bp.x - (blick.x * distNP);
            (erg).y = bp.y - (blick.y * distNP);
            (erg).z = bp.z - (blick.z * distNP);

            dist = Math.Sqrt(Math.Pow((erg).x - mitte.x, 2) + Math.Pow((erg).y - mitte.y, 2) + Math.Pow((erg).z - mitte.z, 2));

            return dist;
        }

        public static Vector3D rotate(Vector3D v, Vector3D axis, double angle)
        {
            // Rotate the point (x,y,z) around the vector (u,v,w)
            // Function RotatePointAroundVector(x#,y#,z#,u#,v#,w#,a#)
            double ux = axis.X * v.X;
            double uy = axis.X * v.Y;
            double uz = axis.X * v.Z;
            double vx = axis.Y * v.X;
            double vy = axis.Y * v.Y;
            double vz = axis.Y * v.Z;
            double wx = axis.Z * v.X;
            double wy = axis.Z * v.Y;
            double wz = axis.Z * v.Z;
            double sa = Math.Sin(angle);
            double ca = Math.Cos(angle);
            double x = axis.X
                    * (ux + vy + wz)
                    + (v.X * (axis.Y * axis.Y + axis.Z * axis.Z) - axis.X
                            * (vy + wz)) * ca + (-wy + vz) * sa;
            double y = axis.Y
                    * (ux + vy + wz)
                    + (v.Y * (axis.X * axis.X + axis.Z * axis.Z) - axis.Y
                            * (ux + wz)) * ca + (wx - uz) * sa;
            double z = axis.Z
                    * (ux + vy + wz)
                    + (v.Z * (axis.X * axis.X + axis.Y * axis.Y) - axis.Z
                            * (ux + vy)) * ca + (-vx + uy) * sa;

            return new Vector3D(x, y, z);

        }

        # endregion

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // CameraGLControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Name = "CameraGLControl";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.CameraGLControl_Paint);
            this.ResumeLayout(false);

        }





    }
}

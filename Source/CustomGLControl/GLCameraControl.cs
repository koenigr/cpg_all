﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GLCameraControl.cs 
//  Copyright (C) 2014/10/18  1:01 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Zeng Wei, Christian Tonn, Sven Schneider 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Drawing;
using System.Windows.Forms;
using CPlan.Geometry;
using CPlan.VisualObjects;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace CPlan.CustomGLControl
{
    public partial class GLCameraControl : GLControl
    {
        //############################################################################################################################################################################
        # region Private Fields

        private Vector3d _lastMouseLocation;
        private bool _panRotate;
        private bool _move;
        private bool _objInteract = true;

        # endregion

        //############################################################################################################################################################################
        # region Properties

        public Camera Camera_2D;
        public Camera Camera_3D;

        /// <summary>
        /// Used camera setting.
        /// </summary>
        public Camera Camera;

        /// <summary> 
        /// Indicates if GLControl control is loaded. 
        /// </summary>
        public bool Loaded { get; protected set; }

        /// <summary> 
        /// If you want to to extend to paint method in your form, SwapBuffers shall be turned off. Call SwapBuffers() after all objects are drawn!
        /// </summary>
        public bool SwapBuffersActivated { get; set; }
        
        /// <summary>
        /// The BackgroundColor of the view;
        /// </summary>
        public Color BackgroundColor { 
            get { return ConvertColor.CreateColor(Camera.BackgroundColor); }
            set
            {
                Camera_2D.BackgroundColor = ConvertColor.CreateOpenTKColor4(value);
                Camera_3D.BackgroundColor = ConvertColor.CreateOpenTKColor4(value);
            } 
        }

        /// <summary>
        /// The RasterColor of the view;
        /// </summary>
        public Color RasterColor
        {
            get { return ConvertColor.CreateColor(Camera.RasterColor); }
            set
            {
                Camera_2D.RasterColor = ConvertColor.CreateOpenTKColor4(value);
                Camera_3D.RasterColor = ConvertColor.CreateOpenTKColor4(value);
            }
        }

        /// <summary>
        /// Background raster on or off.
        /// </summary>
        public bool ShowRaster
        {
            get { return Camera.DrawRaster; }
            set
            {
                Camera_2D.DrawRaster = value;
                Camera_3D.DrawRaster = value;
            }
        }

        /// <summary>
        /// Cellection of all objects that shall be drawn in this view.
        /// </summary>
        public ObjectsAllList AllObjectsList { get; set; }

        public bool ObjectInteraction {
            get { return _objInteract; }
            set { _objInteract = value; }
        }

        # endregion

        //############################################################################################################################################################################
        # region Constructors

        //===================================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of GLCameraControl.
        /// Antialiasing is set via GraphicsMode: http://www.opentk.com/node/3613
        /// </summary>
        public GLCameraControl()
            : base(new GraphicsMode(32, 24, 8, 4))
        {
            InitializeComponent();

            AllObjectsList = new ObjectsAllList();
            
            //MouseMode = "ScreenMove";
            SwapBuffersActivated = true;

            MouseWheel += glControl_MouseWheel;
            MouseMove += glControl_MouseMove;
            MouseDown += glControl_MouseDown;
            MouseUp += glControl_MouseUp;
            Load += glControl_Load;
            Resize += glControl_Resize;
            Paint += glControl_Paint;

            Camera_2D = new Camera
            {
                Beta = -(float) Math.PI/2, 
                Znear = -9999.9f, 
                Zfar = 9999.9f, 
                Ortho = true
            };
            Camera_2D.SetPosition(0, 0, 50);

            Camera_3D = new Camera
            {
                Alpha = -(float) Math.PI/4,
                Beta = -(float) Math.PI/12,
                Zoom = 60.0f,
                Znear = 0.1f,
                Zfar = 9999.9f,
                Ortho = false
            };
            Camera_3D.SetPosition(0.0f, 0.0f, 50f);

            Camera = Camera_2D;
        }

        # endregion

        //############################################################################################################################################################################
        # region Methods
        public void CameraChange()
        {
            if (Camera == Camera_2D) Camera = Camera_3D;
            else if (Camera == Camera_3D) Camera = Camera_2D;
            Invalidate();
        }

        //===================================================================================================================================================================
        private void glControl_Load(object sender, EventArgs e)
        {
            Loaded = true;
            //BackgroundColor = Color.LightGray;
            //RasterColor = Color.White;
            SetupViewport();
        }

        //===================================================================================================================================================================
        /// <summary>
        /// Initialize the GLControl viewport settings
        /// </summary>
        public void SetupViewport()
        {
            MakeCurrent();

            // Bug: for some reason visual studio 2012 crashes when using GL.Viewport from the custom control!
            //int w = Width;
            //int h = Height;
            //GL.Viewport(0, 0, w, h); 
        }

        //===================================================================================================================================================================
        /// <summary>
        /// Draw method of the control.
        /// Draw all objects in AllObjectsList and the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <comment> If you want to add a custom paint part in your form, use SwapBuffers() after your drawing part and deactivate SwapBuffers() here!</comment>
        private void glControl_Paint(object sender, PaintEventArgs e)      
        {
            if (!Loaded) return;
            MakeCurrent();

            InitOpenGL();
            GL.ClearColor(BackgroundColor);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            //Console.WriteLine("Camera: X "+Camera.m_position.x+" Y "+Camera.m_position.y+" Z "+Camera.m_position.z);
            Camera.SetOpenGLMatrixes();
            Camera.DrawKoordinatenkreuz();

            AllObjectsList.Draw();

            if (SwapBuffersActivated) SwapBuffers();
        }

        //===================================================================================================================================================================
        /// <summary>
        /// Initialize the OpenGL settings.
        /// </summary>
        private void InitOpenGL()
        {
            GL.Enable(EnableCap.DepthTest);

            // -- set background color 
            GL.ClearColor(BackgroundColor);

            // -- set clear Z-Buffer value
            GL.ClearDepth(1.0f);

            GL.Enable(EnableCap.Blend);
            GL.Enable(EnableCap.PointSmooth);
            GL.Enable(EnableCap.LineSmooth);
            GL.Enable(EnableCap.PolygonSmooth);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.ShadeModel(ShadingModel.Smooth);
            GL.DepthFunc(DepthFunction.Lequal);
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
        }

        //===================================================================================================================================================================
        private void glControl_Resize(object sender, EventArgs e)
        {
            if (!Loaded) return;
            SetupViewport();
            Invalidate();
        }

        //===================================================================================================================================================================
        /// <summary>
        /// Can be used to deactivate MouseMove, MouseDown, and MouseUp.
        /// Maybe useful for multi threading.
        /// </summary>
        public void DeactivateInteraction()
        {
            MouseMove -= glControl_MouseMove;
            MouseDown -= glControl_MouseDown;
            MouseUp -= glControl_MouseUp;
        }

        /// <summary>
        /// Activate MouseMove, MouseDown, and MouseUp.
        /// </summary>
        public void ActivateInteraction()
        {
            MouseMove += glControl_MouseMove;
            MouseDown += glControl_MouseDown;
            MouseUp += glControl_MouseUp;
        }

        //===================================================================================================================================================================
        private void glControl_MouseDown(object sender, MouseEventArgs e)
        {
            if (!Loaded) return;

            // -- if select move screen button
            //if (MouseMode == "ScreenMove")
            {
                _lastMouseLocation = new Vector3d(Cursor.Position.X, Cursor.Position.Y, 0);

                // -- view control --------------------------------------------------
                // -- left mouse for move
                if (e.Button == MouseButtons.Right)
                {
                    _panRotate = false;
                    _move = true;
                }

                // -- right mouse for pan & rotate
                if (e.Button == MouseButtons.Right && Camera == Camera_3D)
                {
                    _panRotate = true;
                    _move = false;
                }
            }

            // -- objects interactions -----------------------------------------------
            if (ObjectInteraction)
            {
                if (e.Button == MouseButtons.Left)
                {
                    Vector3d pointXy = Camera.GetPointXYPtn(e.X, e.Y);

                    if (pointXy == null) return;
                    Vector2d p = new Vector2d(pointXy.X, pointXy.Y);
                    AllObjectsList.ObjectsToInteract.MouseDown(e, p);
                }

                _panRotate = false;
                _move = false;
            }
            Invalidate();

        }

        //===================================================================================================================================================================
        private void glControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (!Loaded) return;
            
            // -- view control --
            if (_lastMouseLocation == null)
                return;

            double deltaX = Cursor.Position.X - _lastMouseLocation.X;
            double deltaY = Cursor.Position.Y - _lastMouseLocation.Y;

            // -- right mouse move
            if (e.Button == MouseButtons.Right)
            {
                Vector3d preWorldPos = Camera.GetPointXYPtn(_lastMouseLocation);
                Vector3d curWorldPos = Camera.GetPointXYPtn(Cursor.Position.X, Cursor.Position.Y);
                double deltaXWorld = curWorldPos.X - preWorldPos.X;
                double deltaYWorld = curWorldPos.Y - preWorldPos.Y;

                // -- control key down,  zoom
                if (ModifierKeys == Keys.Control)
                {
                    float scaleRatio = 0.01f;
                    if (Camera.Position.z <= 10)
                        scaleRatio = 0.1f;

                    Camera.Position.z *= Convert.ToSingle(1.0 - deltaYWorld * scaleRatio);
                }
                else if (ModifierKeys == Keys.Shift && Camera == Camera_3D)
                {
                    // --- 3D Camera Rotate ---
                    Camera.Alpha += Convert.ToSingle(deltaY * 0.01f);
                    Camera.Beta += Convert.ToSingle(deltaX * 0.01f);

                    while (Camera.Alpha < 0) Camera.Alpha += Convert.ToSingle(2 * Math.PI);
                    while (Camera.Alpha > 2 * Math.PI) Camera.Alpha -= Convert.ToSingle(2 * Math.PI);

                    while (Camera.Beta < 0) Camera.Beta += Convert.ToSingle(2 * Math.PI);
                    while (Camera.Beta > 2 * Math.PI) Camera.Beta -= Convert.ToSingle(2 * Math.PI);
                }

                else if (ModifierKeys == Keys.Alt || (ModifierKeys == Keys.Shift && Camera == Camera_2D))
                {
                    // -- change the origin point here
                    Vector3d originPoint = new Vector3d(0, 0, 0);

                    Vector3d preDir = new Vector3d(preWorldPos.X - originPoint.X, preWorldPos.Y - originPoint.Y, 0);
                    Vector3d curDir = new Vector3d(curWorldPos.X - originPoint.X, curWorldPos.Y - originPoint.Y, 0);

                    double angle1 = Math.Atan2(curDir.Y, curDir.X);
                    double angle2 = Math.Atan2(preDir.Y, preDir.X);
                    double rotateAngle = angle2 - angle1;

                    Camera.Gamma -= (float)rotateAngle;
                }
                else
                {
                    Vector3d newPos = Camera.GetCameraPos;
                    newPos.X += deltaXWorld;
                    newPos.Y += deltaYWorld;
                    Camera.SetPosition(newPos.X, newPos.Y, newPos.Z);
                }

                _lastMouseLocation.X = Cursor.Position.X;
                _lastMouseLocation.Y = Cursor.Position.Y;
                _lastMouseLocation.Z = 0;
                Invalidate();
            }

            // -- objects interactions -----------------------------------------------
            if (ObjectInteraction)
            {
                if (e.Button == MouseButtons.Left)
                {
                    Vector3d pointXy = Camera.GetPointXYPtn(e.X, e.Y);
                    if (pointXy == null) return;

                    //Cursor tmpCursor = Cursors.Default;
                    Vector2d p = new Vector2d(pointXy.X, pointXy.Y);
                    AllObjectsList.ObjectsToInteract.MouseMove(e, p); //, ref glControl, ref tmpCursor);

                    // Hier könnte man noch optimieren, indem man immer nur den Bereich neuzeichnet, in dem das Objekt bewegt wurde.
                }
            }

        }

        //===================================================================================================================================================================
        private void glControl_MouseUp(object sender, MouseEventArgs e)
        {
            if (!Loaded) return;

            // -- view control --
            if (_panRotate || _move)
            {
                _panRotate = false;
                _move = false;
                return;
            }

            // -- objects interactions -----------------------------------------------
            if (ObjectInteraction)
            {
                AllObjectsList.ObjectsToInteract.MouseUp(e); // put the selected element to the fornt
                if (AllObjectsList.ObjectsToInteract.FoundInteractObj())
                {
                    // TODO: pointXy is not nullable, this logic won't work
                    Vector3d pointXy = Camera.GetPointXYPtn(e.X, e.Y);
                    if (pointXy == null) return;
                    Vector2d p = new Vector2d(pointXy.X, pointXy.Y);
                    AllObjectsList.ObjectsToDraw.MouseUp(e, p);
                }
            }
            Invalidate();
        }

        //===================================================================================================================================================================
        private void glControl_MouseWheel(object sender, MouseEventArgs e)
        {
            if (!Loaded) return;

            // -- view control --{
            float scaleRatio = 0.0005f;
            if (Camera.Position.z <= 10)
                scaleRatio = 0.001f;

            Camera.Position.z *= Convert.ToSingle(1.0 - e.Delta * scaleRatio);
            Invalidate();
        }

        //===================================================================================================================================================================
        /// <summary>
        /// Zoom to show all objects.
        /// </summary>
        public void ZoomAll()
        {
            //double zoomfak = 10.1;
            //double zoomfakPerspective = 10.001;
            BoundingBox box = new BoundingBox();

            // -- find the bounding box for all objects --
            if (AllObjectsList.ObjectsToDraw.Count > 0)
            {
                foreach (GeoObject curObj in AllObjectsList.ObjectsToDraw)
                    curObj.GetBoundingBox(box);
            }
            Vector3d min = box.GetMin();
            Vector3d max = box.GetMax();

            //Console.WriteLine("In ZoomAll");

            if (!box.IsValid()) return;
            if (Camera == Camera_2D)
            {
                //Console.WriteLine("box is valid and camera is 2d");
                //Grundriss
                Vector3d midV = (min + max) / 2;

                double cameraZ = Camera.Position.z;
                if (cameraZ <= 0)
                    cameraZ = 50;
                else
                {
                    cameraZ = Math.Max(max.X - min.X, max.Y - min.Y) / 2;
                }
                Camera.SetPosition(-midV.X, -midV.Y, cameraZ);

            }
            else
            {
                //Perspektive
                Vector3d midV = (min + max) / 2;

                double cameraZ = Camera.Position.z;
                if (cameraZ <= 0)
                    cameraZ = 50;
                else
                {
                    // distance of the camera for covering objects in Y-dimension 
                    double zForY = (max.Y - min.Y) / 2 / Math.Tan(Camera.Fovy / 2);

                    Int32[] oldViewport = new Int32[4];
                    GL.GetInteger(GetPName.Viewport, oldViewport);

                    double dAspectRatio = Convert.ToDouble(oldViewport[2]) / Convert.ToDouble(oldViewport[3]);

                    // distance of the camera for covering objects in Y-dimension 
                    double zForX = (max.X - min.X) / 2 / Math.Tan(Camera.Fovy / 2) / dAspectRatio;

                    cameraZ = Math.Max(zForX, zForY);
                }
                Camera.SetPosition(-midV.X, -midV.Y, cameraZ);

                Camera.Alpha = 0;
                Camera.Beta = 0;
                Camera.Gamma = 0;
            }
            Invalidate();
        }

        //===================================================================================================================================================================
        //TODO: should be copied to Geometry / GeoAlgorithms3D
        private double GetDistMitte(CGeom3d.Vec_3d mitte, CGeom3d.Vec_3d blick, CGeom3d.Vec_3d p, double angle, ref CGeom3d.Vec_3d erg, double zoomfak)
        {
            //Distanz Punkt Linie
            double sa, sb, sc, sd;
            double q, r;
            Vector3d bp = new Vector3d();

            sa = blick.x;
            sb = blick.y;
            sc = blick.z;
            sd = -p.x * sa - p.y * sb - p.z * sc;

            q = sa * blick.x + sb * blick.y + sc * blick.z;

            r = (sa * mitte.x + sb * mitte.y + sc * mitte.z + sd) / q;

            bp.X = mitte.x - blick.x * r;
            bp.Y = mitte.y - blick.y * r;
            bp.Z = mitte.z - blick.z * r;

            double distPL = Math.Sqrt(Math.Pow(bp.X - p.x, 2) + Math.Pow(bp.Y - p.y, 2) + Math.Pow(bp.Z - p.z, 2));

            double distNP = distPL / Math.Tan(angle / 2.0f) * zoomfak;

            (erg).x = bp.X - (blick.x * distNP);
            (erg).y = bp.Y - (blick.y * distNP);
            (erg).z = bp.Z - (blick.z * distNP);

            double dist = Math.Sqrt(Math.Pow((erg).x - mitte.x, 2) + Math.Pow((erg).y - mitte.y, 2) + Math.Pow((erg).z - mitte.z, 2));

            return dist;
        }

        //===================================================================================================================================================================
        //TODO: should be copied to Geometry / GeoAlgorithms3D
        public static Vector3d Rotate(Vector3d v, Vector3d axis, double angle)
        {
            // Rotate the point (x,y,z) around the vector (u,v,w)
            // Function RotatePointAroundVector(x#,y#,z#,u#,v#,w#,a#)
            double ux = axis.X * v.X;
            double uy = axis.X * v.Y;
            double uz = axis.X * v.Z;
            double vx = axis.Y * v.X;
            double vy = axis.Y * v.Y;
            double vz = axis.Y * v.Z;
            double wx = axis.Z * v.X;
            double wy = axis.Z * v.Y;
            double wz = axis.Z * v.Z;
            double sa = Math.Sin(angle);
            double ca = Math.Cos(angle);
            double x = axis.X
                    * (ux + vy + wz)
                    + (v.X * (axis.Y * axis.Y + axis.Z * axis.Z) - axis.X
                            * (vy + wz)) * ca + (-wy + vz) * sa;
            double y = axis.Y
                    * (ux + vy + wz)
                    + (v.Y * (axis.X * axis.X + axis.Z * axis.Z) - axis.Y
                            * (ux + wz)) * ca + (wx - uz) * sa;
            double z = axis.Z
                    * (ux + vy + wz)
                    + (v.Z * (axis.X * axis.X + axis.Y * axis.Y) - axis.Z
                            * (ux + vy)) * ca + (-vx + uy) * sa;

            return new Vector3d(x, y, z);
        }

        # endregion


    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Extensions.cs 
//  Copyright (C) 2014/10/18  1:00 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using CPlan.Geometry;
using QuickGraph;
using OpenTK;
using Tektosyne;
using Tektosyne.Geometry;
using Tektosyne.Collections;
namespace CPlan.Evaluation
{
    public static class Extensions
    {
        //============================================================================================================================================================================
        // --- Extend QuickGraph ---

//      // -- remove the already added lines from the graph
        public static void RemoveEdgeDouble<TVertex, TEdge>(this UndirectedGraph<TVertex, TEdge> graph, TEdge curEdge)
            where TEdge : UndirectedEdge<TVertex>
        {
            TEdge removeCandA = (new UndirectedEdge<TVertex>(curEdge.Source, curEdge.Target)) as TEdge;
            TEdge removeCandB = (new UndirectedEdge<TVertex>(curEdge.Target, curEdge.Source)) as TEdge;
            
            //Does not remove the edge correctly!!
            var removed1 = graph.RemoveEdge(removeCandA);
            var removed2 = graph.RemoveEdge(removeCandB);
            if(!(removed1 && removed2))
            {
                throw new InvalidOperationException();
            }
        }

        public static List<TVertex> GetNeighbors<TVertex, TEdge>(this UndirectedGraph<TVertex, TEdge> graph, TVertex curNode)
            where TEdge : IEdge<TVertex>
        {
            List<TVertex> neighVertList = new List<TVertex>();
            if (graph.ContainsVertex(curNode) == false) return neighVertList; // exit if the tested vertex (curNode) is not part of the grahp
            List<TEdge> neighEdgeList = graph.AdjacentEdges(curNode).ToList();
            foreach (TEdge curEdge in neighEdgeList)
            {
                if (! curEdge.Source.Equals(curNode)) neighVertList.Add(curEdge.Source);
                else if (!curEdge.Target.Equals(curNode)) neighVertList.Add(curEdge.Target);
            }
            return neighVertList;
        }

        public static void MoveGraph (UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> graph, Vector2d moveVect)
        {
            for (int i = 0; i < graph.Vertices.Count(); i++)
            {
                graph.Vertices.ToArray()[i] += moveVect;
            }
        }

        
        //============================================================================================================================================================================
        /// <summary>
        /// Computes the mean value of the ArrayEx Single values.
        /// </summary>
        public static float GetMean(ArrayEx<Single> list)
        {
            float sum = Fortran.Sum(list.ToArray());
            return sum / list.Count;
        }

        //==============================================================================================
        public static Dictionary<PointD, Single> GetNormArray(Dictionary<PointD, int> cterD)
        {
            Dictionary<PointD, Single> newDict = new Dictionary<PointD, Single>();

            int[] normDepth = new int[cterD.Count];
            cterD.Values.CopyTo(normDepth, 0);

            Single[] counter = new Single[normDepth.Length];
            for (int i = 0; i < counter.Length; i++) counter[i] = Convert.ToSingle(normDepth[i]);

            Tools.CutMinValue(counter, Fortran.Min(counter.ToArray<Single>()));
            Tools.Normalize(counter);
            Tools.IntervalExtension(counter, Fortran.Max(counter.ToArray<Single>()));

            int k = 0;
            foreach (KeyValuePair<PointD, int> kvp in cterD)
            {
                newDict.Add(kvp.Key, 1 - counter[k]);
                k++;
            }

            return newDict;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// "Inverts" the values by substracting them from the max value of the list.
        /// Afterwards max = 0.
        /// </summary>
        public static void InvertValues(IList<Single> list)
        {
            Single maxV = Fortran.Max(list.ToArray<Single>());
            if (Single.IsInfinity(maxV))
                return;

            for (int i = 0; i < list.Count; i++)
            {
                list[i] = maxV - list[i];
            }
        }

        /// <summary>
        ///Calculates the middel point of two given points. </summary>
        /// <param name="otherPoint"> The other point. <paramref name="grid"/>.</param>
        /// <returns> The middel point as PointD. </returns>
        public static PointD GetMidPoint(this PointD point, PointD otherPoint)
        {
            PointD midPt = point + otherPoint;
            midPt = new PointD(midPt.X / 2, midPt.Y / 2);
            return midPt;
        }
  
        //============================================================================================================================================================================
        /// <summary>
        /// Copy the elements of a ArrayEx to a 2d Single[,] array.
        /// </summary>
        public static Single[,] CopyTo2dArray(ArrayEx<Single> list)
        {
            SizeI size = new SizeI(list.GetLength(0), list.GetLength(1));
            float[,] curValueArray = new float[size.Width, size.Height];
            for (int x = 0; x < size.Width; x++)
            {
                for (int y = 0; y < size.Height; y++)
                {
                    int idx = list.GetIndex(x, y);
                    curValueArray[x, y] = list[idx];
                }
            }
            return curValueArray;
        }

        /// <summary>
        /// Draws the specified <see cref="RegularPolygon"/> 
        /// to a <see cref="OpenTK.OpenGL"/> component.</summary>
        /// <param name="polygon">
        /// The <see cref="RegularPolygon"/> to draw.</param>
        /// <param name="offset">
        /// The offset by which to shift <paramref name="polygon"/>.</param>
        /// <param name="isFilled">
        /// <c>true</c> to use the area covered by <paramref name="polygon"/> for hit-testing,
        /// rendering, and clipping; otherwise, <c>false</c>.</param>
        /// <param name="fillColor"> The fill color of the regular polygons. <paramref name="grid"/>.</param>
        /// <param name="borderColor"> The border color of the regular polygons.  <paramref name="grid"/>.</param>
        /// <param name="borderWidth"> The width of the border shape of the regular polygons.</param>
        //public static void Draw(this RegularPolygon polygon, PointD offset, bool isFilled,
        //    Vector4 fillColor, Vector4 borderColor, float borderWidth)
        //{
        //    PointD[] vertices = polygon.Vertices;
        //    PointD start = vertices[0] + offset;

        //    if (isFilled)
        //    {
        //        GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
        //        GL.Color4(fillColor);
        //        GL.Begin(BeginMode.Polygon);
        //        for (int i = 0; i < vertices.Length; i++)
        //        {
        //            PointD next = vertices[i] + offset;
        //            GL.Vertex2(next.X, next.Y);
        //        }
        //        GL.End();
        //    }

        //    GL.LineWidth(borderWidth);
        //    GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
        //    GL.Color4(borderColor);
        //    GL.Begin(BeginMode.Polygon);
        //    for (int i = 0; i < vertices.Length; i++)
        //    {
        //        PointD next = vertices[i] + offset;
        //        GL.Vertex2(next.X, next.Y);
        //    }
        //    GL.LineWidth(1);
        //    GL.End();
        //}

        ////============================================================================================================================================================================
        ///// <summary>
        ///// Draws the specified <see cref="PolygonGrid"/>         
        ///// to a <see cref="OpenTK.OpenGL"/> component.
        ///// </summary>
        ///// <param name="grid"> The <see cref="PolygonGrid"/> to draw.</param>
        ///// <param name="isFilled"> <c>true</c> to use the area covered by <paramref name="grid"/> for
        ///// rendering, and clipping; otherwise, <c>false</c>.</param>
        ///// <param name="offset"> The offset by which to shift <paramref name="grid"/>.</param>
        ///// <param name="fillColor"> The fill color of the regular polygons. <paramref name="grid"/>.</param>
        ///// <param name="borderColor"> The border color of the regular polygons.  <paramref name="grid"/>.</param>
        ///// <param name="borderWidth"> The width of the border shape of the regular polygons.</param>
        //public static void Draw(this PolygonGrid grid, PointD offset, bool isFilled,
        //    Vector4 fillColor, Vector4 borderColor, float borderWidth)
        //{

        //    int width = grid.Size.Width, height = grid.Size.Height;
        //    for (int x = 0; x < width; x++)
        //        for (int y = 0; y < height; y++)
        //        {
        //            PointD element = grid.GridToDisplay(x, y);
        //            grid.Element.Draw(element + offset, isFilled, fillColor, borderColor, borderWidth);
        //        }
        //}

        //============================================================================================================================================================================
        /// <summary>
        /// Creates a Subdivision from the PolygonGrid instance.      
        /// </summary>
        /// <param name="offset"> Offsets all elements of the resulting Subdivision.</param>
        public static Tektosyne.Geometry.Subdivision ToSubdivisionFromLines(this PolygonGrid grid, PointD offset, List<Line2D> walls)
        {
            //--- Create a Subdivision (Graph) from the Grid ---
            double GridCellSize = grid.Element.Length;
            //PointD offset = new PointD(GridCellSize / 2, GridCellSize / 2);
            PolygonGrid tempGrid = new PolygonGrid(grid); // the temoGrid is necessary, because of there is a bug? in the ToSubdivision method of the PolygonGrid! (one Row and one Column to much)
            // tempGrid.Size = new SizeI(CurGrid.Size.Width - 1, CurGrid.Size.Height - 1);
            ListEx<LineD> subdivLines = new ListEx<LineD>();
            Tektosyne.Geometry.LineIntersection sPt = new Tektosyne.Geometry.LineIntersection();
            foreach (PointI node in tempGrid.Nodes)
            {
                PointD curNode = tempGrid.GridToDisplay(node);
                IList<PointI> neighb = tempGrid.GetNeighbors(node);
                foreach (PointI nb in neighb)
                {
                    PointD toNode = tempGrid.GridToDisplay(nb);
                    LineD curLine = new LineD(curNode + offset, toNode + offset);
                    bool intsectTest = false;
                    // --- add lines to the graph only, if there is no intersection with a wall! ---
                    foreach (Line2D curWall in walls)
                    {
                        sPt = Tektosyne.Geometry.LineIntersection.Find(curWall.Start.ToPointD(), curWall.End.ToPointD(), curLine.Start, curLine.End);
                        if (sPt.Exists)
                        {
                            intsectTest = true;
                            break;
                        }
                    }
                    if (!intsectTest) subdivLines.Add(curLine);
                    // -------------------------------------------------------------------------------
                }
            }
            Tektosyne.Geometry.Subdivision curSubdiv = Tektosyne.Geometry.Subdivision.FromLines(subdivLines.ToArray());
            return curSubdiv;
        }


        public static void Move(this LineD line, PointD offset)
        {
            line.Start.Add(offset);
            line.End.Add(offset);
        }

        public static void Offset(this PointD point, PointD offset)
        {
            point.Add(offset);
        }

        //==============================================================================================
        public static PointD ToPointD(this Vector2d origPt)
        {
            return new PointD(origPt.X, origPt.Y);
        }

        public static double Gradient(this LineD line)
        {
            // m_Gradient = (P1.Y - P2.Y) / (P1.X - P2.X);
            double gradient = (line.Start.Y - line.End.Y) / (line.Start.X - line.End.X);
            return gradient;
        }



        public static PointD MultiplyD(this PointD origPt, double value)
        {
            PointD newPoint = new PointD(origPt.X * value, origPt.Y * value);
            return newPoint;
        }

        public static Vector2d ToVector2D(this PointD origPt)
        {
            Vector2d newPoint = new Vector2d(origPt.X, origPt.Y);
            return newPoint;
        }

        public static Vector2d ToVector2D(this PointI origPt)
        {
            Vector2d newPoint = new Vector2d(origPt.X, origPt.Y);
            return newPoint;
        }

    }
}

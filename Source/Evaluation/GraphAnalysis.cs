﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GraphAnalysis.cs 
//  Copyright (C) 2014/10/18  1:00 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using CPlan.Geometry;
using QuickGraph;
using OpenTK;

namespace CPlan.Evaluation
{
    public class GraphAnalysis
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Attributes

        /// <summary> <see cref="QuickGraph.UndirectedGraph"/> Instance to hold the network graph using the QuickGraph library. </summary>
        private UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> _network = new UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>();
        
        /// <summary> Indicates if the graph analysis was calculated. </summary>
        private bool _calculated = false;
       
        /// <summary>
        /// Intance to hold the analysis results. It makes the calculation methods acessible, too. This needs to changed.
        /// But so fal it is used to return the results.
        /// </summary>
        private ShortestPath _shortPahtesAngular;

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        /// <summary>
        /// Returns the analysis retults.
        /// </summary>
        public ShortestPath ResultsAngular
        { get 
            { if (_calculated)
                return _shortPahtesAngular;
            else throw new Exception("No graph analysis is calculated");
            }
        }

        /// <summary>
        /// Returns the Network as UndirectedGraph.
        /// </summary>
        public UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> Network
        { get { return _network; } }

        public bool Finished { get; set; }  

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        /// <summary>
        /// Creates the graph for shortest path analysis.
        /// </summary>
        /// <param name="graphEdges"> A list of graph edges. </param>
        public GraphAnalysis(List<Line2D> graphEdges)
        {
            _network = null;
            _shortPahtesAngular = null;
            _network = GraphTools.CreateGraphFromLines(graphEdges);
            _calculated = false;
            Finished = false;
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        //============================================================================================================================================================================
        /// <summary>
        /// Starts the calculation of the shortest path analysis. 
        /// </summary>
        /// <param name="typeOfWeights"> Indicates which analysis shall be run:
        /// | "angle" | "distance" | "both" | "custom" |.
        /// </param>
        /// <param name="radiuses"> Array of radiuses that are used for the analysis. 
        /// To analysis te complete graph use float.MaxValue. </param>
        public void Calculate(string typeOfWeights, float[] radiuses = null)
        {
            FW_GPU_NET.Graph fwInverseGraphAngular;
            FW_GPU_NET.Graph fwGraph;
            
            //------------------------------------------------------------------------------//
            fwGraph = GraphTools.GenerateFWGpuGraph(_network);
            
            //Dictionary<Vector2d, Line2D> inverseEdgesMapping = new Dictionary<Vector2d, Line2D>();
            //Dictionary<Line2D, double> realDist = new Dictionary<Line2D, double>();
            //Dictionary<Line2D, double> realAngle = new Dictionary<Line2D, double>();
            
            //ShortPahtesAngular.EvaluateOnServer (new System.Windows.Forms.ToolStripProgressBar(), fwGraph, FWInverseGraph);//  null);//FloydWarshall();//Dijikstra();//

            //Vector4[] ffColors = ConvertColor.CreateFFColor(ShortPahtesAngular.GetNormChoiceArray());
            //Vector4[] ffColors = null;
            //if (rB_fitChoice.Checked) ffColors = ConvertColor.CreateFFColor4(ShortPahtesAngular.GetNormChoiceArray());
            //if (rB_fitCentral.Checked) ffColors = ConvertColor.CreateFFColor4(ShortPahtesAngular.GetNormCentralityMetricArray());
            //radiuses = new float[] { float.MaxValue, 700, 200 }; 

            if (typeOfWeights == "angle")
            {
                
                fwInverseGraphAngular = GraphTools.GenerateInverseDoubleGraph(fwGraph); // the costs/wightings are calculated here...
                _shortPahtesAngular = new ShortestPath(_network);
                _shortPahtesAngular.Evaluate(new System.Windows.Forms.ToolStripProgressBar(), fwGraph, fwInverseGraphAngular, radiuses);//  null);//FloydWarshall();//Dijikstra();//
                fwInverseGraphAngular.Dispose();
            }
            else if (typeOfWeights == "distance")
            {
                // not implemented yet...
                throw new Exception("The function for analysis with distance weiths is not implemented yet.");
            }
            else if (typeOfWeights == "both")
            {
                // not implemented yet...
                throw new Exception("The function for analysis with both weiths is not implemented yet.");
            }
            else if (typeOfWeights == "custom")
            {
                // not implemented yet...
                throw new Exception("The function for analysis with custom weiths is not implemented yet.");
            }
            else
            {
                // Fehlermedung
                throw new Exception("Graph analysis weiths are not secified.");
            }

            // -- delete all graphs that were only necessary for calculation --
            fwGraph.ClearGraph();
            fwGraph.Dispose();

            _calculated = true;
        }

        #endregion
    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Node.cs 
//  Copyright (C) 2014/10/18  1:00 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using Tektosyne.Geometry;

namespace CPlan.Evaluation
{
    [Serializable]
    public class GNode
    {
        public GNode(PointD point, bool active)
        {
            Point = point;// new PointD(x, y);
            Active = active;
            Connectivity = 0;
            SelectProbability = 0;
        }

        public readonly PointD Point;
        public bool Active;
        public int Connectivity;// {get; set;}
        public double SelectProbability;
        public IList<PointD> Neighbours;
        

    }

    [Serializable]
    public class DataNode
    {
        public double LengthToGrow = 30;
        public int AngleToGrow = 1;

        public DataNode()
        {
        }
        
        public DataNode(DataNode oldDataNode)
        {
            LengthToGrow = oldDataNode.LengthToGrow;
            AngleToGrow = oldDataNode.AngleToGrow;
        }
    }
}

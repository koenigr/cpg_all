﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GraphTools.cs 
//  Copyright (C) 2014/10/18  1:00 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using CPlan.Geometry;
using FW_GPU_NET;
using QuickGraph;
using Tektosyne.Geometry;
using OpenTK;

namespace CPlan.Evaluation
{
    [Serializable]
    public class GraphTools
    {

        public static float GetInverseAngleCost(Edge inputEdge1, Edge inputEdge2, Node inputConnectionNode)
        {
            /* Die Koordinaten zum richtigen berechnen der Winkelkosten sollten mittels inputNodes UserData (z.B. x,y) erfolgen
             * bitte nicht inputEdge UserData verwenden, da es vom GenerateInverseDoubleGraph Algorithmus verwendet wird
             * */
            Vector2d ptA, ptB;
            Vector2d midPt = (Vector2d)inputConnectionNode.UserData;
            // --- look for the other points to describe the triangle ---
            if ((Vector2d)inputEdge1.End.UserData == midPt)
            {
                ptA = (Vector2d)inputEdge1.Start.UserData;
            }
            else ptA = (Vector2d)inputEdge1.End.UserData;

            if ((Vector2d)inputEdge2.End.UserData == midPt)
            {
                ptB = (Vector2d)inputEdge2.Start.UserData;
            }
            else ptB = (Vector2d)inputEdge2.End.UserData;

            float angle = GraphTools.AngleForChoice(ptA, midPt, ptB);

            return angle;
            //return 1;
        }

        //============================================================================================================================================================================
        //============================================================================================================================================================================
        public static List<Line2D> ToListLine2D(UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> graph)
        {
            List<Line2D> lineList = new List<Line2D>();
            List<UndirectedEdge<Vector2d>> edges = graph.Edges.ToList();
            foreach (UndirectedEdge<Vector2d> edge in edges)
            {
                Line2D edgeLine = new Line2D(edge.Source.X, edge.Source.Y, edge.Target.X, edge.Target.Y);
                lineList.Add(edgeLine);
            }
            return lineList;
        }

        public static List<Line2D> ToListLine2D(IEnumerable<LineD> graph)
        {
            List<Line2D> lineList = new List<Line2D>();
            foreach (LineD edge in graph)
            {
                Line2D edgeLine = new Line2D(edge.Start.X, edge.Start.Y, edge.End.X, edge.End.Y);
                lineList.Add(edgeLine);
            }
            return lineList;
        }

        //============================================================================================================================================================================
        public static UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> CreateGraphFromLines(List<Line2D> graphEdges)
        {
            UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> network = new UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>();
            Vector2d startPt, endPt;

            foreach (Line2D edge in graphEdges)
            {
                startPt = edge.Start;
                endPt = edge.End;
                UndirectedEdge<Vector2d> newEdge = new UndirectedEdge<Vector2d>(startPt, endPt);
                UndirectedEdge<Vector2d> reverseNewEdge = network.Edges.ToList().Find(x => x.Source == endPt && x.Target == startPt);

                // --- a new edge (& vertex) is added ---                       
                if (network.ContainsVertex(startPt) && network.ContainsVertex(endPt))
                {
                    if (reverseNewEdge == null)
                        network.AddEdge(newEdge);
                }
                else if (reverseNewEdge == null)
                {
                    network.AddVerticesAndEdge(newEdge);
                }
            }
            return network;

        }
        //============================================================================================================================================================================
        public static FW_GPU_NET.Graph GenerateFWGpuGraph(Tektosyne.Geometry.Subdivision curGraph)
        {
            FW_GPU_NET.Graph fwGpuGraph;
            //fwGpuGraph = null;
            fwGpuGraph = new FW_GPU_NET.Graph();
            List<PointD> curNodes = curGraph.Nodes.ToList();
            List<LineD> curEdges = curGraph.ToLines().ToList();
            int start, end;

            // --- create the new nodes and add them to the FW_GPU_NET.Graph --- 
            FW_GPU_NET.Node[] newNodes = new FW_GPU_NET.Node[curNodes.Count];
            for (int i = 0; i < curNodes.Count; i++)
            {
                newNodes[i] = fwGpuGraph.AddNode();
                newNodes[i].UserData = curNodes[i];
            }

            // --- create the new edges by using the new nodes and add them to the FW_GPU_NET.Graph --- 
            foreach (LineD line in curEdges)
            {
                start = curNodes.IndexOf(line.Start);
                end = curNodes.IndexOf(line.End);
                //weight = Convert.ToSingle(refValues[line]);// Dictionary<LineD, double> refValues
                fwGpuGraph.AddEdge(newNodes[start], newNodes[end], 1);
            }

            return fwGpuGraph;
        }

        //============================================================================================================================================================================
        public static FW_GPU_NET.Graph GenerateFWGpuGraph(UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> curGraph)
        {
            FW_GPU_NET.Graph fwGpuGraph;
            //fwGpuGraph = null;
            fwGpuGraph = new FW_GPU_NET.Graph();
            List<Vector2d> curNodes = curGraph.Vertices.ToList();
            List<Line2D> curEdges = new List<Line2D>();
            foreach (UndirectedEdge<Vector2d> edge in curGraph.Edges) curEdges.Add(new Line2D(edge.Source, edge.Target));
            int start, end;

            // --- create the new nodes and add them to the FW_GPU_NET.Graph --- 
            FW_GPU_NET.Node[] newNodes = new FW_GPU_NET.Node[curNodes.Count];
            for (int i = 0; i < curNodes.Count; i++)
            {
                newNodes[i] = fwGpuGraph.AddNode();
                newNodes[i].UserData = curNodes[i];
            }

            // --- create the new edges by using the new nodes and add them to the FW_GPU_NET.Graph --- 
            foreach (Line2D line in curEdges)
            {
                start = curNodes.IndexOf(line.Start);
                end = curNodes.IndexOf(line.End);
                //weight = Convert.ToSingle(refValues[line]);// Dictionary<LineD, double> refValues
                fwGpuGraph.AddEdge(newNodes[start], newNodes[end], 1);
            }

            return fwGpuGraph;
        }

        //============================================================================================================================================================================
        public static FW_GPU_NET.Graph GenerateInverseDoubleGraph(FW_GPU_NET.Graph input)
        {
            FW_GPU_NET.Graph outputGraph;
            outputGraph = null;
            outputGraph = new FW_GPU_NET.Graph();

            //Knoten für jede input-Kante erzeugen (es sind alles gerichtete Kanten)
            foreach (Edge edge in input.Edges)
            {
                Node node = outputGraph.AddNode();

                //dem Knoten seine input-Kante mitgeben und umgekehrt
                node.UserData = edge;
                edge.UserData = node;
            }

            //Parent-Child Zuordnung
            Node[] outputNodes = outputGraph.Nodes;
            for (int i = 0; i < outputNodes.Length; ++i)
                for (int j = i + 1; j < outputNodes.Length; ++j)
                {
                    Edge inputEdge1 = (Edge)outputNodes[i].UserData;
                    Edge inputEdge2 = (Edge)outputNodes[j].UserData;
                    if ((inputEdge1.Start == inputEdge2.End) &&
                        (inputEdge2.Start == inputEdge1.End)) //selbe edge als Node Grundlage
                        outputNodes[j].Parent = outputNodes[i];
                }

            //alle vorwärtsgerichteten Kanten im output hinzufügen
            foreach (Node outputNode in outputGraph.Nodes)
            {
                //Zielknoten der zugeordneten input-Edge finden
                Edge inputEdge = (Edge)outputNode.UserData;
                Node aimNode = inputEdge.End;

                //aus allen rausgehenden Kanten des aimNode inputEdge-Richtung entfernen
                Edge[] outgoing = aimNode.OutgoingEdges;
                //System.Collections.Generic.List<Edge> inputEdgeList = new System.Collections.Generic.List<Edge>();
                foreach (Edge edge in outgoing)
                {
                    if (!(edge.End == inputEdge.Start))
                    {
                        //inputEdgeList.Add(edge);               
                        //neue output-Kante einfügen
                        Node outputAimNode = (Node)edge.UserData;
                        outputGraph.AddDirectedEdge(outputNode, outputAimNode, GetInverseAngleCost(inputEdge, edge, aimNode));
                    }
                    //else
                    //{
                    //    break;
                    //}
                }
            }

            return outputGraph;
        }

        //============================================================================================================================================================================
        public static UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> ConstructInverseGraph(UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> curGraph,
            ref Dictionary<UndirectedEdge<Vector2d>, double> realDist, ref Dictionary<UndirectedEdge<Vector2d>, double> realAngle, ref Dictionary<Vector2d, UndirectedEdge<Vector2d>> inverseEdgesMapping)
        {
            List<Line2D> curLinesArr = new List<Line2D>();
            foreach (UndirectedEdge<Vector2d> edge in curGraph.Edges)
                curLinesArr.Add(new Line2D(edge.Source, edge.Target));

            List<UndirectedEdge<Vector2d>> newLines = new List<UndirectedEdge<Vector2d>>();
            inverseEdgesMapping = new Dictionary<Vector2d, UndirectedEdge<Vector2d>>();
            realDist = new Dictionary<UndirectedEdge<Vector2d>, double>();
            realAngle = new Dictionary<UndirectedEdge<Vector2d>, double>();

            foreach (Line2D curLine in curLinesArr)
            {
                Vector2d midPt = curLine.Center;// GetMidPoint(curLine.Start, curLine.End);
                if (!inverseEdgesMapping.ContainsKey(midPt))
                    inverseEdgesMapping.Add(midPt,  
                        new UndirectedEdge<Vector2d>(new Vector2d(curLine.Start.X, curLine.Start.Y), new Vector2d(curLine.End.X, curLine.End.Y)));

                // -- check neighbours of the start point of a line --
                List<Vector2d> curNeighb = curGraph.GetNeighbors(curLine.Start);
                double dist, angle;
                foreach (Vector2d neiPt in curNeighb)
                {
                    //if (! neiPt.Equals(curLine.End))
                    if (!(neiPt.X == curLine.End.X && neiPt.Y == curLine.End.Y))
                    {
                        Vector2d neiMidPt = new Line2D(curLine.Start, neiPt).Center; //.Start.GetMidPoint(neiPt);
                        if (!midPt.Equals(neiMidPt))
                        {
                            UndirectedEdge<Vector2d> newLine = new UndirectedEdge<Vector2d>(midPt, neiMidPt);
                            UndirectedEdge<Vector2d> testNewEdge = newLines.Find(x => x.Source == midPt && x.Target == neiMidPt);
                            UndirectedEdge<Vector2d> testRevNewEdge = newLines.Find(x => x.Source == neiMidPt && x.Target == midPt);
                            if (testNewEdge == null && testRevNewEdge == null)
                            {
                                newLines.Add(newLine);
                                dist = GeoAlgorithms2D.DistancePoints(midPt, curLine.Start) +
                                       GeoAlgorithms2D.DistancePoints(neiMidPt, curLine.Start);
                                realDist.Add(newLine, dist);
                                if (curNeighb.Count <= 2)
                                    angle = 0;
                                else
                                {
                                    Single crossArmsFactor = 1 + Convert.ToSingle(((curNeighb.Count-2) * 0.1));
                                    angle = crossArmsFactor * AngleForChoiceInSteps(neiMidPt, curLine.Start, midPt); //, Convert.ToSingle(dist));
                                }
                                realAngle.Add(newLine, angle);
                            }
                        }
                    }
                }

                // -- check neighbours of the end point of a line --
                curNeighb = curGraph.GetNeighbors(curLine.End);
                foreach (Vector2d neiPt in curNeighb)
                {
                    //if (! neiPt.Equals(curLine.Start))
                    if (!(neiPt.X == curLine.Start.X && neiPt.Y == curLine.Start.Y))
                    {
                        Vector2d neiMidPt = new Line2D(curLine.End, neiPt).Center; //.End.GetMidPoint(neiPt);
                        if (!(midPt.Equals(neiMidPt)))
                        {
                            UndirectedEdge<Vector2d> newLine = new UndirectedEdge<Vector2d>(midPt, neiMidPt);
                            UndirectedEdge<Vector2d> testNewEdge = newLines.Find(x => x.Source == midPt && x.Target == neiMidPt);
                            UndirectedEdge<Vector2d> testRevNewEdge = newLines.Find(x => x.Source == neiMidPt && x.Target == midPt);
                            if (testNewEdge == null && testRevNewEdge == null)
                            {
                                newLines.Add(newLine);
                                dist = GeoAlgorithms2D.DistancePoints(midPt, curLine.End) +
                                       GeoAlgorithms2D.DistancePoints(neiMidPt, curLine.End);
                                realDist.Add(newLine, dist);
                                if (curNeighb.Count <= 2)
                                    angle = 0;
                                else
                                {
                                    Single crossArmsFactor = 1 + Convert.ToSingle(((curNeighb.Count - 2) *0.1));
                                    angle = crossArmsFactor * AngleForChoiceInSteps(neiMidPt, curLine.End, midPt); //, Convert.ToSingle(dist) );
                                }
                                realAngle.Add(newLine, angle);
                            }
                        }
                    }
                }
            }

            return newLines.ToUndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>();
        }

        //============================================================================================================================================================================
        /* public static Tektosyne.Geometry.Subdivision ConstructInverseGraph(Tektosyne.Geometry.Subdivision curGraph,
             ref Dictionary<LineD, double> realDist, ref Dictionary<LineD, double> realAngle, ref Dictionary<PointD, LineD> inverseEdgesMapping)
         {
             LineD[] curLinesArr = curGraph.ToLines();
             List<LineD> newLines = new List<LineD>();
             inverseEdgesMapping = new Dictionary<PointD, LineD>();
             realDist = new Dictionary<LineD, double>();
             realAngle = new Dictionary<LineD, double>();
             double dist, angle;

             foreach (LineD curLine in curLinesArr)
             {
                 PointD midPt = curLine.Start.GetMidPoint(curLine.End);// GetMidPoint(curLine.Start, curLine.End);
                 IList<PointD> curNeighb = curGraph.GetNeighbors(curLine.Start);
                 foreach (PointD neiPt in curNeighb)
                 {
                     PointD neiMidPt = curLine.Start.GetMidPoint(neiPt);
                     if (!(midPt.Equals(neiMidPt)))
                     {
                         LineD newLine = new LineD(midPt, neiMidPt);
                         //LineD newLineB = new LineD(neiMidPt, midPt);
                         if (!newLines.Contains(newLine))
                         {
                             newLines.Add(newLine);
                             dist = curGraph.GetDistance(midPt, curLine.Start) + curGraph.GetDistance(neiMidPt, curLine.Start);
                             realDist.Add(newLine, dist);
                             angle = GraphTools.AngleForChoice(midPt, curLine.Start, neiMidPt);//, Convert.ToSingle(dist));
                             realAngle.Add(newLine, angle);
                             if (!inverseEdgesMapping.ContainsKey(midPt))
                                 inverseEdgesMapping.Add(midPt, curLine);
                         }
                     }

                 }
                 curNeighb = curGraph.GetNeighbors(curLine.End);
                 foreach (PointD neiPt in curNeighb)
                 {
                     PointD neiMidPt = curLine.End.GetMidPoint(neiPt);
                     if (!(midPt.Equals(neiMidPt)))
                     {
                         LineD newLine = new LineD(midPt, neiMidPt);
                         if (!newLines.Contains(newLine))
                         {
                             newLines.Add(newLine);
                             dist = curGraph.GetDistance(midPt, curLine.End) + curGraph.GetDistance(neiMidPt, curLine.End);
                             realDist.Add(newLine, dist);
                             angle = GraphTools.AngleForChoice(midPt, curLine.End, neiMidPt);//, Convert.ToSingle(dist) );
                             realAngle.Add(newLine, angle);
                             if (!inverseEdgesMapping.ContainsKey(midPt))
                                 inverseEdgesMapping.Add(midPt, curLine);
                         }
                     }
                 }
             }

             LineD[] newLinesArr = newLines.ToArray();
             Tektosyne.Geometry.Subdivision inverseGraph = new Tektosyne.Geometry.Subdivision();
             inverseGraph = Tektosyne.Geometry.Subdivision.FromLines(newLinesArr);
             return inverseGraph;
         }*/

        //============================================================================================================================================================================
        /*public static Tektosyne.Geometry.Subdivision ConstructInverseGraph(UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> curGraph,
            ref Dictionary<LineD, double> realDist, ref Dictionary<LineD, double> realAngle, ref Dictionary<PointD, LineD> inverseEdgesMapping)
        {
            List<LineD> qLines = new List<LineD>();
            foreach (UndirectedEdge<Vector2d> edge in curGraph.Edges) qLines.Add(new LineD(edge.Source.ToPointD(), edge.Target.ToPointD()));

            LineD[] curLinesArr = qLines.ToArray();
            List<LineD> newLines = new List<LineD>();
            inverseEdgesMapping = new Dictionary<PointD, LineD>();
            realDist = new Dictionary<LineD, double>();
            realAngle = new Dictionary<LineD, double>();
            double dist, angle;

            foreach (LineD curLine in curLinesArr)
            {
                PointD midPt = curLine.Start.GetMidPoint(curLine.End);// GetMidPoint(curLine.Start, curLine.End);
                List<Vector2d> tempNeighb = curGraph.GetNeighbors(curLine.Start.ToVector2D());
                List<PointD> curNeighb = new List<PointD>();
                foreach (Vector2d pt in tempNeighb) curNeighb.Add (new PointD(pt.X, pt.Y));
                //IList<PointD> curNeighb = curGraph.GetNeighbors(curLine.Start.ToVector2D() );
                foreach (PointD neiPt in curNeighb)
                {
                    PointD neiMidPt = curLine.Start.GetMidPoint(neiPt);
                    if (!(midPt.Equals(neiMidPt)))
                    {
                        LineD newLine = new LineD(midPt, neiMidPt);
                        //LineD newLineB = new LineD(neiMidPt, midPt);
                        if (!newLines.Contains(newLine))
                        {
                            newLines.Add(newLine);
                            dist = GeoAlgorithms2D.DistancePoints(midPt.ToVector2D(), curLine.Start.ToVector2D()) + GeoAlgorithms2D.DistancePoints(neiMidPt.ToVector2D(), curLine.Start.ToVector2D());
                            realDist.Add(newLine, dist);
                            angle = GraphTools.AngleForChoice(midPt, curLine.Start, neiMidPt);//, Convert.ToSingle(dist));
                            realAngle.Add(newLine, angle);
                            if (!inverseEdgesMapping.ContainsKey(midPt))
                                inverseEdgesMapping.Add(midPt, curLine);
                        }
                    }
                }

                tempNeighb = new List<Vector2d>(); 
                tempNeighb = curGraph.GetNeighbors(curLine.Start.ToVector2D());
                curNeighb = new List<PointD>();
                foreach (Vector2d pt in tempNeighb) curNeighb.Add(new PointD(pt.X, pt.Y));
                //curNeighb = curGraph.GetNeighbors(curLine.End);
                foreach (PointD neiPt in curNeighb)
                {
                    PointD neiMidPt = curLine.End.GetMidPoint(neiPt);
                    if (!(midPt.Equals(neiMidPt)))
                    {
                        LineD newLine = new LineD(midPt, neiMidPt);
                        if (!newLines.Contains(newLine))
                        {
                            newLines.Add(newLine);
                            dist = GeoAlgorithms2D.DistancePoints(midPt.ToVector2D(), curLine.End.ToVector2D()) + GeoAlgorithms2D.DistancePoints(neiMidPt.ToVector2D(), curLine.End.ToVector2D());
                            realDist.Add(newLine, dist);
                            angle = GraphTools.AngleForChoice(midPt, curLine.End, neiMidPt);//, Convert.ToSingle(dist) );
                            realAngle.Add(newLine, angle);
                            if (!inverseEdgesMapping.ContainsKey(midPt))
                                inverseEdgesMapping.Add(midPt, curLine);
                        }
                    }
                }
            }

            LineD[] newLinesArr = newLines.ToArray();
            Tektosyne.Geometry.Subdivision inverseGraph = new Tektosyne.Geometry.Subdivision();
            inverseGraph = Tektosyne.Geometry.Subdivision.FromLines(newLinesArr);
            return inverseGraph;
        }*/

        //==============================================================================================
        public static Single AngleDepth(IList<PointD> path)
        {
            double sum = 0, tmpA = 0;
            PointD thisPt, nextPt, overNextPt, firstVect, scondVect;

            for (int i = path.Count - 1; i > 1; i--)
            {
                thisPt = path[i];
                nextPt = path[i - 1];
                overNextPt = path[i - 2];
                firstVect = nextPt - thisPt;
                scondVect = overNextPt - nextPt;
                tmpA = firstVect.AngleBetween(scondVect);
                tmpA = Math.Abs(Angle.RadiansToDegrees * tmpA);
                if (tmpA > 90 & tmpA <= 180)
                    tmpA = 180 - tmpA;
                else if (tmpA > 180 & tmpA <= 270)
                    tmpA = tmpA - 180; // these values never occurs...?
                else if (tmpA > 270)
                    tmpA = 360 - tmpA;// these values never occurs...?

                sum += tmpA;
            }

            return Convert.ToSingle(sum);
        }

        //==============================================================================================
        public static Single AngleForChoiceSteps(Vector2d firstPt, Vector2d midPt, Vector2d secondPt)//, float dist)
        {
            double tmpA = 0;
            Vector2d firstVect, scondVect;

            firstVect = midPt - firstPt;
            scondVect = secondPt - midPt;
            tmpA = GeoAlgorithms2D.AngleBetweenD(firstVect, scondVect); // firstVect.AngleBetween(scondVect);
            //tmpA = Angle3Points(nextPt, thisPt, overNextPt);
            //tmpA = Math.Abs(Angle.RadiansToDegrees * tmpA);
             if (tmpA > 90 & tmpA <= 180)
                 tmpA = 180 - tmpA;
             else if (tmpA > 180 & tmpA <= 270)
                 tmpA = tmpA - 180; // these values never occurs...?
             else if (tmpA > 270)
                 tmpA = 360 - tmpA;// these values never occurs...?
             //return Convert.ToSingle(tmpA);

            if (tmpA <= 180)
                tmpA = 180 - tmpA;
            else if (tmpA > 180)
                tmpA = tmpA - 180; // these values never occurs...?

            //Single weight = Convert.ToSingle(tmpA);
             Single weight = 0;
             if (tmpA < 22.5) weight = 0.0f;
             else if (tmpA < 60) weight = 0.25f;
             else if (tmpA < 90) weight = 0.5f;
             else if (tmpA < 120) weight = 0.75f;
             else weight = 1.0f;

            return weight;
        }

        //==============================================================================================
        public static Single AngleForChoice(Vector2d firstPt, Vector2d midPt, Vector2d secondPt)//, float dist)
        {
            double tmpA = 0;
            Vector2d firstVect, scondVect;

            firstVect = midPt - firstPt;
            scondVect = secondPt - midPt;

            var dotProd = firstVect.DotProduct(scondVect);
            var lenProd = firstVect.Length * scondVect.Length;
            if (lenProd == 0) return 0;
            var divOperation = dotProd / lenProd;

            tmpA = Math.Abs(Math.Acos(divOperation)) * 180.0 / Math.PI;          
            Single weight = Convert.ToSingle(tmpA);
            return weight;
        }


        public static Single AngleForChoiceInSteps(Vector2d firstPt, Vector2d midPt, Vector2d secondPt)//, float dist)
        {
            double tmpA = 0;
            Vector2d firstVect, scondVect;

            firstVect = midPt - firstPt;
            scondVect = secondPt - midPt;

            var angle = GeoAlgorithms2D.AngleBetweenD(firstVect, scondVect);

            Single weight = 0;
            if (angle < 30) weight = 0.0f;
            else if (angle < 60) weight = 0.25f;
            else if (angle < 90) weight = 0.5f;
            else if (angle < 120) weight = 0.75f;
            else weight = 1.0f;

            return weight;
            //return Convert.ToSingle(angle);
        }

        public static Single AngleForChoiceInRad(Vector2d firstPt, Vector2d midPt, Vector2d secondPt)//, float dist)
        {
            double tmpA = 0;
            Vector2d firstVect, scondVect;

            firstVect = midPt - firstPt;
            scondVect = secondPt - midPt;

            var angle = GeoAlgorithms2D.AngleBetweenR(firstVect, scondVect);

            return Convert.ToSingle(angle);
        }

        //==============================================================================================
        /*      public Single TotalAngleDepth()
              {
                  double sum = 0;
                  IList<T> curPath;// = Nodes(d);
                  foreach (T node in CurGraph.Nodes)
                  {
                      curPath = Nodes(node);
                      sum += curPath.Count - 1; // -1: without counting the start node
                  }
                  return Convert.ToSingle(sum);
              }*/
    }
}


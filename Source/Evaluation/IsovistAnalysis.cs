﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = IsovistAnalysis.cs 
//  Copyright (C) 2014/10/18  1:00 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using CPlan.Geometry;
using CPlan.Isovist2D;
using OpenTK;
using System.Threading.Tasks;


//using CPlan.CommunicateToLucy;

namespace CPlan.Evaluation
{
    [Serializable]
    public class IsovistAnalysis
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Attributes

        private List<Line2D> _obstacleLines = new List<Line2D>();
        //private List<Vector2d> _analysisPoints = new List<Vector2d>();
        private float _precision = 0.02f;
        private IsovistField2D _isovistField;
        private List<Poly2D> _buildings;
        private Poly2D _border;
        private int _resultID = -1;

        //private MqttClient _mqttClient;
        //private ConfigSettings _configInfo;

        //private MqttClient mqttClient;
        //private ConfigSettings _configInfo;

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties
        public List<double> AreaCleaned { get; set; }
        public List<double> AreaDistWeightedCleaned { get; set; }
        public List<double> PerimeterCleaned { get; set; }
        public List<double> CompactnessCleaned { get; set; }
        public List<double> OcclusivityCleaned { get; set; }
        public List<double> MinRadialCleaned { get; set; }
        public List<double> MeanRadialCleaned { get; set; }
        public List<double> MaxRadialCleaned { get; set; }
        public List<double> StdDevRadialsCleaned { get; set; }
        public List<double> VarianceCleaned { get; set; }
        public List<double> SkewnessCleaned { get; set; }
        public List<double> NumberOfOcclusionsCleaned { get; set; }
        public List<double> MeanOcclusivityCleaned { get; set; }
        public List<double> RelativeOcclusivityCleaned { get; set; }
        public List<double> CircularityCleaned { get; set; }
        public List<double> DispersionCleaned { get; set; }
        public List<double> ElongationCleaned { get; set; }
        public List<double> CompactnessBattyCleaned { get; set; }

        // --- Arrays ---
        [Category("Measures"), Description("Area")]
        public double[] Area { get; set; }
        [Category("Measures"), Description("AreaDistWeighted")]
        public double[] AreaDistWeighted { get; set; }
        [Category("Measures"), Description("Compactness")]
        public double[] Compactness { get; set; }
        [Category("Measures"), Description("Perimeter")]
        public double[] Perimeter { get; set; }
        [Category("Measures"), Description("Occlusivity")]
        public double[] Occlusivity { get; set; }
        [Category("Measures"), Description("MinRadial")]
        public double[] MinRadial { get; set; }
        [Category("Measures"), Description("MeanRadial")]
        public double[] MeanRadial { get; set; }
        [Category("Measures"), Description("MaxRadial")]
        public double[] MaxRadial { get; set; }
        [Category("Measures"), Description("StdDevRadials")]
        public double[] StdDevRadials { get; set; }
        [Category("Measures"), Description("Variance")]
        public double[] Variance { get; set; }
        [Category("Measures"), Description("Skewness")]
        public double[] Skewness { get; set; }
        [Category("Measures"), Description("NumberOfOcclusions")]
        public double[] NumberOfOcclusions { get; set; }
        [Category("Measures"), Description("MeanOcclusivity")]
        public double[] MeanOcclusivity { get; set; }
        [Category("Measures"), Description("RelativeOcclusivity")]
        public double[] RelativeOcclusivity { get; set; }
        [Category("Measures"), Description("Circularity")]
        public double[] Circularity { get; set; }
        [Category("Measures"), Description("Dispersion")]
        public double[] Dispersion { get; set; }
        [Category("Measures"), Description("Elongation")]
        public double[] Elongation { get; set; }
        [Category("Measures"), Description("CompactnessBatty")]
        public double[] CompactnessBatty { get; set; }
        [Category("Measures"), Description("White: no measure")]
        public double[] White { get; set; }

        [Category("Settings"), Description("Precision of Isovist Calculation")]
        public float Precision { get { return _precision; } set { _precision = value; } }
        [Category("Settings"), Description("Which Isovist-Field property shall be shown")]
        public string DisplayMeasure { get; set; }
        [Category("Settings"), Description("Size of a cell used for a IsovisField")]
        public float CellSize { get; set; }

        //public List<Dictionary<AnalysisMeasure, double>> Results { get; set; }
        List<Dictionary<AnalysisMeasure, float>> Results { get; set; }
        public List<Vector2d> AnalysisPoints { get; protected set; }
        public List<Byte> FingerprintAnalysisPoints { get; protected set; }

        //----------------------------------------------------------------------
        /// <summary>
        /// Fingerprint of a layout.
        /// Contains e.g. information if a grid cell is overlapping with a building or not.
        /// </summary>
        public List<float> FingerprintGrid { get; protected set; }

        //----------------------------------------------------------------------
        /// <summary>
        /// Number of cells in a line or row of the FingerprintGrid.
        /// </summary>
        public int FingerprintGridLineLgth { get; set; }

        public bool Finished { get; set; }  

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        //============================================================================================================================================================================
        public IsovistAnalysis(List<Vector2d> analysisPoints, List<Line2D> obstacleLines, float cellSize)
        {
            CellSize = cellSize;
            _obstacleLines = obstacleLines;
            AnalysisPoints = analysisPoints;
            DisplayMeasure = "Area";
            SizeArrays();

            Finished = false;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Create a Isovist Analysis instance.
        /// This constructor shall be used when calculating via Lucy!
        /// </summary>
        /// <param name="border"> The border of the Isovist Field.</param>
        /// <param name="buildings"> The buildings polygons, which are the obstacles for the Isovists.</param>
        /// <param name="cellSize"> The cell size of a cell in the Analysis grid.</param>
        public IsovistAnalysis(Poly2D border, List<Poly2D> buildings, float cellSize)
        {
            CellSize = cellSize;
            _buildings = buildings;
            _border = border;
            foreach (Poly2D curPoly in buildings)
            {
                _obstacleLines.AddRange(curPoly.Edges.ToList());
            }
            _obstacleLines.AddRange(border.Edges);

            AnalysisPoints = PointsForIsovistField(border, buildings, cellSize);
            DisplayMeasure = "Area";
            SizeArrays();

            Finished = false;
        }

        //============================================================================================================================================================================
        public IsovistAnalysis(IsovistField2D isovistField, float cellSize)
        {
            CellSize = cellSize;
            _obstacleLines = isovistField.ObstacleLines;
            AnalysisPoints = isovistField.Points;
            _isovistField = isovistField;
            DisplayMeasure = "Area";
            SizeArrays();

            Finished = false;
        }

        //============================================================================================================================================================================
        public IsovistAnalysis(IsovistAnalysis previousField)
        {
            CellSize = previousField.CellSize;
            _buildings = previousField._buildings;
            _border = previousField._border;
            _resultID = previousField._resultID;
            _obstacleLines = previousField._obstacleLines;
            _isovistField = previousField._isovistField;
            DisplayMeasure = previousField.DisplayMeasure;
            Results = previousField.Results;
            FingerprintAnalysisPoints = previousField.FingerprintAnalysisPoints;
            FingerprintGrid = previousField.FingerprintGrid;
            AnalysisPoints = previousField.AnalysisPoints;

            Finished = previousField.Finished;
            SizeArrays();
            FillArrays();
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        //============================================================================================================================================================================
        /// <summary>
        /// Size the arrays
        /// </summary>
        private void SizeArrays()
        {

            Area = new double[AnalysisPoints.Count];
            Compactness = new double[AnalysisPoints.Count];
            Perimeter = new double[AnalysisPoints.Count];
            Occlusivity = new double[AnalysisPoints.Count];
            MinRadial = new double[AnalysisPoints.Count];
            MaxRadial = new double[AnalysisPoints.Count];
            Area = new double[AnalysisPoints.Count];
            AreaDistWeighted = new double[AnalysisPoints.Count];
            Occlusivity = new double[AnalysisPoints.Count];
            MinRadial = new double[AnalysisPoints.Count];
            MeanRadial = new double[AnalysisPoints.Count];
            MaxRadial = new double[AnalysisPoints.Count];
            StdDevRadials = new double[AnalysisPoints.Count];
            Variance = new double[AnalysisPoints.Count];
            Skewness = new double[AnalysisPoints.Count];
            NumberOfOcclusions = new double[AnalysisPoints.Count];
            MeanOcclusivity = new double[AnalysisPoints.Count];
            RelativeOcclusivity = new double[AnalysisPoints.Count];
            Circularity = new double[AnalysisPoints.Count];
            Dispersion = new double[AnalysisPoints.Count];
            Elongation = new double[AnalysisPoints.Count];
            CompactnessBatty = new double[AnalysisPoints.Count];
            White = new double[AnalysisPoints.Count];
        }

        //============================================================================================================================================================================
        public void Calculate(float precision = 0.02f)
        {
            _precision = precision;
            _isovistField = new IsovistField2D(AnalysisPoints, _obstacleLines, _precision);
            _isovistField.Calculate();
            Results = _isovistField.Results;
            //int index = ResultManager.Instance.Count;
            //ResultManager.Instance.AddResult(index, this);
            FillArrays();
        }

        //============================================================================================================================================================================
        public List<Vector2d> PointsForIsovistField(Poly2D field, List<Poly2D> buildings, float cellSize)
        {
            List<Vector2d> result = new List<Vector2d>();
            FingerprintAnalysisPoints = new List<byte>();
            FingerprintGrid = new List<float>();
            CellSize = cellSize;

            FingerprintGridLineLgth = (int)(field.BoundingBox().Width / cellSize);

            int countX = FingerprintGridLineLgth;//(int)(field.BoundingBox().Width / cellSize);
            int countY = (int)(field.BoundingBox().Height / cellSize);
            Vector2d startP = field.BoundingBox().BottomLeft;
            for (int i = 0; i < countX + 1; i++)
            {
                for (int j = 0; j < countY + 1; j++)
                {
                    Vector2d curPos = new Vector2d(startP.X + i * cellSize + cellSize / 2, startP.Y + j * cellSize + cellSize / 2);
                    //m_activeCells[j * m_countX + i] = true;
                    bool active = true;
                    bool inField = false;
                    foreach (Poly2D curPoly in buildings)
                    {
                        if (field.ContainsPoint(curPos))
                        {
                            inField = true;
                        }
                        //if (!curGen.Open)   
                        if (curPoly.ContainsPoint(curPos))
                        {
                            active = false;
                            break;
                        }
                    }

                    if (active)
                    {
                        if (inField)
                        {
                            result.Add(curPos);
                            FingerprintAnalysisPoints.Add(0); // += "\t1";
                        }
                        FingerprintGrid.Add(0);
                    }
                    else
                    {
                        if (inField)
                        {
                            FingerprintAnalysisPoints.Add(1);
                        } 
                        FingerprintGrid.Add(1);
                    }
                }
            }
            return result;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Copy the results into individual arrays.
        /// </summary>
        private async Task FillArrays()
        {
            AreaCleaned = new List<double>();
            AreaDistWeightedCleaned = new List<double>();
            PerimeterCleaned = new List<double>();
            CompactnessCleaned = new List<double>();
            OcclusivityCleaned = new List<double>();
            MinRadialCleaned = new List<double>();
            MeanRadialCleaned = new List<double>();
            MaxRadialCleaned = new List<double>();
            StdDevRadialsCleaned = new List<double>();
            VarianceCleaned = new List<double>();
            SkewnessCleaned = new List<double>();
            NumberOfOcclusionsCleaned = new List<double>();
            MeanOcclusivityCleaned = new List<double>();
            RelativeOcclusivityCleaned = new List<double>();
            CircularityCleaned = new List<double>();
            DispersionCleaned = new List<double>();
            ElongationCleaned = new List<double>();
            CompactnessBattyCleaned = new List<double>();

            if (Results == null)
                return;
            

            Task taskA = Task.Run(() =>
            {

                for (int i = 0; i < Results.Count; i++)
                {
                    Area[i] = Results[i][AnalysisMeasure.Area];
                    if (!double.IsNaN(Area[i]) && Area[i] > 0) AreaCleaned.Add(Area[i]);
                    Compactness[i] = Results[i][AnalysisMeasure.Compactness];
                    if (!double.IsNaN(Compactness[i]) && Compactness[i] > 0) CompactnessCleaned.Add(Compactness[i]);
                    Perimeter[i] = Results[i][AnalysisMeasure.Perimeter];
                    if (!double.IsNaN(Perimeter[i]) && Perimeter[i] > 0) PerimeterCleaned.Add(Perimeter[i]);
                    Occlusivity[i] = Results[i][AnalysisMeasure.Occlusivity];
                    if (!double.IsNaN(Occlusivity[i]) && Occlusivity[i] > 0) OcclusivityCleaned.Add(Occlusivity[i]);
                    MinRadial[i] = Results[i][AnalysisMeasure.MinRadial];
                    if (!double.IsNaN(MinRadial[i]) && MinRadial[i] > 0) MinRadialCleaned.Add(MinRadial[i]);
                    MaxRadial[i] = Results[i][AnalysisMeasure.MaxRadial];
                    if (!double.IsNaN(MaxRadial[i]) && MaxRadial[i] > 0) MaxRadialCleaned.Add(MaxRadial[i]);
                    AreaDistWeighted[i] = Results[i][AnalysisMeasure.AreaDistWeighted];
                    if (!double.IsNaN(AreaDistWeighted[i]) && AreaDistWeighted[i] > 0) AreaDistWeightedCleaned.Add(AreaDistWeighted[i]);
                    MeanRadial[i] = Results[i][AnalysisMeasure.MeanRadial];
                    if (!double.IsNaN(MeanRadial[i]) && MeanRadial[i] > 0) MeanRadialCleaned.Add(MeanRadial[i]);
                    StdDevRadials[i] = Results[i][AnalysisMeasure.StdDevRadials];
                    if (!double.IsNaN(StdDevRadials[i]) && StdDevRadials[i] > 0) StdDevRadialsCleaned.Add(StdDevRadials[i]);
                    Variance[i] = Results[i][AnalysisMeasure.Variance];
                    if (!double.IsNaN(Variance[i]) && Variance[i] > 0) VarianceCleaned.Add(Variance[i]);
                    Skewness[i] = Results[i][AnalysisMeasure.Skewness];
                    if (!double.IsNaN(Skewness[i]) && Skewness[i] > 0) SkewnessCleaned.Add(Skewness[i]);
                    NumberOfOcclusions[i] = Results[i][AnalysisMeasure.NumberOfOcclusions];
                    if (!double.IsNaN(NumberOfOcclusions[i]) && NumberOfOcclusions[i] > 0) NumberOfOcclusionsCleaned.Add(NumberOfOcclusions[i]);
                    MeanOcclusivity[i] = Results[i][AnalysisMeasure.MeanOcclusivity];
                    if (!double.IsNaN(MeanOcclusivity[i]) && MeanOcclusivity[i] > 0) MeanOcclusivityCleaned.Add(MeanOcclusivity[i]);
                    RelativeOcclusivity[i] = Results[i][AnalysisMeasure.RelativeOcclusivity];
                    if (!double.IsNaN(RelativeOcclusivity[i]) && RelativeOcclusivity[i] > 0) RelativeOcclusivityCleaned.Add(RelativeOcclusivity[i]);
                    Circularity[i] = Results[i][AnalysisMeasure.Circularity];
                    if (!double.IsNaN(Circularity[i]) && Circularity[i] > 0) CircularityCleaned.Add(Circularity[i]);
                    Dispersion[i] = Results[i][AnalysisMeasure.Dispersion];
                    if (!double.IsNaN(Dispersion[i]) && Dispersion[i] > 0) DispersionCleaned.Add(Dispersion[i]);
                    Elongation[i] = Results[i][AnalysisMeasure.Elongation];
                    if (!double.IsNaN(Elongation[i]) && Elongation[i] > 0) ElongationCleaned.Add(Elongation[i]);
                    CompactnessBatty[i] = Results[i][AnalysisMeasure.CompactnessBatty];
                    if (!double.IsNaN(CompactnessBatty[i]) && CompactnessBatty[i] > 0) CompactnessBattyCleaned.Add(CompactnessBatty[i]);

                }
                Finished = true;
            });

            await taskA;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Access the measures by a string.
        /// </summary>
        /// <param name="measure">Which measure shall be returned.</param>
        /// <returns>Array of measure values, which are assigned to the analysis points.</returns>
        public double[] GetMeasure(string measure = "")
        {
            if (measure != "") DisplayMeasure = measure;
            switch (DisplayMeasure)
            {
                case "Area":
                    return Area;
                case "Compactness":
                    return Compactness;
                case "Perimeter":
                    return Perimeter;
                case "Occlusivity":
                    return Occlusivity;
                case "MinRadial":
                    return MinRadial;
                case "MaxRadial":
                    return MaxRadial;
                case "White":
                    return White;
                default:
                    return Area;
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////     
        ////////////////////// Calculate via Lucy ////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Lucy
/*
        public string CalculateViaLucy(int id)
        {
            // create scenario

            // create service -> returns mqtt-topic
            //string mqtt_topic = create_service(...);
            string mqtt_topic = "123";
            ServiceManager.AddMqttTopic(mqtt_topic);

            return mqtt_topic;
        }


        //private Communication2Lucy _cl = null;
        private int _curScenarioID = -1;
        //private Communication2Lucy _localCL = null;

        public void CalculateViaLucy(int id)
        {
            //Finished = false;
            //ConnectLucy();
            //_cl = GlobalConnection.CL;
            _localCL = new Communication2Lucy();
            _localCL.connect(GlobalConnection.IP, GlobalConnection.Port);

            CreateIsovistLucyScenario();
            IsovistAnalysisViaLucy(id);
        }

        //============================================================================================================================================================================
        private void CreateIsovistLucyScenario()
        {
            // --- connect --- 
            // --- use lukas & 1234 ---
            string info = "";
            if (GlobalConnection.CL == null)
            {
                InfoNoConnection();
                return;
            }

            NetTopologySuite.Features.FeatureCollection featurecollection;
            System.Collections.ObjectModel.Collection<NetTopologySuite.Features.Feature> features = new System.Collections.ObjectModel.Collection<Feature>();

            AttributesTable buildingAttr = new AttributesTable();
            buildingAttr.AddAttribute("layer", "building");
            AttributesTable boundaryAttr = new AttributesTable();
            boundaryAttr.AddAttribute("layer", "boundary");

            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
            //int index = 0;
            NetTopologySuite.Geometries.Polygon[] polyArray = new NetTopologySuite.Geometries.Polygon[_buildings.Count + 1];
            foreach (Poly2D poly in _buildings)
            {
                LinearRing r1 = new NetTopologySuite.Geometries.LinearRing(GeometryConverter.ToCoordinates(poly));
                NetTopologySuite.Geometries.Polygon p = new NetTopologySuite.Geometries.Polygon(r1);
                //polyArray[index] = p;
                features.Add(new Feature(p, buildingAttr));
                //index++;
            }

            LinearRing r2 = new NetTopologySuite.Geometries.LinearRing(GeometryConverter.ToCoordinates(_border));

            NetTopologySuite.Geometries.Polygon p2 = new NetTopologySuite.Geometries.Polygon(r2);
            //polyArray[index] = p2;
            //NetTopologySuite.Features.Feature feature = new Feature(mPoly, buildingAttr);
            features.Add(new Feature(p2, boundaryAttr));


            //MultiPolygon mPoly = new MultiPolygon(polyArray);

            featurecollection = new FeatureCollection(features);

            //string geoJSONPoly = GeoJSONWriter.Write(mPoly);
            string geoJSONPoly = GeoJSONWriter.Write(featurecollection);
            geoJSONPoly = geoJSONPoly.Substring(0, geoJSONPoly.Length - 1);
            string createScenario4Geo = "{'action':'create_scenario','name':'test','geometry':{'format':'GeoJSON','value':" + geoJSONPoly + "}}}}";

            info += "____________  \r\n";
            info += "--- SEND --- : \r\n";
            info += "sent to lucy: " + " \r\n"; //+ createScenario4Geo

            JObject scenarioObj = GlobalConnection.CL.sendAction2Lucy(createScenario4Geo);

            if (scenarioObj != null)
            {
                JProperty result = ((JProperty)(scenarioObj.First));
                if (result != null)
                {
                    info += "______________  \r\n";
                    info += "--- Result --- : \r\n";
                    info += (result.Name + ": " + result.Value + " \r\n");

                    if (result.Value["ScID"] != null)
                        _curScenarioID = (int)result.Value["ScID"];
                }
            }
            else
                _curScenarioID = -1;
        }

        //============================================================================================================================================================================
        private void IsovistAnalysisViaLucy(int id)
        {
            if (GlobalConnection.CL == null)
            {
                InfoNoConnection();
                return;
            }

            _resultID = id;
            //string info = "";
            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
            //float cellSize = 10;
            //List<Vector2d> gridPoints = PointsForLucyIsovist(_curLayout.Border, _curLayout.Gens, cellSize);
            MultiPoint gridPointsJ = GeometryConverter.ToMultiPoint(AnalysisPoints);

            string geoJSONPoints = GeoJSONWriter.Write(gridPointsJ);

            string msg = "{'action':'create_service','inputs':{'inputVals':{'format':'text','value':'" + geoJSONPoints + "'}},'classname':'Isovist','ScID':'" + _curScenarioID + "'}";

            if (GlobalConnection.CL == null)
            {
                InfoNoConnection();
            }
            else
            {
                _configInfo = GlobalConnection.CL.sendCreateService2Lucy(msg, _curScenarioID);
                //if (_configInfo.mqtt_topic != null)
                //{
                //    if (mqttClient.IsConnected)
                //    {
                //        mqttClient.Subscribe(new string[] { _configInfo.mqtt_topic }, new byte[] { 1 });
                //        mqttClient.Publish(_configInfo.mqtt_topic, Encoding.UTF8.GetBytes("RUN"));
                //    }
                //}
                NotificationBroker.Instance.Register(NotificationEvaluationReadyHandler, _configInfo.mqtt_topic);
                GlobalConnection.CL.startServiceViaMQTT(_configInfo.mqtt_topic, _configInfo.objID);


            }


            //    JObject result = GlobalConnection.CL.sendService2Lucy(msg);

            //    if (result != null)
            //    {
            //        JToken outputs = result.Value<JToken>("outputs");
            //        if (outputs != null)
            //        {
            //            string output = (string)((Newtonsoft.Json.Linq.JProperty)(outputs.First())).Value;
            //            //messageBox.Text += ("result: " + outputs + " - output rows: " + output + "\r\n");

            //            GeoJsonReader GeoJSONReader = new GeoJsonReader();
            //            //IsovistField2DWrapper iso_client_wrapper_result = GeoJSONReader.Read<IsovistField2DWrapper>(output);
            //            IsovistResultsWrapper iso_results_wrapper = GeoJSONReader.Read<IsovistResultsWrapper>(output);

            //            Results = iso_results_wrapper.Results;
            //            FillArrays();

            //            info += ("New results are set! \r\n");
            //        }
            //        else
            //            info += ("result: null \r\n");
            //    }
            //    else
            //        info += ("something went wrong ... \r\n");
            //}
        }

        void NotificationEvaluationReadyHandler(object sender, Dictionary<int, object> userInfo)
        {
            //bool calculated = false;
            if (userInfo != null)
            {
                JObject result = (JObject)userInfo.First().Value;
                if (result != null)
                {
                    JToken outputs = result.Value<JToken>("outputs");
                    if (outputs != null)
                    {
                        string output = (string)(outputs.Value<JObject>("outputVals").Value<String>("value"));

                        GeoJsonReader GeoJSONReader = new GeoJsonReader();
                        IsovistResultsWrapper iso_results_wrapper = GeoJSONReader.Read<IsovistResultsWrapper>(output);

                        Results = iso_results_wrapper.Results;
                        FillArrays();
                    }
                    //calculated = true;
                }
            }
            //int index = ResultManager.Instance.Count;
            //ResultManager.Instance.AddResult(index, calculated);
            ResultManager.Instance.AddResult(_resultID, this);
            Finished = true;
        }

        private string InfoNoConnection()
        {
            return ("No Connection to Lucy established yet! Please connect to Lucy first of all! \r\n");
        }
*/

        # endregion

        # endregion

        public async Task AnalyseLucyResults(string output)
        {
            if (output != null)
            {
                IsovistResultsWrapper iso_results_wrapper = new NetTopologySuite.IO.GeoJsonReader().Read<IsovistResultsWrapper>(output);

                if (iso_results_wrapper != null)
                {
                    Results = iso_results_wrapper.Results;
                    await FillArrays();
                }
            }
        }
    }
}

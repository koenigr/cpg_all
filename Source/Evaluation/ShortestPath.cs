﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = ShortestPath.cs 
//  Copyright (C) 2014/10/18  1:00 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CPlan.Geometry;
using FW_GPU_NET;
using QuickGraph;
using Tektosyne;
using Tektosyne.Collections;
using Tektosyne.Geometry;
using OpenTK;

namespace CPlan.Evaluation
{
    /// <summary>
    /// Computes a shortest path analysis.
    /// <remarks><para>
    /// With this analysis, for each place in a grid total depth for 
    /// metric distances and angular distances (Space Syntax) is computed.
    /// </para></remarks>
    [Serializable]
    public class ShortestPath
    {

        private int _nrRad = 1;

        //############################################################################################################################################################################
        # region Properties

        public readonly LineD[] OrigEdges;
        public readonly PointD[] OrigPoints;

        /// <summary> <see cref="ArrayEx"/> instance to hold the computed metric distance (total depth) for each node </summary>
        private ArrayEx<Single>[] DistMetric;
        /// <summary> <see cref="ArrayEx"/> instance to hold the computed Distance in steps for each node </summary>
        private ArrayEx<Single>[] DistSteps;  // works with wrong values... thus it is useless...
        /// <summary> <see cref="Dictionary"/> instance to hold the counter, how often a node was part of a shortest path (choice).</summary>
        private IList<Single>[] Choice;
        private Dictionary<FW_GPU_NET.Node, Single>[] ChoiceDict;

        public ArrayEx<Single> Connectivity;
        //private Dictionary<Line2D, double> RefValues;
        public bool Connected;

        TimeSpan _calcTime1;
        TimeSpan _calcTime2;
        # endregion

        //############################################################################################################################################################################
        # region Constructors
        /// <summary>
        /// Initialise a new instance of the SubdShortestPath class.
        /// </summary>
        /// <param name="grid">A grid corresponding to a Tektosyne.Geometry.Subdivision.</param>
        /// <param name="refValues">Reference values as weight/cost for the edges. </param>
        public ShortestPath(Tektosyne.Geometry.Subdivision grid)//, Dictionary<Line2D, double> refValues) //FW_GPU_NET.Graph fwGraph, 
        {
            if (grid == null)
                ThrowHelper.ThrowArgumentNullException("grid");
            //OrigGraph = grid;
            OrigEdges = grid.ToLines();
            OrigPoints = grid.Nodes.ToArray();
            //FWGraph = fwGraph;
            //RefValues = refValues;

            //DistMetric = new ArrayEx<Single>(OrigEdges.Length); // OrigEdges insted of OrigGraph... /// OrigGraph.Edges.Count / 2
            //DistSteps = new ArrayEx<Single>(OrigEdges.Length);
            //Choice = new List<Single>();
            //ChoiceDict = new Dictionary<FW_GPU_NET.Node, Single>();
            //Connectivity = new ArrayEx<Single>(OrigEdges.Length);
        }

        //############################################################################################################################################################################
        /// <summary>
        /// Initialise a new instance of the SubdShortestPath class.
        /// </summary>
        /// <param name="inGraph"> Input graph for analysis. </param>
        /// <param name="refValues">Reference values as weight/cost for the edges. </param>
        public ShortestPath(UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> inGraph)//, Dictionary<Line2D, double> refValues) //FW_GPU_NET.Graph fwGraph, 
        {
            if (inGraph == null)
                ThrowHelper.ThrowArgumentNullException("inGraph");
            //OrigGraph = grid;

            List<LineD> curEdges = new List<LineD>();
            foreach (UndirectedEdge<Vector2d> edge in inGraph.Edges) curEdges.Add(new LineD(edge.Source.ToPointD(), edge.Target.ToPointD()));
            OrigEdges = curEdges.ToArray();

            //OrigPoints = inGraph.Vertices.ToArray();
            List<PointD> curPts = new List<PointD>();
            foreach (Vector2d pt in inGraph.Vertices) curPts.Add(new PointD(pt.X, pt.Y));
            OrigPoints = curPts.ToArray();

            //FWGraph = fwGraph;
            //RefValues = refValues;

            //DistMetric = new ArrayEx<Single>(OrigEdges.Length ); // OrigEdges insted of OrigGraph...
            //DistSteps = new ArrayEx<Single>(OrigEdges.Length );
            //Choice = new List<Single>();
            //ChoiceDict = new Dictionary<FW_GPU_NET.Node, Single>();
            //Connectivity = new ArrayEx<Single>(OrigEdges.Length );
        }

        # endregion

        //############################################################################################################################################################################
        # region Methods

        //============================================================================================================================================================================    
        /// <summary>
        /// Computes the shortes pathes for the graph.
        /// </summary>
        /// <remarks><para>
        /// The results of this computation are stored in the DistMetric, Choice and DistAngle <see cref="ArrayEx"/>.
        /// </para></remarks>
        /// <param name="progressBar">For the use of a ToolStripProgressBar.
        /// If there is no ProgressBar use "null" as parameter.</param>
     /*   public void EvaluateOnServer(ToolStripProgressBar progressBar, FW_GPU_NET.Graph originalGraph, FW_GPU_NET.Graph fwGraph)
        {
            GraphProperties measures;
            //Connected = true;
            if (progressBar != null)
            {
                progressBar.Visible = true;
                progressBar.Value = 100;
            }
          
            Console.WriteLine("Press enter to connect to server and call methods...");
            Console.ReadLine();

            DateTime start = DateTime.Now;

            //Create a client that can call methods of Calculator Service that is running on local computer and 10083 TCP port
            //Since IScsServiceClient is IDisposible, it closes connection at the end of the using block
            //using (var client = ScsServiceClientBuilder.CreateClient<IEvaluationService>(new ScsTcpEndPoint("127.0.0.1", 10083)))
            {
                var client = ScsServiceClientBuilder.CreateClient<IGraphEvaluationService>(new ScsTcpEndPoint("127.0.0.1", 10083));
                //Connect to the server
                client.Connect();

                //Call a remote method of server
                //var division = client.ServiceProxy.Divide(42, 3);
                measures = client.ServiceProxy.EvaluateGraph(originalGraph, OrigEdges.ToArray(), fwGraph);

                //Write the result to the screen
                //Console.WriteLine("Result: " + division);
            }

            DistMetric = measures.DistMetric;
            DistSteps = measures.DistSteps;
            Choice = measures.Choice;
            ChoiceDict = measures.ChoiceDict;
            Connectivity = measures.Connectivity;

            DateTime end = DateTime.Now;
            _calcTime1 = end - start;

            Console.WriteLine("Press enter to stop client application");
            Console.ReadLine();   

            start = DateTime.Now;

            // --- handle unconnected graphs ---
            if (DistMetric[0] == float.PositiveInfinity)
            {
                Connected = false;
                for (int i = 0; i < DistMetric.Count; i++)
                {
                    DistMetric[i] = -1;
                    DistSteps[i] = -1;
                }
            }
            else
            {
                Extensions.InvertValues(DistMetric);
                Extensions.InvertValues(DistSteps);
            }

            if (progressBar != null)
            {
                progressBar.Value = 0;
                progressBar.Visible = false;
            }

            end = DateTime.Now; ;
            _calcTime2 = end - start;

            Debug.WriteLine("graph with parents: " + originalGraph.NodeCount.ToString() + " needs: ");
            Debug.WriteLine("my calculation time 1: " + _calcTime1.TotalMilliseconds.ToString() + " ms");
            Debug.WriteLine("my calculation time 2: " + _calcTime2.TotalMilliseconds.ToString() + " ms");
        }
        */

        //============================================================================================================================================================================    
        /// <summary>
        /// Computes the shortes pathes for the graph.
        /// </summary>
        /// <remarks><para>
        /// The results of this computation are stored in the DistMetric, Choice and DistAngle <see cref="ArrayEx"/>.
        /// </para></remarks>
        /// <param name="progressBar">For the use of a ToolStripProgressBar.
        /// If there is no ProgressBar use "null" as parameter.</param>
        public void Evaluate(ToolStripProgressBar progressBar, FW_GPU_NET.Graph originalGraph, FW_GPU_NET.Graph inverseGraph, float[] radiuses = null)
        {
            Connected = true;
            if (progressBar != null)
            {
                progressBar.Visible = true;
                progressBar.Value = 100;
            }
  
            //define the radiuses we want to consider
            //if we want to calculate for the whole graph we use float.MaxValue
            if (radiuses == null)
                radiuses = new float[] { float.MaxValue }; //700, 200 }; // 700, 
            _nrRad = radiuses.Length;

            Connectivity = new ArrayEx<Single>(OrigEdges.Length);

            DistMetric = new ArrayEx<Single>[_nrRad];
            DistSteps = new ArrayEx<Single>[_nrRad];
            Choice = new List<Single>[_nrRad];
            ChoiceDict = new Dictionary<FW_GPU_NET.Node, Single>[_nrRad];

            for (int r = 0; r < _nrRad; r++)
            {
                DistMetric[r] = new ArrayEx<Single>(OrigEdges.Length);
                DistSteps[r] = new ArrayEx<Single>(OrigEdges.Length);
                Choice[r] = new List<Single>();
                ChoiceDict[r] = new Dictionary<FW_GPU_NET.Node, Single>();                
            }

            // -- filter all parents ---
            System.Collections.Generic.List<FW_GPU_NET.Node> parentNodes = new System.Collections.Generic.List<FW_GPU_NET.Node>();
            foreach (FW_GPU_NET.Node testNode in inverseGraph.Nodes)
                if (testNode.Parent == null)
                    parentNodes.Add(testNode);

            // --- reset the counting value -----------------------------
            for (int i = 0; i < Connectivity.Count; i++)
            {
                Connectivity.SetValue(0, i);
            }
            for (int r = 0; r < _nrRad; r++)
            {
                for (int i = 0; i < DistMetric[r].Count; i++)
                {
                    DistMetric[r].SetValue(0, i);
                    DistSteps[r].SetValue(0, i); 
                }

                foreach (FW_GPU_NET.Node curPt in parentNodes) ChoiceDict[r][curPt] = 0f;
            }
            
            DateTime start = DateTime.Now;

            //inverseGraph.Compute(FW_GPU_NET.Graph.ComputeType.GPU);
            //inverseGraph.Compute(FW_GPU_NET.Graph.ComputeType.CPU_FLOYD_WARSHALL);
            inverseGraph.Compute(FW_GPU_NET.Graph.ComputeType.CPU_DIJKSTRA_BASIC);// CPU_DIJKSTRA_PARALLEL);
            //inverseGraph.Compute(FW_GPU_NET.Graph.ComputeType.CPU);
            //graph.ComputeEdgeUsedCounts();

            DateTime end = DateTime.Now;
            _calcTime1 = end - start;

            start = DateTime.Now;

            //Calculate the node positions, for this we need to take the original graph where the nodes correspond to edges
            PointD sPoint;
            PointD ePoint;
            int graphSize = 2 * (int)parentNodes.Count;
            NodePosition[] positions = new NodePosition[parentNodes.Count];
            for (int i = 0; i < graphSize; i += 2)
            {
                sPoint = ((Vector2d)originalGraph.Edges[i].Start.UserData).ToPointD();
                ePoint = ((Vector2d)originalGraph.Edges[i].End.UserData).ToPointD();
                positions[i/2] = new NodePosition(parentNodes[i/2], (sPoint.X + ePoint.X) / 2,(sPoint.Y + ePoint.Y) / 2);                
            }       

            //create the output where we store the node betweenness values for each radius
            List<NodeBetweenness[]> parentNodesPerRadius = new List<NodeBetweenness[]>();
            for (int i = 0; i < radiuses.Length; i++)
            {
                List<NodeBetweenness> betweennessNodes = new List<NodeBetweenness>();
                foreach (Node node in parentNodes)
                {
                    betweennessNodes.Add(new NodeBetweenness(node));
                }
                parentNodesPerRadius.Add(betweennessNodes.ToArray());
            }

            //fwGraph.ComputeEdgeUsedCountsWithChilds(parentNodesPerRadius[0].ToArray(), true);
            //fwGraph.ComputeEdgeUsedCountsWithChilds(parentNodesPerRadius[0].ToArray(), true, positions, 200);            
            inverseGraph.ComputeEdgeUsedCountsWithChilds(parentNodesPerRadius.ToArray(), true, positions, radiuses);
            //inverseGraph.ComputeEdgeUsedCounts();

            //float test = inverseGraph.GetPathLength(parentNodes[0], parentNodes[parentNodes.Count - 1]);
            //test = test * 2;
            //now the parentNodesPerRadius contains two lists of nodes, where the first contains betweennes for radius 700 and second for 200

            for (int i = 0; i < parentNodes.Count; i++)
            {
                FW_GPU_NET.Edge refEdge = (FW_GPU_NET.Edge)parentNodes[i].UserData;
                LineD refLine = new LineD(((Vector2d)refEdge.Start.UserData).ToPointD(), ((Vector2d)refEdge.End.UserData).ToPointD());
                //LineD refLine = new LineD(((PointD)refEdge.Start.UserData), ((PointD)refEdge.End.UserData));
                int origIdx = Array.IndexOf(OrigEdges, refLine); //OrigEdges.IndexOf(refLine);
                if (origIdx == -1)
                {
                    LineD refLineB = refLine.Reverse();
                    origIdx = Array.IndexOf(OrigEdges, refLineB); //OrigEdges.IndexOf(refLineB);
                }
                if (origIdx != -1)
                {
                    Connectivity[origIdx] = parentNodes[i].OutgoingEdges.Count(); // = connectivity
                    Connectivity[origIdx] += parentNodes[i].IncomingEdges.Count();

                    double[] inCentrality = inverseGraph.GetAllPathLengthWithChilds(parentNodes[i], positions, radiuses);
                    inverseGraph.GetPathLength(parentNodes[0], parentNodes[parentNodes.Count - 1]);

                    for (int r = 0; r < _nrRad; r++)
                    {
                        NodeBetweenness[] betweennessNodesTmp = parentNodesPerRadius[r];
                        DistSteps[r][origIdx] = betweennessNodesTmp[i].UsedStepSum;
                        DistMetric[r][origIdx] = (float) inCentrality[r];

                        ChoiceDict[r][parentNodes[origIdx]] = betweennessNodesTmp[i].UsedCount;
                    }
                }
            }

            for (int r = 0; r < _nrRad; r++)
            {
                Choice[r] = ChoiceDict[r].Values.ToList();
                //CollectionsExtend.InvertValues(Choice);
            }

            // --- handle unconnected graphs ---
            float connTester = Fortran.Max(DistMetric[0].ToArray<Single>()); //DistMetric[0];
            if (float.IsInfinity(connTester) || float.IsNegativeInfinity(connTester) || float.IsPositiveInfinity(connTester) || float.IsNaN(connTester))
            {
                Connected = false;
                for (int r = 0; r < _nrRad; r++)
                {
                    for (int i = 0; i < DistMetric[r].Count; i++)
                    {
                        DistMetric[r][i] = -1;
                        DistSteps[r][i] = -1;
                    }
                }
            }
            //double[] inCentrality2 = inverseGraph.GetAllPathLengthWithChilds(parentNodes[0], positions, radiuses);
            //inCentrality2[0] += 1;

            if (progressBar != null)
            {
                progressBar.Value = 0;
                progressBar.Visible = false;
            }

             end = DateTime.Now; ;
            _calcTime2 = end - start;

            Console.WriteLine("graph with parents: " + parentNodes.Count.ToString() + " needs: ");
            Console.WriteLine("1: shortest paths calculation time: " + _calcTime1.TotalMilliseconds.ToString() + " ms");
            Console.WriteLine("2: collecting paths calculation time: " + _calcTime2.TotalMilliseconds.ToString() + " ms");
            //Debug.WriteLine("graph with parents: " + parentNodes.Count.ToString() + " needs: ");
            //Debug.WriteLine("my calculation time 1: " + _calcTime1.TotalMilliseconds.ToString() + " ms");
            //Debug.WriteLine("my calculation time 2: " + _calcTime2.TotalMilliseconds.ToString() + " ms");
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Get the normalised Choice value of all graph elements.
        /// </summary>
        /// <returns>
        /// The normalise Choice values as <see cref="Single[]"/> array.
        /// </returns>
        public Single[][] GetNormChoiceArray()
        {
            Single[][] tempNormChoice = new Single[_nrRad][];

            for (int r = 0; r < _nrRad; r++)
            {
                tempNormChoice[r] = Choice[r].ToArray();
                //Single minV = Fortran.Min(tempChoice.ToArray<Single>());
                //Tools.CutMinValue(tempChoice, Fortran.Min(tempChoice.ToArray<Single>()));
                Tools.Normalize(tempNormChoice[r]);
                Tools.IntervalExtension(tempNormChoice[r], Fortran.Max(tempNormChoice[r].ToArray<Single>()));
            }
            if ((OrigEdges.Length) != tempNormChoice[0].Length)
                return null;
            return tempNormChoice;
        }

        /// <summary>
        /// Get the Choice values.
        /// </summary>
        /// <returns></returns>
        public Single[][] GetChoice()
        {
            Single[][] tempChoice = new Single[_nrRad][];
            for (int r = 0; r < _nrRad; r++)
            {
                tempChoice[r] = Choice[r].ToArray();
            }
            return tempChoice;
        }

        /// <summary>
        /// Get the CentralityStepsvalues.
        /// </summary>
        /// <returns></returns>
        public Single[][] GetCentralitySteps()
        {
            Single[][] tempDistSteps = new Single[_nrRad][];
            for (int r = 0; r < _nrRad; r++)
            {
                tempDistSteps[r] = DistSteps[r].ToArray();
            }
            return tempDistSteps;
        }

        /// <summary>
        /// Get the CentralityMetric values.
        /// </summary>
        /// <returns></returns>
        public Single[][] GetCentralityMetric()
        {
            Single[][] tempCentralityMetric = new Single[_nrRad][];
            for (int r = 0; r < _nrRad; r++)
            {
                tempCentralityMetric[r] = DistMetric[r].ToArray();
            }
            return tempCentralityMetric;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Get the normalised centrality value (steps) of all graph elements.
        /// </summary>
        /// <returns>
        /// The normalise step distances as <see cref="Single[]"/> array.
        /// </returns>
        public Single[][] GetNormCentralityStepsArray()
        {  
            Single[][] tempDistSteps = new Single[_nrRad][];

            for (int r = 0; r < _nrRad; r++)
            {
                //Extensions.InvertValues(DistSteps[r]);
                tempDistSteps[r] = DistSteps[r].ToArray();
                Tools.Normalize(tempDistSteps[r]);
                Tools.IntervalExtension(tempDistSteps[r], Fortran.Max(tempDistSteps[r].ToArray<Single>()));
            }
            return tempDistSteps;
        }

        /// <summary>
        /// Get the normalised centrality value (metric) of all graph elements.
        /// </summary>
        /// <returns>
        /// The normalise metric centrality values as <see cref="Single[]"/> array.
        /// </returns>
        public Single[][] GetNormCentralityMetricArray()
        {
            Single[][] tempDistMetric = new Single[_nrRad][];

            for (int r = 0; r < _nrRad; r++)
            {
                //Extensions.InvertValues(DistMetric[r]);
                tempDistMetric[r] = DistMetric[r].ToArray();
                Tools.CutMinValue(tempDistMetric[r], Fortran.Min(tempDistMetric[r].ToArray<Single>()));
                Tools.Normalize(tempDistMetric[r]);
                Tools.IntervalExtension(tempDistMetric[r], Fortran.Max(tempDistMetric[r].ToArray<Single>()));
            }
            if (Single.IsNaN(tempDistMetric[0][0]))
                tempDistMetric = null;
            return tempDistMetric;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Get the normalised Connectivity values of all graph elements.
        /// </summary>
        /// <returns>
        /// The normalise connectivity values as <see cref="Single[]"/> array.
        /// </returns>
        public Single[] GetNormConnectivityArray()
        {
            Single[] tempConnectivity = Connectivity.ToArray();
            Tools.Normalize(tempConnectivity);
            Tools.IntervalExtension(tempConnectivity, Fortran.Max(tempConnectivity.ToArray<Single>()));
            return tempConnectivity.ToArray();
        }

        # endregion

    }
}

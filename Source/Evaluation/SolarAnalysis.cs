﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = SolarAnalysis.cs 
//  Copyright (C) 2014/10/18  1:00 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Windows.Forms;
using CPlan.Geometry;
using OpenTK;

namespace CPlan.Evaluation
{
    [Serializable]
    public class SolarAnalysis
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Attributes

        private static DirectSunLightNet.CDirectSunLightNet _directSunlight;
        private IEnumerable<Vector3d> _gridPoints;
        private IEnumerable<Vector3d> _gridPointNormales;
        private double[] _results;
        private DirectSunLightNet.CalculationMode _lastMode;
        
        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties
        public double[] Results { get { return _results; } }
        public IEnumerable<Vector3d> GridPoints { get { return _gridPoints; } }
        public IEnumerable<Vector3d> GridNormales { get { return _gridPointNormales; } }
        public DirectSunLightNet.CalculationMode LastMode { get { return _lastMode; } }
        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        // TODO: make everything in terms of IEnumberable and remove element-by-element
        public SolarAnalysis(IEnumerable<Vector3d> gridPoints, IEnumerable<Vector3d> gridPointNormales)
        {
            _gridPoints = gridPoints;
            _gridPointNormales = gridPointNormales;
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        public void InitDirectSunlight()
        {
            if (_directSunlight == null)
            {
                _directSunlight = new DirectSunLightNet.CDirectSunLightNet();
                _directSunlight.SetResolution(64);

                
                string pathToHdriFile = Path.GetFullPath(Environment.CurrentDirectory + "..\\..\\..\\..\\..\\Various\\HDRI\\HDRISky_2015_1Year_Singapore.hdr");//"CA Cumulative Sky - Year 2012 23 Minute step.hdr");
                if (! File.Exists(pathToHdriFile))
                {
                    //pathToHdriFile = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "\\CA Cumulative Sky - Year 2012 23 Minute step.hdr"); ;
                    pathToHdriFile = Path.GetFullPath(Environment.CurrentDirectory + "\\CA Cumulative Sky - Year 2012 23 Minute step.hdr"); 
                    if (!File.Exists(pathToHdriFile))
                    {
                        pathToHdriFile = Path.GetFullPath(Environment.CurrentDirectory + "\\data\\HDRI\\CA Cumulative Sky - Year 2012 23 Minute step.hdr");
                        if (!File.Exists(pathToHdriFile))
                        {
                            MessageBox.Show("There is no valid path to a HDRI file!");
                        }
                    }
                }

                //_directSunlight.SetHDRImage("../../../../Various/HDRI/CA Cumulative Sky - Year 2012 23 Minute step.hdr");
                _directSunlight.SetHDRImage(pathToHdriFile);
                _directSunlight.SetCalculationMode(DirectSunLightNet.CalculationMode.illuminance_klux);
                _directSunlight.PrepareHDRCubeMap(0.0f);
                _directSunlight.StartOpenCLOnceAndForever();
            }
        }

        public void Calculate(DirectSunLightNet.CalculationMode mode)
        {
            _lastMode = mode;

            InitDirectSunlight();

            _directSunlight.SetCalculationMode(mode);
            _directSunlight.SetNormale(new CGeom3d.Vec_3d(0, 0, 1));

            // do the calculation
            _results = new double[_gridPoints.Count()];
            CGeom3d.Vec_3d[] positions = _gridPoints.ToGeom3d().ToArray();
            CGeom3d.Vec_3d[] normales = _gridPointNormales.ToGeom3d().ToArray();
            //CGeom3d.Vec_3d[] positions = new CGeom3d.Vec_3d[_gridPoints.Length];
            //foreach (Vector3d[] v3d in _gridPoints)
            //    positions[pos++] = new CGeom3d.Vec_3d(v3d.X, v3d.Y, v3d.Z);
            //CGeom3d.Vec_3d[] normales = new CGeom3d.Vec_3d[_gridPoints.Length];
            //pos = 0;
            //foreach (Vector3d v3d in _gridPointNormales)
            //    normales[pos++] = new CGeom3d.Vec_3d(v3d.X, v3d.Y, v3d.Z);
            float[] erg = new float[_results.Length * 3];
            _directSunlight.CalculateDirectSunLight(positions, normales, ref erg);
            for (int i = 0; i < _results.Length; i++) // TODO: it was ++i . What is correct? Just have no Idea!
            {
                float r = erg[i * 3];
                float g = erg[i * 3 + 1];
                float b = erg[i * 3 + 2];

                _results[i] = Math.Sqrt(r * r + g * g + b * b);
            }
        }

        public void ClearGeometry()
        {
            InitDirectSunlight();
            _directSunlight.DeleteAllGeometry();
        }

        public uint AddPolygon(CGeom3d.Vec_3d[][] pvv, float transparency)
        {
            InitDirectSunlight();
            return (uint)_directSunlight.AddPolygon(pvv, transparency);
        }

        # endregion
    }
}

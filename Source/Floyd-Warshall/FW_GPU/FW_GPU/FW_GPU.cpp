// FW_GPU.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include "stdafx.h"
#include "Floyd_Warshall.h"

#include <stdio.h>
#include <conio.h>
#include <fstream>
#include <iostream>
#include "Timer.h"

void DefineExampleGraph(FW::Graph &graph)
{
	FW::Node* node[6];

	for(int i = 0; i < 6; ++i)
		node[i] = graph.AddNode();
#if 1
	graph.AddDirectedEdge(node[0], node[1], 4);
	graph.AddDirectedEdge(node[0], node[3], 5);
	graph.AddDirectedEdge(node[0], node[4], 5);
	graph.AddDirectedEdge(node[1], node[2], 7);
	graph.AddDirectedEdge(node[1], node[3], 3);
	graph.AddDirectedEdge(node[2], node[5], 4);
	graph.AddDirectedEdge(node[3], node[2], 3);
	graph.AddDirectedEdge(node[3], node[4], 4);
	graph.AddDirectedEdge(node[3], node[5], 3);
	graph.AddDirectedEdge(node[3], node[0], 7);
	graph.AddDirectedEdge(node[4], node[0], 2);
	graph.AddDirectedEdge(node[4], node[3], 6);
	graph.AddDirectedEdge(node[5], node[3], 2);
	graph.AddDirectedEdge(node[5], node[4], 1);
#endif
#if 0
	graph.AddDirectedEdge(node[0], node[1], 1);
	graph.AddDirectedEdge(node[1], node[2], 1);
	graph.AddDirectedEdge(node[2], node[3], 1);
	graph.AddDirectedEdge(node[3], node[4], 1);
	graph.AddDirectedEdge(node[4], node[5], 1);
	graph.AddDirectedEdge(node[5], node[0], 1);
#endif
#if 0
	graph.AddDirectedEdge(node[1], node[0], 1);
	graph.AddDirectedEdge(node[2], node[1], 1);
	graph.AddDirectedEdge(node[3], node[2], 1);
	graph.AddDirectedEdge(node[4], node[3], 1);
	graph.AddDirectedEdge(node[5], node[4], 1);
	graph.AddDirectedEdge(node[0], node[5], 1);
#endif
}

void PrintMatrix(FW::Graph &graph)
{
	FW::Graph::matrixElement *pM = graph.GetMatrix();
	FW::Graph::matrixElementNext *pNM = graph.GetNextMatrix();
	size_t node_count = graph.GetNodeCount();
	float max = std::numeric_limits<float>::max();

	for(size_t y = 0; y < node_count; ++y)
	{
		for(size_t x = 0; x < node_count; ++x, ++pM, ++pNM)
		{
			if (pM->m_fCost < max)
			{
				int last_id = pNM->m_iNext;
				printf("%.0f %d\t", pM->m_fCost, last_id);
			}
			else
				printf("-\t");
		}
		printf("\n");
	}
	printf("\n");
}

void LoadGraph(const char * filename, FW::Graph &graph)
{
	std::fstream f;
	char cstring[256];
	f.open(filename, std::ios::in);
	int node_count, edge_count;

	f.getline(cstring, sizeof(cstring));
	sscanf_s(cstring, "%d Nodes", &node_count);
	std::cout << cstring << std::endl;

	f.getline(cstring, sizeof(cstring));
	sscanf_s(cstring, "%d Edges", &edge_count);
	std::cout << cstring << std::endl;

	f.getline(cstring, sizeof(cstring));
	std::cout << cstring << std::endl;

	FW::Node** ppNode = (FW::Node **)malloc(sizeof(FW::Node *) * node_count);

	for(int i = 0; i < node_count; ++i)
		ppNode[i] = graph.AddNode();

	while (!f.eof())
	{
		int start, end;
		float weight;

		f.getline(cstring, sizeof(cstring));
		int read = sscanf_s(cstring, "%d, %d, %f", &start, &end, &weight);
		//std::cout << cstring << std::endl;
		if ((read == 3) && (start != end))
			graph.AddEdge(ppNode[start], ppNode[end], weight);
	}
	f.close();

	free( ppNode );
}

/*
int _tmain(int argc, _TCHAR* argv[])
{
	FW::Graph graph;

	//DefineExampleGraph(graph);
	Timer *pTimer = new Timer("LoadGraph");
	//LoadGraph("../../Graphen/GraphL.txt", graph);
	LoadGraph("../../Graphen/GraphS.txt", graph);

	printf("\n%d nodes and %d edges loaded for Floyd-Warshall\n", graph.GetNodeCount(), graph.GetEdgeCount());
	
	//CPU OpenMP start

	delete pTimer;
	pTimer = new Timer("--------------------- CPU OpenMP total");

	int counter(1);
	graph.InitMatrix();
	while(graph.IterateMatrix()) ++counter;
	printf("IterateMatrix CPU took %d iterations\n", counter);
	//CPU OpenMP end

	delete pTimer;
	pTimer = new Timer("--------------------- GPU OpenCL total");

	//GPU OpenCL start
	graph.InitMatrix();
	int counter2 = graph.ComputeMatrixOpenCL();
	printf("IterateMatrix GPU took %d iterations\n", counter2);
	//GPU OpenCL end

	delete pTimer;
	pTimer = new Timer("ComputeEdgeUsedCounts");

	graph.ComputeEdgeUsedCounts();

	delete pTimer;
	pTimer = NULL;

	//FW::EdgeVector path;
	//graph.GetPath(2, 1, path);

	//PrintMatrix(graph);

	printf("\npress any key to continue ...");
	_getch();
	return 0;
}
*/

int _tmain(int argc, _TCHAR* argv[])
{
	FW::Graph * pGraph = NULL;

	for(int i = 0; i < 500; ++i)
	{
		pGraph = new FW::Graph();
		delete pGraph;
		pGraph = NULL;
	}

	pGraph = new FW::Graph();

	LoadGraph("../../Graphen/GraphS.txt", *pGraph);
	pGraph->InitMatrix();
	int counter2 = pGraph->ComputeMatrixOpenCL();
	
	delete pGraph;
	printf("\npress any key to continue ...");
	_getch();
	return 0;
}

#include "Floyd_Warshall.h"
#include "Timer.h"

//#define _TIME_DEBUG

using namespace FW;

void Node::AddEdge(Edge * pEdge, bool outgoing)
{
	if (outgoing)
		m_vOutgoingEdges.insert( NodeEdgePair(pEdge->m_pEnd, pEdge) );
	else
		m_vIncomingEdges.insert( NodeEdgePair(pEdge->m_pStart, pEdge) );
}

void Node::RemoveEdge(Edge * pEdge, bool outgoing)
{
	if (outgoing)
		m_vOutgoingEdges.erase( pEdge->m_pEnd );
	else
		m_vIncomingEdges.erase( pEdge->m_pStart );
}

Edge * Node::FindEdge(Node * pEnd) const
{

	Edge *pResult(NULL);

	NodeEdgeMap::const_iterator found = m_vOutgoingEdges.find(pEnd);
	if (found != m_vOutgoingEdges.end())
		pResult = (*found).second; 
/*
	EdgeSet::iterator l_edge, e_edge;

	l_edge = m_vOutgoingEdges.begin();
	e_edge = m_vOutgoingEdges.end();
	for(; l_edge!=e_edge; l_edge++)
	{
		if ((*l_edge)->m_pEnd == pEnd)
		{
			pResult = *l_edge;
			break;
		}
	}
*/
	return pResult;
}

//#ifdef _DEBUG
//inline void CheckOpenCLError(cl_int clerr)
//{
//	if (clerr == CL_SUCCESS)
//		return;
//	else
//	{
//		int stop = 0;
//		switch (clerr)
//		{
//		case CL_INVALID_PLATFORM:              stop = 1;      assert(0); break;
//		case CL_INVALID_VALUE:                 stop = 2;      assert(0); break;
//		case CL_DEVICE_NOT_AVAILABLE:          stop = 3;      assert(0); break;
//		case CL_DEVICE_NOT_FOUND:              stop = 4;      assert(0); break;
//		case CL_OUT_OF_HOST_MEMORY:            stop = 5;      assert(0); break;
//		case CL_INVALID_DEVICE_TYPE:           stop = 6;      assert(0); break;
//		case CL_INVALID_PROGRAM_EXECUTABLE:    stop = 7;      assert(0); break;
//		case CL_INVALID_COMMAND_QUEUE:         stop = 8;      assert(0); break;
//		case CL_INVALID_KERNEL:                stop = 9;      assert(0); break;
//		case CL_INVALID_CONTEXT:               stop = 10;     assert(0); break;
//		case CL_INVALID_KERNEL_ARGS:           stop = 11;     assert(0); break;
//		case CL_INVALID_WORK_DIMENSION:        stop = 12;     assert(0); break;
//		case CL_INVALID_WORK_GROUP_SIZE:       stop = 13;     assert(0); break;
//		case CL_INVALID_WORK_ITEM_SIZE:        stop = 14;     assert(0); break;
//		case CL_INVALID_GLOBAL_OFFSET:         stop = 15;     assert(0); break;
//		case CL_OUT_OF_RESOURCES:              stop = 16;     assert(0); break;
//		case CL_MEM_OBJECT_ALLOCATION_FAILURE: stop = 17;     assert(0); break;
//		case CL_INVALID_EVENT_WAIT_LIST:       stop = 18;     assert(0); break;
//		case CL_INVALID_PROGRAM:               stop = 19;     assert(0); break;
//		case CL_INVALID_DEVICE:                stop = 20;     assert(0); break;
//		case CL_INVALID_BINARY:                stop = 21;     assert(0); break;
//		case CL_INVALID_BUILD_OPTIONS:         stop = 22;     assert(0); break;
//		case CL_INVALID_OPERATION:             stop = 23;     assert(0); break;
//		case CL_COMPILER_NOT_AVAILABLE:        stop = 24;     assert(0); break;
//		case CL_BUILD_PROGRAM_FAILURE:         stop = 25;     assert(0); break;
//		case CL_INVALID_ARG_INDEX:             stop = 27;     assert(0); break; //if arg_index is not a valid argument index.
//		case CL_INVALID_ARG_VALUE:             stop = 28;     assert(0); break; //if arg_value specified is NULL for an argument that is not declared with the __local qualifier or vice-versa.
//		case CL_INVALID_MEM_OBJECT:            stop = 29;     assert(0); break; //for an argument declared to be a memory object when the specified arg_value is not a valid memory object.
//		case CL_INVALID_SAMPLER:               stop = 30;     assert(0); break; //for an argument declared to be of type sampler_t when the specified arg_value is not a valid sampler object.
//		case CL_INVALID_ARG_SIZE:              stop = 31;     assert(0); break; //if arg_size does not match the size of the data type for an argument that is not a memory object or if the argument is a memory object and arg_size != sizeof(cl_mem) or if arg_size is zero and the argument is declared with the __local qualifier or if the argument is a sampler and arg_size != sizeof(cl_sampler)
//		default:                               stop = 9999;   assert(0); break;
//		}
//	}
//}
//#else
//inline void CheckOpenCLError(cl_int clerr) 
//{
//#ifdef _DEBUG_CONSOLE
//	if (clerr == CL_SUCCESS)
//		return;
//	else
//	{
//		switch (clerr)
//		{
//		case CL_INVALID_PLATFORM:              printf("CL_INVALID_PLATFORM\n");                   break;
//		case CL_INVALID_VALUE:                 printf("CL_INVALID_VALUE\n");                      break;
//		case CL_DEVICE_NOT_AVAILABLE:          printf("CL_DEVICE_NOT_AVAILABLE\n");               break;
//		case CL_DEVICE_NOT_FOUND:              printf("CL_DEVICE_NOT_FOUND\n");                   break;
//		case CL_OUT_OF_HOST_MEMORY:            printf("CL_OUT_OF_HOST_MEMORY\n");                 break;
//		case CL_INVALID_DEVICE_TYPE:           printf("CL_INVALID_DEVICE_TYPE\n");                break;
//		case CL_INVALID_PROGRAM_EXECUTABLE:    printf("CL_INVALID_PROGRAM_EXECUTABLE\n");         break;
//		case CL_INVALID_COMMAND_QUEUE:         printf("CL_INVALID_COMMAND_QUEUE\n");              break;
//		case CL_INVALID_KERNEL:                printf("CL_INVALID_KERNEL\n");                     break;
//		case CL_INVALID_CONTEXT:               printf("CL_INVALID_CONTEXT\n");                    break;
//		case CL_INVALID_KERNEL_ARGS:           printf("CL_INVALID_KERNEL_ARGS\n");                break;
//		case CL_INVALID_WORK_DIMENSION:        printf("CL_INVALID_WORK_DIMENSION\n");             break;
//		case CL_INVALID_WORK_GROUP_SIZE:       printf("CL_INVALID_WORK_GROUP_SIZE\n");            break;
//		case CL_INVALID_WORK_ITEM_SIZE:        printf("CL_INVALID_WORK_ITEM_SIZE\n");             break;
//		case CL_INVALID_GLOBAL_OFFSET:         printf("CL_INVALID_GLOBAL_OFFSET\n");              break;
//		case CL_OUT_OF_RESOURCES:              printf("CL_OUT_OF_RESOURCES\n");                   break;
//		case CL_MEM_OBJECT_ALLOCATION_FAILURE: printf("CL_MEM_OBJECT_ALLOCATION_FAILURE\n");      break;
//		case CL_INVALID_EVENT_WAIT_LIST:       printf("CL_INVALID_EVENT_WAIT_LIST\n");            break;
//		case CL_INVALID_PROGRAM:               printf("CL_INVALID_PROGRAM\n");                    break;
//		case CL_INVALID_DEVICE:                printf("CL_INVALID_DEVICE\n");                     break;
//		case CL_INVALID_BINARY:                printf("CL_INVALID_BINARY\n");                     break;
//		case CL_INVALID_BUILD_OPTIONS:         printf("CL_INVALID_BUILD_OPTIONS\n");              break;
//		case CL_INVALID_OPERATION:             printf("CL_INVALID_OPERATION\n");                  break;
//		case CL_COMPILER_NOT_AVAILABLE:        printf("CL_COMPILER_NOT_AVAILABLE\n");             break;
//		case CL_BUILD_PROGRAM_FAILURE:         printf("CL_BUILD_PROGRAM_FAILURE\n");              break;
//		case CL_INVALID_ARG_INDEX:             printf("CL_INVALID_ARG_INDEX\n");                  break; //if arg_index is not a valid argument index.
//		case CL_INVALID_ARG_VALUE:             printf("CL_INVALID_ARG_VALUE\n");                  break; //if arg_value specified is NULL for an argument that is not declared with the __local qualifier or vice-versa.
//		case CL_INVALID_MEM_OBJECT:            printf("CL_INVALID_MEM_OBJECT\n");                 break; //for an argument declared to be a memory object when the specified arg_value is not a valid memory object.
//		case CL_INVALID_SAMPLER:               printf("CL_INVALID_SAMPLER\n");                    break; //for an argument declared to be of type sampler_t when the specified arg_value is not a valid sampler object.
//		case CL_INVALID_ARG_SIZE:              printf("CL_INVALID_ARG_SIZE\n");                   break; //if arg_size does not match the size of the data type for an argument that is not a memory object or if the argument is a memory object and arg_size != sizeof(cl_mem) or if arg_size is zero and the argument is declared with the __local qualifier or if the argument is a sampler and arg_size != sizeof(cl_sampler)
//		default:                               printf("CL_UNKNOWN_ERROR %d\n", clerr);            break;
//		}
//	}
//#endif
//}
//#endif
//
//void OpenCL::InitOpenCL()
//{
//#ifdef _TIME_DEBUG
//	Timer TotalTimer("InitOpenCL()"), *pTimer = new Timer("OpenCL - Context und Queue erstellen");
//#endif
//
//	cl_int clerr = CL_SUCCESS;
//
//	cl_uint num_platforms;
//	cl_platform_id platform_pick;
//	m_platforms = NULL;
//	clerr = clGetPlatformIDs(0, NULL, &num_platforms);
//	CheckOpenCLError(clerr);
//	if (num_platforms > 0)
//	{
//		m_platforms = (cl_platform_id *)malloc(sizeof(cl_platform_id) * num_platforms);
//		clerr = clGetPlatformIDs(num_platforms, m_platforms, NULL);
//		CheckOpenCLError(clerr);
//		platform_pick = m_platforms[GetPlatformPickId()];
//	}
//	else
//		assert(0);
//
//	cl_context_properties props[3];
//	props[0] = (cl_context_properties)CL_CONTEXT_PLATFORM;   // indicates that next element is platform
//	props[1] = (cl_context_properties)platform_pick;         // platform_pick is of type cl_platform_id
//	props[2] = (cl_context_properties)0;                     // last element must be 0
//
//	m_clctx = new cl_context();
//	*m_clctx = clCreateContextFromType(props, CL_DEVICE_TYPE_ALL, NULL, NULL, &clerr);
//	CheckOpenCLError(clerr);
//
//	size_t paramz;
//	clerr = clGetContextInfo(*m_clctx, CL_CONTEXT_DEVICES, 0, NULL, &paramz);
//	CheckOpenCLError(clerr);
//
//	cl_device_id * cldevs = (cl_device_id *)malloc(paramz);
//	clerr = clGetContextInfo(*m_clctx, CL_CONTEXT_DEVICES, paramz, cldevs, NULL);
//	CheckOpenCLError(clerr);
//
//	m_clcmdq = new cl_command_queue();
//	*m_clcmdq = clCreateCommandQueue(*m_clctx, cldevs[0], 0, &clerr);
//	CheckOpenCLError(clerr);
//
//#ifdef _TIME_DEBUG
//	delete pTimer;
//	pTimer = new Timer("OpenCL - Programm und Kernel erstellen");
//#endif
//
//	//Programm und Kernel erstellen
//	const char * source_code = 
//		"__kernel void floyd_warshall(__global float *pMatrix, __global int *pNextMatrix, unsigned int a, unsigned int n)\n"
//		"{\n" 
//		"	unsigned int col = get_global_id(0);"
//		"	\n"
//		"	if (col >= n)\n"
//		"		return;\n"
//		"	\n"
//		"	unsigned int row = get_global_id(1);"
//		"	\n"
//		"	float part1 = pMatrix[a * n + col];\n"
//		"	if (part1 == MAXFLOAT)\n"
//		"		return;\n"
//		"	\n"
//		"	float part2 = pMatrix[row * n + a];\n"
//		"	if (part2 == MAXFLOAT)\n"
//		"		return;\n"
//		"	\n"
//		"	unsigned int arrayIndex = row * n + col;\n"
//		"	float maybeBetter = part1 + part2;\n"
//		"	\n"
//		"   if (maybeBetter < pMatrix[arrayIndex])\n"
//		"	{\n"
//		"		pMatrix[arrayIndex] = maybeBetter;\n"
//		"		pNextMatrix[arrayIndex] = a;\n"
//		"	}\n"
//		"}\n"; 
//	char clcompileflags[4096];
//	sprintf_s(clcompileflags, "-cl-fast-relaxed-math -cl-single-precision-constant -cl-denorms-are-zero -cl-mad-enable");
//
//	m_clpgm = new cl_program();
//	*m_clpgm = clCreateProgramWithSource(*m_clctx, 1, &source_code, NULL, &clerr);
//	CheckOpenCLError(clerr);
//	clerr = clBuildProgram(*m_clpgm, 0, NULL, clcompileflags, NULL, NULL);
//	CheckOpenCLError(clerr);
//
//	m_clkern = new cl_kernel();
//	*m_clkern = clCreateKernel(*m_clpgm, "floyd_warshall", &clerr);
//	CheckOpenCLError(clerr);
//
//#ifdef _TIME_DEBUG
//	delete pTimer;
//	pTimer = NULL;
//#endif
//
//	free( cldevs );
//}
//
//void Graph::InitOpenCL()
//{
//	if (m_bOpenCLInited)
//		return;
//
//	OpenCL * pOpenCL = OpenCL::GetOpenCL();
//
//	m_bOpenCLInited = true;
//}
//
//void OpenCL::QuitOpenCL()
//{
//#ifdef _TIME_DEBUG
//	Timer *pTimer = new Timer("OpenCL - alles wieder freigeben");
//#endif
//
//	//alles wieder freigeben
//	free(m_platforms);
//
//	if (*m_clkern) clReleaseKernel(*m_clkern); 
//	if (*m_clpgm)  clReleaseProgram(*m_clpgm);
//	if (*m_clcmdq) clReleaseCommandQueue(*m_clcmdq);
//	if (*m_clctx)  clReleaseContext(*m_clctx);
//
//	delete m_clkern;
//	delete m_clpgm;
//	delete m_clcmdq;
//	delete m_clctx;
//
//#ifdef _TIME_DEBUG
//	delete pTimer;
//#endif
//}
//
//void Graph::QuitOpenCL()
//{
//	if (!m_bOpenCLInited)
//		return;
//
//	m_bOpenCLInited = false;
//}
//
//int Graph::ComputeMatrixOpenCL()
//{
//	m_computeType = ComputeType::CPU_FLOYD_WARSHALL;
//	if (!m_bOpenCLInited)
//	{
//		assert(0);
//		return 0;
//	}
//
//	OpenCL * pOpenCL = OpenCL::GetOpenCL();
//
//	int iterations(0);
//
//#ifdef _TIME_DEBUG
//	Timer TotalTimer("ComputeMatrixOpenCL()");
//	Timer * pTimer = new Timer("OpenCL - Buffer erstellen");
//#endif
//
//	if (!m_pMatrix || !m_pNextMatrix) 
//		assert(0);
//
//	size_t node_count = m_vNodes.size();
//
//	const int itemsize_x = 256;
//	const int itemsize_y = 1;
//	const int blocksize_x = (int)ceil((double)node_count / (double)itemsize_x);
//	const int blocksize_y = (int)ceil((double)node_count / (double)itemsize_y);
//
//	size_t total_array_size = node_count * node_count;
//
//	cl_int clerr = CL_SUCCESS;
//
//	//Buffer erstellen
//	m_dMatrix     = new cl_mem();
//	m_dNextMatrix = new cl_mem();
//	*m_dMatrix       = clCreateBuffer(*pOpenCL->GetContext(), CL_MEM_READ_WRITE, total_array_size * sizeof(cl_float),      NULL, &clerr);
//	CheckOpenCLError(clerr);
//	*m_dNextMatrix   = clCreateBuffer(*pOpenCL->GetContext(), CL_MEM_WRITE_ONLY, total_array_size * sizeof(unsigned int) , NULL, &clerr);
//	CheckOpenCLError(clerr);
//
//#ifdef _TIME_DEBUG
//	pTimer = new Timer("OpenCL - Buffer schreiben");
//#endif
//
//	//Buffer schreiben
//	clerr = clEnqueueWriteBuffer(*pOpenCL->GetQueue(), *m_dMatrix,       CL_TRUE, 0, total_array_size * sizeof(cl_float),     m_pMatrix,     0, NULL, NULL);
//	CheckOpenCLError(clerr);
//	clerr = clEnqueueWriteBuffer(*pOpenCL->GetQueue(), *m_dNextMatrix,   CL_TRUE, 0, total_array_size * sizeof(unsigned int), m_pNextMatrix, 0, NULL, NULL);
//	CheckOpenCLError(clerr);
//
//#ifdef _TIME_DEBUG
//	delete pTimer;
//	pTimer = new Timer("OpenCL - Variablen des Kernel setzen");
//#endif
//
//	unsigned int a = 0;
//	unsigned int n = node_count;
//
//	//Variablen des Kernel setzen
//	clerr = clSetKernelArg(*pOpenCL->GetKernel(), 0, sizeof(cl_mem), m_dMatrix);
//	CheckOpenCLError(clerr);
//	clerr = clSetKernelArg(*pOpenCL->GetKernel(), 1, sizeof(cl_mem), m_dNextMatrix);
//	CheckOpenCLError(clerr);
//	clerr = clSetKernelArg(*pOpenCL->GetKernel(), 2, sizeof(unsigned int), &a);
//	CheckOpenCLError(clerr);
//	clerr = clSetKernelArg(*pOpenCL->GetKernel(), 3, sizeof(unsigned int), &n);
//	CheckOpenCLError(clerr);
//
//	size_t matrixBufferSize = sizeof(float) * node_count * node_count;
//	float *pTmpMatrix = (float *)malloc(matrixBufferSize);
//
//#ifdef _TIME_DEBUG
//	delete pTimer;
//	pTimer = new Timer("OpenCL - Kernel starten");
//#endif
//
//	//Kernel starten
//	cl_event event;
//	size_t Total_size[2], Item_size[2];
//	Total_size[0] = blocksize_x * itemsize_x;
//	Total_size[1] = blocksize_y * itemsize_y;
//	Item_size[0] = itemsize_x;
//	Item_size[1] = itemsize_y;
//
//	bool changed(true);
//
//	while (changed)
//	{
//		changed = false;
//		++iterations;
//
//		for(a = 0; a < n; ++a)
//		{
//			clerr = clSetKernelArg(*pOpenCL->GetKernel(), 2, sizeof(unsigned int), &a);
//			CheckOpenCLError(clerr);
//
//			clerr = clEnqueueNDRangeKernel(*pOpenCL->GetQueue(), *pOpenCL->GetKernel(), 2, NULL, Total_size, Item_size, 0, NULL, &event);
//			CheckOpenCLError(clerr);
//			clerr = clWaitForEvents(1, &event);
//			CheckOpenCLError(clerr);
//			clerr = clReleaseEvent(event);
//			CheckOpenCLError(clerr);
//		}
//
//		clerr = clEnqueueReadBuffer(*pOpenCL->GetQueue(), *m_dMatrix,     CL_TRUE, 0, total_array_size * sizeof(cl_float), pTmpMatrix, 0, NULL, &event);
//		CheckOpenCLError(clerr);
//		clerr = clWaitForEvents(1, &event);
//		CheckOpenCLError(clerr);
//		clerr = clReleaseEvent(event);
//		CheckOpenCLError(clerr);
//
//		changed = (memcmp(pTmpMatrix, m_pMatrix, matrixBufferSize) != 0);
//		if (changed)
//			memcpy(m_pMatrix, pTmpMatrix, matrixBufferSize);
//	}
//
//#ifdef _TIME_DEBUG
//	delete pTimer;
//	pTimer = new Timer("OpenCL - Buffer lesen");
//#endif
//
//	free( pTmpMatrix );
//	pTmpMatrix = NULL;
//
//	//Buffer lesen
//	//clerr = clEnqueueReadBuffer(clcmdq, dMatrix,     CL_TRUE, 0, total_array_size * sizeof(cl_float), m_pMatrix, 0, NULL, NULL);
//	//CheckOpenCLError(clerr);
//	clerr = clEnqueueReadBuffer(*pOpenCL->GetQueue(), *m_dNextMatrix, CL_TRUE, 0, total_array_size * sizeof(unsigned int), m_pNextMatrix, 0, NULL, NULL);
//	CheckOpenCLError(clerr);
//
//#ifdef _TIME_DEBUG
//	delete pTimer;
//#endif
//
//	clReleaseMemObject(*m_dMatrix);
//	clReleaseMemObject(*m_dNextMatrix);
//
//	delete m_dMatrix;
//	delete m_dNextMatrix;
//
//	return iterations;
//}
//
//CString OpenCL::GetInformation(cl_platform_id platform, cl_platform_info info)
//{
//	CString erg;
//	size_t size;
//	cl_int clerr = CL_SUCCESS;
//
//	clerr = clGetPlatformInfo(platform, info, 0, NULL, &size);
//	CheckOpenCLError(clerr);
//	char * char_text = (char *)malloc(size);
//	clerr = clGetPlatformInfo(platform, info, size, char_text, NULL);
//	CheckOpenCLError(clerr);
//	erg.Format("%s",char_text);
//	free(char_text);
//
//	return erg;
//}
//
//CString OpenCL::GetInformation(cl_device_id device, cl_device_info info)
//{
//	CString erg;
//	size_t size;
//	cl_int clerr = CL_SUCCESS;
//
//	char * char_text;
//	cl_uint * erg_uint;
//	cl_uint * erg_bool;
//	cl_ulong * erg_ulong;
//	size_t * erg_size_t;
//	cl_platform_id * erg_platform;
//	cl_device_fp_config * erg_fp;
//	cl_device_exec_capabilities * erg_exec;
//	cl_device_mem_cache_type * erg_cache;
//	cl_device_local_mem_type * erg_mem;
//	cl_command_queue_properties * erg_queue;
//	cl_device_type * erg_device;
//
//	clerr = clGetDeviceInfo(device, info, 0, NULL, &size);
//	if (clerr != CL_SUCCESS)
//	{
//		if (clerr == CL_INVALID_VALUE)
//			return _T("unsupported value");
//		else
//			CheckOpenCLError(clerr);
//	}
//
//	switch (info)
//	{
//	case CL_DEVICE_ADDRESS_BITS: //cl_uint
//	case CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE:
//	case CL_DEVICE_MAX_CLOCK_FREQUENCY:
//	case CL_DEVICE_MAX_COMPUTE_UNITS:
//	case CL_DEVICE_MAX_CONSTANT_ARGS:
//	case CL_DEVICE_MAX_READ_IMAGE_ARGS:
//	case CL_DEVICE_MAX_SAMPLERS:
//	case CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS:
//	case CL_DEVICE_MAX_WRITE_IMAGE_ARGS:
//	case CL_DEVICE_MEM_BASE_ADDR_ALIGN:
//	case CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE:
//	case CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR:
//	case CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT:
//	case CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT:
//	case CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG:
//	case CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT:
//	case CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE:
//	case CL_DEVICE_VENDOR_ID:
//		erg_uint = (cl_uint *)malloc(size);
//		clerr = clGetDeviceInfo(device, info, size, erg_uint, NULL);
//		CheckOpenCLError(clerr);
//		erg.Format("%d", erg_uint[0]);
//		free(erg_uint);
//		break;
//	case CL_DEVICE_AVAILABLE: //cl_bool
//	case CL_DEVICE_COMPILER_AVAILABLE:
//	case CL_DEVICE_ENDIAN_LITTLE:
//	case CL_DEVICE_ERROR_CORRECTION_SUPPORT:
//	case CL_DEVICE_IMAGE_SUPPORT:
//		erg_bool = (cl_bool *)malloc(size);
//		clerr = clGetDeviceInfo(device, info, size, erg_bool, NULL);
//		CheckOpenCLError(clerr);
//		if (erg_bool[0])
//			erg.Format("true");
//		else
//			erg.Format("false");
//		free(erg_bool);
//		break;
//	case CL_DEVICE_DOUBLE_FP_CONFIG: //cl_device_fp_config
//	case CL_DEVICE_HALF_FP_CONFIG:
//	case CL_DEVICE_SINGLE_FP_CONFIG:
//		erg_fp = (cl_device_fp_config *)malloc(size);
//		clerr = clGetDeviceInfo(device, info, size, erg_fp, NULL);
//		CheckOpenCLError(clerr);
//		if (erg_fp[0] & CL_FP_DENORM)   erg.AppendFormat("CL_FP_DENORM");
//		if (erg_fp[0] & CL_FP_INF_NAN)  
//			if (erg.GetLength()>0) 
//				erg.AppendFormat(", CL_FP_DENORM");
//			else
//				erg.AppendFormat("CL_FP_DENORM");
//		if (erg_fp[0] & CL_FP_FMA)  
//			if (erg.GetLength()>0) 
//				erg.AppendFormat(", CL_FP_FMA");
//			else
//				erg.AppendFormat("CL_FP_FMA");
//		if (erg_fp[0] & CL_FP_ROUND_TO_NEAREST)  
//			if (erg.GetLength()>0) 
//				erg.AppendFormat(", CL_FP_ROUND_TO_NEAREST");
//			else
//				erg.AppendFormat("CL_FP_ROUND_TO_NEAREST");
//		if (erg_fp[0] & CL_FP_ROUND_TO_ZERO)  
//			if (erg.GetLength()>0) 
//				erg.AppendFormat(", CL_FP_ROUND_TO_ZERO");
//			else
//				erg.AppendFormat("CL_FP_ROUND_TO_ZERO");
//		if (erg_fp[0] & CL_FP_ROUND_TO_INF)  
//			if (erg.GetLength()>0) 
//				erg.AppendFormat(", CL_FP_ROUND_TO_INF");
//			else
//				erg.AppendFormat("CL_FP_ROUND_TO_INF");
//		free(erg_fp);
//		break;
//	case CL_DEVICE_EXECUTION_CAPABILITIES: //cl_device_exec_capabilities
//		erg_exec = (cl_device_exec_capabilities *)malloc(size);
//		clerr = clGetDeviceInfo(device, info, size, erg_exec, NULL);
//		CheckOpenCLError(clerr);
//		if (erg_exec[0] & CL_EXEC_KERNEL)   erg.AppendFormat("CL_EXEC_KERNEL");
//		if (erg_exec[0] & CL_EXEC_NATIVE_KERNEL)  
//			if (erg.GetLength()>0) 
//				erg.AppendFormat(", CL_EXEC_NATIVE_KERNEL");
//			else
//				erg.AppendFormat("CL_EXEC_NATIVE_KERNEL");
//		free(erg_exec);
//		break;
//	case CL_DEVICE_EXTENSIONS: //char
//	case CL_DEVICE_NAME:
//	case CL_DEVICE_PROFILE:
//	case CL_DEVICE_VENDOR:
//	case CL_DEVICE_VERSION:
//	case CL_DRIVER_VERSION:
//		char_text = (char *)malloc(size);
//		clerr = clGetDeviceInfo(device, info, size, char_text, NULL);
//		CheckOpenCLError(clerr);
//		erg.Format("%s", char_text);
//		free(char_text);
//		break;
//	case CL_DEVICE_GLOBAL_MEM_CACHE_SIZE: //cl_ulong
//	case CL_DEVICE_GLOBAL_MEM_SIZE:
//	case CL_DEVICE_LOCAL_MEM_SIZE:
//	case CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE:
//	case CL_DEVICE_MAX_MEM_ALLOC_SIZE:
//		erg_ulong = (cl_ulong *)malloc(size);
//		clerr = clGetDeviceInfo(device, info, size, erg_ulong, NULL);
//		CheckOpenCLError(clerr);
//		erg.Format("%ld", erg_ulong[0]);
//		free(erg_ulong);
//		break;
//	case CL_DEVICE_GLOBAL_MEM_CACHE_TYPE: //cl_device_mem_cache_type 
//		erg_cache = (cl_device_mem_cache_type *)malloc(size);
//		clerr = clGetDeviceInfo(device, info, size, erg_cache, NULL);
//		CheckOpenCLError(clerr);
//		if (erg_cache[0] & CL_NONE)   erg.AppendFormat("CL_NONE");
//		if (erg_cache[0] & CL_READ_ONLY_CACHE)  
//			if (erg.GetLength()>0) 
//				erg.AppendFormat(", CL_READ_ONLY_CACHE");
//			else
//				erg.AppendFormat("CL_READ_ONLY_CACHE");
//		if (erg_cache[0] & CL_READ_WRITE_CACHE)  
//			if (erg.GetLength()>0) 
//				erg.AppendFormat(", CL_READ_WRITE_CACHE");
//			else
//				erg.AppendFormat("CL_READ_WRITE_CACHE");
//		free(erg_cache);
//		break;
//	case CL_DEVICE_IMAGE2D_MAX_HEIGHT: //size_t
//	case CL_DEVICE_IMAGE2D_MAX_WIDTH:
//	case CL_DEVICE_IMAGE3D_MAX_DEPTH:
//	case CL_DEVICE_IMAGE3D_MAX_HEIGHT:
//	case CL_DEVICE_IMAGE3D_MAX_WIDTH:
//	case CL_DEVICE_MAX_PARAMETER_SIZE:
//	case CL_DEVICE_MAX_WORK_GROUP_SIZE:
//	case CL_DEVICE_PROFILING_TIMER_RESOLUTION:
//		erg_size_t = (size_t *)malloc(size);
//		clerr = clGetDeviceInfo(device, info, size, erg_size_t, NULL);
//		CheckOpenCLError(clerr);
//		erg.Format("%ld", erg_size_t[0]);
//		free(erg_size_t);
//		break;
//	case CL_DEVICE_LOCAL_MEM_TYPE: //cl_device_local_mem_type 
//		erg_mem = (cl_device_local_mem_type *)malloc(size);
//		clerr = clGetDeviceInfo(device, info, size, erg_mem, NULL);
//		CheckOpenCLError(clerr);
//		if (erg_mem[0] & CL_LOCAL)   erg.AppendFormat("CL_LOCAL");
//		if (erg_mem[0] & CL_GLOBAL)  
//			if (erg.GetLength()>0) 
//				erg.AppendFormat(", CL_GLOBAL");
//			else
//				erg.AppendFormat("CL_GLOBAL");
//		free(erg_mem);
//		break;
//	case CL_DEVICE_MAX_WORK_ITEM_SIZES: //size_t[]
//		erg_size_t = (size_t *)malloc(size);
//		clerr = clGetDeviceInfo(device, info, size, erg_size_t, NULL);
//		CheckOpenCLError(clerr);
//		erg.Format("%ld", erg_size_t[0]);
//		for(unsigned int z = 1; z<size/sizeof(size_t); ++z)
//			erg.AppendFormat(", %ld", erg_size_t[z]);
//		free(erg_size_t);
//		break;
//	case CL_DEVICE_PLATFORM: //cl_platform_id
//		erg_platform = (cl_platform_id *)malloc(size);
//		clerr = clGetDeviceInfo(device, info, size, erg_platform, NULL);
//		CheckOpenCLError(clerr);
//		erg.Format("%d", erg_platform[0]);
//		free(erg_platform);
//		break;
//	case CL_DEVICE_QUEUE_PROPERTIES: //cl_command_queue_properties 
//		erg_queue = (cl_command_queue_properties *)malloc(size);
//		clerr = clGetDeviceInfo(device, info, size, erg_queue, NULL);
//		CheckOpenCLError(clerr);
//		if (erg_queue[0] & CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE)   erg.AppendFormat("CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE");
//		if (erg_queue[0] & CL_QUEUE_PROFILING_ENABLE)  
//			if (erg.GetLength()>0) 
//				erg.AppendFormat(", CL_QUEUE_PROFILING_ENABLE");
//			else
//				erg.AppendFormat("CL_QUEUE_PROFILING_ENABLE");
//		free(erg_queue);
//		break;
//	case CL_DEVICE_TYPE: //cl_device_type 
//		erg_device = (cl_device_type *)malloc(size);
//		clerr = clGetDeviceInfo(device, info, size, erg_device, NULL);
//		CheckOpenCLError(clerr);
//		if (erg_device[0] & CL_DEVICE_TYPE_CPU)   erg.AppendFormat("CL_DEVICE_TYPE_CPU");
//		if (erg_device[0] & CL_DEVICE_TYPE_GPU)  
//			if (erg.GetLength()>0) 
//				erg.AppendFormat(", CL_DEVICE_TYPE_GPU");
//			else
//				erg.AppendFormat("CL_DEVICE_TYPE_GPU");
//		if (erg_device[0] & CL_DEVICE_TYPE_ACCELERATOR)  
//			if (erg.GetLength()>0) 
//				erg.AppendFormat(", CL_DEVICE_TYPE_ACCELERATOR");
//			else
//				erg.AppendFormat("CL_DEVICE_TYPE_ACCELERATOR");
//		if (erg_device[0] & CL_DEVICE_TYPE_DEFAULT)  
//			if (erg.GetLength()>0) 
//				erg.AppendFormat(", CL_DEVICE_TYPE_DEFAULT");
//			else
//				erg.AppendFormat("CL_DEVICE_TYPE_DEFAULT");
//		free(erg_device);
//		break;
//	default:
//		assert(0);
//		break;
//	}
//
//	return erg;
//}
//
//int OpenCL::GetPlatformPickId()
//{
//	int result = 0;
//	cl_int clerr = CL_SUCCESS;
//
//	cl_platform_id platform_pick;
//	cl_platform_id *platforms = NULL;
//	clerr = clGetPlatformIDs(0, NULL, &m_num_platforms);
//	CheckOpenCLError(clerr);
//	if (m_num_platforms > 0)
//	{
//		platforms = (cl_platform_id *)malloc(sizeof(cl_platform_id) * m_num_platforms);
//		clerr = clGetPlatformIDs(m_num_platforms, platforms, NULL);
//		CheckOpenCLError(clerr);
//		//platform_pick = platforms[GetPlatformPickId()];
//	}
//	else
//		assert(0);
//
//	for(int z=0; z<(int)m_num_platforms; ++z)
//	{
//		CString infoText;
//		platform_pick = platforms[z];
//
//		cl_context_properties props[3];
//		props[0] = (cl_context_properties)CL_CONTEXT_PLATFORM;   // indicates that next element is platform
//		props[1] = (cl_context_properties)platform_pick;         // platform_pick is of type cl_platform_id
//		props[2] = (cl_context_properties)0;                     // last element must be 0
//
//		cl_context clctx = clCreateContextFromType(props, CL_DEVICE_TYPE_ALL, NULL, NULL, &clerr);
//		CheckOpenCLError(clerr);
//
//		size_t paramz;
//		clerr = clGetContextInfo(clctx, CL_CONTEXT_DEVICES, 0, NULL, &paramz);
//		CheckOpenCLError(clerr);
//
//		cl_device_id * cldevs = (cl_device_id *)malloc(paramz);
//		clerr = clGetContextInfo(clctx, CL_CONTEXT_DEVICES, paramz, cldevs, NULL);
//		CheckOpenCLError(clerr);
//
//		infoText = GetInformation(platform_pick, CL_PLATFORM_VENDOR);
//		infoText.Trim();
//
//		if (infoText.Find("NVIDIA") >= 0)
//			result = z;
//		if (infoText.Find("AMD") >= 0)
//			result = z;
//		if (infoText.Find("ATI") >= 0)
//			result = z;
//
//		free(cldevs);
//	}
//	free(platforms);
//
//	return result;
//}

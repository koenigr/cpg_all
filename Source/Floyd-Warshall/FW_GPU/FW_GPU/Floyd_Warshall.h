#pragma once

#include <vector>
#include <set>
#include <map>
#include <assert.h>
#include <limits>
#include <omp.h>
#include <atlstr.h>
#include <math.h>

//#include <CL/opencl.h>

#include <boost/heap/fibonacci_heap.hpp>
#include <boost/heap/d_ary_heap.hpp>
#include <boost/mpl/if.hpp>

#include <Windows.h>

//#ifdef WIN64
//#pragma comment(lib,"OpenCL_x64.lib")
//#else
//#pragma comment(lib,"OpenCL.lib")
//#endif

#ifdef max
#undef max
#endif

namespace FW
{
	//class OpenCL;
	//static OpenCL * m_pOpenCL = NULL;

	//class OpenCL
	//{
	//private:
	//	//OpenCL stuff
	//	cl_uint m_num_platforms;
	//	cl_kernel *m_clkern;
	//	cl_program *m_clpgm;
	//	cl_command_queue *m_clcmdq;
	//	cl_context *m_clctx;
	//	cl_platform_id *m_platforms;

	//	OpenCL()
	//	{
	//		InitOpenCL();
	//	}

	//public:

	//	const cl_kernel * GetKernel()		{ return m_clkern; }
	//	const cl_program * GetProgram()		{ return m_clpgm; }
	//	const cl_command_queue * GetQueue() { return m_clcmdq; }
	//	const cl_context * GetContext()		{ return m_clctx; }

	//	static OpenCL * GetOpenCL()
	//	{
	//		if (!m_pOpenCL)
	//			m_pOpenCL = new OpenCL();
	//		return m_pOpenCL;
	//	}

	//	~OpenCL()
	//	{
	//		QuitOpenCL();
	//	}

	//	void InitOpenCL();
	//	void QuitOpenCL();
	//	int GetPlatformPickId();
	//	CString GetInformation(cl_platform_id platform, cl_platform_info info);
	//	CString GetInformation(cl_device_id device, cl_device_info info);
	//};

	class Edge;
	class Node;
	class NodeBetweenness;
	class NodePosition;

	typedef std::vector<Edge *> EdgeVector;
	typedef std::set<Edge *> EdgeSet;
	typedef std::map<Node *, Edge *> NodeEdgeMap;
	typedef std::pair<Node *, Edge *> NodeEdgePair;
	typedef std::vector<Node *> NodeVector;
	typedef std::vector<NodeBetweenness *> NodeBetweennessVector;
	typedef std::map<int, NodePosition *> NodePositionsMap;

	class NodeBetweenness
	{
	public:
		FW::Node *m_node;
		size_t m_iUsedCount;			//wie oft wurde es benutzt
		size_t m_iUsedStepSum;          //wieviele Schritte braucht man zu allen

		NodeBetweenness(FW::Node *node): m_node(node), m_iUsedCount(0), m_iUsedStepSum(0)
		{}

		~NodeBetweenness()
		{}
	};

	class NodePosition
	{
	public:
		FW::Node *m_node;
		double m_x,m_y;

		NodePosition(FW::Node *node, double x, double y): m_node(node), m_x(x), m_y(y)
		{}

		~NodePosition()
		{}
	};

	class Node
	{
	public:
		size_t m_iID;					//Position in der Matrix
		NodeEdgeMap m_vOutgoingEdges;	//nur "rausgehende" Edges
		NodeEdgeMap m_vIncomingEdges;	//nur "eingehende" Edges
		size_t m_iUsedCount;			//wie oft wurde es benutzt
		size_t m_iUsedStepSum;          //wieviele Schritte braucht man zu allen anderen

		Node * m_pParent;               //Eltern Node
		NodeVector m_vChilds;           //Child Nodes

		Node(int iID): m_iID(iID), m_iUsedCount(0), m_iUsedStepSum(0), m_pParent(NULL)
		{
		}

		~Node()
		{
			if (!m_vOutgoingEdges.empty())
				assert(0);

			if (!m_vIncomingEdges.empty())
				assert(0);
		}

		void AddEdge(Edge * pEdge, bool outgoing);

		void RemoveEdge(Edge * pEdge, bool outgoing);

		Edge * FindEdge(Node * pEnd) const;
	};

	class Edge //directed Edge
	{
	public:
		Node * m_pStart;
		Node * m_pEnd;
		float m_fCost;
		size_t m_iUsedCount;

		Edge(Node * pStart, Node * pEnd, float fCost): m_pStart(pStart), m_pEnd(pEnd), m_fCost(fCost), m_iUsedCount(0)
		{
			m_pStart->AddEdge(this, true);
			m_pEnd->AddEdge(this, false);
		}

		~Edge()
		{
			m_pStart->RemoveEdge(this, true);
			m_pEnd->RemoveEdge(this, false);
		}

		size_t GetUndirectedUsedCount()
		{
			size_t result = m_iUsedCount;

			Edge * pOpposite = m_pEnd->FindEdge(m_pStart);
			if (pOpposite)
				result += pOpposite->m_iUsedCount;

			return result;
		}
	};

	typedef std::vector<Node *> NodeVector;

	class Graph
	{
		NodeVector m_vNodes;
		EdgeVector m_vEdges;
		std::vector< std::vector< int >* > m_neighbours;
		std::vector< std::vector< float >* > m_weights;

	public:
		enum ComputeType {
			CPU_FLOYD_WARSHALL = 10,
			CPU_DIJKSTRA_ORIGINAL = 20,
			CPU_DIJKSTRA_PARALLEL = 21,
			CPU_DIJKSTRA_BASIC = 22
		};
		//warning: optimized code accesses elements of this structure directly
		struct matrixElement
		{
			float m_fCost;
			//int m_iNext;
		};

		struct matrixElementNext
		{
			int m_iNext;
		};

	private:
		struct matrixChildPathEntry
		{
			float cost;
			Node *src;
			Node *dst;
		};

		matrixChildPathEntry * m_childPathsCache;

	protected:
		matrixElement * m_pMatrix;
		matrixElementNext * m_pNextMatrix;
		bool m_bOpenCLInited;
		ComputeType m_computeType;

	public:
		Node * AddNode()
		{
			m_neighbours.push_back(new std::vector<int>());
			m_weights.push_back(new std::vector<float>());

			Node * pNode = new Node(m_vNodes.size());
			m_vNodes.push_back(pNode);
			return pNode;
		}

		Edge * AddDirectedEdge(Node *pStart, Node *pEnd, float fCost)
		{
			Edge *pEdge = new Edge(pStart, pEnd, fCost);
			m_vEdges.push_back(pEdge);

			m_neighbours[pEnd->m_iID]->push_back(pStart->m_iID);
			m_weights[pEnd->m_iID]->push_back(fCost);

			return pEdge;
		}

		EdgeVector AddEdge(Node *pNode1, Node *pNode2, float fCost)
		{
			Edge *pEdge1 = new Edge(pNode1, pNode2, fCost);
			Edge *pEdge2 = new Edge(pNode2, pNode1, fCost);
			m_vEdges.push_back(pEdge1);
			m_vEdges.push_back(pEdge2);
			EdgeVector result;
			result.push_back(pEdge1);
			result.push_back(pEdge2);

			return result;
		}

		EdgeVector AddEdgeNoDuplicates(Node *pNode1, Node *pNode2, float fCost)
		{
			//prevent having multiple edges between two nodes
			Edge *pEdge1 = pNode1->FindEdge(pNode2);
			if (pEdge1 == NULL){
				pEdge1 = new Edge(pNode1, pNode2, fCost);
				m_vEdges.push_back(pEdge1);
				m_neighbours[pNode1->m_iID]->push_back(pNode2->m_iID);
				m_weights[pNode1->m_iID]->push_back(fCost);
			}
			else {
				if (pEdge1->m_fCost > fCost) {
					pEdge1->m_fCost = fCost;
					for (int i = 0; i < m_neighbours[pNode1->m_iID]->size(); i++) {
						if (m_neighbours[pNode1->m_iID]->at(i) == pNode2->m_iID) {
							m_weights[pNode1->m_iID]->at(i) = fCost;
							break;
						}
					}
				}
			}

			Edge *pEdge2 = pNode2->FindEdge(pNode1);
			if (pEdge2 == NULL) {
				pEdge2 = new Edge(pNode2, pNode1, fCost);
				m_vEdges.push_back(pEdge2);
				m_neighbours[pNode2->m_iID]->push_back(pNode1->m_iID);
				m_weights[pNode2->m_iID]->push_back(fCost);
			}
			else {
				if (pEdge2->m_fCost > fCost) {
					pEdge2->m_fCost = fCost;
					for (int i = 0; i < m_neighbours[pNode2->m_iID]->size(); i++) {
						if (m_neighbours[pNode2->m_iID]->at(i) == pNode1->m_iID) {
							m_weights[pNode2->m_iID]->at(i) = fCost;
							break;
						}
					}
				}
			}

			EdgeVector result;
			result.push_back(pEdge1);
			result.push_back(pEdge2);
			return result;
		}

		void ClearGraph()
		{
			while(!m_neighbours.empty()){
				std::vector< int >* vec = m_neighbours.back();
				m_neighbours.pop_back();
				delete vec;
			}

			while (!m_weights.empty()){
				std::vector<float> *vec =m_weights.back();
				m_weights.pop_back();
				delete vec;
			}

			EdgeVector::iterator l_edge, e_edge;

			l_edge = m_vEdges.begin();
			e_edge = m_vEdges.end();
			for(; l_edge!=e_edge; l_edge++)
			{
				Edge *pEdge = *l_edge;
				delete pEdge;
			}
			m_vEdges.clear();

			NodeVector::iterator l_node, e_node;

			l_node = m_vNodes.begin();
			e_node = m_vNodes.end();
			for(; l_node!=e_node; l_node++)
			{
				Node *pNode = *l_node;
				delete pNode;
			}
			m_vNodes.clear();
		}

		Graph(): m_pMatrix( NULL ), m_pNextMatrix( NULL ), m_bOpenCLInited( false ), m_childPathsCache(NULL)
		{
			//InitOpenCL();
		}

		~Graph()
		{
			ClearGraph();
			//QuitOpenCL();

			if (m_pMatrix)
			{
				free( m_pMatrix );
				m_pMatrix = NULL;
			}

			if (m_pNextMatrix)
			{
				free( m_pNextMatrix );
				m_pNextMatrix = NULL;
			}

			if (m_childPathsCache)
			{
				free(m_childPathsCache);
				m_childPathsCache = NULL;
			}
		}

		void InitMatrix()
		{
			if (m_pMatrix)
			{
				free( m_pMatrix );
				m_pMatrix = NULL;
			}
			if (m_pNextMatrix)
			{
				free( m_pNextMatrix );
				m_pNextMatrix = NULL;
			}

			size_t node_count = m_vNodes.size();
			m_pMatrix = (matrixElement *)malloc(sizeof(matrixElement) * node_count * node_count);
			m_pNextMatrix = (matrixElementNext *)malloc(sizeof(matrixElementNext) * node_count * node_count);
			matrixElement * pM = m_pMatrix;
			matrixElementNext * pNM = m_pNextMatrix;
			float max = std::numeric_limits<float>::max();

			//alles auf 0 und "unendlich" stellen
			for(size_t y = 0; y < node_count; ++y)
				for(size_t x = 0; x < node_count; ++x, ++pM, ++pNM)
				{
					if (x == y)
						pM->m_fCost = 0.0f;
					else
						pM->m_fCost = max;
					pNM->m_iNext = -1;
				}

				//StartMatrix setzen
				for(size_t y = 0; y < node_count; ++y)
				{
					Node *pNode = m_vNodes[y];
					NodeEdgeMap::iterator l_edge, e_edge;
					l_edge = pNode->m_vOutgoingEdges.begin();
					e_edge = pNode->m_vOutgoingEdges.end();

					for(; l_edge!=e_edge; l_edge++)
					{
						Edge * pEdge = (*l_edge).second;
						size_t x = pEdge->m_pEnd->m_iID;

						matrixElement * pM = m_pMatrix + (y * node_count + x);
						pM->m_fCost = pEdge->m_fCost;
						//pM->m_pNext = pNode;
					}
				}
		}

		matrixElement * GetMatrix()
		{
			return m_pMatrix;
		}

		matrixElementNext * GetNextMatrix()
		{
			return m_pNextMatrix;
		}

		size_t GetNodeCount()
		{
			return m_vNodes.size();
		}

		size_t GetEdgeCount()
		{
			return m_vEdges.size();
		}

#define ELEM(x, y, matrix, node_count) ((matrix) + ((y) * (node_count) + (x)))
//#define DISTANCE(positions, x, y) sqrt((positions[2*x] - positions[2*y])*(positions[2*x] - positions[2*y]) + (positions[2*x+1] - positions[2*y+1])*(positions[2*x+1] - positions[2*y+1]))
		/*inline double DISTANCE(const std::vector<double> &positions, int x, int y) const {
			return sqrt((positions[2*x] - positions[2*y])*(positions[2*x] - positions[2*y]) + (positions[2*x+1] - positions[2*y+1])*(positions[2*x+1] - positions[2*y+1]));
		}*/
		inline double EuclideanDistance(NodePosition *a, NodePosition *b) const {
			return sqrt((a->m_x - b->m_x)*(a->m_x - b->m_x) + (a->m_y - b->m_y)*(a->m_y - b->m_y));
		}
		/*
		matrixElement * Dist(size_t x, size_t y)
		{
		return m_pMatrix + (y * m_vNodes.size() + x);
		}

		matrixElement * DistIntermediate(size_t x, size_t y)
		{
		return m_pIntermediate + (y * m_vNodes.size() + x);
		}
		*/

		//----------------------------------------------------------------------------------------------------------------------------

		//define the type of heap we want to use
		//altough fibonnaci heap has best theoretical performance, d-ary heap seems to be better in practice (atleast for the graph we tested on)
		//comparison of different heaps can be found here http://www.boost.org/doc/libs/1_54_0/doc/html/heap/data_structures.html

		typedef boost::heap::fibonacci_heap< std::pair<float, int>, boost::heap::compare< std::greater< std::pair<float, int> > > > heap;
		//typedef boost::heap::d_ary_heap< std::pair<float, int>, boost::heap::mutable_<true>, boost::heap::arity<4> , boost::heap::compare< std::greater< std::pair<float, int> > > > heap;

		void Dijkstra(ComputeType type){
			Dijkstra(type, std::numeric_limits<float>::max());
		}

		void Dijkstra(ComputeType type, float max_path_length){
			m_computeType = type;
			int node_count = (int) m_vNodes.size();
			float max = std::numeric_limits<float>::max();

			if (!m_pMatrix)
				m_pMatrix = (matrixElement *)  malloc(sizeof(matrixElement) * node_count * node_count);

			if (!m_pNextMatrix)
				m_pNextMatrix = (matrixElementNext *) malloc(sizeof(matrixElementNext) * node_count * node_count);

			float *p_matrix = (float *) m_pMatrix;
			int *p_next_matrix = (int *) m_pNextMatrix;
			int n_procs = (type == ComputeType::CPU_DIJKSTRA_PARALLEL) ? omp_get_num_procs() : 1;

#pragma omp parallel shared(p_matrix, p_next_matrix) num_threads(n_procs)
			{
				heap::handle_type *handles = (heap::handle_type *)malloc(sizeof(heap::handle_type) * node_count);
				int *mask = (int *) malloc(sizeof(int) * node_count);

				//tid in <0, num_threads>
				int tid = omp_get_thread_num();
				for(int x = 0; x < node_count; ++x) {
					//split the work between the threads
					if (x % n_procs != tid)
						continue;

					//initalized shortest paths where all vertices are max, excepts from source which is 0
					for(int i = 0; i < (int) node_count; ++i){
						mask[i] = 0;
						p_matrix[x * node_count + i] = max;
						p_next_matrix[x * node_count + i] = -1;
					}
					p_matrix[x * node_count + x] = 0;

					//call appropriate single source all-pairs shortest paths
					switch (type){
					case ComputeType::CPU_DIJKSTRA_ORIGINAL:
					case ComputeType::CPU_DIJKSTRA_PARALLEL:
						DijkstraStep(x, handles, mask, p_matrix, p_next_matrix, max_path_length, false);
						break;
					case ComputeType::CPU_DIJKSTRA_BASIC:
						DijkstraStepRepresentation(x, handles, mask, p_matrix, p_next_matrix, max_path_length, false);
						break;
					}
				}

				free(handles);
				free(mask);
			}
		}

		//standard dijkstra algorithm
		//TODO: shortest path are not computed correctly in optimized version (isAllPairsShortestPath = true)
		//isAllPairsShortestPath computes correct cost only if all edges are undirected
		void DijkstraStep(int source, heap::handle_type *handles, int *mask, float *p_matrix, int *p_next_matrix, float max_path_length, bool isAllPairsShortestPath){
			std::pair<float, int> node;
			size_t y;
			int node_count = (int) m_vNodes.size();
			float max = std::numeric_limits<float>::max();
			Edge *pEdge;
			Node *pNode;
			NodeEdgeMap::iterator l_edge, e_edge;

			heap Q;

			if (!isAllPairsShortestPath) {
				//start with a source vertex
				handles[source] = Q.push( std::make_pair(0,source) );
			}
			else {
				for (int x = 0; x <= source; x++) {
					//we have already computed this distance in previous iteration
					//instead of recomping this again, we directly take this as a best cost and update our neighbours.
					//The code would be nicer if we instead enqueued this node into the heap, but since push and pop
					//operations are expesive, we want to avoid that

					//warning: assumes sequential execution which does not hold when computing in parallel
					p_matrix[source * node_count + x] = p_matrix[x * node_count + source];
					if (p_matrix[source * node_count + x] >= max_path_length)
						continue;

					pNode = m_vNodes[x];

					l_edge = pNode->m_vIncomingEdges.begin();
					e_edge = pNode->m_vIncomingEdges.end();

					for(; l_edge!=e_edge; l_edge++) {
						pEdge = (*l_edge).second;
						y = pEdge->m_pStart->m_iID;

						if (pEdge->m_fCost + p_matrix[source * node_count + x] < p_matrix[source * node_count + y] &&
							max_path_length > pEdge->m_fCost + p_matrix[source * node_count + x]){
								p_matrix[source * node_count + y] = pEdge->m_fCost + p_matrix[source * node_count + x];
								p_next_matrix[source * node_count + y] = x;
						}
					}
				}

				//enqueue all nodes for which we do not have best path yet and which are directly reachable from one of the nodes
				//with already known best path
				for (int x = source + 1; x < node_count; ++x){
					if (p_matrix[source * node_count + x] != max) {
						mask[x] = 1;
						handles[x] = Q.push( std::make_pair(p_matrix[source * node_count + x],x) );
					}
				}
			}

			while (!Q.empty()) {
				//at each step pick the vertex with minimum distance, this is achieved using heap data structure
				node = Q.top(); Q.pop();
				//mask array is used to denote which vertices are in the heap
				mask[node.second] = 0;

				pNode = m_vNodes[node.second];
				l_edge = pNode->m_vIncomingEdges.begin();
				e_edge = pNode->m_vIncomingEdges.end();

				//iterate over all edges of selected vertex
				for(; l_edge!=e_edge; l_edge++) {
					pEdge = (*l_edge).second;
					y = pEdge->m_pStart->m_iID;

					//we already know best path for nodes less than source so we skip it
					if (isAllPairsShortestPath && y <= source)
						continue;

					//see if we can improve the existing path, given that the new path does not exceed maximum length
					if (pEdge->m_fCost + p_matrix[source * node_count + node.second] < p_matrix[source * node_count + y] &&
						max_path_length > pEdge->m_fCost + p_matrix[source * node_count + node.second]){
							p_next_matrix[source * node_count + y] = node.second;
							p_matrix[source * node_count + y] = pEdge->m_fCost + p_matrix[source * node_count + node.second];

							//if the updatex vertex is not in heap, we need to add it
							if (mask[y] == 0) {
								handles[y] = Q.push( std::make_pair(p_matrix[source * node_count + y], y) );
							}
							else {
								//otherwise we need to update the existing entry with new cost
								(*handles[y]).first = p_matrix[source * node_count + y];
								//adjust position of vertex in the heap since we updates it's value
								Q.increase(handles[y]);
							}
							mask[y] = 1;
					}
				}
			}
		}

		//same as DijkstraStep except that we use more efficient way of iterating over node edges
		void DijkstraStepRepresentation(int source, heap::handle_type *handles, int *mask, float *p_matrix, int *p_next_matrix, float max_path_length, bool isAllPairsShortestPath){
			std::pair<float, int> node;
			size_t y; float cost, weight_node;
			int node_count = (int) m_vNodes.size();
			float max = std::numeric_limits<float>::max();
			std::vector<int>::const_iterator iter;

			//instead of storing node edges in a hashmap, we use two arrays
			//neighbours array stores ID of neighbouring edges and weights store the cost.
			std::vector<int> *neighbours;
			std::vector<float> *weights;

			heap Q;

			if (!isAllPairsShortestPath){
				handles[source] = Q.push( std::make_pair(0,source) );
			} else {
				for (int x = 0; x <= source; x++) {
					//we have already computed this distance in previous iteration. Work only if all edges are undirected
					p_matrix[source * node_count + x] = p_matrix[x * node_count + source];
					if (p_matrix[source * node_count + x] >= max_path_length)
						continue;

					neighbours = m_neighbours[x];
					weights = m_weights[x];

					int i = 0;
					for(iter = neighbours->cbegin(); iter != neighbours->cend(); iter++) {
						y = *iter;
						cost = weights->at(i++) + p_matrix[source * node_count + x];

						if (cost < p_matrix[source * node_count + y] && max_path_length > cost){
							p_matrix[source * node_count + y] = cost;
							p_next_matrix[source * node_count + y] = x;
						}
					}
				}

				for (int x = source + 1; x < node_count; ++x){
					if (p_matrix[source * node_count + x] != max) {
						mask[x] = 1;
						handles[x] = Q.push( std::make_pair(p_matrix[source * node_count + x],x) );
					}
				}
			}

			while (!Q.empty()) {
				node = Q.top();
				Q.pop();
				mask[node.second] = 0;
				weight_node = p_matrix[source * node_count + node.second];

				neighbours = m_neighbours[node.second];
				weights = m_weights[node.second];

				int i = -1;
				for(iter = neighbours->cbegin(); iter != neighbours->cend(); iter++) {
					y = *iter;
					i++;
					if (isAllPairsShortestPath && y <= source)
						continue;

					cost = weights->at(i) + weight_node;

					if (cost < p_matrix[source * node_count + y] && max_path_length > cost) {
						p_matrix[source * node_count + y] = cost;
						p_next_matrix[source * node_count + y] = node.second;
						if (mask[y] == 0) {
							handles[y] = Q.push( std::make_pair(cost, y) );
						}
						else {
							(*handles[y]).first = cost;
							Q.increase(handles[y]);
						}
						mask[y] = 1;
					}
				}
			}
		}

		//----------------------------------------------------------------------------------------------------------------------------

		void FloydWarshall(){
			m_computeType = ComputeType::CPU_FLOYD_WARSHALL;
			if (!m_pMatrix)
				assert(0);

			int node_count = (int) m_vNodes.size();
			float max = std::numeric_limits<float>::max();

			int *p_next_matrix = (int *) m_pNextMatrix;
			float *matrix = (float *) m_pMatrix;

			for(int k = 0; k < node_count; ++k)
				for(int i = 0; i < node_count; ++i) {
					float ik = matrix[i * node_count + k];
					if (ik == max)
						continue;

					for(int j = 0; j < node_count; j++) {
						if (ik + matrix[k * node_count + j] < matrix[i * node_count + j]){
							matrix[i * node_count + j] = ik + matrix[k * node_count + j];
							p_next_matrix[i * node_count + j] = k;
						}
					}
				}
		}

		bool IterateMatrix()
		{
			m_computeType = ComputeType::CPU_FLOYD_WARSHALL;
			if (!m_pMatrix)
				assert(0);

			size_t node_count = m_vNodes.size();
			float max = std::numeric_limits<float>::max();
			bool changed = false;
			matrixElement * pMatrix = m_pMatrix;
			matrixElementNext * pNextMatrix = m_pNextMatrix;

			////this parallelization is not sound and can produce wrong results
			//			omp_set_num_threads( omp_get_num_procs() );
			//#pragma omp parallel firstprivate(pMatrix, pNextMatrix, node_count, max) shared(changed) default(none)
			{
				//#pragma omp for
				for(int a = 0; a < (int)node_count; ++a)
					for(size_t y = 0; y < node_count; ++y)
						if (a != y)
							for(size_t x = 0; x < node_count; ++x)
								if ((x != y) && (a != x))
								{
									matrixElement * pPart1 = ELEM(x, a, pMatrix, node_count);
									if (pPart1->m_fCost == max)
										continue;
									matrixElement * pPart2 = ELEM(a, y, pMatrix, node_count);
									if (pPart2->m_fCost == max)
										continue;

									float sum = pPart1->m_fCost + pPart2->m_fCost;
									matrixElement * pOrg = ELEM(x, y, pMatrix, node_count);
									if (sum < pOrg->m_fCost)
									{
										pOrg->m_fCost = sum;
										ELEM(x, y, pNextMatrix, node_count)->m_iNext = a;
										changed = true;
									}
								}
			}

			return changed;
		}

		void GetMatrixPos(matrixElement *pElem, size_t &x, size_t &y)
		{
			size_t delta = ((size_t)pElem - (size_t)m_pMatrix) / sizeof(matrixElement);
			size_t node_count = m_vNodes.size();
			x = delta % node_count;
			y = delta / node_count;
		}

		void GetPathFloydWarshall(int from, int to, EdgeVector &result){
			if (from == to)
				return;

			float max = std::numeric_limits<float>::max();
			size_t node_count = m_vNodes.size();
			matrixElement *pElem = ELEM(to, from, m_pMatrix, node_count);
			matrixElementNext *pNext = ELEM(to, from, m_pNextMatrix, node_count);
			if (pElem->m_fCost == max)
				return;

			if (pNext->m_iNext == -1) //direkte Kante
			{
				Edge *pEdge = m_vNodes[from]->FindEdge(m_vNodes[to]);
				if (pEdge)
					result.push_back(pEdge);
			}
			else
			{
				size_t intermediate = pNext->m_iNext;

				GetPathFloydWarshall(from, intermediate, result);
				GetPathFloydWarshall(intermediate, to, result);
			}
		}

		void GetPathDijkstra(int from, int to, EdgeVector &result){
			if (from == to)
				return;

			matrixElementNext *pNext = ELEM(from, to, m_pNextMatrix, m_vNodes.size());
			size_t intermediate = pNext->m_iNext;

			Edge *pEdge = m_vNodes[from]->FindEdge(m_vNodes[intermediate]);
			if (pEdge)
				result.push_back(pEdge);
			GetPathDijkstra(intermediate, to, result);
		}

		void GetPath(int from, int to, EdgeVector &result)
		{
			switch (m_computeType)
			{
			case FW::Graph::CPU_FLOYD_WARSHALL:
				GetPathFloydWarshall(from,to,result);
				break;
			case FW::Graph::CPU_DIJKSTRA_ORIGINAL:
			case FW::Graph::CPU_DIJKSTRA_PARALLEL:
			case FW::Graph::CPU_DIJKSTRA_BASIC:
				if (ELEM(from, to, m_pNextMatrix, m_vNodes.size())->m_iNext != -1)
					GetPathDijkstra(from,to,result);
				break;
			default:
				assert(false);
				break;
			}
		}

		void GetPath(Node * pFrom, Node *pTo, EdgeVector &result)
		{
			GetPath(pFrom->m_iID, pTo->m_iID, result);
		}

		unsigned int IncreaseParentPath(Node * pFrom, Node *pTo, const int processor, NodeBetweennessVector &pNodeVector, const NodePositionsMap &positions, const std::map<int, int> &parent_to_node_map)
		{
			EdgeVector path;
			//NodeVector parentPath;
			Node * lastParent(NULL);
			unsigned int iSteps(0);

			GetPath(pFrom->m_iID, pTo->m_iID, path);

			//Path auf Parents einschmelzen
			EdgeVector::iterator l, e;
			l = path.begin();
			e = path.end();
			if (l!=e)
			{
				//Start Node behandeln
				Edge *pEdge = *l;

				Node *pParent = pEdge->m_pStart;
				while(pParent->m_pParent)
					pParent = pParent->m_pParent;
				//parentPath.push_back(pParent);
				lastParent = pParent;
			}

			//alle weiteren Kanten Endnodes behandeln
			for(; l!=e; l++)
			{
				Edge *pEdge = *l;

				Node *pParent = pEdge->m_pEnd;
				while(pParent->m_pParent)
					pParent = pParent->m_pParent;
				if (pParent != lastParent)
				{
					//parentPath.push_back(pParent);
					lastParent = pParent;

					//pParent hochz�hlen

					//InterlockedIncrement(&(pParent->m_iUsedCount));
					//we need to locate node with pParent->m_iID in the pNodeVector array					
					int parent_id = parent_to_node_map.at(pParent->m_iID);					
					if (pParent->m_iID != pNodeVector[parent_id]->m_node->m_iID){
						throw std::logic_error("expected node with wrong ID");
					}
					InterlockedIncrement(&(pNodeVector[parent_id]->m_iUsedCount));

					++iSteps;
				}
			}

			return iSteps;
		}

		unsigned int IncreasePathFloydWarshall(int from, int to, const int processor)
		{
			unsigned int iSteps(0);

			if (from == to)
				return iSteps;

			float max = std::numeric_limits<float>::max();
			size_t node_count = m_vNodes.size();
			matrixElement *pElem = ELEM(to, from, m_pMatrix, node_count);
			matrixElementNext *pNext = ELEM(to, from, m_pNextMatrix, node_count);
			if (pElem->m_fCost == max)
				return iSteps;

			if (pNext->m_iNext == -1) //direkte Kante
			{
				Edge *pEdge = m_vNodes[from]->FindEdge(m_vNodes[to]);
				if (pEdge)
				{
					InterlockedIncrement(&(pEdge->m_iUsedCount));
					InterlockedIncrement(&(pEdge->m_pEnd->m_iUsedCount));
					++iSteps;
				}
			}
			else
			{
				size_t intermediate = pNext->m_iNext;

				iSteps += IncreasePathFloydWarshall(from, intermediate, processor);
				iSteps += IncreasePathFloydWarshall(intermediate, to, processor);
			}

			return iSteps;
		}

		unsigned int IncreasePathDijkstra(int from, int to, const int processor)
		{
			unsigned int iSteps(0);

			if (from == to)
				return iSteps;

			matrixElementNext *pNext = ELEM(from, to, m_pNextMatrix, m_vNodes.size());

			size_t intermediate = pNext->m_iNext;
			Edge *pEdge = m_vNodes[from]->FindEdge(m_vNodes[intermediate]);
			if (pEdge) {
				InterlockedIncrement(&(pEdge->m_iUsedCount));
				InterlockedIncrement(&(pEdge->m_pEnd->m_iUsedCount));

				++iSteps;
			}

			return iSteps + IncreasePathDijkstra(intermediate, to, processor);
		}

		unsigned int IncreasePath(int from, int to, const int processor){
			switch (m_computeType)
			{
			case FW::Graph::CPU_FLOYD_WARSHALL:
				return IncreasePathFloydWarshall(from,to,processor);
			case FW::Graph::CPU_DIJKSTRA_ORIGINAL:
			case FW::Graph::CPU_DIJKSTRA_PARALLEL:
			case FW::Graph::CPU_DIJKSTRA_BASIC:
				if (ELEM(from, to, m_pNextMatrix, m_vNodes.size())->m_iNext == -1)
					return 0;
				else
					return IncreasePathDijkstra(from,to,processor);
			default:
				assert(false);
				return -1;
			}
		}

		void ComputeEdgeUsedCounts()
		{
			EdgeVector::iterator l_edge, e_edge;

			//set all to 0
			l_edge = m_vEdges.begin();
			e_edge = m_vEdges.end();
			for(; l_edge!=e_edge; l_edge++)
			{
				Edge *pEdge = *l_edge;
				pEdge->m_iUsedCount = 0;
			}

			NodeVector::iterator l_node, e_node;
			l_node = m_vNodes.begin();
			e_node = m_vNodes.end();
			for(; l_node!=e_node; l_node++)
			{
				Node * pNode = *l_node;
				pNode->m_iUsedCount = 0;
				pNode->m_iUsedStepSum = 0;
			}

			size_t node_count = m_vNodes.size();
			NodeVector *pNodeVector = &m_vNodes;

			omp_set_num_threads( omp_get_num_procs() );
#pragma omp parallel firstprivate(node_count, pNodeVector) default(none)
			{
#pragma omp for
				for(int i = 0; i<(int)node_count; ++i)
					for(size_t j = 0; j<node_count; ++j)
						if (i != (int)j)
						{
							(*pNodeVector)[i]->m_iUsedStepSum += IncreasePath(i,j, omp_get_thread_num());
							//++((*pNodeVector)[i]->m_iUsedCount);
							InterlockedIncrement(&((*pNodeVector)[i]->m_iUsedCount));
						}
			}
		}

		//quickly retrieves the result from the cache if it has been already calculated
		//or calculated the results and inserts into the cache otherwise.
		matrixChildPathEntry getPathLengthWithChilds(Node *pTakeStart, Node *pTakeEnd){

			size_t node_count = m_vNodes.size();
			int index = (pTakeStart->m_iID) * (node_count) + (pTakeEnd->m_iID);
			if (m_childPathsCache != NULL && m_childPathsCache[index].cost != -1){
				return m_childPathsCache[index];
			}


			Node *pBestStart, *pBestEnd, *pChild, *pChild2;
			//richtiges Ende und Start finden
			float cost, min_cost = FLT_MAX;

			pChild = pTakeEnd;
			pChild2 = pTakeStart;
			cost = GetPathLength(pChild2, pChild);
			if (cost < min_cost)
			{
				min_cost = cost;
				pBestStart = pChild2;
				pBestEnd = pChild;
			}

			NodeVector::iterator l_node, e_node;
			NodeVector::iterator l2_node, e2_node;

			l2_node = pTakeStart->m_vChilds.begin();
			e2_node = pTakeStart->m_vChilds.end();
			for(; l2_node!=e2_node; l2_node++)
			{
				pChild2 = *l2_node;
				cost = GetPathLength(pChild2, pChild);
				if (cost < min_cost)
				{
					min_cost = cost;
					pBestStart = pChild2;
					pBestEnd = pChild;
				}
			}

			l_node = pTakeEnd->m_vChilds.begin();
			e_node = pTakeEnd->m_vChilds.end();
			for(; l_node!=e_node; l_node++)
			{
				pChild = *l_node;

				pChild2 = pTakeStart;
				cost = GetPathLength(pChild2, pChild);
				if (cost < min_cost)
				{
					min_cost = cost;
					pBestStart = pChild2;
					pBestEnd = pChild;
				}

				l2_node = pTakeStart->m_vChilds.begin();
				e2_node = pTakeStart->m_vChilds.end();
				for(; l2_node!=e2_node; l2_node++)
				{
					pChild2 = *l2_node;
					cost = GetPathLength(pChild2, pChild);
					if (cost < min_cost)
					{
						min_cost = cost;
						pBestStart = pChild2;
						pBestEnd = pChild;
					}
				}
			}

			matrixChildPathEntry result;
			result.cost = min_cost;
			result.src = pBestStart;
			result.dst = pBestEnd;

			if (m_childPathsCache != NULL) {
				m_childPathsCache[index] = result;
			}

			return result;
		}

		std::vector<double> GetAllPathLengthWithChilds(Node *pFrom, const NodePositionsMap &positions, const std::vector<float> &distanceThresholds, bool isGraphDistance){
			std::vector<double> result(distanceThresholds.size(), 0);
			float max = std::numeric_limits<float>::max();

			Node *pSource = pFrom;
			while(pSource->m_pParent != nullptr)
				pSource = pSource->m_pParent;

			double dist;
			int node_count = m_vNodes.size();

			for(int i = 0; i < node_count; ++i) {
				Node *pDest = m_vNodes[i];
				if (pDest->m_pParent == nullptr){
					matrixChildPathEntry entry = getPathLengthWithChilds(pSource, pDest);

					if (isGraphDistance){
						dist = entry.cost;
					}
					else {
						if (positions.find(pSource->m_iID) == positions.end() || positions.find(pDest->m_iID) == positions.end()){
							throw std::logic_error("expected node with wrong ID");
						}
						dist = (positions.empty()) ? 0 : EuclideanDistance(positions.at(pSource->m_iID), positions.at(pDest->m_iID));							
					}

					if (distanceThresholds[0] == max || dist <= distanceThresholds[0]){

						int t = 0; //find the smallest threshold which is larger than the distance
						while (t < distanceThresholds.size() - 1 && dist <= distanceThresholds[t + 1]) {
							t++;
						}

						result[t] += entry.cost;
					}
				}
			}

			//Collect the results, start from the smallest
			for (int i = distanceThresholds.size() - 2; i >= 0; i--){
				result[i] += result[i + 1];
			}

			return result;
		}

		void ComputeEdgeUsedCountsWithChildsRadius(std::vector<FW::NodeBetweennessVector> *check_nodes, const NodePositionsMap &positions, const std::map<int, int> &parent_to_node_map, const std::vector<float> &distanceThresholds, bool isGraphDistance, bool increase_only_parents = true)
		{	
			int radius_count = check_nodes->size();
			EdgeVector::iterator l_edge, e_edge;

			//set all to 0
			l_edge = m_vEdges.begin();
			e_edge = m_vEdges.end();
			for(; l_edge!=e_edge; l_edge++)
			{
				Edge *pEdge = *l_edge;
				pEdge->m_iUsedCount = 0;
			}

			//initlialize child path cache
			//If we use cache, subsequent queries for GetAllPathLengthWithChilds are about 2x faster
			if (m_childPathsCache == NULL) {
				size_t graph_node_size = m_vNodes.size();
				m_childPathsCache = (matrixChildPathEntry *)malloc(sizeof(matrixChildPathEntry) * graph_node_size * graph_node_size);

				for(int i = 0; i < graph_node_size*graph_node_size; ++i){
					m_childPathsCache[i].cost = -1;
				}
			}

			//// We no longer store the result in the original nodes, therefore reseting nodes is not necessary
			//			NodeVector::iterator l_node, e_node;
			//			l_node = m_vNodes.begin();
			//			e_node = m_vNodes.end();
			//			for(; l_node!=e_node; l_node++)
			//			{
			//				Node * pNode = *l_node;
			//				pNode->m_iUsedCount = 0;
			//				pNode->m_iUsedStepSum = 0;
			//			}

			NodeBetweennessVector pNodeVector = check_nodes->at(0);
			int node_count = check_nodes->at(0).size();
			float max = std::numeric_limits<float>::max();

			omp_set_num_threads( omp_get_num_procs() );
#pragma omp parallel firstprivate(node_count, pNodeVector, max, increase_only_parents, isGraphDistance) default(none) shared(positions, distanceThresholds, radius_count, check_nodes, parent_to_node_map)
			{
#pragma omp for
				for(int i = 0; i < node_count; ++i) {					
					for(int j = 0; j < node_count; ++j) {	

						double dist;
						if (isGraphDistance){
							dist = GetPathLength(pNodeVector[i]->m_node->m_iID, pNodeVector[j]->m_node->m_iID);
						}
						else {				
							dist = (positions.empty()) ? 0 : EuclideanDistance(positions.at(pNodeVector[i]->m_node->m_iID), positions.at(pNodeVector[j]->m_node->m_iID));
								//DISTANCE(positions, i, j);
						}
						
						if (i != (int)j && (distanceThresholds[0] == max || dist <= distanceThresholds[0]))
						{
							Node * pTakeStart, * pTakeEnd, *pBestStart, *pBestEnd;

							int t = 0; //find the smallest threshold which is larger than the distance
							while (t < radius_count - 1 && dist <= distanceThresholds[t + 1])
								t++;
		
							pNodeVector = check_nodes->at(t);
					
							pTakeStart = pNodeVector[i]->m_node;
							pTakeEnd = pNodeVector[j]->m_node;
							matrixChildPathEntry entry = getPathLengthWithChilds(pTakeStart, pTakeEnd);
							float min_cost = entry.cost;
							pBestStart = entry.src;
							pBestEnd = entry.dst;

							if (min_cost < max)
							{
								//we need to locate node with pParent->m_iID in the pNodeVector array							
								int parent_id = parent_to_node_map.at(pTakeStart->m_iID);
								if (pTakeStart->m_iID != pNodeVector[parent_id]->m_node->m_iID){
									throw std::logic_error("expected node with wrong ID");
								}
								if (increase_only_parents)
									pNodeVector[parent_id]->m_iUsedStepSum += IncreaseParentPath(pBestStart, pBestEnd, omp_get_thread_num(), pNodeVector, positions, parent_to_node_map);
								else
									pNodeVector[parent_id]->m_iUsedStepSum += IncreasePath(pBestStart->m_iID, pBestEnd->m_iID, omp_get_thread_num());
								//++(pTakeStart->m_iUsedCount);
								InterlockedIncrement(&(pNodeVector[parent_id]->m_iUsedCount));
							}
						}
					}
				}
			}

			//Collect the results, start from the smallest
			for (int i = radius_count - 2; i >= 0; i--){
				NodeBetweennessVector pNodeVectorLargerRadius = check_nodes->at(i);
				NodeBetweennessVector pNodeVectorSmallerRadius = check_nodes->at(i+1);
				for (int j = 0; j < node_count; j++){
					pNodeVectorLargerRadius[j]->m_iUsedCount += pNodeVectorSmallerRadius[j]->m_iUsedCount;
					pNodeVectorLargerRadius[j]->m_iUsedStepSum += pNodeVectorSmallerRadius[j]->m_iUsedStepSum;
				}
			}
		}

		float GetPathLength(int from, int to)
		{
			if (!m_pMatrix)
			{
				assert(0);
				return -1;
			}

			switch (m_computeType) {
			case FW::Graph::CPU_FLOYD_WARSHALL:
				return ELEM(to, from, m_pMatrix, m_vNodes.size())->m_fCost;
				break;
			case FW::Graph::CPU_DIJKSTRA_ORIGINAL:
			case FW::Graph::CPU_DIJKSTRA_PARALLEL:
			case FW::Graph::CPU_DIJKSTRA_BASIC:
				return ELEM(from, to, m_pMatrix, m_vNodes.size())->m_fCost;
				break;
			default:
				assert(false);
				return -1;			
			}
		}

		float GetPathLength(Node * pFrom, Node *pTo)
		{
			return GetPathLength(pFrom->m_iID, pTo->m_iID);
		}

		//int ComputeMatrixOpenCL();
		//void InitOpenCL();
		//void QuitOpenCL();

		////OpenCL stuff
		//cl_mem *m_dMatrix;
		//cl_mem *m_dNextMatrix;
	};
}

//von Anselm
#include "stdafx.h"
#include "Timer.h"

#include <iostream>
#include <time.h>
#include <windows.h>
#include <mmsystem.h>
#include <math.h>

#define _DEBUG_CONSOLE

struct TimerImpl 
{  
   static const int m_sample_count = 100;
   unsigned long m_sample[m_sample_count];
   int m_sample_pos;
   int m_sample_total;

   TimerImpl(bool print_erg)   
   { 
      this->m_bPrintErg = print_erg;
      this->m_Name = "";
      this->m_sample_total = 0;
      this->m_sample_pos = 0;
      if (timeGetDevCaps (&resolution, sizeof (TIMECAPS)) == TIMERR_NOERROR)
      {
         //printf ("Minimum supported resolution = %d\n", resolution.wPeriodMin);
         //printf ("Maximum supported resolution = %d\n", resolution.wPeriodMax);
      }
      if (timeBeginPeriod (1) == TIMERR_NOERROR)
         start = timeGetTime ();
   }

   TimerImpl(char * name_, bool print_erg)   
   { 
      this->m_bPrintErg = print_erg;
      this->m_Name = name_;
      this->m_sample_total = 0;
      this->m_sample_pos = 0;
      if (timeGetDevCaps (&resolution, sizeof (TIMECAPS)) == TIMERR_NOERROR)
      {
         //printf ("Minimum supported resolution = %d\n", resolution.wPeriodMin);
         //printf ("Maximum supported resolution = %d\n", resolution.wPeriodMax);
      }
      if (timeBeginPeriod (1) == TIMERR_NOERROR)
         start = timeGetTime ();
   }

   unsigned long GetDuration()
   {
      return timeGetTime () - start;
   }

   ~TimerImpl()  
   { 
      finish = timeGetTime ();
      duration = finish - start;
      timeEndPeriod (1);
      if (m_bPrintErg)
         std::cout << "### TIMER ### " << this->m_Name << " took " << duration << " milliseconds" << std::endl; 
   }

   void Restart()
   {
      this->m_sample_total = 0;
      this->m_sample_pos = 0;
      timeEndPeriod (1);
      if (timeBeginPeriod (1) == TIMERR_NOERROR)
         start = timeGetTime ();
   }

   void SetDonePercent(float percent) //0.0 ... 1.0 Percent
   {
      if (m_sample_total < 0xffff)
         ++m_sample_total;
      m_sample[ m_sample_pos ] = (unsigned long)floor((double)GetDuration() / (double)percent + 0.5);
      ++m_sample_pos;
      if (m_sample_pos >= m_sample_count)
         m_sample_pos -= m_sample_count;
   }

   unsigned long GetCompleteDuration()
   {
      unsigned long erg(0);
      int max_pos = m_sample_pos;

      if (m_sample_total > m_sample_count)
         max_pos = m_sample_count;
      for(int z = 0; z < max_pos; ++z)
         erg += m_sample[z];
      if (max_pos > 0)
         erg = (unsigned long)floor((double)erg / (double)max_pos + 0.5);
      return erg;
   }

   TIMECAPS resolution;
   unsigned long start, finish, duration;

   char*   m_Name;
   bool    m_bPrintErg;
};

#ifdef _DEBUG_CONSOLE

Timer::Timer() : pimpl_( new TimerImpl(true)) 
{
}

Timer::Timer(char* name_, bool print_erg) : pimpl_( new TimerImpl(name_, print_erg)) 
{
}

#else

Timer::Timer() : pimpl_( new TimerImpl(false)) 
{
}

Timer::Timer(char* name_, bool print_erg) : pimpl_( new TimerImpl(name_, false)) 
{
}

#endif

Timer::~Timer() 
{ 
   delete pimpl_; 
   pimpl_ = NULL; 
}

unsigned long Timer::GetDuration()
{
   return pimpl_->GetDuration();
}

void Timer::Restart()
{
   pimpl_->Restart();
}

void Timer::SetDonePercent(float percent)
{
   pimpl_->SetDonePercent( percent );
}

unsigned long Timer::GetCompleteDuration()
{
   return pimpl_->GetCompleteDuration();
}

void Timer::SetItQuiet()
{
   pimpl_->m_bPrintErg = false;
}

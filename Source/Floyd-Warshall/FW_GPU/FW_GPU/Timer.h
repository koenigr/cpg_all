//von Anselm
#ifndef __MYTIMER_H__INCLUDED__
#define __MYTIMER_H__INCLUDED__


struct TimerImpl; 

class Timer 
{
   public:
      Timer();
      Timer(char* name_, bool print_erg = true);
      ~Timer();
      unsigned long GetDuration();
      void Restart();
      void SetDonePercent(float percent); //0.0 ... 1.0 Percent
      unsigned long GetCompleteDuration();
      void SetItQuiet();

   private:
      TimerImpl* pimpl_;
};

#endif //__MYTIMER_H__INCLUDED__

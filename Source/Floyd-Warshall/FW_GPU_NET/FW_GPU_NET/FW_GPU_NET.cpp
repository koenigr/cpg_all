// Dies ist die Haupt-DLL.

#include "stdafx.h"

#include "FW_GPU_NET.h"

using namespace FW_GPU_NET;

Edge ^ Node::FindNextEdge( FW::Node * pBaseNextNode )
{
	for each (Edge ^edge in m_vOutgoingEdges)
		if (edge->End->GetBaseNode() == pBaseNextNode)
			return edge;

	return nullptr;
}



// FW_GPU_NET.h

#pragma once

#include "../../FW_GPU/FW_GPU/Floyd_Warshall.h"
#include "../../FW_GPU/FW_GPU/Timer.h"
#include <algorithm>
#include <map>

using namespace System;
using namespace System::Runtime::InteropServices;

namespace FW_GPU_NET
{
	public ref class ScopedHGlobal: public IDisposable
	{
	public:
		ScopedHGlobal(IntPtr p) : ptr(p), disposed(false) { }
		~ScopedHGlobal()
		{
			this->!ScopedHGlobal();
			disposed = true;
		}

		char* c_str()
		{
			return reinterpret_cast<char*>(ptr.ToPointer());
		}

		!ScopedHGlobal()
		{
			Marshal::FreeHGlobal(ptr);
		}

	private:
		System::IntPtr ptr;
		bool disposed;
	};

	public ref class NetTimer: public IDisposable
	{
		Timer *m_pTimer;
		char *m_name;
		String ^m_orgName;
		bool m_bPrint;
		bool disposed;

	public:
		NetTimer(String ^name, bool print_erg): disposed(false)
		{
			ScopedHGlobal s_handle(Marshal::StringToHGlobalAnsi(name));
			char* su = s_handle.c_str();
			m_name = _strdup(su);

			m_pTimer = new Timer(m_name, false);
			m_bPrint = print_erg;
			m_orgName = name;
		}

		void WriteOut()
		{
			if (m_bPrint)
				Console::WriteLine("### TIMER ### "+ m_orgName +" took "+ m_pTimer->GetDuration().ToString() +" milliseconds");
		}

		~NetTimer()
		{
			this->!NetTimer();
			disposed = true;
		}

		!NetTimer()
		{
			if (m_pTimer)
			{
				WriteOut();
				delete m_pTimer;
			}
			if (m_name)
				free(m_name);
		}

		void End()
		{
			WriteOut();
			delete m_pTimer;
			m_pTimer = NULL;
			if (m_name)
				free(m_name);
			m_name = NULL;
		}
	};

	ref class Edge;

	[Serializable]
	public ref class Node: public IDisposable
	{
		FW::Node * m_pNode;
		System::Collections::Generic::List<Edge ^>^ m_vOutgoingEdges;
		System::Collections::Generic::List<Edge ^>^ m_vIncomingEdges;
		Object ^ m_pUserData;

		Node ^ m_pParent;
		System::Collections::Generic::List<Node ^>^ m_vChilds;

		bool disposed;

	public:

		Node(FW::Node * pNode) : m_pNode(pNode), m_pUserData(nullptr), disposed(false)
		{
			m_vOutgoingEdges = gcnew System::Collections::Generic::List<Edge ^>();
			m_vIncomingEdges = gcnew System::Collections::Generic::List<Edge ^>();
			m_vChilds = gcnew System::Collections::Generic::List<Node ^>();

			if (!pNode)
				throw gcnew NotImplementedException();
		}

		~Node()
		{
			this->!Node();
			disposed = true;
		}

		!Node()
		{
			if (m_vOutgoingEdges->Count > 0)
				throw gcnew NotImplementedException();

			if (m_vIncomingEdges->Count > 0)
				throw gcnew NotImplementedException();
		}

		property unsigned int ID
		{
			unsigned int get()
			{
				return m_pNode->m_iID;
			};
		}

		property array<Edge ^>^ OutgoingEdges
		{
			array<Edge ^>^ get()
			{
				array<Edge ^>^ result = gcnew array<Edge ^>(m_vOutgoingEdges->Count);
				int i = 0;

				for each(Edge ^ edge in m_vOutgoingEdges)
					result[i++] = edge;

				return result;
			};
		}

		property array<Edge ^>^ IncomingEdges
		{
			array<Edge ^>^ get()
			{
				array<Edge ^>^ result = gcnew array<Edge ^>(m_vIncomingEdges->Count);
				int i = 0;

				for each(Edge ^ edge in m_vIncomingEdges)
					result[i++] = edge;

				return result;
			};
		}

		property unsigned int UsedCount
		{
			unsigned int get()
			{
				return m_pNode->m_iUsedCount;
			}
		}

		property unsigned int UsedStepSum
		{
			unsigned int get()
			{
				return m_pNode->m_iUsedStepSum;
			}
		}

		property Node ^ Parent
		{
			Node ^get()
			{
				return m_pParent;
			}

			void set(Node ^value)
			{
				//altes Child entfernen
				if (m_pNode->m_pParent)
				{
					m_pParent->m_vChilds->Remove(this);
					m_pNode->m_pParent->m_vChilds.erase(std::find(m_pNode->m_pParent->m_vChilds.begin(), m_pNode->m_pParent->m_vChilds.end(), GetBaseNode()));
				}

				//Parent setzen
				m_pNode->m_pParent = value->GetBaseNode();
				m_pParent = value;

				//neues Child setzen
				m_pParent->m_vChilds->Add(this);
				m_pNode->m_pParent->m_vChilds.push_back(GetBaseNode());
			}
		}

		property array<Node ^>^ Childs
		{
			array<Node ^>^ get()
			{
				array<Node ^>^ result = gcnew array<Node ^>(m_vChilds->Count);
				int i = 0;

				for each(Node ^ node in m_vChilds)
					result[i++] = node;

				return result;
			};

			void set(array<Node ^>^ value)
			{
				m_vChilds->Clear();
				m_pNode->m_vChilds.clear();

				for each(Node ^ node in value)
				{
					m_vChilds->Add(node);
					m_pNode->m_vChilds.push_back(node->GetBaseNode());
				}
			}
		}

		void AddEdge(Edge ^pEdge, bool outgoing)
		{
			if (outgoing)
				m_vOutgoingEdges->Add( pEdge );
			else
				m_vIncomingEdges->Add( pEdge );
		}

		void RemoveEdge(Edge ^pEdge, bool outgoing)
		{
			if (outgoing)
				m_vOutgoingEdges->Remove( pEdge );
			else
				m_vIncomingEdges->Remove( pEdge );
		}

		FW::Node * GetBaseNode()
		{
			return m_pNode;
		}

		Edge ^ FindNextEdge( FW::Node * pBaseNextNode );

		property Object ^ UserData
		{
			Object ^ get()
			{
				return m_pUserData;
			}

			void set(Object ^value)
			{
				m_pUserData = value;
			}
		}
	};

	[Serializable]
	public ref class NodePosition: public IDisposable
	{
		FW::NodePosition * m_pNode;

	public:
		NodePosition(Node ^pNode, double x, double y)
		{
			m_pNode = new FW::NodePosition(pNode->GetBaseNode(), x, y);
		}

		~NodePosition()
		{
			delete m_pNode;
		}

		FW::NodePosition *GetBaseNode()
		{
			return m_pNode;
		}

		double GetX(){
			return m_pNode->m_x;
		}

		double GetY(){
			return m_pNode->m_y;
		}
	};

	[Serializable]
	public ref class NodeBetweenness: public IDisposable
	{
		FW::NodeBetweenness * m_pNode;

	public:
		NodeBetweenness(Node ^pNode)
		{
			m_pNode = new FW::NodeBetweenness(pNode->GetBaseNode());
		}

		~NodeBetweenness()
		{
			delete m_pNode;
		}

		FW::NodeBetweenness * GetBaseNode()
		{
			return m_pNode;
		}

		property unsigned int UsedCount
		{
			unsigned int get()
			{
				return m_pNode->m_iUsedCount;
			}
		}

		property unsigned int UsedStepSum
		{
			unsigned int get()
			{
				return m_pNode->m_iUsedStepSum;
			}
		}
	};

	[Serializable]
	public ref class Edge: public IDisposable
	{
		FW::Edge * m_pEdge;
		Node ^ m_pStart;
		Node ^ m_pEnd;
		Object ^ m_pUserData;

		bool disposed;

	public:

		Edge(FW::Edge * pEdge, Node ^pStart, Node ^pEnd): m_pEdge(pEdge), m_pStart(pStart), m_pEnd(pEnd), m_pUserData(nullptr), disposed(false)
		{
			m_pStart->AddEdge( this, true );
			m_pEnd->AddEdge(this, false);
		}

		~Edge()
		{
			this->!Edge();
			disposed = true;
		}

		!Edge()
		{
			m_pStart->RemoveEdge( this, true );
			m_pEnd->RemoveEdge( this, false );
		}

		property float Cost
		{
			float get()
			{
				return m_pEdge->m_fCost;
			}
			void set(float value)
			{
				m_pEdge->m_fCost = value;
			}
		}

		property Node ^Start
		{
			Node ^get()
			{
				return m_pStart;
			}
		}

		property Node ^End
		{
			Node ^get()
			{
				return m_pEnd;
			}
		}

		property unsigned int UsedCount
		{
			unsigned int get()
			{
				return m_pEdge->GetUndirectedUsedCount();
			}
		}

		property unsigned int DirectedUsedCount
		{
			unsigned int get()
			{
				return m_pEdge->m_iUsedCount;
			}
		}

		property Object ^ UserData
		{
			Object ^ get()
			{
				return m_pUserData;
			}

			void set(Object ^value)
			{
				m_pUserData = value;
			}
		}
	};

	[Serializable]
	public ref class Graph: public IDisposable
	{
		FW::Graph * m_pGraph;

		System::Collections::Generic::List<Edge ^>^ m_vEdges;
		System::Collections::Generic::List<Node ^>^ m_vNodes;

		bool disposed;

	public:

		Graph(): disposed(false)
		{
			m_pGraph = new FW::Graph();

			m_vEdges = gcnew System::Collections::Generic::List<Edge ^>();
			m_vNodes = gcnew System::Collections::Generic::List<Node ^>();
		}

		~Graph()
		{
			this->!Graph();
			disposed = true;
		}

		!Graph()
		{
			ClearGraph();

			delete m_pGraph;
		}

		Node ^ AddNode()
		{
			Node ^pNode = gcnew Node(m_pGraph->AddNode());
			m_vNodes->Add(pNode);
			return pNode;
		}

		Edge ^ AddDirectedEdge(Node ^ pStart, Node ^ pEnd, float fCost)
		{
			Edge ^pEdge = gcnew Edge(m_pGraph->AddDirectedEdge(pStart->GetBaseNode(), pEnd->GetBaseNode(), fCost), pStart, pEnd);
			m_vEdges->Add(pEdge);
			return pEdge;
		}

		array<Edge ^>^ AddEdge(Node ^pNode1, Node ^pNode2, float fCost)
		{
			FW::EdgeVector edge_pair = m_pGraph->AddEdge(pNode1->GetBaseNode(), pNode2->GetBaseNode(), fCost);

			if (edge_pair.size()!=2)
				throw gcnew NotImplementedException();

			Edge ^pEdge1 = gcnew Edge(edge_pair[0], pNode1, pNode2);
			Edge ^pEdge2 = gcnew Edge(edge_pair[1], pNode2, pNode1);

			m_vEdges->Add(pEdge1);
			m_vEdges->Add(pEdge2);

			array<Edge ^>^ result = gcnew array<Edge ^>(2);
			result[0] = pEdge1;
			result[1] = pEdge2;

			return result;
		}

		void ClearGraph()
		{
			for each(Edge ^edge in m_vEdges)
				delete edge;
			m_vEdges->Clear();

			for each(Node ^node in m_vNodes)
				delete node;
			m_vNodes->Clear();

			m_pGraph->ClearGraph();
		}

		//---------------------------------------------------------------------------------------------
		enum class ComputeType
		{
			CPU,
			CPU_FLOYD_WARSHALL = 10,
			CPU_DIJKSTRA_ORIGINAL = 20,
			CPU_DIJKSTRA_PARALLEL = 21,
			CPU_DIJKSTRA_BASIC = 22
		};

		/// <summary>
		/// Computes all pairs shortests paths for this graph.	
		/// </summary>
		/// <param name="computeType">Specifies the type of algorithm used for the computation of the all pairs shortest paths</param>
		int Compute(ComputeType computeType){
			return Compute(computeType, std::numeric_limits<float>::max());
		}

		/// <summary>
		/// Computes all pairs shortests paths for this graph.	
		/// </summary>
		/// <param name="computeType">Specifies the type of algorithm used for the computation of the all pairs shortest paths</param>
		/// <param name="max_path_length">Used to specify maximum path length</param>
		int Compute(ComputeType computeType, float max_path_length)
		{
			int counter(1);

			if (computeType == ComputeType::CPU) {
				//CPU - OpenMP
				m_pGraph->InitMatrix();
				while(m_pGraph->IterateMatrix()) ++counter;
			}
			else if (computeType == ComputeType::CPU_DIJKSTRA_ORIGINAL ||
				computeType == ComputeType::CPU_DIJKSTRA_PARALLEL ||
				computeType == ComputeType::CPU_DIJKSTRA_BASIC) {
					//no need to initialize matrix as this is done as part of Dijkstra
					m_pGraph->Dijkstra( (FW::Graph::ComputeType) computeType, max_path_length);
			}
			else if (computeType == ComputeType::CPU_FLOYD_WARSHALL){
				m_pGraph->InitMatrix();
				m_pGraph->FloydWarshall();
			}
			//else {
			//	//GPU - OpenCL
			//	m_pGraph->InitMatrix();
			//	counter = m_pGraph->ComputeMatrixOpenCL();
			//}

			return counter;
		}

		//---------------------------------------------------------------------------------------------

		property size_t NodeCount
		{
			size_t get()
			{
				return m_vNodes->Count;
			}
		}

		property size_t EdgeCount
		{
			size_t get()
			{
				return m_vEdges->Count;
			}
		}

		property array<Node ^>^ Nodes
		{
			array<Node ^>^ get()
			{
				array<Node ^>^ result = gcnew array<Node ^>(m_vNodes->Count);
				unsigned int i(0);
				for each(Node ^ node in m_vNodes)
					result[i++] = node;
				return result;
			}
		}

		property array<Edge ^>^ Edges
		{
			array<Edge ^>^ get()
			{
				array<Edge ^>^ result = gcnew array<Edge ^>(m_vEdges->Count);
				unsigned int i(0);
				for each(Edge ^ edge in m_vEdges)
					result[i++] = edge;
				return result;
			}
		}

		array<Edge ^>^ GetPath(Node ^ pFrom, Node ^ pTo)
		{
			FW::EdgeVector path;
			FW::EdgeVector::iterator l, e;

			m_pGraph->GetPath(pFrom->GetBaseNode(), pTo->GetBaseNode(), path);

			array<Edge ^>^ result = gcnew array<Edge ^>(path.size());

			int i(0);
			l = path.begin();
			e = path.end();
			Node ^ pWalker = pFrom;
			for(; l!=e; l++)
			{
				FW::Edge * pBaseEdge = *l;
				Edge ^ edge = pWalker->FindNextEdge(pBaseEdge->m_pEnd);
				if (edge == nullptr)
					throw gcnew NotImplementedException();
				result[i++] = edge;
				pWalker = edge->End;
			}

			return result;
		}

		float GetPathLength(Node ^ pFrom, Node ^ pTo)
		{
			return m_pGraph->GetPathLength(pFrom->GetBaseNode(), pTo->GetBaseNode());
		}

		/// <summary>
		/// Computes cummulative length of shortests paths from node pFrom to all other nodes in the graph.
		/// </summary>
		float GetAllPathLength(Node ^pFrom)
		{
			float fResult(0);

			for each(Node ^node in m_vNodes)
				if (node != pFrom)
					fResult += GetPathLength(pFrom, node);

			return fResult;
		}		
		

		//****
		// Helper functions to convert between managed and native C++
		//****
		template <class T>
		inline array<T> ^vector_to_marray(std::vector<T> &vector){
			array<T>^ results = gcnew array<T>(vector.size());
			for (int i = 0; i < vector.size(); i++){
				results[i] = vector[i];
			}

			return results;
		}

		template <class T>
		inline std::vector<T> marray_to_vector(array<T> ^in){
			int length = (in != nullptr) ? in->Length : 0;
			std::vector<T> result(length);
			for (int i = 0; i < length; i++){
				result[i] = in[i];
			}
			return result;
		}

		template <class T>
		inline T* marray_to_array(array<T> ^in){
			IntPtr unmanagedAddr = Marshal::AllocHGlobal(in->Length * sizeof(T));
			Marshal::Copy(in, 0 ,unmanagedAddr, in->Length);
			return (T *) unmanagedAddr.ToPointer();

			//returned pointer needs to be properly freed
			//Marshal::FreeHGlobal(unmanagedAddr);
		}

		//****

		//The old version of GetAllPathLengthWithChilds
		/*
		double GetAllPathLengthWithChilds(Node ^pFrom)
		{
			double fResult(0);

			//Eltern ermitteln von pFrom
			Node ^pSource = pFrom;
			while(pSource->Parent != nullptr)
				pSource = pSource->Parent;

			//alle Eltern ermitteln und durchgehen
			for each(Node ^pDest in m_vNodes)
				if (pDest->Parent == nullptr)
				{
					float fPart(FLT_MAX);
					Node ^ pSourceWalker = pSource;

					fPart = GetPathLength(pSourceWalker, pDest);
					for each(Node ^ pDestWalker in pDest->Childs)
					{
						float fTmp = GetPathLength(pSourceWalker, pDestWalker);
						if (fTmp < fPart)
							fPart = fTmp;
					}

					for each(pSourceWalker in pSource->Childs)
					{
						float fTmp = GetPathLength(pSourceWalker, pDest);
						if (fTmp < fPart)
							fPart = fTmp;

						for each(Node ^ pDestWalker in pDest->Childs)
						{
							fTmp = GetPathLength(pSourceWalker, pDestWalker);
							if (fTmp < fPart)
								fPart = fTmp;
						}
					}

					fResult += fPart;
				}

				return fResult;
		}
		*/


		/// <summary>
		/// Computes cummulative length of shortests paths from node pFrom to all other nodes in the graph.		
		/// In this version instead of shortest path between x and y, a shortests path between x plus its children and y plus its children are considered.
		/// For each vertex it is possible to restrict calculation only to a subset of vertices that are closer than a certain distance (suplied as distanceThresholds parameter).
		/// The distance between the vertices x,y can be calculated in one of two ways: 
		///		1. If the 'positions' parameter is supplied then the distance is calculated as euclidean distance between position[x] and position[y].
		///		2. Otherwise the distance is calcualted as GetPathLength(x,y)
		/// </summary>
		/// <param name="pFrom">The source vertex</param>
		/// <returns>cummulative length of shortests paths from node pFrom to all other nodes in the graph</returns>
		double GetAllPathLengthWithChilds(Node ^pFrom){
			return GetAllPathLengthWithChilds(pFrom, std::numeric_limits<float>::max());
		}

		/// <summary>
		/// Computes cummulative length of shortests paths from node pFrom to all other nodes in the graph.		
		/// In this version instead of shortest path between x and y, a shortests path between x plus its children and y plus its children are considered.
		/// For each vertex it is possible to restrict calculation only to a subset of vertices that are closer than a certain distance (suplied as distanceThresholds parameter).
		/// The distance between the vertices x,y can be calculated in one of two ways: 
		///		1. If the 'positions' parameter is supplied then the distance is calculated as euclidean distance between position[x] and position[y].
		///		2. Otherwise the distance is calcualted as GetPathLength(x,y)
		/// </summary>
		/// <param name="pFrom">The source vertex</param>
		/// <param name="distanceThreshold">
		/// Distance restriction for calculating betweenness centrality.
		/// To calculate values for the whole graph include value 'float.MaxValue'.
		/// </param>
		/// <returns>cummulative length of shortests paths from node pFrom to all other nodes in the graph</returns>
		double GetAllPathLengthWithChilds(Node ^pFrom, float distanceThreshold){
			array<float>^ distanceThresholds = gcnew array<float>(1);
			distanceThresholds[0] = distanceThreshold;
			return GetAllPathLengthWithChilds(pFrom, nullptr, distanceThresholds)[0];
		}

		/// <summary>
		/// Computes cummulative length of shortests paths from node pFrom to all other nodes in the graph.		
		/// In this version instead of shortest path between x and y, a shortests path between x plus its children and y plus its children are considered.
		/// For each vertex it is possible to restrict calculation only to a subset of vertices that are closer than a certain distance (suplied as distanceThresholds parameter).
		/// The distance between the vertices x,y can be calculated in one of two ways: 
		///		1. If the 'positions' parameter is supplied then the distance is calculated as euclidean distance between position[x] and position[y].
		///		2. Otherwise the distance is calcualted as GetPathLength(x,y)
		/// </summary>
		/// <param name="pFrom">The source vertex</param>
		/// <param name="distanceThresholds">
		/// Distance restriction for calculating betweenness centrality.
		/// To calculate values for the whole graph include value 'float.MaxValue'.
		/// </param>
		/// <returns>array of cummulative lengths of shortests paths from node pFrom to all other nodes in the graph. Value at position 'i' in the array corresponds to distance distanceThresholds[i]</returns>
		array<double>^ GetAllPathLengthWithChilds(Node ^pFrom, array<float> ^distanceThresholds){			
			return GetAllPathLengthWithChilds(pFrom, nullptr, distanceThresholds);
		}

		/// <summary>
		/// Computes cummulative length of shortests paths from node pFrom to all other nodes in the graph.		
		/// In this version instead of shortest path between x and y, a shortests path between x plus its children and y plus its children are considered.
		/// For each vertex it is possible to restrict calculation only to a subset of vertices that are closer than a certain distance (suplied as distanceThresholds parameter).
		/// The distance between the vertices x,y can be calculated in one of two ways: 
		///		1. If the 'positions' parameter is supplied then the distance is calculated as euclidean distance between position[x] and position[y].
		///		2. Otherwise the distance is calcualted as GetPathLength(x,y)
		/// </summary>
		/// <param name="pFrom">The source vertex</param>
		/// <param name="positions">
		/// Determines the positions of each node in the graph.
		/// The distance between two vertices x,y in the graph is then euclidean distance of the positions[x] and positions[y]
		/// </param>
		/// <param name="distanceThreshold">
		/// Distance restriction for calculating betweenness centrality.
		/// To calculate values for the whole graph include value 'float.MaxValue'.
		/// </param>
		/// <returns>cummulative length of shortests paths from node pFrom to all other nodes in the graph</returns>
		double GetAllPathLengthWithChilds(Node ^pFrom, array<NodePosition ^>^ positions, float distanceThreshold){
			array<float>^ distanceThresholds = gcnew array<float>(1);
			distanceThresholds[0] = distanceThreshold;
			return GetAllPathLengthWithChilds(pFrom, positions, distanceThresholds)[0];
		}
		
		/// <summary>
		/// Computes cummulative length of shortests paths from node pFrom to all other nodes in the graph.		
		/// In this version instead of shortest path between x and y, a shortests path between x plus its children and y plus its children are considered.
		/// For each vertex it is possible to restrict calculation only to a subset of vertices that are closer than a certain distance (suplied as distanceThresholds parameter).
		/// The distance between the vertices x,y can be calculated in one of two ways: 
		///		1. If the 'positions' parameter is supplied then the distance is calculated as euclidean distance between position[x] and position[y].
		///		2. Otherwise the distance is calcualted as GetPathLength(x,y)
		/// </summary>
		/// <param name="pFrom">The source vertex</param>
		/// <param name="positions">
		/// Determines the positions of each node in the graph.
		/// The distance between two vertices x,y in the graph is then euclidean distance of the positions[x] and positions[y]
		/// </param>
		/// <param name="distanceThresholds">
		/// An array specifying distance restriction for calculating betweenness centrality. 
		/// For performance reasons the distances are required to be in the decreasing order.
		/// To calculate values for the whole graph include value 'float.MaxValue'.
		/// </param>
		/// <returns>array of cummulative lengths of shortests paths from node pFrom to all other nodes in the graph. Value at position 'i' in the array corresponds to distance distanceThresholds[i]</returns>
		array<double>^ GetAllPathLengthWithChilds(Node ^pFrom, array<NodePosition ^>^ positions, array<float>^ distanceThresholds){

			//if (positions == nullptr && !(distanceThresholds->Length == 1 && distanceThresholds[0] == std::numeric_limits<float>::max())){
			//	throw gcnew ArgumentException("possition array is NULL. Unable to compute radius restriction.");
			//}
			
			for (int i = 1; i < distanceThresholds->Length; i++) {
				if (distanceThresholds[i] > distanceThresholds[i-1])
					throw gcnew ArgumentException("array of radius distances must be in decreasing order");
			}

			FW::NodePositionsMap positions_map;
			if (positions != nullptr) {				
				for each(NodePosition^ node in positions) {					
					positions_map[node->GetBaseNode()->m_node->m_iID] = node->GetBaseNode();
				}
			}

			std::vector<float> radiuses = marray_to_vector<float>(distanceThresholds);
			//std::vector<double> pos = marray_to_vector<double>(positions);
			std::vector<double> raw_result = m_pGraph->GetAllPathLengthWithChilds(pFrom->GetBaseNode(), positions_map, radiuses, positions == nullptr);

			return vector_to_marray<double>(raw_result);
		}
		
		/// <summary>
		/// Computes Betweenness centrality in this graph. 
		/// It is equal to the number of shortest paths from all vertices to all other verices that pass through that node.
		/// </summary>
		void ComputeEdgeUsedCounts()
		{
			m_pGraph->ComputeEdgeUsedCounts();
		}

		/// <summary>
		/// Computes Betweenness centrality in this graph. 
		/// It is equal to the number of shortest paths from all vertices to all other verices that pass through that node.
		/// In this version instead of shortest path between x and y, a shortests path between x plus its children and y plus its children is considered.
		/// For each vertex it is possible to restrict calculation of betweenness centrality only to a subset of vertices that are closer than a certain distance (suplied as distanceThresholds parameter).
		/// The distance between the vertices x,y can be calculated in one of two ways: 
		///		1. If the 'positions' parameter is supplied then the distance is calculated as euclidean distance between position[x] and position[y].
		///		2. Otherwise the distance is calcualted as GetPathLength(x,y)
		/// </summary>
		/// <param name="nodes">Contains an array of NodeBetweenness thas holds the results for each value in distanceThresholds</param>
		void ComputeEdgeUsedCountsWithChilds(array<NodeBetweenness ^>^ nodes, bool increase_only_parents){
			ComputeEdgeUsedCountsWithChilds(nodes, increase_only_parents, nullptr, std::numeric_limits<float>::max());
		}

		/// <summary>
		/// Computes Betweenness centrality in this graph. 
		/// It is equal to the number of shortest paths from all vertices to all other verices that pass through that node.
		/// In this version instead of shortest path between x and y, a shortests path between x plus its children and y plus its children is considered.
		/// For each vertex it is possible to restrict calculation of betweenness centrality only to a subset of vertices that are closer than a certain distance (suplied as distanceThresholds parameter).
		/// The distance between the vertices x,y can be calculated in one of two ways: 
		///		1. If the 'positions' parameter is supplied then the distance is calculated as euclidean distance between position[x] and position[y].
		///		2. Otherwise the distance is calcualted as GetPathLength(x,y)
		/// </summary>
		/// <param name="nodes">Contains an array of NodeBetweenness thas holds the results</param>
		/// <param name="distanceThreshold">
		/// Distance restriction for calculating betweenness centrality.
		/// To calculate betweenness centrality for the whole graph include value 'float.MaxValue'.
		/// </param>
		void ComputeEdgeUsedCountsWithChilds(array<NodeBetweenness ^>^ nodes, bool increase_only_parents, float distanceThreshold)
		{
			ComputeEdgeUsedCountsWithChilds(nodes, increase_only_parents, nullptr, distanceThreshold);
		}

		/// <summary>
		/// Computes Betweenness centrality in this graph. 
		/// It is equal to the number of shortest paths from all vertices to all other verices that pass through that node.
		/// In this version instead of shortest path between x and y, a shortests path between x plus its children and y plus its children is considered.
		/// For each vertex it is possible to restrict calculation of betweenness centrality only to a subset of vertices that are closer than a certain distance (suplied as distanceThresholds parameter).
		/// The distance between the vertices x,y can be calculated in one of two ways: 
		///		1. If the 'positions' parameter is supplied then the distance is calculated as euclidean distance between position[x] and position[y].
		///		2. Otherwise the distance is calcualted as GetPathLength(x,y)
		/// </summary>
		/// <param name="nodesArray">Contains an array of NodeBetweenness thas holds the results for each value in distanceThresholds</param>
		/// <param name="distanceThresholds">
		/// An array specifying distance restriction for calculating betweenness centrality. 
		/// For performance reasons the distances are required to be in the decreasing order.
		/// To calculate betweenness centrality for the whole graph include value 'float.MaxValue'.
		/// </param>
		void ComputeEdgeUsedCountsWithChilds(array<array<NodeBetweenness ^>^>^ nodesArray, bool increase_only_parents, array<float>^ distanceThresholds)
		{						
			ComputeEdgeUsedCountsWithChilds(nodesArray, increase_only_parents, nullptr, distanceThresholds);
		}

		/// <summary>
		/// Computes Betweenness centrality in this graph. 
		/// It is equal to the number of shortest paths from all vertices to all other verices that pass through that node.
		/// In this version instead of shortest path between x and y, a shortests path between x plus its children and y plus its children is considered.
		/// For each vertex it is possible to restrict calculation of betweenness centrality only to a subset of vertices that are closer than a certain distance (suplied as distanceThresholds parameter).
		/// The distance between the vertices x,y can be calculated in one of two ways: 
		///		1. If the 'positions' parameter is supplied then the distance is calculated as euclidean distance between position[x] and position[y].
		///		2. Otherwise the distance is calcualted as GetPathLength(x,y)
		/// </summary>
		/// <param name="nodes">Contains an array of NodeBetweenness thas holds the results</param>
		/// <param name="positions">
		/// Determines the positions of each node in the graph.
		/// The distance between two vertices x,y in the graph is then euclidean distance of the positions[x] and positions[y]
		/// </param>
		/// <param name="distanceThreshold">
		/// Distance restriction for calculating betweenness centrality.
		/// To calculate betweenness centrality for the whole graph include value 'float.MaxValue'.
		/// </param>
		void ComputeEdgeUsedCountsWithChilds(array<NodeBetweenness ^>^ nodes, bool increase_only_parents, array<NodePosition ^>^ positions, float distanceThreshold)
		{
			array<array<NodeBetweenness ^>^>^ nodesArray = gcnew array<array<NodeBetweenness ^>^>(1);
			nodesArray[0] = nodes;

			array<float>^ distanceThresholds = gcnew array<float>(1);
			distanceThresholds[0] = distanceThreshold;

			ComputeEdgeUsedCountsWithChilds(nodesArray, increase_only_parents, positions, distanceThresholds);
		}

		/// <summary>
		/// Computes Betweenness centrality in this graph. 
		/// It is equal to the number of shortest paths from all vertices to all other verices that pass through that node.
		/// In this version instead of shortest path between x and y, a shortests path between x plus its children and y plus its children is considered.
		/// For each vertex it is possible to restrict calculation of betweenness centrality only to a subset of vertices that are closer than a certain distance (suplied as distanceThresholds parameter).
		/// The distance between the vertices x,y can be calculated in one of two ways: 
		///		1. If the 'positions' parameter is supplied then the distance is calculated as euclidean distance between position[x] and position[y].
		///		2. Otherwise the distance is calcualted as GetPathLength(x,y)
		/// </summary>
		/// <param name="nodesArray">Contains an array of NodeBetweenness thas holds the results for each value in distanceThresholds</param>
		/// <param name="positions">
		/// Determines the positions of each node in the graph.
		/// The distance between two vertices x,y in the graph is then euclidean distance of the positions[x] and positions[y]
		/// </param>
		/// <param name="distanceThresholds">
		/// An array specifying distance restriction for calculating betweenness centrality. 
		/// For performance reasons the distances are required to be in the decreasing order.
		/// To calculate betweenness centrality for the whole graph include value 'float.MaxValue'.
		/// </param>
		void ComputeEdgeUsedCountsWithChilds(array<array<NodeBetweenness ^>^>^ nodesArray, bool increase_only_parents, array<NodePosition ^>^ positions, array<float>^ distanceThresholds)
		{
			//if (positions == nullptr && !(distanceThresholds->Length == 1 && distanceThresholds[0] == std::numeric_limits<float>::max())){
			//	throw gcnew ArgumentException("possition array is NULL. Unable to compute radius restriction.");
			//}

			if (nodesArray->Length != distanceThresholds->Length){
				throw gcnew ArgumentException("array of output nodes and associated distance must have same size");
			}
			
			for (int i = 1; i < distanceThresholds->Length; i++) {
				if (distanceThresholds[i] > distanceThresholds[i-1])
					throw gcnew ArgumentException("array of radius distances must be in decreasing order");
			}

			std::vector<FW::NodeBetweennessVector> vNodesArray;
			
			for each(array<NodeBetweenness ^>^ nodes in nodesArray) {
				FW::NodeBetweennessVector vNodes;
				for each(NodeBetweenness ^node in nodes)
					vNodes.push_back(node->GetBaseNode());
				vNodesArray.push_back(vNodes);
			}

			std::map<int, int> parent_to_node_map;
			int i = 0;
			for each(NodeBetweenness ^node in nodesArray[0]) {				
				parent_to_node_map[node->GetBaseNode()->m_node->m_iID] = i++;
			}

			FW::NodePositionsMap positions_map;
			if (positions != nullptr) {				
				for each(NodePosition^ node in positions) {					
					positions_map[node->GetBaseNode()->m_node->m_iID] = node->GetBaseNode();
				}
			}

			std::vector<float> radiuses = marray_to_vector<float>(distanceThresholds);
			//std::vector<double> pos = marray_to_vector<double>(positions);
			try {				
				m_pGraph->ComputeEdgeUsedCountsWithChildsRadius(&vNodesArray, positions_map, parent_to_node_map, radiuses, (positions == nullptr), increase_only_parents);
			}
			catch (const std::exception &e){
				throw gcnew ArgumentException(gcnew String(e.what()));
			}
		}
	};
}

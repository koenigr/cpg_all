
#ifndef BOOST_GRAPH_DIJKSTRA_TESTING_DIETMAR
#  define BOOST_GRAPH_DIJKSTRA_TESTING
#endif

#undef NDEBUG 

#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/timer.hpp>
#include <vector>
#include <iostream>
#include <iomanip>

#include <iterator>
#include <utility>
#include <boost/random/uniform_int.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/erdos_renyi_generator.hpp>
#include <boost/detail/lightweight_test.hpp>


#include "../../FW_GPU/FW_GPU/Floyd_Warshall.h"
#include <time.h>
#include <cassert>

using namespace boost;

typedef adjacency_list<vecS, vecS, undirectedS, no_property,
                         property<edge_weight_t, float> > Graph;

int run_test(unsigned n, unsigned m);

int main(int argc, char* argv[])
{
	unsigned n = 10u;
	run_test(n, 5*n);
}

// Verify that the results are equivalent
template<class T>
void verify_result(int n, T *A, T *B, float max_path_length){
	std::cout << "Verifying the results...";
	float max = std::numeric_limits<float>::max();

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (i == j)
				continue;
			//printf("%f %f\n", A[j*n + i], B[j*n + i]);
			//results might not be exactly the same due to the numerical inaccuracies
			assert(A[j*n + i] == max || A[j*n + i] < max_path_length);
			if (A[j*n + i] < max_path_length) {
				assert(abs(A[j*n + i] - B[j*n + i]) < 0.0001);			
			}
		}
	}
	std::cout << " Results verified\n";
}

template<class T>
void verify_result(int n, T *A, T *B){
	verify_result<T>(n,A,B,std::numeric_limits<float>::max());
}

template<class T>
void print_matrix(T *matrix, int n, int max_elem){
	T max = std::numeric_limits<T>::max();

	for (int i = 0; i < max_elem; i++){
		for (int j = 0; j < max_elem; j++)
			std::cout << matrix[i*n + j] << std::setw(3); //((matrix[i*n + j]) == max) ? -1 : matrix[i*n + j];
		std::cout << "\n";
	}
}

void run_test(FW::Graph *fw_graph, FW::Graph::ComputeType type, char *name, float max_path_length){
	timer t;
	
    std::cout << "Running custom Dijkstra's " << name << "..."; std::cout.flush();  t.restart();  
	fw_graph->InitMatrix();
	fw_graph->Dijkstra(type, max_path_length); 
	
	std::cout << t.elapsed() << " seconds.\n";
}

int run_test(unsigned n, unsigned m) { 
  minstd_rand gen(time(NULL));
  std::cout << "Generating graph...\n";

  std::vector<FW::Node *> nodes;
  FW::Graph *fw_graph = new FW::Graph();
  for (int i = 0; i < n; i++)
	  nodes.push_back(fw_graph->AddNode());
  
  // Build random graph      
  double p = double(m)/(double(n)*double(n));
  Graph g(erdos_renyi_iterator<minstd_rand, Graph>(gen, n, p),
          erdos_renyi_iterator<minstd_rand, Graph>(),
          n);
  std::cout << n << " vertices, " << num_edges(g) << " edges.\n";
  uniform_real<float> rand01(0.0, 1.0);
  graph_traits<Graph>::edge_iterator ei, ei_end;

  for (boost::tie(ei, ei_end) = edges(g); ei != ei_end; ++ei) {
	float weight = rand01(gen);
	//printf("%d %d %f\n", source(*ei,g), target(*ei,g), weight);

	put(edge_weight, g, *ei, weight);
	//fw_graph->AddEdgeNoDuplicates(nodes[source(*ei,g)],nodes[target(*ei,g)], weight);
	fw_graph->AddEdge(nodes[source(*ei,g)],nodes[target(*ei,g)], weight);
  }
  std::cout << fw_graph->GetEdgeCount() << "\n";


  timer t;

  
  //----------------------
  //Run BGL dijkstra implementation

    
  float *binary_heap_distances = (float *) malloc(sizeof(float) * n * n);
  float *relaxed_heap_distances = (float *) malloc(sizeof(float) * n * n);  

  int *binary_heap_predecestor = (int *) malloc(sizeof(int) * n * n);
  int *relaxed_heap_predecestor = (int *) malloc(sizeof(int) * n * n);
  

  // Run binary or d-ary heap version
  std::cout << "Running Dijkstra's with binary heap..."; std::cout.flush(); t.restart();
  dijkstra_relaxed_heap = false;
  for (int i = 0; i < n; i++) {
	  dijkstra_shortest_paths(g, vertex(i, g),
						  predecessor_map(&binary_heap_predecestor[i*n]).
                          distance_map(&binary_heap_distances[i*n]));
  }  
  double binary_heap_time = t.elapsed();
  std::cout << binary_heap_time << " seconds.\n";
  print_matrix<int>(binary_heap_predecestor,n,10);
	//for (int i = 0; i < 10; i++){
	//	for (int j = 0; j < 10; j++)
	//		printf("%d ", binary_heap_predecestor[i*n + j]);
	//	printf("\n");
	//}

  // Run relaxed heap version
#ifdef BOOST_GRAPH_DIJKSTRA_USE_RELAXED_HEAP
  std::cout << "Running Dijkstra's with relaxed heap..."; std::cout.flush(); t.restart();
#else
  std::cout << "Running Dijkstra's with d-ary heap (d=4)..."; std::cout.flush(); t.restart();
#endif
   
  dijkstra_relaxed_heap = true;
  for (int i = 0; i < n; i++) {
	dijkstra_shortest_paths(g, vertex(i, g),
						  predecessor_map(&relaxed_heap_predecestor[i*n]).
                          distance_map(&relaxed_heap_distances[i*n]));
  }
  double relaxed_heap_time = t.elapsed();
  std::cout << relaxed_heap_time << " seconds.\n"; 
  print_matrix<int>(relaxed_heap_predecestor,n,10);
 // 	for (int i = 0; i < 10; i++){
	//	for (int j = 0; j < 10; j++)
	//		printf("%d ", relaxed_heap_predecestor[i*n + j]);
	//	printf("\n");
	//}

  //------------------------

  //Try all different implementations to see whether they produce correct results
  float max = std::numeric_limits<float>::max();
  float max_path_length = max;

  run_test(fw_graph, FW::Graph::ComputeType::CPU_DIJKSTRA_ORIGINAL, "CPU_DIJKSTRA_ORIGINAL", max_path_length);
  print_matrix<int>((int *)fw_graph->GetNextMatrix(),n,10);
  	for (int i = 0; i < 10; i++){
		for (int j = 0; j < 10; j++)
			printf("%.3f\t", (((float *)fw_graph->GetMatrix())[i*n + j]) == max ? -1 : ((float *)fw_graph->GetMatrix())[i*n + j]);
		printf("\n");
	}
  verify_result<float>(n, binary_heap_distances, (float *) fw_graph->GetMatrix(), max_path_length);
  //verify_result<int>(n, binary_heap_predecestor, (int *) fw_graph->GetNextMatrix());
  
 /* for (int i = 0; i < 10; i++){
		for (int j = 0; j < 10; j++)
			printf("%d ", ((int *)fw_graph->GetNextMatrix())[i*n + j]);
		printf("\n");
	}*/

 // run_test(fw_graph, FW::Graph::ComputeType::CPU_DIJKSTRA_BASIC, "CPU_DIJKSTRA_BASIC", max_path_length);
 // print_matrix<int>((int *)fw_graph->GetNextMatrix(),n,10);

 // verify_result<float>(n, binary_heap_distances, (float *) fw_graph->GetMatrix(), max_path_length);
 // //verify_result<int>(n, binary_heap_predecestor, (int *) fw_graph->GetNextMatrix());
 //

	//run_test(fw_graph, FW::Graph::ComputeType::CPU_DIJKSTRA_OPTIMIZED, "CPU_DIJKSTRA_OPTIMIZED", max_path_length);
	//verify_result<float>(n, binary_heap_distances, (float *) fw_graph->GetMatrix(), max_path_length);
	////verify_result<int>(n, binary_heap_predecestor, (int *) fw_graph->GetNextMatrix());
 //
 // 

 // run_test(fw_graph, FW::Graph::ComputeType::CPU_DIJKSTRA_PARALLEL, "CPU_DIJKSTRA_PARALLEL", max_path_length);
 // verify_result<float>(n, binary_heap_distances, (float *) fw_graph->GetMatrix(), max_path_length);
 //// verify_result<int>(n, binary_heap_predecestor, (int *) fw_graph->GetNextMatrix());


  //-------------
	
    std::cout << "Running custom Floyd-Warshall ...\n"; std::cout.flush();  t.restart();  
	fw_graph->InitMatrix();
	//print_matrix<float>((float *)fw_graph->GetMatrix(),n,10);
	fw_graph->IterateMatrix(); 
	
	std::cout << t.elapsed() << " seconds.\n";
	print_matrix<int>((int *)fw_graph->GetNextMatrix(),n,10);
	for (int i = 0; i < 10; i++){
		for (int j = 0; j < 10; j++)
			printf("%.3f\t", (((float *)fw_graph->GetMatrix())[i*n + j]) == max ? -1 : ((float *)fw_graph->GetMatrix())[i*n + j]);
		printf("\n");
	}
	verify_result<float>(n, binary_heap_distances, (float *) fw_graph->GetMatrix(), max_path_length);
	//-------------

  free(binary_heap_distances); free(binary_heap_predecestor);
  free(relaxed_heap_distances); free(relaxed_heap_predecestor);
  int result = boost::report_errors();

  std::cout << "press any key to continue ...\n";
  getchar();  
  return result;
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FW_GPU_NET;
using System.IO;

namespace FW_NET_Example
{
    class FreeUserClass
    {
        float m_x, m_y;
    
        public FreeUserClass(float x, float y)
        {
            m_x = x;
            m_y = y;
        }
    }

    class Program
    {
        static Node[] node;
        static Edge[] edge;

        static void DefineExample(Graph graph)
        {
            node = new Node[6];

            for (int i = 0; i < 6; ++i)
            {
                node[i] = graph.AddNode();
                node[i].UserData = new FreeUserClass(1, 2);
            }

            graph.AddDirectedEdge(node[0], node[1], 4);
            graph.AddDirectedEdge(node[0], node[3], 5);
            graph.AddDirectedEdge(node[0], node[4], 5);
            graph.AddDirectedEdge(node[1], node[2], 7);
            graph.AddDirectedEdge(node[1], node[3], 3);
            graph.AddDirectedEdge(node[2], node[5], 4);
            graph.AddDirectedEdge(node[3], node[2], 3);
            graph.AddDirectedEdge(node[3], node[4], 4);
            graph.AddDirectedEdge(node[3], node[5], 3);
            graph.AddDirectedEdge(node[3], node[0], 7);
            graph.AddDirectedEdge(node[4], node[0], 2);
            graph.AddDirectedEdge(node[4], node[3], 6);
            graph.AddDirectedEdge(node[5], node[3], 2);
            graph.AddDirectedEdge(node[5], node[4], 1);
        }

        static void DefineExample2(Graph graph)
        {
            node = new Node[13];

            for (int i = 0; i < 13; ++i)
                node[i] = graph.AddNode();

            //node[0].Childs = new Node[] { node[4], node[5] };
            node[4].Parent = node[0];
            node[5].Parent = node[0];
            //node[1].Childs = new Node[] { node[6], node[7], node[8] };
            node[6].Parent = node[1];
            node[7].Parent = node[1];
            node[8].Parent = node[1];
            //node[2].Childs = new Node[] { node[9], node[10] };
            node[9].Parent = node[2];
            node[10].Parent = node[2];
            //node[3].Childs = new Node[] { node[11], node[12] };
            node[11].Parent = node[3];
            node[12].Parent = node[3];
 
            graph.AddDirectedEdge(node[0], node[4], 0);
            graph.AddDirectedEdge(node[0], node[5], 0);
            graph.AddDirectedEdge(node[1], node[6], 0);
            graph.AddDirectedEdge(node[1], node[7], 0);
            graph.AddDirectedEdge(node[1], node[8], 0);
            graph.AddDirectedEdge(node[2], node[9], 0);
            graph.AddDirectedEdge(node[2], node[10], 0);
            graph.AddDirectedEdge(node[3], node[11], 0);
            graph.AddDirectedEdge(node[3], node[12], 0);
            graph.AddDirectedEdge(node[4], node[7], 1);
            graph.AddDirectedEdge(node[4], node[8], 1);
            graph.AddDirectedEdge(node[5], node[12], 1);
            graph.AddDirectedEdge(node[6], node[5], 1);
            graph.AddDirectedEdge(node[7], node[10], 1);
            graph.AddDirectedEdge(node[8], node[11], 1);
            graph.AddDirectedEdge(node[9], node[6], 1);
            graph.AddDirectedEdge(node[9], node[8], 1);
            graph.AddDirectedEdge(node[11], node[4], 1);
            graph.AddDirectedEdge(node[12], node[6], 1);
            graph.AddDirectedEdge(node[12], node[7], 1);
        }

        static float GetInverseCost(Edge inputEdge1, Edge inputEdge2, Node inputConnectionNode)
        {
            /*
             * Die Koordinaten zum richtigen berechnen der Winkelkosten sollten mittels inputNodes UserData (z.B. x,y) erfolgen
             * 
             * bitte nicht inputEdge UserData verwenden, da es vom GenerateInverseDoubleGraph Algorithmus verwendet wird
             */
/*
            if (((inputEdge1 == edge[0 * 2]) || (inputEdge2 == edge[2 * 2])) ||
                ((inputEdge2 == edge[0 * 2]) || (inputEdge1 == edge[2 * 2]))) return 1;
            if (((inputEdge1 == edge[0 * 2]) || (inputEdge2 == edge[1 * 2])) ||
                ((inputEdge2 == edge[0 * 2]) || (inputEdge1 == edge[1 * 2]))) return 0;
            if (((inputEdge1 == edge[1 * 2]) || (inputEdge2 == edge[2 * 2])) ||
                ((inputEdge2 == edge[1 * 2]) || (inputEdge1 == edge[2 * 2]))) return 0.25f;

            if (((inputEdge1 == edge[0 * 2 + 1]) || (inputEdge2 == edge[2 * 2 + 1])) ||
                ((inputEdge2 == edge[0 * 2 + 1]) || (inputEdge1 == edge[2 * 2 + 1]))) return 1;
            if (((inputEdge1 == edge[0 * 2 + 1]) || (inputEdge2 == edge[1 * 2 + 1])) ||
                ((inputEdge2 == edge[0 * 2 + 1]) || (inputEdge1 == edge[1 * 2 + 1]))) return 0;
            if (((inputEdge1 == edge[1 * 2 + 1]) || (inputEdge2 == edge[2 * 2 + 1])) ||
                ((inputEdge2 == edge[1 * 2 + 1]) || (inputEdge1 == edge[2 * 2 + 1]))) return 0.25f;

            throw new NotImplementedException();
 */
            return 1;
        }
        
        static Graph GenerateInverseDoubleGraph(Graph input)
        {
            Graph output = new Graph();

            //Knoten für jede input-Kante erzeugen (es sind alles gerichtete Kanten)
            foreach (Edge edge in input.Edges)
            {
                Node node = output.AddNode();

                //dem Knoten seine input-Kante mitgeben und umgekehrt
                node.UserData = edge;
                edge.UserData = node;
            }
            
            //Parent-Child Zuordnung
            Node[] outputNodes = output.Nodes;
            for (int i = 0; i < outputNodes.Length; ++i)
                for (int j = i + 1; j < outputNodes.Length; ++j)
                {
                    Edge inputEdge1 = (Edge)outputNodes[i].UserData;
                    Edge inputEdge2 = (Edge)outputNodes[j].UserData;
                    if ((inputEdge1.Start == inputEdge2.End) &&
                        (inputEdge2.Start == inputEdge1.End)) //selbe edge als Node Grundlage
                        outputNodes[j].Parent = outputNodes[i];
                }

            //alle vorwärtsgerichteten Kanten im output hinzufügen
            foreach (Node outputNode in output.Nodes)
            {
                //Zielknoten der zugeordneten input-Edge finden
                Edge inputEdge = (Edge)outputNode.UserData;
                Node aimNode = inputEdge.End;

                //aus allen rausgehenden Kanten des aimNode inputEdge-Richtung entfernen
                Edge[] outgoing = aimNode.OutgoingEdges;
                //System.Collections.Generic.List<Edge> inputEdgeList = new System.Collections.Generic.List<Edge>();
                foreach (Edge edge in outgoing)
                {
                    if (!(edge.End == inputEdge.Start))
                    {
                        //inputEdgeList.Add(edge);               

                        //neue output-Kante einfügen
                        Node outputAimNode = (Node)edge.UserData;
                        output.AddDirectedEdge(outputNode, outputAimNode, GetInverseCost(inputEdge, edge, aimNode));
                    }
                }
            }

            return output;
        }
        
        static void DefineExample3(ref Graph graph)
        {
            //Orginal Straßengraphen definieren
            node = new Node[5];
            for (int i = 0; i < 5; ++i)
                node[i] = graph.AddNode();

            /*     (0)---(1)---(2)---(4)
             *           /
             *          /
             *        (3)
             */

            graph.AddEdge(node[0], node[1], 1);
            graph.AddEdge(node[1], node[2], 1);
            graph.AddEdge(node[1], node[3], 1);
            graph.AddEdge(node[2], node[4], 1);

            edge = graph.Edges;

            //"inversen" Doppelgraph daraus erzeugen
            Graph graph2 = GenerateInverseDoubleGraph(graph);

            //graph durch graph2 ersetzen
            graph = graph2;
            node = graph.Nodes;
        }
        
        static void LoadGraph(String filename, Graph graph)
        {
            StreamReader streamReader = new StreamReader(filename);

            String text = streamReader.ReadLine();
            String[] split = text.Split(' ');
            int node_count;
            Int32.TryParse(split[0], out node_count);
            Console.WriteLine(text);

            text = streamReader.ReadLine();
            split = text.Split(' ');
            int edge_count;
            Int32.TryParse(split[0], out edge_count);
            Console.WriteLine(text);

            text = streamReader.ReadLine();
            Console.WriteLine(text);

            node = new Node[node_count];
            for (int i = 0; i < node_count; ++i)
                node[i] = graph.AddNode();

            while (!streamReader.EndOfStream)
            {
                text = streamReader.ReadLine();
                split = text.Split(',');
                int start, end;
                double weight;

                Int32.TryParse(split[0], out start);
                Int32.TryParse(split[1], out end);
                Double.TryParse(split[2], out weight);

                graph.AddEdge(node[start], node[end], (float)weight);
            }

            streamReader.Close();
        }

        static void Main(string[] args)
        {
            Graph graph = null;
            for (int i = 0; i < 500; i++)
            {
                graph = new Graph();
                DefineExample3(ref graph);

                //foreach (FW_GPU_NET.Edge cEdge in graph.Edges)
                //{
                //    //cEdge.UserData = null;
                //    cEdge.Dispose();
                //}
                //foreach (FW_GPU_NET.Node cNode in graph.Nodes)
                //{
                //    //cNode.UserData = null;
                //    cNode.Dispose();
                //}

                graph.ClearGraph();
                graph.Dispose();
                graph = null;

                //GC.Collect();
                //GC.WaitForPendingFinalizers();
            }
            graph = new Graph();

            //DefineExample(graph);
            //DefineExample2(graph);
            //(ref graph);
            //LoadGraph("../../../../Graphen/GraphL.txt", graph);
            //LoadGraph("../../../../Graphen/GraphS.txt", graph);
            LoadGraph("TestGraph01.txt", graph);
           // Graph graph2 = GenerateInverseDoubleGraph(graph);

            //graph durch graph2 ersetzen
           // graph = graph2;

            Console.WriteLine();
            Console.WriteLine("There are " + graph.NodeCount.ToString() + " Nodes and " + graph.EdgeCount.ToString() + " Edges in the Graph.");

            NetTimer local = new NetTimer("Compute CPU", true);
            graph.Compute(Graph.ComputeType.CPU_DIJKSTRA_ORIGINAL);

            //local.End();
            //local = new NetTimer("Compute GPU", true);
            //graph.Compute(Graph.ComputeType.GPU);

            local.End();
            local = new NetTimer("ComputeEdgeUsedCountsWithChilds CPU", true);

            //graph.ComputeEdgeUsedCounts();

            //alle Parents rausfiltern
            System.Collections.Generic.List<Node> test_nodes = new System.Collections.Generic.List<Node>();
            foreach (Node testNode in graph.Nodes)
                if (testNode.Parent == null)
                    test_nodes.Add(testNode);

            graph.ComputeEdgeUsedCountsWithChilds(test_nodes.ToArray(), true);

            local.End();

            Edge[] path = graph.GetPath(node[2], node[1]);
            float fPathCost = graph.GetPathLength(node[2], node[1]);
            float fAllPathCost = graph.GetAllPathLength(node[2]);
            float fAllPathCost2 = graph.GetAllPathLengthWithChilds(node[0]);

            Console.Write("press any key to continue ...");
            Console.ReadKey();
        }
    }
}

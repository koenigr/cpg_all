﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GeoAlgorithms2D.cs 
//  Copyright (C) 2014/10/18  12:58 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using ClipperLib;
using MIConvexHull;
using Point = System.Drawing.Point;
using OpenTK;
using GeoAPI.Geometries;
using NetTopologySuite.Operation.Polygonize;
using NetTopologySuite.Geometries;


namespace CPlan.Geometry
{
    using PolygonI = List<IntPoint>;
    using PolygonsI = List<List<IntPoint>>;
    using PolygonD = List<Vector2d>;
    
    [Serializable]
    public class GeoAlgorithms2D
    {
        public enum IntersectionType : int
        {
            NoIntersection = 0,
            Touches = 1,
            Intersects = 2
        }

        private static double factorDoubleToInt = 100000;
        private static double thresholdDoubleToInt = 10000;

        /// <summary>
        /// Convert Degree to Radion.
        /// </summary>
        /// <param name="angle">The angle in degree.</param>
        /// <returns>The angle in radian.</returns>
        public static double DegreeToRadian(double angle)
        {
            return Math.PI * angle / 180.0;
        }

        /// <summary>
        /// Convert Radian to Degree.
        /// </summary>
        /// <param name="angle">The angle in radians.</param>
        /// <returns>The angle in degree.</returns>
        public static double RadianToDegree(double angle)
        {
            return angle * (180.0 / Math.PI);
        }

        /// <summary>
        /// Convert Polar Coordinates to Cartesian Coordinates.
        /// </summary>
        /// <param name="degrees">Angle</param>
        /// <param name="radius">Radius</param>
        /// <returns>(X,Y) Point</returns>
        public static Vector2d PolarToCartesian(double degrees, double radius)
        {
            //double dividor = Math.PI / 180.0;
            double radians = DegreeToRadian(degrees); //degrees * dividor;
            Vector2d result = new Vector2d();
            result.X = Convert.ToSingle( Math.Cos(radians) * radius);
            result.Y = Convert.ToSingle( Math.Sin(radians) * radius);
            return result;
        }

        /// <summary>
        /// Converting Cartesian Coordinates to Polar Coordinates.
        /// </summary>
        /// <param name="p">point</param>
        /// <param name="angle">out angle in degree</param>
        /// <param name="radius">out radius</param>
        public static void CartesianToPolar(Vector2d p, out float angle, out float radius)
        {
            double tmpAngle;
            radius = Convert.ToSingle( Math.Sqrt((p.X * p.X) + (p.Y * p.Y)));
            double div = Math.PI / 2.0;
            if (Math.Abs(p.X) < 0.000005)
            {//Direct Up or Down
                if (p.Y > 0.0)
                    tmpAngle = div; //up 90deg 
                else if (p.Y < 0.0)
                    tmpAngle = 3 * div; // or angle = -div;
                else
                    tmpAngle = 0.0; // when X = 0 and Y = 0
            }
            else
            {
                tmpAngle = Math.Atan(p.Y / p.X);
                //correct for minus X or y
                if (p.X < 0)
                    tmpAngle += (2 * div); //ii OR iii 90 to 180 degrees or 180 to 270 degrees
                else if (p.Y < 0.0)
                    tmpAngle += (4 * div); //iv 270 to 360 degrees
            }
            //Convert Rad to Deg
            angle = Convert.ToSingle(RadianToDegree(tmpAngle)); //(int)Math.Round(angle * (180 / Math.PI));
        }

        /// <summary>
        /// Convert a Vector2d to a IntPoint.
        /// </summary>
        /// <param name="vec">The Vector2d.</param>
        /// <param name="scale">The scaling factor.</param>
        /// <returns>The IntPoint.</returns>
        public static IntPoint Vector2DToIntPoint(Vector2d vec, double scale = 1)
        {
            IntPoint intPoint = new IntPoint((long)(scale * vec.X), (long)(scale * vec.Y));
            return intPoint;
        }

        /// <summary>
        /// Triangulates a polygon.
        /// </summary>
        /// <param name="poly">The polygon to be triangulated.</param>
        /// <returns>Array of triangles.</returns>
        //public static Triangle3D[] Triangulate2D(Poly2D poly)
        //{
        //    CGeom3d.Triangle[] result = new CGeom3d.Triangle[0];

        //    COpenGLTesselator.Tesselate(poly.PointsVec3D, ref result);

        //    Triangle3D[] result2 = new Triangle3D[result.Length];
        //    int index = 0;
        //    foreach (CGeom3d.Triangle tri in result)
        //    {
        //        result2[index] = new Triangle3D(tri.m_pP1, tri.m_pP2, tri.m_pP3);
        //        ++index;
        //    }
        //    return result2;
        //}

        /// <summary>
        /// Offset a polygon.
        /// </summary>
        /// <param name="points">Polygon points to be offseted.</param>
        /// <param name="delta">Offset value.</param>
        /// <returns></returns>
        public static Vector2d[] OffsetPoly(Vector2d[] points, double delta)
        {
            double faktor = factorDoubleToInt;
            if (Area(points.ToList()) > thresholdDoubleToInt)
                faktor = 1.0;
            PolygonI pO = new PolygonI();
            PolygonsI pOff = new PolygonsI();
            PolygonsI offsetPoly = new PolygonsI();

            for (int i = 0; i < points.Length; i++)
            {
                pO.Add(new IntPoint(Convert.ToInt32(points[i].X * faktor), Convert.ToInt32(points[i].Y * faktor)));
            }

            bool orient = Clipper.Orientation(pO);
            if (!orient)
            {
                pO = Reverse(pO);
            }

            pOff.Add(pO);
            delta *= faktor;

            ClipperOffset co = new ClipperOffset();
            co.AddPaths(pOff, JoinType.jtMiter, EndType.etClosedPolygon);
            co.Execute(ref offsetPoly, delta);

            //PolygonsI offsetPoly = Clipper.OffsetPolygons(pOff, delta, JoinType.jtMiter, 3);

            if (offsetPoly.Count > 0) // there is a clipping polygon
            {
                Vector2d[] offsetPoints = new Vector2d[offsetPoly[0].Count];
                for (int i = 0; i < offsetPoly[0].Count; i++)
                {
                    offsetPoints[i] = new Vector2d(Convert.ToDouble(offsetPoly[0][i].X / faktor), Convert.ToDouble(offsetPoly[0][i].Y / faktor));
                }
                return offsetPoints;
            }
            else
                return null;
       }

        //==============================================================================================
        /// <summary>
        /// Reverse the polygon. From clockwise to counterclockwise and vice versa.
        /// </summary>
        public static PolygonI Reverse(PolygonI poly)
        {
            PolygonI newPoly = new PolygonI();
            int nrPts = poly.Count;
            IntPoint[] newPoints = new IntPoint[nrPts];
            for (int i = 0; i < nrPts; i++)
            {
                newPoints[nrPts - 1 - i] = poly[i];
            }
            for (int i = 0; i < nrPts; i++)
            {
                newPoly.Add(newPoints[i]);
            }
            return newPoly;
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Find all intersection points between a line and a set of lines.
        /// </summary>
        /// <param name="line">The line to test the intersection.</param>
        /// <param name="lineSet">The line set.</param>
        /// <param name="intersectLines"> Optional: returns the lines that are intersected. </param>
        /// <returns>List of intersection points.</returns>
        public static List<Vector2d> IntersectLines(Line2D line, IEnumerable<Line2D> lineSet, List<Line2D> intersectLines = null)
        {
            List<Vector2d> intersetionPoints = new List<Vector2d>();
            if (intersectLines == null) intersectLines = new List<Line2D>();
            if (!lineSet.Any()) return intersetionPoints;
            foreach (Line2D testLine in lineSet)
            {
                //int interTest;
                Vector2d interPt, closeP1, closeP2 = new Vector2d();
                bool linesIntersect, segmentsIntersect;
                LineIntersection(line.Start, line.End, testLine.Start, testLine.End, out linesIntersect, out segmentsIntersect, out interPt, out closeP1, out closeP2);

                // --- uncommented lines used GeoObj (PureGeom3D) ---
                //interTest = LineIntersection(line.Start, line.End, testLine.Start, testLine.End, ref interPt);   
                //if (interTest == 2) // there is an intersection (excluding possible common start and end points)
                if (segmentsIntersect) // intersection 
                {
                    if (interPt != line.Start && interPt != line.End)
                    {
                        intersetionPoints.Add(interPt);
                        intersectLines.Add(testLine);
                    }
                }
            }
            return intersetionPoints;
        }


        //=====================================================================================================================================================
        /// <summary>
        /// Finds the nearest neighbour of a point set to a given point.
        /// </summary>
        /// <param name="ptSet">Point set</param>
        /// <param name="curPt">Point</param>
        /// <returns> The nearest neighbour point.</returns>     
        public static Vector2d NearestNeighbour(IEnumerable<Vector2d> ptSet, Vector2d curPt)
        {
            if (curPt == null) throw new ArgumentNullException("curPt");
            List<Vector2d> inRect;
            double width;
            Vector2d nearestPt = new Vector2d();
            Rect2D boundingBox = BoundingBox(ptSet.Union(new[] { curPt }).ToArray());
            double width0 = Math.Sqrt(boundingBox.Width);
            double height0 = Math.Sqrt(boundingBox.Height);
            int cter = 1;

            // --- consider only points inside a box around the base point to avoid expensive distance calculations ---
            do
            {
                width = width0 * cter;
                double height = height0 * cter;
                Rect2D searchRect = new Rect2D(curPt.X - width / 2, curPt.Y + height / 2, width, height);
                inRect = new List<Vector2d>();
                // --- test which points are inside the search rectangle --------------------------------
                foreach (Vector2d testPt in ptSet)
                {
                    if (PointInRect(testPt, searchRect)) inRect.Add(testPt);
                }
                cter++;
            } while ((inRect.Count <= 0) && (width <= boundingBox.Width*2));

            // --- find the shortest distance and the nearest neighbout ---
            if (inRect.Count > 0)
            {
                double minDist = double.MaxValue;
                foreach (Vector2d testPt in inRect)
                {
                    double dist = DistancePoints(testPt, curPt);
                    if (dist < minDist)
                    {
                        minDist = dist;
                        nearestPt = testPt;
                    }
                }
            }
            return nearestPt;
        }

        public static int NearestPoint(IList<Vector2d> points, Vector2d q)
        {
            if (points == null || points.Count == 0)
                throw new ArgumentNullException("points");

            Vector2d vector = q - points[0];
            double num1 = vector.LengthSquared;
            if (num1 == 0) return 0;
            int index = 1;
            int num2 = 0;
            while (index < points.Count)
            {
                vector = q - points[index];
                double lengthSquared = vector.LengthSquared;
                if (num1 > lengthSquared)
                {
                    if (lengthSquared == 0.0)
                        return index;
                    num1 = lengthSquared;
                    num2 = index;
                }
                checked { ++index; }
            }
            return num2;

            //for (int i = 1; i < points.Count; i++)
            //{
            //    vector = q - points[i];
            //    double distance = vector.LengthSquared;

            //    if (minDistance > distance)
            //    {
            //        if (distance == 0) return i;
            //        minDistance = distance;
            //        minIndex = i;
            //    }
            //}

            //return minIndex;
        }
        public static int NearestNeighbourIdx(Vector2d[] ptSet, Vector2d curPt)
        {
            if (curPt == null) throw new ArgumentNullException("curPt");
            List<Vector2d> inRect;
            double width;
            
            List<Vector2d> allPts = ptSet.ToList();
            allPts.Add(curPt);
            Rect2D boundingBox = BoundingBox(allPts.ToArray());
            double width0 = Math.Sqrt(boundingBox.Width);
            double height0 = Math.Sqrt(boundingBox.Height);
            int cter = 1;

            // --- consider only points inside a box around the base point to avoid expensive distance calculations ---
            do
            {
                width = width0 * cter;
                double height = height0 * cter;
                Rect2D searchRect = new Rect2D(curPt.X - width / 2, curPt.Y + height / 2, width, height);
                inRect = new List<Vector2d>();
                // --- test which points are inside the search rectangle --------------------------------
                foreach (Vector2d testPt in ptSet)
                {
                    if (PointInRect(testPt, searchRect)) inRect.Add(testPt);
                }
                cter++;
            } while ((inRect.Count <= 0) && (width <= boundingBox.Width * 2));

            // --- find the shortest distance and the nearest neighbout ---
            int nearestPtIdx = 0;
            if (inRect.Count > 0)
            {
                double minDist = double.MaxValue;
               
                int nearestCount = 0;
                foreach (Vector2d testPt in inRect)
                {
                    double dist = DistancePoints(testPt, curPt);
                    if (dist < minDist)
                    {
                        minDist = dist;
                        nearestPtIdx = nearestCount;
                    }
                    nearestCount++;
                }
            }
            return nearestPtIdx;
        }
        //=====================================================================================================================================================
        /// <summary>
        /// Find the nearest line to a point.
        /// </summary>
        /// <param name="linesSet">Set of lines</param>
        /// <param name="point">Point</param>
        /// <param name="distance">Returns the minimal distance</param>
        /// <returns>The nearest line</returns>
        public static Line2D NearestLineToPoint(IEnumerable<Line2D> linesSet, Vector2d point, out double distance)
        {
            Vector2d notNeeded;
            Line2D nearestLine = null;
            double minDist = double.MaxValue;
            foreach (Line2D testLine in linesSet)
            {
                //double dist = DistancePointToLine(testLine, point);
                double dist = DistancePointToSegment(point, testLine.Start, testLine.End, out notNeeded);
                if (dist < minDist)
                {
                    minDist = dist;
                    nearestLine = testLine;
                }
            }
            distance = minDist;
            return nearestLine;
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Find the nearest point to a line.
        /// </summary>
        /// <param name="pointsSet"> Set of points </param>
        /// <param name="line"> Line </param>
        /// <param name="distance"> Returns the minimal distance. </param>
        /// <param name="excludePt"> Point that shall be excluded ftom the test. 
        /// Usually the origin point of the line that is connected to the graph or network. </param>
        /// <returns>The nearest point. </returns>
        public static Vector2d NearestPointToLine(IEnumerable<Vector2d> pointsSet, Line2D line, Vector2d excludePt, out double distance)
        {
            Vector2d notNeeded;
            Vector2d nearestPt = new Vector2d();
            double minDist = double.MaxValue;
            foreach (Vector2d testPt in pointsSet)
            {
                if (excludePt != testPt)
                {
                    //double dist = DistancePointToLine(line, testPt);
                    double dist = DistancePointToSegment(testPt, line.Start, line.End, out notNeeded);
                    if (dist < minDist)
                    {
                        minDist = dist;
                        nearestPt = testPt;
                    }
                }
            }
            distance = minDist;
            return nearestPt;
        }

        //============================================================================================================================================================================ 
        /// <summary>
        /// Calculate the distance between a point and a line segment.
        /// </summary>
        /// <param name="pt">Point</param>
        /// <param name="p1">Line segment start point. </param>
        /// <param name="p2">Line segment end point. </param>
        /// <param name="closest">The closest point on the segment. </param>
        /// <returns>The distance between point and line segment. </returns>
        public static double DistancePointToSegment(Vector2d pt, Vector2d p1, Vector2d p2, out Vector2d closest)
        {
            double dx = p2.X - p1.X;
            double dy = p2.Y - p1.Y;
            if ((dx == 0.0) && (dy == 0.0))
            {
                // It's a point not a line segment.
                closest = p1;
                dx = pt.X - p1.X;
                dy = pt.Y - p1.Y;
                return Math.Sqrt(dx * dx + dy * dy);
            }

            // Calculate the t that minimizes the distance.
            double t = ((pt.X - p1.X) * dx + (pt.Y - p1.Y) * dy) / (dx * dx + dy * dy);

            // See if this represents one of the segment's
            // end points or a point in the middle.
            if (t < 0)
            {
                closest = new Vector2d(p1.X, p1.Y);
                dx = pt.X - p1.X;
                dy = pt.Y - p1.Y;
            }
            else if (t > 1)
            {
                closest = new Vector2d(p2.X, p2.Y);
                dx = pt.X - p2.X;
                dy = pt.Y - p2.Y;
            }
            else
            {
                closest = new Vector2d(p1.X + t * dx, p1.Y + t * dy);
                dx = pt.X - closest.X;
                dy = pt.Y - closest.Y;
            }

            return Math.Sqrt(dx * dx + dy * dy);
        }



        // Return the Points with shortest distance between the two segments
        // p1 --> p2 and p3 --> p4.     
        public static double DistanceBetweenSegments(
            Vector2d p1, Vector2d p2, Vector2d p3, Vector2d p4,
            out Vector2d close1, out Vector2d close2)
        {
            // See if the segments intersect.
            bool lines_intersect, segments_intersect;
            Vector2d intersection;
            LineIntersection(p1, p2, p3, p4,
                out lines_intersect, out segments_intersect,
                out intersection, out close1, out close2);
            if (segments_intersect)
            {
                // They intersect.
                close1 = intersection;
                close2 = intersection;
                return 0;
            }

            // Find the other possible distances.
            Vector2d closest;
            double best_dist = double.MaxValue, test_dist;

            // Try p1.
            test_dist = FindDistanceToSegment(p1, p3, p4, out closest);
            if (test_dist < best_dist)
            {
                best_dist = test_dist;
                close1 = p1;
                close2 = closest;
            }

            // Try p2.
            test_dist = FindDistanceToSegment(p2, p3, p4, out closest);
            if (test_dist < best_dist)
            {
                best_dist = test_dist;
                close1 = p2;
                close2 = closest;
            }

            // Try p3.
            test_dist = FindDistanceToSegment(p3, p1, p2, out closest);
            if (test_dist < best_dist)
            {
                best_dist = test_dist;
                close1 = closest;
                close2 = p3;
            }

            // Try p4.
            test_dist = FindDistanceToSegment(p4, p1, p2, out closest);
            if (test_dist < best_dist)
            {
                best_dist = test_dist;
                close1 = closest;
                close2 = p4;
            }

            return best_dist;
        }

        // Calculate the distance between
        // point pt and the segment p1 --> p2.
        private static double FindDistanceToSegment(
            Vector2d pt, Vector2d p1, Vector2d p2, out Vector2d closest)
        {
            double dx = p2.X - p1.X;
            double dy = p2.Y - p1.Y;
            if ((dx == 0) && (dy == 0))
            {
                // It's a point not a line segment.
                closest = p1;
                dx = pt.X - p1.X;
                dy = pt.Y - p1.Y;
                return Math.Sqrt(dx * dx + dy * dy);
            }

            // Calculate the t that minimizes the distance.
            double t = ((pt.X - p1.X) * dx + (pt.Y - p1.Y) * dy) /
                (dx * dx + dy * dy);

            // See if this represents one of the segment's
            // end points or a point in the middle.
            if (t < 0)
            {
                closest = new Vector2d(p1.X, p1.Y);
                dx = pt.X - p1.X;
                dy = pt.Y - p1.Y;
            }
            else if (t > 1)
            {
                closest = new Vector2d(p2.X, p2.Y);
                dx = pt.X - p2.X;
                dy = pt.Y - p2.Y;
            }
            else
            {
                closest = new Vector2d(p1.X + t * dx, p1.Y + t * dy);
                dx = pt.X - closest.X;
                dy = pt.Y - closest.Y;
            }

            return Math.Sqrt(dx * dx + dy * dy);
        }


         //============================================================================================================================================================================
        /// <summary>
        /// Rotate a point around another point.
        /// </summary>
        /// <param name="origPt">Point to rotate. </param>
        /// <param name="refPt">Anchor point for the rotation.</param>
        /// <param name="alpha">Rotation angle in radians.</param>
        /// <returns>A point with the new rotated position of the origPt.</returns>
        public static Vector2d RotatePoint(Vector2d origPt, Vector2d refPt, double alpha)
        {
            Vector2d retPoint;
            try
            {
                double ptX = Math.Cos(alpha) * (refPt.X - origPt.X) - Math.Sin(alpha) * (refPt.Y - origPt.Y) + origPt.X;
                double ptY = Math.Sin(alpha) * (refPt.X - origPt.X) + Math.Cos(alpha) * (refPt.Y - origPt.Y) + origPt.Y;
                retPoint = new Vector2d(ptX, ptY);
                return retPoint;
            }
            catch
            {
                return refPt;
            }
        }

        /*
        //=====================================================================================================================================================
        /// <summary>
        /// Clip a polygon with a list of other polygons.
        /// </summary>
        /// <param name="polyA">The polygon that will be clipped.</param>
        /// <param name="polyB">The list of polygons to clip at.</param>
        /// <returns>The clipping result.</returns>
        public static List<Vector2d> ClipPoly(PolygonD polyA, PolygonD polyB)
        {
            List<Vector2d> points = new List<Vector2d>();
            double faktor = 1000.0;
            PolygonI pA = new PolygonI();
            PolygonI pB = new PolygonI();

            for (int i = 0; i < polyA.Count; i++)
            {
                pA.Add(new IntPoint(Convert.ToInt32(polyA[i].X * faktor), Convert.ToInt32(polyA[i].Y * faktor)));
            }
            for (int i = 0; i < polyB.Count; i++)
            {
                pB.Add(new IntPoint(Convert.ToInt32(polyB[i].X * faktor), Convert.ToInt32(polyB[i].Y * faktor)));
            }

            PolygonsI clipPoly = ClipPoly(pA, pB);
            if (clipPoly.Count > 0) // there is a clipping polygon
            {
                for (int i = 0; i < clipPoly[0].Count; i++)
                {
                    points.Add(new Vector2d(Convert.ToDouble(clipPoly[0][i].X / faktor), Convert.ToDouble(clipPoly[0][i].Y / faktor)));
                }
                return points;
            }
            else { 
                return null; 
            }
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Clip polyA with polyB.
        /// </summary>
        /// <param name="polyA">The polygon that will be clipped.</param>
        /// <param name="polyB">The polygon to clip at.</param>
        /// <returns>The clipping result.</returns>
        public static PolygonsI ClipPoly(PolygonI polyA, PolygonI polyB)
        {
            PolygonsI solution = new PolygonsI();
            Clipper c = new Clipper();
	        c.AddPolygon(polyA, PolyType.ptSubject);
	        c.AddPolygon(polyB, PolyType.ptClip);
	        c.Execute(ClipType.ctIntersection, solution, PolyFillType.pftEvenOdd, PolyFillType.pftEvenOdd);
            return solution;
        }

         * */

        //=====================================================================================================================================================
        /// <summary>
        /// Melt polyA with polyB.
        /// </summary>
        /// <param name="poly1">The polygon that will be clipped.</param>
        /// <param name="poly2">The polygon to clip at.</param>
        /// <returns>The unified polygon.</returns>
        public static Poly2D UnifyPolys(Poly2D poly1, Poly2D poly2)
        {
            double faktor = factorDoubleToInt;
            if (Area(poly1.PointsFirstLoop.ToList()) > thresholdDoubleToInt && Area(poly2.PointsFirstLoop.ToList())> thresholdDoubleToInt)
                faktor = 1.0;

            PolygonsI solution = new PolygonsI();
            Clipper c = new Clipper();
            PolygonsI polyA = new PolygonsI();
            PolygonsI polyB = new PolygonsI();
            for (int i = 0; i < poly1.Points.GetLength(0); i++)
            {
                PolygonI newPoly = new PolygonI();
                for (int j = 0; j < poly1.Points[i].Length; j++)
                {
                    newPoly.Add(Vector2DToIntPoint(poly1.Points[i][j], faktor));
                }
                polyA.Add(newPoly);
            }

            for (int i = 0; i < poly2.Points.GetLength(0); i++)
            {
                PolygonI newPoly = new PolygonI();
                for (int j = 0; j < poly2.Points[i].Length; j++)
                {
                    newPoly.Add(Vector2DToIntPoint(poly2.Points[i][j], faktor));
                }
                polyB.Add(newPoly);
            }

            c.AddPaths(polyA, PolyType.ptSubject, true);
            c.AddPaths(polyB, PolyType.ptClip, true);
            c.Execute(ClipType.ctUnion, solution, PolyFillType.pftEvenOdd, PolyFillType.pftEvenOdd);

            Vector2d[] vecArray = new Vector2d[solution[0].Count];
            for (int i = 0; i < solution[0].Count; i++)
                vecArray[i] = new Vector2d(solution[0][i].X / faktor, solution[0][i].Y / faktor);
            Poly2D finalPoly = new Poly2D(vecArray);

            return finalPoly;
        }

        //=====================================================================================================================================================
        /// <summary>
        /// intersects polyA with polyB.
        /// </summary>
        /// <param name="poly1">The polygon that will be substracted.</param>
        /// <param name="poly2">The polygon that is substracted.</param>
        /// <returns></returns>
        public static Poly2D SubstractPolys(Poly2D poly1, Poly2D poly2)
        {
            double faktor = factorDoubleToInt;
            if (Area(poly1.PointsFirstLoop.ToList()) > thresholdDoubleToInt && Area(poly2.PointsFirstLoop.ToList()) > thresholdDoubleToInt)
                faktor = 1.0;

            if (poly1.IsValid() && poly2.IsValid())
            {
                PolygonsI solution = new PolygonsI();
                Clipper c = new Clipper();
                PolygonsI polyA = new PolygonsI();
                PolygonsI polyB = new PolygonsI();
                for (int i = 0; i < poly1.Points.GetLength(0); i++)
                {
                    PolygonI newPoly = new PolygonI();
                    for (int j = 0; j < poly1.Points[i].Length; j++)
                    {
                        newPoly.Add(Vector2DToIntPoint(poly1.Points[i][j], faktor));
                    }
                    polyA.Add(newPoly);
                }

                for (int i = 0; i < poly2.Points.GetLength(0); i++)
                {
                    PolygonI newPoly = new PolygonI();
                    for (int j = 0; j < poly2.Points[i].Length; j++)
                    {
                        newPoly.Add(Vector2DToIntPoint(poly2.Points[i][j], faktor));
                    }
                    polyB.Add(newPoly);
                }

                c.AddPaths(polyA, PolyType.ptSubject, true);
                c.AddPaths(polyB, PolyType.ptClip, true);
                c.Execute(ClipType.ctDifference, solution, PolyFillType.pftNonZero, PolyFillType.pftNonZero);

                if (solution.Count > 0)
                {
                    Poly2D poly = null;
                    int j = 0;
                    bool goFurther = true;
                    while (goFurther)
                    {
                        Vector2d[] vecArray = new Vector2d[solution[j].Count];
                        for (int i = 0; i < solution[j].Count; i++)
                            vecArray[i] = new Vector2d(solution[j][i].X / faktor, solution[j][i].Y / faktor);

                        poly = new Poly2D(vecArray);
                        j++;

                        if (poly != null)
                            if (poly.IsValid())
                                goFurther = false;
                        if (j >= solution.Count)
                            goFurther = false;
                    }

                    if (poly != null)
                        if (poly.IsValid())
                            return poly;
                        else
                            return null;
                    else
                        return null;
                }
                else
                    return null;
            }
            else return null;

        }


        //=====================================================================================================================================================
        /// <summary>
        /// intersects polyA with polyB.
        /// </summary>
        /// <param name="poly1">The polygon that will be clipped.</param>
        /// <param name="poly2">The polygon to clip at.</param>
        /// <returns></returns>
        public static List<Poly2D> IntersectPolys(Poly2D poly1, Poly2D poly2)
        {
            double faktor = factorDoubleToInt;
            if (Area(poly1.PointsFirstLoop.ToList()) > thresholdDoubleToInt && Area(poly2.PointsFirstLoop.ToList()) > thresholdDoubleToInt)
                faktor = 1.0;

            PolygonsI solution = new PolygonsI();
            Clipper c = new Clipper();
            PolygonsI polyA = new PolygonsI();
            PolygonsI polyB = new PolygonsI();
            int loops = poly1.Points.GetLength(0);
            //double area1=0, area2=0;

            for (int i = 0; i < loops; i++)
            {
                PolygonI newPoly = new PolygonI();
                for (int j = 0; j < poly1.Points[i].Length; j++)
                {
                    newPoly.Add(Vector2DToIntPoint(poly1.Points[i][j], faktor));
                }
                //area1 = Clipper.Area(newPoly);
                //if (area1 < 0) { 
                //    newPoly = Reverse(newPoly);
                //    area1 = Clipper.Area(newPoly);
                //}
                polyA.Add(newPoly);          
            }

            for (int i = 0; i < poly2.Points.GetLength(0); i++)
            {
                PolygonI newPoly = new PolygonI();
                for (int j = 0; j < poly2.Points[i].Length; j++)
                {
                    newPoly.Add(Vector2DToIntPoint(poly2.Points[i][j], faktor));
                }
                //area2 = Clipper.Area(newPoly);
                //if (area2 < 0)
                //{
                //    newPoly = Reverse(newPoly);
                //    area2 = Clipper.Area(newPoly);
                //}
                polyB.Add(newPoly);
            }

            //if (area2 < area1) area1 = area2;
            c.AddPaths(polyA, PolyType.ptSubject, true);
            c.AddPaths(polyB, PolyType.ptClip, true);
            c.Execute(ClipType.ctIntersection, solution, PolyFillType.pftNonZero, PolyFillType.pftNonZero);//, PolyFillType.pftEvenOdd, PolyFillType.pftEvenOdd);

            List<Poly2D> intersectionPolys = new List<Poly2D>();

            if (solution.Count > 0)
            {
                for (int i = 0; i < solution.Count; i++)
                {
                    Vector2d[] vecArray = new Vector2d[solution[i].Count];
                    for (int j = 0; j < solution[i].Count; j++)
                        vecArray[j] = new Vector2d(solution[i][j].X / faktor, solution[i][j].Y / faktor);
                    if (vecArray.Length > 0)
                        intersectionPolys.Add(new Poly2D(vecArray));
                }
                return intersectionPolys;
            }
            else
                return null;

        }

        /// <summary>
        /// Calculates Convex Hull from points
        /// Source: https://stackoverflow.com/questions/25190164/graham-scan-issue-at-high-amount-of-points
        /// </summary>
        /// <param name="initialPoints"></param>
        /// <returns></returns>
        public static List<Vector2d> ConvexHullFromPoints(IEnumerable<Vector2d> pointsToAnalyse)
        {
            var initialPoints = pointsToAnalyse.ToList();
            if (initialPoints.Count() < 2)
            {
                return initialPoints;
            }

            // Find point with minimum y; if more than one, minimize x also.
            int iMin = Enumerable.Range(0, initialPoints.Count()).Aggregate((jMin, jCur) =>
            {
                if (initialPoints[jCur].Y < initialPoints[jMin].Y)
                    return jCur;
                if (initialPoints[jCur].Y > initialPoints[jMin].Y)
                    return jMin;
                if (initialPoints[jCur].X < initialPoints[jMin].X)
                    return jCur;
                return jMin;
            });
            // Sort them by polar angle from iMin, 
            var sortQuery = Enumerable.Range(0, initialPoints.Count())
                .Where((i) => (i != iMin)) // Skip the min point
                .Select((i) => new KeyValuePair<double, Vector2d>(Math.Atan2(initialPoints[i].Y - initialPoints[iMin].Y, 
                            initialPoints[i].X - initialPoints[iMin].X), initialPoints[i]))
                .OrderBy((pair) => pair.Key)
                .Select((pair) => pair.Value);
            List<Vector2d> points = new List<Vector2d>(initialPoints.Count());
            points.Add(initialPoints[iMin]);     // Add minimum point
            points.AddRange(sortQuery);          // Add the sorted points.

            int M = 0;
            for (int i = 1, N = points.Count; i < N; i++)
            {
                bool keepNewPoint = true;
                if (M == 0)
                {
                    // Find at least one point not coincident with points[0]
                    keepNewPoint = !NearlyEqual(points[0], points[i]);
                }
                else
                {
                    while (true)
                    {
                        var flag = WhichToRemoveFromBoundary(points[M - 1], points[M], points[i]);
                        if (flag == RemovalFlag.None)
                            break;
                        else if (flag == RemovalFlag.MidPoint)
                        {
                            if (M > 0)
                                M--;
                            if (M == 0)
                                break;
                        }
                        else if (flag == RemovalFlag.EndPoint)
                        {
                            keepNewPoint = false;
                            break;
                        }
                        else
                            throw new Exception("Unknown RemovalFlag");
                    }
                }
                if (keepNewPoint)
                {
                    M++;
                    Vector2d temp = points[M];
                    points[M] = points[i];
                    points[i] = temp;
                }
            }
            // points[M] is now the last point in the boundary.  Remove the remainder.
            points.RemoveRange(M + 1, points.Count - M - 1);
            return points;
        }

        /// <summary>
        /// Method to test equality
        /// Source: https://stackoverflow.com/questions/25190164/graham-scan-issue-at-high-amount-of-points
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool NearlyEqual(Vector2d a, Vector2d b)
        {
            return NearlyEqual(a.X, b.X) && NearlyEqual(a.Y, b.Y);
        }

        /// <summary>
        /// Method to test equality
        /// Source: https://stackoverflow.com/questions/25190164/graham-scan-issue-at-high-amount-of-points
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="epsilon"></param>
        /// <returns></returns>
        public static bool NearlyEqual(double a, double b, double epsilon = 1e-10)
        {
            // See here: http://floating-point-gui.de/errors/comparison/
            // shortcut, handles infinities
            if (a == b)
            { 
                return true;
            }

            double absA = Math.Abs(a);
            double absB = Math.Abs(b);
            double diff = Math.Abs(a - b);
            double sum = absA + absB;
            if (diff < 4 * double.Epsilon || sum < 4 * double.Epsilon)
                // a or b is zero or both are extremely close to it
                // relative error is less meaningful here
                return true;

            // use relative error
            return diff / (absA + absB) < epsilon;
        }

        /// <summary>
        /// Method to calculate if counter clock wise
        /// Source: https://stackoverflow.com/questions/25190164/graham-scan-issue-at-high-amount-of-points
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <returns></returns>
        private static double CCW(Vector2d p1, Vector2d p2, Vector2d p3)
        {
            // Compute (p2 - p1) X (p3 - p1)
            double cross1 = (p2.X - p1.X) * (p3.Y - p1.Y);
            double cross2 = (p2.Y - p1.Y) * (p3.X - p1.X);
            if (NearlyEqual(cross1, cross2))
                return 0;
            return cross1 - cross2;
        }

        /// <summary>
        /// Source: https://stackoverflow.com/questions/25190164/graham-scan-issue-at-high-amount-of-points
        /// </summary>
        private enum RemovalFlag
        {
            None,
            MidPoint,
            EndPoint
        };

        /// <summary>
        /// Source: https://stackoverflow.com/questions/25190164/graham-scan-issue-at-high-amount-of-points
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <returns></returns>
        private static RemovalFlag WhichToRemoveFromBoundary(Vector2d p1, Vector2d p2, Vector2d p3)
        {
            var cross = CCW(p1, p2, p3);
            if (cross < 0)
                // Remove p2
                return RemovalFlag.MidPoint;
            if (cross > 0)
                // Remove none.
                return RemovalFlag.None;
            // Check for being reversed using the dot product off the difference vectors.
            var dotp = (p3.X - p2.X) * (p2.X - p1.X) + (p3.Y - p2.Y) * (p2.Y - p1.Y);
            if (NearlyEqual(dotp, 0.0))
                // Remove p2
                return RemovalFlag.MidPoint;
            if (dotp < 0)
                // Remove p3
                return RemovalFlag.EndPoint;
            else
                // Remove p2
                return RemovalFlag.MidPoint;
            //return ConvexHullMaker(points);
            // --- not used anymore because of problems with CGeom3d ---
            //List<Vector2d> convexHullPts = new List<Vector2d>();

            //var vertices = new double[points.Count][];
            //for (var i = 0; i < points.Count; i++)
            //{
            //    var location = new double[2];
            //    location[0] = points[i].X;
            //    location[1] = points[i].Y;
            //    vertices[i] = location;
            //}
            //var convexHull = ConvexHull.Create(vertices);

            //double[][] hullPoints = convexHull.Points.Select(p => p.Position).ToArray();

            //for (int i = 0; i < hullPoints.Length; i++)
            //    convexHullPts.Add(new Vector2d(hullPoints[i][0], hullPoints[i][1]));

            //Vector2d centroid = Centroid(points);

            //SortedList<double, Vector2d> convexHullPtsSorted = new SortedList<double, Vector2d>();

            //foreach (Vector2d vec in convexHullPts)
            //{
            //    double angle = CGeom3d.GetXYAngle(new CGeom3d.Vec_3d(vec.X - centroid.X, vec.Y - centroid.Y, 0));
            //    if (!convexHullPtsSorted.ContainsKey(angle))
            //        convexHullPtsSorted.Add(angle, vec);
            //}

            //convexHullPts.Clear(); 
            //return convexHullPtsSorted.Values.ToList();
        }

        /// <summary>
        /// Calculate the line length of the given cluster points and the network lines
        /// </summary>
        /// <param name="edges">edges of the street network</param>
        /// <param name="cluster">cluster points</param>
        /// <returns></returns>
        public static double TotalClusterLength(IEnumerable<Tuple<Vector2d, Vector2d>> edges, IEnumerable<Vector2d> cluster)
        {
            var totalLength = 0.0;
            var clusterPoints = new HashSet<Tuple<double, double>>();
            foreach(var clusterPoint in cluster)
            {
                clusterPoints.Add(Tuple.Create(clusterPoint.X, clusterPoint.Y));
            }
            foreach(var edge in edges)
            {
                if(clusterPoints.Contains(Tuple.Create(edge.Item1.X, edge.Item1.Y)) &&
                    clusterPoints.Contains(Tuple.Create(edge.Item2.X, edge.Item2.Y)))
                {
                    totalLength += (edge.Item2 - edge.Item1).Length;
                }
            }
            return totalLength;
        }

        public static List<Vector2d> ConvexHullMaker(List<Vector2d> points)
        {
            List<PointF> ptsForConvexHull = new List<PointF>();
            List<PointF> convexHullPts = new List<PointF>();
            List<Vector2d> outPoints = new List<Vector2d>();

            for (var i = 0; i < points.Count; i++)
            {
                ptsForConvexHull.Add(points[i].ToPointF());
            }

            convexHullPts = BoundingGeometry.MakeConvexHull(ptsForConvexHull);

            for (int i = 0; i < convexHullPts.Count; i++)
                outPoints.Add(new Vector2d(convexHullPts[i].X, convexHullPts[i].Y));

            return outPoints;
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Calculates the bounding rectangle for a set of points
        /// </summary>
        /// <param name="p">Set of points as array.</param>
        /// <returns></returns>
        public static Rect2D BoundingBox(Vector2d[] p)
        {
            double xmin = double.MaxValue;
            double ymin = double.MaxValue;
            double xmax = double.MinValue;
            double ymax = double.MinValue;
            for ( int i=0; i < p.Length; i++ )
            {
                Vector2d p0 = p[i];   //next vertex
                if ( p0.X < xmin ) xmin = p0.X; //move the left wall to the left
                if ( p0.X > xmax ) xmax = p0.X; //move the right wall to the right
                if ( p0.Y < ymin ) ymin = p0.Y; //raise the upper wall
                if ( p0.Y > ymax ) ymax = p0.Y; //sink the lower wall
            }
            Rect2D boundingRect = new Rect2D( xmin, ymax, xmax-xmin, ymax-ymin );
            return boundingRect;
        }

        public static Rect2D BoundingBoxAdjusted(Vector2d[] p)
        {
            double xmin = double.MaxValue;
            double ymin = double.MaxValue;
            double xmax = double.MinValue;
            double ymax = double.MinValue;
            for (int i = 0; i < p.Length; i++)
            {
                Vector2d p0 = p[i];   //next vertex
                if (p0.X < xmin) xmin = p0.X; //move the left wall to the left
                if (p0.X > xmax) xmax = p0.X; //move the right wall to the right
                if (p0.Y < ymin) ymin = p0.Y; //raise the upper wall
                if (p0.Y > ymax) ymax = p0.Y; //sink the lower wall
            }
            Rect2D boundingRect = new Rect2D(xmin, ymax, xmax - xmin, ymax - ymin, true);

            return boundingRect;
        }

        private static double Sqr(double x) { return x * x; }
        private static double Dist2(Vector2d v, Vector2d w) { return Sqr(v.X - w.X) + Sqr(v.Y - w.Y); }
        public static double DistancePointToLine(Vector2d p, Vector2d v, Vector2d w)
        {
          var l2 = Dist2(v, w);
          if (l2 == 0.0) return Dist2(p, v);
          var t = ((p.X - v.X) * (w.X - v.X) + (p.Y - v.Y) * (w.Y - v.Y)) / l2;
          if (t < 0) return Dist2(p, v);
          if (t > 1) return Dist2(p, w);
            Vector2d temp = new Vector2d(v.X + t * (w.X - v.X), v.Y + t * (w.Y - v.Y));
            return Dist2(p, temp);
        }

        //==============================================================================================
        /// <summary>
        /// Calculates distance from point to line ----- Abstand Punkt Gerade
        /// </summary>
        /// <param name="point">Vector of the point</param>
        /// <param name="vectorA">Startpunkt der Linie</param>
        /// <param name="vectorB">Endpunkt der Linie</param>
        /// <param name="pointOfIntersection">Lotfußpunkt</param>
        /// <returns></returns>
        public static double DistancePointToLine(Vector2d point, Vector2d vectorA, Vector2d vectorB, out Vector2d pointOfIntersection)//, Vector2d Translation = null)
        {
            Vector2d calc = vectorA - vectorB;
            double distance = 0.0;


            double norm = calc.Length;

            double u = (((point.X - vectorA.X) * (vectorB.X - vectorA.X)) +
                         ((point.Y - vectorA.Y) * (vectorB.Y - vectorA.Y))) /
                       (norm * norm);

            if (u < 0.0 || u > 1.0)
            {
                //return -1.0;   // closest point does not fall within the line segment
                double distA = point.Distance(vectorA);
                double distB = point.Distance(vectorB);
                if (distA < distB)
                {
                    pointOfIntersection = vectorA;
                   // pointOfIntersection.X = vectorA.X;
                  //  pointOfIntersection.Y = vectorA.Y;
                    //Translation = PointOfIntersection - point;
                    return distA;
                }
                else
                {
                    pointOfIntersection = vectorB;
                   // pointOfIntersection.X = vectorA.X;
                   // pointOfIntersection.Y = vectorA.Y;
                    //Translation = PointOfIntersection - point;
                    return distB;
                }
            }

            pointOfIntersection = vectorA;
            pointOfIntersection.X = vectorA.X + u * (vectorB.X - vectorA.X);
            pointOfIntersection.Y = vectorA.Y + u * (vectorB.Y - vectorA.Y);
            //Translation = PointOfIntersection - point;
            distance = point.Distance(pointOfIntersection);

            return distance;
        }
        /// <summary>
        /// Calculate the distance from line to point.
        /// </summary>
        /// /// <param name="line">The line.</param>
        /// <param name="point">The point.</param>
        /// <returns>The distance.</returns>
        public static double DistancePointToLine(Line2D line, Vector2d point)
        {
            if (point == line.Start || point == line.End) return 0;

            //return DistancePointToLine(point, line.Start, line.End);

            double x = line.Start.X, y = line.Start.Y;
            double ax = line.End.X - x, ay = line.End.Y - y;

            // set (x,y) to nearest Line2D point from q
            if (ax != 0 || ay != 0)
            {
                double u = ((point.X - x) * ax + (point.Y - y) * ay) / (ax * ax + ay * ay);
                if (u > 1)
                {
                    x = line.End.X; y = line.End.Y;
                }
                else if (u > 0)
                {
                    x += u * ax; y += u * ay;
                }
            }

            x = point.X - x;
            y = point.Y - y;
            return Math.Sqrt(x * x + y * y);
        }


        ////=====================================================================================================================================================
        ///// <summary>
        ///// Calculates the intersection point of two lines.
        ///// </summary>
        ///// <param name="start1"> Start point of line 1 </param>
        ///// <param name="end1"> End point of line 1 </param>
        ///// <param name="start2"> Start point of line 2 </param>
        ///// <param name="end2"> End point of line 2</param>
        ///// <returns> The intersection point </returns>
        //public static Vector2d LineIntersection(Vector2d start1, Vector2d end1, Vector2d start2, Vector2d end2)
        //{
        //    double denom = ((end1.X - start1.X) * (end2.Y - start2.Y)) - ((end1.Y - start1.Y) * (end2.X - start2.X));

        //    //  AB & CD are parallel 
        //    if (denom == 0)
        //        return null;// PointF.Empty;

        //    double numer = ((start1.Y - start2.Y) * (end2.X - start2.X)) - ((start1.X - start2.X) * (end2.Y - start2.Y));

        //    double r = numer / denom;

        //    double numer2 = ((start1.Y - start2.Y) * (end1.X - start1.X)) - ((start1.X - start2.X) * (end1.Y - start1.Y));

        //    double s = numer2 / denom;

        //    if ((r < 0 || r > 1) || (s < 0 || s > 1))
        //        return null;// PointF.Empty;

        //    // Find intersection point
        //    Vector2d result = new Vector2d();
        //    result.X = start1.X + (r * (end1.X - start1.X));
        //    result.Y = start1.Y + (r * (end1.Y - start1.Y));

        //    return result;
        //}

        //=====================================================================================================================================================
        /// <summary>
        /// Find the point of intersection between two lines.
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <param name="a1"></param>
        /// <param name="b1"></param>
        /// <param name="a2"></param>
        /// <param name="b2"></param>
        /// <param name="intersect">The intersection point.</param>
        /// <returns>True, if there is an intersection, false otherwise.</returns>
        public static bool LineIntersectionG(double x1, double y1, double x2, double y2, double a1, double b1, double a2, double b2, ref Vector2d intersect)
        {
            double dx = x2 - x1;
            double dy = y2 - y1;
            double da = a2 - a1;
            double db = b2 - b1;
            double t;

            // If the segments are parallel, return False.
            if (Math.Abs(da * dy - db * dx) < 0.001) return false;

            // Find the point of intersection.
            t = (da * (y1 - b1) + db * (a1 - x1)) / (db * dx - da * dy);
            intersect = new Vector2d(x1 + t * dx, y1 + t * dy);
            return true;
        }


        /// <summary>
        /// Tests if two line segments intersect or not.
        /// The orientation of each line to other line's endpoints is used to determine
        /// the intersection.
        /// </summary>
        /// <param name="line1">Line 1.</param>
        /// <param name="line2">Line 2.</param>
        /// <returns>True if the lines intersect each other.</returns>
        public static bool DoLinesIntersect(Line2D line1, Line2D line2)
        {
            return CrossProduct(line1.Start, line1.End, line2.Start) !=
                   CrossProduct(line1.Start, line1.End, line2.End) ||
                   CrossProduct(line2.Start, line2.End, line1.Start) !=
                   CrossProduct(line2.Start, line2.End, line1.End);
        }


        // Find the point of intersection between two lines.
        public static bool IntersectionTest(float L1P1X, float L1P1Y, float L1P2X, float L1P2Y,
                                            float L2P1X, float L2P1Y,float L2P2X, float L2P2Y)
        {
    
            float d = (L2P2Y - L2P1Y) * (L1P2X - L1P1X) - (L2P2X - L2P1X) * (L1P2Y - L1P1Y);
            float n_a = (L2P2X - L2P1X) * (L1P1Y - L2P1Y) - (L2P2Y - L2P1Y) * (L1P1X - L2P1X);
            float n_b = (L1P2X - L1P1X) * (L1P1Y - L2P1Y) - (L1P2Y - L1P1Y) * (L1P1X - L2P1X);

            if (d == 0)
                return false;

            float ua = n_a / d;
            float ub = n_b / d;

            if (ua >= 0.0 && ua <= 1.0 && ub >= 0.0 && ub <= 1.0)
            {
                return true;
            }
            return false;
        }

        // Find the point of intersection between two lines.
        public static bool FindIntersection(float X1, float Y1, float X2, float Y2, float A1, float B1, float A2, float B2, ref Vector2d intersection)
        {
            float dx = X2 - X1;
            float dy = Y2 - Y1;
            float da = A2 - A1;
            float db = B2 - B1;
            float s, t;

            // If the segments are parallel, return False.
            if (Math.Abs(da * dy - db * dx) < 0.001) return false;

            // Find the point of intersection.
            s = (dx * (B1 - Y1) + dy * (X1 - A1)) / (da * dy - db * dx);
            t = (da * (Y1 - B1) + db * (A1 - X1)) / (db * dx - da * dy);
            intersection = new Vector2d(X1 + t * dx, Y1 + t * dy);
            return true;
        }

        /// <summary>
        /// Find the point of intersection between two lines.
        /// Use the other LineIntersection method to avoid using CGeom3d / PureGeom3d.dll
        /// </summary>
        /// <param name="line1P1"></param>
        /// <param name="line1P2"></param>
        /// <param name="line2P1"></param>
        /// <param name="line2P2"></param>
        /// <param name="intersect"></param>
        /// <returns>
        /// return 0 : IntersectionType.NoIntersection
        /// return 1 : IntersectionType.Touches
        /// return 2 : IntersectionType.Intersects
        /// </returns>
        public static int LineIntersection(Vector2d line1P1, Vector2d line1P2, Vector2d line2P1, Vector2d line2P2,
            ref Vector2d intersect)
        {
            CGeom3d.Line_3d l1 = new CGeom3d.Line_3d();
            l1.p = new CGeom3d.Vec_3d(line1P1.X, line1P1.Y, 0);
            l1.v = new CGeom3d.Vec_3d(line1P2.X - line1P1.X, line1P2.Y - line1P1.Y, 0);
            CGeom3d.Line_3d l2 = new CGeom3d.Line_3d();
            l2.p = new CGeom3d.Vec_3d(line2P1.X, line2P1.Y, 0);
            l2.v = new CGeom3d.Vec_3d(line2P2.X - line2P1.X, line2P2.Y - line2P1.Y, 0);
            CGeom3d.Vec_3d p = new CGeom3d.Vec_3d();
            CGeom3d.Point_LL_3d(l1, l2, p);
            if (CGeom3d.Err_3d == 0)
                return (int) IntersectionType.NoIntersection;

            intersect = new Vector2d(p.x, p.y);
            double eps = 0.00001;
            if (CGeom3d.InLine_3d(l1.p, l1.p + l1.v, p) && CGeom3d.InLine_3d(l2.p, l2.p + l2.v, p))
                if ((DistancePoints(intersect, line2P1) <= eps) || (DistancePoints(intersect, line2P2) <= eps) ||
                    (DistancePoints(intersect, line1P1) <= eps) || (DistancePoints(intersect, line1P2) <= eps))
                    return (int) IntersectionType.Touches;
                else
                    return (int) IntersectionType.Intersects;
            else
                return (int) IntersectionType.NoIntersection;
        }

        //=====================================================================================================================================================
        // Find the point of intersection between
        // the lines p1 --> p2 and p3 --> p4.
        // code from: http://csharphelper.com/blog/2014/08/determine-where-two-lines-intersect-in-c/ 
        public static void LineIntersection(Vector2d p1, Vector2d p2, Vector2d p3, Vector2d p4, out bool lines_intersect, 
            out bool segments_intersect, out Vector2d intersection, out Vector2d close_p1, out Vector2d close_p2)
        {
            // Get the segments' parameters.
            double dx12 = p2.X - p1.X;
            double dy12 = p2.Y - p1.Y;
            double dx34 = p4.X - p3.X;
            double dy34 = p4.Y - p3.Y;

            // Solve for t1 and t2
            double denominator = (dy12 * dx34 - dx12 * dy34);

            double t1 = ((p1.X - p3.X) * dy34 + (p3.Y - p1.Y) * dx34) / denominator;
            if (double.IsInfinity(t1))
            {
                // The lines are parallel (or close enough to it).
                lines_intersect = false;
                segments_intersect = false;
                intersection = new Vector2d(double.NaN, double.NaN);
                close_p1 = new Vector2d(double.NaN, double.NaN);
                close_p2 = new Vector2d(double.NaN, double.NaN);
                return;
            }
            lines_intersect = true;

            double t2 = ((p3.X - p1.X) * dy12 + (p1.Y - p3.Y) * dx12) / -denominator;

            // Find the point of intersection.
            intersection = new Vector2d(p1.X + dx12 * t1, p1.Y + dy12 * t1);

            // The segments intersect if t1 and t2 are between 0 and 1.
            segments_intersect = ((t1 >= 0) && (t1 <= 1) && (t2 >= 0) && (t2 <= 1));

            // Find the closest points on the segments.
            if (t1 < 0)
            {
                t1 = 0;
            }
            else if (t1 > 1)
            {
                t1 = 1;
            }

            if (t2 < 0)
            {
                t2 = 0;
            }
            else if (t2 > 1)
            {
                t2 = 1;
            }

            close_p1 = new Vector2d(p1.X + dx12 * t1, p1.Y + dy12 * t1);
            close_p2 = new Vector2d(p3.X + dx34 * t2, p3.Y + dy34 * t2);
        }
        
                /// <summary>Find the point of intersection between two lines.</summary>
        /// <returns>Intersection point or null if predicate was not met.</returns>
        public static Vector2d? LineIntersection(Vector2d line1P1, Vector2d line1P2, Vector2d line2P1, Vector2d line2P2, Func<IntersectionType, bool> intersectionPredicate = null)
        {
            var intersect = new Vector2d();
            var type = LineIntersection(line1P1, line1P2, line2P1, line2P2, ref intersect);
            var predicate = intersectionPredicate ?? (i => i != IntersectionType.NoIntersection);
            return predicate((IntersectionType)type) ? (Vector2d?)intersect : null;
        }

        public static Line2DIntersection LineIntersection(Line2D line1,Line2D line2, double epsilon)
        {

            Line2DIntersection result;
            try
            {
                result = Line2DIntersection.Find(line1.Start, line1.End, line2.Start, line2.End,
                    epsilon);
            }
            catch (Exception e)
            {
                throw new Exception("line2dIntersection problem");
                
            }
            return result;

        }
        //=====================================================================================================================================================
        /// <summary>
        /// Calculate the distance between two points.
        /// </summary>
        /// <param name="v1">First point.</param>
        /// <param name="v2">Second point.</param>
        /// <returns>The distance between first and second point.</returns>
        public static double DistancePoints(Vector2d v1, Vector2d v2)
        {
            double dist = Math.Sqrt((Math.Pow(v1.X - v2.X, 2) + Math.Pow(v1.Y - v2.Y, 2)));
            return dist;
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Calculate the distance between two polygons.
        /// </summary>
        /// <param name="polyA">First polygon.</param>
        /// <param name="polyB">Second polygon.</param>
        /// <returns>The distance between first and second polygon.</returns>
        public static double DistancePolygons(Poly2D polyA, Poly2D polyB)
        {
            LinearRing ringA = CreateRing(polyA);
            LinearRing ringB = CreateRing(polyB);
            return ringA.Distance(ringB);
        }

        //==============================================================================================
        /// <summary>
        /// Calculate distance between a polygon and a line (segment).
        /// </summary>
        /// <param name="poly">Polygon</param>
        /// <param name="line">Line segment</param>
        /// <returns>The distance between line and polygon.</returns>
        public static double DistancePolygonToLine(Poly2D poly, Line2D line)
        {
            LinearRing ringA = CreateRing(poly);
            Coordinate[] curCoords = new Coordinate[2];
            curCoords[0] = new Coordinate(line.Start.X, line.Start.Y);
            curCoords[1] = new Coordinate(line.End.X, line.End.Y);
            LineString lineSt = new LineString(curCoords);

            return ringA.Distance(lineSt);
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Create a LinearRing from a Poly2D.
        /// </summary>
        /// <param name="poly">The Poly2D object.</param>
        /// <returns>LinearRing object.</returns>
        public static LinearRing CreateRing(Poly2D poly)
        {
            LinearRing ring;
            Vector2d[] pts = poly.Points[0];
            Coordinate[] curCoords = new Coordinate[pts.Length + 1];

            for (int i = 0; i < pts.Length; i++)
            {
                curCoords[i] = new Coordinate(pts[i].X, pts[i].Y);
            }
            curCoords[pts.Length] = new Coordinate(pts[0].X, pts[0].Y);
            ring = new LinearRing(curCoords);
            return ring;
        }

        public static Line2D[] ConnectPoints(bool isClosed, Vector2d[] vertices)
        {
            if (vertices == null)
                throw new System.ArgumentNullException("vertices");

            if (vertices.Length < 2)
                return new Line2D[0];

            Line2D[] lines = new Line2D[isClosed ? vertices.Length : vertices.Length - 1];

            for (int i = 0; i < vertices.Length - 1; i++)
                lines[i] = new Line2D(vertices[i], vertices[i + 1]);

            if (isClosed)
                lines[lines.Length - 1] = new Line2D(vertices[vertices.Length - 1], vertices[0]);

            return lines;
        }
        //=====================================================================================================================================================
        /// <summary>
        /// Returns the distance from this to the other rect. 0 if there is an overlap.
        /// </summary>
        /// <param name="first">First rectangle.</param> 
        /// <param name="other">Other rectangle.</param> 
        public static double Distance(Rect2D first, Rect2D other)
        {
            if (first.Overlaps(other))
                return 0;

            if (other.Left > first.BottomRight.X)
            {
                // Other is to the right
                if (other.Bottom > first.TopLeft.Y)
                {
                    // Other is to the top right
                    Vector2d ptTopRight = new Vector2d(first.BottomRight.X, first.TopLeft.Y);
                    return ptTopRight.Distance(new Vector2d(other.Left, other.Bottom));
                }
                else if (other.Top < first.BottomRight.Y)
                {
                    // Other to the bottom right
                    return first.BottomRight.Distance(other.TopLeft);
                }
                else
                {
                    // to the right
                    return other.Left - first.BottomRight.X;
                }
            }
            else if (other.Right < first.TopLeft.X)
            {
                // Other to the left
                if (other.Bottom > first.TopLeft.Y)
                {
                    // Other is to the top left
                    return first.TopLeft.Distance(other.BottomRight);
                }
                else if (other.Top < first.BottomRight.Y)
                {
                    // Other to the bottom left
                    Vector2d ptBottomLeft = new Vector2d(first.TopLeft.X, first.BottomRight.Y);
                    return ptBottomLeft.Distance(new Vector2d(other.Right, other.Top));
                }
                else
                {
                    //Just to the left
                    return (first.TopLeft.X - other.Right);
                }
            }
            else
            {
                // There is horizontal overlap;
                if (other.Bottom > first.TopLeft.Y)
                    return other.Bottom - first.TopLeft.Y;
                else
                    return first.BottomRight.Y - other.Top;
            }
        }

        //=====================================================================================================================================================
        /// <summary>
        /// True if there is an overlap.
        /// </summary>
        /// <param name="first">First rectangle.</param> 
        /// <param name="other">Other rectangle.</param> 
        public static bool Overlaps(Rect2D first, Rect2D other)
        {
            bool bOvX = !(other.Left >= first.BottomRight.X ||
                          other.Right <= first.TopLeft.X);

            bool bOvY = !(other.Bottom >= first.TopLeft.Y ||
                          other.Top <= first.BottomRight.Y);

            return bOvX && bOvY;
        }

        //=====================================================================================================================================================
        /// <summary>
        /// True if there is an overlap, returns the overlap.
        /// </summary>
        /// <param name="first">First rectangle.</param> 
        /// <param name="other">Second rectangle.</param> 
        /// <param name="overlap">Output. The overlap.</param> 
        public static bool Overlaps(Rect2D first, Rect2D other, Rect2D overlap)
        {
            Vector2d ptOvTl = new Vector2d();
            Vector2d ptOvBr = new Vector2d();

            ptOvTl.Y = Math.Min(first.TopLeft.Y, other.TopLeft.Y);
            ptOvBr.Y = Math.Max(first.BottomRight.Y, other.BottomRight.Y);

            ptOvTl.X = Math.Max(first.TopLeft.X, other.TopLeft.X);
            ptOvBr.X = Math.Min(first.BottomRight.X, other.BottomRight.X);

            overlap.Set(ptOvTl, ptOvBr);

            return overlap.IsValid();
        }

        //=====================================================================================================================================================
        #region "Bounding Rectangle"

        private static int _mNumPoints;

        //=====================================================================================================================================================
        /// <summary>
        /// The points that have been used in test edges.
        /// </summary>
        private static bool[] _mEdgeChecked;

        /// <summary>
        /// The four caliper control points. They start:
        ///       _controlPoints(0)      Left edge       xmin
        ///       _controlPoints(1)      Bottom edge     ymax
        ///       _controlPoints(2)      Right edge      xmax
        ///       _controlPoints(3)      Top edge        ymin
        /// </summary>
        private static int[] _controlPoints = new int[4];

        /// <summary>
        /// The line from this point to the next one forms one side of the next bounding rectangle.
        /// </summary>
        private static int _currentControlPoint = -1;

        /// <summary>
        /// The area of the current and best bounding rectangles.
        /// </summary>
        private static double _currentArea = double.MaxValue;

        private static Vector2d[] _currentRectangle ;
        private static double _bestArea = double.MaxValue;
        private static Vector2d[] _bestRectangle;

        //=====================================================================================================================================================
        /// <summary>
        /// Get ready to start.
        /// </summary>
        /// <param name="polygon"></param>
        /// <returns></returns>
        private static bool ResetBoundingRect(PolygonD polygon)
        {
            _mNumPoints = polygon.Count();

            // Find the initial control points.
            if (!FindInitialControlPoints(polygon))
                return false;

            // So far we have not checked any edges.
            _mEdgeChecked = new bool[_mNumPoints];

            // Start with this bounding rectangle.
            _currentControlPoint = 1;
            _bestArea = double.MaxValue;

            // Find the initial bounding rectangle.
            if (!BoundingRectangle(polygon))
                return false;

            // Remember that we have checked this edge.
            _mEdgeChecked[_controlPoints[_currentControlPoint]] = true;

            return true;
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Find the initial control points.
        /// </summary>
        /// <param name="polygon"></param>
        /// <returns></returns>
        private static bool FindInitialControlPoints(PolygonD polygon)
        {
            for (int i = 0; i < _mNumPoints; i++)
            {
                if (CheckInitialControlPoints(polygon, i)) return true;
            }
            return false;
        }

        //=====================================================================================================================================================
        /// <summary>
        /// See if we can use segment i -. i + 1 as the base for the initial control points.
        /// </summary>
        /// <param name="polygon"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        private static bool CheckInitialControlPoints(PolygonD polygon, int i)
        {
            // Get the i . i + 1 unit vector.
            int i1 = (i + 1) % _mNumPoints;
            double vix = polygon[i1].X - polygon[i].X;
            double viy = polygon[i1].Y - polygon[i].Y;

            // The candidate control point indexes.
            for (int num = 0; num < 4; num++)
            {
                _controlPoints[num] = i;
            }

            // Check backward from i until we find a vector
            // j . j+1 that points opposite to i . i+1.
            for (int num = 1; num < _mNumPoints; num++)
            {
                // Get the new edge vector.
                int j = (i - num + _mNumPoints) % _mNumPoints;
                int j1 = (j + 1) % _mNumPoints;
                double vjx = polygon[j1].X - polygon[j].X;
                double vjy = polygon[j1].Y - polygon[j].Y;

                // Project vj along vi. The length is vj dot vi.
                double dotProduct = vix * vjx + viy * vjy;

                // If the dot product < 0, then j1 is
                // the index of the candidate control point.
                if (dotProduct < 0)
                {
                    _controlPoints[0] = j1;
                    break;
                }
            }

            // If j == i, then i is not a suitable control point.
            if (_controlPoints[0] == i) return false;

            // Check forward from i until we find a vector
            // j . j+1 that points opposite to i . i+1.
            for (int num = 1; num < _mNumPoints; num++)
            {
                // Get the new edge vector.
                int j = (i + num) % _mNumPoints;
                int j1 = (j + 1) % _mNumPoints;
                double vjx = polygon[j1].X - polygon[j].X;
                double vjy = polygon[j1].Y - polygon[j].Y;

                // Project vj along vi. The length is vj dot vi.
                double dotProduct = vix * vjx + viy * vjy;

                // If the dot product <= 0, then j is
                // the index of the candidate control point.
                if (dotProduct <= 0)
                {
                    _controlPoints[2] = j;
                    break;
                }
            }

            // If j == i, then i is not a suitable control point.
            if (_controlPoints[2] == i) return false;

            // Check forward from _controlPoints[2] until
            // we find a vector j . j+1 that points opposite to
            // _controlPoints[2] . _controlPoints[2]+1.

            i = _controlPoints[2] - 1;//@
            double temp = vix;
            vix = viy;
            viy = -temp;

            for (int num = 1; num < _mNumPoints; num++)
            {
                // Get the new edge vector.
                int j = (i + num) % _mNumPoints;
                int j1 = (j + 1) % _mNumPoints;
                double vjx = polygon[j1].X - polygon[j].X;
                double vjy = polygon[j1].Y - polygon[j].Y;

                // Project vj along vi. The length is vj dot vi.
                double dotProduct = vix * vjx + viy * vjy;

                // If the dot product <=, then j is
                // the index of the candidate control point.
                if (dotProduct <= 0)
                {
                    _controlPoints[3] = j;
                    break;
                }
            }

            // If j == i, then i is not a suitable control point.
            if (_controlPoints[0] == i) return false;

            // These control points work.
            return true;
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Find the next bounding rectangle and check it.
        /// </summary>
        /// <param name="polygon"></param>
        private static void CheckNextRectangle(PolygonD polygon)
        {
            // Increment the current control point.
            // This means we are done with using this edge.
            if (_currentControlPoint >= 0)
            {
                _controlPoints[_currentControlPoint] =
                    (_controlPoints[_currentControlPoint] + 1) % _mNumPoints;
            }

            // Find the next point on an edge to use.
            double dx0, dy0, dx1, dy1, dx2, dy2, dx3, dy3;
            FindDxDy(polygon, out dx0, out dy0, _controlPoints[0]);
            FindDxDy(polygon, out dx1, out dy1, _controlPoints[1]);
            FindDxDy(polygon, out dx2, out dy2, _controlPoints[2]);
            FindDxDy(polygon, out dx3, out dy3, _controlPoints[3]);

            // Switch so we can look for the smallest opposite/adjacent ratio.
            double opp0 = dx0;
            double adj0 = dy0;
            double opp1 = -dy1;
            double adj1 = dx1;
            double opp2 = -dx2;
            double adj2 = -dy2;
            double opp3 = dy3;
            double adj3 = -dx3;

            // Assume the first control point is the best point to use next.
            double bestopp = opp0;
            double bestadj = adj0;
            int bestControlPoint = 0;

            // See if the other control points are better.
            if (opp1 * bestadj < bestopp * adj1)
            {
                bestopp = opp1;
                bestadj = adj1;
                bestControlPoint = 1;
            }
            if (opp2 * bestadj < bestopp * adj2)
            {
                bestopp = opp2;
                bestadj = adj2;
                bestControlPoint = 2;
            }
            if (opp3 * bestadj < bestopp * adj3)
            {
                bestControlPoint = 3;
            }

            // Use the new best control point.
            _currentControlPoint = bestControlPoint;

            // Remember that we have checked this edge.
            _mEdgeChecked[_controlPoints[_currentControlPoint]] = true;

            // Find the current bounding rectangle
            // and see if it is an improvement.
            BoundingRectangle(polygon);
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Find the current bounding rectangle and see if it is better than the previous best.
        /// </summary>
        /// <param name="polygon"></param>
        /// <returns></returns>
        private static bool BoundingRectangle(PolygonD polygon)
        {
            // See which point has the current edge.
            int i1 = _controlPoints[_currentControlPoint];
            int i2 = (i1 + 1) % _mNumPoints;
            double dx = polygon[i2].X - polygon[i1].X;
            double dy = polygon[i2].Y - polygon[i1].Y;

            // Make dx and dy work for the first line.
            switch (_currentControlPoint)
            {
                case 0: // Nothing to do.
                    break;
                case 1: // dx = -dy, dy = dx
                    double temp1 = dx;
                    dx = -dy;
                    dy = temp1;
                    break;
                case 2: // dx = -dx, dy = -dy
                    dx = -dx;
                    dy = -dy;
                    break;
                case 3: // dx = dy, dy = -dx
                    double temp2 = dx;
                    dx = dy;
                    dy = -temp2;
                    break;
            }

            double px0 = polygon[_controlPoints[0]].X;
            double py0 = polygon[_controlPoints[0]].Y;
            double dx0 = dx;
            double dy0 = dy;
            double px1 = polygon[_controlPoints[1]].X;
            double py1 = polygon[_controlPoints[1]].Y;
            double dx1 = dy;
            double dy1 = -dx;
            double px2 = polygon[_controlPoints[2]].X;
            double py2 = polygon[_controlPoints[2]].Y;
            double dx2 = -dx;
            double dy2 = -dy;
            double px3 = polygon[_controlPoints[3]].X;
            double py3 = polygon[_controlPoints[3]].Y;
            double dx3 = -dy;
            double dy3 = dx;

            // Find the points of intersection.
            _currentRectangle = new Vector2d[4];
            if (!LineIntersectionG(px0, py0, px0 + dx0, py0 + dy0, px1, py1, px1 + dx1, py1 + dy1, ref _currentRectangle[0])) return false;
            if (!LineIntersectionG(px1, py1, px1 + dx1, py1 + dy1, px2, py2, px2 + dx2, py2 + dy2, ref _currentRectangle[1])) return false;
            if (!LineIntersectionG(px2, py2, px2 + dx2, py2 + dy2, px3, py3, px3 + dx3, py3 + dy3, ref _currentRectangle[2])) return false;
            if (!LineIntersectionG(px3, py3, px3 + dx3, py3 + dy3, px0, py0, px0 + dx0, py0 + dy0, ref _currentRectangle[3])) return false;

            // See if this is the best bounding rectangle so far.
            // Get the area of the bounding rectangle.
            double vx0 = _currentRectangle[0].X - _currentRectangle[1].X;
            double vy0 = _currentRectangle[0].Y - _currentRectangle[1].Y;
            double len0 = Math.Sqrt(vx0 * vx0 + vy0 * vy0);

            double vx1 = _currentRectangle[1].X - _currentRectangle[2].X;
            double vy1 = _currentRectangle[1].Y - _currentRectangle[2].Y;
            double len1 = Math.Sqrt(vx1 * vx1 + vy1 * vy1);

            // See if this is an improvement.
            _currentArea = len0 * len1;
            if (_currentArea < _bestArea)
            {
                _bestArea = _currentArea;
                _bestRectangle = _currentRectangle;
            }
            return true;
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Find the slope of the edge from point i to point i + 1.
        /// </summary>
        /// <param name="polygon"></param>
        /// <param name="dx"></param>
        /// <param name="dy"></param>
        /// <param name="i"></param>
        private static void FindDxDy(PolygonD polygon, out double dx, out double dy, int i)
        {
            int i2 = (i + 1) % _mNumPoints;
            dx = polygon[i2].X - polygon[i].X;
            dy = polygon[i2].Y - polygon[i].Y;
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Find a smallest bounding rectangle.
        /// </summary>
        /// <param name="polygon"> Initial polygon for which the bounding rectangle is calculated. </param>
        /// <returns>Smallest bounding rectangle.</returns>
        public static Vector2d[] MinimalBoundingRectangle(PolygonD polygon)
        {
            // -- first we calculate the convex hull as basis for the minimal bounding rectangle --
            PolygonD convexHull = ConvexHullMaker(polygon);

            // -- for the minimal bounding rectangle algorithm we need to add the first point to the list again --
            convexHull.Add(convexHull[0]);

            if (!IsPolygonOrientedClockwise(convexHull))
            {
                convexHull.Reverse();
            }

            // Get ready;
            bool status = ResetBoundingRect(convexHull);
            if (status == false)
                return null;

            // Check all possible bounding rectangles.
            for (int i = 0; i < convexHull.Count(); i++)
            {
                CheckNextRectangle(convexHull);
            }

            // Return the best result.
            if (IsPolygonOrientedClockwise(convexHull))
            {
                convexHull.Reverse();
            }
            Array.Reverse(_bestRectangle);
            return _bestRectangle;
                  
        }

        # endregion

        //===================================================================================================================================================== 
        #region BoudingCircle

        public static void MinimalBoundingCircle(IEnumerable<Vector2d> inPoints, out Vector2d inCenter, out float radius)
        {
            List<PointF> points = inPoints.Select(pt => (pt.ToPointF())).ToList();
            PointF center;
            BoundingGeometry.FindMinimalBoundingCircle(points, out center, out radius);
            inCenter = new Vector2d(center.X, center.Y);
        }

        #endregion


        //=====================================================================================================================================================
        #region "Orientation Routines"
        /// <summary>
        /// Test if the polygon is oriented clockwise.
        /// </summary>
        /// <param name="polygon"> The polygon to test. </param>
        /// <returns> True if the polygon is oriented clockwise. </returns>
        public static bool IsPolygonOrientedClockwise(PolygonD polygon)
        {
            return (Area(polygon) < 0);
        }

        //=====================================================================================================================================================
        /// <summary>
        /// If the polygon is oriented counterclockwise, reverse the order of its points.
        /// </summary>
        /// <param name="polygon"></param>
        public static void OrientPolygonClockwise(PolygonD polygon)
        {
            if (!IsPolygonOrientedClockwise(polygon))
                polygon.Reverse();
        }

        #endregion

        //=====================================================================================================================================================
        // code from: http://csharphelper.com/blog/2014/07/calculate-the-area-of-a-polygon-in-c/
        // Return the polygon's area in "square units."
        public static double PolygonArea(List<Vector2d> vertices)
        {
            // Return the absolute value of the signed area.
            // The signed area is negative if the polyogn is
            // oriented clockwise.
            return Math.Abs(SignedPolygonArea(vertices));
        }

        // Return the polygon's area in "square units."
        // The value will be negative if the polygon is
        // oriented clockwise.
        private static double SignedPolygonArea(List<Vector2d> vertices)
        {
            // Add the first point to the end.
            vertices.Add(vertices[0]);
            Vector2d[] pts = vertices.ToArray();


            // Get the areas.
            double area = 0;
            for (int i = 0; i < vertices.Count-1 ; i++)
            {
                area += (pts[i + 1].X - pts[i].X) * (pts[i + 1].Y + pts[i].Y) / 2;
            }

            // Return the result.
            return area;
        }


        public static double Area(List<Vector2d> vertices)
        {
            return PolygonArea(vertices);
            // --- old CGeom3d usage ---
            //CGeom3d.Vec_3d[] points = new CGeom3d.Vec_3d[vertices.Count];
            //for(int i=0; i< vertices.Count; i++)
            //    points[i] = new CGeom3d.Vec_3d(vertices[i].X, vertices[i].Y, 0);

            //double area = 0;
            //CGeom3d.AreaLoop(points, ref area);

            //return Math.Abs(area);
        }

        public static double PolygonArea(params Vector2d[] polygon)
        {
            if (polygon == null)
               throw new ArgumentNullException("polygon");
            if (polygon.Length < 3)
                throw new ArgumentException(
                    "polygon.Length");

            double area = 0;

            for (int i = polygon.Length - 1, j = 0; j < polygon.Length; i = j++)
                area += (polygon[i].X * polygon[j].Y - polygon[j].X * polygon[i].Y);

            return area / 2.0;
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Calculate the perimeter of a polygon.
        /// </summary>
        /// <param name="polygon">The polygon to be analysed.</param>
        /// <returns>The perimenter length.</returns>
        public static double Perimeter(PolygonD polygon)
        {
            double tDLength = 0.0;

            if (polygon.Count() >= 2)
            {
                PolygonD.Enumerator tIterPoints = polygon.GetEnumerator();

                tIterPoints.MoveNext();
                Vector2d tV0 = tIterPoints.Current;

                Vector2d tV1;
                while (tIterPoints.MoveNext() != false)
                {
                    tV1 = tIterPoints.Current;
                    tDLength += tV0.Distance(tV1);
                    tV0 = tV1;
                }
                tV1 = polygon[0]; // frontPoint();
                tDLength += tV0.Distance(tV1);
            }

            return tDLength;
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Calculate the form factor of a polygon.
        /// </summary>
        /// <param name="polygon"></param>
        /// <returns></returns>
        public static double FormFaktor(PolygonD polygon)
        {
            double tDp = Perimeter(polygon);
            double tDa = Area(polygon);

            return tDp * tDp * 0.25 / (Math.PI * tDa);
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Calculates the centroid of a polygon.
        /// </summary>
        /// <param name="vertices">A list of Vector2d points which define the polygon.</param>
        /// <returns>The coordinate of the centroid.</returns>
        public static Vector2d Centroid(List<Vector2d> vertices)
        {
            Vector2d centroid = new Vector2d { X = 0.0, Y = 0.0 };
            double signedArea = 0.0;
            double x0; // Current vertex X
            double y0; // Current vertex Y
            double x1; // Next vertex X
            double y1; // Next vertex Y
            double a;  // Partial signed area

            // For all vertices except last
            int i;
            for (i = 0; i < vertices.Count - 1; ++i)
            {
                x0 = vertices[i].X;
                y0 = vertices[i].Y;
                x1 = vertices[i+1].X;
                y1 = vertices[i+1].Y;
                a = x0*y1 - x1*y0;
                signedArea += a;
                centroid.X += (x0 + x1)*a;
                centroid.Y += (y0 + y1)*a;
            }

            // Do last vertex
            x0 = vertices[i].X;
            y0 = vertices[i].Y;
            x1 = vertices[0].X;
            y1 = vertices[0].Y;
            a = x0*y1 - x1*y0;
            signedArea += a;
            centroid.X += (x0 + x1)*a;
            centroid.Y += (y0 + y1)*a;

            signedArea *= 0.5;
            centroid.X /= (6*signedArea);
            centroid.Y /= (6*signedArea);

            return centroid;
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Find the minimum X position of a polygon. 
        /// </summary>
        /// <param name="polygon"> The polygon to be analysed.</param>
        /// <returns>The minimum X coordinate.</returns>
        public static double MinX(PolygonD polygon)
        {
            double minX = double.PositiveInfinity;

            foreach (Vector2d aPoint in polygon)
            {
                double thisX = aPoint.X;
                if (thisX < minX)
                {
                    minX = thisX;
                }
            }

            return minX;
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Find the maximum X position of a polygon. 
        /// </summary>
        /// <param name="polygon"> The polygon to be analysed.</param>
        /// <returns>The maximum X coordinate.</returns>
        public static double MaxX(PolygonD polygon)
        {
            double maxX = double.MinValue;

            foreach (Vector2d aPoint in polygon)
            {
                double thisX = aPoint.X;
                if (thisX > maxX)
                {
                    maxX = thisX;
                }
            }

            return maxX;
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Find the minimum Y position of a polygon. 
        /// </summary>
        /// <param name="polygon"> The polygon to be analysed.</param>
        /// <returns>The minimum Y coordinate.</returns>
        public static double MinY(PolygonD polygon)
        {
            double minY = double.PositiveInfinity;

            foreach (Vector2d aPoint in polygon)
            {
                double thisY = aPoint.Y;
                if (thisY < minY)
                {
                    minY = thisY;
                }
            }

            return minY;
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Find the maximum Y position of a polygon. 
        /// </summary>
        /// <param name="polygon"> The polygon to be analysed.</param>
        /// <returns>The maximum Y coordinate.</returns>
        public static double MaxY(PolygonD polygon)
        {
            double maxY = double.MinValue;

            foreach (Vector2d aPoint in polygon)
            {
                double thisY = aPoint.Y;
                if (thisY > maxY)
                {
                    maxY = thisY;
                }
            }

            return maxY;
        }

        /// <summary>
        /// Test if a point is inside a rectangle.
        /// </summary>
        /// <param name="point">The point to test.</param>
        /// <param name="rectangle">The rectangle for the test.</param>
        /// <returns>True if the point is inside the rectangle or on its border, false otherwise.</returns>
        public static bool PointInRect(Vector2d point, Rect2D rectangle)
        {
            bool isInside = false;
            if (point.X >= rectangle.Left && point.X <= rectangle.Right)
            {
                if (point.Y >= rectangle.Bottom && point.Y <= rectangle.Top)
                {
                    isInside = true;
                }
            }
            return isInside;
        }

        public static int PointInPolygonOld(Vector2d a_Point, Poly2D polygon)
        {

            PolygonD polyD = new PolygonD(polygon.PointsFirstLoop.ToList());

            return PointInPolygonOld(a_Point, polyD);
        }

        // Return True if the point is in the polygon.
        public static bool PointInPolygon(Vector2d pt, Poly2D poly)
        {
            // Get the angle between the point and the
            // first and last vertices.
            int max_point = poly.PointsFirstLoop.Length - 1;
            double total_angle = GetAngle(
                poly.PointsFirstLoop[max_point].X, poly.PointsFirstLoop[max_point].Y,
                pt.X, pt.Y,
                poly.PointsFirstLoop[0].X, poly.PointsFirstLoop[0].Y);

            // Add the angles from the point
            // to each other pair of vertices.
            for (int i = 0; i < max_point; i++)
            {
                total_angle += GetAngle(
                    poly.PointsFirstLoop[i].X, poly.PointsFirstLoop[i].Y,
                    pt.X, pt.Y,
                    poly.PointsFirstLoop[i + 1].X, poly.PointsFirstLoop[i + 1].Y);
            }

            // The total angle should be 2 * PI or -2 * PI if
            // the point is in the polygon and close to zero
            // if the point is outside the polygon.
            return (Math.Abs(total_angle) > 0.000001);
        }

        // Return the angle ABC.
        // Return a value between PI and -PI.
        // Note that the value is the opposite of what you might
        // expect because Y coordinates increase downward.
        public static double GetAngle(double Ax, double Ay,
            double Bx, double By, double Cx, double Cy)
        {
            // Get the dot product.
            double dot_product = DotProduct(Ax, Ay, Bx, By, Cx, Cy);

            // Get the cross product.
            double cross_product = CrossProductLength(Ax, Ay, Bx, By, Cx, Cy);

            // Calculate the angle.
            return Math.Atan2(cross_product, dot_product);
        }

        // Return the dot product AB · BC.
        // Note that AB · BC = |AB| * |BC| * Cos(theta).
        private static double DotProduct(double Ax, double Ay,
            double Bx, double By, double Cx, double Cy)
        {
            // Get the vectors' coordinates.
            double BAx = Ax - Bx;
            double BAy = Ay - By;
            double BCx = Cx - Bx;
            double BCy = Cy - By;

            // Calculate the dot product.
            return (BAx * BCx + BAy * BCy);
        }

        // Return the cross product AB x BC.
        // The cross product is a vector perpendicular to AB
        // and BC having length |AB| * |BC| * Sin(theta) and
        // with direction given by the right-hand rule.
        // For two vectors in the X-Y plane, the result is a
        // vector with X and Y components 0 so the Z component
        // gives the vector's length and direction.
        public static double CrossProductLength(double Ax, double Ay,
            double Bx, double By, double Cx, double Cy)
        {
            // Get the vectors' coordinates.
            double BAx = Ax - Bx;
            double BAy = Ay - By;
            double BCx = Cx - Bx;
            double BCy = Cy - By;

            // Calculate the Z coordinate of the cross product.
            return (BAx * BCy - BAy * BCx);
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Test if a point is inside a polygon.
        /// </summary>
        /// <param name="aPoint"> The point to test. </param>
        /// <param name="polygon"> The polygon for the test. </param>
        /// <returns> 
        /// 1 if point in polygon is true or
        /// 0 if point on polygon boundary is true or
        /// -1 if point is outside of the polygon or
        /// -2 if an unexpected error occurred.
        /// </returns>
        public static int PointInPolygonOld(Vector2d aPoint, PolygonD polygon)
        {
            if (polygon.Count() < 3) return -2; // Unexpected error!

            bool tBDone = false;
            int tNCounter = 0;

            Vector2d pV0;
            Vector2d pV1;

            //public List<Vector2d>.Enumerator beginPoints() { return m_Points.GetEnumerator(); }

            PolygonD.Enumerator tIterPoint = polygon.GetEnumerator();
            tIterPoint.MoveNext();
            while (!tBDone)
            {
                if (tIterPoint.Equals(false))
                    return -2; // Unexpected error!
                pV0 = tIterPoint.Current;

                if (tIterPoint.MoveNext() == false)
                {
                    pV1 = polygon[0]; // frontPoint();
                    tBDone = true;
                }
                else
                {
                    pV1 = tIterPoint.Current;
                }

                ////////////////////////////////////////////////////////
                // Compute orientation of the point to the current edge
                double tDTurn = Turn(pV0, pV1, aPoint);

                if (tDTurn == 0) return 0; // Point on polygon boundary!

                if ((aPoint.Y < pV0.Y) && (pV1.Y < aPoint.Y))
                {
                    // UpDown-Edge
                    if (0.0 < tDTurn)
                    {
                        ++tNCounter;
                    }
                }
                else if ((pV0.Y < aPoint.Y) && (aPoint.Y < pV1.Y))
                {
                    // DownUp-Edge
                    if (tDTurn < 0.0)
                    {
                        ++tNCounter;
                    }

                }
                else if (aPoint.Y == pV0.Y
                         && aPoint.X > pV0.X
                         && pV1.Y < aPoint.Y)
                {
                    // Region change
                    ++tNCounter;
                }
                else if (aPoint.Y == pV1.Y
                         && aPoint.X > pV1.X
                         && pV0.Y < aPoint.Y)
                {
                    // Region change
                    ++tNCounter;
                }
            }

            if ((tNCounter % 2) == 1) return 1; // Point in polygon!

            return -1; // Point not in polygon!
        }


        //=====================================================================================================================================================
        /// <summary>
        /// Returns clockwise-rotated vector, using given angle and centered at vector.
        /// </summary>
        /// <param name="thetaRadian">Angle to rotate the point.</param>
        /// <param name="center">Rotation center.</param>
        /// <param name="vector">The vector to rotate.</param>
        /// <returns>The new vecor after rotation.</returns>
        public static Vector2d RotateVector(double thetaRadian, Vector2d center, Vector2d vector)
        {
            // Basically still similar operation with rotation on origin
            // except we treat given rotation center (vector) as our origin now
            double newX = vector.X - center.X;
            double newY = vector.Y - center.Y;
            double sinTheta = Math.Sin(thetaRadian);
            double cosTheta = Math.Cos(thetaRadian);

            Vector2d vectorRes = new Vector2d(cosTheta * newX - sinTheta * newY, sinTheta * newX + cosTheta * newY);
            vectorRes += center;
            return vectorRes;
        }

        

        //=====================================================================================================================================================
        /// <summary>
        /// Returns clockwise-rotated polygon, using given angle and centered at vector.
        /// </summary>
        /// <param name="thetaRadian">Angle to rotate the polygon.</param>
        /// <param name="center">Rotation center.</param>
        /// <param name="points">The polygon to rotate.</param>
        /// <returns>The new polygon after rotation.</returns>
        public static Vector2d[][] RotatePoly(double thetaRadian, Vector2d center, Vector2d[][] points)
        {
            int loops = points.GetLength(0);            
            Vector2d[][] rotPoly = new Vector2d[loops][];
            for (int i = 0; i < loops; i++)
            {
                int nrPts = points[i].Length;
                Vector2d[] loop = new Vector2d[points[i].Length];
                for (int j = 0; j < nrPts; j++)
                {
                    loop[j] = RotateVector(thetaRadian, center, points[i][j]);
                }

                rotPoly[i] = loop;
            }

            return rotPoly;
        }

        //=====================================================================================================================================================
        public static double Turn(Vector2d point0, Vector2d point1, Vector2d point2)
        {

            Vector2d v0 = point0 - point2;
            Vector2d v1 = point1 - point2;
            return v0.X * v1.Y - v1.X * v0.Y;
        }

        //=====================================================================================================================================================
        /// <summary>
        /// Test if a point is left, on, or right of an infinite line.
        /// larger than 0 for P2 left of the line through P0 and P1 
        /// == 0 for P2 on the line
        /// smaller than 0 for P2 right of the line 
        /// </summary>
        /// <param name="p0"> Start point of the line. </param>
        /// <param name="p1"> End point of the line. </param>
        /// <param name="p2"> Point to test. </param>
        /// <returns> 
        /// larger than 0 for P2 left of the line through P0 and P1 
        /// == 0 for P2 on the line
        /// smaller than 0 for P2 right of the line 
        /// </returns>
        public static double IsLeft(Vector2d p0, Vector2d p1, Vector2d p2)
        {
            return (p1.X - p0.X) * (p2.Y - p0.Y) - (p2.X - p0.X) * (p1.Y - p0.Y);
        }

        /// <summary>
        /// Calculate the angle between the two vectors a and b.
        /// </summary>
        /// <param name="a">First vector</param>
        /// <param name="b">Second vector</param>
        /// <returns>Angle between the two vectors a and b (in degree).</returns>
        public static double AngleBetweenD(Vector2d a, Vector2d b)
        {
            var dotProd = a.DotProduct(b);
            var lenProd = a.Length * b.Length;
            var divOperation = dotProd / lenProd;
            if (divOperation > 1.0)
                divOperation = 1.0;
            if (divOperation < -1.0)
                divOperation = -1.0;
            return Math.Acos(divOperation) * (180.0 / Math.PI);
        }

        /// <summary>
        /// Calculate the angle between the two vectors a and b.
        /// </summary>
        /// <param name="a">First vector</param>
        /// <param name="b">Second vector</param>
        /// <returns>Angle between the two vectors a and b (in radians).</returns>
        public static double AngleBetweenR(Vector2d a, Vector2d b)
        {
            var dotProd = a.DotProduct(b);
            var lenProd = a.Length * b.Length;
            var divOperation = dotProd / lenProd;
            if (divOperation > 1.0)
                divOperation = 1.0;
            if (divOperation < -1.0)
                divOperation = -1.0;
            return Math.Acos(divOperation);
        }

        /// <summary>
        /// Calculates the angle between two lines.
        /// </summary>
        /// <param name="line1">First line</param>
        /// <param name="line2">Second line</param>
        /// <returns></returns>
        private static double AngleBetweenLines(Line2D line1, Line2D line2)
        {
            double x1 = line1.Start.X;
            double x2 = line1.End.X;
            double x3 = line2.Start.X;
            double x4 = line2.End.X;
            double y1 = line1.Start.Y;
            double y2 = line1.End.Y;
            double y3 = line2.Start.Y;
            double y4 = line2.End.Y;
            Double angle = Math.Atan2(y2 - y1, x2 - x1) - Math.Atan2(y4 - y3, x4 - x3);

            //double tanOfAngle=(line2.Gradient() - line1.Gradient())/(1+line1.Gradient() * line2.Gradient());
            //double angle = Math.Atan(tanOfAngle)* 180/Math.PI;
            return angle;
        }

        /// <summary>
        /// Calculate the angle between the three vectors a, b and c in radians.
        /// </summary>
        /// <param name="a">First vector</param>
        /// <param name="b">Second vector</param>
        /// <param name="center">Center to relate the angle to</param>
        /// <returns>Angle between the two vectors a and b (in radians).</returns>
        public static double AngleBetween(Vector2d a, Vector2d b, Vector2d center)
        {
            double ax = a.X - center.X, ay = a.Y - center.Y;
            double bx = b.X - center.X, by = b.Y - center.Y;
            double y = ax * by - ay * bx;
            double x = ax * bx + ay * by;
            return Math.Atan2(y, x);
        }

        /// <summary>
        /// Calculate the angle between the three vectors a, b and c in degree.
        /// </summary>
        /// <param name="a">First vector</param>
        /// <param name="b">Second vector</param>
        /// <param name="center">Center to relate the angle to</param>
        /// <returns>Angle between the two vectors a and b (in degree).</returns>
        public static double AngleBetweenDegree(Vector2d a, Vector2d b, Vector2d center)
        {
            double ax = a.X - center.X, ay = a.Y - center.Y;
            double bx = b.X - center.X, by = b.Y - center.Y;
            double y = ax * by - ay * bx;
            double x = ax * bx + ay * by;
            return Math.Atan2(y, x) * (180.0 / Math.PI);
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Calculates the angle (in radians) between two vectors pointing outward from one center.
        /// </summary>
        /// <param name="c">Center point</param>
        /// <param name="p0">First point</param>
        /// <param name="p1">Second point</param>
        /// <returns></returns>
        private static double Angle3Points(Vector2d c, Vector2d p0, Vector2d p1)
        {
            double p0c = Math.Sqrt(Math.Pow(c.X - p0.X, 2) + Math.Pow(c.Y - p0.Y, 2)); // p0->c (b)   
            var p1c = Math.Sqrt(Math.Pow(c.X - p1.X, 2) + Math.Pow(c.Y - p1.Y, 2)); // p1->c (a)
            var p0p1 = Math.Sqrt(Math.Pow(p1.X - p0.X, 2) + Math.Pow(p1.Y - p0.Y, 2)); // p0->p1 (c)
            return Math.Acos((p1c * p1c + p0c * p0c - p0p1 * p0p1) / (2 * p1c * p0c));
        }

        //==============================================================================================
        /// <summary>
        /// Compute the dot product AB . AC
        /// http://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
        /// </summary>
        /// <param name="pointA"></param>
        /// <param name="pointB"></param>
        /// <param name="pointC"></param>
        /// <returns></returns>
        public static double DotProduct(double[] pointA, double[] pointB, double[] pointC)
        {
            double[] ab = new double[2];
            double[] bc = new double[2];
            ab[0] = pointB[0] - pointA[0];
            ab[1] = pointB[1] - pointA[1];
            bc[0] = pointC[0] - pointB[0];
            bc[1] = pointC[1] - pointB[1];
            double dot = ab[0] * bc[0] + ab[1] * bc[1];

            return dot;
        }

        /// <summary>
        /// Compute the cross product AB x AC
        /// </summary>
        /// <param name="pointA"></param>
        /// <param name="pointB"></param>
        /// <param name="pointC"></param>
        /// <returns></returns>
        public static double CrossProduct(double[] pointA, double[] pointB, double[] pointC)
        {
            double[] ab = new double[2];
            double[] ac = new double[2];
            ab[0] = pointB[0] - pointA[0];
            ab[1] = pointB[1] - pointA[1];
            ac[0] = pointC[0] - pointA[0];
            ac[1] = pointC[1] - pointA[1];
            double cross = ab[0] * ac[1] - ab[1] * ac[0];

            return cross;
        }

        /// <summary>
        /// Finds the cross product of the 2 vectors created by the 3 vertices.
        /// Vector 1 = v1 -> v2, Vector 2 = v2 -> v3
        /// The vectors make a "right turn" if the sign of the cross product is negative.
        /// The vectors make a "left turn" if the sign of the cross product is positive.
        /// The vectors are colinear (on the same line) if the cross product is zero.
        /// </summary>
        /// <param name="p1">First point.</param>
        /// <param name="p2">Second point.</param>
        /// <param name="p3">Third point.</param>
        /// <returns>Cross product of the two vectors.</returns>
        public static double CrossProduct(Vector2d pntA, Vector2d pnB, Vector2d pntC)
        {
            return (pnB.X - pntA.X) * (pntC.Y - pntA.Y) - (pntC.X - pntA.X) * (pnB.Y - pntA.Y);
        }

        //public static double CrossProduct(Vector2d pntA, Vector2d pnB, Vector2d pntC)
        //{
        //    Vector2d ab = new Vector2d();
        //    Vector2d ac = new Vector2d();
        //    ab.X = pnB.X - pntA.X;
        //    ab.Y = pnB.Y - pntA.Y;
        //    ac.X = pntC.X - pntA.X;
        //    ac.Y = pntC.Y - pntA.Y;
        //    return ab.X * ac.Y - ab.Y * ac.X;
        //}

        //==============================================================================================
        /// <summary>
        /// Generates a Voronoi Diagram based on a series of points.
        /// </summary>
        public static List<Line2D> VoronoiDiagramLinesOnly(Vector2d[] points)
        {

            List<DefaultVertex> vertices = new List<DefaultVertex>();
            for (var i = 0; i < points.Length; i++)
            {
                var vi = new DefaultVertex();
                vi.Position = new double[2];
                vi.Position[0] = points[i].X;
                vi.Position[1] = points[i].Y;
                vertices.Add(vi);
            }

            VoronoiMesh<DefaultVertex, DefaultTriangulationCell<DefaultVertex>, VoronoiEdge<DefaultVertex, DefaultTriangulationCell<DefaultVertex>>> voronoiMesh;
            voronoiMesh = VoronoiMesh.Create<DefaultVertex, DefaultTriangulationCell<DefaultVertex>, VoronoiEdge<DefaultVertex, DefaultTriangulationCell<DefaultVertex>>>(vertices);

            List<Line2D> lines = new List<Line2D>();

            foreach (var edge in voronoiMesh.Edges)
            {
                Vector2d from = GetCircumcenter(edge.Source.Vertices[0].Position[0], edge.Source.Vertices[0].Position[1],
                    edge.Source.Vertices[1].Position[0], edge.Source.Vertices[1].Position[1],
                    edge.Source.Vertices[2].Position[0], edge.Source.Vertices[2].Position[1]);

                Vector2d to = GetCircumcenter(edge.Target.Vertices[0].Position[0], edge.Target.Vertices[0].Position[1],
                    edge.Target.Vertices[1].Position[0], edge.Target.Vertices[1].Position[1],
                    edge.Target.Vertices[2].Position[0], edge.Target.Vertices[2].Position[1]);

                Line2D line = new Line2D( from.X, from.Y, to.X, to.Y);

                bool exists = false;
                foreach (Line2D existingLine in lines)
                {
                    if (line.IsSimilarTo(existingLine, 0.0001))
                    {
                        exists = true;
                        break;
                    }
                }
                if (!exists)
                    lines.Add(line);
            }
            
            foreach (var cell in voronoiMesh.Vertices)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (cell.Adjacency[i] == null)
                    {
                        Vector2d from = GetCircumcenter(cell.Vertices[0].Position[0], cell.Vertices[0].Position[1],
                            cell.Vertices[1].Position[0], cell.Vertices[1].Position[1],
                            cell.Vertices[2].Position[0], cell.Vertices[2].Position[1]);
                        var t = cell.Vertices.Where((_, j) => j != i).ToArray();
                        var factor = 100 * IsLeft(t[0].Position.ToVector2d(), t[1].Position.ToVector2d(), from)
                            * IsLeft(t[0].Position.ToVector2d(), t[1].Position.ToVector2d(), CenterTriangle(cell.Vertices[0].Position.ToVector2d(), cell.Vertices[1].Position.ToVector2d(), cell.Vertices[2].Position.ToVector2d()));
                        Vector2d dir = new Vector2d(0.5 * (t[0].Position[0] + t[1].Position[0]), 0.5 * (t[0].Position[1] + t[1].Position[1])) - from;
                        var to = from + factor * dir;
                        lines.Add( new Line2D(from.X, from.Y, to.X, to.Y) );
                    }
                }
            }


            /*
            foreach (var edge in voronoiMesh.Edges)
            {
                for (int i = 0; i < edge.Source.Vertices.Length; i++)
                {
                    int j = i + 1;
                    if (i == edge.Source.Vertices.Length - 1)
                        j = 0;

                    Line2D line = new Line2D(edge.Source.Vertices[i].Position[0], edge.Source.Vertices[i].Position[1], edge.Source.Vertices[j].Position[0], edge.Source.Vertices[j].Position[1]);
                    bool exists = false;
                    foreach (Line2D existingLine in lines)
                    {
                        if (line.IsSimilarTo(existingLine, 0.0001))
                        {
                            exists = true;
                            break;
                        }
                    }
                    if (!exists)
                        lines.Add(line);
                }
            }

            //Triangulation!
            
            foreach (var cell in voronoiMesh.Vertices)
            {
                for (int i=0; i< cell.Vertices.Length; i++)
                {
                    int j=i+1;
                    if (i==cell.Vertices.Length-1)
                        j=0;

                    Line2D line = new Line2D(cell.Vertices[i].Position[0], cell.Vertices[i].Position[1], cell.Vertices[j].Position[0], cell.Vertices[j].Position[1]);
                    bool exists = false;
                    foreach (Line2D existingLine in lines)
                    {
                        if (line.IsSimilarTo(existingLine, 0.0001))
                        {
                            exists = true;
                            break;
                        }
                    }
                    if (!exists)
                        lines.Add(line);
                }     
            }
             * */
            return lines;
        }

        //==============================================================================================
        /// <summary>
        /// Generates a Voronoi Diagram based on a series of points inside a boundary poly.
        /// </summary>
        public static List<Line2D> VoronoiDiagramLinesOnly(Vector2d[] points, Poly2D boundary)
        {

            List<DefaultVertex> vertices = new List<DefaultVertex>();
            for (var i = 0; i < points.Length; i++)
            {
                var vi = new DefaultVertex();
                vi.Position = new double[2];
                vi.Position[0] = points[i].X;
                vi.Position[1] = points[i].Y;
                vertices.Add(vi);
            }

            VoronoiMesh<DefaultVertex, DefaultTriangulationCell<DefaultVertex>, VoronoiEdge<DefaultVertex, DefaultTriangulationCell<DefaultVertex>>> voronoiMesh;
            voronoiMesh = VoronoiMesh.Create<DefaultVertex, DefaultTriangulationCell<DefaultVertex>, VoronoiEdge<DefaultVertex, DefaultTriangulationCell<DefaultVertex>>>(vertices);

            List<Line2D> lines = new List<Line2D>();

            foreach (var edge in voronoiMesh.Edges)
            {
                Vector2d from = GetCircumcenter(edge.Source.Vertices[0].Position[0], edge.Source.Vertices[0].Position[1],
                    edge.Source.Vertices[1].Position[0], edge.Source.Vertices[1].Position[1],
                    edge.Source.Vertices[2].Position[0], edge.Source.Vertices[2].Position[1]);

                Vector2d to = GetCircumcenter(edge.Target.Vertices[0].Position[0], edge.Target.Vertices[0].Position[1],
                    edge.Target.Vertices[1].Position[0], edge.Target.Vertices[1].Position[1],
                    edge.Target.Vertices[2].Position[0], edge.Target.Vertices[2].Position[1]);

                Line2D line = new Line2D(from.X, from.Y, to.X, to.Y);

                bool exists = false;
                foreach (Line2D existingLine in lines)
                {
                    if (line.IsSimilarTo(existingLine, 0.0001))
                    {
                        exists = true;
                        break;
                    }
                }
                
                if (!exists)
                {
                    //mit boundary verschneiden
                    bool containsStartP = boundary.ContainsPoint(line.Start);
                    bool containsEndP = boundary.ContainsPoint(line.End);

                    List<Vector2d> intPoints = line.Intersect(boundary);
                    if (intPoints == null)
                        if (containsStartP && containsEndP)
                            lines.Add(line);
                    else
                    {
                        if (containsStartP)
                            line = new Line2D(line.Start, intPoints[0]);
                        else if (containsEndP)
                            line = new Line2D(line.End, intPoints[0]);
                        lines.Add(line);
                    }
                }
            }

            foreach (var cell in voronoiMesh.Vertices)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (cell.Adjacency[i] == null)
                    {
                        Vector2d from = GetCircumcenter(cell.Vertices[0].Position[0], cell.Vertices[0].Position[1],
                            cell.Vertices[1].Position[0], cell.Vertices[1].Position[1],
                            cell.Vertices[2].Position[0], cell.Vertices[2].Position[1]);
                        var t = cell.Vertices.Where((_, j) => j != i).ToArray();
                        var factor = 100 * IsLeft(t[0].Position.ToVector2d(), t[1].Position.ToVector2d(), from) * IsLeft(t[0].Position.ToVector2d(), t[1].Position.ToVector2d(), CenterTriangle(cell.Vertices[0].Position.ToVector2d(), cell.Vertices[1].Position.ToVector2d(), cell.Vertices[2].Position.ToVector2d()));
                        Vector2d dir = new Vector2d(0.5 * (t[0].Position[0] + t[1].Position[0]), 0.5 * (t[0].Position[1] + t[1].Position[1])) - from;
                        var to = from + factor * dir;

                        Line2D line = new Line2D(from.X, from.Y, to.X, to.Y);

                        //mit boundary verschneiden
                        List<Vector2d> intPoints = line.Intersect(boundary);
                        if (intPoints == null)
                            if (boundary.ContainsPoint(line.Start) && boundary.ContainsPoint(line.End))
                                lines.Add(line);
                        else
                        {
                            if (boundary.ContainsPoint(line.Start))
                                line = new Line2D(line.Start, intPoints[0]);
                            else if (boundary.ContainsPoint(line.End))
                                line = new Line2D(line.End, intPoints[0]);
                            lines.Add(line);
                        }

                    }
                }
            }
            return lines;
        }

        static Vector2d CenterTriangle(Vector2d p1, Vector2d p2, Vector2d p3 )
        {
            return ((p1 + p2 + p3) / 3);
        }

        //needed for voronoi
        public static Vector2d GetCircumcenter(double x1, double y1, double x2, double y2, double x3, double y3)
        {
            // From MathWorld: http://mathworld.wolfram.com/Circumcircle.html

            double[][] points = new double[3][];
            points[0] = new double[2];
            points[1] = new double[2];
            points[2] = new double[2]; 
            points[0][0] = x1;
            points[0][1] = y1;
            points[1][0] = x2;
            points[1][1] = y2;
            points[2][0] = x3;
            points[2][1] = y3;

            double[,] m = new double[3, 3];

            // x, y, 1
            for (int i = 0; i < 3; i++)
            {
                m[i, 0] = points[i][0];
                m[i, 1] = points[i][1];
                m[i, 2] = 1;
            }
            var a = StarMath.determinant(m);

            // size, y, 1
            for (int i = 0; i < 3; i++)
            {
                m[i, 0] = StarMath.norm2(points[i], 2, true);
            }
            var dx = -StarMath.determinant(m);

            // size, x, 1
            for (int i = 0; i < 3; i++)
            {
                m[i, 1] = points[i][0];
            }
            var dy = StarMath.determinant(m);

            // size, x, y
            for (int i = 0; i < 3; i++)
            {
                m[i, 2] = points[i][1];
            }
            //var c = -StarMath.determinant(m);

            var s = -1.0 / (2.0 * a);
            //var r = Math.Abs(s) * Math.Sqrt(dx * dx + dy * dy - 4 * a * c);

            return new Vector2d(s * dx, s * dy);
        }


        //==============================================================================================
        /// <summary>
        /// Generates Points in a regular grid inside a region with obstacle lines by filling the space between.
        /// </summary>
        public static List<Vector2d> GetGridPoints(Vector2d fillPoint, Rect2D boundingRect, List<Line2D> obstacleLines, double cellSize)
        {
            int countX = Convert.ToInt32(Math.Round(boundingRect.Width / cellSize, 0));
            int countY = Convert.ToInt32(Math.Round(boundingRect.Height / cellSize, 0));
            Bitmap bmp = new Bitmap(countX, countY);
            
            

            //schalte alle Gridpoints, die unter einer Linie liegen inaktiv
            Graphics graph = Graphics.FromImage(bmp);
            Pen pen = new Pen(Brushes.Red);
            foreach (Line2D line in obstacleLines)
            {
                Vector2d p1 = new Vector2d(line.Start.X - boundingRect.TopLeft.X, line.Start.Y - boundingRect.Bottom);
                Vector2d p2 = new Vector2d(line.End.X - boundingRect.TopLeft.X, line.End.Y - boundingRect.Bottom);

                double gridX = Math.Round((p1.X / boundingRect.Width) * countX);
                double gridY = Math.Round((p1.Y / boundingRect.Height) * countY);
                Point point1 = new Point((int)gridX, (int)gridY);
                gridX = Math.Round((p2.X / boundingRect.Width) * countX);
                gridY = Math.Round((p2.Y / boundingRect.Height) * countY);
                Point point2 = new Point((int)gridX, (int)gridY);
                graph.DrawLine(pen, point1, point2);
            }

            //at random Point until ratio of filled to all is > 0.3
            Point fPoint;

            Vector2d p = new Vector2d(fillPoint.X - boundingRect.TopLeft.X, fillPoint.Y - boundingRect.Bottom);
            double gX = Math.Round((p.X / boundingRect.Width) * countX);
            double gY = Math.Round((p.Y / boundingRect.Height) * countY);
            fPoint = new Point((int)gX, (int)gY);

            //int nrOfRuns = 0;

            AbstractFloodFiller floodFiller = new AbstractFloodFiller(bmp);
            QueueLinearFloodFiller queueFloodFiller = new QueueLinearFloodFiller(floodFiller);
            int count = queueFloodFiller.FloodFill(fPoint);
            //nrOfRuns++;

            List<Vector2d> gridPoints = new List<Vector2d>();

            for (int i = 0; i < queueFloodFiller.Bitmap.Bitmap.Width; i++)
                for (int j = 0; j < queueFloodFiller.Bitmap.Bitmap.Height; j++)
                {
                    byte[] bitmapBits = queueFloodFiller.Bitmap.Bits;

                    int idx = (floodFiller.Bitmap.Stride * j) + (i * floodFiller.Bitmap.PixelFormatSize);

                    if ((bitmapBits[idx] == 255) && (bitmapBits[idx + 1] == 0) && (bitmapBits[idx + 2] == 255))
                        gridPoints.Add(new Vector2d(boundingRect.Left + i * cellSize, boundingRect.Bottom + j * cellSize));

                }
            return gridPoints;
        }

        private class LineStringAdder : IGeometryComponentFilter
        {
           NetTopologySuite.Operation.Polygonize.PolygonizeGraph _graph;
            public LineStringAdder(NetTopologySuite.Operation.Polygonize.PolygonizeGraph graph)
            {
                _graph = graph;
            }

            public void Filter(IGeometry g)
            {
                var lineString = g as GeoAPI.Geometries.ILineString;
                if (lineString != null)
                    _graph.AddEdge(lineString);

            }
        }
        public static PolygonizeGraph Add_lines_to_graph(ICollection<IGeometry> geomList)
        {
            IGeometry geom_init = geomList.First() as IGeometry;
            NetTopologySuite.Operation.Polygonize.PolygonizeGraph _graph = new NetTopologySuite.Operation.Polygonize.PolygonizeGraph(geom_init.Factory);

            foreach (var geometry in geomList)
            {

                LineStringAdder _lineStringAdder = new LineStringAdder(_graph);
                geometry.Apply(_lineStringAdder);

            }
            // System.Windows.Forms.MessageBox.Show("geom num " + _graph.Nodes.Count.ToString() + " " + geomList.Count.ToString());
            return _graph;
        }

        public static ICollection<IGeometry> Polygonize(NetTopologySuite.Operation.Polygonize.PolygonizeGraph _graph)
        {
            ICollection<IGeometry> _polyList = new List<IGeometry>();
            ICollection<ILineString> _dangles = new List<ILineString>();
            ICollection<ILineString> _cutEdges = new List<ILineString>();
            IList<IGeometry> _invalidRingLines = new List<IGeometry>();
            List<EdgeRing> _holeList = new List<EdgeRing>();
            List<EdgeRing> _shellList = new List<EdgeRing>();

            // if no geometries were supplied it's possible that graph is null
            _dangles = _graph.DeleteDangles();
            _cutEdges = _graph.DeleteCutEdges();
            var edgeRingList = _graph.GetEdgeRings();

            IList<EdgeRing> validEdgeRingList = new List<EdgeRing>();
            foreach (var er in edgeRingList)
            {
                if (er.IsValid)
                    validEdgeRingList.Add(er);
                else _invalidRingLines.Add(er.LineString);
            }


            foreach (var er in validEdgeRingList)
            {
                er.ComputeHole();
                if (er.IsHole)
                    _holeList.Add(er);
                else _shellList.Add(er);
            }

            foreach (EdgeRing holeEdgeRing in _shellList)
            {
                //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!buggy part with null reference exception
                EdgeRing shell = null;
                try
                {
                    shell = EdgeRing.FindEdgeRingContaining(holeEdgeRing, _shellList);
                }
                catch (System.NullReferenceException)
                {
                    continue;
                }


                if (shell != null)
                {
                    shell.AddHole(holeEdgeRing);
                }

                /*
                if (!holeER.hasShell()) {
                    System.out.println("DEBUG: Outer hole: " + holeER);
                }
                */
            }
            // order the shells to make any subsequent processing deterministic
            _shellList.Sort(new EdgeRing.EnvelopeComparator());

            var includeAll = true;

            foreach (var er in _shellList)
            {
                if (includeAll || er.IsIncluded)
                {
                    _polyList.Add(er.Polygon);
                }
            }
            return _polyList;
        }
    }
}

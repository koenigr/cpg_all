﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GeoAlgorithms3D.cs 
//  Copyright (C) 2014/10/18  12:58 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using Tess;
using OpenTK;

namespace CPlan.Geometry
{
    /*public class Geom : CGeom3d
    {
        public Geom(): base() {}
    }*/
    

    /// <summary>
    /// Provides standard algorithms and auxiliary methods for computational geometry.
    /// </summary>
    [Serializable]
    public class GeoAlgorithms3D : CGeom3d   
    {

        //===================================================================================================================================================================
        public static Triangle3D[] Triangulate3D(Poly3D poly)
        {
            Triangle[] result = new Triangle[0];
            Vec_3d[][] points = new Vec_3d[1][];
            points[0] = poly.PointsVec3D;
            COpenGLTesselator.Tesselate(points, ref result);

            Triangle3D[] result2 = new Triangle3D[result.Length];
            int index = 0;
            foreach (Triangle tri in result)
            {
                //int x;
                //if (tri.m_pP1 == null)
                //    x = 5;

                result2[index] = new Triangle3D(tri.m_pP1, tri.m_pP2, tri.m_pP3);
                ++index;


            }

            return result2;
        }

        //===================================================================================================================================================================
        private double GetDistMitte(CGeom3d.Vec_3d mitte, CGeom3d.Vec_3d blick, CGeom3d.Vec_3d p, double angle, ref CGeom3d.Vec_3d erg, double zoomfak)
        {
            // -- distance point - line 
            double sa, sb, sc, sd;
            double q, r;
            Vector3d bp = new Vector3d();

            sa = blick.x;
            sb = blick.y;
            sc = blick.z;
            sd = -p.x * sa - p.y * sb - p.z * sc;

            q = sa * blick.x + sb * blick.y + sc * blick.z;

            r = (sa * mitte.x + sb * mitte.y + sc * mitte.z + sd) / q;

            bp.X = mitte.x - blick.x * r;
            bp.Y = mitte.y - blick.y * r;
            bp.Z = mitte.z - blick.z * r;

            double distPL = Math.Sqrt(Math.Pow(bp.X - p.x, 2) + Math.Pow(bp.Y - p.y, 2) + Math.Pow(bp.Z - p.z, 2));

            double distNP = distPL / Math.Tan(angle / 2.0f) * zoomfak;

            (erg).x = bp.X - (blick.x * distNP);
            (erg).y = bp.Y - (blick.y * distNP);
            (erg).z = bp.Z - (blick.z * distNP);

            double dist = Math.Sqrt(Math.Pow((erg).x - mitte.x, 2) + Math.Pow((erg).y - mitte.y, 2) + Math.Pow((erg).z - mitte.z, 2));

            return dist;
        }

        //===================================================================================================================================================================
        public static Vector3d Rotate(Vector3d v, Vector3d axis, double angle)
        {
            // -- Rotate the point (x,y,z) around the vector (u,v,w)
            // -- Function RotatePointAroundVector(x#,y#,z#,u#,v#,w#,a#)
            double ux = axis.X * v.X;
            double uy = axis.X * v.Y;
            double uz = axis.X * v.Z;
            double vx = axis.Y * v.X;
            double vy = axis.Y * v.Y;
            double vz = axis.Y * v.Z;
            double wx = axis.Z * v.X;
            double wy = axis.Z * v.Y;
            double wz = axis.Z * v.Z;
            double sa = Math.Sin(angle);
            double ca = Math.Cos(angle);
            double x = axis.X
                    * (ux + vy + wz)
                    + (v.X * (axis.Y * axis.Y + axis.Z * axis.Z) - axis.X
                            * (vy + wz)) * ca + (-wy + vz) * sa;
            double y = axis.Y
                    * (ux + vy + wz)
                    + (v.Y * (axis.X * axis.X + axis.Z * axis.Z) - axis.Y
                            * (ux + wz)) * ca + (wx - uz) * sa;
            double z = axis.Z
                    * (ux + vy + wz)
                    + (v.Z * (axis.X * axis.X + axis.Y * axis.Y) - axis.Z
                            * (ux + vy)) * ca + (-vx + uy) * sa;

            return new Vector3d(x, y, z);
        }


    }
}

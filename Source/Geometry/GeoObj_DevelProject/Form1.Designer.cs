﻿namespace WindowsFormsApplication6
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripProgressBar_analyse = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel_out = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.glControl1 = new OpenTK.GLControl();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripB_calcVisibility = new System.Windows.Forms.ToolStripButton();
            this.toolStripB_showArea = new System.Windows.Forms.ToolStripButton();
            this.toolStripB_showDist = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripB_calcDistWalls = new System.Windows.Forms.ToolStripButton();
            this.toolStripB_showDistWalls = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.calcShortPath = new System.Windows.Forms.ToolStripButton();
            this.toolStripB_showMetricDist = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripB_showChoice = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripB_clipPolys = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripB_drawWall = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.LeftToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBar_analyse,
            this.toolStripStatusLabel_out});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(849, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripProgressBar_analyse
            // 
            this.toolStripProgressBar_analyse.Name = "toolStripProgressBar_analyse";
            this.toolStripProgressBar_analyse.Size = new System.Drawing.Size(100, 16);
            this.toolStripProgressBar_analyse.Step = 1;
            this.toolStripProgressBar_analyse.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.toolStripProgressBar_analyse.Visible = false;
            
            // 
            // toolStripStatusLabel_out
            // 
            this.toolStripStatusLabel_out.Name = "toolStripStatusLabel_out";
            this.toolStripStatusLabel_out.Size = new System.Drawing.Size(185, 17);
            this.toolStripStatusLabel_out.Text = "Move elements by drag and drop!";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.glControl1);
            this.splitContainer1.Panel2Collapsed = true;
            this.splitContainer1.Size = new System.Drawing.Size(745, 497);
            this.splitContainer1.SplitterDistance = 389;
            this.splitContainer1.TabIndex = 1;
            // 
            // glControl1
            // 
            this.glControl1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.glControl1.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.glControl1.BackColor = System.Drawing.Color.Black;
            this.glControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.glControl1.Location = new System.Drawing.Point(0, 0);
            this.glControl1.Name = "glControl1";
            this.glControl1.Size = new System.Drawing.Size(743, 495);
            this.glControl1.TabIndex = 9;
            this.glControl1.VSync = false;
            this.glControl1.Load += new System.EventHandler(this.glControl1_Load);
            this.glControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.glControl1_Paint);
            this.glControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseDown);
            this.glControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseMove);
            this.glControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseUp);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.AutoScroll = true;
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(745, 497);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            // 
            // toolStripContainer1.LeftToolStripPanel
            // 
            this.toolStripContainer1.LeftToolStripPanel.Controls.Add(this.toolStrip1);
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.RightToolStripPanelVisible = false;
            this.toolStripContainer1.Size = new System.Drawing.Size(849, 544);
            this.toolStripContainer1.TabIndex = 10;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripB_calcVisibility,
            this.toolStripB_showArea,
            this.toolStripB_showDist,
            this.toolStripSeparator2,
            this.toolStripB_calcDistWalls,
            this.toolStripB_showDistWalls,
            this.toolStripButton5,
            this.toolStripSeparator3,
            this.calcShortPath,
            this.toolStripB_showMetricDist,
            this.toolStripButton6,
            this.toolStripB_showChoice,
            this.toolStripSeparator6,
            this.toolStripB_clipPolys,
            this.toolStripSeparator1,
            this.toolStripB_drawWall,
            this.toolStripButton1,
            this.toolStripSeparator4,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripSeparator5,
            this.toolStripButton4});
            this.toolStrip1.Location = new System.Drawing.Point(0, 3);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(104, 399);
            this.toolStrip1.TabIndex = 0;
            // 
            // toolStripB_calcVisibility
            // 
            this.toolStripB_calcVisibility.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripB_calcVisibility.Image = ((System.Drawing.Image)(resources.GetObject("toolStripB_calcVisibility.Image")));
            this.toolStripB_calcVisibility.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripB_calcVisibility.Name = "toolStripB_calcVisibility";
            this.toolStripB_calcVisibility.Size = new System.Drawing.Size(102, 19);
            this.toolStripB_calcVisibility.Text = "Calculate Visib.";
            this.toolStripB_calcVisibility.Click += new System.EventHandler(this.toolStripButton_calc_Click);
            // 
            // toolStripB_showArea
            // 
            this.toolStripB_showArea.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripB_showArea.Image = ((System.Drawing.Image)(resources.GetObject("toolStripB_showArea.Image")));
            this.toolStripB_showArea.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripB_showArea.Name = "toolStripB_showArea";
            this.toolStripB_showArea.Size = new System.Drawing.Size(102, 19);
            this.toolStripB_showArea.Text = "Show Area";
            this.toolStripB_showArea.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripB_showDist
            // 
            this.toolStripB_showDist.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripB_showDist.Image = ((System.Drawing.Image)(resources.GetObject("toolStripB_showDist.Image")));
            this.toolStripB_showDist.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripB_showDist.Name = "toolStripB_showDist";
            this.toolStripB_showDist.Size = new System.Drawing.Size(102, 19);
            this.toolStripB_showDist.Text = "Show Distance";
            this.toolStripB_showDist.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(102, 6);
            // 
            // toolStripB_calcDistWalls
            // 
            this.toolStripB_calcDistWalls.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripB_calcDistWalls.Image = ((System.Drawing.Image)(resources.GetObject("toolStripB_calcDistWalls.Image")));
            this.toolStripB_calcDistWalls.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripB_calcDistWalls.Name = "toolStripB_calcDistWalls";
            this.toolStripB_calcDistWalls.Size = new System.Drawing.Size(102, 19);
            this.toolStripB_calcDistWalls.Text = "Calculate Dist.";
            this.toolStripB_calcDistWalls.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripB_showDistWalls
            // 
            this.toolStripB_showDistWalls.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripB_showDistWalls.Image = ((System.Drawing.Image)(resources.GetObject("toolStripB_showDistWalls.Image")));
            this.toolStripB_showDistWalls.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripB_showDistWalls.Name = "toolStripB_showDistWalls";
            this.toolStripB_showDistWalls.Size = new System.Drawing.Size(102, 19);
            this.toolStripB_showDistWalls.Text = "Show DistWalls";
            this.toolStripB_showDistWalls.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(102, 19);
            this.toolStripButton5.Text = "White Cells";
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click_1);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(102, 6);
            // 
            // calcShortPath
            // 
            this.calcShortPath.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.calcShortPath.Image = ((System.Drawing.Image)(resources.GetObject("calcShortPath.Image")));
            this.calcShortPath.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.calcShortPath.Name = "calcShortPath";
            this.calcShortPath.Size = new System.Drawing.Size(102, 19);
            this.calcShortPath.Text = "Calc. ShortPath";
            this.calcShortPath.Click += new System.EventHandler(this.calcShortPath_Click);
            // 
            // toolStripB_showMetricDist
            // 
            this.toolStripB_showMetricDist.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripB_showMetricDist.Image = ((System.Drawing.Image)(resources.GetObject("toolStripB_showMetricDist.Image")));
            this.toolStripB_showMetricDist.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripB_showMetricDist.Name = "toolStripB_showMetricDist";
            this.toolStripB_showMetricDist.Size = new System.Drawing.Size(102, 19);
            this.toolStripB_showMetricDist.Text = "Show Metric Dist.";
            this.toolStripB_showMetricDist.Click += new System.EventHandler(this.toolStripB_showMetricDist_Click);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(102, 19);
            this.toolStripButton6.Text = "Show Angle Dist";
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // toolStripB_showChoice
            // 
            this.toolStripB_showChoice.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripB_showChoice.Image = ((System.Drawing.Image)(resources.GetObject("toolStripB_showChoice.Image")));
            this.toolStripB_showChoice.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripB_showChoice.Name = "toolStripB_showChoice";
            this.toolStripB_showChoice.Size = new System.Drawing.Size(102, 19);
            this.toolStripB_showChoice.Text = "Show Choice";
            this.toolStripB_showChoice.Click += new System.EventHandler(this.toolStripB_showAngularDist_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(102, 6);
            // 
            // toolStripB_clipPolys
            // 
            this.toolStripB_clipPolys.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripB_clipPolys.Image = ((System.Drawing.Image)(resources.GetObject("toolStripB_clipPolys.Image")));
            this.toolStripB_clipPolys.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripB_clipPolys.Name = "toolStripB_clipPolys";
            this.toolStripB_clipPolys.Size = new System.Drawing.Size(102, 19);
            this.toolStripB_clipPolys.Text = "Clip Polygons";
            this.toolStripB_clipPolys.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(102, 6);
            // 
            // toolStripB_drawWall
            // 
            this.toolStripB_drawWall.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripB_drawWall.Image = ((System.Drawing.Image)(resources.GetObject("toolStripB_drawWall.Image")));
            this.toolStripB_drawWall.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripB_drawWall.Name = "toolStripB_drawWall";
            this.toolStripB_drawWall.Size = new System.Drawing.Size(102, 19);
            this.toolStripB_drawWall.Text = "Draw Wall";
            this.toolStripB_drawWall.ToolTipText = "Drag&drop to create wall";
            this.toolStripB_drawWall.Click += new System.EventHandler(this.toolStripB_drawLine_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(102, 19);
            this.toolStripButton1.Text = "Delete Element";
            this.toolStripButton1.ToolTipText = "Select element to delete it";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click_1);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(102, 6);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(102, 19);
            this.toolStripButton2.Text = "Start Particles";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click_1);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(102, 19);
            this.toolStripButton3.Text = "Stop Particles";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click_1);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(102, 6);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(102, 19);
            this.toolStripButton4.Text = "Create Voronoi";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 544);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "Form1";
            this.Text = "GeoObj Test Application";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.LeftToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.LeftToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private OpenTK.GLControl glControl1;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripB_calcVisibility;
        private System.Windows.Forms.ToolStripButton toolStripB_showArea;
        private System.Windows.Forms.ToolStripButton toolStripB_showDist;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripB_clipPolys;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_out;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar_analyse;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripB_calcDistWalls;
        private System.Windows.Forms.ToolStripButton toolStripB_showDistWalls;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripB_drawWall;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton calcShortPath;
        private System.Windows.Forms.ToolStripButton toolStripB_showMetricDist;
        private System.Windows.Forms.ToolStripButton toolStripB_showChoice;
        private System.Windows.Forms.ToolStripButton toolStripButton6;

    }
}


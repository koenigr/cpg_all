﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Diagnostics;
using System.Collections.Generic;
using Tektosyne.Geometry;
using Tektosyne.Graph;
using Tektosyne.Collections;
using GeoObj;
using GeoObj.BasicClasses;
using GeoObj.Helper;
using GeoObj.Graph;

namespace WindowsFormsApplication6
{
    public partial class Form1 : Form
    {
        public static Random Rnd = new Random();
        bool loaded = false;
        ObjectsAllList AllObjectsList = new ObjectsAllList();
        //ObjectsDrawList ObjectsToDraw = new ObjectsDrawList(); // the objects in this list are drawn 
        //ObjectsInteractList ObjectsToInteract = new ObjectsInteractList(); // with the objects in this list an user can interact with
        public List<GLine> Walls = new List<GLine>();
        GridVisibility testVisibility;
        GridDistances testDistances;
        GridShortestPathes testDistMetric;
        GGrid testGrid;
        GPolygon[] ClipperPolys = new GPolygon[2];
        int GridCellSize;

        //============================================================================================================================================================================  
        public Form1()
        {
            InitializeComponent();
        }

        //============================================================================================================================================================================  
        private void glControl1_Load(object sender, EventArgs e)
        {
            loaded = true;
            GL.ClearColor(Color.LightGray); // Yey! .NET Colors can be used directly!
            SetupViewport();
            CreateObjects();
        }

        //============================================================================================================================================================================
        private void SetupViewport()
        {
            int w = glControl1.Width;
            int h = glControl1.Height;
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, w, 0, h, -1, 1); // Bottom-left corner pixel has coordinate (0, 0)
            GL.Viewport(0, 0, w, h); // Use all of the glControl painting area

            //Kantenglättung:
            GL.Enable(EnableCap.LineSmooth);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.Hint(HintTarget.LineSmoothHint, HintMode.DontCare);
        }

        //============================================================================================================================================================================
        private void CreateObjects()
        {
            GridCellSize = 20;
            RegularPolygon StandardPolygon = new RegularPolygon(GridCellSize, 4, PolygonOrientation.OnEdge, true);
            PolygonGrid Grid = new PolygonGrid(StandardPolygon);
            double height = glControl1.Height /2;
            double width = glControl1.Width;
            Grid.Size = new SizeI((int)(width / StandardPolygon.Length), (int)(height / StandardPolygon.Length));
            testGrid = new GGrid(Grid);
            testGrid.SetBorderColor(Color.Black, 1);
            Random rnd = new Random();
            for (int i = 0; i < testGrid.Grid.Size.Width; i++)
            {
                for (int k = 0; k < testGrid.Grid.Size.Height; k++)
                {
                    float testCol = (float)rnd.NextDouble();
                    testGrid.ColorGrid[i, k] = new Vector4(testCol, testCol, testCol, 1);
                }
            }

            AllObjectsList.Add(testGrid, true, false);
            CreateBorder();

            //-----------------------------------------------------------------------------
        
            RegularPolygon testPoly = new RegularPolygon(50, 5, PolygonOrientation.OnEdge);
            GRegularPolygon gTestRegPoly = new GRegularPolygon(testPoly, new PointD(100, height + 100));
            GCircle testNewCircle = new GCircle( new PointD(180, 250), 5, 10);
            testNewCircle.SetFillColor(Color.Green, 1f);
            AllObjectsList.Add(testNewCircle);

            GCircle testNewCircle2 = new GCircle(new PointD(180, 250), 5);
            testNewCircle2.SetFillColor(Color.Red, 1f);
            AllObjectsList.Add(testNewCircle2);

            GCircle testCircle = new GCircle(new PointD(50, height + 150), 20);
            AllObjectsList.Add(testCircle,true,true);
            
            GEllipse testEllipse = new GEllipse(new PointD(50, height + 50), 20, 30);
            AllObjectsList.Add(testEllipse, true, true);

            //-----------------------------------------------------------------------------
            GRectangle testRect;
            testRect = new GRectangle(10, height + 50, 40, 100);
            //PointD test = new PointD(testRect.Center.X, testRect.Center.Y);
            //testRect.Move(test + new PointD(0,50));
            testRect.BorderWidth = 2;
            testRect.SetFillColor(Color.Red, 1f);
            AllObjectsList.Add(testRect, true, true);
             

            //-----------------------------------------------------------------------------

          
           PointD[] polyPts = new PointD[3];
           polyPts[0] = new PointD(150, 350);
           polyPts[1] = new PointD(180, 250);
           polyPts[2] = new PointD(100, 300);
           GPolygon testPolygonA = new GPolygon(polyPts);
           testPolygonA.BorderWidth = 10;
           testPolygonA.FillColor = new Vector4(0.5f, 0.1f, 0.2f, 0.5f);
           AllObjectsList.Add(testPolygonA, true, true);
          
         //  GPolygon tempPolygon = new GPolygon(gTestRegPoly.Path);
           //tempPolygon.FillColor = new Vector4(0.1f, 0.4f, 0.7f, 0.5f);
          // AllObjectsList.Add(tempPolygon, true, true);

   /*
           PointD[] intersectPts = testPolygonA.Intersection(tempPolygon)[0];
           GPolygon intersectPolygon = new GPolygon(intersectPts);
           intersectPolygon.FillColor = new Vector4(0.9f, 0.3f, 0.5f, 0.5f);
           AllObjectsList.Add(intersectPolygon, true, true);
            * */

           ClipperPolys[0] = testPolygonA;
           //ClipperPolys[1] = tempPolygon;  
            

            //-----------------------------------------------------------------------------
            IGraph2D<PointI> curGraph = testGrid.Grid;
            testVisibility = new GridVisibility(curGraph, Walls, Grid.Size);
            testDistances = new GridDistances(curGraph, Walls, Grid.Size);
            testDistMetric = new GridShortestPathes(Grid, Walls, Grid.Size, GridCellSize);

            //-----------------------------------------------------------------------------
            // --- the two walls ---
            GLine testLine;
            testLine = new GLine(244, 10, 254, 100);
            testLine.Width = 5;
            testLine.SetColor(Color.Black, 1f);
            AllObjectsList.Add(testLine, true, true);
            Walls.Add(testLine);

            testLine = new GLine(44, 55, 144, 50);
            testLine.Width = 5;
            testLine.SetColor(Color.Black, 0.8f);
            AllObjectsList.Add(testLine, true, true);
            Walls.Add(testLine);

            Walls.AddRange(testRect.LinesList);


            //-----------------------------------------------------------------------------
            glControl1.Invalidate();
        }

        //============================================================================================================================================================================
        private void toolStripButton_calc_Click(object sender, EventArgs e)
        {
            testVisibility.Evaluate(toolStripProgressBar_analyse);
            Vector4[,] ffColors = ConvertColor.CreateFFColor(testVisibility.GetNormCounterArray());
            testGrid.SetColorGrid(ffColors);
            toolStripStatusLabel_out.Text = "Mean Visibility Area = " + Math.Round(CollectionsExtend.GetMean(testVisibility.Counter), 2).ToString();
            glControl1.Invalidate();
        }

        //============================================================================================================================================================================
        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            testDistances.Evaluate(toolStripProgressBar_analyse);
            Vector4[,] ffColors = ConvertColor.CreateFFColor(testDistances.GetNormDistWallsArray());
            testGrid.SetColorGrid(ffColors);
            toolStripStatusLabel_out.Text = "Mean Distances to Walls = " + Math.Round(CollectionsExtend.GetMean(testDistances.DistWalls), 2).ToString();
            glControl1.Invalidate();
        }

        //============================================================================================================================================================================
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Vector4[,] ffColors = ConvertColor.CreateFFColor(testVisibility.GetNormCounterArray());
            testGrid.SetColorGrid(ffColors);
            glControl1.Invalidate();
            toolStripStatusLabel_out.Text = "Mean Visibility Area = " + Math.Round(CollectionsExtend.GetMean(testVisibility.Counter), 2).ToString();
        }

        //============================================================================================================================================================================
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            Vector4[,] ffColors = ConvertColor.CreateFFColor(testVisibility.GetNormDistancesArray()); 
            testGrid.SetColorGrid(ffColors);
            glControl1.Invalidate();
            toolStripStatusLabel_out.Text = "Mean Distances Area = " + Math.Round(CollectionsExtend.GetMean(testVisibility.Distances), 2).ToString();
        }

        //============================================================================================================================================================================
        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            Vector4[,] ffColors = ConvertColor.CreateFFColor(testDistances.GetNormDistWallsArray());
            testGrid.SetColorGrid(ffColors);
            glControl1.Invalidate();
            toolStripStatusLabel_out.Text = "Mean Distances to Walls = " + Math.Round(CollectionsExtend.GetMean(testDistances.DistWalls), 2).ToString();
        }
        //============================================================================================================================================================================
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            PointD[] intersectPts = ClipperPolys[0].Intersection(ClipperPolys[1])[0];
            if (intersectPts != null)
            {
                GPolygon intersectPolygon = new GPolygon(intersectPts);
                intersectPolygon.FillColor = new Vector4(0.9f, 0.3f, 0.5f, 0.5f);
                AllObjectsList.Add(intersectPolygon, true, true);
            }
        }

        private void toolStripButton5_Click_1(object sender, EventArgs e)
        {
             testGrid.ResetColorGrid(Color.White);
             glControl1.Invalidate();
        }

        //============================================================================================================================================================================
        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            if (!loaded) return;

            AllObjectsList.Draw();

            glControl1.SwapBuffers();
        }

        //============================================================================================================================================================================
        private void glControl1_Resize_1(object sender, EventArgs e)
        {
            SetupViewport();
            glControl1.Invalidate();
        }

        //============================================================================================================================================================================
        private void glControl1_MouseDown(object sender, MouseEventArgs e)
        {
            if (!loaded) return;
            if (newLine)
            {
                startPt = new PointD(e.X, e.Y);
                tmpObj = new GLine(e.X, glControl1.Height - e.Y, e.X, glControl1.Height - e.Y);
                AllObjectsList.Add(tmpObj, true, false);
            }
            else AllObjectsList.ObjectsToInteract.MouseDown(e, ref glControl1);
        }

        //============================================================================================================================================================================
        private void glControl1_MouseMove(object sender, MouseEventArgs e)
        {
            if (!loaded) return;
            Cursor tmpCursor = Cursors.Default;
            AllObjectsList.ObjectsToInteract.MouseMove(e, ref glControl1, ref tmpCursor);
            this.Cursor = tmpCursor;
            if (newLine & tmpObj != null) // --- draws the temporary line while dragging
            {
                tmpObj.SetCoordinates(tmpObj.LineD.Start, new PointD(e.X, e.Y));
                glControl1.Invalidate();
            }

        }

        //============================================================================================================================================================================
        private void glControl1_MouseUp(object sender, MouseEventArgs e)
        {
            if (!loaded) return;

            if (deleteLine) // --- delete the selected element, if there is one (only ObjectsToInteract are considered)
            {
                GeoObj.BasicClasses.GeoObject tempObj = AllObjectsList.ObjectsToInteract.FindObject(e);
                AllObjectsList.Remove(tempObj, true, true);
                
                GLine testLine = (GLine)tempObj;
                if (testLine != null) Walls.Remove(testLine); //if (tempObj.GetType() is GLine)
                deleteLine = false;
            }

            AllObjectsList.ObjectsToInteract.MouseUp(e);// --- put the selected element to the fornt
            if (AllObjectsList.ObjectsToInteract.FoundInteractObj())
            {
                AllObjectsList.ObjectsToDraw.MouseUp(e, ref glControl1);
            }

            int refHeight = glControl1.Height;
            if (newLine) // --- creates a new wall
            {
                GLine newWall;
                newWall = new GLine(startPt.X, refHeight - startPt.Y, e.X, refHeight - e.Y);
                newWall.Width = 5;
                newWall.SetColor(Color.Black, 1f);
                AllObjectsList.Add(newWall, true, true);
                Walls.Add(newWall);
                newLine = false;
                AllObjectsList.Remove(tmpObj, true, false);
                tmpObj = null;
            }


            //toolStripButton4_Click( sender,  e);
            glControl1.Invalidate();
        }

        //============================================================================================================================================================================
        bool newLine = false;
        bool deleteLine = false;
        PointD startPt;
        GLine tmpObj;
        private void toolStripB_drawLine_Click(object sender, EventArgs e)
        {
            newLine = true;
            deleteLine = false;
        }

        //============================================================================================================================================================================
        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
            deleteLine = true;
            newLine = false;
        }

        //============================================================================================================================================================================
        public void CreateBorder()
        {
            GLine newLine;
            newLine = new GLine(testGrid.Grid.DisplayBounds.TopLeft, testGrid.Grid.DisplayBounds.TopRight);
            newLine.Move(new PointD(0, -1));
            newLine.Width = 5;
            newLine.SetColor(Color.Black, 1f);
            AllObjectsList.Add(newLine, true, false);
            Walls.Add(newLine);

            newLine = new GLine(testGrid.Grid.DisplayBounds.TopRight, testGrid.Grid.DisplayBounds.BottomRight);
            newLine.Move(new PointD(1, 0));
            newLine.Width = 5;
            newLine.SetColor(Color.Black, 1f);
            AllObjectsList.Add(newLine, true, false);
            Walls.Add(newLine);

            newLine = new GLine(testGrid.Grid.DisplayBounds.BottomRight, testGrid.Grid.DisplayBounds.BottomLeft );
            newLine.Move(new PointD(0, 1));
            newLine.Width = 5;
            newLine.SetColor(Color.Black, 1f);
            AllObjectsList.Add(newLine, true, false);
            Walls.Add(newLine);

            newLine = new GLine(testGrid.Grid.DisplayBounds.BottomLeft, testGrid.Grid.DisplayBounds.TopLeft);
            newLine.Move(new PointD(-1, 0));
            newLine.Width = 5;
            newLine.SetColor(Color.Black, 1f);
            AllObjectsList.Add(newLine, true, false);
            Walls.Add(newLine);
        }

        //============================================================================================================================================================================
        // COLLISSION / PARTICLE
        Collision Collider;
        public List<GeoObject> Nodes = new List<GeoObject>();

        private void toolStripButton2_Click_1(object sender, EventArgs e)
        {
            if (Collider != null) Collider.Stop();
            GCircle curNode;
            PointD startPt;
            for (int i = 0; i < 10000; i++)
            {
                startPt = new PointD(Rnd.NextDouble() * Width, Rnd.NextDouble() * Height);
                curNode = new GCircle(startPt, 5, 8);
                curNode.Vector = new PointD(Rnd.NextDouble() * 2 - 1, Rnd.NextDouble() * 2 - 1);
                curNode.SetFillColor(Color.Green, 0.3f);
                Nodes.Add(curNode);
                AllObjectsList.Add(curNode, true, false);
            }

            Collider = new Collision(Nodes, glControl1, new RectD(glControl1.Left, glControl1.Top, glControl1.Width, glControl1.Height));
            Collider.Start();
        }

        private void toolStripButton3_Click_1(object sender, EventArgs e)
        {
            Collider.Stop();
            foreach (GeoObject curNode in Nodes)
            {
                AllObjectsList.Remove(curNode);
            }
            Nodes.Clear();
        }

        List<GeoObject> VoroObjects = new List<GeoObject>(); // used to remove old Voronoi elements
        //============================================================================================================================================================================
        private void toolStripButton4_Click_1(object sender, EventArgs e)
        {
            if (VoroObjects.Count > 0)
            {
                foreach (GeoObject curObj in VoroObjects)
                {
                    AllObjectsList.Remove(curObj, true, false);
                }
            }

            double height = glControl1.Height / 2;
            double width = glControl1.Width /2;
            //-----------------------------------------------------------------------------
            // --- the voronoi ---
            RectD VoronoiBounds = new RectD(width, height+2, width -2, height-4);
            GRectangle VoroRect = new GRectangle(VoronoiBounds);
            AllObjectsList.Add(VoroRect, true, false);
            VoroObjects.Add(VoroRect);

            PointD[] testPtArr = new PointD[100];
            // Create the basic points to start with
            for (int i = 0; i < testPtArr.Length; i++)
            {
                testPtArr[i] = new PointD(Rnd.NextDouble() * VoronoiBounds.Width + VoronoiBounds.X,
                    Rnd.NextDouble() * VoronoiBounds.Height + VoronoiBounds.Y);
                GCircle tempC = new GCircle(testPtArr[i], 1, 6);
                AllObjectsList.Add(tempC, true, false);
                VoroObjects.Add(tempC);
            }
            // Calculate the Voronoi diagram
            VoronoiResults testVoro = Voronoi.FindAll(testPtArr, VoronoiBounds);
            VoronoiEdge[] voroEdge = testVoro.VoronoiEdges;
            PointD[] voroVerti = testVoro.VoronoiVertices;

            PointD[][] voroRegions = testVoro.VoronoiRegions;
            LineD[] delaunayEdges = testVoro.DelaunayEdges;
            //Subdivision delaunaySubDiv = testVoro.ToDelaunySubdivision(true);
            Subdivision delaunaySubDiv = Subdivision.FromLines(delaunayEdges);

            //-----------------------------------------------------------------------------
            // Create the drawable Voronoi edges
            for (int i = 0; i < voroEdge.Length; i++)
            {
                int idxV1 = voroEdge[i].Vertex1;
                int idxV2 = voroEdge[i].Vertex2;
                GLine tempLine = new GLine(voroVerti[idxV1], voroVerti[idxV2]);
                tempLine.Width = 2;
                AllObjectsList.Add(tempLine, true, false);
                VoroObjects.Add(tempLine);
            }

            //-----------------------------------------------------------------------------
            // Use the DelaunayEdges as graph for shortest pathes calculations
            IGraph2D<PointD> curGraph = delaunaySubDiv;
            var shortPathes = new Dijkstra<PointD>(curGraph);
            IEnumerable<PointD> alllNodes = delaunaySubDiv.Nodes;
            Dictionary<PointD, int> steps = new Dictionary<PointD, int>();

            foreach(PointD curNode in alllNodes)
            {
                shortPathes.CalculateDistance(curNode);
                steps.Add(curNode, 0);
                steps[curNode] = shortPathes.TotalDepth();
            }

            Dictionary<PointD, Single> normSteps = Tools.GetNormArray(steps);
            Dictionary<PointD, Vector4> normColors = ConvertColor.CreateFFColor(normSteps);

            //-----------------------------------------------------------------------------
            // Create the polygons
            int nrRegions = voroRegions.GetLength(0);
            for (int i = 0; i < nrRegions; i++)
            {
                Vector4 col = ConvertColor.CreateFFColor(Color.ForestGreen);
                GPolygon newPoly = new GPolygon(voroRegions[i]);

                foreach (KeyValuePair<PointD, Vector4> kvp in normColors)
                {
                    PointD curPt = kvp.Key;
                    PolygonLocation testPt = GeoAlgorithms.PointInPolygon(curPt, newPoly.PolyPoints);
                    if (testPt == 0) // the point in polygon test is nessesary to find the correct voronoi polygon! 
                    {
                        col = kvp.Value;
                    }
                }
                //col.W = 0.8f;
                newPoly.FillColor = col;
                AllObjectsList.Add(newPoly, true, true);
                VoroObjects.Add(newPoly);
            }

            // Create the drawable DelaunayEdges
            for (int i = 0; i < delaunayEdges.Length; i++)
            {
                GLine tempLine = new GLine(delaunayEdges[i]);
                tempLine.Color = new Vector4(0.3f, 0.5f, 0.6f, .2f);
                AllObjectsList.Add(tempLine, true, false);
                VoroObjects.Add(tempLine);
            }

            glControl1.Invalidate();
        }

        
        private void calcShortPath_Click(object sender, EventArgs e)
        {
            testDistMetric.Evaluate(toolStripProgressBar_analyse);
            Vector4[,] ffColors = ConvertColor.CreateFFColor(testDistMetric.GetNormDistMetricArray());
            testGrid.SetColorGrid(ffColors);
            toolStripStatusLabel_out.Text = "Mean Metric Distance = " + Math.Round(CollectionsExtend.GetMean(testDistMetric.DistMetric), 2).ToString();
            glControl1.Invalidate();
        }

        private void toolStripB_showMetricDist_Click(object sender, EventArgs e)
        {
            Vector4[,] ffColors = ConvertColor.CreateFFColor(testDistMetric.GetNormDistMetricArray());
            testGrid.SetColorGrid(ffColors);
            toolStripStatusLabel_out.Text = "Mean Metric Distance = " + Math.Round(CollectionsExtend.GetMean(testDistMetric.DistMetric), 2).ToString();
            glControl1.Invalidate();
        }

        private void toolStripB_showAngularDist_Click(object sender, EventArgs e)
        {
            Vector4[,] ffColors = ConvertColor.CreateFFColor(testDistMetric.GetNormChoiceArray());
            testGrid.SetColorGrid(ffColors);
            toolStripStatusLabel_out.Text = "Mean Choice Value = " + Math.Round(CollectionsExtend.GetMean(testDistMetric.Choice), 2).ToString();
            glControl1.Invalidate();
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            Vector4[,] ffColors = ConvertColor.CreateFFColor(testDistMetric.GetNormDistAngleArray());
            testGrid.SetColorGrid(ffColors);
            toolStripStatusLabel_out.Text = "Mean Angle Distance = " + Math.Round(CollectionsExtend.GetMean(testDistMetric.DistAngle), 2).ToString()
                + " THERE IS AN ERROR SOMEWHERE!?!?!";
            glControl1.Invalidate();
        }




    }
}
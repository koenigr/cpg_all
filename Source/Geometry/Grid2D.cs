﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Grid2D.cs 
//  Copyright (C) 2014/10/18  12:58 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.ComponentModel;
using OpenTK;

namespace CPlan.Geometry
{
    [Serializable]
    public class Grid2D
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Fields

        private double[] displayValues;
        protected GridCell[,] gridCells { get; set; }

        protected Vector3d[] colorValues;
        protected Vector2d m_startP, m_endP, m_size;
        protected int m_countX, m_countY;
        protected double m_cellSize;
        protected String m_name;
        protected bool show = true;
        protected float transparency = 1;

        int colorMode = 2; //0 = greyScale, 1 = Farbe
        int interpolation = 0; //0 = linear, 1 = abhängig von der Verteilung der Werte

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties


        public Vector2d StartP { get { return m_startP; } set { m_startP = value;  } }
        public Vector2d EndP { get { return m_endP; } set { m_endP = value; } }
        public GridCell[,] GridCells { get { return gridCells; } }
        public String Name { get { return m_name; } set { m_name = value; } }
        //[CategoryAttribute("Settings"), DescriptionAttribute("X Coordinate of the first corner of the Grid")]
        //public double P1_X { get { return Math.Round(m_startP.X, 2); } set { m_startP.X = value; } }
        //[CategoryAttribute("Settings"), DescriptionAttribute("Y Coordinate of the first corner of the Grid")]
        //public double P1_Y { get { return Math.Round(m_startP.Y, 2); } set { m_startP.Y = value; } }
        //[CategoryAttribute("Settings"), DescriptionAttribute("X Coordinate of the second corner of the Grid")]
        //public double P2_X { get { return Math.Round(m_endP.X, 2); } set { m_endP.X = value; } }
        //[CategoryAttribute("Settings"), DescriptionAttribute("Y Coordinate of the second corner of the Grid")]
        //public double P2_Y { get { return Math.Round(m_endP.Y, 2); } set { m_endP.Y = value; } }
        [Category("Settings"), Description("Size of the Grid Cells")]
        public double CellSize { get { return m_cellSize; } set { m_cellSize = value; } }
        [Category("Visualisation"), Description("Show Grid on Screen")]
        public bool ShowGrid { get { return show; } set { show = value; } }
        [Category("Visualisation"), Description("Set Transparency of the grid (0 = invisble, 1 = opaque)")]
        public float Transparency { get { return transparency; } set { transparency = value; } }

        [Category("Information"), Description("Grid Cells in X Direction")]
        public int CountX { get { return m_countX; } }
        [Category("Information"), Description("Grid Cells in Y Direction")]
        public int CountY { get { return m_countY; } }
        [Category("Information"), Description("Total Number of Grid Cells")]
        public int NumberOfCells { get { return m_countY * m_countX; } }
        [Category("Information"), Description("Size of the Grid")]
        public Vector2d Size { get { return m_size; } }
        [Category("Information"), Description("Number of Active GridCells")]
        public int NumberOfActiveCells { 
            get 
            { 
                int cnt = 0;
                for (int i = 0; i < m_countX; i++) 
                    for (int j = 0; j < m_countY; j++) 
                        if (gridCells[i, j].Active) cnt++; 
                return cnt; 
            } 
        }
        //public bool[] ActiveCells { get { int cnt = 0; for (int i = 0; i < m_countX; i++) for (int j = 0; j < m_countY; j++) if (gridCells[i, j].Active) cnt++; return cnt; } }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        //2D Grid...
        public Grid2D(Vector2d _startP, Vector2d _endP, double _cellSize, String _name)
        {
            m_startP = new Vector2d();
            m_endP = new Vector2d();
            if (m_name != null)
                m_name = _name;
            else
                m_name = "Grid";

            if (_startP.X > _endP.X)
            {
                m_startP.X = _endP.X;
                m_endP.X = _startP.X;
            }
            else
            {
                m_startP.X = _startP.X;
                m_endP.X = _endP.X;
            }

            if (_startP.Y > _endP.Y)
            {
                m_startP.Y = _endP.Y;
                m_endP.Y = _startP.Y;
            }
            else
            {
                m_startP.Y = _startP.Y;
                m_endP.Y = _endP.Y;
            }
            m_cellSize = _cellSize;

            m_size = new Vector2d((m_endP.X - m_startP.X), (m_endP.Y - m_startP.Y));
            m_countX = Convert.ToInt32(Math.Round(m_size.X / m_cellSize, 0));
            m_countY = Convert.ToInt32(Math.Round(m_size.Y / m_cellSize, 0));
            m_size.X = m_cellSize * m_countX;
            m_size.Y = m_cellSize * m_countY;

            gridCells = new GridCell[m_countX, m_countY];
            for (int i = 0; i < m_countX; i++)
                for (int j = 0; j < m_countY; j++)
                    gridCells[i, j] = new GridCell(true);

            colorValues = new Vector3d[m_countX * m_countY];
            show = true;
        }


        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        //============================================================================================================================================================================
        /// <summary>
        /// Get the normalised Distances value of all grid elements.
        /// </summary>
        /// <remarks>
        /// It is useful for the definition of the color values of the regular polygons
        /// of the corresponding <see cref="GGrid"/>.
        /// </remarks>
        /// <returns>
        /// The normalise Distances values as <see cref="float[,]"/> array.
        /// </returns>
        public void GetNormArray()
        {
            Tools.CutMinValue(displayValues, Tools.GetMin(displayValues));
            Tools.Normalize(displayValues);
            Tools.IntervalExtension(displayValues, Tools.GetMax(displayValues));
        }

        /// <summary>
        /// Sets the color mode of the grid.
        /// </summary>
        /// <param m_name="col">(int) color mode. 0 - grey scale, 1 - color.</param>
        public void SetColorMode(int col)
        {
            if (col < 0 || col > 2) colorMode = 2;
            else colorMode = col;
        }

        public void AdjustGrid()
        {
            if (m_startP.X > m_endP.X)
            {
                double tmp = m_startP.X;
                m_startP.X = m_endP.X;
                m_endP.X = tmp;
            }
            if (m_startP.Y > m_endP.Y)
            {
                double tmp = m_startP.Y;
                m_startP.Y = m_endP.Y;
                m_endP.Y = tmp;
            }
            m_size = new Vector2d((m_endP.X - m_startP.X), (m_endP.Y - m_startP.Y));
            m_countX = Convert.ToInt32(Math.Round(m_size.X / m_cellSize, 0));
            m_countY = Convert.ToInt32(Math.Round(m_size.Y / m_cellSize, 0));
            m_size.X = m_cellSize * m_countX;
            m_size.Y = m_cellSize * m_countY;


            gridCells = new GridCell[m_countX, m_countY];
            for (int i = 0; i < m_countX; i++)
                for (int j = 0; j < m_countY; j++)
                    gridCells[i, j] = new GridCell(true);

            colorValues = new Vector3d[m_countX * m_countY];
        }

        # endregion
    }

}
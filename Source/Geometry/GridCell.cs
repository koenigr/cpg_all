﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GridCell.cs 
//  Copyright (C) 2014/10/18  12:58 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;

namespace CPlan.Geometry
{
    [Serializable]
    public class GridCell
    {
        double val;
        bool active = true;
        bool selected = false;

        public bool Active { get { return active; } set { active = value; } }
        public double Value { get { return val; } set { val = value; } }
        public bool Blocked { get; set; }


        public GridCell()
        {
        }

        public GridCell(double _value)
        {
            val = _value;
        }

        public GridCell(double _value, bool _active)
        {
            val = _value;
            active = _active;
        }

        public GridCell(bool _active)
        {
            active = _active;
        }
    }
}

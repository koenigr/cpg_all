﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Line2D.cs 
//  Copyright (C) 2014/10/18  12:58 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using OpenTK;

namespace CPlan.Geometry
{
    [Serializable]
    public class Line2D
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Private Fields

        private Vector2d _point1;
        private Vector2d _point2;
        public static readonly Line2D Empty = new Line2D();

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        //==============================================================================================
        /// <summary>
        /// The start coordinates of the Line2DS.
        /// </summary>
        public Vector2d Start
        {
            get { return _point1; }
            set { _point1 = value; }
        }

        //==============================================================================================
        /// <summary>
        /// The end coordinates of the Line2DS.
        /// </summary>
        public Vector2d End
        {
            get { return _point2; }
            set { _point2 = value; }
        }

        //==============================================================================================
        /// <summary>
        /// The length of the Line2D.
        /// </summary>
        public double Length
        {
            get
            {
                double x = End.X - Start.X;
                double y = End.Y - Start.Y;
                return Math.Sqrt(x * x + y * y);
            }
        }

        //==============================================================================================
        /// <summary>
        /// The center or middle point of the Line2D.
        /// </summary>
        public Vector2d Center
        {
            get { return ((_point1 + _point2) / 2); }
        }

        public double Angle
        {
            get
            {
                double x = End.X - Start.X;
                double y = End.Y - Start.Y;
                return Math.Atan2(y, x);
            }
        }

        public Vector2d Vector
        {
            get { return new Vector2d(End.X - Start.X, End.Y - Start.Y); }
        }

                //==============================================================================================
        /// <summary>
        /// Direction Vector Start End.
        /// </summary>
        public Vector2d DirectionVectorStartEnd
        {            
            get 
            { 
                Vector2d dir = new Vector2d(_point2.X - _point1.X, _point2.Y - _point1.Y);
                //dir.Normalize();
                return dir; 
            }
        }

        //==============================================================================================
        /// <summary>
        /// Direction Vector End Start .
        /// </summary>
        public Vector2d DirectionVectorEndStart
        {
            get
            {
                Vector2d dir = new Vector2d(_point1.X - _point2.X, _point1.Y - _point2.Y);
                dir.Normalize();
                return dir;
            }
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        //==============================================================================================
        /// <summary>
        /// Initializes a new instance of the Line2D class.
        /// </summary>
        /// <param name="point1"> The start point of the Line2D segment. </param>
        /// <param name="point2"> The end point of the Line2D segment. </param>
        public Line2D()
        {
            _point1 = new Vector2d();
            _point2 = new Vector2d();
        }

        //==============================================================================================
        /// <summary>
        /// Initializes a new instance of the Line2D class.
        /// </summary>
        /// <param name="point1"> The start point of the Line2D segment. </param>
        /// <param name="point2"> The end point of the Line2D segment. </param>
        public Line2D(Vector2d point1, Vector2d point2)
		{
            _point1 = point1;
            _point2 = point2;
		}

        //==============================================================================================
        /// <summary>
        /// Initializes a new instance of the Line2D class.
        /// </summary>
        /// <param name="p1X"> X coordinate of the first point. </param>
        /// <param name="p1Y"> Y coordinate of the first point. </param>
        /// <param name="p2X"> X coordinate of the second point. </param>
        /// <param name="p2Y"> Y coordinate of the second point. </param>
        public Line2D(double p1X, double p1Y, double p2X, double p2Y)//, GUI gui)
        {
            _point1 = new Vector2d(p1X, p1Y);
            _point2 = new Vector2d(p2X, p2Y);
        }

        //==============================================================================================
        /// <summary>
        /// Initializes a new instance of the Line2D class.
        /// </summary>
        /// <param name="source">The Line2D to be recreated. </param>
        public Line2D(Line2D source)
        {
            _point1 = source._point1;
            _point2 = source._point2;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        //==============================================================================================
        /// <summary>
        /// Clone the Line2D.
        /// </summary>
        /// <returns>Returns exact clone of the Line2D.</returns>
        public Line2D Clone()
        {
            Line2D clone = new Line2D(this);
            return clone;
        }

        //==============================================================================================
        /// <summary>
        /// Calculate the distance to a point.
        /// </summary>
        /// <param name="q">The point.</param>
        /// <returns>The distance.</returns>
        public double DistanceToPoint(Vector2d q)
        {
            return GeoAlgorithms2D.DistancePointToLine(this, q);
        }


        //==============================================================================================
        /// <summary>
        /// Calculate the distance to a linesegment.
        /// </summary>
        /// <param name="ptherLine">The other Line.</param>
        /// <returns>The distance.</returns>
        public double DistanceToLine(Line2D otherLine)
        {
            Vector2d p1, p2;
            GeoAlgorithms2D.DistanceBetweenSegments(this.Start, this.End, otherLine.Start, otherLine.End, out p1, out p2);
            if (p1 != null && p2 != null)
                return p1.Distance(p2);
            else return -1;
        }

        //==============================================================================================
        /// <summary>
        /// Calculate the distance to a linesegment.
        /// </summary>
        /// <param name="ptherLine">The other Line.</param>
        /// <returns>The distance.</returns>
        public double DistanceToLine(Line2D otherLine, out Line2D shortestDistLine)
        {
            Vector2d p1, p2;
            GeoAlgorithms2D.DistanceBetweenSegments(this.Start, this.End, otherLine.Start, otherLine.End, out p1, out p2);
            if (p1 != null && p2 != null)
            {
                shortestDistLine = new Line2D(p1, p2);
                return p1.Distance(p2);
            }
            else
            {
                shortestDistLine = null;
                return -1;
            }
        }


        //==============================================================================================
        /// <summary>
        /// Calculates a point at .
        /// </summary>
        /// <param name="q">The point.</param>
        /// <returns>The Point.</returns>
        public Vector2d PointAt(double a)
        {
            Vector2d dir = DirectionVectorStartEnd;
            return new Vector2d(_point1.X+a*dir.X, _point1.Y+a*dir.Y);
        }

        //==============================================================================================
        /// <summary>
        /// Calculates points on the line, by deviding it into equal segments.
        /// </summary>
        /// <param name="n">The number of divisions (without start & endpoint).</param>
        /// <returns>The List of points.</returns>
        public List<Vector2d> Divide(int n)
        {
            n++;

            List<Vector2d> points = new List<Vector2d>();

            Vector2d dir = new Vector2d(End.X - Start.X, End.Y - Start.Y);
            dir.X /= n;
            dir.Y /= n;

            for (int i = 1; i < n; i++)
            {
                points.Add(new Vector2d(Start.X + i * dir.X, Start.Y + i * dir.Y));
            }


            return points;
        }

        //==============================================================================================
        /// <summary>
        /// Test if this Line2D intersects with another Line2D.
        /// </summary>
        /// <param name="otherLine2D">The other Line2D.</param>
        /// <returns>True, if there is an intersection, false otherwise.</returns>
        public bool Intersect(Line2D otherLine2D)
        {
            //Vector2d intersect = new Vector2d(double.MaxValue, double.MaxValue);
            //if ((GeoAlgorithms2D.LineIntersection(this.Start, this.End, otherLine.Start, otherLine.End, ref intersect) > 1))
            //    return true;
            //else
            //    return false;
            //return GeoAlgorithms2D.DoLinesIntersect(this, otherLine);
            return GeoAlgorithms2D.IntersectionTest((float)Start.X, (float)Start.Y, (float)End.X, (float)End.Y, (float)otherLine2D.Start.X, (float)otherLine2D.Start.Y, (float)otherLine2D.End.X, (float)otherLine2D.End.Y);

        }

        //==============================================================================================
        /// <summary>
        /// Test if this Line2D intersects with another Line2D.
        /// </summary>
        /// <param name="otherLine2D">The other Line2D.</param>
        /// <returns>The intersection point..</returns>
        public Vector2d IntersectionPoint(Line2D otherLine2D)
        {
            Vector2d intersect = new Vector2d(double.MaxValue, double.MaxValue);
            int testInter = GeoAlgorithms2D.LineIntersection(Start, End, otherLine2D.Start, otherLine2D.End, ref intersect);
            if (testInter >=1)
                return intersect;
            else
                return new Vector2d(double.MaxValue, double.MaxValue);
        }
        public Line2DIntersection getLine2DIntersection(Line2D line2D)
        {
            try
            {
        
                return Line2DIntersection.Find(Start, End, line2D.Start, line2D.End);
            }
            catch (Exception e)
            {
              //  MessageBox.Show(Start.X + " " + Start.Y + " " +End.X + " " + End.Y + " " + line2D.Start.X + " " + line2D.Start.Y + " " + line2D.End.X + " " + line2D.End.Y);
                return new Line2DIntersection(Line2DRelation.Divergent);
            }
        }

        //==============================================================================================
        /// <summary>
        /// Find the perpedicular intersection point
        /// </summary>
        /// <param name="point"> Point</param>
        /// <returns>Intersection point</returns>
        public Vector2d Intersect(Vector2d point)
        {
          
            Vector2d intersect ;
            GeoAlgorithms2D.DistancePointToLine(point, Start, End, out intersect);
            return intersect;
        }

        //==============================================================================================
        /// <summary>
        /// Checks if this Line2D has similar points as another Line2D within a given range (eps).
        /// </summary>
        public bool IsSimilarTo(Line2D otherLine2D, double eps)
        {
            if ( (Start.IsSimilarTo(otherLine2D.Start, eps) && End.IsSimilarTo(otherLine2D.End, eps)) || (Start.IsSimilarTo(otherLine2D.End, eps) && End.IsSimilarTo(otherLine2D.Start, eps)) )
                return true;
            else
                return false;
        }


        //==============================================================================================
        /// <summary>
        /// Intersects with a poly.
        /// </summary>
        /// <param name="otherLine2D">The Polygon.</param>
        /// <returns>A List of intersection points.</returns>
        public List<Vector2d> Intersect(Poly2D poly)
        {
            List<Vector2d> intersectionPoints = new List<Vector2d>();

            for (int i = 0; i<poly.Points.GetLength(0); i++)
                for (int j = 0; j < poly.Points[i].Length; j++)
                {
                    int k = j+1;
                    if (j == poly.Points[i].Length - 1)
                        k = 0;
                    Vector2d intPoint = new Vector2d();
                    GeoAlgorithms2D.LineIntersection(Start, End, poly.Points[i][j], poly.Points[i][k], ref intPoint);
                    if (intPoint != null)
                        intersectionPoints.Add(intPoint);
                }

            if (intersectionPoints.Count > 0)
                return intersectionPoints;
            else
                return null;
        }

        public double FindX(double y)
        {

            double d = End.Y - Start.Y;
            if (d == 0) return Double.MaxValue;

            if (y == Start.Y) return Start.X;
            if (y == End.Y) return End.X;

            return Start.X + (y - Start.Y) * (End.X - Start.X) / d;
        }

        public Line2DLocation Locate(Vector2d q)
        {
            double epsilon = 1e-3;
            double qx0 = q.X - Start.X, qy0 = q.Y - Start.Y;
            if (Math.Abs(qx0) <  epsilon &&Math.Abs(qy0) < epsilon)
                return Line2DLocation.Start;

            double qx1 = q.X - End.X, qy1 = q.Y - End.Y;
            if (Math.Abs(qx1) < epsilon  && Math.Abs(qy1) < epsilon)
                return Line2DLocation.End;

            double ax = End.X - Start.X, ay = End.Y - Start.Y;
            double area = ax * qy0 - qx0 * ay;
            if (area > 0) return Line2DLocation.Left;
            if (area < 0) return Line2DLocation.Right;

            if (qx0 * qx1 <= 0 && qy0 * qy1 <= 0)
                return Line2DLocation.Between;

            if (ax * qx0 < 0 || ay * qy0 < 0)
                return Line2DLocation.Before;
            else
                return Line2DLocation.After;
        }


        #endregion

        public double InverseSlope
        {
            get
            {
                double d = End.Y - Start.Y;
                return (d == 0 ? Double.MaxValue : (End.X - Start.X) / d);
            }
        }
        public Line2DLocation Locate(Vector2d q, double epsilon)
        {

            double qx0 = q.X - Start.X, qy0 = q.Y - Start.Y;
            if (Math.Abs(qx0) <= epsilon && Math.Abs(qy0) <= epsilon)
                return Line2DLocation.Start;

            double qx1 = q.X - End.X, qy1 = q.Y - End.Y;
            if (Math.Abs(qx1) <= epsilon && Math.Abs(qy1) <= epsilon)
                return Line2DLocation.End;

            double ax = End.X - Start.X, ay = End.Y - Start.Y;
            double area = ax * qy0 - qx0 * ay;
            double epsilon2 = epsilon * (Math.Abs(ax) + Math.Abs(ay));
            if (area > epsilon2) return Line2DLocation.Left;
            if (area < -epsilon2) return Line2DLocation.Right;

            if ((qx0 * qx1 <= 0 || Math.Abs(qx0) <= epsilon || Math.Abs(qx1) <= epsilon) &&
                (qy0 * qy1 <= 0 || Math.Abs(qy0) <= epsilon || Math.Abs(qy1) <= epsilon))
                return Line2DLocation.Between;

            if (ax * qx0 < 0 || ay * qy0 < 0)
                return Line2DLocation.Before;
            else
                return Line2DLocation.After;
        }
    }
}

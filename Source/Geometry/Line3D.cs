﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Line3D.cs 
//  Copyright (C) 2014/10/18  12:58 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion
using OpenTK;

namespace CPlan.Geometry
{
    public class Line3D: CGeom3d.Line_3d
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        public Vector3d Point
        {
            get { return p.ToVector3d(); }
            set { p = value.ToGeom3d(); }
        }

        public Vector3d Direction
        {
            get { return v.ToVector3d(); }
            set { v = value.ToGeom3d(); }
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

/*
        public Line3D(Vector3d point, Vector3d direction)
        {
            GeoAlgorithms3D.Line_VP_3d(direction, point, this);
        }
*/
        public Line3D()
        {
        }

        public Line3D(Vector3d point1, Vector3d point2)
        {
            SetTwoPoints(point1, point2);
        }

        public void SetPointAndDirection(Vector3d point, Vector3d direction)
        {
            CGeom3d.Line_VP_3d(direction.ToGeom3d(), point.ToGeom3d(), this);
        }

        public void SetTwoPoints(Vector3d point1, Vector3d point2)
        {
            CGeom3d.Line_PP_3d(point1.ToGeom3d(), point2.ToGeom3d(), this);
        }

        public void IntersectionWithPlane(Plane3D plane, Vector3d cutPoint, ref bool isCutting)
        {
            CGeom3d.Point_LS_3d(this, plane, cutPoint.ToGeom3d());
            isCutting = (CGeom3d.Err_3d == 1);
        }

        public void IntersectionWithLine(Line3D line2, Vector3d cutPoint, ref bool isCutting)
        {
            CGeom3d.Point_LL_3d(this, line2, cutPoint.ToGeom3d());
            isCutting = (CGeom3d.Err_3d == 1);
        }

        public double DistanceToPoint(Vector3d point)
        {
            return CGeom3d.Dist_PL_3d(point.ToGeom3d(), this);
        }

        public double DistanceToLine(Line3D line2)
        {
            return CGeom3d.Dist_LL_3d(this, line2);
        }

        public void RotatePoint(Vector3d point, double sinw, double cosw)
        {
            CGeom3d.Rot_L_3d(point.ToGeom3d(), this, sinw, cosw);
        }

        # endregion
    }
}

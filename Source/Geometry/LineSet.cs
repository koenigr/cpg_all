﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK;

namespace CPlan.Geometry
{
    public class LineSet : ICloneable, IEnumerable<Tuple<Vector3d,Vector3d>>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Private Fields

        protected PointSet _points;
        private List<Tuple<int, int>> _indexes;

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        /// <summary>
        /// Access to points of the set.
        /// If one changes the point that shared among several lines, all lines are affected (the method does not add new points!)
        /// </summary>
        /// <param name="i">index of a point</param>
        /// <returns></returns>
        public virtual Tuple<Vector3d,Vector3d> this[int i]
        {
            get { return new Tuple<Vector3d,Vector3d>(_points[_indexes[i].Item1], _points[_indexes[i].Item2]); }
            set {
                // TODO: should we create new points if there are other lines linking to this point?
                _points[_indexes[i].Item1] = value.Item1;
                _points[_indexes[i].Item2] = value.Item2;
            }
        }

        /// <summary>
        /// Number of line segments
        /// </summary>
        public virtual int Count
        {
            get { return _indexes.Count; }
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        /// <summary>
        /// Empty line set
        /// </summary>
        public LineSet()
        {
            _points = new PointSet();
            _indexes = new List<Tuple<int, int>>();
        }

        public LineSet(IEnumerable<Vector3d> points, IEnumerable<Tuple<int, int>> indexes)
        {
            _points = new PointSet(points);
            _indexes = indexes.ToList();
        }

        public LineSet(PointSet points, IEnumerable<Tuple<int, int>> indexes)
        {
            _points = points;
            _indexes = indexes.ToList();
        }

        public LineSet(IEnumerable<Tuple<Vector3d,Vector3d>> lines)
        {
            _points = new PointSet(lines.SelectMany(l => new List<Vector3d>() { l.Item1, l.Item2 }));
            _indexes = Enumerable.Range(0,lines.Count()/2).Select(i => new Tuple<int,int>(i*2,i*2+1)).ToList();
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Manipulation

        /// <summary>
        /// Adds a line segment to the set
        /// </summary>
        /// <param name="line"></param>
        public virtual void Add(Vector3d startPoint, Vector3d endPoint)
        {
            var ps = _points.AsList;
            ps.Add(startPoint);
            ps.Add(endPoint);
            _indexes.Add(new Tuple<int, int>(ps.Count - 2, ps.Count - 1));
        }

        /// <summary>
        /// Adds a line segment to the set as connection of two points that are already in a line
        /// </summary>
        /// <param name="indexes"></param>
        public virtual void Add(int startIndex, int endIndex)
        {
            _indexes.Add(new Tuple<int,int>(startIndex, endIndex));
        }

        /// <summary>
        /// Adds a line segment to the set, starting from existing point, ending with new point
        /// </summary>
        /// <param name="startIndex">index of the point in the PointSet of this LineSet</param>
        /// <param name="endPoint"></param>
        public virtual void Add(int startIndex, Vector3d endPoint)
        {
            var ps = _points.AsList;
            ps.Add(endPoint);
            _indexes.Add(new Tuple<int, int>(startIndex, ps.Count - 1 ));
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region ICloneable implementation

        object ICloneable.Clone()
        {
            return Clone();
        }

        public virtual LineSet Clone()
        {
            LineSet ls = new LineSet();
            ls._points = _points.Clone();
            ls._indexes = new List<Tuple<int, int>>(this._indexes);
            return ls;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region IEnumerable Implementation

        public IEnumerator<Tuple<Vector3d, Vector3d>> GetEnumerator()
        {
            return new LineSetEnumerator(this);
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _points.GetEnumerator();
        }

        public class LineSetEnumerator : IEnumerator<Tuple<Vector3d, Vector3d>>
        {
            private LineSet _lineSet;
            private int _curIndex;

            public LineSetEnumerator(LineSet ls)
            {
                _lineSet = ls;
                _curIndex = -1;
            }

            public Tuple<Vector3d, Vector3d> Current
            {
                get { return _lineSet[_curIndex]; }
            }

            public void Dispose() { }

            object System.Collections.IEnumerator.Current
            {
                get { return Current; }
            }

            public bool MoveNext()
            {
                return ++_curIndex < _lineSet.Count;
            }

            public void Reset()
            {
                _curIndex = -1;
            }
        }

        # endregion

    }
}

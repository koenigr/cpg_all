﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK;
using MathNet.Numerics.LinearAlgebra.Double;

namespace CPlan.Geometry
{
    [Serializable]
    public class Plane : ICloneable, IApproximatelyComparable<Plane>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Private Fields

        private Vector3d _center, _xDir, _yDir;
        private bool _normalized = false;

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        //==============================================================================================

        /// <summary>
        /// Whether axes are normalized or not.
        /// NB: if false, no checks performed - axes can even be collinear (setup by user)
        /// </summary>
        public bool Normalized
        {
            get { return _normalized; }
            set
            {
                if (value == false)
                    _normalized = false;
                else
                    Normalize();
            }
        }

        /// <summary>
        /// The start coordinates of the Plane.
        /// </summary>
        public Vector3d Center
        {
            get { return _center; }
            set { _center = value; }
        }

        public Vector3d XDirection
        {
            get { return _xDir; }
            set
            {
                _xDir = value;
                if (_normalized)
                {
                    _xDir.Normalize();
                    _yDir = _xDir.OrthogonalComponent(_yDir);
                    _yDir.Normalize();
                }
            }
        }

        public Vector3d YDirection
        {
            get { return _yDir; }
            set
            {
                _yDir = value;
                if (_normalized)
                {
                    
                    _yDir.Normalize();
                    _xDir = _yDir.OrthogonalComponent(_xDir);
                    _xDir.Normalize();
                }
            }
        }

        /// <summary>
        /// The normal of the plane.
        /// </summary>
        public Vector3d Normal
        {
            get
            {
                return _xDir.CrossProduct(_yDir);
            }
            set
            {
                if (_normalized){
                    _yDir = value.CrossProduct(_xDir);
                    _yDir.Normalize();
                    _xDir = _yDir.CrossProduct(value);
                    _xDir.Normalize();
                }
                else
                {
                    // TODO: make it as a rotation!
                    var ly = _yDir.Length;
                    var lx = _xDir.Length;
                    _xDir.Normalize();
                    _yDir.Normalize();
                    _yDir = value.CrossProduct(_xDir);
                    _xDir = _yDir.CrossProduct(value);
                    _yDir.Normalize();
                    _yDir *= ly;
                    _xDir.Normalize();
                    _xDir *= lx;
                }
            }
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        //==============================================================================================
        /// <summary>
        /// Initializes a new instance of the Plane class.
        /// </summary>
        public Plane(bool normalized = true)
        {
            _center = new Vector3d();
            _xDir = new Vector3d(1, 0, 0);
            _yDir = new Vector3d(0, 1, 0);
            _normalized = normalized; // note, we do not need normalization procedure here, because the axes are already normalized
        }

        //==============================================================================================
        /// <summary>
        /// Plane going through the point in the direction.
        /// Y if y is not orthogonal to X, then Y is orthogonalized.
        /// </summary>
        /// <param name="point">Some point on the Plane - zero point of a plane</param>
        /// <param name="xDir">x-Direction of a Plane</param>
        /// <param name="yDir">y-Direction of a Plane</param>
        /// <param name="Normalized">Whether to normalize direction vectors or not (default = true; if false then allows scaling)</param>
        public Plane(Vector3d point, Vector3d xDir, Vector3d yDir, bool normalized = true)
        {
            _center = point;
            _xDir = xDir;
            _yDir = yDir;
            Normalized = normalized; // if true, we normalize axes - do it after the vectors are initialized
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Math

        /// <summary>
        /// returns projection of a point to the Plane.
        /// Start of the coordinates is the point Start.
        /// </summary>
        /// <param name="p">point to project on Plane</param>
        /// <returns></returns>
        public Vector2d Projection(Vector3d p)
        {
            var v = p - this.Center;
            if (_normalized)
                return new Vector2d(
                    _xDir.DotProduct(v),
                    _yDir.DotProduct(v)
                    );
            else
                return new Vector2d(
                    _xDir.DotProduct(v) / _xDir.LengthSquared,
                    _yDir.DotProduct(v) / _yDir.LengthSquared
                    );
        }

        /// <summary>
        /// returns point in 3D for project point on a plane
        /// </summary>
        /// <param name="p">Projected point</param>
        /// <returns>Position of the projected point in 3D space</returns>
        public Vector3d In3D(Vector2d p)
        {
            return _xDir * p.X + _yDir * p.Y + _center;
        }

        /// <summary>
        /// This method makes axes of a plane to be normal and orthogonal, possibly right-ordered
        /// </summary>
        private void Normalize()
        {
            if (!_normalized)
            {
                bool x0 = Approximately.Equal(_xDir.LengthSquared, 0);
                bool y0 = Approximately.Equal(_yDir.LengthSquared, 0);
                double dot = _xDir.DotProduct(_yDir);
                if (x0 && y0)
                {
                    _xDir = new Vector3d(1, 0, 0);
                    _yDir = new Vector3d(0, 1, 0);
                }
                else if (x0)
                {
                    _yDir.Normalize();
                    var n = new Vector3d(0, 0, 1);
                    if (Approximately.Equal(Math.Abs(n.DotProduct(_yDir)), 1))
                        n = new Vector3d(1, 0, 0);
                    _xDir = _yDir.CrossProduct(n);
                    _xDir.Normalize();
                }
                else if (y0 || Approximately.Equal(dot, 1))
                {
                    _xDir.Normalize();
                    var n = new Vector3d(0, 0, 1);
                    if (Approximately.Equal(Math.Abs(n.DotProduct(_xDir)), 1))
                        n = new Vector3d(0, 1, 0);
                    _yDir = n.CrossProduct(_xDir);
                    _yDir.Normalize();
                }
                else if (Approximately.Equal(dot, 0))
                {
                    _xDir.Normalize();
                    _yDir.Normalize();
                }
                else
                {
                    _xDir.Normalize();
                    var n = _xDir.CrossProduct(_yDir);
                    _yDir = n.CrossProduct(_xDir);
                    _yDir.Normalize();
                }
            }
            _normalized = true;
        }


        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region ICloneable implementation

        object ICloneable.Clone()
        {
            return Clone();
        }

        public Plane Clone()
        {
            Plane l = new Plane();
            l._center = _center;
            l._xDir = _xDir;
            l._yDir = _yDir;
            l._normalized = _normalized;
            return l;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region IApproximatelyComparable implementation

        public Plane Round()
        {
            Plane p = new Plane(_normalized);
            p._center = Approximately.Round(_center);
            p._xDir = Approximately.Round(_xDir);
            p._yDir = Approximately.Round(_yDir);
            return p;
        }

        public Plane Round(double precision)
        {
            Plane p = new Plane(_normalized);
            p._center = Approximately.Round(_center, precision);
            p._xDir = Approximately.Round(_xDir, precision);
            p._yDir = Approximately.Round(_yDir, precision);
            return p;
        }

        public bool EqualsApprox(Plane b)
        {
            return Approximately.Equal(_center, b._center) &&
                Approximately.Equal(_xDir, b._xDir) &&
                Approximately.Equal(_yDir, b._yDir);
        }

        public bool EqualsApprox(Plane b, double precision)
        {
            return Approximately.Equal(_center, b._center, precision) &&
                Approximately.Equal(_xDir, b._xDir, precision) &&
                Approximately.Equal(_yDir, b._yDir, precision);
        }

        # endregion

    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Plane3D.cs 
//  Copyright (C) 2014/10/18  12:58 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion
using OpenTK;

namespace CPlan.Geometry
{
    public class Plane3D: CGeom3d.Plane_3d
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        public Vector3d Normal
        {
            get { return new Vector3d(a, b, c); }
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        public Plane3D()
        {
        }

        public Plane3D(Vector3d point, Vector3d normale)
        {
            CGeom3d.Plane_PN_3d(point.ToGeom3d(), normale.ToGeom3d(), this);
        }

        public Plane3D(Vector3d point, Vector3d direction1, Vector3d direction2)
        {
            CGeom3d.Plane_PVV_3d(point.ToGeom3d(), direction1.ToGeom3d(), direction2.ToGeom3d(), this);
        }

        public void IntersectionWithLine(Line3D line, Vector3d cutPoint, ref bool isCutting)
        {
            CGeom3d.Point_LS_3d(line, this, cutPoint.ToGeom3d());
            isCutting = (CGeom3d.Err_3d == 1);
        }

        public void IntersectionWithPlane(Plane3D plane, Line3D cutLine, ref bool isCutting)
        {
            CGeom3d.Line_SS_3d(this, plane, cutLine);
            isCutting = (CGeom3d.Err_3d == 1);
        }

        public double DistanceToPoint(Vector3d point)
        {
            return CGeom3d.Dist_PS_3d(point.ToGeom3d(), this);
        }

        public double DistanceToPlane(Plane3D plane2)
        {
            return CGeom3d.Dist_SS_3d(this, plane2);
        }

        public Vector3d ProjectPointWithNormal(Vector3d point)
        {
            CGeom3d.Vec_3d result = new CGeom3d.Vec_3d(0, 0, 0);
            CGeom3d.Proj_N_3d(point.ToGeom3d(), this, result);
            return result.ToVector3d();
        }

        public Vector3d ProjectPointWithVector(Vector3d point, Vector3d direction)
        {
            CGeom3d.Vec_3d result = new CGeom3d.Vec_3d(0, 0, 0);
            CGeom3d.Proj_V_3d(point.ToGeom3d(), direction.ToGeom3d(), this, result);
            return result.ToVector3d();
        }

        # endregion
    }
}

﻿// Node.cs
//----------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using CPlan.Geometry;
using OpenTK;
using ClipperLib;
using CPlan.Geometry;

namespace CPlan.Geometry.PlotGeneration
{
    public class Node
    {


        #region PROPERTIES

        /// <summary>
        /// Gets an array of building polygons for a Node object. Indizes - building style: 0 - row, 1 - block, 2 - ribbon, 3 - free-standing.
        /// </summary>
        public Polygon[] Building { get; private set; }

        /// <summary>
        /// Gets or sets the left child node of a Node object.
        /// </summary>
        /// <value>(Node) left child.</value>
        public Node LeftChild { get; set; }

        /// <summary>
        /// Gets or sets the right child node of a Node object.
        /// </summary>
        /// <value>(Node) right child.</value>
        public Node RightChild { get; set; }

        /// <summary>
        /// Gets the level of a Node object in the syntax tree.
        /// </summary>
        /// <value>(int) level.</value>
        public int Level { get; private set; }

        /// <summary>
        /// Gets or sets the number of leaves connected to a Node object.
        /// </summary>
        /// <value>(int) number of leaves.</value>
        public int NoOfLeaves { get; set; }

        /// <summary>
        /// Gets or sets the operator, a function or a terminal, of a Node object.
        /// </summary>
        /// <value>(char) operator value.</value>
        public char Operator { get; set; }

        /// <summary>
        /// Gets or sets the parent node of a Node object.
        /// </summary>
        /// <value>(Node) parent.</value>
        public Node Parent { get; set; }

        public Polygon Buildable { get; set; }

        public Polygon Test { get; set; }

        /// <summary>
        /// Gets or sets the region of a Node object.
        /// </summary>
        /// <value>(Polygon) region.</value>
        public Polygon Region { get; set; }


        public double angle { get; set; }

        public Subdivision RootGraph { get; set; }

        /// <summary>
        /// Gets or sets the root polygon of a Node object.
        /// </summary>
        public Polygon RootRegion { get; set; }

        /// <summary>
        /// Gets or sets the root polygon of a Node object.
        /// </summary>
        public Polygon RootBuildable { get; set; }

        /// <summary>
        /// Gets the sub division of a Node object.
        /// </summary>
        /// <value>(Line2D) sub division.</value>
        public Line2D SubDivision { get; set; }

        /// <summary>
        /// Gets the frontage lines of a Node object.
        /// </summary>
        /// <value>(Line2D[]) frontage lines.</value>
        public Line2D[] FrontageLines { get; private set; }

        /// <summary>
        /// Gets the restriciton lines of a Node object.
        /// </summary>
        /// <value>(Line2D[]) restriction lines.</value>
        public Line2D[] RestrictionLines { get; private set; }

        public bool Paired { get; set; }

        public  Node Sibling { get; set; }

        public  int LeafIdx { get; set; }
        public int LongestEdgeOnStreetIdx { get; set; }

        private double min1 = -3.0d/4.0d*Math.PI;
        private double max1 = -1.0d/4.0d*Math.PI;
        private double min2 = 1.0d/4.0d*Math.PI;
        private double max2 = 3.0d/4.0d*Math.PI;

        #endregion


        #region CONSTRUCTORS

        /// <summary>
        /// Initializes a new instance of the Node type using default values.
        /// </summary>
        public Node()
        {
        }

        /// <summary>
        /// Initializes a new instance of the Node type using the specified level of this node in the tree, e.g. root node is level 0.
        /// </summary>
        /// <param name="_level">(int) level.</param>
        public Node(int _level)
        {
            this.Level = _level;
            this.NoOfLeaves = 0;
            this.Building = new Polygon[4];
          
            
        }

        /// <summary>
        /// Initializes a new instance of the Node type using the specified region belonging to this node.
        /// </summary>
        /// <param name="_region"></param>
        public Node(Polygon _region)
        {
            this.Region = _region;
            this.Level = 0;
            this.NoOfLeaves = 0;
            this.Building = new Polygon[4];
      
        }

        #endregion



        #region METHODS

        #region CALCULATION

        /// <summary>
        /// Calculates the split value.
        /// </summary>
        /// <param name="_divFactor">(double) division factor (value between 0 and 1).</param>
        /// <returns>(double) split value.</returns>
        public double CalculateSplitValue(double _divFactor)
        {
            if (this.Operator == 'V')
            {

                return this.Region.Bounds.Left + this.Region.Bounds.Width*_divFactor;
            }
            if (this.Operator == 'H')
            {
                return this.Region.Bounds.Top + this.Region.Bounds.Height*_divFactor;
            }

            return 0;

        }


        public double CalculateSplitValueWithLength(double length)
        {
            if (this.Operator == 'V')
            {

                return this.Region.Bounds.Left + length* this.Region.Bounds.Width/ this.Region.Edges[this.LongestEdgeOnStreetIdx] .Length ;

            }

            if (this.Operator == 'H')
            {
                return this.Region.Bounds.Top + length * this.Region.Bounds.Height/ this.Region.Edges[this.LongestEdgeOnStreetIdx].Length;
            }

            return 0;

        }


        /// <summary>
        /// Determines the buildable region of this Node object.
        /// </summary>
        /// <param name="_buildable">(Polygon) buildable root region.</param>
        private void DetermineBuildable()
        {
            if (this.RootBuildable != this.RootRegion)
            {
                if (this.Region == this.RootRegion)
                {
                    this.Buildable = this.RootBuildable;
                }
                else
                {
                    this.Buildable = Polygon.ClipPolygons(this.Region, this.RootBuildable);
                }
            }
            else this.Buildable = this.Region;
        }

        #region DENSITY

        /// <summary>
        /// Gets the density of the current node in regard to the specified building style.
        /// </summary>
        /// <param name="_style">(int) building style: 0 - row, 1 - block, 2 - ribbon, 3 - free.</param>
        /// <returns>(double) density.</returns>
        public double GetDensity(int _style)
        {
            return this.CalculateDensity(_style);
        }

        /// <summary>
        /// Calculates the density of this Node object in regard to the specified building style.
        /// </summary>
        /// <param name="_style">(int) building style: 0 - row, 1 - block, 2 - ribbon, 3 - free.</param>
        /// <returns>(double) density.</returns>
        private double CalculateDensity(int _style)
        {
            if (this.IsLeaf())
            {
                if (this.Building != null)
                {
                    return this.Building[_style].GetArea()/this.Region.GetArea();
                }
                else return 0.0d;
            }
            else return 0.0d;
        }

        #endregion

        #region BUILDING CALCULATION

        /// <summary>
        /// Calculates all possible building types regarding the specified boundaries and density.
        /// </summary>
        /// <param name="_boundaries">(List Line2D) boundaries.</param>
        /// <param name="_density">(double) density.</param>
        public void CalculateBuildings(List<Line2D> _boundaries, Parameter _param, Polygon _buildable)
        {
            Line2D boundaryLine;

            this.DetermineBuildable();
            this.CalculateBuildingLines(_boundaries, _param);

            if (_param.FrontageLine != 0.0) boundaryLine = this.GetLongestFrontageLine();
            else
            {
                _boundaries.Sort(
                    delegate(Line2D l1, Line2D l2) { return Comparer<double>.Default.Compare(l1.Length, l2.Length); });
                boundaryLine = _boundaries[_boundaries.Count - 1];
            }

            this.Building[0] = this.CalculateRow(boundaryLine, _param);
        
            if (_boundaries.Count == 1)
            {
                this.Building[1] = this.Building[0];
            }
            else
            {

                //todo: problematic part
                //try
                //{
                //    this.Building[1] = (_param.FrontageLine != 0.0)
                //        ? this.CalculateBlock(this.FrontageLines, _param)[0]
                //        : this.CalculateBlock(_boundaries.ToArray(), _param)[0];
                //}
                //catch
                //{

                //}
            }

            this.Building[2] = this.CalculateRibbon(boundaryLine, _param);


            this.Building[3] = this.CalculateFreeStanding(boundaryLine, _param);

           // double rotateAngle = -this.angle;
            //MessageBox.Show(rotateAngle/Math.PI*180 + " ");
            //for (int i = 0; i < this.Building.Length; ++i)
            //{
            //    for (int k = 0; k < this.Building[i].Vertices.Length; ++k)
            //    {
            //        Vector2d coords_buildings = this.Building[i].Vertices[k];
            //        double new_building_x = coords_buildings.X * Math.Cos(rotateAngle) -
            //                                coords_buildings.Y * Math.Sin(rotateAngle);
            //        double new_building_y = coords_buildings.X * Math.Sin(rotateAngle) +
            //                                coords_buildings.Y * Math.Cos(rotateAngle);
            //        this.Building[i].Vertices[k] = new Vector2d(new_building_x, new_building_y);





                    
                 
            //    }




               
              

            //}

            //for (int i = 0; i < this.Building[0].Vertices.Length; ++i)
            //{
            //    test.sw.Write(this.Building[0].Vertices[i].X + " " + this.Building[0].Vertices[i].Y+ " ");
            //}

            //test.sw.WriteLine();

        }

        /// <summary>
        /// Calculates a free standing building using the specified density.
        /// </summary>
        /// <param name="_density">(double) density.</param>
        /// <returns>(Polygon) free standing building.</returns>
        public Polygon CalculateFreeStanding(Line2D _boundary, Parameter _param)
        {
            double absDensity = Math.Sqrt(_param.Density)*
                                (this.Region.GetArea()/(this.Region.Bounds.Width*this.Region.Bounds.Height));
            double densArea = this.Region.GetArea()*_param.Density;

            double width = this.Region.Bounds.Width*absDensity;
            double height = this.Region.Bounds.Height*absDensity;


            if ((_boundary.Angle > min1 && _boundary.Angle < max1) || (_boundary.Angle > min2 && _boundary.Angle < max2))
            {
                if (width < _param.MinBuildingDepth) width = _param.MinBuildingDepth;
                else if (width > _param.MaxBuildingDepth) width = _param.MaxBuildingDepth;
                //height = densArea / width;
            }
            else
            {
                if (height < _param.MinBuildingDepth) height = _param.MinBuildingDepth;
                else if (height > _param.MaxBuildingDepth) height = _param.MaxBuildingDepth;
                //width = densArea / height;
            }

            Vector2d center = Polygon.GetCentroidAsVector2d(this.Region.Vertices);

            Vector2d[] polygon = new Vector2d[4];

            polygon[0] = new Vector2d(center.X - width/2, center.Y - height/2);
            polygon[1] = new Vector2d(center.X + width/2, center.Y - height/2);
            polygon[2] = new Vector2d(center.X + width/2, center.Y + height/2);
            polygon[3] = new Vector2d(center.X - width/2, center.Y + height/2);

            Polygon poly = new Polygon(polygon);

            if (_param.FrontageLine != 0.0d)
            {
                poly.MoveTo(_boundary);
            }

            return poly;
        }

        /// <summary>
        /// Calculates the absolute density of this Node's region.
        /// </summary>
        /// <param name="_param">(Parameter) layout parameters.</param>
        /// <returns>(double) absolute density.</returns>
        private double AbsoluteDensity(Parameter _param)
        {
            return _param.Density*(this.Region.GetArea()/(this.Region.Bounds.Width*this.Region.Bounds.Height));
        }

        /// <summary>
        /// Calculates the building depth for a specified offset line taking the region's boundaries into account and using the given layout parameters and dividing it with the specified factor.
        /// </summary>
        /// <param name="_offsetLine">(Line2D) line element chosen for offset.</param>
        /// <param name="_param">(Parameter) layout parameters.</param>
        /// <param name="_factor">(int) division factor for the number of boundaries (for blocks, else 1).</param>
        /// <returns>(double) depth.</returns>
        private double BuildingDepth(Line2D _offsetLine, Parameter _param, int _factor)
        {
            double offset;
            double absDensity;
            absDensity = this.AbsoluteDensity(_param);

            if ((_offsetLine.Angle > min1 && _offsetLine.Angle < max1) ||
                (_offsetLine.Angle > min2 && _offsetLine.Angle < max2))
            {
                offset = this.Region.Bounds.Width*absDensity/_factor;
                if (offset < _param.MinBuildingDepth) offset = _param.MinBuildingDepth;
                else if (offset > _param.MaxBuildingDepth) offset = _param.MaxBuildingDepth;
            }
            else
            {
                offset = this.Region.Bounds.Height*absDensity/_factor;
                
                if (offset < _param.MinBuildingDepth) offset = _param.MinBuildingDepth;
                else if (offset > _param.MaxBuildingDepth) offset = _param.MaxBuildingDepth;
            }

            return offset;
        }

        /// <summary>
        /// Calculates a building block using the specified boundaries and density.
        /// </summary>
        /// <param name="_boundaries">(List Line2D) boundaries.</param>
        /// <param name="_density">(double) density.</param>
        /// <returns>(Polygon[]) building block.</returns>
        public Polygon[] CalculateBlock(Line2D[] _boundaries, Parameter _param)
        {
            double[] offset = new double[_boundaries.Length];
            Line2D[] newLine = new Line2D[_boundaries.Length];
            Line2D tmpLine;
            Vector2d[][] vertices = new Vector2d[_boundaries.Length][];
            Vector2d[][] polygon;
            Polygon[] polys;
            int count;

            Polygon region = (_param.FrontageLine != 0.0d) ? this.Buildable : this.Region;

            if (this.Region == RootRegion) count = this.Region.Edges.Length;
            else if (_boundaries.Length >= 4) count = 3;
            else count = _boundaries.Length;

            for (int i = 0; i < _boundaries.Length; i++)
            {
                offset[i] = this.BuildingDepth(_boundaries[i], _param, count);

                if (this.Region != this.RootRegion) offset[i] = -offset[i];
            }

            List<List<IntPoint>> clip = new List<List<IntPoint>>();
            List<List<IntPoint>> solution = new List<List<IntPoint>>();


            if (this.Region == this.RootRegion)
            {
                // return new Polygon[] {region};
                double tmparea = 0;
                for (int i = 0; i < offset.Length; i++)
                {
                    tmparea += offset[i];
                }

                // double offS = (_param.FrontageLine != 0.0d) ? tmparea/offset.Length : tmparea/offset.Length;
                double offS = (_param.FrontageLine != 0.0d) ? tmparea/offset.Length : 0;
                Polygon pTemp = Polygon.OffsetPolygons(region, offS);
                if (pTemp == null) return new Polygon[] {region};
                // generate polygon with interior rings
                Vector2d[] pts = new Vector2d[pTemp.Vertices.Length + region.Vertices.Length];

                for (int i = 0; i < region.Vertices.Length; i++)
                {
                    pts[i] = region.Vertices[i];

                }
                // pts[region.Vertices.Length] = region.Vertices[0];
                pts[region.Vertices.Length + 1] = pTemp.Vertices[0];
                //System.Windows.Forms.MessageBox.Show("interior");
                //pTemp vertices in reversed order
                for (int i = 0; i < pTemp.Vertices.Length; i++)
                {
                    pts[region.Vertices.Length + 2 + i] = pTemp.Vertices[pTemp.Vertices.Length - 1 - i];
                    //  System.Windows.Forms.MessageBox.Show(pts[region.Vertices.Length+2+i].X + " " + pts[region.Vertices.Length+2+i].Y);
                }

                //System.Windows.Forms.MessageBox.Show("root");
                polys = new Polygon[] {new Polygon(pts)};
                return polys;


            }
            //if (this.Region == this.RootRegion)
            //{
            //    // return new Polygon[] {region};
            //    double tmparea = 0;
            //    for (int i = 0; i < offset.Length; i++)
            //    {
            //        tmparea += offset[i];
            //    }

            //    // double offS = (_param.FrontageLine != 0.0d) ? tmparea/offset.Length : tmparea/offset.Length;
            //    double offS = (_param.FrontageLine != 0.0d) ? tmparea / offset.Length : 0;
            //    Polygon pTemp = Polygon.OffsetPolygons(region, offS);
            //    if (pTemp == null) return new Polygon[] { region };
            //    // generate polygon with interior rings
            //    Vector2d[] pts = new Vector2d[pTemp.Vertices.Length + region.Vertices.Length + 2];

            //    for (int i = 0; i < region.Vertices.Length; i++)
            //    {
            //        pts[i] = region.Vertices[i];
            //        //  System.Windows.Forms.MessageBox.Show(pts[i].X + " " + pts[i].Y );
            //    }
            //    pts[region.Vertices.Length] = region.Vertices[0];
            //    pts[region.Vertices.Length + 1] = pTemp.Vertices[0];
            //    //System.Windows.Forms.MessageBox.Show("interior");
            //    //pTemp vertices in reversed order
            //    for (int i = 0; i < pTemp.Vertices.Length; i++)
            //    {
            //        pts[region.Vertices.Length + 2 + i] = pTemp.Vertices[pTemp.Vertices.Length - 1 - i];
            //        //  System.Windows.Forms.MessageBox.Show(pts[region.Vertices.Length+2+i].X + " " + pts[region.Vertices.Length+2+i].Y);
            //    }

            //    //System.Windows.Forms.MessageBox.Show("root");
            //    polys = new Polygon[] { new Polygon(pts) };
            //    return polys;


            //}



            for (int i = 0; i < _boundaries.Length; i++)
            {
                newLine[i] = this.ParallelOffsetLine2D(_boundaries[i], offset[i]);

                tmpLine = region.ParallelIntersection(newLine[i], _boundaries[i], out vertices[i]);
                if (tmpLine == null)
                {
                    newLine[i] = this.ParallelOffsetLine2D(_boundaries[i], -offset[i]);
                    tmpLine = region.ParallelIntersection(newLine[i], _boundaries[i], out vertices[i]);
                }
                newLine[i] = tmpLine;
            }

            for (int i = 0; i < vertices.GetLength(0); i++)
            {
                List<IntPoint> tmp = new List<IntPoint>();
                for (int j = 0; j < vertices[i].Length; j++)
                {
                    tmp.Add(new IntPoint((long) vertices[i][j].X, (long) vertices[i][j].Y));
                }
                clip.Add(tmp);
            }

            Clipper c = new Clipper();

            c.AddPaths(clip, PolyType.ptSubject, true);

            c.Execute(ClipType.ctXor, solution, PolyFillType.pftNonZero, PolyFillType.pftNonZero);

            polygon = new Vector2d[solution.Count][];
            polys = new Polygon[solution.Count];

            for (int i = 0; i < solution.Count; i++)
            {
                polygon[i] = new Vector2d[solution[i].Count];
                for (int j = 0; j < solution[i].Count; j++)
                {
                    polygon[i][j] = new Vector2d((double) solution[i][j].X, (double) solution[i][j].Y);
                }
                polys[i] = new Polygon(polygon[i]);
            }
            if (polys.Length == 2)
            {
                Vector2d[] tmpArr = new Vector2d[polys[0].Vertices.Length + polys[1].Vertices.Length];
                for (int j = 0; j < polys[0].Vertices.Length; j++)
                {
                    tmpArr[j] = polys[0].Vertices[j];
                }

                for (int j = 0; j < polys[1].Vertices.Length; j++)
                {
                    tmpArr[polys[0].Vertices.Length + 1 + j] = polys[1].Vertices[j];
                }


                return new Polygon[] {new Polygon(tmpArr)};

            }
            //if (polys.Length == 2)
            //{
            //    Vector2d[] tmpArr = new Vector2d[polys[0].Vertices.Length + polys[1].Vertices.Length + 2];
            //    for (int j = 0; j < polys[0].Vertices.Length; j++)
            //    {
            //        tmpArr[j] = polys[0].Vertices[j];
            //    }
            //    //end node for the outer polygon
            //    tmpArr[polys[0].Vertices.Length] = polys[0].Vertices[0];

            //    for (int j = 0; j < polys[1].Vertices.Length; j++)
            //    {
            //        tmpArr[polys[0].Vertices.Length + 1 + j] = polys[1].Vertices[j];
            //    }

            //    tmpArr[polys[0].Vertices.Length + polys[1].Vertices.Length + 1] = polys[1].Vertices[0];
            //    return new Polygon[] { new Polygon(tmpArr) };

            //}
            return polys;



        }

        public double getBuildingOffset(Line2D _offsetLine,Parameter _param)
        {
            double offset;
            
            offset = this.BuildingDepth(_offsetLine, _param, 1);

            if (this.Region != this.RootRegion) offset = -offset;
            return offset;
        }
        /// <summary>
        /// Calculates the row building using a given line to offset and a specified density.
        /// </summary>
        /// <param name="_offsetLine">(Line2D) offset line.</param>
        /// <param name="_density">(double) density.</param>
        /// <returns>(Polygon) row building.</returns>
        public Polygon CalculateRow(Line2D _offsetLine, Parameter _param)
        {
          
            Line2D newLine;
            Vector2d[] vertices;
            //double additional = (_param.FrontageLine != 0.0) ? _param.FrontageLine : 0.0d;

            double offset = getBuildingOffset(_offsetLine, _param);


            newLine = this.ParallelOffsetLine2D(_offsetLine, offset);




            if (this.Buildable == null || this.Buildable.Edges.Length == 0)
            {
                newLine = this.Region.ParallelIntersection(newLine, _offsetLine, out vertices);
            }
            else
            {
                newLine = this.Buildable.ParallelIntersection(newLine, _offsetLine, out vertices);
            }


            Polygon newPoly = new Polygon(vertices);

         



            return newPoly;
        }

        /// <summary>
        /// Calculates a ribbon building using a given line to offset and a specified density.
        /// </summary>
        /// <param name="_offsetLine">(Line2D) offset line.</param>
        /// <param name="_density">(double) density to achieve.</param>
        /// <returns>(Polygon) ribbon building.</returns>
        public Polygon CalculateRibbon(Line2D _offsetLine, Parameter _param)
        {
       
            Line2D newLine;
            Vector2d[] vertices;
            Polygon region = (_param.FrontageLine != 0.0d) ? this.Buildable : this.Region;
            // Global_Vars.sw.WriteLine(_offsetLine.Start.X + " " + _offsetLine.Start.Y + " " + _offsetLine.End.X + " " + _offsetLine.End.Y);
            Line2D perpendicular = this.CalculatePerpendicular(_offsetLine);
            //Global_Vars.sw.WriteLine(perpendicular.Start.X + " " + perpendicular.Start.Y + " " + perpendicular.End.X + " " + perpendicular.End.Y);

            double offset = getBuildingOffset(perpendicular, _param);

            newLine = this.ParallelOffsetLine2D(perpendicular, offset);


            region.PerpendicularIntersection(perpendicular, newLine, _offsetLine, out vertices);

           
            return new Polygon(vertices);
        }

        /// <summary>
        /// Calculates a perpendicular line to the specified line element.
        /// </summary>
        /// <param name="_line">(Line2D) line.</param>
        /// <returns>(Line2D) perpendicular line.</returns>
        private Line2D CalculatePerpendicular(Line2D _line)
        {
            Vector2d dir = _line.Vector;
            //   Global_Vars.sw.WriteLine(dir.X + " " + dir.Y); 
            Vector2d normal = new Vector2d(-dir.Y, dir.X);

            normal = Vector2d.Normalize(normal);

            Vector2d midpt = new Vector2d((_line.Start.X + _line.End.X)/2, (_line.Start.Y + _line.End.Y)/2);
            Vector2d endpt = new Vector2d(midpt.X + 100*normal.X, midpt.Y + 100*normal.Y);

            Line2D perpendicular = new Line2D(midpt, endpt);

            //double angle1 = _line.Angle;
            //double angle2 = perpendicular.Angle;

            return perpendicular;
        }

        /// <summary>
        /// Calculates a parallel offset to a given line element using the specified offset value.
        /// </summary>
        /// <param name="_line">(Line2D) line.</param>
        /// <param name="_offset">(double) offset.</param>
        /// <returns>(Line2D) parallel line.</returns>
        public Line2D ParallelOffsetLine2D(Line2D _line, double _offset)
        {
            double x1, x2, y1, y2;

            x1 = _line.Start.X;
            x2 = _line.End.X;
            y1 = _line.Start.Y;
            y2 = _line.End.Y; // The original line

            double L = Math.Sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2));

            double x1p, x2p, y1p, y2p, ym, xm;

            // This is the second line
            x1p = x1 + _offset*(y2 - y1)/L;
            x2p = x2 + _offset*(y2 - y1)/L;
            y1p = y1 + _offset*(x1 - x2)/L;
            y2p = y2 + _offset*(x1 - x2)/L;

            Line2D parallel = new Line2D(x1p, y1p, x2p, y2p);
            if ((y2p - y1p) == 0.0d)
            {
                ym = y1p;
                xm = x1p + ((x2p - x1p)/2);
            }
            else
            {
                ym = y1p + ((y2p - y1p)/2);
                xm = parallel.FindX(ym);

            }
            Vector2d pt = new Vector2d(xm, ym);
            List<Vector2d> region_verts = new List<Vector2d>(this.Region.Vertices);
            //if (GeoAlgorithms2D.PointInPolygon(pt, region_verts) < 0)
            //{
            //    parallel = ParallelOffsetLine2D(_line, -_offset);

            //}
            //    System.Windows.Forms.MessageBox.Show(parallel.Start.X + " " + parallel.Start.Y + " " + parallel.End.X + " "+ parallel.End.Y);
            return parallel;
        }

        #endregion

        #region BUILDING LINES

        /// <summary>
        /// Calculates the building frontage and restriction lines using the specified boundaries and parameter.
        /// </summary>
        /// <param name="_boundaries">(List Line2D) boundaries.</param>
        /// <param name="_param">(Parameter) layout parameters.</param>
        private void CalculateBuildingLines(List<Line2D> _boundaries, Parameter _param)
        {
            Polygon pTemp;
            if (_boundaries == null || _boundaries.Count == 0) return;

            if (_param.FrontageLine != 0.0)
            {
                if (this.Region == this.RootRegion)
                {
                    this.FrontageLines = this.Buildable.Edges;
                }
                else
                {
                    this.FrontageLines = CalculateFrontageLines();
                }
            }

            if (_param.BuildingRestrictionLine != 0.0)
            {

                double max = (this.Region.Bounds.Width < this.Region.Bounds.Height)
                    ? this.Region.Bounds.Width
                    : this.Region.Bounds.Height;

                if (this.Region == this.RootRegion)
                {
                    pTemp = Polygon.OffsetPolygons(this.Region, _param.BuildingRestrictionLine);

                    if (pTemp == null) this.RestrictionLines = new Line2D[] {Line2D.Empty};
                    else
                    {
                        this.RestrictionLines = pTemp.Edges;
                    }
                }
                else
                {
                    this.RestrictionLines = CalculateBOffsetLines(_boundaries, _param.BuildingRestrictionLine);
                }

            }
        }


        /// <summary>
        /// Gets the longest frontage line.
        /// </summary>
        /// <returns>(Line2D) longest frontage line.</returns>
        private Line2D GetLongestFrontageLine()
        {
            Line2D longest;
            if (this.FrontageLines == null || this.FrontageLines.Length == 0) return Line2D.Empty;
            else
            {
                longest = this.FrontageLines[0];
                foreach (Line2D line in this.FrontageLines)
                {
                    if (line.Length > longest.Length) longest = line;
                }
                return longest;
            }
        }

        /// <summary>
        /// Calculates the frontage lines of a Node object.
        /// </summary>
        /// <returns>(Line2D[]) frontage line array.</returns>
        private Line2D[] CalculateFrontageLines()
        {
            List<Line2D> lines = new List<Line2D>();
            double dist;
            Vector2d closest;

            if (this.Buildable == null || this.RootBuildable == null) return new Line2D[] {Line2D.Empty};

            for (int i = 0; i < this.Buildable.Edges.Length; i++)
            {
                for (int j = 0; j < this.RootBuildable.Edges.Length; j++)
                {
                    double y = this.Buildable.Edges[i].Start.Y +
                               (this.Buildable.Edges[i].End.Y - this.Buildable.Edges[i].Start.Y)/2;
                    double x = this.Buildable.Edges[i].Start.X +
                               (this.Buildable.Edges[i].End.X - this.Buildable.Edges[i].Start.X)/2;

                    dist = FindDistanceToSegment(new Vector2d(x, y), this.RootBuildable.Edges[j].Start,
                        this.RootBuildable.Edges[j].End, out closest);
                    if (Math.Abs(dist) <= 1.0d)
                    {
                        lines.Add(this.Buildable.Edges[i]);
                        break;
                    }
                }
            }
            if (lines.Count == 0) lines.Add(this.Buildable.Edges[0]);
            return lines.ToArray();
        }

        // Calculate the distance between
        // point pt and the segment p1 --> p2.
        private double FindDistanceToSegment(Vector2d pt, Vector2d p1, Vector2d p2, out Vector2d closest)
        {
            double dx = p2.X - p1.X;
            double dy = p2.Y - p1.Y;
            if ((dx == 0) && (dy == 0))
            {
                // It's a point not a line segment.
                closest = p1;
                dx = pt.X - p1.X;
                dy = pt.Y - p1.Y;
                return Math.Sqrt(dx*dx + dy*dy);
            }

            // Calculate the t that minimizes the distance.
            double t = ((pt.X - p1.X)*dx + (pt.Y - p1.Y)*dy)/(dx*dx + dy*dy);

            // See if this represents one of the segment's
            // end points or a point in the middle.
            if (t < 0)
            {
                closest = new Vector2d(p1.X, p1.Y);
                dx = pt.X - p1.X;
                dy = pt.Y - p1.Y;
            }
            else if (t > 1)
            {
                closest = new Vector2d(p2.X, p2.Y);
                dx = pt.X - p2.X;
                dy = pt.Y - p2.Y;
            }
            else
            {
                closest = new Vector2d(p1.X + t*dx, p1.Y + t*dy);
                dx = pt.X - closest.X;
                dy = pt.Y - closest.Y;
            }

            return Math.Sqrt(dx*dx + dy*dy);
        }

        /// <summary>
        /// Calculates the building offsetLines for the given boundaries using the specified offset parameter.
        /// </summary>
        /// <param name="_boundaries">(List Line2D) boundaries.</param>
        /// <param name="_offset">(double) offset parameter.</param>
        /// <returns>(Line2D[]) building offset lines.</returns>
        private Line2D[] CalculateBOffsetLines(List<Line2D> _boundaries, double _offset)
        {
            Line2D[] newLines = new Line2D[_boundaries.Count];
            Line2DIntersection intersect;

            Line2DLocation loc;
            Vector2d shared, connection;

            for (int i = 0; i <= _boundaries.Count; i++)
            {
                int prev = (i > 0) ? i - 1 : _boundaries.Count - 1;

                if (i < _boundaries.Count)
                {
                    newLines[i] = this.ParallelOffsetLine2D(_boundaries[i], -_offset);
                    if (_boundaries.Count == 1) return newLines;
                }

                if (i > 0)
                {
                    if (i == _boundaries.Count) i = 0;
                    if (_boundaries[i].Start == _boundaries[prev].Start || _boundaries[i].Start == _boundaries[prev].End ||
                        _boundaries[i].End == _boundaries[prev].Start || _boundaries[i].End == _boundaries[prev].End)
                    {
                        if (_boundaries[i].Start == _boundaries[prev].Start ||
                            _boundaries[i].Start == _boundaries[prev].End)
                            connection = _boundaries[i].Start;
                        else
                            connection = _boundaries[i].End;

                        intersect = newLines[i].getLine2DIntersection(newLines[prev]);

                        loc = intersect.First;
                        // Console.WriteLine(loc);
                        // if(loc == Line2DLocation.Between)
                        if (loc == Line2DLocation.Between || loc == Line2DLocation.Start || loc == Line2DLocation.End)
                        {
                            shared = (Vector2d) intersect.Shared;

                            if (connection == _boundaries[i].Start)
                                newLines[i].Start = shared;
                            else
                                newLines[i].End = shared;

                            if (connection == _boundaries[prev].Start)
                                newLines[prev].Start = shared;
                            else
                                newLines[prev].End = shared;
                        }
                    }
                    if (i == 0) i = _boundaries.Count;
                }
            }
            return newLines;
        }


        #endregion

        #endregion

        /// <summary>
        /// Adjusts the level of a Node as well as all its children.
        /// </summary>
        /// <param name="_newLevel">(int) new level.</param>
        private void AdjustLevel(int _newLevel)
        {
            this.Level = _newLevel;
            if (this.LeftChild != null && this.RightChild != null)
            {
                this.LeftChild.AdjustLevel(_newLevel + 1);
                this.RightChild.AdjustLevel(_newLevel + 1);
            }
        }

        /// <summary>
        /// Finds all leaves of a branch recursively and stores them in the specified list.
        /// </summary>
        /// <param name="_tmpList">(List Node) list of leaves.</param>
        /// <param name="_curNode">(Node) the current node.</param>
        private void FindAllLeavesOfBranch(List<Node> _tmpList, Node _curNode)
        {
            if (_curNode.LeftChild == null && _curNode.RightChild == null)
            {
                _tmpList.Add(_curNode);
            }
            else
            {
                this.FindAllLeavesOfBranch(_tmpList, _curNode.LeftChild);
                this.FindAllLeavesOfBranch(_tmpList, _curNode.RightChild);
            }
        }

        /// <summary>
        /// Returns true when a Node is a leaf and false if it is not.
        /// </summary>
        /// <returns>(bool) is leaf.</returns>
        public bool IsLeaf()
        {
            if (this.Operator == 'o')
            {
                return true;
            }
            else return false;
        }

        public void rotateBack(double rotationEpsilon)
        {
            if (Math.Abs(this.angle) > rotationEpsilon)
            {
                double rotateAngle = -this.angle;
                for (int j = 0; j < this.Region.Vertices.Length; ++j)
                {
                    Vector2d coords = this.Region.Vertices[j];
                    double new_x = coords.X*Math.Cos(rotateAngle) - coords.Y*Math.Sin(rotateAngle);
                    double new_y = coords.X*Math.Sin(rotateAngle) + coords.Y*Math.Cos(rotateAngle);
                    this.Region.Vertices[j] = new Vector2d(new_x, new_y);


                }
                this.Region.Edges = GeoAlgorithms2D.ConnectPoints(true, this.Region.Vertices);
                this.Region.Bounds = GeoAlgorithms2D.BoundingBoxAdjusted(Region.Vertices);
                this.angle = 0;
            }

        }

        #endregion

        
    }
}
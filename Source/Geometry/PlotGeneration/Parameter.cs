﻿using System;
using System.Collections.Generic;

namespace CPlan.Geometry.PlotGeneration
{
    public class Parameter
    {
        #region PROPERTIES

        #region Subdivision Parameters

        /// <summary>
        /// Gets or sets the max distance for connection.
        /// </summary>
        /// <value>(double) connection distance.</value>
        public double ConnectionDistance
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the permitted maximum area size for a building lot.
        /// </summary>
        /// <value>(double) maximal area.</value>
        public double MaxAreaValue
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets the street offset.
        /// </summary>
        /// <value>(double) street offset.</value>
        public double StreetOffset
        {
            get;
            set;
        }

        #endregion

        #region Building Lot Parameters

        /// <summary>
        /// Gets or sets the building restriction line offset for a building lot.
        /// </summary>
        /// <value>(double) building restriction line offset value.</value>
        public double BuildingRestrictionLine
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the density of the regions.
        /// </summary>
        /// <value>(double) density.</value>
        public double Density
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the frontage line offset for a building lot.
        /// </summary>
        /// <value>(double) frontage line offset value.</value>
        public double FrontageLine
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the minimum building depth.
        /// </summary>
        /// <value>(double) minimum depth of the buildings.</value>
        public double MinBuildingDepth
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the maximum building depth.
        /// </summary>
        /// <value>(double) maximum depth of the buildings.</value>
        public double MaxBuildingDepth
        {
            get;
            set;
        }


        public double buildingWidth
        {
            get; set;
        }

        public double buildingLength { get; set; }

        public List<Line2D> specifiedBorder { get; set; }

        public double scalingFactor { get; set; }
        #endregion

        #endregion


        #region CONSTRUCTOR

        /// <summary>
        /// Initializes a new new instance of the Parameter type using default values.
        /// </summary>
        public Parameter()
        {
            this.ConnectionDistance = 5.0d;
            this.MaxAreaValue = 1000.0d;
            this.StreetOffset = 3.0d;
            this.BuildingRestrictionLine = 0.0d;
            this.Density = 0.3d;
            this.FrontageLine = 0.0d;
            this.MinBuildingDepth = 10.0d;
            this.MaxBuildingDepth = 15.0d;
           // this.buildingHeight = 0.0d;

            this.buildingWidth = 20.0d;
            this.buildingLength = 30.0d;
            this.specifiedBorder = new List<Line2D>();
        }

        /// <summary>
        /// Initializes a new new instance of the Parameter type using the specified connection distance, max. area, street offset, front and building restriction line, density, minimum and maximum building depth values.
        /// </summary>
        /// <param name="_conDist">(double) minimum connection distance between vertices.</param>
        /// <param name="_maxArea">(double) maximum area value for building lot.</param>
        /// <param name="_offset">(double) street offset.</param>
        /// <param name="_frontLine">(double) front line, offset distance from street.</param>
        /// <param name="_restLine">(double) building restriction line, offset distance from street.</param>
        /// <param name="_dens">(double) density.</param>
        /// <param name="_minDepth">(double) minimum building depth.</param>
        /// <param name="maxDepth">(double) maximum building depth.</param>

        public Parameter(double _conDist, double _maxArea, double _offset, double _frontLine, double _restLine, double _dens, double _minDepth, double _maxDepth)
        {
            this.ConnectionDistance = _conDist;
            this.MaxAreaValue = _maxArea;
            this.StreetOffset = _offset;
            this.BuildingRestrictionLine = _restLine;
            this.Density = _dens;
            this.FrontageLine = _frontLine;
            this.MinBuildingDepth = _minDepth;
            this.MaxBuildingDepth = _maxDepth;

            this.buildingLength = 30.0d;
            this.buildingWidth = 20.0d;
            this.specifiedBorder = new List<Line2D>();
        }

        public Parameter(double _conDist, double _maxArea, double _offset, double _frontLine, double _restLine, double _dens, double _minDepth, double _maxDepth,  double _buildingWidth, double _buildingLength, double _scalingFactor, List<Line2D> _specifiedBorder )
        {
            this.ConnectionDistance = _conDist;
            this.MaxAreaValue = _maxArea;
            this.StreetOffset = _offset;
            this.BuildingRestrictionLine = _restLine;
            this.Density = _dens;
            this.FrontageLine = _frontLine;
            this.MinBuildingDepth = _minDepth;
            this.MaxBuildingDepth = _maxDepth;

            this.specifiedBorder = _specifiedBorder;
            this.buildingWidth =( _buildingWidth > 0) ? _buildingWidth :20.0d;
            this.buildingLength = (_buildingLength > 0) ? _buildingLength : 30.0d;
            this.scalingFactor = _scalingFactor;
        }
        #endregion

        #region METHODS

        #endregion
    }
}
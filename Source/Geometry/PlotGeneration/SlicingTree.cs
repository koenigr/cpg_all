﻿// SlicingTree.cs
//----------------------------------
// Last modified on 04/17/2012
// by Katja Knecht
//
// Description:
// The STree class describes a slicing tree with its nodes and leaves as well as syntax and contains all necessary methods to build and alter such a tree.
//
// Comments:
// 
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Windows.Forms;
using CPlan.Geometry;
using MathNet.Numerics.Financial;
using NetTopologySuite.IO;
using NetTopologySuite.Operation.Distance3D;
using OpenTK;
using OpenTK.Graphics.ES20;
using OpenTK.Graphics.OpenGL4;
using QuickGraph;
using QuickGraph.Algorithms.MaximumFlow;
using QuickGraph.Serialization;


namespace CPlan.Geometry.PlotGeneration
{
    public class STree
    {
        #region PROPERTIES

        /// <summary>
        /// Gets or sets the layout parameters for the building lot as well as  building generation.
        /// </summary>
        public Parameter LayoutParameters
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the list of leaves of a Tree object.
        /// </summary>
        /// <value>(List Node) leaves.</value>
        public List<Node> Leaves
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the list of nodes of a Tree object.
        /// </summary>
        /// <value>(List Node) nodes.</value>
        public List<Node> Nodes
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the number of leaves of a Tree object.
        /// </summary>
        /// <value>(int) number of leaves.</value>
        public int NoOfLeaves
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the list of polygons of a Tree object..
        /// </summary>
        /// <value>(List Vector2d[]) polygons</value>
        //public List<Vector2d[]> Polygons
        //{
        //    get;
        //    private set;
        //}

        public List<Polygon> Polygons
        {
            get;
            private set;
        }
        /// <summary>
        /// Gets the regions of a Tree object.
        /// </summary>
        /// <value>(Polygon[]) regions.</value>
        public Polygon[] BuildingBlocks
        {
            get;
            private set;
        }

        public Polygon[] Regions
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the root node of a Tree object.
        /// </summary>
        /// <value>(Node) root.</value>
        private Node Root
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the root nodes of a Tree object.
        /// </summary>
        /// <value>(Node[]) roots.</value>
        public Node[] Roots
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the graph of the tree.
        /// </summary>
        /// <value>(Subdivision) graph.</value>
        public Subdivision Graph
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the original graph of the tree.
        /// </summary>
        /// <value>(Subdivision) original graph.</value>
        private Subdivision OriginalGraph
        {
            get;
            set;
        }

        private Subdivision[] subGraphs
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the syntax of a Tree object.
        /// </summary>
        /// <value>(String) syntax.</value>
        private String Syntax
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Syntaxes of the Tree.
        /// </summary>
        /// <value>(String[]) syntaxes.</value>
        public String[] Syntaxes
        {
            get;
            set;
        }



        public List<Line2D> boundaryEdges
        {
            get; set;
        }
        public int getLevels()
        {
            return this._levelCounter;
        }


        public Node firstLevelNode { get; set; }

        public List<Node> firstLevelNodes { get; set; }
        // Counter

        private int _levelCounter;

        // Operators

        private char[] operators = new char[] { 'H', 'V' };
        private char terminal = 'o';

        private static double _splitRate = 0.5d;
        private Random rand = new Random(RandomNumberGenerator.Create().GetHashCode());
        private double rotationEpsilon = 0.01;

        private Node rootNode;
        #endregion


        #region CONSTRUCTORS

        /// <summary>
        /// Initializes a new instance of the STree type using the specified Subdivision, max area and connection distance values.
        /// </summary>
        /// <param name="_graph">(Subdivision) graph.</param>
        /// <param name="_maxArea">(double) max. area.</param>
        /// <param name="_connectionDist">(double) connection distance.</param>
        public STree(Subdivision _graph, double _maxArea, double _connectionDist, double _offset, double _density)
        {
            this.Graph = _graph as Subdivision;
            this.OriginalGraph = _graph as Subdivision;
            this.LayoutParameters = new Parameter();
            this.LayoutParameters.MaxAreaValue = _maxArea;
            this.LayoutParameters.ConnectionDistance = _connectionDist;
            this.LayoutParameters.StreetOffset = _offset;
            this.LayoutParameters.Density = _density;
            this.LayoutParameters.buildingWidth = 20.0d;

            this.Initialize();

        }


        public STree(Subdivision _graph, double _maxArea, double _connectionDist, double _offset, double _density, double _buidlingWidth)
        {
            this.Graph = _graph as Subdivision;
            this.OriginalGraph = _graph as Subdivision;
            this.LayoutParameters = new Parameter();
            this.LayoutParameters.MaxAreaValue = _maxArea;
            this.LayoutParameters.ConnectionDistance = _connectionDist;
            this.LayoutParameters.StreetOffset = _offset;
            this.LayoutParameters.Density = _density;
            this.LayoutParameters.buildingWidth = _buidlingWidth;

            this.Initialize();
        }

        /// <summary>
        /// Initializes a new instance of the STree type using the specified Subdivision and max area values.
        /// </summary>
        /// <param name="_graph">(Subdivision) graph.</param>
        /// <param name="_maxArea">(double) max. area.</param>
        public STree(Subdivision _graph, double _maxArea) : this(_graph, _maxArea, 5.0d, 3.0d, 0.3d)
        {

        }

        /// <summary>
        /// Initializes a new instance of the STree type using the specified Subdivision and parameters.
        /// </summary>
        /// <param name="_graph">(Subdivision) graph.</param>
        /// <param name="_param">(Parameter) layout parameter.</param>
        public STree(Subdivision _graph, Parameter _param)
        {
            this.Graph = _graph as Subdivision;
            this.OriginalGraph = _graph as Subdivision;
            this.LayoutParameters = _param;
            this.boundaryEdges = this.LayoutParameters.specifiedBorder;

            this.Initialize();

            //empowershack
           // this.Initialize2();
        }



        #endregion


        #region METHODS

        #region Initialization

        /// <summary>
        /// Initializes the Tree object.
        /// </summary>
        public void Initialize()
        {

            this.Leaves = new List<Node>();
            this.Nodes = new List<Node>();
            this.Polygons = new List<Polygon>();
            this.firstLevelNodes = new List<Node>();
            Polygon[] tmpRegions = Polygon.ToPolygonArrayFromVector2dArray(this.Graph.ToPolygons());

            //roate the polygons when initializing
            this.Regions = Polygon.OffsetPolygons2(tmpRegions, this.LayoutParameters.StreetOffset);

            //test.sw = new StreamWriter(@"C:\\Users\\Yufan Miao\\Desktop\\work\\test.txt");
            //test.sw.WriteLine(this.boundaryEdges[0].Start.X + " " + this.boundaryEdges[0].Start.Y + " " + this.boundaryEdges[0].End.X + " " + this.boundaryEdges[0].End.Y);
            for (int i = 0; i < this.Regions.Length; i++)
            {
                for (int j = 0; j < this.Regions[i].Edges.Length; j++)
                {
                    Line2D curEdge = this.Regions[i].Edges[j];
                    // test.sw.WriteLine(curEdge.Start.X + " " + curEdge.Start.Y + " " + curEdge.End.X + " " + curEdge.End.Y);
                    for (int k = 0; k < this.boundaryEdges.Count; k++)
                    {
                        bool isQuasiCollinear = checkQuasiCollinear(curEdge, this.boundaryEdges[k],
                            2 * this.LayoutParameters.scalingFactor + this.LayoutParameters.StreetOffset, 1);



                        if (isQuasiCollinear)
                        //  if (relation == Line2DRelation.Collinear || (relation == Line2DRelation.Parallel && (curEdge.DistanceToLine(this.boundaryEdges[k]) < 2 + this.LayoutParameters.StreetOffset)))
                        {
                            this.Regions[i].edgesOnBoundaryIdx.Add(j);
                            //test.sw.WriteLine(tmpRegions[i].Edges[j].Start.X + " " + tmpRegions[i].Edges[j].Start.Y + " " + tmpRegions[i].Edges[j].End.X + " " + tmpRegions[i].Edges[j].End.Y );
                        }
                    }


                }

            }
            //test.sw.Close();
            this.Graph = Subdivision.FromPolygons(Polygon.ToListVector2dArray(this.Regions));

            for (int i = 0; i < Regions.Length; ++i)
            {
                double maxEdge = 0;
                int maxEdgeIdx = 0;
                for (int j = 0; j < Regions[i].minBounds.Edges.Count; ++j)
                {
                    if (Regions[i].minBounds.Edges[j].Length > maxEdge)
                    {
                        maxEdge = Regions[i].minBounds.Edges[j].Length;
                        maxEdgeIdx = j;
                    }
                }
                //absolute angle of the edge
                double angleEdgeHorizontal = Regions[i].minBounds.Edges[maxEdgeIdx].Angle;

                // limit the angle from -pi to pi
                if (angleEdgeHorizontal > Math.PI)
                {
                    angleEdgeHorizontal -= 2 * Math.PI;
                }


                //keep all the angles to be positive
                //unify the rotation between 0 to pi
                if (angleEdgeHorizontal < 0)
                {
                    angleEdgeHorizontal += Math.PI;
                }


                if (angleEdgeHorizontal - Math.PI / 2.0 > rotationEpsilon)
                {
                    double angleDiff = angleEdgeHorizontal - Math.PI * 3.0 / 4.0;
                    if (angleDiff > rotationEpsilon)
                    {
                        Regions[i].rotatedAngle = Math.PI - angleEdgeHorizontal;
                    }
                    else
                    {
                        Regions[i].rotatedAngle = Math.PI / 2.0 - angleEdgeHorizontal;
                    }
                }
                else if (Math.PI / 2.0 - angleEdgeHorizontal > rotationEpsilon)
                {
                    double angleDiff = angleEdgeHorizontal - Math.PI / 4.0;
                    if (angleDiff > rotationEpsilon)
                    {
                        Regions[i].rotatedAngle = Math.PI / 2.0 - angleEdgeHorizontal;
                    }
                    else
                    {
                        Regions[i].rotatedAngle = -angleEdgeHorizontal;
                    }
                }

                //if rotateAngle is larger than the tolerance
                if (Math.Abs(Regions[i].rotatedAngle) > this.rotationEpsilon)
                {
                    //rotate points of the region and reform the region

                    for (int j = 0; j < Regions[i].Vertices.Length; ++j)
                    {
                        Vector2d coords = Regions[i].Vertices[j];
                        double new_x = coords.X * Math.Cos(Regions[i].rotatedAngle) -
                                       coords.Y * Math.Sin(Regions[i].rotatedAngle);
                        double new_y = coords.X * Math.Sin(Regions[i].rotatedAngle) +
                                       coords.Y * Math.Cos(Regions[i].rotatedAngle);
                        Regions[i].Vertices[j] = new Vector2d(new_x, new_y);

                    }

                    Regions[i].Bounds = GeoAlgorithms2D.BoundingBoxAdjusted(Regions[i].Vertices);

                    Regions[i].Edges = GeoAlgorithms2D.ConnectPoints(true, Regions[i].Vertices);


                }
                else
                {
                    Regions[i].rotatedAngle = 0;
                }


            }

            //subgraphs for each block to ensure each block is subdivided independently
            subGraphs = new Subdivision[Regions.Length];
            for (int i = 0; i < Regions.Length; ++i)
            {
                IList<Vector2d[]> Poly2DivideList = new List<Vector2d[]>();

                Poly2DivideList.Add(Regions[i].Vertices);

                subGraphs[i] = Subdivision.FromPolygons(Poly2DivideList);

            }
            ////if (this.LayoutParameters.FrontageLine != 0.0d)
            ////    this.BuildingBlocks = Polygon.OffsetPolygons2(tmpRegions, this.LayoutParameters.FrontageLine + this.LayoutParameters.StreetOffset);
            ////else this.BuildingBlocks = this.Regions;
            if (this.LayoutParameters.FrontageLine != 0.0d)
                this.BuildingBlocks = Polygon.OffsetPolygons2(Regions, this.LayoutParameters.StreetOffset);
            else this.BuildingBlocks = this.Regions;

            // this.Graph = Subdivision.FromPolygons(Polygon.ToListVector2dArray(this.Regions));

            this.Roots = new Node[this.Regions.Length];


            this._levelCounter = 0;


            if (this.Syntaxes == null)
            {
                this.Syntaxes = new String[Regions.Length];

                for (int i = 0; i < Regions.Length; i++)
                {

                    this._levelCounter = 0;

                    this.Root = new Node(this.Regions[i]);
                    this.Root.RootGraph = subGraphs[i];
                    if (this.BuildingBlocks.Length > i)
                        this.Root.Buildable = this.BuildingBlocks[i];
                    else
                        this.Root.Buildable = Polygon.Empty;
                    //CreateTreeAndSyntax uses DetermineOperation which uses this.Root



                    this.Roots[i] = this.CreateTreeAndSyntax(this.Regions[i]);


                    if (this.BuildingBlocks.Length > i)
                        this.Roots[i].Buildable = this.BuildingBlocks[i];
                    else this.Roots[i].Buildable = Polygon.Empty;
                    this.Syntaxes[i] = this.Syntax;
                    // this.Syntax = "";

                }


                //List<Line2D> lines = new List<Line2D>();
                //leaves were initialized in CreateTreeAndSyntax


                foreach (Node leaf in this.Leaves)
                {

                    this.Polygons.Add(leaf.Region);
                    //foreach (Line2D line in leaf.Region.Edges)
                    //{
                    //    lines.Add(line);
                    //}

                    if (leaf.IsLeaf())
                    {
                        if (leaf.Region != leaf.RootRegion)
                        {
                            leaf.RootRegion = rotateRegionBack(leaf.RootRegion.rotatedAngle, leaf.RootRegion,
                                rotationEpsilon);
                        }
                        // leaf.RootBuildable = rotateRegionBack(leaf.angle, leaf.RootBuildable, rotationEpsilon);
                        //newNode.DetermineBuildable();
                        this.CalculateBuildings(leaf);
                        //for (int i = 0; i < leaf.Region.Vertices.Length; i++)
                        //{

                        //    test.sw.Write(leaf.Region.Vertices[i].X + " " + leaf.Region.Vertices[i].Y + " ");
                        //}
                        //test.sw.WriteLine();

                    }

                }

                // MessageBox.Show(this.Leaves.Count.ToString());
                Polygon[] polygon4Graph = this.Polygons.ToArray();

                this.Graph = Subdivision.FromPolygons(Polygon.ToListVector2dArray(polygon4Graph));
                this.Graph = this.Merge(this.OriginalGraph, this.Graph);
            }


        }


        //for empowershack
        public void Initialize2()
        {

            this.Leaves = new List<Node>();
            this.Nodes = new List<Node>();
            this.Polygons = new List<Polygon>();
            this.firstLevelNodes = new List<Node>();
            Polygon[] tmpRegions = Polygon.ToPolygonArrayFromVector2dArray(this.Graph.ToPolygons());

            //roate the polygons when initializing
            this.Regions = Polygon.OffsetPolygons2(tmpRegions, this.LayoutParameters.StreetOffset);

            //test.sw = new StreamWriter(@"C:\\Users\\Yufan Miao\\Desktop\\work\\test.txt");
            //test.sw.WriteLine(this.boundaryEdges[0].Start.X + " " + this.boundaryEdges[0].Start.Y + " " + this.boundaryEdges[0].End.X + " " + this.boundaryEdges[0].End.Y);
            for (int i = 0; i < this.Regions.Length; i++)
            {
                for (int j = 0; j < this.Regions[i].Edges.Length; j++)
                {
                    Line2D curEdge = this.Regions[i].Edges[j];
                    // test.sw.WriteLine(curEdge.Start.X + " " + curEdge.Start.Y + " " + curEdge.End.X + " " + curEdge.End.Y);
                    for (int k = 0; k < this.boundaryEdges.Count; k++)
                    {
                        bool isQuasiCollinear = checkQuasiCollinear(curEdge, this.boundaryEdges[k],
                            2 * this.LayoutParameters.scalingFactor + this.LayoutParameters.StreetOffset, 1);



                        if (isQuasiCollinear)
                        //  if (relation == Line2DRelation.Collinear || (relation == Line2DRelation.Parallel && (curEdge.DistanceToLine(this.boundaryEdges[k]) < 2 + this.LayoutParameters.StreetOffset)))
                        {
                            this.Regions[i].edgesOnBoundaryIdx.Add(j);
                            //test.sw.WriteLine(tmpRegions[i].Edges[j].Start.X + " " + tmpRegions[i].Edges[j].Start.Y + " " + tmpRegions[i].Edges[j].End.X + " " + tmpRegions[i].Edges[j].End.Y );
                        }
                    }


                }

            }
            //test.sw.Close();
            this.Graph = Subdivision.FromPolygons(Polygon.ToListVector2dArray(this.Regions));

            for (int i = 0; i < Regions.Length; ++i)
            {
                double maxEdge = 0;
                int maxEdgeIdx = 0;
                for (int j = 0; j < Regions[i].minBounds.Edges.Count; ++j)
                {
                    if (Regions[i].minBounds.Edges[j].Length > maxEdge)
                    {
                        maxEdge = Regions[i].minBounds.Edges[j].Length;
                        maxEdgeIdx = j;
                    }
                }
                //absolute angle of the edge
                double angleEdgeHorizontal = Regions[i].minBounds.Edges[maxEdgeIdx].Angle;

                // limit the angle from -pi to pi
                if (angleEdgeHorizontal > Math.PI)
                {
                    angleEdgeHorizontal -= 2 * Math.PI;
                }


                //keep all the angles to be positive
                //unify the rotation between 0 to pi
                if (angleEdgeHorizontal < 0)
                {
                    angleEdgeHorizontal += Math.PI;
                }


                if (angleEdgeHorizontal - Math.PI / 2.0 > rotationEpsilon)
                {
                    double angleDiff = angleEdgeHorizontal - Math.PI * 3.0 / 4.0;
                    if (angleDiff > rotationEpsilon)
                    {
                        Regions[i].rotatedAngle = Math.PI - angleEdgeHorizontal;
                    }
                    else
                    {
                        Regions[i].rotatedAngle = Math.PI / 2.0 - angleEdgeHorizontal;
                    }
                }
                else if (Math.PI / 2.0 - angleEdgeHorizontal > rotationEpsilon)
                {
                    double angleDiff = angleEdgeHorizontal - Math.PI / 4.0;
                    if (angleDiff > rotationEpsilon)
                    {
                        Regions[i].rotatedAngle = Math.PI / 2.0 - angleEdgeHorizontal;
                    }
                    else
                    {
                        Regions[i].rotatedAngle = -angleEdgeHorizontal;
                    }
                }

                //if rotateAngle is larger than the tolerance
                if (Math.Abs(Regions[i].rotatedAngle) > this.rotationEpsilon)
                {
                    //rotate points of the region and reform the region

                    for (int j = 0; j < Regions[i].Vertices.Length; ++j)
                    {
                        Vector2d coords = Regions[i].Vertices[j];
                        double new_x = coords.X * Math.Cos(Regions[i].rotatedAngle) -
                                       coords.Y * Math.Sin(Regions[i].rotatedAngle);
                        double new_y = coords.X * Math.Sin(Regions[i].rotatedAngle) +
                                       coords.Y * Math.Cos(Regions[i].rotatedAngle);
                        Regions[i].Vertices[j] = new Vector2d(new_x, new_y);

                    }

                    Regions[i].Bounds = GeoAlgorithms2D.BoundingBoxAdjusted(Regions[i].Vertices);

                    Regions[i].Edges = GeoAlgorithms2D.ConnectPoints(true, Regions[i].Vertices);


                }
                else
                {
                    Regions[i].rotatedAngle = 0;
                }


            }

            //subgraphs for each block to ensure each block is subdivided independently
            subGraphs = new Subdivision[Regions.Length];
            for (int i = 0; i < Regions.Length; ++i)
            {
                IList<Vector2d[]> Poly2DivideList = new List<Vector2d[]>();

                Poly2DivideList.Add(Regions[i].Vertices);

                subGraphs[i] = Subdivision.FromPolygons(Poly2DivideList);

            }
            ////if (this.LayoutParameters.FrontageLine != 0.0d)
            ////    this.BuildingBlocks = Polygon.OffsetPolygons2(tmpRegions, this.LayoutParameters.FrontageLine + this.LayoutParameters.StreetOffset);
            ////else this.BuildingBlocks = this.Regions;
            if (this.LayoutParameters.FrontageLine != 0.0d)
                this.BuildingBlocks = Polygon.OffsetPolygons2(Regions, this.LayoutParameters.StreetOffset);
            else this.BuildingBlocks = this.Regions;

            // this.Graph = Subdivision.FromPolygons(Polygon.ToListVector2dArray(this.Regions));

            //sliced polygons into two parts here




            for (int k = 0; k < this.Regions.Length; k++)
            {
                this.Root = new Node(this.Regions[k]);
                this.Root.RootGraph = subGraphs[k];
                if (this.BuildingBlocks.Length > k)
                    this.Root.Buildable = this.BuildingBlocks[k];
                else
                    this.Root.Buildable = Polygon.Empty;



                // MessageBox.Show("region num " + this.Regions.Length);

                List<Polygon> bisectPolygons = BisectionBlocks(this.Regions[k]);

                // MessageBox.Show("divided num " + bisectPolygons.Count);
                //for (int i = 0; i < bisectPolygons.Count; i++)
                //{
                //    for (int j = 0; j < bisectPolygons[i].Vertices.Length; j++)
                //    {
                //        test.sw.Write(bisectPolygons[i].Vertices[j].X + " " + bisectPolygons[i].Vertices[j].Y + " ");
                //    }
                //    test.sw.WriteLine();
                //}

                List<Node> bisectNodes = new List<Node>();


                //if (this.Syntaxes == null)
                //{
                //this.Syntaxes = new String[bisectPolygons.Count];

                for (int i = 0; i < bisectPolygons.Count; i++)
                {



                    this._levelCounter = -1;
                    Node bisectNode = DefineNewNode2(bisectPolygons[i]);
                    double edges2SplitLength = bisectNode.Region.Edges[bisectNode.LongestEdgeOnStreetIdx].Length;
                    //calculate subblocks widths
                    int parcelNumWithPreciseWidth = (int) (edges2SplitLength/this.LayoutParameters.buildingWidth);

                    List<int> depthTrees = new List<int>();
                    List<double> subBlockWidths = new List<double>();
                    int previousValidatedLeaveNum = 0;

                    double previousWidth = 0;
                    while (parcelNumWithPreciseWidth > 0)
                    {
                        parcelNumWithPreciseWidth -= previousValidatedLeaveNum;
                        //node depth not leaves
                        int depthTree = (int) Math.Floor(Math.Log(parcelNumWithPreciseWidth, 2)) - 1;
                        previousValidatedLeaveNum = (int) Math.Pow(2, depthTree + 1);
                        depthTrees.Add(depthTree);
                        double currentWidth = previousWidth +
                                              this.LayoutParameters.buildingWidth*Math.Pow(2, depthTree + 1);
                        subBlockWidths.Add(currentWidth);
                        previousWidth = currentWidth;
                    }

                    //slice the block into subblocks

                    List<Polygon> splittedPolygons = SplitBisectPolygon(bisectNode, subBlockWidths);



                    //CreateTree uses DetermineOperation which uses this.Root

                    for (int j = 0; j < splittedPolygons.Count; j++)
                    {


                        bisectNodes.Add(this.CreateTree2(splittedPolygons[j]));


                    }


                }


                // this.Syntax = "";

                // }


                //List<Line2D> lines = new List<Line2D>();
                //leaves were initialized in CreateTreeAndSyntax
            }
            int count = 0;
            foreach (Node leaf in this.Leaves)
                {

                    this.Polygons.Add(leaf.Region);
                    //foreach (Line2D line in leaf.Region.Edges)
                    //{
                    //    lines.Add(line);
                    //}

                    if (leaf.IsLeaf())
                    {
                    //if (leaf.Region != leaf.RootRegion)
                    //{
                    //    leaf.RootRegion = rotateRegionBack(leaf.RootRegion.rotatedAngle, leaf.RootRegion,
                    //        rotationEpsilon);
                    //}

                        leaf.Region = rotateRegionBack(leaf.RootRegion.rotatedAngle, leaf.Region, rotationEpsilon);
                    // leaf.RootBuildable = rotateRegionBack(leaf.angle, leaf.RootBuildable, rotationEpsilon);
                    //newNode.DetermineBuildable();
                    this.CalculateBuildings(leaf);
                        //for (int i = 0; i < leaf.Region.Vertices.Length; i++)
                        //{

                        //    test.sw.Write(leaf.Region.Vertices[i].X + " " + leaf.Region.Vertices[i].Y + " ");
                        //}
                        //test.sw.WriteLine();

                    }

     

                // MessageBox.Show(this.Leaves.Count.ToString());
                Polygon[] polygon4Graph = this.Polygons.ToArray();

                this.Graph = Subdivision.FromPolygons(Polygon.ToListVector2dArray(polygon4Graph));
                this.Graph = this.Merge(this.OriginalGraph, this.Graph);
                    count++;
                }


        }
        public List<Polygon> BisectionBlocks( Polygon region)
        {
            List<Polygon> bisectionRegions = new List<Polygon>();

            
            //divide from the middle
           
           
                Node parentNode =  DefineNewNode2(region);
                bool notBisectable = false;
                if (parentNode.Operator == 'H')
                {
                    parentNode.Operator = 'V';
                }
                else if (parentNode.Operator == 'V')
                {
                    parentNode.Operator = 'H';
                }
                else
                {
                    notBisectable = true;
                }

                if (!notBisectable)
                {
                    List<Line2D> edges = region.minBounds.Edges;
                    edges.Sort(
                        delegate(Line2D l1, Line2D l2)
                        {
                            return Comparer<double>.Default.Compare(l1.Length, l2.Length);
                        });

                    if (edges.First().Length >= 2*this.LayoutParameters.buildingLength)
                    {
                        //Line2D div = parentNode.ParallelOffsetLine2D(edges.Last(), edges.First().Length / 2);
                        double divValue = parentNode.CalculateSplitValue(0.5);
                        Line2D div;
                        if (parentNode.Operator == 'H')
                        {

                            div = new Line2D(parentNode.Region.Bounds.TopLeft.X - 0.1d, divValue,
                                parentNode.Region.Bounds.BottomRight.X + 0.1d,
                                divValue);

                        }
                        else
                        {
                            div = new Line2D(divValue, parentNode.Region.Bounds.TopLeft.Y - 0.1d, divValue,
                                parentNode.Region.Bounds.BottomRight.Y + 0.1d);

                        }

                        Polygon[] subPolygons;
                        Line2D[] polygonLines;


                        if (div.Length != 0)
                        {

                            //try
                            //{
                            //parentNode.Region.ParallelIntersection(div, edges.Last(), out leftPolygon);
                            //Polygon leftPoly = new Polygon(leftPolygon);
                            //leftPoly.rotatedAngle = Regions[i].rotatedAngle;

                            // test.sw.WriteLine("div "+div.Start.X + " " + div.Start.Y + " " + div.End.X + " " + div.End.Y);
                            //  test.sw.WriteLine("bound " + parentNode.Region.Bounds.BottomLeft.X + " " + parentNode.Region.Bounds.BottomLeft.Y + " " + parentNode.Region.Bounds.BottomRight.X + " " +parentNode.Region.Bounds.BottomRight.Y + " " + parentNode.Region.Bounds.TopRight.X + " " + parentNode.Region.Bounds.TopRight.Y + " " + parentNode.Region.Bounds.TopLeft.X + " " + parentNode.Region.Bounds.TopLeft.Y);



                            div = parentNode.Region.IntersectsWith(div, out polygonLines);


                            subPolygons = parentNode.Region.CalculateSplitPolygons(polygonLines);


                            //for (int j = 0; j < subPolygons.Length; j++)
                            //{
                            //    test.sw.WriteLine(subPolygons[j].Bounds.BottomLeft.X + " " + subPolygons[j].Bounds.BottomLeft.Y + " " + subPolygons[j].Bounds.BottomRight.X + " " + subPolygons[j].Bounds.BottomRight.Y + " " + subPolygons[j].Bounds.TopRight.X + " " + subPolygons[j].Bounds.TopRight.Y + " " + subPolygons[j].Bounds.TopLeft.X + " " + subPolygons[j].Bounds.TopLeft.Y);
                            //}
                            for (int j = 0; j < subPolygons.Length; j++)
                            {
                                bisectionRegions.Add(new Polygon(subPolygons[j].Vertices));
                            }

                            //}
                            //catch (Exception e)
                            //{
                            //    System.Console.WriteLine("left polygon is not successfully bisected");
                            //}


                        }
                        else
                        {
                            bisectionRegions.Add(new Polygon(region.Vertices));
                        }
                    }
                    else
                    {
                        bisectionRegions.Add(new Polygon(region.Vertices));
                    }

                }
                else
                {
                    bisectionRegions.Add(new Polygon(region.Vertices));
                }

        

          
            return bisectionRegions;
        }
        /// <summary>
        /// Merges two given Subdivision graphs.
        /// </summary>
        /// <param name="_graph1">(Subdivision) graph 1.</param>
        /// <param name="_graph2">(Subdivision) graph 2.</param>
        /// <returns>(Subdivision) merged graph.</returns>
        private Subdivision Merge(Subdivision _graph1, Subdivision _graph2)
        {

            IList<Vector2d[]> poly1 = _graph1.ToPolygons();
            IList<Vector2d[]> poly2 = _graph2.ToPolygons();

            var merged = poly1.Concat(poly2).ToList();
            return Subdivision.FromPolygons(merged);

        }

        #endregion

        #region Tree, Index Sequence and Syntax Creation

        private Node DefineNewNode(Polygon _region)
        {
            char operation;
            Node newNode = new Node(this._levelCounter);

            newNode.Region = _region;
            newNode.angle = _region.rotatedAngle;

            if (newNode.Region.edgesOnBoundaryIdx.Count == 0)
            {
                for (int i = 0; i < newNode.Region.Edges.Length; i++)
                {
                    for (int j = 0; j < this.Root.Region.edgesOnBoundaryIdx.Count; j++)
                    {
                        int edgeIdxOnBound = this.Root.Region.edgesOnBoundaryIdx[j];

                        bool isQuasiCollinear = checkQuasiCollinear(newNode.Region.Edges[i],
                            this.Root.Region.Edges[edgeIdxOnBound], 1.0 * this.LayoutParameters.scalingFactor, 0.01);


                        if (isQuasiCollinear)
                        // if (relation == Line2DRelation.Collinear|| (relation == Line2DRelation.Parallel && (newNode.Region.Edges[i].DistanceToLine(this.Root.Region.Edges[edgeIdxOnBound]) < 2 )))
                        {
                            newNode.Region.edgesOnBoundaryIdx.Add(i);

                            //  break;

                        }
                    }

                }
            }
            //double regionArea = _region.GetArea();
            // Function nodes
            operation = this.DetermineOperation(newNode);// this.functions[rand.Next(0, 2)];






            //if (regionArea >= this.LayoutParameters.MaxAreaValue && (operation!=this.terminal))
            //if (operation != this.terminal)
            // {

            //          //this.Syntax += operation + "(";          
            //  }
            //  else // Leaves
            //  {
            //     // operation = this.terminal;
            //     // this.Syntax += operation;


            //      this._levelCounter--;
            //  }

            newNode.Operator = operation;

            return newNode;
        }

        /// <summary>
        /// Creates a syntax and the respective Tree and returns the Tree's root node.
        /// </summary>
        /// <value>(Node) root node.</value>
        private Node CreateTreeAndSyntax(Polygon _region)
        {
            //if (this.Leaves.Count == 11)
            //{
            //    MessageBox.Show("debug");
            //}

            Node newNode = DefineNewNode(_region);
            newNode.RootGraph = this.Root.RootGraph;

            if (newNode.Operator != this.terminal) // continue building the tree
            {



                Polygon[] subpolygons = this.CreateSubdivision(newNode);



                //if (subpolygons.Length == 1)
                //{
                //    MessageBox.Show("problem");
                //}

                //  test.sw.WriteLine(subpolygons.Length.ToString());
                if (subpolygons[0] == newNode.Region)
                {
                    //edge condition for the recursion
                    newNode.Operator = this.terminal;
                    newNode.NoOfLeaves = 1;

                    newNode.rotateBack(this.rotationEpsilon);

                    this.Leaves.Add(newNode);


                }
                else
                {
                    // left branch; the tree goes down to the deepest left branch
                    _levelCounter++;
                    Node leftNode = new Node();
                    leftNode.Parent = newNode;
                    leftNode.RootGraph = newNode.RootGraph;
                    leftNode = this.CreateTreeAndSyntax(subpolygons[0]);
                    leftNode.Parent = newNode;

                    newNode.LeftChild = leftNode;
                    if (leftNode.Operator != this.terminal)
                    {
                        newNode.NoOfLeaves += leftNode.NoOfLeaves;
                    }
                    else
                    {
                        newNode.NoOfLeaves += 1;
                    }
                    // this.Syntax += ")(";
                    if (subpolygons.Length > 1)
                    {
                        Node rightNode = new Node();
                        rightNode.Parent = newNode;
                        rightNode.RootGraph = newNode.RootGraph;
                        rightNode = this.CreateTreeAndSyntax(subpolygons[1]);
                        rightNode.Parent = newNode;

                        newNode.RightChild = rightNode;
                        if (rightNode.Operator != terminal)
                        {
                            newNode.NoOfLeaves += rightNode.NoOfLeaves;
                        }
                        else
                        {
                            newNode.NoOfLeaves += 1;
                        }
                    }
                    // this.Syntax += ")";
                    _levelCounter--;

                }
                this.Nodes.Add(newNode);

            }
            else
            {
                newNode.NoOfLeaves = 1;

                newNode.rotateBack(this.rotationEpsilon);

                this.Leaves.Add(newNode);

            }


            newNode.RootRegion = this.Root.Region;
            newNode.RootBuildable = this.Root.Buildable;

            //if (newNode.IsLeaf())
            //{
            //    //newNode.DetermineBuildable();
            //    this.CalculateBuildings(newNode);
            //}

            return newNode;
        }

        private Node CreateTree2(Polygon _region)
        {
            
            Node newNode = DefineNewNode2(_region);
            newNode.RootGraph = this.Root.RootGraph;

            if (newNode.Operator != this.terminal) // continue building the tree
            {



                Polygon[] subpolygons = this.CreateSubdivision(newNode);



                //if (subpolygons.Length == 1)
                //{
                //    MessageBox.Show("problem");
                //}

                //  test.sw.WriteLine(subpolygons.Length.ToString());
                if (subpolygons[0] == newNode.Region)
                {
                    //edge condition for the recursion
                    newNode.Operator = this.terminal;
                    newNode.NoOfLeaves = 1;

                    newNode.rotateBack(this.rotationEpsilon);

                    this.Leaves.Add(newNode);


                }
                else
                {
                    // left branch; the tree goes down to the deepest left branch
                    _levelCounter++;
                    Node leftNode = new Node();
                    leftNode.Parent = newNode;
                    leftNode.RootGraph = newNode.RootGraph;
                    leftNode = this.CreateTree2(subpolygons[0]);
                    leftNode.Parent = newNode;

                    newNode.LeftChild = leftNode;
                    if (leftNode.Operator != this.terminal)
                    {
                        newNode.NoOfLeaves += leftNode.NoOfLeaves;
                    }
                    else
                    {
                        newNode.NoOfLeaves += 1;
                    }
                    // this.Syntax += ")(";
                    if (subpolygons.Length > 1)
                    {
                        Node rightNode = new Node();
                        rightNode.Parent = newNode;
                        rightNode.RootGraph = newNode.RootGraph;
                        rightNode = this.CreateTree2(subpolygons[1]);
                        rightNode.Parent = newNode;

                        newNode.RightChild = rightNode;
                        if (rightNode.Operator != terminal)
                        {
                            newNode.NoOfLeaves += rightNode.NoOfLeaves;
                        }
                        else
                        {
                            newNode.NoOfLeaves += 1;
                        }
                    }
                    // this.Syntax += ")";
                    _levelCounter--;

                }
                this.Nodes.Add(newNode);

            }
            else
            {
                newNode.NoOfLeaves = 1;

                newNode.rotateBack(this.rotationEpsilon);

                this.Leaves.Add(newNode);

            }


            newNode.RootRegion = this.Root.Region;
            newNode.RootBuildable = this.Root.Buildable;

            //if (newNode.IsLeaf())
            //{
            //    //newNode.DetermineBuildable();
            //    this.CalculateBuildings(newNode);
            //}

            return newNode;
        }

        //for empowershack
        private Node DefineNewNode2(Polygon _region)
        {
            char operation;
            Node newNode = new Node(this._levelCounter);

            newNode.Region = _region;
            newNode.angle = _region.rotatedAngle;

            if (newNode.Region.edgesOnBoundaryIdx.Count == 0)
            {
                for (int i = 0; i < newNode.Region.Edges.Length; i++)
                {
                    for (int j = 0; j < this.Root.Region.edgesOnBoundaryIdx.Count; j++)
                    {
                        int edgeIdxOnBound = this.Root.Region.edgesOnBoundaryIdx[j];

                        bool isQuasiCollinear = checkQuasiCollinear(newNode.Region.Edges[i],
                            this.Root.Region.Edges[edgeIdxOnBound], 1.0 * this.LayoutParameters.scalingFactor, 0.01);


                        if (isQuasiCollinear)

                        {
                            newNode.Region.edgesOnBoundaryIdx.Add(i);

                            //  break;

                        }
                    }

                }
            }



            //for empowershack
            operation = this.DetermineOperation2(newNode);


            newNode.Operator = operation;

            return newNode;
        }
    
 
        /// <summary>
        /// Determines the operation in the specified node.
        /// </summary>
        /// <param name="_curNode">(Node) current node.</param>
        /// <returns>(char) operation.</returns>
        private char DetermineOperation(Node _curNode)
        {
            List<Line2D> streetEdges = new List<Line2D>();

            List<Line2D> nonStreetEdges = new List<Line2D>();

            //only edges collinear to the streets are considered



            for (int i = 0; i < this.Root.Region.Edges.Length; i++)
            {
                //make sure buildings are built along the street
                for (int j = 0; j < _curNode.Region.Edges.Length; j++)
                {
                    bool isQuasiCollinear = checkQuasiCollinear(this.Root.Region.Edges[i], _curNode.Region.Edges[j], 1.0 * this.LayoutParameters.scalingFactor, 0.01);
                    bool excluded = false;
                    if (isQuasiCollinear)
                    {

                        for (int k = 0; k < _curNode.Region.edgesOnBoundaryIdx.Count; k++)
                        {
                            if (_curNode.Region.edgesOnBoundaryIdx[k] == j)
                            {
                                excluded = true;

                            }
                            if (excluded)
                            {

                                break;
                            }
                        }

                        if (!excluded)
                        {


                            streetEdges.Add(_curNode.Region.Edges[j]);

                        }
                    }

                    if (!excluded && (!isQuasiCollinear))
                    {
                        nonStreetEdges.Add(_curNode.Region.Edges[j]);
                    }


                }
            }






            if (streetEdges.Count == 0)
            {


                return this.terminal;
            }


            //subDLine = CheckProportion(boundaryEdges);
            streetEdges.Sort(delegate (Line2D l1, Line2D l2) { return Comparer<double>.Default.Compare(l1.Length, l2.Length); });
            nonStreetEdges.Sort(delegate (Line2D l1, Line2D l2) { return Comparer<double>.Default.Compare(l1.Length, l2.Length); });


            //double offset = 5.0d;
            //double angleTolerance = Math.PI/6.0;
            //double length = 30;
            bool isQuasiParallel = false;
            bool breakLoop = false;
            if (streetEdges.Count >= 2 && (nonStreetEdges.Count > 0))
            {
                for (int i = 0; i < nonStreetEdges.Count; i++)

                {
                    List<int> oppositeStreetEdgeIdx = new List<int>();
                    for (int j = 0; j < streetEdges.Count; j++)
                    {

                        bool isNeighbor = nonStreetEdges[i].Intersect(streetEdges[j]);
                        if (isNeighbor)
                        {

                            oppositeStreetEdgeIdx.Add(j);
                        }

                        if (oppositeStreetEdgeIdx.Count == 2)
                        {
                            isQuasiParallel = checkQuasiCollinear(streetEdges[oppositeStreetEdgeIdx[0]], streetEdges[oppositeStreetEdgeIdx.Last()], 2.0 * this.LayoutParameters.buildingLength, 0.01);
                        }

                        if (isQuasiParallel)
                        {

                            streetEdges.Add(nonStreetEdges[i]);
                            //  test.sw.WriteLine(nonStreetEdges[i].Start.X + " " + nonStreetEdges[i].Start.Y + " " + nonStreetEdges[i].End.X + " " + nonStreetEdges[i].End.Y);
                            breakLoop = true;

                            break;
                        }

                    }
                    if (breakLoop) break;
                }

            }

            return checkEdge4Slicing(streetEdges);
            // //check if the  divided line is long enough to place the building 

            // char operation = checkBuildingWidth(streetEdges);

            // if (operation == this.terminal)
            // {
            //     streetEdges.RemoveAt(streetEdges.Count - 1);
            //     if (streetEdges.Count > 0)
            //     {
            //         for (int i = 0; i < streetEdges.Count; ++i)
            //         {
            //             operation = checkBuildingWidth(streetEdges);

            //             //check the parallel streets here

            //             if (operation != this.terminal)
            //             {
            //                 return operation;
            //             }
            //         }

            //     }
            //     return this.terminal;
            // }

            // return operation;



        }

        //for empowershack project
        private char DetermineOperation2(Node _curNode)
        {
            List<Line2D> streetEdges = new List<Line2D>();

            List<Line2D> nonStreetEdges = new List<Line2D>();

            //only edges collinear to the streets are considered

            //find out the longest edges in root
            List<Line2D> sortedEdgesRoot = new List<Line2D>(this.Root.Region.Edges);

            sortedEdgesRoot.Sort(
                 delegate (Line2D l1, Line2D l2) { return Comparer<double>.Default.Compare(l1.Length, l2.Length); });

            Line2D[] longestEdges = new Line2D[2] { sortedEdgesRoot.Last(), sortedEdgesRoot[sortedEdgesRoot.Count - 2] };

            for (int i = 0; i < longestEdges.Length; i++)
            {

                //make sure buildings are built along the street
                for (int j = 0; j < _curNode.Region.Edges.Length; j++)
                {
                    bool isQuasiCollinear = checkQuasiCollinear(longestEdges[i], _curNode.Region.Edges[j],
                        1.0 * this.LayoutParameters.scalingFactor, 0.01);
                    bool excluded = false;
                    if (isQuasiCollinear)
                    {

                        for (int k = 0; k < _curNode.Region.edgesOnBoundaryIdx.Count; k++)
                        {
                            if (_curNode.Region.edgesOnBoundaryIdx[k] == j)
                            {
                                excluded = true;

                            }
                            if (excluded)
                            {

                                break;
                            }
                        }

                        if (!excluded)
                        {


                            streetEdges.Add(_curNode.Region.Edges[j]);

                        }
                    }

                    if (!excluded && (!isQuasiCollinear))
                    {
                        nonStreetEdges.Add(_curNode.Region.Edges[j]);
                    }


                }


            }




            if (streetEdges.Count == 0)
            {


                return this.terminal;
            }


            //subDLine = CheckProportion(boundaryEdges);
            streetEdges.Sort(delegate (Line2D l1, Line2D l2) { return Comparer<double>.Default.Compare(l1.Length, l2.Length); });

            for (int i = 0; i < _curNode.Region.Edges.Length; i++)
            {
                if (_curNode.Region.Edges[i] == streetEdges.Last())
                {
                    _curNode.LongestEdgeOnStreetIdx = i;
                    break;
                }
            }
            return SplitBasedOnExactLength(streetEdges.Last(), this.LayoutParameters.buildingWidth);
            // //check if the  divided line is long enough to place the building 

            // char operation = checkBuildingWidth(streetEdges);

            // if (operation == this.terminal)
            // {
            //     streetEdges.RemoveAt(streetEdges.Count - 1);
            //     if (streetEdges.Count > 0)
            //     {
            //         for (int i = 0; i < streetEdges.Count; ++i)
            //         {
            //             operation = checkBuildingWidth(streetEdges);

            //             //check the parallel streets here

            //             if (operation != this.terminal)
            //             {
            //                 return operation;
            //             }
            //         }

            //     }
            //     return this.terminal;
            // }

            // return operation;



        }
        /// <summary>
        /// Checks the proportion of the region formed by the given boundaries and returns the line for Subdivision.
        /// </summary>
        /// <param name="_boundaries">(List Line2D) boundaries.</param>
        /// <returns>(Line2D) line for Subdivision (height or width).</returns>
        private Line2D CheckProportion(List<Line2D> _boundaries)
        {
            Polygon poly;
            List<Vector2d> points = new List<Vector2d>();

            for (int i = 0; i < _boundaries.Count; i++)
            {
                Vector2d pts = _boundaries[i].Start;
                Vector2d pte = _boundaries[i].End;
                bool s = false;
                bool e = false;

                if (points.Count >= 1)
                {
                    for (int j = 0; j < points.Count; j++)
                    {
                        if (pts == points[j]) s = true;
                        if (pte == points[j]) e = true;
                    }
                    if (!s) points.Add(pts);
                    if (!e) points.Add(pte);
                }
                else
                {
                    points.Add(pts);
                    points.Add(pte);
                }
            }

            poly = new Polygon(points.ToArray());

            Line2D height = new Line2D(poly.Bounds.TopLeft, poly.Bounds.BottomLeft);
            Line2D width = new Line2D(poly.Bounds.TopLeft, poly.Bounds.TopRight);

            double proportion = (width.Length < height.Length) ? width.Length / height.Length : height.Length / width.Length;


            if (proportion < 0.75d) // proportion parameter?
                return (width.Length < height.Length) ? height : width;


            _boundaries.Sort(delegate (Line2D l1, Line2D l2) { return Comparer<double>.Default.Compare(l1.Length, l2.Length); });

            // //check if the  divided line is long enough to place the building 
            Line2D line2Divide = _boundaries[_boundaries.Count - 1];





            return line2Divide;





        }


        private int cnt = 0;

        /// <summary>
        /// Creates the sub division partition of a specified Node object.
        /// </summary>
        /// <param name="_region">(Node) current node.</param>
        /// <value>(Polygon[]) subpolygons.</value>
        public Polygon[] CreateSubdivision(Node _curNode)
        {
            int dir = (_curNode.Operator != this.operators[0]) ? 0 : 1;
            Polygon[] subPolygons;
            bool isAdded = false;
            Line2D div;
            double divValue = _curNode.CalculateSplitValue(_splitRate);

            //Calculate the dividing line
            if (dir == 1)
            {
                //the rectangle is vertical
                //Line2D div = curNode.Region.Bounds.Intersect(line, curNode.Subdivision); bei beliebiger Linie einsetzen

                div = new Line2D(_curNode.Region.Bounds.TopLeft.X, divValue, _curNode.Region.Bounds.BottomRight.X,
                    divValue);


            }
            else
            {

                div = new Line2D(divValue, _curNode.Region.Bounds.TopLeft.Y, divValue,
                    _curNode.Region.Bounds.BottomRight.Y);

            }

            // test.sw.WriteLine(div.Start.X + " " + div.Start.Y + " " + div.End.X + " " + div.End.Y);

            if (double.IsNaN(div.Length) || div.Length == 0.0d)
            {
                //    Console.WriteLine(cnt.ToString());
                //    if (cnt > 10)
                //    {
                //        subPolygons = new Polygon[0];

                //        cnt = 0;
                //        return subPolygons;
                //    }
                //    //MessageBox.Show(cnt.ToString());
                //    _splitRate = 0.5d;

                //    //if (cnt % 2 == 0)
                //    //{
                //    //    //_splitRate = 0.5d;
                //    //    // _splitRate += 0.5d/(2^cnt);
                //    //    _splitRate += rand.NextDouble() / 2;
                //    //    // _splitRate += 0.5d/2.0;
                //    //    // _splitRate += epsilon;
                //    //}
                //    //else
                //    //{
                //    //    // _splitRate -= 0.5d/2.0;
                //    //    //_splitRate = 0.5d;
                //    //    _splitRate -= rand.NextDouble() / 2;
                //    //    //_splitRate -= epsilon;
                //    //    //_splitRate -= 0.1/2;
                //    //}
                //    cnt++;


                try
                {
                    // subPolygons = this.CreateSubdivision(_curNode);

                    //for empowershack
                    subPolygons = this.CreateSubdivision(_curNode);
                }
                catch
                {
                    subPolygons = new Polygon[1];
                    subPolygons[0] = _curNode.Region;

                }


            }
            else
            {
                Line2D[] polygonlines;

                //  test.sw.WriteLine(div.Start.X + " " +  div.Start.Y + " " + div.End.X + " " + div.End.Y);


                div = _curNode.Region.IntersectsWith(div, out polygonlines);
                //for (int i = 0; i < polygonlines.Length; i++)
                //{
                //    test.sw.WriteLine(polygonlines[i].Start.X + " " + polygonlines[i].Start.Y + " " + polygonlines[i].End.X + " " + polygonlines[i].End.Y);
                //}
                // test.sw.WriteLine(polygonlines.Length.ToString());
                subPolygons = _curNode.Region.CalculateSplitPolygons(polygonlines);

                if (subPolygons.Length < 1)
                {
                    //  MessageBox.Show("problem");
                    subPolygons = new Polygon[1];
                    subPolygons[0] = _curNode.Region;
                }

                subPolygons[0].rotatedAngle = _curNode.angle;

                if (subPolygons.Length > 1)
                {

                    subPolygons[1].rotatedAngle = _curNode.angle;
                }
                //// start edge
                //double distS, distE;
                ////local subdivision


                //SubdivisionEdge startEdge = _curNode.RootGraph.FindNearestEdge(div.Start, out distS);


                //Vector2d nearestNode;
                //double distance;

                //Line2D sLine = new Line2D(startEdge.Origin, startEdge.Destination);
                //Vector2d sPt = sLine.Intersect(div.Start);


                //nearestNode = _curNode.RootGraph.GetNearestNode(sPt);
                //distance = GeoAlgorithms2D.DistancePoints(sPt, nearestNode);
                //Line2DLocation location = sLine.Locate(nearestNode);

                //if (distance < this.LayoutParameters.ConnectionDistance)
                ///* if(distance < this.LayoutParameters.ConnectionDistance 
                // && (location == Line2DLocation.Start || location == Line2DLocation.End))
                // //nearestNode.IsCollinear(sLine.Start, sLine.End)) // node on the specific edge*/
                //{
                //    if (location == Line2DLocation.Start)
                //    {
                //        sPt = startEdge.Origin;
                //    }
                //    else if (location == Line2DLocation.End)
                //    {
                //        sPt = startEdge.Destination;
                //    }
                //    else
                //    {
                //        sPt = nearestNode;
                //    }
                //}
                //else
                //{

                //    SubdivisionEdge halfEdgeS = _curNode.RootGraph.SplitEdge(startEdge.Key);
                //    bool onHalfEdge = false;
                //    if ((halfEdgeS.ToLine()).Locate(sPt) == Line2DLocation.Between)
                //    {
                //        onHalfEdge = true;
                //    }
                //    //  else onHalfEdge = false;

                //    distance = GeoAlgorithms2D.DistancePoints(halfEdgeS.Origin, sPt);
                //    if (onHalfEdge)
                //    {
                //        halfEdgeS.Origin.Move(halfEdgeS.Destination, (float)distance);
                //    }
                //    else
                //    {

                //        halfEdgeS.Origin.Move(startEdge.Origin, (float)distance);
                //    }
                //    _curNode.RootGraph.MoveVertex(_curNode.RootGraph.FindNearestVertex(halfEdgeS.Origin), sPt);

                //    sPt = halfEdgeS.Origin;

                //}




                //// end edge

                //SubdivisionEdge endEdge = _curNode.RootGraph.FindNearestEdge(div.End, out distE);

                //Line2D eLine = new Line2D(endEdge.Origin, endEdge.Destination);
                //Vector2d ePt = eLine.Intersect(div.End);


                //nearestNode = _curNode.RootGraph.GetNearestNode(ePt);
                //distance = GeoAlgorithms2D.DistancePoints(ePt, nearestNode);
                //location = eLine.Locate(nearestNode);

                //if (distance < this.LayoutParameters.ConnectionDistance)
                ///* if(distance < this.LayoutParameters.ConnectionDistance
                //  && (location == Line2DLocation.Start || location == Line2DLocation.End)) //nearestNode.IsCollinear(sLine.Start, sLine.End)) // node on the specific edge */
                //{
                //    if (location == Line2DLocation.Start)
                //    {
                //        ePt = endEdge.Origin;
                //    }
                //    else if (location == Line2DLocation.End)
                //    {
                //        ePt = endEdge.Destination;
                //    }
                //    else
                //    {
                //        ePt = nearestNode;
                //    }
                //}
                //else
                //{


                //    SubdivisionEdge halfEdgeE = _curNode.RootGraph.SplitEdge(endEdge.Key);

                //    bool onHalfEdge = false;
                //    Line2DLocation locHalfEdge = halfEdgeE.ToLine().Locate(ePt);
                //    Line2DLocation locEndEdge = endEdge.ToLine().Locate(ePt);
                //    if (locHalfEdge == Line2DLocation.Between)
                //    {
                //        onHalfEdge = true;
                //    }
                //    else onHalfEdge = false;

                //    if (onHalfEdge) distance = GeoAlgorithms2D.DistancePoints(halfEdgeE.Origin, ePt);
                //    else distance = GeoAlgorithms2D.DistancePoints(endEdge.Origin, ePt);
                //    Vector2d newEPt = (onHalfEdge)
                //        ? halfEdgeE.Origin.Move(halfEdgeE.Destination, (float)distance)
                //        : halfEdgeE.Origin.Move(endEdge.Origin, (float)distance);

                //    _curNode.RootGraph.MoveVertex(_curNode.RootGraph.FindNearestVertex(halfEdgeE.Origin), ePt);
                //    ePt = halfEdgeE.Origin;
                //}



                //Line2D subD = new Line2D(sPt, ePt);
                //_curNode.SubDivision = subD;
                //if (subD.Length != 0.0d)
                //{
                //    int a, b;

                //    _curNode.RootGraph.AddEdge(sPt, ePt, out a, out b);

                //    if (a != -1)
                //    {
                //        isAdded = true;
                //    }
                //    else
                //    {

                //        //isAdded = false;
                //        Console.WriteLine("Subdivision could not be added.");
                //    }
                //}
                //else
                //{
                //    //isAdded = false;
                //    Console.WriteLine("Subdivision length is zero.");
                //}

                //if (isAdded)
                //{

                //    Vector2d[] poly1 = _curNode.RootGraph.FindEdge(subD.Start, subD.End).CyclePolygon;
                //    Vector2d[] poly2 = _curNode.RootGraph.FindEdge(subD.End, subD.Start).CyclePolygon;
                //    subPolygons = new Polygon[2];
                //    subPolygons[0] = new Polygon(poly1);
                //    subPolygons[0].rotatedAngle = _curNode.angle;
                //    subPolygons[1] = new Polygon(poly2);
                //    subPolygons[1].rotatedAngle = _curNode.angle;

                //}
                //else
                //{
                //    //Console.WriteLine(cnt.ToString());
                //    if (cnt > 10)
                //    {
                //        subPolygons = new Polygon[0];

                //        cnt = 0;

                //        return subPolygons;
                //    }
                //    cnt++;
                //    //    //MessageBox.Show(cnt.ToString());
                //    //if (cnt % 3 == 0) _splitRate += rand.NextDouble() / 2;
                //    //else if (cnt % 3 == 1) _splitRate -= rand.NextDouble() / 2;
                //    //    //if (cnt % 3 == 0) _splitRate += epsilon / 2;
                //    //    //else if (cnt % 3 == 1) _splitRate -= epsilon / 2;
                //    //else _curNode.Operator = (_curNode.Operator == this.operators[1]) ? this.operators[0] : this.operators[1];
                //    //    //if (cnt % 3 == 0) _splitRate = 0.5d;
                //    //    //else if (cnt % 3 == 1) _splitRate = 0.5d;
                //    //if ((!(cnt % 3 == 0)) && (!(cnt % 3 == 1)))
                //    //{
                //    //    _curNode.Operator = (_curNode.Operator == this.operators[1]) ? this.operators[0] : this.operators[1];
                //    //}
                //    try
                //{
                //    subPolygons = this.CreateSubdivision(_curNode);
                //}
                //catch
                //{
                //    subPolygons = new Polygon[0];

                //}
                //// _splitRate = 0.5;
                // }



            }



            return subPolygons;
        }

        public int GetTreeDepthNumWithSpecWidth(Line2D longestEdge)
        {

            int depth = (int)Math.Floor(Math.Log(longestEdge.Length / this.LayoutParameters.buildingWidth, 2));

            return depth;
        }
        //for empowershack project
        public List<Polygon> SplitBisectPolygon(Node _curNode, List<double> subBlockWidth)
        {
            int dir = (_curNode.Operator != this.operators[0]) ? 0 : 1;
            Polygon[] subPolygons;
            List<Polygon> splitedPolygons = new List<Polygon>();
            Line2D div ;
            Polygon poly2Divide = new Polygon(_curNode.Region.Vertices); 

            for (int i = 0; i < subBlockWidth.Count; i++)
            {


                double divValue = _curNode.CalculateSplitValueWithLength(subBlockWidth[i]);

                //Calculate the dividing line
                if (dir == 1)
                {
                    //the rectangle is vertical
                    //Line2D div = curNode.Region.Bounds.Intersect(line, curNode.Subdivision); bei beliebiger Linie einsetzen

                    div = new Line2D(_curNode.Region.Bounds.TopLeft.X, divValue, _curNode.Region.Bounds.BottomRight.X,
                        divValue);
                    

                }
                else
                {

                    div = new Line2D(divValue, _curNode.Region.Bounds.TopLeft.Y, divValue,
                        _curNode.Region.Bounds.BottomRight.Y);

                }



                if (double.IsNaN(div.Length) || div.Length == 0.0d)
                {

                    try
                    {
                        subPolygons = SplitBisectPolygon(_curNode, subBlockWidth).ToArray();
                    }
                    catch
                    {
                        subPolygons = new Polygon[1];
                        subPolygons[0] = _curNode.Region;

                    }

                    if (subPolygons.Length > 1)
                    {
                        poly2Divide = subPolygons[1];
                    }
                    else
                    {
                        poly2Divide = subPolygons[0];
                    }
                    
                }
                else
                {
                    Line2D[] polygonlines;


                    div = poly2Divide.IntersectsWith(div, out polygonlines);

                    subPolygons = poly2Divide.CalculateSplitPolygons(polygonlines);

                    if (subPolygons.Length < 1)
                    {

                        subPolygons = new Polygon[1];
                        subPolygons[0] = _curNode.Region;
                       
                    }

                    subPolygons[0].rotatedAngle = _curNode.Region.rotatedAngle;
                    poly2Divide = new Polygon(subPolygons[0].Vertices);

                    if (subPolygons.Length > 1)
                    {

                        
                        subPolygons[1].rotatedAngle = _curNode.Region.rotatedAngle;
                        poly2Divide = new Polygon(subPolygons[1].Vertices);
                    }

                }

               
            splitedPolygons.Add(subPolygons[0]);

            }
            return splitedPolygons;
        }
        #endregion



        #region Building Calculation



        /// <summary>
        /// Calculates the buildings for a specified node and adds them to the list of buildings of the Tree.
        /// </summary>
        /// <param name="_curNode">(Node) current node.</param>
        private void CalculateBuildings(Node _curNode)
        {
            List<Line2D> boundaryEdges = new List<Line2D>();
            List<Line2D> innerEdges = new List<Line2D>();



            for (int i = 0; i < _curNode.RootRegion.Edges.Length; i++)
            {
                for (int j = 0; j < _curNode.Region.Edges.Length; j++)
                {

                    bool isCollinear = checkQuasiCollinear(_curNode.RootRegion.Edges[i], _curNode.Region.Edges[j], 1.0 * this.LayoutParameters.scalingFactor, 0.01);

                    if (isCollinear)
                    {
                        bool excluded = false;
                        for (int k = 0; k < _curNode.RootRegion.edgesOnBoundaryIdx.Count; k++)
                        {
                            excluded =
                                checkQuasiCollinear(
                                    _curNode.RootRegion.Edges[_curNode.RootRegion.edgesOnBoundaryIdx[k]],
                                    _curNode.Region.Edges[j], 1.0 * this.LayoutParameters.scalingFactor, 0.01);

                            if (excluded)
                            {

                                break;
                            }
                        }
                        if (!excluded)
                        {
                            boundaryEdges.Add(_curNode.Region.Edges[j]);
                        }
                    }
                    else
                    {
                        innerEdges.Add(_curNode.Region.Edges[j]);
                    }
                }
            }

            // MessageBox.Show(boundaryEdges.Count + " ");
            //boundaryEdges.Sort(delegate(Line2D l1, Line2D l2) { return Comparer<double>.Default.Compare(l1.Length, l2.Length); });
            if (boundaryEdges.Count > 0)
            {

                _curNode.CalculateBuildings(boundaryEdges, this.LayoutParameters, this.Root.Buildable);

            }
        }

        #endregion

        #region Adjustments and Settings

        /// <summary>
        /// Sets the syntax of the Tree recursively.
        /// </summary>
        /// <param name="_curNode">(Node) current node.</param>
        private void SetSyntax(Node _curNode)
        {
            if (_curNode.LeftChild != null && _curNode.RightChild != null)
            {
                this.Syntax += _curNode.Operator.ToString() + '(';
                this.SetSyntax(_curNode.LeftChild);
                this.Syntax += ")(";
                this.SetSyntax(_curNode.RightChild);
                this.Syntax += ')';
            }
            else
            {
                this.Syntax += _curNode.Operator.ToString();
            }
        }



        /// <summary>
        /// Converts the Tree object into an array of polygons.
        /// </summary>
        /// <returns>(Vector2d[][]) tree polygons.</returns>
        public Vector2d[][] ToPolygons()
        {
            Vector2d[][] tmpArray = new Vector2d[this.Polygons.Count][];
            for (int i = 0; i < this.Polygons.Count; ++i)
            {
                tmpArray[i] = this.Polygons[i].Vertices;
            }
            return tmpArray;
        }

        public Polygon rotateRegionBack(double angle, Polygon _region, double rotationEpsilon)
        {

            Polygon region = _region;
            if (Math.Abs(angle) > rotationEpsilon)
            {
                double rotateAngle = -angle;
                for (int j = 0; j < region.Vertices.Length; ++j)
                {
                    Vector2d coords = region.Vertices[j];
                    double new_x = coords.X * Math.Cos(rotateAngle) - coords.Y * Math.Sin(rotateAngle);
                    double new_y = coords.X * Math.Sin(rotateAngle) + coords.Y * Math.Cos(rotateAngle);
                    region.Vertices[j] = new Vector2d(new_x, new_y);

                    region.rotatedAngle = 0;
                }
                region.Edges = GeoAlgorithms2D.ConnectPoints(true, region.Vertices);
                region.Bounds = GeoAlgorithms2D.BoundingBoxAdjusted(region.Vertices);

            }
            return region;
        }



        public char checkBuildingWidth(List<Line2D> edgesOnStreet)
        {
            Line2D subDLine = edgesOnStreet.Last();

            double splittedSubDLineLength = subDLine.Length * _splitRate;


            double min1 = -3.0d / 4.0d * Math.PI;
            double max1 = -1.0d / 4.0d * Math.PI;
            double min2 = 1.0d / 4.0d * Math.PI;
            double max2 = 3.0d / 4.0d * Math.PI;

            // double offset = 1.0d ;
            double thresholdEdge = this.LayoutParameters.buildingWidth;
            bool checkLength = true;


            for (int i = 0; i < this.Root.Region.Edges.Length; i++)
            {
                bool isOnStreet = checkQuasiCollinear(subDLine, this.Root.Region.Edges[i],
                    1.0 * this.LayoutParameters.scalingFactor + this.LayoutParameters.StreetOffset, 0.01);

                if (isOnStreet)
                {
                    checkLength = false;
                    break;
                }


            }

            if (checkLength)
            {
                thresholdEdge = this.LayoutParameters.buildingLength;
            }

            if (splittedSubDLineLength > thresholdEdge)
            {


                if ((subDLine.Angle > min1 && subDLine.Angle < max1) || (subDLine.Angle > min2 && subDLine.Angle < max2))
                {
                    return this.operators[0];

                }

                return this.operators[1];

            }

            return this.terminal;

        }

        public char SplitBasedOnExactLength(Line2D subDLine, double splittedSubDLineLength)
        {

  


            double min1 = -3.0d / 4.0d * Math.PI;
            double max1 = -1.0d / 4.0d * Math.PI;
            double min2 = 1.0d / 4.0d * Math.PI;
            double max2 = 3.0d / 4.0d * Math.PI;

            // double offset = 1.0d ;




            if (splittedSubDLineLength < subDLine.Length)
            {


                if ((subDLine.Angle > min1 && subDLine.Angle < max1) || (subDLine.Angle > min2 && subDLine.Angle < max2))
                {
                    return this.operators[0];

                }

                return this.operators[1];

            }

            return this.terminal;

        }
        bool checkQuasiCollinear(Line2D line1, Line2D line2, double epsilon_dist, double epsilon_angle)
        {


            Line2DRelation relationLines = line1.getLine2DIntersection(line2).Relation;

            double angle = Math.Abs(line1.Angle - line2.Angle);
            bool quasiParallel = false;
            if (angle > Math.PI / 2.0)
            {
                if (Math.Abs(angle - Math.PI) < epsilon_angle)
                {
                    quasiParallel = true;
                }
            }
            else
            {
                if (angle < epsilon_angle)
                {
                    quasiParallel = true;
                }
            }


            if (relationLines == Line2DRelation.Collinear || (quasiParallel && (line1.DistanceToLine(line2) < epsilon_dist)))
            {
                return true;
            }

            return false;
        }

        char checkEdge4Slicing(List<Line2D> streetEdges)
        {
            char operation = checkBuildingWidth(streetEdges);

            if (operation == this.terminal)
            {
                streetEdges.RemoveAt(streetEdges.Count - 1);
                if (streetEdges.Count > 0)
                {

                    operation = checkEdge4Slicing(streetEdges);

                    //check the parallel streets here

                    if (operation != this.terminal)
                    {
                        return operation;
                    }


                }

            }

            return operation;

        }
        //private void scalingParamsUp()
        //{
        //    double scaling = this.LayoutParameters.scalingFactor;
        //    this.LayoutParameters.BuildingRestrictionLine *= scaling;
        //    this.LayoutParameters.ConnectionDistance *= scaling;
        //    this.LayoutParameters.BuildingRestrictionLine *= scaling;
        //    this.LayoutParameters.Density *= scaling;
        //    this.LayoutParameters.MaxAreaValue *= (scaling*scaling);
        //    this.LayoutParameters.FrontageLine *= scaling;
        //    this.LayoutParameters.StreetOffset *= scaling;
        //    this.LayoutParameters.MaxBuildingDepth *= scaling;
        //    this.LayoutParameters.MinBuildingDepth *= scaling;
        //    this.LayoutParameters.buildingLength *= scaling;
        //    this.LayoutParameters.buildingWidth *= scaling;

        public void ParcelShapeRectification(Node parcel)
        {
            if (parcel.IsLeaf())
            {
                List<Line2D> envelopeEdges = parcel.Region.minBounds.Edges;
                // lets define longer edge as width

                if (envelopeEdges[0].Length > envelopeEdges[1].Length)
                {
                    if (envelopeEdges[0].Length > 2.0 * this.LayoutParameters.buildingWidth)
                    {
                        // extra edges
                    }
                }
                else
                {
                    if (envelopeEdges[1].Length > 2.0 * this.LayoutParameters.buildingWidth)
                    {
                        // extra edges
                    }
                }

            }
        }

        public void DefineLeavesSiblings()
        {

            for (int i = 0; i < this.Leaves.Count; i++)
            {
                Leaves[i].LeafIdx = i;
            }

            //using (StreamWriter sw = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\leaveIdx.txt", false))
            //{
            //    foreach (var leaf in this.Leaves)
            //    {
            //        sw.WriteLine(leaf.LeafIdx);
            //    }
            //}
            List<Node> NotPairedLeafList = new List<Node>(this.Leaves);
            for (int i = 0; i < this.Leaves.Count; ++i)
            {

                if (!this.Leaves[i].Paired)
                {
                    int pairedIdx = -1;

                    NotPairedLeafList.Remove(this.Leaves[i]);
                    for (int j = 0; j < NotPairedLeafList.Count; j++)
                    {

                        if (this.Leaves[i].Parent == NotPairedLeafList[j].Parent)
                        {
                            this.Leaves[i].Sibling = NotPairedLeafList[j];
                            this.Leaves[NotPairedLeafList[j].LeafIdx].Sibling = this.Leaves[i];
                            this.Leaves[i].Paired = true;
                            this.Leaves[NotPairedLeafList[j].LeafIdx].Paired = true;
                            pairedIdx = j;
                            break;
                        }
                    }
                    //can be problematic
                    if (pairedIdx >= 0)
                    {
                        NotPairedLeafList.RemoveAt(pairedIdx);
                    }
                    else
                    {
                        break;
                    }
                }

            }

            if (NotPairedLeafList.Count > 0)
            {
                foreach (var leaf in NotPairedLeafList)
                {
                    if (leaf == leaf.Parent.LeftChild)
                    {
                        Node siblingNode = leaf.Parent.RightChild;
                        while (!siblingNode.IsLeaf())
                        {
                            siblingNode = siblingNode.LeftChild;
                        }
                        this.Leaves[leaf.LeafIdx].Sibling = siblingNode;
                        this.Leaves[leaf.LeafIdx].Paired = true;
                    }
                    else
                    {
                        Node siblingNode = leaf.Parent.LeftChild;
                        while (!siblingNode.IsLeaf())
                        {
                            siblingNode = siblingNode.RightChild;
                        }
                        this.Leaves[leaf.LeafIdx].Sibling = siblingNode;
                        this.Leaves[leaf.LeafIdx].Paired = true;
                    }
                }
            }

        }

        public void PrintLeaveClusters()
        {
            using (StreamWriter sw = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\parcel_clusters.txt", false))
            {
                sw.WriteLine("LeafIdx" + "\t" + "siblingIdx");
                foreach (var leaf in this.Leaves)
                {

                    sw.Write(leaf.LeafIdx + "\t");
                    if (leaf.Sibling != null)
                    {
                        sw.Write(leaf.Sibling.LeafIdx + "\t");
                    }
                    sw.WriteLine();
                }
            }

        }
        //}
        #endregion

        #endregion
    }
}
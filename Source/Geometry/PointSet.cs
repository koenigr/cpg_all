﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK;

namespace CPlan.Geometry
{
    public class PointSet : ICloneable, IEnumerable<Vector3d>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Private Fields

        private List<Vector3d> _points;

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        /// <summary>
        /// Access to points of the set
        /// </summary>
        /// <param name="i">index of a point</param>
        /// <returns></returns>
        public Vector3d this[int i]
        {
            get { return _points[i]; }
            set { _points[i] = value; }
        }

        /// <summary>
        /// Returns an inner list of points that actually stores them
        /// </summary>
        public List<Vector3d> AsList
        {
            get { return _points; }
        }

        /// <summary>
        /// number of points in a set
        /// </summary>
        public int Count
        {
            get { return _points.Count; }
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        public PointSet()
        {
            _points = new List<Vector3d>();
        }

        public PointSet(IEnumerable<Vector3d> points)
        {
            _points = points.ToList();
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Helpers



        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region ICloneable implementation

        object ICloneable.Clone()
        {
            return Clone();
        }

        public PointSet Clone()
        {
            PointSet ps = new PointSet();
            ps._points = new List<Vector3d>(this._points);
            return ps;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region IEnumerable Implementation

        public IEnumerator<Vector3d> GetEnumerator()
        {
            return _points.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _points.GetEnumerator();
        }

        # endregion

    }
}

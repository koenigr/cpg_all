﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Poly2D.cs 
//  Copyright (C) 2014/10/18  12:58 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK;

namespace CPlan.Geometry
{

    [Serializable]
    public class Poly2D
    {
        private Vector2d[][] _points;

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        //==============================================================================================
        /// <summary>
        /// Area of the polygon.
        /// </summary>
        public double Area
        {
            get
            {
                double area = GeoAlgorithms2D.PolygonArea(_points[0].ToList()); 
                for (int i=1; i<_points.GetLength(0);i++)
                    area -= GeoAlgorithms2D.PolygonArea(_points[i].ToList()); 

                return area;
            }
        }

        //==============================================================================================
        /// <summary>
        /// Compactness of the polygon.
        /// </summary>
        public double Compactness
        {
            get
            {
                List<Line2D> perimeterlines = Edges;
                double perimeter = 0;
                foreach (Line2D l in perimeterlines)
                    perimeter += l.Length;


                return (Area * Math.PI*4) / (perimeter*perimeter);
            }
        }

        //==============================================================================================
        /// <summary>
        /// Convex Hull of the polygon.
        /// </summary>
        public Poly2D ConvexHull
        {
            get
            {
                var convexHullPoints = GeoAlgorithms2D.ConvexHullFromPoints(_points[0]);
                return new Poly2D(convexHullPoints.ToArray());
            }
        }


        //==============================================================================================
        /// <summary>
        /// Ratio of the Area of the polygon to Convex Hull Area.
        /// </summary>
        public double ConvexDeficiency
        {
            get
            {
                return Area/ConvexHull.Area;
            }
        }

        //==============================================================================================
        /// <summary>
        /// Minimal Distance from Center to Boundary of the polygon.
        /// </summary>
        public double MinDistFromCenter
        {
            get
            {
                Vector2d center = this.Center;

                List<Line2D> perimeterlines = Edges;
                double minWidth = double.MaxValue;
                foreach (Line2D l in perimeterlines)
                {
                    double dist = l.DistanceToPoint(center);
                    if (dist < minWidth)
                        minWidth = dist;

                }
                return minWidth;
            }
        }

        //==============================================================================================
        /// <summary>
        /// Maximal Distance from Center to Boundary of the polygon.
        /// </summary>
        public double MaxDistFromCenter
        {
            get
            {
                Vector2d center = this.Center;

                List<Line2D> perimeterlines = Edges;
                double maxWidth = double.MinValue;
                foreach (Line2D l in perimeterlines)
                {
                    double dist = l.DistanceToPoint(center);
                    if (dist > maxWidth)
                        maxWidth = dist;

                }
                return maxWidth;
            }
        }

        //==============================================================================================
        /// <summary>
        /// Width To Length Ratio of the polygon calculated by distances from center point
        /// </summary>
        public double WidthLengthRatio
        {
            get
            {
                return (MinDistFromCenter / MaxDistFromCenter);
            }
        }

        //==============================================================================================
        /// <summary>
        /// Area of the (boundary) polygon.
        /// </summary>
        public double AreaBoundary
        {
            get
            {
                // Which one is faster? This one:
                //Double area = 0;
                //CGeom3d.Vec_3d[][] points = new CGeom3d.Vec_3d[1][];
                //points[0] = Vec3DPoints;
                //CGeom3d.AreaPoly(points, ref area);
                //return area;
                // Or this one:
                return GeoAlgorithms2D.Area(_points[0].ToList());
            }
        }

        //==============================================================================================
        /// <summary>
        /// Center point of the polygon.
        /// </summary>
        public Vector2d Center
        {
            get { return GeoAlgorithms2D.Centroid(_points[0].ToList()); }
        }

        //==============================================================================================
        /// <summary>
        /// Position Vector = Center
        /// </summary>
        public Vector2d Position
        {
            get
            {
                return Center;
            }
            set
            {
                Vector2d diff = value - Center;
                Move(diff);
            }

        }

        //==============================================================================================
        /// <summary>
        /// Corner points of the polygon (and it's holes).
        /// The points of the outer boundary are at index [0]. The other indices containing the holes. 
        /// You can access the outer boundary only also with PointsFirstLoop.
        /// </summary>
        public Vector2d[][] Points
        {
            get { return _points; }
            set { _points = value; }
        }

        /// <summary>
        /// Corner points of the outer boundary of the polygon.
        /// </summary>
        public Vector2d[] PointsFirstLoop
        {
            get { return _points[0]; }
            set { _points[0] = value; }
        }

        //==============================================================================================
        /// <summary>
        /// Corner points of the polygon as 3D points.
        /// </summary>
        public CGeom3d.Vec_3d[] PointsFirstLoopVec3D
        {
            get {
                CGeom3d.Vec_3d[] vec3DList = new CGeom3d.Vec_3d[_points[0].Length];
                for (int i = 0; i < _points[0].Length; i++)
                {
                    vec3DList[i] = new CGeom3d.Vec_3d(_points[0][i].X, _points[0][i].Y, 0);
                }
                return vec3DList;
            }
        }

        //==============================================================================================
        /// <summary>
        /// Corner points of the polygon as 3D points.
        /// </summary>
        public CGeom3d.Vec_3d[][] PointsVec3D
        {
            get
            {
                CGeom3d.Vec_3d[][] vec3DList = new CGeom3d.Vec_3d[_points.GetLength(0)][];
                for (int i = 0; i < _points.Length; i++)
                {
                    CGeom3d.Vec_3d[] loop = new CGeom3d.Vec_3d[_points[i].Length];
                    for (int j=0;j<_points[i].Length;j++)
                        loop[j] = new CGeom3d.Vec_3d(_points[i][j].X, _points[i][j].Y, 0);
                    vec3DList[i] = loop;
                }
                return vec3DList;
            }
        }

        //==============================================================================================
        /// <summary>
        /// Edges of the polygon as vector.
        /// </summary>
        public List<Vector2d> EdgesV
        {
            get{ return BuildEdges(); }
        }

        /// <summary>
        /// Edges of the polygon as lines.
        /// </summary>
        public List<Line2D> Edges
        {
            get { return GetBorderLines(); }
        }

        //==============================================================================================
        /// <summary>
        /// The max y-coordinate ot the polygon.
        /// </summary>
        public double Top
        {
            get { return GeoAlgorithms2D.MaxY(Points[0].ToList()); }

        }

        //==============================================================================================
        /// <summary>
        /// The min x-coordinate ot the polygon.
        /// </summary>
        public double Left
        {
            get { return GeoAlgorithms2D.MinX (Points[0].ToList()); }
        }

        //==============================================================================================
        /// <summary>
        /// The max x-coordinate ot the polygon.
        /// </summary>
        public double Right
        {
            get { return GeoAlgorithms2D.MaxX(Points[0].ToList()); }
        }

        //==============================================================================================
        /// <summary>
        /// The min y-coordinate ot the polygon.
        /// </summary>
        public double Bottom
        {
            get { return GeoAlgorithms2D.MinY(Points[0].ToList()); }
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructor

        //==============================================================================================
        /// <summary>
        /// Create an empty polygon.
        /// </summary>
        public Poly2D()
        {
            _points = new Vector2d[1][];
        }

        //==============================================================================================
        /// <summary>
        /// Create a polygon like a rectangle. 
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        public Poly2D(double x1, double y1, double x2, double y2)
        {
            _points = new Vector2d[1][];
            _points[0] = new Vector2d[4];
            _points[0][0] = new Vector2d(x1, y1);
            _points[0][1] = new Vector2d(x2, y1);
            _points[0][2] = new Vector2d(x2, y2);
            _points[0][3] = new Vector2d(x1, y2);

        }

        //==============================================================================================
        /// <summary>
        /// Create a polygon by an array of points.
        /// </summary>
        /// <param name="vPoints">Array of Vector2d points.</param>
        public Poly2D(Vector2d[] vPoints)
        {
            _points = new Vector2d[1][];
            Points[0] = vPoints;
        }

        //==============================================================================================
        /// <summary>
        /// Create a polygon by an array of points.
        /// </summary>
        /// <param name="vPoints">Array of Vector2d points.</param>
        public Poly2D(Vector2d[][] vPoints)
        {
            Points = vPoints;
        }

        /// <summary>
        /// Create a polygon from a list of lines.
        /// </summary>
        /// <param name="lines">List of Lines2D, which form the border of the polygon.</param>
        public Poly2D(List<Line2D> lines)
        {
            _points = new Vector2d[1][];
            _points[0] = new Vector2d[lines.Count];
            for (int i = 0; i < lines.Count; i++)
            {
                _points[0][i] = lines[i].Start;
            }
        }

        //==============================================================================================
        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="source">The source polygon.</param>
        public Poly2D(Poly2D source)
        {
            _points = new Vector2d[source.Points.GetLength(0)][];

            for (int i = 0; i < source.Points.GetLength(0); i++)
            {
                Vector2d[] loop = new Vector2d[source.Points[i].Length];
                for (int j = 0; j < loop.Length; j++)
                    loop[j] = source.Points[i][j];

                _points[i] = loop;
            }
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        //==============================================================================================
        /// <summary>
        /// Clone the polygon.
        /// </summary>
        /// <returns>Returns exact clone of the polygon.</returns>
        public Poly2D Clone()
        {
            Poly2D clone = new Poly2D(this);
            return clone;
        }

        //==============================================================================================
        /// <summary>
        /// Add a corner point to the polygon (first loop).
        /// </summary>
        /// <param name="point">Point ot be added.</param>
        public void Add(Vector2d point)
        {
            if (_points[0] != null)
            {
                Vector2d[] ptList = new Vector2d[_points[0].Length + 1];
                for (int i = 0; i < _points[0].Length; i++)
                    ptList[i] = _points[0][i];

                ptList[_points[0].Length] = point;
                _points[0] = ptList;
            }
            else
            {
                _points[0] = new Vector2d[1];
                _points[0][0] = point;
            }            
        }

        //==============================================================================================
        /// <summary>
        /// Add a corner point to the polygon to the loop indexed.
        /// </summary>
        /// <param name="point">Point ot be added.</param>
        /// <param name="loopNr"></param>
        public void Add(Vector2d point, int loopNr)
        {
            if (_points[loopNr] != null)
            {
                Vector2d[] ptList = new Vector2d[_points[loopNr].Length + 1];
                for (int i = 0; i < _points[loopNr].Length; i++)
                    ptList[i] = _points[loopNr][i];

                ptList[_points[loopNr].Length] = point;
                _points[loopNr] = ptList;
            }
            else
            {
                _points[loopNr] = new Vector2d[1];
                _points[loopNr][0] = point;
            }
        }

        // Return true if the polygon is oriented clockwise.
        public bool IsOrientedClockwise()
        {
            return (SignedPolygonArea() < 0);
        }

        // Return the polygon's area in "square units."
        // The value will be negative if the polygon is
        // oriented clockwise.
        public double SignedPolygonArea()
        {
            // Add the first point to the end.
            int num_points = PointsFirstLoop.Length;
            Vector2d[] pts = new Vector2d[num_points + 1];
            Points.CopyTo(pts, 0);
            pts[num_points] = PointsFirstLoop[0];

            // Get the areas.
            double area = 0;
            for (int i = 0; i < num_points; i++)
            {
                area +=
                    (pts[i + 1].X - pts[i].X) *
                    (pts[i + 1].Y + pts[i].Y) / 2;
            }

            // Return the result.
            return area;
        }

        //==============================================================================================
        /// <summary>
        /// Unify this polygon with another one.
        /// </summary>
        /// <param name="mergePoly">The other polygon for merging.</param>
        /// <returns>The unified polygon.</returns>
        public Poly2D Union(Poly2D mergePoly)
        {

            if (_points.Length == 0)
            {
                return mergePoly.Clone();
            }
            else
            {
                //union..
                Poly2D union = GeoAlgorithms2D.UnifyPolys(this, mergePoly);
                return union;
            }       
        }

        //==============================================================================================
        public void ClearPoints()
        {
            _points = new Vector2d[1][];
        }

        //==============================================================================================
        private void CleanPoly()
        {
            double eps = 0.01;
            List<Vector2d> points = _points[0].ToList();
            for (int i = 0; i < points.Count; i++)
            {
                int j;
                if (i < points.Count - 1)
                    j = i + 1;
                else
                    j = 0;
                if (((points[i].X >= points[j].X - eps) && (points[i].X <= points[j].X + eps)
                    && (points[i].Y >= points[j].Y - eps)) && (points[i].Y <= points[j].Y + eps))
                    points.Remove(points[j]);
            }
            _points[0] = points.ToArray();
        }

        //==============================================================================================
        /// <summary>
        /// Checks if Polygon has at least 3 distinct Points.
        /// </summary>
        /// <returns>The intersection polygon.</returns>
        public bool IsValid()
        {
            CleanPoly();
            bool valid = true;
            for (int i = 0; i < _points.GetLength(0); i++)
            {
                if (_points[i].Length < 3)
                    valid = false;
            }
            return valid;
        }

        //==============================================================================================
        /// <summary>
        /// Calculate the overlapping area of this and another polygon.
        /// </summary>
        /// <param name="otherPoly">Other polygon for the overlapping test.</param>
        /// <returns>The common area of both polygons.</returns>
        public double Overlap(Poly2D otherPoly)
        {
            double shapeArea;
            List<Poly2D> isectPoly;
            isectPoly = Intersection(otherPoly);
            if (isectPoly == null) shapeArea = 0;
            else shapeArea = isectPoly[0].Area;

            return shapeArea;
        }

        //==============================================================================================
        /// <summary>
        /// Intersect this polygon with another one.
        /// </summary>
        /// <param name="intersectPoly">The other polygon for intersecting.</param>
        /// <returns>The intersection polygon.</returns>
        public List<Poly2D> Intersection(Poly2D intersectPoly)
        {
            if (_points.Length == 0)
            {
                return null;
            }
            else
            {
                List<Poly2D> intersection = GeoAlgorithms2D.IntersectPolys(this, intersectPoly);
                return intersection;
            }
        }

        //==============================================================================================
        /// <summary>
        /// Intersect this polygon with a line.
        /// </summary>
        /// <param name="intersectLine">The line for intersecting.</param>
        /// <returns>The intersection points.</returns>
        public List<Vector2d> Intersection(Line2D intersectLine)
        {
            List<Vector2d> interPts = new List<Vector2d>();
            if (_points.Length == 0)
            {
                return interPts;
            }
            else
            {
                List<Line2D> boderEdges = Edges;
                foreach (Line2D iLine in boderEdges)
                {
                    Vector2d interPt = new Vector2d();
                    int intersectTest = GeoAlgorithms2D.LineIntersection(intersectLine.Start, intersectLine.End, iLine.Start, iLine.End, ref interPt);
                    if (intersectTest == 2)
                    {
                        if (interPt != null)
                            interPts.Add(interPt);
                    }
                }
                return interPts;
            }
        }

        //==============================================================================================
        /// <summary>
        /// Test if a point is inside the polygon (first loop).
        /// </summary>
        /// <param name="point">Point to test if it is inside.</param>
        /// <returns>True if the point is inside the polygon, false otherwise.</returns>
        public bool ContainsPoint(Vector2d point)
        {
            bool isInside = true;
            if (!GeoAlgorithms2D.PointInPolygon(point, this)) 
                isInside = false;
            return isInside;
        }

        //==============================================================================================
        /// <summary>
        /// Build the edges of the polygon.
        /// </summary>
        private List<Vector2d> BuildEdges()
        {
            List<Vector2d> edges = new List<Vector2d>();
            Vector2d p1;
            Vector2d p2;
            for (int i = 0; i < PointsFirstLoop.Length; i++)
            {
                p1 = _points[0][i];
                if (i + 1 >= PointsFirstLoop.Length)
                {
                    p2 = PointsFirstLoop[0];
                }
                else
                {
                    p2 = PointsFirstLoop[i + 1];
                }
                edges.Add(p2 - p1);
            }
            return edges;
        }

        //==============================================================================================
        /// <summary>
        /// Get the edges of the polygon.
        /// </summary>
        /// <returns>List of Line2D objects.</returns>
        public List<Line2D> GetBorderLines()
        {
            List<Line2D> edges = new List<Line2D>();
            Vector2d p1;
            Vector2d p2;
            for (int i = 0; i < PointsFirstLoop.Length; i++)
            {
                p1 = _points[0][i];
                if (i + 1 >= PointsFirstLoop.Length)
                {
                    p2 = _points[0][0];
                }
                else
                {
                    p2 = _points[0][i + 1];
                }
                edges.Add(new Line2D(p1, p2) );
            }
            return edges;
        }

        /// <summary>
        /// Get the edges of the polygon.
        /// </summary>
        /// <returns>List of Line2D objects.</returns>
        public List<Tuple<Vector2d, Vector2d>> GetBorderLinesAsTuples()
        {
            var edges = new List<Tuple<Vector2d, Vector2d>>();
            Vector2d p1;
            Vector2d p2;
            for (int i = 0; i < PointsFirstLoop.Length; i++)
            {
                p1 = _points[0][i];
                if (i + 1 >= PointsFirstLoop.Length)
                {
                    p2 = _points[0][0];
                }
                else
                {
                    p2 = _points[0][i + 1];
                }
                edges.Add(Tuple.Create(p1, p2));
            }
            return edges;
        }

        //==============================================================================================
        /// <summary>
        /// Calculate the minimum distance between this and another polygon.
        /// </summary>
        /// <param name="otherPoly">Other polygon.</param>
        /// <returns></returns>
        public double Distance(Poly2D otherPoly)
        {
            //Vector2d test;
            //double dist = Distance(otherPoly, out test);
            double dist = GeoAlgorithms2D.DistancePolygons(this, otherPoly);
            return dist;
        }

        //==============================================================================================
        /// <summary>
        /// Calculate the minimum distance between this and a point.
        /// </summary>
        /// <param name="otherPoly">Other polygon.</param>
        /// <returns></returns>
        public double Distance(Vector2d point)
        {
            //Vector2d test;
            //double dist = Distance(otherPoly, out test);
            double minDist = double.MaxValue;
            List<Line2D> lines = this.GetBorderLines();
            foreach (Line2D l in lines)
            {
                double dist = GeoAlgorithms2D.DistancePointToLine(l, point);
                if (dist < minDist)
                    minDist = dist;
            }

            return minDist;
        }

        //==============================================================================================
        /// <summary>
        /// Calculate the minimum distance between this and another polygon 
        /// + gives the vector to move the otherPoly to this one.
        /// </summary>
        /// <param name="otherPoly">Other polygon.</param>
        /// <param name="delta"></param>
        /// <returns>Vector of minimal distance.</returns>
        public double Distance(Poly2D otherPoly, out Vector2d delta)
        {
            double dist = GeoAlgorithms2D.DistancePolygons(this, otherPoly);
            if (dist <= 0)
            {
                delta = new Vector2d(0, 0);
                return dist;
            }
            else
            {
                Vector2d[] otherPoints = otherPoly.Points[0];// points;
                List<Vector2d> outPoints = new List<Vector2d>();
                foreach (Vector2d tPt in otherPoints)
                {
                    if (!ContainsPoint(tPt)) outPoints.Add(tPt);
                }

                List<Line2D> borderLines = GetBorderLines();
                // -- find closest line of this polygon
                Line2D closestBorderLn = null;
                double minDistLn = double.MaxValue;
                foreach (Line2D borderL in borderLines)
                {
                    foreach (Vector2d outPt in outPoints)
                    {
                        dist = GeoAlgorithms2D.DistancePointToLine(outPt, borderL.Start, borderL.End);
                        if (dist < minDistLn) // did we look at the closest line of the current polygon?
                        {
                            minDistLn = dist;
                            closestBorderLn = borderL.Clone();
                        }
                    }
                }

                Vector2d lotPt = new Vector2d();
                Vector2d curLotPt = new Vector2d();
                Vector2d nearPt = new Vector2d();
                //double maxDistPt = double.MinValue;
                double minDistPt = double.MaxValue;
                foreach (Vector2d outPt in outPoints)
                {
                    if (closestBorderLn != null)
                        dist = GeoAlgorithms2D.DistancePointToLine(outPt, closestBorderLn.Start, closestBorderLn.End, out lotPt);
                    if (dist < minDistPt) // find point with largerst distance
                    {
                        minDistPt = dist;
                        nearPt = outPt;
                        curLotPt = lotPt;
                    }
                }
                delta = curLotPt - nearPt;
                return minDistPt;
            }
        }

        //==============================================================================================
        /// <summary>
        /// Calculate the maximum distance between this and another polygon.
        /// </summary>
        /// <param name="otherPoly">Other polygon.</param>
        /// <param name="delta">The vector to the point with the maximum distance.</param>
        /// <returns>Distance between this and another polygon</returns>
        public double MaxDistance(Poly2D otherPoly, out Vector2d delta)
        {
            Vector2d[] otherPoints = otherPoly.Points[0];// points;
            List<Vector2d> outPoints = new List<Vector2d>();
            foreach (Vector2d tPt in otherPoints)
            {
                if (!ContainsPoint(tPt)) outPoints.Add(tPt);
            }
            List<Line2D> borderLines = GetBorderLines();
            foreach (Line2D borderL in borderLines)
            {
                List<Vector2d> removePoints = new List<Vector2d>();
                foreach (Vector2d tPt in outPoints)
                {
                    if (GeoAlgorithms2D.DistancePointToLine(borderL, tPt) < 0.001) 
                        removePoints.Add(tPt);
                }
                foreach (Vector2d remover in removePoints)
                {
                    outPoints.Remove(remover);
                }
            }
            

            if (outPoints.Count() == 0) // if otherPoly is completely in this polygon
            {
                delta = new Vector2d(0,0);
                return 0;
            }

            // -- find closest line of this polygon
            Line2D closestBorderLn = null;
            double minDistLn = double.MaxValue;
            foreach (Line2D borderL in borderLines)
            { 
                foreach (Vector2d outPt in outPoints)
                {
                    double dist = GeoAlgorithms2D.DistancePointToLine(outPt, borderL.Start, borderL.End);
                    if(dist < minDistLn) // did we look at the closest line of the current polygon?
                    {
                        minDistLn = dist;
                        closestBorderLn = borderL.Clone();
                    }
                }
            }

            Vector2d lotPt = new Vector2d();
            Vector2d curLotPt = new Vector2d();
            Vector2d farPt = new Vector2d();
            double maxDistPt = double.MinValue;
            foreach (Vector2d outPt in outPoints)
            {
                if (closestBorderLn != null)
                {
                    double dist = GeoAlgorithms2D.DistancePointToLine(outPt, closestBorderLn.Start, closestBorderLn.End, out lotPt);
                    if(dist > maxDistPt) // find point with largerst distance
                    {
                        maxDistPt = dist;
                        farPt = outPt;
                        curLotPt = lotPt;
                    }
                }
            }
            delta = curLotPt - farPt;
            if (delta.Length > maxDistPt) // todo: if this is true, there is a bug somewhere...!
                delta = Center - farPt; // this is only an approximation to a good solution...!
            return maxDistPt;
        }

        //==============================================================================================
        /// <summary>
        /// Moves the rectangle.
        /// </summary>
        /// <param name="delta">The vector that indicates distance and direction of the movement.</param>
        public void Move(Vector2d delta)
        {
            for (int i = 0; i < _points.GetLength(0); i++)
            {
                for (int j = 0; j < _points[i].Length; j++)
                {
                    _points[i][j] += delta;
                }
            }
        }

        //==============================================================================================
        /// <summary>
        /// Rotate the polygon by a given angle around a rotation center.
        /// </summary>
        /// <param name="angleRadians">Angle to rotate the polygon.</param>
        /// <param name="center">Rotation center.</param>
        public void Rotate(double angleRadians, Vector2d center)
        {
            _points = GeoAlgorithms2D.RotatePoly(angleRadians, center, Points);
        }
        /// <summary>
        /// Rotate the polygon by a given angle around a rotation center.
        /// </summary>
        /// <param name="angleRadians">Angle to rotate the polygon.</param>
        /// <param name="center">Rotation center.</param>
        public void Rotate(double angleRadians)
        {
            _points = GeoAlgorithms2D.RotatePoly(angleRadians, Center, Points);
        }

        /// <summary>
        /// Scale the polygon by a given vector, which holds the x- and y-sacaling factor.
        /// </summary>
        /// <param name="scaling">Scaling vector.</param>
        public void Scale(Vector2d scaling)
        {
            Vector2d middle = Center;
            for (int i = 0; i < _points.GetLength(0); i++)
            {
                for (int j = 0; j < _points[i].Length; j++)
                {
                    Vector2d curDir = _points[i][j] - middle;
                    curDir.Normalize();
                    _points[i][j] += scaling * curDir;
                }
            }
        }

        //==============================================================================================
        /// <summary>
        /// Scale the polygon by a given vector, which holds the x- and y-sacaling factor.
        /// The scaling is applied to the local coordinate system of the minimal bounding rectangle.
        /// </summary>
        public void ScaleNorm(Vector2d newProp)
        {
            Vector2d[] clone = (Vector2d[])Points.Clone();
            List<Vector2d> tempPoly = clone.ToList();
            //tempPoly.Add(Points[0]);
            Vector2d[] minBR = GeoAlgorithms2D.MinimalBoundingRectangle(tempPoly);
            Vector2d vecA = minBR[1] - minBR[0];
            Vector2d vecB = new Vector2d(1, 0);
            double rotation = -(GeoAlgorithms2D.AngleBetweenR(vecA, vecB));
            GeoAlgorithms2D.RadianToDegree(rotation);
            //rotation = justToSee;
            Poly2D rotatedGeom = new Poly2D(tempPoly.ToArray());
            rotatedGeom.Rotate(rotation); 
            //Poly2D rotatedGeom = new Poly2D(GeoAlgorithms2D.RotatePoly(rotation, minBR[0], Points.ToList()).ToArray());
            Rect2D approxRect = GeoAlgorithms2D.BoundingBox(rotatedGeom.Points[0]);

            Vector2d diffProp = new Vector2d(0, 0);
            diffProp.X = (newProp.X - approxRect.Width)/2;
            diffProp.Y = (newProp.Y - approxRect.Height)/2;
            double dlength = diffProp.Length;
            if (diffProp.X + diffProp.Y < 0) dlength = dlength * -1;

            Vector2d middle = Center;
            for (int i = 0; i < _points.GetLength(0); i++)
            {
                for (int j = 0; j < _points[i].Length; j++)
                {
                    Vector2d curDir = _points[i][j] - middle;
                    curDir.Normalize();
                    Vector2d diff = new Vector2d(0, 0);
                    //diff.X = (diffProp.X) * Math.Sign(curDir.X);
                    //diff.Y = (diffProp.Y) * Math.Sign(curDir.Y);

                    diff = dlength * curDir;
                    _points[i][j] += diff;
                }
            }
        }

        //==============================================================================================
        /// <summary>
        /// Offset the polygon by a given factor.
        /// </summary>
        /// <param name="scaling">Offset factor.</param>
        public bool Offset(double scaling)
        {
            bool offseted = false;
            Vector2d[] offPoly = GeoAlgorithms2D.OffsetPoly(Points[0], scaling);
            if (offPoly != null)
            {
                Points[0] = new Vector2d[offPoly.Length];
                Points[0] = offPoly;
                offseted = true;
            }
            return offseted;
        }

        //==============================================================================================
        /// <summary>
        /// Reverse the polygon. From clockwise to counterclockwise and vice versa.
        /// </summary>
        public void Reverse()
        {
            int nrPts = _points.Length;
            Vector2d[] newPoints = new Vector2d[nrPts]; 
            for (int i = 0; i < nrPts; i++)
            {
                newPoints[nrPts - 1 - i] = _points[0][i];
            }
            for (int i = 0; i < nrPts; i++)
            {
                _points[0][i] = newPoints[i];
            }
        }

        //==============================================================================================
        /// <summary>
        /// Retrun the bounding box of the polygon.
        /// </summary>
        /// <returns>Rect2D bounding rectangle.</returns>
        public Rect2D BoundingBox()
        {
            return GeoAlgorithms2D.BoundingBox(_points[0]);
        }

        //==============================================================================================
        /// <summary>
        /// Retrun the minimum bounding rectangle of the polygon. 
        /// </summary>
        /// <returns> Bounding rectangle as Poly2D. </returns>
        public Poly2D MinimalBoundingRectangle()
        {
            return new Poly2D( GeoAlgorithms2D.MinimalBoundingRectangle(Points[0].ToList()));
        }



        //==============================================================================================
        // !!! is's slower than the one below from GeoAlgorithms2D !!! but maybe for some special purpose???
        //private Vector2d GetCentroid()
        //{
        //    Vector2d centroid = new Vector2d();
        //    Triangle3D[] triangles = GeoAlgorithms2D.Triangulate2D(this);
        //    double area = Area;
        //    for (int i = 0; i < triangles.Length; i++)
        //    {
        //        if (!Double.IsNaN(triangles[i].GetArea())) //falls Dreieck nur eine Linie ist
        //        {
        //            centroid.X += triangles[i].GetCentroid2D().X * (triangles[i].GetArea() / area);
        //            centroid.Y += triangles[i].GetCentroid2D().Y * (triangles[i].GetArea() / area);
        //        }
        //    }
        //    return centroid;
        //}

        /// <summary>
        /// Compares the polygon to another and checks if they are similar
        /// </summary>
        /// <param name="poly">The Polygon to compare with</param>
        public bool IsSimilarTo(Poly2D poly, double eps)
        {
            if (this.PointsFirstLoop.Length != poly.PointsFirstLoop.Length)
                return false;

            List<Vector2d> compareList = new List<Vector2d>();
            foreach (Vector2d vec in poly.PointsFirstLoop)
                compareList.Add(vec);

            foreach (Vector2d vec in PointsFirstLoop)
            {
                foreach (Vector2d vec2 in compareList)
                    if (vec.IsSimilarTo(vec2, eps))
                    {
                        compareList.Remove(vec2);
                        break;
                    }
            }

            if (compareList.Count == 0)
                return true;
            else
                return false;
        }


        # endregion
    }
}

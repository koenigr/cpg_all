﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Poly2DExtruded.cs 
//  Copyright (C) 2014/10/18  12:58 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK;

namespace CPlan.Geometry
{
    [Serializable]
    public class Poly2DExtruded
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Fields
        

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        /// <summary>
        /// Base polygon.
        /// </summary>
        public Poly2D BasePoly { get; protected set; }
        public Poly3D BasePoly3D { get { return new Poly3D(BasePoly, 0); } }

        public Vector2d[] BasePolyPoints { get { return BasePoly.PointsFirstLoop; } }
        public Vector3d[] BasePolyPoints3D { get { return BasePoly3D.PointsFirstLoop; } }

        /// <summary>
        /// The 6 surface polygons of the cube.
        /// *** need to generalized for multi side polygons *** !!!!!!!!!!
        /// </summary>
        public Poly3D[] Polygons
        {
            get
            {
                Poly3D[] polygons = new Poly3D[6];
                polygons[0] = BasePoly3D;

                Poly3D topP = new Poly3D(BasePoly, Height);
                //Vector3d delta3D = new Vector3d(0, 0, Height);
                //for (int i = 0; i < topP.PointsFirstLoop.Count(); i++)
                //{
                //    topP.PointsFirstLoop[i] += delta3D;
                //}
                polygons[1] = new Poly3D(topP);

                Poly3D side = new Poly3D(BasePoly3D);
                side.PointsFirstLoop[0] = BasePolyPoints3D[0];
                side.PointsFirstLoop[1] = BasePolyPoints3D[1];
                side.PointsFirstLoop[2] = topP.PointsFirstLoop[1];
                side.PointsFirstLoop[3] = topP.PointsFirstLoop[0];
                polygons[2] = new Poly3D(side);

                side = new Poly3D(BasePoly3D);
                side.PointsFirstLoop[0] = BasePolyPoints3D[1];
                side.PointsFirstLoop[1] = BasePolyPoints3D[2];
                side.PointsFirstLoop[2] = topP.PointsFirstLoop[2];
                side.PointsFirstLoop[3] = topP.PointsFirstLoop[1];
                polygons[3] = new Poly3D(side);

                side = new Poly3D(BasePoly3D);
                side.PointsFirstLoop[0] = BasePolyPoints3D[2];
                side.PointsFirstLoop[1] = BasePolyPoints3D[3];
                side.PointsFirstLoop[2] = topP.PointsFirstLoop[3];
                side.PointsFirstLoop[3] = topP.PointsFirstLoop[2];
                polygons[4] = new Poly3D(side);

                side = new Poly3D(BasePoly3D);
                side.PointsFirstLoop[0] = BasePolyPoints3D[3];
                side.PointsFirstLoop[1] = BasePolyPoints3D[0];
                side.PointsFirstLoop[2] = topP.PointsFirstLoop[0];
                side.PointsFirstLoop[3] = topP.PointsFirstLoop[3];
                polygons[5] = new Poly3D(side);

                return polygons;
            }
        }

        /// <summary>
        /// The height of the cube.
        /// </summary>
        public float Height { get; protected set; }

        /// <summary> List with the edges of the CubeExtruded. </summary>
        public Line3D[] Edges
        {
            get
            {
                Line3D[] edges = new Line3D[BasePoly.PointsFirstLoop.Count() * 3];
                // bottom Polygon
                for (int i = 0; i < BasePoly.PointsFirstLoop.Count(); i++)
                {
                    int k = i + 1;
                    if (k >= BasePoly.PointsFirstLoop.Count()) k = 0;
                    Vector3d p1 = BasePoly3D.PointsFirstLoop[i];
                    Vector3d p2 = BasePoly3D.PointsFirstLoop[k];

                    Vector3d p1_up = new Vector3d(p1.X, p1.Y, p1.Z + Height);
                    Vector3d p2_up = new Vector3d(p2.X, p2.Y, p2.Z + Height);

                    edges[i * 3] = new Line3D(p1, p2);
                    edges[i * 3 + 1] = new Line3D(p1_up, p2_up);
                    edges[i * 3 + 2] = new Line3D(p1, p1_up);
                }
                return edges;
            }
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructor

        /// <summary>
        /// Create an empty instance.
        /// </summary>
        public Poly2DExtruded() { }

        //============================================================================================================================================================================        
        /// <summary>
        /// Initialise a new instance of the <b>CubeExtruded</b> class in three-dimensional space.
        /// </summary>
        public Poly2DExtruded(Vector2d[] polygon, float height)
        {
            BasePoly = new Poly2D(polygon);
            Height = height;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>CubeExtruded</b> class in three-dimensional space.
        /// </summary>
        public Poly2DExtruded(List<Vector2d> polygon, float height)
        {
            BasePoly = new Poly2D(polygon.ToArray());
            Height = height;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        //============================================================================================================================================================================        
        public void Move(Vector2d delta)
        {
            //Vector3d delta3D = new Vector3d(delta.X, delta.Y, 0);
            for (int i = 0; i < BasePoly.PointsFirstLoop.Count(); i++)
            {
                BasePoly.PointsFirstLoop[i] += delta;
            }
        }

        /// <summary>
        /// Bounding box for the footprint polygon.
        /// </summary>
        /// <param name="box"></param>
        public void GetBoundingBox(BoundingBox box)
        {
            for (int i = 0; i < BasePoly.PointsFirstLoop.Count(); i++)
            {
                Vector3d curPt = BasePoly3D.PointsFirstLoop[i];
                Vector3d upPt = new Vector3d(curPt.X, curPt.Y, curPt.Z + Height);
                box.AddPoint(curPt);
                box.AddPoint(upPt);
            }
        }

        /// <summary>
        /// The footprint area.
        /// </summary>
        /// <returns></returns>
        public double GetArea()
        {
            return BasePoly.Area;
        }

        # endregion

    }
}

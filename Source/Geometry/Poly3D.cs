﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Poly3D.cs 
//  Copyright (C) 2014/10/18  12:58 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using OpenTK;
using System.Linq;
using System.Collections.Generic;

namespace CPlan.Geometry
{
    [Serializable]
    public class Poly3D
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Fields

        CGeom3d.Vec_3d[][] m_vPoints;

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        public double Area
        {
            get
            {
                Double area = new Double();
                GeoAlgorithms3D.AreaPoly(m_vPoints, ref area);
                return area;
            }
        }

        public Vector3d Normal
        {
            get
            {
                CGeom3d.Vec_3d normal = new CGeom3d.Vec_3d(0,0,0), center = new CGeom3d.Vec_3d(0,0,0);
                GeoAlgorithms3D.ComputePlane(m_vPoints, center, normal);
                normal.Normalize();
                return normal.ToVector3d();
            }
        }

        public Vector3d Center
        {
            get
            {
                CGeom3d.Vec_3d normal = new CGeom3d.Vec_3d(0, 0, 0), center = new CGeom3d.Vec_3d(0, 0, 0);
                GeoAlgorithms3D.ComputePlane(m_vPoints, center, normal);
                return center.ToVector3d();
            }
        }

        public Vector3d[][] Points
        {
            get { return m_vPoints.Select(l => l.ToVectors3d().ToArray()).ToArray(); }
            set { m_vPoints = value.Select(l => l.ToGeom3d().ToArray()).ToArray(); }
        }

        public Vector3d[] PointsFirstLoop {
            get { return m_vPoints[0].ToVectors3d().ToArray(); }
            set { m_vPoints[0] = value.ToGeom3d().ToArray(); }
        }

        public CGeom3d.Vec_3d[] PointsVec3D
        {
            get
            {
                CGeom3d.Vec_3d[] vec3DList = new CGeom3d.Vec_3d[m_vPoints[0].Length];
                for (int i = 0; i < m_vPoints[0].Length; i++)
                {
                    vec3DList[i] = new CGeom3d.Vec_3d(m_vPoints[0][i].x, m_vPoints[0][i].y, m_vPoints[0][i].z);
                }
                return vec3DList;
            }
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructor
        
        public Poly3D()
        {
        }

        public Poly3D(Vector2d[] vpoints, double z) {
            m_vPoints = new CGeom3d.Vec_3d[1][];
            m_vPoints[0] = new CGeom3d.Vec_3d[vpoints.Length];
            for (int i = 0; i < vpoints.Length; i++ )
            {
                m_vPoints[0][i] = new CGeom3d.Vec_3d(vpoints[i].X, vpoints[i].Y, z);
            }
        }

        public Poly3D(Vector3d[][] vPoints)
        {
            m_vPoints = vPoints.Select(l => l.ToGeom3d().ToArray()).ToArray();
        }

        public Poly3D(Vector3d[] vPoints)
        {
            m_vPoints = new CGeom3d.Vec_3d[1][];
            m_vPoints[0] = vPoints.ToGeom3d().ToArray();
        }

        public Poly3D(Poly2D poly2D, double z)
        {
            //poly2D.Points.GetLength(0);
            m_vPoints = new CGeom3d.Vec_3d[1][];
            m_vPoints[0] = new CGeom3d.Vec_3d[poly2D.Points.GetLength(0)];
            for (int i = 0; i < m_vPoints[0].Length; i++)           
            {
                Vector2d vect2D = poly2D.Points[0][i];
                m_vPoints[0][i] = new CGeom3d.Vec_3d(vect2D.X, vect2D.Y, z);
            }
        }

        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="previousPoly3D"></param>
        public Poly3D(Poly3D previousPoly3D)
        {
            m_vPoints = previousPoly3D.Points.Select(l => l.ToGeom3d().ToArray()).ToArray();
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        public bool InPoly3D(Vector3d testPoint, double delta)
        {
            return GeoAlgorithms3D.In_Poly_3d(m_vPoints, testPoint.ToGeom3d(), delta);
        }

        public void AnalysePolygon(ref CGeom3d.Vec_3d base_point, ref CGeom3d.Vec_3d vx, ref CGeom3d.Vec_3d vy, ref CGeom3d.Vec_3d normale, ref CGeom3d.Vec_3d size)
        {

            CGeom3d.Vec_3d start_point, temp_point, dist_point, erg_point, min_point, max_point, last_point, best_vx, best_vy, best_size, best_basepoint;
            best_basepoint = new CGeom3d.Vec_3d();
            best_vx = new CGeom3d.Vec_3d();
            best_vy = new CGeom3d.Vec_3d();
            best_size = new CGeom3d.Vec_3d();
            temp_point = new CGeom3d.Vec_3d();

            bool start = true;
            double area, best_area = -1;

            //vx bestimmen
            normale = Normal.ToGeom3d();
            foreach (CGeom3d.Vec_3d[] pLoop in m_vPoints)
            {
                foreach (CGeom3d.Vec_3d pPoint in pLoop)
                {
                    if (start)
                    {
                        start = false;
                        temp_point = new CGeom3d.Vec_3d(pPoint.x, pPoint.y, pPoint.z);
                        start_point = new CGeom3d.Vec_3d(pPoint.x, pPoint.y, pPoint.z);
                    }
                    else
                    {
                        last_point = temp_point;
                        temp_point = new CGeom3d.Vec_3d(pPoint.x, pPoint.y, pPoint.z);

                        vx = last_point - temp_point;
                        vx.Normalize();
                        if (vx.Value() > 0.9)
                        {
                            vy = normale.amul(vx);
                            vx = vy.amul(normale);
                            vx.Normalize();
                            vy.Normalize();

                            AnalysePolygonAxes(vx, vy, normale, ref base_point, ref size);

                            area = Math.Abs(size.x * size.y);
                            if ((area < best_area) || (best_area < 0))
                            {
                                best_vx = vx;
                                best_vy = vy;
                                best_area = area;
                                best_basepoint = base_point;
                                best_size = size;
                            }
                        }
                    }
                }
            }

            vx = best_vx;
            vy = best_vy;
            base_point = best_basepoint;
            size = best_size;
        }

        public void AnalysePolygonAxes(CGeom3d.Vec_3d vx, CGeom3d.Vec_3d vy, CGeom3d.Vec_3d normale, ref CGeom3d.Vec_3d base_point, ref CGeom3d.Vec_3d size)
        {
            bool start = true;
            CGeom3d.Vec_3d start_point, dist_point, erg_point, min_point, max_point;
            max_point = new CGeom3d.Vec_3d(0, 0, 0);
            min_point = new CGeom3d.Vec_3d(0, 0, 0);

            //alle Punkte durchgehen
            start_point = m_vPoints[0][0];
            foreach (CGeom3d.Vec_3d[] pLoop in m_vPoints)
            {
                foreach (CGeom3d.Vec_3d pPoint in pLoop)
                {
                    dist_point = pPoint;
                    dist_point = dist_point - start_point;

                    //Gleichungssystem lösen
                    erg_point = CGeom3d.Cramer(vx, vy, normale, dist_point);

                    if (start)
                    {
                        start = false;
                        min_point = new CGeom3d.Vec_3d(erg_point.x, erg_point.y, erg_point.z);
                        max_point = new CGeom3d.Vec_3d(erg_point.x, erg_point.y, erg_point.z);
                    }
                    else
                    {
                        if (erg_point.x < min_point.x) min_point.x = erg_point.x;
                        if (erg_point.y < min_point.y) min_point.y = erg_point.y;
                        if (erg_point.z < min_point.z) min_point.z = erg_point.z;
                        if (erg_point.x > max_point.x) max_point.x = erg_point.x;
                        if (erg_point.y > max_point.y) max_point.y = erg_point.y;
                        if (erg_point.z > max_point.z) max_point.z = erg_point.z;
                    }
                }
            }

            //Werte setzen
            base_point = start_point + ((vx) * min_point.x)
                                      + ((vy) * min_point.y)
                                      + ((normale) * min_point.z);
            size = max_point - min_point;
        }


        # endregion
    }
}

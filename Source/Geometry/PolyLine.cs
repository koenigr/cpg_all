﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using OpenTK;

//namespace CPlan.Geometry
//{
//    /// <summary>
//    /// Connected set of lines
//    /// </summary>
//    public class PolyLine : LineSet
//    {
//        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        # region Private Fields

//        private bool _closed;

//        # endregion

//        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        # region Properties

//        /// <summary>
//        /// Whether the last and the first points are connected or not
//        /// </summary>
//        public bool Closed { get { return _closed; } }

//        /// <summary>
//        /// Access to the points of the set.
//        /// Every change affects current segment and adjacent segments (the method does not add new points!)
//        /// </summary>
//        /// <param name="i">index of a point</param>
//        /// <returns></returns>
//        public override Tuple<Vector3d,Vector3d> this[int i]
//        {
//            get {
//                if (_closed && i == _points.Count && _points.Count > 0)
//                    return new Tuple<Vector3d,Vector3d>(_points[i-1], _points[0]);
//                else
//                    return new Tuple<Vector3d,Vector3d>(_points[i], _points[i+1]);
//            }
//            set {
//                if (_closed && i == _points.Count && _points.Count > 0)
//                {
//                    _points[i-1] = value.Item1;
//                    _points[0] = value.Item2;
//                }
//                else
//                {
//                    _points[_indexes[i].Item1] = value.Item1;
//                    _points[_indexes[i].Item2] = value.Item2;
//                }
//            }
//        }

//        /// <summary>
//        /// Number of line segments
//        /// </summary>
//        public override int Count
//        {
//            get { return _indexes.Count + (_closed && (_indexes.Count > 0) ? 1 : 0); }
//        }

//        # endregion

//        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        # region Constructors

//        /// <summary>
//        /// Creates an empty PolyLine
//        /// </summary>
//        /// <param name="closed"></param>
//        public PolyLine(bool closed = true)
//        {
//            _points = new PointSet();
//            _indexes = new List<Tuple<int, int>>();
//            _closed = closed;
//        }

//        /// <summary>
//        /// PolyLine from sequence of points
//        /// </summary>
//        /// <param name="points"></param>
//        /// <param name="closed"></param>
//        public PolyLine(IEnumerable<Vector3d> points, bool closed = true)
//        {
//            _points = new PointSet(points);
//            _indexes = Enumerable.Range(0, _points.Count - 2 ).Select(i => new Tuple<int, int>(i, i + 1)).ToList();
//            _closed = closed;
//        }

//        /// <summary>
//        /// PolyLine from sequence of points
//        /// </summary>
//        /// <param name="points"></param>
//        /// <param name="indexes">wether we should connect last and first points or not</param>
//        public PolyLine(PointSet points, bool closed = true)
//        {
//            _points = points;
//            _indexes = Enumerable.Range(0, _points.Count - 2).Select(i => new Tuple<int, int>(i, i + 1)).ToList();
//            _closed = closed;
//        }


//        # endregion

//        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        # region Manipulation

//        /// <summary>
//        /// Adds a line segment to the set
//        /// </summary>
//        /// <param name="line"></param>
//        public virtual void Add(Vector3d startPoint, Vector3d endPoint)
//        {
//            var ps = _points.AsList;
//            ps.Add(startPoint);
//            ps.Add(endPoint);
//            _indexes.Add(new Tuple<int, int>(ps.Count - 2, ps.Count - 1));
//        }

//        /// <summary>
//        /// Adds a line segment to the set as connection of two points that are already in a line
//        /// </summary>
//        /// <param name="indexes"></param>
//        public virtual void Add(int startIndex, int endIndex)
//        {
//            _indexes.Add(new Tuple<int,int>(startIndex, endIndex));
//        }

//        /// <summary>
//        /// Adds a line segment to the set, starting from existing point, ending with new point
//        /// </summary>
//        /// <param name="startIndex">index of the point in the PointSet of this LineSet</param>
//        /// <param name="endPoint"></param>
//        public virtual void Add(int startIndex, Vector3d endPoint)
//        {
//            var ps = _points.AsList;
//            ps.Add(endPoint);
//            _indexes.Add(new Tuple<int, int>(startIndex, ps.Count - 1 ));
//        }

//        # endregion

//        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        # region ICloneable implementation

//        public PolyLine Clone()
//        {
//            PolyLine ls = new PolyLine();
//            ls._points = _points.Clone();
//            ls._indexes = new List<Tuple<int, int>>(this._indexes);
//            return ls;
//        }

//        # endregion

//    }
//}

﻿ #region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Rect2D.cs 
//  Copyright (C) 2014/10/18  12:58 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Linq;
using System.Security.Policy;
using OpenTK;

namespace CPlan.Geometry
{

    /// <summary>
    /// Represents a 2D rectangle using double coordinates.
    /// </summary>
    [Serializable]
    public class Rect2D
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Fields

        private Vector2d _pos = new Vector2d();
        private Vector2d _size = new Vector2d();
        private bool _ifUpsideDown = false;
        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        /// <summary>
        /// The four edges of the rectangle.
        /// </summary>
        //public Line2D[] Edges
        //{
        //    get
        //    {
        //        return m_edges;
        //    }
        //}

        //==============================================================================================
        /// <summary>
        /// The corner points of the rectangle.
        /// </summary>
        public Vector2d[] Points
        {
            get
            {
                Vector2d[] points = new Vector2d[4];
                points[0] = TopLeft;
                points[1] = TopRight;
                points[2] = BottomRight;
                points[3] = BottomLeft;
                return points;
            }
        }

        //==============================================================================================
        /// <summary>
        /// The corner points of the rectangle in vec3d.
        /// </summary>
        public CGeom3d.Vec_3d[] PointsVec3D
        {
            get
            {
                CGeom3d.Vec_3d[] points = new CGeom3d.Vec_3d[4];
                points[0] = new CGeom3d.Vec_3d(Left, Top, 0);
                points[1] = new CGeom3d.Vec_3d(Right, Top, 0);
                points[2] = new CGeom3d.Vec_3d(Right, Bottom, 0);
                points[3] = new CGeom3d.Vec_3d(Left, Bottom, 0);
                return points;
            }
        }

        /// <summary> List with the 4 Lines of the rectangle. </summary>
        public Line2D[] Edges
        {
            get
            {
                Line2D[] edges = new Line2D[4];
                edges[0] = new Line2D(BottomLeft, TopLeft);
                edges[1] = new Line2D(TopLeft, TopRight);
                edges[2] = new Line2D(TopRight, BottomRight);
                edges[3] = new Line2D(BottomRight, BottomLeft);
                return edges;
            }
        }

        //==============================================================================================
        /// <summary>
        /// The y-coordinate ot the rectangles top edge.
        /// </summary>
        public double Top
        {

            get
            {
                if (this._ifUpsideDown)
                {
                    return _pos.Y - Height;
                } 
                return _pos.Y;
            }
            set
            {
                _pos.Y = value;
            }
        }

        //==============================================================================================
        /// <summary>
        /// The x-coordinate ot the rectangles left edge.
        /// </summary>
        public double Left
        {
            get { return _pos.X; }
            set
            {
                _pos.X = value;
            }
        }

        //==============================================================================================
        /// <summary>
        /// The x-coordinate ot the rectangles right edge.
        /// </summary>
        public double Right
        {
            get { return _pos.X + _size.X; }
        }

        //==============================================================================================
        /// <summary>
        /// The y-coordinate ot the rectangles bottom edge.
        /// </summary>
        public double Bottom
        {

            get
            {
                if (this._ifUpsideDown)
                {
                    return _pos.Y ;
                }
                else
                
                    return _pos.Y - _size.Y;
               
                
            }
        }

        //==============================================================================================
        /// <summary>
        /// The width of the rectangle.
        /// </summary>
        public double Width
        {
            get { return _size.X; }
            set
            {
                _size.X = value;
            }
        }

        //==============================================================================================
        /// <summary>
        /// The height of the rectangle.
        /// </summary>
        public double Height
        {
            get { return _size.Y; }
            set
            {
                _size.Y = value;
            }
        }

        //==============================================================================================
        /// <summary>
        /// The area of the rectangle.
        /// </summary>
        public double Area
        {
            get { return _size.X * _size.Y; }
        }

        //==============================================================================================
        /// <summary>
        /// Position Vector
        /// </summary>
        public Vector2d Position
        {
            get
            {
                return _pos;
            }
            set
            {
                _pos = value;
            }

        }

        //==============================================================================================
        /// <summary>
        /// The X coordinate of the rectangle. 
        /// The same as Position.X or Left.
        /// </summary>
        public double X
        {
            get
            {

                return _pos.X;
            }
            set { _pos.X = value; }
        }

        //==============================================================================================
        /// <summary>
        /// The Y coordinate of the rectangle. 
        /// The same as Position.Y or Top.
        /// </summary>
        public double Y
        {
            get
            {
                if (_ifUpsideDown)
                {
                    return _pos.Y - Height;
                }
                return _pos.Y;
            }
            set { _pos.Y = value;
            }
        }

        //==============================================================================================
        /// <summary>
        /// Size Vector
        /// </summary>
        public Vector2d Size
        {
            get
            {
                return _size;
            }
            set
            {
                _size = value;
            }

        }

        //==============================================================================================
        /// <summary>
        /// Top left.
        /// </summary>
        public Vector2d TopLeft
        {
            get
            {
                return new Vector2d(Left,Top);
            }

        }

        //==============================================================================================
        /// <summary>
        /// Top right.
        /// </summary>
        public Vector2d TopRight
        {
            get
            {
                return new Vector2d(Right, Top);
            }

        }

        //==============================================================================================
        /// <summary>
        /// Bottom right.
        /// </summary>
        public Vector2d BottomRight
        {
            get
            {
                return new Vector2d(Right, Bottom);
            }
        }

        //==============================================================================================
        /// <summary>
        /// Bottom left.
        /// </summary>
        public Vector2d BottomLeft
        {
            get
            {
                return new Vector2d(Left,Bottom);
            }
        }

        //==============================================================================================
        /// <summary>
        /// The middle point of the rectangle.
        /// </summary>
        /// <returns>The rectangles center point.</returns>
        public Vector2d Center
        {
            get
            {
                return GeoAlgorithms2D.Centroid(Points.ToList());
            }
            set
            {
                _pos.X = value.X - (_size.X / 2);
                _pos.Y = value.Y + (_size.Y / 2);
            }
        }




        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        //==============================================================================================
        /// <summary>
        /// Initializes a new empty instance of the Geo2DRect class.
        /// </summary>
        public Rect2D() { }

        //==============================================================================================
        /// <summary>
        /// Initializes a new instance of the Geo2DRect class.
        /// </summary>
        /// <param name="x">The x-coordinate of the top left corner.</param>
        /// <param name="y">The y-coordinate of the top left corner.</param>
        /// <param name="width">The width of the rectangle.</param>
        /// <param name="height">The height of the rectangle.</param>
        public Rect2D(double x, double y, double width, double height)
        {
            _pos = new Vector2d(x, y);
            _size = new Vector2d(width, height);

            // --- check rectangle -> reverse it if necessary ---
            ValidatePolygon();
        }

        public Rect2D(double x, double y, double width, double height, bool ifUpsideDown)
        {

            _pos = new Vector2d(x, y);
            _size = new Vector2d(width, height);
            this._ifUpsideDown = ifUpsideDown;
            // --- check rectangle -> reverse it if necessary ---
            ValidatePolygon();
        }
        //==============================================================================================
        /// <summary>
        /// Initializes a new instance of the Geo2DRect class.
        /// </summary>
        /// <param name="pt1">Top left corner of the rectangle. </param>
        /// <param name="pt2">Bottom right corner of the rectangle. </param>
        public Rect2D(Vector2d pt1, Vector2d pt2)
        {
            _pos = pt1;
            Width = Math.Abs(pt2.X - pt1.X);
            Height = Math.Abs(pt2.Y - pt1.Y);

            // --- check rectangle -> reverse it if necessary ---
            ValidatePolygon();
        }

        //==============================================================================================
        /// <summary>
        /// Initializes a new instance of the Geo2DRect class.
        /// </summary>
        public Rect2D(Rect2D source)
        {
            _pos = new Vector2d(source.Position.X, source.Position.Y);
            _size = new Vector2d(source.Size.X, source.Size.Y);

            // --- check rectangle -> reverse it if necessary ---
            ValidatePolygon();
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        //==============================================================================================
        /// <summary>
        /// Clone the rectangle.
        /// </summary>
        /// <returns>Returns exact clone of the rectangle.</returns>
        public Rect2D Clone()
        {
            Rect2D clone = new Rect2D(this);
            return clone;
        }

        //==============================================================================================
        /// <summary>
        /// Create a new rectangle geometry
        /// </summary>
        /// <param name="pt1">Top left corner of the rectangle. </param>
        /// <param name="pt2">Bottom right corner of the rectangle. </param>
        public void CreateNew(Vector2d pt1, Vector2d pt2)
        {
            _pos = pt1;
            Width = Math.Abs(pt2.X - pt1.X);
            Height = Math.Abs(pt2.Y - pt1.Y);

            // --- check rectangle -> reverse it if necessary ---
            ValidatePolygon();
        }

        //==============================================================================================
        /// <summary>
        /// Create a new rectangle geometry
        /// </summary>
        public void CreateNew(Rect2D source)
        {
            _pos = source.Position;
            _size = source.Size;

            // --- check rectangle -> reverse it if necessary ---
            ValidatePolygon();
        }

        //==============================================================================================
        /// <summary>
        /// Moves the rectangle.
        /// </summary>
        /// <param name="delta">The vector that indicates distance and direction of the movement.</param>
        public void Move(Vector2d delta)
        {
            Position += delta;
        }

        //==============================================================================================
        /// <summary>
        /// Check rectangles orientation and reverse it if necessary.
        /// Afterwards it is a valid polygon.
        /// </summary>
        public void ValidatePolygon()
        {
            if (Left > Right)
            {
                Left = Right;
                Width *= -1;
            }
            //if (Bottom > Top)
            //{
            //    Top = Bottom;
            //    Height *= -1;
            //}
        }

        //==============================================================================================
        /// <summary>
        /// Returns the distance from this to the other rect. 0 if there is an overlap.
        /// </summary>
        /// <param name="other">Other rectangle.</param> 
        public double Distance(Rect2D other)
        {
            return GeoAlgorithms2D.Distance(this, other);
        }

        //==============================================================================================
        /// <summary>
        /// Checks if pos and size of this and another Rect2D are equal
        /// </summary>
        /// <param name="other">Other rectangle.</param> 
        public bool IsIdenticalTo(Rect2D other)
        {
            if ((_pos.X == other.Position.X) && (_pos.Y == other.Position.Y) && (_size.X == other.Size.X) && (_size.Y == other.Size.Y))
                return true;
            else
                return false;
        }

        //==============================================================================================
        /// <summary>
        /// True if there is an overlap.
        /// </summary>
        /// <param name="other">Other rectangle.</param> 
        public bool Overlaps(Rect2D other)
        {
            return GeoAlgorithms2D.Overlaps(this, other);
        }

        //==============================================================================================
        /// <summary>
        /// True if there is an overlap, returns the overlap.
        /// </summary>
        /// <param name="other">Rectangle.</param> 
        /// <param name="overlap">Output. The overlap.</param> 
        public bool Overlaps(Rect2D other, Rect2D overlap)
        {
            return GeoAlgorithms2D.Overlaps(this, other, overlap);
        }

        //==============================================================================================
        /// <summary>
        /// True if there is an overlap, returns the overlap.
        /// </summary>
        /// <param name="other">The other rectangle.</param> 
        public Rect2D IntersectionRect(Rect2D other)
        {
            Rect2D overlap = new Rect2D();
            if (GeoAlgorithms2D.Overlaps(this, other, overlap))
                return overlap;
            else
                return null;
        }

        /// <summary>
        /// Cuts an Rectangle from this (Works just, if one Rectangle remains, and if result is convex!)
        /// </summary>
        /// <param name="other">Rectangle.</param>
        public Rect2D Cut(Rect2D other)
        {
            Rect2D newRect = new Rect2D(this);

            if ((other.Left <= Left) && (other.Right >= Right))
            {
                if (other.Top >= Top)
                {
                    newRect._size.Y = Math.Abs(Bottom - other.Bottom);
                    newRect._pos.Y = other.Bottom;
                }
                else
                {
                    newRect._size.Y = Math.Abs(Top - other.Top);
                }
            }

            if ((other.Top >= Top) && (other.Bottom <= Bottom))
            {
                if (other.Left <= Left)
                {
                    newRect._size.X = Math.Abs(Right - other.Right);
                    newRect._pos.X = other.Right;
                }
                else
                {
                    newRect._size.X = Math.Abs(other.Left - Left);
                }
            }

            return newRect;
        }

        //==============================================================================================
        /// <summary>
        /// Assignment.
        /// </summary>
        /// <param name="ptTopLeft">The top left point.</param>  
        /// <param name="ptBottomRight">The bottom right point.</param>  
        public void Set(Vector2d ptTopLeft, Vector2d ptBottomRight)
        {
            _pos.X = ptTopLeft.X;
            _pos.Y = ptTopLeft.Y;

            _size.X = ptBottomRight.X - ptTopLeft.X;
    
            
                _size.Y = Math.Abs(ptBottomRight.Y - ptTopLeft.Y);
           
        }

        //==============================================================================================
        /// <summary>
        /// If the area is positive e.g. the top is greater than the bottom.
        /// </summary>
        public bool IsValid()
        {
            if (_ifUpsideDown)
            {
                return ((TopLeft.X < BottomRight.X) && (TopLeft.Y > BottomRight.Y));
            }
            else
            {
                return ((TopLeft.X < BottomRight.X) && (TopLeft.Y < BottomRight.Y));
            }
        }

        //==============================================================================================
        /// <summary>
        /// Convert the Rect2D to Poly2D. 
        /// </summary>
        /// <returns></returns>
        public Poly2D ToPoly2D()
        {
            Poly2D poly2D = new Poly2D(Points);
            return poly2D;
        }

        //==============================================================================================
        /// <summary>
        /// Create a new rectangle with offset.
        /// </summary>
        /// <param name="centroid">Centroid for the offset. </param>
        /// <param name="targetArea"></param>
        /// <returns></returns>
        public Rect2D WeightedOffset(Vector2d centroid, double targetArea)
        {
            double weightV = (centroid.Y - Top) / Height;
            double weightH = (centroid.X - Left) / Width;
            double offsetFactor = Math.Sqrt(targetArea / Area);
            double newX = Left + Width*( weightH * (1-offsetFactor));
            double newY = Top + Height*( weightV * (1-offsetFactor));
            double newW = Width * offsetFactor;
            double newH = Height * offsetFactor;
            Rect2D rect = new Rect2D(newX, newY, newW, newH);
            return rect;
        }

        //==============================================================================================
        /// <summary>
        /// Offsets the rectangle.
        /// </summary>
        /// <param name="offset">Distance for the offset. (a positive offset makes the Rect bigger, negative offset makes it smaller) </param>
        public void Offset(double offset)
        {
            _size.X += offset * 2;
            _size.Y += offset * 2;
            _pos.X -= offset;

                _pos.Y += offset;
           
        }

        //==============================================================================================
        /// <summary>
        /// Test if a point is inside the rechtangle.
        /// </summary>
        /// <param name="point">Point to test if it is inside.</param>
        /// <returns>True if the point is inside the rectangle, false otherwise.</returns>
        public bool ContainsPoint(Vector2d point)
        {
            bool isInside = false; // true;
            if(GeoAlgorithms2D.PointInRect(point, this)) isInside = true;
            //if (GeoAlgorithms2D.PointInPolygon(point, Points.ToList()) < 0) isInside = false;
            return isInside;
        }


        public bool IntersectsWith(Line2D line)
        {

           // double x0 = line.Start.X, y0 = line.Start.Y;
            //double dx = line.End.X - x0, dy = line.End.Y - y0;
           // double t0 = 0, t1 = 1, p = 0, q = 0;

            
            int intersect_num = 0;
            for (int i = 0; i < 4; i++)
            {
                
                Line2D edge = this.Edges[i];
                if (edge.Intersect(line))
                {
                    
                    intersect_num ++;
                }
            }

            if (intersect_num == 0)
            {
                return false;
            }
            return true;
            // traverse all four rectangle borders
            //for (int border = 0; border < 4; border++)
            //{
            //    switch (border)
            //    {
            //        case 0: p = -dx; q = x0 - X; break;
            //        case 1: p = +dx; q = X + Width - x0; break;
            //        case 2: p = -dy; q = y0 - Y; break;
            //        case 3: p = +dy; q = Height + Y - y0; break;
            //    }


            //    if (p == 0)
            //    {
            //        if (q < 0)
            //        {
            //            return false;
            //        }
            //    }
            //    else
            //    {
            //        //p < 0 || p > 0
            //        double r = q/p;
            //        if (p < 0)
            //        {
            //            if (r > 1)
            //            {
            //                //q < 0; abs(q) > abs(p); q < p 
            //                return false;
            //            }
            //            else if(r > 0 && (r <= 1))
            //            {
            //                //q < 0; abs(q) <= abs(p)
            //            }
            //        }
            //        else
            //        {
            //            //p > 0
            //        }
            //    }
            //    //if (p == 0)
            //    //{
            //    //    // parallel line outside of rectangle
            //    //    if (q < 0) return false;
            //    //}
            //    //else {
            //    //    double r = q / p;
            //    //    if (p < 0)
            //    //    {
            //    //        if (r > t1) return false;
            //    //        else if (r > t0) t0 = r;
            //    //    }
            //    //    else {
            //    //        if (r < t0) return false;
            //    //        else if (r < t1) t1 = r;
            //    //    }
            //    //}
            //}

            //return true;
        }

        //public bool IntersectsWith(Line2D line)
        //{

        //    double x0 = line.Start.X, y0 = line.Start.Y;
        //    double dx = line.End.X - x0, dy = line.End.Y - y0;
        //    double t0 = 0, t1 = 1, p = 0, q = 0;


        //    // traverse all four rectangle borders
        //    for (int border = 0; border < 4; border++)
        //    {
        //        if (this._ifUpsideDown)
        //        {
        //            switch (border)
        //            {
        //                case 0:
        //                    p = -dx;
        //                    q = x0 - X;
        //                    break;
        //                case 1:
        //                    p = +dx;
        //                    q = X + Width - x0;
        //                    break;
        //                case 2:
        //                    p = -dy;
        //                    q = -y0 + Y;
        //                    break;
        //                case 3:
        //                    p = +dy;
        //                    q =   y0-Y;
        //                    break;
        //            }
        //        }
        //        else
        //        {
        //            switch (border)
        //            {
        //                case 0:
        //                    p = -dx;
        //                    q = x0 - X;
        //                    break;
        //                case 1:
        //                    p = +dx;
        //                    q = X + Width - x0;
        //                    break;
        //                case 2:
        //                    p = -dy;
        //                    q = y0 - Y;
        //                    break;
        //                case 3:
        //                    p = +dy;
        //                    q = Height + Y - y0;
        //                    break;
        //            }
        //        }
        //        if (p == 0)
        //        {
        //            // parallel line outside of rectangle
        //            if (q < 0) return false;
        //        }
        //        else {
        //            double r = q / p;
        //            if (p < 0)
        //            {
        //                if (r > t1) return false;
        //                else if (r > t0) t0 = r;
        //            }
        //            else {
        //                if (r < t0) return false;
        //                else if (r < t1) t1 = r;
        //            }
        //        }
        //    }

        //    return true;
        //}
        #endregion

    }

}

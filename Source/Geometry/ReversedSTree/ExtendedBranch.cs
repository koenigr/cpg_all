﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenTK;
namespace CPlan.Geometry.ReversedSTree
{
    public class ExtendedBranch
    {
        #region ATTRIBUTES

        private double _minDepthParcel;
        private double _minWidthParcel;
      //  private double _segmentLength;
        private int _maxNumParcels = 50;
        List<Leaf> _parcels ;
        #endregion

        #region PROPERTIES
        public int EdgeIdx { get; set; }
        public int NumParcels { get; set; }


        #endregion

        #region CONSTRUCTOR


        public ExtendedBranch(int edgeIdx,  double minDepthParcel, double minWidthParcel)
        {
            Random randomParcelNum = new Random((int)DateTime.Now.Ticks);
            
            this.EdgeIdx = edgeIdx;
      
            this._minDepthParcel = minDepthParcel;
            this._minWidthParcel = minWidthParcel;
            this.NumParcels = randomParcelNum.Next(this._maxNumParcels);
            this.NumParcels = this._maxNumParcels;
            this._parcels = new List<Leaf>(this.NumParcels);
          
            initialize();
        }
        public ExtendedBranch(int edgeIdx, int numParcels, double minDepthParcel, double minWidthParcel)
        {
            this.EdgeIdx = edgeIdx;
      
            this._minDepthParcel = minDepthParcel;
            this._minWidthParcel = minWidthParcel;
           
            this.NumParcels = (this._maxNumParcels >= numParcels) ? numParcels : this._maxNumParcels;
            this._parcels = new List<Leaf>(this.NumParcels);
           
            initialize();
        }

        #endregion
        #region METHODS

        public void initialize()
        {
            //randomly initialize nodes and put the nodes on the branch

            Random randIdxGenerator = new Random((int)DateTime.Now.Ticks);

            // always generate polygon occupying at most 3 street segments
            //this number can be generated from certain distributions too
            //if the specified number of the parcels for the branch is larger
            //than 1, then it could not have connectivity of 3. Otherwise, it
            //could have connectivities of 1, 2, and 3.

            if (this.NumParcels > 1)
            {
                List<int> connectivity2StreetPool = new List<int>(this.NumParcels);
                int connectivity2Street = randIdxGenerator.Next(1, 2);
                for (int i = 0; i < this.NumParcels-1; i++)
                {
                    int numConnectivityOf2 = 0;
                    foreach (int connectivityNum in connectivity2StreetPool)
                    {
                        if (connectivityNum == 2)
                        {
                            numConnectivityOf2++;
                        }
                    }
                    if (numConnectivityOf2 == 2)
                    {
                        connectivity2Street = 1;
                    }

                    connectivity2StreetPool.Add( connectivity2Street);
                    this._parcels.Add( new Leaf(i , this._minDepthParcel, this._minWidthParcel, connectivity2Street, this.EdgeIdx));
                }
                if (this.NumParcels < this._maxNumParcels)
                {
                    this._parcels[this._parcels.Count - 1] = new Leaf(this._parcels.Count-1 , this._minDepthParcel, this._minWidthParcel, 1, this.EdgeIdx);
                }
                else
                {
                    this._parcels[this._parcels.Count - 1] = new Leaf(this._parcels.Count - 1, this._minDepthParcel, this._minWidthParcel, 2, this.EdgeIdx);
                }
            }
            else
            {
                int connectivity2Street = randIdxGenerator.Next(1, 3);
                this._parcels.Add( new Leaf(0 , this._minDepthParcel, this._minWidthParcel, connectivity2Street, this.EdgeIdx));
            }

            //sequetial process
            // place the parcels with connectivity of 2 on the corners
            int leftCornerParcelIdx = this._parcels.FindIndex(x => x.Connectivity2Street == 2);
            int rightCornerParcelIdx = leftCornerParcelIdx;
            if (leftCornerParcelIdx != this._parcels.Count - 1)
            {
                rightCornerParcelIdx = this._parcels.FindIndex(leftCornerParcelIdx + 1,
                     x => x.Connectivity2Street == 2);
            }

            if (leftCornerParcelIdx > 0)
            {
                Leaf parcelLeftCorner = this._parcels[leftCornerParcelIdx];
                this._parcels.RemoveAt(leftCornerParcelIdx);
                this._parcels.Insert(0, parcelLeftCorner);
            }
            if (rightCornerParcelIdx != leftCornerParcelIdx)
            {
                Leaf parcelRightCorner = this._parcels[rightCornerParcelIdx];
                this._parcels.RemoveAt(rightCornerParcelIdx);
                this._parcels.Insert(this._parcels.Count - 1, parcelRightCorner);
            }

            //correct parcel index
            for (int i = 0; i < this._parcels.Count; i++)
            {
                this._parcels[i].LeafIdx = i + this.EdgeIdx*10;
            }
            // find neighbors of each parcel
           
            for (int i = 0; i < this._parcels.Count ; i++)
            {
                if (i - 1 >= 0)
                {
                    this._parcels[i].LeftLeafIdx = this._parcels[i - 1].LeafIdx;
                }
                if (i + 1 < this._parcels.Count)
                {
                    this._parcels[i].RightLeafIdx = this._parcels[i + 1].LeafIdx;
                }
            }
            
            //cluster the nodes on the same edge

            //group the neighboring two leaves

            /* restrict the width and length;
            those that are collinear to the street segments
            should always be the width (width > length) of
            the slicing rectangle  
            */

        }

        public ReversedTree Grow2TreeBranch()
        {
            //ceiling (smallest integer greater or equal to the floating-point number) of
            //the total number of the room for tree depth
            //we use complete tree here but the advantages need to be investigated
            ReversedTree rSTree ;

            double depth =Math.Ceiling(Math.Log((double) this.NumParcels, 2.0));
            int deepestPacerlNum =this.NumParcels- (int) (Math.Pow(2, depth) - (double) this.NumParcels);
            if (deepestPacerlNum > 0)
            {
                for (int i = 0; i < this._parcels.Count; ++i)
                {
                    if (i < deepestPacerlNum)
                    {
                        this._parcels[i].TreeDepth = (int) depth;
                    }
                    else
                    {

                        this._parcels[i].TreeDepth = (int) depth - 1;
                    }
                }
            }
            else
            {
                for (int i = 0; i < this._parcels.Count; ++i)
                {

                    this._parcels[i].TreeDepth = (int) depth;
                }
            }

            rSTree = new ReversedTree(this._parcels);

                return rSTree;
            
        }




        #endregion
    }
}


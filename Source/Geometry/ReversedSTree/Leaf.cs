﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CPlan.Geometry.ReversedSTree
{
   public class Leaf
    {
        private int i;
        private double _minDepth;
        private double _minWidth;


        #region PROPERTIES

        public  int LeafIdx { get; set; }
        //minimum length of the parcel (length > width)
        public double MinDepth { get; set; }
        //minimum width of the parcel (length > width)
        public  double MinWidth { get; set; }
        // left is 0 and right is 1

        //connectivity of the parcel to the street; it has to be larger than 0
        public int Connectivity2Street { get; set; }

        public int EdgeIdx { get; set; }

        public int LeftLeafIdx { get; set; }

        public int RightLeafIdx { get; set; }

        public int TreeDepth { get; set; }

        public  Node Parent { get; set; }

        public Polygon Region { get; set; }

        #endregion

        #region CONSTRUCTORS

        public Leaf(int leafIdx, int connectivity2Street, int edgeIdx)
        {
            this.LeafIdx = leafIdx;
            this.MinDepth = 30;
            this.MinWidth = 20;
  
            this.Connectivity2Street = connectivity2Street;
            this.EdgeIdx = edgeIdx;
            this.LeftLeafIdx = -1;
            this.RightLeafIdx = -1;
            this.TreeDepth = -1;
            if (ValidateProperties() == 0)
            {
                throw new Exception("leaf properties not correctly specified");
            }

        }
        public Leaf(int leafIdx, double minDepth, double minWidth, int connectivity2Street, int edgeIdx)
        {
            this.LeafIdx = leafIdx;
            this.MinDepth = minDepth;
            this.MinWidth = minWidth;

            this.Connectivity2Street = connectivity2Street;
            this.EdgeIdx = edgeIdx;
            this.LeftLeafIdx = -1;
            this.RightLeafIdx = -1;
            this.TreeDepth = -1;
            //if (ValidateProperties() == 0)
            //{
            //    throw new Exception("leaf properties not correctly specified");
            //}
        }




        #endregion


        #region METHODS

        //if return 1, then succeeds; otherwise, fails
        private int ValidateProperties()
        {
            
            //some constraints go here
            bool checklength = this.MinDepth < this.MinWidth;
            bool checkIdx = this.LeafIdx >= 0;
            bool checkConnectivity2Street = this.Connectivity2Street > 0  ;
            //legnth should be larger than width
            if (checklength && checkIdx && checkConnectivity2Street)
            {
                return 1;
            }
            //default is a failure
            return 0;
        }
        

        #endregion

    }
}

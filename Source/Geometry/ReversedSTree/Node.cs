﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPlan.Geometry.ReversedSTree
{
  public class Node
    {

        #region PROPERTIES

        public int NodeIdx { set; get; }
        public List<Node> Children { set; get; }
        public Node LeftNode { set; get; }
        public Node RightNode { set; get; } 
        public Node Parent { set; get; }

        public List<Leaf> Leaves { set;get;}

        

        public  int Depth { set; get; }
        #endregion

        #region CONSTRUCTORS

        public Node(int nodeIdx, int depth)
        {
            this.NodeIdx = nodeIdx;
            this.Depth = depth;
           
        }

        public Node(int nodeIdx, int depth, int leftOrRight, List<Node> children, Node parent)
        {
            this.NodeIdx = nodeIdx;
            this.Depth = depth;
            this.Children = children;
            this.Parent = parent;
         
        }

        public Node(int nodeIdx, List<Node> children, Node parent,  List<Leaf> leaves )
        {
            this.NodeIdx = nodeIdx;
            this.Children = children;
            this.Parent = parent;
            this.Leaves = leaves;
        }

        #endregion

        #region METHODS


        #endregion
    }
}

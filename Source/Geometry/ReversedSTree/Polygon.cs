﻿// Polygon.cs
//----------------------------------
// Last modified on 04/17/2012
// by Katja Knecht
//
// Description:
// The Polygon class describes a two-dimensional polygon. A Polygon object represents e.g. regions in a sub division tree.
//
// Comments:
//
// To Dos:
// 

using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using CPlan.Geometry;
using OpenTK;
using NetTopologySuite.Geometries;
using GeoAPI.Geometries;
using ClipperLib;
using NetTopologySuite.Noding.Snapround;
using NetTopologySuite.IO;
using NetTopologySuite.Operation.Polygonize;
namespace CPlan.Geometry.ReversedSTree
{
    public class Polygon
    {
        #region PROPERTIES

        public static readonly Polygon Empty;

        /// <summary>
        /// Gets or sets the array of vertices of a Polygon object.
        /// </summary>
        /// <value>(Vector2[]) vertices.</value>
        public Vector2d[] Vertices
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the array of polygon edges of a Polygon object.
        /// </summary>
        /// <value>(LineF[]) edges.</value>
        public Line2D[] Edges
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the bounding rectangle of the Polygon.
        /// </summary>
        public Rect2D Bounds
        {
            get;
            set;
        }

        public Poly2D minBounds
        {
            get;
            private set;
        }

        public double rotatedAngle
        {
            get;
            set;
        }

        public List<int> edgesOnBoundaryIdx
        {
            get; set;
        }
  
        #endregion

        #region CONSTRUCTORS

        /// <summary>
        /// Initializes a new instance of the Polygon type using the specified vertex points.
        /// </summary>
        /// <param name="_vertices">(Vector2d[]) vertex points.</param>
        public Polygon(Vector2d[] _vertices)
        {
            this.Vertices = _vertices;
            this.Edges = GeoAlgorithms2D.ConnectPoints(true, _vertices);
            this.edgesOnBoundaryIdx = new List<int>();
            if (_vertices.Length != 0)
            {
                this.Bounds = GeoAlgorithms2D.BoundingBoxAdjusted(Vertices);
                List<Vector2d> vertsList = new List<Vector2d>(Vertices);
                Vector2d[] minBoundsVerts = GeoAlgorithms2D.MinimalBoundingRectangle(vertsList);
                this.minBounds = new Poly2D(minBoundsVerts);
                this.rotatedAngle = 0;
               
            }
        }

        #endregion

        #region METHODS

        /// <summary>
        /// Computes the area of the <b>Polygon</b>
        /// </summary>
        /// <returns>
        /// The area of the <b>Polygon</b>, as <see cref="double"/> value.
        /// </returns>
        public Double GetArea()
        {
            double area;

            area = Math.Abs(GeoAlgorithms2D.PolygonArea(this.Vertices));
            return area;
        }

        #region INTERSECTION AND SPLIT METHODS

        /// <summary>
        /// Finds the intersection line with a specified line.
        /// </summary>
        /// <param name="_line">(Line2D) line.</param>
        /// <returns>(Line2D) intersection.</returns>
        public Line2D IntersectsWith(Line2D _line)
        {
            Vector2d pt;
            Vector2d[] ipts = new Vector2d[2];
          
            int counter = 0;
            Line2DIntersection intersection = new Line2DIntersection();

            //add offsets on both end to guarantee the intersection
            double offset_length = 50;
            Vector2d end_start_vec = _line.DirectionVectorEndStart;
            Vector2d start_end_vec = _line.DirectionVectorStartEnd;
            Vector2d new_line_start = new Vector2d(_line.Start.X + end_start_vec.X*offset_length, _line.Start.Y + end_start_vec.Y * offset_length);
            Vector2d new_line_end = new Vector2d(_line.End.X + start_end_vec.X*offset_length, _line.End.Y + start_end_vec.Y* offset_length);
            _line = new Line2D(new_line_start, new_line_end);

            for (int i = 0; i < this.Edges.Length; i++)
            {
                try
                {
                    intersection = this.Edges[i].getLine2DIntersection(_line);
                }
                catch
                {
                    Console.WriteLine("Error in method IntersectsWith Polygon. Intersection could not be calculated.");
                }

                if (intersection.Exists)
                {
                    pt = intersection.Shared.Value;
                    ipts[counter] = pt;
                    counter++;
                }
                if (counter == 2) return new Line2D(ipts[0], ipts[1]);
            }
            return new Line2D(0, 0,0,0);
        }

        /// <summary>
        /// Calculates the line intersection between the Polygon and the given line and its parallel perpendicular to the specified edge and returns the intersecting polygon with this Node's region.
        /// </summary>
        /// <param name="_perpendicular">(Line2D) line perpendicular to specified edge for intersection.</param>
        /// <param name="_perpParallel">(Line2D) parallel to perpendicular line for intersection.</param>
        /// <param name="_edge">(Line2D) edge perpendicular to line.</param>
        /// <param name="_polygon">(Vector2d[]) intersecting polygon.</param>
        /// <returns>(Line2D) intersection line.</returns>
        public Line2D PerpendicularIntersection(Line2D _perpendicular, Line2D _perpParallel, Line2D _edge, out Vector2d[] _polygon)
        {
            Vector2d pt; // temporary point
            Vector2d[] ipts = new Vector2d[2]; // points for intersection line
            int counter = 0;

            Line2DIntersection intersection = new Line2DIntersection();
            Line2D prevEdge = new Line2D(), nextEdge = new Line2D();
            //inBetweenP: between prevEdge; inBetweenN: between nextEdge 
            List<Line2D> inBetweenP = new List<Line2D>(), inBetweenN = new List<Line2D>();
            List<Vector2d> tmpPointList = new List<Vector2d>();

            //swap lines if polygon direction is counter clockwise or offset of parallel line in other direction
            if (GeoAlgorithms2D.DistancePoints(_edge.getLine2DIntersection(_perpendicular).Shared.Value, _edge.End) > GeoAlgorithms2D.DistancePoints(_edge.getLine2DIntersection(_perpParallel).Shared.Value, _edge.End))
            {
                Line2D tmp = _perpendicular;
                _perpendicular = _perpParallel;
                _perpParallel = tmp;
            }

            intersection = _edge.getLine2DIntersection(_perpendicular);
            pt = intersection.Shared.Value;
            // perpendicular intersection with the _edge
            tmpPointList.Add(pt);

            //decide nextEdge and prevEdge
            for (int i = 0; i < this.Edges.Length; i++)
            {
                if (this.Edges[i].End == _edge.Start)
                {
                    prevEdge = this.Edges[i];
                }
                else if (this.Edges[i].Start == _edge.End)
                {
                    nextEdge = this.Edges[i];
                }
            }

            intersection = _edge.getLine2DIntersection(_perpParallel);

            if (intersection.First != Line2DLocation.Between && intersection.First != Line2DLocation.End && intersection.First != Line2DLocation.Start)
            {
                tmpPointList.Add(_edge.Start);

                if (prevEdge != null)
                {
                    // System.Windows.Forms.MessageBox.Show(prevEdge.Start.X + " " + prevEdge.Start.Y + " " + prevEdge.End.X +
                    //                                      " " + prevEdge.End.Y);
                    while (prevEdge.getLine2DIntersection(_perpParallel).First != Line2DLocation.Between && prevEdge.getLine2DIntersection(_perpParallel).First != Line2DLocation.End && prevEdge.getLine2DIntersection(_perpParallel).First != Line2DLocation.Start)
                    {
                        // double angle = GeoAlgorithms2D.AngleBetweenR( prevEdge.Vector,_edge.Vector );
                        if (inBetweenP.Count != 0 && inBetweenP[0] == prevEdge)
                        {
                            _polygon = new Vector2d[0];
                            return Line2D.Empty;
                        }
                        // System.Windows.Forms.MessageBox.Show("to add " + prevEdge.Start.X + " " + prevEdge.Start.Y + " " + prevEdge.End.X +
                        //                                  " " + prevEdge.End.Y);
                        inBetweenP.Add(prevEdge);
                        for (int i = 0; i < this.Edges.Length; i++)
                        {

                            if (this.Edges[i].End == inBetweenP[inBetweenP.Count - 1].Start)
                            {
                                tmpPointList.Add(prevEdge.Start);
                                prevEdge = this.Edges[i];
                                //System.Windows.Forms.MessageBox.Show("update " + prevEdge.Start.X +  " " + prevEdge.Start.Y +
                                //                                     " " + prevEdge.End.X +
                                //                                     " " + prevEdge.End.Y);
                                break;
                            }
                        }
                    }

                    intersection = prevEdge.getLine2DIntersection(_perpParallel);
                    if (intersection.Shared != null)
                    {
                        pt = intersection.Shared.Value;
                        ipts[counter] = pt;
                        counter++;

                        tmpPointList.Add(pt);
                    }
                    else
                    {
                        Console.WriteLine("no intersection with preEdge");
                    }
                }
                for (int i = 0; i < this.Edges.Length; i++)
                {
                    //check other edges for preEdge
                    if (this.Edges[i].End == prevEdge.Start)
                    {
                        prevEdge = this.Edges[i];
                    }
                }
            }
            else
            {
                pt = intersection.Shared.Value;
                tmpPointList.Add(pt);
            }

            if (prevEdge != null)
            {
                inBetweenP.Clear();
                while (prevEdge.getLine2DIntersection(_perpParallel).First != Line2DLocation.Between && prevEdge.getLine2DIntersection(_perpParallel).First != Line2DLocation.End && prevEdge.getLine2DIntersection(_perpParallel).First != Line2DLocation.Start)
                {
                    // double angle =GeoAlgorithms2D.AngleBetweenR( prevEdge.Vector,_edge.Vector);
                    inBetweenP.Add(prevEdge);
                    for (int i = 0; i < this.Edges.Length; i++)
                    {
                        if (this.Edges[i].End == inBetweenP[inBetweenP.Count - 1].Start)
                        {
                            prevEdge = this.Edges[i];
                            break;
                        }
                    }
                }

                intersection = prevEdge.getLine2DIntersection(_perpParallel);
                if (intersection.Shared != null)
                {
                    pt = intersection.Shared.Value;
                    ipts[counter] = pt;
                    counter++;

                    tmpPointList.Add(pt);
                }
                else
                {
                    Console.WriteLine("no intersection");
                }
            }

            if (nextEdge != null)
            {
                inBetweenN.Clear();
                inBetweenN.Add(nextEdge);
                intersection = nextEdge.getLine2DIntersection(_perpendicular);


                //    while (intersection.First != Line2DLocation.Between && intersection.First != Line2DLocation.Start && intersection.First != Line2DLocation.End && intersection.First != Line2DLocation.Before && intersection.First != Line2DLocation.After) { 
                //tolerating some mistakes when generating the polygons; not the optimal solution
                while (intersection.First != Line2DLocation.Between && intersection.First != Line2DLocation.Start && intersection.First != Line2DLocation.End && intersection.First != Line2DLocation.Before && intersection.First != Line2DLocation.After)
                {

                    for (int i = 0; i < this.Edges.Length; i++)
                    {
                        // Console.Write(this.Edges[i].Start.ToString() + " " + inBetweenN[inBetweenN.Count - 1].End.ToString());
                        if (this.Edges[i].Start == inBetweenN[inBetweenN.Count - 1].End)
                        {
                            nextEdge = this.Edges[i];
                            inBetweenN.Add(nextEdge);
                            break;
                        }


                    }
                    //if (count == this.Edges.Length)
                    //{
                    //    break;
                    //}

                    //Console.Write(" " + nextEdge.getLine2DIntersection(_perpendicular).First.ToString() + " " +
                    //               Line2DLocation.Between.ToString() + " " +
                    //               nextEdge.getLine2DIntersection(_perpendicular).First.ToString() + " " +
                    //               Line2DLocation.End.ToString() + " " +
                    //               nextEdge.getLine2DIntersection(_perpendicular).First.ToString() + " " +
                    //               Line2DLocation.Start.ToString() + " ");
                    //Console.WriteLine(" ;");
                    intersection = nextEdge.getLine2DIntersection(_perpendicular);
                }


                if (intersection.Shared != null)
                {
                    pt = intersection.Shared.Value;
                }
                else
                {
                    Console.WriteLine("no intersection");
                }
            }

            if (prevEdge != nextEdge)
            {
                inBetweenP.Clear();

                Line2D tmpEdge = prevEdge;

                while (tmpEdge != nextEdge)
                {
                    inBetweenP.Add(tmpEdge);
                    for (int i = 0; i < this.Edges.Length; i++)
                    {
                        if (this.Edges[i].End == inBetweenP[inBetweenP.Count - 1].Start)
                        {
                            tmpEdge = this.Edges[i];
                            break;
                        }
                    }
                }

                for (int i = 0; i < inBetweenP.Count; i++)
                {
                    if (inBetweenP[i].Length != 0.0d)
                    {
                        tmpPointList.Add(inBetweenP[i].Start);
                    }
                }
            }

            //tmpPointList.Add(pt);

            _polygon = tmpPointList.ToArray();

            return new Line2D(ipts[0], ipts[1]);
        }

        /// <summary>
        /// Calculates the line intersection between the Polygon and the given line using the specified edge parallel to the line and returns the intersection polygon.
        /// </summary>
        /// <param name="_line">(Line2D) line for intersection.</param>
        /// <param name="_parallelEdge">(Line2D) edge parallel to line.</param>
        /// <param name="_polygon">(Vector2d[]) intersection polygon.</param>
        /// <returns>(Line2D) intersection line.</returns>
        public Line2D ParallelIntersection(Line2D _line, Line2D _parallelEdge, out Vector2d[] _polygon)
        {
            Vector2d pt; // temporary point
            Vector2d[] ipts = new Vector2d[2]; // points for intersection line
            int counter = 0;

            Line2DIntersection intersection = new Line2DIntersection();
            Line2D prevEdge = new Line2D(), nextEdge = new Line2D();
            List<Line2D> inBetweenP = new List<Line2D>(), inBetweenN = new List<Line2D>();
            List<Vector2d> tmpPointList = new List<Vector2d>();

            for (int i = 0; i < this.Edges.Length; i++)
            {
                if (this.Edges[i].End == _parallelEdge.Start)
                {
                    prevEdge = this.Edges[i];
                }
                else if (this.Edges[i].Start == _parallelEdge.End)
                {
                    nextEdge = this.Edges[i];
                }
            }

            tmpPointList.Add(_parallelEdge.Start);
            tmpPointList.Add(_parallelEdge.End);

            if (prevEdge != null)
            {
                while (prevEdge.getLine2DIntersection(_line).First != Line2DLocation.Between && prevEdge.getLine2DIntersection(_line).First != Line2DLocation.End && prevEdge.getLine2DIntersection(_line).First != Line2DLocation.Start)
                {
                    double angle = GeoAlgorithms2D.AngleBetweenR(prevEdge.Vector, _parallelEdge.Vector);
                    if (inBetweenP.Count != 0 && inBetweenP[0] == prevEdge)
                    {
                        _polygon = new Vector2d[0];
                        return Line2D.Empty;
                    }
                    inBetweenP.Add(prevEdge);
                    for (int i = 0; i < this.Edges.Length; i++)
                    {
                        if (this.Edges[i].End == inBetweenP[inBetweenP.Count - 1].Start)
                        {
                            prevEdge = this.Edges[i];
                            break;
                        }
                    }
                }

                intersection = prevEdge.getLine2DIntersection(_line);
                if (intersection.Shared != null)
                {
                    pt = intersection.Shared.Value;
                    ipts[counter] = pt;
                    counter++;
                }
                else
                {
                    Console.WriteLine("no intersection");
                }
            }

            if (nextEdge != null)
            {
                while (nextEdge.getLine2DIntersection(_line).First != Line2DLocation.Between && nextEdge.getLine2DIntersection(_line).First != Line2DLocation.End && nextEdge.getLine2DIntersection(_line).First != Line2DLocation.Start)
                {
                    if (inBetweenN.Count != 0 && inBetweenN[0] == nextEdge)
                    {
                        _polygon = new Vector2d[0];
                        return Line2D.Empty;
                    }
                    inBetweenN.Add(nextEdge);
                    for (int i = 0; i < this.Edges.Length; i++)
                    {
                        if (this.Edges[i].Start == inBetweenN[inBetweenN.Count - 1].End)
                        {
                            nextEdge = this.Edges[i];
                            break;
                        }
                    }
                }

                intersection = nextEdge.getLine2DIntersection(_line);
                if (intersection.Shared != null)
                {
                    pt = intersection.Shared.Value;
                    ipts[counter] = pt;
                    counter++;
                }
                else
                {
                    Console.WriteLine("no intersection");
                }
            }

            for (int i = 0; i < inBetweenN.Count; i++)
            {
                if (inBetweenN[i].Length != 0.0d)
                {
                    tmpPointList.Add(inBetweenN[i].End);
                }
            }

            List<Vector2d> tmpList = new List<Vector2d> { ipts[0], ipts[1] };
            // Vector2d[] tmpArray = tmpList.ToArray();
            Vector2d curPoint = tmpPointList[tmpPointList.Count - 1];
            //int idx = GeoAlgorithms2D.NearestPoint( tmpArray, curPoint);

            double dist1 = GeoAlgorithms2D.DistancePoints(tmpList[0], curPoint);
            double dist2 = GeoAlgorithms2D.DistancePoints(tmpList[1], curPoint);
            double dist3 = GeoAlgorithms2D.DistancePoints(tmpList[0], tmpPointList[0]);
            double dist4 = GeoAlgorithms2D.DistancePoints(tmpList[1], tmpPointList[0]);
            if (dist1 < dist2)
            {
                tmpPointList.Add(tmpList[0]);
                tmpList.Remove(tmpList[0]);
            }
            else
            {
                tmpPointList.Add(tmpList[1]);
                tmpList.Remove(tmpList[1]);
            }
            // tmpPointList.Add(tmpList[idx]);

            tmpPointList.Add(tmpList[0]);

            for (int i = inBetweenP.Count - 1; i >= 0; i--)
            {
                if (inBetweenP[i].Length != 0.0d)
                {
                    tmpPointList.Add(inBetweenP[i].Start);
                }
            }

            //check intersection
            Line2D edge1 = new Line2D(tmpPointList[1], tmpPointList[2]);
            Line2D edge2 = new Line2D(tmpPointList[3], tmpPointList[0]);
            if (edge1.Intersect(edge2))
            {
                //change crossing polygon
                Vector2d tmp = tmpPointList[2];
                tmpPointList[2] = tmpPointList[3];
                tmpPointList[3] = tmp;
            }
            _polygon = tmpPointList.ToArray();

            return new Line2D(ipts[0], ipts[1]);
        }

        /// <summary>
        /// Finds the intersection line with a specified line and returns all sections of the polygon.
        /// </summary>
        /// <param name="_line">(Line2D) line.</param>
        /// <param name="_sections">(Line2D[]) sections of the polygon including intersecting line.</param>
        /// <returns>(Line2D) intersection.</returns>
        public Line2D IntersectsWith(Line2D _line, out Line2D[] _sections)
        {
            Vector2d pt;
           List<Vector2d> ipts = new List<Vector2d>()  ;
            double offset = 0.00001d;

            double diffY = _line.End.Y - _line.Start.Y ;
            double diffX = _line.End.X - _line.Start.X;

            double coefficient = (_line.Start.X < _line.End.X) ? 1 : -1;

            double start_x = _line.Start.X - coefficient* offset;         
            double end_x = _line.End.X + coefficient* offset;
           

            Line2D offsetedLine;
            if (diffY != 0 && (diffX != 0))
            {
                //offset the line a bit

               double start_y = _line.Start.Y - coefficient* (_line.End.Y - _line.Start.Y)/(_line.End.X - _line.Start.X)*offset;


               double end_y = _line.End.Y + coefficient* (_line.End.Y - _line.Start.Y)/(_line.End.X - _line.Start.X)*offset;
                offsetedLine = new Line2D(start_x, start_y, end_x, end_y);

            }
            else if (diffY == 0)
            {
                
                offsetedLine = new Line2D(start_x, _line.Start.Y, end_x, _line.End.Y);
            }
            else
            {
                if (_line.End.Y - _line.Start.Y > 0)
                {
                    offsetedLine = new Line2D(_line.Start.X, _line.Start.Y - offset, _line.End.X,
                        _line.End.Y + offset);
                }
                else
                {
                    offsetedLine = new Line2D(_line.Start.X, _line.Start.Y + offset, _line.End.X,
                        _line.End.Y - offset);
                }
            }

           
    
        

            Line2DIntersection intersection = new Line2DIntersection();
            List<Line2D> tmpList = new List<Line2D>();
            Line2D tmp;

            //test.sw = new StreamWriter(@"C:\\Users\\yufan\\Desktop\\work\\test.txt");
            //for (int i = 0; i < this.Edges.Length; i++)
            //{
            //   test.sw.WriteLine (this.Edges[i].Start.X + " " + this.Edges[i].Start.Y + " " + this.Edges[i].End.X + " " +
            //     this.Edges[i].End.Y);
            //}

            //test.sw.WriteLine(_line.Start.X +" "+ _line.Start.Y + " "+ _line.End.X + " "+ _line.End.Y);
            //test.sw.Close();

            
            if (_line.Length != 0 && this.Bounds.IntersectsWith(offsetedLine))
            {
                for (int i = 0; i < this.Edges.Length; i++)
                {
                    //try
                    //{
                        intersection = this.Edges[i].getLine2DIntersection(offsetedLine);

                    //}
                    //catch
                    //{
                    //    Console.WriteLine("Error in method IntersectsWith. Intersection could not be calculated.");
                    //}

                    if (intersection != null && intersection.Exists && (ipts.Count < 2))
                    {
                        pt = intersection.Shared.Value;
                        if (!ipts.Contains(pt))
                        {
                            ipts.Add(pt);

                            tmp = new Line2D(this.Edges[i].Start, pt);
                            if (tmp.Length != 0) tmpList.Add(new Line2D(this.Edges[i].Start, pt));
                            tmp = new Line2D(pt, this.Edges[i].End);
                            if (tmp.Length != 0) tmpList.Add(new Line2D(pt, this.Edges[i].End));
                        }
                    }
                    else
                    {
                        tmpList.Add(this.Edges[i]);
                    }
                }

              
                Line2D split = new Line2D(ipts[0], ipts[1]);
                if (split.Length != 0)
                {
                    tmpList.Add(split);
                  
                }
            
        
                _sections = new Line2D[tmpList.Count];

                for (int i = 0; i < _sections.Length; i++)
                {
                    _sections[i] = tmpList[i];
                    //test.sw.WriteLine(tmpList[i].Start.X + " " + tmpList[i].Start.Y + " " + tmpList[i].End.X + " " + tmpList[i].End.Y);
                }
                return split;
            }
            else
            {
                _sections = new Line2D[0];
                return Line2D.Empty;
            }
        }

        /// <summary>
        /// Calculates the split polygons using the specified lines array.
        /// </summary>
        /// <param name="_lines">(Line2D[]) lines.</param>
        /// <returns>(Polygon[]) split polygons.</returns>
        public Polygon[] CalculateSplitPolygons(Line2D[] _lines)
        {
          


            Subdivision subd = Subdivision.FromLines(_lines);

            Vector2d[][] tempPolygons = subd.ToPolygons();

            
            Polygon[] polygons = new Polygon[tempPolygons.GetLength(0)];



            try
            {
                polygons = this.RectifyPolygonSuccession(tempPolygons);
            }
            catch
            {

                for (int i = 0; i < tempPolygons.GetLength(0); i++)
                {
                    polygons[i] = new Polygon(tempPolygons[i]);
                }
            }

            for (int i = 0; i < polygons.Length; i++)
            {
                tempPolygons[i] = new Vector2d[polygons[i].Vertices.Length];
                for (int j = 0; j < polygons[i].Vertices.Length; j++)
                {
                    tempPolygons[i][j] = polygons[i].Vertices[j];
                }
            }

            return polygons;
        }

        /// <summary>
        /// Rectifies the succession of the polygons in the array. 0 - left or top polygon, 1 - right or bottom polygon.
        /// </summary>
        /// <param name="_polygons">(Vector2d[][]) polygons.</param>
        /// <returns>(Polygon[]) rectified polygons.</returns>
        private Polygon[] RectifyPolygonSuccession(Vector2d[][] _polygons)
        {
            Vector2d[] vertices = new Vector2d[2];
            Polygon[] temp = new Polygon[2];
          
            int count = 0;

            bool breakLoop = false;
            //look for the vertices of the dividing line
            for (int i = 0; i < _polygons.GetLength(0) - 1; i++)
            {
                for (int j = 0; j < _polygons[i].Length; j++)
                {
                    for (int k = 0; k < _polygons[i + 1].Length; k++)
                    {
                      
                        if (_polygons[i][j] == _polygons[i + 1][k])
                        {
                            
                            vertices[count] = _polygons[i][j];
                            
                            count++;
                        }
                        if (count > 1 && (vertices[count-1] != vertices[count-2]))
                        {
                            breakLoop = true;
                            break;
                        }
                       
                        if(count > 1 && (vertices[count-1] == vertices[count - 2]))
                        {
                            count --;
                        }

                    }
                     if (breakLoop) break;
                }
                if (breakLoop) break;
            }
            Line2D intersection = new Line2D(vertices[0], vertices[1]);
        //     test.sw = new StreamWriter(@"C:\\Users\\yufan\\Desktop\\work\\test.txt");
       //     test.sw.WriteLine(vertices[0].X + " " + vertices[0].Y + " " + vertices[1].X + " " + vertices[1].Y);

           
            //check the number of nodes within the line
            int n = 0;
            while (intersection.Locate(_polygons[0][n]) == Line2DLocation.Start || intersection.Locate(_polygons[0][n]) == Line2DLocation.End)
            {
                n++;
            }
         //   test.sw.WriteLine(_polygons[0][n].X + " " + _polygons[0][n].Y);
         //   test.sw.Close();
          //  System.Environment.Exit(1);
            double min = -Math.PI * 3 / 4;
            double max = Math.PI / 4;

            // n is either start or end
            if (_polygons.GetLength(0) > 1)
            {
                if (intersection.Angle > min && intersection.Angle < max)
                {
                    if (intersection.Locate(_polygons[0][n]) == Line2DLocation.Left)
                    {
                        temp[0] = new Polygon(_polygons[1]);
                        temp[1] = new Polygon(_polygons[0]);
                    }
                    else
                    {
                        temp[0] = new Polygon(_polygons[0]);
                        temp[1] = new Polygon(_polygons[1]);
                    }
                }
                else
                {
                    if (intersection.Locate(_polygons[0][n]) == Line2DLocation.Right)
                    {
                        temp[0] = new Polygon(_polygons[1]);
                        temp[1] = new Polygon(_polygons[0]);
                    }
                    else
                    {
                        temp[0] = new Polygon(_polygons[0]);
                        temp[1] = new Polygon(_polygons[1]);
                    }
                }
            }
            else temp = new Polygon[1] { new Polygon(_polygons[0]) };
            return temp;
        }

        #endregion
        /// <summary>
        /// Moves a Polygon object to and parallel a specified line.
        /// </summary>
        /// <param name="_line">(Line2D) line.</param>
        public void MoveTo(Line2D _line)
        {
            Vector2d midptL = CalculateMidPoint(_line);
            List<Vector2d> midpts = new List<Vector2d>();

            for (int i = 0; i < this.Edges.Length; i++)
            {
                midpts.Add(CalculateMidPoint(this.Edges[i]));
            }

            Vector2d[] midptsArray = midpts.ToArray();
            int nearest = GeoAlgorithms2D.NearestPoint(midptsArray, midptL);
            this.Move(midptL, midpts[nearest]);
            this.Rotate(this.Edges[nearest], _line);
        }

        /// <summary>
        /// Rotates a Polygon object to a given line using its midpoint as origin and reference edge.
        /// </summary>
        /// <param name="_origin">(Vector2d) rotation origin.</param>
        /// <param name="_reference">(Line2D) reference edge of Polygon.</param>
        /// <param name="_line">(Line2D) end of rotation.</param>
        public void Rotate(Line2D _reference, Line2D _line)
        {
            Line2DIntersection inter = _reference.getLine2DIntersection(_line);
            if (inter.Relation != Line2DRelation.Divergent) return;

            Vector2d refpt = _reference.Start;
            Vector2d nearest = (GeoAlgorithms2D.DistancePoints(refpt, _line.Start) < GeoAlgorithms2D.DistancePoints(refpt, _line.End)) ? _line.Start : _line.End;
            Vector2d _origin = (Vector2d)inter.Shared;

            double angle, x, y;
            angle = GeoAlgorithms2D.AngleBetween(refpt, nearest, _origin);

            Vector2d[] vertices = new Vector2d[this.Vertices.Length];

            for (int i = 0; i < this.Vertices.Length; i++)
            {
                Vector2d trans = Vertices[i] - _origin;

                x = trans.X * Math.Cos(angle) - trans.Y * Math.Sin(angle);
                y = trans.X * Math.Sin(angle) + trans.Y * Math.Cos(angle);

                vertices[i] = new Vector2d(x, y) + _origin;
            }

            this.Update(vertices);
        }

        private void Update(Vector2d[] _vertices)
        {
            this.Vertices = _vertices;
            this.Edges = GeoAlgorithms2D.ConnectPoints(true, _vertices);
            if (_vertices.Length != 0) this.Bounds = GeoAlgorithms2D.BoundingBoxAdjusted(Vertices);
        }

        /// <summary>
        /// Moves the Polygon object to a specified destination using the given reference point.
        /// </summary>
        /// <param name="_destination">(Vector2d) destination.</param>
        /// <param name="_reference">(Vector2d) reference.</param>
        public void Move(Vector2d _destination, Vector2d _reference)
        {
            double dx = _destination.X - _reference.X;
            double dy = _destination.Y - _reference.Y;

            Vector2d[] vertices = new Vector2d[this.Vertices.Length];

            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i] = new Vector2d(Vertices[i].X + dx, Vertices[i].Y + dy);
            }

            this.Update(vertices);
        }

        /// <summary>
        /// Calculates the mid point for a given line.
        /// </summary>
        /// <param name="_line">(Line2D) line.</param>
        /// <returns>(Vector2d) mid point.</returns>
        private Vector2d CalculateMidPoint(Line2D _line)
        {
            double x = _line.Start.X + (_line.End.X - _line.Start.X) / 2;
            double y = _line.Start.Y + (_line.End.Y - _line.Start.Y) / 2;
            Vector2d midpt = new Vector2d(x, y);
            return midpt;
        }

        #region STATIC METHODS

        /// <summary>
        /// Finds the intersection point between two lines. http://stackoverflow.com/questions/1119451/how-to-tell-if-a-line-intersects-a-polygon-in-c
        /// </summary>
        /// <param name="_line1">(Line2D) line 1.</param>
        /// <param name="_line2">(Line2D) line 2.</param>
        /// <returns>(Vector2d) intersection point.</returns>
        public static Vector2d FindLine2DIntersection(Line2D _line1, Line2D _line2)
        {
            double denom = ((_line1.End.X - _line1.Start.X) * (_line2.End.Y - _line2.Start.Y)) - ((_line1.End.Y - _line1.Start.Y) * (_line2.End.X - _line2.Start.X));

            //  AB & CD are parallel 
            if (denom == 0)
                return new Vector2d();

            double numer = ((_line1.Start.Y - _line2.Start.Y) * (_line2.End.X - _line2.Start.X)) - ((_line1.Start.X - _line2.Start.X) * (_line2.End.Y - _line2.Start.Y));

            double r = numer / denom;

            double numer2 = ((_line1.Start.Y - _line2.Start.Y) * (_line1.End.X - _line1.Start.X)) - ((_line1.Start.X - _line2.Start.X) * (_line1.End.Y - _line1.Start.Y));

            double s = numer2 / denom;

            if ((r < 0 || r > 1) || (s < 0 || s > 1))
                return new Vector2d();

            // Find intersection point
            Vector2d result = new Vector2d(_line1.Start.X + (r * (_line1.End.X - _line1.Start.X)), _line1.Start.Y + (r * (_line1.End.Y - _line1.Start.Y)));

            return result;
        }

        /// <summary>
        /// Clips two specified Polygon objects and returns their intersection.
        /// </summary>
        /// <param name="_clip">(Polygon) clip polygon.</param>
        /// <param name="_subject">(Polygon) subject polygon.</param>
        /// <returns>(Polygon) intersection polygon.</returns>
        public static Polygon ClipPolygons(Polygon _clip, Polygon _subject)
        {
            List<List<IntPoint>> clipList = new List<List<IntPoint>>();
            List<List<IntPoint>> solution = new List<List<IntPoint>>();
            List<List<IntPoint>> solution2 = new List<List<IntPoint>>();
            List<IntPoint> tmp1 = new List<IntPoint>();
            List<IntPoint> tmp2 = new List<IntPoint>();
            Clipper c = new Clipper();

            tmp2 = _subject.FromPolygonToListIntPoint();
            clipList.Add(tmp2);
            c.AddPaths(clipList, PolyType.ptSubject, true);

            tmp1 = _clip.FromPolygonToListIntPoint();
            clipList.Add(tmp1);
            c.AddPaths(clipList, PolyType.ptClip, true);

            c.Execute(ClipType.ctXor, solution, PolyFillType.pftPositive, PolyFillType.pftPositive);
            if (solution.Count == 0)
                Console.WriteLine("no intersection");
            else
                Console.WriteLine("intersection");

            clipList.Clear();
            clipList.Add(tmp1);
            clipList.Add(solution[0]);
            c.Execute(ClipType.ctDifference, solution2);

            Polygon poly;
            if (solution2.Count > 0)
            {
                poly = Polygon.FromListIntPointToPolygon(solution2[0]);
            }
            else
            {
                poly = Polygon.Empty;
            }
            return poly;
        }

        private static Polygon FromListIntPointToPolygon(List<IntPoint> _intPTList)
        {
            Polygon poly;
            Vector2d[] polygon = new Vector2d[_intPTList.Count];

            for (int i = 0; i < _intPTList.Count; i++)
            {
                polygon[i] = new Vector2d((double)_intPTList[i].X, (double)_intPTList[i].Y);
            }
            poly = new Polygon(polygon);
            return poly;
        }

        private List<IntPoint> FromPolygonToListIntPoint()
        {
            List<IntPoint> tmp = new List<IntPoint>();

            for (int i = 0; i < this.Vertices.Length; i++)
            {
                tmp.Add(new IntPoint((long)this.Vertices[i].X, (long)this.Vertices[i].Y));
            }
            return tmp;
        }


        /// <summary>
        /// Offsets the given polygons by the specified offset distance.
        /// </summary>
        /// <param name="_polygons">(Polygon[]) polygons.</param>
        /// <param name="_offsetDist">(double) offset distance.</param>
        /// <returns>(Polygon[]) offset polygons.</returns>
        public static Polygon[] OffsetPolygons(Polygon[] _polygons, double _offsetDist)
        {
            List<List<IntPoint>> tmppts = new List<List<IntPoint>>();
            List<Vector2d[]> tmpPolys = new List<Vector2d[]>();

            for (int i = 0; i < _polygons.Length; i++)
            {
                List<IntPoint> tmplist = new List<IntPoint>();

                for (int j = 0; j < _polygons[i].Vertices.Length; j++)
                {
                    tmplist.Add(new IntPoint((long)_polygons[i].Vertices[j].X, (long)_polygons[i].Vertices[j].Y));
                }
                //tmplist.Reverse();
                tmppts.Add(tmplist);
            }

            //ClipperOffset co = new ClipperOffset();
            //co.AddPaths(pOff, JoinType.jtMiter, EndType.etClosedPolygon);
            //co.Execute(ref offsetPoly, delta);

            List<List<IntPoint>> clippolys = Clipper.OffsetPaths(tmppts, -_offsetDist, JoinType.jtMiter, EndType_.etClosed, 5.0);
            List<Vector2d> tmpList = new List<Vector2d>();

            for (int i = 0; i < clippolys.Count; i++)
            {
                tmpList.Clear();
                for (int j = 0; j < clippolys[i].Count; j++)
                {
                    Vector2d tmpPT = new Vector2d(clippolys[i][j].X, clippolys[i][j].Y);
                    bool isUnique = true;
                    for (int n = 0; n < tmpList.Count; n++)
                    {
                        if (tmpList[n] == tmpPT)
                        {
                            isUnique = false;
                            break;
                        }
                        else if (GeoAlgorithms2D.DistancePoints(tmpList[n], tmpPT) < 1.0d)
                        {
                            isUnique = false;
                            break;
                        }
                    }
                    if (isUnique) tmpList.Add(tmpPT);
                }
                tmpPolys.Add(tmpList.ToArray());
            }

            Polygon[] tmp = Polygon.ToPolygonArrayFromVector2dArray(tmpPolys.ToArray());
            return tmp;
        }


        public static Polygon[] OffsetPolygons2(Polygon[] _polygons, double _offsetDist)
        {

            //a new version 
            //we dont want to re-form all the polgyons from a set of vertices which is error-prone
            //we want to re-form each polgyon in the polygon set one by one

            //initialize the polgyon array
            Polygon[] tmp = new Polygon[_polygons.Length];

            //loop through the polgyon array and assign polgyons one by one
            for (int i = 0; i < tmp.Length; ++i)
            {
                List<IntPoint> tmplist = new List<IntPoint>();

                for (int j = 0; j < _polygons[i].Vertices.Length; j++)
                {
                    tmplist.Add(new IntPoint((long)_polygons[i].Vertices[j].X, (long)_polygons[i].Vertices[j].Y));
                }

                List<List<IntPoint>> tmppts = new List<List<IntPoint>>();
                tmppts.Add(tmplist);

                //this clipper.offsetPaths is just stupid but we still want to use it by making some adaption
                List<List<IntPoint>> clippolys = Clipper.OffsetPaths(tmppts, -_offsetDist, JoinType.jtMiter, EndType_.etClosed, 5.0);

                List<IntPoint> tmplist1 = new List<IntPoint>();

                if (clippolys.Count > 0)
                {
                    for (int j = 0; j < clippolys[0].Count; j++)
                    {
                        IntPoint tmpPT = new IntPoint(clippolys[0][j].X, clippolys[0][j].Y);
                        bool isUnique = true;
                        for (int n = 0; n < tmplist1.Count; n++)
                        {

                            Vector2d start = new Vector2d(tmplist1[n].X, tmplist1[n].Y);
                            Vector2d end = new Vector2d(tmpPT.X, tmpPT.Y);
                            if (start == end)
                            {
                                isUnique = false;
                                break;
                            }

                            if (GeoAlgorithms2D.DistancePoints(start, end) < 1.0d)
                            {
                                isUnique = false;
                                break;
                            }
                        }
                        if (isUnique)
                        {
                            tmplist1.Add(tmpPT);
                        }
                    }
                    tmp[i] = Polygon.FromListIntPointToPolygon(tmplist1);

                }
                else
                {
                    tmp[i] = Polygon.FromListIntPointToPolygon(tmplist);
                }
            }

            return tmp;
        }
        /// <summary>
        /// Offsets the given polygons by the specified offset distance.
        /// </summary>
        /// <param name="_polygon">(Polygon[]) polygons.</param>
        /// <param name="_offsetDist">(double) offset distance.</param>
        /// <returns>(Polygon[]) offset polygons.</returns>
        public static Polygon OffsetPolygons(Polygon _polygon, double _offsetDist)
        {
            List<List<IntPoint>> tmppts = new List<List<IntPoint>>();
            List<Vector2d[]> tmpPolys = new List<Vector2d[]>();

            List<IntPoint> tmplist = new List<IntPoint>();
            for (int j = 0; j < _polygon.Vertices.Length; j++)
            {
                tmplist.Add(new IntPoint((long)_polygon.Vertices[j].X, (long)_polygon.Vertices[j].Y));
            }
            tmppts.Add(tmplist);

            List<List<IntPoint>> clippolys = Clipper.OffsetPaths(tmppts, -_offsetDist, JoinType.jtMiter, EndType_.etClosed, 5.0);
            List<Vector2d> tmpList = new List<Vector2d>();

            for (int i = 0; i < clippolys.Count; i++)
            {
                tmpList.Clear();
                for (int j = 0; j < clippolys[i].Count; j++)
                {
                    Vector2d tmpPT = new Vector2d(clippolys[i][j].X, clippolys[i][j].Y);
                    bool isUnique = true;
                    for (int n = 0; n < tmpList.Count; n++)
                    {
                        if (tmpList[n] == tmpPT)
                        {
                            isUnique = false;
                            break;
                        }
                        else if (GeoAlgorithms2D.DistancePoints(tmpList[n], tmpPT) < 1.0d)
                        {
                            isUnique = false;
                            break;
                        }
                    }
                    if (isUnique) tmpList.Add(tmpPT);
                }
                tmpPolys.Add(tmpList.ToArray());
            }

            Polygon[] tmp = Polygon.ToPolygonArrayFromVector2dArray(tmpPolys.ToArray());
            if (tmp.Length != 0) return tmp[0];
            else return Polygon.Empty;
        }

        /// <summary>
        /// Gets the centroid of a Polygon using the specified vertices.
        /// </summary>
        /// <param name="_vertices">(Vector2d[]) vertices.</param>
        /// <returns>(Vector2d) centroid.</returns>
        public static Vector2d GetCentroidAsVector2d(Vector2d[] _vertices)
        {
            Vector2d centroid = new Vector2d();
            double signedArea = 0.0;
            double x0 = 0.0; // Current vertex X
            double y0 = 0.0; // Current vertex Y
            double x1 = 0.0; // Next vertex X
            double y1 = 0.0; // Next vertex Y
            double a = 0.0;  // Partial signed area
            double cx = 0.0d;
            double cy = 0.0d;

            // For all vertices except last
            int i = 0;
            for (i = 0; i < _vertices.Length - 1; ++i)
            {
                x0 = _vertices[i].X;
                y0 = _vertices[i].Y;
                x1 = _vertices[i + 1].X;
                y1 = _vertices[i + 1].Y;
                a = x0 * y1 - x1 * y0;
                signedArea += a;
                cx += (x0 + x1) * a;
                cy += (y0 + y1) * a;
            }

            // Do last vertex
            x0 = _vertices[i].X;
            y0 = _vertices[i].Y;
            x1 = _vertices[0].X;
            y1 = _vertices[0].Y;
            a = x0 * y1 - x1 * y0;
            signedArea += a;
            cx += (x0 + x1) * a;
            cy += (y0 + y1) * a;

            signedArea *= 0.5;
            cx /= (6 * signedArea);
            cy /= (6 * signedArea);

            centroid = new Vector2d(cx, cy);

            return centroid;
        }

        /// <summary>
        /// Gets the centroid of a Polygon using the specified vertices.
        /// </summary>
        /// <param name="_vertices">(Vector2d[]) vertices.</param>
        /// <returns>(Point) centroid.</returns>
        public static System.Drawing.Point GetCentroidAsPoint(Vector2d[] _vertices)
        {
            Vector2d pt = GetCentroidAsVector2d(_vertices);
            System.Drawing.Point pt1 = new System.Drawing.Point((int)pt.X, (int)pt.Y);
            return pt1;
        }

        /// <summary>
        /// Converts a list of lists of polygon points into a list of point arrays.
        /// </summary>
        /// <param name="_polygons">(List List Vector2d) polygon points as list.</param>
        /// <returns>(List Vector2d[]) polygon points as array.</returns>
        public static List<Vector2d[]> ToListVector2dArray(List<List<Vector2d>> _polygons)
        {
            List<Vector2d[]> tmpList = new List<Vector2d[]>();
            Vector2d[] tmpArray;

            for (int i = 0; i < _polygons.Count; i++)
            {
                tmpArray = new Vector2d[_polygons[i].Count];
                for (int j = 0; j < tmpArray.Length; j++)
                {
                    tmpArray[j] = _polygons[i][j];
                }
                tmpList.Add(tmpArray);
            }

            return tmpList;
        }

        /// <summary>
        /// Converts a list of lists of polygon points into a list of point arrays.
        /// </summary>
        /// <param name="_polygons">(Polygon[]) polygon array.</param>
        /// <returns>(List Vector2d[]) polygon points as array.</returns>
        public static List<Vector2d[]> ToListVector2dArray(Polygon[] _polygons)
        {
            List<Vector2d[]> tmpList = new List<Vector2d[]>();
            Vector2d[] tmpArray;

            for (int i = 0; i < _polygons.Length; i++)
            {
                tmpArray = _polygons[i].Vertices;
                tmpList.Add(tmpArray);
            }

            return tmpList;
        }

        /// <summary>
        /// Creates an array of Polygons from point arrays.
        /// </summary>
        /// <param name="_polygons">(Vector2d[][]) polygon points.</param>
        /// <returns>(Polygon[]) polygon array.</returns>
        public static Polygon[] ToPolygonArrayFromVector2dArray(Vector2d[][] _polygons)
        {
            Polygon[] tmpArray = new Polygon[_polygons.GetLength(0)];
            Vector2d[] tmpPoly;

            for (int i = 0; i < _polygons.GetLength(0); i++)
            {
                tmpPoly = _polygons[i];
                tmpArray[i] = new Polygon(tmpPoly);
            }

            return tmpArray;
        }

        #endregion

        #endregion
    }
}
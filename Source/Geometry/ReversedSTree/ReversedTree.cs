﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.Integration;

namespace CPlan.Geometry.ReversedSTree
{
    public class ReversedTree
    {
        #region PROPERTIES

        public List<Node> Nodes
        {
          private  set; get;
        }

        public List<Leaf> Leaves {  set; get; }

        public List<List<Node>> RSTreeNodes { set; get; }



        #endregion



        #region CONSTRUCTORS

        public ReversedTree(List<Leaf> leaves )
        {
            this.Leaves = leaves;
            Initialize();
        }

    
        #endregion


        #region METHODS

        private void Initialize()
        {

            int nodeIdx = 0;
            int treeDepth = this.Leaves[0].TreeDepth;
            for (int i = 0; i < this.Leaves.Count; i++)
            {

                if (this.Leaves[i].Parent == null)
                {
                    bool hasRightSilbling = false;
                    //left leaves
                    if (this.Leaves[i].LeftLeafIdx == -1)
                    {
                        hasRightSilbling = true;
                        Node Parent = new Node(nodeIdx, this.Leaves[i].TreeDepth - 1);
                        this.Leaves[i].Parent = Parent;
                        Nodes.Add(Parent);
                        nodeIdx++;
                        
                    }
                    //right leaves 
                    if (this.Leaves[i].RightLeafIdx == -1)
                    {
                        
                        if (!hasRightSilbling )
                        {
                            //isolated leaves
                            Node Parent = new Node(nodeIdx, this.Leaves[i].TreeDepth - 2);
                            this.Leaves[i].Parent = Parent;
                            Nodes.Add(Parent);
                            nodeIdx++;
                        }
                        else 
                        {   
                            //normal right leaves
                            int leftLeafIdx = this.Leaves[i].LeftLeafIdx;
                            this.Leaves[i].Parent = this.Leaves[leftLeafIdx].Parent;
                        }
                    }
                }
            }


          // build up relationship between nodes at the same depth  

   // cluster the nodes based on their depth and store them 
   // in the RSTreeNodes, index is reversed order of depth

            RSTreeNodes= new List<List<Node>>();
            //loop through all the left node
            for (int i = treeDepth - 1; i >= 0; i--)
            {
               
                List<Node> tempNodes = new List<Node>();
                for (int j = tempNodes.Count; j < Nodes.Count; j++)
                {
                    if (Nodes[j].Depth == i)
                    {
                        tempNodes.Add(Nodes[j]);
                    }
                  
                }
                RSTreeNodes.Add(tempNodes);  
              
            }

            for (int i = 0; i < RSTreeNodes.Count; i++)
            {


                for (int j = 0; j < RSTreeNodes[i].Count-1; j += 2)
                {
                    if (RSTreeNodes[i][j].RightNode == null)
                    {
                        RSTreeNodes[i][j].RightNode = RSTreeNodes[i][j+1];
                    }
                    if (RSTreeNodes[i][j+1].LeftNode == null)
                    {
                        RSTreeNodes[i][j + 1].LeftNode = RSTreeNodes[i][j];
                    }
                    Node Parent = new Node(nodeIdx++, RSTreeNodes[i][j].Depth - 1);

                    if (i + 1 < RSTreeNodes.Count)
                    {
                        RSTreeNodes[i + 1].Add(Parent);
                    }
                }

            }
        }

        #endregion
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using OpenTK;


namespace CPlan.Geometry
{

    [Serializable]
    public class ScalarField
    {
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Fields

        // texture mapping
        private Vector3d[] _pntCoordinates;
        private Vector2d[] _texCoordinates;

        // calculation grid
        private double _cellSize;

        // projection helpers
        private Plane _plane;

        // settings
        // TODO: this does not work at the moment
        private int _colorMode = 2; //0 = greyScale, 1 = Farbe
        private float _opacity = 1;

        // texture
        private int _texNx;
        private int _texNy;
        private byte[] _texBuffer;  // BGRA


        #endregion

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        public Vector2d OriginPoint { get; set; }
        public static readonly Dictionary<string, Tuple<double, double>> ValueRange = new Dictionary<string, Tuple<double, double>>();

        public Vector3d[] Grid
        {
            get
            {
                return (from j in Enumerable.Range(0, _texNy)
                        from i in Enumerable.Range(0, _texNx)
                        select new Vector2d((i + 0.5) / _texNx, (j + 0.5) / _texNy))  // generate rectangular planar grid
                        .Select(v => _plane.In3D(v)).ToArray();                       // ... and map onto our plane in 3D
            }
        }

        public int GridSize { get { return _texNx * _texNy; } }

        [CategoryAttribute("Settings"), DescriptionAttribute("Size of the Grid Cells")]
        public double CellSize { get { return _cellSize; } }
        // TODO: setter is not implemented! (need to update the buffer _texBuffer)
        [CategoryAttribute("Visualisation"), DescriptionAttribute("Set Transparency of the grid (0 = invisble, 1 = opaque)")]
        public float Opacity { get { return _opacity; } set { _opacity = value; } }
        [CategoryAttribute("Visualisation"), DescriptionAttribute("Get coordinates of the points for wich the texture was computed")]
        public Vector3d[] PointCoordinates { get { return _pntCoordinates; } }
        [CategoryAttribute("Visualisation"), DescriptionAttribute("Get projection coordinates to map the texture onto vertices")]
        public Vector2d[] TextureCoordinates { get { return _texCoordinates; } }

        public Vector3d Normal { get { return _plane.Normal.Normalized(); } }
        public byte[] Texture { get { return _texBuffer; } } 
        public int TexWidth { get { return _texNx; } }
        public int TexHeight { get { return _texNy; } }
        public List<double> SolarResults { get; protected set; }   
 
        # endregion

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        public ScalarField(Vector3d[] polygonPoints, double cellSize, Vector3d front)
        {
            // setup input
            OriginPoint = new Vector2d(0, 0);

            if (polygonPoints.Length <= 4)
                _pntCoordinates = polygonPoints;
            else
            { // triangulate it first
                CGeom3d.Triangle[] rez = null;
                Tess.COpenGLTesselator.Tesselate(
                    new []{polygonPoints.Select(v => new CGeom3d.Vec_3d(v.X,v.Y,v.Z)).ToArray()}, ref rez
                    );
                var firstN = rez[0].m_pP1.amul(rez[0].m_pP2);
                _pntCoordinates = rez.SelectMany(t => t.m_pP1.amul(t.m_pP2).imul(firstN) >= 0 ?
                    new[]{
                    new Vector3d(t.m_pP1.x, t.m_pP1.y, t.m_pP1.z),
                    new Vector3d(t.m_pP2.x, t.m_pP2.y, t.m_pP2.z),
                    new Vector3d(t.m_pP3.x, t.m_pP3.y, t.m_pP3.z)
                } : new[]{
                    new Vector3d(t.m_pP3.x, t.m_pP3.y, t.m_pP3.z),
                    new Vector3d(t.m_pP2.x, t.m_pP2.y, t.m_pP2.z),
                    new Vector3d(t.m_pP1.x, t.m_pP1.y, t.m_pP1.z)
                }).ToArray();
            }
            _cellSize = cellSize;

            // get the best plane for the points
            _plane = _pntCoordinates.Plane(false);


            // rotate plane to align the rectangle
            if (_pntCoordinates.Length == 4)
            {
                var xDir = (_pntCoordinates[1] - _pntCoordinates[0]).Normalized();
                var yDir = xDir.OrthogonalComponent(_pntCoordinates[3] - _pntCoordinates[0]).Normalized();
                _plane.XDirection = xDir;
                _plane.YDirection = yDir;
            }

            // swap axes if normal does not fit the front side of a mesh
            if (front.DotProduct(_plane.Normal) < 0)
            {
                var t = _plane.XDirection;
                _plane.XDirection = _plane.YDirection;
                _plane.YDirection = t;
            }
            

            // now we can make a proper projection
            {
                // initial projection of the points to a plane
                var projected = _pntCoordinates.Select(v => _plane.Projection(v));
                // shift the center of the plane so that all points have non-negative coordinates
                _plane.Center = _plane.In3D(
                        new Vector2d(
                            -projected.Max(v => -v.X),
                            -projected.Max(v => -v.Y)
                        )
                    );
                // scale the axes so that all points lie in [(0,0),(1,1)]
                var max = new Vector2d(
                            projected.Max(v => v.X),
                            projected.Max(v => v.Y)
                        );
                _plane.XDirection *= max.X;
                _plane.YDirection *= max.Y;

                // get coordinates of the poins on a 2d plane - these are the texture coordinates
                _texCoordinates = _pntCoordinates.Select(v => _plane.Projection(v)).ToArray();


                // number of points on a grid in X and Y directions
                _texNx = (int)Math.Ceiling(_plane.XDirection.Length / _cellSize);
                _texNy = (int)Math.Ceiling(_plane.YDirection.Length / _cellSize);
            }
        }


        # endregion

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods



        /// <summary>
        /// Generates texture buffer out of values.
        /// For normalization uses the pre-defined min/max values from a Dictionary<string, Tuple<double,double>> ValueRange
        /// </summary>
        /// <param name="values">array of values</param>
        /// <param name="valueType">String key for a dictionary to lookup min/max values</param>
        public void GenerateTexture(IEnumerable<double> values, string valueType)
        {
            if (values.Count() != _texNx * _texNy)
                throw new ArgumentException("ScalarField: texture generation failed, because the results array length is not equal to a texture size.");
            _texBuffer = new byte[values.Count() * 4];

            SolarResults = new List<double>();
            SolarResults = values.ToList();

            // adjust min / max
            // TODO: separate this part from the texture generation code, because we need to find these bounds BEFORE creating any texture!
            if(ValueRange.ContainsKey(valueType))
                ValueRange[valueType] = new Tuple<double, double>(
                    Math.Min(ValueRange[valueType].Item1, values.Min()),
                    Math.Max(ValueRange[valueType].Item2, values.Max())
                    );
            else
                ValueRange[valueType] = new Tuple<double, double>(
                    values.Min(),
                    values.Max()
                    );

            // min - max distance
            var span = ValueRange[valueType].Item2 - ValueRange[valueType].Item1;
            span = span == 0 ? 1 : span;

            // normalize values
            values = values.Select(a => Math.Min(Math.Max((a - ValueRange[valueType].Item1) / span, 0), 1));

            Vector3d[] colorValues = ConvertColor.CreateFFColor1D(values.ToArray());
            //switch (_colorMode)
            //{
            //    case 0:
            //        break;
            //    case 1:
            //        colorValues = ConvertColor.CreateFFColor1D(values);
            //        break;
            //    case 2:
            //        colorValues = ConvertColor.CreateFFColor1D(values);
            //        break;
            //    default:
            //        break;
            //}

            int pos = 0;
            foreach (Vector3d v in colorValues)
            {
                _texBuffer[pos++] = (byte)Math.Floor(v.X * 255.0 + 0.5);
                _texBuffer[pos++] = (byte)Math.Floor(v.Y * 255.0 + 0.5);
                _texBuffer[pos++] = (byte)Math.Floor(v.Z * 255.0 + 0.5);
                _texBuffer[pos++] = (byte)Math.Floor(_opacity * 255.0 + 0.5);
            }

        }

        //================================================================================================================================================
        public void MoveTo(Vector2d newLocation)
        {
            Vector2d diff = newLocation - OriginPoint;
            OriginPoint += diff;

            for (int i = 0; i < _pntCoordinates.Length; i++)
            {
                _pntCoordinates[i].X += diff.X;
                _pntCoordinates[i].Y += diff.Y;
            }             
        }

        # endregion
    }

    # region MaxBy for IEnumerable (no need at this moment)
    //static class EnumerableExtensions
    //{

    //    public static TSource MaxBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector)
    //    {
    //        return source.MaxBy(selector, Comparer<TKey>.Default);
    //    }


    //    public static TSource MaxBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> selector, IComparer<TKey> comparer)
    //    {
    //        if (source == null) throw new ArgumentNullException("source");
    //        if (selector == null) throw new ArgumentNullException("selector");
    //        if (comparer == null) throw new ArgumentNullException("comparer");
    //        using (var sourceIterator = source.GetEnumerator())
    //        {
    //            if (!sourceIterator.MoveNext())
    //            {
    //                throw new InvalidOperationException("Sequence contains no elements");
    //            }
    //            var max = sourceIterator.Current;
    //            var maxKey = selector(max);
    //            while (sourceIterator.MoveNext())
    //            {
    //                var c = sourceIterator.Current;
    //                var v = selector(c);
    //                if (comparer.Compare(v, maxKey) > 0)
    //                {
    //                    max = c;
    //                    maxKey = v;
    //                }
    //            }
    //            return max;
    //        }
    //    }
    //}
    # endregion
}

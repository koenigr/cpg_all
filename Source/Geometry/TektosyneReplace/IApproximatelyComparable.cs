﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Numerics;
using OpenTK;

namespace CPlan.Geometry
{
    public interface IApproximatelyComparable<MO> where MO : IApproximatelyComparable<MO>
    {
        /// <summary>
        /// Round all components of an objects according to a rule specified in the project
        /// </summary>
        /// <returns>Copy of object with rounded components</returns>
        MO Round();

        /// <summary>
        /// Round all components of an objects according to a rule specified in the project
        /// </summary>
        /// <param name="precision">delta precision for rounding</param>
        /// <returns>Copy of object with rounded components</returns>
        MO Round(double precision);

        /// <summary>
        /// Compares all components of two objects up to specified number of fractional digits
        /// </summary>
        /// <param name="b">Object to compare</param>
        /// <returns>true if the objects are "approximately" equal</returns>
        bool EqualsApprox(MO b);

        /// <summary>
        /// Compares all components of two objects up to specified number of fractional digits
        /// </summary>
        /// <param name="b">Object to compare</param>
        /// <param name="precision">delta precision for rounding</param>
        /// <returns>true if the objects are "approximately" equal</returns>
        bool EqualsApprox(MO b, double precision);
    }

    /// <summary>
    /// Static rounding and comparison methods.
    /// Rounding methods for the objects that cannot implement an interface.
    /// </summary>
    public static class Approximately
    {
        public static Complex Round(Complex v)
        {
            return new Complex(
                Round(v.Real),
                Round(v.Imaginary)
                );
        }

        public static Complex Round(Complex v, double precision)
        {

            return new Complex(
                Round(v.Real, precision),
                Round(v.Imaginary, precision)
                );
        }

        public static double Round(double v)
        {
            return Round(v, Properties.Settings.Default.RoundingPrecision);
        }

        public static double Round(double v, double precision)
        {
            double r = Math.Round(v / precision);
            int d = (int)Math.Round(Math.Log10(r) / 2);
            double p2 = Math.Pow(10, d);
            double i = Math.Round(v / precision / p2) * p2;
            if (r == i)
                return r;
            else
                return v;
        }

        public static RMO Round<RMO>(RMO o)
            where RMO : IApproximatelyComparable<RMO>
        {
            return o.Round();
        }

        public static RMO Round<RMO>(RMO o, double precision)
            where RMO : IApproximatelyComparable<RMO>
        {
            return o.Round(precision);
        }

        public static bool Equal<RMO>(RMO a, RMO b)
            where RMO : IApproximatelyComparable<RMO>
        {
            return a.EqualsApprox(b);
        }

        public static bool Equal<RMO>(RMO a, RMO b, double precision)
            where RMO : IApproximatelyComparable<RMO>
        {
            return a.EqualsApprox(b, precision);
        }

        public static bool Equal(double a, double b)
        {
            return Math.Abs(a - b) <= Properties.Settings.Default.RoundingPrecision;
        }

        public static bool Equal(double a, double b, double precision)
        {
            return Math.Abs(a - b) <= precision;
        }

        public static bool Equal(Complex a, Complex b)
        {
            return (a - b).Magnitude <= Properties.Settings.Default.RoundingPrecision;
        }

        public static bool Equal(Complex a, Complex b, double precision)
        {
            return (a - b).Magnitude <= precision;
        }

        ////////////////////////////////////////////
        // MathNet.Numerics.LinearAlgebra
        ////////////////////////////////////////////
        # region MathNet.Numerics.LinearAlgebra

        public static RMO Round<RMO>(Vector<double> o)
            where RMO : Vector<double>, new()
        {
            RMO r = new RMO();
            for (int i = 0; i < o.Count; i++)
                r[i] = Round(o[i]);
            return r;
        }

        public static RMO Round<RMO>(Vector<Complex> o)
            where RMO : Vector<Complex>, new()
        {
            RMO r = new RMO();
            for (int i = 0; i < o.Count; i++)
                r[i] = Round(o[i]);
            return r;
        }

        public static RMO Round<RMO>(Matrix<double> o)
            where RMO : Matrix<double>, new()
        {
            RMO r = new RMO();
            for (int i = 0; i < o.RowCount; i++)
                for (int j = 0; j < o.ColumnCount; j++)
                    r[i, j] = Round(o[i, j]);
            return r;
        }

        public static RMO Round<RMO>(Matrix<Complex> o)
            where RMO : Matrix<Complex>, new()
        {
            RMO r = new RMO();
            for (int i = 0; i < o.RowCount; i++)
                for (int j = 0; j < o.ColumnCount; j++)
                    r[i, j] = Round(o[i, j]);
            return r;
        }

        public static bool Equal(Vector<double> a, Vector<double> b)
        {
            var p = Properties.Settings.Default.RoundingPrecision;
            for (int i = 0; i < a.Count; i++)
                if (Math.Abs(a[i] - b[i]) > p)
                    return false;
            return true;
        }

        public static bool Equal(Vector<Complex> a, Vector<Complex> b)
        {
            var p = Properties.Settings.Default.RoundingPrecision;
            for (int i = 0; i < a.Count; i++)
                if (Math.Abs(a[i].Real - b[i].Real) > p || Math.Abs(a[i].Imaginary - b[i].Imaginary) > p)
                    return false;
            return true;
        }

        public static bool Equal(Matrix<double> a, Matrix<double> b)
        {
            var p = Properties.Settings.Default.RoundingPrecision;
            for (int i = 0; i < a.RowCount; i++)
                for (int j = 0; j < a.ColumnCount; j++)
                    if (Math.Abs(a[i, j] - b[i, j]) > p)
                        return false;
            return true;
        }

        public static bool Equal(Matrix<Complex> a, Matrix<Complex> b)
        {
            var p = Properties.Settings.Default.RoundingPrecision;
            for (int i = 0; i < a.RowCount; i++)
                for (int j = 0; j < a.ColumnCount; j++)
                    if (Math.Abs(a[i, j].Real - b[i, j].Real) > p || Math.Abs(a[i, j].Imaginary - b[i, j].Imaginary) > p)
                        return false;
            return true;
        }

        # endregion


        ////////////////////////////////////////////
        // OpenTK Vectors
        ////////////////////////////////////////////
        # region OpenTK Vectors

        public static Vector3d Round(Vector3d o)
        {
           return new Vector3d(
                Round(o.X),
                Round(o.Y),
                Round(o.Z)
                );
        }

        public static Vector3d Round(Vector3d o, double precision)
        {
            return new Vector3d(
                 Round(o.X, precision),
                 Round(o.Y, precision),
                 Round(o.Z, precision)
                 );
        }

        public static bool Equal(Vector3d a, Vector3d b)
        {
            return Equal(a.X, b.X) && Equal(a.Y, b.Y) && Equal(a.Z, b.Z);
        }

        public static bool Equal(Vector3d a, Vector3d b, double precision)
        {
            return Equal(a.X, b.X, precision) && Equal(a.Y, b.Y, precision) && Equal(a.Z, b.Z, precision);
        }

        public static Vector2d Round(Vector2d o)
        {
            return new Vector2d(
                 Round(o.X),
                 Round(o.Y)
                 );
        }

        public static Vector2d Round(Vector2d o, double precision)
        {
            return new Vector2d(
                 Round(o.X, precision),
                 Round(o.Y, precision)
                 );
        }

        public static bool Equal(Vector2d a, Vector2d b)
        {
            return Equal(a.X, b.X) && Equal(a.Y, b.Y);
        }

        public static bool Equal(Vector2d a, Vector2d b, double precision)
        {
            return Equal(a.X, b.X, precision) && Equal(a.Y, b.Y, precision);
        }

        # endregion
    }
}
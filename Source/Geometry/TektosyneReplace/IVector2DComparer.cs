﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
namespace CPlan.Geometry
{
    using System;
    using System.Collections;
    using System.Collections.Generic;



    public interface IVector2dComparer : IComparer<Vector2d>, IComparer
    {
        #region Epsilon

        /// <summary>
        /// Gets or sets the epsilon used for coordinate comparisons.</summary>
        /// <value><para>
        /// The maximum absolute difference at which coordinates should be considered equal.
        /// </para><para>-or-</para><para>
        /// Zero to use exact coordinate comparisons. The default is zero.</para></value>
        /// <exception cref="ArgumentOutOfRangeException">
        /// The property is set to a negative value.</exception>

        double Epsilon { get; set; }

        #endregion
        #region CompareEpsilon

        /// <summary>
        /// Compares two specified <see cref="Vector2d"/> instances and returns an indication of their
        /// lexicographic ordering, given the current <see cref="Epsilon"/> for coordinate
        /// comparisons.</summary>
        /// <param name="a">
        /// The first <see cref="Vector2d"/> to compare.</param>
        /// <param name="b">
        /// The second <see cref="Vector2d"/> to compare.</param>
        /// <returns><list type="table"><listheader>
        /// <term>Value</term><description>Condition</description>
        /// </listheader><item>
        /// <term>Less than zero</term>
        /// <description><paramref name="a"/> is sorted before <paramref name="b"/>, given the
        /// current <see cref="Epsilon"/>.</description>
        /// </item><item>
        /// <term>Zero</term>
        /// <description><paramref name="a"/> and <paramref name="b"/> are equal, given the current
        /// <see cref="Epsilon"/>.</description>
        /// </item><item>
        /// <term>Greater than zero</term>
        /// <description><paramref name="a"/> is sorted after <paramref name="b"/>, given the
        /// current <see cref="Epsilon"/>.</description>
        /// </item></list></returns>

        int CompareEpsilon(Vector2d a, Vector2d b);

        #endregion
        #region FindNearest

        /// <summary>
        /// Searches the specified sorted <see cref="PointD"/> collection for the element nearest to
        /// the specified coordinates, given the current <see cref="Epsilon"/> for coordinate
        /// comparisons.</summary>
        /// <param name="points">
        /// An <see cref="IList{T}"/> containing the <see cref="PointD"/> coordinates to search,
        /// sorted lexicographically using the current <see cref="IPointDComparer"/>.</param>
        /// <param name="q">
        /// The <see cref="PointD"/> coordinates to locate.</param>
        /// <returns>
        /// The zero-based index of any occurrence of <paramref name="q"/> in <paramref
        /// name="points"/>, if found; otherwise, the zero-based index of the <paramref
        /// name="points"/> element with the smallest Euclidean distance to <paramref name="q"/>.
        /// </returns>
        /// <exception cref="ArgumentNullOrEmptyException">
        /// <paramref name="points"/> is a null reference or an empty collection.</exception>
        /// <remarks>
        /// <b>FindNearest</b> combines a fast initial approximation with a radius search, depending
        /// on the concrete implementation of <see cref="IPointDComparer"/>. Please refer to <see
        /// cref="PointDComparerX"/> and <see cref="PointDComparerY"/> for details.</remarks>

        int FindNearest(IList<Vector2d> points, Vector2d q);

        #endregion
    }


}



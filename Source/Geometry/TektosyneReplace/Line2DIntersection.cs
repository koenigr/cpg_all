﻿using System;
using OpenTK;
using System.Globalization;
using System.Windows.Forms;

namespace CPlan.Geometry
{
        public struct Line2DIntersection : IEquatable<Line2DIntersection>
        {
            #region LineIntersection(LineRelation)

            /// <overloads>
            /// Initializes a new instance of the <see cref="Line2DIntersection"/> structure.</overloads>
            /// <summary>
            /// Initializes a new instance of the <see cref="Line2DIntersection"/> structure with the
            /// specified spatial relationship between the intersected lines.</summary>
            /// <param name="relation">
            /// The spatial relationship between the two line segments.</param>
            /// <remarks>
            /// The <see cref="First"/>, <see cref="Second"/>, and <see cref="Shared"/> properties
            /// remain at their default values. Use this constructor for collinear or parallel line
            /// segments that share no points.</remarks>

            public Line2DIntersection(Line2DRelation relation) : this(null, 0, 0, relation) { }

            #endregion
            #region LineIntersection(Vector2d, LineLocation, LineLocation, LineRelation)

            /// <summary>
            /// Initializes a new instance of the <see cref="Line2DIntersection"/> structure with the
            /// specified shared coordinates, relative locations, and spatial relationship between the
            /// intersected lines.</summary>
            /// <param name="shared">
            /// The <see cref="Vector2d"/> coordinates shared by the two line segments or their infinite
            /// extensions.</param>
            /// <param name="first">
            /// The location of the <see cref="Shared"/> coordinates relative to the first line segment.
            /// </param>
            /// <param name="second">
            /// The location of the <see cref="Shared"/> coordinates relative to the second line
            /// segment.</param>
            /// <param name="relation">
            /// The spatial relationship between the two line segments.</param>
            
            public Line2DIntersection(Vector2d? shared,
                Line2DLocation first, Line2DLocation second, Line2DRelation relation)
            {

                Shared = shared;
                First = first;
                Second = second;
                Relation = relation;
            }

            #endregion
            #region Exists

            /// <summary>
            /// Gets a value indicating whether both line segments contain the <see cref="Shared"/>
            /// coordinates.</summary>
            /// <value>
            /// <c>true</c> if both <see cref="First"/> and <see cref="Second"/> equal either <see
            /// cref="Line2DLocation.Start"/>, <see cref="Line2DLocation.Between"/>, or <see
            /// cref="Line2DLocation.End"/>; otherwise, <c>false</c>.</value>
            /// <remarks><para>
            /// <b>Exists</b> requires that both <see cref="First"/> and <see cref="Second"/> equal one
            /// of the indicated <see cref="Line2DLocation"/> values, but not necessarily the same value.
            /// </para><para>
            /// <b>Exists</b> indicates whether the two line segments themselves intersect. The <see
            /// cref="Shared"/> coordinates may be valid even if <b>Exists</b> is <c>false</c>,
            /// indicating an intersection of the infinite extensions of either or both line segments.
            /// </para></remarks>

            public bool Exists
            {
                get { return (Contains(First) && Contains(Second)); }
            }

            #endregion
            #region ExistsBetween

            /// <summary>
            /// Gets a value indicating whether both line segments contain the <see cref="Shared"/>
            /// coordinates, excluding the end points of at least one line segment.</summary>
            /// <value>
            /// <c>true</c> if either <see cref="First"/> or <see cref="Second"/> equals <see
            /// cref="Line2DLocation.Between"/>, and the other property equals either <see
            /// cref="Line2DLocation.Start"/>, <see cref="Line2DLocation.Between"/>, or <see
            /// cref="Line2DLocation.End"/>; otherwise, <c>false</c>.</value>
            /// <remarks>
            /// <b>ExistsBetween</b> indicates whether the two line segments themselves intersect.
            /// Unlike <see cref="Exists"/>, at least one line segment must be properly intersected by
            /// the other, i.e. not just touched at an end point.</remarks>

            public bool ExistsBetween
            {
                get
                {
                    return ((Contains(First) && Second == Line2DLocation.Between)
                        || (First == Line2DLocation.Between && Contains(Second)));
                }
            }

            #endregion
            #region First

            /// <summary>
            /// The location of the <see cref="Shared"/> coordinates relative to the first line segment.
            /// </summary>
            /// <remarks>
            /// <b>First</b> holds a <see cref="Line2DLocation"/> value indicating the location of the
            /// <see cref="Shared"/> coordinates relative to the first line segment, or <see
            /// cref="Line2DLocation.None"/> if no intersection was found. The default is <see
            /// cref="Line2DLocation.None"/>.</remarks>

            public readonly Line2DLocation First;

            #endregion
            #region Relation

            /// <summary>
            /// The spatial relationship between the two line segments.</summary>
            /// <remarks>
            /// <b>Relation</b> holds a <see cref="Line2DRelation"/> value indicating the spatial
            /// relationship between the two line segments. The default is <see
            /// cref="Line2DRelation.Parallel"/>.</remarks>

            public readonly Line2DRelation Relation;

            #endregion
            #region Second

            /// <summary>
            /// The location of the <see cref="Shared"/> coordinates relative to the second line
            /// segment.</summary>
            /// <remarks>
            /// <b>Second</b> holds a <see cref="Line2DLocation"/> value indicating the location of the
            /// <see cref="Shared"/> coordinates relative to the second line segment, or <see
            /// cref="Line2DLocation.None"/> if no intersection was found. The default is <see
            /// cref="Line2DLocation.None"/>.</remarks>

            public readonly Line2DLocation Second;

            #endregion
            #region Shared

            /// <summary>
            /// The <see cref="Vector2d"/> coordinates shared by the two line segments or their infinite
            /// extensions.</summary>
            /// <remarks><para>
            /// <b>Shared</b> holds the <see cref="Vector2d"/> coordinates shared by the two line segments
            /// or their infinite extensions, or a null reference if no intersection was found. The
            /// default is a null reference.
            /// </para><para>
            /// Valid <b>Shared</b> coordinates are generally computed, but may be copied from the <see
            /// cref="LineD.Start"/> or <see cref="LineD.End"/> points of an intersecting line if <see
            /// cref="First"/> and/or <see cref="Second"/> equals <see cref="Line2DLocation.Start"/> or
            /// <see cref="Line2DLocation.End"/>.
            /// </para><para>
            /// <b>Shared</b> holds the following special values if <see cref="Relation"/> equals <see
            /// cref="Line2DRelation.Collinear"/>:
            /// </para><list type="bullet"><item>
            /// If the two line segments overlap, <b>Shared</b> is not computed but set directly to the
            /// <see cref="LineD.Start"/> or <see cref="LineD.End"/> point of the second line segment,
            /// whichever is contained by the first line segment and is lexicographically smaller,
            /// according to <see cref="Vector2dComparerY"/>.
            /// </item><item>
            /// Otherwise, <b>Shared</b> is set to a null reference, even though the infinite extensions
            /// of the line segments share all their points.</item></list></remarks>

            public readonly Vector2d? Shared;

            #endregion
            #region Contains

            /// <summary>
            /// Determines whether the specified <see cref="Line2DLocation"/> value specifies that the
            /// tested line segment contains the tested point.</summary>
            /// <param name="location">
            /// The <see cref="Line2DLocation"/> value to examine.</param>
            /// <returns>
            /// <c>true</c> if <paramref name="location"/> equals <see cref="Line2DLocation.Start"/>, <see
            /// cref="Line2DLocation.Between"/>, or <see cref="Line2DLocation.End"/>; otherwise,
            /// <c>false</c>.</returns>
            /// <remarks>
            /// <b>Contains</b> uses a bit mask for efficient testing of the specified <paramref
            /// name="location"/> for the three acceptable <see cref="Line2DLocation"/> values.</remarks>

            public static bool Contains(Line2DLocation location)
            {
                const Line2DLocation mask = Line2DLocation.Start | Line2DLocation.Between | Line2DLocation.End;
                return ((location & mask) != 0);
            }

            #endregion
            #region Find(Vector2d, Vector2d, Vector2d, Vector2d)

            /// <overloads>
            /// Finds the intersection of the specified line segments.</overloads>
            /// <summary>
            /// Finds the intersection of the specified line segments, using exact coordinate
            /// comparisons.</summary>
            /// <param name="a">
            /// The <see cref="LineD.Start"/> point of the first line segment.</param>
            /// <param name="b">
            /// The <see cref="LineD.End"/> point of the first line segment.</param>
            /// <param name="c">
            /// The <see cref="LineD.Start"/> point of the second line segment.</param>
            /// <param name="d">
            /// The <see cref="LineD.End"/> point of the second line segment.</param>
            /// <returns>
            /// A <see cref="Line2DIntersection"/> instance that describes if and how the line segments
            /// from <paramref name="a"/> to <paramref name="b"/> and from <paramref name="c"/> to
            /// <paramref name="d"/> intersect.</returns>
            /// <remarks><para>
            /// <b>Find</b> was adapted from the <c>Segments-Intersect</c> algorithm by Thomas H. Cormen
            /// et al., <em>Introduction to Algorithms</em> (3rd ed.), The MIT Press 2009, p.1018, for
            /// intersection testing; and from the <c>SegSegInt</c> and <c>ParallelInt</c> algorithms by
            /// Joseph O’Rourke, <em>Computational Geometry in C</em> (2nd ed.), Cambridge University
            /// Press 1998, p.224f, for line relationships and shared coordinates.
            /// </para><para>
            /// Cormen’s intersection testing first examines the <see cref="Vector2d.CrossProductLength"/>
            /// for each triplet of specified points. If that is insufficient, O’Rourke’s algorithm then
            /// examines the parameters of both line equations. This is mathematically redundant since
            /// O’Rourke’s algorithm alone should produce all desired information, but the combination
            /// of both algorithms proved much more resilient against misjudging line relationships due
            /// to floating-point inaccuracies.
            /// </para><para>
            /// Although most comparisons in this overload are exact, cross-product testing is always
            /// performed with a minimum epsilon of 1e-10. Moreover, <b>Find</b> will return the result
            /// of the other <see cref="Find(Vector2d, Vector2d, Vector2d, Vector2d, Double)"/> overload with an
            /// epsilon of 2e-10 if cross-product testing contradicts line equation testing. Subsequent
            /// contradictions result in further recursive calls, each time with a doubled epsilon,
            /// until an intersection can be determined without contradictions.</para></remarks>
            
            public static Line2DIntersection Find(Vector2d a, Vector2d b, Vector2d c, Vector2d d)
            {


            //numerical error with 1e-10 and thus is changed to 1e-9
                const double epsilon = 1e-8;
             
                Line2DLocation first, second;

                double bax = b.X - a.X, bay = b.Y - a.Y;
                double dcx = d.X - c.X, dcy = d.Y - c.Y;

                // compute cross-products for all end points
                double d1 = (a.X - c.X) * dcy - (a.Y - c.Y) * dcx;
                double d2 = (b.X - c.X) * dcy - (b.Y - c.Y) * dcx;
                double d3 = (c.X - a.X) * bay - (c.Y - a.Y) * bax;
                double d4 = (d.X - a.X) * bay - (d.Y - a.Y) * bax;

            //Debug.Assert(d1 == c.CrossProductLength(a, d));
            //Debug.Assert(d2 == c.CrossProductLength(b, d));
            //Debug.Assert(d3 == a.CrossProductLength(c, b));
            //Debug.Assert(d4 == a.CrossProductLength(d, b));

            /*
             * Some cross-products are zero: corresponding end point triplets are collinear.
             * 
             * The infinite lines intersect on the corresponding end points. The lines are collinear
             * exactly if all cross-products are zero; otherwise, the lines are divergent and we
             * need to check whether the finite line segments also intersect on the end points.
             * 
             * We always perform epsilon comparisons on cross-products, even in the exact overload,
             * because almost-zero cases are very frequent, especially for collinear lines.
             */

            //bool isStackOverflow = false;
            //bool conditionIsFulfilled = false;
            //try
            //{
            //    conditionIsFulfilled = (Math.Abs(d1) < 1.0d) && (Math.Abs(d2) <= 1.0d) && (Math.Abs(d3) <= 1.0d) && (Math.Abs(d4) <= 1.0d)
            //   ;



            //}
            //catch (StackOverflowException e)
            //{
            //    isStackOverflow = true;
            //}
            //    if (!isStackOverflow && conditionIsFulfilled)
            //    {
                    if ((Math.Abs(d1) <= epsilon) && (Math.Abs(d2) <= epsilon) &&
                        (Math.Abs(d3) <= epsilon) && (Math.Abs(d4) <= epsilon))
                    {

                        // find lexicographically first point where segments overlap

                        if (Vector2dComparerY.CompareExact(c, d) < 0)
                        {
                            first = LocateCollinear(a, b, c);
                            if (Contains(first))
                                return new Line2DIntersection(c, first, Line2DLocation.Start, Line2DRelation.Collinear);

                            first = LocateCollinear(a, b, d);
                            if (Contains(first))
                                return new Line2DIntersection(d, first, Line2DLocation.End, Line2DRelation.Collinear);
                        }
                        else
                        {
                            first = LocateCollinear(a, b, d);
                            if (Contains(first))
                                return new Line2DIntersection(d, first, Line2DLocation.End, Line2DRelation.Collinear);

                            first = LocateCollinear(a, b, c);
                            if (Contains(first))
                                return new Line2DIntersection(c, first, Line2DLocation.Start, Line2DRelation.Collinear);
                        }

                        // collinear line segments without overlapping points
                        return new Line2DIntersection(Line2DRelation.Collinear);
                    }

                    // check for divergent lines with end point intersection
                    if (Math.Abs(d1) <= epsilon)
                    {
                        try
                        {
                            second = LocateCollinear(c, d, a);
                            return new Line2DIntersection(a, Line2DLocation.Start, second, Line2DRelation.Divergent);
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show("c " + c.X + " " + c.Y + " d " + d.X + " " + d.Y);
                        }

                    }
                    if (Math.Abs(d2) <= epsilon)
                    {
                        second = LocateCollinear(c, d, b);
                        return new Line2DIntersection(b, Line2DLocation.End, second, Line2DRelation.Divergent);
                    }
                    if (Math.Abs(d3) <= epsilon)
                    {
                        first = LocateCollinear(a, b, c);
                        return new Line2DIntersection(c, first, Line2DLocation.Start, Line2DRelation.Divergent);
                    }
                    if (Math.Abs(d4) <= epsilon)
                    {
                        first = LocateCollinear(a, b, d);
                        return new Line2DIntersection(d, first, Line2DLocation.End, Line2DRelation.Divergent);
                    }
              //  }
                /*
                 * All cross-products are non-zero: divergent or parallel lines.
                 * 
                 * The lines and segments might intersect, but not on any end point.
                 * Compute parameters of both line equations to determine intersections.
                 * Zero denominator indicates parallel lines (but not collinear, see above).
                 */
                double denom = dcx * bay - bax * dcy;
                if (Math.Abs(denom) <= epsilon)
                    return new Line2DIntersection(Line2DRelation.Parallel);

                /*
                 * Compute position of intersection point relative to line segments, and also perform
                 * sanity checks for floating-point inaccuracies. If a check fails, we cannot give a
                 * reliable result at the current precision and must recurse with a greater epsilon.
                 * 
                 * Cross-products have pairwise opposite signs exactly if the corresponding line segment
                 * straddles the infinite extension of the other line segment, implying a line equation
                 * parameter between zero and one. Pairwise identical signs imply a parameter less than
                 * zero or greater than one. Parameters cannot be exactly zero or one, as that indicates
                 * end point intersections which were already ruled out by cross-product testing.
                 */
                double snum = a.X * dcy - a.Y * dcx - c.X * d.Y + c.Y * d.X;
                double s = snum / denom;

                if ((d1 < 0 && d2 < 0) || (d1 > 0 && d2 > 0))
                {
                    if (s < 0) first = Line2DLocation.Before;
                    else if (s > 1) first = Line2DLocation.After;
                    //else return Find(a, b, c, d, 2 * epsilon);
                    //yufan's adaption
                    else return Find(a,b, c, d, 10*epsilon);
                }
                else {
                    if (s > 0 && s < 1) first = Line2DLocation.Between;
                    //else return Find(a, b, c, d, 2 * epsilon);
                    //yufan's adaption
                    else
                    {
                        return Find(a, b, c, d, 10* epsilon);
                    }
                }

                double tnum = c.Y * bax - c.X * bay + a.X * b.Y - a.Y * b.X;
                double t = tnum / denom;

                if ((d3 < 0 && d4 < 0) || (d3 > 0 && d4 > 0))
                {
                    if (t < 0) second = Line2DLocation.Before;
                    else if (t > 1) second = Line2DLocation.After;
                    //else return Find(a, b, c, d, 2 * epsilon);
                    //yufan's adaption
                    else return Find(a, b, c, d, 10 * epsilon);
            }
                else {
                    if (t > 0 && t < 1) second = Line2DLocation.Between;
                // else return Find(a, b, c, d, 2 * epsilon);
                //yufan's adaption
                else return Find(a, b, c, d, 10 * epsilon);
            }

                Vector2d shared = new Vector2d(a.X + s * bax, a.Y + s * bay);
                return new Line2DIntersection(shared, first, second, Line2DRelation.Divergent);
            }

            #endregion
            #region Find(Vector2d, Vector2d, Vector2d, Vector2d, Double)

            /// <summary>
            /// Finds the intersection of the specified line segments, given the specified epsilon for
            /// coordinate comparisons.</summary>
            /// <param name="a">
            /// The <see cref="LineD.Start"/> point of the first line segment.</param>
            /// <param name="b">
            /// The <see cref="LineD.End"/> point of the first line segment.</param>
            /// <param name="c">
            /// The <see cref="LineD.Start"/> point of the second line segment.</param>
            /// <param name="d">
            /// The <see cref="LineD.End"/> point of the second line segment.</param>
            /// <param name="epsilon">
            /// The maximum absolute difference at which coordinates and intermediate results should be
            /// considered equal. This value is always raised to a minium of 1e-10.</param>
            /// <returns>
            /// A <see cref="Line2DIntersection"/> instance that describes if and how the line segments
            /// from <paramref name="a"/> to <paramref name="b"/> and from <paramref name="c"/> to
            /// <paramref name="d"/> intersect.</returns>
            /// <remarks><para>
            /// <b>Find</b> is identical with the basic <see cref="Find(Vector2d, Vector2d, Vector2d,
            /// Vector2d)"/> overload but uses the specified <paramref name="epsilon"/> to compare
            /// coordinates and intermediate results.
            /// </para><para>
            /// <b>Find</b> always raises the specified <paramref name="epsilon"/> to a minimum of 1e-10
            /// because the algorithm is otherwise too unstable, and would initiate multiple recursions
            /// with a greater epsilon anyway.</para></remarks>

            public static Line2DIntersection Find(Vector2d a, Vector2d b, Vector2d c, Vector2d d, double epsilon)
            {
               // if (epsilon < 1e-10) epsilon = 1e-10;
                if (epsilon < 1e-8) epsilon = 1e-8;
                Line2DLocation first, second;

                double bax = b.X - a.X, bay = b.Y - a.Y;
                double dcx = d.X - c.X, dcy = d.Y - c.Y;

                // compute cross-products for all end points
                double d1 = (a.X - c.X) * dcy - (a.Y - c.Y) * dcx;
                double d2 = (b.X - c.X) * dcy - (b.Y - c.Y) * dcx;
                double d3 = (c.X - a.X) * bay - (c.Y - a.Y) * bax;
                double d4 = (d.X - a.X) * bay - (d.Y - a.Y) * bax;

            //Debug.Assert(d1 == c.CrossProductLength(a, d));
            //Debug.Assert(d2 == c.CrossProductLength(b, d));
            //Debug.Assert(d3 == a.CrossProductLength(c, b));
            //Debug.Assert(d4 == a.CrossProductLength(d, b));

            // check for collinear (but not parallel) lines
           // bool isStackOverflow = false;
            //bool conditionIsFulfilled = false;
            //try
            //{
            //    conditionIsFulfilled = (Math.Abs(d1) < 1.0d) && (Math.Abs(d2) <= 1.0d) && (Math.Abs(d3) <= 1.0d) && (Math.Abs(d4) <= 1.0d)
            //   ;



            //}
            //catch (StackOverflowException e)
            //{
            //    isStackOverflow = true;
            //}
            //if (!isStackOverflow && conditionIsFulfilled)
           // {
             //   if (Math.Abs(d1) < 1.0d && (Math.Abs(d2) <= 1.0d) && (Math.Abs(d3) <= 1.0d) && (Math.Abs(d4) <= 1.0d)){

                    if (Math.Abs(d1) <= epsilon && Math.Abs(d2) <= epsilon &&
                        Math.Abs(d3) <= epsilon && Math.Abs(d4) <= epsilon)
                    {

                        // find lexicographically first point where segments overlap
                        if (Vector2dComparerY.CompareExact(c, d) < 0)
                        {
                            first = LocateCollinear(a, b, c, epsilon);
                            if (Contains(first))
                                return new Line2DIntersection(c, first, Line2DLocation.Start, Line2DRelation.Collinear);

                            first = LocateCollinear(a, b, d, epsilon);
                            if (Contains(first))
                                return new Line2DIntersection(d, first, Line2DLocation.End, Line2DRelation.Collinear);
                        }
                        else
                        {
                            first = LocateCollinear(a, b, d, epsilon);
                            if (Contains(first))
                                return new Line2DIntersection(d, first, Line2DLocation.End, Line2DRelation.Collinear);

                            first = LocateCollinear(a, b, c, epsilon);
                            if (Contains(first))
                                return new Line2DIntersection(c, first, Line2DLocation.Start, Line2DRelation.Collinear);
                        }

                        // collinear line segments without overlapping points
                        return new Line2DIntersection(Line2DRelation.Collinear);
                    }

                    // check for divergent lines with end point intersection
                    if (Math.Abs(d1) <= epsilon)
                    {
                        second = LocateCollinear(c, d, a, epsilon);
                        return new Line2DIntersection(a, Line2DLocation.Start, second, Line2DRelation.Divergent);
                    }
                    if (Math.Abs(d2) <= epsilon)
                    {
                        second = LocateCollinear(c, d, b, epsilon);
                        return new Line2DIntersection(b, Line2DLocation.End, second, Line2DRelation.Divergent);
                    }
                    if (Math.Abs(d3) <= epsilon)
                    {
                        first = LocateCollinear(a, b, c, epsilon);
                        return new Line2DIntersection(c, first, Line2DLocation.Start, Line2DRelation.Divergent);
                    }
                    if (Math.Abs(d4) <= epsilon)
                    {
                        first = LocateCollinear(a, b, d, epsilon);
                        return new Line2DIntersection(d, first, Line2DLocation.End, Line2DRelation.Divergent);
                    }
               // }
                // compute parameters of line equations
                double denom = dcx * bay - bax * dcy;
                if (Math.Abs(denom) <= epsilon)
                    return new Line2DIntersection(Line2DRelation.Parallel);

                double snum = a.X * dcy - a.Y * dcx - c.X * d.Y + c.Y * d.X;
                double s = snum / denom;

                if ((d1 < 0 && d2 < 0) || (d1 > 0 && d2 > 0))
                {
                    if (s < 0) first = Line2DLocation.Before;
                    else if (s > 1) first = Line2DLocation.After;
                    else return Find(a, b, c, d, 10 * epsilon);
                }
                else {
                    if (s > 0 && s < 1) first = Line2DLocation.Between;
                    else return Find(a, b, c, d, 10 * epsilon);
                }

                double tnum = c.Y * bax - c.X * bay + a.X * b.Y - a.Y * b.X;
                double t = tnum / denom;

                if ((d3 < 0 && d4 < 0) || (d3 > 0 && d4 > 0))
                {
                    if (t < 0) second = Line2DLocation.Before;
                    else if (t > 1) second = Line2DLocation.After;
                    else return Find(a, b, c, d, 10 * epsilon);
                }
                else {
                    if (t > 0 && t < 1) second = Line2DLocation.Between;
                    else return Find(a, b, c, d, 10 * epsilon);
                }

                Vector2d shared = new Vector2d(a.X + s * bax, a.Y + s * bay);

                /*
                 * Epsilon comparisons of cross products (or line equation parameters) might miss
                 * epsilon-close end point intersections of very long line segments. We compensate by
                 * directly comparing the computed intersection point against the four end points.
                 */

                
                if((Math.Abs( a.X - shared.X) <= epsilon) && (Math.Abs( a.Y - shared.Y) <= epsilon))
                    first = Line2DLocation.Start;
                else if ((Math.Abs(b.X - shared.X) <= epsilon) && (Math.Abs(b.Y - shared.Y) <= epsilon))
                    first = Line2DLocation.End;

                if ((Math.Abs(c.X - shared.X) <= epsilon) && (Math.Abs(c.Y - shared.Y) <= epsilon))
                    second = Line2DLocation.Start;
                else if ((Math.Abs(d.X - shared.X) <= epsilon) && (Math.Abs(d.Y - shared.Y) <= epsilon))
                    second = Line2DLocation.End;

                return new Line2DIntersection(shared, first, second, Line2DRelation.Divergent);
            }

            #endregion
            #region LocateCollinear(Vector2d, Vector2d, Vector2d)

            /// <overloads>
            /// Determines the location of the specified <see cref="Vector2d"/> coordinates relative to
            /// the specified line segment, assuming they are collinear.</overloads>
            /// <summary>
            /// Determines the location of the specified <see cref="Vector2d"/> coordinates relative to
            /// the specified line segment, assuming they are collinear and using exact coordinate
            /// comparisons.</summary>
            /// <param name="a">
            /// The <see cref="LineD.Start"/> point of the line segment.</param>
            /// <param name="b">
            /// The <see cref="LineD.End"/> point of the line segment.</param>
            /// <param name="q">
            /// The <see cref="Vector2d"/> coordinates to examine.</param>
            /// <returns>
            /// A <see cref="Line2DLocation"/> value indicating the location of <paramref name="q"/>
            /// relative to the line segment from <paramref name="a"/> to <paramref name="b"/>.
            /// </returns>
            /// <remarks>
            /// <b>LocateCollinear</b> is identical with the <see cref="LineD.LocateCollinear(Vector2d)"/> 
            /// instance method of the <see cref="LineD"/> structure but takes the start and end points
            /// of the line segment as explicit parameters.</remarks>

            public static Line2DLocation LocateCollinear(Vector2d a, Vector2d b, Vector2d q)
            {

                double epsilon = 1e-8;
            //yufan's adaption
            if ((q.X < epsilon && (q.Y < epsilon)) || (a.X < epsilon && (a.Y < epsilon)) || Math.Abs(q.X -a.X)  < epsilon && (Math.Abs(q.Y - a.Y) < epsilon)) return Line2DLocation.Start;
            if ((q.X < epsilon && (q.Y < epsilon)) || (b.X < epsilon && (b.Y < epsilon)) || Math.Abs(q.X - b.X) < epsilon && (Math.Abs(q.Y - b.Y) < epsilon)) return Line2DLocation.End;

            //if (q == a) return Line2DLocation.Start;
            //    if (q == b) return Line2DLocation.End;

                double ax = b.X - a.X, ay = b.Y - a.Y;
                double bx = q.X - a.X, by = q.Y - a.Y;

                if (ax * bx < 0 || ay * by < 0)
                    return Line2DLocation.Before;
                if (ax * ax + ay * ay < bx * bx + by * by)
                    return Line2DLocation.After;

                return Line2DLocation.Between;
            }

            #endregion
            #region LocateCollinear(Vector2d, Vector2d, Vector2d, Double)

            /// <summary>
            /// Determines the location of the specified <see cref="Vector2d"/> coordinates relative to
            /// the specified line segment, assuming they are collinear and given the specified epsilon
            /// for coordinate comparisons.</summary>
            /// <param name="a">
            /// The <see cref="LineD.Start"/> point of the line segment.</param>
            /// <param name="b">
            /// The <see cref="LineD.End"/> point of the line segment.</param>
            /// <param name="q">
            /// The <see cref="Vector2d"/> coordinates to examine.</param>
            /// <param name="epsilon">
            /// The maximum absolute value at which intermediate results should be considered zero.
            /// </param>
            /// <returns>
            /// A <see cref="Line2DLocation"/> value indicating the location of <paramref name="q"/>
            /// relative to the line segment from <paramref name="a"/> to <paramref name="b"/>.
            /// </returns>
            /// <remarks><para>
            /// <b>LocateCollinear</b> is identical with the <see cref="LineD.LocateCollinear(Vector2d,
            /// Double)"/> instance method of the <see cref="LineD"/> structure but takes the start and
            /// end points of the line segment as explicit parameters.
            /// </para><para>
            /// The specified <paramref name="epsilon"/> must be greater than zero, but
            /// <b>LocateCollinear</b> does not check this condition.</para></remarks>

            public static Line2DLocation LocateCollinear(Vector2d a, Vector2d b, Vector2d q, double epsilon)
            {
            //yufan's adjustment
            if (((a.X < epsilon)&& (q.X < epsilon))|| ((a.Y < epsilon) && (q.Y < epsilon)) || (Math.Abs(a.X - q.X) <= epsilon) && (Math.Abs(a.Y - q.Y) <= epsilon)) return Line2DLocation.Start;
            if (((b.X < epsilon) && (q.X < epsilon)) || ((a.Y < epsilon) && (q.Y < epsilon)) || (Math.Abs(b.X - q.X) <= epsilon) && (Math.Abs(b.Y - q.Y) <= epsilon)) return Line2DLocation.End;

           // if ((Math.Abs(a.X - q.X) <= epsilon) && (Math.Abs(a.Y - q.Y) <= epsilon)) return Line2DLocation.Start;
           //     if ((Math.Abs(b.X - q.X) <= epsilon) && (Math.Abs(b.Y - q.Y) <= epsilon)) return Line2DLocation.End;

                double ax = b.X - a.X, ay = b.Y - a.Y;
                double bx = q.X - a.X, by = q.Y - a.Y;

                if (ax * bx < 0 || ay * by < 0)
                    return Line2DLocation.Before;
                if (ax * ax + ay * ay < bx * bx + by * by)
                    return Line2DLocation.After;

                return Line2DLocation.Between;
            }

            #endregion
            #region GetHashCode

            /// <summary>
            /// Returns the hash code for this <see cref="Line2DIntersection"/> instance.</summary>
            /// <returns>
            /// An <see cref="Int32"/> hash code.</returns>
            /// <remarks>
            /// <b>GetHashCode</b> returns the result of <see cref="Vector2d.GetHashCode"/> for the <see
            /// cref="Shared"/> property, if valid; otherwise, zero.</remarks>

            public override unsafe int GetHashCode()
            {
                unchecked
                {
                    Vector2d shared = Shared.GetValueOrDefault();
                    double x = shared.X, y = shared.Y;
                    long xi = *((long*)&x), yi = *((long*)&y);
                    return (int)xi ^ (int)(xi >> 32) ^ (int)yi ^ (int)(yi >> 32);
                }
            }

            #endregion
            #region ToString

            /// <summary>
            /// Returns a <see cref="String"/> that represents the <see cref="Line2DIntersection"/>.
            /// </summary>
            /// <returns>
            /// A <see cref="String"/> containing the values of the <see cref="Shared"/>, <see
            /// cref="First"/>, <see cref="Second"/>, and <see cref="Relation"/> properties.</returns>

            public override string ToString()
            {
                string shared = (Shared.HasValue ? Shared.ToString() : "null");

                return String.Format(CultureInfo.InvariantCulture,
                    "{{Shared={0}, First={1}, Second={2}, Relation={3}}}",
                    shared, First, Second, Relation);
            }

            #endregion
            #region operator==

            /// <summary>
            /// Determines whether two <see cref="Line2DIntersection"/> instances have the same value.
            /// </summary>
            /// <param name="x">
            /// The first <see cref="Line2DIntersection"/> to compare.</param>
            /// <param name="y">
            /// The second <see cref="Line2DIntersection"/> to compare.</param>
            /// <returns>
            /// <c>true</c> if the value of <paramref name="x"/> is the same as the value of <paramref
            /// name="y"/>; otherwise, <c>false</c>.</returns>
            /// <remarks>
            /// This operator invokes the <see cref="Equals(Line2DIntersection)"/> method to test the two
            /// <see cref="Line2DIntersection"/> instances for value equality.</remarks>

            public static bool operator ==(Line2DIntersection x, Line2DIntersection y)
            {
                return x.Equals(y);
            }

            #endregion
            #region operator!=

            /// <summary>
            /// Determines whether two <see cref="Line2DIntersection"/> instances have different values.
            /// </summary>
            /// <param name="x">
            /// The first <see cref="Line2DIntersection"/> to compare.</param>
            /// <param name="y">
            /// The second <see cref="Line2DIntersection"/> to compare.</param>
            /// <returns>
            /// <c>true</c> if the value of <paramref name="x"/> is different from the value of
            /// <paramref name="y"/>; otherwise, <c>false</c>.</returns>
            /// <remarks>
            /// This operator invokes the <see cref="Equals(Line2DIntersection)"/> method to test the two
            /// <see cref="Line2DIntersection"/> instances for value inequality.</remarks>

            public static bool operator !=(Line2DIntersection x, Line2DIntersection y)
            {
                return !x.Equals(y);
            }

            #endregion
            #region IEquatable Members
            #region Equals(Object)

            /// <overloads>
            /// Determines whether two <see cref="Line2DIntersection"/> instances have the same value.
            /// </overloads>
            /// <summary>
            /// Determines whether this <see cref="Line2DIntersection"/> instance and a specified object,
            /// which must be a <see cref="Line2DIntersection"/>, have the same value.</summary>
            /// <param name="obj">
            /// An <see cref="Object"/> to compare to this <see cref="Line2DIntersection"/> instance.
            /// </param>
            /// <returns>
            /// <c>true</c> if <paramref name="obj"/> is another <see cref="Line2DIntersection"/> instance
            /// and its value is the same as this instance; otherwise, <c>false</c>.</returns>
            /// <remarks>
            /// If the specified <paramref name="obj"/> is another <see cref="Line2DIntersection"/>
            /// instance, <b>Equals</b> invokes the strongly-typed <see
            /// cref="Equals(Line2DIntersection)"/> overload to test the two instances for value equality.
            /// </remarks>

            public override bool Equals(object obj)
            {
                if (obj == null || !(obj is Line2DIntersection))
                    return false;

                return Equals((Line2DIntersection)obj);
            }

            #endregion
            #region Equals(LineIntersection)

            /// <summary>
            /// Determines whether this instance and a specified <see cref="Line2DIntersection"/> have the
            /// same value.</summary>
            /// <param name="other">
            /// A <see cref="Line2DIntersection"/> to compare to this instance.</param>
            /// <returns>
            /// <c>true</c> if the value of <paramref name="other"/> is the same as this instance;
            /// otherwise, <c>false</c>.</returns>
            /// <remarks>
            /// <b>Equals</b> compares the values of the <see cref="First"/>, <see cref="Second"/>, <see
            /// cref="Relation"/>, and <see cref="Shared"/> properties of the two <see
            /// cref="Line2DIntersection"/> instances to test for value equality.</remarks>

            public bool Equals(Line2DIntersection other)
            {
                return (First == other.First && Second == other.Second &&
                    Relation == other.Relation && Shared == other.Shared);
            }

            #endregion
            #region Equals(LineIntersection, LineIntersection)

            /// <summary>
            /// Determines whether two specified <see cref="Line2DIntersection"/> instances have the same
            /// value.</summary>
            /// <param name="x">
            /// The first <see cref="Line2DIntersection"/> to compare.</param>
            /// <param name="y">
            /// The second <see cref="Line2DIntersection"/> to compare.</param>
            /// <returns>
            /// <c>true</c> if the value of <paramref name="x"/> is the same as the value of <paramref
            /// name="y"/>; otherwise, <c>false</c>.</returns>
            /// <remarks>
            /// <b>Equals</b> invokes the non-static <see cref="Equals(Line2DIntersection)"/> overload to
            /// test the two <see cref="Line2DIntersection"/> instances for value equality.</remarks>

            public static bool Equals(Line2DIntersection x, Line2DIntersection y)
            {
                return x.Equals(y);
            }

            #endregion
            #endregion
        }
    }


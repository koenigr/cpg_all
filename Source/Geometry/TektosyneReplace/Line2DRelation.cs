﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPlan.Geometry
{
    public enum Line2DRelation
    {

        /// <summary>
        /// Specifies that the two line segments are parallel displacements of each other, and
        /// therefore cannot share any points.</summary>

        Parallel,

        /// <summary>
        /// Specifies that the two line segments are part of the same infinite line, and therefore
        /// may share some or all their points.</summary>

        Collinear,

        /// <summary>
        /// Specifies that the two line segments have different angles, and therefore may share a
        /// single point of intersection.</summary>

        Divergent
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPlan.Geometry
{
    internal enum MoveEdgeResult
    {

        /// <summary>
        /// Neither the half-edge nor its twin equals <see cref="SubdivisionFace.OuterEdge"/> or any
        /// <see cref="SubdivisionFace.InnerEdges"/> element. No properties were changed.</summary>

        Unchanged,

        /// <summary>
        /// The half-edge or its twin equals <see cref="SubdivisionFace.OuterEdge"/>, and that
        /// property was changed to another <see cref="SubdivisionEdge"/>.</summary>

        OuterChanged,

        /// <summary>
        /// The half-edge or its twin equals an <see cref="SubdivisionFace.InnerEdges"/> element,
        /// and that element was changed to another <see cref="SubdivisionEdge"/>.</summary>

        InnerChanged,

        /// <summary>
        /// The half-edge or its twin equals an <see cref="SubdivisionFace.InnerEdges"/> element,
        /// and that element was removed since its cycle contains no other half-edges.</summary>

        InnerRemoved
    }
}

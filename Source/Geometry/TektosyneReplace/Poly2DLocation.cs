﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPlan.Geometry
{
    public enum Poly2DLocation
    {
        /// <summary>
        /// Specifies that the point is inside the polygon.</summary>

        Inside,

        /// <summary>
        /// Specifies that the point is outside the polygon.</summary>

        Outside,

        /// <summary>
        /// Specifies that the point coincides with an edge of the polygon.</summary>

        Edge,

        /// <summary>
        /// Specifies that the point coincides with a vertex of the polygon.</summary>

        Vertex
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPlan.Geometry
{
    public enum SubdivisionElementType
    {

        /// <summary>
        /// Specifies an <see cref="SubdivisionElement.Edge"/>.</summary>

        Edge,

        /// <summary>
        /// Specifies a <see cref="SubdivisionElement.Face"/>.</summary>

        Face,

        /// <summary>
        /// Specifies a <see cref="SubdivisionElement.Vertex"/>.</summary>

        Vertex
    }
}

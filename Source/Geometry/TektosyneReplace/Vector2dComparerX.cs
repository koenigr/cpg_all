﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using OpenTK;


namespace CPlan.Geometry
{
    public class Vector2dComparerX: IVector2dComparer
    {
        #region Private Fields

        /// <summary>The epsilon used for coordinate comparisons.</summary>
        private double _epsilon;

        #endregion
        #region Epsilon

        /// <summary>
        /// Gets or sets the epsilon used for coordinate comparisons.</summary>
        /// <value><para>
        /// The maximum absolute difference at which coordinates should be considered equal.
        /// </para><para>-or-</para><para>
        /// Zero to use exact coordinate comparisons. The default is zero.</para></value>
        /// <exception cref="ArgumentOutOfRangeException">
        /// The property is set to a negative value.</exception>
        /// <remarks>
        /// <b>Epsilon</b> determines whether <see cref="Compare"/> dispatches to <see
        /// cref="CompareExact"/> or <see cref="CompareEpsilon"/>.</remarks>

        public double Epsilon
        {
            [DebuggerStepThrough]
            get { return _epsilon; }
            [DebuggerStepThrough]
            set
            {
                if (value < 0.0)
                    throw new System.ArgumentException("Out of range", "value");
                _epsilon = value;
            }
        }

        #endregion
        #region CompareEpsilon(Vector2d, Vector2d)

        /// <overloads>
        /// Compares two specified <see cref="Vector2d"/> instances and returns an indication of their
        /// lexicographic ordering, given an epsilon for coordinate comparisons.</overloads>
        /// <summary>
        /// Compares two specified <see cref="Vector2d"/> instances and returns an indication of their
        /// lexicographic ordering, given the current <see cref="Epsilon"/> for coordinate
        /// comparisons.</summary>
        /// <param name="a">
        /// The first <see cref="Vector2d"/> to compare.</param>
        /// <param name="b">
        /// The second <see cref="Vector2d"/> to compare.</param>
        /// <returns><list type="table"><listheader>
        /// <term>Value</term><description>Condition</description>
        /// </listheader><item>
        /// <term>Less than zero</term>
        /// <description><paramref name="a"/> is sorted before <paramref name="b"/>, given the
        /// current <see cref="Epsilon"/>.</description>
        /// </item><item>
        /// <term>Zero</term>
        /// <description><paramref name="a"/> and <paramref name="b"/> are equal, given the current
        /// <see cref="Epsilon"/>.</description>
        /// </item><item>
        /// <term>Greater than zero</term>
        /// <description><paramref name="a"/> is sorted after <paramref name="b"/>, given the
        /// current <see cref="Epsilon"/>.</description>
        /// </item></list></returns>
        /// <remarks>
        /// <b>CompareEpsilon</b> is identical with <see cref="Compare"/> but always calls <see
        /// cref="MathUtility.Compare"/> with the current <see cref="Epsilon"/> for coordinate
        /// comparisons. This is slightly faster if <see cref="Epsilon"/> is known to be positive.
        /// </remarks>

        public int CompareEpsilon(Vector2d a, Vector2d b)
        {
         

            double delta_X = a.X - b.X;
            
            if (Math.Abs(delta_X) > _epsilon)
            {
                return (delta_X < 0 ? -1 : 1);
            }

            double delta_Y = a.Y - b.Y;
            if (Math.Abs(delta_Y) > _epsilon)
            {
                return (delta_Y < 0 ? -1 : 1);
            }
            else
            {
                return 0;
            }
           
        }

        #endregion
        #region CompareEpsilon(Vector2d, Vector2d, Double)

        /// <summary>
        /// Compares two specified <see cref="Vector2d"/> instances and returns an indication of their
        /// lexicographic ordering, given the specified epsilon for coordinate comparisons.
        /// </summary>
        /// <param name="a">
        /// The first <see cref="Vector2d"/> to compare.</param>
        /// <param name="b">
        /// The second <see cref="Vector2d"/> to compare.</param>
        /// <param name="epsilon">
        /// The maximum absolute difference at which the coordinates of <paramref name="a"/> and
        /// <paramref name="b"/> should be considered equal.</param>
        /// <returns><list type="table"><listheader>
        /// <term>Value</term><description>Condition</description>
        /// </listheader><item>
        /// <term>Less than zero</term>
        /// <description><paramref name="a"/> is sorted before <paramref name="b"/>, given the
        /// specified <paramref name="epsilon"/>.</description>
        /// </item><item>
        /// <term>Zero</term>
        /// <description><paramref name="a"/> and <paramref name="b"/> are equal, given the
        /// specified <paramref name="epsilon"/>.</description>
        /// </item><item>
        /// <term>Greater than zero</term>
        /// <description><paramref name="a"/> is sorted after <paramref name="b"/>, given the
        /// specified <paramref name="epsilon"/>.</description>
        /// </item></list></returns>
        /// <remarks><para>
        /// <b>CompareEpsilon</b> is identical with <see cref="CompareExact"/> but calls <see
        /// cref="MathUtility.Compare"/> with the specified <paramref name="epsilon"/> for
        /// coordinate comparisons.
        /// </para><para>
        /// The specified <paramref name="epsilon"/> must be greater than zero, but
        /// <b>CompareEpsilon</b> does not check this condition.</para></remarks>

        public static int CompareEpsilon(Vector2d a, Vector2d b, double epsilon)
        {

            double delta_X = a.X - b.X;

            if (Math.Abs(delta_X) > epsilon)
            {
                return (delta_X < 0 ? -1 : 1);
            }

            double delta_Y = a.Y - b.Y;
            if (Math.Abs(delta_Y) > epsilon)
            {
                return (delta_Y < 0 ? -1 : 1);
            }
            else
            {
                return 0;
            }
        }

        #endregion
        #region CompareExact

        /// <summary>
        /// Compares two specified <see cref="Vector2d"/> instances and returns an indication of their
        /// lexicographic ordering, using exact coordinate comparisons.</summary>
        /// <param name="a">
        /// The first <see cref="Vector2d"/> to compare.</param>
        /// <param name="b">
        /// The second <see cref="Vector2d"/> to compare.</param>
        /// <returns><list type="table"><listheader>
        /// <term>Value</term><description>Condition</description>
        /// </listheader><item>
        /// <term>Less than zero</term>
        /// <description><paramref name="a"/> is sorted before <paramref name="b"/>.</description>
        /// </item><item>
        /// <term>Zero</term>
        /// <description><paramref name="a"/> and <paramref name="b"/> are equal.</description>
        /// </item><item>
        /// <term>Greater than zero</term>
        /// <description><paramref name="a"/> is sorted after <paramref name="b"/>.</description>
        /// </item></list></returns>
        /// <remarks>
        /// <b>CompareExact</b> determines the lexicographic ordering of <paramref name="a"/> and
        /// <paramref name="b"/> by first comparing their <see cref="Vector2d.X"/> coordinates, and in
        /// case of equality then comparing their <see cref="Vector2d.Y"/> coordinates. Smaller
        /// coordinates are sorted before greater coordinates.</remarks>

        public static int CompareExact(Vector2d a, Vector2d b)
        {

            if (a.X < b.X) return -1; if (a.X > b.X) return +1;
            if (a.Y < b.Y) return -1; if (a.Y > b.Y) return +1;
            return 0;
        }

        #endregion
        #region FindNearest
        public int FindNearest(IList<Vector2d> pnts, Vector2d q)
        {
            List<Vector2d> points = pnts as List<Vector2d>;
            if (points == null || points.Count == 0)
                throw new ArgumentNullException("points");
        

            int last = points.Count - 1;
            if (last == 0) return 0;

            // use binary search for lexicographic approximation
            // (see Vector2dComparerY for an alternative algorithm)
            int index = (_epsilon == 0 ?
               points.BinarySearch(q, Comparer<Vector2d>.Create(CompareExact)) :
                points.BinarySearch(q, Comparer<Vector2d>.Create(CompareEpsilon)));
            /*
             * Return immediate binary search hit only if Epsilon is zero.
             * Otherwise, we still need to search for nearer points in the vicinity,
             * as we might have found a non-nearest point within Epsilon distance.
             */

            if (index < 0)
                index = Math.Min(~index, points.Count - 1);
            else if (_epsilon == 0)
                return index;

            // restrict search radius to first approximation
            Vector2d vector = points[index] - q;
            double minDistance = vector.LengthSquared;
            if (minDistance == 0) return index;

            int minIndex = index;
            double epsilon2 = 2 * _epsilon;

            // expand search in both directions until radius exceeded
            bool searchPlus = true, searchMinus = true;
            for (int search = 1; searchPlus || searchMinus; search++)
            {

                if (searchPlus)
                {
                    int i = index + search;
                    if (i >= points.Count)
                        searchPlus = false;
                    else {
                        // check if we exceeded search radius
                        vector = points[i] - q;
                        double x = Math.Abs(vector.X) - epsilon2;
                        if (x * x - epsilon2 > minDistance)
                            searchPlus = false;
                        else {
                            // check if we found smaller distance
                            double distance = vector.LengthSquared;
                            if (minDistance > distance)
                            {
                                if (distance == 0) return i;
                                minDistance = distance;
                                minIndex = i;
                            }
                        }
                    }
                }

                if (searchMinus)
                {
                    int i = index - search;
                    if (i < 0)
                        searchMinus = false;
                    else {
                        // check if we exceeded search radius
                        vector = points[i] - q;
                        double x = Math.Abs(vector.X) - epsilon2;
                        if (x * x - epsilon2 > minDistance)
                            searchMinus = false;
                        else {
                            // check if we found smaller distance
                            double distance = vector.LengthSquared;
                            if (minDistance > distance)
                            {
                                if (distance == 0) return i;
                                minDistance = distance;
                                minIndex = i;
                            }
                        }
                    }
                }
            }

            return minIndex;
        }

#endregion


        #region IComparer Members

        /// <overloads>
        /// Compares two specified <see cref="Vector2d"/> instances and returns an indication of their
        /// lexicographic ordering.</overloads>
        /// <summary>
        /// Compares two specified objects, which must be <see cref="Vector2d"/> instances, and
        /// returns an indication of their lexicographic ordering.</summary>
        /// <param name="a">
        /// The first <see cref="Object"/> to compare.</param>
        /// <param name="b">
        /// The second <see cref="Object"/> to compare.</param>
        /// <returns><list type="table"><listheader>
        /// <term>Value</term><description>Condition</description>
        /// </listheader><item>
        /// <term>Less than zero</term>
        /// <description><paramref name="a"/> is sorted before <paramref name="b"/>.</description>
        /// </item><item>
        /// <term>Zero</term>
        /// <description><paramref name="a"/> and <paramref name="b"/> are equal.</description>
        /// </item><item>
        /// <term>Greater than zero</term>
        /// <description><paramref name="a"/> is sorted after <paramref name="b"/>.</description>
        /// </item></list></returns>
        /// <exception cref="ArgumentException">
        /// <paramref name="a"/> or <paramref name="b"/> is neither a <see cref="Vector2d"/> nor a
        /// null reference.</exception>
        /// <remarks><para>
        /// The specified <paramref name="a"/> and <paramref name="b"/> must both be either null
        /// references or <see cref="Vector2d"/> instances. Null references are always sorted before
        /// valid <see cref="Vector2d"/> instances. Two null references are considered equal.
        /// </para><para>
        /// <b>Compare</b> determines the relative order of the two instances by calling the
        /// strongly-typed <see cref="Compare(Vector2d, Vector2d)"/> overload.</para></remarks>

        public int Compare(object a, object b)
        {

            if (a == null)
            {
                if (b == null) return 0;
                if (!(b is Vector2d))
                    throw new System.ArgumentException("invalid argument", "b");

                return -1;
            }

            if (!(a is Vector2d))
                throw new System.ArgumentException("invalid argument", "a");

            if (b == null) return 1;
            if (!(b is Vector2d))
                throw new System.ArgumentException("invalid argument", "b");

            return Compare((Vector2d)a, (Vector2d)b);
        }

        #endregion
        #region IComparer<Vector2d> Members

        /// <summary>
        /// Compares two specified <see cref="Vector2d"/> instances and returns an indication of their
        /// lexicographic ordering.</summary>
        /// <param name="a">
        /// The first <see cref="Vector2d"/> to compare.</param>
        /// <param name="b">
        /// The second <see cref="Vector2d"/> to compare.</param>
        /// <returns><list type="table"><listheader>
        /// <term>Value</term><description>Condition</description>
        /// </listheader><item>
        /// <term>Less than zero</term>
        /// <description><paramref name="a"/> is sorted before <paramref name="b"/>.</description>
        /// </item><item>
        /// <term>Zero</term>
        /// <description><paramref name="a"/> and <paramref name="b"/> are equal.</description>
        /// </item><item>
        /// <term>Greater than zero</term>
        /// <description><paramref name="a"/> is sorted after <paramref name="b"/>.</description>
        /// </item></list></returns>
        /// <remarks>
        /// <b>Compare</b> returns the result of either <see cref="CompareExact"/> or <see
        /// cref="CompareEpsilon"/> for <paramref name="a"/> and <paramref name="b"/>, depending on
        /// whether the current <see cref="Epsilon"/> equals zero.</remarks>

        public int Compare(Vector2d a, Vector2d b)
        {
            return (_epsilon == 0 ? CompareExact(a, b) : CompareEpsilon(a, b, _epsilon));
        }

        #endregion
    }
}


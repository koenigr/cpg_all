﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPlan.Utilities;
namespace CPlan.Geometry
{
    public struct Vector2I : IEquatable<Vector2I>
    {
        #region Vector2i(Int32, Int32)

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector2I"/> structure with the specified
        /// coordinates.</summary>
        /// <param name="x">
        /// The <see cref="X"/> coordinate of the <see cref="Vector2I"/>.</param>
        /// <param name="y">
        /// The <see cref="Y"/> coordinate of the <see cref="Vector2I"/>.</param>

        public Vector2I(int x, int y)
        {
            X = x;
            Y = y;
        }

        #endregion
        #region Empty

        /// <summary>
        /// An empty read-only <see cref="Vector2I"/> instance.</summary>
        /// <remarks>
        /// <b>Empty</b> contains a <see cref="Vector2I"/> instance that was created with the default
        /// constructor.</remarks>

        public static readonly Vector2I Empty = new Vector2I();

        #endregion
        #region X

        /// <summary>
        /// The x-coordinate of the <see cref="Vector2I"/>.</summary>

        public readonly int X;

        #endregion
        #region Y

        /// <summary>
        /// The y-coordinate of the <see cref="Vector2I"/>.</summary>

        public readonly int Y;

        #endregion
        #region Angle

        /// <summary>
        /// Gets the polar angle of the vector represented by the <see cref="Vector2I"/>, in radians.
        /// </summary>
        /// <value><para>
        /// The polar angle, in radians, of the vector represented by the <see cref="Vector2I"/>.
        /// </para><para>-or-</para><para>
        /// Zero if <see cref="X"/> and <see cref="Y"/> both equal zero.</para></value>
        /// <remarks>
        /// <b>Angle</b> returns the result of <see cref="Math.Atan2"/> for the <see cref="Y"/> and
        /// <see cref="X"/> coordinates. The possible range of values is (-<see cref="Math.PI"/>,
        /// +<see cref="Math.PI"/>].</remarks>

        public double Angle
        {
            get { return Math.Atan2(Y, X); }
        }

        #endregion
        #region Length

        /// <summary>
        /// Gets the absolute length of the vector represented by the <see cref="Vector2I"/>.
        /// </summary>
        /// <value>
        /// A non-negative <see cref="Double"/> value indicating the absolute length of the vector
        /// represented by the <see cref="Vector2I"/>.</value>
        /// <remarks>
        /// <b>Length</b> returns the square root of the sum of the squares of the <see cref="X"/>
        /// and <see cref="Y"/> coordinates.</remarks>

        public double Length
        {
            get { return Math.Sqrt(X * X + Y * Y); }
        }

        #endregion
        #region LengthSquared

        /// <summary>
        /// Gets the squared absolute length of the vector represented by the <see cref="PointF"/>.
        /// </summary>
        /// <value>
        /// A non-negative <see cref="Int32"/> value that equals the square of the <see
        /// cref="Length"/> property.</value>
        /// <remarks>
        /// <b>LengthSquared</b> performs the same operations as <see cref="Length"/> but without
        /// the final <see cref="Math.Sqrt"/> call, and is therefore faster if you only need the
        /// squared <see cref="Length"/>.</remarks>

        public int LengthSquared
        {
            get { return (X * X + Y * Y); }
        }

        #endregion
        #region Public Methods
        #region Add

        /// <summary>
        /// Adds the coordinates of the specified <see cref="Vector2I"/> to this instance.</summary>
        /// <param name="point">
        /// The <see cref="Vector2I"/> whose coordinates to add to this instance.</param>
        /// <returns>
        /// A <see cref="Vector2I"/> whose <see cref="X"/> and <see cref="Y"/> properties equal the
        /// corresponding properties of the specified <paramref name="point"/> added to those of
        /// this instance.</returns>

        public Vector2I Add(Vector2I point)
        {
            return new Vector2I(X + point.X, Y + point.Y);
        }

        #endregion
        #region AngleBetween(Vector2i)

        /// <overloads>
        /// Computes the angle between two <see cref="Vector2I"/> vectors.</overloads>
        /// <summary>
        /// Computes the angle between the vector represented by this instance and the specified
        /// <see cref="Vector2I"/> vector.</summary>
        /// <param name="vector">
        /// The <see cref="Vector2I"/> vector to compare with this instance.</param>
        /// <returns>
        /// The angle, in radians, between this instance and the specified <paramref
        /// name="vector"/>, in that order.</returns>
        /// <remarks>
        /// <b>AngleBetween</b> returns the result of <see cref="Math.Atan2"/> for the cross product
        /// length and the scalar dot product of the two vectors. The possible range of values is
        /// (-<see cref="Math.PI"/>, +<see cref="Math.PI"/>].</remarks>

        public double AngleBetween(Vector2I vector)
        {

            double y = X * vector.Y - Y * vector.X;
            double x = X * vector.X + Y * vector.Y;

            return Math.Atan2(y, x);
        }

        #endregion
        #region AngleBetween(Vector2i, Vector2i)

        /// <summary>
        /// Computes the angle between the vectors from this instance to the specified <see
        /// cref="Vector2I"/> coordinates.</summary>
        /// <param name="a">
        /// The <see cref="Vector2I"/> coordinates where the first vector ends.</param>
        /// <param name="b">
        /// The <see cref="Vector2I"/> coordinates where the second vector ends.</param>
        /// <returns>
        /// The angle, in radians, between the vectors from this instance to <paramref name="a"/>
        /// and from this instance to <paramref name="b"/>, in that order.</returns>
        /// <remarks>
        /// <b>AngleBetween</b> returns the result of <see cref="Math.Atan2"/> for the cross product
        /// length and the scalar dot product of the two vectors. The possible range of values is
        /// (-<see cref="Math.PI"/>, +<see cref="Math.PI"/>].</remarks>

        public double AngleBetween(Vector2I a, Vector2I b)
        {

            double ax = a.X - X, ay = a.Y - Y;
            double bx = b.X - X, by = b.Y - Y;

            double y = ax * by - ay * bx;
            double x = ax * bx + ay * by;

            return Math.Atan2(y, x);
        }

        #endregion
        #region CrossProductLength(Vector2i)

        /// <overloads>
        /// Computes the length of the cross-product of two <see cref="Vector2I"/> vectors.
        /// </overloads>
        /// <summary>
        /// Computes the length of the cross-product of the vector represented by this instance and
        /// the specified <see cref="Vector2I"/> vector.</summary>
        /// <param name="vector">
        /// The <see cref="Vector2I"/> vector to multiply with this instance.</param>
        /// <returns>
        /// An <see cref="Int32"/> value indicating the length of the cross-product of this instance
        /// and the specified <paramref name="vector"/>, in that order.</returns>
        /// <remarks>
        /// The absolute value of <b>CrossProductLength</b> equals the area of the parallelogram
        /// spanned by this instance and the specified <paramref name="vector"/>. The sign indicates
        /// their spatial relationship, which is described in the other <see
        /// cref="CrossProductLength(Vector2I, Vector2I)"/> overload.</remarks>

        public int CrossProductLength(Vector2I vector)
        {
            return X * vector.Y - vector.X * Y;
        }

        #endregion
        #region CrossProductLength(Vector2i, Vector2i)

        /// <summary>
        /// Computes the length of the cross-product of the vectors from this instance to the
        /// specified <see cref="Vector2I"/> coordinates.</summary>
        /// <param name="a">
        /// The <see cref="Vector2I"/> coordinates where the first vector ends.</param>
        /// <param name="b">
        /// The <see cref="Vector2I"/> coordinates where the second vector ends.</param>
        /// <returns>
        /// An <see cref="Int32"/> value indicating the length of the cross-product of the vectors
        /// from this instance to <paramref name="a"/> and from this instance to <paramref
        /// name="b"/>, in that order.</returns>
        /// <remarks><para>
        /// The absolute value of <b>CrossProductLength</b> equals the area of the parallelogram
        /// spanned by the two vectors from the current coordinates to <paramref name="a"/> and
        /// <paramref name="b"/>. The sign indicates their spatial relationship, as follows:
        /// </para><list type="table"><listheader>
        /// <term>Return Value</term><description>Relationship</description>
        /// </listheader><item>
        /// <term>Less than zero</term><description>
        /// The sequence from this instance to <paramref name="a"/> and then <paramref name="b"/>
        /// constitutes a right-hand turn, assuming y-coordinates increase upward.</description>
        /// </item><item>
        /// <term>Zero</term><description>
        /// This instance, <paramref name="a"/>, and <paramref name="b"/> are collinear.
        /// </description></item><item>
        /// <term>Greater than zero</term><description>
        /// The sequence from this instance to <paramref name="a"/> and then <paramref name="b"/>
        /// constitutes a left-hand turn, assuming y-coordinates increase upward.</description>
        /// </item></list></remarks>

        public int CrossProductLength(Vector2I a, Vector2I b)
        {
            return (a.X - X) * (b.Y - Y) - (b.X - X) * (a.Y - Y);
        }

        #endregion
        #region FromPolar

        /// <summary>
        /// Creates a <see cref="Vector2I"/> from the specified polar coordinates.</summary>
        /// <param name="length">
        /// The distance from the origin to the <see cref="Vector2I"/>.</param>
        /// <param name="angle">
        /// The polar angle, in radians, of the <see cref="Vector2I"/>.</param>
        /// <returns>
        /// A <see cref="Vector2I"/> whose <see cref="Length"/> and <see cref="Angle"/> equal the
        /// specified <paramref name="length"/> and <paramref name="angle"/>.</returns>
        /// <remarks><para>
        /// <b>FromPolar</b> returns <see cref="Empty"/> if the specified <paramref name="length"/>
        /// equals zero, and inverts the signs of the <see cref="X"/> and <see cref="Y"/>
        /// coordinates if <paramref name="length"/> is less than zero.
        /// </para><para>
        /// The calculated <see cref="X"/> and <see cref="Y"/> coordinates are converted to the
        /// nearest <see cref="Int32"/> values using <see cref="Fortran.NInt"/> rounding. The
        /// resulting <see cref="Length"/> and <see cref="Angle"/> may differ accordingly from the
        /// specified <paramref name="length"/> and <paramref name="angle"/>.</para></remarks>

        public static Vector2I FromPolar(double length, double angle)
        {
            return new Vector2I(
                Fortran.NInt(length * Math.Cos(angle)),
                Fortran.NInt(length * Math.Sin(angle)));
        }

        #endregion
        #region GetHashCode

        /// <summary>
        /// Returns the hash code for this <see cref="Vector2I"/> instance.</summary>
        /// <returns>
        /// An <see cref="Int32"/> hash code.</returns>
        /// <remarks>
        /// <b>GetHashCode</b> combines the values of the <see cref="X"/> and <see cref="Y"/>
        /// properties.</remarks>

        public override int GetHashCode()
        {
            unchecked { return X ^ Y; }
        }

        #endregion
        #region IsCollinear

        /// <summary>
        /// Determines whether this instance is collinear with the specified <see cref="Vector2I"/>
        /// instances.</summary>
        /// <param name="a">
        /// The first <see cref="Vector2I"/> instance to examine.</param>
        /// <param name="b">
        /// The second <see cref="Vector2I"/> instance to examine.</param>
        /// <returns>
        /// <c>true</c> if the <see cref="Vector2I"/> is collinear with <paramref name="a"/> and
        /// <paramref name="b"/>; otherwise, <c>false</c>.</returns>
        /// <remarks>
        /// <b>IsCollinear</b> returns <c>true</c> exactly if <see cref="CrossProductLength(Vector2I,
        /// Vector2I)"/> returns zero for <paramref name="a"/> and <paramref name="b"/>.</remarks>

        public bool IsCollinear(Vector2I a, Vector2I b)
        {
            return (CrossProductLength(a, b) == 0);
        }

        #endregion
        #region Multiply

        /// <summary>
        /// Multiplies the vectors represented by the specified <see cref="Vector2I"/> and by this
        /// instance.</summary>
        /// <param name="vector">
        /// The <see cref="Vector2I"/> to multiply with this instance.</param>
        /// <returns>
        /// An <see cref="Int32"/> value that represents the scalar dot product of the specified
        /// <paramref name="vector"/> and this instance.</returns>
        /// <remarks>
        /// <b>Multiply</b> returns the sum of the pairwise products of the <see cref="X"/> and <see
        /// cref="Y"/> coordinates of both instances. That sum equals <see cref="LengthSquared"/> if
        /// the specified <paramref name="vector"/> equals this instance.</remarks>

        public int Multiply(Vector2I vector)
        {
            return X * vector.X + Y * vector.Y;
        }

        #endregion
        #region Restrict

        /// <summary>
        /// Restricts the <see cref="Vector2I"/> to the specified coordinate range.</summary>
        /// <param name="minX">
        /// The smallest permissible <see cref="X"/> coordinate.</param>
        /// <param name="minY">
        /// The smallest permissible <see cref="Y"/> coordinate.</param>
        /// <param name="maxX">
        /// The greatest permissible <see cref="X"/> coordinate.</param>
        /// <param name="maxY">
        /// The greatest permissible <see cref="Y"/> coordinate.</param>
        /// <returns>
        /// A <see cref="Vector2I"/> whose <see cref="X"/> and <see cref="Y"/> coordinates equal those
        /// of this instance, restricted to the indicated coordinate range.</returns>

        public Vector2I Restrict(int minX, int minY, int maxX, int maxY)
        {
            int x = X, y = Y;

            if (x < minX) x = minX; else if (x > maxX) x = maxX;
            if (y < minY) y = minY; else if (y > maxY) y = maxY;

            return new Vector2I(x, y);
        }

        #endregion
        #region Subtract

        /// <summary>
        /// Subtracts the coordinates of the specified <see cref="Vector2I"/> from this instance.
        /// </summary>
        /// <param name="point">
        /// The <see cref="Vector2I"/> whose coordinates to subtract from this instance.</param>
        /// <returns>
        /// A <see cref="Vector2I"/> whose <see cref="X"/> and <see cref="Y"/> properties equal the
        /// corresponding properties of the specified <paramref name="point"/> subtracted from those
        /// of this instance.</returns>

        public Vector2I Subtract(Vector2I point)
        {
            return new Vector2I(X - point.X, Y - point.Y);
        }

        #endregion
        #region ToString

        /// <summary>
        /// Returns a <see cref="String"/> that represents the <see cref="Vector2I"/>.</summary>
        /// <returns>
        /// A <see cref="String"/> containing the values of the <see cref="X"/> and <see cref="Y"/>
        /// properties.</returns>

        public override string ToString()
        {
            return String.Format(CultureInfo.InvariantCulture, "{{X={0}, Y={1}}}", X, Y);
        }

        #endregion
        #endregion
        #region Public Operators
        #region operator==

        /// <summary>
        /// Determines whether two <see cref="Vector2I"/> instances have the same value.</summary>
        /// <param name="a">
        /// The first <see cref="Vector2I"/> to compare.</param>
        /// <param name="b">
        /// The second <see cref="Vector2I"/> to compare.</param>
        /// <returns>
        /// <c>true</c> if the value of <paramref name="a"/> is the same as the value of <paramref
        /// name="b"/>; otherwise, <c>false</c>.</returns>
        /// <remarks>
        /// This operator invokes the <see cref="Equals(Vector2I)"/> method to test the two <see
        /// cref="Vector2I"/> instances for value equality.</remarks>

        public static bool operator ==(Vector2I a, Vector2I b)
        {
            return a.Equals(b);
        }

        #endregion
        #region operator!=

        /// <summary>
        /// Determines whether two <see cref="Vector2I"/> instances have different values.</summary>
        /// <param name="a">
        /// The first <see cref="Vector2I"/> to compare.</param>
        /// <param name="b">
        /// The second <see cref="Vector2I"/> to compare.</param>
        /// <returns>
        /// <c>true</c> if the value of <paramref name="a"/> is different from the value of
        /// <paramref name="b"/>; otherwise, <c>false</c>.</returns>
        /// <remarks>
        /// This operator invokes the <see cref="Equals(Vector2I)"/> method to test the two <see
        /// cref="Vector2I"/> instances for value inequality.</remarks>

        public static bool operator !=(Vector2I a, Vector2I b)
        {
            return !a.Equals(b);
        }

        #endregion
        #region operator+

        /// <summary>
        /// Adds the coordinates of two <see cref="Vector2I"/> instances.</summary>
        /// <param name="a">
        /// The first <see cref="Vector2I"/> to add.</param>
        /// <param name="b">
        /// The second <see cref="Vector2I"/> to add.</param>
        /// <returns>
        /// A <see cref="Vector2I"/> whose <see cref="X"/> and <see cref="Y"/> properties equal the
        /// corresponding properties of <paramref name="a"/> added to those of <paramref name="b"/>.
        /// </returns>
        /// <remarks>
        /// This operator invokes <see cref="Add"/> to add the coordinates of the two <see
        /// cref="Vector2I"/> instances.</remarks>

        public static Vector2I operator +(Vector2I a, Vector2I b)
        {
            return a.Add(b);
        }

        #endregion
        #region operator*

        /// <summary>
        /// Multiplies the vectors represented by two <see cref="Vector2I"/> instances.</summary>
        /// <param name="a">
        /// The first <see cref="Vector2I"/> to multiply.</param>
        /// <param name="b">
        /// The second <see cref="Vector2I"/> to multiply.</param>
        /// <returns>
        /// An <see cref="Int32"/> value that represents the scalar dot product of <paramref
        /// name="a"/> and <paramref name="b"/>.</returns>
        /// <remarks>
        /// This operator invokes <see cref="Multiply"/> to multiply the vectors represented by the
        /// two <see cref="Vector2I"/> instances.</remarks>

        public static int operator *(Vector2I a, Vector2I b)
        {
            return a.Multiply(b);
        }

        #endregion
        #region operator-

        /// <summary>
        /// Subtracts the coordinates of two <see cref="Vector2I"/> instances.</summary>
        /// <param name="a">
        /// The <see cref="Vector2I"/> to subtract from.</param>
        /// <param name="b">
        /// The <see cref="Vector2I"/> to subtract.</param>
        /// <returns>
        /// A <see cref="Vector2I"/> whose <see cref="X"/> and <see cref="Y"/> properties equal the
        /// corresponding properties of <paramref name="a"/> subtracted from those of <paramref
        /// name="b"/>.</returns>
        /// <remarks>
        /// This operator invokes <see cref="Subtract"/> to subtract the coordinates of the two <see
        /// cref="Vector2I"/> instances.</remarks>

        public static Vector2I operator -(Vector2I a, Vector2I b)
        {
            return a.Subtract(b);
        }

        #endregion
        #endregion
        #region IEquatable Members
        #region Equals(Object)

        /// <overloads>
        /// Determines whether two <see cref="Vector2I"/> instances have the same value.</overloads>
        /// <summary>
        /// Determines whether this <see cref="Vector2I"/> instance and a specified object, which must
        /// be a <see cref="Vector2I"/>, have the same value.</summary>
        /// <param name="obj">
        /// An <see cref="Object"/> to compare to this <see cref="Vector2I"/> instance.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="obj"/> is another <see cref="Vector2I"/> instance and its
        /// value is the same as this instance; otherwise, <c>false</c>.</returns>
        /// <remarks>
        /// If the specified <paramref name="obj"/> is another <see cref="Vector2I"/> instance,
        /// <b>Equals</b> invokes the strongly-typed <see cref="Equals(Vector2I)"/> overload to test
        /// the two instances for value equality.</remarks>

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Vector2I))
                return false;

            return Equals((Vector2I)obj);
        }

        #endregion
        #region Equals(Vector2i)

        /// <summary>
        /// Determines whether this instance and a specified <see cref="Vector2I"/> have the same
        /// value.</summary>
        /// <param name="point">
        /// A <see cref="Vector2I"/> to compare to this instance.</param>
        /// <returns>
        /// <c>true</c> if the value of <paramref name="point"/> is the same as this instance;
        /// otherwise, <c>false</c>.</returns>
        /// <remarks>
        /// <b>Equals</b> compares the values of the <see cref="X"/> and <see cref="Y"/> properties
        /// of the two <see cref="Vector2I"/> instances to test for value equality.</remarks>

        public bool Equals(Vector2I point)
        {
            return (X == point.X && Y == point.Y);
        }

        #endregion
        #region Equals(Vector2i, Vector2i)

        /// <summary>
        /// Determines whether two specified <see cref="Vector2I"/> instances have the same value.
        /// </summary>
        /// <param name="a">
        /// The first <see cref="Vector2I"/> to compare.</param>
        /// <param name="b">
        /// The second <see cref="Vector2I"/> to compare.</param>
        /// <returns>
        /// <c>true</c> if the value of <paramref name="a"/> is the same as the value of <paramref
        /// name="b"/>; otherwise, <c>false</c>.</returns>
        /// <remarks>
        /// <b>Equals</b> invokes the non-static <see cref="Equals(Vector2I)"/> overload to test the
        /// two <see cref="Vector2I"/> instances for value equality.</remarks>

        public static bool Equals(Vector2I a, Vector2I b)
        {
            return a.Equals(b);
        }

        #endregion
        #endregion
    }
}

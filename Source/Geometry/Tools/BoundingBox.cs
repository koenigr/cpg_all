﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = BoundingBox.cs 
//  Copyright (C) 2014/10/18  12:58 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using OpenTK;

namespace CPlan.Geometry
{
    [Serializable]
    public class BoundingBox
    {
        Vector3d m_min, m_max;

        //==============================================================================================
        public BoundingBox()
        {
            m_min = new Vector3d(double.MaxValue, double.MaxValue, double.MaxValue);
            m_max = new Vector3d(double.MinValue, double.MinValue, double.MinValue);
        }

        //==============================================================================================
        public void AddPoint(Vector3d point)
        {
            if (point.X < m_min.X) m_min.X = point.X;
            if (point.Y < m_min.Y) m_min.Y = point.Y;
            if (point.Z < m_min.Z) m_min.Z = point.Z;

            if (point.X > m_max.X) m_max.X = point.X;
            if (point.Y > m_max.Y) m_max.Y = point.Y;
            if (point.Z > m_max.Z) m_max.Z = point.Z;
        }

        //==============================================================================================
        public void AddPoint(Vector2d point)
        {
            if (point.X < m_min.X) m_min.X = point.X;
            if (point.Y < m_min.Y) m_min.Y = point.Y;
            if (m_min.Z > 0) m_min.Z = 0;

            if (point.X > m_max.X) m_max.X = point.X;
            if (point.Y > m_max.Y) m_max.Y = point.Y;
            if (m_max.Z < 0) m_max.Z = 0;
        }

        //==============================================================================================
        public Vector3d GetMin()
        {
            return m_min;
        }

        //==============================================================================================
        public Vector3d GetMax()
        {
            return m_max;
        }

        //==============================================================================================
        public bool IsValid()
        {
            return ((m_min.X <= m_max.X) && (m_min.Y <= m_max.Y) && (m_min.Y <= m_max.Y));
        }

    }
}

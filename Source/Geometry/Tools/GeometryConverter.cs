﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GeometryConverter.cs 
//  Copyright (C) 2014/10/18  12:58 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
//using System.Linq;
using GeoAPI.Geometries;
using NetTopologySuite.Geometries;
//using QuickGraph;

using OpenTK;
using Tektosyne.Geometry;

namespace CPlan.Geometry

{
    public class GeometryConverter
    {
        

        public static Coordinate[] ToCoordinates(Poly2D poly)
        {
            List<Coordinate> result = new List<Coordinate>();
            foreach (Vector2d point in poly.Points[0])
            {
                result.Add(new Coordinate(point.X, point.Y));
            }
            result.Add(new Coordinate(poly.Points[0][0].X, poly.Points[0][0].Y));
            return result.ToArray();
        }

    
        public static Coordinate[] ToCoordinates(Line2D line)
        {
            List<Coordinate> result = new List<Coordinate>();
            result.Add(new Coordinate(line.Start.X, line.Start.Y, 0));
            result.Add(new Coordinate(line.End.X, line.End.Y, 0));
            return result.ToArray();
        }

        public static Coordinate ToCoordinates(Vector2d point)
        {
            Coordinate result = new Coordinate(point.X, point.Y);
            return result;
        }

        public static Poly2D ToPoly2D(Coordinate[] coordinates)
        {
            Poly2D resultPoly;
            List<Vector2d> polyPts = new List<Vector2d>();
            foreach (Coordinate point in coordinates)
            {
                polyPts.Add(new Vector2d(point.X, point.Y));
            }
            resultPoly = new Poly2D(polyPts.ToArray());
            return resultPoly;
        }

        public static Point ToPoint(Vector2d point)
        {
            return new Point(point.X, point.Y);
        }

        public static LineString ToLineString(Line2D line, int roundPrec = -1)
        {
            
            if (roundPrec == -1)
            {
                return new LineString(new Coordinate[] 
            { 
                new Coordinate(line.Start.X, line.Start.Y, 0), 
                new Coordinate(line.End.X, line.End.Y, 0) 
            }
            );
            }
            else
            {
                return new LineString(new Coordinate[] 
            { 
                new Coordinate(Math.Round(line.Start.X, roundPrec), Math.Round(line.Start.Y, roundPrec), 0), 
                new Coordinate(Math.Round(line.End.X, roundPrec), Math.Round(line.End.Y, roundPrec), 0) 
            }
                );
            }

        }

        public static Vector2d ToVector2d(IPoint point)
        {
            return new Vector2d(point.X, point.Y);
        }

        public static Line2D ToLine2D(ILineString line)
        {
            return new Line2D(line.StartPoint.X, line.StartPoint.Y, line.EndPoint.X, line.EndPoint.Y);
        }

        public static MultiPoint ToMultiPoint(List<Vector2d> points)
        {
            Point[] tmp = new Point[points.Count];

            for (int i = 0; i < points.Count; i++)
            {
                tmp[i] = ToPoint(points[i]);
            }

            return new MultiPoint(tmp);
        }

        public static List<Vector2d> ToVector2dList(MultiPoint points)
        {
            List<Vector2d> result = new List<Vector2d>();

            foreach (IPoint point in points.Geometries)
            {
                result.Add(ToVector2d(point));
            }

            return result;
        }


        public static MultiLineString ToMultiLineString(List<Line2D> lines)
        {
            LineString[] tmp = new LineString[lines.Count];

            for (int i = 0; i < lines.Count; i++)
            {
                tmp[i] = ToLineString(lines[i]);
            }

            return new MultiLineString(tmp);
        }

        public static List<Line2D> ToLine2DList(MultiLineString lines)
        {
            List<Line2D> result = new List<Line2D>();

            foreach (ILineString line in lines.Geometries)
            {
                result.Add(ToLine2D(line));
            }

            return result;
        }

        

        //============================================================================================================================================================================
        /// <summary>
        /// Convert a list of Line2D to a list of LineD (Tektosyne class).
        /// </summary>
        /// <param name="lineList"></param>
        /// <returns></returns>
        public static List<Line2D> ToListLine2D(List<LineD> lineList)
        {
            List<Line2D> Line2DList = new List<Line2D>();

            Line2D toAddLine, reverseLine;
            // --- check for double lines -> check coordinates! ---
            foreach (LineD line in lineList)
            {
                //bool contains = false;
                Vector2d start = new Vector2d(line.Start.X, line.Start.Y);
                Vector2d end = new Vector2d(line.Start.X, line.Start.Y);
                
                toAddLine = new Line2D(start, end);
                //reverseLine = new Line2D (line.End.ToVector2d(), line.Start.ToVector2d());
                //foreach (Line2D tempLine in tektoLineList)
                //{                   
                //    if (tempLine.Start.X == toAddLine.Start.X && tempLine.Start.Y == toAddLine.Start.Y
                //        && tempLine.End.X == toAddLine.End.X && tempLine.End.Y == toAddLine.End.Y)
                //        contains = true;
                //    else if (tempLine.Start.X == reverseLine.Start.X && tempLine.Start.Y == reverseLine.Start.Y
                //        && tempLine.End.X == reverseLine.End.X && tempLine.End.Y == reverseLine.End.Y)
                //        contains = true;
                //}
                //if (!contains)
               Line2DList.Add(toAddLine);
            }
            return Line2DList;
        }

     
        //============================================================================================================================================================================
        /// <summary>
        /// Convert an array of polygons with Vector2d (Tektosyne class) coordinates to a list of Poly2D.
        /// </summary>
        /// <param name="inPolygons"></param>
        /// <returns></returns>
        public static List<Poly2D> ToListPoly2D(Vector2d[][] inPolygons)
        {
            List<Vector2d> polyPts;
            List<Poly2D> outPlolygons = new List<Poly2D>();
            int nrPolys = inPolygons.GetLength(0);
            for (int i = 0; i < nrPolys; i++)
            {
                polyPts = new List<Vector2d>();
                foreach (Vector2d pt in inPolygons[i])
                {
                    polyPts.Add(new Vector2d(pt.X, pt.Y)) ;
                }
                outPlolygons.Add(new Poly2D(polyPts.ToArray()));
            }
            return outPlolygons;
        }

        public static Poly2D ToPoly2D(Vector2d[] inPolygon)
        {

                List<Vector2d>  polyPnts = new List<Vector2d>();
            
                foreach (Vector2d pt in inPolygon)
                {
                    polyPnts.Add(new Vector2d(pt.X, pt.Y));
                }
                
                return new Poly2D(polyPnts.ToArray());
      

        }
        public static IList<Vector2d[]> ToListofVector2dArray(List<Poly2D> inPolygons)
        {
            
            
            int nrPolys = inPolygons.Count;
            IList<Vector2d[]> outPolygons = new List<Vector2d[]>(); 
            foreach(Poly2D polygon in inPolygons) {
                int nodeNum = polygon.Points[0].Length;
                Vector2d[] polyPnts = new Vector2d[nodeNum];
                for (int j = 0; j < nodeNum; ++j)
                    {

                    polyPnts[j].X = polygon.Points[0][j].X;
                    polyPnts[j].Y = polygon.Points[0][j].Y;
                    }
                outPolygons.Add(polyPnts);
                    

            }
            return outPolygons;

        }
        public static IList<PointD[]> ToListofPointDs(List<Poly2D> polys)
        {
            List<PointD[]> results = new List<PointD[]>();
            foreach (Poly2D polygon in polys)
            {
                int nodeNum = polygon.Points[0].Length;
                PointD[] polyPnts = new PointD[nodeNum];
                for (int j = 0; j < nodeNum; ++j)
                {
                    polyPnts[j].X = polygon.Points[0][j].X;
                    polyPnts[j].Y = polygon.Points[0][j].Y;
                }

                results.Add(polyPnts);
            }
        
            return results;
        }


    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Tools.cs 
//  Copyright (C) 2014/10/18  12:58 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using Troschuetz.Random.Distributions.Continuous;
using Troschuetz.Random.Generators;
using System.Collections.ObjectModel;
using System.Drawing;

namespace CPlan.Geometry
{

    //============================================================================================================================================================================
    [Serializable]
    public static class Tools
    {

        //============================================================================================================================================================================
        public static NormalDistribution NormDist = new NormalDistribution();

        public static void NewNormDistGenerator()
        {
            ALFGenerator newGenerator = new ALFGenerator((int) DateTime.Now.Ticks);
            NormDist = new NormalDistribution(newGenerator);
        }

        //============================================================================================================================================================================
        public static int Faculty(int n)
        {
            return n == 0 ? 1 : n*Faculty(n - 1);
        }

        //==============================================================================================   
        //==============================================================================================

        #region Color Generators

        //============================================================================================================================================================================



        /// <summary>
        /// List of 128 distinct color values.
        /// Source: http://stackoverflow.com/questions/2328339/how-to-generate-n-different-colors-for-any-natural-number-n
        /// </summary>
        public static String[] ColorsIndex = new String[]
        {
            "#FFFF00", "#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
            "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
            "#5A0007", "#809693", "#FEFFE6", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
            "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100",
            "#DDEFFF", "#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
            "#372101", "#FFB500", "#C2FFED", "#A079BF", "#CC0744", "#C0B9B2", "#C2FF99", "#001E09",
            "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68", "#7A87A1", "#788D66",
            "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED", "#886F4C",

            "#34362D", "#B4A8BD", "#00A6AA", "#452C2C", "#636375", "#A3C8C9", "#FF913F", "#938A81",
            "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1", "#1E6E00",
            "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
            "#549E79", "#FFF69F", "#201625", "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329",
            "#5B4534", "#FDE8DC", "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C",
            "#83AB58", "#001C1E", "#D1F7CE", "#004B28", "#C8D0F6", "#A3A489", "#806C66", "#222800",
            "#BF5650", "#E83000", "#66796D", "#DA007C", "#FF1A59", "#8ADBB4", "#1E0200", "#5B4E51",
            "#C895C5", "#320033", "#FF6832", "#66E1D3", "#CFCDAC", "#D0AC94", "#7ED379", "#012C58"
        };

        //============================================================================================================================================================================
        /// <summary>
        /// Convert the ColroIndex to a list of colors.
        /// </summary>
        /// <returns>List of colors.</returns>
        public static List<Color> ColorsIndexList()
        {
            List<Color> colorList = new List<Color>();
            for (int i = 0; i < ColorsIndex.Length; i ++)
            {
                colorList.Add(HexToColor(ColorsIndex[i]));
            }
            return colorList;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// 20 distinct color values from http://stackoverflow.com/questions/470690/how-to-automatically-generate-n-distinct-colors
        /// </summary>
        public static readonly List<Color> ColorsKellysMaxContrast = new List<Color>
        {
            UIntToColor(0xFFFFB300), //Vivid Yellow
            UIntToColor(0xFF803E75), //Strong Purple
            UIntToColor(0xFFFF6800), //Vivid Orange
            UIntToColor(0xFFA6BDD7), //Very Light Blue
            UIntToColor(0xFFC10020), //Vivid Red
            UIntToColor(0xFFCEA262), //Grayish Yellow
            UIntToColor(0xFF817066), //Medium Gray

            //The following will not be good for people with defective color vision
            UIntToColor(0xFF007D34), //Vivid Green
            UIntToColor(0xFFF6768E), //Strong Purplish Pink
            UIntToColor(0xFF00538A), //Strong Blue
            UIntToColor(0xFFFF7A5C), //Strong Yellowish Pink
            UIntToColor(0xFF53377A), //Strong Violet
            UIntToColor(0xFFFF8E00), //Vivid Orange Yellow
            UIntToColor(0xFFB32851), //Strong Purplish Red
            UIntToColor(0xFFF4C800), //Vivid Greenish Yellow
            UIntToColor(0xFF7F180D), //Strong Reddish Brown
            UIntToColor(0xFF93AA00), //Vivid Yellowish Green
            UIntToColor(0xFF593315), //Deep Yellowish Brown
            UIntToColor(0xFFF13A13), //Vivid Reddish Orange
            UIntToColor(0xFF232C16), //Dark Olive Green
        };

        //============================================================================================================================================================================
        /// <summary>
        /// 9 distinct color values from http://stackoverflow.com/questions/470690/how-to-automatically-generate-n-distinct-colors
        /// </summary>
        public static readonly List<Color> ColorsBoyntonOptimized = new List<Color>
        {
            Color.FromArgb(0, 0, 255),      //Blue
            Color.FromArgb(255, 0, 0),      //Red
            Color.FromArgb(0, 255, 0),      //Green
            Color.FromArgb(255, 255, 0),    //Yellow
            Color.FromArgb(255, 0, 255),    //Magenta
            Color.FromArgb(255, 128, 128),  //Pink
            Color.FromArgb(128, 128, 128),  //Gray
            Color.FromArgb(128, 0, 0),      //Brown
            Color.FromArgb(255, 128, 0),    //Orange
        };

        //============================================================================================================================================================================
        /// <summary>
        /// Generate a defined number of different colors.
        /// Source: http://stackoverflow.com/questions/470690/how-to-automatically-generate-n-distinct-colors
        /// </summary>
        /// <param name="nrColors"> number of colors to generate.</param>
        /// <returns></returns>
        public static List<Color> ColorsGenerator(int nrColors)
        {
            List<Color> colorGroup = new List<Color>();
            Random rnd = new Random();
            // assumes hue [0, 360), saturation [0, 100), lightness [0, 100)
            for (int i = 0; i < 360; i += 360 / nrColors)
            {
                HSLColor c = new HSLColor();
                c.Hue = i;
                c.Saturation = 90 + rnd.NextDouble() * 10;
                c.Luminosity = 50 + rnd.NextDouble() * 10;

                colorGroup.Add(c);
            }
            return colorGroup;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Color converter.
        /// </summary>
        static public Color HexToColor(string col)
        {
            Color color = System.Drawing.ColorTranslator.FromHtml(col);
            return color;
        }

        static public Color UIntToColor(uint color)
        {
            var a = (byte)(color >> 24);
            var r = (byte)(color >> 16);
            var g = (byte)(color >> 8);
            var b = (byte)(color >> 0);
            return Color.FromArgb(a, r, g, b);
        }
      
        #endregion


        //==============================================================================================   
        //==============================================================================================
        #region Basic Frame Counter

        private static int lastTick;
        private static int lastFrameRate;
        private static int frameRate;

        //==============================================================================================
        public static int CalculateFrameRate()
        {
            if (Environment.TickCount - lastTick >= 1000)
            {
                lastFrameRate = frameRate;
                frameRate = 0;
                lastTick = Environment.TickCount;
            }
            frameRate++;
            return lastFrameRate;
        }
        #endregion
        //==============================================================================================

        //============================================================================================================================================================================
        /// <summary>
        /// Normalise the values of all <b>IList</b> elements.
        /// </summary>
        public static float Normalize(IList<float> list)
        {
            float sum = 0f;
            foreach (float value in list)
            {
                sum += value;
            }

            if (sum != 0f)
            {
                for (int i = 0; i < list.Count; i++)
                    list[i] /= sum;
            }
            else
            {
                float value = 1f / list.Count;
                for (int i = 0; i < list.Count; i++)
                    list[i] = value;
            }

            return sum;
        }

        //============================================================================================================================================================================

        /// <summary>
        /// Write the characteristic values for the variance of the solution sets based on the poisson distribution.
        /// </summary>
        /// <param name="poissonValues">A list of characteristic values over all iterations. </param>
        public static void WritePoissonValues(List<PoissonValues> poissonValues)
        {
            string fileName = Application.StartupPath + "\\Variance.txt";
            StreamWriter file = new StreamWriter(fileName, false);

            if (poissonValues != null)
            {
                // --- heading ---
                string titleLine = "Range Deviations, Sum Deviation, Average Deviation, Lambda, Error";
                file.WriteLine(titleLine);

                // -- write the lines into the text file --
                for (int i = 0; i < poissonValues.Count(); i++)
                {
                    string valueLine = poissonValues[i].Range.ToString();
                    valueLine += ", " + poissonValues[i].Sum.ToString();
                    valueLine += ", " + poissonValues[i].AvgDeviation.ToString();
                    valueLine += ", " + poissonValues[i].Lambda.ToString();
                    valueLine += ", " + poissonValues[i].Error.ToString();

                    file.WriteLine(valueLine); // add a new line to the text file
                }
            }

            file.Close();
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Normalise the values of all <b>IList</b> elements.
        /// </summary>
        public static double Normalize(IList<double> list)
        {
            double sum = 0f;
            foreach (double value in list)
            {
                sum += value;
            }

            if (sum != 0f)
            {
                for (int i = 0; i < list.Count; i++)
                    list[i] /= sum;
            }
            else
            {
                double value = 1f / list.Count;
                for (int i = 0; i < list.Count; i++)
                    list[i] = value;
            }

            return sum;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Normalise the values of all array elements.
        /// </summary>
        public static double Normalize(double[] array1D)
        {
            double sum = 0;
            foreach (double value in array1D)
            {
                sum += value;
            }

            if (sum != 0)
            {
                for (int i = 0; i < array1D.Length; i++)
                    array1D[i] /= sum;
            }
            else
            {
                double value = 1 / array1D.Length;
                for (int i = 0; i < array1D.Length; i++)
                    array1D[i] = value;
            }
            
            return sum;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Normalise the IEnumerable values. 
        /// </summary>
        /// <param name="data">The IEnumerable data to be normalized.</param>
        /// <param name="min">The new min value.</param>
        /// <param name="max">The new max value.</param>
        /// <returns></returns>
        public static float[] Normalize(IEnumerable<float> data, int min, int max)
        {
            if (data != null && data.Any())
            {
                double dataMax = data.Max();
                double dataMin = data.Min();
                double range = dataMax - dataMin;

                return data
                    .Select(d => (d - dataMin)/range)
                    .Select(n => (float) ((1 - n)*min + n*max))
                    .ToArray();
            }
            else return null;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Substract all values of the IList by the minV.
        /// </summary>
        public static void CutMinValue(IList<float> list, Single minV)
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i] -= minV;
            }
        }

        /// <summary>
        /// Substract all values of the 2D Array by the minV.
        /// </summary>
        public static void CutMinValue(double[] array1D, double minV)
        {
            for (int i = 0; i < array1D.Length; i++)
                array1D[i] -= minV;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Extends the normalised values of a IList, that the given maxV = 1 
        /// and the other values are in the interval [0, 1]. 
        /// </summary>
        public static void IntervalExtension(IList<float> list, Single maxV)
        {
            Single multi = 1 / maxV;
            for (int i = 0; i < list.Count; i++)
            {
                list[i] *= multi;
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Extends the normalised values of a 2D Array, than the given maxV = 1 
        /// and the other values are in the interval [0, 1]. 
        /// </summary>
        public static void IntervalExtension(double[] array1D, double maxV)
        {
            double multi = 1 / maxV;
            for (int i = 0; i < array1D.Length; i++)
                array1D[i] *= multi;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Inverts the normalised values by substracting them from 1.
        /// </summary>
        public static void InvertNormals(IList<float> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i] = 1 - list[i];
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Inverts the normalised values by substracting them from 1.
        /// </summary>
        public static void InvertNormals(double[] array1D)
        {
            for (int i = 0; i < array1D.Length; i++)
                array1D[i] = 1 - array1D[i];
        }

        //============================================================================================================================================================================
        /// <summary>
        /// "Inverts" the values by substracting them from the max value of the list.
        /// Afterwards max = 0.
        /// </summary>
        public static void InvertValues(double[] array1D)
        {
            double maxV = GetMax(array1D);

            for (int i = 0; i < array1D.Length; i++)
                array1D[i] = maxV - array1D[i];
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Computes the mean value of the 2D Array.
        /// </summary>
        public static double GetMean(double[] array1D)
        {
            double sum = GetSum(array1D);
            return sum / array1D.Length;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Computes the max value of the 2D Array.
        /// </summary>
        public static double GetMax(double[] array1D)
        {
            double max = Double.MinValue;
            for (int i = 0; i < array1D.Length; i++)
            {
                if (array1D[i] > max)
                    max = array1D[i];
            }

            return max;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Computes the max value of the 2D Array.
        /// </summary>
        public static double GetMin(double[] array1D)
        {
            double min = Double.MaxValue;
            for (int i = 0; i < array1D.Length; i++)
                if (array1D[i] < min)
                    min = array1D[i];

            return min;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Computes the sum of the 2D Array values.
        /// </summary>
        public static double GetSum(double[] array1D)
        {
            double sum = 0; ;
            for (int i = 0; i < array1D.Length; i++)
                sum += array1D[i];

            return sum;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Populates an array with the given value
        /// </summary>
        public static void Populate<T>(this T[] arr, T value)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = value;
            }
        }
    }
}

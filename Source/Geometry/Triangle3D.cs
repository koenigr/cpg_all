﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Triangle3D.cs 
//  Copyright (C) 2014/10/18  12:58 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion
using OpenTK;

namespace CPlan.Geometry
{
    public class Triangle3D: CGeom3d.Triangle
    {

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructor

        public Triangle3D(Vector3d p1, Vector3d p2, Vector3d p3)
            : base(p1.ToGeom3d(), p2.ToGeom3d(), p3.ToGeom3d())
        {
        }

        public Triangle3D(CGeom3d.Vec_3d p1, CGeom3d.Vec_3d p2, CGeom3d.Vec_3d p3)
            : base(p1, p2, p3)
        {
        }

        #endregion


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        public double GetArea()
        {
            double area = 0;

            CGeom3d.AreaTriangle(m_pP1, m_pP2, m_pP3, ref area);
            
            return area;
        }


        public Vector2d GetCentroid2D()
        {
            Vector2d centriod = new Vector2d();

            centriod.X = (m_pP1.x + m_pP2.x + m_pP3.x)/3;
            centriod.Y = (m_pP1.y + m_pP2.y + m_pP3.y) / 3;

            return centriod;
        }



        #endregion


    }
}

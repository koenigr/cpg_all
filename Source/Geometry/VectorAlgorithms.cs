﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK;
using MathNet.Numerics.LinearAlgebra.Double;

namespace CPlan.Geometry
{
    public static class VectorAlgorithms
    {
        //////////////////////////////////////////////////////////////////////////////////////////
        # region Statistical Moments

        public static Vector3d Mean(this IEnumerable<Vector3d> vecs)
        {
            return vecs.Aggregate((m,v) => m + v) / vecs.Count();
        }

        public static Vector2d Mean(this IEnumerable<Vector2d> vecs)
        {
            return vecs.Aggregate((m, v) => m + v) / vecs.Count();
        }

        public static double Mean(this IEnumerable<double> vals)
        {
            return vals.Average();
        }

        public static Vector3d Variance(this IEnumerable<Vector3d> vecs)
        {
            var m = Mean(vecs);
            return vecs.Select(v => v - m).Aggregate((a, v) => a + v*v) / Math.Max(1,vecs.Count()-1);
        }

        public static Vector2d Variance(this IEnumerable<Vector2d> vecs)
        {
            var m = Mean(vecs);
            return vecs.Select(v => v - m).Aggregate((a, v) => a + v * v) / Math.Max(1, vecs.Count() - 1);
        }

        public static double Variance(this IEnumerable<double> vals)
        {
            var m = Mean(vals);
            return vals.Select(v => v - m).Aggregate((a, v) => a + v * v) / Math.Max(1, vals.Count() - 1);
        }

        public static DenseMatrix Covariance(this IEnumerable<Vector3d> vecs)
        {
            var C = new DenseMatrix(3);
            var n = vecs.Count();
            var mean = vecs.Mean();
            var temp = new Vector3d();
            // compute covariances
            foreach (var v in vecs)
            {
                temp = v - mean;
                for (int i = 0; i < 3; i++)
                    for (int j = i; j < 3; j++)
                        C[i, j] = temp[i] * temp[j];
            }
            // Normalize
            C.Divide(Math.Max(n - 1, 1), C);
            // Symmetric Matrix
            for (int i = 0; i < 3; i++)
                for (int j = i + 1; j < 3; j++)
                    C[j, i] = C[i, j];
            return C;
        }


        public static DenseMatrix Covariance(this IEnumerable<Vector2d> vecs)
        {
            var C = new DenseMatrix(2);
            var n = vecs.Count();
            var mean = vecs.Mean();
            var temp = new Vector2d();
            // compute covariances
            foreach (var v in vecs)
            {
                temp = v - mean;
                for (int i = 0; i < 2; i++)
                    for (int j = i; j < 2; j++)
                        C[i, j] = temp[i] * temp[j];
            }
            // Normalize
            C.Divide(Math.Max(n - 1, 1), C);
            // Symmetric Matrix
            for (int i = 0; i < 2; i++)
                for (int j = i + 1; j < 2; j++)
                    C[j, i] = C[i, j];
            return C;
        }

        # endregion


        //////////////////////////////////////////////////////////////////////////////////////////
        # region Projections on a Plane

        /// <summary>
        /// Return the "best" plane for a point set, i.e. that minimizes lost variance
        /// </summary>
        /// <param name="vals"></param>
        /// <returns></returns>
        public static Plane Plane(this IEnumerable<Vector3d> vals, bool normalized = true)
        {
            var cov = vals.Covariance();
            var svd = cov.Svd();
            var x = svd.VT.Column(0) as DenseVector;
            var y = svd.VT.Column(1) as DenseVector;
            return new Plane(vals.Mean(), x.ToVector3dMathNet(), y.ToVector3dMathNet(), normalized);
        }

        /// <summary>
        /// Find the "best" plane for a point set, i.e. that minimizes lost variance, and then adjust all points to be on that plane
        /// </summary>
        /// <param name="vals"></param>
        /// <returns></returns>
        public static IEnumerable<Vector3d> Planarize(this IEnumerable<Vector3d> vals)
        {
            var mean = vals.Mean();
            var cov = vals.Covariance();
            var normal = (cov.Svd().VT.Column(2) as DenseVector).ToVector3dMathNet();
            normal.Normalize();
            return vals.Select(v => normal.OrthogonalComponent(v - mean) + mean);
        }

        # endregion


        //////////////////////////////////////////////////////////////////////////////////////////
        # region Projections on a Line

        /// <summary>
        /// Component of the vector that is parallel to a normal vector "this"
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Vector3d CoaxialComponent(this Vector3d a, Vector3d b)
        {
            return a * a.DotProduct(b) / a.LengthSquared;
        }

        /// <summary>
        /// Component of the vector that is orthogonal to a normal vector "this"
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Vector3d OrthogonalComponent(this Vector3d a, Vector3d b)
        {
            return b - a * a.DotProduct(b) / a.LengthSquared;
        }

        /// <summary>
        /// Calculates linear approximation for the data - linear regression, and then adjust all point to that line
        /// </summary>
        /// <param name="vals"></param>
        /// <returns></returns>
        public static IEnumerable<Vector3d> Linearize(this IEnumerable<Vector3d> vals)
        {
            var mean = vals.Mean();
            var cov = vals.Covariance();
            var normal = (cov.Svd().U.Column(0) as DenseVector).ToVector3dMathNet();
            normal.Normalize();
            return vals.Select(v => normal.CoaxialComponent(v - mean) + mean);
        }

        # endregion


        //////////////////////////////////////////////////////////////////////////////////////////
    }
}

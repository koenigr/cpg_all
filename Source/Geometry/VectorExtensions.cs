﻿using System;
using System.Collections.Generic;
using System.Linq;

using OpenTK;

using MathNet.Numerics.LinearAlgebra.Double;

namespace CPlan.Geometry
{
    public static class VectorExtensions
    {
        ///////////////////////////////////////////////////////////////////////////////////////////
        # region OpenTK - additional functions

        /// <summary>
        /// Move a point into a target direction
        /// </summary>
        /// <param name="thisVect">Basic point</param>
        /// <param name="target">Target point</param>
        /// <param name="distance">Distance to move the basic point towards the target point</param>
        /// <returns></returns>
        public static Vector2d Move(this Vector2d thisVect, Vector2d target, float distance)
        {
            //Vector2d thisPos = new Vector2d(coords[0], coords[1]);
            if (distance == 0) return thisVect;
            double dx = target.X - thisVect.X, dy = target.Y - thisVect.Y;

            if (dx == 0) {
                if (dy > 0)
                    return new Vector2d(thisVect.X, thisVect.Y + distance);
                else if (dy < 0)
                    return new Vector2d(thisVect.X, thisVect.Y - distance);
                else
                    return thisVect;
            }

            if (dy == 0) {
                if (dx > 0)
                    return new Vector2d(thisVect.X + distance, thisVect.Y);
                else
                    return new Vector2d(thisVect.X - distance, thisVect.Y);
            }

            double length = Math.Sqrt(dx * dx + dy * dy);
            return new Vector2d(
                thisVect.X + distance * dx / length,
                thisVect.Y + distance * dy / length);
        }


        # endregion


        ///////////////////////////////////////////////////////////////////////////////////////////
        # region Math.Net - OpenTK interop

        //*------------------- Single Vectors --------------------------------------------------*//

        /// <summary>
        /// Convert OpenTK 3D Vector to Math.Net Vector
        /// </summary>
        /// <param name="openTKvec">3D double-valued OpenTK Vector structure</param>
        /// <returns>Math.Net instance of double-valued dense vector</returns>
        public static DenseVector ToMathNet(this Vector3d openTKvec)
        {
            return new DenseVector(new double[] { openTKvec.X, openTKvec.Y, openTKvec.Z });
        }

        /// <summary>
        /// Convert OpenTK 2D Vector to Math.Net Vector
        /// </summary>
        /// <param name="openTKvec">2D double-valued OpenTK Vector structure</param>
        /// <returns>Math.Net instance of double-valued dense vector</returns>
        public static DenseVector ToMathNet(this Vector2d openTKvec)
        {
            return new DenseVector(new double[] { openTKvec.X, openTKvec.Y });
        }

        /// <summary>
        /// Convert Math.Net Vector to OpenTK 3D vector
        /// </summary>
        /// <param name="mathnetVec">Math.Net instance of double-valued dense vector</param>
        /// <returns>3D double-valued OpenTK Vector structure</returns>
        public static Vector3d ToVector3dMathNet(this DenseVector mathnetVec)
        {
            if (mathnetVec.Count == 1)
                return new Vector3d(mathnetVec[0], 0, 0);
            if (mathnetVec.Count == 2)
                return new Vector3d(mathnetVec[0], mathnetVec[1], 0);
            return new Vector3d(mathnetVec[0], mathnetVec[1], mathnetVec[2]);
        }

        /// <summary>
        /// Convert Math.Net Vector to OpenTK 2D vector
        /// </summary>
        /// <param name="mathnetVec">Math.Net instance of double-valued dense vector</param>
        /// <returns>2D double-valued OpenTK Vector structure</returns>
        public static Vector2d ToVector2dMathNet(this DenseVector mathnetVec)
        {
            if (mathnetVec.Count == 1)
                return new Vector2d(mathnetVec[0], 0);
            return new Vector2d(mathnetVec[0], mathnetVec[1]);
        }


        //*------------------- Collections of Vectors ------------------------------------------*//

        /// <summary>
        /// Convert OpenTK 3D Vectors to Math.Net Vectors
        /// </summary>
        /// <param name="openTKvec">Collection of 3D double-valued OpenTK Vector structures</param>
        /// <returns>Collection of Math.Net instances of double-valued dense vectors</returns>
        public static IEnumerable<DenseVector> ToMathNet(this IEnumerable<Vector3d> openTKvecs)
        {
            return openTKvecs.Select(otk => otk.ToMathNet());
        }

        /// <summary>
        /// Convert OpenTK 3D Vectors to Math.Net Vectors
        /// </summary>
        /// <param name="openTKvec">Collection of 2D double-valued OpenTK Vector structures</param>
        /// <returns>Collection of Math.Net instances of double-valued dense vectors</returns>
        public static IEnumerable<DenseVector> ToMathNet(this IEnumerable<Vector2d> openTKvecs)
        {
            return openTKvecs.Select(otk => otk.ToMathNet());
        }

        /// <summary>
        /// Convert Math.Net Vectors to OpenTK 3D vectors
        /// </summary>
        /// <param name="mathnetVec">Collection of Math.Net instances of double-valued dense vectors</param>
        /// <returns>Collection of 3D double-valued OpenTK vector structures</returns>
        public static IEnumerable<Vector3d> ToVectors3dMathNet(this IEnumerable<DenseVector> mathnetVecs)
        {
            return mathnetVecs.Select(mnv => mnv.ToVector3dMathNet());
        }

        /// <summary>
        /// Convert Math.Net Vectors to OpenTK 2D vectors
        /// </summary>
        /// <param name="mathnetVec">Collection of Math.Net instances of double-valued dense vectors</param>
        /// <returns>Collection of 2D double-valued OpenTK vector structures</returns>
        public static IEnumerable<Vector2d> ToVectors2dMathNet(this IEnumerable<DenseVector> mathnetVecs)
        {
            return mathnetVecs.Select(mnv => mnv.ToVector2dMathNet());
        }

        # endregion


        ///////////////////////////////////////////////////////////////////////////////////////////
        # region Basic types - OpenTK interop

        //*------------------- Arrays to Vectors -----------------------------------------------*//

        /// <summary>
        /// Converts array of doubles to an OpenTK vector
        /// </summary>
        /// <param name="coords"></param>
        /// <returns></returns>
        public static Vector2d ToVector2d(this double[] coords)
        {
            return new Vector2d(coords[0], coords.Length > 1 ? coords[1] : 0);
        }

        /// <summary>
        /// Converts array of doubles to an OpenTK vector
        /// </summary>
        /// <param name="coords"></param>
        /// <returns></returns>
        public static Vector3d ToVector3d(this double[] coords)
        {
            return new Vector3d(coords[0], coords.Length > 1 ? coords[1] : 0, coords.Length > 2 ? coords[2] : 0);
        }

        /// <summary>
        /// Converts an OpenTK vector to array of doubles
        /// </summary>
        /// <param name="coords"></param>
        /// <returns></returns>
        public static double[] ToArray(this Vector2d vec)
        {
            return new double[] { vec.X, vec.Y };
        }

        /// <summary>
        /// Converts an OpenTK vector to array of doubles
        /// </summary>
        /// <param name="coords"></param>
        /// <returns></returns>
        public static double[] ToArray(this Vector3d vec)
        {
            return new double[] { vec.X, vec.Y, vec.Z };
        }

        //*------------------- Other Converters ------------------------------------------------*//

        public static System.Drawing.PointF ToPointF(this Vector2d vec)
        {
            return new System.Drawing.PointF((float)vec.X, (float)vec.Y);
        }

        # endregion


        ///////////////////////////////////////////////////////////////////////////////////////////
        # region PureGeom3d - OpenTK interop

        //*------------------- Single Vectors --------------------------------------------------*//

        /// <summary>
        /// Convert OpenTK 3D Vector to PureGeom3d vector
        /// </summary>
        /// <param name="openTKvec">3D double-valued OpenTK Vector structure</param>
        /// <returns>PureGeom3d vector</returns>
        public static CGeom3d.Vec_3d ToGeom3d(this Vector3d openTKvec)
        {
            return new CGeom3d.Vec_3d( openTKvec.X, openTKvec.Y, openTKvec.Z);
        }

        /// <summary>
        /// Convert OpenTK 2D Vector to PureGeom3d vector
        /// </summary>
        /// <param name="openTKvec">3D double-valued OpenTK Vector structure</param>
        /// <returns>PureGeom3d vector</returns>
        public static CGeom3d.Vec_3d ToGeom3d(this Vector2d openTKvec)
        {
            return new CGeom3d.Vec_3d(openTKvec.X, openTKvec.Y, 0);
        }

        /// <summary>
        /// Convert PureGeom3d vector to OpenTK 3D vector
        /// </summary>
        /// <param name="mathnetVec">PureGeom3d vector</param>
        /// <returns>3D double-valued OpenTK Vector structure</returns>
        public static Vector3d ToVector3d(this CGeom3d.Vec_3d geomVec)
        {
            return new Vector3d(geomVec.x, geomVec.y, geomVec.z);
        }

        //*------------------- Collections of Vectors ------------------------------------------*//

        /// <summary>
        /// Convert OpenTK 3D vectors to PureGeom3d Vectors
        /// </summary>
        /// <param name="openTKvec">Collection of 3D double-valued OpenTK Vector structures</param>
        /// <returns>Collection of PureGeom3d vectors</returns>
        public static IEnumerable<CGeom3d.Vec_3d> ToGeom3d(this IEnumerable<Vector3d> openTKvecs)
        {
            return openTKvecs.Select(otk => otk.ToGeom3d());
        }

        /// <summary>
        /// Convert PureGeom3d vectors to OpenTK 3D vectors
        /// </summary>
        /// <param name="mathnetVec">Collection of PureGeom3d vectors</param>
        /// <returns>Collection of 3D double-valued OpenTK vector structures</returns>
        public static IEnumerable<Vector3d> ToVectors3d(this IEnumerable<CGeom3d.Vec_3d> geomVecs)
        {
            return geomVecs.Select(gv => gv.ToVector3d());
        }


        # endregion


        ///////////////////////////////////////////////////////////////////////////////////////////
        # region OpenTK interop

        /// <summary>
        /// Convert OpenTK vector2D to Tectosyne Vector2d
        /// </summary>
        /// <param name="Vector2dVect"></param>
        /// <returns></returns>
        public static Vector2d ToVector2d(this Vector2d Vector2dVect)
        {
            return new Vector2d(Vector2dVect.X, Vector2dVect.Y);
        }

        # endregion


        ///////////////////////////////////////////////////////////////////////////////////////////
        # region OpenTK simple math

        //*------------------- Distances -------------------------------------------------------*//

        /// <summary>
        /// Squared Euclidian (L2) distance between two points
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double DistanceSquared(this Vector3d a, Vector3d b)
        {
            return (a - b).LengthSquared;
        }

        /// <summary>
        /// Squared Euclidian (L2) distance between two points
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double DistanceSquared(this Vector2d a, Vector2d b)
        {
            return (a - b).LengthSquared;
        }

        /// <summary>
        /// Euclidian (L2) distance between two points
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double Distance(this Vector3d a, Vector3d b)
        {
            return (a - b).Length;
        }

        /// <summary>
        /// Euclidian (L2) distance between two points
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double Distance(this Vector2d a, Vector2d b)
        {
            return (a - b).Length;
        }


        //*------------------- Products --------------------------------------------------------*//

        /// <summary>
        /// Dot product of two vectors
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double DotProduct(this Vector3d a, Vector3d b)
        {
            return a.X * b.X + a.Y * b.Y + a.Z * b.Z;
        }

        /// <summary>
        /// Dot product of two vectors
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double DotProduct(this Vector2d a, Vector2d b)
        {
            return a.X * b.X + a.Y * b.Y;
        }

        /// <summary>
        /// Vector (cross) product of two vectors in 3D
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Vector3d CrossProduct(this Vector3d a, Vector3d b)
        {
            return new Vector3d(
                a.Y*b.Z - a.Z*b.Y,
                a.Z*b.X - a.X*b.Z,
                a.X*b.Y - a.Y*b.X
                );
        }

        /// <summary>
        /// Orthogonal vector to this one in 2D - rotates on 90 degrees counter-clockwise
        /// </summary>
        /// <param name="a">current vector</param>
        /// <returns>orthogonal verctor</returns>
        public static Vector2d Orthogonal(this Vector2d a)
        {
            return new Vector2d(-a.Y, a.X);
        }


        //*------------------- Comparisons ------------------------------------------------------*//

        /// <summary>
        /// Checks if two vectors are close to each other enough (to a precision eps per each coordinate)
        /// </summary>
        /// <param name="a"></param>
        /// <param name="otherVec"></param>
        /// <param name="eps"></param>
        /// <returns></returns>
        public static bool IsSimilarTo(this Vector2d a, Vector2d otherVec, double eps)
        {
            return (a.X <= otherVec.X + eps) &&
                   (a.X >= otherVec.X - eps) &&
                   (a.Y <= otherVec.Y + eps) &&
                   (a.Y >= otherVec.Y - eps);
        }

        /// <summary>
        /// Checks if two vectors are close to each other enough (to a precision eps per each coordinate)
        /// </summary>
        /// <param name="a"></param>
        /// <param name="otherVec"></param>
        /// <param name="eps"></param>
        /// <returns></returns>
        public static bool IsSimilarTo(this Vector3d a, Vector3d otherVec, double eps)
        {
            return (a.X <= otherVec.X + eps) &&
                   (a.X >= otherVec.X - eps) &&
                   (a.Y <= otherVec.Y + eps) &&
                   (a.Y >= otherVec.Y - eps) &&
                   (a.Z <= otherVec.Z + eps) &&
                   (a.Z >= otherVec.Z - eps);
        }

        # endregion
    }
}
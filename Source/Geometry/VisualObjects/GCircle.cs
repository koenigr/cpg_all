﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using Tektosyne.Geometry;

namespace CPlan.Geometry
{
    using GdiPointF = System.Drawing.PointF;

    /// <summary>
    /// Represents a circle in two-dimensional space, using <see cref="Double"/> coordinates.
    /// </summary>
    /// <remarks><para>
    /// <b>GCircle</b> inherits from <b>GPolygon</b>, thus it has the same methods,
    /// only the constructor is individual.
    /// </para></remarks>
    [Serializable]
    public class GCircle : GPolygon
    {
        public double Radius { get; protected set; }
        public Vector2D Vector { get; set; }
        public Vector2D midPoint { get; set; }

        public Vector2D Center
        {
            get
            {
                //return midPoint;
                return GeoAlgorithms2D.Centroid(Poly.Points.ToList());
            }
        }
        /// <summary>
        /// Initialise a new instance of the <b>GCircle</b> class in two-dimensional space.
        /// </summary>
        /// <param name="midPt">The middle point of the circle.</param>
        /// <param name="radius">The radius of the circle.</param>
        public GCircle(Vector2D midPt, float radius)
        {
            midPoint = midPt;
            Radius = radius;
            Poly = new Poly2D();
            Poly.Points = new Vector2D[360];
            GdiPointF[] pathPts = new GdiPointF[360];
            for (int i = 0; i < 360; i++)
            {
                double degInRad = i * 3.1416 / 180;
                Poly.Points[i] = new Vector2D(Math.Cos(degInRad) * radius + midPt.X, Math.Sin(degInRad) * radius + midPt.Y);
                pathPts[i] = Poly.Points[i].ToPointF();// GdiConversions.ToGdiPointF(Poly.Points[i].ToPointF()); //new GdiPointF(Convert.ToSingle(Math.Cos(degInRad) * radius), Convert.ToSingle(Math.Sin(degInRad) * radius));
            }
        }

        /// <summary>
        /// Initializes a new instance of the <b>GCircle</b> from two <see cref="Tektosyne.PointD"/> and glControl.
        /// using PICTURE COORDINATES
        /// use for mouse interaction
        /// </summary>
        /// <param name="E"></param>
        /// <param name="glCtrl"></param>
        //public GCircle(Vector2D midPt, float radius, ref GLControl glCtrl)
        //{

        //    midPoint = new Vector2D();
        //    midPoint.Add(Coordinates.GetWorldCoordinatesFromFormsCoordinates(midPt, ref glCtrl));
        //    Radius = radius;
        //    Poly.Points = new Vector2D[180];
        //    for (int i = 0; i < Poly.Points.Length; i++)
        //    {
        //        double degInRad = i * 3.1416 / 180;
        //        Poly.Points[i] = new Vector2D(Math.Cos(degInRad) * radius + midPoint.X, Math.Sin(degInRad) * radius + midPoint.Y);
        //    }
        //}


        //============================================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>GCircle</b> class in two-dimensional space.
        /// The circle is created from a regular polygon. This means, its roundness is defined by the number of sides.
        /// </summary>
        /// <param name="startPt">The middle point of the circle.</param>
        /// <param name="radius">The radius of the circle.</param>
        /// <param name="sides">The number of sides of the regular polygon.</param>
        public GCircle(Vector2D midPt, Double radius, int sides)
        {
            midPoint = midPt;
            Radius = radius;
            double u = 2 * radius * Math.PI;
            double sideLength = u / sides;
            RegularPolygon curPoly = new RegularPolygon(sideLength, sides, PolygonOrientation.OnEdge);
            Tektosyne.Geometry.PointD[] tempVertices = curPoly.Vertices;
            Poly = new Poly2D();
            Poly.Points = new Vector2D[tempVertices.Length];
            for (int i = 0; i < tempVertices.Length; i++)
             // Poly.Points[i] = new Vector2D(Math.Cos(degInRad) * radius + midPt.X, Math.Sin(degInRad) * radius + midPt.Y);
                Poly.Points[i] = new Vector2D(tempVertices[i].X * radius + midPt.X, tempVertices[i].Y * radius + midPt.Y);               
        }


        //############################################################################################################################################################################
        # region Methods

        /// <summary>
        /// Is not implemented for <b>GPolygon</b>.
        /// </summary>
        public override bool Selector(Vector2D mPoint)
        {
            selected = 0;


            if (mPoint.Distance(midPoint) < Radius)
                selected = 1; //Kreisinneres selektiert
            if ((mPoint.DistanceTo(midPoint) >= Radius) && (mPoint.DistanceTo(midPoint) <= Radius + 4))
                selected = 2;
            /*
           double distance = Math.Sqrt((Math.Pow(mPoint.X - Start.X, 2.0) - Math.Pow(mPoint.Y - Start.Y, 2.0)));
           if ((distance <= pickingThreshold) && (LineD.DistanceSquared(mPoint) < 20))
               selected = 2; //Startpunkt = selected

           distance = Math.Sqrt((Math.Pow(mPoint.X - End.X, 2.0) - Math.Pow(mPoint.Y - End.Y, 2.0)));
           if ((distance <= pickingThreshold) && (LineD.DistanceSquared(mPoint) < 20))
               selected = 3; //Endpunkt = selected
            * */

            

            if (selected != 0)
                return true;
            else
                return false;
        }


        /// <summary>
        /// Moves the <b>GCircle</b>
        /// </summary>
        /// <param name="delta">The 2d vector to move the polygon.</param>
        public override void Move(Vector2D delta)
        {
            if (selected == 1)
            {
                midPoint = midPoint + delta;
                for (int i = 0; i < Poly.Points.Length; i++)
                {
                    Poly.Points[i] = Poly.Points[i] + delta;
                }
            }
            if (selected == 2)
            {
                //this = new GCircle(midPoint, delta.Length);
            }
        }


        //============================================================================================================================================================================
        /// <summary>
        /// Draw the <b>GCircle</b> to a <see cref="OpenTK.OpenGL"/> component
        /// </summary>
        public override void Draw()
        {
            double dZ = 0;
            Vector2D[] vertices = Poly.Points;

            if (selected == 1)
            {
                GL.LineWidth(BorderWidth * m_zoomFactor + 6);
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
                GL.Color4(SelectColor);
                GL.Begin(BeginMode.Polygon);
                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector2D next = vertices[i];// +offset;
                    GL.Vertex3(next.X, dZ, -next.Y);
                }
                GL.End();
            }
            //PointD start = vertices[0] + offset;
            // Draw the filled polygon 
            //if (isFilled)
            {
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(FillColor);
                GL.Begin(BeginMode.Polygon);
                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector2D next = vertices[i];// +offset;
                    GL.Vertex3(next.X, dZ, -next.Y);
                }
                GL.End();
            }

            GL.LineWidth(BorderWidth * m_zoomFactor);
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
            GL.Color4(BorderColor);
            GL.Begin(BeginMode.Polygon);
            for (int i = 0; i < vertices.Length; i++)
            {
                Vector2D next = vertices[i];// +offset;
                GL.Vertex3(next.X, dZ, -next.Y);
            }
            GL.LineWidth(1 * m_zoomFactor);
            GL.End();
        }

        # endregion

    }
}

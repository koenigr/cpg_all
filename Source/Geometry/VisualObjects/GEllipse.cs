﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics.OpenGL;
using Tektosyne.Geometry;

namespace CPlan.Geometry
{
    using GdiPointF = System.Drawing.PointF;

    [Serializable]
    public class GEllipse : GPolygon
    {
        
        /// <summary>
        /// Initialise a new instance of the <b>GEllipse</b> class in two-dimensional space.
        /// </summary>
        /// <param name="p">The center point</param>
        /// <param name="radius1">The first radius</param>
        /// <param name="radius2">The second radius</param>
        public GEllipse(Vector2D p, int radius1, int radius2)
        {
            int length = Convert.ToInt32(2.0f * 3.14159 / 0.01f) +1;
            Poly.Points = new Vector2D[length];
            GdiPointF[] pathPts = new GdiPointF[length];
            double vectorY1=p.Y+radius2;
            double vectorX1=p.X;
            double vectorX;
            double vectorY;
            int i = 0;
            /*if(fill)
                    GL.Begin(BeginMode.Polygon);
            else
                    GL.Begin(BeginMode.LineLoop);  */                 
            for(float angle=0.0f;angle<=(2.0f*3.14159);angle+=0.01f)
            {
                vectorX=p.X+(radius1*(float)Math.Sin((double)angle));
                vectorY=p.Y+(radius2*(float)Math.Cos((double)angle));
                Poly.Points[i] = new Vector2D(vectorX1, vectorY1);
                pathPts[i] = Poly.Points[i].ToPointF(); 
                vectorY1=vectorY;
                vectorX1=vectorX; 
                i++;      
            }
        }

        public override void Draw()
        {
            double dZ = 0;
            GL.LineWidth(BorderWidth);
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
            GL.Color4(BorderColor);
            GL.Begin(BeginMode.Polygon);
            for (int i = 0; i < Poly.Points.Length; i++)
            {
                Vector2D next = Poly.Points[i];// +offset;
                GL.Vertex3(next.X, dZ, -next.Y);
            }
            GL.LineWidth(1);
            GL.End();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tektosyne.Geometry;
using System.Drawing;
using OpenTK;

namespace CPlan.Geometry
{
    /// <summary>
    /// Represents a rectangular grid composed of regular polygons.
    /// </summary>
    /// <remarks><para>
    /// <b>GGrid</b> encapsulate a Tektosyne <see cref="Tektosyne.Geometry.PolygonGrid"/> class 
    /// to represent its grid structure.
    /// The Tektosyne Library for the .NET Framework and its documentation are © 2002–2011 by Christoph Nahr 
    /// but available for free download under the MIT license: http://www.kynosarges.de/Tektosyne.html
    /// </para></remarks>
    [Serializable]
    public class GGrid : Surfaces
    {
        //############################################################################################################################################################################
        # region Properties
        /// <summary> Tektosyne <see cref="PolygonGrid"/> class. </summary>
        public PolygonGrid Grid { get; protected set; }
        /// <summary> A vector array form OpenTK to hold the color values for all grid polygons. </summary>
        public Vector4[,] ColorGrid { get; set; }

        public Vector2D StartP;
        public Vector2D Size;


        # endregion



        //############################################################################################################################################################################
        # region Constructors
        /// <summary>
        /// Initialise a new instance of the <b>GGrid</b> class 
        /// from a <see cref="Tektosyne.Geometry.PolygonGrid"/>.
        /// </summary>
        /// <param name="grid">A <see cref="Tektosyne.Geometry.PolygonGrid[]"/>.</param>
        public GGrid(PolygonGrid grid)
        {
            this.Grid = grid;
           // Path.AddRectangle(new Rectangle((int)Grid.DisplayBounds.X, (int)Grid.DisplayBounds.Y, (int)Grid.DisplayBounds.Width, (int)Grid.DisplayBounds.Height)); // Path is necessary for mouse-interaction!
            ColorGrid = this.Grid.CreateArray<Vector4>();

            StartP = new Vector2D(0, 0);
            Size = new Vector2D(Grid.Size.Width, Grid.Size.Height);

        }


        public GGrid(Vector2D startP, Vector2D endP, float cellSize)
        {
            StartP = new Vector2D((float)startP.X, (float)startP.Y);
            Size = new Vector2D((float)(endP.X - startP.X), (float)(endP.Y - startP.Y));
            

            RegularPolygon StandardPolygon = new RegularPolygon(cellSize, 4, PolygonOrientation.OnEdge, true);
            this.Grid = new PolygonGrid(StandardPolygon);
            Grid.Size = new SizeI((int)(Size.X / StandardPolygon.Length), (int)(Size.Y / StandardPolygon.Length));
            ColorGrid = this.Grid.CreateArray<Vector4>();
            

        }


        # endregion

        //############################################################################################################################################################################
        # region Methods
        /// <summary>
        /// Is not implemented for <b>GGrid</b>.
        /// </summary>
        public override void Move(Vector2D delta)
        {

        }

        /// <summary>
        /// Is not implemented for <b>GGrid</b>.
        /// </summary>
        public override bool Selector(Vector2D mPoint)
        {
            return false;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Computes the area of the <b>GGrid</b>
        /// </summary>
        /// <returns>
        /// The area of the <b>GGrid</b>, as <see cref="double"/> value.
        /// </returns>
        public override Double GetArea()
        {
            double area;
            area = this.Grid.DisplayBounds.Width * this.Grid.DisplayBounds.Height;
            return area;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Reset the colors of all elements of the <b>GGrid</b> to white.
        /// </summary>
        public void ResetColorGrid()
        {
            ColorGrid = this.Grid.CreateArray<Vector4>();
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Set the colors of all elements of the <b>GGrid</b>.
        /// </summary>
        /// <param name="values">A 2d array with float values in the interval [0, 1] 
        /// to create the colors from.</param>
        public void SetColorGrid(float[,] values)
        {
            int width = Grid.Size.Width, height = Grid.Size.Height;
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    float val = values[x, y];
                    ColorGrid[x, y] = new Vector4(val, val, val, 1);
                }
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Set the colors of all elements of the <b>GGrid</b>.
        /// </summary>
        /// <param name="values">A 2d array with Vector4 color values.</param>
        public void SetColorGrid(Vector4[,] values)
        {
            int width = Grid.Size.Width, height = Grid.Size.Height;
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    ColorGrid[x, y] = values[x,y];
                }
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Set the colors of all elements of the <b>GGrid</b>.
        /// </summary>
        /// <param name="values">A Color values.</param>
        public void ResetColorGrid(Color col)
        {
            int width = Grid.Size.Width, height = Grid.Size.Height;
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    ColorGrid[x, y] = ConvertColor.CreateColor4(col);
                }
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Draw the <b>GGrid</b> by drawing all regular polygons to a <see cref="OpenTK.OpenGL"/> component.
        /// </summary>
        public override void Draw()
        {
            int width = Grid.Size.Width, height = Grid.Size.Height;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    PointD element = Grid.GridToDisplay(x, y);
                    element = new PointD(element.X + StartP.X, element.Y + StartP.Y);
                    //element.X += StartP.X;
                    //element.Y += StartP.Y;

                    FillColor = ColorGrid[x, y];
                    Grid.Element.Draw(element, true, FillColor, BorderColor, BorderWidth * m_zoomFactor);
                }
            }
        }

        public override void GetBoundingBox(BoundingBox box)
        {
            box.AddPoint(StartP);
            box.AddPoint(new Vector2D(StartP.X + Grid.Size.Width, StartP.Y));
            box.AddPoint(new Vector2D(StartP.X, StartP.Y + Grid.Size.Height));
            box.AddPoint(new Vector2D(StartP.X + Grid.Size.Width, StartP.Y + Grid.Size.Height));
        }

        # endregion
    }
}

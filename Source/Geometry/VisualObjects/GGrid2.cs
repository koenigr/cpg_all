﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using Tektosyne;
using Tektosyne.Collections;
using Tektosyne.Geometry;
using System.ComponentModel;
using CPlan.Evaluation;

namespace CPlan.Geometry
{

    [Serializable]
    public class GGrid2: Surfaces
    {

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Fields

        private double[] displayValues;
        protected Gridcell[,] gridCells { get; set; }

        protected Vector3D[] colorValues;
        protected Vector2D m_startP, m_endP, m_size;
        protected int m_countX, m_countY;
        protected double m_cellSize;
        protected String m_name;
        protected bool show = true;
        protected float transparency = 1;
        //private bool blocked;

        int colorMode = 2; //0 = greyScale, 1 = Farbe
        int interpolation = 0; //0 = linear, 1 = abhängig von der Verteilung der Werte

        //ToDo: Grid allgemeingültig machen für alle Polygone:
        GPolygon refPlane;
        GPolygon rectangularPlane;

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties


        public Vector2D StartP { get { return m_startP; } set { m_startP = value;  } }
        public Vector2D EndP { get { return m_endP; } set { m_endP = value; } }
        //public bool Blocked { get; set; }

        //[CategoryAttribute("Settings"), DescriptionAttribute("X Coordinate of the first corner of the Grid")]
        //public double P1_X { get { return Math.Round(m_startP.X, 2); } set { m_startP.X = value; } }
        //[CategoryAttribute("Settings"), DescriptionAttribute("Y Coordinate of the first corner of the Grid")]
        //public double P1_Y { get { return Math.Round(m_startP.Y, 2); } set { m_startP.Y = value; } }
        //[CategoryAttribute("Settings"), DescriptionAttribute("X Coordinate of the second corner of the Grid")]
        //public double P2_X { get { return Math.Round(m_endP.X, 2); } set { m_endP.X = value; } }
        //[CategoryAttribute("Settings"), DescriptionAttribute("Y Coordinate of the second corner of the Grid")]
        //public double P2_Y { get { return Math.Round(m_endP.Y, 2); } set { m_endP.Y = value; } }
        [CategoryAttribute("Settings"), DescriptionAttribute("Size of the Grid Cells")]
        public double CellSize { get { return m_cellSize; } set { m_cellSize = value; } }
        [CategoryAttribute("Visualisation"), DescriptionAttribute("Show Grid on Screen")]
        public bool ShowGrid { get { return show; } set { show = value; } }
        [CategoryAttribute("Visualisation"), DescriptionAttribute("Set Transparency of the grid (0 = invisble, 1 = opaque)")]
        public float Transparency { get { return transparency; } set { transparency = value; } }

        [CategoryAttribute("Information"), DescriptionAttribute("Grid Cells in X Direction")]
        public int CountX { get { return m_countX; } }
        [CategoryAttribute("Information"), DescriptionAttribute("Grid Cells in Y Direction")]
        public int CountY { get { return m_countY; } }
        [CategoryAttribute("Information"), DescriptionAttribute("Total Number of Grid Cells")]
        public int NumberOfCells { get { return m_countY * m_countX; } }
        [CategoryAttribute("Information"), DescriptionAttribute("Size of the Grid")]
        public Vector2D Size { get { return m_size; } }
        [CategoryAttribute("Information"), DescriptionAttribute("Number of Active GridCells")]
        public int NumberOfActiveCells { 
            get 
            { 
                int cnt = 0;
                for (int i = 0; i < m_countX; i++) 
                    for (int j = 0; j < m_countY; j++) 
                        if (gridCells[i, j].Active) cnt++; 
                return cnt; 
            } 
        }
        //public bool[] ActiveCells { get { int cnt = 0; for (int i = 0; i < m_countX; i++) for (int j = 0; j < m_countY; j++) if (gridCells[i, j].Active) cnt++; return cnt; } }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        //2D Grid...
        public GGrid2(Vector2D _startP, Vector2D _endP, double _cellSize, String _name)
        {
            m_startP = new Vector2D();
            m_endP = new Vector2D();
            if (m_name != null)
                m_name = _name;
            else
                m_name = "Grid";

            if (_startP.X > _endP.X)
            {
                m_startP.X = _endP.X;
                m_endP.X = _startP.X;
            }
            else
            {
                m_startP.X = _startP.X;
                m_endP.X = _endP.X;
            }

            if (_startP.Y > _endP.Y)
            {
                m_startP.Y = _endP.Y;
                m_endP.Y = _startP.Y;
            }
            else
            {
                m_startP.Y = _startP.Y;
                m_endP.Y = _endP.Y;
            }
            m_cellSize = _cellSize;

            m_size = new Vector2D((m_endP.X - m_startP.X), (m_endP.Y - m_startP.Y));
            m_countX = Convert.ToInt32(Math.Round(m_size.X / m_cellSize, 0));
            m_countY = Convert.ToInt32(Math.Round(m_size.Y / m_cellSize, 0));
            m_size.X = m_cellSize * m_countX;
            m_size.Y = m_cellSize * m_countY;

            gridCells = new Gridcell[m_countX, m_countY];
            for (int i = 0; i < m_countX; i++)
                for (int j = 0; j < m_countY; j++)
                    gridCells[i, j] = new Gridcell(true);

            colorValues = new Vector3D[m_countX * m_countY];
            show = true;
        }


        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        //============================================================================================================================================================================
        /// <summary>
        /// Get the normalised Distances value of all grid elements.
        /// </summary>
        /// <remarks>
        /// It is useful for the definition of the color values of the regular polygons
        /// of the corresponding <see cref="GGrid"/>.
        /// </remarks>
        /// <returns>
        /// The normalise Distances values as <see cref="float[,]"/> array.
        /// </returns>
        public void GetNormArray()
        {
            Extensions.CutMinValue(displayValues, Extensions.GetMin(displayValues));
            Extensions.Normalize(displayValues);
            Extensions.IntervalExtension(displayValues, Extensions.GetMax(displayValues));
        }

        /// <summary>
        /// Sets the color mode of the grid.
        /// </summary>
        /// <param m_name="col">(int) color mode. 0 - grey scale, 1 - color.</param>
        public void SetColorMode(int col)
        {
            if (col < 0 || col > 2) this.colorMode = 2;
            else this.colorMode = col;
        }

        public void AdjustGrid()
        {
            if (m_startP.X > m_endP.X)
            {
                double tmp = m_startP.X;
                m_startP.X = m_endP.X;
                m_endP.X = tmp;
            }
            if (m_startP.Y > m_endP.Y)
            {
                double tmp = m_startP.Y;
                m_startP.Y = m_endP.Y;
                m_endP.Y = tmp;
            }
            m_size = new Vector2D((m_endP.X - m_startP.X), (m_endP.Y - m_startP.Y));
            m_countX = Convert.ToInt32(Math.Round(m_size.X / m_cellSize, 0));
            m_countY = Convert.ToInt32(Math.Round(m_size.Y / m_cellSize, 0));
            m_size.X = m_cellSize * m_countX;
            m_size.Y = m_cellSize * m_countY;


            gridCells = new Gridcell[m_countX, m_countY];
            for (int i = 0; i < m_countX; i++)
                for (int j = 0; j < m_countY; j++)
                    gridCells[i, j] = new Gridcell(true);

            colorValues = new Vector3D[m_countX * m_countY];
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Interfaces

        public void Dialog()
        {
        }

        public override void Draw()
        {
            if (!show)
                return;


            displayValues = new double[NumberOfActiveCells];
            int k = 0;
            for (int i = 0; i < m_countX; i++)
                for (int j = 0; j < m_countY; j++)
                    if (gridCells[i, j].Active)
                    {
                        displayValues[k] = gridCells[i, j].Value;
                        k++;
                    }

            switch (colorMode)
            {
                case 0:
                    GetNormArray();
                    break;
                case 1:
                    GetNormArray();
                    colorValues = ConvertColor.CreateFFColor1D(displayValues);
                    break;
                case 2:

                    GetNormArray();
                    colorValues = ConvertColor.CreateFFColor1D(displayValues);
                    break;
                default:
                    GetNormArray();
                    break;

            }

            //Offset
            float offset = -2;// -640;
            if (offset != 0.0f)
            {
                GL.Enable(EnableCap.PolygonOffsetPoint);
                GL.Enable(EnableCap.PolygonOffsetLine);
                GL.Enable(EnableCap.PolygonOffsetFill);
                GL.PolygonOffset(0.0f, -offset);
            }

            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
            //GL.DepthMask(false);
            k = 0;
            for (int x = 0; x < m_countX; x++)
                for (int y = 0; y < m_countY; y++)
                {
                    if (gridCells[x, y].Active)
                    {
                        //hier noch unterschiedliche Farbskalen/Interpolationen einbauen
                        Vector4 FillColor = new Vector4();
                        switch (colorMode)
                        {
                            case 0:
                                //float cl = (float)gridValues[x,y];
                                //FillColor = new Vector4(cl, cl, cl, transparency);
                                break;
                            case 1:
                                //FillColor = new Vector4((float)colorValues[x, y].X, (float)colorValues[x, y].Y, (float)colorValues[x, y].Z, transparency);
                                break;
                            case 2:
                                if (gridCells[x, y].Active)
                                {
                                    //Farbe
                                    FillColor = new Vector4((float)colorValues[k].X, (float)colorValues[k].Y, (float)colorValues[k].Z, transparency);
                                    k++;
                                }
                                else
                                    FillColor = new Vector4(0, 0, 0, 1);
                                break;
                            default:
                                GetNormArray();
                                break;

                        }

                        GL.Begin(BeginMode.Polygon);
                        GL.Color4(FillColor);
                        GL.Vertex3(m_startP.X + x * m_size.X / m_countX, 0, -(m_startP.Y + y * m_size.Y / m_countY));
                        GL.Vertex3(m_startP.X + x * m_size.X / m_countX, 0, -(m_startP.Y + y * m_size.Y / m_countY + m_cellSize));
                        GL.Vertex3(m_startP.X + x * m_size.X / m_countX + m_cellSize, 0, -(m_startP.Y + y * m_size.Y / m_countY + m_cellSize));
                        GL.Vertex3(m_startP.X + x * m_size.X / m_countX + m_cellSize, 0, -(m_startP.Y + y * m_size.Y / m_countY));
                        GL.End();
                    }
                }

            //GL.DepthMask(true);
            if (offset != 0.0f)
            {
                GL.Disable(EnableCap.PolygonOffsetPoint);
                GL.Disable(EnableCap.PolygonOffsetLine);
                GL.Disable(EnableCap.PolygonOffsetFill);
            }

        }

        public string GetIdentificationName()
        {
            return m_name;
        }

        #endregion


        public override double GetArea()
        {
            return m_size.X * m_size.Y;
        }

        public override void Move(Vector2D delta)
        {
            throw new NotImplementedException();
        }

        public override bool Selector(Vector2D delta)
        {
            throw new NotImplementedException();
        }

        public override void GetBoundingBox(BoundingBox box)
        {
            Rect2D tempRect = new Rect2D(StartP.X, -StartP.Y, EndP.X, -EndP.Y);
            for (int i = 0; i < tempRect.Points.Length; i++)
            {
                box.AddPoint(tempRect.Points[i]);
            }
        }
    }


    [Serializable]
    public class Gridcell
    {
        double val;
        bool active = true;
        bool selected = false;

        public bool Active { get { return active; } set { active = value; } }
        public double Value { get { return val; } set { val = value; } }
        public bool Blocked { get; set; }


        public Gridcell()
        {
        }

        public Gridcell(double _value)
        {
            val = _value;
        }

        public Gridcell(double _value, bool _active)
        {
            val = _value;
            active = _active;
        }

        public Gridcell(bool _active)
        {
            active = _active;
        }
    }


}


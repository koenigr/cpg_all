﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System.Diagnostics;
using System.Drawing.Drawing2D;
using Tektosyne.Geometry;

namespace CPlan.Geometry
{
    /// <summary>
    /// Represents a directed line segment in two-dimensional space, using <see cref="Double"/>
    /// coordinates.</summary>
    [Serializable]
    public class GLine : Lines
    {
        //############################################################################################################################################################################
        # region Properties
        /// <summary>
        /// Basic Line2D object. </summary>
        public Line2D Line
        {
            get
            {
                return new Line2D(Start.Pos, End.Pos);
            }
            protected set
            {
                this.Start.Pos = value.Start;
                this.End.Pos = value.End;
            }
        }
        public GPoint Start { get; protected set; }
        public GPoint End { get; protected set; }
        public bool ActiveHandles { get; protected set; } // if the line is constructed by reference points, handles needs to be deactivated
        public int Selected{ get; set; } // 0 = not selected, 1 = line selected, 2 = start-point selected, 3 = end-point selected
        //pickingThreshold --> should be multiplied by view scale factor...

        # endregion

        //############################################################################################################################################################################
        # region Constructors
        public GLine() { }
        /// <summary>
        /// Initialise a new instance of the <b>GLine</b> class in two-dimensional space, 
        /// using <see cref="double"/> coordinates.
        /// using WORLD coordinates
        /// do not use for mouse interaction
        /// </summary>
        /// <param name="x1">The x-coordinate of the start point.</param>
        /// <param name="y1">The y-coordinate of the start point.</param>
        /// <param name="x2">The x-coordinate of the end point.</param>
        /// <param name="y3">The y-coordinate of the end point.</param>
        public GLine(double x1, double y1, double x2, double y2)
        {
            this.Start = new GPoint(x1, y1);
            this.End = new GPoint(x2, y2);
            Selected = 0;
            ActiveHandles = true;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>GLine</b> class in two-dimensional space, 
        /// using the <see cref="System.Drawing.PointF"/> point structure.
        /// using WORLD coordinates
        /// do not use for mouse interaction
        /// </summary>
        /// <param name="pt1">The start point of the line.</param>
        /// <param name="pt2">The end point of the line.</param>
        public GLine(System.Drawing.PointF pt1, System.Drawing.PointF pt2)
        {
            this.Start = new GPoint(pt1.X, pt1.Y);
            this.End = new GPoint(pt2.X, pt2.Y);
            Selected = 0;
            ActiveHandles = true;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>GLine</b> class in two-dimensional space, 
        /// using the <see cref="Tektosyne.Geometry.PointD"/> point structure.
        /// using WORLD coordinates
        /// </summary>
        /// <param name="pt1">The start point of the line.</param>
        /// <param name="pt2">The end point of the line.</param>
        public GLine(Vector2D pt1, Vector2D pt2)
        {
            this.Start = new GPoint(pt1.X, pt1.Y);
            this.End = new GPoint(pt2.X, pt2.Y);
            Selected = 0;
            ActiveHandles = true;
        }

        public GLine(ref GPoint pt1, ref GPoint pt2)
        {
            Start = pt1;
            End = pt2;
            Selected = 0;
            ActiveHandles = false;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Initializes a new instance of the <b>GLine</b> from a <see cref="Tektosyne.LineD"/>.
        /// do not use for mouse interaction
        /// </summary>
        /// <param name="line">A <see cref="Tektosyne.LineD"/>.</param>
        public GLine(LineD line)
        {
            this.Start = new GPoint(line.Start.X, line.Start.Y);
            this.End = new GPoint(line.End.X, line.End.Y);
            Selected = 0;
            ActiveHandles = true;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Initializes a new instance of the <b>GLine</b> from a <see cref="Line2D"/>.
        /// </summary>
        /// <param name="line">A <see cref="Tektosyne.LineD"/>.</param>
        public GLine(Line2D line)
        {
            this.Start = new GPoint(line.Start.X, line.Start.Y);
            this.End = new GPoint(line.End.X, line.End.Y);
            Selected = 0;
            ActiveHandles = true;
        }

        # endregion

        //############################################################################################################################################################################
        # region Methods
        /// <summary>
        /// Moves the <b>GLine</b>
        /// </summary>
        /// <param name="delta">The 2d vector to move the line segment.</param>
        public override void Move(Vector2D delta)
        {
            Matrix mat = new Matrix();
            switch (Selected)
            {
                case 1:
                    Start.Pos += delta;
                    End.Pos += delta;
                    mat.Translate((int)delta.X, (int)delta.Y);
                    break;
                case 2:
                    Start.Pos += delta;
                    mat.Translate((int)delta.X, (int)delta.Y);
                    break;
                case 3:
                    End.Pos += delta;
                    mat.Translate((int)delta.X, (int)delta.Y);
                    break;
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Highlights the Line or the End-Points of the <b>GLine</b>
        /// </summary>
        /// <param name="delta">The 2d vector to move the line segment.</param>
        public override bool Selector(Vector2D mPoint)
        {
            Selected = 0;
            double lineDist = Line.DistanceToPoint(mPoint);
            
            if (ActiveHandles)
            {
                if (lineDist < PickingThreshold)
                    Selected = 1; //Linie selektiert

                double distance = Math.Sqrt((Math.Pow(mPoint.X - Start.Pos.X, 2.0) - Math.Pow(mPoint.Y - Start.Pos.Y, 2.0)));
                if ((distance <= PickingThreshold))//&& (lineDist < 10))
                    Selected = 2; //Startpunkt = selected

                distance = Math.Sqrt((Math.Pow(mPoint.X - End.Pos.X, 2.0) - Math.Pow(mPoint.Y - End.Pos.Y, 2.0)));
                if ((distance <= PickingThreshold))//&& (lineDist < 10))
                    Selected = 3; //Endpunkt = selected
            }
            if (Selected != 0)
                return true;
            else
                return false;
        }
        
        //============================================================================================================================================================================

        public void SetCoordinates(Vector2D pt1, Vector2D pt2)
        {
            this.Start = new GPoint(pt1.X, pt1.Y);
            this.End = new GPoint(pt2.X, pt2.Y);
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Draw the <b>GLine</b> to a <see cref="OpenTK.OpenGL"/> component
        /// </summary>
        public override void Draw()
        {
            double dZ = 0;

            if (Selected == 0)
            {
                GL.LineWidth(Width);// * m_zoomFactor);
                GL.Color4(Color);
                GL.Begin(BeginMode.Lines);
                GL.Vertex3(Start.Pos.X, dZ, -Start.Pos.Y);
                GL.Vertex3(End.Pos.X, dZ, -End.Pos.Y);
                GL.End();
            }
            else if (Selected >= 1)
            {
                GL.LineWidth(Width);
                GL.Color4(SelectColor);
                GL.Begin(BeginMode.Lines);
                GL.Vertex3(Start.Pos.X, dZ, -Start.Pos.Y);
                GL.Vertex3(End.Pos.X, dZ, -End.Pos.Y);
                GL.End();
            }

            if (Selected == 2) //zeichne Startpunkt-Anfasser (Handle)
            {
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(SelectColor);
                GL.LineWidth(1);
                GL.Begin(BeginMode.Quads);
                GL.Vertex3(Start.Pos.X - HandleSize, dZ, -Start.Pos.Y - HandleSize);
                GL.Vertex3(Start.Pos.X - HandleSize, dZ, -Start.Pos.Y + HandleSize);
                GL.Vertex3(Start.Pos.X + HandleSize, dZ, -Start.Pos.Y + HandleSize);
                GL.Vertex3(Start.Pos.X + HandleSize, dZ, -Start.Pos.Y - HandleSize);
                GL.End();
            }
            else if (Selected == 3) //zeichne Endpunkt-Anfasser (Handle)
            {
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(SelectColor);
                GL.LineWidth(1);
                GL.Begin(BeginMode.Quads);
                GL.Vertex3(End.Pos.X - HandleSize, dZ, -End.Pos.Y - HandleSize);
                GL.Vertex3(End.Pos.X - HandleSize, dZ, -End.Pos.Y + HandleSize);
                GL.Vertex3(End.Pos.X + HandleSize, dZ, -End.Pos.Y + HandleSize);
                GL.Vertex3(End.Pos.X + HandleSize, dZ, -End.Pos.Y - HandleSize);
                GL.End();
            }
        }

        public override void GetBoundingBox(BoundingBox box)
        {
            box.AddPoint(Start.Pos);
            box.AddPoint(End.Pos);
        }

        #endregion

    }
}

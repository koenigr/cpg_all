﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using Tektosyne.Geometry;

namespace CPlan.Geometry
{
    [Serializable]
    public class GPoint : GeoObject
    {
        public Vector2D Pos { get; set; }
        public Vector2D[] PolyPoints { get; set; }
        public double Radius { get; protected set; }
        public int selected;

        public GPoint(double x, double y)
        {
            Pos = new Vector2D(x, y);

            Radius = 0.2;
            int sides = 12;
            PolyPoints = new Vector2D[sides];
            for (int i = 0; i < PolyPoints.Length; i++)
            {
                double degInRad = i * 3.1416 / (sides/2);
                PolyPoints[i] = new Vector2D(Math.Cos(degInRad) * Radius + Pos.X, Math.Sin(degInRad) * Radius + Pos.Y);
            }
        }

        //############################################################################################################################################################################
        # region Methods

        public override bool Selector(Vector2D mPoint)
        {
            selected = 0;

            if (mPoint.DistanceTo(Pos) < Radius)
                selected = 1; //Kreisinneres selektiert
            //if ((mPoint.DistanceTo(Position) >= Radius) && (mPoint.DistanceTo(Position) <= Radius + 4))
            //    selected = 2;

            if (selected != 0)
                return true;
            else
                return false;

        }


        /// <summary>
        /// Moves the Point
        /// </summary>
        /// <param name="delta">The 2d vector to move the polygon.</param>
        public override void Move(Vector2D delta)
        {
            if (selected == 1)
            {
                Pos = Pos + delta;
                for (int i = 0; i < PolyPoints.Length; i++)
                {
                    PolyPoints[i] = PolyPoints[i] + delta;
                }
            }
            if (selected == 2)
            {
                //this = new GCircle(midPoint, delta.Length);
            }

        }


        //============================================================================================================================================================================
        /// <summary>
        /// Draw the <b>GCircle</b> to a <see cref="OpenTK.OpenGL"/> component
        /// </summary>
        public override void Draw()
        {
            double dZ = 0;
            Vector2D[] vertices = PolyPoints;
            int width = 1;

            if (selected == 0)
            {
                GL.LineWidth(width);
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(Color.Black);
                GL.Begin(BeginMode.Polygon);
                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector2D next = vertices[i];// +offset;
                    GL.Vertex3(next.X, dZ, -next.Y);
                }
                GL.End();
            }
            else if (selected >= 1)
            {
                GL.LineWidth(width );
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(SelectColor);
                GL.Begin(BeginMode.Polygon);
                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector2D next = vertices[i];// +offset;
                    GL.Vertex3(next.X, dZ, -next.Y);
                }
                GL.LineWidth(1 * m_zoomFactor);
                GL.End();
            }
        }

        public override void GetBoundingBox(BoundingBox box)
        {
            box.AddPoint(Pos);
        }

        public Tektosyne.Geometry.PointD ToPointD()
        {
            return new Tektosyne.Geometry.PointD(Pos.X, Pos.Y);
        }

        # endregion

    }
}

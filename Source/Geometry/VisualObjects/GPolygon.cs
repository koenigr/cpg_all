﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tektosyne;
using Tektosyne.Geometry;
//using System.Drawing;
using System.Drawing.Drawing2D;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using ClipperLib;

namespace CPlan.Geometry
{
    using GdiPointF = System.Drawing.PointF;
    using GdiPoint = System.Drawing.Point;
    using PolygonC = List<IntPoint>;
    using PolygonsC = List<List<IntPoint>>;

    /// <summary>
    /// Represents a polygon in two-dimensional space, using <see cref="Double"/> coordinates.
    /// </summary>

    [Serializable]
    public class GPolygon : Surfaces
    {
        //############################################################################################################################################################################
        # region Properties

        /// <summary>
        /// Poly2D class.
        /// </summary>
        public Poly2D Poly { get; protected set; }

        public Vector2D[] PolyPoints { get { return Poly.Points; } }

        public int selected; // 0 = not selected, 1 = point selected, 2 = line selected, 3 = polygon selected 
        public int selectedPoint;
        public int selectedLineP1, selectedLineP2;

        ///// <summary>
        ///// Indicates if the rectangle is filled.
        ///// </summary>
        //public bool Filled { get; set; }

        /// <summary> List with the edges of the polygon. </summary>
        public Line2D[] Edges
        {
            get
            {
                Line2D[] edges = new Line2D[Poly.Points.Count()];
                for (int i = 0; i < Poly.Points.Count(); i++)
                {
                    int k = i + 1;
                    if (k >= Poly.Points.Count()) k = 0;
                    edges[i] = new Line2D(Poly.Points[i], Poly.Points[k]);
                }
                return edges;
            }
        }

        # endregion

        //############################################################################################################################################################################
        # region Constructors
        
        public GPolygon() { }
        
        /// <summary>
        /// Initialise a new instance of the <b>GPolygon</b> class in two-dimensional space.
        /// </summary>

        public GPolygon(params Vector2D[] polygon)
        {
            if (polygon == null)

                ThrowHelper.ThrowArgumentNullException("polygon");
            if (polygon.Length < 3)
                ThrowHelper.ThrowArgumentExceptionWithFormat(
                    "polygon.Length", Strings.ArgumentLessValue, 3);

            GdiPointF[] konvVert = new GdiPointF[polygon.Length];
            Poly = new Poly2D(polygon);

            //for (int i = 0; i < polygon.Length; i++)
            //{
            //    konvVert[i] = polygon[i].ToPointF();
            //    Poly.Points[i] = polygon[i];
            //}
            //Path.AddPolygon(konvVert);   // Path is necessary for mouse-interaction! 
        }


        public GPolygon(List<Vector2D> polygon)
        {
            if (polygon.Count == 0)
                ThrowHelper.ThrowArgumentNullException("polygon");
            if (polygon.Count < 3)
                ThrowHelper.ThrowArgumentExceptionWithFormat(
                    "polygon.Length", Strings.ArgumentLessValue, 3);
            
            Poly = new Poly2D(polygon.ToArray());
            //Poly.Points = new Vector2D[polygon.Count];
        }


        public GPolygon(Poly2D polygon)
        {
            if (polygon.Points.Count() == 0)
                ThrowHelper.ThrowArgumentNullException("polygon");
            if (polygon.Points.Count() < 3)
                ThrowHelper.ThrowArgumentExceptionWithFormat(
                    "polygon.Length", Strings.ArgumentLessValue, 3);

            Poly = new Poly2D(polygon);
            //Poly.Points = new Vector2D[polygon.Count];
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="source">GPolygon to copy</param>
        public GPolygon(GPolygon source)
        {
            this.Poly = source.Poly.Clone();     //new GPolygon PointD(source.Position.X, source.Position.Y);
            this.selected = source.selected; // 0 = not selected, 1 = point selected, 2 = line selected, 3 = polygon selected 
            this.selectedPoint = source.selectedPoint;
            this.selectedLineP1 = source.selectedLineP1;
            this.selectedLineP2 = source.selectedLineP2;

        }

        # endregion

        //############################################################################################################################################################################
        # region Methods

        /// <summary>
        /// Clone the GPolygon.
        /// </summary>
        /// 
        /// <returns>Returns exact clone of the polygon.</returns>
        /// 
        public object Clone()
        {
            GPolygon clone = new GPolygon(this);
            return clone;
        }

        /// <summary>
        /// Moves the <b>GPolygon</b>
        /// </summary>
        /// <param name="delta">The 2d vector to move the polygon.</param>
        public override void Move(Vector2D delta)    
        {
            Vector2D[] vertices;
            switch (selected)
            {
                case 1:
                    Poly.Points[selectedPoint] += delta;
                    break;

                case 2:
                    Poly.Points[selectedLineP1] += delta;
                    Poly.Points[selectedLineP2] += delta;
                    break;

                case 3:
                    goto default;
                    //vertices = Poly.Points;
                    //for (int i = 0; i < vertices.Length; i++)
                    //{
                    //    vertices[i] = vertices[i] + delta;
                    //}
                    //break;

                default:
                    vertices = Poly.Points;
                    for (int i = 0; i < vertices.Length; i++)
                    {
                        vertices[i] = vertices[i] + delta;
                    }
                    break;
            }
        }

        /// <summary>
        /// Is not implemented for <b>GPolygon</b>.
        /// </summary>
        public override bool Selector(Vector2D mPoint)
        {       
            selected = 0;

            //if (IsPointInside(mPoint))
              //selected = 3; //Polygon selektiert

            double distance = 0;
            //= Math.Sqrt((Math.Pow(mPoint.X - Start.X, 2.0) - Math.Pow(mPoint.Y - Start.Y, 2.0)));
            //if ((distance <= pickingThreshold) && (LineD.DistanceSquared(mPoint) < 20))
              //  selected = 2; //Startpunkt = selected

            //alle Linien durchschauen
            Line2D curEdge;
            for (int i = 0; i < Poly.Points.Length-1; i++)
            {
                curEdge = new Line2D(Poly.Points[i], Poly.Points[i + 1]);
                if (curEdge.DistanceToPoint(mPoint) <= PickingThreshold)
                {
                    selected = 2; //Linie = selected
                    selectedLineP1 = i;
                    selectedLineP2 = i + 1;
                }
            }
            curEdge = new Line2D(Poly.Points[0], Poly.Points[Poly.Points.Length - 1]);
            if (curEdge.DistanceToPoint(mPoint) <= PickingThreshold)
            {
                selected = 2; //Linie = selected
                selectedLineP1 = 0;
                selectedLineP2 = Poly.Points.Length - 1;
            }

            //alle Punkte durchschauen
            for (int i = 0; i < Poly.Points.Length; i++ )
            {
                distance = Math.Sqrt((Math.Pow(mPoint.X - Poly.Points[i].X, 2.0) - Math.Pow(mPoint.Y - Poly.Points[i].Y, 2.0)));
                if ( distance <= PickingThreshold ) 
                {
                    selected = 1; //Punkt = selected
                    selectedPoint = i;
                }
            }
            
            if (selected != 0)
                return true;
            else
                return false;
        }


        //============================================================================================================================================================================
        //public bool IsPointInside(Vector2D testPoint)
        //{
        //    if (GeoAlgorithms2D.PointInPolygon(testPoint, Poly.Points.ToList()) == 1)
        //    {
        //        return true;
        //    }
        //    else
        //        return false;
        //}

        public bool ContainsPoint(Vector2D point)
        {
            return Poly.ContainsPoint(point);
        }

        //============================================================================================================================================================================
        //public double FindDistanceToPolygon(GPolygon otherPolygon)
        //{
        //    PointF close1, close2;
        //    Double distance = double.MaxValue;
        //    int j, l;
        //    PointF p1, p2, p3, p4;
        //    for (int i = 0; i < Poly.Points.Length; i++)
        //    {
        //        j = i + 1;
        //        if (j > Path.PathPoints.Length - 1) j = 0;
        //        p1 = Poly.Points[i].ToPointF();
        //        p2 = Poly.Points[j].ToPointF();

        //        for (int k = 0; k < otherPolygon.Poly.Points.Length; k++)
        //        {
        //            l = k + 1;
        //            if (l > Path.PathPoints.Length - 1) l = 0;
        //            p3 = Poly.Points[k].ToPointF();
        //            p4 = Poly.Points[l].ToPointF();

        //            double tempDistance = FindDistanceBetweenSegments(p1, p2, p3, p4, out close1, out close2);
        //            if (distance > tempDistance) distance = tempDistance; 
        //            if (distance == 0) break; 
        //        }
        //        if (distance == 0) break; 
        //    }
        //    return distance;
        //}
        
        
        //============================================================================================================================================================================
        // comes from http://blog.csharphelper.com/2010/03/27/find-the-shortest-distance-between-two-line-segments-in-c.aspx
        // but the method working with a LinearRing is faster!

        /// <summary>
        /// Return the shortest distance between the two segments p1 --> p2 and p3 --> p4.
        /// </summary>
        public double FindDistanceBetweenSegments(PointF p1, PointF p2, PointF p3, PointF p4,  out PointF close1, out PointF close2)
        {
            // See if the segments intersect.
            bool lines_intersect, segments_intersect;
            PointF intersection;
            FindIntersection(p1, p2, p3, p4,
                out lines_intersect, out segments_intersect,
                out intersection, out close1, out close2);
            if (segments_intersect)
            {
                // They intersect.
                close1 = intersection;
                close2 = intersection;
                return 0;
            }

            // Find the other possible distances.
            PointF closest;
            double best_dist = double.MaxValue, test_dist;

            // Try p1.
            test_dist = FindDistanceToSegment(p1, p3, p4, out closest);
            if (test_dist < best_dist)
            {
                best_dist = test_dist;
                close1 = p1;
                close2 = closest;
            }

            // Try p2.
            test_dist = FindDistanceToSegment(p2, p3, p4, out closest);
            if (test_dist < best_dist)
            {
                best_dist = test_dist;
                close1 = p2;
                close2 = closest;
            }

            // Try p3.
            test_dist = FindDistanceToSegment(p3, p1, p2, out closest);
            if (test_dist < best_dist)
            {
                best_dist = test_dist;
                close1 = closest;
                close2 = p3;
            }

            // Try p4.
            test_dist = FindDistanceToSegment(p4, p1, p2, out closest);
            if (test_dist < best_dist)
            {
                best_dist = test_dist;
                close1 = closest;
                close2 = p4;
            }

            return best_dist;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Find the point of intersection between the lines p1 --> p2 and p3 --> p4.
        /// </summary>
        private void FindIntersection(PointF p1, PointF p2, PointF p3, PointF p4,
            out bool lines_intersect, out bool segments_intersect,
            out PointF intersection, out PointF close_p1, out PointF close_p2)
        {
            // Get the segments' parameters.
            float dx12 = p2.X - p1.X;
            float dy12 = p2.Y - p1.Y;
            float dx34 = p4.X - p3.X;
            float dy34 = p4.Y - p3.Y;

            // Solve for t1 and t2
            float denominator = (dy12 * dx34 - dx12 * dy34);

            float t1;
            try
            {
                t1 = ((p1.X - p3.X) * dy34 + (p3.Y - p1.Y) * dx34) / denominator;
            }
            catch
            {
                // The lines are parallel (or close enough to it).
                lines_intersect = false;
                segments_intersect = false;
                intersection = new PointF(float.NaN, float.NaN);
                close_p1 = new PointF(float.NaN, float.NaN);
                close_p2 = new PointF(float.NaN, float.NaN);
                return;
            }
            lines_intersect = true;

            float t2 = ((p3.X - p1.X) * dy12 + (p1.Y - p3.Y) * dx12) / -denominator;

            // Find the point of intersection.
            intersection = new PointF(p1.X + dx12 * t1, p1.Y + dy12 * t1);

            // The segments intersect if t1 and t2 are between 0 and 1.
            segments_intersect = ((t1 >= 0) && (t1 <= 1) && (t2 >= 0) && (t2 <= 1));

            // Find the closest points on the segments.
            if (t1 < 0)
            {
                t1 = 0;
            }
            else if (t1 > 1)
            {
                t1 = 1;
            }

            if (t2 < 0)
            {
                t2 = 0;
            }
            else if (t2 > 1)
            {
                t2 = 1;
            }

            close_p1 = new PointF(p1.X + dx12 * t1, p1.Y + dy12 * t1);
            close_p2 = new PointF(p3.X + dx34 * t2, p3.Y + dy34 * t2);
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Calculate the distance between point pt and the segment p1 --> p2.
        /// </summary>
        /// <returns>
        /// The distance between point pt and the segment p1 --> p2
        /// </returns>
        private double FindDistanceToSegment(PointF pt, PointF p1, PointF p2, out PointF closest)
        {
            float dx = p2.X - p1.X;
            float dy = p2.Y - p1.Y;
            if ((dx == 0) && (dy == 0))
            {
                // It's a point not a line segment.
                closest = p1;
                dx = pt.X - p1.X;
                dy = pt.Y - p1.Y;
                return Math.Sqrt(dx * dx + dy * dy);
            }

            // Calculate the t that minimizes the distance.
            float t = ((pt.X - p1.X) * dx + (pt.Y - p1.Y) * dy) / (dx * dx + dy * dy);

            // See if this represents one of the segment's
            // end points or a point in the middle.
            if (t < 0)
            {
                closest = new PointF(p1.X, p1.Y);
                dx = pt.X - p1.X;
                dy = pt.Y - p1.Y;
            }
            else if (t > 1)
            {
                closest = new PointF(p2.X, p2.Y);
                dx = pt.X - p2.X;
                dy = pt.Y - p2.Y;
            }
            else
            {
                closest = new PointF(p1.X + t * dx, p1.Y + t * dy);
                dx = pt.X - closest.X;
                dy = pt.Y - closest.Y;
            }

            return Math.Sqrt(dx * dx + dy * dy);
        }


        //============================================================================================================================================================================
        /// <summary>
        /// Computes the area of the <b>GPolygon</b>
        /// </summary>
        /// <returns>
        /// The area of the <b>GPolygon</b>, as <see cref="double"/> value.
        /// </returns>
        public override Double GetArea()
        {
            return Poly.Area;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Get the  the <b>GPolygon</b> as polygon for a clipping operation using the ClipperLib.
        /// </summary>
        /// <returns>
        /// A Polygon as a List of IntPoints. <see cref="IntPoint"/> is a ClipperLib type.
        /// </returns>
        public PolygonC GetClipPolygon()
        {
            PolygonC outPolygon = new PolygonC();
            for (int i = 0; i < Poly.Points.Length; i++)
            {
                outPolygon.Add(new IntPoint((int)Poly.Points[i].X, (int)Poly.Points[i].Y));
            }
            return outPolygon;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Computes the intersection of two <b>GPolygon</b> instances using the ClipperLib 
        /// </summary>
        /// <param name="otherPoly">The other <b>GPolygon</b> to compute the intersection with this one.</param>
        /// <remarks>
        /// "Clipper" is a freeware polygon clipping library developed by Angus Johnson.
        /// http://www.angusj.com/delphi/clipper.php
        /// </remarks>
        /// <returns>
        /// A Polygon as a List of IntPoints.
        /// <see cref="IntPoint"/> is a ClipperLib type.
        /// </returns>     
        public List<PointD[]> Intersection (GPolygon otherPoly)
        {
            PointD[] resultPts;

            PolygonsC subjs = new PolygonsC();
            PolygonsC clips = new PolygonsC();
            PolygonsC solution = new PolygonsC();

            subjs.Add(this.GetClipPolygon());// 
            clips.Add(otherPoly.GetClipPolygon());

            Clipper c = new Clipper();
            c.AddPolygons(subjs, PolyType.ptSubject);
            c.AddPolygons(clips, PolyType.ptClip);
            c.Execute(ClipType.ctIntersection, solution, PolyFillType.pftEvenOdd, PolyFillType.pftEvenOdd);

            List<PointD[]> intersectPointLists = new List<PointD[]>();

            if (solution.Count == 0) 
            { 
                return null; 
            }
            else 
            {
                for (int i = 0; i < solution.Count; i++)
                {
                    PolygonC curPoly = solution[i].ToList();
                    resultPts = new PointD[curPoly.Count];
                    for (int j = 0; j < curPoly.Count; j++)
                    {
                        resultPts[j] = new PointD(curPoly[j].X, curPoly[j].Y);
                    }
                    intersectPointLists.Add(resultPts);
                }
                return intersectPointLists;
            }
        }

        public bool IntersectLine(Line2D testLine)
        {
            bool intersect = false;
            for (int j = 0; j < 4; j++)
            {
                int idx2 = j+1;
                if (idx2 >= 4) idx2 = idx2 - 4;
                Vector2D testIntersection = GeoAlgorithms2D.LineIntersection(testLine.Start, testLine.End, Poly.Points[j], Poly.Points[idx2]);
                if (testIntersection != null)
                {
                    intersect = true;
                    return intersect;
                }
            }
            return intersect;
        }

        public Vector2D Intersect(Line2D testLine)
        {
            Vector2D intersect = new Vector2D();
            for (int j = 0; j < 4; j++)
            {
                int idx2 = j + 1;
                if (idx2 >= 4) idx2 = idx2 - 4;
                Vector2D testIntersection = GeoAlgorithms2D.LineIntersection(testLine.Start, testLine.End, Poly.Points[j], Poly.Points[idx2]);
                if (testIntersection != null)
                {
                    intersect = testIntersection;
                    return intersect;
                }
            }
            return null;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Draw the <b>GPolygon</b> to a <see cref="OpenTK.OpenGL"/> component
        /// </summary>
        public override void Draw()
        {
            double dZ = 0;
            Vector2D[] vertices = Poly.Points;

            if (selected == 1) //zeichne Anfasser (Handle)
            {
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(SelectColor);
                GL.LineWidth(1 * m_zoomFactor);
                GL.Begin(BeginMode.Quads);
                GL.Vertex3(Poly.Points[selectedPoint].X - HandleSize, dZ, -Poly.Points[selectedPoint].Y - HandleSize);
                GL.Vertex3(Poly.Points[selectedPoint].X - HandleSize, dZ, -Poly.Points[selectedPoint].Y + HandleSize);
                GL.Vertex3(Poly.Points[selectedPoint].X + HandleSize, dZ, -Poly.Points[selectedPoint].Y + HandleSize);
                GL.Vertex3(Poly.Points[selectedPoint].X + HandleSize, dZ, -Poly.Points[selectedPoint].Y - HandleSize);
                GL.End();

                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
                GL.Color4(0,0,0,.5);
                GL.LineWidth(1 * m_zoomFactor);
                GL.Begin(BeginMode.Polygon);
                GL.Vertex3(Poly.Points[selectedPoint].X - HandleSize, dZ, -Poly.Points[selectedPoint].Y - HandleSize);
                GL.Vertex3(Poly.Points[selectedPoint].X - HandleSize, dZ, -Poly.Points[selectedPoint].Y + HandleSize);
                GL.Vertex3(Poly.Points[selectedPoint].X + HandleSize, dZ, -Poly.Points[selectedPoint].Y + HandleSize);
                GL.Vertex3(Poly.Points[selectedPoint].X + HandleSize, dZ, -Poly.Points[selectedPoint].Y - HandleSize);
                GL.End();
            }

            if (selected == 2) //zeichne Linie
            {
                GL.LineWidth(BorderWidth );
                GL.Color4(SelectColor);
                GL.Begin(BeginMode.Lines);
                GL.Vertex3(Poly.Points[selectedLineP1].X, dZ, -Poly.Points[selectedLineP1].Y);
                GL.Vertex3(Poly.Points[selectedLineP2].X, dZ, -Poly.Points[selectedLineP2].Y);
                GL.End();
            }

            if (selected == 3)
            {
                GL.LineWidth(BorderWidth );
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
                GL.Color4(SelectColor);
                GL.Begin(BeginMode.Polygon);
                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector2D next = vertices[i];// +offset;
                    GL.Vertex3(next.X, dZ, -next.Y);
                }
                GL.End();
            }
            
            //PointD start = vertices[0] + offset;
            // Draw the filled polygon 

            ////To do: Tesselierung ....
            //List<Vector2D[]> triangles = Triangulation2D.Triangulate(vertices);
            //// fill the polygon with its component trangles, each with a different color
            //GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
            //GL.Color4(FillColor);
            //for (int i = 0; i < triangles.Count; i++)
            //{
            //    GL.Begin(BeginMode.Triangles);
            //    GL.Vertex3(triangles[i][0].X, dZ, -triangles[i][0].Y);
            //    GL.Vertex3(triangles[i][1].X, dZ, -triangles[i][1].Y);
            //    GL.Vertex3(triangles[i][2].X, dZ, -triangles[i][2].Y);
            //    GL.End();

            //}

            if (Filled)
            {
                GL.LineWidth(BorderWidth);
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(FillColor);
                GL.Begin(BeginMode.Polygon);
                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector2D next = vertices[i];// +offset;
                    GL.Vertex3(next.X, dZ, -next.Y);
                }
                GL.End();
            }

            GL.LineWidth(BorderWidth);
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
            GL.Color4(BorderColor);
            GL.Begin(BeginMode.Polygon);
            for (int i = 0; i < vertices.Length; i++)
            {
                Vector2D next = vertices[i];// +offset;
                GL.Vertex3(next.X, dZ, -next.Y);
            }
            GL.LineWidth(1);
            GL.End();
        }

        public override void GetBoundingBox(BoundingBox box)
        {
            for (int i = 0; i < Poly.Points.Count(); i++)
            {
                box.AddPoint(Poly.Points[i]);
            }
        }

        public Vector2D GetCentroid()
        {
            return GeoAlgorithms2D.Centroid(Poly.Points.ToList());
        }

        # endregion
    }
}

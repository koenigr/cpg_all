﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System.Diagnostics;
using System.Drawing.Drawing2D;
using Tektosyne.Geometry;
using System.Collections.Generic;

namespace CPlan.Geometry
{
    /// <summary>
    /// Represents a rectangular region in two-dimensional space, using <see cref="Double"/>
    /// coordinates.</summary>
    [Serializable]
    public class GRectangle : Surfaces
    {
        //############################################################################################################################################################################
        # region Properties
        /// <summary>
        /// Rect2D class.
        /// </summary>
        public Rect2D Rect { get; protected set; }

        ///// <summary>
        ///// Indicates if the rectangle is filled.
        ///// </summary>
        //public bool Filled { get; set; }

        /// <summary> Array containing the 4 points of the rectangle. Clockwise, starting with BottomLeft </summary>
        public Vector2D[] PolyPoints { get { return Rect.Points; } }

        /// <summary> List with the 4 Lines of the rectangle. </summary>
        public Line2D[] Edges { get {
            //Line2D[] edges = new Line2D[4];
            //edges[0] = new Line2D(Rect.BottomLeft, Rect.TopLeft);
            //edges[1] = new Line2D(Rect.TopLeft, Rect.TopRight);
            //edges[2] = new Line2D(Rect.TopRight, Rect.BottomRight);
            //edges[3] = new Line2D(Rect.BottomRight, Rect.BottomLeft);
            return Rect.Edges;
        } }

        /// <summary> Height of the rectangle. </summary>
        public double Height
        {
            get
            {
                return Rect.Height;
            }
            set
            {
                this.Rect.Height = value;
            }
        }

        /// <summary> Width of the rectangle. </summary>
        public double Width
        {
            get
            {
                return Rect.Width;
            }
            set
            {
                this.Rect.Width = value;
            }
        }

        /// <summary> 0 if nothing is selected, 1 if Point is selected, 2 if Line is selected </summary>
        private int selected = 0;

        /// <summary> 0 = bottomleft, 1 = topleft, 2 = topright, 3 = bottomright </summary>
        private int selectedPoint = 0;


        public double dZ = 0;

        //============================================================================================================================================================================
        /// <summary>
        /// Gets the center of the rectangle as <see cref="Vector2d"/>.
        /// </summary>
        public Vector2D Center
        {
            get
            {
                Vector2D diag = this.Rect.BottomRight - this.Rect.TopLeft;
                Vector2D diagonal = new Vector2D(diag.X, diag.Y);
                Vector2D temp = new Vector2D(this.Rect.TopLeft.X, this.Rect.TopLeft.Y);
                return (temp + 0.5 * diagonal);
            }
            private set { }
        }

        # endregion

        //############################################################################################################################################################################
        # region Constructors
        public GRectangle() { }
        /// <summary>
        /// Initialise a new instance of the <b>GRectangle</b> class in two-dimensional space, using <see cref="double"/> coordinates.
        /// Initializes a new GRectangle
        /// </summary>
        /// <param name="x1">The x-coordinate of the top left corner.</param>
        /// <param name="y1">The y-coordinate of the top left corner.</param>
        /// <param name="width">The x-coordinate of the bottom right corner.</param>
        /// <param name="height">The y-coordinate of the bottom right corner.</param>
        public GRectangle(double x1, double y1, double width, double height)
        {
            this.Rect = new Rect2D(x1, y1, width, height);        }

        //============================================================================================================================================================================
        /// <summary>
        /// Initializes a new instance of the <b>GRectangle</b> from a <see cref="Tektosyne.RectD"/>.
        /// </summary>
        /// <param name="rect">A <see cref="Tektosyne.Geometry.RectD"/>.</param>
        public GRectangle(Rect2D rect)
        {
            this.Rect = rect;
        }

        # endregion

        //############################################################################################################################################################################
        # region Methods

        /// <summary>
        /// Moves the <b>GRectangle</b>
        /// </summary>
        /// <param name="delta">The 2d vector to move the rectangle.</param>
        public override void Move(Vector2D delta)
        {
            this.Rect.Position += delta;
        }

        /// <summary>
        /// Is not implemented for <b>GRectangle</b>.
        /// </summary>
        public override bool Selector(Vector2D mPoint)
        {
            selected = 0;

            
            //if (IsPointInside(mPoint))
              //selected = 3; //Polygon selektiert

             
            double distance = 0;
            //= Math.Sqrt((Math.Pow(mPoint.X - Start.X, 2.0) - Math.Pow(mPoint.Y - Start.Y, 2.0)));
            //if ((distance <= pickingThreshold) && (LineD.DistanceSquared(mPoint) < 20))
              //  selected = 2; //Startpunkt = selected

            //alle Linien durchschauen
            Line2D curEdge;
            for (int i = 0; i < 3; i++)
            {
                curEdge = Edges[i];
                if (curEdge.DistanceToPoint(mPoint) <= PickingThreshold)
                {
                    selected = 2; //Linie = selected
                  //  selectedLineP1 = i;
                  //  selectedLineP2 = i + 1;
                }
            }

            // alle Punkte durchsuchen
            distance = Math.Sqrt((Math.Pow(mPoint.X - this.Rect.BottomLeft.X, 2.0) - Math.Pow(mPoint.Y - this.Rect.BottomLeft.Y, 2.0)));
                if ( distance <= PickingThreshold ) 
                {
                    selected = 1; //Punkt = selected
                    selectedPoint = 0;
                }
            distance = Math.Sqrt((Math.Pow(mPoint.X - this.Rect.TopLeft.X, 2.0) - Math.Pow(mPoint.Y - this.Rect.TopLeft.Y, 2.0)));
                if ( distance <= PickingThreshold ) 
                {
                    selected = 1; //Punkt = selected
                    selectedPoint = 1;
                }
            distance = Math.Sqrt((Math.Pow(mPoint.X - this.Rect.TopRight.X, 2.0) - Math.Pow(mPoint.Y - this.Rect.TopRight.Y, 2.0)));
                if ( distance <= PickingThreshold ) 
                {
                    selected = 1; //Punkt = selected
                    selectedPoint = 2;
                }
            distance = Math.Sqrt((Math.Pow(mPoint.X - this.Rect.BottomRight.X, 2.0) - Math.Pow(mPoint.Y - this.Rect.BottomRight.Y, 2.0)));
                if ( distance <= PickingThreshold ) 
                {
                    selected = 1; //Punkt = selected
                    selectedPoint = 3;
                }      

            if (selected != 0)
                return true;
            else
                return false;
        }
        


        //============================================================================================================================================================================
        /// <summary>
        /// Computes the area of the <b>GRectangle</b>
        /// </summary>
        /// <returns>
        /// The area of the <b>GRectangle</b>, as <see cref="double"/> value.
        /// </returns>
        public override Double GetArea()
        {
            double area;
            area = this.Rect.Width * this.Rect.Height;
            return area;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Draw the <b>GRectangle</b> to a <see cref="OpenTK.OpenGL"/> component
        /// </summary>
        public override void Draw()
        {
            
            if (selected == 1)
            {
                GLine tempLine = new GLine(Edges[selectedPoint]);

                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(SelectColor);
                GL.LineWidth(BorderWidth);
                GL.Begin(BeginMode.Quads);
                GL.Vertex3(tempLine.Start.Pos.X - HandleSize, dZ, -tempLine.Start.Pos.Y - HandleSize);
                GL.Vertex3(tempLine.Start.Pos.X - HandleSize, dZ, -tempLine.Start.Pos.Y + HandleSize);
                GL.Vertex3(tempLine.Start.Pos.X + HandleSize, dZ, -tempLine.Start.Pos.Y + HandleSize);
                GL.Vertex3(tempLine.Start.Pos.X + HandleSize, dZ, -tempLine.Start.Pos.Y - HandleSize);
                GL.End();
            }
            
            // Draw the filled rectangles 
            if (Filled)
            {
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(FillColor);
                GL.Begin(BeginMode.Quads);
                // GL.Rect(RectD.TopLeft.X, RectD.TopLeft.Y, RectD.BottomRight.X, RectD.BottomRight.Y);
                GL.Vertex3(Rect.TopLeft.X, dZ, -Rect.TopLeft.Y);
                GL.Vertex3(Rect.BottomRight.X, dZ, -Rect.TopLeft.Y);
                GL.Vertex3(Rect.BottomRight.X, dZ, -Rect.BottomRight.Y);
                GL.Vertex3(Rect.TopLeft.X, dZ, -Rect.BottomRight.Y);
                GL.End();
            }

            // Draw the outline of the rectangles   
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
            GL.LineWidth(BorderWidth);
            GL.Color4(BorderColor);
            GL.Begin(BeginMode.Quads);
        //  GL.Rect(RectD.TopLeft.X, RectD.TopLeft.Y, RectD.TopLeft.X, RectD.BottomRight.Y);
            GL.Vertex3(Rect.TopLeft.X, dZ, -Rect.TopLeft.Y);
            GL.Vertex3(Rect.BottomRight.X, dZ, -Rect.TopLeft.Y);
            GL.Vertex3(Rect.BottomRight.X, dZ, -Rect.BottomRight.Y);
            GL.Vertex3(Rect.TopLeft.X, dZ, -Rect.BottomRight.Y);
            //GL.LineWidth(1 * m_zoomFactor);
            GL.End();
            
        }

        public override void GetBoundingBox(BoundingBox box)
        {
            for (int i = 0; i < PolyPoints.Length; i++)
            {
                box.AddPoint(PolyPoints[i]);
            }
        }

        public bool ContainsPoint(Vector2D point)
        {
            return Rect.ContainsPoint(point);
        }

        # endregion
    }
}

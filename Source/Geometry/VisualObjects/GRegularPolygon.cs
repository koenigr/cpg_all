﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tektosyne.Geometry;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using OpenTK;

namespace CPlan.Geometry
{
    /// <summary>
    /// Represents a regular polygon in two-dimensional space, using <see cref="Double"/>
    /// coordinates.</summary>
    [Serializable]
    public class GRegularPolygon : Surfaces
    {
        //############################################################################################################################################################################
        # region Properties

        public Vector2D[] PolyPoints { get; protected set; }

        # endregion

        //############################################################################################################################################################################
        # region Constructors
        public GRegularPolygon() { }
        /// <summary>
        /// Initialise a new instance of the <b>GRegularPolygon</b> class in two-dimensional space 
        /// </summary>

        public GRegularPolygon(Vector2D[] vertices)
        {
            //this.RegularPolygon = regPolygon;
            //PointD[] vertices = RegularPolygon.Vertices;
            PolyPoints = vertices;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>GRegularPolygon</b> class in two-dimensional space 
        /// from a <see cref="Tektosyne.RegularPolygon"/>.
        /// </summary>

        /// <param name="startPt">The starting point (offset) for the <b>GRegularPolygon</b>.</param>
        public GRegularPolygon(Vector2D[] vertices, Vector2D startPt)
        {
            PolyPoints = vertices;
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i] += startPt;
            }
        }

        # endregion

        //############################################################################################################################################################################
        # region Methods
        /// <summary>
        /// Moves the <b>GRegularPolygon</b>
        /// </summary>
        /// <param name="delta">The 2d vector to move the regular polygon.</param>
        public override void Move(Vector2D delta)
        {
            Vector2D[] vertices = PolyPoints;
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i] = vertices[i] + delta;
            }
        }


        /// <summary>
        /// Is not implemented for <b>GRegularPolygon</b>.
        /// </summary>
        public override bool Selector(Vector2D mPoint)
        {
            return false;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Computes the area of the <b>GRegularPolygon</b>
        /// </summary>
        /// <returns>
        /// The area of the <b>GRegularPolygon</b>, as <see cref="double"/> value.
        /// </returns>
        public override Double GetArea()
        {
            double area;
            area = GeoAlgorithms2D.Area(PolyPoints.ToList());
            return area;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Draw the <b>GRegularPolygon</b> to a <see cref="OpenTK.OpenGL"/> component
        /// </summary>
        public override void Draw()
        {
            double dZ = 0;
            Vector2D[] vertices = PolyPoints;
            //PointD start = vertices[0] + offset;
            // Draw the filled polygon 
            //if (isFilled)
            {
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(FillColor);
                GL.Begin(BeginMode.Polygon);
                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector2D next = vertices[i];// +offset;
                    GL.Vertex3(next.X, dZ, -next.Y);
                }
                GL.End();
            }

            GL.LineWidth(BorderWidth*m_zoomFactor);
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
            GL.Color4(BorderColor);
            GL.Begin(BeginMode.Polygon);
            for (int i = 0; i < vertices.Length; i++)
            {
                Vector2D next = vertices[i];// +offset;
                GL.Vertex3(next.X, dZ, -next.Y);
            }
            GL.LineWidth(1);
            GL.End();
        }

        public override void GetBoundingBox(BoundingBox box)
        {
            for (int i = 0; i < PolyPoints.Count(); i++)
            {
                box.AddPoint(PolyPoints[i]);
            }
        }

        # endregion
    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = AnalysisPoint.cs 
//  Copyright (C) 2014/10/18  12:59 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Sven Schneider, Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using OpenTK;

namespace CPlan.Isovist2D
{
    [Serializable]
    public class AnalysisPoint
    {
        public double X, Y;

        public Dictionary<AnalysisMeasure, float> Results
        {
            get { return m_analysisResults; }
            set { m_analysisResults = value; }
        }

        // Todo: m_analysisResults and Properties are used for the same purpose in different ways! 
        Dictionary<AnalysisMeasure, float> m_analysisResults = new Dictionary<AnalysisMeasure, float>();

        public AnalysisPoint(Vector2d point)
        {
            X = point.X;
            Y = point.Y;
            Position = point;
            Properties = new Dictionary<AnalysisMeasure, float>();
            VisualNeighbors = new HashSet<AnalysisPoint>();
            MooreNeighbors = new HashSet<AnalysisPoint>();
            NeumannNeighbors = new HashSet<AnalysisPoint>();

        }

        // --- new part from Sven ---
        public AnalysisPoint(Vector2d point, Dictionary<AnalysisMeasure, float> properties = null)
        {
            X = point.X;
            Y = point.Y;
            Position = point;
            Properties = new Dictionary<AnalysisMeasure, float>();
            if (properties !=null)
                Properties = properties;
            VisualNeighbors = new HashSet<AnalysisPoint>();
            MooreNeighbors = new HashSet<AnalysisPoint>();
            NeumannNeighbors = new HashSet<AnalysisPoint>();
        }

        public bool tmpVisited; // custom Sven, evtl. not necessary.

        public Dictionary<AnalysisMeasure, float> Properties { get; set; }

        public Vector2d Position { get; set; }

        public int NodeXLVertexID { get; set; }

        public HashSet<AnalysisPoint> VisualNeighbors { get; set; }

        public HashSet<AnalysisPoint> NeumannNeighbors { get; set; }

        public HashSet<AnalysisPoint> MooreNeighbors { get; set; }

        public bool IsSimilarTo(AnalysisPoint directNeighbor, AnalysisPoint originP, double devIdentical, double devNew)
        {
            double dev = devIdentical;
            double dev2 = devNew;

            List<AnalysisPoint> identicalPoints = new List<AnalysisPoint>();
            List<AnalysisPoint> newPoints = new List<AnalysisPoint>();
            foreach (AnalysisPoint ap in directNeighbor.VisualNeighbors)
            {
                bool notInList = true;
                foreach (AnalysisPoint ap2 in originP.VisualNeighbors)
                {
                    if (ap == ap2)
                    {
                        identicalPoints.Add(ap);
                        notInList = false;
                        break;
                    }
                }
                if (notInList)
                    newPoints.Add(ap);
            }

            double ratioIdenticalToOrinal = (double)identicalPoints.Count / originP.VisualNeighbors.Count;
            double ratioNewToOrinal = (double)newPoints.Count / originP.VisualNeighbors.Count;

            if (ratioIdenticalToOrinal > dev && ratioNewToOrinal < dev2)
                return true;
            else return false;
        }
    }
}

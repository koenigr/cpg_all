﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GPUIsovistField.cs 
//  Copyright (C) 2014/10/18  12:59 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Sven Schneider, Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Cloo;

namespace CPlan.Isovist2D
{
    public class GPUIsovistField
    {
        protected static GPUIsovistField m_instance;

        float[] m_wallP1X;
        float[] m_wallP1Y;
        float[] m_wallP2X;
        float[] m_wallP2Y;
        float[] m_extWallPX;
        float[] m_extWallPY;
        float[] m_pX;
        float[] m_pY;
        float m_precision;
        float m_viewRange;

        ComputeProgram program;
        ComputePlatform platform;
        IList<ComputeDevice> devices;
        ComputeContextPropertyList properties;
        ComputeContext context;

        ComputeBuffer<float> wallP1X_buf;
        ComputeBuffer<float> wallP1Y_buf;
        ComputeBuffer<float> wallP2X_buf;
        ComputeBuffer<float> wallP2Y_buf;
        ComputeBuffer<float> extWallPX_buf;
        ComputeBuffer<float> extWallPY_buf;
        ComputeBuffer<float> pX_buf;
        ComputeBuffer<float> pY_buf;
        ComputeBuffer<float> area_buf;
        ComputeBuffer<float> areaDist_buf;
        ComputeBuffer<float> perimeter_buf;
        ComputeBuffer<float> minRadial_buf;
        ComputeBuffer<float> meanRadial_buf; 
        ComputeBuffer<float> maxRadial_buf;
        ComputeBuffer<float> occlusivity_buf;
        ComputeBuffer<float> stdDevRadials_buf;
        ComputeBuffer<float> variance_buf;
        ComputeBuffer<float> skewness_buf;
        ComputeBuffer<float> angle_buf;
        ComputeBuffer<float> nrOccl_buf;
        ComputeBuffer<bool> activeCells_buf;
        ComputeBuffer<float> extView_buf;
        ComputeBuffer<float> distExtWall_buf;
        ComputeKernel kernel;
        ComputeEventList eventList;
        ComputeCommandQueue commands;

        
        #region ComputationTextMultipleIsovistCalculation
        string clIsovistMultipleCalculation = @"
float2 LineIntersect(float L1P1X, float L1P1Y, float L1P2X, float L1P2Y,
float L2P1X, float L2P1Y,float L2P2X, float L2P2Y)
{
    float2 ptIntersection;
    ptIntersection.x = MAXFLOAT;
    ptIntersection.y = MAXFLOAT;
    
    float d = (L2P2Y - L2P1Y) * (L1P2X - L1P1X) - (L2P2X - L2P1X) * (L1P2Y - L1P1Y);
    float n_a = (L2P2X - L2P1X) * (L1P1Y - L2P1Y) - (L2P2Y - L2P1Y) * (L1P1X - L2P1X);
    float n_b = (L1P2X - L1P1X) * (L1P1Y - L2P1Y) - (L1P2Y - L1P1Y) * (L1P1X - L2P1X);

    if (d == 0)
        return ptIntersection;

    float ua = n_a / d;
    float ub = n_b / d;

    if (ua >= 0.0 && ua <= 1.0 && ub >= 0.0 && ub <= 1.0)
    {
        ptIntersection.x = L1P1X + (ua * (L1P2X - L1P1X));
        ptIntersection.y = L1P1Y + (ua * (L1P2Y - L1P1Y));
        return ptIntersection;
    }
    return ptIntersection;
}

bool PointOnLine(float pointX, float pointY, float startX, float startY, float endX, float endY)
{
float dxc = pointX - startX;
float dyc = pointY - startY;

float dxl = endX - startX;
float dyl = endY - startY;

float cross = dxc * dyl - dyc * dxl;

if (cross == 0)
  return true;
else
  return false;
}


float Map(float val, float minOrigin, float maxOrigin, float minTarget, float maxTarget)
{
    if (val > maxOrigin)
        return maxTarget;
    else
        return minTarget - ((minTarget - maxTarget) * ((val - minOrigin) / (maxOrigin - minOrigin)));
}

kernel void Calculate(
    global  read_only float* wallP1X,
    global  read_only float* wallP1Y,
    global  read_only float* wallP2X,
    global  read_only float* wallP2Y,
    global  read_only float* pX,
    global  read_only float* pY,
    
    global write_only float* area,
    global write_only float* areaDist,
    global write_only float* perimeter,    
    global write_only float* minRadial,
    global write_only float* meanRadial,
    global write_only float* maxRadial,
    global write_only float* occlusivity,
    global write_only float* stddevRadials,
    global write_only float* variance,
    global write_only float* skewness,
    global write_only float* cntocclusions,
    float exactness,
    int wallCount,
    global  read_only float* extWallPX,
    global  read_only float* extWallPY,
    int extWallPCount,
    global write_only float* extView,
    global write_only float* distExtWall,
    float viewRange
)
{
        int gID0 = get_global_id(0);
    
        int rayCount = (int) (2*3.14159265359 / exactness);

        float2 curPerimeterPoint;
        float2 oldPerimeterPoint;
        float2 curPWeighted;
        float2 oldPWeighted;
        float2 firstPerimeterPoint;
        float2 rayP1 = (float2){ pX[gID0], pY[gID0] };
        float tmp_minRadial = MAXFLOAT;
        float tmp_meanRadial = 0.0f;
        float tmp_maxRadial = -MAXFLOAT;
        float tmp_area = 0.0f;
        float tmp_areaDist = 0.0f;
        float tmp_perimeter = 0.0f;
        float tmp_occlusivity = 0.0f;
        float tmp_cntOcclusions = 0.0f;
        float tmp_extView = 0.0f;
        float tmp_distExtWall = MAXFLOAT;


        //Daylight und ExteriorView...................................................
        for (int i=0; i<extWallPCount; i++)
        {
            bool intersects = false;
            for (int j = 0; j < wallCount; j++)
            {
                if (!PointOnLine(extWallPX[i], extWallPY[i], wallP1X[j], wallP1Y[j], wallP2X[j], wallP2Y[j]))
                {                    
                    float2 iPoint = LineIntersect(rayP1.x, rayP1.y, extWallPX[i], extWallPY[i], wallP1X[j], wallP1Y[j], wallP2X[j], wallP2Y[j]);
                    if ((iPoint.x != MAXFLOAT) && (iPoint.y != MAXFLOAT))
                        intersects = true;                    
                }
            }

            if (!intersects)
            {
                tmp_extView = tmp_extView + 1;
                float delta_x = rayP1.x - extWallPX[i];
                float delta_y = rayP1.y - extWallPY[i];
                float dist = sqrt(delta_x * delta_x + delta_y * delta_y);
                if (dist < tmp_distExtWall)
                    tmp_distExtWall = dist;
            }
        }
        
        distExtWall[gID0] = Map(tmp_distExtWall, 0,10,5,0);

        //Isovist berechnen...........................................................
        int curWall = 0;
        int prevWall = 0; //für occlusivity
        
        for (int i = 0; i< rayCount; i++)
        {
            float angle = i*exactness;
            float l = viewRange;
            float2 rayP2;        
            rayP2.x = rayP1.x + sin(angle) * l;
            rayP2.y = rayP1.y + cos(angle) * l;

            float2 cutPoint;
            float mindist = MAXFLOAT;
            bool cutted = false; 

            for (int j = 0; j < wallCount; j++)
            {
                bool intersects = true;
                float2 iPoint = LineIntersect(rayP1.x, rayP1.y, rayP2.x, rayP2.y, wallP1X[j], wallP1Y[j], wallP2X[j], wallP2Y[j]);
                if ((iPoint.x == MAXFLOAT) && (iPoint.y == MAXFLOAT))
                    intersects = false;

                if (intersects)
                {
                    float delta_x = rayP1.x - iPoint.x;
                    float delta_y = rayP1.y - iPoint.y;
                    float dist = sqrt(delta_x * delta_x + delta_y * delta_y);

                    if (dist < mindist)
                    {
                        cutPoint = iPoint;
                        cutted = true;
                        mindist = dist;
                        curWall = j;
                    }
                }
            }           

            if (!cutted)
                cutPoint = rayP2;
            
            if (i == 0)
                firstPerimeterPoint = cutPoint;                

            oldPerimeterPoint = curPerimeterPoint;
            curPerimeterPoint = cutPoint;
            
            if (i != 0)
            {
                tmp_area = tmp_area + (curPerimeterPoint.x*oldPerimeterPoint.y-curPerimeterPoint.y*oldPerimeterPoint.x);
                                
                float distWeighted = sqrt(distance(rayP1, curPerimeterPoint));
                curPWeighted.x = rayP1.x + sin(angle) * distWeighted;
                curPWeighted.y = rayP1.y + cos(angle) * distWeighted;

                distWeighted = sqrt(distance(rayP1, oldPerimeterPoint));
                angle = (i-1)*exactness;
                oldPWeighted.x = rayP1.x + sin(angle) * distWeighted;
                oldPWeighted.y = rayP1.y + cos(angle) * distWeighted;

                tmp_areaDist = tmp_areaDist + (curPWeighted.x*oldPWeighted.y-curPWeighted.y*oldPWeighted.x);

                tmp_perimeter = tmp_perimeter + distance(curPerimeterPoint, oldPerimeterPoint);
                if (curWall != prevWall)
                {
                    tmp_occlusivity += distance(curPerimeterPoint, oldPerimeterPoint);
                    tmp_cntOcclusions = tmp_cntOcclusions + 1.0f;
                    prevWall = curWall;
                }
            }
            
            float dist = distance(rayP1, cutPoint);
            tmp_meanRadial = tmp_meanRadial + dist;

            if (dist < tmp_minRadial)
                tmp_minRadial = dist;
            if (dist > tmp_maxRadial)
                tmp_maxRadial = dist;           
        }

        tmp_area = tmp_area + (firstPerimeterPoint.x*curPerimeterPoint.y-firstPerimeterPoint.y*curPerimeterPoint.x);
        tmp_area = tmp_area/2;
           
        float distWeighted = sqrt(distance(rayP1, curPerimeterPoint));
        float angle = (rayCount-1)*exactness;
        curPWeighted.x = rayP1.x + sin(angle) * distWeighted;
        curPWeighted.y = rayP1.y + cos(angle) * distWeighted;
        distWeighted = sqrt(distance(rayP1, oldPerimeterPoint));
        oldPWeighted.x = rayP1.x + sin(0.0) * distWeighted;
        oldPWeighted.y = rayP1.y + cos(0.0) * distWeighted;
        tmp_areaDist = tmp_areaDist + (curPWeighted.x*oldPWeighted.y-curPWeighted.y*oldPWeighted.x);
        tmp_areaDist = tmp_areaDist/2;

        tmp_perimeter = tmp_perimeter + distance(firstPerimeterPoint, curPerimeterPoint);
        tmp_meanRadial=tmp_meanRadial/rayCount;

        minRadial[gID0] = tmp_minRadial;
        meanRadial[gID0] = tmp_meanRadial;
        maxRadial[gID0] = tmp_maxRadial;
        area[gID0] = tmp_area;
        areaDist[gID0] = tmp_areaDist;
        perimeter[gID0] = tmp_perimeter;
        occlusivity[gID0] = tmp_occlusivity;
        cntocclusions[gID0] = tmp_cntOcclusions;
        extView[gID0] = tmp_extView;
        

        //Isovist nochmal berechnen, wegen Variance, Skewness:
        float tmp_variance = 0;
        float tmp_skewness = 0;
        float tmp_stddevRadials = 0;
           

        for (int i = 0; i< rayCount; i++)
        {
            float angle = i*exactness;
            float l = 10000;
            float2 rayP2;        
            rayP2.x = rayP1.x + sin(angle) * l;
            rayP2.y = rayP1.y + cos(angle) * l;

            float2 cutPoint;
            float mindist = MAXFLOAT;
            bool cutted = false;
            
    
            for (int j = 0; j < wallCount; j++)
            {
                bool intersects = true;
                float2 iPoint = LineIntersect(rayP1.x, rayP1.y, rayP2.x, rayP2.y, wallP1X[j], wallP1Y[j], wallP2X[j], wallP2Y[j]);
                if ((iPoint.x == MAXFLOAT) && (iPoint.y == MAXFLOAT))
                    intersects = false;

                if (intersects)
                {
                    float delta_x = rayP1.x - iPoint.x;
                    float delta_y = rayP1.y - iPoint.y;
                    float dist = sqrt(delta_x * delta_x + delta_y * delta_y);

                    if (dist < mindist)
                    {
                        cutPoint = iPoint;
                        cutted = true;
                        mindist = dist;
                        curWall = j;
                    }
                }
            }
            if (!cutted)
                cutPoint = rayP2;
            
            if (i == 0)
                firstPerimeterPoint = cutPoint;                

            oldPerimeterPoint = curPerimeterPoint;
            curPerimeterPoint = cutPoint;            
            
            float dist = distance(rayP1, cutPoint);

            tmp_stddevRadials = tmp_stddevRadials + fabs(dist - tmp_meanRadial);
            tmp_variance = tmp_variance + ((dist - tmp_meanRadial)*(dist - tmp_meanRadial));
            tmp_skewness = tmp_variance + ((dist - tmp_meanRadial)*(dist - tmp_meanRadial)*(dist - tmp_meanRadial));
        }

        stddevRadials[gID0] = tmp_stddevRadials/rayCount;
        variance[gID0] = tmp_variance/rayCount;
        skewness[gID0] = tmp_skewness/rayCount;

}";

        #endregion
        /*
        #region ComputationTextIsovistFieldCalculation
        string clIsovistFieldCalculation = @"

float2 LineIntersect(float L1P1X, float L1P1Y, float L1P2X, float L1P2Y,
float L2P1X, float L2P1Y,float L2P2X, float L2P2Y)
{
    //See http://local.wasp.uwa.edu.au/~pbourke/geometry/lineline2d/
    
    float2 ptIntersection;
    ptIntersection.x = MAXFLOAT;
    ptIntersection.y = MAXFLOAT;
    
    float d = (L2P2Y - L2P1Y) * (L1P2X - L1P1X) - (L2P2X - L2P1X) * (L1P2Y - L1P1Y);
    float n_a = (L2P2X - L2P1X) * (L1P1Y - L2P1Y) - (L2P2Y - L2P1Y) * (L1P1X - L2P1X);
    float n_b = (L1P2X - L1P1X) * (L1P1Y - L2P1Y) - (L1P2Y - L1P1Y) * (L1P1X - L2P1X);

    if (d == 0)
        return ptIntersection;

    float ua = n_a / d;
    float ub = n_b / d;

    if (ua >= 0.0 && ua <= 1.0 && ub >= 0.0 && ub <= 1.0)
    {
        ptIntersection.x = L1P1X + (ua * (L1P2X - L1P1X));
        ptIntersection.y = L1P1Y + (ua * (L1P2Y - L1P1Y));
        return ptIntersection;
    }
    return ptIntersection;
}


kernel void Calculate(
    global  read_only float* wallP1X,
    global  read_only float* wallP1Y,
    global  read_only float* wallP2X,
    global  read_only float* wallP2Y,
    global write_only float* area,
    global write_only float* areaDist,
    global write_only float* perimeter,    
    global write_only float* minRadial,
    global write_only float* maxRadial,
    global write_only float* occlusivity,
    global write_only float* viewingangle,
    global write_only int* nrOcclEdges,
    global write_only bool* activeCells,

    float startPX,
    float startPY,
    float cellSize,
    int countX,
    int countY,
    float exactness,
    int wallCount)

{
    int gID0 = get_global_id(0);
    int gID1 = get_global_id(1);
    
    if ( ( gID0 < countX) && (gID1 < countY) && (activeCells[gID1 * countX + gID0]) )
    {
        int rayCount = (int) (2*M_PI / exactness);
        int cntRaysCutted = 0;
        float2 curPerimeterPoint;
        float2 oldPerimeterPoint;
        float2 curPWeighted;
        float2 oldPWeighted;
        float2 firstPerimeterPoint;
        float2 rayP1 = (float2){ startPX+cellSize*gID0+cellSize/2, startPY+cellSize*gID1+cellSize/2 };
        float tmp_minRadial = MAXFLOAT;
        float tmp_maxRadial = -MAXFLOAT;
        float tmp_area = 0.0f;
        float tmp_areaDist = 0.0f;
        float tmp_perimeter = 0.0f;
        float tmp_occlusivity = 0.0f;
        float tmp_nrOccl = 0.0f;
        float tmp_angle = 0.0f;        

        int curWall = 0;
        int prevWall = 0; //für occlusivity
        
        //Isovist berechnen:
        for (int i = 0; i< rayCount; i++)
        {
            float angle = i*exactness;
            float l = 10000;
            float2 rayP2;        
            rayP2.x = rayP1.x + sin(angle) * l;
            rayP2.y = rayP1.y + cos(angle) * l;

            float2 cutPoint;
            float mindist = MAXFLOAT;
            bool cutted = false;
            
    
            for (int j = 0; j < wallCount; j++)
            {
                bool intersects = true;
                float2 iPoint = LineIntersect(rayP1.x, rayP1.y, rayP2.x, rayP2.y, wallP1X[j], wallP1Y[j], wallP2X[j], wallP2Y[j]);
                if ((iPoint.x == MAXFLOAT) && (iPoint.y == MAXFLOAT))
                    intersects = false;

                if (intersects)
                {
                    float delta_x = rayP1.x - iPoint.x;
                    float delta_y = rayP1.y - iPoint.y;
                    float dist = sqrt(delta_x * delta_x + delta_y * delta_y);

                    if (dist < mindist)
                    {
                        cutPoint = iPoint;
                        cutted = true;
                        mindist = dist;
                        curWall = j;
                    }
                }
            }
            if (!cutted)
                cutPoint = rayP2;
            
            //Winkel berechnen
            if (cutted)
            {
                float2 vecWall = (float2){wallP2X[curWall] - wallP1X[curWall], wallP2Y[curWall] - wallP1Y[curWall] };
                float2 vecRay = (float2){cutPoint.x - rayP1.x, cutPoint.y - rayP1.y};
                float a = acos( (vecWall.x*vecRay.x+vecWall.y*vecRay.y) / (sqrt(vecWall.x*vecWall.x+vecWall.y*vecWall.y)*sqrt(vecRay.x*vecRay.x+vecRay.y*vecRay.y) ) );
                if (a > M_PI/2)
                    a = a - M_PI/2;
                tmp_angle = tmp_angle + a;
                cntRaysCutted = cntRaysCutted+1;
            }

            if (i == 0)
                firstPerimeterPoint = cutPoint;                

            oldPerimeterPoint = curPerimeterPoint;
            curPerimeterPoint = cutPoint;
            
            if (i != 0)
            {
                tmp_area = tmp_area + (curPerimeterPoint.x*oldPerimeterPoint.y-curPerimeterPoint.y*oldPerimeterPoint.x);
                                
                float distWeighted = sqrt(distance(rayP1, curPerimeterPoint));
                float angle = i*exactness;
                curPWeighted.x = rayP1.x + sin(angle) * distWeighted;
                curPWeighted.y = rayP1.y + cos(angle) * distWeighted;

                distWeighted = sqrt(distance(rayP1, oldPerimeterPoint));
                angle = (i-1)*exactness;
                oldPWeighted.x = rayP1.x + sin(angle) * distWeighted;
                oldPWeighted.y = rayP1.y + cos(angle) * distWeighted;

                tmp_areaDist = tmp_areaDist + (curPWeighted.x*oldPWeighted.y-curPWeighted.y*oldPWeighted.x);
                float dist = distance(curPerimeterPoint, oldPerimeterPoint);
                tmp_perimeter = tmp_perimeter + dist;
                if ((curWall != prevWall) && (dist < 0.2) )
                {
                    tmp_occlusivity += dist;
                    tmp_nrOccl += 1;
                    prevWall = curWall;
                }
            }
            
            float dist = distance(rayP1, cutPoint);
            if (dist < tmp_minRadial)
                tmp_minRadial = dist;
            if (dist > tmp_maxRadial)
                tmp_maxRadial = dist;

            

        }

        tmp_area = tmp_area + (firstPerimeterPoint.x*curPerimeterPoint.y-firstPerimeterPoint.y*curPerimeterPoint.x);
        tmp_area = tmp_area/2;
           
        float distWeighted = sqrt(distance(rayP1, curPerimeterPoint));
        float angle = (rayCount-1)*exactness;
        curPWeighted.x = rayP1.x + sin(angle) * distWeighted;
        curPWeighted.y = rayP1.y + cos(angle) * distWeighted;
        distWeighted = sqrt(distance(rayP1, oldPerimeterPoint));
        oldPWeighted.x = rayP1.x + sin(0.0) * distWeighted;
        oldPWeighted.y = rayP1.y + cos(0.0) * distWeighted;
        tmp_areaDist = tmp_areaDist + (curPWeighted.x*oldPWeighted.y-curPWeighted.y*oldPWeighted.x);
        tmp_areaDist = tmp_areaDist/2;

        tmp_perimeter = tmp_perimeter + distance(firstPerimeterPoint, curPerimeterPoint);

        minRadial[gID1 * countX + gID0] = tmp_minRadial;
        maxRadial[gID1 * countX + gID0] = tmp_maxRadial;
        area[gID1 * countX + gID0] = tmp_area;
        areaDist[gID1 * countX + gID0] = tmp_areaDist;
        perimeter[gID1 * countX + gID0] = tmp_perimeter;
        occlusivity[gID1 * countX + gID0] = tmp_occlusivity;
        viewingangle[gID1 * countX + gID0] = tmp_angle;
        nrOcclEdges[gID1 * countX + gID0] = tmp_nrOccl;
    }
}
";

        #endregion
        */
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructor

        protected GPUIsovistField()
        {
            InitializeGPU();
        }

        ~GPUIsovistField()
        {
            m_instance = null;
        }

        

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        static public GPUIsovistField GetInstance()
        {
            if (m_instance == null)
                m_instance = new GPUIsovistField();
            return m_instance;
        }

        public void Create(float[] wallP1X, float[] wallP1Y, float[] wallP2X, float[] wallP2Y, float[] extWallPX, float[] extWallPY, float[] pX, float[] pY, float precision, float viewRange)
        {
            m_wallP1X = wallP1X;
            m_wallP1Y = wallP1Y;
            m_wallP2X = wallP2X;
            m_wallP2Y = wallP2Y;
            if (extWallPX.Length != 0)
            {
                m_extWallPX = extWallPX;
                m_extWallPY = extWallPY;
            }
            else
            {
                m_extWallPX = new float[1];
                m_extWallPX[0] = float.MaxValue;          
                m_extWallPY = new float[1];
                m_extWallPY[0] = float.MaxValue;
            }
            m_precision = precision;
            m_pX = pX;
            m_pY = pY;
            m_viewRange = viewRange;

            SetParameters();
        }


        private void InitializeGPU()
        {
            try
            {
                //ComputeDevice dev = ComputePlatform.Platforms.
                if (ComputePlatform.Platforms.Count > 1)
                {
                    platform = ComputePlatform.Platforms[ComputePlatform.Platforms.Count-1];
                    for (int p = 0; p < ComputePlatform.Platforms.Count; p++)
                    {
                        Console.WriteLine(ComputePlatform.Platforms[p].Name);
                        Console.WriteLine(ComputePlatform.Platforms[p].Vendor);
                        Console.WriteLine(ComputePlatform.Platforms[p].Profile);
                        Console.WriteLine(ComputePlatform.Platforms[p].Version);
                        Console.WriteLine(ComputePlatform.Platforms[p].Devices.Select(d => d.Name + d.Available).Aggregate((a,b) => a + " " + b));
                        string vers = ComputePlatform.Platforms[p].Version;
                        if (vers.Contains("CUDA"))  platform = ComputePlatform.Platforms[p];
                    }
                }
                else platform = ComputePlatform.Platforms[0];

                devices = new List<ComputeDevice>();
                devices.Add(platform.Devices[0]);
                properties = new ComputeContextPropertyList(platform);
                context = new ComputeContext(devices, properties, null, IntPtr.Zero);
                eventList = new ComputeEventList();
                commands = new ComputeCommandQueue(context, context.Devices[0], ComputeCommandQueueFlags.None);

                program = new ComputeProgram(context, clIsovistMultipleCalculation);

                program.Build(null, null, null, IntPtr.Zero);
            }
            catch (System.Exception ex)
            {
                StringBuilder output = new StringBuilder();
                StringWriter log = new StringWriter(output);
                log.WriteLine(ex.ToString());
                log.WriteLine(" ");
                if (program != null)
                    log.WriteLine(program.GetBuildLog(context.Devices[0]));

                string[] text = ParseLines(output.ToString());
                //MyTrace.Trace(String.Format(ex.ToString()));
                //MyTrace.Trace(program.GetBuildLog(context.Devices[0]));
            }
            try
            {
                kernel = program.CreateKernel("Calculate");
            }
            catch (System.Exception ex)
            {
                StringBuilder output = new StringBuilder();
                StringWriter log = new StringWriter(output);
                log.WriteLine(ex.ToString());
                log.WriteLine(" ");
                if (program != null)
                    log.WriteLine(program.GetBuildLog(context.Devices[0]));

                string[] text = ParseLines(output.ToString());
                //MyTrace.Trace(String.Format(ex.ToString()));
                //MyTrace.Trace(program.GetBuildLog(context.Devices[0]));
            }

        }

        public void SetParameters()
        {
            int cnt = m_pY.Length;
            //activeCells_buf = new ComputeBuffer<bool>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, isovistField.ActiveCells);
            area_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);
            areaDist_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);
            minRadial_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);
            meanRadial_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt); 
            maxRadial_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);
            perimeter_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);
            occlusivity_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);
            stdDevRadials_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);
            variance_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);
            skewness_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);
            nrOccl_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);
            extView_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);
            distExtWall_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, cnt);

            wallP1X_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_wallP1X);
            wallP1Y_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_wallP1Y);
            wallP2X_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_wallP2X);
            wallP2Y_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_wallP2Y);
            extWallPX_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_extWallPX);
            extWallPY_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_extWallPY);
            pX_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_pX);
            pY_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, m_pY);

            //Übergabe der Parameter:
            kernel.SetMemoryArgument(0, wallP1X_buf);
            kernel.SetMemoryArgument(1, wallP1Y_buf);
            kernel.SetMemoryArgument(2, wallP2X_buf);
            kernel.SetMemoryArgument(3, wallP2Y_buf);

            kernel.SetMemoryArgument(4, pX_buf);
            kernel.SetMemoryArgument(5, pY_buf);

            kernel.SetMemoryArgument(6, area_buf);
            kernel.SetMemoryArgument(7, areaDist_buf);
            kernel.SetMemoryArgument(8, perimeter_buf);
            kernel.SetMemoryArgument(9, minRadial_buf);
            kernel.SetMemoryArgument(10, meanRadial_buf);
            kernel.SetMemoryArgument(11, maxRadial_buf);
            kernel.SetMemoryArgument(12, occlusivity_buf);
            kernel.SetMemoryArgument(13, stdDevRadials_buf);
            kernel.SetMemoryArgument(14, variance_buf);
            kernel.SetMemoryArgument(15, skewness_buf);
            kernel.SetMemoryArgument(16, nrOccl_buf);

            kernel.SetValueArgument(17, m_precision);
            kernel.SetValueArgument(18, m_wallP1X.Length);
            kernel.SetMemoryArgument(19, extWallPX_buf);
            kernel.SetMemoryArgument(20, extWallPY_buf);
           
            if ((m_extWallPX[0] == float.MaxValue) && (m_extWallPX[0] == float.MaxValue))
                kernel.SetValueArgument(21, 0);
            else
                kernel.SetValueArgument(21, m_extWallPX.Length);
            kernel.SetMemoryArgument(22, extView_buf);
            kernel.SetMemoryArgument(23, distExtWall_buf);
            kernel.SetValueArgument(24, m_viewRange);

        }


        private string[] ParseLines(string text)
        {
            List<string> lineList = new List<string>();
            StringReader reader = new StringReader(text);
            string line = reader.ReadLine();
            while (line != null)
            {
                lineList.Add(line);
                line = reader.ReadLine();
            }
            return lineList.ToArray();
        }

        public float[,] Run()
        {
            float[,] results = new float[m_pX.Length, 14];
 
            try
            {
                if (m_wallP1X.Length > 0)
                {
                    if (m_wallP1X.Length <= 0)
                        return null;

                    long[] globalSize = new long[] { (long)Math.Ceiling(m_pX.Length / 256.0) * 256 };
                    long[] localSize = new long[] { 256 };
                        
                    commands.Execute(kernel, null, globalSize, localSize, eventList);
                    commands.Finish();

                    float[] area = new float[m_pX.Length];
                    float[] areaDistWeight = new float[m_pX.Length];
                    float[] perimeter = new float[m_pX.Length];
                    float[] minradial = new float[m_pX.Length];
                    float[] meanradial = new float[m_pX.Length];
                    float[] maxradial = new float[m_pX.Length];
                    float[] occlusivity = new float[m_pX.Length];
                    float[] stddevradial = new float[m_pX.Length];
                    float[] variance = new float[m_pX.Length];
                    float[] skewness = new float[m_pX.Length];
                    float[] cntOccl = new float[m_pX.Length];

                    float[] extView = new float[m_pX.Length];
                    float[] distFromExtWall = new float[m_pX.Length];

                    commands.ReadFromBuffer(area_buf, ref area, false, eventList);
                    commands.ReadFromBuffer(areaDist_buf, ref areaDistWeight, false, eventList);
                    commands.ReadFromBuffer(perimeter_buf, ref perimeter, false, eventList);
                    commands.ReadFromBuffer(minRadial_buf, ref minradial, false, eventList);
                    commands.ReadFromBuffer(meanRadial_buf, ref meanradial, false, eventList);
                    commands.ReadFromBuffer(maxRadial_buf, ref maxradial, false, eventList);
                    commands.ReadFromBuffer(occlusivity_buf, ref occlusivity, false, eventList);
                    commands.ReadFromBuffer(stdDevRadials_buf, ref stddevradial, false, eventList);
                    commands.ReadFromBuffer(variance_buf, ref variance, false, eventList);
                    commands.ReadFromBuffer(skewness_buf, ref skewness, false, eventList);
                    commands.ReadFromBuffer(nrOccl_buf, ref cntOccl, false, eventList);
                    commands.ReadFromBuffer(extView_buf, ref extView, false, eventList);
                    commands.ReadFromBuffer(distExtWall_buf, ref distFromExtWall, false, eventList);

                    commands.Finish();


                    for (int i = 0; i < m_pX.Length; i++)
                    {
                        results[i, 0] = area[i];
                        results[i, 1] = (float)perimeter[i];
                        results[i, 2] = (float)(4.0 * Math.PI * area[i] / (perimeter[i] * perimeter[i]));
                        results[i, 3] = (float)occlusivity[i];
                        results[i, 4] = (float)minradial[i];
                        results[i, 5] = (float)maxradial[i];
                        results[i, 6] = (float)areaDistWeight[i];
                        results[i, 7] = (float)meanradial[i];
                        results[i, 8] = (float)stddevradial[i];
                        results[i, 9] = (float)variance[i];
                        results[i, 10] = (float)skewness[i];
                        results[i, 11] = (float)cntOccl[i];
                        results[i, 12] = (float)extView[i];
                        results[i, 13] = (float)distFromExtWall[i];
                    }
                }
            }
            
            catch (System.Exception ex)
            {
                results = null;            
            }

            return results;
        }


        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using CPlan.Geometry;
using CPlan.Isovist2D;
using OpenTK;
using Smrf.NodeXL.Core;
using Smrf.NodeXL.Algorithms;
using QuickGraph;

namespace CPlan.Evaluation
{
    [Serializable]
    public class GridGraphAnalysis
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Attributes

        private List<Line2D> m_obstacleLines =new List<Line2D>();
        private List<Line2D> m_exteriorLines = new List<Line2D>();
        private List<Line2D> m_boundaryLines = new List<Line2D>();

        //Analysis Grid
        //private bool m_useGPU = true;
        private float m_precision = 0.02f;
        private Vector2d m_startP, m_endP;
        private bool m_calcGraph = true;
        private bool m_calcVisbilityGraph = true;
        private bool m_calcCluster = false;
        private bool m_calcNeumannGraph = false;
        private bool m_calcMooreGraph = false;
        private bool m_calcAnalysisGrid = true;

        [NonSerialized]
        private List<AnalysisPoint> m_analysisPoints;
        [NonSerialized]
        private List<APEdge> m_visibilityEdges;
        [NonSerialized]
        private List<APEdge> m_neumannEdges;
        [NonSerialized]
        private List<APEdge> m_mooreEdges;

        private double m_cellSize = -1;

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        public List<AnalysisPoint> AnalysisPoints
        {
            get { return m_analysisPoints; }
        }

        public double Intelligibility
        {
            get
            {
                double rsquared = -1;
                double yintercept;
                double slope;

                try
                {                   
                    if (AnalysisPoints.Count > 0)
                    {
                        double[] integration = new double[AnalysisPoints.Count];
                        double[] connectivity = new double[AnalysisPoints.Count];

                        for (int i = 0; i < AnalysisPoints.Count; i++)
                        {
                            integration[i] = AnalysisPoints[i].Properties[AnalysisMeasure.ClosenessVGA];
                            connectivity[i] = AnalysisPoints[i].VisualNeighbors.Count;
                        }
                        LinearRegression(integration, connectivity, 0, integration.Length, out rsquared, out yintercept, out slope);
                    }
                }
                catch
                {
                    rsquared = -1;
                }
                return rsquared;
            }
        }

        #endregion
        
         //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        public GridGraphAnalysis(){}

        public GridGraphAnalysis(List<AnalysisPoint> _analysisPoints, List<Line2D> _boundaryLines, List<Line2D> _boundaryLinesExterior, List<Line2D> _obstacleLines)
        {
            if (_boundaryLines != null)
                m_boundaryLines = _boundaryLines;
            if (_boundaryLinesExterior != null)
                m_exteriorLines = _boundaryLinesExterior;
            if (_obstacleLines != null)
                m_obstacleLines = _obstacleLines;

            m_analysisPoints = new List<AnalysisPoint>();
            Poly2D lotBoundary = new Poly2D(_boundaryLines);
            m_cellSize = _analysisPoints[1].Position.Distance(_analysisPoints[0].Position);

            foreach (AnalysisPoint ap in _analysisPoints)
            {
                AnalysisPoint newAP = new AnalysisPoint(ap.X, ap.Y, ap.Results);
                m_analysisPoints.Add(newAP);
                if (lotBoundary != null)
                {
                    double dist = lotBoundary.Distance(newAP.Position);

                    if (lotBoundary.ContainsPoint(newAP.Position))
                        newAP.Properties[AnalysisMeasure.InsideBoundary] = 0;
                    else
                        newAP.Properties[AnalysisMeasure.InsideBoundary] = (float)dist;
                }
            }
        }


        public GridGraphAnalysis(GridGraphAnalysis old, bool deleteMost)
        {
            m_boundaryLines = new List<Line2D>(old.m_boundaryLines);
            m_exteriorLines = new List<Line2D>(old.m_exteriorLines);
            m_obstacleLines = new List<Line2D>(old.m_obstacleLines);

            m_cellSize = old.m_cellSize;
            m_calcCluster = old.m_calcCluster;
            m_calcGraph = old.m_calcGraph;
            m_calcMooreGraph = old.m_calcMooreGraph;
            m_calcNeumannGraph = old.m_calcNeumannGraph;
            m_calcVisbilityGraph = old.m_calcVisbilityGraph;
            m_startP = new Vector2d(old.m_startP.X, old.m_startP.Y); 
            m_endP = new Vector2d(old.m_endP.X, old.m_endP.Y);
            m_precision = old.m_precision;

            if (!deleteMost)
            {
                if (old.m_analysisPoints != null)
                    m_analysisPoints = new List<AnalysisPoint>(old.m_analysisPoints);
                if (old.m_mooreEdges != null)
                    m_mooreEdges = new List<APEdge>(old.m_mooreEdges);
                if (old.m_neumannEdges != null) 
                    m_neumannEdges = new List<APEdge>(old.m_neumannEdges);
            }
            else
            {
                m_analysisPoints = new List<AnalysisPoint>();
                if (old.m_analysisPoints != null)
                {
                    foreach (AnalysisPoint ap in old.m_analysisPoints)
                    {
                        Dictionary<AnalysisMeasure, float> measure = new Dictionary<AnalysisMeasure, float>();
                        measure.Add(AnalysisMeasure.ClosenessVGA, ap.Properties[AnalysisMeasure.ClosenessVGA]);
                        AnalysisPoint apNew = new AnalysisPoint(ap.Position.X, ap.Position.Y, measure);
                        m_analysisPoints.Add(apNew);
                    }
                }
            }

        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        #region Helper

        /// <summary>
        /// Fits a line to a collection of (x,y) points.
        /// </summary>
        /// <param name="xVals">The x-axis values.</param>
        /// <param name="yVals">The y-axis values.</param>
        /// <param name="inclusiveStart">The inclusive inclusiveStart index.</param>
        /// <param name="exclusiveEnd">The exclusive exclusiveEnd index.</param>
        /// <param name="rsquared">The r^2 value of the line.</param>
        /// <param name="yintercept">The y-intercept value of the line (i.e. y = ax + b, yintercept is b).</param>
        /// <param name="slope">The slop of the line (i.e. y = ax + b, slope is a).</param>
        public static void LinearRegression(double[] xVals, double[] yVals,
                                            int inclusiveStart, int exclusiveEnd,
                                            out double rsquared, out double yintercept,
                                            out double slope)
        {
            double sumOfX = 0;
            double sumOfY = 0;
            double sumOfXSq = 0;
            double sumOfYSq = 0;
            double ssX = 0;
            double ssY = 0;
            double sumCodeviates = 0;
            double sCo = 0;
            double count = exclusiveEnd - inclusiveStart;

            for (int ctr = inclusiveStart; ctr < exclusiveEnd; ctr++)
            {
                double x = xVals[ctr];
                double y = yVals[ctr];
                sumCodeviates += x * y;
                sumOfX += x;
                sumOfY += y;
                sumOfXSq += x * x;
                sumOfYSq += y * y;
            }
            ssX = sumOfXSq - ((sumOfX * sumOfX) / count);
            ssY = sumOfYSq - ((sumOfY * sumOfY) / count);
            double RNumerator = (count * sumCodeviates) - (sumOfX * sumOfY);
            double RDenom = (count * sumOfXSq - (sumOfX * sumOfX))
             * (count * sumOfYSq - (sumOfY * sumOfY));
            sCo = sumCodeviates - ((sumOfX * sumOfY) / count);

            double meanX = sumOfX / count;
            double meanY = sumOfY / count;
            double dblR = RNumerator / Math.Sqrt(RDenom);
            rsquared = dblR * dblR;
            yintercept = meanY - ((sCo / ssX) * meanX);
            slope = sCo / ssX;
        }

        public void ResetAnalysisPointVisited()
        {
            foreach (AnalysisPoint ap in AnalysisPoints)
                ap.tmpVisited = false;
        }

        public double GetAverage(AnalysisMeasure measure)
        {
            double sum = 0;
            foreach (AnalysisPoint ap in AnalysisPoints)
                sum += ap.Properties[measure];

            return sum / AnalysisPoints.Count;
        }

        public List<Line2D> GetAllLines()
        {
            List<Line2D> allLines = new List<Line2D>();
            
            if (m_exteriorLines != null)
                foreach (Line2D l in m_exteriorLines)
                    allLines.Add(l);

            if (m_obstacleLines != null) 
                foreach (Line2D l in m_obstacleLines)
                    allLines.Add(l);

            if (m_boundaryLines != null) 
                foreach (Line2D l in m_boundaryLines)
                    allLines.Add(l);

            return allLines;
        }

        public List<Line2D> GetBoundaryLines()
        {
            List<Line2D> allLines = new List<Line2D>();

            if (m_exteriorLines != null)
                foreach (Line2D l in m_exteriorLines)
                    allLines.Add(l);
            
            if (m_boundaryLines != null)
                foreach (Line2D l in m_boundaryLines)
                    allLines.Add(l);


            return allLines;
        }

        public AnalysisPoint GetAnalysisPoint(Vector2d vec)
        {
            AnalysisPoint ap = null;
            double minDist = double.MaxValue;

            foreach (AnalysisPoint a in AnalysisPoints)
            {
                double dist = a.Position.Distance(vec);
                if (dist < minDist)
                {
                    minDist = dist;
                    ap = a;
                }
            }
            return ap;
        }


        double IntervisibilityPointToPoint(Vector2d p1, Vector2d p2)
        {
            //Sichtbarkeitstest
            List<Line2D> obstacleLines = GetAllLines();
            Line2D sightline = new Line2D(p1, p2);
            bool isVisible = true;
            foreach (Line2D obstacleLine in obstacleLines)
                if (sightline.Intersect(obstacleLine))
                    isVisible = false;

            if (isVisible)
                return 1;
            else
                return 0;
        }

        double IntervisibilityGridToPoint(List<AnalysisPoint> pointList, Vector2d point)
        {
            int nrAp1 = pointList.Count;
            double intervisibility = 0;
            int cntInteresecting = 0;
            foreach (AnalysisPoint ap1 in pointList)
            {
                //Sichtbarkeitstest
                List<Line2D> obstacleLines = GetAllLines();
                Line2D sightline = new Line2D(ap1.Position, point);
                bool intersects = false;
                foreach (Line2D obstacleLine in obstacleLines)
                    if (sightline.Intersect(obstacleLine))
                        intersects = true;

                if (intersects)
                    cntInteresecting++;
            }

            intervisibility += (double)(nrAp1 - cntInteresecting) / (double)nrAp1;

            return intervisibility;
        }


        #endregion
        
        #region AnalysisGrid

        public void CalculateField()
        {
            List<Line2D> allLines = GetAllLines();

            //Create Visibility Graph
            if (m_calcGraph)
            {
                CreateGraph(allLines);
                CalculateGraphMeasures();
            }
        }

        public void CalculateGraphMeasures()
        {
            Smrf.NodeXL.Core.Graph graph = new Smrf.NodeXL.Core.Graph();

            foreach (AnalysisPoint ap in m_analysisPoints)
            {
                Vertex vertex = new Vertex();
                graph.Vertices.Add(vertex);
                ap.NodeXLVertexID = vertex.ID;
            }

            BrandesFastCentralityCalculator centralityCalculator = new BrandesFastCentralityCalculator();
            //BackgroundWorker bgWorker = new BackgroundWorker();
            ICollection<Community> communities = null;
            ClusterCalculator clusterCalculator = new ClusterCalculator();
            List<AnalysisPoint> tmpNodes = new List<AnalysisPoint>();

            GraphMetricCalculatorBase.SetSnapGraphMetricCalculatorPath("../../ExternalLibs/NodeXL/SnapGraphMetricCalculator.exe");

            if (m_calcVisbilityGraph)
            {
                //VISIBILITY GRAPH..............................................
                /*bool weighted1a = false;
                bool weighted1b = false;
                bool weighted2 = false;
                foreach (APEdge edge in m_visibilityEdges)
                {
                    try
                    {                     
                        IVertex vertex1, vertex2;
                        graph.Vertices.Find(edge.Node1.NodeXLVertexID, out vertex1);
                        graph.Vertices.Find(edge.Node2.NodeXLVertexID, out vertex2);
                        graph.Edges.Add(vertex1, vertex2, false);

                        if (!weighted1a)
                            if (edge.Node1.Position.IsSimilarTo(new Vector2d(5.7, 200), 4.5))
                            {
                                weighted1a = true;
                                for (int i = 0; i < 70; i++)
                                {
                                    Vertex WeightVertex = new Vertex();
                                    graph.Vertices.Add(WeightVertex);
                                    graph.Edges.Add(vertex1, WeightVertex, false);
                                }
                            }
                        
                        if (!weighted1b)
                            if (edge.Node1.Position.IsSimilarTo(new Vector2d(213, 202), 4.5))
                            {
                                weighted1b = true;
                                for (int i = 0; i < 70; i++)
                                {
                                    Vertex WeightVertex = new Vertex();
                                    graph.Vertices.Add(WeightVertex);
                                    graph.Edges.Add(vertex1, WeightVertex, false);
                                }
                            }

                        if (!weighted2)
                        {
                            if (edge.Node1.Position.IsSimilarTo(new Vector2d(106, 13), 6))
                            {
                                weighted2 = true;
                                for (int i = 0; i < 200; i++)
                                {
                                    Vertex WeightVertex = new Vertex();
                                    graph.Vertices.Add(WeightVertex);
                                    graph.Edges.Add(vertex1, WeightVertex, false);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    { }
                }
                */

                Dictionary<Int32, BrandesVertexCentralities> metricsCentralityVisibility = new Dictionary<int, BrandesVertexCentralities>();
                try
                {
                    centralityCalculator.TryCalculateGraphMetrics(graph, null, out metricsCentralityVisibility);
                }
                catch (Exception ex) { }

                foreach (AnalysisPoint ap in m_analysisPoints)
                {
                    ap.Properties.Add(AnalysisMeasure.ClosenessVGA, (float)metricsCentralityVisibility[ap.NodeXLVertexID].ClosenessCentrality);
                    ap.Properties.Add(AnalysisMeasure.BetweennessVGA, (float)metricsCentralityVisibility[ap.NodeXLVertexID].BetweennessCentrality);
                }

                /*
                ClusteringCoefficientCalculator clusteringCoefficientCalculator = new ClusteringCoefficientCalculator();
                Dictionary<int, double> metricClusteringCoeff = new Dictionary<int, double>();
                clusteringCoefficientCalculator.TryCalculateGraphMetrics(graph, null, out metricClusteringCoeff);

                foreach (AnalysisPoint ap in m_analysisPoints)
                    ap.Properties.Add(AnalysisMeasure.ClusteringCoefficient, metricClusteringCoeff[ap.NodeXLVertexID]);
                */

                if (m_calcCluster)
                {
                    clusterCalculator.Algorithm = ClusterAlgorithm.WakitaTsurumi;

                    try { clusterCalculator.TryCalculateGraphMetrics(graph, null, out communities); }
                    catch (Exception ex)
                    {
                        int a = 5;
                    }

                    foreach (AnalysisPoint node in m_analysisPoints)
                        tmpNodes.Add(node);

                    List<List<AnalysisPoint>> clusterFirstProcess = new List<List<AnalysisPoint>>();

                    //while (tmpNodes.Count != 0)
                    //foreach (AnalysisPoint curNode in m_analysisPoints)
                    {
                        //AnalysisPoint curNode = tmpNodes[0];

                        int k = 0;
                        foreach (Community community in communities)
                        {
                            List<AnalysisPoint> cluster = new List<AnalysisPoint>();
                            foreach (Vertex vertex in community.Vertices)
                            {
                                int m = 0;
                                while (m < tmpNodes.Count)
                                {

                                    if (tmpNodes[m].NodeXLVertexID == vertex.ID)
                                    {
                                        tmpNodes[m].Properties.Add(AnalysisMeasure.ClusterVGA, k);
                                        cluster.Add(tmpNodes[m]);
                                        tmpNodes.Remove(tmpNodes[m]);
                                        m--;
                                    }
                                    m++;
                                }
                            }
                            k++;
                            clusterFirstProcess.Add(cluster);
                        }
                    }

                }
            }

            ////NEUMANN GRAPH..............................................
            if (m_calcNeumannGraph)
            {
                graph.Edges.Clear();
                foreach (APEdge edge in m_neumannEdges)
                {

                    IVertex vertex1, vertex2;
                    graph.Vertices.Find(edge.Node1.NodeXLVertexID, out vertex1);
                    graph.Vertices.Find(edge.Node2.NodeXLVertexID, out vertex2);
                    if (vertex1 != null && vertex2 != null)
                        graph.Edges.Add(vertex1, vertex2, false);
                }
                Dictionary<Int32, BrandesVertexCentralities> metricsCentralityNeumann = new Dictionary<int, BrandesVertexCentralities>();
                centralityCalculator.TryCalculateGraphMetrics(graph, null, out metricsCentralityNeumann);


                foreach (AnalysisPoint ap in m_analysisPoints)
                {
                    ap.Properties.Add(AnalysisMeasure.ClosenessNeumann, (float)metricsCentralityNeumann[ap.NodeXLVertexID].ClosenessCentrality);
                    ap.Properties.Add(AnalysisMeasure.BetweennessNeumann, (float)metricsCentralityNeumann[ap.NodeXLVertexID].BetweennessCentrality);
                }
                if (m_calcCluster)
                {
                    communities = null;
                    try { clusterCalculator.TryCalculateGraphMetrics(graph, null, out communities); }
                    catch (Exception ex) { }

                    tmpNodes = new List<AnalysisPoint>();
                    foreach (AnalysisPoint node in m_analysisPoints)
                        tmpNodes.Add(node);

                    while (tmpNodes.Count != 0)
                    //foreach (AnalysisPoint curNode in m_analysisPoints)
                    {
                        AnalysisPoint curNode = tmpNodes[0];

                        int k = 0;
                        foreach (Community community in communities)
                        {
                            foreach (Vertex vertex in community.Vertices)
                            {
                                if (curNode.NodeXLVertexID == vertex.ID)
                                {
                                    curNode.Properties.Add(AnalysisMeasure.ClusterNeumann, k);
                                    break;
                                }
                            }
                            k++;
                        }
                        tmpNodes.Remove(curNode);
                    }
                }
            }

            if (m_calcMooreGraph)
            {
                if (!m_calcNeumannGraph)
                {
                    graph.Edges.Clear();
                    foreach (APEdge edge in m_neumannEdges)
                    {
                        IVertex vertex1, vertex2;
                        graph.Vertices.Find(edge.Node1.NodeXLVertexID, out vertex1);
                        graph.Vertices.Find(edge.Node2.NodeXLVertexID, out vertex2);
                        if (vertex1 != null && vertex2 != null)
                            graph.Edges.Add(vertex1, vertex2, false);
                    }
                }
                //MOORE GRAPH..............................................
                foreach (APEdge edge in m_mooreEdges)
                {
                    IVertex vertex1, vertex2;
                    graph.Vertices.Find(edge.Node1.NodeXLVertexID, out vertex1);
                    graph.Vertices.Find(edge.Node2.NodeXLVertexID, out vertex2);
                    if (vertex1 != null && vertex2 != null)
                        graph.Edges.Add(vertex1, vertex2, false);
                }

                Dictionary<Int32, BrandesVertexCentralities> metricsCentralityMoore = new Dictionary<int, BrandesVertexCentralities>();
                centralityCalculator.TryCalculateGraphMetrics(graph, null, out metricsCentralityMoore);

                foreach (AnalysisPoint ap in m_analysisPoints)
                {
                    ap.Properties.Add(AnalysisMeasure.ClosenessMoore, (float)metricsCentralityMoore[ap.NodeXLVertexID].ClosenessCentrality);
                    ap.Properties.Add(AnalysisMeasure.BetweennessMoore, (float)metricsCentralityMoore[ap.NodeXLVertexID].BetweennessCentrality);
                }
                if (m_calcCluster)
                {
                    communities = null;
                    try { clusterCalculator.TryCalculateGraphMetrics(graph, null, out communities); }
                    catch (Exception ex) { }

                    tmpNodes = new List<AnalysisPoint>();
                    foreach (AnalysisPoint node in m_analysisPoints)
                        tmpNodes.Add(node);

                    while (tmpNodes.Count != 0)
                    //foreach (AnalysisPoint curNode in m_analysisPoints)
                    {
                        AnalysisPoint curNode = tmpNodes[0];

                        int k = 0;
                        foreach (Community community in communities)
                        {
                            foreach (Vertex vertex in community.Vertices)
                            {
                                if (curNode.NodeXLVertexID == vertex.ID)
                                {
                                    curNode.Properties.Add(AnalysisMeasure.ClusterMoore, k);
                                    break;
                                }
                            }
                            k++;
                        }
                        tmpNodes.Remove(curNode);
                    }
                }
            }
            //FW_GPU_NET.Graph fwInverseGraphAngular = null;
            //FW_GPU_NET.Graph fwGraph;
            //float[] radi = new float[] { float.MaxValue };

            ////------------------------------------------------------------------------------//
            //fwGraph = GraphTools.GenerateFWGpuGraph(_network);
            //fwInverseGraphAngular = GraphTools.GenerateInverseDoubleGraph(fwGraph); // the costs/wightings are calculated here...

            //inverseGraph.Compute(FW_GPU_NET.Graph.ComputeType.CPU_DIJKSTRA_BASIC);
            //fwGraph.ComputeEdgeUsedCountsWithChilds(parentNodesPerRadius[0].ToArray(), true);


            //_shortPahtesAngular = new ShortestPath(_network);
            //_shortPahtesAngular.Evaluate(new System.Windows.Forms.ToolStripProgressBar(), fwGraph, fwInverseGraphAngular, radi);
            //fwInverseGraphAngular.Dispose();
            //fwInverseGraphAngular = null;

        }

        //List<List<AnalysisPoint>> shortestPaths = new List<List<AnalysisPoint>>();
        public void ShortestMetricDistanceFromOneToAll(AnalysisPoint origin)
        {
            UndirectedGraph<AnalysisPoint, UndirectedEdge<AnalysisPoint>> graph = new UndirectedGraph<AnalysisPoint, UndirectedEdge<AnalysisPoint>>();

            for (int n = 0; n < m_neumannEdges.Count(); n++)
            {
                UndirectedEdge<AnalysisPoint> newEdge = new UndirectedEdge<AnalysisPoint>(m_neumannEdges[n].Node1, m_neumannEdges[n].Node2);
                graph.AddVerticesAndEdge(newEdge);
            }
            Func<UndirectedEdge<AnalysisPoint>, double> edgeCost = e => m_cellSize; //1; // constant cost
            var dijkstra = new QuickGraph.Algorithms.ShortestPath.UndirectedDijkstraShortestPathAlgorithm<AnalysisPoint, UndirectedEdge<AnalysisPoint>>(graph, edgeCost);
            //var vis = new QuickGraph.Algorithms.Observers.UndirectedVertexPredecessorRecorderObserver<AnalysisPoint, UndirectedEdge<AnalysisPoint>>();

            dijkstra.Compute(origin);

            // -- add the weighting afterwards, since we work in a grid...
            // Todo: generalize - now it works only for von Neumann graph!
            foreach (AnalysisPoint ap in AnalysisPoints)
            {
                double dist = -1;
                dijkstra.TryGetDistance(ap, out dist);
                if (dist > 100)
                    dist = 100;
                ap.Properties[AnalysisMeasure.ShortestMetricDistance] = (float)(dist);// * m_cellSize);  // rk: to edgeCost!              
            }
        }

        public void ShortestVisualDistanceFromOneToAll(AnalysisPoint origin)
        {
            UndirectedGraph<AnalysisPoint, UndirectedEdge<AnalysisPoint>> graph = new UndirectedGraph<AnalysisPoint, UndirectedEdge<AnalysisPoint>>();

            for (int n = 0; n < m_visibilityEdges.Count(); n++)
            {
                UndirectedEdge<AnalysisPoint> newEdge = new UndirectedEdge<AnalysisPoint>(m_visibilityEdges[n].Node1, m_visibilityEdges[n].Node2);
                graph.AddVerticesAndEdge(newEdge);
            }
            Func<UndirectedEdge<AnalysisPoint>, double> edgeCost = e => 1; // constant cost
            var dijkstra = new QuickGraph.Algorithms.ShortestPath.UndirectedDijkstraShortestPathAlgorithm<AnalysisPoint, UndirectedEdge<AnalysisPoint>>(graph, edgeCost);
            var vis = new QuickGraph.Algorithms.Observers.UndirectedVertexPredecessorRecorderObserver<AnalysisPoint, UndirectedEdge<AnalysisPoint>>();

            dijkstra.Compute(origin);

            foreach (AnalysisPoint ap in AnalysisPoints)
            {
                double dist = -1;
                dijkstra.TryGetDistance(ap, out dist);
                if (dist > 100)
                    dist = 100;
                ap.Properties[AnalysisMeasure.ShortestVisualDistance] = (float)dist;
            }
        }
        
        public List<double> ShortestPath(AnalysisPoint origin, List<AnalysisPoint> targets)
        {
            UndirectedGraph<AnalysisPoint, UndirectedEdge<AnalysisPoint>> graph = new UndirectedGraph<AnalysisPoint, UndirectedEdge<AnalysisPoint>>();

            for (int n = 0; n < m_mooreEdges.Count(); n++)
            {
                UndirectedEdge<AnalysisPoint> newEdge = new UndirectedEdge<AnalysisPoint>(m_mooreEdges[n].Node1, m_mooreEdges[n].Node2);
                graph.AddVerticesAndEdge(newEdge);
            }
            Func<UndirectedEdge<AnalysisPoint>, double> edgeCost = e => 1; // constant cost
            var dijkstra = new QuickGraph.Algorithms.ShortestPath.UndirectedDijkstraShortestPathAlgorithm<AnalysisPoint, UndirectedEdge<AnalysisPoint>>(graph, edgeCost);
            //var vis = new QuickGraph.Algorithms.Observers.UndirectedVertexPredecessorRecorderObserver<AnalysisPoint, UndirectedEdge<AnalysisPoint>>();

            dijkstra.Compute(origin);

            List<double> distances = new List<double>();
            foreach (AnalysisPoint ap in targets)
            {
                double dist = -1;
                dijkstra.TryGetDistance(ap, out dist);
                if (dist > 100)
                    dist = -1;
                distances.Add(dist);
            }
            return distances;
        }
        
        private void CreateGraph(List<Line2D> lines)
        {
            List<AnalysisPoint> tmpNodes = new List<AnalysisPoint>();
            foreach (AnalysisPoint node in m_analysisPoints)
            {
                node.VisualNeighbors = new HashSet<AnalysisPoint>();
                tmpNodes.Add(node);
            }

            m_visibilityEdges = new List<APEdge>();
            m_neumannEdges = new List<APEdge>();
            m_mooreEdges = new List<APEdge>();
            double eps = 0.0001;
            double testRadiusForMooreOrNeumann = Math.Sqrt(2*m_cellSize*m_cellSize) + eps;

            while (tmpNodes.Count != 0)
            {
                AnalysisPoint curNode = tmpNodes[0];

                for (int i = 1; i < tmpNodes.Count; i++)
                {
                    bool isVisible = true;
                    foreach (Line2D line in lines)
                        if (GeoAlgorithms2D.IntersectionTest((float)line.Start.X, (float)line.Start.Y, (float)line.End.X, (float)line.End.Y,
                            (float)curNode.Position.X, (float)curNode.Position.Y, (float)tmpNodes[i].Position.X, (float)tmpNodes[i].Position.Y))
                        {
                            isVisible = false;
                            break;
                        }
                    if (isVisible)
                    {
                        curNode.VisualNeighbors.Add(tmpNodes[i]);
                        tmpNodes[i].VisualNeighbors.Add(curNode);
                        m_visibilityEdges.Add(new APEdge(curNode, tmpNodes[i]));
                  
                        // Todo: replace m_cellSize
                        if (tmpNodes[i].Position.Distance(curNode.Position) <= testRadiusForMooreOrNeumann)
                        {
                            curNode.MooreNeighbors.Add(tmpNodes[i]);
                            tmpNodes[i].MooreNeighbors.Add(curNode);
                            m_mooreEdges.Add(new APEdge(curNode, tmpNodes[i]));

                            // Todo: replace m_cellSize
                            if (tmpNodes[i].Position.Distance(curNode.Position) <= m_cellSize + eps)
                            {
                                curNode.NeumannNeighbors.Add(tmpNodes[i]);
                                tmpNodes[i].NeumannNeighbors.Add(curNode);
                                m_neumannEdges.Add(new APEdge(curNode, tmpNodes[i]));
                            }
                        }

                    }
                }
                tmpNodes.Remove(curNode);
            }

        }

        #endregion


        
        #endregion


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region AdditionalClasses

        [Serializable]
        public class APEdge
        {
            AnalysisPoint m_node1;
            AnalysisPoint m_node2;

            public AnalysisPoint Node1
            {
                get { return m_node1; }
                set { m_node1 = value; }
            }
            public AnalysisPoint Node2
            {
                get { return m_node2; }
                set { m_node2 = value; }
            }

            public APEdge(AnalysisPoint node1, AnalysisPoint node2)
            {
                m_node1 = node1;
                m_node2 = node2;
            }
        }
        

        #endregion
    }
}

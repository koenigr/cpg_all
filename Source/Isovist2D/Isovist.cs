﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Isovist.cs 
//  Copyright (C) 2014/10/18  12:59 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Sven Schneider, Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using OpenTK;
using CPlan.Geometry;

namespace CPlan.Isovist2D
{
    [Serializable]
    public class Isovist2D
    {

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Attributes

        double m_viewingAngle = 1;  //0..1  0=0° 1=360°
        double m_visibilityRange = 100;
        int m_calculationMethod = 0; //0=Rayintersection, 1=Analytically
        double m_precision = 0.01;
        
        Vector2d m_vantagePoint;
        Vector2d m_viewingDirection;
        List<Line2D> m_obstacleLines = new List<Line2D>();
        List<bool> m_obstacleLinesHit = new List<bool>();
        List<double> m_obstacleLinesHitAngle = new List<double>();

        List<Vector2d> m_perimeterPoints = new List<Vector2d>();
        List<Vector2d> m_allPerimeterPoints = new List<Vector2d>();
        List<Line2D> m_surfacePerimeterLines = new List<Line2D>();
        List<Line2D> m_occludingPerimeterLines = new List<Line2D>();

        //Isovist Measures:
        double m_perimeter;             // Vx
        double m_area;                  // Ax
        double m_areaDistWeigth;
        double m_compactness;
        double m_circularity;           // Nx = Vx² / (4*Pi*Ax)
        double m_convexDeficiency;
        double m_realSurfacePerimeter;  // Px = |Sx|
        double m_occlusivity;           // Qx = |Rx|       alle Umrisslinien des Isovists, die sich nicht mit Wänden überlagern
        int m_occlusionCount;
        double m_variance;              // M2,x = M2(lx,o)
        double m_standardDeviation;     // Standardabweichung der Länge der Strahlen
        double m_skewness;              // M3,x = M3(lx,o)
        double m_minRadialLength;       // shortest Radial
        double m_maxRadialLength;       // longest Radial
        double m_meanRadialLength;      // MRL
        double m_angularity;
        double m_driftMagnitude;
        double m_driftAngle;

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        public double Area
        { get { return m_area; } }

        public double AreaDistWeight
        { get { return m_area; } }

        public double Occlusivity
        { get { return m_occlusivity; } }

        public int NrOfOccludingEdges
        { get { return m_occlusionCount; } }

        public double Compactness
        { get { return m_compactness; } }

        public double Angularity
        { get { return m_angularity; } }

        public double Circularity
        { get { return m_circularity; } }
        public double ConvexDeficiency
        { get { return m_convexDeficiency; } }
        
        public double Perimeter
        { get { return m_perimeter; } }

        public double MinRadialLength
        { get { return m_minRadialLength; } }

        public double MaxRadialLength
        { get { return m_maxRadialLength; } }

        public double MeanRadialLength
        { get { return m_meanRadialLength; } }

        public double StandardDeviation
        { get { return m_standardDeviation; } }

        public double Variance
        { get { return m_variance; } }

        public double Skewness
        { get { return m_skewness; } }

        public double Elongation
        { get { return m_meanRadialLength/m_maxRadialLength; } }

        public double Dispersion
        { get { return m_meanRadialLength-m_standardDeviation; } }

        public double DriftMagnitude
        { get { return m_driftMagnitude; } }

        public double DriftAngle
        { get { return m_driftAngle; } }


        public Vector2d VantagePoint
        {
            get { return m_vantagePoint; }
            set { m_vantagePoint = value; }
        }

        public Vector2d ViewingDirection
        {
            get { return m_viewingDirection; }
            set { m_viewingDirection = value; }
        }

        public double ViewingAngle
        {
            get { return m_viewingAngle; }
            set { m_viewingAngle = value; }
        }

        public double ViewingRange
        {
            get { return m_visibilityRange; }
            set { m_visibilityRange = value; }
        }

        public double Precision
        {
            get { return m_precision; }
            set { m_precision = value; }
        }

        public int CalculationMethod
        {
            get { return m_calculationMethod; }
            set { m_calculationMethod = value; }
        }

        public List<Line2D> ObstacleLines
        {
            get { return m_obstacleLines; }
            set { m_obstacleLines = value; }
        }

        public List<Vector2d> PerimeterPoints
        {
            get { return m_perimeterPoints; }
        }

        public List<Vector2d> AllPerimeterPoints
        {
            get { return m_allPerimeterPoints; }
        }

        public List<Line2D> RealSurfacePerimeterLines
        {
            get { return m_surfacePerimeterLines; }
        }

        public List<Line2D> OccludingPerimeterLines
        {
            get { return m_occludingPerimeterLines; }
        }

        public List<bool> IndexOfHittedObstacleLines
        {
            get { return m_obstacleLinesHit; }
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Constructors


        public Isovist2D(double x, double y, List<Line2D> lines)
        {
            m_vantagePoint = new Vector2d(x, y);
            m_viewingDirection = new Vector2d(0, 1);
            m_viewingDirection.Normalize();
            m_obstacleLines = lines;
        }

        # endregion
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods


        public void Calculate()
        {
            m_perimeter = 0;
            m_occlusivity = 0;
            m_occlusionCount = 0;

            m_obstacleLinesHit.Clear();
            m_obstacleLinesHitAngle.Clear();
            for (int i = 0; i < m_obstacleLines.Count; i++)
            {
                m_obstacleLinesHit.Add(false);
                m_obstacleLinesHitAngle.Add(-1);
            }

            switch (m_calculationMethod)
            {
                case 0:
                    CalculateByRays();
                    break;
                case 1:
                    CalculateAnalytically();
                    break;
                default:
                    CalculateByRays();
                    break;
            }

            CalculateProperties();
        }

        public void Calculate(List<Line2D> lines)
        {
            m_obstacleLines = lines;

            Calculate();
        }

        private void CalculateAnalytically()
        {
            if (m_obstacleLines == null) return;

            Poly2D originalPoly2D = new Poly2D();

            for (int angle = 0; angle < 360; angle += 20)
            {
                originalPoly2D.Add(new Vector2d(m_vantagePoint.X + Math.Cos(angle * Math.PI / 180.0) * m_visibilityRange,
                                    m_vantagePoint.Y + Math.Sin(angle * Math.PI / 180.0) * m_visibilityRange));
            }

            Line2D oldLine = null;

            foreach (Line2D line2D in m_obstacleLines)
            {
                //create shadow polygon
                Poly2D shadowPoly2D = new Poly2D();
                shadowPoly2D.Add(line2D.Start);
                shadowPoly2D.Add(line2D.End);

                //calculate last two points:
                Vector2d dir = new Vector2d(line2D.End.X - m_vantagePoint.X, line2D.End.Y - m_vantagePoint.Y);
                dir.Normalize();
                Vector2d.Multiply(dir, m_visibilityRange * m_visibilityRange * 2);
                Vector2d newPoint = new Vector2d(line2D.End.X + dir.X, line2D.End.Y + dir.Y);
                shadowPoly2D.Add(newPoint);
                dir = new Vector2d(line2D.Start.X - m_vantagePoint.X, line2D.Start.Y - m_vantagePoint.Y);
                dir.Normalize();
                Vector2d.Multiply(dir, m_visibilityRange * m_visibilityRange * 2);
                newPoint = new Vector2d(line2D.Start.X + dir.X, line2D.Start.Y + dir.Y);
                shadowPoly2D.Add(newPoint);

                //substract the shadow polygon from the original polygon
                Poly2D intersectionPoly = GeoAlgorithms2D.SubstractPolys(originalPoly2D, shadowPoly2D);

                if (intersectionPoly != null)
                {
                    //if (intersectionPoly.PointsFirstLoop.Length > 4)
                    {
                        originalPoly2D.ClearPoints();
                        for (int i = 0; i < intersectionPoly.PointsFirstLoop.Length; i++)
                        {
                            originalPoly2D.Add(intersectionPoly.PointsFirstLoop[i]);
                        }
                    }
                }
                else
                {
                    //originalPoly2D.ClearPoints();
                    int i = 5;
                    intersectionPoly = GeoAlgorithms2D.SubstractPolys(originalPoly2D, shadowPoly2D);
                }

                oldLine = line2D;
            }

            m_perimeterPoints.Clear();
            for (int i = 0; i < originalPoly2D.PointsFirstLoop.Length; i++)
            {
                m_perimeterPoints.Add(originalPoly2D.PointsFirstLoop[i]);
            }


        }

        private void CalculateByRays()
        {
            if (m_obstacleLines == null)
                return;

            Line2D rotateLine = new Line2D();
            rotateLine.Start = m_vantagePoint;
            m_perimeterPoints.Clear();

            //Winkel berechnen:
            Vector2d Vec1 = new Vector2d(0, 1);
            Vector2d Vec2 = m_viewingDirection;
            double dirAngle = Vec1.DotProduct(Vec2) / (Vec1.Length * Vec2.Length);
            dirAngle = Math.Acos(dirAngle);
            if (Vec2.X < 0)
                dirAngle = 2 * Math.PI - dirAngle;
            double angle = dirAngle - m_viewingAngle * Math.PI;

            //Falls kein 360° isovist, dann muss der blickpunkt zu den Perimeterpunkten hinzugefügt werden
            if (m_viewingAngle < 1)
                m_perimeterPoints.Add(new Vector2d(m_vantagePoint.X, m_vantagePoint.Y));

            Line2D prevWall = null;
            double tmp_angle = 0;
            int cntRaysCutted = 0;
            
            //Isovistberechnung
            while (angle < dirAngle + m_viewingAngle * Math.PI)
            {
                rotateLine.End = new Vector2d(
                        m_vantagePoint.X + Math.Sin(angle) * m_visibilityRange,
                        m_vantagePoint.Y + Math.Cos(angle) * m_visibilityRange
                    );

                Vector2d cutPoint = new Vector2d();
                Line2D cutWall = null;                
                double mindist = double.MaxValue;
                Vector2d iSectPoint = new Vector2d();

                //MyTrace.Trace(String.Format("My Angle is {0} with tolerance {1}", angle, 2));

                //gehe alle Obstacle Linien durch:
                int i = 0;
                int index = -1;
                foreach (Line2D line2D in m_obstacleLines)
                {                    
                    bool intersects = LineIntersection(line2D, rotateLine, ref iSectPoint);
                    if (intersects)
                    {
                        if (mindist > iSectPoint.Distance(m_vantagePoint))
                        {
                            cutPoint = iSectPoint;
                            cutWall = line2D;
                            mindist = (float)iSectPoint.Distance(m_vantagePoint);
                            index = i;
                        }
                    }
                    i++;
                }

                if (cutWall != null)
                {
                    m_obstacleLinesHit[index] = true;
                        
                    m_perimeterPoints.Add(cutPoint);
                    Vector2d vecWall = cutWall.End - cutWall.Start;
                    Vector2d vecRay = cutPoint - m_vantagePoint;
                    double a = Math.Acos((vecWall.X * vecRay.Y + vecWall.Y * vecRay.Y) / (Math.Sqrt(vecWall.X * vecWall.X + vecWall.Y * vecWall.Y) * Math.Sqrt(vecRay.X * vecRay.X + vecRay.Y * vecRay.Y)));
                    if (a > Math.PI)
                        a = a - Math.PI;
                    if (a > Math.PI / 2)
                        a = a - Math.PI / 2;
                    tmp_angle = tmp_angle + a;
                    cntRaysCutted = cntRaysCutted+1;

                    //calculate occlusivity (get rid of small occlusions, based on low resolution)
                    if ((prevWall != cutWall) && (m_perimeterPoints.Count > 1))
                    {
                        bool wallsTouch = false;
                        if (prevWall != null)
                        {
                            Vector2d iPoint = new Vector2d();
                            bool intersects = LineIntersection(prevWall, cutWall, ref iPoint);
                            if (intersects)
                                if (cutPoint.Distance(iPoint) < 0.01)
                                    wallsTouch = true;

                            // find closest point on line to cutPoint                       
                            if (cutPoint.Distance(cutWall.Start) > cutPoint.Distance(cutWall.End))
                            {
                                if ((cutWall.End.IsSimilarTo(prevWall.Start, 0.01)) || (cutWall.End.IsSimilarTo(prevWall.End, 0.01)))
                                    wallsTouch = true;
                            }
                            else
                                if ((cutWall.Start.IsSimilarTo(prevWall.Start, 0.01)) || (cutWall.Start.IsSimilarTo(prevWall.End, 0.01)))
                                    wallsTouch = true;     
                        }
                        Line2D perimeterLine = new Line2D(cutPoint, m_perimeterPoints[m_perimeterPoints.Count - 2]);

                        if ((!wallsTouch) || (prevWall == null))
                        {
                            double dist = perimeterLine.Length; // cutPoint.Distance(m_perimeterPoints[m_perimeterPoints.Count - 2]);
                            m_occlusivity += dist;
                            m_occlusionCount++;
                            m_occludingPerimeterLines.Add(perimeterLine);
                        }
                        else
                            m_surfacePerimeterLines.Add(perimeterLine);

                    }
                    else
                    {
                        if (m_perimeterPoints.Count > 1)
                        {
                            Line2D perimeterLine = new Line2D(cutPoint, m_perimeterPoints[m_perimeterPoints.Count - 2]);
                            m_surfacePerimeterLines.Add(perimeterLine);
                        }
                    }

                    prevWall = cutWall;
                   
                }
                else //nimm Ränder als polyPunkte
                {
                    m_perimeterPoints.Add(rotateLine.End);

                    if (prevWall != null)
                    {

                        prevWall = null;
                    }
                }

                angle += m_precision;
            }

            foreach (Vector2d v in m_perimeterPoints)
                m_allPerimeterPoints.Add(v);

            EleminatePoints();

            m_angularity = tmp_angle / cntRaysCutted;
        }

        private void EleminatePoints()
        {
            if (m_perimeterPoints.Count == 0)
                return;

            // eleminate points
            Vector2d startP, midP, endP;
            Vector2d dir, dir2;
            startP = m_perimeterPoints[0];

            int j = 0;

            while (j < m_perimeterPoints.Count - 2)
            {
                midP = m_perimeterPoints[j + 1];
                endP = m_perimeterPoints[j + 2];

                dir = new Vector2d(startP.X - midP.X, startP.Y - midP.Y);
                dir.Normalize();
                dir2 = new Vector2d(midP.X - endP.X, midP.Y - endP.Y);
                dir2.Normalize();

                if ((dir.X == dir2.X) && (dir.Y == dir2.Y))
                    m_perimeterPoints.Remove(midP);
                else
                {
                    startP = midP;
                    j++;
                }
            }

            //letzten Punkt prüfen:
            startP = m_perimeterPoints[m_perimeterPoints.Count - 2];
            midP = m_perimeterPoints[m_perimeterPoints.Count - 1];
            endP = m_perimeterPoints[0];
            dir = new Vector2d(startP.X - midP.X, startP.Y - midP.Y);
            dir.Normalize();
            dir2 = new Vector2d(midP.X - endP.X, midP.Y - endP.Y);
            dir2.Normalize();

            if ((dir.X == dir2.X) && (dir.Y == dir2.Y))
                m_perimeterPoints.Remove(midP);
        }

        private bool LineIntersection(Line2D L1, Line2D L2, ref Vector2d intPoint)
        {
            intPoint = new Vector2d();
            // Denominator for ua and ub are the same, so store this calculation
            double d = (L2.End.Y - L2.Start.Y) * (L1.End.X - L1.Start.X) - (L2.End.X - L2.Start.X) * (L1.End.Y - L1.Start.Y);

            //n_a and n_b are calculated as seperate values for readability
            double n_a = (L2.End.X - L2.Start.X) * (L1.Start.Y - L2.Start.Y) - (L2.End.Y - L2.Start.Y) * (L1.Start.X - L2.Start.X);
            double n_b = (L1.End.X - L1.Start.X) * (L1.Start.Y - L2.Start.Y) - (L1.End.Y - L1.Start.Y) * (L1.Start.X - L2.Start.X);

            // Make sure there is not a division by zero - this also indicates that
            // the lines are parallel.  
            // If n_a and n_b were both equal to zero the lines would be on top of each 
            // other (coincidental).  This check is not done because it is not 
            // necessary for this implementation (the parallel check accounts for this).
            if (d == 0)
                return false;

            // Calculate the intermediate fractional point that the lines potentially intersect.
            double ua = n_a / d;
            double ub = n_b / d;

            // The fractional point will be between 0 and 1 inclusive if the lines
            // intersect.  If the fractional calculation is larger than 1 or smaller
            // than 0 the lines would need to be longer to intersect.
            if (ua >= 0d && ua <= 1d && ub >= 0d && ub <= 1d)
            {
                intPoint = new Vector2d (L1.Start.X + (ua * (L1.End.X - L1.Start.X)), L1.Start.Y + (ua * (L1.End.Y - L1.Start.Y)));
                return true;
            }
            return false;
        }

        private void CalculateProperties()
        {
            double eps = 0.0001;

            if (m_perimeterPoints.Count < 2)
                return;
            
            for (int i = 0; i < m_perimeterPoints.Count; i++)
            {
                if (i < m_perimeterPoints.Count - 1)
                {
                    m_perimeter += m_perimeterPoints[i].Distance(m_perimeterPoints[i + 1]);

                    Vector2d dir = new Vector2d(m_perimeterPoints[i].X - m_perimeterPoints[i + 1].X, m_perimeterPoints[i].Y - m_perimeterPoints[i + 1].Y);
                    dir.Normalize();
                    Vector2d dir2 = new Vector2d(m_perimeterPoints[i].X - m_vantagePoint.X, m_perimeterPoints[i].Y - m_vantagePoint.Y);
                    dir2.Normalize();

                    if ((((dir.X < dir2.X + eps) && (dir.X > dir2.X - eps)) && ((dir.Y < dir2.Y + eps) && (dir.Y > dir2.Y - eps)))
                        || (((dir.X < -dir2.X + eps) && (dir.X > -dir2.X - eps)) && ((dir.Y < -dir2.Y + eps) && (dir.Y > -dir2.Y - eps))))
                    {
                        m_occlusivity += m_perimeterPoints[i].Distance(m_perimeterPoints[i + 1]);
                        m_occlusionCount++;
                    }
                }
                else
                {
                    m_perimeter += m_perimeterPoints[i].Distance(m_perimeterPoints[0]);

                    Vector2d dir = new Vector2d(m_perimeterPoints[i].X - m_perimeterPoints[0].X, m_perimeterPoints[i].Y - m_perimeterPoints[0].Y);
                    dir.Normalize();
                    Vector2d dir2 = new Vector2d(m_perimeterPoints[i].X - m_vantagePoint.X, m_perimeterPoints[i].Y - m_vantagePoint.Y);
                    dir2.Normalize();

                    if (m_calculationMethod == 1)
                    {
                        if ((((dir.X < dir2.X + eps) && (dir.X > dir2.X - eps)) && ((dir.Y < dir2.Y + eps) && (dir.Y > dir2.Y - eps)))
                            || (((dir.X < -dir2.X + eps) && (dir.X > -dir2.X - eps)) && ((dir.Y < -dir2.Y + eps) && (dir.Y > -dir2.Y - eps))))
                        {
                            m_occlusivity += m_perimeterPoints[i].Distance(m_perimeterPoints[0]);
                            m_occlusionCount++;
                        }
                    }
                }
            }

            m_minRadialLength = double.MaxValue;
            m_maxRadialLength = double.MinValue;
            double sum = 0;
            foreach (Vector2d v in m_allPerimeterPoints)
            {
                double dist = v.Distance(m_vantagePoint);
                if (dist < m_minRadialLength)
                    m_minRadialLength = dist;
                if (dist > m_maxRadialLength)
                    m_maxRadialLength = dist;

                sum += dist;
            }
            m_meanRadialLength = sum / m_allPerimeterPoints.Count;

            m_standardDeviation = 0;
            m_variance = 0;
            m_skewness = 0;
            foreach (Vector2d v in m_allPerimeterPoints)
            {
                double dist = v.Distance(m_vantagePoint);
                m_standardDeviation = m_standardDeviation + Math.Abs(dist - m_meanRadialLength);
                m_variance = m_variance + Math.Pow(dist - m_meanRadialLength, 2);
                m_skewness = m_skewness + Math.Pow(dist - m_meanRadialLength, 3);
            }

            m_area = CalcArea(m_perimeterPoints);
            m_areaDistWeigth = CalcAreaDistWeighted(m_allPerimeterPoints);
            m_compactness = (4.0 * Math.PI * m_area / (m_perimeter * m_perimeter));
            m_circularity = (Math.PI * Math.Pow(m_meanRadialLength, 2)) / m_area;


            Poly2D poly = new Poly2D(m_perimeterPoints.ToArray());
            m_convexDeficiency = poly.ConvexDeficiency;
            Vector2d pCenter = poly.Center;
            m_driftMagnitude = VantagePoint.Distance(pCenter);
            m_driftAngle = GeoAlgorithms2D.AngleBetween(m_viewingDirection, pCenter - m_vantagePoint, m_vantagePoint);
        }

        private double CrossProd(Vector2d p1, Vector2d p2)
        {
            return (p1.X * p2.Y - p1.Y * p2.X);
        }

        
        private double CalcAreaDistWeighted(List<Vector2d> pointList)
        {
            double Ac = 0;
            Vector2d curPWeighted = new Vector2d(0, 0);
            Vector2d oldPWeighted = new Vector2d(0, 0);
            double angle = 0;
            for (int i = 0; i < pointList.Count; i++)
            {
                int j = i + 1;
                if (i == pointList.Count - 1)
                    j = 0;

                double distWeighted = Math.Sqrt(m_vantagePoint.Distance(pointList[i]));                
                curPWeighted.X = m_vantagePoint.X + Math.Sin(angle) * distWeighted;
                curPWeighted.Y = m_vantagePoint.Y + Math.Cos(angle) * distWeighted;

                angle += m_precision;
                if (i == pointList.Count - 1)
                    angle = 0;
                distWeighted = Math.Sqrt(m_vantagePoint.Distance(pointList[j]));
                oldPWeighted.X = m_vantagePoint.X + Math.Sin(angle) * distWeighted;
                oldPWeighted.Y = m_vantagePoint.Y + Math.Cos(angle) * distWeighted;

                angle += m_precision;
                Ac += CrossProd(curPWeighted, oldPWeighted);
            }
          

            return Math.Abs(Ac * 0.5);
        }


        private double CalcArea(List<Vector2d> pointList)
        {
            int j = pointList.Count - 1;

            //double Ac = pointList[0].CrossProductLength(pointList[j]);
            double Ac = CrossProd(pointList[0], pointList[j]);

            for (int i = 1; i <= j; i++)
                //Ac += pointList[i].CrossProductLength(pointList[i-1]);
                Ac += CrossProd(pointList[i], pointList[i - 1]);

            return Math.Abs(Ac * 0.5);
        }

        # endregion
        
    }




}

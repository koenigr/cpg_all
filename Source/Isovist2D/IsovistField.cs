﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = IsovistField.cs 
//  Copyright (C) 2014/10/18  12:59 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Sven Schneider, Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using OpenTK;
using CPlan.Geometry;

namespace CPlan.Isovist2D
{
    [Serializable]
    public enum AnalysisMeasure
    {
        Area, AreaDistWeighted, Perimeter, Compactness, Occlusivity, MinRadial, MeanRadial, MaxRadial, 
        StdDevRadials, Variance, Skewness, NumberOfOcclusions, MeanOcclusivity, RelativeOcclusivity, Circularity, Dispersion, Elongation,
        CompactnessBatty, ExteriorView, Daylight,

        ShortestMetricDistance, ShortestVisualDistance,

        ClosenessVGA, BetweennessVGA, ClosenessNeumann, BetweennessNeumann, ClosenessMoore, BetweennessMoore,
        ClusteringCoefficient, ClusterVGA, ClusterVGA2, ClusterMoore, ClusterNeumann, StableNeighborhoodLength,

        Region, RegionTransparency, SeperatedRegions, InsideBoundary
    };

    [Serializable]
    public class IsovistField2D
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        # region Attributes

        private List<Line2D> m_obstacleLines = new List<Line2D>();
        private List<Line2D> m_exteriorWalls = new List<Line2D>();

        private bool m_isGPUinitialized = false;
        private bool m_useGPU = true;
        private float m_precision = 0.02f;
        private float m_viewRange = 100;

        private List<AnalysisPoint> m_isovists;

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        # region Properties

        //[CategoryAttribute("Settings"), DescriptionAttribute("Precision of Isovist Calculation")]
        public float Precision
        {
            get { return m_precision; }
            set { m_precision = value; }
        }

        public List<Line2D> ObstacleLines
        {
            get { return m_obstacleLines; }
            set { m_obstacleLines = value; }
        }

        public List<Dictionary<AnalysisMeasure, float>> Results
        {
            get
            {
                List<Dictionary<AnalysisMeasure, float>> resultList = new List<Dictionary<AnalysisMeasure, float>>();
                foreach (AnalysisPoint ap in m_isovists)
                {
                    //Pavol Bielik: Handle case when calculate was not called, but we tried to retrieve results
                    if (ap.Results.Count == 0)
                    {
                        continue;
                    }

                    resultList.Add(ap.Results);
                }
                return resultList;
            }
            set { }
        }

        public List<AnalysisPoint> AnalysisPoints
        {
            get
            {
                return m_isovists;
            }
        }

        public List<Vector2d> Points
        {
            get
            {
                List<Vector2d> points = new List<Vector2d>();
                foreach (AnalysisPoint point in m_isovists)
                {
                    points.Add(new Vector2d(point.X, point.Y));
                }
                return points;
            }
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        # region Constructors

        public IsovistField2D(List<Vector2d> analysisPoints, List<Line2D> _obstacleLines, float precision = 0.02f)
        {
            m_precision = precision;
            m_isovists = new List<AnalysisPoint>();
            m_obstacleLines = _obstacleLines;
            for (int i = 0; i < analysisPoints.Count; i++)
            {
                m_isovists.Add(new AnalysisPoint(analysisPoints[i]));
            }
        }

        public IsovistField2D(List<Vector2d> analysisPoints, List<Line2D> _obstacleLines, List<Line2D> _exteriorWalls, float precision = 0.02f)
        {
            m_precision = precision;
            m_isovists = new List<AnalysisPoint>();
            m_obstacleLines = _obstacleLines;
            m_exteriorWalls = _exteriorWalls;
            for (int i = 0; i < analysisPoints.Count; i++)
            {
                m_isovists.Add(new AnalysisPoint(analysisPoints[i]));
            }
        }

        public IsovistField2D(IsovistField2D previousField)
        {
            m_isovists = new List<AnalysisPoint>(previousField.m_isovists);
            m_obstacleLines = previousField.m_obstacleLines;
            m_exteriorWalls = previousField.m_exteriorWalls;
            m_precision = previousField.m_precision;

            Results = new List<Dictionary<AnalysisMeasure, float>>(previousField.Results);
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        # region Methods

        public void Calculate(float viewRange, float precision = 0.02f, bool useGPU = true)
        {
            m_precision = precision;
            m_viewRange = viewRange;

            if (useGPU)
                CalculateIsovistFieldGPU();
            else
                CalculateIsovistFieldCPU();
        }

        private void CalculateIsovistFieldCPU()
        {
            //Isovist2D isovist = new Isovist2D(StartP.X, StartP.Y, model, vModel.Ref);
        }

        private void CalculateIsovistFieldGPU()
        {
            float[] wallP1X = new float[m_obstacleLines.Count];
            float[] wallP1Y = new float[m_obstacleLines.Count];
            float[] wallP2X = new float[m_obstacleLines.Count];
            float[] wallP2Y = new float[m_obstacleLines.Count];
            float[] pX = new float[m_isovists.Count];
            float[] pY = new float[m_isovists.Count];

            List<Vector2d> exteriorWallPoints = new List<Vector2d>();
            double threshold = 0.5;
            int k = 0;
            float[] extWallPX = new float[0];
            float[] extWallPY = new float[0];
            if (m_exteriorWalls != null)
            {
                foreach (Line2D extWall in m_exteriorWalls)
                {
                    int n = (int)(extWall.Length / threshold);
                    exteriorWallPoints.AddRange(extWall.Divide(n));
                }

                extWallPX = new float[exteriorWallPoints.Count];
                extWallPY = new float[exteriorWallPoints.Count];
                k = 0;
                foreach (Vector2d extWP in exteriorWallPoints)
                {
                    extWallPX[k] = (float)extWP.X;
                    extWallPY[k] = (float)extWP.Y;
                    k++;
                }
            }

            k = 0;
            foreach (Line2D tmpLineSLink in m_obstacleLines)
            {
                Line2D tmpLine = tmpLineSLink;
                wallP1X[k] = (float) tmpLine.Start.X;
                wallP1Y[k] = (float) tmpLine.Start.Y;
                wallP2X[k] = (float) tmpLine.End.X;
                wallP2Y[k] = (float) tmpLine.End.Y;
                k++;
            }

            k = 0;
            foreach (AnalysisPoint point in m_isovists)
            {
                pX[k] = (float) point.X;
                pY[k] = (float) point.Y;
                k++;
            }

            int test = 1;
            GPUIsovistField gpu = GPUIsovistField.GetInstance();
            gpu.Create(wallP1X, wallP1Y, wallP2X, wallP2Y, extWallPX, extWallPY, pX, pY, m_precision, m_viewRange);
            float[,] results = gpu.Run();

            for (int i = 0; i < results.GetLength(0); i++)
            {
                Dictionary<AnalysisMeasure, float> newDict = new Dictionary<AnalysisMeasure, float>();

                newDict.Add(AnalysisMeasure.Area, results[i, 0]);
                newDict.Add(AnalysisMeasure.AreaDistWeighted, results[i, 6]);
                newDict.Add(AnalysisMeasure.Perimeter, results[i, 1]);
                newDict.Add(AnalysisMeasure.Compactness, results[i, 2]);
                newDict.Add(AnalysisMeasure.CompactnessBatty, (float)(Math.Sqrt(results[i, 0] / Math.PI) / (results[i, 1] / 2 * Math.PI))); //Batty laut Dalton, 2001
                newDict.Add(AnalysisMeasure.Circularity, (float)((Math.PI * Math.Pow(results[i, 7], 2)) / results[i, 0])); //laut Benedikt, 1979: Math.Pow(results[i, 1], 2) / (Math.PI * 4*results[i, 0])) //laut Dalton, 2001: (Math.PI*Math.Pow(results[i, 7], 2)) / results[i, 0]
                
                newDict.Add(AnalysisMeasure.Occlusivity, results[i, 3]);
                newDict.Add(AnalysisMeasure.MeanOcclusivity, results[i, 3] / results[i, 11]);
                newDict.Add(AnalysisMeasure.RelativeOcclusivity, results[i, 3] / results[i, 1]);
                newDict.Add(AnalysisMeasure.NumberOfOcclusions, results[i, 11]);

                newDict.Add(AnalysisMeasure.MinRadial, results[i, 4]);
                newDict.Add(AnalysisMeasure.MaxRadial, results[i, 5]);
                newDict.Add(AnalysisMeasure.MeanRadial, results[i, 7]);
                newDict.Add(AnalysisMeasure.StdDevRadials, results[i, 8]);
                newDict.Add(AnalysisMeasure.Variance, results[i, 9]);
                newDict.Add(AnalysisMeasure.Skewness, results[i, 10]);
                newDict.Add(AnalysisMeasure.Dispersion, results[i, 7] - results[i, 8]); //laut Dalton, 2001
                newDict.Add(AnalysisMeasure.Elongation, results[i, 7] / results[i, 5]); //laut Dalton, 2001

                newDict.Add(AnalysisMeasure.ExteriorView, results[i, 12]);
                newDict.Add(AnalysisMeasure.Daylight, results[i, 13]);
                
                m_isovists[i].Results = newDict;
            }

            # endregion
        }
    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = IsovistResultsWrapper.cs 
//  Copyright (C) 2014/10/18  12:59 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Sven Schneider, Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion
/*
using System.Collections.Generic;

namespace CPlan.Isovist2D
{
    public class IsovistResultsWrapper
    {
        private float m_precision;
        private List<Dictionary<AnalysisMeasure, float>> m_results;

        [Newtonsoft.Json.JsonConstructor]
        public IsovistResultsWrapper(List<Dictionary<AnalysisMeasure, float>> results, float precision)
        {
            m_precision = precision;
            m_results = results;
        }

        public List<Dictionary<AnalysisMeasure, float>> Results
        {
            get { return m_results; }
            set { m_results = value; }
        }

        public float Precision
        {
            get { return m_precision; }
            set { m_precision = value; }
        }
    }

}
*/

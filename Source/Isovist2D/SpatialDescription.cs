﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using System.Security.Cryptography;
using Smrf.NodeXL.Core;
using Smrf.NodeXL.Algorithms;
using System.ComponentModel;

namespace CPlan.Isovist2D
{
    [Serializable]
    public class SpatialDescription
    {
        //////////////////////////////////////////////////////////////////////////////
        # region Attributes

        VisibilityGraphAnalysis m_spatialDiscretizer;
        List<Space> m_spaces = new List<Space>();
        List<Space> m_fixedSpaces = new List<Space>(); 
        List<PerformanceMembership> m_performanceRequirements = new List<PerformanceMembership>();
        List<Relationship> m_relationshipRequirements = new List<Relationship>();
        double m_allowedOverlap = 0.05;
        double m_minPointPerformance = 0.09;
        double m_minOSpacePerformance = 0.09;
        double m_allowedOverlapForSearchingPointCluster = 0.5;

        //just for calculation
        [NonSerialized]
        List<KeyValuePair<Dictionary<Space, AssignedSpace>, double[]>> setsOfValidCombinations = new List<KeyValuePair<Dictionary<Space, AssignedSpace>, double[]>>();
        [NonSerialized]
        List<KeyValuePair<Dictionary<Space, AssignedSpace>, double[]>> setsOfInvalidCombinations = new List<KeyValuePair<Dictionary<Space, AssignedSpace>, double[]>>();

        bool m_divideByCommunity = true;

        #endregion

        //////////////////////////////////////////////////////////////////////////////
        #region Properties 

        public Dictionary<Space, AssignedSpace> BestCombination;

        public double MinPointPerformance
        {
            get { return m_minPointPerformance; }
            set { m_minPointPerformance = value; }
        }

        public double AllowedOverlap
        {
            get { return m_allowedOverlap; }
            set { m_allowedOverlap = value; }
        }
        public double PropertiesPerformance = -1;

        public double RelationshipPerformance = -1;

        public double MinPropertiesPerformance = -1;

        public double MinRelationshipPerformance = -1;

        public double AmountOfFulfilledRelations = -1;

        public double Performance
        {
            get
            {
                double summedPerf = Math.Sqrt(
                        PropertiesPerformance * PropertiesPerformance +
                        RelationshipPerformance * RelationshipPerformance +
                        MinPropertiesPerformance * MinPropertiesPerformance  +
                        //MinRelationshipPerformance * MinRelationshipPerformance +
                        AmountOfFulfilledRelations * AmountOfFulfilledRelations
                        );

                return summedPerf; //Math.Sqrt(PropertiesPerformance * PropertiesPerformance + RelationshipPerformance*RelationshipPerformance); 
            }
        }

        public double Performance2
        {
            get
            {
                double summedPerf = Math.Sqrt(
                        //PropertiesPerformance * PropertiesPerformance +
                        RelationshipPerformance * RelationshipPerformance +
                        MinPropertiesPerformance * MinPropertiesPerformance
                    //MinRelationshipPerformance * MinRelationshipPerformance +
                        //AmountOfFulfilledRelations * AmountOfFulfilledRelations
                        );

                return summedPerf; //Math.Sqrt(PropertiesPerformance * PropertiesPerformance + RelationshipPerformance*RelationshipPerformance); 
            }
        }

        public List<PerformanceMembership> PerformanceRequirements
        {
            get { return m_performanceRequirements; }
            set { m_performanceRequirements = value; }
        }

        public List<Relationship> RelationshipRequirements
        {
            get { return m_relationshipRequirements; }
            set { m_relationshipRequirements = value; }
        }

        public List<Space> RequiredSpaces
        {
            get { return m_spaces; }
            set { m_spaces = value; }
        }

        public List<Space> FixedSpaces
        {
            get { return m_fixedSpaces; }
            set { m_fixedSpaces = value; }
        }

        public double MaxPropertiesPerformance
        {
            get
            {
                double sumPerformances = 0;

                foreach (Space s in RequiredSpaces)
                    sumPerformances += s.PerformanceRequirements.Count;

                sumPerformances += m_performanceRequirements.Count;

                sumPerformances = RequiredSpaces.Count;
                if (m_performanceRequirements.Count > 0)
                    sumPerformances += 1;


                return sumPerformances;
            }
        }

        public double MaxRelationshipPerformance
        {
            get
            {
                double sumPerformances = 0;

                sumPerformances += m_relationshipRequirements.Count;

                return sumPerformances;
            }
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////
        #region Constructors
        
        public SpatialDescription(VisibilityGraphAnalysis _spatialDiscretizer)
        {
            m_spatialDiscretizer = _spatialDiscretizer;
        }

        public SpatialDescription(SpatialDescription old)
        {
            m_spatialDiscretizer = old.m_spatialDiscretizer;
            m_spaces = new List<Space>(old.m_spaces);
            if (old.BestCombination != null)
                BestCombination = new Dictionary<Space, AssignedSpace>(old.BestCombination);
            PropertiesPerformance = old.PropertiesPerformance;
            RelationshipPerformance = old.RelationshipPerformance;
            MinPropertiesPerformance = old.MinPropertiesPerformance;
            MinRelationshipPerformance = old.MinRelationshipPerformance;
            AmountOfFulfilledRelations = old.AmountOfFulfilledRelations;


            if (old.m_performanceRequirements != null) 
                m_performanceRequirements = new List<PerformanceMembership>(old.m_performanceRequirements);
            if (old.m_relationshipRequirements != null) 
                m_relationshipRequirements = new List<Relationship>(old.m_relationshipRequirements);

        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////
        #region Methods

        public double GetValueFromConvexSpace(PerformanceMembership perfFunction, VisibilityGraphAnalysis.ConvexSpace region)
        {
            switch (perfFunction.Name)
            {
                case "Floor Area":
                    return region.Poly.Area;
                case "Compactness":
                    return region.Poly.Compactness;
                case "Minimal Width":
                    return region.Poly.MinDistFromCenter * 2;
                case "Width/Length":
                    return region.Poly.WidthLengthRatio;
                case "Enclosure":
                    return region.Enclosure;
                case "Max Open Border":
                    return region.MaxOpenBoundaryValue;
                case "Open Edges Length":
                    return region.OpenEdgesLength;
                case "Visual Integration":
                    return region.AverageAnalysisPointMeasures(AnalysisMeasure.ClosenessVGA);
                case "Isovist Area":
                    return region.AverageAnalysisPointMeasures(AnalysisMeasure.Area);
                case "Isovist Compactness":
                    return region.AverageAnalysisPointMeasures(AnalysisMeasure.Compactness);
                case "Isovist Occlusivity":
                    return region.AverageAnalysisPointMeasures(AnalysisMeasure.Occlusivity);
                case "Isovist Relative Occlusivity":
                    return region.AverageAnalysisPointMeasures(AnalysisMeasure.RelativeOcclusivity);
                case "Daylight":
                    return region.AverageAnalysisPointMeasures(AnalysisMeasure.Daylight);
                case "Inside Boundary":
                    int cnt = 0;
                    foreach (VisibilityGraphAnalysis.AnalysisPoint ap in region.AnalysisPoints)
                    {
                        if (ap.Properties[AnalysisMeasure.InsideBoundary] <= 0.5)
                            cnt++;
                    }
                    return (double)cnt / (double)region.AnalysisPoints.Count;
                default:
                    return -1;
            }
        }

        public double GetValueFromPoint(PerformanceMembership perfFunction, VisibilityGraphAnalysis.AnalysisPoint ap)
        {
            switch (perfFunction.Name)
            {
                case "Visual Integration":
                    return ap.Properties[AnalysisMeasure.ClosenessVGA];
                case "Isovist Area":
                    return ap.Properties[AnalysisMeasure.Area];
                case "Isovist Compactness":
                    return ap.Properties[AnalysisMeasure.Compactness];
                case "Isovist Occlusivity":
                    return ap.Properties[AnalysisMeasure.Occlusivity];
                case "Isovist Relative Occlusivity":
                    return ap.Properties[AnalysisMeasure.RelativeOcclusivity];
                case "Isovist MinRadial":
                    return ap.Properties[AnalysisMeasure.MinRadial];
                case "Daylight":
                    return ap.Properties[AnalysisMeasure.Daylight];
                case "Inside Boundary":
                    return ap.Properties[AnalysisMeasure.InsideBoundary];
                default:
                    return -1;
            }
        }

        public double GetPerformances(Space space, VisibilityGraphAnalysis.ConvexSpace region, int type)
        {
            List<double> performances = new List<double>();
            double minPerf = double.MaxValue;
            double maxPerf = double.MinValue;
            foreach (PerformanceMembership perfFunction in space.PerformanceRequirements)
            {
                performances.Add(perfFunction.GetMembershipDegree(GetValueFromConvexSpace(perfFunction, region)));
            }
            double sumPerf = 0;
            foreach (double p in performances)
            {
                sumPerf += p;

                if (p < minPerf)
                    minPerf = p;
                if (p > maxPerf)
                    maxPerf = p;
            }

            if (type == 0)
                return sumPerf / space.PerformanceRequirements.Count;
            else if (type == 1)
                return minPerf;
            else
                return maxPerf;

        }
        
        public void InitializeRequirementScenarioUrban(int variant)
        {
            RequiredSpaces.Clear();
            FixedSpaces.Clear(); 
            double avgIntegration = m_spatialDiscretizer.GetAverage(AnalysisMeasure.ClosenessVGA);
            double avgArea = m_spatialDiscretizer.GetAverage(AnalysisMeasure.Area);
            double avgCompactness = m_spatialDiscretizer.GetAverage(AnalysisMeasure.Compactness);

            m_divideByCommunity = true;

            switch (variant)
            {
                case 0:
                    Space s1 = new Space("Main Square 1", 0, this);
                    PerformanceMembership req = new PerformanceMembership("Floor Area", 3000, 4000, 6000, 10000);
                    s1.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Visual Integration", avgIntegration, avgIntegration*1.5, 1, 1);
                    s1.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Isovist Compactness", avgCompactness/2, avgCompactness*1.5, 1.0, 1.0);
                    s1.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Width/Length", 0.3, 0.4, 1.0, 1.0);
                    s1.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Enclosure", 0.4, 0.7, 1.0, 1.0);
                    s1.PerformanceRequirements.Add(req);        
                    req = new PerformanceMembership("Max Open Border", 0.0, 0.0, 0.4, 0.6);
                    s1.PerformanceRequirements.Add(req);   
                    req = new PerformanceMembership("Inside Boundary", 0.3, 0.5, 0.7, 1.0);
                    s1.PerformanceRequirements.Add(req);


                    Space s2 = new Space("Main Square 2", 0, this);
                    req = new PerformanceMembership("Floor Area", 1500, 2000, 3500, 5000);
                    s2.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Visual Integration", avgIntegration*0.8, avgIntegration*1.5, 1, 1);
                    s2.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Isovist Compactness", avgCompactness/2, avgCompactness*1.5, 1.0, 1.0);
                    s2.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Width/Length", 0.3, 0.4, 1.0, 1.0);
                    s2.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Enclosure", 0.4, 0.7, 1.0, 1.0);
                    s2.PerformanceRequirements.Add(req);                    
                    req = new PerformanceMembership("Max Open Border", 0.0, 0.0, 0.4, 0.6);
                    s2.PerformanceRequirements.Add(req);   
                    req = new PerformanceMembership("Inside Boundary", 0.3, 0.5, 0.7, 1.0);
                    s2.PerformanceRequirements.Add(req);


                    Space s3 = new Space("Playground", 1, this);
                    s3.MinArea = 400;
                    req = new PerformanceMembership("Isovist Area", avgArea/4, avgArea / 3, avgArea / 2, avgArea);
                    s3.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Isovist Compactness", avgCompactness/2, avgCompactness*1.5, 1.0, 1.0);
                    s3.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Visual Integration", 0, 0, avgIntegration/2, avgIntegration);
                    s3.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Inside Boundary", 0, 0, 1, 1.3);
                    s3.PerformanceRequirements.Add(req);

                    Space s4 = new Space("Cafe Place 1", 1, this);
                    s4.MinArea = 300;
                    req = new PerformanceMembership("Isovist MinRadial", 0, 0, 5, 10);
                    s4.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Isovist Area", avgArea*0.8, avgArea, 1000000, 1000000);
                    s4.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Isovist Compactness", avgCompactness / 2, avgCompactness * 1.5, 1.0, 1.0);
                    s4.PerformanceRequirements.Add(req);
                    //req = new PerformanceMembership("Visual Integration", avgIntegration, avgIntegration*1.1, 1, 1);
                    //s4.PerformanceRequirements.Add(req);
                    //req = new PerformanceMembership("Inside Boundary", 0, 0, 1, 1.5);
                    //s4.PerformanceRequirements.Add(req);
                    

                    Space s5 = new Space("Cafe Place 2", 1, this);
                    s5.MinArea = 200;
                    req = new PerformanceMembership("Isovist MinRadial", 0, 0, 5, 10);
                    s5.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Isovist Area", avgArea*0.8, avgArea, 1000000, 1000000);
                    s5.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Isovist Compactness", avgCompactness / 2, avgCompactness, 1.0, 1.0);
                    s5.PerformanceRequirements.Add(req);
                    //req = new PerformanceMembership("Visual Integration", avgIntegration, avgIntegration*1.1, 1, 1);
                    //s5.PerformanceRequirements.Add(req);
                    //req = new PerformanceMembership("Inside Boundary", 0, 0, 1, 1.5);
                    //s5.PerformanceRequirements.Add(req);

                    RequiredSpaces.Add(s1);
                    RequiredSpaces.Add(s2);
                    RequiredSpaces.Add(s3);
                    RequiredSpaces.Add(s4);
                    RequiredSpaces.Add(s5);
                    
                    //PerformanceRequirements.Add(req);

                    //Main Squares
                    Relationship relation;
                    relation = new Relationship(s1, s2, 1, 0, 0, 0.25, 0.33); //Square 1 and 2 shall not be visible
                    RelationshipRequirements.Add(relation);

                    //Backyard intervisibility
                    //relation = new Relationship(s3, s4, 1, 0.1, 0.2, 0.3, 0.4); //Backyards shall be semi-visible
                    //RelationshipRequirements.Add(relation);
                    //relation = new Relationship(s4, s5, 1, 0.1, 0.2, 0.3, 0.4); //Backyards shall be semi-visible
                    //RelationshipRequirements.Add(relation);
                    //relation = new Relationship(s3, s5, 1, 0.0, 0.0, 0.0, 0.3); //Backyards 1 und 3 shall be not visible
                    //RelationshipRequirements.Add(relation);

                    //Main Squares to Backyards
                    //relation = new Relationship(s1, s3, 1, 0.1, 0.2, 0.3, 0.4); //Playground shall be semi-visible
                    //RelationshipRequirements.Add(relation);
                    //relation = new Relationship(s2, s3, 1, 0.1, 0.2, 0.3, 0.4); //Playground shall be semi-visible
                    //RelationshipRequirements.Add(relation);
                    //relation = new Relationship(s1, s4, 1, 0.5, 0.8, 1, 1); //Cafe 1 shall be fully visible
                    //RelationshipRequirements.Add(relation);
                    //relation = new Relationship(s2, s5, 1, 0.5, 0.8, 1, 1); //Cafe 2 shall be fully visible
                    //RelationshipRequirements.Add(relation);
                    relation = new Relationship(s4, s1, 2, 0.5, 0.8, 1, 1); //Cafe 1 shall may fully overlap with Square 1
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s5, s2, 2, 0.5, 0.8, 1, 1); //Cafe 2 shall may fully overlap with Square 2
                    RelationshipRequirements.Add(relation);

                    relation = new Relationship(s3, s4, 1, 0.1, 0.2, 0.3, 0.4); //Playground & Cafe 1 shall be semi-visible
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s3, s5, 1, 0.1, 0.2, 0.3, 0.4); //Playground & Cafe 2 shall be semi-visible
                    RelationshipRequirements.Add(relation);

                    break;
            }

        }

        public void InitializeRequirementScenarioFlat(int variant)
        {
            RequiredSpaces.Clear();
            FixedSpaces.Clear();
            double avgIntegration = m_spatialDiscretizer.GetAverage(AnalysisMeasure.ClosenessVGA);

            switch (variant)
            {
                //Meine Wohnung, mittels O-Spaces
                case 0:
                    Space s0 = new Space("Entrance", 2, this);

                    Space s1 = new Space("Living Room", 0, this);
                    PerformanceMembership req = new PerformanceMembership("Floor Area", 25, 30, 100, 100);
                    s1.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Isovist Area", 40, 50, 100, 100);
                    s1.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Visual Integration", avgIntegration, avgIntegration*1.2, 1, 1);
                    s1.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Daylight", 1, 1.5, 2.5, 3);
                    s1.PerformanceRequirements.Add(req);

                    Space s2 = new Space("Child's Room", 0, this);
                    req = new PerformanceMembership("Floor Area", 12, 14, 16, 18);
                    s2.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Width/Length", 0, 0.3, 0.5, 0.6);
                    s2.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Daylight", 2, 4, 10, 10);
                    s2.PerformanceRequirements.Add(req);

                    Space s3 = new Space("Bedroom", 0, this);
                    req = new PerformanceMembership("Floor Area", 12, 14, 16, 18);
                    s3.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Width/Length", 0.5, 0.7, 1.0, 1.0);
                    s3.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Visual Integration", 0.0, 0.0, avgIntegration*0.5, avgIntegration);
                    s3.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Daylight", 2, 4, 10, 10);
                    s3.PerformanceRequirements.Add(req);

                    Space s4 = new Space("Bathroom", 0, this);
                    req = new PerformanceMembership("Floor Area", 4, 6, 8, 10);
                    s4.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Daylight", 0, 0, 1, 2);
                    s4.PerformanceRequirements.Add(req);

                    Space s5 = new Space("Loggia", 0, this);
                    req = new PerformanceMembership("Floor Area", 6, 8, 10, 12);
                    s5.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Isovist Area", 25, 35, 100, 100);
                    s5.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Daylight", 3, 5, 10, 10);
                    s5.PerformanceRequirements.Add(req);

                    RequiredSpaces.Add(s0);
                    RequiredSpaces.Add(s1);
                    RequiredSpaces.Add(s2);
                    RequiredSpaces.Add(s3);
                    RequiredSpaces.Add(s4);
                    RequiredSpaces.Add(s5);
                    PerformanceRequirements.Add(req);

                    Relationship relation = new Relationship(s0, s1, 1, 0, 0, 0, 0); //Living shall be not visible from the entrance
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s0, s2, 1, 0, 0, 0, 0); //Child not visible from the entrance
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s0, s3, 1, 0, 0.0, 0.1, 0.2); //Sleeping is almost not visible from the entrance
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s0, s4, 1, 0, 0.0, 0.1, 0.2); //Sanitary is almost not visible from the entrance
                    RelationshipRequirements.Add(relation);

                    relation = new Relationship(s1, s2, 1, 0.2, 0.3, 0.5, 0.6); //Living und Child shall be semi visible
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s1, s3, 1, 0.0, 0.0, 0.1, 0.2); //Living und Sleeping shall be not visible
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s1, s4, 1, 0, 0, 0, 0); //Sanitary shall be not visible from the living
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s1, s5, 1, 0.5, 0.7, 0.8, 0.9); //Living und Loggia shall be well visible, but not fully
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s1, s5, 0, 0.0, 0.0, 1.0, 2.0); //Living und Loggia shall be close
                    RelationshipRequirements.Add(relation);

                    relation = new Relationship(s2, s3, 1, 0, 0, 0, 0.0); //Child und Sleeping shall be not visible
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s2, s4, 1, 0, 0, 0, 0.0); //Child und Sanitär shall be not visible
                    RelationshipRequirements.Add(relation);

                    relation = new Relationship(s3, s4, 1, 0, 0, 0, 0.0); //Sleeping und Sanitär shall be not visible
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s3, s4, 0, 0.5, 1.0, 2, 2.5); //Sleeping und Sanitär shall be close
                    RelationshipRequirements.Add(relation);

                    break;
                //.................................................................................................
                case 1:
                    //Meine Wohnung, mittels P-Spaces
                    AllowedOverlap = 0.1;

                    s0 = new Space("Entrance", 2, this);
                    
                    Space s1a = new Space("Relaxing", 1, this);
                    s1a.MinArea = 4;
                    req = new PerformanceMembership("Visual Integration", avgIntegration, avgIntegration*1.2, 1, 1);
                    s1a.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Isovist Compactness", 0.2, 0.4, 1, 1);
                    s1a.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Isovist Area", 35, 50, 100, 100);
                    s1a.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Isovist MinRadial", 0, 0.5, 1, 1.5);
                    s1a.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Daylight", 2, 4, 10, 10);
                    s1a.PerformanceRequirements.Add(req);

                    Space s1b = new Space("Cooking", 1, this);
                    s1b.MinArea = 4;
                    req = new PerformanceMembership("Visual Integration", avgIntegration, avgIntegration*1.2, 1, 1);
                    s1b.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Isovist Compactness", 0.2, 0.4, 1, 1);
                    s1b.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Isovist Area", 35, 50, 100, 100);
                    s1b.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Daylight", 0.5, 1, 1.5, 2);
                    s1b.PerformanceRequirements.Add(req);

                    Space s2a = new Space("Child Sleeping", 1, this);
                    s2a.MinArea = 3;
                    req = new PerformanceMembership("Visual Integration", 0, 0, avgIntegration*0.8, avgIntegration);
                    s2a.PerformanceRequirements.Add(req);

                    Space s2b = new Space("Child Playing", 1, this);
                    s2b.MinArea = 3;
                    req = new PerformanceMembership("Daylight", 3, 5, 100, 100);
                    s2b.PerformanceRequirements.Add(req);

                    Space s3a = new Space("Sleeping", 1, this);
                    s3a.MinArea = 4;
                    req = new PerformanceMembership("Visual Integration", 0, 0, avgIntegration*0.8, avgIntegration);
                    s3a.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Isovist Compactness", 0.25, 0.5, 1, 1);
                    s3a.PerformanceRequirements.Add(req);

                    Space s3b = new Space("Working", 1, this);
                    s3b.MinArea = 2;
                    req = new PerformanceMembership("Daylight", 3, 5, 100, 100);
                    s3b.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Isovist MinRadial", 0, 0.5, 1, 1.5);
                    s3b.PerformanceRequirements.Add(req);

                    Space s5a = new Space("Eating", 1, this);
                    s5a.MinArea = 2;
                    req = new PerformanceMembership("Visual Integration", 0, 0, avgIntegration*0.8, avgIntegration);
                    s5a.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Daylight", 3, 5, 100, 100);
                    s5a.PerformanceRequirements.Add(req);

                    Space s5b = new Space("Reading", 1, this);
                    s5b.MinArea = 1;
                    req = new PerformanceMembership("Daylight", 4, 5, 100, 100);
                    s5b.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Isovist MinRadial", 0, 0.5, 0.9, 1);
                    s5b.PerformanceRequirements.Add(req);
                                        
                    s4 = new Space("Bathroom", 0, this);
                    req = new PerformanceMembership("Floor Area", 4, 6, 8, 10);
                    s4.PerformanceRequirements.Add(req);
                    req = new PerformanceMembership("Daylight", 0, 0, 1, 2);
                    s4.PerformanceRequirements.Add(req);
                    //req = new PerformanceMembership("Open Edges Length", 0.6, 0.8, 1.2, 1.5);
                    //s4.PerformanceRequirements.Add(req);

                    RequiredSpaces.Add(s0);
                    RequiredSpaces.Add(s1a);
                    RequiredSpaces.Add(s1b);
                    RequiredSpaces.Add(s2a);
                    RequiredSpaces.Add(s2b);
                    RequiredSpaces.Add(s3a);
                    RequiredSpaces.Add(s3b);
                    RequiredSpaces.Add(s4);
                    RequiredSpaces.Add(s5a);
                    RequiredSpaces.Add(s5b);

                    //Entrance Relations
                    relation = new Relationship(s0, s1a, 1, 0, 0, 0, 0.1); //Entrance + shall be not visible from the entrance
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s0, s1b, 1, 0, 0, 0, 0.1); //Entrance + shall be not visible from the entrance
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s0, s2a, 1, 0, 0, 0, 0.1); //Entrance + shall be not visible from the entrance
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s0, s3a, 1, 0, 0.1, 0.2, 0.3); //Entrance + shall be not visible from the entrance
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s0, s3b, 1, 0, 0, 0, 0.1); //Entrance + shall be not visible from the entrance
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s0, s4, 1, 0, 0.1, 0.2, 0.3); //Entrance + shall be not visible from the entrance
                    RelationshipRequirements.Add(relation);
                    
                    //relaxing
                    relation = new Relationship(s1a, s1b, 1, 0.9, 1, 1, 1); //Cooking shall be visible
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s1a, s5a, 1, 0.3, 0.4, 0.6, 0.7); //Eating shall be semi visible
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s1a, s5b, 1, 0.3, 0.4, 0.6, 0.7); //Readin shall be  visible
                    RelationshipRequirements.Add(relation);

                    relation = new Relationship(s1a, s4, 1, 0, 0, 0, 0.1); //Sanitary shall be not visible 
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s1a, s3a, 1, 0, 0, 0, 0.1); // Sleeping shall be not visible 
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s1a, s3b, 1, 0, 0, 0, 0.1); // Working shall be not visible
                    RelationshipRequirements.Add(relation);

                    //Child sleeping
                    relation = new Relationship(s2a, s2b, 1, 0.9, 1, 1, 1); //Playing shall be visible
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s2a, s3a, 1, 0, 0, 0, 0.1); //Sleeping shall be not visible
                    RelationshipRequirements.Add(relation);
                    //relation = new Relationship(s2a, s4, 0, 1, 1.5, 2, 2.5); //Sanitary shall be not far
                    //RelationshipRequirements.Add(relation);

                    //Playing
                    relation = new Relationship(s2b, s3a, 1, 0, 0, 0, 0.1); //Sleeping shall be not visible
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s2b, s3b, 1, 0, 0, 0, 0.1); //Working shall be not visible
                    RelationshipRequirements.Add(relation);

                    //Sleeping
                    relation = new Relationship(s3a, s3b, 1, 0.9, 1, 1, 1); //Working shall be visible
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s3a, s4, 1, 0, 0, 0, 0.1); // Sanitary shall be not visible
                    RelationshipRequirements.Add(relation);
                    relation = new Relationship(s3a, s4, 0, 1, 1.5, 2, 2.5); //Sanitary shall be not far
                    RelationshipRequirements.Add(relation);

                    //Working
                    relation = new Relationship(s3b, s4, 1, 0, 0, 0, 0.1); // Sanitary shall be not visible
                    RelationshipRequirements.Add(relation);

                    //Eating
                    relation = new Relationship(s5a, s5b, 1, 0.8, 0.9, 1, 1); //Reading shall be visible
                    RelationshipRequirements.Add(relation);

                    break;

                case 3:
                    //Optimierungsszenario 1 (Wohnung)
                    s0 = new SpatialDescription.Space("Entrance", 1, this);
                    AllowedOverlap = 0.0;
                    //SpatialDescription.Space s1 = new SpatialDescription.Space("Living", 0, m_spatialDescription);
                    //SpatialDescription.PerformanceMembership req = new SpatialDescription.PerformanceMembership("Floor Area", 20, 25, 200, 200);
                    //s1.PerformanceRequirements.Add(req);
                    //req = new SpatialDescription.PerformanceMembership("Compactness", 0.5, 0.7, 1.0, 1.0);
                    //s1.PerformanceRequirements.Add(req);
                    ////req = new SpatialDescription.PerformanceMembership("Width/Length", 0.4, 0.55, 1.0, 1.0);
                    ////s1.PerformanceRequirements.Add(req);
                    //req = new SpatialDescription.PerformanceMembership("Minimal Width", 2.3, 2.6, 10.0, 10.0);
                    //s1.PerformanceRequirements.Add(req);
                    //req = new SpatialDescription.PerformanceMembership("Daylight", 2, 3, 10.0, 10.0);
                    //s1.PerformanceRequirements.Add(req);

                    s1 = new SpatialDescription.Space("Living", 0, this);
                    s1.MinArea = 20;
                    req = new SpatialDescription.PerformanceMembership("Floor Area", 25, 35, 100, 100);
                    s1.PerformanceRequirements.Add(req);
                    req = new SpatialDescription.PerformanceMembership("Isovist Area", 25, 35, 100, 100);
                    s1.PerformanceRequirements.Add(req);
                    //req = new SpatialDescription.PerformanceMembership("Isovist Compactness", 0.2, 0.4, 1.0, 1.0);
                    //s1.PerformanceRequirements.Add(req);
                    req = new SpatialDescription.PerformanceMembership("Visual Integration", avgIntegration*0.9, avgIntegration*1.2, 1, 1);
                    s1.PerformanceRequirements.Add(req); 
                    req = new SpatialDescription.PerformanceMembership("Daylight", 2, 4, 10.0, 10.0);
                    s1.PerformanceRequirements.Add(req);
                    req = new SpatialDescription.PerformanceMembership("Width/Length", 0.2, 0.4, 1.0, 1.0);
                    s1.PerformanceRequirements.Add(req);

                    //SpatialDescription.Space s2 = new SpatialDescription.Space("Sleeping", 1, m_spatialDescription);
                    //req = new SpatialDescription.PerformanceMembership("Floor Area", 12, 16, 20, 34);
                    //s2.PerformanceRequirements.Add(req);
                    //req = new SpatialDescription.PerformanceMembership("Isovist Compactness", 0.2, 0.5, 1.0, 1.0);
                    //s2.PerformanceRequirements.Add(req);
                    //req = new SpatialDescription.PerformanceMembership("Minimal Width", 2.3, 2.6, 10.0, 10.0);
                    //s2.PerformanceRequirements.Add(req);

                    s2 = new SpatialDescription.Space("Sleeping", 1, this);
                    s2.MinArea = 6;
                    req = new SpatialDescription.PerformanceMembership("Isovist Area", 12, 16, 25, 35);
                    s2.PerformanceRequirements.Add(req);
                    req = new SpatialDescription.PerformanceMembership("Isovist Compactness", 0.2, 0.4, 1.0, 1.0);
                    s2.PerformanceRequirements.Add(req);
                    req = new SpatialDescription.PerformanceMembership("Visual Integration", 0, 0, avgIntegration*0.8, avgIntegration*1.0);
                    s2.PerformanceRequirements.Add(req); 
                    req = new SpatialDescription.PerformanceMembership("Daylight", 2, 4, 10.0, 10.0);
                    s2.PerformanceRequirements.Add(req);


                    s3 = new SpatialDescription.Space("Bathroom", 0, this);
                    s3.MinArea = 5;
                    req = new SpatialDescription.PerformanceMembership("Floor Area", 4, 6, 10, 14);
                    s3.PerformanceRequirements.Add(req);
                    req = new SpatialDescription.PerformanceMembership("Minimal Width", 1.3, 2.0, 10.0, 10.0);
                    s3.PerformanceRequirements.Add(req);
                    req = new SpatialDescription.PerformanceMembership("Isovist Compactness", 0.2, 0.4, 1.0, 1.0);
                    s3.PerformanceRequirements.Add(req);
                    req = new SpatialDescription.PerformanceMembership("Visual Integration", 0, 0, avgIntegration * 0.5, avgIntegration);
                    s3.PerformanceRequirements.Add(req); 
                    //req = new SpatialDescription.PerformanceMembership("Isovist Area", 12, 16, 25, 41); //tmp for debug
                    //s3.PerformanceRequirements.Add(req);
                    req = new SpatialDescription.PerformanceMembership("Open Edges Length", 0.5, 0.8, 1.2, 2.5);
                    s3.PerformanceRequirements.Add(req); 
                                    

                    //s4 = new SpatialDescription.Space("Cooking", 1, this);
                    //s4.MinArea = 4;
                    ////req = new SpatialDescription.PerformanceMembership("Floor Area", 20, 30, 100, 100);
                    ////s1.PerformanceRequirements.Add(req);
                    //req = new SpatialDescription.PerformanceMembership("Isovist Area", 25, 35, 100, 100);
                    //s4.PerformanceRequirements.Add(req);
                    //req = new SpatialDescription.PerformanceMembership("Isovist MinRadial", 0.0, 0.0, 1.5, 1.5);
                    //s1.PerformanceRequirements.Add(req);
                    //req = new SpatialDescription.PerformanceMembership("Visual Integration", avgIntegration*0.8, avgIntegration*1.2, 1, 1);
                    //s4.PerformanceRequirements.Add(req); 
                    //req = new SpatialDescription.PerformanceMembership("Daylight", 2, 4, 10.0, 10.0);
                    //s4.PerformanceRequirements.Add(req);


                    RequiredSpaces.Add(s0);
                    RequiredSpaces.Add(s1);
                    RequiredSpaces.Add(s2);
                    RequiredSpaces.Add(s3);
                    //RequiredSpaces.Add(s4);

                    relation = new SpatialDescription.Relationship(s0, s1, 2, 0, 0, 0.3, 0.5); //Entrance und Living might overlap
                    RelationshipRequirements.Add(relation);

                    relation = new SpatialDescription.Relationship(s2, s3, 0, 0.5, 1, 3, 6); //Sleeping und Sanitary shall be seperated, but close
                    RelationshipRequirements.Add(relation);
                    relation = new SpatialDescription.Relationship(s0, s1, 1, 0, 0, 0.3, 0.5); //Entrance und Living shall be visually seperated
                    RelationshipRequirements.Add(relation);
                    relation = new SpatialDescription.Relationship(s0, s2, 1, 0, 0, 0.1, 0.3); //Entrance und Sleeping shall be very visually seperated
                    RelationshipRequirements.Add(relation);
                    relation = new SpatialDescription.Relationship(s0, s3, 1, 0, 0, 0.3, 0.7); //Entrance und Sanitary shall be visually seperated
                    RelationshipRequirements.Add(relation);

                    //relation = new SpatialDescription.Relationship(s4, s1, 2, 0.1, 0.3, 0.6, 1.0); //Cooking und Living can be in one place
                    //RelationshipRequirements.Add(relation);
                    //relation = new SpatialDescription.Relationship(s4, s2, 1, 0.0, 0.0, 0.2, 0.4); //Cooking und Sleeping visually seperated
                    //RelationshipRequirements.Add(relation);

                    //relation = new SpatialDescription.Relationship(s0, s3, 0, 1, 2, 10, 50); //Entrance und Sanitary shall be not too touching
                    //RelationshipRequirements.Add(relation);
                    //relation = new SpatialDescription.Relationship(s0, s1, 1, 0.5, 0.7, 1, 1); //Entrance und Living shall be visible
                    //RelationshipRequirements.Add(relation);

                    relation = new SpatialDescription.Relationship(s1, s2, 1, 0.0, 0.0, 0.1, 0.3); //Sleeping und Living shall be maximum semi visible
                    RelationshipRequirements.Add(relation);
                    //relation = new SpatialDescription.Relationship(s1, s3, 0, 0.5, 1, 3, 6); //Living und Sanitär shall be close
                    //RelationshipRequirements.Add(relation);
                    relation = new SpatialDescription.Relationship(s1, s3, 1, 0, 0, 0.3, 0.5); //Living und Sanitär shall be visually seperated
                    RelationshipRequirements.Add(relation);
                    relation = new SpatialDescription.Relationship(s2, s3, 1, 0, 0, 0.3, 0.5); //Sleeping und Sanitär shall be visually seperated
                    RelationshipRequirements.Add(relation);

                    break;


            }



        }

        public void InitializeRequirementScenarioPointCluster()
        {
            RequiredSpaces.Clear();
            FixedSpaces.Clear();

            double avgIntegration = m_spatialDiscretizer.GetAverage(AnalysisMeasure.ClosenessVGA);
            avgIntegration *= 0.99;

            Space s1 = new Space("R1", 0, this);
            s1.MinArea = 4;
            //PerformanceMembership req = new PerformanceMembership("Visual Integration", 0, 0, avgIntegration, avgIntegration); //privat
            //s1.PerformanceRequirements.Add(req);
            PerformanceMembership req = new PerformanceMembership("Floor Area", 14, 20, 24, 30);
            s1.PerformanceRequirements.Add(req);
            //req = new PerformanceMembership("Isovist Area", 15, 30, 35, 50);
            //s1.PerformanceRequirements.Add(req); 
            //req = new PerformanceMembership("Isovist Compactness", 0.2, 0.2, 1.0, 1.0);
            //s1.PerformanceRequirements.Add(req); 
            req = new PerformanceMembership("Daylight", 2.5, 4, 10, 10);
            s1.PerformanceRequirements.Add(req);
            req = new PerformanceMembership("Minimal Width", 2.3, 2, 10, 10);
            s1.PerformanceRequirements.Add(req);

            Space s2 = new Space("R2", 1, this);
            s2.MinArea = 4;
            req = new PerformanceMembership("Visual Integration", 0, 0, avgIntegration*0.5, avgIntegration); //privat
            s2.PerformanceRequirements.Add(req);
            req = new PerformanceMembership("Isovist Area", 12, 16, 25, 30);
            s2.PerformanceRequirements.Add(req);
            req = new PerformanceMembership("Isovist Compactness", 0.3, 0.5, 1.0, 1.0);
            s2.PerformanceRequirements.Add(req);
            //req = new PerformanceMembership("Daylight", 2, 3, 10, 10);
            //s2.PerformanceRequirements.Add(req);

            Space s3 = new Space("R3", 0, this);
            s3.MinArea = 10;
            req = new PerformanceMembership("Visual Integration", 0, 0, 0.75*avgIntegration, avgIntegration); //public
            s3.PerformanceRequirements.Add(req);
            req = new PerformanceMembership("Floor Area", 10, 12, 16, 18);
            s3.PerformanceRequirements.Add(req);
            req = new PerformanceMembership("Minimal Width", 2.3, 2, 10, 10);
            s3.PerformanceRequirements.Add(req);
            //req = new PerformanceMembership("Isovist Area", 20, 30, 50, 60);
            //s3.PerformanceRequirements.Add(req);
            //req = new PerformanceMembership("Isovist Compactness", 0.1, 0.2, 1.0, 1.0);
            //s3.PerformanceRequirements.Add(req);
            req = new PerformanceMembership("Daylight", 2.5, 4, 10, 10);
            s3.PerformanceRequirements.Add(req);


            RequiredSpaces.Add(s1);
            RequiredSpaces.Add(s2);
            RequiredSpaces.Add(s3);

            Relationship relation = new Relationship(s1, s3, 1, 0, 0, 0, 0.3); //R1 und R3 shall be not visible
            RelationshipRequirements.Add(relation);
            relation = new Relationship(s1, s2, 0, 0.5, 1, 3, 5); //R2 und R3 shall be not visible
            RelationshipRequirements.Add(relation);
            relation = new Relationship(s2, s3, 1, 0.1, 0.2, 0.3, 0.5); //R2 und R3 shall be well visible
            RelationshipRequirements.Add(relation);

            //Space s0 = new Space("Entrance", new Vector2d(0.2, 8.5), this);
            //relation = new Relationship(s0, s1, 1, 0.4, 0.7, 1, 1); //R2 und R3 shall be well visible
            //RelationshipRequirements.Add(relation);
            //FixedSpaces.Add(s0);

            //Relationship relation = new Relationship(s1, s3, 1, 0, 0, 0, 0.3); //R1 und R3 shall be not visible
            //RelationshipRequirements.Add(relation);
            //relation = new Relationship(s2, s3, 1, 0, 0, 0, 0.3); //R2 und R3 shall be not visible
            //RelationshipRequirements.Add(relation);
            //relation = new Relationship(s1, s2, 1, 0.5, 0.7, 1.0, 1.0); //R2 und R3 shall be not visible
            //RelationshipRequirements.Add(relation);
        }

        void GetPotentialPointCluster(Space space)
        {
            List<VisibilityGraphAnalysis.PointCluster> pointClusters = new List<VisibilityGraphAnalysis.PointCluster>();

            bool measureExists = true;
            try { double a = m_spatialDiscretizer.AnalysisPoints[0].Properties[CPlan.Isovist2D.AnalysisMeasure.Region]; }
            catch { measureExists = false; }

            //find all
            List<VisibilityGraphAnalysis.AnalysisPoint> region = new List<VisibilityGraphAnalysis.AnalysisPoint>();
            foreach (VisibilityGraphAnalysis.AnalysisPoint ap in m_spatialDiscretizer.AnalysisPoints)
            {
                ap.tmpVisited = false;
                float perf = (float)GetPointPerformance(ap, space.PerformanceRequirements, 1);
                if (perf > m_minPointPerformance)
                {
                    if (measureExists)
                    {
                        ap.Properties[AnalysisMeasure.Region] = 0;
                        ap.Properties[AnalysisMeasure.RegionTransparency] = perf;
                        ap.Properties[AnalysisMeasure.SeperatedRegions] = -1;
                    }
                    else
                    {
                        ap.Properties.Add(AnalysisMeasure.Region, 0);
                        ap.Properties.Add(AnalysisMeasure.RegionTransparency, perf);
                        ap.Properties.Add(AnalysisMeasure.SeperatedRegions, -1);
                    }
                    region.Add(ap);
                }
                else
                {
                    if (measureExists)
                    {
                        ap.Properties[AnalysisMeasure.Region] = -1;
                        ap.Properties[AnalysisMeasure.RegionTransparency] = 0;
                        ap.Properties[AnalysisMeasure.SeperatedRegions] = -1;
                    }
                    else
                    {
                        ap.Properties.Add(AnalysisMeasure.Region, -1);
                        ap.Properties.Add(AnalysisMeasure.RegionTransparency, 0);
                        ap.Properties.Add(AnalysisMeasure.SeperatedRegions, -1);
                    }
                }
            }

            //find seperate
            while (region.Count > 0)
            {
                VisibilityGraphAnalysis.AnalysisPoint start = region[0];
                start.tmpVisited = true;
                List<VisibilityGraphAnalysis.AnalysisPoint> cluster = new List<VisibilityGraphAnalysis.AnalysisPoint>();
                cluster.Add(start);

                Traverse(start, cluster);

                VisibilityGraphAnalysis.PointCluster pCluster = new VisibilityGraphAnalysis.PointCluster(m_spatialDiscretizer);
                pCluster.Points.AddRange(cluster);
                pointClusters.Add(pCluster);

                foreach (VisibilityGraphAnalysis.AnalysisPoint ap in cluster)
                    region.Remove(ap);
            }

            double area;
            double areaPerf;
            double minArea;
            List<VisibilityGraphAnalysis.PointCluster> finalPointClusterSet = new List<VisibilityGraphAnalysis.PointCluster>();

            if (m_divideByCommunity)
            {
                //divide by community detection
                BackgroundWorker bgWorker = new BackgroundWorker();
                ICollection<Community> communities = null;
                ClusterCalculator clusterCalculator = new ClusterCalculator();
                List<VisibilityGraphAnalysis.AnalysisPoint> tmpNodes = new List<VisibilityGraphAnalysis.AnalysisPoint>();
                
                int k = 0;
                foreach (VisibilityGraphAnalysis.PointCluster pointCluster in pointClusters)
                {
                    area = pointCluster.Points.Count * m_spatialDiscretizer.m_cellSize * m_spatialDiscretizer.m_cellSize;
                    areaPerf = -1;
                    minArea = space.MinArea;
                    if (area > 6)
                    {

                        Smrf.NodeXL.Core.Graph graph = new Smrf.NodeXL.Core.Graph();

                        foreach (VisibilityGraphAnalysis.AnalysisPoint ap in pointCluster.Points)
                        {
                            Vertex vertex = new Vertex();
                            graph.Vertices.Add(vertex);
                            ap.NodeXLVertexID = vertex.ID;
                        }

                        foreach (VisibilityGraphAnalysis.APEdge edge in m_spatialDiscretizer.m_visibilityEdges)
                        {
                            try
                            {
                                IVertex vertex1, vertex2;
                                graph.Vertices.Find(edge.Node1.NodeXLVertexID, out vertex1);
                                graph.Vertices.Find(edge.Node2.NodeXLVertexID, out vertex2);
                                if (vertex1 != null && vertex2 != null)
                                    graph.Edges.Add(vertex1, vertex2, false);
                            }
                            catch (Exception e)
                            { }
                        }

                        clusterCalculator.Algorithm = ClusterAlgorithm.WakitaTsurumi;

                        try { clusterCalculator.TryCalculateGraphMetrics(graph, null, out communities); }
                        catch (Exception ex)
                        {
                            int a = 5;
                        }

                        foreach (VisibilityGraphAnalysis.AnalysisPoint node in pointCluster.Points)
                            tmpNodes.Add(node);

                        List<List<VisibilityGraphAnalysis.AnalysisPoint>> clusterFirstProcess = new List<List<VisibilityGraphAnalysis.AnalysisPoint>>();


                        foreach (Community community in communities)
                        {
                            List<VisibilityGraphAnalysis.AnalysisPoint> newCluster = new List<VisibilityGraphAnalysis.AnalysisPoint>();
                            foreach (Vertex vertex in community.Vertices)
                            {
                                int m = 0;
                                while (m < tmpNodes.Count)
                                {
                                    if (tmpNodes[m].NodeXLVertexID == vertex.ID)
                                    {
                                        //tmpNodes[m].Properties[AnalysisMeasure.SeperatedRegions] = k;

                                        newCluster.Add(tmpNodes[m]);
                                        tmpNodes.Remove(tmpNodes[m]);
                                        m--;
                                    }
                                    m++;
                                }
                            }
                            k++;
                            clusterFirstProcess.Add(newCluster);
                        }

                        foreach (List<VisibilityGraphAnalysis.AnalysisPoint> cluster in clusterFirstProcess)
                        {
                            VisibilityGraphAnalysis.PointCluster newPCluster = new VisibilityGraphAnalysis.PointCluster(m_spatialDiscretizer);
                            newPCluster.Points.AddRange(cluster);
                            finalPointClusterSet.Add(newPCluster);
                        }
                    }
                    else
                    {
                        finalPointClusterSet.Add(pointCluster);
                    }
                }
            }
            else
            {
                finalPointClusterSet = pointClusters;
            }


            //check, wie oft die benötigte Fläche in die Regionen reinpasst, und generiere ein Set an möglichen Regionen innerhalb des clusters
            int idSepRegion=0;
            foreach (VisibilityGraphAnalysis.PointCluster pointCluster in finalPointClusterSet)
            {
                idSepRegion++;
                foreach (VisibilityGraphAnalysis.AnalysisPoint node in pointCluster.Points)
                     node.Properties[AnalysisMeasure.SeperatedRegions] = idSepRegion;
                
                List<VisibilityGraphAnalysis.PointCluster> smallerClusters = new List<VisibilityGraphAnalysis.PointCluster>();
                area = pointCluster.Points.Count * m_spatialDiscretizer.m_cellSize * m_spatialDiscretizer.m_cellSize;
                areaPerf = -1;
                minArea = space.MinArea;

                //anzahl an zu erzeugenden Positionen innerhalb der potential region
                int n = (int)(area / minArea);
                Random rnd = new Random((int)DateTime.Now.Ticks + RandomNumberGenerator.Create().GetHashCode());
                maxN = (int)Math.Round(minArea / (m_spatialDiscretizer.m_cellSize * m_spatialDiscretizer.m_cellSize));
                int iterations = 0;
                while (smallerClusters.Count < n*2 && iterations < n * 4)
                {
                    int rndPoint = (int)(pointCluster.Points.Count * rnd.NextDouble());
                    List<VisibilityGraphAnalysis.AnalysisPoint> cluster = new List<VisibilityGraphAnalysis.AnalysisPoint>();
                    m_spatialDiscretizer.ResetAnalysisPointVisited();
                    curN = 0;
                    cluster = FindCluster(pointCluster.Points[rndPoint], maxN, idSepRegion);
                    //TraverseNTimes(pointCluster.Points[rndPoint], cluster);
                    VisibilityGraphAnalysis.PointCluster pC = new VisibilityGraphAnalysis.PointCluster(m_spatialDiscretizer);
                    pC.Points.AddRange(cluster);

                    if (smallerClusters.Count == 0)
                        smallerClusters.Add(pC);
                    else
                    {
                        bool foundSimilar = false;
                        foreach (VisibilityGraphAnalysis.PointCluster sCluster in smallerClusters)
                            if (sCluster.OverlapTo(pC) > m_allowedOverlapForSearchingPointCluster)
                            {
                                foundSimilar = true;
                                break;
                            }

                        if (!foundSimilar)
                            smallerClusters.Add(pC);
                    }
                    iterations++;

                }

                foreach (VisibilityGraphAnalysis.PointCluster pC in smallerClusters)
                {
                    List<double> perfValues = new List<double>();

                    double sum = 0;
                    foreach (VisibilityGraphAnalysis.AnalysisPoint ap in pC.Points)
                        sum += GetPointPerformance(ap, space.PerformanceRequirements, 1);

                    space.PotentialRegions.Add(new AssignedSpace(space, pC, sum/pC.Points.Count));
                }
            }

        }
        
        int maxN, curN;

        List<VisibilityGraphAnalysis.AnalysisPoint> FindCluster(VisibilityGraphAnalysis.AnalysisPoint origin, int size, int iDSepRegion)
        {
            //Variante While Schleife:
            List<VisibilityGraphAnalysis.AnalysisPoint> newPoints = new List<VisibilityGraphAnalysis.AnalysisPoint>();
            newPoints.Add(origin);
            origin.tmpVisited = true;
            List<VisibilityGraphAnalysis.AnalysisPoint> tmpPtList1 = new List<VisibilityGraphAnalysis.AnalysisPoint>();
            foreach (VisibilityGraphAnalysis.AnalysisPoint ap in origin.MooreNeighbors)
            {
                if (ap.Properties[CPlan.Isovist2D.AnalysisMeasure.SeperatedRegions] == iDSepRegion)
                    tmpPtList1.Add(ap);
                ap.tmpVisited = true;
            }

            List<VisibilityGraphAnalysis.AnalysisPoint> tmpPtList2 = new List<VisibilityGraphAnalysis.AnalysisPoint>();
            int iterations = 0;
            while (newPoints.Count < size && iterations < size * 4)
            {
                if (tmpPtList1.Count > 0)
                {
                    newPoints.Add(tmpPtList1[0]);
                    foreach (VisibilityGraphAnalysis.AnalysisPoint ap in tmpPtList1[0].MooreNeighbors)
                        if (!tmpPtList2.Contains(ap) && !ap.tmpVisited)
                        {
                            if (ap.Properties[CPlan.Isovist2D.AnalysisMeasure.SeperatedRegions] == iDSepRegion)
                                tmpPtList2.Add(ap);
                            ap.tmpVisited = true;
                        }
                    tmpPtList1.Remove(tmpPtList1[0]);
                }
                else
                {
                    tmpPtList1 = new List<VisibilityGraphAnalysis.AnalysisPoint>(tmpPtList2);
                    tmpPtList2.Clear();
                }

                iterations++;
            }

            return newPoints;
        }

        void TraverseNTimes(VisibilityGraphAnalysis.AnalysisPoint origin, List<VisibilityGraphAnalysis.AnalysisPoint> resultCluster)
        {
            
            List<VisibilityGraphAnalysis.AnalysisPoint> newPoints = new List<VisibilityGraphAnalysis.AnalysisPoint>();
            foreach (VisibilityGraphAnalysis.AnalysisPoint ap in origin.NeumannNeighbors)
            {
                if (!ap.tmpVisited)
                {
                    if (ap.Properties[CPlan.Isovist2D.AnalysisMeasure.Region] == 0)
                    {
                        if (resultCluster.Count < maxN)
                        {
                            resultCluster.Add(ap);
                            newPoints.Add(ap);
                            ap.tmpVisited = true;
                        }
                    }
                }
            }
            foreach (VisibilityGraphAnalysis.AnalysisPoint ap in newPoints)
            {
                TraverseNTimes(ap, resultCluster);
            }
        }

        void Traverse(VisibilityGraphAnalysis.AnalysisPoint origin, List<VisibilityGraphAnalysis.AnalysisPoint> cluster)
        {
            foreach (VisibilityGraphAnalysis.AnalysisPoint ap in origin.NeumannNeighbors)
            {
                if (!ap.tmpVisited)
                {
                    if (ap.Properties[CPlan.Isovist2D.AnalysisMeasure.Region] == 0)
                    {
                        cluster.Add(ap);
                        ap.tmpVisited = true;
                        Traverse(ap, cluster);
                    }
                }
            }
        }

        double GetPointPerformance(VisibilityGraphAnalysis.AnalysisPoint ap, List<PerformanceMembership> perfRequirements, int type )
        {
            int n = 0;
            double summedPerf = 0;
            double minPerf = double.MaxValue;
            double maxPerf = double.MinValue;

            foreach (SpatialDescription.PerformanceMembership perf in perfRequirements)
            {
                double value = GetValueFromPoint(perf, ap);
                double performance = perf.GetMembershipDegree(value);

                if (performance < minPerf)
                    minPerf = performance;
                if (performance > maxPerf)
                    maxPerf = performance;
                summedPerf += performance;
                n++;
            }

            if (type == 0)
                return summedPerf / n;
            else if (type == 1)
                return minPerf;
            else
                return maxPerf;
        }

        public void CalculateRequirementPerformance(bool useReducedSetOnly, bool useNonoverlappingSSpaces)
        {
            //suche nach passenden Konfigurationen


            //Vorauswahl treffen??
            List<VisibilityGraphAnalysis.ConvexSpace> selectedConvexSpaces = new List<VisibilityGraphAnalysis.ConvexSpace>();
            if (useReducedSetOnly)
            {
                foreach (VisibilityGraphAnalysis.ConvexSpace lRegion in m_spatialDiscretizer.m_tmpLargeConvexSpacesReduced)
                    if (lRegion.SubRegions.Count > 1)
                        selectedConvexSpaces.Add(lRegion);
            }
            else
            {
                foreach (VisibilityGraphAnalysis.ConvexSpace lRegion in m_spatialDiscretizer.OverlappingConvexSpaces)
                    //if (lRegion.SubRegions.Count > 1)
                        selectedConvexSpaces.Add(lRegion);
            }
            
            //S-Spaces, die keine Überlappung aufweisen hinzunehmen
            if (useNonoverlappingSSpaces) 
                foreach (VisibilityGraphAnalysis.ConvexSpace lRegion in m_spatialDiscretizer.SmallConvexSpaces)
                    if (lRegion.SubRegions[0].LargerRegions.Count == 1)
                        selectedConvexSpaces.Add(lRegion);


            //find all possible Regions for each required space
            foreach (Space space in RequiredSpaces)
            {
                if (space.Type == 0)
                {
                    space.PotentialRegions.Clear();
                    foreach (VisibilityGraphAnalysis.ConvexSpace lRegion in selectedConvexSpaces)
                    {
                        if (lRegion.Center.X > 60 && lRegion.Center.X < 70 && lRegion.Center.Y > 130 && lRegion.Center.Y < 140)
                        {
                            int x = 5;
                        }

                        double perf = GetPerformances(space, lRegion, 1);
                        if (perf > m_minOSpacePerformance)
                            space.PotentialRegions.Add(new AssignedSpace(space, lRegion, perf));
                    }
                }
                else if (space.Name == "Entrance")
                {
                    space.PotentialRegions.Clear();
                    foreach (Vector2d doorP in m_spatialDiscretizer.potentialDoorPoints)
                    {
                        //space.PotentialRegions.Add(new AssignedSpace(space, doorP, 1));

                        VisibilityGraphAnalysis.AnalysisPoint dP = m_spatialDiscretizer.GetAnalysisPoint(doorP);
                        if (dP != null)
                        {
                            VisibilityGraphAnalysis.PointCluster DP = new VisibilityGraphAnalysis.PointCluster(m_spatialDiscretizer);
                            DP.Points.Add(dP);
                            space.PotentialRegions.Add(new AssignedSpace(space, DP, 1));
                        }
                    }
                    //space.Type = 1;
                }
                else if (space.Type == 1)
                {
                    space.PotentialRegions.Clear();
                    GetPotentialPointCluster(space);
                }
            }

            //sortiere spaces nach Anzahl der potentiellen Spaces, geringste Anzahl zuerst!
            RequiredSpaces.Sort(delegate(Space s1, Space s2) { return s1.PotentialRegions.Count.CompareTo(s2.PotentialRegions.Count); });

            //find best combination
            setsOfValidCombinations.Clear();
            setsOfInvalidCombinations.Clear();

            foreach (AssignedSpace curRegion in RequiredSpaces[0].PotentialRegions)
            {
                List<AssignedSpace> usedRegions = new List<AssignedSpace>();
                usedRegions.Add(curRegion);
                TraverseSpaceRequirements(RequiredSpaces[0], usedRegions, new List<Relationship>(), 1, curRegion.Performance, 0, curRegion.Performance, 1, 0);
            }

            double bestP = 0;
            double bestPropertiesPerformance = double.MinValue;
            double bestRelationshipPerformance = double.MinValue;
            double bestMinPropertiesPerformance = double.MinValue;
            double bestMinRelationshipPerformance = double.MinValue;
            double bestNrOfFulfilledRelations = double.MinValue;
     
            if (setsOfValidCombinations.Count > 0)
                foreach (KeyValuePair<Dictionary<Space, AssignedSpace>, double[]> combination in setsOfValidCombinations)
                {
                    //double summedPerf = Math.Sqrt(combination.Value[0] * combination.Value[0] + combination.Value[1] * combination.Value[1]);
                    double avgPropPerf = combination.Value[0] / MaxPropertiesPerformance;
                    double avgRelPerf = combination.Value[1] / MaxRelationshipPerformance;
                    double minPropPerf = combination.Value[2];
                    double minRelPerf = combination.Value[3];
                    double fulfilledRel = (m_relationshipRequirements.Count - combination.Value[4]) / m_relationshipRequirements.Count;

                    double summedPerf = Math.Sqrt(
                        avgPropPerf * avgPropPerf +
                        avgRelPerf * avgRelPerf +
                        minPropPerf * minPropPerf +
                        fulfilledRel * fulfilledRel);

                    //test ob die beiden reichen:
                    //summedPerf = Math.Sqrt(
                    //    avgRelPerf * avgRelPerf +
                    //    minPropPerf * minPropPerf);

                    if (summedPerf > bestP)
                    {
                        bestP = summedPerf;
                        BestCombination = combination.Key;

                        bestPropertiesPerformance = avgPropPerf;
                        bestRelationshipPerformance = avgRelPerf;
                        bestMinPropertiesPerformance = minPropPerf;
                        bestMinRelationshipPerformance = minRelPerf;
                        bestNrOfFulfilledRelations = fulfilledRel;
                    }
                }
            //else
            //    foreach (KeyValuePair<Dictionary<Space, AssignedSpace>, double[]> combination in setsOfInvalidCombinations)
            //    {
            //        //double summedPerf = Math.Sqrt(combination.Value[0] * combination.Value[0] + combination.Value[1] * combination.Value[1]);
            //        double summedPerf = Math.Sqrt(combination.Value[2] * combination.Value[2] + combination.Value[3] * combination.Value[3]); 
            //        if (summedPerf > bestP)
            //        {
            //            bestP = summedPerf;
            //            BestCombination = combination.Key;
            //            //bestPropertiesPerformance = combination.Value[0];
            //            //bestRelationshipPerformance = combination.Value[1];
            //            bestPropertiesPerformance = combination.Value[2];
            //            bestRelationshipPerformance = combination.Value[3];
            //        }
            //    }

            if (bestPropertiesPerformance == double.MinValue)
                bestPropertiesPerformance = -0.1;
            if (bestRelationshipPerformance == double.MinValue)
                bestRelationshipPerformance = -0.1;
            if (bestMinPropertiesPerformance == double.MinValue)
                bestMinPropertiesPerformance = -0.1;
            if (bestMinRelationshipPerformance == double.MinValue)
                bestMinRelationshipPerformance = -0.1;
            if (bestNrOfFulfilledRelations == double.MinValue)
                bestNrOfFulfilledRelations = -0.1;

            PropertiesPerformance = 1 - bestPropertiesPerformance;
            RelationshipPerformance = 1 - bestRelationshipPerformance;
            MinPropertiesPerformance = 1-bestMinPropertiesPerformance;
            MinRelationshipPerformance = 1- bestMinRelationshipPerformance;
            AmountOfFulfilledRelations = 1 - bestNrOfFulfilledRelations;



            //debug:
            //foreach (Space s in RequiredSpaces)
            //    if (s.Name == "Entrance")
            //        s.Type = 2;

        }


        double CalculateRelationshipPerformance(AssignedSpace region1, AssignedSpace region2, Relationship rel)
        {
            double relPerf = 0;

            switch (rel.Type)
            {
                case 0:
                    double distance=-1;
                    if (region1.Type == 0)
                        if (region2.Type == 0)
                            distance = region1.ConvexSpace.DistanceToViaGrid(region2.ConvexSpace);
                    if (region1.Type == 0)
                        if (region2.Type == 1)
                            distance = region1.ConvexSpace.DistanceToViaGrid(region2.PointCluster);
                    //if (region1.Type == 0)
                    //    if (region2.Type == 2)
                    //        distance = region1.ConvexSpace.DistanceTo(region2.Point);

                    if (region1.Type == 1)
                        if (region2.Type == 0)
                            distance = region2.ConvexSpace.DistanceToViaGrid(region1.PointCluster);
                    if (region1.Type == 1)
                        if (region2.Type == 1)
                            distance = region1.PointCluster.DistanceToViaGrid(region2.PointCluster);
                    //if (region1.Type == 1)
                    //    if (region2.Type == 2)
                    //        distance = region1.PointCluster.DistanceTo(region2.Point);

                    //if (region1.Type == 2)
                    //    if (region2.Type == 0)
                    //        distance = region2.ConvexSpace.DistanceTo(region1.Point);

                    //if (region1.Type == 2)
                    //    if (region2.Type == 1)
                    //        distance = region2.PointCluster.DistanceTo(region1.Point);

                    relPerf = rel.GetMembershipDegree(distance);
                    break;
                case 1:
                    double visibility=-1;

                    if (region1.Type == 0)
                        if (region2.Type == 0)
                            visibility = region1.ConvexSpace.VisibilityTo(region2.ConvexSpace);

                    if (region1.Type == 0)
                        if (region2.Type == 1)
                            visibility = region1.ConvexSpace.VisibilityTo(region2.PointCluster);

                    //if (region1.Type == 0)
                    //    if (region2.Type == 2)
                    //        visibility = region1.ConvexSpace.VisibilityTo(region2.Point);

                    if (region1.Type == 1)
                        if (region2.Type == 0)
                            visibility = region2.ConvexSpace.VisibilityTo(region1.PointCluster);

                    if (region1.Type == 1)
                        if (region2.Type == 1)
                            visibility = region1.PointCluster.VisibilityTo(region2.PointCluster);

                    //if (region1.Type == 1)
                    //    if (region2.Type == 2)
                    //        visibility = region1.PointCluster.VisibilityTo(region2.Point);

                    //if (region1.Type == 2)
                    //    if (region2.Type == 0)
                    //        visibility = region2.ConvexSpace.VisibilityTo(region1.Point);

                    //if (region1.Type == 2)
                    //    if (region2.Type == 1)
                    //        visibility = region2.PointCluster.VisibilityTo(region1.Point);

                    int k = 5;
                    if (visibility == 0 && rel.FromSpace.Name=="R1" && rel.ToSpace.Name=="R2")
                        k = 5;

                    relPerf = rel.GetMembershipDegree(visibility);
                    break;

                case 2:
                    double overlap = -1;

                    AssignedSpace r1, r2;

                    if (rel.FromSpace == region1.RequiredSpace)
                    {
                        r1 = region1;
                        r2 = region2;
                    }
                    else
                    {
                        r1 = region2;
                        r2 = region1;
                    }

                    if (r1.Type == 0)
                        if (r2.Type == 0)
                            overlap = r1.ConvexSpace.OverlapTo(r2.ConvexSpace);

                    if (r1.Type == 0)
                        if (r2.Type == 1)
                            overlap = r1.ConvexSpace.OverlapTo(r2.PointCluster);

                    if (r1.Type == 1)
                        if (r2.Type == 0)
                            overlap = r1.PointCluster.OverlapTo(r2.ConvexSpace);

                    if (r1.Type == 1)
                        if (r2.Type == 1)
                            overlap = r1.PointCluster.OverlapTo(r2.PointCluster);

                    relPerf = rel.GetMembershipDegree(overlap);
                    break;
            }

            return relPerf;
        }

        //double CalculateToPointRelationshipPerformance(Vector2d point, AssignedSpace region, Relationship rel)
        //{
        //    double relPerf = 0;

        //    switch (rel.Type)
        //    {
        //        case 0:
        //            double distance = -1;
        //            if (region.Type == 0)
        //                distance = region.ConvexSpace.DistanceTo(point);
                   
        //            relPerf = rel.GetMembershipDegree(distance);
        //            break;
        //        case 1:
        //            double visibility = -1;
        //            if (region.Type == 0)
        //                visibility = region.ConvexSpace.VisibilityTo(point);
        //            if (region.Type == 1)
        //                visibility = region.PointCluster.VisibilityTo(point);
        //            relPerf = rel.GetMembershipDegree(visibility);
        //            break;
        //    }

        //    return relPerf;
        //}

        bool Overlaps(List<AssignedSpace> usedRegions, AssignedSpace checkSpace)
        {
            bool found = false;
            foreach (AssignedSpace aS in usedRegions)
            {
                bool overlapRelationshipExists = false;
                foreach (Relationship rel in checkSpace.RequiredSpace.RelationshipRequirements)
                    if (rel.Type == 2)
                        if (rel.FromSpace == aS.RequiredSpace || rel.ToSpace == aS.RequiredSpace)
                            overlapRelationshipExists = true;
                
                if (!overlapRelationshipExists)
                {
                    if (aS.Type == 0)
                    {
                        if (checkSpace.Type == 0)
                        {
                            if (aS.ConvexSpace == checkSpace.ConvexSpace)
                            {
                                found = true;
                                break;
                            }
                            if (checkSpace.ConvexSpace.OverlapTo(aS.ConvexSpace) > m_allowedOverlap)
                            {
                                found = true;
                                break;
                            }

                        }
                        else if (checkSpace.Type == 1)
                        {
                            if (checkSpace.PointCluster.OverlapTo(aS.ConvexSpace) > m_allowedOverlap) //lieber den Overlap checken??
                            {
                                found = true;
                                break;
                            }
                        }
                    }
                    if (aS.Type == 1)
                    {
                        if (checkSpace.Type == 1)
                        {
                            if (checkSpace.PointCluster.OverlapTo(aS.PointCluster) > m_allowedOverlap) //lieber den Overlap checken??
                            {
                                found = true;
                                break;
                            }
                        }
                        else if (checkSpace.Type == 0)
                        {
                            if (checkSpace.ConvexSpace.OverlapTo(aS.PointCluster) > m_allowedOverlap) //lieber den Overlap checken??
                            {
                                found = true;
                                break;
                            }
                        }
                    }
                }
            }
            return found;
        }
        
        int maxRecursions = 0;

        void TraverseSpaceRequirements(Space curSpace, List<AssignedSpace> usedRegions, List<Relationship> usedRelationships, int i, double summedPerformance, double summedRelationshipPerformance, double minPropPerf, double minRelPerf, int nrNotFulfilledRel)
        {
            if (i < RequiredSpaces.Count)
            {
                if (i == 2)
                {
                    int x = 5;
                }
                Space Si = RequiredSpaces[i];
                if (Si.PotentialRegions.Count == 0)
                    return;
                else
                    foreach (AssignedSpace regionSi in Si.PotentialRegions)
                    {

                        if ((!Overlaps(usedRegions, regionSi)) || (regionSi.Type == 2))
                        {
                            List<double> relationshipPerformance = new List<double>();
                            List<Relationship> usedRelNew = new List<Relationship>();
                            foreach (Relationship rel in usedRelationships)
                                usedRelNew.Add(rel);
                            foreach (Relationship rel in RelationshipRequirements)
                            {
                                if (!usedRelNew.Contains(rel))
                                {
                                    int nrRegionToRelateTo = -1;
                                    if (rel.FromSpace == Si)
                                    {
                                        for (int l = 0; l < RequiredSpaces.Count; l++)
                                            if (rel.ToSpace == RequiredSpaces[l])
                                                nrRegionToRelateTo = l;
                                    }
                                    if (rel.ToSpace == Si)
                                    {
                                        for (int l = 0; l < RequiredSpaces.Count; l++)
                                            if (rel.FromSpace == RequiredSpaces[l])
                                                nrRegionToRelateTo = l;
                                    }

                                    if (nrRegionToRelateTo != -1 && nrRegionToRelateTo < usedRegions.Count)
                                    {
                                        relationshipPerformance.Add(CalculateRelationshipPerformance(usedRegions[nrRegionToRelateTo], regionSi, rel));
                                        usedRelNew.Add(rel);
                                    }
                                }
                            }

                            bool oneRelIsNotFulfilled = false;
                            double relPSummed = 0;
                            double newMinRelPerf = minRelPerf;
                            int newNotFullfilledRel = nrNotFulfilledRel;
                            foreach (double p in relationshipPerformance)
                            {
                                relPSummed += p;
                                if (p == 0)
                                {
                                    oneRelIsNotFulfilled = true;
                                    newNotFullfilledRel++;
                                }
                                if (p < newMinRelPerf)
                                    newMinRelPerf = p;

                            }

                            if (oneRelIsNotFulfilled)
                            {
                                Dictionary<Space, AssignedSpace> combination = new Dictionary<Space, AssignedSpace>();
                                for (int k = 0; k < usedRegions.Count; k++)
                                    combination.Add(RequiredSpaces[k], usedRegions[k]);
                                double[] performances = new double[5];
                                performances[0] = summedPerformance;
                                performances[1] = summedRelationshipPerformance;
                                performances[2] = minPropPerf;
                                performances[3] = newMinRelPerf;
                                performances[4] = newNotFullfilledRel;
                                setsOfInvalidCombinations.Add(new KeyValuePair<Dictionary<Space, AssignedSpace>, double[]>(combination, performances));
                            }
                            //falls doch nicht alle Kombinationen durchsucht werden sollen, dann das else wieder einkommentieren
                            //else
                            {
                                double newSumRelP = summedRelationshipPerformance + relPSummed;
                                double newSumP = summedPerformance + regionSi.Performance;
                                double newMinPropPerf = minPropPerf;
                                if (regionSi.Performance < newMinPropPerf)
                                    newMinPropPerf = regionSi.Performance;
                                    
                                List<AssignedSpace> usedRegionsNew = new List<AssignedSpace>();
                                foreach (AssignedSpace sr in usedRegions) { usedRegionsNew.Add(sr); }
                                usedRegionsNew.Add(regionSi);
                                if (usedRegionsNew.Count >= maxRecursions)
                                    maxRecursions = usedRegionsNew.Count;
                                TraverseSpaceRequirements(Si, usedRegionsNew, usedRelNew, i + 1, newSumP, newSumRelP, newMinPropPerf, newMinRelPerf, newNotFullfilledRel);
                            }
                            //else
                            //{
                            //    Dictionary<Space, AssignedSpace> combination = new Dictionary<Space, AssignedSpace>();
                            //    for (int k = 0; k < usedRegions.Count; k++)
                            //        combination.Add(RequiredSpaces[k], usedRegions[k]);
                            //    double[] performances = new double[2];
                            //    performances[0] = summedPerformance;
                            //    performances[1] = summedRelationshipPerformance;
                            //    setsOfInvalidCombinations.Add(new KeyValuePair<Dictionary<Space, AssignedSpace>, double[]>(combination, performances));

                            //}
                        }
                    }
                    
            }
            else
            {                
                Dictionary<Space, AssignedSpace> combination = new Dictionary<Space, AssignedSpace>();
                for (int k = 0; k < usedRegions.Count; k++)
                    combination.Add(RequiredSpaces[k], usedRegions[k]);
                double[] performances = new double[5];
                performances[0] = summedPerformance;
                performances[1] = summedRelationshipPerformance;
                performances[2] = minPropPerf;
                performances[3] = minRelPerf;
                performances[4] = nrNotFulfilledRel;
                setsOfValidCombinations.Add(new KeyValuePair<Dictionary<Space, AssignedSpace>, double[]>(combination, performances));
            }

        }

        #endregion

        #region Additional Classes ............................................................................................

        [Serializable]
        public class Space
        {
            int m_type; //0=ConvexSpace, 1=PointCluster, 2=Fixed Point
            double m_minArea = 1; //only for pointClusters
            SpatialDescription m_reqGraph;
            Vector2d m_position; //nur wenn type==2
            public String Name { get; set; }
            List<PerformanceMembership> m_performanceRequirements = new List<PerformanceMembership>();
            List<Relationship> m_relationshipRequirements = new List<Relationship>();

            [NonSerialized]
            public List<AssignedSpace> PotentialRegions = new List<AssignedSpace>();

            public List<PerformanceMembership> PerformanceRequirements
            {
                get { return m_performanceRequirements; }
                set { m_performanceRequirements = value; }
            }

            public List<Relationship> RelationshipRequirements
            {
                get { return m_relationshipRequirements; }
            }

            public int Type { get { return m_type; } set { m_type = value; } }

            public double MinArea { get { return m_minArea; } set { m_minArea = value; } }

            public AssignedSpace BestFitRegion
            {
                get
                {
                    AssignedSpace region = null;
                    if (m_reqGraph.BestCombination != null)
                        if (m_reqGraph.BestCombination.TryGetValue(this, out region))
                            return region;

                    return region;
                }
            }

            public Vector2d Position
            {
                get { return m_position; }
            }

            //Constructor
            public Space(String _name, int _type, SpatialDescription _reqGraph)
            {
                Name = _name;
                m_reqGraph = _reqGraph;
                m_type = _type;
            }


            //Fixed Point Constructor
            public Space(String _name, Vector2d _pos, SpatialDescription _reqGraph)
            {
                Name = _name;
                m_reqGraph = _reqGraph;
                m_type = 2;
                m_position = _pos;
            }

        }

        [Serializable]
        public class AssignedSpace
        {
            int m_type; //0=ConvexSpace, 1=PointCluster, 2=Fixed Point
            VisibilityGraphAnalysis.ConvexSpace m_convexSpace;
            VisibilityGraphAnalysis.PointCluster m_pointCluster;
            //Vector2d m_fixedPoint;
            Space m_space;
            double m_performance;

            public double Performance
            {
                get { return m_performance; }
                //set { m_performance = value; }
            }

            public int Type { get { return m_type; } }

            public Space RequiredSpace { get { return m_space; } }

            public double GetAverageAnalysisMeasure(AnalysisMeasure measure)
            {
                double sum = 0;
                double cnt = 0;
                if (m_type == 0)
                {
                    List<VisibilityGraphAnalysis.AnalysisPoint> aPs = new List<VisibilityGraphAnalysis.AnalysisPoint>();
                    foreach (VisibilityGraphAnalysis.AnalysisPoint ap in m_convexSpace.AnalysisPoints)
                        sum += ap.Properties[measure];
                    cnt = m_convexSpace.AnalysisPoints.Count;
                }
                else if (m_type == 1)
                {
                    List<VisibilityGraphAnalysis.AnalysisPoint> aPs = new List<VisibilityGraphAnalysis.AnalysisPoint>();
                    foreach (VisibilityGraphAnalysis.AnalysisPoint ap in m_pointCluster.Points)
                        sum += ap.Properties[measure];
                    cnt = m_pointCluster.Points.Count;
                }



                return sum / cnt;
            }

            public VisibilityGraphAnalysis.ConvexSpace ConvexSpace { get { return m_convexSpace; } }

            //public Vector2d Point { get { return m_fixedPoint; } }

            public VisibilityGraphAnalysis.PointCluster PointCluster { get { return m_pointCluster; } }

            public Vector2d Center
            {
                get
                {
                    if (Type == 0)
                        return m_convexSpace.Center;
                    else if (Type == 1)
                        return m_pointCluster.Center;
                    else
                        return new Vector2d(double.MaxValue, double.MaxValue);
                }
            }
            
            public AssignedSpace(Space _reqSpace, VisibilityGraphAnalysis.ConvexSpace _convexSpace, double performance)
            {
                m_convexSpace = _convexSpace;
                m_space = _reqSpace;
                m_type = 0;
                m_performance = performance;
            }

            public AssignedSpace(Space _reqSpace, VisibilityGraphAnalysis.PointCluster _pointCluster, double performance)
            {
                m_pointCluster = _pointCluster;
                m_space = _reqSpace;
                m_type = 1;
                m_performance = performance;
            }

            //public AssignedSpace(Space _reqSpace, Vector2d _point, double performance)
            //{
            //    //m_fixedPoint = _point;
            //    m_space = _reqSpace;
            //    m_type = 2;
            //    m_performance = performance;
            //}


        }

        [Serializable]
        public class Relationship
        {
            public int Type { get; set; }  //0=physical Distance, 1=Intervisibility, 2=Overlap

            public double Min { get; set; }
            public double MinOK { get; set; }
            public double MaxOK { get; set; }
            public double Max { get; set; }

            public Space FromSpace;
            public Space ToSpace;

            public String Name
            { get { return FromSpace.Name + " - " + ToSpace.Name; } }

            public Relationship(Space from, Space to, int type, bool connectDisconnect)
            {
                Type = type;
                FromSpace = from;
                ToSpace = to;

                from.RelationshipRequirements.Add(this);
                to.RelationshipRequirements.Add(this);

                if (connectDisconnect)
                {
                    Min = 1;
                    MinOK = 1;
                    MaxOK = 1;
                    Max = 1;
                }
                else
                {

                }
            }

            public Relationship(Space from, Space to, int type, double min, double minOK, double maxOK, double max)
            {
                Type = type;
                FromSpace = from;
                ToSpace = to;

                from.RelationshipRequirements.Add(this);
                to.RelationshipRequirements.Add(this);

                Min = min;
                MinOK = minOK;
                MaxOK = maxOK;
                Max = max;
            }


            public double GetMembershipDegree(double val)
            {
                int k = 0;
                if (val >= 0.122 && val <= 0.124)
                    k = 2;


                double mDegree = 0;
                if (val >= MinOK && val <= MaxOK)
                    mDegree = 1;

                if (val < MinOK)
                    mDegree = Map(val, Min, MinOK, 0.1, 1);

                if (val > MaxOK)
                    mDegree = Map(val, MaxOK, Max, 1, 0.1);

                if (val < Min)
                    mDegree = Map(val, 0, Min, 0, 0.1);

                if (val > Max)
                    mDegree = Map(val, Max, Max*2, 0.1, 0);

                return mDegree;
            }


            private double Map(double val, double minOrigin, double maxOrigin, double minTarget, double maxTarget)
            {

                if (val < minOrigin)
                    return minTarget;
                else if (val > maxOrigin)
                    return maxTarget;

                if (minTarget < maxTarget)
                    return minTarget + (maxTarget - minTarget) * (val - minOrigin) / (maxOrigin - minOrigin);
                else
                    return minTarget - (minTarget - maxTarget) * (val - minOrigin) / (maxOrigin - minOrigin);

            }
        }

        [Serializable]
        public class PerformanceMembership
        {
            public String Name { get; set; }
            public double Min { get; set; }
            public double MinOK { get; set; }
            public double MaxOK { get; set; }
            public double Max { get; set; }
            public double Value { get; set; }

            public PerformanceMembership(String name, double min, double minOK, double maxOK, double max)
            {
                Name = name;
                Min = min;
                MinOK = minOK;
                MaxOK = maxOK;
                Max = max;
            }

            public double GetMembershipDegreeSig(double val, double slope, double slope2)
            {

                //Sigmoidal:
                double mDegree = Sig(val, slope, (Min+MinOK)/2) - Sig(val, slope2, (Max+MaxOK)/2);


                return mDegree;
            }

            public double GetMembershipDegree(double val)
            {
                //Trapezoid:
                double mDegree = 0;
                if (val >= MinOK && val <= MaxOK)
                    mDegree = 1;

                if (val < MinOK)
                    mDegree = Map(val, Min, MinOK, 0.1, 1);

                if (val > MaxOK)
                    mDegree = Map(val, MaxOK, Max, 1, 0.1);


                if (val < Min)
                    mDegree = Map(val, 0, Min, 0, 0.1);

                if (val > Max)
                    mDegree = Map(val, Max, Max * 2, 0.1, 0);

                //Sigmoidal:
                //mDegree = Sig(val, (MinOK + MaxOK) / 2, Min) - Sig(val, (MinOK + MaxOK) / 2, Max);

                
                return mDegree;
            }

            double Sig(double x, double a, double c)
            {
                return (1 / (1 + Math.Pow(Math.E, -(a*x - a*c))));
            }


            private double Map(double val, double minOrigin, double maxOrigin, double minTarget, double maxTarget)
            {
                if (val < minOrigin)
                    return minTarget;
                else if (val > maxOrigin)
                    return maxTarget;

                if (minTarget < maxTarget)
                    return minTarget + (maxTarget - minTarget) * (val - minOrigin) / (maxOrigin - minOrigin);
                else
                    return minTarget - (minTarget - maxTarget) * (val - minOrigin) / (maxOrigin - minOrigin);

            }

        }

        #endregion

    }
}

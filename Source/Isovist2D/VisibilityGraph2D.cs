﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = VisibilityGraph2D.cs 
//  Copyright (C) 2014/10/18  12:59 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Sven Schneider, Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK;
using CPlan.Geometry;

namespace CPlan.Isovist2D
{
    [Serializable]
    public class VisibilityGraph2D
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Attributes

        HashSet<Node2D> m_nodes;
        HashSet<Edge2D> m_edges;
        List<Line2D> m_obstacleLines;

        #endregion
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        public HashSet<Node2D> Nodes
        {
            get { return m_nodes; }
            set { m_nodes = value; }
        }

        public HashSet<Edge2D> Edges
        {
            get { return m_edges; }
            set { m_edges = value; }
        }

        public List<Line2D> ObstacleLines
        {
            get { return m_obstacleLines; }
            set { m_obstacleLines = value;}
        }



        #endregion
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        public VisibilityGraph2D(List<Vector2d> points, List<Line2D> lines)
        {
            //create Graph:
            HashSet<Node2D> tmpNodes = new HashSet<Node2D>();
            foreach (Vector2d vec in points)
                tmpNodes.Add(new Node2D(vec, null));

            m_edges = new HashSet<Edge2D>();
            m_nodes = new HashSet<Node2D>();
            m_obstacleLines = lines;

            BuildGraph(tmpNodes);
        }

        public VisibilityGraph2D(List<Vector2d> points, List<Line2D> lines, List<Line2D> edges)
        {
            //create Graph:
            HashSet<Node2D> tmpNodes = new HashSet<Node2D>();
            foreach (Vector2d vec in points)
                tmpNodes.Add(new Node2D(vec, null));

            m_edges = new HashSet<Edge2D>();
            m_nodes = new HashSet<Node2D>();
            m_obstacleLines = lines;

            BuildGraph(tmpNodes, edges);
        }

        #endregion
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        public void BuildGraph(HashSet<Node2D> tmpNodes)
        {

            while (tmpNodes.Count != 0)
            {
                Node2D curNode = tmpNodes.ElementAt<Node2D>(0);

                foreach(Node2D elem in tmpNodes)
                {
                    if (elem == curNode)
                        continue;

                    bool isVisible = true;
                    foreach (Line2D line in m_obstacleLines)
                    {
                        Vector2d intersectionPoint = new Vector2d();
                        if (GeoAlgorithms2D.LineIntersection(line.Start, line.End, curNode.Position, elem.Position, ref intersectionPoint) == 2)//IntersectionType.Intersects)
                        {
                            isVisible = false;
                            break;
                        }
                    }
                    if (isVisible)
                    {
                        curNode.Neighbors.Add(elem);
                        elem.Neighbors.Add(curNode);
                        m_edges.Add(new Edge2D(curNode, elem));
                    }
                }
                m_nodes.Add(curNode);
                tmpNodes.Remove(curNode);
            }
        }

        public void BuildGraph()
        {
            List<Node2D> tmpNodes = new List<Node2D>();
            Vector2d temp = new Vector2d();
            foreach (Node2D node in m_nodes)
                tmpNodes.Add(new Node2D(node.Position, null));

            m_nodes.Clear();
            m_edges.Clear();

            while (tmpNodes.Count != 0)
            {
                Node2D curNode = tmpNodes[0];

                for (int i = 1; i < tmpNodes.Count; i++)
                {
                    bool isVisible = true;
                    foreach (Line2D line in m_obstacleLines)
                    {
                        if (GeoAlgorithms2D.LineIntersection(line.Start, line.End, curNode.Position, tmpNodes[i].Position, ref temp) != null)
                        {
                            isVisible = false;
                            break;
                        }
                    }
                    if (isVisible)
                    {
                        curNode.Neighbors.Add(tmpNodes[i]);
                        tmpNodes[i].Neighbors.Add(curNode);
                        m_edges.Add(new Edge2D(curNode, tmpNodes[i]));
                    }
                }
                m_nodes.Add(curNode);
                tmpNodes.Remove(curNode);
            }
        }

        public void BuildGraph(HashSet<Node2D> tmpNodes, List<Line2D> edges)
        {
            m_nodes.Clear();
            m_edges.Clear();

            for (int i = 0; i < edges.Count; i++)
            {
                foreach (Node2D node in tmpNodes)
                {
                    bool startIsNode = edges[i].Start.IsSimilarTo(node.Position, 0.001);
                    bool endIsNode = edges[i].End.IsSimilarTo(node.Position, 0.001);

                    if (startIsNode)
                    {
                        //find node with end-position
                        Node2D node2 = null;
                        foreach (Node2D tmpNode in tmpNodes)
                        {
                            if (edges[i].End.IsSimilarTo(tmpNode.Position, 0.001))
                            {
                                node2 = tmpNode;
                                break;
                            }
                        }

                        if (node2 != null)
                        {
                            m_edges.Add(new Edge2D(node, node2));
                            node.Neighbors.Add(node2);
                            node2.Neighbors.Add(node);
                        }
                        
                        break;
                    }
                    if (endIsNode)
                    {
                        //find node with Start-position
                        Node2D node2 = null;
                        foreach (Node2D tmpNode in tmpNodes)
                        {
                            if (edges[i].Start.IsSimilarTo(tmpNode.Position, 0.001))
                            {
                                node2 = tmpNode;
                                break;
                            }
                        }

                        if (node2 != null)
                        {
                            m_edges.Add(new Edge2D(node2, node));
                            node.Neighbors.Add(node2);
                            node2.Neighbors.Add(node);
                        }
                        
                        break;
                    }                    
                }
            }
            foreach (Node2D n in tmpNodes)
            {
                m_nodes.Add(n);

                /*
                foreach (Edge2D e in m_edges)
                {
                    if (n.Position == e.Node1.Position && !m_nodes.Contains(e.Node1))
                    {
                        m_nodes.Add(e.Node1);
                        break;
                    }
                    if (n.Position == e.Node2.Position && !m_nodes.Contains(e.Node2))
                    {
                        m_nodes.Add(e.Node2);
                        break;
                    }
                }*/
            }
        }

        private Node2D GetNodeFromPosition(Vector2d pos)
        {
            foreach (Node2D node in m_nodes)
            {
                if (node.Position.IsSimilarTo(pos, 0.001))
                    return node;
            }
            return null;
        }

        public void CalculateTotalDepth()
        {
            int count = 0;
            foreach (Node2D curNode in m_nodes)
            {
                //create List of all expept first view
                HashSet<Node2D> tmpNodes = new HashSet<Node2D>();
                HashSet<Node2D> curStepDepthNodes = new HashSet<Node2D>();
                foreach (Node2D n in m_nodes)
                {
                    if (!curNode.Neighbors.Contains(n) && n != curNode)
                        tmpNodes.Add(n);
                    else if (n != curNode)
                        curStepDepthNodes.Add(n);
                    count++;
                }
                curNode.NeighborsByStepDepth.Add(curStepDepthNodes);

                while (tmpNodes.Count != 0)
                {
                    curStepDepthNodes = NextStepDepth(curNode, curStepDepthNodes, ref tmpNodes, ref count);
                    if (curStepDepthNodes.Count > 0)
                        curNode.NeighborsByStepDepth.Add(curStepDepthNodes);
                    else
                        break;
                }
            }
            int final = count;
            //Calculate Centrality From StepDepthCountings...
            foreach (Node2D curNode in m_nodes)
            {
                for (int i = 0; i < curNode.NeighborsByStepDepth.Count; i++)
                {
                    curNode.Closeness += curNode.NeighborsByStepDepth[i].Count * (i + 1);
                }
            }
        }

        HashSet<Node2D> NextStepDepth(Node2D curNode, HashSet<Node2D> curStepDepthNodes, ref HashSet<Node2D> tmpNodes, ref int count)
        {
            HashSet<Node2D> nextStepDepthNodes = new HashSet<Node2D>();
            HashSet<Node2D> tmpNodesCopy = new HashSet<Node2D>();

            foreach (Node2D elem in tmpNodes)
            {
                bool found = false;
                foreach (Node2D node in curStepDepthNodes)
                {
                    if (node.Neighbors.Contains(elem))
                    {
                        nextStepDepthNodes.Add(elem);
                        count++;
                        found = true;
                        break;
                    }
                }

                if (!found)
                    tmpNodesCopy.Add(elem);
            }

            tmpNodes = tmpNodesCopy;

            return nextStepDepthNodes;
        }

        public Vector2d GetMinMaxCloseness()
        {

            double max = double.MinValue;
            double min = double.MaxValue;
            foreach (Node2D node in m_nodes)
            {
                if (node.Closeness > max)
                    max = node.Closeness;
                if (node.Closeness < min)
                    min = node.Closeness;
            }
            return new Vector2d(min, max);
            
        }

        #endregion

    }

    [Serializable]
    public class Node2D
    {
        Vector2d m_pos;
        HashSet<Node2D> m_neighbors;
        List<HashSet<Node2D>> m_neighborsByStepDepth = new List<HashSet<Node2D>>();
        double m_closeness;

        public Vector2d Position
        {
            get { return m_pos; }
            set { m_pos = value; }
        }

        public HashSet<Node2D> Neighbors
        {
            get { return m_neighbors; }
            set { m_neighbors = value; }
        }

        public List<HashSet<Node2D>> NeighborsByStepDepth
        {
            get { return m_neighborsByStepDepth; }
            set { m_neighborsByStepDepth = value; }
        }

        public int Connectivity
        {
            get { return m_neighbors.Count; }
        }

        public double Closeness
        {
            get { return m_closeness; }
            set { m_closeness = value; }
        }


        public Node2D(Vector2d pos, HashSet<Node2D> neighbors)
        {
            m_pos = pos;
            
            if (neighbors != null)
                m_neighbors = neighbors;
            else
                m_neighbors = new HashSet<Node2D>();
        }

        public Node2D(Node2D old)
        {
            m_pos = old.m_pos;
            m_neighbors = old.m_neighbors;
        }

    }

    [Serializable]
    public class Edge2D
    {
        Node2D m_node1;
        Node2D m_node2;


        public Node2D Node1
        {
            get { return m_node1; }
            set { m_node1 = value; }
        }
        public Node2D Node2
        {
            get { return m_node2; }
            set { m_node2 = value; }
        }

        public Edge2D(Node2D node1, Node2D node2)
        {
            m_node1 = node1;
            m_node2 = node2;                       
        }

    }




}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Isovist3d_GPU;
using System.Collections;

namespace Isovist3dCalculation
{
    [Serializable]
    public class Isovist3dCalculation
    {
        struct PolyInfo
        {
            public CGeom3d.Vec_3d[][] mPoints;
            public CGeom3d.Plane_3d mPlane;

            public PolyInfo(CGeom3d.Vec_3d[][] points, CGeom3d.Plane_3d plane)
            {
                mPoints = points;
                mPlane = plane;
            }
        }

        [NonSerialized]
        static CIsovist3d mIsovistSingleton = null;
        [NonSerialized]
        static int mIsovistResolution; // muss vielfaches von 64 sein
        [NonSerialized]
        static Dictionary<uint, PolyInfo> mLookup;

        CGeom3d.Vec_3d mPoint;
        Dictionary<int, CGeom3d.Vec_3d[][]> mIdPolys;
        float[] mDepthResult;
        float[] mDepthMask;
        double mVolume;
        double mSurfaceArea;

        double mSurfaceAreaAir;
        double mSurfaceAreaWalls;

        public Isovist3dCalculation(int resolution)
        {
            mIsovistResolution = (int)Math.Ceiling(resolution / 64.0) * 64;
            mDepthResult = null;
            GetCalcInstance();
        }

        public CGeom3d.Vec_3d IsovistCenter
        {
            get { return mPoint; }
            set { mPoint = value; }
        }

        public int IsovistResolution
        {
            get { return mIsovistResolution; }
            set { mIsovistResolution = (int)Math.Ceiling(value / 64.0) * 64; mDepthResult = null; }
        }

        public double Volume
        {
            get { return mVolume; }
        }

        public double SurfaceArea
        {
            get { return mSurfaceArea; }
        }

        public double SurfaceAreaAir
        {
            get { return mSurfaceAreaAir; }
        }

        public double SurfaceAreaWalls
        {
            get { return mSurfaceAreaWalls; }
        }

        public void ClearGeometry()
        {
            CIsovist3d mCalc = GetCalcInstance();
            mCalc.DeleteAllGeometry();
            mLookup.Clear();
        }

        public uint AddPolygon(CGeom3d.Vec_3d[][] pvv)
        {
            CIsovist3d mCalc = GetCalcInstance();
            uint id = (uint)mCalc.AddPolygon(pvv);
            CGeom3d.Vec_3d center = new CGeom3d.Vec_3d(), normale = new CGeom3d.Vec_3d();
            CGeom3d.ComputePlane(pvv, center, normale);
            CGeom3d.Plane_3d plane = new CGeom3d.Plane_3d();
            CGeom3d.Plane_PN_3d(center, normale, plane);
            mLookup.Add(id, new PolyInfo(pvv, plane));
            return id;
        }

        public static CIsovist3d GetCalcInstance()
        {
            if (mIsovistSingleton == null)
            {
                mIsovistSingleton = new CIsovist3d();
                mIsovistSingleton.SetResolution(mIsovistResolution);
                mLookup = new Dictionary<uint, PolyInfo>();
            }
            return mIsovistSingleton;
        }

        public void CalculateDepth()
        {
            CIsovist3d mCalc = GetCalcInstance();

            mCalc.CalculateIsovist3d(mPoint);

            if (mDepthResult == null)
                mDepthResult = new float[mIsovistResolution * mIsovistResolution * 6];

            for (int y = 0; y < mIsovistResolution; ++y)
                for (int x = 0; x < mIsovistResolution * 6; ++x)
                {
                    uint id = (uint)mCalc.GetFBOPixelId(x, y);
                    float depth = 0;

                    if (id != 0)
                    {
                        CGeom3d.Vec_3d ray = mCalc.GetFBOPixelRay(x, y);
                        CGeom3d.Plane_3d plane = mLookup[id].mPlane;
                        CGeom3d.Line_3d line = new CGeom3d.Line_3d();
                        CGeom3d.Line_VP_3d(ray, mPoint, line);

                        CGeom3d.Vec_3d cutPoint = new CGeom3d.Vec_3d(); ;
                        CGeom3d.Point_LS_3d(line, plane, cutPoint);
                        depth = (float)(mPoint - cutPoint).Value();
                    }

                    mDepthResult[y * mIsovistResolution * 6 + x] = depth;
                }
        }

        public float GetDepth(int x, int y)
        {
            int pos = y * mIsovistResolution * 6 + x;

            if (mDepthMask != null)
                return mDepthResult[pos] * mDepthMask[pos];
            else
                return mDepthResult[pos];
        }

        public CGeom3d.Vec_3d GetRay(int x, int y)
        {
            CIsovist3d mCalc = GetCalcInstance();
            return mCalc.GetFBOPixelRay(x, y);
        }

        public enum Direction
        {
            eTop, eBottom, eLeft, eRight, eUndefined
        }

        public struct SideDir
        {
            public int mSide;
            public Direction mDirection;
            public bool mMirror;

            public SideDir(int side, Direction dir)
            {
                mSide = side;
                mDirection = dir;
                mMirror = false;
            }
            public SideDir(int side, Direction dir, bool mirror)
            {
                mSide = side;
                mDirection = dir;
                mMirror = mirror;
            }
        }

        Dictionary<SideDir, SideDir> mSideSide;

        private void MoveTexelPos(ref int x, ref int y, int dx, int dy)
        {
            int orgX = x;
            int orgY = y;
            int orgSide = x / mIsovistResolution;
            int newX = x + dx;
            int newY = y + dy;
            int newSide = newX / mIsovistResolution;

            bool sideChanged = (newSide != orgSide) || (newY < 0) || (newY >= mIsovistResolution);

            if (!sideChanged)
            {
                x = newX;
                y = newY;
            }
            else
            {
                if (mSideSide == null)
                {
                    mSideSide = new Dictionary<SideDir, SideDir>();
                    mSideSide.Add(new SideDir(0, Direction.eRight), new SideDir(3, Direction.eLeft, false));
                    mSideSide.Add(new SideDir(0, Direction.eTop), new SideDir(5, Direction.eBottom, false));
                    mSideSide.Add(new SideDir(0, Direction.eBottom), new SideDir(4, Direction.eTop, false));
                    mSideSide.Add(new SideDir(1, Direction.eRight), new SideDir(0, Direction.eLeft, false));
                    mSideSide.Add(new SideDir(1, Direction.eTop), new SideDir(5, Direction.eLeft, false));
                    mSideSide.Add(new SideDir(1, Direction.eBottom), new SideDir(4, Direction.eLeft, true));
                    mSideSide.Add(new SideDir(2, Direction.eRight), new SideDir(1, Direction.eLeft, false));
                    mSideSide.Add(new SideDir(2, Direction.eTop), new SideDir(5, Direction.eTop, true));
                    mSideSide.Add(new SideDir(2, Direction.eBottom), new SideDir(4, Direction.eBottom, true));
                    mSideSide.Add(new SideDir(3, Direction.eRight), new SideDir(2, Direction.eLeft, false));
                    mSideSide.Add(new SideDir(3, Direction.eTop), new SideDir(5, Direction.eRight, true));
                    mSideSide.Add(new SideDir(3, Direction.eBottom), new SideDir(4, Direction.eRight, false));
                }

                //Seite identifizieren
                Direction moveDirection = Direction.eUndefined;
                if (newSide < orgSide)
                    moveDirection = Direction.eLeft;
                else if (newSide > orgSide)
                    moveDirection = Direction.eRight;
                else if (newY < 0)
                    moveDirection = Direction.eTop;
                else if (newY >= mIsovistResolution)
                    moveDirection = Direction.eBottom;
                else
                    throw new NotImplementedException();

                //Anschlussseite finden
                SideDir moveToSide = new SideDir(-1, Direction.eUndefined);
                bool found = false;
                foreach (KeyValuePair<SideDir, SideDir> pair in mSideSide)
                    if ((pair.Key.mSide == orgSide) && (pair.Key.mDirection == moveDirection))
                    {
                        moveToSide = pair.Value;
                        found = true;
                        break;
                    }

                if (!found)
                    return;

                //Koordinaten entsprechend verschieben
                int over = 0, same = 0;
                switch (moveDirection)
                {
                    case Direction.eRight:
                        over = newX % mIsovistResolution;
                        same = newY;
                        break;
                    case Direction.eLeft:
                        over = mIsovistResolution - (newX % mIsovistResolution) - 1;
                        same = newY;
                        break;
                    case Direction.eTop:
                        over = -newY - 1;
                        same = newX % mIsovistResolution;
                        break;
                    case Direction.eBottom:
                        over = newY % mIsovistResolution;
                        same = newX % mIsovistResolution;
                        break;
                    default:
                        throw new NotImplementedException();
                }

                int newPartX = 0, newPartY = 0;
                switch (moveToSide.mDirection)
                {
                    case Direction.eRight:
                        newPartX = mIsovistResolution - over - 1;
                        newPartY = !moveToSide.mMirror ? same : (mIsovistResolution - same - 1);
                        break;
                    case Direction.eLeft:
                        newPartX = over;
                        newPartY = !moveToSide.mMirror ? same : (mIsovistResolution - same - 1);
                        break;
                    case Direction.eTop:
                        newPartX = !moveToSide.mMirror ? same : (mIsovistResolution - same - 1);
                        newPartY = over;
                        break;
                    case Direction.eBottom:
                        newPartX = !moveToSide.mMirror ? same : (mIsovistResolution - same - 1);
                        newPartY = mIsovistResolution - over - 1;
                        break;
                    default:
                        throw new NotImplementedException();
                }

                x = newPartX + mIsovistResolution * moveToSide.mSide;
                y = newPartY;

                if (y < 0)
                    y = 0;
                if (y >= mIsovistResolution)
                    y = mIsovistResolution - 1;
                if (x < 0)
                    x = 0;
                if (x >= 6 * mIsovistResolution)
                    x = 6 * mIsovistResolution - 1;
            }
        }

        public struct Triangles
        {
            public List<CGeom3d.Vec_3d> mP1, mP2, mP3;
        }

        private void AddTriangle(CGeom3d.Vec_3d[] pv, Dictionary<CGeom3d.Plane_3d, Triangles> planeTriangles, CGeom3d.Vec_3d isovistCenter)
        {
            CGeom3d.Plane_3d plane = new CGeom3d.Plane_3d();
            CGeom3d.Vec_3d v1 = isovistCenter - pv[0];
            CGeom3d.Vec_3d v2 = pv[1] - pv[0];
            CGeom3d.Vec_3d v3 = pv[2] - pv[0];
            CGeom3d.Vec_3d n = (v2).amul(v3);
            if (n.Normalize())
            {
                CGeom3d.Plane_PN_3d(pv[0], n, plane);
                bool found = false;
                Triangles pTri = new Triangles();
                foreach (KeyValuePair<CGeom3d.Plane_3d, Triangles> pair in planeTriangles)
                {
                    if ((Math.Abs(CGeom3d.Vec_3d.cos(pair.Key.GetNormale(), plane.GetNormale())) > 0.999) && (CGeom3d.Dist_SS_3d(pair.Key, plane) < 0.0001))
                    {
                        found = true;
                        pTri = pair.Value;
                        break;
                    }
                }
                if (!found)
                {
                    pTri = new Triangles();
                    pTri.mP1 = new List<CGeom3d.Vec_3d>();
                    pTri.mP2 = new List<CGeom3d.Vec_3d>();
                    pTri.mP3 = new List<CGeom3d.Vec_3d>();
                    planeTriangles.Add(plane, pTri);
                }
                pTri.mP1.Add(pv[0]);
                pTri.mP2.Add(pv[1]);
                pTri.mP3.Add(pv[2]);
            }

            //Visualisation.UserDraw.DrawTriangle(gui, pv[0], pv[1], pv[2], colR, colG, colB, /*(float)gui.Transparency*/1.0f, 0.0f);

            CGeom3d.Vec_3d norm = (v2).amul(v3);
            double surfaceArea = norm.Value() / 2;
            double volume = (norm * v1).Value() / 6.0;

            mSurfaceArea += surfaceArea;

            if (((pv[0] - isovistCenter).Value2() < 0.000001) ||
                ((pv[1] - isovistCenter).Value2() < 0.000001) ||
                ((pv[2] - isovistCenter).Value2() < 0.000001))
                mSurfaceAreaAir += surfaceArea;
            else
            {
                mVolume += volume;
                mSurfaceAreaWalls += surfaceArea;
            }
        }

        public Dictionary<CGeom3d.Plane_3d, Triangles> GetPlaneSortedTriangleMesh()
        {
            CIsovist3d mCalc = GetCalcInstance();
            CGeom3d.Vec_3d isovistCenter = mPoint;
            Dictionary<CGeom3d.Plane_3d, Triangles> planeTriangles = new Dictionary<CGeom3d.Plane_3d, Triangles>();
            mVolume = 0;
            mSurfaceArea = 0;
            mSurfaceAreaAir = 0;
            mSurfaceAreaWalls = 0;

            if (mDepthResult != null)
            {
                CGeom3d.Vec_3d[] points = new CGeom3d.Vec_3d[mDepthResult.GetLength(0)];
                for (int y = 0; y < mIsovistResolution; ++y)
                    for (int x = 0; x < mIsovistResolution * 6; ++x)
                    {
                        float depth = GetDepth(x,y);
                        CGeom3d.Vec_3d ray = new CGeom3d.Vec_3d(0, 0, 0);
                        if (depth > 0)
                            ray = mCalc.GetFBOPixelRay(x, y) * depth;
                        points[y * mIsovistResolution * 6 + x] = ray;
                    }
                for (int iy = -1; iy < mIsovistResolution; ++iy)
                {
                    for (int ix = 0; ix < mIsovistResolution * 6; ++ix)
                    {
                        int x11 = ix;
                        int y11 = iy;
                        int x12 = ix;
                        int y12 = iy;
                        int x21 = ix;
                        int y21 = iy;
                        int x22 = ix;
                        int y22 = iy;

                        int side = ix / mIsovistResolution;

                        if (iy == -1)
                        {
                            if (side < 4)
                            {
                                y11 = y12 = y21 = y22 = 0;
                                MoveTexelPos(ref x12, ref y12, 0, -1);
                                MoveTexelPos(ref x21, ref y21, 1, 0);
                                MoveTexelPos(ref x22, ref y22, 1, -1);
                            }
                            else
                                continue;
                        }
                        else
                        {
                            if (side < 4)
                            {
                                MoveTexelPos(ref x12, ref y12, 0, 1);
                                MoveTexelPos(ref x21, ref y21, 1, 0);
                                MoveTexelPos(ref x22, ref y22, 1, 1);
                            }
                            else
                            {
                                if (((x11 + 1) / mIsovistResolution != side) || (y11 + 1 >= mIsovistResolution))
                                    continue;

                                MoveTexelPos(ref x12, ref y12, 0, 1);
                                MoveTexelPos(ref x21, ref y21, 1, 0);
                                MoveTexelPos(ref x22, ref y22, 1, 1);
                            }
                        }

                        bool valid_11 = (GetDepth(x11, y11) > 0);
                        bool valid_21 = (GetDepth(x21, y21) > 0);
                        bool valid_12 = (GetDepth(x12, y12) > 0);
                        bool valid_22 = (GetDepth(x22, y22) > 0);

                        int tri1 = 0;
                        if (valid_11) tri1 += 1;
                        if (valid_21) tri1 += 1;
                        if (valid_12) tri1 += 1;
                        int tri2 = 0;
                        if (valid_21) tri2 += 1;
                        if (valid_22) tri2 += 1;
                        if (valid_12) tri2 += 1;
                        int tri3 = 0;
                        if (valid_11) tri3 += 1;
                        if (valid_22) tri3 += 1;
                        if (valid_12) tri3 += 1;
                        int tri4 = 0;
                        if (valid_21) tri4 += 1;
                        if (valid_22) tri4 += 1;
                        if (valid_11) tri4 += 1;

                        bool valid_tri1 = tri1 > 1;
                        bool valid_tri2 = tri2 > 1;
                        bool valid_tri3 = tri3 > 1;
                        bool valid_tri4 = tri4 > 1;

                        //check which diagonal is better
                        if (Math.Max(tri1, tri2) >= Math.Max(tri3, tri4))
                            valid_tri3 = valid_tri4 = false;
                        else
                            valid_tri1 = valid_tri2 = false;


                        if (valid_tri1)
                        {
                            CGeom3d.Vec_3d[] pv = new CGeom3d.Vec_3d[3];
                            pv[0] = isovistCenter + points[y11 * mIsovistResolution * 6 + x11];
                            pv[1] = isovistCenter + points[y21 * mIsovistResolution * 6 + x21];
                            pv[2] = isovistCenter + points[y12 * mIsovistResolution * 6 + x12];

                            AddTriangle(pv, planeTriangles, isovistCenter);
                        }

                        if ((valid_tri2) && ((x22 != x12) || (y22 != y12)) && ((y22 != y21) || (x22 != y21))) // is it a box corner --> skip
                        {
                            CGeom3d.Vec_3d[] pv = new CGeom3d.Vec_3d[3];
                            pv[0] = isovistCenter + points[y21 * mIsovistResolution * 6 + x21];
                            pv[1] = isovistCenter + points[y22 * mIsovistResolution * 6 + x22];
                            pv[2] = isovistCenter + points[y12 * mIsovistResolution * 6 + x12];

                            AddTriangle(pv, planeTriangles, isovistCenter);
                        }

                        if (valid_tri3)
                        {
                            CGeom3d.Vec_3d[] pv = new CGeom3d.Vec_3d[3];
                            pv[0] = isovistCenter + points[y11 * mIsovistResolution * 6 + x11];
                            pv[1] = isovistCenter + points[y22 * mIsovistResolution * 6 + x22];
                            pv[2] = isovistCenter + points[y12 * mIsovistResolution * 6 + x12];

                            AddTriangle(pv, planeTriangles, isovistCenter);
                        }

                        if ((valid_tri4) && ((x22 != x12) || (y22 != y12)) && ((y22 != y21) || (x22 != y21))) // is it a box corner --> skip
                        {
                            CGeom3d.Vec_3d[] pv = new CGeom3d.Vec_3d[3];
                            pv[0] = isovistCenter + points[y21 * mIsovistResolution * 6 + x21];
                            pv[1] = isovistCenter + points[y22 * mIsovistResolution * 6 + x22];
                            pv[2] = isovistCenter + points[y11 * mIsovistResolution * 6 + x11];

                            AddTriangle(pv, planeTriangles, isovistCenter);
                        }

                        //debug
/*
                        if (valid_11 && valid_12 && valid_21 && valid_22)
                        {
                            CGeom3d.Vec_3d[] pv = new CGeom3d.Vec_3d[4];
                            pv[0] = isovistCenter + points[y21 * mIsovistResolution * 6 + x21];
                            pv[1] = isovistCenter + points[y22 * mIsovistResolution * 6 + x22];
                            pv[2] = isovistCenter + points[y12 * mIsovistResolution * 6 + x12];
                            pv[3] = isovistCenter + points[y11 * mIsovistResolution * 6 + x11];

                            Visualisation.UserDraw.DrawLineLoop(gui, pv);
                        }
    */
                    }
                }
            }
            return planeTriangles;
        }

        public bool SetDepthMask(float[] mask)
        {
            if (mask.Length != mDepthResult.Length)
                return false;

            mDepthMask = mask;
            return true;
        }

        public void ClearDepthMask()
        {
            mDepthMask = null;
        }

        private void MatrixMultiplication(double[] f1, double[] f2, double[] erg)
        {
           //f1, f2, erg sind alles double[16] arrays

           for (int i=0;i<4;i++)
           {
              int i2 = (i*4);
              for (int j=0;j<4;j++)
              {
                 erg[i2+j] = f1[j] * f2[i2] + f1[j+4] * f2[i2+1] + f1[j+8] * f2[i2+2] + f1[j+12] * f2[i2+3];
              }
           }
        }

        private void MatrixMultiplication(double[] f, CGeom3d.Vec_3d org, CGeom3d.Vec_3d erg)
        {
           //f ist double[16] array

           erg.x = (f[15] * (f[0] * org.x + f[1] * org.y + f[2]  * org.z + f[3]));
           erg.y = (f[15] * (f[4] * org.x + f[5] * org.y + f[6]  * org.z + f[7]));
           erg.z = (f[15] * (f[8] * org.x + f[9] * org.y + f[10] * org.z + f[11]));
        }

        public void SetViewportMask(CGeom3d.Vec_3d lookat, double openingAngleH, double openingAngleV)
        {
            CGeom3d.Plane_3d[] viewPlane = new CGeom3d.Plane_3d[4];
            for (int i = 0; i < 4; ++i)
                viewPlane[i] = new CGeom3d.Plane_3d();
            CGeom3d.Vec_3d center = new CGeom3d.Vec_3d(0, 0, 0);
            CGeom3d.Vec_3d v0 = new CGeom3d.Vec_3d(0, 0, -1);
            CGeom3d.Rot_X_3d(v0, Math.Sin(openingAngleV / 2.0), Math.Cos(openingAngleV / 2.0));
            CGeom3d.Vec_3d v1 = new CGeom3d.Vec_3d(0, 0, 1);
            CGeom3d.Rot_X_3d(v1, Math.Sin(-openingAngleV / 2.0), Math.Cos(-openingAngleV / 2.0));
            CGeom3d.Vec_3d v2 = new CGeom3d.Vec_3d(1, 0, 0);
            CGeom3d.Rot_Z_3d(v2, Math.Sin(openingAngleH / 2.0), Math.Cos(openingAngleH / 2.0));
            CGeom3d.Vec_3d v3 = new CGeom3d.Vec_3d(-1, 0, 0);
            CGeom3d.Rot_Z_3d(v3, Math.Sin(-openingAngleH / 2.0), Math.Cos(-openingAngleH / 2.0));

            CGeom3d.Plane_PN_3d(center, v0, viewPlane[0]);
            CGeom3d.Plane_PN_3d(center, v1, viewPlane[1]);
            CGeom3d.Plane_PN_3d(center, v2, viewPlane[2]);
            CGeom3d.Plane_PN_3d(center, v3, viewPlane[3]);

            double angleH = CGeom3d.GetXYAngle(lookat);
            double[] rz = new double[16];
            double cosz = Math.Cos(-angleH);
            double sinz = Math.Sin(-angleH);
            rz[0] = cosz;  rz[1] = -sinz;   rz[2] = 0.0;    rz[3] = 0.0;
            rz[4] = sinz;  rz[5] = cosz;    rz[6] = 0.0;    rz[7] = 0.0;
            rz[8] = 0.0;   rz[9] = 0.0;     rz[10] = 1.0;   rz[11] = 0.0;
            rz[12] = 0.0;  rz[13] = 0.0;    rz[14] = 0.0;   rz[15] = 1.0;

            CGeom3d.Vec_3d lookatXY = new CGeom3d.Vec_3d(lookat.x, lookat.y, 0);
            double angleV = Math.Acos(lookatXY.Value() / lookat.Value());
            if (lookat.z < 0)
                angleV *= -1;
            double[] rx = new double[16];
            double cosx = Math.Cos(-angleV);
            double sinx = Math.Sin(-angleV);
            rx[0] = 1.0;   rx[1] = 0.0;     rx[2] = 0.0;    rx[3] = 0.0;
            rx[4] = 0.0;   rx[5] = cosx;    rx[6] = -sinx;  rx[7] = 0.0;
            rx[8] = 0.0;   rx[9] = sinx;    rx[10] = cosx;  rx[11] = 0.0;
            rx[12] = 0.0;  rx[13] = 0.0;    rx[14] = 0.0;   rx[15] = 1.0;

            float[] mask = new float[mIsovistResolution * mIsovistResolution * 6];
            CGeom3d.Vec_3d tmp = new CGeom3d.Vec_3d();
            CGeom3d.Vec_3d erg = new CGeom3d.Vec_3d();
            for (int y = 0; y < mIsovistResolution; ++y)
                for (int x = 0; x < mIsovistResolution * 6; ++x)
                {
                    CGeom3d.Vec_3d ray = GetRay(x, y);
                    MatrixMultiplication(rz, ray, tmp);
                    MatrixMultiplication(rx, tmp, erg);

                    int planeSum = 0;
                    for (int i = 0; i < 4; ++i)
                        if (CGeom3d.Dist_PS_3d(erg, viewPlane[i]) > 0)
                            planeSum++;

                    bool valid = (planeSum == 4);

                    mask[y * mIsovistResolution * 6 + x] = valid ? 1.0f : 0.0f;
                }

            if (!SetDepthMask(mask))
                throw new NotImplementedException();
        }

        public class IsovistRay
        {
            public CGeom3d.Vec_3d mRayDirection;
            public uint mPolyId;
            public double mCosAngle;
            public double mDistance;

            public IsovistRay(CGeom3d.Vec_3d direction, uint polyId, double cosAngle, double distance)
            {
                mRayDirection = direction;
                mPolyId = polyId;
                mCosAngle = cosAngle;
                mDistance = distance;
            }
        }
        
        public List<IsovistRay> GetIsovistRays()
        {
            CIsovist3d mCalc = GetCalcInstance();
            CGeom3d.Vec_3d isovistCenter = mPoint;
            List<IsovistRay> rays = new List<IsovistRay>();
            mVolume = 0;
            mSurfaceArea = 0;

            if (mDepthResult != null)
            {
                for (int y = 0; y < mIsovistResolution; ++y)
                    for (int x = 0; x < mIsovistResolution * 6; ++x)
                    {
                        float depth = GetDepth(x, y);
                        uint polyId = (uint)mCalc.GetFBOPixelId(x, y);
                        if ((depth > 0) && (polyId != 0))
                        {
                            CGeom3d.Vec_3d ray = new CGeom3d.Vec_3d(0, 0, 0);
                            ray = mCalc.GetFBOPixelRay(x, y);
                            CGeom3d.Plane_3d plane = mLookup[polyId].mPlane;
                            double cosAngle = Math.Abs(CGeom3d.Vec_3d.cos(ray, plane.GetNormale()));
                            rays.Add(new IsovistRay(ray, polyId, cosAngle, depth));
                        }
                    }
            }

            return rays;
        }

        ////////////////////////////////////////////////////////// Analytical Part //////////////////////////////////////////////////////////////

        class IPoint
        {
            public CGeom3d.Vec_3d mPosition;
            public HashSet<IEdge> mEdges;

            public IPoint(CGeom3d.Vec_3d position)
            {
                mPosition = position;
                mEdges = new HashSet<IEdge>();
            }
        }

        class IPointCompare: IComparer<IPoint>
        {
            private double mEps;

            public IPointCompare(double eps)
            {
                mEps = eps;
            }
            public int Compare(IPoint a, IPoint b)
            {
                CGeom3d.Vec_3d va = a.mPosition;
                CGeom3d.Vec_3d vb = b.mPosition;
                if ((va.x - vb.x < -mEps) ||
			       ((Math.Abs(va.x - vb.x) <= mEps) && (va.y - vb.y < -mEps)) ||
                   ((Math.Abs(va.x - vb.x) <= mEps) && (Math.Abs(va.y - vb.y) <= mEps) && (va.z - vb.z < -mEps)))
			       return -1;
                if ((va.x - vb.x > mEps) ||
                   ((Math.Abs(va.x - vb.x) <= mEps) && (va.y - vb.y > mEps)) ||
                   ((Math.Abs(va.x - vb.x) <= mEps) && (Math.Abs(va.y - vb.y) <= mEps) && (va.z - vb.z > mEps)))
                    return 1;
		        return 0;
                //return a.mPosition.CompareTo(b.mPosition);
            }
        }
        
        enum IEdgeType
        {
            ePolygonBorder, eIntern, eCut
        }

        class IEdge
        {
            public IPoint mP1;
            public IPoint mP2;
            private IEdge mCutEdge;
            private HashSet<IEdge> mCuttingOtherEdges;
            public HashSet<ITriangle> mTriangles;
            public CGeom3d.Line_3d mLine;
            public IEdgeType mType;
            public IEdge mParentEdge;
            public HashSet<IEdge> mChildEdges;

            public IEdge(IPoint p1, IPoint p2, IEdge parentEdge)
            {
                mP1 = p1;
                mP2 = p2;
                mCutEdge = null;
                mTriangles = new HashSet<ITriangle>();
                mLine = new CGeom3d.Line_3d();
                //CGeom3d.Line_PP_3d(mP1.mPosition, mP2.mPosition, mLine);
                CGeom3d.Vec_3d v = mP2.mPosition - mP1.mPosition;
                v.Normalize();
                CGeom3d.Line_VP_3d(v, mP1.mPosition, mLine);

                mType = IEdgeType.eIntern;
                mCuttingOtherEdges = new HashSet<IEdge>();
                mParentEdge = parentEdge;
                mChildEdges = new HashSet<IEdge>();
                if (mParentEdge != null)
                    mParentEdge.mChildEdges.Add(this);
            }

            public IPoint GetOtherPoint(IPoint p)
            {
                if (p == mP1)
                    return mP2;
                else if (p == mP2)
                    return mP1;
                else
                    return null;
            }

            public bool GetIntersect(CGeom3d.Plane_3d plane, ref CGeom3d.Vec_3d cutPoint)
            {
                CGeom3d.Point_LS_3d(mLine, plane, cutPoint);
                if (CGeom3d.Err_3d == 1)
                    return CGeom3d.InLine_3d(mP1.mPosition, mP2.mPosition, cutPoint);
                else
                    return false;
            }

            public bool GetIntersectWithoutEndPoints(CGeom3d.Plane_3d plane, ref CGeom3d.Vec_3d cutPoint)
            {
                CGeom3d.Point_LS_3d(mLine, plane, cutPoint);
                if (CGeom3d.Err_3d == 1)
                    return CGeom3d.InLineWithoutEndPoints_3d(mP1.mPosition, mP2.mPosition, cutPoint);
                else
                    return false;
            }

            public bool Contains(IPoint p)
            {
                return ((p == mP1) || (p == mP2));
            }

            public IPoint GetCorner(IEdge edge)
            {
                if ((mP1 == edge.mP1) || (mP1 == edge.mP2))
                    return mP1;
                if ((mP2 == edge.mP1) || (mP2 == edge.mP2))
                    return mP2;
                throw new NotImplementedException();
            }

            public IEdge CutEdge
            {
                get { return mCutEdge; }
                set
                {
                    if (value != mCutEdge)
                    {
                        if (mCutEdge != null)
                            mCutEdge.mCuttingOtherEdges.Remove(this);
                        mCutEdge = value;
                        if (mCutEdge != null)
                            mCutEdge.mCuttingOtherEdges.Add(this);
                    }
                }
            }

            public bool DoPartsExist
            {
                get
                {
                    if (mTriangles.Count > 0)
                        return true;
                    foreach(IEdge child in mChildEdges)
                        if (child.DoPartsExist)
                            return true;
                    return false;
                }
            }

        }

        class ITriangle
        {
            public IEdge[] mEdges; // 3 pieces
            public uint mOrgId;
            private CGeom3d.Plane_3d mPlane;
            private CGeom3d.Vec_3d mTriMin;
            private CGeom3d.Vec_3d mTriMax;


            public ITriangle(IEdge e0, IEdge e1, IEdge e2, uint orgId, CGeom3d.Plane_3d plane)
            {
                if ((e0 == e1) || (e1 == e2) || (e2 == e0))
                    throw new NotImplementedException();

                mEdges = new IEdge[3];
                mEdges[0] = e0;
                mEdges[1] = e1;
                mEdges[2] = e2;
                mOrgId = orgId;
                mPlane = plane;

                IPoint p1 = P1;
                IPoint p2 = P2;
                IPoint p3 = P3;
                mTriMin = new CGeom3d.Vec_3d(Math.Min(p1.mPosition.x, Math.Min(p2.mPosition.x, p3.mPosition.x)),
                                             Math.Min(p1.mPosition.y, Math.Min(p2.mPosition.y, p3.mPosition.y)),
                                             Math.Min(p1.mPosition.z, Math.Min(p2.mPosition.z, p3.mPosition.z)));
                mTriMax = new CGeom3d.Vec_3d(Math.Max(p1.mPosition.x, Math.Max(p2.mPosition.x, p3.mPosition.x)),
                                             Math.Max(p1.mPosition.y, Math.Max(p2.mPosition.y, p3.mPosition.y)),
                                             Math.Max(p1.mPosition.z, Math.Max(p2.mPosition.z, p3.mPosition.z)));

                //if (Area / LineLength < 0.0001)
                //    throw new NotImplementedException();
            }

            public IPoint P1
            {
                get
                {
                    return mEdges[0].mP1;
                }
            }

            public IPoint P2
            {
                get
                {
                    return mEdges[0].mP2;
                }
            }

            public IPoint P3
            {
                get
                {
                    IPoint result = mEdges[1].GetOtherPoint(mEdges[0].mP2);
                    if (result == null)
                        result = mEdges[1].GetOtherPoint(mEdges[0].mP1);
                    return result;
                }
            }

            public CGeom3d.Vec_3d Normal
            {
                get
                {
                    CGeom3d.Vec_3d v0 = P2.mPosition - P1.mPosition;
                    CGeom3d.Vec_3d v1 = P3.mPosition - P1.mPosition;
                    CGeom3d.Vec_3d n = v0.amul(v1);
                    if (n.Normalize())
                        return n;
                    else
                        return mPlane.GetNormale();
                }
            }

            public CGeom3d.Plane_3d Plane
            {
                get
                {
                    CGeom3d.Plane_3d plane = new CGeom3d.Plane_3d();
                    CGeom3d.Plane_PN_3d(P1.mPosition, Normal, plane);
                    return plane;
                }
            }

            public CGeom3d.Plane_3d OrgPlane
            {
                get { return mPlane; }
            }

            public IPoint GetOtherPoint(IPoint t1, IPoint t2)
            {
                if (t1 == t2)
                    throw new NotImplementedException();

                IPoint p1 = P1;
                IPoint p2 = P2;
                IPoint p3 = P3;

                if (((t1 == p1) && (t2 == p2)) || ((t1 == p2) && (t2 == p1)))
                    return p3;
                if (((t1 == p2) && (t2 == p3)) || ((t1 == p3) && (t2 == p2)))
                    return p1;
                if (((t1 == p3) && (t2 == p1)) || ((t1 == p1) && (t2 == p3)))
                    return p2;

                throw new NotImplementedException();
            }

            public IEdge GetOtherEdge(IPoint corner, IEdge edge)
            {
                for (int i = 0; i < 3; ++i)
                    if (mEdges[i].Contains(corner) && (mEdges[i] != edge))
                        return mEdges[i];
                throw new NotImplementedException();
            }

            public IEdge GetOtherEdge(IEdge edge1, IEdge edge2)
            {
                for (int i = 0; i < 3; ++i)
                    if ((mEdges[i] != edge1) && (mEdges[i] != edge2))
                        return mEdges[i];
                throw new NotImplementedException();
            }

            public bool IsInTriangleBoundingBox(CGeom3d.Vec_3d p, double eps)
            {
                return (mTriMin.x - p.x < eps) && (p.x - mTriMax.x < eps) &&
                       (mTriMin.y - p.y < eps) && (p.y - mTriMax.y < eps) &&
                       (mTriMin.z - p.z < eps) && (p.z - mTriMax.z < eps);
            }

            public bool IsInTriangle(CGeom3d.Vec_3d p, double eps)
            {
                IPoint p1 = P1;
                IPoint p2 = P2;
                IPoint p3 = P3;

                if (IsInTriangleBoundingBox(p, eps))
                {
                    return (((p1.mPosition - p).Value() < eps) ||
                            ((p2.mPosition - p).Value() < eps) ||
                            ((p3.mPosition - p).Value() < eps) ||
                            CGeom3d.InLine_3d(mEdges[0].mP1.mPosition, mEdges[0].mP2.mPosition, p, eps) ||
                            CGeom3d.InLine_3d(mEdges[1].mP1.mPosition, mEdges[1].mP2.mPosition, p, eps) ||
                            CGeom3d.InLine_3d(mEdges[2].mP1.mPosition, mEdges[2].mP2.mPosition, p, eps) ||
                            CGeom3d.In_Triangle_3d(p1.mPosition, p2.mPosition, p3.mPosition, p, eps));
                }
                else
                    return false;
            }

            public bool AreTriangleBoxesTouching(ITriangle tri2)
            {
                return ((((mTriMin.x - tri2.mTriMin.x < 0.0001) && (tri2.mTriMin.x - mTriMax.x < 0.0001)) ||
                         ((mTriMin.x - tri2.mTriMax.x < 0.0001) && (tri2.mTriMax.x - mTriMax.x < 0.0001)) ||
                         ((tri2.mTriMin.x - mTriMin.x < 0.0001) && (mTriMin.x - tri2.mTriMax.x < 0.0001)) ||
                         ((tri2.mTriMin.x - mTriMax.x < 0.0001) && (mTriMax.x - tri2.mTriMax.x < 0.0001))) &&
                        (((mTriMin.y - tri2.mTriMin.y < 0.0001) && (tri2.mTriMin.y - mTriMax.y < 0.0001)) ||
                         ((mTriMin.y - tri2.mTriMax.y < 0.0001) && (tri2.mTriMax.y - mTriMax.y < 0.0001)) ||
                         ((tri2.mTriMin.y - mTriMin.y < 0.0001) && (mTriMin.y - tri2.mTriMax.y < 0.0001)) ||
                         ((tri2.mTriMin.y - mTriMax.y < 0.0001) && (mTriMax.y - tri2.mTriMax.y < 0.0001))) &&
                        (((mTriMin.z - tri2.mTriMin.z < 0.0001) && (tri2.mTriMin.z - mTriMax.z < 0.0001)) ||
                         ((mTriMin.z - tri2.mTriMax.z < 0.0001) && (tri2.mTriMax.z - mTriMax.z < 0.0001)) ||
                         ((tri2.mTriMin.z - mTriMin.z < 0.0001) && (mTriMin.z - tri2.mTriMax.z < 0.0001)) ||
                         ((tri2.mTriMin.z - mTriMax.z < 0.0001) && (mTriMax.z - tri2.mTriMax.z < 0.0001))));
            }

            public CGeom3d.Vec_3d Center
            {
                get { return (P1.mPosition + P2.mPosition + P3.mPosition) * 1.0 / 3.0; }
            }

            public double Area
            {
                get
                {
                    double area = 0;
                    CGeom3d.AreaTriangle(P1.mPosition, P2.mPosition, P3.mPosition, ref area);
                    return Math.Abs(area);
                }
            }

            public double LineLength
            {
                get 
                {
                    CGeom3d.Vec_3d p1 = P1.mPosition;
                    CGeom3d.Vec_3d p2 = P2.mPosition;
                    CGeom3d.Vec_3d p3 = P3.mPosition;
                    return (p1 - p2).Value() + (p2 - p3).Value() + (p3 - p1).Value();
                }
            }

            public CGeom3d.Vec_3d BoundingBoxMin
            {
                get { return mTriMin; }
            }

            public CGeom3d.Vec_3d BoundingBoxMax
            {
                get { return mTriMax; }
            }

            public bool AreSamePlane(ITriangle tri2)
            {
                if ((Math.Abs(CGeom3d.Vec_3d.cos(this.mPlane.GetNormale(), tri2.mPlane.GetNormale())) > 0.999) && (CGeom3d.Dist_SS_3d(this.mPlane, tri2.mPlane) < 0.0001))
                    return true;
                return false;
            }

            public bool ContainsPoint(IPoint p)
            {
                return (P1 == p) || (P2 == p) || (P3 == p);
            }

            public bool MoveToBoundary(ref CGeom3d.Vec_3d p, double eps)
            {
                for (int i = 0; i < 3; ++i)
                {
                    IEdge edge = mEdges[i];

                    if (CGeom3d.Dist_PL_3d(p, edge.mLine) <= eps)
                    {
                        CGeom3d.Vec_3d cutPoint = new CGeom3d.Vec_3d();
                        CGeom3d.Plane_3d plane = new CGeom3d.Plane_3d();
                        CGeom3d.Plane_PN_3d(p, edge.mLine.v, plane);
                        CGeom3d.Point_LS_3d(edge.mLine, plane, cutPoint);
                        if (CGeom3d.InLine_3d(edge.mP1.mPosition, edge.mP2.mPosition, cutPoint, eps))
                        {
                            p = cutPoint;
                            return true;
                        }
                    }
                }
                return false;
            }
        }

        class IBox
        {
            private CGeom3d.Vec_3d mBoxMin;
            private CGeom3d.Vec_3d mBoxMax;

            public IBox()
            {
                mBoxMin = new CGeom3d.Vec_3d(double.MaxValue, double.MaxValue, double.MaxValue);
                mBoxMax = new CGeom3d.Vec_3d(double.MinValue, double.MinValue, double.MinValue);
            }

            public IBox(ITriangle tri)
            {
                CGeom3d.Vec_3d min = tri.BoundingBoxMin;
                CGeom3d.Vec_3d max = tri.BoundingBoxMax;
                mBoxMin = new CGeom3d.Vec_3d(min.x, min.y, min.z);
                mBoxMax = new CGeom3d.Vec_3d(max.x, max.y, max.z);
            }

            public void Extend(CGeom3d.Vec_3d p)
            {
                if (p.x < mBoxMin.x) mBoxMin.x = p.x;
                if (p.y < mBoxMin.y) mBoxMin.y = p.y;
                if (p.z < mBoxMin.z) mBoxMin.z = p.z;
                if (p.x > mBoxMax.x) mBoxMax.x = p.x;
                if (p.y > mBoxMax.y) mBoxMax.y = p.y;
                if (p.z > mBoxMax.z) mBoxMax.z = p.z;
            }

            public bool AreBoxesTouching(IBox box2)
            {
                return ((((mBoxMin.x - box2.mBoxMin.x < 0.0001) && (box2.mBoxMin.x - mBoxMax.x < 0.0001)) ||
                         ((mBoxMin.x - box2.mBoxMax.x < 0.0001) && (box2.mBoxMax.x - mBoxMax.x < 0.0001)) ||
                         ((box2.mBoxMin.x - mBoxMin.x < 0.0001) && (mBoxMin.x - box2.mBoxMax.x < 0.0001)) ||
                         ((box2.mBoxMin.x - mBoxMax.x < 0.0001) && (mBoxMax.x - box2.mBoxMax.x < 0.0001))) &&
                        (((mBoxMin.y - box2.mBoxMin.y < 0.0001) && (box2.mBoxMin.y - mBoxMax.y < 0.0001)) ||
                         ((mBoxMin.y - box2.mBoxMax.y < 0.0001) && (box2.mBoxMax.y - mBoxMax.y < 0.0001)) ||
                         ((box2.mBoxMin.y - mBoxMin.y < 0.0001) && (mBoxMin.y - box2.mBoxMax.y < 0.0001)) ||
                         ((box2.mBoxMin.y - mBoxMax.y < 0.0001) && (mBoxMax.y - box2.mBoxMax.y < 0.0001))) &&
                        (((mBoxMin.z - box2.mBoxMin.z < 0.0001) && (box2.mBoxMin.z - mBoxMax.z < 0.0001)) ||
                         ((mBoxMin.z - box2.mBoxMax.z < 0.0001) && (box2.mBoxMax.z - mBoxMax.z < 0.0001)) ||
                         ((box2.mBoxMin.z - mBoxMin.z < 0.0001) && (mBoxMin.z - box2.mBoxMax.z < 0.0001)) ||
                         ((box2.mBoxMin.z - mBoxMax.z < 0.0001) && (mBoxMax.z - box2.mBoxMax.z < 0.0001))));
            }

            public CGeom3d.Vec_3d Min
            {
                get { return mBoxMin; }
            }

            public CGeom3d.Vec_3d Max
            {
                get { return mBoxMax; }
            }

            public CGeom3d.Vec_3d Center
            {
                get { return (mBoxMin + mBoxMax) * 0.5; }
            }
        }

        IPoint AddPoint(CGeom3d.Vec_3d position, System.Collections.Generic.SortedDictionary<IPoint, IPoint> sortedPoints, bool skipSortedPoints)
        {
            IPoint tmp = new IPoint(position), result;
            if (skipSortedPoints)
                return tmp;
            if (!sortedPoints.TryGetValue(tmp, out result))
            {
                sortedPoints.Add(tmp, tmp);
                result = tmp;
            }
            return result;
        }

        IPoint AddPoint(CGeom3d.Vec_3d position, System.Collections.Generic.SortedDictionary<IPoint, IPoint> sortedPoints)
        {
            return AddPoint(position, sortedPoints, false);
        }

        IPoint FindPoint(CGeom3d.Vec_3d position, System.Collections.Generic.SortedDictionary<IPoint, IPoint> sortedPoints)
        {
            IPoint tmp = new IPoint(position), result;
            if (sortedPoints.TryGetValue(tmp, out result))
                return result;
            return null;
        }

        IEdge AddEdge(IPoint p1, IPoint p2, IEdge parentEdge)
        {
            if (p1 == p2)
                throw new NotImplementedException();

            foreach (IEdge edge in p1.mEdges)
            {
                if ((edge.mP1 == p2) || (edge.mP2 == p2))
                    return edge;
            }
            foreach (IEdge edge in p2.mEdges)
            {
                if ((edge.mP1 == p1) || (edge.mP2 == p1))
                    return edge;
            }
            IEdge result = new IEdge(p1, p2, parentEdge);
            p1.mEdges.Add(result);
            p2.mEdges.Add(result);
            return result;
        }

        IEdge FindEdge(IPoint p1, IPoint p2)
        {
            foreach (IEdge edge in p1.mEdges)
            {
                if ((edge.mP1 == p2) || (edge.mP2 == p2))
                    return edge;
            }
            foreach (IEdge edge in p2.mEdges)
            {
                if ((edge.mP1 == p1) || (edge.mP2 == p1))
                    return edge;
            }
            return null;
        }

        ITriangle AddTriangle(IEdge e0, IEdge e1, IEdge e2, uint orgId, CGeom3d.Plane_3d plane)
        {
            ITriangle result = new ITriangle(e0, e1, e2, orgId, plane);
            e0.mTriangles.Add(result);
            e1.mTriangles.Add(result);
            e2.mTriangles.Add(result);
            return result;
        }

        int GetTriangleCutPoints(ITriangle tri, CGeom3d.Plane_3d cutPlane, ref CGeom3d.Vec_3d cutP1, ref CGeom3d.Vec_3d cutP2)
        {
            CGeom3d.Vec_3d[] cutPoints = new CGeom3d.Vec_3d[3];
            for (int i = 0; i < 3; ++i)
                cutPoints[i] = new CGeom3d.Vec_3d();

            int foundCuts = 0;
            for (int i = 0; i < 3; ++i)
            {
                if (tri.mEdges[i].GetIntersect(cutPlane, ref cutPoints[foundCuts]))
                    ++foundCuts;
            }

            if (foundCuts > 1)
            {
                if (foundCuts == 2)
                {
                    if ((cutPoints[0] - cutPoints[1]).Value() < 0.000001)
                        foundCuts -= 1;
                }
                else // (foundCuts == 3)
                {
                    if ((cutPoints[0] - cutPoints[1]).Value() < 0.000001)
                    {
                        foundCuts -= 1;
                        cutPoints[0] = cutPoints[1];
                        cutPoints[1] = cutPoints[2];
                    }
                    else if ((cutPoints[0] - cutPoints[2]).Value() < 0.000001)
                    {
                        foundCuts -= 1;
                        cutPoints[0] = cutPoints[1];
                        cutPoints[1] = cutPoints[2];
                    }
                    else if ((cutPoints[1] - cutPoints[2]).Value() < 0.000001)
                    {
                        foundCuts -= 1;
                        cutPoints[1] = cutPoints[2];
                    }
                }
            }

            if (foundCuts >= 1)
                cutP1 = cutPoints[0];
            if (foundCuts >= 2)
                cutP2 = cutPoints[1];
            
            return foundCuts;
        }

        bool SplittTriangle(ITriangle tri, CGeom3d.Vec_3d p, HashSet<ITriangle> killTriangles, HashSet<ITriangle> newTriangles, System.Collections.Generic.SortedDictionary<IPoint, IPoint> sortedPoints, IEdge frontCutEdge)
        {
            if (((p - tri.P1.mPosition).Value() < 0.000001) ||
                ((p - tri.P2.mPosition).Value() < 0.000001) ||
                ((p - tri.P3.mPosition).Value() < 0.000001))
                return false;

            for (int i = 0; i < 3; ++i )
                if (CGeom3d.InLineWithoutEndPoints_3d(tri.mEdges[i].mP1.mPosition, tri.mEdges[i].mP2.mPosition, p))
                {
                    IPoint ip = AddPoint(p, sortedPoints);
                    if ((ip == tri.mEdges[i].mP1) ||
                        (ip == tri.mEdges[i].mP2) ||
                        (ip == tri.GetOtherPoint(tri.mEdges[i].mP1, tri.mEdges[i].mP2)))
                        return false;
                    
                    killTriangles.Add(tri);

                    IEdge ie0 = AddEdge(ip, tri.mEdges[i].mP1, tri.mEdges[i]);
                    IEdge ie1 = AddEdge(ip, tri.mEdges[i].mP2, tri.mEdges[i]);
                    IEdge ie2 = AddEdge(ip, tri.GetOtherPoint(tri.mEdges[i].mP1, tri.mEdges[i].mP2), null);
                    ie0.mType = tri.mEdges[i].mType;
                    ie1.mType = tri.mEdges[i].mType;
                    ie2.mType = IEdgeType.eCut;
                    ie0.CutEdge = tri.mEdges[i].CutEdge;
                    ie1.CutEdge = tri.mEdges[i].CutEdge;
                    ie2.CutEdge = frontCutEdge;
                    ITriangle t1, t2;
                    t1 = AddTriangle(ie0, ie2, tri.GetOtherEdge(tri.mEdges[i].mP1, tri.mEdges[i]), tri.mOrgId, tri.OrgPlane);
                    t2 = AddTriangle(ie1, ie2, tri.GetOtherEdge(tri.mEdges[i].mP2, tri.mEdges[i]), tri.mOrgId, tri.OrgPlane);

                    newTriangles.Add(t1);
                    newTriangles.Add(t2);

                    //if (Math.Abs(tri.Area - t1.Area - t2.Area) > 0.001)
                    //    throw new NotImplementedException();

                    return true;
                }

            return false;
        }

        bool SplittTriangle(ITriangle tri, CGeom3d.Plane_3d plane, HashSet<ITriangle> killTriangles, HashSet<ITriangle> newTriangles, System.Collections.Generic.SortedDictionary<IPoint, IPoint> sortedPoints, IEdge frontCutEdge)
        {
            CGeom3d.Vec_3d cutP1 = new CGeom3d.Vec_3d();
            CGeom3d.Vec_3d cutP2 = new CGeom3d.Vec_3d();
            int cuts = GetTriangleCutPoints(tri, plane, ref cutP1, ref cutP2);

            if (cuts == 2)
            {
                IPoint p1 = tri.P1;
                IPoint p2 = tri.P2;
                IPoint p3 = tri.P3;

                bool isCutP1Corner = (((cutP1 - p1.mPosition).Value() < 0.000001) ||
                                      ((cutP1 - p2.mPosition).Value() < 0.000001) ||
                                      ((cutP1 - p3.mPosition).Value() < 0.000001));
                bool isCutP2Corner = (((cutP2 - p1.mPosition).Value() < 0.000001) ||
                                      ((cutP2 - p2.mPosition).Value() < 0.000001) ||
                                      ((cutP2 - p3.mPosition).Value() < 0.000001));
                if (isCutP1Corner && isCutP2Corner)
                    return false;
                else if (isCutP1Corner && !isCutP2Corner)
                    return SplittTriangle(tri, cutP2, killTriangles, newTriangles, sortedPoints, frontCutEdge);
                else if (!isCutP1Corner && isCutP2Corner)
                    return SplittTriangle(tri, cutP1, killTriangles, newTriangles, sortedPoints, frontCutEdge);
                else
                {
                    // real cut with 3 result triangles
                    for (int i = 0; i < 3; ++i)
                        if (CGeom3d.InLineWithoutEndPoints_3d(tri.mEdges[i].mP1.mPosition, tri.mEdges[i].mP2.mPosition, cutP1))
                            for(int j = 0; j < 3; ++j)
                                if ((i != j) && (CGeom3d.InLineWithoutEndPoints_3d(tri.mEdges[j].mP1.mPosition, tri.mEdges[j].mP2.mPosition, cutP2)))
                                {

                                    IPoint ip0 = AddPoint(cutP1, sortedPoints);
                                    IPoint ip1 = AddPoint(cutP2, sortedPoints);
                                    IPoint corner = tri.mEdges[j].GetCorner(tri.mEdges[i]);

                                    if ((ip0 == ip1) ||
                                        (ip0 == corner) ||
                                        (ip0 == tri.mEdges[i].GetOtherPoint(corner)) ||
                                        (ip1 == corner) ||
                                        (ip1 == tri.mEdges[j].GetOtherPoint(corner)) ||
                                        (ip0 == tri.mEdges[j].GetOtherPoint(corner)))
                                        return false;

                                    killTriangles.Add(tri);

                                    IEdge ie0 = AddEdge(ip0, ip1, null);
                                    IEdge ie1 = AddEdge(ip0, corner, tri.mEdges[i]);
                                    IEdge ie2 = AddEdge(ip0, tri.mEdges[i].GetOtherPoint(corner), tri.mEdges[i]);
                                    IEdge ie3 = AddEdge(ip1, corner, tri.mEdges[j]);
                                    IEdge ie4 = AddEdge(ip1, tri.mEdges[j].GetOtherPoint(corner), tri.mEdges[j]);
                                    IEdge ie5 = AddEdge(ip0, tri.mEdges[j].GetOtherPoint(corner), null);
                                    ie0.mType = IEdgeType.eCut;
                                    ie1.mType = tri.mEdges[i].mType;
                                    ie2.mType = tri.mEdges[i].mType;
                                    ie3.mType = tri.mEdges[j].mType;
                                    ie4.mType = tri.mEdges[j].mType;
                                    ie5.mType = IEdgeType.eIntern;
                                    ie0.CutEdge = frontCutEdge;
                                    ie1.CutEdge = tri.mEdges[i].CutEdge;
                                    ie2.CutEdge = tri.mEdges[i].CutEdge;
                                    ie3.CutEdge = tri.mEdges[j].CutEdge;
                                    ie4.CutEdge = tri.mEdges[j].CutEdge;
                                    ITriangle t1, t2, t3;
                                    t1 = AddTriangle(ie0, ie1, ie3, tri.mOrgId, tri.OrgPlane);
                                    t2 = AddTriangle(ie0, ie4, ie5, tri.mOrgId, tri.OrgPlane);
                                    t3 = AddTriangle(ie2, ie5, tri.GetOtherEdge(tri.mEdges[i], tri.mEdges[j]), tri.mOrgId, tri.OrgPlane);

                                    newTriangles.Add(t1);
                                    newTriangles.Add(t2);
                                    newTriangles.Add(t3);
/*
                                    if (Math.Abs(tri.Area - t1.Area - t2.Area - t3.Area) > 0.001)
                                        throw new NotImplementedException();
*/
                                    return true;
                                }
                }
            }
            return false;
        }

        public interface IDebugDraw
        {
            void DrawLine(CGeom3d.Vec_3d p1, CGeom3d.Vec_3d p2, float r, float g, float b, float a);
            void DrawTriangle(CGeom3d.Vec_3d p1, CGeom3d.Vec_3d p2, CGeom3d.Vec_3d p3, float r, float g, float b, float a);
        }

        public class MeshInfo
        {
            public CGeom3d.Vec_3d mP1;
            public CGeom3d.Vec_3d mP2;

            public MeshInfo(CGeom3d.Vec_3d p1, CGeom3d.Vec_3d p2)
            {
                mP1 = p1;
                mP2 = p2;
            }
        }

        public enum TriangleType
        {
            Surface,
            SideAirSurface,
            DebugSurface
        }

        public class TriangleOutput
        {
            public TriangleType mType;
            public uint mOrgId;

            public TriangleOutput(TriangleType type, uint orgId)
            {
                mType = type;
                mOrgId = orgId;
            }
        }

        class OctTreeNode
        {
            ///////////////// Data
            private IBox mBox;
            private HashSet<ITriangle> mTriangles;
            private OctTreeNode[] mChilds;
            private OctTreeNode mParent;
            private int mLevel;

            public const int MaxCellTriangleCount = 4;
            public const int MaxCellLevel = 8;
            //////////////////////

            public OctTreeNode(IBox box)
            {
                mBox = box;
                mTriangles = null;
                mChilds = null;
                mParent = null;
                mLevel = 0;
            }

            public void AddTriangle(ITriangle tri)
            {
                IBox triBox = new IBox(tri);
                if (mChilds == null)
                {
                    if (mTriangles == null)
                        mTriangles = new HashSet<ITriangle>();

                    mTriangles.Add(tri);

                    if ((mTriangles.Count > MaxCellTriangleCount) && (mLevel < MaxCellLevel))
                        Splitt();
                }
                else
                {
                    int touching = 0;
                    OctTreeNode bestChild = null;
                    foreach (OctTreeNode child in mChilds)
                        if (child.mBox.AreBoxesTouching(triBox))
                        {
                            touching++;
                            bestChild = child;
                        }
                    if (touching == 1)
                        bestChild.AddTriangle(tri);
                    else
                        mTriangles.Add(tri);
                }
            }

            public void RemoveTriangle(ITriangle tri)
            {
                if (mChilds == null)
                {
                    if (mTriangles == null)
                        return;
                    mTriangles.Remove(tri);
                }
                else
                {
                    if (mTriangles.Contains(tri))
                    {
                        mTriangles.Remove(tri);
                        return;
                    }
                    else
                    {
                        IBox triBox = new IBox(tri);
                        foreach (OctTreeNode child in mChilds)
                            if (child.mBox.AreBoxesTouching(triBox))
                                child.RemoveTriangle(tri);
                    }
                }
            }

            private void Splitt()
            {
                if (mChilds != null)
                    throw new NotImplementedException();

                IBox[] boxes = new IBox[8];
                mChilds = new OctTreeNode[8];
                CGeom3d.Vec_3d minBox = mBox.Min;
                CGeom3d.Vec_3d maxBox = mBox.Max;

                for (int i = 0; i < 8; ++i)
                {
                    boxes[i] = new IBox();
                    boxes[i].Extend(mBox.Center);
                    switch (i)
                    {
                        case 0: boxes[i].Extend(new CGeom3d.Vec_3d(minBox.x, minBox.y, minBox.z)); break;
                        case 1: boxes[i].Extend(new CGeom3d.Vec_3d(maxBox.x, minBox.y, minBox.z)); break;
                        case 2: boxes[i].Extend(new CGeom3d.Vec_3d(minBox.x, maxBox.y, minBox.z)); break;
                        case 3: boxes[i].Extend(new CGeom3d.Vec_3d(maxBox.x, maxBox.y, minBox.z)); break;
                        case 4: boxes[i].Extend(new CGeom3d.Vec_3d(minBox.x, minBox.y, maxBox.z)); break;
                        case 5: boxes[i].Extend(new CGeom3d.Vec_3d(maxBox.x, minBox.y, maxBox.z)); break;
                        case 6: boxes[i].Extend(new CGeom3d.Vec_3d(minBox.x, maxBox.y, maxBox.z)); break;
                        case 7: boxes[i].Extend(new CGeom3d.Vec_3d(maxBox.x, maxBox.y, maxBox.z)); break;
                        default: throw new NotImplementedException();
                    }

                    mChilds[i] = new OctTreeNode(boxes[i]);
                    mChilds[i].mParent = this;
                    mChilds[i].mLevel = this.mLevel + 1;
                }

                HashSet<ITriangle> kill = new HashSet<ITriangle>();
                foreach (ITriangle tri in mTriangles)
                {
                    IBox triBox = new IBox(tri);
                    int touching = 0;
                    OctTreeNode bestChild = null;
                    foreach (OctTreeNode child in mChilds)
                        if (child.mBox.AreBoxesTouching(triBox))
                        {
                            bestChild = child;
                            touching++;
                        }
                    if (touching == 1)
                    {
                        bestChild.AddTriangle(tri);
                        //mTriangles.Remove(tri);
                        kill.Add(tri);
                    }
                }
                foreach (ITriangle tri in kill)
                    mTriangles.Remove(tri);
            }

            private void GetBoundedTriangles(HashSet<ITriangle> result, IBox box)
            {
                if (mBox.AreBoxesTouching(box))
                {
                    if (mTriangles != null)
                        foreach (ITriangle tri in mTriangles)
                            result.Add(tri);

                    if (mChilds != null)
                        foreach (OctTreeNode child in mChilds)
                            child.GetBoundedTriangles(result, box);
                }
            }

            public HashSet<ITriangle> GetBoundedTriangles(IBox box)
            {
                HashSet<ITriangle> result = new HashSet<ITriangle>();
                GetBoundedTriangles(result, box);
                return result;
            }

            private void GetAllFilledLeaves(HashSet<OctTreeNode> result)
            {
                if ((mTriangles != null) && (mTriangles.Count > 0))
                    result.Add(this);

                if (mChilds != null)
                    foreach (OctTreeNode child in mChilds)
                        child.GetAllFilledLeaves(result);
            }

            public HashSet<OctTreeNode> GetAllFilledLeaves()
            {
                HashSet<OctTreeNode> result = new HashSet<OctTreeNode>();
                GetAllFilledLeaves(result);
                return result;
            }

            public void DrawYourself(IDebugDraw debugDraw)
            {
                CGeom3d.Vec_3d minBox = mBox.Min;
                CGeom3d.Vec_3d maxBox = mBox.Max;
                CGeom3d.Vec_3d[] p = new CGeom3d.Vec_3d[8];
                p[0] = new CGeom3d.Vec_3d(minBox.x, minBox.y, minBox.z);
                p[1] = new CGeom3d.Vec_3d(minBox.x, maxBox.y, minBox.z);
                p[2] = new CGeom3d.Vec_3d(maxBox.x, maxBox.y, minBox.z);
                p[3] = new CGeom3d.Vec_3d(maxBox.x, minBox.y, minBox.z);
                p[4] = new CGeom3d.Vec_3d(minBox.x, minBox.y, maxBox.z);
                p[5] = new CGeom3d.Vec_3d(minBox.x, maxBox.y, maxBox.z);
                p[6] = new CGeom3d.Vec_3d(maxBox.x, maxBox.y, maxBox.z);
                p[7] = new CGeom3d.Vec_3d(maxBox.x, minBox.y, maxBox.z);
                debugDraw.DrawLine(p[0], p[1], 0, 0, 0, 1);
                debugDraw.DrawLine(p[1], p[2], 0, 0, 0, 1);
                debugDraw.DrawLine(p[2], p[3], 0, 0, 0, 1);
                debugDraw.DrawLine(p[3], p[0], 0, 0, 0, 1);
                debugDraw.DrawLine(p[4], p[5], 0, 0, 0, 1);
                debugDraw.DrawLine(p[5], p[6], 0, 0, 0, 1);
                debugDraw.DrawLine(p[6], p[7], 0, 0, 0, 1);
                debugDraw.DrawLine(p[7], p[4], 0, 0, 0, 1);
                debugDraw.DrawLine(p[0], p[4], 0, 0, 0, 1);
                debugDraw.DrawLine(p[1], p[5], 0, 0, 0, 1);
                debugDraw.DrawLine(p[2], p[6], 0, 0, 0, 1);
                debugDraw.DrawLine(p[3], p[7], 0, 0, 0, 1);
            }
        }

        public Dictionary<CGeom3d.Triangle, TriangleOutput> CalculateAnalyticIsovist3d(IDebugDraw debugDraw, CGeom3d.Vec_3d lookat, double openingAngleH, double openingAngleV)
        {
            System.Collections.Generic.SortedDictionary<IPoint, IPoint> sortedPoints = new System.Collections.Generic.SortedDictionary<IPoint, IPoint>(new IPointCompare(0.000001));
            Dictionary<uint, List<ITriangle>> idPoly = new Dictionary<uint, List<ITriangle>>();
            HashSet<ITriangle> allTriangles = new HashSet<ITriangle>();
            CGeom3d.Vec_3d isovistCenter = mPoint;

            if (lookat != null)
            {
                CGeom3d.Plane_3d[] viewPlane = new CGeom3d.Plane_3d[5];
                for (int i = 0; i < 5; ++i)
                    viewPlane[i] = new CGeom3d.Plane_3d();
                CGeom3d.Vec_3d v0 = new CGeom3d.Vec_3d(0, 0, -1);
                CGeom3d.Rot_X_3d(v0, Math.Sin(openingAngleV / 2.0), Math.Cos(openingAngleV / 2.0));
                CGeom3d.Vec_3d v1 = new CGeom3d.Vec_3d(0, 0, 1);
                CGeom3d.Rot_X_3d(v1, Math.Sin(-openingAngleV / 2.0), Math.Cos(-openingAngleV / 2.0));
                CGeom3d.Vec_3d v2 = new CGeom3d.Vec_3d(1, 0, 0);
                CGeom3d.Rot_Z_3d(v2, Math.Sin(openingAngleH / 2.0), Math.Cos(openingAngleH / 2.0));
                CGeom3d.Vec_3d v3 = new CGeom3d.Vec_3d(-1, 0, 0);
                CGeom3d.Rot_Z_3d(v3, Math.Sin(-openingAngleH / 2.0), Math.Cos(-openingAngleH / 2.0));
                CGeom3d.Vec_3d v4 = new CGeom3d.Vec_3d(0, 1, 0);

                double angleH = CGeom3d.GetXYAngle(lookat);
                double[] rz = new double[16];
                double cosz = Math.Cos(angleH);
                double sinz = Math.Sin(angleH);
                rz[0] = cosz; rz[1] = -sinz; rz[2] = 0.0; rz[3] = 0.0;
                rz[4] = sinz; rz[5] = cosz; rz[6] = 0.0; rz[7] = 0.0;
                rz[8] = 0.0; rz[9] = 0.0; rz[10] = 1.0; rz[11] = 0.0;
                rz[12] = 0.0; rz[13] = 0.0; rz[14] = 0.0; rz[15] = 1.0;

                CGeom3d.Vec_3d lookatXY = new CGeom3d.Vec_3d(lookat.x, lookat.y, 0);
                double angleV = Math.Acos(lookatXY.Value() / lookat.Value());
                if (lookat.z < 0)
                    angleV *= -1;
                double[] rx = new double[16];
                double cosx = Math.Cos(angleV);
                double sinx = Math.Sin(angleV);
                rx[0] = 1.0; rx[1] = 0.0; rx[2] = 0.0; rx[3] = 0.0;
                rx[4] = 0.0; rx[5] = cosx; rx[6] = -sinx; rx[7] = 0.0;
                rx[8] = 0.0; rx[9] = sinx; rx[10] = cosx; rx[11] = 0.0;
                rx[12] = 0.0; rx[13] = 0.0; rx[14] = 0.0; rx[15] = 1.0;

                CGeom3d.Vec_3d erg0 = new CGeom3d.Vec_3d();
                CGeom3d.Vec_3d erg1 = new CGeom3d.Vec_3d();
                CGeom3d.Vec_3d erg2 = new CGeom3d.Vec_3d();
                CGeom3d.Vec_3d erg3 = new CGeom3d.Vec_3d();
                CGeom3d.Vec_3d erg4 = new CGeom3d.Vec_3d();
                MatrixMultiplication(rx, v0, erg0);
                MatrixMultiplication(rx, v1, erg1);
                MatrixMultiplication(rx, v2, erg2);
                MatrixMultiplication(rx, v3, erg3);
                MatrixMultiplication(rx, v4, erg4);
                MatrixMultiplication(rz, erg0, v0);
                MatrixMultiplication(rz, erg1, v1);
                MatrixMultiplication(rz, erg2, v2);
                MatrixMultiplication(rz, erg3, v3);
                MatrixMultiplication(rz, erg4, v4);

                CGeom3d.Plane_PN_3d(isovistCenter, v0, viewPlane[0]);
                CGeom3d.Plane_PN_3d(isovistCenter, v1, viewPlane[1]);
                CGeom3d.Plane_PN_3d(isovistCenter, v2, viewPlane[2]);
                CGeom3d.Plane_PN_3d(isovistCenter, v3, viewPlane[3]);
                CGeom3d.Plane_PN_3d(isovistCenter + v4, v4, viewPlane[4]);

                CGeom3d.Line_3d cutLine = new CGeom3d.Line_3d();
                CGeom3d.Vec_3d corner0 = new CGeom3d.Vec_3d();
                CGeom3d.Vec_3d corner1 = new CGeom3d.Vec_3d();
                CGeom3d.Vec_3d corner2 = new CGeom3d.Vec_3d();
                CGeom3d.Vec_3d corner3 = new CGeom3d.Vec_3d();
                CGeom3d.Line_SS_3d(viewPlane[0], viewPlane[2], cutLine);
                CGeom3d.Point_LS_3d(cutLine, viewPlane[4], corner0);
                CGeom3d.Line_SS_3d(viewPlane[0], viewPlane[3], cutLine);
                CGeom3d.Point_LS_3d(cutLine, viewPlane[4], corner1);
                CGeom3d.Line_SS_3d(viewPlane[1], viewPlane[3], cutLine);
                CGeom3d.Point_LS_3d(cutLine, viewPlane[4], corner2);
                CGeom3d.Line_SS_3d(viewPlane[1], viewPlane[2], cutLine);
                CGeom3d.Point_LS_3d(cutLine, viewPlane[4], corner3);

/*
                debugDraw.DrawLine(corner0, corner1, 0, 0, 0, 1);
                debugDraw.DrawLine(corner1, corner2, 0, 0, 0, 1);
                debugDraw.DrawLine(corner2, corner3, 0, 0, 0, 1);
                debugDraw.DrawLine(corner3, corner0, 0, 0, 0, 1);
 */

                IPoint i0 = AddPoint(corner0, sortedPoints, true);
                IPoint i1 = AddPoint(corner1, sortedPoints, true);
                IPoint i2 = AddPoint(corner2, sortedPoints, true);
                IPoint i3 = AddPoint(corner3, sortedPoints, true);
                IEdge e0 = AddEdge(i0, i1, null);
                IEdge e1 = AddEdge(i1, i2, null);
                IEdge e2 = AddEdge(i2, i3, null);
                IEdge e3 = AddEdge(i3, i0, null);
                IEdge e4 = AddEdge(i0, i2, null);
                e0.mType = IEdgeType.ePolygonBorder;
                e1.mType = IEdgeType.ePolygonBorder;
                e2.mType = IEdgeType.ePolygonBorder;
                e3.mType = IEdgeType.ePolygonBorder;
                e4.mType = IEdgeType.eIntern;
                ITriangle t1 = AddTriangle(e0, e1, e4, uint.MaxValue, viewPlane[4]);
                ITriangle t2 = AddTriangle(e2, e3, e4, uint.MaxValue, viewPlane[4]);
                allTriangles.Add(t1);
                allTriangles.Add(t2);

                if (!idPoly.ContainsKey(uint.MaxValue))
                    idPoly[uint.MaxValue] = new List<ITriangle>();
                idPoly[uint.MaxValue].Add(t1);
                idPoly[uint.MaxValue].Add(t2);
            }
            
            //build triangles
            foreach(KeyValuePair<uint, PolyInfo> pair in mLookup)
            {
                CGeom3d.Triangle[] triangles = null;
                CGeom3d.Plane_3d plane = new CGeom3d.Plane_3d();
                CGeom3d.Vec_3d center = new CGeom3d.Vec_3d(), normale = new CGeom3d.Vec_3d();
                CGeom3d.ComputePlane(pair.Value.mPoints, center, normale);
                CGeom3d.Plane_PN_3d(center, normale, plane);
                Tess.COpenGLTesselator.Tesselate(pair.Value.mPoints, ref triangles);

                foreach (CGeom3d.Triangle triangle in triangles)
                {
                    CGeom3d.Vec_3d vt0 = triangle.m_pP2 - triangle.m_pP1;
                    CGeom3d.Vec_3d vt1 = triangle.m_pP3 - triangle.m_pP1;
                    CGeom3d.Vec_3d tNormale = vt0.amul(vt1);
                    if (!tNormale.Normalize())
                        continue;
                        //tNormale = normale;
                    //CGeom3d.Plane_3d tPlane = new CGeom3d.Plane_3d();
                    //CGeom3d.Plane_PN_3d(triangle.m_pP1, tNormale, tPlane);
                    //if (Math.Abs(CGeom3d.Dist_PS_3d(isovistCenter, tPlane)) < 0.000001)
                    //    continue;

                    IPoint i0 = AddPoint(triangle.m_pP1, sortedPoints);
                    IPoint i1 = AddPoint(triangle.m_pP2, sortedPoints);
                    IPoint i2 = AddPoint(triangle.m_pP3, sortedPoints);
                    IEdge e0 = AddEdge(i0, i1, null);
                    IEdge e1 = AddEdge(i1, i2, null);
                    IEdge e2 = AddEdge(i2, i0, null);
                    ITriangle t = AddTriangle(e0, e1, e2, pair.Key, plane);
                    allTriangles.Add(t);

                    if (!idPoly.ContainsKey(pair.Key))
                        idPoly[pair.Key] = new List<ITriangle>();
                    idPoly[pair.Key].Add(t);
                }
            }

            // build octtree
            IBox maxBox = new IBox();
            foreach (KeyValuePair<IPoint, IPoint> valuePair in sortedPoints)
                maxBox.Extend(valuePair.Key.mPosition);
            OctTreeNode octTreeRoot = new OctTreeNode(maxBox);
            foreach (ITriangle tri in allTriangles)
                octTreeRoot.AddTriangle(tri);
/*
            // display octtree
            HashSet<OctTreeNode> leaves = octTreeRoot.GetAllFilledLeaves();
            foreach (OctTreeNode leave in leaves)
                leave.DrawYourself(debugDraw);
*/


            //cut project polygon boundaries
            foreach (KeyValuePair<uint, PolyInfo> pair in mLookup)
            {
                foreach (CGeom3d.Vec_3d[] loop in pair.Value.mPoints)
                {
                    CGeom3d.Vec_3d last = loop.Last();
                    foreach (CGeom3d.Vec_3d p in loop)
                    {
                        IPoint p0 = FindPoint(last, sortedPoints);
                        IPoint p1 = FindPoint(p, sortedPoints);
                        IEdge edge = null;
                        if ((p0 != null) && (p1 != null))
                            edge = FindEdge(p0, p1);

                        if (edge != null)
                            edge.mType = IEdgeType.ePolygonBorder;

                        last = p;
                    }
                }
            }

            // splitt intersecting triangles
            bool repeatSplitting;
            do 
            {
                repeatSplitting = false;
                HashSet<ITriangle> killTriangles3 = new HashSet<ITriangle>();
                HashSet<ITriangle> newTriangles3 = new HashSet<ITriangle>();

                foreach (ITriangle tri in allTriangles)
                {
                    if (killTriangles3.Contains(tri))
                    {
                        repeatSplitting = true;
                        continue;
                    }

                    HashSet<ITriangle> neighbours = octTreeRoot.GetBoundedTriangles(new IBox(tri));
                    foreach (ITriangle tri2 in neighbours)
                    {
                        if (killTriangles3.Contains(tri2))
                        {
                            repeatSplitting = true;
                            continue;
                        }
                        if ((tri.mOrgId != tri2.mOrgId) && (tri.mOrgId != uint.MaxValue) && (tri2.mOrgId != uint.MaxValue))
                        {
                            CGeom3d.Vec_3d cut1P1 = new CGeom3d.Vec_3d();
                            CGeom3d.Vec_3d cut1P2 = new CGeom3d.Vec_3d();
                            CGeom3d.Vec_3d cut2P1 = new CGeom3d.Vec_3d();
                            CGeom3d.Vec_3d cut2P2 = new CGeom3d.Vec_3d();
                            if (tri.AreTriangleBoxesTouching(tri2) &&
                                !tri.AreSamePlane(tri2) &&
                                (GetTriangleCutPoints(tri, tri2.Plane, ref cut1P1, ref cut1P2) == 2) &&
                                (GetTriangleCutPoints(tri2, tri.Plane, ref cut2P1, ref cut2P2) == 2) &&
                                (CGeom3d.InLineWithoutEndPoints_3d(cut1P1, cut1P2, cut2P1) ||
                                 CGeom3d.InLineWithoutEndPoints_3d(cut1P1, cut1P2, cut2P2) ||
                                 CGeom3d.InLineWithoutEndPoints_3d(cut2P1, cut2P2, cut1P1) ||
                                 CGeom3d.InLineWithoutEndPoints_3d(cut2P1, cut2P2, cut1P2) ||
                                 (((cut1P1 - cut2P1).Value() < 0.000001) && ((cut1P2 - cut2P2).Value() < 0.000001)) ||
                                 (((cut1P2 - cut2P1).Value() < 0.000001) && ((cut1P1 - cut2P2).Value() < 0.000001))))
                            {
                                bool splitt0 = SplittTriangle(tri, tri2.Plane, killTriangles3, newTriangles3, sortedPoints, null);
                                bool splitt1 = SplittTriangle(tri2, tri.Plane, killTriangles3, newTriangles3, sortedPoints, null);
                                if (splitt0 || splitt1)
                                    break;
                            }
                        }
                    }
                }
                // kill and make new to allTriangles
                foreach (ITriangle tri in killTriangles3)
                {
                    for (int i = 0; i < 3; ++i)
                    {
                        IEdge edge = tri.mEdges[i];
                        edge.mTriangles.Remove(tri);
                        if (edge.mTriangles.Count == 0)
                        {
                            edge.mP1.mEdges.Remove(edge);
                            edge.mP2.mEdges.Remove(edge);
                        }
                    }
                    allTriangles.Remove(tri);
                    octTreeRoot.RemoveTriangle(tri);
                }
                foreach (ITriangle tri in newTriangles3)
                {
                    allTriangles.Add(tri);
                    octTreeRoot.AddTriangle(tri);
                    //debugDraw.DrawTriangle(tri.P1.mPosition, tri.P2.mPosition, tri.P3.mPosition, 0, 0, 1, 1);
                }

            } while (repeatSplitting);

            bool somethingChanged;
            do 
            {
                //kill "behind" triangles
                if (true)
                {
                    HashSet<ITriangle> killTriangles2 = new HashSet<ITriangle>();
                    CGeom3d.Vec_3d projT0 = new CGeom3d.Vec_3d();
                    CGeom3d.Vec_3d projT1 = new CGeom3d.Vec_3d();
                    CGeom3d.Vec_3d projT2 = new CGeom3d.Vec_3d();
                    foreach (ITriangle tri1 in allTriangles)
                    {
                        if (tri1.mOrgId == uint.MaxValue)
                            continue;

                        if (killTriangles2.Contains(tri1))
                            continue;
                        CGeom3d.Plane_3d plane1 = tri1.Plane;
                        CGeom3d.Vec_3d t1p1 = tri1.P1.mPosition;
                        CGeom3d.Vec_3d t1p2 = tri1.P2.mPosition;
                        CGeom3d.Vec_3d t1p3 = tri1.P3.mPosition;
                        double dist1 = (t1p1 - isovistCenter).Value();
                        double dist2 = (t1p2 - isovistCenter).Value();
                        double dist3 = (t1p3 - isovistCenter).Value();

                        IBox tri1Box = new IBox(tri1);
                        tri1Box.Extend(isovistCenter);

                        HashSet<ITriangle> neighbours = octTreeRoot.GetBoundedTriangles(tri1Box);
                        HashSet<uint> neighbourOrgIds = new HashSet<uint>();
                        foreach (ITriangle neighbour in neighbours)
                            if (!neighbourOrgIds.Contains(neighbour.mOrgId))
                                neighbourOrgIds.Add(neighbour.mOrgId);

                        foreach (KeyValuePair<uint, List<ITriangle>> poly in idPoly)
                        {
                            if (poly.Key != uint.MaxValue)
                            {
                                bool p1Behind = false;
                                bool p2Behind = false;
                                bool p3Behind = false;

                                if ((tri1.mOrgId != poly.Key) && (neighbourOrgIds.Contains(poly.Key)))
                                    foreach (ITriangle tri2 in poly.Value)
                                    {

                                        IBox tri2Box = new IBox(tri2);

                                        if (!tri1Box.AreBoxesTouching(tri2Box))
                                            continue;

                                        //check tri1 on tri2
                                        CGeom3d.Plane_3d plane2 = tri2.Plane;
                                        CGeom3d.Proj_V_3d(t1p1, t1p1 - isovistCenter, plane2, projT0);
                                        bool projected0 = (CGeom3d.Err_3d == 1) && !CGeom3d.InLine_3d(t1p1, projT0, isovistCenter, 0.0001);
                                        CGeom3d.Proj_V_3d(t1p2, t1p2 - isovistCenter, plane2, projT1);
                                        bool projected1 = (CGeom3d.Err_3d == 1) && !CGeom3d.InLine_3d(t1p2, projT1, isovistCenter, 0.0001);
                                        CGeom3d.Proj_V_3d(t1p3, t1p3 - isovistCenter, plane2, projT2);
                                        bool projected2 = (CGeom3d.Err_3d == 1) && !CGeom3d.InLine_3d(t1p3, projT2, isovistCenter, 0.0001);
                                        if (projected0 && (dist1 - (projT0 - isovistCenter).Value() >= -0.000001) && tri2.IsInTriangle(projT0, 0.001)) p1Behind = true;
                                        if (projected1 && (dist2 - (projT1 - isovistCenter).Value() >= -0.000001) && tri2.IsInTriangle(projT1, 0.001)) p2Behind = true;
                                        if (projected2 && (dist3 - (projT2 - isovistCenter).Value() >= -0.000001) && tri2.IsInTriangle(projT2, 0.001)) p3Behind = true;

                                        if (p1Behind && p2Behind && p3Behind)
                                            break;
                                    }

                                if (p1Behind && p2Behind && p3Behind)
                                {
                                    killTriangles2.Add(tri1);
                                    break;
                                }
/*
                                //debug
                                CGeom3d.Vec_3d polyCenter = new CGeom3d.Vec_3d(0,0,0);
                                int pCount = 0;
                                foreach(CGeom3d.Vec_3d[] loop in mLookup[poly.Key].mPoints)
                                    foreach (CGeom3d.Vec_3d p in loop)
                                    {
                                        polyCenter = polyCenter + p;
                                        pCount++;
                                    }
                                if (pCount > 0)
                                    polyCenter /= pCount;
                                double crossDist = 0.1;
                                if (p1Behind)
                                {
                                    debugDraw.DrawLine(t1p1 + new CGeom3d.Vec_3d(crossDist, 0, 0), t1p1 - new CGeom3d.Vec_3d(crossDist, 0, 0), 0, 0, 0, 1);
                                    debugDraw.DrawLine(t1p1 + new CGeom3d.Vec_3d(0, crossDist, 0), t1p1 - new CGeom3d.Vec_3d(0, crossDist, 0), 0, 0, 0, 1);
                                    debugDraw.DrawLine(t1p1 + new CGeom3d.Vec_3d(0, 0, crossDist), t1p1 - new CGeom3d.Vec_3d(0, 0, crossDist), 0, 0, 0, 1);
                                    debugDraw.DrawLine(tri1.Center, t1p1, 1, 0, 0, 1);
                                    if (Math.Abs(tri1.Normal.y) > 0.999)
                                        debugDraw.DrawLine((tri1.Center + t1p1) * 0.5, polyCenter, 1, 1, 0, 1);
                                }
                                if (p2Behind)
                                {
                                    debugDraw.DrawLine(t1p2 + new CGeom3d.Vec_3d(crossDist, 0, 0), t1p2 - new CGeom3d.Vec_3d(crossDist, 0, 0), 0, 0, 0, 1);
                                    debugDraw.DrawLine(t1p2 + new CGeom3d.Vec_3d(0, crossDist, 0), t1p2 - new CGeom3d.Vec_3d(0, crossDist, 0), 0, 0, 0, 1);
                                    debugDraw.DrawLine(t1p2 + new CGeom3d.Vec_3d(0, 0, crossDist), t1p2 - new CGeom3d.Vec_3d(0, 0, crossDist), 0, 0, 0, 1);
                                    debugDraw.DrawLine(tri1.Center, t1p2, 1, 0, 0, 1);
                                    if (Math.Abs(tri1.Normal.y) > 0.999)
                                        debugDraw.DrawLine((tri1.Center + t1p2) * 0.5, polyCenter, 1, 1, 0, 1);
                                }
                                if (p3Behind)
                                {
                                    debugDraw.DrawLine(t1p3 + new CGeom3d.Vec_3d(crossDist, 0, 0), t1p3 - new CGeom3d.Vec_3d(crossDist, 0, 0), 0, 0, 0, 1);
                                    debugDraw.DrawLine(t1p3 + new CGeom3d.Vec_3d(0, crossDist, 0), t1p3 - new CGeom3d.Vec_3d(0, crossDist, 0), 0, 0, 0, 1);
                                    debugDraw.DrawLine(t1p3 + new CGeom3d.Vec_3d(0, 0, crossDist), t1p3 - new CGeom3d.Vec_3d(0, 0, crossDist), 0, 0, 0, 1);
                                    debugDraw.DrawLine(tri1.Center, t1p3, 1, 0, 0, 1);
                                    if (Math.Abs(tri1.Normal.y) > 0.999)
                                        debugDraw.DrawLine((tri1.Center + t1p3) * 0.5, polyCenter, 1, 1, 0, 1);
                                }
 */
                            }
                            else
                            {
                                // tri1 muss in poly liegen oder es berühren
                                bool p1Behind = false;
                                bool p2Behind = false;
                                bool p3Behind = false;
                                bool borderCutting = false;

                                if (tri1.mOrgId != poly.Key)
                                    foreach (ITriangle tri2 in poly.Value)
                                    {
                                        CGeom3d.Plane_3d plane2 = tri2.Plane;

                                        //check tri1 on tri2
                                        CGeom3d.Proj_V_3d(t1p1, t1p1 - isovistCenter, plane2, projT0);
                                        bool projected0 = (CGeom3d.Err_3d == 1) && !CGeom3d.InLine_3d(t1p1, projT0, isovistCenter, 0.0001);
                                        CGeom3d.Proj_V_3d(t1p2, t1p2 - isovistCenter, plane2, projT1);
                                        bool projected1 = (CGeom3d.Err_3d == 1) && !CGeom3d.InLine_3d(t1p2, projT1, isovistCenter, 0.0001);
                                        CGeom3d.Proj_V_3d(t1p3, t1p3 - isovistCenter, plane2, projT2);
                                        bool projected2 = (CGeom3d.Err_3d == 1) && !CGeom3d.InLine_3d(t1p3, projT2, isovistCenter, 0.0001);
                                        if (projected0 && tri2.IsInTriangle(projT0, 0.001)) p1Behind = true;
                                        if (projected1 && tri2.IsInTriangle(projT1, 0.001)) p2Behind = true;
                                        if (projected2 && tri2.IsInTriangle(projT2, 0.001)) p3Behind = true;

                                        for (int i = 0; i < 3; ++i)
                                        {
                                            for (int j = 0; j < 3; ++j)
                                            {
                                                CGeom3d.Plane_3d cutPlane = new CGeom3d.Plane_3d();
                                                CGeom3d.Plane_PVV_3d(isovistCenter, tri2.mEdges[j].mP1.mPosition - isovistCenter, tri2.mEdges[j].mP2.mPosition - isovistCenter, cutPlane);
                                                CGeom3d.Vec_3d cutPoint = new CGeom3d.Vec_3d();
                                                CGeom3d.Vec_3d cutPoint2 = new CGeom3d.Vec_3d();
                                                if (tri1.mEdges[i].GetIntersectWithoutEndPoints(cutPlane, ref cutPoint))
                                                {
                                                    CGeom3d.Plane_PVV_3d(isovistCenter, tri1.mEdges[i].mP1.mPosition - isovistCenter, tri1.mEdges[i].mP2.mPosition - isovistCenter, cutPlane);
                                                    if (tri2.mEdges[j].GetIntersectWithoutEndPoints(cutPlane, ref cutPoint2) && !CGeom3d.InLine_3d(cutPoint, cutPoint2, isovistCenter))
                                                    {
                                                        borderCutting = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (borderCutting)
                                                break;
                                        }

                                        if (borderCutting)
                                            break;
                                    }

                                if ((!p1Behind && !borderCutting) ||
                                    (!p2Behind && !borderCutting) ||
                                    (!p3Behind && !borderCutting))
                                {
                                    killTriangles2.Add(tri1);
                                    break;
                                }
                            }
                        }
                    }
                    // perform kill
                    foreach (ITriangle tri in killTriangles2)
                    {
                        for (int i = 0; i < 3; ++i)
                        {
                            IEdge edge = tri.mEdges[i];
                            edge.mTriangles.Remove(tri);
                            if (edge.mTriangles.Count == 0)
                            {
                                edge.mP1.mEdges.Remove(edge);
                                edge.mP2.mEdges.Remove(edge);
                            }
                        }
                        allTriangles.Remove(tri);
                        octTreeRoot.RemoveTriangle(tri);

                        //debug
/*
                        debugDraw.DrawTriangle(tri.P1.mPosition, tri.P2.mPosition, tri.P3.mPosition, 0, 1, 0, 0.2f);
                        debugDraw.DrawLine(tri.P1.mPosition, tri.P2.mPosition, 0, 0, 0, 0.2f);
                        debugDraw.DrawLine(tri.P2.mPosition, tri.P3.mPosition, 0, 0, 0, 0.2f);
                        debugDraw.DrawLine(tri.P3.mPosition, tri.P1.mPosition, 0, 0, 0, 0.2f);
 */
                    }
                }

                somethingChanged = false;
                HashSet<ITriangle> killTriangles = new HashSet<ITriangle>();
                HashSet<ITriangle> newTriangles = new HashSet<ITriangle>();

                // splitt run
                foreach (ITriangle frontTriangle in allTriangles)
                {
                    if (killTriangles.Contains(frontTriangle))
                        continue;

                    //if (Math.Abs(CGeom3d.Vec_3d.cos(frontTriangle.Center - isovistCenter, frontTriangle.Normal)) < 0.001)
                    //    continue;

                    for (int j = 0; j < 3; ++j)
                    {
                        if (killTriangles.Contains(frontTriangle))
                            continue;

                        IEdge edge = frontTriangle.mEdges[j];
                        if (edge.mType == IEdgeType.ePolygonBorder) // || (edge.mType == IEdgeType.eCut))
                        {
                            uint polyId = frontTriangle.mOrgId;
                            IPoint p0 = edge.mP1;
                            IPoint p1 = edge.mP2;

                            IBox frontBox = new IBox();
                            frontBox.Extend(p0.mPosition);
                            frontBox.Extend(p1.mPosition);

                            CGeom3d.Plane_3d cutPlane = new CGeom3d.Plane_3d();
                            CGeom3d.Line_3d cutLine = new CGeom3d.Line_3d();
                            CGeom3d.Vec_3d v0 = p0.mPosition - isovistCenter;
                            CGeom3d.Vec_3d v1 = p1.mPosition - isovistCenter;
                            if (!v0.Normalize())
                                continue;
                            if (!v1.Normalize())
                                continue;
                            CGeom3d.Vec_3d cutN = v0.amul(v1);
                            if (cutN.Normalize())
                            {
                                CGeom3d.Plane_PN_3d(isovistCenter, cutN, cutPlane);
                                CGeom3d.Plane_3d sourcePlane = frontTriangle.Plane;
                                CGeom3d.Vec_3d triP0 = new CGeom3d.Vec_3d();
                                CGeom3d.Vec_3d triP1 = new CGeom3d.Vec_3d();
                                CGeom3d.Vec_3d proj0 = new CGeom3d.Vec_3d();
                                CGeom3d.Vec_3d proj1 = new CGeom3d.Vec_3d();

                                //perform cut if necessary
                                foreach (ITriangle tri in allTriangles)
                                {
                                    if (killTriangles.Contains(frontTriangle))
                                        break;
                                    if (killTriangles.Contains(tri))
                                        continue;
                                    if (tri.mOrgId == uint.MaxValue)
                                        continue;

                                    //if (Math.Abs(CGeom3d.Vec_3d.cos(tri.Center - isovistCenter, tri.Normal)) < 0.001)
                                    //    continue;

                                    IBox triBox = new IBox(tri);
                                    triBox.Extend(isovistCenter);

                                    if (!frontBox.AreBoxesTouching(triBox))
                                        continue;

                                    if (tri.mOrgId != polyId)
                                    {
                                        CGeom3d.Line_SS_3d(cutPlane, tri.Plane, cutLine);
                                        if (CGeom3d.Err_3d == 1) //really cutting
                                        {
                                            if (GetTriangleCutPoints(tri, cutPlane, ref triP0, ref triP1) == 2)
                                            {
                                                //project triP0 and triP1 on sourcePlane
                                                CGeom3d.Proj_V_3d(triP0, triP0 - isovistCenter, sourcePlane, proj0);
                                                bool projected0 = (CGeom3d.Err_3d == 1);
                                                CGeom3d.Proj_V_3d(triP1, triP1 - isovistCenter, sourcePlane, proj1);
                                                bool projected1 = (CGeom3d.Err_3d == 1);
                                                if (!projected0 || !projected1)
                                                    continue;

                                                if ( // ((cutP0infinity || cutP1infinity) && !(cutP0infinity && cutP1infinity)) ||
                                                    CGeom3d.InLineWithoutEndPoints_3d(proj0, proj1, p0.mPosition) ||
                                                    CGeom3d.InLineWithoutEndPoints_3d(proj0, proj1, p1.mPosition) ||
                                                    CGeom3d.InLineWithoutEndPoints_3d(p0.mPosition, p1.mPosition, proj0) ||
                                                    CGeom3d.InLineWithoutEndPoints_3d(p0.mPosition, p1.mPosition, proj1) ||
                                                    ((p0.mPosition - proj0).Value() < 0.000001) && (CGeom3d.InLine_3d(proj0, proj1, p1.mPosition) || CGeom3d.InLine_3d(p0.mPosition, p1.mPosition, proj1)) ||
                                                    ((p0.mPosition - proj1).Value() < 0.000001) && (CGeom3d.InLine_3d(proj0, proj1, p1.mPosition) || CGeom3d.InLine_3d(p0.mPosition, p1.mPosition, proj0)) ||
                                                    ((p1.mPosition - proj0).Value() < 0.000001) && (CGeom3d.InLine_3d(proj0, proj1, p0.mPosition) || CGeom3d.InLine_3d(p0.mPosition, p1.mPosition, proj1)) ||
                                                    ((p1.mPosition - proj1).Value() < 0.000001) && (CGeom3d.InLine_3d(proj0, proj1, p0.mPosition) || CGeom3d.InLine_3d(p0.mPosition, p1.mPosition, proj0))) // liegen die beiden Strecken p0-p1 und proj0-proj0 irgendwie ineinander?
                                                {
                                                    // liegen die Strecken triP0-triP1 unf proj0-proj1 auf derselben Seite von isovistCenter
                                                    CGeom3d.Vec_3d vDistTriP0 = triP0 - isovistCenter;
                                                    CGeom3d.Vec_3d vDistTriP1 = triP1 - isovistCenter;
                                                    CGeom3d.Vec_3d vDistProj0 = proj0 - isovistCenter;
                                                    CGeom3d.Vec_3d vDistProj1 = proj1 - isovistCenter;

                                                    double distTriP0 = vDistTriP0.Value();
                                                    double distTriP1 = vDistTriP1.Value();
                                                    double distProj0 = vDistProj0.Value();
                                                    double distProj1 = vDistProj1.Value();

                                                    if (!vDistTriP0.Normalize()) vDistTriP0 = new CGeom3d.Vec_3d(0, 0, 0);
                                                    if (!vDistTriP1.Normalize()) vDistTriP1 = new CGeom3d.Vec_3d(0, 0, 0);
                                                    if (!vDistProj0.Normalize()) vDistProj0 = new CGeom3d.Vec_3d(0, 0, 0);
                                                    if (!vDistProj1.Normalize()) vDistProj1 = new CGeom3d.Vec_3d(0, 0, 0);

                                                    if (((vDistTriP0 + vDistProj0).Value() < 0.1) ||
                                                        ((vDistTriP1 + vDistProj1).Value() < 0.1))
                                                        continue;

                                                    // ist Strecke triP0-triP1 nicht dahinter ? ->continue
                                                    if ((((distTriP0 - distProj0 < -0.000001) && (distTriP1 - distProj1 < 0.000001)) ||
                                                        ((distTriP1 - distProj1 < -0.000001) && (distTriP0 - distProj0 < 0.000001))) && (frontTriangle.mOrgId != uint.MaxValue))
                                                        continue;

                                                    //liegen triP0-triP1 richtig dahinter ? -> splitten
                                                    if (((distTriP0 - distProj0 >= -0.000001) &&
                                                        (distTriP1 - distProj1 >= -0.000001)) || (frontTriangle.mOrgId == uint.MaxValue))
                                                    {
                                                        if (frontTriangle.mOrgId == uint.MaxValue)
                                                        {
                                                            if (SplittTriangle(tri, cutPlane, killTriangles, newTriangles, sortedPoints, null))
                                                            {
                                                                //debugDraw.DrawLine(triP0, triP1, 0, 0, 1, 1);
                                                                somethingChanged = true;
                                                                continue;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (SplittTriangle(tri, cutPlane, killTriangles, newTriangles, sortedPoints, edge))
                                                            {
                                                                //debugDraw.DrawLine(triP0, triP1, 0, 0, 1, 1);
                                                                somethingChanged = true;
                                                                continue;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // kill and make new to allTriangles
                foreach (ITriangle tri in killTriangles)
                {
                    for (int i = 0; i < 3; ++i)
                    {
                        IEdge edge = tri.mEdges[i];
                        edge.mTriangles.Remove(tri);
                        if (edge.mTriangles.Count == 0)
                        {
                            edge.mP1.mEdges.Remove(edge);
                            edge.mP2.mEdges.Remove(edge);
                        }
                    }
                    allTriangles.Remove(tri);
                    octTreeRoot.RemoveTriangle(tri);
                }

                foreach (ITriangle tri in newTriangles)
                {
                    allTriangles.Add(tri);
                    octTreeRoot.AddTriangle(tri);
                }

            } while (somethingChanged);

            // cure all edges
            bool changedCure;
            int cureRuns = 0;
            do 
            {
                changedCure = false;
                cureRuns++;

                HashSet<ITriangle> killTriangles4 = new HashSet<ITriangle>();
                HashSet<ITriangle> newTriangles4 = new HashSet<ITriangle>();
                foreach (ITriangle tri in allTriangles)
                {
                    if (killTriangles4.Contains(tri))
                        continue;

                    HashSet<ITriangle> neighbours = octTreeRoot.GetBoundedTriangles(new IBox(tri));
                    HashSet<IPoint> neighbourPoints = new HashSet<IPoint>();
                    foreach (ITriangle neighbour in neighbours)
                    {
                        neighbourPoints.Add(neighbour.P1);
                        neighbourPoints.Add(neighbour.P2);
                        neighbourPoints.Add(neighbour.P3);
                    }

                    for (int i = 0; i < 3; ++i)
                    {
                        if (killTriangles4.Contains(tri))
                            continue;
                        IEdge edge = tri.mEdges[i];

                        foreach (IPoint point in neighbourPoints)
                        //foreach (KeyValuePair<IPoint, IPoint> valuePair in sortedPoints)
                        {
                            //IPoint point = valuePair.Key;
                            if (!tri.IsInTriangleBoundingBox(point.mPosition, 0.001) || tri.ContainsPoint(point))
                                continue;

                            if (CGeom3d.InLineWithoutEndPoints_3d(edge.mP1.mPosition, edge.mP2.mPosition, point.mPosition, 0.001))
                            {
                                // cure edge
                                killTriangles4.Add(tri);
                                IPoint otherCorner = tri.GetOtherPoint(edge.mP1, edge.mP2);
                                IEdge part0 = AddEdge(edge.mP1, point, edge);
                                IEdge part1 = AddEdge(edge.mP2, point, edge);
                                IEdge diagonal = AddEdge(otherCorner, point, null);
                                part0.mType = edge.mType;
                                part0.CutEdge = edge.CutEdge;
                                part1.mType = edge.mType;
                                part1.CutEdge = edge.CutEdge;
                                diagonal.mType = IEdgeType.eIntern;
                                diagonal.CutEdge = null;
                                ITriangle partTri0 = AddTriangle(part0, diagonal, tri.GetOtherEdge(edge.mP1, edge), tri.mOrgId, tri.Plane);
                                ITriangle partTri1 = AddTriangle(part1, diagonal, tri.GetOtherEdge(edge.mP2, edge), tri.mOrgId, tri.Plane);
                                newTriangles4.Add(partTri0);
                                newTriangles4.Add(partTri1);
                                changedCure = true;
                                break;
                            }
                        }
                    }
                }
            
                // kill and make new to allTriangles
                foreach (ITriangle tri in killTriangles4)
                {
                    for (int i = 0; i < 3; ++i)
                    {
                        IEdge edge = tri.mEdges[i];
                        edge.mTriangles.Remove(tri);
                        if (edge.mTriangles.Count == 0)
                        {
                            edge.mP1.mEdges.Remove(edge);
                            edge.mP2.mEdges.Remove(edge);
                        }
                    }
                    allTriangles.Remove(tri);
                    octTreeRoot.RemoveTriangle(tri);
                }

                foreach (ITriangle tri in newTriangles4)
                {
                    allTriangles.Add(tri);
                    octTreeRoot.AddTriangle(tri);
                }

            } while (changedCure && (cureRuns < 20));
/*
            foreach (ITriangle tri in allTriangles)
            {
                debugDraw.DrawTriangle(tri.P1.mPosition, tri.P2.mPosition, tri.P3.mPosition, 0, 0, 1, 0.2f);

                for (int i = 0; i < 3; ++i)
                {
                    IEdge edge = tri.mEdges[i];
                    if (edge.mTriangles.Count == 1)
                        debugDraw.DrawLine(edge.mP1.mPosition, edge.mP2.mPosition, 1, 0, 0, 1);
                    else
                        debugDraw.DrawLine(edge.mP1.mPosition, edge.mP2.mPosition, 0, 0, 0, 0.2f);
                }
            }
*/

            // generate mesh
            mVolume = 0;
            mSurfaceArea = 0;
            mSurfaceAreaAir = 0;
            mSurfaceAreaWalls = 0;

            //debug
            //return new List<CGeom3d.Triangle>();

            Dictionary<CGeom3d.Triangle, TriangleOutput> meshResult = new Dictionary<CGeom3d.Triangle, TriangleOutput>();
            Dictionary<IEdge, List<MeshInfo>> edgeMeshInfo = new Dictionary<IEdge, List<MeshInfo>>();

            foreach (ITriangle tri in allTriangles)
            {
                if (tri.mOrgId == uint.MaxValue)
                    continue;

                //if (Math.Abs(tri.Area) < 0.00001)
                //    continue;

                meshResult.Add(new CGeom3d.Triangle(tri.P1.mPosition, tri.P2.mPosition, tri.P3.mPosition), new TriangleOutput(TriangleType.Surface, tri.mOrgId));

                //debugDraw.DrawTriangle(tri.P1.mPosition, tri.P2.mPosition, tri.P3.mPosition, 0, 1, 0, 0.2f);

                //calculate volume and surface
                CGeom3d.Vec_3d vs1 = isovistCenter - tri.P1.mPosition;
                CGeom3d.Vec_3d vs2 = tri.P2.mPosition - tri.P1.mPosition;
                CGeom3d.Vec_3d vs3 = tri.P3.mPosition - tri.P1.mPosition;
                CGeom3d.Vec_3d norm = (vs2).amul(vs3);
                double surfaceArea = norm.Value() / 2;
                double volume = (norm * vs1).Value() / 6.0;

                mVolume += volume;
                mSurfaceArea += surfaceArea;
                mSurfaceAreaWalls += surfaceArea;
///*
                // add side triangles
                for (int i = 0; i < 3; ++i)
                {
                    IEdge edge = tri.mEdges[i];

                    if (edge.mType == IEdgeType.eCut)
                    {
                        if ((edge.CutEdge != null) && (edge.mTriangles.Count == 1) && (edge.CutEdge.DoPartsExist))
                        {
                            // project on cut edge
                            IEdge cutEdge = edge.CutEdge; // == nearEdge
                            CGeom3d.Vec_3d edgeP1P = new CGeom3d.Vec_3d(edge.mP1.mPosition.x, edge.mP1.mPosition.y, edge.mP1.mPosition.z);
                            CGeom3d.Vec_3d edgeP2P = new CGeom3d.Vec_3d(edge.mP2.mPosition.x, edge.mP2.mPosition.y, edge.mP2.mPosition.z);

                            CGeom3d.Vec_3d v1 = edgeP1P - isovistCenter;
                            CGeom3d.Vec_3d v2 = edgeP2P - isovistCenter;
                            CGeom3d.Vec_3d nv = v1.amul(v2);
                            nv.Normalize();
                            CGeom3d.Plane_3d nearPlane = new CGeom3d.Plane_3d();
                            CGeom3d.Vec_3d nearN = cutEdge.mLine.v.amul(nv);
                            nearN.Normalize();
                            CGeom3d.Plane_PN_3d(cutEdge.mP1.mPosition, nearN, nearPlane);

                            // project edge on cutEdge
                            CGeom3d.Vec_3d proj1 = new CGeom3d.Vec_3d();
                            CGeom3d.Vec_3d proj2 = new CGeom3d.Vec_3d();
                            CGeom3d.Proj_V_3d(edgeP1P, edgeP1P - isovistCenter, nearPlane, proj1);
                            CGeom3d.Proj_V_3d(edgeP2P, edgeP2P - isovistCenter, nearPlane, proj2);

                            //debugDraw.DrawLine(proj1, edgeP1P, 0, 0, 1, 1);
                            //debugDraw.DrawLine(proj2, edgeP2P, 0, 0, 1, 1);

                            //intersect proj1-proj2 and cutEdge.mP1-cutEdge.mP2 - set new edgeP1-edgeP2 if necessary
                            CGeom3d.Plane_3d distPlane = new CGeom3d.Plane_3d();
                            CGeom3d.Vec_3d lineN = cutEdge.mLine.v;
                            lineN.Normalize();
                            CGeom3d.Plane_PN_3d(cutEdge.mP1.mPosition, lineN, distPlane);
                            double paramProj1 = CGeom3d.Dist_PS_3d(proj1, distPlane);
                            double paramProj2 = CGeom3d.Dist_PS_3d(proj2, distPlane);
                            double paramCutEdgeP1 = CGeom3d.Dist_PS_3d(cutEdge.mP1.mPosition, distPlane);
                            double paramCutEdgeP2 = CGeom3d.Dist_PS_3d(cutEdge.mP2.mPosition, distPlane);

                            if (paramProj1 > paramProj2)
                            {
                                double dummy;
                                dummy = paramProj1;
                                paramProj1 = paramProj2;
                                paramProj2 = dummy;
                            }

                            if (paramCutEdgeP1 > paramCutEdgeP2)
                            {
                                double dummy;
                                dummy = paramCutEdgeP1;
                                paramCutEdgeP1 = paramCutEdgeP2;
                                paramCutEdgeP2 = dummy;
                            }

                            double paramNewP1 = paramCutEdgeP1;
                            double paramNewP2 = paramCutEdgeP2;
                            //Anpassung
                            if ((paramProj1 > paramNewP1) && (paramProj1 - paramNewP2 <= 0.000001)) paramNewP1 = paramProj1;
                            if ((paramProj2 < paramNewP2) && (paramProj2 - paramNewP1 >= -0.000001)) paramNewP2 = paramProj2;

                            proj1 = cutEdge.mP1.mPosition + lineN * paramNewP1;
                            proj2 = cutEdge.mP1.mPosition + lineN * paramNewP2;

                            //project cutEdge proj1-proj2 on edge
                            CGeom3d.Plane_3d farPlane = new CGeom3d.Plane_3d();
                            CGeom3d.Vec_3d farN = edge.mLine.v.amul(nv);
                            farN.Normalize();
                            CGeom3d.Plane_PN_3d(edgeP1P, farN, farPlane);
                            CGeom3d.Proj_V_3d(proj1, proj1 - isovistCenter, farPlane, edgeP1P);
                            CGeom3d.Proj_V_3d(proj2, proj2 - isovistCenter, farPlane, edgeP2P);

                            //debugDraw.DrawLine(edgeP1P, edgeP2P, 0, 1, 0, 1);

                            // Dreiecke bauen
                            //CGeom3d.Proj_V_3d(edgeP1, edgeP1 - isovistCenter, nearPlane, proj1);
                            //CGeom3d.Proj_V_3d(edgeP2, edgeP2 - isovistCenter, nearPlane, proj2);

                            meshResult.Add(new CGeom3d.Triangle(edgeP1P, edgeP2P, proj1), new TriangleOutput(TriangleType.SideAirSurface, 0));

                            //calculate surface
                            vs2 = edgeP2P - edgeP1P;
                            vs3 = proj1 - edgeP1P;
                            norm = (vs2).amul(vs3);
                            surfaceArea = norm.Value() / 2;
                            mSurfaceArea += surfaceArea;
                            mSurfaceAreaAir += surfaceArea;

                            meshResult.Add(new CGeom3d.Triangle(proj1, proj2, edgeP2P), new TriangleOutput(TriangleType.SideAirSurface, 0));

                            //calculate surface
                            vs2 = proj2 - proj1;
                            vs3 = edgeP2P - proj1;
                            norm = (vs2).amul(vs3);
                            surfaceArea = norm.Value() / 2;
                            mSurfaceArea += surfaceArea;
                            mSurfaceAreaAir += surfaceArea;

                            if (!edgeMeshInfo.ContainsKey(cutEdge))
                                edgeMeshInfo[cutEdge] = new List<MeshInfo>();
                            edgeMeshInfo[cutEdge].Add(new MeshInfo(proj1, proj2));
                        }
                    }
                }
//*/
            } 
///*
            foreach (ITriangle tri in allTriangles)
            {
                if (tri.mOrgId == uint.MaxValue)
                    continue;

                //if (Math.Abs(tri.Area) < 0.00001)
                //    continue;

                // add side triangles
                for (int i = 0; i < 3; ++i)
                {
                    IEdge edge = tri.mEdges[i];

                    if (edge.mTriangles.Count == 1)
                    {
                        if (edge.CutEdge == null)
                        {
                            if (!edgeMeshInfo.ContainsKey(edge))
                            {
                                meshResult.Add(new CGeom3d.Triangle(edge.mP1.mPosition, edge.mP2.mPosition, isovistCenter), new TriangleOutput(TriangleType.SideAirSurface, 0));

                                //calculate surface
                                CGeom3d.Vec_3d vs2 = edge.mP2.mPosition - edge.mP1.mPosition;
                                CGeom3d.Vec_3d vs3 = isovistCenter - edge.mP1.mPosition;
                                CGeom3d.Vec_3d norm = (vs2).amul(vs3);
                                double surfaceArea = norm.Value() / 2;
                                mSurfaceArea += surfaceArea;
                                mSurfaceAreaAir += surfaceArea;
                            }
                            else
                            {
                                List<MeshInfo> doneEdges = edgeMeshInfo[edge];
                                HashSet<MeshInfo> triangleEdges = new HashSet<MeshInfo>();
                                triangleEdges.Add(new MeshInfo(edge.mP1.mPosition, edge.mP2.mPosition));

                                CGeom3d.Vec_3d lineNormal = edge.mP1.mPosition - edge.mP2.mPosition;
                                if (!lineNormal.Normalize())
                                    continue;
                                CGeom3d.Plane_3d plane = new CGeom3d.Plane_3d();
                                CGeom3d.Plane_PN_3d(edge.mP1.mPosition, lineNormal, plane);
                                if (CGeom3d.Dist_PS_3d(edge.mP2.mPosition, plane) < 0)
                                {
                                    lineNormal = lineNormal * -1;
                                    CGeom3d.Plane_PN_3d(edge.mP1.mPosition, lineNormal, plane);
                                }

                                // cut out done Edges
                                foreach (MeshInfo doneEdge in doneEdges)
                                {
                                    MeshInfo toCutEdge = null;
                                    foreach (MeshInfo triangleEdge in triangleEdges)
                                    {
                                        if (CGeom3d.InLine_3d(triangleEdge.mP1, triangleEdge.mP2, doneEdge.mP1) &&
                                            CGeom3d.InLine_3d(triangleEdge.mP1, triangleEdge.mP2, doneEdge.mP2))
                                        {
                                            toCutEdge = triangleEdge;
                                            break;
                                        }
                                    }

                                    if (toCutEdge != null)
                                    {
                                        // perform cut
                                        triangleEdges.Remove(toCutEdge);

                                        double paramToCutEdge1 = CGeom3d.Dist_PS_3d(toCutEdge.mP1, plane);
                                        double paramToCutEdge2 = CGeom3d.Dist_PS_3d(toCutEdge.mP2, plane);
                                        double paramDoneEdge1 = CGeom3d.Dist_PS_3d(doneEdge.mP1, plane);
                                        double paramDoneEdge2 = CGeom3d.Dist_PS_3d(doneEdge.mP2, plane);

                                        if ((Math.Abs(paramToCutEdge1 - paramDoneEdge1) <= 0.000001) && (Math.Abs(paramToCutEdge2 - paramDoneEdge2) > 0.000001))
                                        {
                                            // new line from paramToCutEdge2 to paramDoneEdge2
                                            CGeom3d.Vec_3d newP1 = edge.mP1.mPosition + lineNormal * paramToCutEdge2;
                                            CGeom3d.Vec_3d newP2 = edge.mP1.mPosition + lineNormal * paramDoneEdge2;
                                            triangleEdges.Add(new MeshInfo(newP1, newP2));
                                        }
                                        else if ((Math.Abs(paramToCutEdge1 - paramDoneEdge2) <= 0.000001) && (Math.Abs(paramToCutEdge2 - paramDoneEdge1) > 0.000001))
                                        {
                                            // new line from paramToCutEdge2 to paramDoneEdge1
                                            CGeom3d.Vec_3d newP1 = edge.mP1.mPosition + lineNormal * paramToCutEdge2;
                                            CGeom3d.Vec_3d newP2 = edge.mP1.mPosition + lineNormal * paramDoneEdge1;
                                            triangleEdges.Add(new MeshInfo(newP1, newP2));
                                        }
                                        else if ((Math.Abs(paramToCutEdge2 - paramDoneEdge1) <= 0.000001) && (Math.Abs(paramToCutEdge1 - paramDoneEdge2) > 0.000001))
                                        {
                                            // new line from paramToCutEdge1 to paramDoneEdge2
                                            CGeom3d.Vec_3d newP1 = edge.mP1.mPosition + lineNormal * paramToCutEdge1;
                                            CGeom3d.Vec_3d newP2 = edge.mP1.mPosition + lineNormal * paramDoneEdge2;
                                            triangleEdges.Add(new MeshInfo(newP1, newP2));
                                        }
                                        else if ((Math.Abs(paramToCutEdge2 - paramDoneEdge2) <= 0.000001) && (Math.Abs(paramToCutEdge1 - paramDoneEdge1) > 0.000001))
                                        {
                                            // new line from paramToCutEdge1 to paramDoneEdge1
                                            CGeom3d.Vec_3d newP1 = edge.mP1.mPosition + lineNormal * paramToCutEdge1;
                                            CGeom3d.Vec_3d newP2 = edge.mP1.mPosition + lineNormal * paramDoneEdge1;
                                            triangleEdges.Add(new MeshInfo(newP1, newP2));
                                        }
                                        else if ((Math.Abs(paramToCutEdge1 - paramDoneEdge1) > 0.000001) && (Math.Abs(paramToCutEdge2 - paramDoneEdge2) > 0.000001) &&
                                                 (Math.Abs(paramToCutEdge1 - paramDoneEdge2) > 0.000001) && (Math.Abs(paramToCutEdge2 - paramDoneEdge1) > 0.000001))
                                        {
                                            // two new lines
                                            if (paramDoneEdge1 > paramDoneEdge2)
                                            {
                                                double dummy = paramDoneEdge1;
                                                paramDoneEdge1 = paramDoneEdge2;
                                                paramDoneEdge2 = dummy;
                                            }
                                            if (paramToCutEdge1 > paramToCutEdge2)
                                            {
                                                double dummy = paramToCutEdge1;
                                                paramToCutEdge1 = paramToCutEdge2;
                                                paramToCutEdge2 = dummy;
                                            }

                                            CGeom3d.Vec_3d newP1 = edge.mP1.mPosition + lineNormal * paramToCutEdge1;
                                            CGeom3d.Vec_3d newP2 = edge.mP1.mPosition + lineNormal * paramDoneEdge1;
                                            triangleEdges.Add(new MeshInfo(newP1, newP2));
                                            newP1 = edge.mP1.mPosition + lineNormal * paramDoneEdge2;
                                            newP2 = edge.mP1.mPosition + lineNormal * paramToCutEdge2;
                                            triangleEdges.Add(new MeshInfo(newP1, newP2));
                                        }

                                    }
                                }

                                // output triangleEdges
                                foreach (MeshInfo triangleEdge in triangleEdges)
                                //if (CGeom3d.InLine_3d(edge.mP1.mPosition, edge.mP2.mPosition, triangleEdge.mP1) &&
                                //    CGeom3d.InLine_3d(edge.mP1.mPosition, edge.mP2.mPosition, triangleEdge.mP2))
                                {
                                    meshResult.Add(new CGeom3d.Triangle(triangleEdge.mP1, triangleEdge.mP2, isovistCenter), new TriangleOutput(TriangleType.SideAirSurface, 0));

                                    //calculate surface
                                    CGeom3d.Vec_3d vs2 = triangleEdge.mP2 - triangleEdge.mP1;
                                    CGeom3d.Vec_3d vs3 = isovistCenter - triangleEdge.mP1;
                                    CGeom3d.Vec_3d norm = (vs2).amul(vs3);
                                    double surfaceArea = norm.Value() / 2;
                                    mSurfaceArea += surfaceArea;
                                    mSurfaceAreaAir += surfaceArea;

                                }
                            }
                        }
                    }
                }
            }
 //*/
            //buggy workaround
            //KillLonelyTriangleEdges(ref meshResult, debugDraw);

            return meshResult;
        }

        private void KillLonelyTriangleEdges(ref Dictionary<CGeom3d.Triangle, TriangleType> meshResult, IDebugDraw debugDraw)
        {
            System.Collections.Generic.SortedDictionary<IPoint, IPoint> sortedPoints = new System.Collections.Generic.SortedDictionary<IPoint, IPoint>(new IPointCompare(0.001));
            HashSet<ITriangle> tris = new HashSet<ITriangle>();

            foreach (KeyValuePair<CGeom3d.Triangle, TriangleType> triangle in meshResult)
            {
                IPoint i0 = AddPoint(triangle.Key.m_pP1, sortedPoints);
                IPoint i1 = AddPoint(triangle.Key.m_pP2, sortedPoints);
                IPoint i2 = AddPoint(triangle.Key.m_pP3, sortedPoints);
                if ((i0 == i1) || (i0==i2) || (i1==i2))
                    continue;
                IEdge e0 = AddEdge(i0, i1, null);
                IEdge e1 = AddEdge(i1, i2, null);
                IEdge e2 = AddEdge(i2, i0, null);
                ITriangle t = AddTriangle(e0, e1, e2, (uint)triangle.Value, null);
                tris.Add(t);
            }

            // cure all edges
            bool changedCure;
            int cureRuns = 0;
            do
            {
                changedCure = false;
                cureRuns++;

                HashSet<ITriangle> killTriangles4 = new HashSet<ITriangle>();
                HashSet<ITriangle> newTriangles4 = new HashSet<ITriangle>();
                foreach (ITriangle tri in tris)
                {
                    if (killTriangles4.Contains(tri))
                        continue;
                    for (int i = 0; i < 3; ++i)
                    {
                        if (killTriangles4.Contains(tri))
                            continue;
                        IEdge edge = tri.mEdges[i];

                        foreach (KeyValuePair<IPoint, IPoint> valuePair in sortedPoints)
                        {
                            IPoint point = valuePair.Key;
                            if (!tri.IsInTriangleBoundingBox(point.mPosition, 0.001) || tri.ContainsPoint(point))
                                continue;

                            if (CGeom3d.InLineWithoutEndPoints_3d(edge.mP1.mPosition, edge.mP2.mPosition, point.mPosition, 0.001))
                            {
                                // cure edge
                                killTriangles4.Add(tri);
                                IPoint otherCorner = tri.GetOtherPoint(edge.mP1, edge.mP2);
                                IEdge part0 = AddEdge(edge.mP1, point, edge);
                                IEdge part1 = AddEdge(edge.mP2, point, edge);
                                IEdge diagonal = AddEdge(otherCorner, point, null);
                                part0.mType = edge.mType;
                                part0.CutEdge = edge.CutEdge;
                                part1.mType = edge.mType;
                                part1.CutEdge = edge.CutEdge;
                                diagonal.mType = IEdgeType.eIntern;
                                diagonal.CutEdge = null;
                                ITriangle partTri0 = AddTriangle(part0, diagonal, tri.GetOtherEdge(edge.mP1, edge), tri.mOrgId, null);
                                ITriangle partTri1 = AddTriangle(part1, diagonal, tri.GetOtherEdge(edge.mP2, edge), tri.mOrgId, null);
                                newTriangles4.Add(partTri0);
                                newTriangles4.Add(partTri1);
                                changedCure = true;
                                break;
                            }
                        }
                    }
                }

                // kill and make new to allTriangles
                foreach (ITriangle tri in killTriangles4)
                {
                    for (int i = 0; i < 3; ++i)
                    {
                        IEdge edge = tri.mEdges[i];
                        edge.mTriangles.Remove(tri);
                        if (edge.mTriangles.Count == 0)
                        {
                            edge.mP1.mEdges.Remove(edge);
                            edge.mP2.mEdges.Remove(edge);
                        }
                    }
                    tris.Remove(tri);
                }

                foreach (ITriangle tri in newTriangles4)
                    tris.Add(tri);

            } while (changedCure && (cureRuns < 20));

            bool killed;
            int killRun = 0;
            do
            {
                killed = false;
                ++killRun;
                HashSet<ITriangle> killList = new HashSet<ITriangle>();

                foreach (ITriangle tri in tris)
                {
                    int missingBorderConnections = 0;
                    for (int i = 0; i < 3; ++i)
                    {
                        IEdge edge = tri.mEdges[i];
                        if (edge.mTriangles.Count <= 1)
                            ++missingBorderConnections;
                    }
                    if (missingBorderConnections == 3)
                    {
                        killList.Add(tri);
                        killed = true;
                    }
                }
/*
                if ((killRun == 1) && (killList.Count > 0))
                {
                    foreach (ITriangle tri in tris)
                    {
                        if (killList.Contains(tri))
                            debugDraw.DrawTriangle(tri.P1.mPosition, tri.P2.mPosition, tri.P3.mPosition, 1, 0, 0, 1);
                        else
                            debugDraw.DrawTriangle(tri.P1.mPosition, tri.P2.mPosition, tri.P3.mPosition, 0, 0, 1, 0.2f);

                        debugDraw.DrawLine(tri.P1.mPosition, tri.P2.mPosition, 0, 0, 0, 1);
                        debugDraw.DrawLine(tri.P2.mPosition, tri.P3.mPosition, 0, 0, 0, 1);
                        debugDraw.DrawLine(tri.P3.mPosition, tri.P1.mPosition, 0, 0, 0, 1);
                    }
                        
                }
*/
                foreach (ITriangle tri in killList)
                {
                    for (int i = 0; i < 3; ++i)
                    {
                        IEdge edge = tri.mEdges[i];
                        edge.mTriangles.Remove(tri);
                        if (edge.mTriangles.Count == 0)
                        {
                            edge.mP1.mEdges.Remove(edge);
                            edge.mP2.mEdges.Remove(edge);
                        }
                    }
                    tris.Remove(tri);
                }
            } while (killed);

            meshResult = new Dictionary<CGeom3d.Triangle, TriangleType>();
            foreach (ITriangle tri in tris)
                meshResult.Add(new CGeom3d.Triangle(tri.P1.mPosition, tri.P2.mPosition, tri.P3.mPosition), (TriangleType)tri.mOrgId);
        }
    }
}

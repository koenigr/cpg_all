﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = ControlValues.cs 
//  Copyright (C) 2014/10/18  8:25 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;

namespace CPlan.Kremlas
{
    public struct ControlValues
    {

        public Double SizeRate;
        public Double OldSizeRate;
        public Double MoveRate;
        public Double OldMoveRate;

        public ControlValues(double sizeRate, double oldSizeRate, double moveRate, double oldMoveRate)
        {
            this.SizeRate = sizeRate;
            this.OldSizeRate = oldSizeRate;
            this.MoveRate = moveRate;
            this.OldMoveRate = oldMoveRate;
        }
        public override string ToString()
        {
            return (String.Format("({0}, {1}, {2}, {3})", SizeRate, OldSizeRate, MoveRate, OldMoveRate));
        }

    }
}

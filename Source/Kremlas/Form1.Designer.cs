﻿namespace CPlan.Kremlas
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbl_Time = new System.Windows.Forms.Label();
            this.bStop = new System.Windows.Forms.Button();
            this.cB_testScenario = new System.Windows.Forms.CheckBox();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.bStart = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rB_8Rooms = new System.Windows.Forms.RadioButton();
            this.rB_10Rooms = new System.Windows.Forms.RadioButton();
            this.cB_showTopo = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.uD_sDamp = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.uD_propRate = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.uD_propForce = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.uD_repRate = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.uD_repForce = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.uD_p = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.uD_RLap = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.lbl_succsess = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.uD_nr = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbl_fitOverl = new System.Windows.Forms.Label();
            this.lbl_Generation = new System.Windows.Forms.Label();
            this.lbl_fitDist = new System.Windows.Forms.Label();
            this.lbl_move = new System.Windows.Forms.Label();
            this.cb_Topo = new System.Windows.Forms.CheckBox();
            this.cB_actiCross = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cB_elite = new System.Windows.Forms.CheckBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.rb_plus = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.uD_crossRate = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.uD_pressure = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.uD_rooms = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.uD_population = new System.Windows.Forms.NumericUpDown();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_sDamp)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_propRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_propForce)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_repRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_repForce)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_p)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_RLap)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_nr)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_crossRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_pressure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_rooms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_population)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.pictureBox1);
            this.splitContainer1.Panel1.Margin = new System.Windows.Forms.Padding(5);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel2);
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(1235, 618);
            this.splitContainer1.SplitterDistance = 620;
            this.splitContainer1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(58, 75);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(500, 430);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint_1);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown_1);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove_1);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp_1);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.lbl_Time);
            this.panel2.Controls.Add(this.bStop);
            this.panel2.Controls.Add(this.cB_testScenario);
            this.panel2.Controls.Add(this.chart2);
            this.panel2.Controls.Add(this.button4);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.chart1);
            this.panel2.Controls.Add(this.bStart);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(162, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(447, 616);
            this.panel2.TabIndex = 7;
            // 
            // lbl_Time
            // 
            this.lbl_Time.AutoSize = true;
            this.lbl_Time.Location = new System.Drawing.Point(216, 42);
            this.lbl_Time.Name = "lbl_Time";
            this.lbl_Time.Size = new System.Drawing.Size(37, 13);
            this.lbl_Time.TabIndex = 30;
            this.lbl_Time.Text = "00000";
            // 
            // bStop
            // 
            this.bStop.Location = new System.Drawing.Point(93, 11);
            this.bStop.Name = "bStop";
            this.bStop.Size = new System.Drawing.Size(75, 23);
            this.bStop.TabIndex = 29;
            this.bStop.Text = "stop";
            this.bStop.UseVisualStyleBackColor = true;
            this.bStop.Click += new System.EventHandler(this.bStop_Click);
            // 
            // cB_testScenario
            // 
            this.cB_testScenario.AutoSize = true;
            this.cB_testScenario.Location = new System.Drawing.Point(96, 41);
            this.cB_testScenario.Name = "cB_testScenario";
            this.cB_testScenario.Size = new System.Drawing.Size(89, 17);
            this.cB_testScenario.TabIndex = 28;
            this.cB_testScenario.Text = "TestScenario";
            this.cB_testScenario.UseVisualStyleBackColor = true;
            this.cB_testScenario.CheckedChanged += new System.EventHandler(this.cB_testScenario_CheckedChanged);
            // 
            // chart2
            // 
            this.chart2.BorderlineColor = System.Drawing.Color.Black;
            this.chart2.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea5.AlignmentOrientation = System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Horizontal;
            chartArea5.AxisX.Maximum = 100000D;
            chartArea5.AxisX.Minimum = 0D;
            chartArea5.AxisX.Title = "overlappings";
            chartArea5.AxisY.Maximum = 200D;
            chartArea5.AxisY.Minimum = 0D;
            chartArea5.AxisY.Title = "distances";
            chartArea5.Name = "Pareto-Front";
            this.chart2.ChartAreas.Add(chartArea5);
            this.chart2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.chart2.Location = new System.Drawing.Point(0, 62);
            this.chart2.Name = "chart2";
            series9.ChartArea = "Pareto-Front";
            series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastPoint;
            series9.MarkerColor = System.Drawing.Color.DarkGreen;
            series9.MarkerImageTransparentColor = System.Drawing.Color.Green;
            series9.MarkerSize = 8;
            series9.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series9.Name = "seriesPareto";
            series9.YValuesPerPoint = 2;
            series10.ChartArea = "Pareto-Front";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastPoint;
            series10.MarkerColor = System.Drawing.Color.Black;
            series10.MarkerSize = 4;
            series10.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series10.Name = "seriesChilds";
            this.chart2.Series.Add(series9);
            this.chart2.Series.Add(series10);
            this.chart2.Size = new System.Drawing.Size(443, 321);
            this.chart2.TabIndex = 27;
            this.chart2.Text = "chart2";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(15, 35);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 26;
            this.button4.Text = "single step";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(313, 11);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 12;
            this.button3.Text = "analyse";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // chart1
            // 
            this.chart1.BorderlineColor = System.Drawing.Color.Black;
            this.chart1.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea6.AlignmentOrientation = System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Horizontal;
            chartArea6.AxisX.Title = "generations";
            chartArea6.AxisY.Title = "fitness";
            chartArea6.AxisY2.Title = "overlappings";
            chartArea6.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea6);
            this.chart1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.chart1.Location = new System.Drawing.Point(0, 383);
            this.chart1.Name = "chart1";
            series11.BorderColor = System.Drawing.Color.Blue;
            series11.BorderWidth = 2;
            series11.ChartArea = "ChartArea1";
            series11.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series11.Color = System.Drawing.Color.Blue;
            series11.MarkerColor = System.Drawing.Color.Blue;
            series11.Name = "sizeRate";
            series11.YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
            series11.YValuesPerPoint = 32;
            series12.BorderColor = System.Drawing.Color.DarkGray;
            series12.ChartArea = "ChartArea1";
            series12.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Range;
            series12.Color = System.Drawing.Color.Gold;
            series12.Name = "fitness";
            series12.YValuesPerPoint = 2;
            this.chart1.Series.Add(series11);
            this.chart1.Series.Add(series12);
            this.chart1.Size = new System.Drawing.Size(443, 229);
            this.chart1.TabIndex = 3;
            this.chart1.Text = "chart1";
            // 
            // bStart
            // 
            this.bStart.Location = new System.Drawing.Point(15, 10);
            this.bStart.Name = "bStart";
            this.bStart.Size = new System.Drawing.Size(75, 23);
            this.bStart.TabIndex = 6;
            this.bStart.Text = "start";
            this.bStart.UseVisualStyleBackColor = true;
            this.bStart.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(178, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "reset";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.cB_showTopo);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.uD_sDamp);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.uD_p);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.uD_RLap);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.cb_Topo);
            this.panel1.Controls.Add(this.cB_actiCross);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.uD_crossRate);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.uD_pressure);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.uD_rooms);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.uD_population);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(162, 616);
            this.panel1.TabIndex = 6;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rB_8Rooms);
            this.groupBox4.Controls.Add(this.rB_10Rooms);
            this.groupBox4.Location = new System.Drawing.Point(7, 295);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(145, 61);
            this.groupBox4.TabIndex = 35;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "groupBox4";
            // 
            // rB_8Rooms
            // 
            this.rB_8Rooms.Checked = true;
            this.rB_8Rooms.Location = new System.Drawing.Point(10, 19);
            this.rB_8Rooms.Name = "rB_8Rooms";
            this.rB_8Rooms.Size = new System.Drawing.Size(67, 17);
            this.rB_8Rooms.TabIndex = 28;
            this.rB_8Rooms.TabStop = true;
            this.rB_8Rooms.Text = "8 Rooms";
            this.rB_8Rooms.UseVisualStyleBackColor = true;
            this.rB_8Rooms.CheckedChanged += new System.EventHandler(this.rB_8Rooms_CheckedChanged);
            // 
            // rB_10Rooms
            // 
            this.rB_10Rooms.AutoSize = true;
            this.rB_10Rooms.Location = new System.Drawing.Point(10, 36);
            this.rB_10Rooms.Name = "rB_10Rooms";
            this.rB_10Rooms.Size = new System.Drawing.Size(73, 17);
            this.rB_10Rooms.TabIndex = 27;
            this.rB_10Rooms.Text = "10 Rooms";
            this.rB_10Rooms.UseVisualStyleBackColor = true;
            this.rB_10Rooms.CheckedChanged += new System.EventHandler(this.rB_10Rooms_CheckedChanged);
            // 
            // cB_showTopo
            // 
            this.cB_showTopo.AutoSize = true;
            this.cB_showTopo.Location = new System.Drawing.Point(17, 437);
            this.cB_showTopo.Name = "cB_showTopo";
            this.cB_showTopo.Size = new System.Drawing.Size(94, 17);
            this.cB_showTopo.TabIndex = 34;
            this.cB_showTopo.Text = "show topology";
            this.cB_showTopo.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(18, 462);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 13);
            this.label17.TabIndex = 33;
            this.label17.Text = "spring Damp:";
            // 
            // uD_sDamp
            // 
            this.uD_sDamp.Location = new System.Drawing.Point(91, 460);
            this.uD_sDamp.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.uD_sDamp.Name = "uD_sDamp";
            this.uD_sDamp.Size = new System.Drawing.Size(53, 20);
            this.uD_sDamp.TabIndex = 32;
            this.uD_sDamp.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.uD_sDamp.ValueChanged += new System.EventHandler(this.uD_population_ValueChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.uD_propRate);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.uD_propForce);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.uD_repRate);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.uD_repForce);
            this.groupBox3.Location = new System.Drawing.Point(7, 182);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(145, 112);
            this.groupBox3.TabIndex = 31;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Strategy Parameter";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 89);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(69, 13);
            this.label15.TabIndex = 21;
            this.label15.Text = "proport.Rate:";
            // 
            // uD_propRate
            // 
            this.uD_propRate.Location = new System.Drawing.Point(84, 87);
            this.uD_propRate.Name = "uD_propRate";
            this.uD_propRate.Size = new System.Drawing.Size(53, 20);
            this.uD_propRate.TabIndex = 20;
            this.uD_propRate.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.uD_propRate.ValueChanged += new System.EventHandler(this.uD_population_ValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 67);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 13);
            this.label16.TabIndex = 19;
            this.label16.Text = "proport.Force:";
            // 
            // uD_propForce
            // 
            this.uD_propForce.Location = new System.Drawing.Point(84, 65);
            this.uD_propForce.Name = "uD_propForce";
            this.uD_propForce.Size = new System.Drawing.Size(53, 20);
            this.uD_propForce.TabIndex = 18;
            this.uD_propForce.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.uD_propForce.ValueChanged += new System.EventHandler(this.uD_population_ValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(21, 47);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 13);
            this.label14.TabIndex = 17;
            this.label14.Text = "repel Rate:";
            // 
            // uD_repRate
            // 
            this.uD_repRate.Location = new System.Drawing.Point(84, 45);
            this.uD_repRate.Name = "uD_repRate";
            this.uD_repRate.Size = new System.Drawing.Size(53, 20);
            this.uD_repRate.TabIndex = 16;
            this.uD_repRate.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.uD_repRate.ValueChanged += new System.EventHandler(this.uD_population_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 13);
            this.label13.TabIndex = 15;
            this.label13.Text = "repel Force:";
            // 
            // uD_repForce
            // 
            this.uD_repForce.Location = new System.Drawing.Point(84, 23);
            this.uD_repForce.Name = "uD_repForce";
            this.uD_repForce.Size = new System.Drawing.Size(53, 20);
            this.uD_repForce.TabIndex = 14;
            this.uD_repForce.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.uD_repForce.ValueChanged += new System.EventHandler(this.uD_population_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 89);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 13);
            this.label12.TabIndex = 30;
            this.label12.Text = "crossover p:";
            // 
            // uD_p
            // 
            this.uD_p.Location = new System.Drawing.Point(91, 87);
            this.uD_p.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.uD_p.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_p.Name = "uD_p";
            this.uD_p.Size = new System.Drawing.Size(53, 20);
            this.uD_p.TabIndex = 29;
            this.uD_p.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.uD_p.ValueChanged += new System.EventHandler(this.uD_population_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 28;
            this.label2.Text = "Rooms Overlap:";
            // 
            // uD_RLap
            // 
            this.uD_RLap.Location = new System.Drawing.Point(91, 112);
            this.uD_RLap.Name = "uD_RLap";
            this.uD_RLap.Size = new System.Drawing.Size(53, 20);
            this.uD_RLap.TabIndex = 27;
            this.uD_RLap.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.uD_RLap.ValueChanged += new System.EventHandler(this.uD_population_ValueChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.lbl_succsess);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.uD_nr);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.lbl_fitOverl);
            this.groupBox2.Controls.Add(this.lbl_Generation);
            this.groupBox2.Controls.Add(this.lbl_fitDist);
            this.groupBox2.Controls.Add(this.lbl_move);
            this.groupBox2.Location = new System.Drawing.Point(7, 483);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(145, 129);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "info";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 85);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "successRate.:";
            // 
            // lbl_succsess
            // 
            this.lbl_succsess.AutoSize = true;
            this.lbl_succsess.Location = new System.Drawing.Point(85, 85);
            this.lbl_succsess.Name = "lbl_succsess";
            this.lbl_succsess.Size = new System.Drawing.Size(37, 13);
            this.lbl_succsess.TabIndex = 20;
            this.lbl_succsess.Text = "00000";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Show Nr.:";
            // 
            // uD_nr
            // 
            this.uD_nr.Location = new System.Drawing.Point(88, 101);
            this.uD_nr.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.uD_nr.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_nr.Name = "uD_nr";
            this.uD_nr.Size = new System.Drawing.Size(45, 20);
            this.uD_nr.TabIndex = 18;
            this.uD_nr.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_nr.ValueChanged += new System.EventHandler(this.uD_nr_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(39, 18);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "FitOverl:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(23, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Generation:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(46, 35);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "FitDist:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(19, 69);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "strat.Param.:";
            // 
            // lbl_fitOverl
            // 
            this.lbl_fitOverl.AutoSize = true;
            this.lbl_fitOverl.Location = new System.Drawing.Point(85, 18);
            this.lbl_fitOverl.Name = "lbl_fitOverl";
            this.lbl_fitOverl.Size = new System.Drawing.Size(37, 13);
            this.lbl_fitOverl.TabIndex = 7;
            this.lbl_fitOverl.Text = "00000";
            // 
            // lbl_Generation
            // 
            this.lbl_Generation.AutoSize = true;
            this.lbl_Generation.Location = new System.Drawing.Point(85, 52);
            this.lbl_Generation.Name = "lbl_Generation";
            this.lbl_Generation.Size = new System.Drawing.Size(37, 13);
            this.lbl_Generation.TabIndex = 9;
            this.lbl_Generation.Text = "00000";
            // 
            // lbl_fitDist
            // 
            this.lbl_fitDist.AutoSize = true;
            this.lbl_fitDist.Location = new System.Drawing.Point(85, 35);
            this.lbl_fitDist.Name = "lbl_fitDist";
            this.lbl_fitDist.Size = new System.Drawing.Size(37, 13);
            this.lbl_fitDist.TabIndex = 10;
            this.lbl_fitDist.Text = "00000";
            // 
            // lbl_move
            // 
            this.lbl_move.AutoSize = true;
            this.lbl_move.Location = new System.Drawing.Point(85, 69);
            this.lbl_move.Name = "lbl_move";
            this.lbl_move.Size = new System.Drawing.Size(37, 13);
            this.lbl_move.TabIndex = 11;
            this.lbl_move.Text = "00000";
            // 
            // cb_Topo
            // 
            this.cb_Topo.AutoSize = true;
            this.cb_Topo.Checked = true;
            this.cb_Topo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_Topo.Location = new System.Drawing.Point(17, 418);
            this.cb_Topo.Name = "cb_Topo";
            this.cb_Topo.Size = new System.Drawing.Size(107, 17);
            this.cb_Topo.TabIndex = 25;
            this.cb_Topo.Text = "activate topology";
            this.cb_Topo.UseVisualStyleBackColor = true;
            // 
            // cB_actiCross
            // 
            this.cB_actiCross.Checked = true;
            this.cB_actiCross.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cB_actiCross.Location = new System.Drawing.Point(17, 159);
            this.cB_actiCross.Name = "cB_actiCross";
            this.cB_actiCross.Size = new System.Drawing.Size(113, 17);
            this.cB_actiCross.TabIndex = 24;
            this.cB_actiCross.Text = "activate crossover";
            this.cB_actiCross.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cB_elite);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.rb_plus);
            this.groupBox1.Location = new System.Drawing.Point(7, 362);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(145, 50);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Method";
            // 
            // cB_elite
            // 
            this.cB_elite.AutoSize = true;
            this.cB_elite.Location = new System.Drawing.Point(79, 30);
            this.cB_elite.Name = "cB_elite";
            this.cB_elite.Size = new System.Drawing.Size(46, 17);
            this.cB_elite.TabIndex = 23;
            this.cB_elite.Text = "Elite";
            this.cB_elite.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(10, 30);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(71, 17);
            this.radioButton2.TabIndex = 22;
            this.radioButton2.Text = "(μ, λ)- ES ";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // rb_plus
            // 
            this.rb_plus.AutoSize = true;
            this.rb_plus.Checked = true;
            this.rb_plus.Location = new System.Drawing.Point(10, 15);
            this.rb_plus.Name = "rb_plus";
            this.rb_plus.Size = new System.Drawing.Size(68, 17);
            this.rb_plus.TabIndex = 21;
            this.rb_plus.TabStop = true;
            this.rb_plus.Text = "(μ+λ)- ES";
            this.rb_plus.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 139);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Cross-Rate %:";
            // 
            // uD_crossRate
            // 
            this.uD_crossRate.Location = new System.Drawing.Point(91, 137);
            this.uD_crossRate.Name = "uD_crossRate";
            this.uD_crossRate.Size = new System.Drawing.Size(53, 20);
            this.uD_crossRate.TabIndex = 18;
            this.uD_crossRate.Value = new decimal(new int[] {
            75,
            0,
            0,
            0});
            this.uD_crossRate.ValueChanged += new System.EventHandler(this.uD_population_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Pressure: 1/";
            // 
            // uD_pressure
            // 
            this.uD_pressure.Location = new System.Drawing.Point(91, 62);
            this.uD_pressure.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.uD_pressure.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_pressure.Name = "uD_pressure";
            this.uD_pressure.Size = new System.Drawing.Size(53, 20);
            this.uD_pressure.TabIndex = 16;
            this.uD_pressure.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.uD_pressure.ValueChanged += new System.EventHandler(this.uD_population_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(44, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Rooms:";
            // 
            // uD_rooms
            // 
            this.uD_rooms.Location = new System.Drawing.Point(91, 37);
            this.uD_rooms.Name = "uD_rooms";
            this.uD_rooms.Size = new System.Drawing.Size(53, 20);
            this.uD_rooms.TabIndex = 14;
            this.uD_rooms.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.uD_rooms.ValueChanged += new System.EventHandler(this.uD_population_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Population:";
            // 
            // uD_population
            // 
            this.uD_population.Location = new System.Drawing.Point(91, 12);
            this.uD_population.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.uD_population.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_population.Name = "uD_population";
            this.uD_population.Size = new System.Drawing.Size(53, 20);
            this.uD_population.TabIndex = 12;
            this.uD_population.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_population.ValueChanged += new System.EventHandler(this.uD_population_ValueChanged);
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1235, 618);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.Text = "Packing";
            this.Activated += new System.EventHandler(this.Form1_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_sDamp)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_propRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_propForce)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_repRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_repForce)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_p)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_RLap)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_nr)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_crossRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_pressure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_rooms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_population)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button bStart;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        protected internal System.Windows.Forms.NumericUpDown uD_pressure;
        protected internal System.Windows.Forms.RadioButton rb_plus;
        protected internal System.Windows.Forms.NumericUpDown uD_crossRate;
        protected internal System.Windows.Forms.RadioButton radioButton2;
        protected internal System.Windows.Forms.CheckBox cB_elite;
        protected internal System.Windows.Forms.CheckBox cB_actiCross;
        protected internal System.Windows.Forms.NumericUpDown uD_rooms;
        protected internal System.Windows.Forms.NumericUpDown uD_population;
        protected internal System.Windows.Forms.CheckBox cb_Topo;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Label lbl_fitOverl;
        internal System.Windows.Forms.Label lbl_Generation;
        internal System.Windows.Forms.Label lbl_fitDist;
        internal System.Windows.Forms.Label lbl_move;
        internal System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        internal System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.Label label1;
        protected internal System.Windows.Forms.NumericUpDown uD_nr;
        internal System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        internal System.Windows.Forms.NumericUpDown uD_RLap;
        internal System.Windows.Forms.Label label11;
        internal System.Windows.Forms.Label lbl_succsess;
        private System.Windows.Forms.Label label12;
        protected internal System.Windows.Forms.NumericUpDown uD_p;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label13;
        protected internal System.Windows.Forms.NumericUpDown uD_repForce;
        private System.Windows.Forms.Label label15;
        protected internal System.Windows.Forms.NumericUpDown uD_propRate;
        private System.Windows.Forms.Label label16;
        protected internal System.Windows.Forms.NumericUpDown uD_propForce;
        private System.Windows.Forms.Label label14;
        protected internal System.Windows.Forms.NumericUpDown uD_repRate;
        private System.Windows.Forms.Label label17;
        protected internal System.Windows.Forms.NumericUpDown uD_sDamp;
        protected internal System.Windows.Forms.CheckBox cB_testScenario;
        private System.Windows.Forms.Button bStop;
        internal System.Windows.Forms.Label lbl_Time;
        protected internal System.Windows.Forms.CheckBox cB_showTopo;
        private System.Windows.Forms.GroupBox groupBox4;
        protected internal System.Windows.Forms.RadioButton rB_8Rooms;
        protected internal System.Windows.Forms.RadioButton rB_10Rooms;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
    }
}


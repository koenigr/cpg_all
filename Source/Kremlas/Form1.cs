﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Form1.cs 
//  Copyright (C) 2014/10/18  8:25 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CPlan.Kremlas
{
    public partial class Form1 : Form
    {
        public REvo[] EA;
        //private REvo SecEA;
        public int Generations = 0;
        public static RTools myTools = new RTools();

        private List<Label> m_idLabels = new List<Label>();
        private Rectangle m_canvas;
        private int m_popSize;//popSizePos;//, m_popSize;// = 50;
        private int m_nrRooms = 10;

        public Thread workerThread = null;
        private volatile bool needToStop = false;
       
        //----------------------------------------------------------------------------------------------
        public Form1()
        {
            InitializeComponent();
            Initialse();
        }

        //==============================================================================================
        //==============================================================================================
        public void Initialse()
        {
            int breite, hoehe;
            m_popSize = (int)uD_population.Value;
            breite = pictureBox1.Width;
            hoehe = pictureBox1.Height;
            m_nrRooms = (int)uD_rooms.Value;
            
            if (EA != null)
            {
                for (int i = 0; i < EA.Count(); i++)
                {
                    EA[i] = null;
                }
            }
            SetParameters();

            //int anzahlCPU = System.Environment.ProcessorCount;

            //if (anzahlCPU > 2)
            //{
            //    if (anzahlCPU >= RParameter.popSize) anzahlCPU = RParameter.popSize - 1;
            //    EA = new REvo[anzahlCPU];     
            //}
            //else EA = new REvo[2];
            EA = new REvo[1];
           
            lbl_fitOverl.Text = EA.Count().ToString();

            m_canvas = new Rectangle(0, 0, breite, hoehe);
            EA[0] = new REvo(m_canvas, m_popSize, m_nrRooms);
            EA[0].Mode = "plus";
            for (int i = 1; i < EA.Count()-0; i++)
            {
                EA[i] = new REvo(m_canvas, m_popSize, m_nrRooms);
                EA[i].Mode = "comma";
            }

        //    EA[EA.Count()-1] = new REvo(m_canvas, 100, m_nrRooms); // the random population

            //EA[0].BuildPhenotype();
            pictureBox1.Invalidate();
            Generations = 0;

            chart1.Series[0].Points.Clear();
            chart1.Series[1].Points.Clear();

            initTextBoxes(m_nrRooms);
            panel1.Invalidate();

            chart2.ChartAreas[0].AxisX.Maximum = 1.0;// 100000;// 1.0;// .3;
            chart2.ChartAreas[0].AxisY.Maximum = 1.0;// 100;// .3;
            chart2.ChartAreas[0].AxisX.MinorGrid.Enabled = false;
            chart2.ChartAreas[0].AxisX.MajorGrid.Interval = .1;// 1.0 / 32;
            chart2.ChartAreas[0].AxisY.MinorGrid.Enabled = false;
            chart2.ChartAreas[0].AxisY.MajorGrid.Interval = .1;//1.0 / 32;
            chart2.Series[1].MarkerSize = 1;
            chart2.Series[0].MarkerSize = 4;

           //Application.Idle += new EventHandler(Application_Idle);
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            //GetParameters();
            //MainLoop();
        }

        //==============================================================================================
        private void button1_Click_1(object sender, EventArgs e)
        {
            InitialiseParameters();
            // disable all settings controls except "Stop" button
            EnableControls(false);

            // run worker thread
            needToStop = false;
            workerThread = new Thread(new ThreadStart(SearchSolution));
            workerThread.Start();
        }

        public void InitialiseParameters()
        {
            // update settings controls
            UpdateSettings();
            SetParameters();

        }

        public void SetParameters()
        {
            RParameter.repForce = (double)this.uD_repForce.Value / 100;
            RParameter.repRate = (double)this.uD_repRate.Value / 100;
            RParameter.propForce = (double)this.uD_propForce.Value / 100;
            RParameter.propRate = (double)this.uD_propRate.Value / 100;
            RParameter.sDamp = (double)this.uD_sDamp.Value / 100;

            RParameter.pressVal = (double)this.uD_pressure.Value;
            RParameter.crossRate = (double)this.uD_crossRate.Value / 100.0;
            RParameter.doCross = this.cB_actiCross.Checked;
            RParameter.elite = this.cB_elite.Checked;
            RParameter.plusSel = this.rb_plus.Checked;

            RParameter.roomsNr = (double)this.uD_rooms.Value;
            RParameter.thresH = (double)this.uD_rooms.Value * 1.25;

            RParameter.addDist = (int)this.uD_RLap.Value;
            RParameter.popSize = (int)this.uD_population.Value;

            RParameter.p = (int)this.uD_p.Value;

        }

        //==============================================================================================
        //==============================================================================================
        //==============================================================================================

        // Delegates to enable async calls for setting controls properties
        private delegate void SetTextCallback(System.Windows.Forms.Control control, string text);

        // Thread safe updating of control's text property
        private void SetText(System.Windows.Forms.Control control, string text)
        {
            if (control.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                Invoke(d, new object[] { control, text });
            }
            else
            {
                control.Text = text;
            }
        }

        //==============================================================================================
        private delegate void SetChartCallback(System.Windows.Forms.DataVisualization.Charting.Chart chart, double valueX, double valueY);

        private void SetChart(System.Windows.Forms.DataVisualization.Charting.Chart chart, double valueX, double valueY)
        {
            if (chart.InvokeRequired)
            {
                SetChartCallback d = new SetChartCallback(SetChart);
                Invoke(d, new object[] { chart, valueX, valueY });
            }
            else
            {
                chart.Series[1].Points.AddXY(valueX, valueY);
                //SetChart(Series[0].Points.AddXY(tempElement.NormFitOverl, tempElement.NormFitDist));
            }
        }


       

        //==============================================================================================
        // Delegates to enable async calls for setting controls properties
        private delegate void EnableCallback(bool enable);

        // Enable/disale controls (safe for threading)
        private void EnableControls(bool enable)
        {
            if (InvokeRequired)
            {
                EnableCallback d = new EnableCallback(EnableControls);
                Invoke(d, new object[] { enable });
            }
            else
            {
                bStart.Enabled = enable;
              /*  startButton.Enabled = enable;
                  stopButton.Enabled = !enable;
              */
            }
        }

        //==============================================================================================
        // Update settings controls
        private void UpdateSettings()
        {
          //  citiesCountBox.Text = citiesCount.ToString();
          //  populationSizeBox.Text = populationSize.ToString();
          //  iterationsBox.Text = iterations.ToString();
        }

        //==============================================================================================
        // On main form closing
        private void MainForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // check if worker thread is running
            if ((workerThread != null) && (workerThread.IsAlive))
            {
                needToStop = true;
                while (!workerThread.Join(100))
                    Application.DoEvents();
            }
        }
        //==============================================================================================
        //==============================================================================================
        //==============================================================================================
        public void initTextBoxes(int nPoints)
        {
             Label newTB = new Label();

            // Initializes and populates the AreaBoxes list.
            if (this.m_idLabels.Count > 0)
            {
                for (int j = 0; j < m_idLabels.Count; j++)
                {
                    m_idLabels[j].Dispose();
                }
                this.m_idLabels.Clear();
            }

            for (int j = 0; j < nPoints; j++)
            {
                newTB = SetLabel(j.ToString());
                this.m_idLabels.Add(newTB);
            }
        }

        //==============================================================================================
        private Label SetLabel(string name)
        {
            Label curLabel = new System.Windows.Forms.Label(); //new Label();
            curLabel.BackColor = Color.LightGray;//Color.Transparent;
            curLabel.AutoSize = true;
            curLabel.Location = new System.Drawing.Point(1, 1);
            curLabel.Name = "mylabel"+name;
            curLabel.Size = new System.Drawing.Size(43, 13);
            curLabel.TabIndex = 12;
            curLabel.Text = "Fitness:";
            return curLabel;
        }



        //==============================================================================================
        private void button4_Click(object sender, EventArgs e)
        {
           // Step();
        }

        //==============================================================================================
        private void button2_Click_1(object sender, EventArgs e)
        {
            chart2.Series[1].Points.Clear();
            Initialse();
            SetParameters();
        }

        //==============================================================================================
        public void Step()
        {
            //Application.DoEvents();

                //Actions
                //EA[0].Run();
                Parallel.For(0, EA.Count() - 0, delegate(int i)
                {
                    EA[i].Run();
                    Thread.Sleep(1); //frees up the cpu
                });          

                pictureBox1.Invalidate();
                Generations++;

                // --- interface information ---
                int test = RTools.CalculateFrameRate();
                string toHead = "Packing with " + test.ToString() + " fps";
                SetText(this, toHead);

                Migration();

        }

        //==============================================================================================
        //private void MainLoop()
        void SearchSolution( )
        {
            //FPS set up
            double FPS = 30.0;
            long ticks1 = 0;
            long ticks2 = 0;
            double interval = (double)Stopwatch.Frequency / FPS;
            DateTime start = DateTime.Now;// DateTime.MinValue;
            DateTime end = DateTime.MinValue;
            TimeSpan diff;
            //
            EnableControls(false);

            while (!needToStop)
            {
                Application.DoEvents();
                end = DateTime.Now;
                diff = end - start;
              /*  if (diff.TotalSeconds > 10.0)
                {
                    needToStop = true;
                }*/
                SetText(lbl_Time, (diff.TotalSeconds).ToString());
                // Migration();

                ticks2 = Stopwatch.GetTimestamp();
                if (ticks2 >= ticks1 + interval)
                {
                    ticks1 = Stopwatch.GetTimestamp();

                    //Actions
                    //EA[0].Run();
                    Parallel.For(0, EA.Count()-0, delegate(int i)
                    {
                        EA[i].Run();
                        Thread.Sleep(1);
                    });
                    //

                    pictureBox1.Invalidate();
                    Generations++;

                    // --- interface information ---
                    int test = RTools.CalculateFrameRate();
                    string toHead = "Packing with " + test.ToString() + " fps";
                    SetText(this, toHead);
                    //chart2.Invoke( chart2.Series[0].Points.Clear() ) ;
                    
                    //SetText(lbl_move, Math.Round(EA[0].Parents[0].CValues.MoveRate, 3).ToString());
                    // --- Achtung: Ading data to chart makes the program slow down over time!!! ------
                    foreach (RChromosome tempElement in EA[0].Parents)
                    {
                        SetChart(chart2, tempElement.NormFitOverl, tempElement.NormFitDist);
                        //chart2.Series[0].Points.AddXY(tempElement.NormFitOverl, tempElement.NormFitDist);
                    }
                    SetChart(chart1, Generations, EA[0].Parents[0].FitRealOverl);
                    
                    //Migration();
                }

                Thread.Sleep(1); //frees up the cpu
            }
            EnableControls(true);
        }

        //==============================================================================================
        private void Migration()
        {// --- Migration of Individuals to the conservative population ---
            for (int i = 1; i < EA.Count()-0; i++)
            {
                if (EA[0].Parents.Count > 1)
                {
                    if (RTools.Rnd.NextDouble() > 0.25)
                    {
                        int idx = 0;//i - 1;//
                        if (RTools.Rnd.NextDouble() > 0.5) EA[idx].Parents.Remove(EA[idx].Parents.FindWorstDistance());
                        else
                            EA[idx].Parents.Remove(EA[idx].Parents.FindWorstOverlap());
                    }
                }
            }
            for (int i = 1; i < EA.Count(); i++)
            {
                //EA[0].Parents.Add(EA[i].Parents[0]);
                int idx = 0;//i - 1;//
                if (RTools.Rnd.NextDouble() > 0.5) EA[idx].Parents.Add(EA[i].Parents.FindFittestOverlap());
                else EA[idx].Parents.Add(EA[i].Parents.FindFittestDistance());
            }
        //    EA[1].Parents.Remove(EA[1].Parents.FindWorstOverlap());
        //    EA[1].Parents.Add(EA[EA.Count() - 1].Parents[RTools.Rnd.Next(EA[EA.Count()-1].Parents.Count)]); // take one from the random population

            //   EA[1].Parents.Remove(EA[1].Parents.FindWorstOverlap());
            //   EA[1].Parents.Add(EA[3].Parents.FindFittestOverlap());

            if (Math.IEEERemainder(Generations, 10) == 0)
            {
                int idx = 1;// RTools.Rnd.Next(EA.Count() - 1) + 1;
                EA[idx].Parents.Remove(EA[idx].Parents.Last());
                EA[idx].Parents.Add(EA[0].Parents.FindFittestOverlap());
            }
        }

        //==============================================================================================
        private void pictureBox1_Paint_1(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            Rectangle border = new Rectangle(m_canvas.Left+4, m_canvas.Top+4, m_canvas.Width-10, m_canvas.Height-10);

            // Der Bereich, der neugezeichnet werden soll, wird auf die Zeichenfläche
            // beschränkt, damit Objekte, die darüber hinausschauen abgeschnitten werden.
            /*Region clip = new Region(m_canvas);
            clip.Intersect(e.Graphics.Clip);
            e.Graphics.Clip = clip;
            e.Graphics.Clear(Color.White);*/
            
            Point beginPt = new Point(), endPt = new Point();
            // --- draw the fittest individual -------
            foreach (RPolygon polygon in EA[0].PhenotypeGdi)
            {
                polygon.Brush = new SolidBrush(Color.Gray); 
                polygon.Pen = new Pen(Color.Black, 2);
                polygon.Draw(e.Graphics);

                beginPt.X = m_canvas.Left; beginPt.Y = m_canvas.Top +1;
                endPt.X = m_canvas.Right; endPt.Y = m_canvas.Top + 1;
                Pen curPen = new Pen(Color.Black, 3); //new Pen (pen.Color, 3);
                e.Graphics.DrawLine(curPen, beginPt, endPt);

                beginPt.X = m_canvas.Left; beginPt.Y = m_canvas.Bottom - 4;
                endPt.X = m_canvas.Right; endPt.Y = m_canvas.Bottom - 4;
                curPen = new Pen(Color.Black, 3); //new Pen (pen.Color, 3);
                e.Graphics.DrawLine(curPen, beginPt, endPt);

                beginPt.X = m_canvas.Left+1; beginPt.Y = m_canvas.Top;
                endPt.X = m_canvas.Left+1; endPt.Y = m_canvas.Bottom;
                curPen = new Pen(Color.Black, 3); //new Pen (pen.Color, 3);
                e.Graphics.DrawLine(curPen, beginPt, endPt);

                beginPt.X = m_canvas.Right - 4; beginPt.Y = m_canvas.Top;
                endPt.X = m_canvas.Right - 4; endPt.Y = m_canvas.Bottom;
                curPen = new Pen(Color.Black, 3); //new Pen (pen.Color, 3);
                e.Graphics.DrawLine(curPen, beginPt, endPt);
            }
            
            Pen pen = new Pen(Color.Black, 10);
            e.Graphics.DrawRectangle(pen, border);

            // --- draw the doors ------------------------------------
            //RChromosome curChromo = EA[0].Parents.FindFittestOverlap();
            //curChromo.CreateDoors();
            //foreach (RDoors door in curChromo.Doors)
            //{
            //    if (door != null)
            //    {
            //        Pen curPen = new Pen(Color.White, 1); //new Pen(Color.Beige, 1);
            //        SolidBrush curBrush = new SolidBrush(Color.White);
            //        e.Graphics.FillPath(curBrush, door.DoorPath);
            //        e.Graphics.DrawPath(curPen, door.DoorPath);
            //    }
            //}

            if (cB_showTopo.Checked)
                DrawConnections(e, EA[0].PhenotypeGdi);
        }

        //==============================================================================================
        private void DrawConnections(PaintEventArgs e, List<RPolygon> phenoType)
        {
            Pen pen = new Pen(Color.Orange, 0.1f);
            // --- draw connections ------------------
            RPolygon fistPoly, secondPoly, testPoly;
            int ellipseSize = 2;
            if (cb_Topo.Checked)
            {
                for (int i = 0; i < phenoType.Count; i++)
                {
                    fistPoly = phenoType[i];
                    e.Graphics.DrawEllipse(pen, (int)fistPoly.Center.X - ellipseSize, (int)fistPoly.Center.Y - ellipseSize, ellipseSize * 2, ellipseSize * 2);
                    for (int m = 0; m < fistPoly.neighbours.Count; m++)
                    {
                        for (int k = 0; k < phenoType.Count; k++)
                        {
                            testPoly = phenoType[k];
                            // -- draw the lines --
                            if (fistPoly.neighbours[m] == testPoly.Id)
                            {
                                secondPoly = testPoly;
                                e.Graphics.DrawLine(pen, fistPoly.Center, secondPoly.Center);
                                break;
                            }
                        }
                    }
                }
            }
        }

        //==============================================================================================
        //==============================================================================================
        //==============================================================================================
        Point lastMouseLocation;
        RPolygon movingChild;
        RGen curGen;
        int movingIdx = -1;
        //==============================================================================================
        private void pictureBox1_MouseDown_1(object sender, MouseEventArgs e)
        {
            base.OnMouseDown(e);
            // Wenn die Maus außerhalb der Zeichenfläche gedrückt wurde, wird abgebrochen.
            if (! m_canvas.Contains(e.Location)) return;
            // Anderenfalls wird die Liste mit den gezeichneten Objekten von hinten (damit
            // das oberste Objekte gefunden wird) durchgegangen und geprüft, über welchem
            // Objekt die Maus sich befindet.
            //_population.Parents[_fittestIdx].objects
            if (movingIdx == -1)
            if (e.Button == MouseButtons.Left) // left button to move polygons
            {
                for (int i = 0; i < EA[0].PhenotypeGdi.Count; i++)
                {
                    RPolygon go = EA[0].PhenotypeGdi[i];
                    if (go.Contains(e.Location))
                    {
                        movingChild = go;
                        movingIdx = i;
                        //for (int m = 0; m < EA.Count(); m++)
                        int m = 0;
                        {
                            for (int k = 0; k < EA[m].Parents.Count; k++)
                            {
                                curGen = EA[m].Parents[k].Gens[movingIdx];
                                curGen.Locked = true;
                            }
                        }
                        /*for (int k = 0; k < EA[0].parentsForm.Count; k++)
                        {
                            curGen = EA[0].parentsForm[k].Gens[movingIdx];
                            curGen.Locked = true;
                        }*/
                        break;
                    }
                }
            }
            lastMouseLocation = e.Location;
        }

        //==============================================================================================
        private void pictureBox1_MouseMove_1(object sender, MouseEventArgs e)
        {
            base.OnMouseMove(e);
            Vector vel;
            RGen curGen;
            if (movingChild != null)
            {
                // Wenn gerade ein Objekt verschoben werden soll, wird die Differenz zur letzten
                // Mausposition ausgerechnet und das Objekt um diese verschoben.
                vel.X = e.X;// -lastMouseLocation.X;// movingChild.centre.X;
                vel.Y = e.Y;// -lastMouseLocation.Y;
                for (int m = 0; m < EA.Count(); m++)
                {
                    for (int k = 0; k < EA[m].Parents.Count; k++)
                    {
                        curGen = EA[m].Parents[k].Gens[movingIdx];
                       // curGen.Offset(vel.X, vel.Y);
                        curGen.Center =new Vector(vel.X, vel.Y);
                    }
                }
                /*for (int k = 0; k < EA[0].parentsForm.Count; k++)
                {
                    curGen = EA[0].parentsForm[k].Gens[movingIdx];
                    curGen.Offset(vel.X, vel.Y);
                }*/
            }
            // Hier könnte man noch optimieren, indem man immer nur den Bereich
            // neuzeichnet, in dem das Objekt bewegt wurde.
            lastMouseLocation = e.Location;
            this.Invalidate();
        }

        //==============================================================================================
        private void pictureBox1_MouseUp_1(object sender, MouseEventArgs e)
        {
            base.OnMouseUp(e);
            Vector vel;
            RGen curGen;
            vel.X = 0;
            vel.Y = 0;
            {
                if (movingChild != null)
                {
                    movingChild = null;
                    for (int m = 0; m < EA.Count(); m++)
                    {
                        for (int k = 0; k < EA[m].Parents.Count; k++)
                        {
                            curGen = EA[m].Parents[k].Gens[movingIdx];
                            curGen.Locked = false;                  
                        }
                    }
                    /*for (int k = 0; k < EA[0].parentsForm.Count; k++)
                    {
                        curGen = EA[0].parentsForm[k].Gens[movingIdx];
                        curGen.Locked = false;
                    }*/
                }
            }
            movingIdx = -1;
        }

        private void button3_Click(object sender, EventArgs e)
        {

            FormChart NeueForm = new FormChart();
            NeueForm.Show();
            //this.Hide();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            int rOverl = (int)uD_RLap.Value;
            e.Graphics.DrawLine(Pens.Black, 8, 140, 8 + rOverl, 140);
        }

        private void uD_RLap_ValueChanged(object sender, EventArgs e)
        {
            panel1.Invalidate();
        }

        private void uD_nr_ValueChanged(object sender, EventArgs e)
        {
            pictureBox1.Invalidate();
        }

        private void cB_testScenario_CheckedChanged(object sender, EventArgs e)
        {
            uD_rooms.Value = 16; 
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // check if worker thread is running
            if ((workerThread != null) && (workerThread.IsAlive))
            {
                needToStop = true;
                while (!workerThread.Join(100))
                    Application.DoEvents();
            }
        }

        // On "Stop" button click
        private void bStop_Click(object sender, EventArgs e)
        {
			// stop worker thread
            if ( workerThread != null )
            {
                needToStop = true;
                while ( !workerThread.Join( 100 ) )
                    Application.DoEvents( );
                workerThread = null;
            }
        }

        private void uD_population_ValueChanged(object sender, EventArgs e)
        {
            SetParameters();
        }

        private void rB_8Rooms_CheckedChanged(object sender, EventArgs e)
        {
            uD_rooms.Value = 8;
        }

        private void rB_10Rooms_CheckedChanged(object sender, EventArgs e)
        {
            uD_rooms.Value = 10;
        }


    }
}

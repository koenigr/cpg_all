﻿namespace CPlan.Kremlas
{
    partial class FormChart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cB_log = new System.Windows.Forms.CheckBox();
            this.cB_disturb = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.upDown_disturb = new System.Windows.Forms.NumericUpDown();
            this.lbl_curRep = new System.Windows.Forms.Label();
            this.lbl_curGen = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.upDown_Rep = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.upDown_Gen = new System.Windows.Forms.NumericUpDown();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cB_savePic = new System.Windows.Forms.CheckBox();
            this.gB_strategy = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lbl_rOverl = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbl_CrossInfo = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbl_elite = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lbl_pop = new System.Windows.Forms.Label();
            this.lbl_avgTime = new System.Windows.Forms.Label();
            this.lbl_room = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbl_press = new System.Windows.Forms.Label();
            this.lbl_avgBest = new System.Windows.Forms.Label();
            this.lbl_cross = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.uD_ptSize = new System.Windows.Forms.NumericUpDown();
            this.tB_name = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.chartTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_disturb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_Rep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_Gen)).BeginInit();
            this.panel2.SuspendLayout();
            this.gB_strategy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_ptSize)).BeginInit();
            this.SuspendLayout();
            // 
            // chart1
            // 
            this.chart1.BorderlineColor = System.Drawing.Color.Black;
            this.chart1.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea2.AxisX.Title = "generations";
            chartArea2.AxisY.Title = "overlappings";
            chartArea2.AxisY2.MajorGrid.LineColor = System.Drawing.Color.Transparent;
            chartArea2.AxisY2.Title = "distances";
            chartArea2.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea2);
            this.chart1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.chart1.Location = new System.Drawing.Point(0, 104);
            this.chart1.Name = "chart1";
            series5.BorderColor = System.Drawing.Color.Black;
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series5.Color = System.Drawing.Color.Red;
            series5.MarkerSize = 1;
            series5.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series5.Name = "Series1";
            series6.BorderWidth = 2;
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series6.Color = System.Drawing.Color.Green;
            series6.Name = "Series2";
            series7.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            series7.BorderWidth = 2;
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series7.Name = "Series3";
            series7.YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
            series8.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.DashDot;
            series8.BorderWidth = 2;
            series8.ChartArea = "ChartArea1";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series8.Color = System.Drawing.Color.Maroon;
            series8.Name = "Series4";
            this.chart1.Series.Add(series5);
            this.chart1.Series.Add(series6);
            this.chart1.Series.Add(series7);
            this.chart1.Series.Add(series8);
            this.chart1.Size = new System.Drawing.Size(615, 381);
            this.chart1.TabIndex = 2;
            this.chart1.Text = "chart1";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.cB_log);
            this.panel1.Controls.Add(this.cB_disturb);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.upDown_disturb);
            this.panel1.Controls.Add(this.lbl_curRep);
            this.panel1.Controls.Add(this.lbl_curGen);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.upDown_Rep);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.upDown_Gen);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(252, 104);
            this.panel1.TabIndex = 3;
            // 
            // cB_log
            // 
            this.cB_log.Location = new System.Drawing.Point(200, 9);
            this.cB_log.Name = "cB_log";
            this.cB_log.Size = new System.Drawing.Size(43, 18);
            this.cB_log.TabIndex = 27;
            this.cB_log.Text = "log";
            this.cB_log.UseVisualStyleBackColor = true;
            this.cB_log.CheckedChanged += new System.EventHandler(this.cB_log_CheckedChanged);
            // 
            // cB_disturb
            // 
            this.cB_disturb.Location = new System.Drawing.Point(22, 52);
            this.cB_disturb.Name = "cB_disturb";
            this.cB_disturb.Size = new System.Drawing.Size(13, 18);
            this.cB_disturb.TabIndex = 26;
            this.cB_disturb.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(146, 55);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Generations";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(36, 55);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "Disturb all";
            // 
            // upDown_disturb
            // 
            this.upDown_disturb.Location = new System.Drawing.Point(92, 52);
            this.upDown_disturb.Name = "upDown_disturb";
            this.upDown_disturb.Size = new System.Drawing.Size(48, 20);
            this.upDown_disturb.TabIndex = 8;
            this.upDown_disturb.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // lbl_curRep
            // 
            this.lbl_curRep.AutoSize = true;
            this.lbl_curRep.Location = new System.Drawing.Point(146, 33);
            this.lbl_curRep.Name = "lbl_curRep";
            this.lbl_curRep.Size = new System.Drawing.Size(31, 13);
            this.lbl_curRep.TabIndex = 7;
            this.lbl_curRep.Text = "0000";
            // 
            // lbl_curGen
            // 
            this.lbl_curGen.AutoSize = true;
            this.lbl_curGen.Location = new System.Drawing.Point(146, 11);
            this.lbl_curGen.Name = "lbl_curGen";
            this.lbl_curGen.Size = new System.Drawing.Size(31, 13);
            this.lbl_curGen.TabIndex = 6;
            this.lbl_curGen.Text = "0000";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(10, 73);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "reset";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(90, 73);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(115, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "start / stop";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Repetitions:";
            // 
            // upDown_Rep
            // 
            this.upDown_Rep.Location = new System.Drawing.Point(92, 30);
            this.upDown_Rep.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.upDown_Rep.Name = "upDown_Rep";
            this.upDown_Rep.Size = new System.Drawing.Size(48, 20);
            this.upDown_Rep.TabIndex = 2;
            this.upDown_Rep.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Generations:";
            // 
            // upDown_Gen
            // 
            this.upDown_Gen.Location = new System.Drawing.Point(92, 8);
            this.upDown_Gen.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.upDown_Gen.Name = "upDown_Gen";
            this.upDown_Gen.Size = new System.Drawing.Size(48, 20);
            this.upDown_Gen.TabIndex = 0;
            this.upDown_Gen.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.cB_savePic);
            this.panel2.Controls.Add(this.gB_strategy);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.uD_ptSize);
            this.panel2.Controls.Add(this.tB_name);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(252, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(363, 104);
            this.panel2.TabIndex = 4;
            // 
            // cB_savePic
            // 
            this.cB_savePic.Location = new System.Drawing.Point(251, 8);
            this.cB_savePic.Name = "cB_savePic";
            this.cB_savePic.Size = new System.Drawing.Size(113, 17);
            this.cB_savePic.TabIndex = 25;
            this.cB_savePic.Text = "save pictures";
            this.cB_savePic.UseVisualStyleBackColor = true;
            // 
            // gB_strategy
            // 
            this.gB_strategy.Controls.Add(this.label9);
            this.gB_strategy.Controls.Add(this.lbl_rOverl);
            this.gB_strategy.Controls.Add(this.label8);
            this.gB_strategy.Controls.Add(this.lbl_CrossInfo);
            this.gB_strategy.Controls.Add(this.label3);
            this.gB_strategy.Controls.Add(this.label4);
            this.gB_strategy.Controls.Add(this.label14);
            this.gB_strategy.Controls.Add(this.label5);
            this.gB_strategy.Controls.Add(this.lbl_elite);
            this.gB_strategy.Controls.Add(this.label6);
            this.gB_strategy.Controls.Add(this.label12);
            this.gB_strategy.Controls.Add(this.lbl_pop);
            this.gB_strategy.Controls.Add(this.lbl_avgTime);
            this.gB_strategy.Controls.Add(this.lbl_room);
            this.gB_strategy.Controls.Add(this.label10);
            this.gB_strategy.Controls.Add(this.lbl_press);
            this.gB_strategy.Controls.Add(this.lbl_avgBest);
            this.gB_strategy.Controls.Add(this.lbl_cross);
            this.gB_strategy.Location = new System.Drawing.Point(4, 3);
            this.gB_strategy.Name = "gB_strategy";
            this.gB_strategy.Size = new System.Drawing.Size(241, 93);
            this.gB_strategy.TabIndex = 21;
            this.gB_strategy.TabStop = false;
            this.gB_strategy.Text = "(μ+λ)- ES";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(-3, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Rooms-Overlap:";
            // 
            // lbl_rOverl
            // 
            this.lbl_rOverl.AutoSize = true;
            this.lbl_rOverl.Location = new System.Drawing.Point(81, 74);
            this.lbl_rOverl.Name = "lbl_rOverl";
            this.lbl_rOverl.Size = new System.Drawing.Size(31, 13);
            this.lbl_rOverl.TabIndex = 22;
            this.lbl_rOverl.Text = "0000";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(136, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "avg. Best:";
            // 
            // lbl_CrossInfo
            // 
            this.lbl_CrossInfo.AutoSize = true;
            this.lbl_CrossInfo.Location = new System.Drawing.Point(193, 60);
            this.lbl_CrossInfo.Name = "lbl_CrossInfo";
            this.lbl_CrossInfo.Size = new System.Drawing.Size(31, 13);
            this.lbl_CrossInfo.TabIndex = 20;
            this.lbl_CrossInfo.Text = "0000";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Population:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Evo-Preasure:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(135, 60);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 13);
            this.label14.TabIndex = 19;
            this.label14.Text = "crossover:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Cross-Rate:";
            // 
            // lbl_elite
            // 
            this.lbl_elite.AutoSize = true;
            this.lbl_elite.Location = new System.Drawing.Point(193, 46);
            this.lbl_elite.Name = "lbl_elite";
            this.lbl_elite.Size = new System.Drawing.Size(31, 13);
            this.lbl_elite.TabIndex = 18;
            this.lbl_elite.Text = "0000";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(37, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Rooms:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(141, 46);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "elite Sel.:";
            // 
            // lbl_pop
            // 
            this.lbl_pop.AutoSize = true;
            this.lbl_pop.Location = new System.Drawing.Point(81, 18);
            this.lbl_pop.Name = "lbl_pop";
            this.lbl_pop.Size = new System.Drawing.Size(31, 13);
            this.lbl_pop.TabIndex = 8;
            this.lbl_pop.Text = "0000";
            // 
            // lbl_avgTime
            // 
            this.lbl_avgTime.AutoSize = true;
            this.lbl_avgTime.Location = new System.Drawing.Point(193, 32);
            this.lbl_avgTime.Name = "lbl_avgTime";
            this.lbl_avgTime.Size = new System.Drawing.Size(31, 13);
            this.lbl_avgTime.TabIndex = 16;
            this.lbl_avgTime.Text = "0000";
            // 
            // lbl_room
            // 
            this.lbl_room.AutoSize = true;
            this.lbl_room.Location = new System.Drawing.Point(81, 32);
            this.lbl_room.Name = "lbl_room";
            this.lbl_room.Size = new System.Drawing.Size(31, 13);
            this.lbl_room.TabIndex = 9;
            this.lbl_room.Text = "0000";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(134, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "avg. Time:";
            // 
            // lbl_press
            // 
            this.lbl_press.AutoSize = true;
            this.lbl_press.Location = new System.Drawing.Point(81, 46);
            this.lbl_press.Name = "lbl_press";
            this.lbl_press.Size = new System.Drawing.Size(31, 13);
            this.lbl_press.TabIndex = 10;
            this.lbl_press.Text = "0000";
            // 
            // lbl_avgBest
            // 
            this.lbl_avgBest.AutoSize = true;
            this.lbl_avgBest.Location = new System.Drawing.Point(193, 18);
            this.lbl_avgBest.Name = "lbl_avgBest";
            this.lbl_avgBest.Size = new System.Drawing.Size(31, 13);
            this.lbl_avgBest.TabIndex = 14;
            this.lbl_avgBest.Text = "0000";
            // 
            // lbl_cross
            // 
            this.lbl_cross.AutoSize = true;
            this.lbl_cross.Location = new System.Drawing.Point(81, 60);
            this.lbl_cross.Name = "lbl_cross";
            this.lbl_cross.Size = new System.Drawing.Size(31, 13);
            this.lbl_cross.TabIndex = 11;
            this.lbl_cross.Text = "0000";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(254, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "PointSize:";
            // 
            // uD_ptSize
            // 
            this.uD_ptSize.Location = new System.Drawing.Point(308, 26);
            this.uD_ptSize.Name = "uD_ptSize";
            this.uD_ptSize.Size = new System.Drawing.Size(43, 20);
            this.uD_ptSize.TabIndex = 8;
            this.uD_ptSize.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tB_name
            // 
            this.tB_name.Location = new System.Drawing.Point(251, 50);
            this.tB_name.Name = "tB_name";
            this.tB_name.Size = new System.Drawing.Size(100, 20);
            this.tB_name.TabIndex = 6;
            this.tB_name.Text = "myPicture01";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(251, 73);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "save form";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // chartTimer
            // 
            this.chartTimer.Interval = 1;
            this.chartTimer.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FormChart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 485);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.chart1);
            this.Name = "FormChart";
            this.Text = "Analyse";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormChart_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_disturb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_Rep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDown_Gen)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.gB_strategy.ResumeLayout(false);
            this.gB_strategy.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_ptSize)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown upDown_Rep;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown upDown_Gen;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Timer chartTimer;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label lbl_curRep;
        private System.Windows.Forms.Label lbl_curGen;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox tB_name;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbl_cross;
        private System.Windows.Forms.Label lbl_press;
        private System.Windows.Forms.Label lbl_room;
        private System.Windows.Forms.Label lbl_pop;
        private System.Windows.Forms.Label lbl_avgTime;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbl_avgBest;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbl_CrossInfo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lbl_elite;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown uD_ptSize;
        private System.Windows.Forms.GroupBox gB_strategy;
        protected internal System.Windows.Forms.CheckBox cB_savePic;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbl_rOverl;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown upDown_disturb;
        protected internal System.Windows.Forms.CheckBox cB_disturb;
        protected internal System.Windows.Forms.CheckBox cB_log;
    }
}
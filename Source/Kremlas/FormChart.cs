﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = FormChart.cs 
//  Copyright (C) 2014/10/18  8:25 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Drawing;
using System.Windows.Forms;

namespace CPlan.Kremlas
{
    public partial class FormChart : Form
    {
        private Form1 MainForm;// = new Form1();
        private int m_cterRun;
        private int m_cterGen;
        private double m_avgBest;
        private DateTime m_startZeit, m_endZeit;
        private Double m_sumTime;
        private double [] m_avgFitArr;
        private double[] m_avgFitArrDist;
        private Random m_rnd = new Random();
        private String proportionString;

        //----------------------------------------------------------------------------------------------
        public FormChart()
        {
            InitializeComponent();
            Initialse();
        }

        //==============================================================================================
        public void Initialse()
        {
            chart1.ChartAreas["ChartArea1"].AxisX.IsLogarithmic = false;
            chart1.ChartAreas["ChartArea1"].AxisY.IsLogarithmic = false;
            chart1.ChartAreas["ChartArea1"].AxisY2.IsLogarithmic = false;
         //   chart1.ChartAreas["ChartArea1"].AxisY.Maximum = 30000;
         /*   chart1.ChartAreas["ChartArea1"].AxisX.IsLogarithmic = true;
            chart1.ChartAreas["ChartArea1"].AxisX.IsStartedFromZero = false;
            chart1.ChartAreas["ChartArea1"].AxisX.MajorGrid.Interval = 5;
            chart1.ChartAreas["ChartArea1"].AxisX.Interval = 1;*/
            proportionString = "";

            MainForm = ((Form1)Application.OpenForms["Form1"]);// Application.OpenForms[0];// ["Form1"];
            m_cterRun = 0;
            m_cterGen = 0;
            m_avgBest = 0;
            m_sumTime = 0;// new TimeSpan(0, 0, 0);
            chart1.Series[0].Points.Clear();
            chart1.Series[0].MarkerSize = (int)uD_ptSize.Value;
            chart1.Series[1].Points.Clear();
            chart1.Series[2].Points.Clear();

            chart1.ChartAreas[0].AxisY.Maximum = 1.0;// 100;// .3;
            chart1.ChartAreas[0].AxisY2.Maximum = 1.0;
            //chart1.ChartAreas[0].AxisY.Minimum = 0.01;// 100;// .3;
            //chart1.ChartAreas[0].AxisY2.Minimum = 0.01;
            chart1.ChartAreas[0].AxisY2.MinorGrid.Enabled = false;
            chart1.ChartAreas[0].AxisY2.MajorGrid.Interval = .1;// 1.0 / 32;
            chart1.ChartAreas[0].AxisY.MinorGrid.Enabled = false;
            chart1.ChartAreas[0].AxisY.MajorGrid.Interval = .1;//1.0 / 32;

            //chart1.ChartAreas[0].AxisY.IsLogarithmic = true;
            //chart1.ChartAreas[0].AxisY.LogarithmBase =  10;//Math.E;//
            /*chart1.Series.Clear();
            chart1.Series.Add("fitness");
            chart1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            chart1.Series[0].Color = Color.Red;
            chart1.Series[0].MarkerSize = uD_ptSize.Value;
            chart1.Series[0].MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            chart1.Series[0].BorderColor = Color.Black;*/
            m_avgFitArr = new double[(int)upDown_Gen.Value + 1];
            m_avgFitArrDist = new double[(int)upDown_Gen.Value + 1];

            lbl_pop.Text = MainForm.uD_population.Value.ToString();
            lbl_room.Text = MainForm.uD_rooms.Value.ToString();
            lbl_press.Text = MainForm.uD_pressure.Value.ToString();
            lbl_cross.Text = MainForm.uD_crossRate.Value.ToString();
            lbl_rOverl.Text = MainForm.uD_RLap.Value.ToString();
            lbl_avgBest.Text = "0";
            lbl_avgTime.Text = "0";

            if (MainForm.rb_plus.Checked) { gB_strategy.Text = "(μ+λ)-ES"; }
            else { gB_strategy.Text = "(μ, λ)-ES"; }

            if (MainForm.cB_elite.Checked) { lbl_elite.Text = "on"; }
            else { lbl_elite.Text = "off"; }

            if (MainForm.cB_actiCross.Checked) { lbl_CrossInfo.Text = "on"; }
            else { lbl_CrossInfo.Text = "off"; }

            MainForm.Initialse();
            MainForm.InitialiseParameters();

            this.Invalidate();
        }

        //==============================================================================================
        public void NewGeneration()
        {
            m_cterGen = 0;
            //chart1.Series[2].Points.Clear();
            System.GC.Collect();
            MainForm.Initialse();
        }

        //==============================================================================================
        private void button1_Click(object sender, EventArgs e)
        {
            chartTimer.Enabled = !chartTimer.Enabled;
            m_startZeit = DateTime.Now;

        }

        //==============================================================================================
        private void button2_Click(object sender, EventArgs e)
        {
            Initialse();
            MainForm.InitialiseParameters();
        }

        //==============================================================================================
        private void timer1_Tick(object sender, EventArgs e)
        {
            double disturb = Math.IEEERemainder(MainForm.Generations, (double)upDown_disturb.Value);
            RGen curGen;
            Vector vel;
            if (cB_disturb.Checked)
            {
                if (disturb == 0)
                {
                    int rndRectIdx = m_rnd.Next(MainForm.EA[0].Parents[0].Gens.Count);
                    vel.X = m_rnd.Next(MainForm.pictureBox1.Width*2) - MainForm.pictureBox1.Width ;
                    vel.Y = m_rnd.Next(MainForm.pictureBox1.Width*2) - MainForm.pictureBox1.Width ;
                    for (int k = 0; k < MainForm.EA[0].Parents.Count; k++)
                    {
                        curGen = MainForm.EA[0].Parents[k].Gens[rndRectIdx];
                        curGen.Offset(vel.X, vel.Y);
                    }
                }
            }

            MainForm.Step();
            m_cterGen++;
            RChromosome bestIndi = MainForm.EA[0].Parents.FindFittestOverlap();
            if (bestIndi.NormFitOverl <= 0) bestIndi.NormFitOverl += 0.01; // because log doesn't accept negative values or zero
            if (bestIndi.NormFitDist <= 0) bestIndi.NormFitDist += 0.01;
            m_avgFitArr[MainForm.Generations] += bestIndi.NormFitOverl;// FitRealOverl;
            m_avgFitArrDist[MainForm.Generations] += bestIndi.NormFitDist; //FitRealDist;
            chart1.Series[0].Points.AddXY(MainForm.Generations, bestIndi.NormFitOverl); //FitRealOverl);
            chart1.Series[1].Points.AddXY(MainForm.Generations, m_avgFitArr[MainForm.Generations] / (m_cterRun+1));
            if (MainForm.cb_Topo.Checked) chart1.Series[2].Points.AddXY(MainForm.Generations, m_avgFitArrDist[MainForm.Generations] / (m_cterRun + 1));
            /*if (cB_log.Checked)
            {
                chart1.ChartAreas["ChartArea1"].AxisX.IsLogarithmic = true;
                chart1.ChartAreas["ChartArea1"].AxisY.IsLogarithmic = true;
                chart1.ChartAreas["ChartArea1"].AxisY2.IsLogarithmic = true;
                chart1.ChartAreas["ChartArea1"].AxisY.Maximum = Double.NaN;
                chart1.ChartAreas["ChartArea1"].AxisY2.Maximum = Double.NaN;
            }*/

            // --- next initialization ---
            if (m_cterGen >= upDown_Gen.Value)
            {
                MainForm.pictureBox1.Invalidate();
                m_cterRun++;
                bestIndi.EvaluationProportion();
                m_avgBest += bestIndi.FitRealOverl;
                lbl_avgBest.Text = Math.Round(m_avgBest / m_cterRun, 0).ToString();
                m_endZeit = DateTime.Now;
                TimeSpan GemesseneZeit = m_endZeit - m_startZeit;
                m_sumTime += GemesseneZeit.TotalSeconds;
                lbl_avgTime.Text = Math.Round(m_sumTime / m_cterRun, 2).ToString();

                String propRatio = Math.Round(bestIndi.ProportionRatio, 4).ToString();
                proportionString += propRatio + "; " + "\n";

                if (cB_savePic.Checked)
                {                   
                    string path = propRatio + "_" + tB_name.Text + "_img" + Convert.ToString(m_cterRun);                   
                    ControlToImage(MainForm.pictureBox1, path, cB_log, false);
                }
                // --- at the end ---
                if (m_cterRun >= upDown_Rep.Value)
                {
                    string path = tB_name.Text;
                    FormChart curForm = ((FormChart)Application.OpenForms["FormChart"]);
                    ControlToImage(curForm, path, cB_log, true);

                    TextFile c_textdatei = new TextFile();
                    c_textdatei.WriteFile(@"proportionString.txt", proportionString);
                }

                NewGeneration();
                m_startZeit = DateTime.Now;
                if (m_cterRun < upDown_Rep.Value)
                {
                    chart1.Series[1].Points.Clear();
                    chart1.Series[2].Points.Clear();
                } 
            }

            if (m_cterRun >= upDown_Rep.Value)
            {
                chartTimer.Enabled = false;
                
            } 
            lbl_curRep.Text = m_cterRun.ToString();
            lbl_curGen.Text = m_cterGen.ToString();
        }

        //==============================================================================================
        private void button3_Click(object sender, EventArgs e)
        {
            string path = tB_name.Text;
            ControlToImage(FormChart.ActiveForm, path, cB_log, true);
        }

        //==============================================================================================
        public static void ControlToImage(Control control, string path, CheckBox cbLog, bool logPic)
        {
            bool curState = cbLog.Checked;
            cbLog.Checked = false;
            Bitmap bitmap = new Bitmap(control.Width, control.Height);
            control.DrawToBitmap(bitmap, new System.Drawing.Rectangle(new Point(0, 0), control.Size));
            path += ".wmf";
            bitmap.Save(path, System.Drawing.Imaging.ImageFormat.Wmf);

            if (logPic)
            {
                cbLog.Checked = true;
                bitmap = new Bitmap(control.Width, control.Height);
                control.DrawToBitmap(bitmap, new System.Drawing.Rectangle(new Point(0, 0), control.Size));
                path = path + "_log.wmf";
                bitmap.Save(path, System.Drawing.Imaging.ImageFormat.Wmf);
            }

            cbLog.Checked = curState;
        }

        //==============================================================================================
        private void FormChart_FormClosing(object sender, FormClosingEventArgs e)
        {
            chartTimer.Enabled = false;
        }

        private void cB_log_CheckedChanged(object sender, EventArgs e)
        {
            if (cB_log.Checked)
            {
                chart1.ChartAreas["ChartArea1"].AxisX.IsLogarithmic = true;
                chart1.ChartAreas["ChartArea1"].AxisY.IsLogarithmic = true;
                chart1.ChartAreas["ChartArea1"].AxisY2.IsLogarithmic = true;
                chart1.ChartAreas["ChartArea1"].AxisY.Maximum = 1;
                chart1.ChartAreas["ChartArea1"].AxisY.Maximum = Double.NaN;
                chart1.ChartAreas["ChartArea1"].AxisY2.Maximum = Double.NaN;
                chart1.ChartAreas[0].AxisY.MajorGrid.Interval = .5;// 1.0 / 32;
                chart1.ChartAreas[0].AxisY2.MajorGrid.Interval = .5;// 1.0 / 32;
            }
            if (!cB_log.Checked)
            {
                chart1.ChartAreas["ChartArea1"].AxisX.IsLogarithmic = false;
                chart1.ChartAreas["ChartArea1"].AxisY.IsLogarithmic = false;
                chart1.ChartAreas["ChartArea1"].AxisY2.IsLogarithmic = false;
                chart1.ChartAreas["ChartArea1"].AxisY.Maximum = 1;
                chart1.ChartAreas["ChartArea1"].AxisY2.Maximum = 1;
                chart1.ChartAreas[0].AxisY.MajorGrid.Interval = .1;// 1.0 / 32;
                chart1.ChartAreas[0].AxisY2.MajorGrid.Interval = .1;// 1.0 / 32;
            }
        }

    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = RChromosome.cs 
//  Copyright (C) 2014/10/18  8:25 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion
//using System.Threading;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using CPlan.HelperToolsExtensions;

namespace CPlan.Kremlas
{
    public class RChromosome
    {
        public double NormFitOverl = 1;
        public double NormFitDist = 1;
        public double TmpFit = 0;
        public double FitOverl = 1000000;// 0.0001;
        public double FitRealOverl = 1000000;//1;
        public double FitDist = 0.0001;
        public double FitRealDist = 1;
        public double OldFitness;
        public double ProportionRatio;
        public int SqueezeFit;
        public int[] GridAdress = new int[2];
        public double[] StrategyParam;
        public bool Parent = true;
        public List<RGen> Gens = new List<RGen>();
        public List<RSpring> Springs = new List<RSpring>();
        //public List<RDoors> Doors = new List<RDoors>();
        public ControlValues CValues;
        public static int NrSprings = 0;

        //private Random m_rnd = new Random((int)DateTime.Now.Ticks);
        //private NormalDistribution m_normDist = new NormalDistribution();
        private Collision m_Collision = new Collision();

        //----------------------------------------------------------------------------------------------
        public RChromosome(double maxX, double maxY, int nrPolys, int rndIdx)
        {
            RTools.Rnd = new Random(rndIdx);
            StrategyParam = new double [4] {0.1, 0.25, 0.35, 1.0};
            RGen curGen;
            OldFitness = FitOverl + 1;
            CValues.SizeRate = RTools.Rnd.NextDouble() / 2;
            CValues.OldSizeRate = CValues.SizeRate;
            CValues.MoveRate = RTools.Rnd.NextDouble() / 2;
            CValues.OldMoveRate = CValues.MoveRate;

            // --- create the Gens ---
            for (int i = 0; i < nrPolys; i++) 
            {
                curGen = new RGen(RTools.Rnd.Next((int)maxX), RTools.Rnd.Next((int)maxY), RTools.Rnd.Next(10000), i);//REvo.Rnd.Next(maxX), REvo.Rnd.Next(maxY));
                Gens.Add(curGen);
            }
            if (Application.OpenForms.Count > 0)
            {
                if (((Form1)Application.OpenForms[0]).cB_testScenario.Checked)
                {
                    SetSzenario();
                } else SetNeighbours();
            } else SetNeighbours();
            CreateSprings();
            NrSprings = Springs.Count();
        }
        //----------------------------------------------------------------------------------------------
        // Copy constructor.
        public RChromosome(RChromosome previousChromosome)
        {
            int nrPolys = previousChromosome.Gens.Count;
            List<RGen> oldGens = previousChromosome.Gens;
            List<RSpring> oldSprings = previousChromosome.Springs;
            StrategyParam = new double[4] { previousChromosome.StrategyParam[0], previousChromosome.StrategyParam[1], previousChromosome.StrategyParam[2], previousChromosome.StrategyParam[3]};
            FitOverl = previousChromosome.FitOverl;
            FitRealOverl = previousChromosome.FitRealOverl;
            FitDist = previousChromosome.FitDist;
            FitRealDist = previousChromosome.FitRealDist;
            OldFitness = previousChromosome.OldFitness;
            Parent = previousChromosome.Parent;
            CValues = previousChromosome.CValues;
            NormFitOverl = previousChromosome.NormFitOverl;
            NormFitDist = previousChromosome.NormFitDist;
            ProportionRatio = previousChromosome.ProportionRatio;
            Gens.Clear();
            Springs.Clear();
            RGen curGen;
            RSpring curSpring;
            for (int i = 0; i < nrPolys; i++)
            {
                curGen = new RGen(oldGens[i]);
                Gens.Add(curGen);
            }
            for (int i = 0; i < oldSprings.Count; i++)
            {
                curSpring = new RSpring(oldSprings[i]);
                Springs.Add(curSpring);
            }
        }

        //==============================================================================================
        private void SetNeighbours()
        {
            RGen p = Gens[0];
            for (int i = 1; i < Gens.Count; i++)
            {
                p.Neighbours.Add(i);
            }
        }

        //==============================================================================================
        private void SetSzenario()
        {
            // 3 flats a 5 rooms connected to a corridor
            // for this szenario 19 rooms are necessary!
            int rPF = 4;
            RGen p = Gens[0];
            for (int i = 1; i < 4; i++)
            {
                if (Gens.Count < i) break;
                p.Neighbours.Add(i);
            }
            int cter = 4;
            for (int i = 1; i < 4; i++)
            {
                if (Gens.Count < i) break;
                p = Gens[i];
                for (int j = cter; j < cter + rPF; j++)
                {
                    if (Gens.Count < j) break;
                    p.Neighbours.Add(j);
                }
                cter += rPF;
            }
        }

        //==============================================================================================
        private void CreateSprings()
        {
            RGen firstGen, secondGen;
            RSpring curSpring;
            for (int i = 0; i < Gens.Count; i++)
            {
                firstGen = Gens[i];
                for (int m = 0; m < firstGen.Neighbours.Count; m++)
                {
                    for (int k = 0; k < Gens.Count; k++)
                    {
                        secondGen = Gens[k];
                        if (firstGen.Neighbours[m] == secondGen.Id)
                        {
                            curSpring = new RSpring(firstGen, secondGen);
                            Springs.Add(curSpring);
                        }
                    }
                }
            }
        }

        //==================================================================
        public void RunSprings(double damp)
        {
            for (int i = 0; i < Springs.Count; i++)
            {
                Springs[i].update(Gens, damp); // run the Springs
            }
            for (int i = 0; i < Gens.Count; i++)
            {
                Gens[i].UpdatePos(); // update all positions
            }
        }

        //==================================================================
        //public void CreateDoors()
        //{
        //    foreach (RGen myGen in Gens)
        //    {
        //        myGen.Doors.Clear();
        //    }
        //    Doors.Clear();
        //    for (int i = 0; i < Springs.Count; i++)
        //    {
        //        RDoors curDoor = null;
        //        Springs[i].CreateDoors(Gens, ref curDoor); // run the Springs
        //        Doors.Add(curDoor);
        //    }
        //}

        //==================================================================
        public void RepelPolys(double force, double rate)
        {
            Vector velocity = new Vector(0, 0);
            RGen firstGen, secondGen;
            Vector translation = velocity;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (RTools.Rnd.NextDouble() < rate)
                {
                    firstGen = (RGen)Gens[i];
                    for (int j = i + 1; j < Gens.Count; j++)
                    {
                        secondGen = (RGen)Gens[j];
                        PolygonCollisionResult r = m_Collision.PolygonCollision(firstGen, secondGen, velocity);
                        if (r.WillIntersect)
                        {
                            translation = velocity + r.MinimumTranslationVector;
                            translation.Multiply(force);
                            //translation.X = translation.X ;
                            //translation.Y = translation.Y ;
                            if (!firstGen.Locked) firstGen.Offset(translation);
                            translation.X = -translation.X;
                            translation.Y = -translation.Y;
                            if (!secondGen.Locked) secondGen.Offset(translation);
                        }
                    }
                }
            }
            
            
            
          /*  for (int i = 0; i < Gens.Count; i++)
            {
                Gens[i].Repel(Gens, force);
            }
            for (int i = 0; i < Gens.Count; i++)
            {
               // Gens[i].UpdatePos();
            }*/
        }

        //==================================================================
        public void ChangeProportion(double force, double rate)
        {
            RGen curGen;
            for (int i = 0; i < Gens.Count; i++)
            {
                curGen = Gens[i];
                curGen.LimboWidth = curGen.Width;
                curGen.LimboHeight = curGen.Height;
            }

            RGen firstGen, secondGen;
            Vector velocity = new Vector(0, 0);
            Vector translation = velocity;
            Vector midP;

            for (int i = 0; i < Gens.Count; i++)
            {
                firstGen = (RGen)Gens[i];
                for (int j = i + 1; j < Gens.Count; j++)
                {
                    secondGen = (RGen)Gens[j];
                    PolygonCollisionResult r = m_Collision.PolygonCollision(firstGen, secondGen, velocity);
                    if (r.WillIntersect)
                    {
                        translation = velocity + r.MinimumTranslationVector;
                        translation.Multiply(force);
                        midP = secondGen.Center - firstGen.Center;

                        if (!firstGen.Locked) VaryOnesSize(rate, firstGen, translation, midP);
                        if (!secondGen.Locked) VaryOnesSize(rate, secondGen, translation, midP);
                    }
                }
            }
          /*  
            
            for (int i = 0; i < Gens.Count; i++)
            {
                Gens[i].Proportion(Gens, force, rate);
            }
            for (int i = 0; i < Gens.Count; i++)
            {
                Gens[i].UpdateForm();
            }*/
        }

        //==============================================================================================
        private void VaryOnesSize(double rate, RGen curGen, Vector changer, Vector midP)
        {
            double width, height;
            double mag;
            changer.X = Math.Abs(changer.X);
            changer.Y = Math.Abs(changer.Y);
            midP.X = Math.Abs(midP.X);
            midP.Y = Math.Abs(midP.Y);
            mag = changer.Magnitude;

            if (RTools.Rnd.NextDouble() < rate)
            {
                if (!curGen.Locked)
                {
                    //if (REvo.Rnd.NextDouble() < 0.5)
                    {
                        if (midP.X > midP.Y)
                        {
                            width = curGen.Width - mag;
                        }
                        else
                        {
                            width = curGen.Width + mag;
                        }
                        curGen.CheckWidtHeight(width);
                    }
                    /*else
                    {
                        if (midP.Y > midP.X)
                        {
                            height = curGen.Height - mag;
                        }
                        else
                        {
                            height = curGen.Height + mag;
                        }
                        curGen.CheckHeightWidt(height);
                    }*/
                }
            }
        }

        //==================================================================
        public void Rotate(double rate)
        {
            RGen curGen;
            for (int i = 0; i < Gens.Count; i++)
            {
                if (RTools.Rnd.NextDouble() < rate)
                {
                    curGen = Gens[i];
                    curGen.Rotate90(); //--- rotate random selected polygons
                }
            }
        }

        //==================================================================
        public void CheckBorder()
        {
            Vector translation = new Vector(0, 0);
            int faktor = 1;
            double damp = 1;

            for (int i = 0; i < Gens.Count; i++)
            {
                RGen curGen = Gens[i];
                if (!curGen.Locked)
                {
                    curGen.Velocity = new Vector(0, 0);
                    for (int j = 0; j < curGen.Points.Count; j++)
                    {
                        Vector curVect = curGen.Points[j];
                        if (curVect.X < REvo.Canvas.Left)
                        {
                            translation.X = (REvo.Canvas.Left - curVect.X) / faktor;
                            translation.Y = 0;
                            translation.Multiply(damp);
                            //curGen.velocity = translation;
                            curGen.Offset(translation);
                            //break;
                        }
                        else if (curVect.X > REvo.Canvas.Width)
                        {
                            translation.X = (REvo.Canvas.Width - curVect.X) / faktor;
                            translation.Y = 0;
                            translation.Multiply(damp);
                            //curGen.velocity = translation;
                            curGen.Offset(translation);
                            //break;
                        }
                        if (curVect.Y < REvo.Canvas.Top)
                        {
                            translation.X = 0;
                            translation.Y = (REvo.Canvas.Top - curVect.Y) / faktor;
                            translation.Multiply(damp);
                            //curGen.velocity = translation;
                            curGen.Offset(translation);
                            //break;
                        }
                        else if (curVect.Y > REvo.Canvas.Height)
                        {
                            translation.X = 0;
                            translation.Y = (REvo.Canvas.Height - curVect.Y) / faktor;
                            translation.Multiply(damp);
                            //curGen.velocity = translation;
                            curGen.Offset(translation);
                            //break;
                        }
                    }
                }
            }
        }

        //==============================================================================================
        public void Inversion(double inverseRate, int rIdx)
        {
            RTools.Rnd = new Random((int)DateTime.Now.Ticks + rIdx);
            int rndIdx1, rndIdx2, counter = 0;
            RGen tempGen;
            int nrPolys = Gens.Count;
            //Form1 MainForm = ((Form1)Application.OpenForms["Form1"]);// Application.OpenForms[0];// ["Form1"];
            //int generat = MainForm.Generations;

            if ((RTools.Rnd.NextDouble() < inverseRate))
            {
                //-----------------------------------------------
                rndIdx1 = RTools.Rnd.Next(nrPolys - 2);
                int tmp = nrPolys - rndIdx1 - 1;
                rndIdx2 = rndIdx1 + RTools.Rnd.Next(tmp) + 1;
                RChromosome tmpChrom = new RChromosome(this);
               
                for (int i = 0; i < nrPolys; i++)
                {
                    if (counter > 0) break;
                    counter = 0;
                    if ((rndIdx1 <= i) & (i <= rndIdx2))
                    {
                        for (int j = rndIdx1; j <= rndIdx2; j++)
                        {
                            tempGen = tmpChrom.Gens[j];
                            Gens[rndIdx2 - counter] = new RGen(tempGen);
                            counter++;
                        }
                    }
                }
            }
        }

        //==============================================================================================
        public void Jump(double jumpRate)
        {
            double rndVal;
            RGen curGen;
            Vector newPos = new Vector(0, 0);
            Vector dPos = new Vector(0, 0);

            for (int i = 0; i < Gens.Count; i++)
            {
                curGen = Gens[i];
                if (!curGen.Locked)
                {
                    rndVal = RTools.Rnd.NextDouble();
                    if (rndVal < (jumpRate))
                    {
                        newPos.X = RTools.Rnd.Next(REvo.Canvas.Width) + REvo.Canvas.Left;
                        newPos.Y = RTools.Rnd.Next(REvo.Canvas.Height) + REvo.Canvas.Top;
                        curGen.Center = newPos;
                    }
                }
            }

        }

        //==============================================================================================
        public void MoveRandon(double rate, double sigmaValue)
        {
            RGen curGen;       
            //REvo.Rnd = new Random((int)DateTime.Now.Ticks);
            //if (_normDist.Sigma > 10) _normDist.Sigma = 10;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (RTools.Rnd.NextDouble() < rate)
                {
                    curGen = Gens[i];

                    Vector newPos = new Vector(0, 0);
                    Vector dPos = new Vector(0, 0);

                    Helper.NewNormDistGenerator();
                    Helper.NormDist.Sigma = sigmaValue;

                    if (!curGen.Locked)
                    {
                        dPos.X = Helper.NormDist.NextDouble() - 1; //_rnd.Next(3) - 1;
                        dPos.Y = Helper.NormDist.NextDouble() - 1; //_rnd.Next(3) - 1;
                            
                        //dPos.X = (REvo.Rnd.Next(3) - 1)*1;
                        //dPos.Y = (REvo.Rnd.Next(3) - 1)*1;
                            
                        /*if (REvo.Rnd.NextDouble() < 0.1){
                            dPos.X = REvo.Rnd.Next(100) - 50;
                            dPos.Y = REvo.Rnd.Next(100) - 50;
                            }*/ 
                        curGen.Offset(dPos.X, dPos.Y);
                    }
                }
            }
        }

        //==============================================================================================
        public void ReplaceGen(RGen oldGen, RGen newGen)
        {
            int idx = Gens.IndexOf(oldGen);
            Gens[idx] = new RGen (newGen);
        }

        //==============================================================================================
        public void VarySize(double rate)
        {
            RGen curGen;
            double min = 50.0;
            double width, rVal;
            double height;
            //if (m_normDist.Sigma > 10) m_normDist.Sigma =10;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (RTools.Rnd.NextDouble() < rate)//_parentPolys[k].sizeRate)//
                {
                    double divider;
                    if (RTools.Rnd.NextDouble() < 0.01)
                        divider = (int)Math.Round((decimal)(REvo.Canvas.Height * REvo.Canvas.Width) / 10);
                    else 
                        divider = (int)Math.Round((decimal)(REvo.Canvas.Height * REvo.Canvas.Width) / 180);

                    Helper.NormDist.Sigma = this.FitRealOverl / divider;//// overlap / (1000.0) + 1;//2;//
                    curGen = Gens[i];
                    if (!curGen.Locked)
                    {
                        //rVal = (REvo.Rnd.Next(3) - 1) * 2;
                        rVal = Helper.NormDist.NextDouble() - 1;
                        width = curGen.Width + rVal;
                        if (width < min) 
                            width = min;      //-- Minimale Scalierung X
                        else if (width > REvo.Canvas.Width) width = REvo.Canvas.Width; //-- Maximale Scalierung X
                        height = (curGen.Area / width);
                        curGen.Height = height;
                        curGen.Width = width;
                        if (curGen.Height < min)
                        {
                            curGen.Height = min;
                            curGen.Width = (curGen.Area / curGen.Height);
                        }
                        if (curGen.Height > REvo.Canvas.Height)
                        {
                            curGen.Height = REvo.Canvas.Height;
                            curGen.Width = (curGen.Area / curGen.Height);
                        }
                    }
                }
            }
        }

        public void VarySizeFull(double rate)
        {
            RGen curGen;
            double min = 50.0;
            double width, rVal;
            double height;
            //if (m_normDist.Sigma > 10) m_normDist.Sigma =10;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (RTools.Rnd.NextDouble() < rate)//_parentPolys[k].sizeRate)//
                {

                    Helper.NewNormDistGenerator();
                    Helper.NormDist.Sigma = (int)Math.Round((decimal)(REvo.Canvas.Height + REvo.Canvas.Width) / (Gens.Count), 0);// this.fitReal / 800 + 0;//// overlap / (1000.0) + 1;//2;//
            
                    curGen = Gens[i];
                    if (!curGen.Locked)
                    {
                        //rVal = (REvo.Rnd.Next(3) - 1) * 2;
                        rVal = Helper.NormDist.NextDouble() - 1;
                        width = curGen.Width + rVal;
                        if (width < min) 
                            width = min;      //-- Minimale Scalierung X
                        else if (width > REvo.Canvas.Width) width = REvo.Canvas.Width; //-- Maximale Scalierung X
                        height = (curGen.Area / width);
                        curGen.Height = height;
                        curGen.Width = width;
                        if (curGen.Height < min)
                        {
                            curGen.Height = min;
                            curGen.Width = (curGen.Area / curGen.Height);
                        }
                        if (curGen.Height > REvo.Canvas.Height)
                        {
                            curGen.Height = REvo.Canvas.Height;
                            curGen.Width = (curGen.Area / curGen.Height);
                        }
                    }
                }
            }
        }

        public void VarySizeFull(double rate, double sigmaValue)
        {
            RGen curGen;
            double min = 50.0;
            double width, rVal;
            double height;          
            //if (m_normDist.Sigma > 10) m_normDist.Sigma =10;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (RTools.Rnd.NextDouble() < rate)//_parentPolys[k].sizeRate)//
                {
                    Helper.NewNormDistGenerator();
                    Helper.NormDist.Sigma = sigmaValue;
                    curGen = Gens[i];
                    if (!curGen.Locked)
                    {
                        //rVal = (REvo.Rnd.Next(3) - 1) * 2;
                        rVal = Helper.NormDist.NextDouble() - 1;
                        width = curGen.Width + rVal;
                        if (width < min) 
                            width = min;      //-- Minimale Scalierung X
                        else if (width > REvo.Canvas.Width) width = REvo.Canvas.Width; //-- Maximale Scalierung X
                        height = (curGen.Area / width);
                        curGen.Height = height;
                        curGen.Width = width;
                        if (curGen.Height < min)
                        {
                            curGen.Height = min;
                            curGen.Width = (curGen.Area / curGen.Height);
                        }
                        if (curGen.Height > REvo.Canvas.Height)
                        {
                            curGen.Height = REvo.Canvas.Height;
                            curGen.Width = (curGen.Area / curGen.Height);
                        }
                    }
                }
            }
        }

        //==============================================================================================
        public double Overlap()
        {
            double overlapAreaSum = 0;
            double shapeArea;
            double rectArea = 0;
            RectangleF rect1, rect2;
            RectangleF isectRect;

            //Parallel.For(0, Gens.Count, delegate(int i)
            for (int i = 0; i < Gens.Count; i++)
            {
                rect1 = new RectangleF((float)(Gens[i].Points[0].X), (float)(Gens[i].Points[0].Y), (float)(Gens[i].Width), (float)(Gens[i].Height));
                rectArea = rect1.Width * rect1.Height;
                for (int k = i + 1; k < Gens.Count; k++)
                {
                    rect2 = new RectangleF((float)(Gens[k].Points[0].X), (float)(Gens[k].Points[0].Y), (float)(Gens[k].Width), (float)(Gens[k].Height));
                    isectRect = RectangleF.Intersect(rect1, rect2);
                    shapeArea = (isectRect.Width) * (isectRect.Height);
                    overlapAreaSum = overlapAreaSum + shapeArea;
                }
                isectRect = RectangleF.Intersect(rect1, REvo.Canvas);
                shapeArea = isectRect.Width * isectRect.Height;
                overlapAreaSum += rectArea - shapeArea;
            } //);
            return overlapAreaSum;
        }


        //==============================================================================================
        public double Overlap(RGen firstGen, RGen secondGen)
        {
            double overlapAreaSum = 0;
            double shapeArea;
            RectangleF rect1, rect2;
            RectangleF isectRect;

            rect1 = new RectangleF((float)(firstGen.Points[0].X), (float)(firstGen.Points[0].Y), (float)(firstGen.Width), (float)(firstGen.Height));
            rect2 = new RectangleF((float)(secondGen.Points[0].X), (float)(secondGen.Points[0].Y), (float)(secondGen.Width), (float)(secondGen.Height));
            isectRect = RectangleF.Intersect(rect1, rect2);
            shapeArea = isectRect.Width * isectRect.Height;
            overlapAreaSum = overlapAreaSum + shapeArea;

            return overlapAreaSum;
        }

        //==============================================================================================
        // calculate the distances 
        public double Distances()
        {
            RPolygon fistPoly, secondPoly, testPoly;
            double sum = 0, dist = 0;

            for (int i = 0; i < Gens.Count; i++)
            {
                fistPoly = new RPolygon(Pens.Black, Brushes.LightGray, Gens[i]); //(RPolygon)objects[i];
                for (int m = 0; m < fistPoly.neighbours.Count; m++)
                {
                    for (int k = i+1; k < Gens.Count; k++)
                    {
                        testPoly = new RPolygon(Pens.Black, Brushes.LightGray, Gens[k]); //(RPolygon)objects[k];
                        if (fistPoly.neighbours[m] == testPoly.Id)
                        {
                            secondPoly = testPoly;
                            dist = fistPoly.DistObj(secondPoly);
                            if (dist < 0) dist = 0;
                            //dist *= 10;
                            dist += AdditionalDist(fistPoly, secondPoly, dist);
                            if (dist < 0) dist = 0;
                            dist += AdditionalDistFromOverlap(Gens[i], Gens[k]);
                            sum += dist;
                            secondPoly = null;
                            break;
                        }
                        testPoly = null;
                        secondPoly = null;
                    }
                }
                fistPoly = null;
            }
            return (int)Math.Round(sum);
        }

        //==============================================================================================
        public double AdditionalDistFromOverlap(RGen fistGen, RGen secondGen)
        {
            double addDist = 0;

            RectangleF rect1, rect2;
            RectangleF isectRect;

            rect1 = new RectangleF((float)(fistGen.Points[0].X), (float)(fistGen.Points[0].Y), (float)(fistGen.Width), (float)(fistGen.Height));
            rect2 = new RectangleF((float)(secondGen.Points[0].X), (float)(secondGen.Points[0].Y), (float)(secondGen.Width), (float)(secondGen.Height));
            isectRect = RectangleF.Intersect(rect1, rect2);
            if (isectRect != null)
            {
                if (isectRect.Width < isectRect.Height) addDist = isectRect.Width;
                else addDist = isectRect.Height;
            }

            if (addDist <= RParameter.addDist) addDist = 0;
            else addDist = addDist - RParameter.addDist;

            return addDist;
        }

        //==============================================================================================
        private int AdditionalDist(RPolygon fistPoly, RPolygon secondPoly, double dist)
        {
            // the concept for this method is the same as for the Collision detection from http://www.codeproject.com/KB/GDI-plus/PolygonCollision.aspx
            fistPoly.FindMinMaxXY();
            secondPoly.FindMinMaxXY();
            int addDist = RParameter.addDist; //(int)((Form1)Application.OpenForms[0]).uD_RLap.Value; // minimum required distance
            int innerDist = 0;
            int tmpDist = 0;

            // --- test in x-direction ---
            if (fistPoly.minX < secondPoly.minX)
            {
                tmpDist = secondPoly.minX - fistPoly.maxX;
            } else
            {
                tmpDist = fistPoly.minX - secondPoly.maxX;
            }
            if (tmpDist < 0) innerDist += -1 * tmpDist;

            // --- test in y-direction ---
            if (fistPoly.minY < secondPoly.minY)
            {
                tmpDist = secondPoly.minY - fistPoly.maxY;
            }
            else
            {
                tmpDist = fistPoly.minY - secondPoly.maxY;
            }
            if (tmpDist < 0) innerDist += -1 * tmpDist;

            int returnDist = (addDist - innerDist);
            if (returnDist < 0) returnDist = 0;
         //   int test = (returnDist - (int)Math.Round(dist, 0));

         //   if (test < 0) test = 0;
         //   return test;// returnDist;
            return returnDist;
        }

        //==============================================================================================
        public void EvaluationProportion()
        {
            RGen curGen;
            double ratio = 0, sum = 0;
            for (int k = 0; k < Gens.Count; k++)
            {
                curGen = Gens[k];
                if (curGen.Width <= curGen.Height)
                    ratio = curGen.Width / curGen.Height;
                else
                    ratio = curGen.Height / curGen.Width;
                sum += ratio;
            }
            ProportionRatio = sum / Gens.Count;
        }
    }
}
       
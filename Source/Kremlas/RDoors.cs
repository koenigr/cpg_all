﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = RDoors.cs 
//  Copyright (C) 2014/10/18  8:25 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace CPlan.Kremlas
{
    public class RDoors
    {
        private double m_doorMover = 0;
        public GraphicsPath DoorPath = new GraphicsPath();
        private String m_vertOrHor;
        Vector m_refPoint, m_doorVectA, m_doorVectB, m_maxDoor, m_minDoor;
        public double DoorMover
        {
            get { return m_doorMover; }
            set { m_doorMover = value; }
        }

        public RDoors(String VertOrHor, Vector refPoint, Vector doorVectA, Vector doorVectB, Vector maxDoor, Vector minDoor)
        {
            m_vertOrHor = VertOrHor;
            m_refPoint = refPoint;
            m_doorVectA = doorVectA;
            m_doorVectB = doorVectB;
            m_maxDoor = maxDoor;
            m_minDoor = minDoor;

            CreateDoor();
        }

        //==============================================================================================
        public void CreateDoor()
        {
            PointF[] doorPoints = new PointF[4];
            float doorsDepth = 20;
            Vector doorVectA = new Vector(m_doorVectA);
            Vector doorVectB = new Vector(m_doorVectB);
            DoorPath = new GraphicsPath();

            ScaleDoor(ref doorVectA, ref doorVectB);

            if (m_vertOrHor == "vertical")
            {
                doorPoints[0] = new PointF((float)m_refPoint.X - doorsDepth, (float)doorVectA.Y);
                doorPoints[1] = new PointF((float)m_refPoint.X + doorsDepth, (float)doorVectA.Y);
                doorPoints[2] = new PointF((float)m_refPoint.X + doorsDepth, (float)doorVectB.Y);
                doorPoints[3] = new PointF((float)m_refPoint.X - doorsDepth, (float)doorVectB.Y);
                DoorPath.AddPolygon(doorPoints);
            }
            if (m_vertOrHor == "horizontal")
            {
                doorPoints[0] = new PointF((float)doorVectA.X, (float)m_refPoint.Y - doorsDepth);
                doorPoints[1] = new PointF((float)doorVectA.X, (float)m_refPoint.Y + doorsDepth);
                doorPoints[2] = new PointF((float)doorVectB.X, (float)m_refPoint.Y + doorsDepth);
                doorPoints[3] = new PointF((float)doorVectB.X, (float)m_refPoint.Y - doorsDepth);
                DoorPath.AddPolygon(doorPoints);
            }
        }

        //==============================================================================================
        // --- scale the door ----------------------------
        private void ScaleDoor(ref Vector doorVectA, ref Vector doorVectB)//, Vector maxDoor, Vector minDoor)
        {
            double lengthDoor = doorVectA.DistanceTo(doorVectB);
            double lengthInsert = m_maxDoor.Magnitude;
            double lengthMin = m_minDoor.Magnitude;
            Vector doorVect = doorVectB - doorVectA;
            Vector possibleMoveV = new Vector(doorVect);
            possibleMoveV.Multiply(((lengthDoor - lengthMin) - 2 * lengthInsert) / (lengthDoor - lengthMin)); // find the possible movement vector
            possibleMoveV.Multiply(0.5);                                      // halt to the right or to the left

            if (lengthDoor > (lengthInsert * 2))
            {
                Vector doorRV = m_maxDoor;
                Vector midPt = doorVectA + doorVectB;
                midPt.Multiply(0.5); // get the middel point of the door
                possibleMoveV.Multiply(m_doorMover);//REvo.Rnd.NextDouble()*2 -1 ); // change the door position - randomly...
                midPt += possibleMoveV;
                doorVectA = midPt - doorRV;
                doorVectB = midPt + doorRV;
            }
            else if (lengthDoor > lengthInsert)
            {
                Vector doorRV = doorVectB - doorVectA - m_minDoor;// new Vector(0, -3);
                doorVectB = doorVectA + doorRV;
                doorRV = doorVectA - doorVectB - m_minDoor; // new Vector(0, -3);
                doorVectA = doorVectB + doorRV;
            }
           /* else // delete the door if it is too small
            {
                m_doorVectA = new Vector(0, 0);
                m_doorVectB = new Vector(0, 0);
            }*/
        }

    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = RGen.cs 
//  Copyright (C) 2014/10/18  8:25 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using Topology.Geometries;

namespace CPlan.Kremlas
{
    public class RGen
    {
        public Vector Velocity;
        public double LimboWidth, LimboHeight;
        public bool Locked = false;
        public double Area{get; set;}         
        public int Id;
        public List<int> Neighbours = new List<int>(); // list with neighbouring indizes
        //--------------------------------------------------------------------------------
        //private Random m_rnd = new Random();
        private double m_width, m_height;
        private double m_fitness = 0;
        private List<Vector> m_points = new List<Vector>();
        private List<Vector> m_edges = new List<Vector>();
        public LinearRing m_ring = null;
        private GraphicsPath m_path = new GraphicsPath();
        public List<GraphicsPath> Doors = new List<GraphicsPath>();

        //---------------------------------------------------------------------------------
        public double Left
        {
            get
            {
                return m_points[0].X;
            }
        }

        //---------------------------------------------------------------------------------
        public double Top
        {
            get
            {
                return m_points[0].Y;
            }
        }

        //---------------------------------------------------------------------------------
        public RGen(double x, double y, int iniR, int vId)
        {
            Velocity = new Vector(0, 0);
            Id = vId;
            RTools.Rnd = new Random(iniR);
            Vector centre = new Vector(x, y);
            m_width = m_height = RTools.Rnd.Next(10, 50);// 30;
            Area = m_width * m_height;
            m_points.Add(new Vector(centre.X - Width / 2, centre.Y - Height / 2));
            m_points.Add(new Vector(centre.X + Width / 2, centre.Y - Height / 2));
            m_points.Add(new Vector(centre.X + Width / 2, centre.Y + Height / 2));
            m_points.Add(new Vector(centre.X - Width / 2, centre.Y + Height / 2));
            BuildEdges();

        }

        //--------------------------------------------------------------------------------- 
        //--- for enviro canvas ---
        public RGen(double x, double y, double vWidth, double vHeight)
        {
            Velocity = new Vector(0, 0);
            Vector centre = new Vector(x, y);
            Area = m_width * m_height;
            m_points.Add(new Vector(centre.X - Width / 2, centre.Y - Height / 2));
            m_points.Add(new Vector(centre.X + Width / 2, centre.Y - Height / 2));
            m_points.Add(new Vector(centre.X + Width / 2, centre.Y + Height / 2));
            m_points.Add(new Vector(centre.X - Width / 2, centre.Y + Height / 2));
            BuildEdges();
        }

        // --- copy constructor -----------------------------------------------------------
        public RGen(RGen prevGen)
        {
            Velocity = new Vector(0, 0);
            m_points.Clear();
            m_points.Add(new Vector(prevGen.Points[0].X, prevGen.Points[0].Y));
            m_points.Add(new Vector(prevGen.Points[1].X, prevGen.Points[1].Y));
            m_points.Add(new Vector(prevGen.Points[2].X, prevGen.Points[2].Y));
            m_points.Add(new Vector(prevGen.Points[3].X, prevGen.Points[3].Y));
            BuildEdges();
            Id = prevGen.Id;
            Neighbours.Clear();
            for (int i = 0; i < prevGen.Neighbours.Count; i++)
            {
                Neighbours.Add(prevGen.Neighbours[i]);
            }
            Vector centre = this.Center;
            m_width = prevGen.m_width;
            m_height = prevGen.m_height;
            Area = prevGen.Area;
            Locked = prevGen.Locked;

        }
        //==============================================================================================
        //==============================================================================================
        public void BuildEdges()
        {
            Vector p1;
            Vector p2;
            m_edges.Clear();
            for (int i = 0; i < m_points.Count; i++)
            {
                p1 = m_points[i];
                if (i + 1 >= m_points.Count)
                {
                    p2 = m_points[0];
                }
                else
                {
                    p2 = m_points[i + 1];
                }
                m_edges.Add(p2 - p1);
                CreatePath();
            }
        }

        //==============================================================================================
        // --- Pfad erstellen ---
        public void CreatePath()
        {
            Vector curVect;
            Path.Reset();
            System.Drawing.PointF[] pathPoints = new System.Drawing.PointF[m_points.Count];
            for (int i = 0; i < m_points.Count; i++)
            {
                curVect = m_points[i];
                pathPoints[i].X = (float)curVect.X;
                pathPoints[i].Y = (float)curVect.Y;
            }
            Path.AddPolygon(pathPoints);
            //CreateRing();
        }

        //==============================================================================================
        public virtual void CreateRing()
        {
            PointF[] pts = m_path.PathPoints;
            Coordinate[] curCoords = new Coordinate[pts.Length + 1];

            for (int i = 0; i < pts.Length; i++)
            {
                curCoords[i] = new Coordinate(pts[i].X, pts[i].Y);
            }
            curCoords[pts.Length] = new Coordinate(pts[0].X, pts[0].Y);
            if (m_ring != null) m_ring = null;
            m_ring = new LinearRing(curCoords);
        }

        //==============================================================================================
        public double DistObj(RGen otherObj)
        {
            CreatePath(); // necessary to calculate the distances (create Ring)
            otherObj.CreatePath();
            return Ring.Distance((IGeometry)otherObj.Ring);
        }

        // ==============================================================================================
        public void Rotate90()
        {
            double tmpWidth;
            tmpWidth = Width;
            Width = Height;
            Height = tmpWidth;
        }

        //==============================================================================================
        protected LinearRing Ring
        {
            get { return m_ring; }
            set { m_ring = value; }
        }

        //==============================================================================================
        public GraphicsPath Path
        {
            get { return m_path; }
        }

        //==============================================================================================
        public void UpdatePos()
        {
            Offset(Velocity);
            Velocity.X = 0;
            Velocity.Y = 0;
        }

        //==============================================================================================
        public void UpdateForm()
        {
            Width = LimboWidth;
            Height = LimboHeight;
            LimboWidth = 0;
            LimboHeight = 0;
        }

        //==============================================================================================
        public Vector Center
        {
            get
            {
                double totalX = 0;
                double totalY = 0;
                for (int i = 0; i < m_points.Count; i++)
                {
                    totalX += m_points[i].X;
                    totalY += m_points[i].Y;
                }
                return new Vector(totalX / (double)m_points.Count, totalY / (double)m_points.Count);
            }
            set
            {
                Vector difference = (value - Center);
                Offset(difference);
            }
        }

        //==============================================================================================
        public List<Vector> Edges
        {
            get { return m_edges; }
        }

        //==============================================================================================
        public List<Vector> Points
        {
            get { return m_points; }
        }

        //==============================================================================================
        public void Offset(Vector v)
        {
            Offset(v.X, v.Y);
        }

        //==============================================================================================
        public void Offset(double x, double y)
        {
            for (int i = 0; i < m_points.Count; i++)
            {
                Vector p = m_points[i];
                m_points[i] = new Vector(p.X + x, p.Y + y);
            }
            BuildEdges();
        }

        //==============================================================================================
        public double Fitness
        {
            get { return m_fitness; }
            set { m_fitness = value; }
        }

        //==============================================================================================
        private void Resize()
        {
            Vector zentrum = this.Center;
            m_points[0] = new Vector(zentrum.X - m_width / 2, zentrum.Y - m_height / 2);
            m_points[1] = new Vector(zentrum.X + m_width / 2, zentrum.Y - m_height / 2);
            m_points[2] = new Vector(zentrum.X + m_width / 2, zentrum.Y + m_height / 2);
            m_points[3] = new Vector(zentrum.X - m_width / 2, zentrum.Y + m_height / 2);
            BuildEdges();
        }

        //==============================================================================================
        public double Width
        {
            get { return m_width; }
            set
            {
                m_width = value;
                this.Resize();
            }
        }

        //==============================================================================================
        public double Height
        {
            get { return m_height; }
            set
            {
                m_height = value;
                this.Resize();
            }

        }

         //==============================================================================================
        public void CheckHeightWidt(double m_height)
        {
            double min = 10.0;
            double dH = 1;// canvas.Height / 4;
            double dW = 1;// canvas.Width / 4;
            double m_width;

            if (m_height < min) m_height = min;                                            //-- Minimale Scalierung X
            else if (m_height > REvo.Canvas.Height - dW) m_height = REvo.Canvas.Height - dW; //-- Maximale Scalierung X
            m_width = (this.Area / m_height);
            this.Width = m_width;
            this.Height = m_height;
            if (this.Width  < min)
            {
                this.Width  = min;
                this.Height = (this.Area / this.Width );
            }
            if (this.Width > REvo.Canvas.Width - dH)
            {
                this.Width = REvo.Canvas.Width - dH;
                this.Height = (this.Area / this.Width);
            }
        }

        //==============================================================================================
        public void CheckWidtHeight(double m_width)
        {
            double min = Math.Sqrt(this.Area) *0.75;// 10.0;
            double dH = 0;// canvas.Height / 4;
            double dW = 0;// canvas.Width / 4;
            double m_height;

            if (m_width < min) m_width = min;                                            //-- Minimale Scalierung X
            else if (m_width > REvo.Canvas.Width - dW) m_width = REvo.Canvas.Width - dW; //-- Maximale Scalierung X
            m_height = (this.Area / m_width);
            this.Height = m_height;
            this.Width = m_width;
            if (this.Height < min)
            {
                this.Height = min;
                this.Width = (this.Area / this.Height);
            }
            if (this.Height > REvo.Canvas.Height - dH)
            {
                this.Height = REvo.Canvas.Height - dH;
                this.Width = (this.Area / this.Height);
            }
        }

        //==============================================================================================
        // --- calculate the overlapping rectangle ---
        public RectangleF OverlapRectangle(RGen otherGen)
        {
            RectangleF rect1 = new RectangleF((float)(this.Points[0].X), (float)(this.Points[0].Y), (float)(this.Width), (float)(this.Height));
            RectangleF rect2 = new RectangleF((float)(otherGen.Points[0].X), (float)(otherGen.Points[0].Y), (float)(otherGen.Width), (float)(otherGen.Height));
            RectangleF isectRect = RectangleF.Intersect(rect1, rect2);
            return isectRect;
        }

        //==============================================================================================
        // --- tests if one point of the list is inside the rectangle of this gen ----------------------
        public Boolean PointInside(RGen testGen)
        {
            Boolean isInside = false;
            RectangleF testRect;

            testRect = new RectangleF((float)(this.m_points[0].X), (float)(this.m_points[0].Y), (float)(this.Width), (float)(this.Height));
            for (int i = 0; i < testGen.Points.Count; i++)
            {
                PointF CurPoint = new PointF((float)testGen.m_points[i].X, (float)testGen.m_points[i].Y);
                bool test = testRect.Contains(CurPoint);
                if (test)
                {
                    isInside = true;
                    break;
                }
            }
            return isInside;
        }

        //-------------------------------------------------------------------------------------------------
        public Boolean PointInside(PointF testPoint)
        {
            RectangleF testRect = new RectangleF((float)(this.m_points[0].X), (float)(this.m_points[0].Y), (float)(this.Width), (float)(this.Height));
            Boolean isInside = testRect.Contains(testPoint);
            return isInside;
        }

        //==============================================================================================
        public double DistanceToPoint(Vector toPoint)
        {
            PointF testPoint = new PointF((float)toPoint.X, (float)toPoint.Y);
            if (PointInside(testPoint)) return 0.0; // --- if point is inside the rectangle return distance 0
            int j;
            double TempDist, MinDist = double.MaxValue;
            double[] P1 = new double[2];
            double[] P2 = new double[2];
            double[] ToP = new double[2];
            ToP[0] = toPoint.X;
            ToP[1] = toPoint.Y;
            for (int i = 0; i < m_points.Count; i++)
            {
                P1[0] = m_points[i].X;
                P1[1] = m_points[i].Y;
                j = i + 1;
                if (j == m_points.Count) j = 0;
                P2[0] = m_points[j].X;
                P2[1] = m_points[j].Y;

                TempDist = RTools.LineToPointDistance2D(P1, P2, ToP, true);
                if (TempDist < MinDist) MinDist = TempDist;
            }
            return MinDist;
        }


    }
}

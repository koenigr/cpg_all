#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = RObject.cs 
//  Copyright (C) 2014/10/18  8:25 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using Topology.Geometries;

namespace CPlan.Kremlas{

    public abstract class RGraphicObject
    {
        /// <summary>
        /// Dieses Pen-Objekt wird verwendet um zu �berpr�fen, ob ein
        /// Punkt �ber der Linie des Objekts liegt. Die Farbe ist
        /// hierf�r irrelevant. Als Breite hat sich 4 als sinnvoll erwiesen.
        /// </summary>
        static private Pen m_hitTestPen = new Pen(Brushes.Black, 4);
        private Pen m_pen;
        private Brush m_brush;
        private bool m_bVisible = true;
        private GraphicsPath m_path = new GraphicsPath();
        private LinearRing m_ring = null;
        private List<Vector> m_points = new List<Vector>();
        private List<Vector> m_edges = new List<Vector>();

        //----------------------------------------------------------------------------------------------
        public RGraphicObject(Pen pen, Brush brush)
        {
            m_pen = new Pen (pen.Color, 10);
            m_brush = brush;
        }

        //==============================================================================================
        //==============================================================================================
        protected GraphicsPath Path
        {
            get { return m_path; }
        }
        //==============================================================================================
        protected LinearRing Ring
        {
            get { return m_ring; }
            set { m_ring = value; }
        }

        //==============================================================================================
        /*public Vector Velocity
        {
            get { return _velocity; }
            set { _velocity = value; }
        }*/

        public List<Vector> Points
        {
            get { return m_points; }
        }

        //==============================================================================================
        public Pen Pen
        {
            get { return m_pen; }
            set { m_pen = value; }
        }

        //==============================================================================================
        public Brush Brush
        {
            get { return m_brush; }
            set { m_brush = value;}
        }

        //==============================================================================================
        public bool Visible
        {
            get { return m_bVisible; }
            set { m_bVisible = value; }
        }

        //==============================================================================================
        /// <summary>
        /// Befindet sich der angegebene Punkt �ber der Linie des Objekts?
        /// </summary>
        public virtual bool Hit(System.Drawing.Point pt)
        {
            return m_path.IsOutlineVisible(pt, m_hitTestPen);
        }

        //==============================================================================================
        /// <summary>
        /// Befindet sich der angegebene Punkt innerhalb des Objekts?
        /// </summary>
        public virtual bool Contains(System.Drawing.Point pt)
        {
            return m_path.IsVisible(pt);
        }

        //==============================================================================================
        public virtual void Draw(Graphics g)
        {
            g.FillPath(m_brush, m_path);
            g.DrawPath(m_pen, m_path);
        }

        //==============================================================================================
        /// <summary>
        /// Bewegt das Objekt um deltaX in x-Richtung und deltaY in y-Richtung.
        /// </summary>
        public virtual void Move(float deltaX, float deltaY)
        {
            Matrix mat = new Matrix();
            mat.Translate(deltaX, deltaY);
            m_path.Transform(mat);
        }

        //==============================================================================================
        public void BuildEdges()
        {
            Vector p1;
            Vector p2;
            m_edges.Clear();
            for (int i = 0; i < m_points.Count; i++)
            {
                p1 = m_points[i];
                if (i + 1 >= m_points.Count)
                {
                    p2 = m_points[0];
                }
                else
                {
                    p2 = m_points[i + 1];
                }
                m_edges.Add(p2 - p1);
            }
            CreatePath();

        }

        //==============================================================================================
        // --- Pfad erstellen ---
        public void CreatePath()
        {
            Vector curVect;
            Path.Reset();
            System.Drawing.PointF[] pathPoints = new System.Drawing.PointF[m_points.Count];
            for (int i = 0; i < m_points.Count; i++)
            {
                curVect = m_points[i];
                pathPoints[i].X = (float)curVect.X;
                pathPoints[i].Y = (float)curVect.Y;
            }
            Path.AddPolygon(pathPoints);
            CreateRing();
        }

        //==============================================================================================
        public virtual void CreateRing()
        {
            PointF[] pts = m_path.PathPoints;
            Coordinate[] curCoords = new Coordinate[pts.Length + 1];

            for (int i = 0; i < pts.Length; i++)
            {
                curCoords[i] = new Coordinate(pts[i].X, pts[i].Y);
            }
            curCoords[pts.Length] = new Coordinate(pts[0].X, pts[0].Y);
            if (m_ring != null) m_ring = null;
            m_ring = new LinearRing(curCoords);
        }
    }

}


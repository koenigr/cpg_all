﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = RPolygon.cs 
//  Copyright (C) 2014/10/18  8:25 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System.Collections.Generic;
using System.Drawing;
using Topology.Geometries;

namespace CPlan.Kremlas
{
    public class RPolygon : RGraphicObject
    {
        int area;
        public int Id;
        public List<int> neighbours = new List<int>(); // list with neighbouring indizes
        public int minX, maxX, minY, maxY;

        //----------------------------------------------------------------------------------------------
        public RPolygon(Pen pen, Brush brush, RGen vGen)// List<Vector> vPoints, List<int> vNeighb, int vId)
            : base(pen, brush)
        {
            Id = vGen.Id;
            neighbours = vGen.Neighbours;
            Points.Add(new Vector(vGen.Points[0].X, vGen.Points[0].Y));
            Points.Add(new Vector(vGen.Points[1].X, vGen.Points[1].Y));
            Points.Add(new Vector(vGen.Points[2].X, vGen.Points[2].Y));
            Points.Add(new Vector(vGen.Points[3].X, vGen.Points[3].Y));
            BuildEdges();
        }

        //==============================================================================================
        public double DistObj(RPolygon otherObj)
        {
            //CreateRing();
            return Ring.Distance((IGeometry)otherObj.Ring);
        }

        //==============================================================================================
        public Vector Center
        {
            get
            {
                double totalX = 0;
                double totalY = 0;
                for (int i = 0; i < Points.Count; i++)
                {
                    totalX += Points[i].X;
                    totalY += Points[i].Y;
                }
                return new Vector(totalX / Points.Count, totalY / Points.Count);
            }
            /*set
            {
                Vector dMove;
                dMove.X = value.X - Center.X;
                dMove.Y = value.Y - Center.Y;
                Offset(dMove);
            }*/
        }

        //==============================================================================================
        public void FindMinMaxXY()
        {
            System.Drawing.Point p;
            minX = 999999999;
            minY = 999999999;
            maxX = 0;
            maxY = 0;
            for (int i = 0; i < Points.Count; i++)
            {
                p = Points[i];

                if (p.X < minX)
                {
                    minX = p.X;
                }
                if (p.Y < minY)
                {
                    minY = p.Y;
                }

                if (p.X > maxX)
                {
                    maxX = p.X;
                }
                if (p.Y > maxY)
                {
                    maxY = p.Y;
                }
            }
        }

        //==============================================================================================
        /* public void rotate90()
        {
            int tmpWidth;
            tmpWidth = Width;
            Width = Height;
            Height = tmpWidth;
        }*/

        //==============================================================================================
        public int Area
        {
            get
            {
                return area;
            }
            set { area = value; }
        }

        //==============================================================================================
        public void Offset(Vector v)
        {
            Offset(v.X, v.Y);
        }

        public void Offset(double x, double y)
        {
            for (int i = 0; i < Points.Count; i++)
            {
                Vector p = Points[i];
                Points[i] = new Vector(p.X + x, p.Y + y);
            }
            Move((float)x, (float)y);
        }

        //==============================================================================================
        public override string ToString()
        {
            string result = "";

            for (int i = 0; i < Points.Count; i++)
            {
                if (result != "") result += " ";
                result += "{" + Points[i].ToString(true) + "}";
            }

            return result;
        }

    }
}

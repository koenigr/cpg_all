﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = RProportion.cs 
//  Copyright (C) 2014/10/18  8:25 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;

namespace CPlan.Kremlas
{
    public static class RProportion
    {
        static private Collision m_collision = new Collision();
        //static private Random m_rnd = new Random();

        public static void Proportion(this RGen curGen, RGen otherGen, double force, double rate)
        {
            ChangeProportion(curGen, otherGen, force, rate);
        }

        //==============================================================================================
        public static void Proportion(this RGen curGen, List<RGen> Gens, double force, double rate)
        {
            for (int i = 0; i < Gens.Count; i++)
            {
                if (curGen != Gens[i])
                {
                    ChangeProportion(curGen, Gens[i], force, rate);
                    //ChangeProportionTEST(curGen, Gens[i], force, rate);
                }
            }
        }

        //==============================================================================================
        private static void ChangeProportion(RGen thisGen, RGen otherGen, double force, double rate)
        {
            Vector velocity = new Vector(0, 0);
            Vector translation = velocity;
            Vector midP;
            PolygonCollisionResult r = m_collision.PolygonCollision(thisGen, otherGen, velocity);
            if (r.WillIntersect)
            {
                velocity.X = 0;
                velocity.Y = 0;
                translation = velocity + r.MinimumTranslationVector;
                translation.Multiply(force);
                midP = otherGen.Center - thisGen.Center;

                if (!thisGen.Locked) VaryOnesSize(rate, thisGen, translation, midP);
                if (!otherGen.Locked) VaryOnesSize(rate, otherGen, translation, midP);
            }
        }

        //==============================================================================================
        private static void VaryOnesSize(double rate, RGen curGen, Vector changer, Vector midP)
        {
            double width, height;
            double mag;
            changer.X = Math.Abs(changer.X);
            changer.Y = Math.Abs(changer.Y);
            midP.X = Math.Abs(midP.X);
            midP.Y = Math.Abs(midP.Y);
            mag = changer.Magnitude;

            if (RTools.Rnd.NextDouble() < rate)
            {
                if (!curGen.Locked)
                {
                    //if (Rnd.NextDouble() < 0.5)
                    {
                        if (midP.X > midP.Y)
                        {
                            width = curGen.Width - mag;
                        }
                        else
                        {
                            width = curGen.Width + mag;
                        }
                        curGen.CheckWidtHeight(width);
                    }
                    /*else
                    {
                        if (midP.Y > midP.X)
                        {
                            height = curGen.Height - mag;
                        }
                        else
                        {
                            height = curGen.Height + mag;
                        }
                        curGen.CheckHeightWidt(height);
                    }*/

                }
            }
        }

        //==============================================================================================
        //==============================================================================================
        //==============================================================================================
        /*private static void ChangeProportionTEST(RGen thisGen, RGen otherGen, double force, double rate)
        {
            Vector velocity = new Vector(0, 0);
            Vector translation = velocity;
            Vector curTrans = velocity;
            double MRate = 1;// 0.999;

            PolygonCollisionResult r = Collision.PolygonCollision(thisGen, otherGen, velocity);
            if (r.WillIntersect)
            {
                velocity.X = 0;
                velocity.Y = 0;
                RectangleF OverlRect = thisGen.OverlapRectangle(otherGen); // calculate the overlapping rectangle

                //translation = velocity + r.MinimumTranslationVector;
                translation.X = OverlRect.Width;
                translation.Y = OverlRect.Height;
                if (Rnd.NextDouble() < MRate)
                {
                    if (translation.X < translation.Y) translation.Y = 0;
                    else translation.X = 0;
                }
                else
                {
                    if (translation.X > translation.Y) translation.Y = 0;
                    else translation.X = 0;
                }

                translation.Multiply(force);
                translation.Multiply(0.5);

                // --- test if point is in rectangle ---
                bool PointInside = true;
                if (thisGen.PointInside(otherGen) | otherGen.PointInside(thisGen)) PointInside = true;
                else PointInside = false;

                translation.X = Math.Abs(translation.X);
                translation.Y = Math.Abs(translation.Y);

                if (PointInside) // --- Überschneidung mit Eckpunkt(en) im anderen Rechteck ---
                {
                    VaryOnesSizeTEST(rate, thisGen, translation);
                    VaryOnesSizeTEST(rate, otherGen, translation);
                }
                else // --- "komplette" Überschneidung ---
                {
                    RGen TempGen = new RGen(OverlRect.X, OverlRect.Y, OverlRect.Width, OverlRect.Height); //OverlRect.
                    bool ThisMidInOverlap = OverlRect.Contains((float)thisGen.Center.X, (float)thisGen.Center.Y);
                    bool OtherMidInOverlap = OverlRect.Contains((float)otherGen.Center.X, (float)otherGen.Center.Y);
                    Vector OverlMidPt = new Vector(OverlRect.X + 0.5 * OverlRect.Width, OverlRect.Y + 0.5 * OverlRect.Height); // mid point of the overlapping rectangle

                    if (Math.Abs(translation.X) > 0)
                    {
                        if ((thisGen.Width < otherGen.Width) && (Rnd.NextDouble() < MRate)) // Schmale Seite wird "normal" verändert
                        {
                            if (!ThisMidInOverlap)
                            {
                                curTrans = new Vector(-(thisGen.Height * 0.5 - TempGen.DistanceToPoint(thisGen.Center)), 0);
                                curTrans.Multiply(force);
                                VaryOnesSizeTEST(rate, thisGen, curTrans);
                            }
                            else
                            {
                                VaryOnesSizeTEST(rate, thisGen, translation);
                            }
                            if (!OtherMidInOverlap)
                            {
                                curTrans = new Vector(-(otherGen.Height * 0.5 - TempGen.DistanceToPoint(otherGen.Center)), 0);
                                curTrans.Multiply(force);
                                VaryOnesSizeTEST(rate, otherGen, curTrans);
                            }
                            else
                            {
                                translation.Multiply(-1);
                                VaryOnesSizeTEST(rate, otherGen, translation);
                            }
                        }
                        else // Breitere Seite wird "invertiert" verändert
                        {
                            if (!OtherMidInOverlap)
                            {
                                curTrans = new Vector(-(otherGen.Height * 0.5 - TempGen.DistanceToPoint(otherGen.Center)), 0);
                                curTrans.Multiply(force);
                                VaryOnesSizeTEST(rate, otherGen, curTrans);
                            }
                            else
                            {
                                VaryOnesSizeTEST(rate, otherGen, translation);
                            }
                            if (!ThisMidInOverlap)
                            {
                                curTrans = new Vector(-(thisGen.Height * 0.5 - TempGen.DistanceToPoint(thisGen.Center)), 0);
                                curTrans.Multiply(force);
                                VaryOnesSizeTEST(rate, thisGen, curTrans);
                            }
                            else
                            {
                                translation.Multiply(-1);
                                VaryOnesSizeTEST(rate, thisGen, translation);
                            }
                        }
                    }

                    if ((Math.Abs(translation.Y) > 0) && (Rnd.NextDouble() < MRate))
                    {
                        if (thisGen.Height < otherGen.Height) // Schmale Seite wird "normal" verändert
                        {
                            if (!ThisMidInOverlap)
                            {
                                curTrans = new Vector(0, -(thisGen.Width * 0.5 - TempGen.DistanceToPoint(thisGen.Center)));
                                curTrans.Multiply(force);
                                VaryOnesSizeTEST(rate, thisGen, curTrans);
                            }
                            else
                            {
                                VaryOnesSizeTEST(rate, thisGen, translation);
                            }
                            if (!OtherMidInOverlap)
                            {
                                curTrans = new Vector(0, -(otherGen.Width * 0.5 - TempGen.DistanceToPoint(otherGen.Center)));
                                curTrans.Multiply(force);
                                VaryOnesSizeTEST(rate, otherGen, curTrans);
                            }
                            else
                            {
                                translation.Multiply(-1);
                                VaryOnesSizeTEST(rate, otherGen, translation);
                            }
                        }
                        else // Breitere Seite wird "invertiert" verändert
                        {
                            if (!OtherMidInOverlap)
                            {
                                curTrans = new Vector(0, -(otherGen.Width * 0.5 - TempGen.DistanceToPoint(otherGen.Center)));
                                curTrans.Multiply(force);
                                VaryOnesSizeTEST(rate, otherGen, curTrans);
                            }
                            else
                            {
                                VaryOnesSizeTEST(rate, otherGen, translation);
                            }
                            if (!ThisMidInOverlap)
                            {
                                curTrans = new Vector(0, -(thisGen.Width * 0.5 - TempGen.DistanceToPoint(thisGen.Center)));
                                curTrans.Multiply(force);
                                VaryOnesSizeTEST(rate, thisGen, curTrans);
                            }
                            else
                            {
                                translation.Multiply(-1);
                                VaryOnesSizeTEST(rate, thisGen, translation);
                            }
                        }
                    }
                }
            }
        }

        //==============================================================================================
        //public void VaryOnesSize(double rate, RGen curGen, Vector midP, Vector changer)
        private static void VaryOnesSizeTEST(double rate, RGen curGen, Vector changer)
        {
            double width, height;
            double mag;
            //changer.X = Math.Abs(changer.X);
            //changer.Y = Math.Abs(changer.Y);
            mag = changer.Magnitude;

            if (Rnd.NextDouble() < rate)
            {
                if (!curGen.Locked)
                {
                    //if (midP.X > midP.Y)
                    if (Math.Abs(changer.X) > 0)
                    {
                        //curGen.limboWidth -= Math.Abs(changer.X);
                        //width = curGen.limboWidth - changer.X;
                        height = curGen.LimboHeight + changer.X;
                        CheckHeight(curGen, height);
                    }
                    if (Math.Abs(changer.Y) > 0)
                    {
                        //curGen.limboHeight -= Math.Abs(changer.Y);
                        //height = curGen.limboHeight - changer.Y;
                        width = curGen.LimboWidth + changer.Y;
                        CheckWidth(curGen, width);
                    }
                    //CheckWidt(curGen, width);
                }
            }
        }

        private static void CheckWidth(RGen curGen, double width)
        {
            double min = 10.0;
            double dH = 10.0;// canvas.Height / 4;
            double dW = 10.0;// canvas.Width / 4;
            double height;

            if (width < min) width = min;                                //-- Minimale Scalierung X
            else if (width > REvo.Canvas.Width - dW) width = REvo.Canvas.Width - dW; //-- Maximale Scalierung X
            height = (curGen.Area / width);
            curGen.LimboHeight = height;
            curGen.LimboWidth = width;
            if (curGen.LimboHeight < min)
            {
                curGen.LimboHeight = min;
                curGen.LimboWidth = (curGen.Area / curGen.LimboHeight);
            }
            if (curGen.LimboHeight > REvo.Canvas.Height - dH)
            {
                curGen.LimboHeight = REvo.Canvas.Height - dH;
                curGen.LimboWidth = (curGen.Area / curGen.LimboHeight);
            }
        }

        //==============================================================================================
        private static void CheckHeight(RGen curGen, double height)
        {
            double min = 10.0;
            double dH = 1;// canvas.Height / 4;
            double dW = 1;// canvas.Width / 4;
            double width;

            if (height < min) height = min;                                //-- Minimale Scalierung X
            else if (height > REvo.Canvas.Height - dH) height = REvo.Canvas.Height - dH; //-- Maximale Scalierung X
            width = (curGen.Area / height);
            curGen.LimboHeight = height;
            curGen.LimboWidth = width;

            if (curGen.LimboWidth < min)
            {
                curGen.LimboWidth = min;
                curGen.LimboHeight = (curGen.Area / curGen.LimboWidth);
            }
            if (curGen.LimboWidth > REvo.Canvas.Width - dW)
            {
                curGen.LimboWidth = REvo.Canvas.Width - dW;
                curGen.LimboHeight = (curGen.Area / curGen.LimboWidth);
            }
        }*/
    }
}

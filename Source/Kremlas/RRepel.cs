﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = RRepel.cs 
//  Copyright (C) 2014/10/18  8:25 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System.Collections.Generic;

namespace CPlan.Kremlas
{
    public static class RRepel
    {
        static private Collision m_collision = new Collision();

        //==============================================================================================
        public static void Repel(this RGen curGen, RGen otherGen, double force)
        {
            RepelMe(curGen, otherGen, force);
        }

        //==============================================================================================
        public static void Repel(this RGen curGen, List<RGen> Gens, double force)
        {
            for (int i = 0; i < Gens.Count; i++)
            {
                if (curGen != Gens[i])
                {
                   // if (rnd.NextDouble() < 0.15)
                    RepelMe(curGen, Gens[i], force);
                }
            }
        }

        //==============================================================================================
        public static void RepelMe(RGen thisGen, RGen otherGen, double force)
        {
            Vector velocity = new Vector(0, 0);
            Vector translation = velocity;
            //double factor = 0.5;
            PolygonCollisionResult r = m_collision.PolygonCollision(thisGen, otherGen, velocity);
            if (r.WillIntersect)
            {
                translation = velocity + r.MinimumTranslationVector;
                translation.Multiply(force);

                //if ((!thisGen.Locked) & (!otherGen.Locked)) thisGen.velocity.Add(translation); //thisGen.Offset(translation);
                //if (!thisGen.Locked) thisGen.velocity.Add(translation); 
                if (!thisGen.Locked) thisGen.Offset(translation);

                translation.Invert();
                //if ((!otherGen.Locked) & (!thisGen.Locked)) otherGen.velocity.Add(translation);  //otherGen.Offset(translation);
                //if (!otherGen.Locked) otherGen.velocity.Add(translation);  
                if (!otherGen.Locked) otherGen.Offset(translation);
            }
        }
    }
}

#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Vector.cs 
//  Copyright (C) 2014/10/18  8:25 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Drawing;

namespace CPlan.Kremlas
{

	public struct Vector {

		public double X;
        public double Y;

		static public Vector FromPoint(Point p) {
			return Vector.FromPoint(p.X, p.Y);
		}

		static public Vector FromPoint(int x, int y) {
            return new Vector((double)x, (double)y);
		}

        public Vector(double x, double y)
        {
			this.X = x;
			this.Y = y;
		}

        public Vector(Vector v)
        {
            this.X = v.X;
            this.Y = v.Y;
        }

        public double Magnitude
        {
            get { return (double)Math.Sqrt(X * X + Y * Y); }
		}

		public void Normalize() {
            double magnitude = Magnitude;
			X = X / magnitude;
			Y = Y / magnitude;
		}

        public void Add(Vector toAdd)
        {
            this.X += toAdd.X;
            this.Y += toAdd.Y;
        }

        public void Multiply(double value)
        {
            this.X *= value;
            this.Y *= value;
        }

        public void Invert()
        {
            this.X = -this.X;
            this.Y = -this.Y;
        }

		public Vector GetNormalized() {
            double magnitude = Magnitude;

			return new Vector(X / magnitude, Y / magnitude);
		}

        /*public static Vector operator -(Vector v1, Vector v2)
        {
            return
            (
               new Vector
               (
                   v1.X - v2.X,
                   v1.Y - v2.Y
               )
            );
        }*/

        public double DotProduct(Vector vector)
        {
			return this.X * vector.X + this.Y * vector.Y;
		}

        public double DistanceTo(Vector vector)
        {
            return (double)Math.Sqrt(Math.Pow(vector.X - this.X, 2) + Math.Pow(vector.Y - this.Y, 2));
		}

		public static implicit operator Point(Vector p) {
			return new Point((int)p.X, (int)p.Y);
		}

		//public static implicit operator PointF(Vector p) {
		//	return new PointF(p.X, p.Y);
		//}

		public static Vector operator +(Vector a, Vector b) {
			return new Vector(a.X + b.X, a.Y + b.Y);
		}

		public static Vector operator -(Vector a) {
			return new Vector(-a.X, -a.Y);
		}

		public static Vector operator -(Vector a, Vector b) {
			return new Vector(a.X - b.X, a.Y - b.Y);
		}

        public static Vector operator *(Vector a, double b)
        {
			return new Vector(a.X * b, a.Y * b);
		}

		public static Vector operator *(Vector a, int b) {
			return new Vector(a.X * b, a.Y * b);
		}

		public override bool Equals(object obj) {
			Vector v = (Vector)obj;

			return X == v.X && Y == v.Y;
		}

		public bool Equals(Vector v) {
			return X == v.X && Y == v.Y;
		}

		public override int GetHashCode() {
			return X.GetHashCode() ^ Y.GetHashCode();
		}

		public static bool operator ==(Vector a, Vector b) {
			return a.X == b.X && a.Y == b.Y;
		}

		public static bool operator !=(Vector a, Vector b) {
			return a.X != b.X || a.Y != b.Y;
		}

		public override string ToString() {
			return X + ", " + Y;
		}

		public string ToString(bool rounded) {
			if (rounded) {
				return (int)Math.Round(X) + ", " + (int)Math.Round(Y);
			} else {
				return ToString();
			}
		}


	}

}

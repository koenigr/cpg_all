#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Vector2D.cs 
//  Copyright (C) 2014/10/18  8:25 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections;

///////////////////////////////////////////////////////////////////////////////

namespace CPlan.Kremlas
{
    public class Vector2D : ICloneable, IEnumerable, IComparable
    {
        /// <summary>
        /// Global precision for any calculation
        /// </summary>
        public static int Precision = 10;
        
        //member variables
        private double[] m_dCoord = new double[2];

        //constructor
        public Vector2D()
        {
            m_dCoord[0] = 0.0;
            m_dCoord[1] = 0.0;
        }

        public Vector2D(double a_dX, double a_dY)
        {
            m_dCoord[0] = a_dX;
            m_dCoord[1] = a_dY;
        }

        public Vector2D(double[] a_pCoord)
        {
            m_dCoord[0] = a_pCoord[0];
            m_dCoord[1] = a_pCoord[1];
        }

        public Vector2D(Vector2D a_Vector2D)
        {
            m_dCoord[0] = a_Vector2D.m_dCoord[0];
            m_dCoord[1] = a_Vector2D.m_dCoord[1];
        }

        //methods
        public double X
        {
            get { return m_dCoord[0]; }
            set { m_dCoord[0] = Math.Round(value,Precision); }
        }

        public double Y
        {
            get { return m_dCoord[1]; }
            set { m_dCoord[1] = Math.Round(value,Precision); }
        }

		public void CopyInto(Vector2D a_vec)
		{
			X = a_vec.X;		Y = a_vec.Y;
		}

        /// Gets or sets the value of the vector at the given index
        public double this[int i]
        {
            get { return m_dCoord[i]; }
            set { m_dCoord[i] = Math.Round(value,Precision); }
        }

        public int Dim
        {
            get
            {
                return m_dCoord.Length;
            }
        }

        public Vector2D normalVector()
        {
			double tmp = m_dCoord[0];
			m_dCoord[0] = -m_dCoord[1];
			m_dCoord[1] = tmp;
			return this;
        }

		public Vector2D oppositeVector()
		{
			m_dCoord[0] = -m_dCoord[0];
			m_dCoord[1] = -m_dCoord[1];
			return this;
		}

        public Vector2D add(Vector2D a_Vector2D)
        {
            m_dCoord[0] += a_Vector2D.m_dCoord[0];
            m_dCoord[1] += a_Vector2D.m_dCoord[1];
            return this;
        }

        public Vector2D add(double a_dScalar)
        {
            m_dCoord[0] += a_dScalar;
            m_dCoord[1] += a_dScalar;
            return this;
        }

        public Vector2D subtract(Vector2D a_Vector2D)
        {
            m_dCoord[0] -= a_Vector2D.m_dCoord[0];
            m_dCoord[1] -= a_Vector2D.m_dCoord[1];
            return this;
        }

        public Vector2D subtract(double a_dScalar)
        {
            m_dCoord[0] -= a_dScalar;
            m_dCoord[1] -= a_dScalar;
            return this;
        }

        public Vector2D sum(Vector2D a_Vector2DA, Vector2D a_Vector2DB)
        {
            m_dCoord[0] = a_Vector2DA.m_dCoord[0] + a_Vector2DB.m_dCoord[0];
            m_dCoord[1] = a_Vector2DA.m_dCoord[1] + a_Vector2DB.m_dCoord[1];
            return this;
        }

        public Vector2D difference(Vector2D a_Vector2DA, Vector2D a_Vector2DB)
        {
            m_dCoord[0] = a_Vector2DA.m_dCoord[0] - a_Vector2DB.m_dCoord[0];
            m_dCoord[1] = a_Vector2DA.m_dCoord[1] - a_Vector2DB.m_dCoord[1];
            return this;
        }

        public Vector2D multiply(double a_dScalar)
        {
            m_dCoord[0] *= a_dScalar;
            m_dCoord[1] *= a_dScalar;
            return this;
        }

        public Vector2D product(double a_dScalar, Vector2D a_Vector2D)
        {
            m_dCoord[0] = a_Vector2D.m_dCoord[0];
            m_dCoord[0] *= a_dScalar;
            m_dCoord[1] = a_Vector2D.m_dCoord[1];
            m_dCoord[1] *= a_dScalar;
            return this;
        }

        public double dotProduct(Vector2D a_Vector2D)
        {
            double t_dSum = 0.0;
            double t_dProduct = 0.0;

            t_dProduct = m_dCoord[0];
            t_dProduct *= a_Vector2D.m_dCoord[0];
            t_dSum += t_dProduct;
            t_dProduct = m_dCoord[1];
            t_dProduct *= a_Vector2D.m_dCoord[1];
            t_dSum += t_dProduct;
            return t_dSum;
        }

        /*---------  distance_Point_Line ----- Abstand Punkt Gerade --------
        mit a_VectorA - Startpunkt der Linie
            a_VectorB - Endpunkt der Linie
            PointOfIntersection - Lotfu�punkt
            this - Punkt
        --------------------------------------------------------------------*/
        public double distance_Point_Line(Vector2D a_VectorA, Vector2D a_VectorB, Vector2D PointOfIntersection)
        {
            Vector2D calc = a_VectorA;
            double distance = 0.0;

            calc.difference(a_VectorA, a_VectorB);
            double norm = calc.norm();

            double u = (((this.X - a_VectorA.X) * (a_VectorB.X - a_VectorA.X)) +
                         ((this.Y - a_VectorA.Y) * (a_VectorB.Y - a_VectorA.Y))) /
                       (norm * norm);

            if (u < 0.0 || u > 1.0)
                return -1.0;   // closest point does not fall within the line segment

            PointOfIntersection.X = a_VectorA.X + u * (a_VectorB.X - a_VectorA.X);
            PointOfIntersection.Y = a_VectorA.Y + u * (a_VectorB.Y - a_VectorA.Y);

            distance = this.distance(PointOfIntersection);

            return distance;
        }

        public double normSqr()
        {
            return dotProduct(this);
        }

        public double norm()
        {
            return Math.Sqrt(normSqr());
        }

        public double cosSqr(Vector2D a_Vector2D)
        {
            double t_dResult = dotProduct(a_Vector2D);
            t_dResult *= t_dResult;
            t_dResult /= normSqr();
            t_dResult /= a_Vector2D.normSqr();
            return t_dResult;
        }

        public double cos(Vector2D a_Vector2D)
        {
            double t_dResult = dotProduct(a_Vector2D);
            t_dResult /= norm();
            t_dResult /= a_Vector2D.norm();
            return t_dResult;
        }

        public double distanceSqr(Vector2D a_Vector2D)
        {
            Vector2D t_PointDifference = (Vector2D)this.Clone();
            t_PointDifference.subtract(a_Vector2D);
            return t_PointDifference.normSqr();
        }

        public double distance(Vector2D a_Vector2D)
        {
            Vector2D t_PointDifference = (Vector2D)this.Clone();
            t_PointDifference.subtract(a_Vector2D);
            return t_PointDifference.norm();
        }

        /// <summary>
        /// Get the distance of two vectors
        /// </summary>
        public static double Dist(Vector2D V1, Vector2D V2)
        {
            if (V1.Dim != V2.Dim)
                return -1;
            int i;
            double E = 0, D;
            for (i = 0; i < V1.Dim; i++)
            {
                D = (V1[i] - V2[i]);
                E += D * D;
            }
            return E;

        }

        /// <summary>
        /// Scale all elements by r
        /// </summary>
        /// <param name="r">The scalar</param>
        public void Multiply(double r)
        {
            int i;
            for (i = 0; i < m_dCoord.Length; i++)
            {
                m_dCoord[i] *= r;
            }
        }

        public double SquaredLength
        {
            get
            {
                return this*this;
            }
        }

        public double slope(Vector2D a_Vector2D)
        {
            double t_dSlope = 1.0 - cos(a_Vector2D);

            if (normalVector().dotProduct(a_Vector2D) < 0.0)
            {
                return -t_dSlope;
            }

            return t_dSlope;
        }

        public double atan(Vector2D a_Vector2D)  // --> [0, 2Pi)
        {
            double t_dDX = a_Vector2D.X - X;
            double t_dDY = a_Vector2D.Y - Y;

            if (t_dDY < 0.0) return 2 * Math.PI + Math.Atan2(-t_dDY, -t_dDX);

            return Math.Atan2(t_dDY, t_dDX);
        }

        public double det(Vector2D a_Vector2D)
        {
            return X * a_Vector2D.Y - a_Vector2D.X * Y;
        }

        public double direction(Vector2D a_Vector2D)
        {
            double t_dDX = a_Vector2D.X - X;
            double t_dDY = a_Vector2D.Y - Y;

            if (t_dDX == 0.0 && t_dDY == 0.0)
            {
                return -1.0; // Undefined!
            }

            if (t_dDX < 0.0)
            {
                if (t_dDY < 0.0) // 3. Quadrant
                {
                    return 2.0 + t_dDY / (t_dDX + t_dDY);
                }
                else // 2. Quadrant
                {
                    return 1.0 + t_dDX / (t_dDX - t_dDY);
                }
            }
            else
            {
                if (t_dDY < 0.0) // 4. Quadrant
                {
                    return 3.0 + t_dDX / (t_dDX - t_dDY);
                }
                else // 1. Quadrant
                {
                    return t_dDY / (t_dDX + t_dDY);
                }
            }
        }

        public double half_direction(Vector2D a_Vector2D)
        {
            double t_dDX = a_Vector2D.X - X;
            double t_dDY = a_Vector2D.Y - Y;

            if (t_dDX == 0.0 && t_dDY == 0.0)
            {
                return -1.0; // Undefined!
            }

            if (t_dDX < 0.0)
            {
                if (t_dDY < 0.0) // 3. Quadrant
                {
                    return t_dDY / (t_dDX + t_dDY);
                }
                else // 2. Quadrant
                {
                    if (t_dDY == 0.0) return 0.0;
                    return 1.0 + t_dDX / (t_dDX - t_dDY);
                }
            }
            else
            {
                if (t_dDY < 0.0) // 4. Quadrant
                {
                    return 1.0 + t_dDX / (t_dDX - t_dDY);
                }
                else // 1. Quadrant
                {
                    return t_dDY / (t_dDX + t_dDY);
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return m_dCoord.GetEnumerator();
        }

        public virtual bool Equals(Vector2D a_Vector2D)
        {
            if (Math.Round(X, Precision) != Math.Round(a_Vector2D.X, Precision))
                return false;
            if (Math.Round(Y, Precision) != Math.Round(a_Vector2D.Y, Precision))
                return false;
            return true;
        }

        /// <summary>
        /// Compares this vector with another one
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            Vector2D B = obj as Vector2D;
            if(B==null || m_dCoord.Length != B.m_dCoord.Length)
                return false;
            int i;
            for(i=0;i<m_dCoord.Length;i++)
            {
                if(Math.Abs(m_dCoord[i]-B.m_dCoord[i])>1e-10)
                    return false;
            }
            return true;
        }

        /*        public static bool operator ==(Vector2D a_Vector2D1, Vector2D a_Vector2D2)
                {
                    return Object.Equals(a_Vector2D1, a_Vector2D2);
                }

                public static bool operator !=(Vector2D a_Vector2D1, Vector2D a_Vector2D2)
                {
                    return !(a_Vector2D1 == a_Vector2D2);
                }
        */
        /// <summary>
        /// Retrieves a hashcode that is dependent on the elements
        /// </summary>
        /// <returns>The hashcode</returns>
        public override int GetHashCode()
        {
            int Erg = 0;
            foreach(double D in m_dCoord)
                Erg = Erg ^ Math.Round(D,Precision).GetHashCode();
            return Erg;
        }

        public virtual object Clone()
        {
            return new Vector2D(m_dCoord);
        }

		public Vector2D Copy()
		{
			return new Vector2D(m_dCoord);
		}

        /// <summary>
        /// Compare two vectors
        /// </summary>
        public int CompareTo(object obj)
        {
            Vector2D A = this;
            Vector2D B = obj as Vector2D;
            if(A==null || B==null)
                return 0;
            double Al,Bl;
            Al = A.SquaredLength;
            Bl = B.SquaredLength;
            if(Al>Bl)
                return 1;
            if(Al<Bl)
                return -1;
            int i;
            for(i=0;i<A.Dim;i++)
            {
                if(A[i]>B[i])
                    return 1;
                if(A[i]<B[i])
                    return -1;
            }
            return 0;
        }

        public bool IsInBoundingBox(Vector2D MinPoint, Vector2D MaxPoint)
        {
            if (X >= MinPoint.X && Y >= MinPoint.Y && X <= MaxPoint.X && Y <= MaxPoint.Y)
            {
                return true;
            }
            return false;
        }

        public static Vector2D operator +(Vector2D coord1, Vector2D coord2)
        {
            return new Vector2D(coord1.X + coord2.X, coord1.Y + coord2.Y);
        }

        public static Vector2D operator +(Vector2D coord1, double d)
        {
            return new Vector2D(coord1.X + d, coord1.Y + d);
        }

        public static Vector2D operator +(double d, Vector2D coord1)
        {
            return coord1 + d;
        }

        public static double operator * (Vector2D A, Vector2D B)
        {
            if(A.Dim!=B.Dim)
                throw new Exception("Vectors of different dimension!");
            double Erg = 0;
            int i;
            for(i=0;i<A.Dim;i++)
                Erg+=A[i]*B[i];
            return Erg;
        }

        public static Vector2D operator *(Vector2D coord1, double d)
        {
            return new Vector2D(coord1.X * d, coord1.Y * d);
        }

        public static Vector2D operator *(double d, Vector2D coord1)
        {
            return coord1 * d;
        }

        public static Vector2D operator -(Vector2D coord1, Vector2D coord2)
        {
            return new Vector2D(coord1.X - coord2.X, coord1.Y - coord2.Y);
        }

        public static Vector2D operator -(Vector2D coord1, double d)
        {
            return new Vector2D(coord1.X - d, coord1.Y - d);
        }

        public static Vector2D operator -(double d, Vector2D coord1)
        {
            return coord1 - d;
        }

        public static Vector2D operator /(Vector2D coord1, Vector2D coord2)
        {
            return new Vector2D(coord1.X / coord2.X, coord1.Y / coord2.Y);
        }

        public static Vector2D operator /(Vector2D coord1, double d)
        {
            return new Vector2D(coord1.X / d, coord1.Y / d);
        }

        public static Vector2D operator /(double d, Vector2D coord1)
        {
            return coord1 / d;
        }

		public static bool operator <(Vector2D a_pVec1, Vector2D a_pVec2)
		{
			if (a_pVec1.X < a_pVec2.X) return true;
			if (a_pVec2.X < a_pVec1.X) return false;
			if (a_pVec1.Y < a_pVec2.Y) return true;

			return false;
		}
		public static bool operator >(Vector2D a_pVec1, Vector2D a_pVec2)
		{
			if (a_pVec1 < a_pVec2) return false;
			if (a_pVec1 == a_pVec2) return false;

			return true;
		}
    }
}
///////////////////////////////////////////////////////////////////////////////

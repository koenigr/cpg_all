﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPlan.VisualObjects;
using OpenTK.Graphics.OpenGL;
using CPlan.Geometry;

namespace MyGeometryViewer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void glCameraControl1_Resize(object sender, EventArgs e)
        {
            var curCtrl = (GLCameraControl)sender;
            if (!curCtrl.Loaded) return;
            int w = curCtrl.Width;
            int h = curCtrl.Height;
            curCtrl.MakeCurrent();
            GL.Viewport(0, 0, w, h);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GRectangle myRect = new GRectangle(1, 1, 200, 100);
            myRect.Filled = true;
            myRect.FillColor = ConvertColor.CreateColor4(Color.LightGray);
            glCameraControl1.AllObjectsList.Add(myRect, true, true);
            glCameraControl1.ZoomAll();
            glCameraControl1.Invalidate();
        }
    }
}

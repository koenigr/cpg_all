﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = ChromosomeLayoutSingle.cs 
//  Copyright (C) 2014/10/18  7:52 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using AForge.Genetic;
using CPlan.Geometry;

namespace CPlan.Optimization
{
    [Serializable]
    public class ChromosomeLayoutSingle : ChromosomeLayout, IChromosome 
    {

        double fitness;

        /// <summary>
        /// The fitness values for each criteria.
        /// </summary>
        public double Fitness
        {
            get { return fitness; }
        }

         //----------------------------------------------------------------------------------------------
        public ChromosomeLayoutSingle(Poly2D border, ParametersLayout layoutParameters, int identity = -1)
            : base(border, layoutParameters, identity)
        {  }

        /// <summary>
        /// Evaluate chromosome with specified fitness function.
        /// </summary>
        /// <param name="function">Fitness function to use for evaluation of the chromosome.</param>
        /// <remarks><para>Calculates chromosome's fitness values using the specifed fitness function.</para></remarks>
        public void Evaluate(IFitnessFunction function)
        {
            fitness = function.Evaluate(this);
        }

        /// <summary>
        /// Clone the chromosome.
        /// </summary>
        public new IChromosome Clone()
        {
            return new ChromosomeLayoutSingle(this);
        }

        /// <summary>
		/// Copy Constructor.
		/// </summary>
        protected ChromosomeLayoutSingle(ChromosomeLayoutSingle source)
            : base(source)
		{            
            fitness = source.fitness;
		}

        /// <summary>
        /// Create new random chromosome (factory method)
        /// </summary>
        public new IChromosome CreateNew()
        {
            return new ChromosomeLayoutSingle(Border, LayoutParameters, m_ID++);
        }

        //==============================================================================================
        /// <summary>
        /// Mutation operator
        /// </summary>
        public override void Mutate()
        {
            double repForce = ControlParameters.repForce;
            double repRate = ControlParameters.repRate;
            double propForce = ControlParameters.propForce;
            double propRate = ControlParameters.propRate;

            // --- mutation operators ---
            int moveDist = (int)Math.Round((decimal)((BoundingRect.Height + BoundingRect.Width) / 8), 0); // NrBuildings), 0);

            double rate = MinNrBuildings / 100.0;
            MoveRandon(rate / 2, moveDist);
            //  this.VarySizeFull(roomsNr / 500, moveDist);
            MoveRandon(rate, moveDist / 10);
            Adapt();
        }

        public override void Adapt()
        {
            double repForce = ControlParameters.repForce;
            double repRate = ControlParameters.repRate;
            double propForce = ControlParameters.propForce;
            double propRate = ControlParameters.propRate;
            for (int i = 0; i < 25; i++)
            {
                base.Adaption(propForce, propRate, repForce, repRate);
            }
            CheckBorder();
            CheckPolyBorder();
        }

        /// <summary>
        /// Crossover operator
        /// </summary>
        public void Crossover(IChromosome pair)
        {
            CrossoverTopo(pair);
        }

        //==============================================================================================
        private void CrossoverTopo(IChromosome pair)
        {
            ChromosomeLayoutSingle other = (ChromosomeLayoutSingle)pair;
            // check for correct pair
            if (other != null)
            {

                ChromosomeLayoutSingle[] curChild = new ChromosomeLayoutSingle[2];
                ChromosomeLayoutSingle[] tmpChild = new ChromosomeLayoutSingle[2];

                Double[] normFit = new Double[MinNrBuildings];
                int[] selIdx = new int[2]; // --- index of selected parentsForm
                //     Double sumFit = 0, testSum = 0;
                int rndIdx1, rndIdx2;
                //int rouletteCter;
                //     double rndVal;
                GenBuildingLayout child1, child2, parent1, parent2;
                int nrPolys = MinNrBuildings;

                for (int i = 0; i < 2; i++)
                {
                    selIdx[i] = RndGlobal.Rnd.Next(MinNrBuildings);
                }
                //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                tmpChild[0] = new ChromosomeLayoutSingle(this); // temporary old parents
                tmpChild[1] = new ChromosomeLayoutSingle(other);

                curChild[0] = new ChromosomeLayoutSingle(this); // the new created children
                curChild[1] = new ChromosomeLayoutSingle(other);

                //-----------------------------------------------------
                rndIdx1 = RndGlobal.Rnd.Next(nrPolys - 1);
                int tmp = nrPolys - rndIdx1;
                rndIdx2 = rndIdx1 + RndGlobal.Rnd.Next(tmp);
                List<int> idxChild1 = new List<int>();
                List<int> idxChild2 = new List<int>();
                List<int> fehlerTest = new List<int>();

                // fill the index-List with parents current indexes 
                for (int i = 0; i < nrPolys; i++)
                {
                    idxChild1.Add(Gens[i].Id);
                    idxChild2.Add(other.Gens[i].Id);
                    fehlerTest.Add(Gens[i].Id);
                }

                // crossover
                for (int i = 0; i < nrPolys; i++)
                {
                    child1 = curChild[0].Gens[i];
                    child2 = curChild[1].Gens[i];
                    parent1 = tmpChild[0].Gens[i];
                    parent2 = tmpChild[1].Gens[i];

                    child1.Id = -1;
                    child2.Id = -1;

                    // exchange chromosome middle part 
                    if ((rndIdx1 <= i) & (i <= rndIdx2))
                    {
                        curChild[0].ReplaceGen(child1, parent2);
                        curChild[1].ReplaceGen(child2, parent1);
                        idxChild1.Remove(parent2.Id);
                        idxChild2.Remove(parent1.Id);
                    }
                }

                //CheckDoubleEntries(curChild[0]);

                // keep the positions that are not used by exchange-procedure
                for (int i = 0; i < nrPolys; i++)
                {
                    child1 = curChild[0].Gens[i];
                    child2 = curChild[1].Gens[i];
                    parent1 = tmpChild[0].Gens[i];
                    parent2 = tmpChild[1].Gens[i];

                    if (!((rndIdx1 <= i) & (i <= rndIdx2)))
                    {
                        for (int j = 0; j < idxChild1.Count; j++)
                        {
                            if (idxChild1[j] == parent1.Id)
                            {
                                curChild[0].ReplaceGen(child1, parent1);
                                idxChild1.Remove(parent1.Id);
                                break;
                            }
                        }

                        for (int j = 0; j < idxChild2.Count; j++)
                        {
                            if (idxChild2[j] == parent2.Id)
                            {
                                curChild[1].ReplaceGen(child2, parent2);
                                idxChild2.Remove(parent2.Id);
                                break;
                            }
                        }
                    }
                }

                //CheckDoubleEntries(curChild[0]);

                // fill remaining positions
                for (int i = 0; i < nrPolys; i++)
                {
                    child1 = curChild[0].Gens[i];
                    child2 = curChild[1].Gens[i];

                    if (!((rndIdx1 <= i) & (i <= rndIdx2)))
                    {
                        if (child1.Id == -1) // unbesetzte Position
                        {
                            for (int j = 0; j < nrPolys; j++) // check all parents
                            {
                                parent1 = tmpChild[0].Gens[j];
                                if (idxChild1[0] == parent1.Id) // find unused parents
                                {
                                    curChild[0].ReplaceGen(child1, parent1);
                                    idxChild1.Remove(parent1.Id);
                                    break;
                                }
                            }
                        }

                        if (child2.Id == -1)
                        {
                            for (int j = 0; j < nrPolys; j++) // check all parents
                            {
                                parent2 = tmpChild[1].Gens[j];
                                if (idxChild2[0] == parent2.Id) // find unused parents
                                {
                                    curChild[1].ReplaceGen(child2, parent2);
                                    idxChild2.Remove(parent2.Id);
                                    break;
                                }
                            }
                        }
                    }
                }

                for (int i = 0; i < 2; i++)
                    tmpChild[i] = null;
                idxChild1.Clear();
                idxChild2.Clear();
                fehlerTest.Clear();
                idxChild1 = null;
                idxChild2 = null;
                fehlerTest = null;

            }
        }
        /// <summary>
        /// Compare two chromosomes.
        /// </summary>
        /// 
        /// <param name="o">Binary chromosome to compare to.</param>
        /// 
        /// <returns>Returns comparison result, which equals to 0 if fitness values
        /// of both chromosomes are equal, 1 if fitness value of this chromosome
        /// is less than fitness value of the specified chromosome, -1 otherwise.</returns>
        /// 
        public int CompareTo(object o)
        {
            double f = ((ChromosomeLayoutSingle)o).fitness;

            return (fitness == f) ? 0 : (fitness < f) ? 1 : -1;
        }
    }
}

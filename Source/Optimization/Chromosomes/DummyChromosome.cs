﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = DummyChromosome.cs 
//  Copyright (C) 2014/10/18  1:04 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CPlan.Optimization.Chromosomes
{
    [Serializable]
    class DummyChromosome : IMultiChromosome
    {
        List<double> fitnessValues;

        public int ChromoID { get; set; }

        public DummyChromosome(List<double> values)
        {
            fitnessValues = values;
        }

        public void AssignResult(object result)
        {
            throw new NotImplementedException();
        }

        public int Indentity
        {
            get { throw new NotImplementedException(); }
        }

        public int Generation
        {
            get; set;
        }

        public List<double> FitnessValues
        {
            get { return fitnessValues; }
        }

        public IMultiChromosome CreateNew()
        {
            throw new NotImplementedException();
        }

        //Clone and Mutate method should never be called as the DummyChromosome should be so bad, that it is never selected into the archive
        //However for some reason SPAM selector seems to pick the DummyChromosome anyway so we just dont do anything.
        public IMultiChromosome Clone()
        {
            return this;            
        }

        public void Mutate()
        {
        }

        public virtual void Adapt() { }

        public void Crossover(IMultiChromosome pair)
        {
            throw new NotImplementedException();
        }

// *ac*        public Task<IMultiChromosome> Evaluate(IMultiFitnessFunction fitnessFunction)
        public Task Evaluate(IMultiFitnessFunction fitnessFunction)
        {
            throw new NotImplementedException();
        }

        public bool Equals(IMultiChromosome x, IMultiChromosome y)
        {
            throw new NotImplementedException();
        }

        public int GetHashCode(IMultiChromosome obj)
        {
            throw new NotImplementedException();
        }
    }
}

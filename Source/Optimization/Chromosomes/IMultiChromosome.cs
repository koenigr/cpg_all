﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = IMultiChromosome.cs 
//  Copyright (C) 2014/10/18  1:04 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System.Collections.Generic;
using System.Threading.Tasks;

namespace CPlan.Optimization
{

    public interface IMultiChromosome
    {
        /// <summary>
        /// Chromosome's fintess values.
        /// </summary>
        /// 
        /// <remarks><para>The fitness values represents chromosome's usefulness - the smaller the
        /// value, the more useful it.</para></remarks>
        /// 
        List<double> FitnessValues { get;}

        /// <summary>
        /// The permanent ID of a chromosome.
        /// </summary>
        int ChromoID { get; set; }

        //bool FinisehdFitnessCalculation { get; set; }

        void AssignResult(object result);

        /// <summary>
        /// The generation or iteration in which the chromosome was created.
        /// </summary>
        int Generation { get; set; }

        //double FitnessSummed { get; }

        /// <summary>
        /// Create new random chromosome with same parameters (factory method).
        /// </summary>
        /// 
        /// <remarks><para>The method creates new chromosome of the same type, but randomly
        /// initialized. The method is useful as factory method for those classes, which work
        /// with chromosome's interface, but not with particular chromosome class.</para></remarks>
        /// 
        IMultiChromosome CreateNew();

        /// <summary>
        /// Clone the chromosome.
        /// </summary>
        /// 
        /// <remarks><para>The method clones the chromosome returning the exact copy of it.</para>
        /// </remarks>
        /// 
        IMultiChromosome Clone();

        /// <summary>
        /// Mutation operator.
        /// </summary>
        /// 
        /// <remarks><para>The method performs chromosome's mutation, changing its part randomly.</para></remarks>
        /// 
        void Mutate();

        /// <summary>
        /// Adaption operator.
        /// </summary>
        /// 
        /// <remarks><para>The method performs chromosome's adaptation.</para></remarks>
        /// 
        void Adapt();

        /// <summary>
        /// Crossover operator.
        /// </summary>
        /// 
        /// <param name="pair">Pair chromosome to crossover with.</param>
        /// 
        /// <remarks><para>The method performs crossover between two chromosomes – interchanging some parts of chromosomes.</para></remarks>
        /// 
        void Crossover(IMultiChromosome pair);

        /// <summary>
        /// Evaluate chromosome with specified fitness function.
        /// </summary>
        /// 
        /// <param name="function">Fitness function to use for evaluation of the chromosome.</param>
        /// 
        /// <remarks><para>Calculates chromosome's fitness values using the specifed fitness function.</para></remarks>
        /// 
        Task Evaluate(IMultiFitnessFunction fitnessFunction);
// *ac*        Task<IMultiChromosome> Evaluate(IMultiFitnessFunction fitnessFunction);
    }
}

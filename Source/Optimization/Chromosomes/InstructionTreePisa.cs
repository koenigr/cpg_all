﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = InstructionTreePisa.cs 
//  Copyright (C) 2014/10/18  7:52 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using AForge;
using CPlan.Evaluation;
using CPlan.Geometry;
using CPlan.Geometry.ReversedSTree;
using CPlan.Optimization;
using QuickGraph;
using Tektosyne.Geometry;
using OpenTK;

namespace CPlan.Optimization
{
    [Serializable]
    public class InstructionTreePisa : MultiPisaChromosomeBase
    {
        # region Fields

        /// <summary>
        /// Random generator used for chromosoms' generation.
        /// </summary>
        protected static ThreadSafeRandom rand = new ThreadSafeRandom();
        
        //Random rnd = new Random();
        private int _angleRange = 10;//10;
        private int _lengthRange = 30;
        private int _minLength = 11;
        private int _maxChilds = 4;
        private Poly2D _outerBorder;
        private OpenTK.Vector3d[,] _topoPoints; // points that define the topography
        private List<Func<Vector2d, Vector2d, Vector2d>> _cusomFunctions;

        # endregion

        # region Properties

        public Vector2d Origin
        {
            get;
            set;
        }

        public ShortestPath ShortestPathes
        {
            get;
            set;
        }

        public override void AssignResult(object result)
        {
            if (result.GetType() == typeof(ShortestPath))
            {
                ShortestPathes = (ShortestPath)result;
            }
        }

        public UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> InitalNetwork
        {
            get;
            set;
        }

        public UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> Network//Tektosyne.Geometry.Subdivision Network  
        {
            get;
            set;
        }

        public Poly2D Border
        {
            get;
            set;
        }

        public Poly2D OuterBorder
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the number of nodes of a InstructionTree object.
        /// </summary>
        /// <value>(int) number of rooms.</value>
        public int TreeDepth
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the list of nodes of a InstructionTree object.
        /// </summary>
        /// <value>(List Node) leaves.</value>
        public List<InstructionNodeAbsolute> Nodes
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the highest index from all InstructionNodeAbsolutes
        /// </summary>
        public Int64 HighestIdx
        {
            get
            {
                Int64 hIdx = 0;
                foreach (InstructionNodeAbsolute node in Nodes)
                {
                    if (node.Index > hIdx) hIdx = node.Index;
                }
                return hIdx;
            }
            //private set;
        }

        public int MaxSize { get; private set; }

        

        # endregion
        
        # region Constructors
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>
        /// Empty Constructor.
        /// </summary>
        public InstructionTreePisa()
        {
            Nodes = new List<InstructionNodeAbsolute>();
        }

        /// <summary>
        /// Initializes a new instance of the InstructionTree type using the specified number of rooms.
        /// </summary>
        /// <param name="_rooms">(int) number of rooms.</param>

        //////yufan's data structure for parcels is imposed here//////

        /// ///////////////////////////////////////////////////////////
        
        /// <param name="treeDepth"></param>
        /// <param name="iniNet"></param>
        /// <param name="border"></param>
        /// <param name="outerBorder"></param>
        /// <param name="topoPoints"></param>
        /// <param name="customFunctions"></param>
        /// <param name="identity"></param>
        public InstructionTreePisa(int treeDepth, UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> iniNet, 
            Poly2D border, Poly2D outerBorder = null, Vector3d[,] topoPoints = null,
            List<Func<Vector2d, OpenTK.Vector2d, OpenTK.Vector2d>> customFunctions = null,
            int identity = -1)// : base(treeDepth)
        {
            Border = border;
            OuterBorder = outerBorder;
            TreeDepth = treeDepth;
            InitalNetwork = iniNet;
            Initialize();
            m_ID = (identity == -1) ? m_LastID++ : identity;
            ChromoID = m_ID;
            _topoPoints = topoPoints;
            _cusomFunctions = customFunctions;
        }

        /// <summary>
        /// Copy Constructor.
        /// </summary>
        public InstructionTreePisa(InstructionTreePisa source)//  : base(source)
        {
            Nodes = new List<InstructionNodeAbsolute>();
            foreach (InstructionNodeAbsolute oldNode in source.Nodes)
            {
                InstructionNodeAbsolute newNode = (InstructionNodeAbsolute) oldNode.Clone();
                Nodes.Add(newNode);
            }
            Border = new Poly2D(source.Border);
            OuterBorder = new Poly2D(source.OuterBorder);
            Origin = source.Origin;
            MaxSize = source.MaxSize;
            ChromoID = source.ChromoID;
            ShortestPathes = source.ShortestPathes;
            Network = (UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>)source.Network;   //.Clone(); // (Tektosyne.Geometry.Subdivision)source.Network;//.Clone();
            InitalNetwork = (UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>)source.InitalNetwork;//.Clone();
            TreeDepth = source.TreeDepth;
            _angleRange = source._angleRange;
            _lengthRange = source._lengthRange;
            _minLength = source._minLength;
            _maxChilds = source._maxChilds;
            _topoPoints = source._topoPoints;
            _cusomFunctions = source._cusomFunctions;
            m_ID = m_LastID++;
            //if (this.m_fitness_values.Count > 0)
            {
                m_fitness_values = new List<double>(source.FitnessValues.Count);
                foreach (double value in source.FitnessValues)
                {
                    m_fitness_values.Add(value);
                }
            }
        }

        # endregion 

        # region Initialization

        /// <summary>
        /// Initializes the Tree.
        /// </summary>
        public void Initialize()
        {
            Nodes = new List<InstructionNodeAbsolute>();
            //MaxSize = Convert.ToInt32((TreeDepth*1.4) * (lengthRange + minLength));
            //Border = new Rect2D(-1000.0, -1000.0, 2000.0, 2000.0); // border for the network
            //Border = new Rect2D(0.0, 0.0, MaxSize, MaxSize); // border for the network

            Origin = new Vector2d(0, 0); // global origin of coordinates for the network

            // --- add at least two seed nodes to create at least one seed edge
            InstructionNodeAbsolute seedNode;
            List<InstructionNodeAbsolute> parentNodes = new List<InstructionNodeAbsolute>();
            int idx = 0;
            foreach (Vector2d node in InitalNetwork.Vertices)
            {
                seedNode = new InstructionNodeAbsolute(idx);
                idx++;
                Vector2d pt = new Vector2d(node.X, node.Y);
                seedNode.Position = pt;
                seedNode.Angle = 0;//angle;
                seedNode.Length = 0;//length;
                seedNode.MaxConnectivity = ControlParameters.MaxChilds;
                seedNode.ParentIndex = -1;
                seedNode.LeftParcels = new ExtendedBranch(idx, ControlParameters.ParcelDepth,ControlParameters.ParcelWidth);
                Nodes.Add(seedNode);
                // -- start with three childs ---
                for (int i = 0; i < 3; i++) parentNodes.Add(seedNode);
                
            }
            CreateTree(parentNodes, TreeDepth);
        }

        private bool TestSeedNode(Vector2d node)
        {
            bool valid1strNode = true;
            if (node.X < Border.Left) valid1strNode = false;
            else if (node.Y < (Border.Bottom)) valid1strNode = false;
            else if (node.X > Border.Right) valid1strNode = false;
            else if (node.Y > Border.Top) valid1strNode = false;
            return valid1strNode;
        }

        /// <summary>
        /// Generate random chromosome value.
        /// </summary>
        /// 
        /// <remarks><para>Regenerates chromosome's value using random number generator.</para>
        /// </remarks>
        ///
        public override void Generate()
        {
            Initialize();
        }

        # endregion

        # region Methods
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Create the instruction tree with a iterative function.  
        /// </summary>
        /// <param name="parentsIdx"> List of parent nodes to start from. </param>
        /// <param name="repetitions"> Number of iterations. </param>
        ////public void CreateTreeI(List<int> parentsIdx, int repetitions)
        ////{
        ////    List<int> newParentsIdx = new List<int>();
        ////    while (repetitions > 0)
        ////    {
        ////        foreach (int parIdx in parentsIdx)
        ////        {
        ////            //InstructionNodeAbsolute curNode = new InstructionNodeAbsolute(Nodes.Count, 5, 20);
        ////            InstructionNodeAbsolute curNode = new InstructionNodeAbsolute(Nodes.Count, rnd.Next(angleRange) - angleRange/2, rnd.Next(lengthRange) + minLength);
        ////            InstructionNodeAbsolute parentNode = Nodes.Find(InstructionNodeAbsolute => InstructionNodeAbsolute.Index == parIdx);
        ////            parentNode.Children.Add(curNode);
        ////            curNode.ParentIndex = parIdx;
        ////            curNode.MaxConnectivity = rnd.Next(maxChilds-1) + 2;
        ////            Nodes.Add(curNode);
        ////            int nrArms = rnd.Next(maxChilds - 1) + 1;
        ////            for (int i = 0; i < nrArms; i++)
        ////            {
        ////                newParentsIdx.Add(curNode.Index);
        ////            }
        ////        }
        ////        parentsIdx = new List<int>();
        ////        parentsIdx.AddRange(newParentsIdx);
        ////        newParentsIdx = new List<int>();
        ////        repetitions--;
        ////    }  
        ////}

        public void CreateTree(List<InstructionNodeAbsolute> parents, int repetitions)
        {
            _angleRange = ControlParameters.AngleRange;
            _lengthRange = ControlParameters.LengthRange;
            _minLength = ControlParameters.MinLength;
            _maxChilds = ControlParameters.MaxChilds;

            
            int levelCounter = 1;

            List<InstructionNodeAbsolute> newParents = new List<InstructionNodeAbsolute>();
            while (repetitions > 0)
            {
                foreach (InstructionNodeAbsolute parNode in parents)
                {
                    //length range should be affected by the parcels
                    ExtendedBranch parcelBranch =
                        new ExtendedBranch(Nodes.Count, ControlParameters.ParcelDepth, ControlParameters.ParcelWidth);

                    double branchLength = parcelBranch.NumParcels*ControlParameters.ParcelWidth;


                    //if (parNode.Length > 0)
                    //{
                    //    branchLength = (branchLength <= this.DepthParcel*10) ? branchLength : this.DepthParcel*10;
                    //}

                     // InstructionNodeAbsolute curNode  = new InstructionNodeAbsolute(Nodes.Count, rand.Next(_angleRange) - _angleRange / 2, (float) branchLength);
                    //this is random
                                                InstructionNodeAbsolute curNode = new InstructionNodeAbsolute(Nodes.Count, rand.Next(_angleRange) - _angleRange / 2, rand.Next(_lengthRange) + _minLength);

                    //test
                    // InstructionNodeAbsolute curNode = new InstructionNodeAbsolute(Nodes.Count, parNode, rand.Next(_angleRange) - _angleRange / 2, (float)branchLength);
                    //InstructionNodeAbsolute parentNode = Nodes.Find(InstructionNodeAbsolute => InstructionNodeAbsolute.Index == parNode.ParentIdx);
                    curNode.LeftParcels = parcelBranch;
                    curNode.ParentIndex = parNode.Index;
                    curNode.MaxConnectivity = rand.Next(_maxChilds - 1) + 2;                    

                    Nodes.Add(curNode);

                    int nrArms = rand.Next(_maxChilds - 1) + 1;
                    newParents.Add(curNode);
                    for (int i = 0; i < nrArms-1; i++)
                    {
                        // --- add new parents based on a probability function
                        // --- this is to restrict the exponential growth of the tree!
                        if (levelCounter > 3)
                        {
                            double rndValue = rand.NextDouble();
                            //double slope = Math.Pow(levelCounter, nrArms);
                            double probability = 1.0/(levelCounter / Convert.ToDouble(nrArms+1));
                            if (probability >= rndValue)
                            {
                                newParents.Add(curNode);
                            }
                        }
                        else
                        {
                            newParents.Add(curNode);
                        }
                    }
                }
                parents = new List<InstructionNodeAbsolute>();
                parents.AddRange(newParents);
                newParents = new List<InstructionNodeAbsolute>();
                levelCounter++;
                repetitions--;
            }
        }

        /// <summary>
        /// Create the instruction tree with a recursive function.  
        /// </summary>
        /// <param name="parent"> The parent node to which the current node is connected to. </param>
        /// <param name="level"> The recursion level. </param>
        /// <remarks> The order of nodes produced by this recursive function isn't useful for the street network generating function.
        /// It's better to use the iterative CreateTreeI function.</remarks>
        //public void CreateTreeR(int parentIdx, int level)
        //{
        //    if (level < TreeDepth)
        //    {
        //        for (int i = 0; i < 3; i++)
        //        {
        //            InstructionNodeAbsolute curNode = new InstructionNodeAbsolute(Nodes.Count, 5, 20);
        //            curNode.ParentIndex = parentIdx;
        //            //if (Nodes.Count < NrNodes * 4)
        //            {
        //                Nodes.Add(curNode);
        //                int idx = curNode.Index;
        //                //if (Nodes.Count < NrNodes)
        //                CreateTreeR(idx, level + 1);
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// Collect the nodes of a branch.
        /// </summary>
        /// <param name="branchNodes">List of branch nodes. Initialise with an empty one.</param>
        /// <param name="parIdx">Index of node, from where the branch starts.</param>
        /// <returns>List of branch nodes.</returns>
        public List<InstructionNodeAbsolute> FindBranch(List<InstructionNodeAbsolute> branchNodes, Int64 parIdx)
        {
            List<InstructionNodeAbsolute> addNodes = Nodes.FindAll(InstructionNodeAbsolute => InstructionNodeAbsolute.ParentIndex == parIdx);
            if (addNodes.Count > 0)
            {
                branchNodes.AddRange(addNodes);
                foreach (InstructionNodeAbsolute node in addNodes)
                {
                    FindBranch(branchNodes, node.Index);
                }
            }
            return branchNodes;
        }

        /// <summary>
        /// Collect the nodes of a branch.
        /// </summary>
        /// <param name="branchNodes">List of branch nodes. Initialise with an empty one.</param>
        /// <param name="parIdx">Indes of node, from where the branch starts.</param>
        /// <returns>List of branch nodes.</returns>
        //public List<InstructionNodeAbsolute> FindBranch(List<InstructionNodeAbsolute> branchNodes, InstructionNodeAbsolute parentNode)
        //{
        //    List<InstructionNodeAbsolute> addNodes = parentNode.Children;
        //    if (addNodes.Count > 0)
        //    {
        //        branchNodes.AddRange(addNodes);
        //        foreach (InstructionNodeAbsolute node in addNodes)
        //        {
        //            FindBranch(branchNodes, node);
        //        }
        //    }
        //    return branchNodes;
        //}

        /// <summary>
        /// Create a street network from the instruction tree.
        /// </summary>
        public UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> CreateStreetNetwork() // Tektosyne.Geometry.Subdivision CreateStreetNetwork()//
        {
            StreetPattern streetNetwork = new StreetPattern(Border, OuterBorder, _topoPoints, _cusomFunctions);
            streetNetwork.GrowStreetPatternNew(this, InitalNetwork);
            //streetNetwork.CleanUp();
            return streetNetwork.QGraph;
        }

        /// <summary>
        /// Create new random chromosome (factory method)
        /// </summary>
        public override IMultiChromosome CreateNew()
        {
            //this.m_ID = MultiPisaChromosomeBase.m_LastID++;
            return new InstructionTreePisa(TreeDepth, InitalNetwork, Border);          
        }

        /// <summary>
        /// Clone the chromosome.
        /// </summary>
        public override IMultiChromosome Clone()
        {
            return new InstructionTreePisa(this);
        }

        /// <summary>
        /// Mutation operator
        /// </summary>
        public override void Mutate()
        {
            List<InstructionNodeAbsolute> mutationNodes = Nodes.FindAll(InstructionNodeAbsolute => InstructionNodeAbsolute.ParentIndex <= TreeDepth);
            int mutateRate = Convert.ToInt16(Math.Ceiling(mutationNodes.Count * 0.05));
            double rndVal = rand.NextDouble();
            for (int i = 0; i < mutateRate; i++) // number of mutations per tree
            {
                InstructionNodeAbsolute mutationNode = mutationNodes[rand.Next(mutationNodes.Count - 2) + 2]; // Wahrscheinlichkeit nach Level der Nodes einstellen
                if (rndVal < 0.25)
                {
                    mutationNode.Angle = rand.Next(_angleRange) - _angleRange / 2;
                }
                else if (rndVal < 0.5)
                {
                    mutationNode.Length = rand.Next(_lengthRange) + _minLength;
                }
                else
                {
                    mutationNode.MaxConnectivity = rand.Next(_maxChilds - 1) + 2;
                }
            }
            
        }

        /// <summary>
        /// Crossover operator
        /// </summary>
        public override void Crossover(IMultiChromosome pair)
        {
            MyCrossover(pair);
        }

        public void MyCrossover(IMultiChromosome pair)
        {
            InstructionTreePisa p = (InstructionTreePisa)pair;
            // check for correct pair
            if (p != null)
            {
                // --- choose random nodes
                List<InstructionNodeAbsolute> thisSelNodes = Nodes.FindAll(InstructionNodeAbsolute => InstructionNodeAbsolute.ParentIndex < TreeDepth);
                if (thisSelNodes.Count < 4) return;
                int r = rand.Next(thisSelNodes.Count - 3) + 3;
                InstructionNodeAbsolute childThis = (InstructionNodeAbsolute)thisSelNodes[r];
                Int64 parentThisIdx = childThis.ParentIndex;
                List<InstructionNodeAbsolute> branchThis = new List<InstructionNodeAbsolute>();
                branchThis = FindBranch(branchThis, childThis.Index);
                branchThis.Add(childThis);

                List<InstructionNodeAbsolute> otherSelNodes = p.Nodes.FindAll(InstructionNodeAbsolute => InstructionNodeAbsolute.ParentIndex < p.TreeDepth);
                if (otherSelNodes.Count < 4) return;
                int m = rand.Next(otherSelNodes.Count - 3) + 3;
                InstructionNodeAbsolute childOther = (InstructionNodeAbsolute)otherSelNodes[m];
                Int64 parentOtherIdx = childOther.ParentIndex;
                List<InstructionNodeAbsolute> branchOther = new List<InstructionNodeAbsolute>();
                branchOther = p.FindBranch(branchOther, childOther.Index);
                branchOther.Add(childOther);

                // --- delete nodes from onw InstructionTree 
                //parentThis.Children.Remove(childThis);
                foreach (InstructionNodeAbsolute tNode in branchThis)
                {
                    if (Nodes.Contains(tNode)) Nodes.Remove(tNode);
                }

                //parentOther.Children.Remove(childOther);
                foreach (InstructionNodeAbsolute oNode in branchOther)
                {
                    if (p.Nodes.Contains(oNode)) p.Nodes.Remove(oNode);
                }

                // --- copy nodes to the other InstructionTree
                // -- change indices of all branch nodes --
                Int64 highestThisIdx = HighestIdx;
                foreach (InstructionNodeAbsolute tNode in branchOther)
                {
                    Int64 newIdx = tNode.Index + highestThisIdx;
                    tNode.Index = newIdx;
                    tNode.ParentIndex = tNode.ParentIndex + highestThisIdx;
                }

                Nodes.AddRange(branchOther);
                childOther.ParentIndex = parentThisIdx;

                //parentOther.Children.Add(childThis);
                Int64 highestOtherIdx = p.HighestIdx;
                foreach (InstructionNodeAbsolute tNode in branchThis)
                {
                    Int64 newIdx = tNode.Index + highestOtherIdx;
                    tNode.Index = newIdx;
                    tNode.ParentIndex = tNode.ParentIndex + highestOtherIdx;
                }
                p.Nodes.AddRange(branchThis);
                childThis.ParentIndex = parentOtherIdx;
            }
        }

        # endregion

    }
}

﻿using System;
using System.Collections.Generic;
using AForge;
using CPlan.Evaluation;
using CPlan.Geometry;
using CPlan.Optimization;
using QuickGraph;
using Tektosyne.Geometry;
using OpenTK;
using System.Collections.ObjectModel;
using System.Collections;

namespace CPlan.Optimization
{
    [Serializable]
    public class InstructionTreePisaAbs : MultiPisaChromosomeBase, IEnumerable<InstructionNodeAbsolute>
    {
        /// <summary>
        /// Random generator used for chromosoms' generation.
        /// </summary>
        protected static ThreadSafeRandom rand = new ThreadSafeRandom();

        public UndirectedEdge<Vector2d> InitalEdge { get; set; }

        private List<InstructionNodeAbsolute> nodes = new List<InstructionNodeAbsolute>();

        public ShortestPath ShortestPathes { get; private set; }

        public override void AssignResult(object result)
        {
            if (result.GetType() == typeof(ShortestPath))
            {
                ShortestPathes = (ShortestPath)result;
            }
        }

        public Poly2D Border { get; set; }

        /// <summary>
        /// Gets or sets the number of nodes of a InstructionTree object.
        /// </summary>
        /// <value>(int) number of rooms.</value>
        public int TreeDepth { get; set; }

        /// <summary>
        /// list of nodes with absolute angles. 180 degrees == straight forward.
        /// </summary>
        /// <value>(List Node) leaves.</value>
        public IEnumerable<InstructionNodeAbsolute> Nodes => new ReadOnlyCollection<InstructionNodeAbsolute>(nodes);

        public int MaxSize { get; private set; }

        
        /// <summary>
        /// Create empty tree with init edge 0,0 to 10, 10
        /// </summary>
        public InstructionTreePisaAbs()
        {
            nodes = new List<InstructionNodeAbsolute>();
            InitalEdge = new UndirectedEdge<Vector2d>(new Vector2d(0, 0), new Vector2d(10, 10));
        }

        /// <summary>
        /// Initializes a new instance of the InstructionTree type using the specified number of rooms.
        /// </summary>
        public InstructionTreePisaAbs(int treeDepth, UndirectedEdge<Vector2d> initEdge, Poly2D border)
        {
            Border = border;
            TreeDepth = treeDepth;
            InitalEdge = initEdge;
        }

        public void AddNode(InstructionNodeAbsolute node)
        {
            nodes.Add(node);
        }

        public void AddNodes(IEnumerable<InstructionNodeAbsolute> nodeGroup)
        {
            nodes.AddRange(nodeGroup);
        }


        private bool NodeInBorder(Vector2d node)
        {
            return (node.X > Border.Left) && (node.X < Border.Right) && (node.Y > Border.Top) && (node.Y < Border.Bottom);
        }

        /// <summary>
        /// Generate random chromosome value.
        /// </summary>
        /// 
        /// <remarks><para>Regenerates chromosome's value using random number generator.</para>
        /// </remarks>
        public override void Generate()
        {

            List<InstructionNodeAbsolute> parentNodes = new List<InstructionNodeAbsolute>();
            int idx = 0;
            InstructionNodeAbsolute seedNode1 = new InstructionNodeAbsolute(idx++);
            seedNode1.Angle = 180;
            seedNode1.Length = (float)InitalEdge.Target.Distance(InitalEdge.Source);
            seedNode1.Parent = seedNode1;
            AddNode(seedNode1);
            parentNodes.Add(seedNode1);

            InstructionNodeAbsolute seedNode2 = new InstructionNodeAbsolute(idx++);
            seedNode2.Angle = 0;
            seedNode2.Length = (float)InitalEdge.Target.Distance(InitalEdge.Source);
            seedNode2.Parent = seedNode1;
            AddNode(seedNode2);
            parentNodes.Add(seedNode2);

            CreateTree(parentNodes, TreeDepth);
        }

        public void CreateTree(List<InstructionNodeAbsolute> parents, int repetitions)
        {
            var angleRange = ControlParameters.AngleRange;
            var lengthRange = ControlParameters.LengthRange;
            var minLength = ControlParameters.MinLength;
            var maxChilds = ControlParameters.MaxChilds;

            List<InstructionNodeAbsolute> newParents = new List<InstructionNodeAbsolute>();
            while (repetitions > 0)
            {
                foreach (InstructionNodeAbsolute parNode in parents)
                {
                    InstructionNodeAbsolute curNode = new InstructionNodeAbsolute(nodes.Count, parNode, rand.Next(angleRange) - angleRange / 2, rand.Next(lengthRange) + minLength);
                    nodes.Add(curNode);

                    int nrArms = rand.Next(maxChilds - 1) + 1;
                    for (int i = 0; i < nrArms; i++)
                    {
                        newParents.Add(curNode);
                    }
                }
                parents = new List<InstructionNodeAbsolute>();
                parents.AddRange(newParents);
                newParents = new List<InstructionNodeAbsolute>();

                repetitions--;
            }
        }



        /// <summary>
        /// Collect the nodes of a branch.
        /// </summary>
        /// <param name="branchNodes">List of branch nodes. Initialise with an empty one.</param>
        /// <param name="parIdx">Index of node, from where the branch starts.</param>
        /// <returns>List of branch nodes.</returns>
        public List<InstructionNodeAbsolute> FindBranch(List<InstructionNodeAbsolute> branchNodes, long parIdx)
        {
            List<InstructionNodeAbsolute> addNodes = nodes.FindAll(node => node.Parent.Index == parIdx);
            if (addNodes.Count > 0)
            {
                branchNodes.AddRange(addNodes);
                foreach (InstructionNodeAbsolute node in addNodes)
                {
                    FindBranch(branchNodes, node.Index);
                }
            }
            return branchNodes;
        }

        /// <summary>
        /// Create a street network from the instruction tree.
        /// </summary>
        public UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> CreateStreetNetwork()
        {
            return new StreetPatternAbsoluteAngle().GrowStreetPattern(this); ;
        }


        /// <summary>
        /// Create new random chromosome (factory method)
        /// </summary>
        public override IMultiChromosome CreateNew()
        {
            return new InstructionTreePisaAbs(TreeDepth, this.InitalEdge, Border);          
        }

        /// <summary>
        /// Mutation operator
        /// </summary>
        public override void Mutate()
        {
            var angleRange = ControlParameters.AngleRange;
            var lengthRange = ControlParameters.LengthRange;
            var minLength = ControlParameters.MinLength;
            var maxChilds = ControlParameters.MaxChilds;

            List<InstructionNodeAbsolute> mutationNodes = nodes.FindAll(node => node.Parent.Index <= TreeDepth);
            // Number of mutations per tree
            int mutateRate = Convert.ToInt16(Math.Ceiling(mutationNodes.Count * 0.05));
            double rndVal = rand.NextDouble();
            for (int i = 0; i < mutateRate; i++) 
            {
                // Wahrscheinlichkeit nach Level der Nodes einstellen
                InstructionNodeAbsolute mutationNode = mutationNodes[rand.Next(mutationNodes.Count - 2) + 2]; 
                if (rndVal < 0.25)
                {
                    mutationNode.Angle = rand.Next(angleRange) - angleRange / 2;
                }
                else if (rndVal < 0.5)
                {
                    mutationNode.Length = rand.Next(lengthRange) + minLength;
                }
            }
            
        }

        /// <summary>
        /// Crossover operator
        /// </summary>
        public override void Crossover(IMultiChromosome pair)
        {
            InstructionTreePisaAbs p = (InstructionTreePisaAbs)pair;
            // check for correct pair
            if (p != null)
            {
                // --- choose random nodes
                List<InstructionNodeAbsolute> thisSelNodes = nodes.FindAll(node => node.Parent.Index < TreeDepth);
                if (thisSelNodes.Count < 4) return;
                int r = rand.Next(thisSelNodes.Count - 3) + 3;
                InstructionNodeAbsolute childThis = (InstructionNodeAbsolute)thisSelNodes[r];
                long parentThisIdx = childThis.Parent.Index;
                List<InstructionNodeAbsolute> branchThis = new List<InstructionNodeAbsolute>();
                branchThis = FindBranch(branchThis, childThis.Index);
                branchThis.Add(childThis);

                List<InstructionNodeAbsolute> otherSelNodes = p.nodes.FindAll(node => node.Parent.Index < p.TreeDepth);
                if (otherSelNodes.Count < 4) return;
                int m = rand.Next(otherSelNodes.Count - 3) + 3;
                InstructionNodeAbsolute childOther = (InstructionNodeAbsolute)otherSelNodes[m];
                long parentOtherIdx = childOther.Parent.Index;
                List<InstructionNodeAbsolute> branchOther = new List<InstructionNodeAbsolute>();
                branchOther = p.FindBranch(branchOther, childOther.Index);
                branchOther.Add(childOther);

                // --- delete nodes from onw InstructionTree 
                //parentThis.Children.Remove(childThis);
                foreach (InstructionNodeAbsolute tNode in branchThis)
                {
                    if (nodes.Contains(tNode)) nodes.Remove(tNode);
                }

                //parentOther.Children.Remove(childOther);
                foreach (InstructionNodeAbsolute oNode in branchOther)
                {
                    if (p.nodes.Contains(oNode)) p.nodes.Remove(oNode);
                }

                // --- copy nodes to the other InstructionTree
                // -- change indices of all branch nodes --
                long highestThisIdx = GetHighestIndex();
                foreach (InstructionNodeAbsolute tNode in branchOther)
                {
                    long newIdx = tNode.Index + highestThisIdx;
                    tNode.Index = newIdx;
                    tNode.Parent.Index = tNode.Parent.Index + highestThisIdx;
                }

                AddNodes(branchOther);
                childOther.Parent.Index = parentThisIdx;

                //parentOther.Children.Add(childThis);
                long highestOtherIdx = p.GetHighestIndex();
                foreach (InstructionNodeAbsolute tNode in branchThis)
                {
                    long newIdx = tNode.Index + highestOtherIdx;
                    tNode.Index = newIdx;
                    tNode.Parent.Index = tNode.Parent.Index + highestOtherIdx;
                }
                p.AddNodes(branchThis);
                childThis.Parent.Index = parentOtherIdx;
            }
        }

        public long GetHighestIndex()
        {
            long index = 0;
            foreach(var node in nodes) {
                if(node.Index > index)
                {
                    index = node.Index;
                }
            }
            return index;
        }

        public override IMultiChromosome Clone()
        {
            throw new NotImplementedException();
        }

        public IEnumerator<InstructionNodeAbsolute> GetEnumerator() => nodes.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => nodes.GetEnumerator();
    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = MultiPisaChromosomeBase.cs 
//  Copyright (C) 2014/10/18  1:04 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Pavol Bielik
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CPlan.Optimization
{
    [Serializable]
    public abstract class MultiPisaChromosomeBase : IMultiChromosome
    {
        #region FIELDS

        public List<double> m_fitness_values = new List<double>();
        protected int m_ID;
        protected static int m_LastID = 0;
        
        #endregion

        #region PROPERTIES
        /// <summary>
        /// A list of fitness values for each criteria.
        /// </summary>
        public List<double> FitnessValues
        {
            get { return m_fitness_values; }
        }

        /// <summary>
        /// The permanent ID of a chromosome.
        /// </summary>
        public int ChromoID { get; set; }

        public abstract void AssignResult(object result);

        //public bool FinisehdFitnessCalculation { get; set; }

        /// <summary>
        /// The generation or iteration in which the chromosome was created.
        /// </summary>
        public int Generation
        {
            get; set;
        }

        public int LastID
        {
            get{return m_LastID;}
            set { m_LastID = value; }
        }

        #endregion

        #region METHODS
        /// <summary>
        /// Evaluate chromosome with specified fitness function.
        /// </summary>
        /// <param name="function">Fitness function to use for evaluation of the chromosome.</param>
        /// <remarks><para>Calculates chromosome's fitness values using the specifed fitness function.</para></remarks>
        async public Task Evaluate(IMultiFitnessFunction function)
        {
            m_fitness_values = await function.Evaluate(this);
        }

        /* *ac*
        async public Task<IMultiChromosome> Evaluate(IMultiFitnessFunction function)
        {
            return await function.Evaluate(this).ConfigureAwait(false);
        }*/

        public override bool Equals(Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            MultiPisaChromosomeBase y = obj as MultiPisaChromosomeBase;
            if (y == null)
            {
                return false;
            }

            return FitnessValues.SequenceEqual<double>(y.FitnessValues);
        }

        public override int GetHashCode()//IMultiChromosome obj
        {
            int result = 0;
            foreach (double t in FitnessValues)
            {
                result = t.GetHashCode() + (result << 1);
            }
            return result;
        }

        public int Indentity
        {
            get { return m_ID; }
        }

        #endregion

        #region ABSTRACT
        /// <summary>
        /// Generate random chromosome value.
        /// </summary>
        /// <remarks><para>Regenerates chromosome's value using random number generator.</para>
        /// </remarks>
        public abstract void Generate();

        /// <summary>
        /// Create new random chromosome with same parameters (factory method).
        /// </summary>
        /// <remarks><para>The method creates new chromosome of the same type, but randomly
        /// initialized. The method is useful as factory method for those classes, which work
        /// with chromosome's interface, but not with particular chromosome class.</para></remarks>
        public abstract IMultiChromosome CreateNew();

        /// <summary>
        /// Clone the chromosome.
        /// </summary>
        /// <remarks><para>The method clones the chromosome returning the exact copy of it.</para>
        /// </remarks>
        public abstract IMultiChromosome Clone();

        /// <summary>
        /// Mutation operator.
        /// </summary>
        /// <remarks><para>The method performs chromosome's mutation, changing its part randomly.</para></remarks>
        public abstract void Mutate();

        /// <summary>
        /// Crossover operator.
        /// </summary>
        /// <param name="pair">Pair chromosome to crossover with.</param>
        /// <remarks><para>The method performs crossover between two chromosomes – interchanging some parts of chromosomes.</para></remarks>
        public abstract void Crossover(IMultiChromosome pair);       

        /// <summary>
        /// Adapt operator.
        /// </summary>
        /// <remarks><para>The method may be used for local optimisation of for other adaptation mechanisms.</para></remarks>
        public virtual void Adapt() { }

        #endregion

    }
}
﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = PisaTestChromosome.cs 
//  Copyright (C) 2014/10/18  1:04 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Pavol Bielik
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPlan.Optimization.Chromosomes
{
    
    /// <summary>
    /// Sample Chromosome to test PISA wrapper
    /// Chromosome contains list of number from the interval <0,2>
    /// We try to optimize maximum number of 0 at the beggining, 2 in the middle and 1 at the end
    /// </summary>
    [Serializable]
    class PisaTestChromosome : IMultiChromosome
    {        
        private static readonly int MAX_SIZE = 50;
        static Random rnd = new Random();
        List<int> bites;
        private int ID;
        private static int LastID = 0;

        public void AssignResult(object result)
        {
            throw new NotImplementedException();
        }

        public PisaTestChromosome(int identity = -1)
        {
            bites = new List<int>(MAX_SIZE);
            for (int i = 0; i < MAX_SIZE; i++)
            {
                bites.Add(rnd.Next(3));
            }

            FitnessValues = new List<double>(3);
            FitnessValues.Add(0);
            FitnessValues.Add(0);
            FitnessValues.Add(0);
            recalculateFitness();

                
            ID = (identity == -1) ? LastID++ : identity;
        }

        //default implementation of IEqualityComparer interface using FitnessValues
        public override bool Equals(Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            PisaTestChromosome y = obj as PisaTestChromosome;
            if (y == null)
            {
                return false;
            }
                
            return FitnessValues.SequenceEqual<double>(y.FitnessValues);
        }

        public override int GetHashCode()//IMultiChromosome obj
        {
            int result = 0;
            foreach (double t in FitnessValues)
            {
                result = t.GetHashCode() + (result << 1);
            }
            return result;
        }

        private void recalculateFitness()
        {
            int i = 0;
            while (i < MAX_SIZE && bites[i] == 0)
                i++;
            FitnessValues[0] = MAX_SIZE - i;

            i = MAX_SIZE - 1;
            while (i >= 0 && bites[i] == 1)
                i--;

            FitnessValues[1] = MAX_SIZE - (MAX_SIZE - 1 - i);

            i = MAX_SIZE / 2;
            while (i < MAX_SIZE && bites[i] == 2)
                i++;

            FitnessValues[2] = i - MAX_SIZE / 2;

            i = MAX_SIZE / 2 - 1;
            while (i >= 0 && bites[i] == 2)
                i--;

            FitnessValues[2] += (MAX_SIZE / 2 - 1) - i;
            FitnessValues[2] = MAX_SIZE - FitnessValues[2];
        }

        public List<double> FitnessValues { get; set; }

        /// <summary>
        /// The permanent ID of a chromosome.
        /// </summary>
        public int ChromoID { get; set; }

        public int Generation
        {
            get; set;
        }

        public IMultiChromosome CreateNew()
        {
            return new PisaTestChromosome();
        }

        public IMultiChromosome Clone()
        {
            PisaTestChromosome result = new PisaTestChromosome(ID);
            for (int i = 0; i < MAX_SIZE; i++)
                result.bites[i] = bites[i];

            result.recalculateFitness();
            return result;
        }

        public void Mutate()
        {
            int pos = rnd.Next(MAX_SIZE);
            bites[pos] = rnd.Next(3);
            recalculateFitness();

            ID = LastID++;                  
        }

        public void Crossover(IMultiChromosome pair)
        {
            throw new NotImplementedException();
        }

        public virtual void Adapt() { }

// *ac*            public Task<IMultiChromosome> Evaluate(IMultiFitnessFunction fitnessFunction)
        public async Task Evaluate(IMultiFitnessFunction fitnessFunction)
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0} = ", ID);
            for (int i = 0; i < MAX_SIZE; i++)
            {
                sb.AppendFormat("{0}", bites[i]);
            }
            sb.AppendFormat(" - {0} {1} {2}", FitnessValues[0], FitnessValues[1], FitnessValues[2]);

            return sb.ToString();
        }

        public int Indentity
        {
            get { return ID; }
        }
    }
    
}

﻿using OpenTK;
using QuickGraph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPlan.Optimization.Chromosomes
{
    using Geometry;
    using StreetNetwork = UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>;
    using Cluser = IEnumerable<Vector2d>;

    /// <summary>
    /// Class to generate trees.
    /// </summary>
    public class TreeGenerator
    {
        /// <summary>
        /// Generates the Tree based on the given graph with absolut angles (180 degree == straight forward).
        /// All Points are 0,0 because in the tree only the parent, length and angle is needed.
        /// </summary>
        /// <param name="graph">The source graph</param>
        /// <returns>the generated tree with absolut angles</returns>
        public InstructionTreePisaAbs TreeFromNetwork(StreetNetwork graph)
        {
            if (graph == null)
            {
                var tree = new InstructionTreePisaAbs();
                tree.InitalEdge = new UndirectedEdge<Vector2d>(new Vector2d(0,0), new Vector2d(0,0));
                return tree;
            }

            var lines2DSet = graph.Edges.Select(line => new Line2D(line.Source, line.Target));

            var centerLine = FindCenterLine(lines2DSet, graph.Edges.Count());
            
            return CreateTreePisa(centerLine, graph, graph.Edges, false);
        }

        /// <summary>
        /// Generate a tree from a cluster of points
        /// </summary>
        /// <param name="graph">the source graph</param>
        /// <param name="cluster">the cluster with points</param>
        /// <returns>the generated tree with absolut angles</returns>
        public InstructionTreePisaAbs TreeFromCluster(StreetNetwork graph, Cluser cluster)
        {
            if (graph == null)
            {
                var tree = new InstructionTreePisaAbs();
                tree.InitalEdge = new UndirectedEdge<Vector2d>(new Vector2d(0, 0), new Vector2d(0, 0));
                return tree;
            }

            var clusterPoints = new HashSet<Vector2d>(cluster);

            var filteredEdges = graph.Edges
                .Where(edge => clusterPoints.Contains(edge.Source) && clusterPoints.Contains(edge.Target));

            var asLine2D = filteredEdges.Select(line => new Line2D(line.Source, line.Target));

            var centerLine = FindCenterLine(asLine2D, filteredEdges.Count());

            return CreateTreePisa(centerLine, graph, filteredEdges, true);
        }

        private UndirectedEdge<Vector2d> FindCenterLine(IEnumerable<Line2D> lines, int edgeCount)
        {
            // Find the starting segment/nodes use the nearest segment to the centre
            var sum = new Vector2d(0, 0);
            foreach (var line in lines)
            {
                sum += line.Center;
            }
            Vector2d globalCenter = sum / edgeCount;

            // find nearest segment to centre
            double dist;
            var centerLine = GeoAlgorithms2D.NearestLineToPoint(lines, globalCenter, out dist);
            return new UndirectedEdge<Vector2d>(centerLine.Start, centerLine.End);
        }

        /// <summary>
        /// Create Tree from a graph with absolut angles.
        /// </summary>
        /// <param name="initEdge">The start edge</param>
        /// <param name="graph">The graph from witch the tree is generated</param>
        /// <param name="edgesToSearch">the set witch edges should be searched</param>
        /// <param name="avoidMinima">if not all edgesToSearch are visited the this method 
        /// will be recalled with an unvisited edge as initedge. The best solution (with most notes) will be selected</param>
        /// <returns></returns>
        public InstructionTreePisaAbs CreateTreePisa(UndirectedEdge<Vector2d> initEdge, StreetNetwork graph, IEnumerable<UndirectedEdge<Vector2d>> edgesToSearch, Boolean avoidMinima)
        {
            var edges = edgesToSearch.Select(edge => new Tuple<Vector2d, Vector2d>(edge.Source, edge.Target));
            var edgeSet = new HashSet<Tuple<Vector2d, Vector2d>>(edges);
            var notVisited = new HashSet<Tuple<Vector2d, Vector2d>>(edges);

            //Dictionary with nodeID that points on absolut angle and Vector of posX and poxY
            var nodes = new Dictionary<long, Tuple<InstructionNodeAbsolute, Vector2d>>(graph.VertexCount);

            var treeFromGraph = new InstructionTreePisaAbs();
            treeFromGraph.InitalEdge = initEdge;

            var openEnds = new Queue<InstructionNodeAbsolute>();

            int idx = 0;
            InstructionNodeAbsolute seedNode1 = new InstructionNodeAbsolute(idx++);
            seedNode1.Angle = 180;
            seedNode1.Length = (float)initEdge.Target.Distance(initEdge.Source);
            seedNode1.Parent = seedNode1;
            treeFromGraph.AddNode(seedNode1);
            openEnds.Enqueue(seedNode1);
            nodes[seedNode1.Index] = new Tuple<InstructionNodeAbsolute, Vector2d>(seedNode1, initEdge.Source);

            InstructionNodeAbsolute seedNode2 = new InstructionNodeAbsolute(idx++);
            seedNode2.Angle = 0;
            seedNode2.Length = (float)initEdge.Target.Distance(initEdge.Source);
            seedNode2.Parent = seedNode1;
            treeFromGraph.AddNode(seedNode2);
            openEnds.Enqueue(seedNode2);
            nodes[seedNode2.Index] = new Tuple<InstructionNodeAbsolute, Vector2d>(seedNode2, initEdge.Target);

            seedNode1.Parent = seedNode2;

            var createdLines = new HashSet<Tuple<Vector2d, Vector2d>>();
            while (openEnds.Any())
            {
                var node = openEnds.Dequeue();
                var nodePosition = nodes[node.Index].Item2;
                var parent = nodes[node.Parent.Index];
                var openEdges = graph.AdjacentEdges(nodePosition);

                foreach (var edge in openEdges)
                {
                    var toPoint = nodes[node.Index].Item2 == edge.Source ? edge.Target : edge.Source;
                    var lineTuple = new Tuple<Vector2d, Vector2d>(nodePosition, toPoint);
                    var counterLine = new Tuple<Vector2d, Vector2d>(toPoint, nodePosition);

                    //compaires using hash function 
                    if (!createdLines.Contains(lineTuple) && !createdLines.Contains(counterLine) && toPoint != parent.Item2 
                        && (edgeSet.Contains(lineTuple) || edgeSet.Contains(counterLine)))
                    {
                        InstructionNodeAbsolute currentNode = new InstructionNodeAbsolute(idx++);
                        var angle = GeoAlgorithms2D.AngleBetweenDegree(parent.Item2, toPoint, nodePosition);
                        currentNode.Angle = Convert.ToSingle((360.0 + GeoAlgorithms2D.AngleBetweenDegree(parent.Item2, toPoint, nodePosition)) % 360);
                        currentNode.Length = Convert.ToSingle(nodePosition.Distance(toPoint));
                        currentNode.Parent = node;

                        treeFromGraph.AddNode(currentNode);
                        openEnds.Enqueue(currentNode);
                        nodes[currentNode.Index] = new Tuple<InstructionNodeAbsolute, Vector2d>(currentNode, toPoint);
                        createdLines.Add(lineTuple);
                        notVisited.Remove(lineTuple);
                    }
                }
            }
            if (avoidMinima && notVisited.Count > 0)
            {
                var newInit = notVisited.Select(f => new UndirectedEdge<Vector2d>(f.Item1, f.Item2)).First();
                var secondTree = CreateTreePisa(newInit, graph, edgesToSearch, false);
                if(secondTree.Nodes.Count() > treeFromGraph.Nodes.Count())
                {
                    return secondTree;
                }
            }

            return treeFromGraph;
        }
    }
}

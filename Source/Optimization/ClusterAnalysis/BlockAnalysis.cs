﻿using GeoAPI.Geometries;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using OpenTK;
using QuickGraph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPlan.Optimization.ClusterAnalysis
{
    using Geometry;

    using StreetNetwork = UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>;

    public class BlockAnalysis
    {
        private const double Epsilon = 1e-5;

        /// <summary>Returns all dead ends in the given street network.</summary>
        /// <param name="streetNetwork"></param>
        /// <param name="clusterVertex">Only check streets that are contained in this cluster. Dead ends at any border of the cluster are included in the result.</param>
        /// <returns></returns>
        public HashSet<UndirectedEdge<Vector2d>> GetIgnoreEdges(StreetNetwork streetNetwork, HashSet<Vector2d> clusterVertex = null)
        {
            var skipEdges = new HashSet<UndirectedEdge<Vector2d>>();
            var toCheck = new Queue<Vector2d>(streetNetwork.Vertices);

            while (toCheck.Count > 0)
            {
                var checkVertex = toCheck.Dequeue();
                var adjEdges = streetNetwork.AdjacentEdges(checkVertex)
                    .Where(e => clusterVertex != null ? clusterVertex.Contains(e.Source) && clusterVertex.Contains(e.Target) && !skipEdges.Contains(e) : !skipEdges.Contains(e));
                if (adjEdges.Count() < 2)
                {
                    foreach (var edge in adjEdges)
                    {
                        skipEdges.Add(edge);
                        var oppositVertex = edge.Source == checkVertex ? edge.Target : edge.Source;
                        toCheck.Enqueue(oppositVertex);
                    }
                }
            }
            return skipEdges;
        }

        public Vector2d GetCenter(UndirectedEdge<Vector2d> v1, UndirectedEdge<Vector2d> v2)
        {
            if (v1.Source == v2.Source || v1.Source == v2.Target)
            {
                return v1.Source;
            }

            if (v1.Target == v2.Target || v1.Target == v2.Source)
            {
                return v1.Target;
            }
            return new Vector2d(1, 0);
        }

        /// <summary>
        /// Finds blocks (smallest cycles) from undirected edges.
        /// Precondition: The edges have to be part of a planar graph.
        /// </summary>
        /// <param name="edges"></param>
        /// <returns></returns>
        public List<Poly2D> GetBlocks(IEnumerable<UndirectedEdge<Vector2d>> edges)
        {
            var graphLines = new List<IGeometry>();
            var reader = new WKTReader(new GeometryFactory());

            var poly2DEdges = edges.Select(e => new Line2D(e.Source, e.Target));
            foreach (var edge in poly2DEdges)
            {
                if (edge.Start == edge.End)
                {
                    continue;
                } 
                var lineString = GeometryConverter.ToLineString(edge);
                graphLines.Add(reader.Read(lineString.ToString()));
            }

            var polylineNodes = graphLines as ICollection<IGeometry>;
            var graph = GeoAlgorithms2D.Add_lines_to_graph(polylineNodes);
            var polygons = GeoAlgorithms2D.Polygonize(graph);

            var blocks = new List<Poly2D>();
            foreach (var polygon in polygons)
            {
                var coords = polygon.Coordinates;
                var foundPoly = GeometryConverter.ToPoly2D(coords);
                if (foundPoly.Offset(-1)) { blocks.Add(foundPoly); }
            }

            return blocks;
        }
    }
}

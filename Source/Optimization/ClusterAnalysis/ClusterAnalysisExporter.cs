﻿using CPlan.Evaluation;
using CPlan.Geometry;
using GeoJSON.Net.Feature;
using GeoJSON.Net.Geometry;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenTK;
using QuickGraph;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;


namespace CPlan.Optimization.ClusterAnalysis
{
    using static ClusterAnalysisExporter.Fields;
    using Cluster = IEnumerable<Vector2d>;
    using Clusters = IEnumerable<IEnumerable<Vector2d>>;
    using StreetNetwork = UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>;

    public class ClusterAnalysisExporter
    {
        public enum Fields
        {
            Area,
            TotalLength,
            Density,

            StreetLengthMin,
            StreetLengthMax,
            StreetLengthMean,
            StreetLengthMedian,
            StreetLengthSigma,

            VertexConnectionCountMean,

            StreetAngleMin,
            StreetAngleMax,
            StreetAngleMean,
            StreetAngleSigma,

            BlockCount,
            BlockAreaMin,
            BlockAreaMax,
            BlockAreaMean,
            BlockRelativeAreaMin,
            BlockRelativeAreaMax,
            BlockRelativeAreaMean,

            IntegrationMin,
            IntegrationMax,
            IntegrationMean,

            ChoiceMin,
            ChoiceMax,
            ChoiceMean
        }

        private const int MinLineCount = 3;

        /// <summary>All fields.</summary>
        public static readonly IReadOnlyList<Fields> FieldsAll = Enum.GetValues(typeof(Fields)).Cast<Fields>().ToList().AsReadOnly();

        /// <summary>All fields, except the centrality calculations (choice and integration).</summary>
        public static readonly IReadOnlyList<Fields> FieldsWithoutCentrality = new List<Fields>()
        {
            Area, TotalLength, Density,
            StreetLengthMin, StreetLengthMax, StreetLengthMean, StreetLengthMedian, StreetLengthSigma,
            VertexConnectionCountMean,
            StreetAngleMin, StreetAngleMax, StreetAngleMean, StreetAngleSigma,
            BlockCount, BlockAreaMin, BlockAreaMax, BlockAreaMean, BlockRelativeAreaMin, BlockRelativeAreaMax, BlockRelativeAreaMean
        }.AsReadOnly();

        private readonly StreetNetwork graph;
        private readonly Vector2d[][] clusters;

        private readonly Dictionary<Fields, Lazy<double>>[] chunkProperties;

        /// <summary>Chunk properties as lazy values.</summary>
        public IReadOnlyDictionary<Fields, Lazy<double>>[] ChunkProperties { get; }

        public double this[int chunkIndex, Fields field] => chunkProperties[chunkIndex][field].Value;

        public int ClusterCount => clusters.Length;

        private readonly Lazy<HashSet<Vector2d>>[] clusterHashSets;
        private readonly Lazy<IEnumerable<UndirectedEdge<Vector2d>>>[] filteredEdges;
        private readonly Lazy<Tuple<MinMaxSum<double>, MinMaxSum<double>>[]> integrationAndChoice;
        private readonly Lazy<List<Poly2D>>[] blocks;

        public ClusterAnalysisExporter(StreetNetwork graph, Clusters clusters)
        {
            Contract.Requires(graph != null);
            Contract.Requires(clusters != null);

            this.graph = graph;
            this.clusters = clusters.Select(c => c.ToArray()).ToArray();

            //Setup chunk property dictionaries
            this.chunkProperties = this.clusters.Select(c => new Dictionary<Fields, Lazy<double>>()).ToArray();
            this.ChunkProperties = chunkProperties.Select(c => new ReadOnlyDictionary<Fields, Lazy<double>>(c)).ToArray();

            clusterHashSets = this.clusters.Select(c => new Lazy<HashSet<Vector2d>>(() => new HashSet<Vector2d>(c))).ToArray();
            filteredEdges = this.clusters.Select((c, index) => FilteredEdgesLazy(index)).ToArray();
            blocks = this.clusters.Select((c, index) => GetBlocksLazy(index)).ToArray();
            integrationAndChoice = new Lazy<Tuple<MinMaxSum<double>, MinMaxSum<double>>[]>(() => GetIntegrationAndChoice().ToArray());

            for (int i = 0; i < ClusterCount; ++i)
            {
                //Setup lazy values for the chunk properties
                SetupChunkProperties(i);
            }
        }

        private void SetupChunkProperties(int clusterIndex)
        {
            var cluster = clusters[clusterIndex];
            var properties = chunkProperties[clusterIndex];
            properties[Area] = new Lazy<double>(() => GetPolygonArea(cluster));
            properties[Density] = new Lazy<double>(() => properties[Area].Value / properties[TotalLength].Value);

            //Street length analysis
            var streetLength = StreetLengthAnalysisLazy(clusterIndex);
            properties[TotalLength] = new Lazy<double>(() => streetLength.Value.Total);
            properties[StreetLengthMin] = new Lazy<double>(() => streetLength.Value.MinMaxSum.Min);
            properties[StreetLengthMax] = new Lazy<double>(() => streetLength.Value.MinMaxSum.Max);
            properties[StreetLengthMean] = new Lazy<double>(() => streetLength.Value.MinMaxSum.Sum / streetLength.Value.MinMaxSum.Count);
            properties[StreetLengthMedian] = new Lazy<double>(() => streetLength.Value.Median);
            properties[StreetLengthSigma] = new Lazy<double>(() => streetLength.Value.Variance);

            properties[VertexConnectionCountMean] = new Lazy<double>(() =>
            {
                var degrees = cluster.Select(v => graph.AdjacentDegree(v));
                return degrees.Sum() / (double)degrees.Count();
            });

            //Street angle analysis
            var streetAngle = AngleAnalysisLazy(clusterIndex);
            properties[StreetAngleMin] = new Lazy<double>(() => streetAngle.Value.Item1.Min);
            properties[StreetAngleMax] = new Lazy<double>(() => streetAngle.Value.Item1.Max);
            properties[StreetAngleMean] = new Lazy<double>(() => streetAngle.Value.Item1.Sum / streetAngle.Value.Item1.Count);
            properties[StreetAngleSigma] = new Lazy<double>(() => streetAngle.Value.Item2);

            //Block analysis
            var block = BlockAnalysisLazy(clusterIndex);
            properties[BlockCount] = new Lazy<double>(() => block.Value.BlockArea.Count);
            properties[BlockAreaMin] = new Lazy<double>(() => block.Value.BlockArea.Min);
            properties[BlockAreaMax] = new Lazy<double>(() => block.Value.BlockArea.Max);
            properties[BlockAreaMean] = new Lazy<double>(() => block.Value.BlockArea.Sum / block.Value.BlockArea.Count);

            properties[BlockRelativeAreaMin] = new Lazy<double>(() => block.Value.BlockRelativeArea.Min);
            properties[BlockRelativeAreaMax] = new Lazy<double>(() => block.Value.BlockRelativeArea.Max);
            properties[BlockRelativeAreaMean] = new Lazy<double>(() => block.Value.BlockRelativeArea.Sum / block.Value.BlockRelativeArea.Count);

            //Centrality analysis
            properties[IntegrationMin] = new Lazy<double>(() => integrationAndChoice.Value[clusterIndex].Item1.Min);
            properties[IntegrationMax] = new Lazy<double>(() => integrationAndChoice.Value[clusterIndex].Item1.Max);
            properties[IntegrationMean] = new Lazy<double>(() => integrationAndChoice.Value[clusterIndex].Item1.Sum / integrationAndChoice.Value[clusterIndex].Item1.Count);

            properties[ChoiceMin] = new Lazy<double>(() => integrationAndChoice.Value[clusterIndex].Item2.Min);
            properties[ChoiceMax] = new Lazy<double>(() => integrationAndChoice.Value[clusterIndex].Item2.Max);
            properties[ChoiceMean] = new Lazy<double>(() => integrationAndChoice.Value[clusterIndex].Item2.Sum / integrationAndChoice.Value[clusterIndex].Item2.Count);
        }

        /// <summary>
        /// Returns a lazy value, which can calculate the filtered edges.
        /// Filtered Edges are all edges, which are not a dead end.
        /// </summary>
        private Lazy<IEnumerable<UndirectedEdge<Vector2d>>> FilteredEdgesLazy(int clusterIndex)
        {
            return new Lazy<IEnumerable<UndirectedEdge<Vector2d>>>(() =>
            {
                var hashSet = clusterHashSets[clusterIndex].Value;
                var skipEdges = new BlockAnalysis().GetIgnoreEdges(graph, hashSet);
                return graph.Edges.Where(e => !skipEdges.Contains(e) && hashSet.Contains(e.Source) && hashSet.Contains(e.Target));
            });
        }

        /// <summary>
        /// Returns a lazy value, whcih can calculate analysis data concerning the street length.
        /// Including min, max, sum, count, median and variance of the street lengths in this cluster.
        /// </summary>
        private Lazy<StreetLengthAnalysis> StreetLengthAnalysisLazy(int clusterIndex)
        {
            return new Lazy<StreetLengthAnalysis>(() =>
            {
                var clusterStreetLength = GetClusterStreetLength(graph, clusterHashSets[clusterIndex].Value);
                var analysis = new StreetLengthAnalysis();

                //Total, Min, Max, Sum, Count
                analysis.Total = clusterStreetLength.Sum();
                analysis.MinMaxSum = clusterStreetLength.MinMaxSum();

                //Median
                var edgeLengthArray = clusterStreetLength.ToArray();
                Array.Sort(edgeLengthArray);
                if (edgeLengthArray.Length <= 0) { analysis.Median = double.NaN; }
                else if (edgeLengthArray.Length % 2 != 0) { analysis.Median = edgeLengthArray[edgeLengthArray.Length / 2]; }
                else { analysis.Median = 0.5 * (edgeLengthArray[edgeLengthArray.Length / 2] + edgeLengthArray[edgeLengthArray.Length / 2 - 1]); }

                //Variance
                analysis.Variance = GetVariance(clusterStreetLength);

                return analysis;
            });
        }

        /// <summary>
        /// Returns a lazy value, which can calculate analysis data concerning the street angles.
        /// Including min, max, sum, count and variance of the street angles.
        /// </summary>
        private Lazy<Tuple<MinMaxSum<double>, double>> AngleAnalysisLazy(int clusterIndex)
        {
            return new Lazy<Tuple<MinMaxSum<double>, double>>(() =>
            {
                var adjacentEdges = clusters[clusterIndex].Select(p => graph.AdjacentEdges(p));
                var angleList = adjacentEdges.Select(l =>
                {
                    if (l.Count() < 2) { return new List<double>(); }
                    var compareEdge = l.First();
                    var sorted = l.Skip(1).Select(e => GeoAlgorithms2D.AngleBetweenD(compareEdge.Target - compareEdge.Source, e.Target - e.Source)).OrderBy(e => e).ToList();
                    var lastToFirst = 360 - sorted.Last();
                    for (var i = sorted.Count - 1; i > 0; i--)
                    {
                        sorted[i] -= sorted[i - 1];
                    }
                    sorted.Add(lastToFirst);
                    return sorted;
                });

                //min max mean angle
                return Tuple.Create(GetMinMaxSumAngle(angleList), GetVariance(angleList.SelectMany(a => a)));
            });

        }


        private Lazy<List<Poly2D>> GetBlocksLazy(int clusterIndex)
        {
            return new Lazy<List<Poly2D>>(() =>
            {
                var clusterLines = filteredEdges[clusterIndex].Value;
                return clusterLines.Count() >= MinLineCount ? new BlockAnalysis().GetBlocks(clusterLines) : new List<Poly2D>();
            });
        }

        /// <summary>Returns a lazy value, which can calculate block analysis data.</summary>
        private Lazy<BlockAreaAnalysis> BlockAnalysisLazy(int clusterIndex) => new Lazy<BlockAreaAnalysis>(() => GetMinMaxSumBlocks(clusterIndex));

        public bool Export(string filename, int? clusterIndex = null, IEnumerable<Fields> exportFields = null)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(filename));

            //Set default export options
            if (exportFields == null) { exportFields = FieldsWithoutCentrality; }

            var array = new JArray();
            foreach (var i in Enumerable.Range(clusterIndex ?? 0, clusterIndex.HasValue ? 1 : ClusterCount))
            {
                //Evaluate all lazy values, that are needed for the export
                var properties = exportFields.ToDictionary(field => field.ToString(), field => (object)chunkProperties[i][field].Value);

                var geoLines = this.clusters[i].Select(p => new GeographicPosition(p.X, p.Y));
                var lineString = new LineString(geoLines);
                var feature = new Feature(lineString, properties);
                var geo = JObject.FromObject(feature);
                array.Add(geo);
            }

            using (StreamWriter outputFile = new StreamWriter(filename, false))
            {
                outputFile.Write(array.ToString());
            }
            return true;
        }

        public Dictionary<Fields, double> GetAnalysisData(int clusterIndex, IEnumerable<Fields> fields = null) =>
            (fields ?? FieldsWithoutCentrality).ToDictionary(field => field, field => chunkProperties[clusterIndex][field].Value);

        public Dictionary<Fields, double>[] GetAnalysisData(IEnumerable<Fields> fields = null) =>
            Enumerable.Range(0, ClusterCount).Select(i => GetAnalysisData(i, fields)).ToArray();

        public Dictionary<Fields, double> GetAnalysisData(int clusterIndex, params Fields[] fields) =>
            GetAnalysisData(clusterIndex, fields == null || fields.Length <= 0 ? null : fields);

        public Dictionary<Fields, double>[] GetAnalysisData(params Fields[] fields) =>
            GetAnalysisData(fields == null || fields.Length <= 0 ? null : fields);

        public IEnumerable<Poly2D> GetBlocks(int clusterIndex) => blocks[clusterIndex].Value.Select(b => new Poly2D(b));

        /// <summary>
        /// Calculates the polygon area based on the Convex Hull
        /// </summary>
        /// <param name="cluster"></param>
        /// <returns></returns>
        private double GetPolygonArea(Cluster cluster) =>
            GeoAlgorithms2D.PolygonArea(GeoAlgorithms2D.ConvexHullFromPoints(cluster));

        private List<double> GetClusterStreetLength(StreetNetwork graph, HashSet<Vector2d> cluster)
        {
            return graph.Edges.Where(e => cluster.Contains(e.Source) && cluster.Contains(e.Target))
                .Select(e => (e.Target - e.Source).Length).ToList();
        }

        private MinMaxSum<double> GetMinMaxSumStreetLength(List<double> clusterStreetLength) => clusterStreetLength.MinMaxSum();

        private MinMaxSum<double> GetMinMaxSumAngle(IEnumerable<IEnumerable<double>> anglesPerVertex) => anglesPerVertex.SelectMany(al => al).MinMaxSum();

        private BlockAreaAnalysis GetMinMaxSumBlocks(int clusterIndex)
        {
            var blocks = this.blocks[clusterIndex].Value;
            var circleAreas = blocks.Select(GetBoundingCircleArea);
            var blockAreas = blocks.Select(b => b.Area).ToArray();

            return new BlockAreaAnalysis()
            {
                BlockArea = blockAreas.MinMaxSum(),
                BlockRelativeArea = circleAreas
                    .Select((circle, index) => blockAreas[index] / circle)
                    .Where(a => a <= 1.0)
                    .MinMaxSum()
            };
        }

        /// <summary>Calculates the area of the bounding cirlce.</summary>
        public static double GetBoundingCircleArea(Poly2D polygon)
        {
            var edgesAsPointF = polygon.EdgesV.Select(e => new PointF((float)e.X, (float)e.Y));
            float boudingCircleRadius;
            PointF center;
            BoundingGeometry.FindMinimalBoundingCircle(edgesAsPointF.ToList(), out center, out boudingCircleRadius);
            return boudingCircleRadius * boudingCircleRadius * Math.PI;
        }

        /// <summary>
        /// Returns IntegrationArray and ChoiceArray.
        /// </summary>
        /// <param name="graph"></param>
        /// <returns></returns>
        private IEnumerable<Tuple<MinMaxSum<double>, MinMaxSum<double>>> GetIntegrationAndChoice()
        {
            var fwGraph = GraphTools.GenerateFWGpuGraph(graph);
            var fwInverseGraph = GraphTools.GenerateInverseDoubleGraph(fwGraph);

            var shortPahtesAngular = new ShortestPath(graph);
            shortPahtesAngular.Evaluate(new System.Windows.Forms.ToolStripProgressBar(), fwGraph, fwInverseGraph);
            var integration = shortPahtesAngular.GetNormCentralityStepsArray();
            var choice = shortPahtesAngular.GetNormChoiceArray();

            fwGraph.ClearGraph();
            fwGraph.Dispose();
            fwInverseGraph.Dispose();

            for (int i = 0; i < ClusterCount; ++i)
            {
                yield return GetClusterIntegrationAndChoice(integration, choice, i);
            }
        }

        private Tuple<MinMaxSum<double>, MinMaxSum<double>> GetClusterIntegrationAndChoice(float[][] integration, float[][] choice, int clusterIndex)
        {
            var clusterHashSet = clusterHashSets[clusterIndex].Value;
            var graphEdges = graph.Edges.ToArray();
            var integrationCluster = new List<double>();
            var choiceCluster = new List<double>();

            for (var i = 0; i < graphEdges.Length; i++)
            {
                if (clusterHashSet.Contains(graphEdges[i].Source) && clusterHashSet.Contains(graphEdges[i].Target))
                {
                    integrationCluster.Add(integration[0][i]);
                    choiceCluster.Add(choice[0][i]);
                }
            }
            return Tuple.Create(integrationCluster.MinMaxSum(), choiceCluster.MinMaxSum());
        }

        private double GetVariance(IEnumerable<double> elements, int stepsCount = 100)
        {
            var minMaxSum = elements.MinMaxSum();
            var stepSize = (minMaxSum.Max - minMaxSum.Min) / stepsCount;
            var edgeLength‎Distribution = new Dictionary<int, double>();
            var totalCount = 0;
            foreach (var edgeLength in elements)
            {
                var pos = (int)(edgeLength - (edgeLength % stepSize));
                if (edgeLength‎Distribution.ContainsKey(pos)) { edgeLength‎Distribution[pos]++; }
                else { edgeLength‎Distribution[pos] = 1; }
                totalCount++;
            }
            var expectedValue = 0.0;
            var keyList = edgeLength‎Distribution.Keys.ToList();
            foreach (var key in keyList)
            {
                edgeLength‎Distribution[key] = edgeLength‎Distribution[key] / totalCount;
                expectedValue += key * edgeLength‎Distribution[key];
            }
            var variance = 0.0;
            foreach (var key in keyList)
            {
                var diff = key - expectedValue;
                variance += diff * diff * edgeLength‎Distribution[key];
            }
            return Math.Sqrt(variance);
        }

        private class StreetLengthAnalysis
        {
            public double Total { get; set; }
            public MinMaxSum<double> MinMaxSum { get; set; }
            public double Median { get; set; }
            public double Variance { get; set; }
        }

        private class BlockAreaAnalysis
        {
            public MinMaxSum<double> BlockArea { get; set; }

            /// <summary>Block area divided by the area of the smallest circle around this block.</summary>
            public MinMaxSum<double> BlockRelativeArea { get; set; }
        }
    }
}

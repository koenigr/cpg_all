﻿using OpenTK;
using QuickGraph;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPlan.Optimization.ClusterAnalysis
{
    using StreetNetwork = UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>;
    using Clusters = IEnumerable<IEnumerable<Vector2d>>;
    using Accord.MachineLearning;

    public class FeatureClustering : IClusterAlgorithm
    {
        public const int DefaultClusterCount = 15;
        public const int DefaultMinClusterSize = 10;

        public Clusters FindClusters(StreetNetwork graph, int clusterCount = DefaultClusterCount, int minClusterSize = DefaultMinClusterSize)
        {
            Contract.Requires(graph != null);

            var nodes = graph.Vertices.ToArray();
            var features = CalculateFeatures(graph, nodes);
            var standardised = Accord.Statistics.Tools.Standardize(Accord.Statistics.Tools.Center(features));

            var kmean = new KMeans(clusterCount);
            var labels = kmean.Compute(standardised);

            return CreateConnectedClusters(graph, labels, nodes, minClusterSize);
        }

        /// <summary>Calculates features of each given chunk.</summary>
        private static double[][] CalculateFeatures(StreetNetwork graph, Vector2d[] nodes)
        {
            var analysis = new ClusterAnalysisExporter(graph, new[] { nodes });
            var blocks = analysis.GetBlocks(0);

            var blockLookup = blocks.SelectMany(block =>
            {
                var area = block.Area;
                var circleArea = ClusterAnalysisExporter.GetBoundingCircleArea(block);
                var relativeArea = area / circleArea;

                return block.Points.SelectMany(points => points.Select(point =>
                    new { Block = new { Area = area, RelativeArea = relativeArea }, Point = point }));
            }).ToLookup(element => element.Point, element => element.Block);

            return nodes.Select(vertex => new double[]
            {
                vertex.X,
                vertex.Y,
                blockLookup.Contains(vertex) ? blockLookup[vertex].Average(b => b.RelativeArea) : 0.0,
                //blockLookup.Contains(vertex) ? blockLookup[vertex].Average(b => b.Area) : 0.0,
                //graph.AdjacentDegree(vertex),
                //graph.AdjacentEdges(vertex).Average(edge => (edge.Source - edge.Target).Length),
            }).ToArray();
        }

        /// <summary>
        /// From the clustering defined in labels a new clustering is created, in which clusters are connected subgraphs.
        /// To do this new clusters can be created, where a cluster was not connected before.
        /// Nodes of different clusters are never added to the same cluster in this modified clustering.
        /// </summary>
        private static Clusters CreateConnectedClusters(StreetNetwork graph, int[] labels, Vector2d[] nodes, int minClusterSize)
        {
            Contract.Requires(labels.Length == nodes.Length);

            var vectorToIndex = nodes
                .Select((node, index) => new { Index = index, Node = node })
                .ToDictionary(node => node.Node, node => node.Index);
            var newLabels = new int[labels.Length];
            var currentNewLabel = 0;

            for(int i = 0; i < nodes.Length; ++i)
            {
                if(newLabels[i] == 0)
                {
                    //BFS over all connected nodes, which have the same label
                    //Ensures, that clusters are connected
                    ++currentNewLabel;
                    var label = labels[i];
                    var queue = new Queue<int>();
                    queue.Enqueue(i);

                    while(queue.Count > 0)
                    {
                        var current = queue.Dequeue();
                        if(newLabels[current] == 0)
                        {
                            newLabels[current] = currentNewLabel;
                            foreach (var neighbour in AdjacentNodes(graph, nodes[current])
                                .Select(n => vectorToIndex[n])
                                .Where(n => labels[n] == label && newLabels[n] == 0))
                            {
                                queue.Enqueue(neighbour);
                            }
                        }
                    }
                }
            }

            return newLabels
                .Select((label, index) => new { Label = label, Node = nodes[index] })
                .GroupBy(item => item.Label, item => item.Node)
                .Where(group => group.Count() >= minClusterSize);
        }

        private static IEnumerable<Vector2d> AdjacentNodes(StreetNetwork graph, Vector2d node) =>
            graph.AdjacentEdges(node).Select(edge => edge.Source != node ? edge.Source : edge.Target);

    }
}

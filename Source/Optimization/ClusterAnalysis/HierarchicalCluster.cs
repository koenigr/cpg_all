﻿using OpenTK;
using QuickGraph;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Diagnostics;
using PriorityQueue;
using static System.Math;

namespace CPlan.Optimization
{
    /// <summary>
    /// This class implements hierarchical cluster analysis algorithms.
    /// </summary>
    public class HierarchicalCluster : IClusterAlgorithm
    {
        public enum Strategy { UPGMA, FastUPGMA, WPGMA, SingleLinkage }

        public const int DefaultClusterCount = 10;

        /// <summary>
        /// Creates an empty triangular 2D array (~matrix).
        /// For any a, b where 0 &lt;= a,b &lt;= size the indexing array[Max(a,b), Min(a,b)] is a valid.
        /// </summary>
        /// <param name="size">Size of the array for "both" dimensions. (See the method description for more detail.)</param>
        /// <returns>Array with the described properties.</returns>
        private float[][] CreateTriangular2DArray(int size)
        {
            Contract.Requires(size > 0);
            var array = new float[size][];
            for(int y = 0; y < size; ++y)
            {
                array[y] = new float[y + 1];
            }
            return array;
        }

        /// <summary>All pairs shortest path algorithm.</summary>
        public float[][] FloydWarshall(UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> graph, Vector2d[] nodes, float[][] distance = null, IProgress<int> progress = null)
        {
            progress?.Report(0);
            var n = nodes.Length;
            var vectorToNode = nodes.Select((v, i) => Tuple.Create(i, v)).ToDictionary(i => i.Item2, i => i.Item1);
            if (distance == null) { distance = CreateTriangular2DArray(n); }
            //Initialise default distance (positive infinity)
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j <= i; ++j) { distance[i][j] = float.PositiveInfinity; }
            }
            //Initialise node distances
            foreach (var edge in graph.Edges)
            {
                var length = (float)(edge.Target - edge.Source).Length;
                var item1 = vectorToNode[edge.Source];
                var item2 = vectorToNode[edge.Target];
                distance[Max(item1, item2)][Min(item1, item2)] = length;
            }
            //The distance to itself is 0 for each node
            for (int i = 0; i < n; ++i) { distance[i][i] = 0f; }
            //Floyd warshall algorithm for all pair shortest path
            var progressPercent = 100.0 / n;
            for (int a = 0; a < n; ++a)
            {
                for (int b = 0; b < n; ++b)
                {
                    if (double.IsInfinity(distance[Max(a, b)][Min(a, b)])) { continue; }
                    for (int c = 0; c < n; ++c)
                    {
                        if (distance[Max(b, c)][Min(b, c)] > distance[Max(a, b)][Min(a, b)] + distance[Max(a, c)][Min(a, c)])
                        {
                            distance[Max(b, c)][Min(b, c)] = distance[Max(a, b)][Min(a, b)] + distance[Max(a, c)][Min(a, c)];
                        }
                    }
                }
                progress?.Report((int)(a * progressPercent));
            }
            return distance;
        }

        public float[][] AllPairsDijkstra(UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> graph, Vector2d[] nodes, float[][] distance = null, IProgress<int> progress = null)
        {
            if (distance == null) { distance = CreateTriangular2DArray(nodes.Length); }
            //Create simpler graph representation
            var myGraph = new List<Tuple<int, float>>[nodes.Length];
            var nodeLookup = nodes.Select((node, i) => Tuple.Create(node, i)).ToDictionary(node => node.Item1, node => node.Item2);
            for (int i = 0; i < nodes.Length; ++i)
            {
                myGraph[i] = new List<Tuple<int, float>>();
                foreach(var edge in graph.AdjacentEdges(nodes[i]))
                {
                    var node = nodeLookup[nodes[i] == edge.Source ? edge.Target : edge.Source];
                    myGraph[i].Add(Tuple.Create(node, (float)(edge.Target - edge.Source).Length));
                }
            }
            //Run dijkstra, starting from each node
            var progressPercent = 100.0 / nodes.Length;
            var progrssValue = 0;
            foreach (var result in Enumerable.Range(0, nodes.Length)
                .AsParallel().Select(node => Tuple.Create(node, FullDijkstra(myGraph, nodes, node)))
                .AsSequential())
            {
                for (var i = result.Item1; i >= 0; --i) { distance[result.Item1][i] = result.Item2[i]; }
                ++progrssValue;
                progress?.Report((int)(progrssValue * progressPercent));
            }
            return distance;
        }

        /// <summary>Dijkstra algorithm, which finds the shortest path from a target to all nodes.</summary>
        private float[] FullDijkstra(List<Tuple<int, float>>[] graph, Vector2d[] nodes, int start)
        {
            var distance = new float[nodes.Length];
            var items = new Dictionary<int, PriorityQueue<Tuple<int, float>>.Item>();
            var finished = new bool[nodes.Length];
            var queue = new PriorityQueue<Tuple<int, float>>((a, b) => a.Item2.CompareTo(b.Item2));
            queue.Enqueue(Tuple.Create(start, 0f));
            while(queue.Count > 0)
            {
                var current = queue.Dequeue();
                if (items.ContainsKey(current.Item1)) { items.Remove(current.Item1); }
                distance[current.Item1] = current.Item2;
                finished[current.Item1] = true;
                foreach(var edge in graph[current.Item1])
                {
                    if (finished[edge.Item1]) { continue; }
                    var newEntry = Tuple.Create(edge.Item1, current.Item2 + edge.Item2);
                    if (items.ContainsKey(edge.Item1))
                    {
                        //Update only, if this edge is better than the previous one
                        if (newEntry.Item2 < items[edge.Item1].Data.Item2) { items[edge.Item1] = queue.Update(items[edge.Item1], newEntry); }
                    }
                    else { items[edge.Item1] = queue.Enqueue(newEntry); }
                }
            }
            return distance;
        }

        /// <summary>Finds the cluster using the upgma reduction formula.</summary>
        public IHierarchicalClusterOutput FindClustersUpgma(UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> graph, IProgress<int> progress = null)
        {
            Contract.Requires(graph != null);

            var nodes = graph.Vertices.ToArray();
            //var distance = FloydWarshall(graph, nodes, progress: progress);
            var distance = AllPairsDijkstra(graph, nodes, progress: progress);

            progress?.Report(0);
            var progressPercent = 100.0 / nodes.Length;

            //Create hierarchical clusters
            var clusters = new HashSet<ClusterBase>(nodes.Select((n, i) => new ClusterLeave(i)));
            var chainStack = new List<ClusterBase>(clusters.Take(1));
            while(clusters.Count > 1)
            {
                var last = chainStack[chainStack.Count - 1];
                //Find nearest neighbour to last
                var nearestNeighbour = clusters
                    .Where(cluster => cluster != last)
                    .MinBy(cluster => cluster.Nodes.Average(v => last.Nodes.Average(v2 => distance[Max(v, v2)][Min(v, v2)]))); //upgma
                if (chainStack.Count > 1 && chainStack[chainStack.Count - 2] == nearestNeighbour)
                {
                    //The last two elements of the chain are reciprocal (or mutual) nearest neighbours
                    chainStack.RemoveRange(chainStack.Count - 2, 2);
                    clusters.Remove(last);
                    clusters.Remove(nearestNeighbour);
                    clusters.Add(new Cluster(last, nearestNeighbour));
                    //Update progress
                    progress?.Report((int)(progressPercent * (nodes.Length - clusters.Count + 1)));
                    if (chainStack.Count <= 0) { chainStack.Add(clusters.First()); }
                }
                else
                {
                    chainStack.Add(nearestNeighbour);
                }
            }

            progress?.Report(100);
            return new Output(clusters.First(), nodes);
        }

        public IHierarchicalClusterOutput FindClusters(UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> graph, Strategy strategy = Strategy.WPGMA, IProgress<int> progress = null)
        {
            if(strategy == Strategy.UPGMA) { return FindClustersUpgma(graph, progress); }
            if (strategy == Strategy.SingleLinkage) { return new SingleLinkageCluster().FindClusters(graph); }
            Contract.Requires(graph != null);
            var nodes = graph.Vertices.ToArray();
            Contract.Requires(nodes.Length > 1);

            //var distance = FloydWarshall(graph, nodes, progress: progress);
            var distance = AllPairsDijkstra(graph, nodes, progress: progress);

            progress?.Report(0);
            var progressPercent = 100.0 / nodes.Length;

            //Create hierarchical clusters
            var clusters = nodes.Select((n, i) => new ClusterLeave(i)).Cast<ClusterBase>().ToArray();
            var rootClusters = new HashSet<int>(Enumerable.Range(0, nodes.Length));
            var chainStack = new List<int>(rootClusters.Take(1));
            while (rootClusters.Count > 1)
            {
                var last = chainStack[chainStack.Count - 1];
                //Find nearest neighbour to last
                var nearestNeighbour = rootClusters
                    .Where(cluster => cluster != last)
                    .MinBy(cluster => distance[Max(cluster, last)][Min(cluster, last)]);
                if (chainStack.Count > 1 && chainStack[chainStack.Count - 2] == nearestNeighbour)
                {
                    //The last two elements of the chain are reciprocal (or mutual) nearest neighbours
                    chainStack.RemoveRange(chainStack.Count - 2, 2);
                    rootClusters.Remove(last);
                    rootClusters.Remove(nearestNeighbour);
                    int newIndex;
                    //Combine clusters according to strategie
                    switch (strategy)
                    {
                        case Strategy.WPGMA:
                            newIndex = CombineClusterWpgma(distance, last, nearestNeighbour);
                            break;
                        case Strategy.FastUPGMA:
                            newIndex = CombineClusterUpgma(distance, clusters, last, nearestNeighbour);
                            break;
                        default:
                            throw new InvalidOperationException("Hierarchical Clustering: Unknown Strategy");
                    }
                    //Add new combined cluster
                    clusters[newIndex] = new Cluster(clusters[last], clusters[nearestNeighbour]);
                    rootClusters.Add(newIndex);
                    //Update progress
                    progress?.Report((int)(progressPercent * (nodes.Length - rootClusters.Count + 1)));
                    if (chainStack.Count <= 0) { chainStack.Add(rootClusters.First()); }
                }
                else
                {
                    chainStack.Add(nearestNeighbour);
                }
            }

            progress?.Report(100);
            return new Output(clusters[rootClusters.First()], nodes);
        }

        /// <summary>Update distances, according to the upgma formula to combine the two old clusters into the new one.</summary>
        private static int CombineClusterUpgma(float[][] distance, ClusterBase[] clusters, int oldCluster1, int oldCluster2)
        {
            var newCluster = Min(oldCluster1, oldCluster2);
            var count1 = clusters[oldCluster1].Count();
            var count2 = clusters[oldCluster2].Count();
            for (int i = 0; i < newCluster; ++i)
            {
                var distance1 = distance[Max(oldCluster1, i)][Min(oldCluster1, i)];
                var distance2 = distance[Max(oldCluster2, i)][Min(oldCluster2, i)];
                distance[Max(newCluster, i)][Min(newCluster, i)] = (count1 * distance1 + count2 * distance2) / (count1 + count2);
            }
            return newCluster;
        }

        /// <summary>Update distances, according to the wpgma formula to combine the two old clusters into the new one.</summary>
        private static int CombineClusterWpgma(float[][] distance, int oldCluster1, int oldCluster2)
        {
            var newCluster = Min(oldCluster1, oldCluster2);
            for (int i = 0; i < newCluster; ++i)
            {
                distance[Max(newCluster, i)][Min(newCluster, i)] = (distance[Max(oldCluster1, i)][Min(oldCluster1, i)] + distance[Max(oldCluster2, i)][Min(oldCluster2, i)]) / 2;
            }
            return newCluster;
        }

        /// <summary>
        /// Result of a hierarchical clustering.
        /// The <see cref="Output.Clusters(int, bool)"/> method can be used to generate specific clusters with given cluster count.
        /// </summary>
        private class Output : IHierarchicalClusterOutput
        {
            private readonly ClusterBase root;
            private readonly Vector2d[] nodes;

            public Output(ClusterBase root, Vector2d[] nodes)
            {
                this.root = root;
                this.nodes = nodes;
            }

            private IEnumerable<IEnumerable<Vector2d>> GenerateOutput(int clusterCount)
            {
                //Return requested amount of clusters
                var queue = new Queue<ClusterBase>();
                queue.Enqueue(root);
                var count = 1;
                while (count < clusterCount && queue.Any(cluster => cluster is Cluster))
                {
                    var current = queue.Dequeue();
                    if (current is Cluster)
                    {
                        queue.Enqueue(((Cluster)current).Left);
                        queue.Enqueue(((Cluster)current).Right);
                        if (((Cluster)current).Left.Count() > 3 && ((Cluster)current).Right.Count() > 3) { ++count; }
                    }
                    else
                    {
                        queue.Enqueue(current);
                    }
                }
                return queue.Select(cluster => cluster.Select(node => nodes[node]).ToList()).ToList();
            }

            private IEnumerable<IEnumerable<Vector2d>> GenerateOutputModified(int clusterCount)
            {
                var set = new HashSet<ClusterBase>();
                set.Add(root);
                var count = 1;
                while (count < clusterCount && set.Any(cluster => cluster is Cluster))
                {
                    var current = set.Where(cluster => cluster is Cluster).MaxBy(cluster => cluster.Nodes.Count());
                    set.Remove(current);
                    set.Add(((Cluster)current).Left);
                    set.Add(((Cluster)current).Right);
                    if (((Cluster)current).Left.Count() > 3 && ((Cluster)current).Right.Count() > 3) { ++count; }
                }
                return set.Select(cluster => cluster.Select(node => nodes[node]).ToList()).ToList();
            }

            public IEnumerable<IEnumerable<Vector2d>> Clusters(int clusterCount = DefaultClusterCount, bool modifiedClusterSizes = true)
            {
                Contract.Requires(clusterCount <= nodes.Length, "Can't create more clusters than there are vertices in the given graph.");
                return modifiedClusterSizes ? GenerateOutputModified(clusterCount) : GenerateOutput(clusterCount);
            }
        }

        [DebuggerDisplay("{Nodes}")]
        private abstract class ClusterBase : IEnumerable<int>
        {
            public abstract IEnumerable<int> Nodes { get; }

            public IEnumerator<int> GetEnumerator() => Nodes.GetEnumerator();
            IEnumerator IEnumerable.GetEnumerator() => Nodes.GetEnumerator();
        }

        [DebuggerDisplay("{Nodes}")]
        private class Cluster : ClusterBase
        {
            public ClusterBase Left { get; }
            public ClusterBase Right { get; }

            public override IEnumerable<int> Nodes => Left.Nodes.Union(Right.Nodes);

            public Cluster(ClusterBase left, ClusterBase right)
            {
                this.Left = left;
                this.Right = right;
            }
        }

        [DebuggerDisplay("{Node}")]
        private class ClusterLeave : ClusterBase
        {
            public int Node { get; }

            public override IEnumerable<int> Nodes
            {
                get { yield return Node; }
            }

            public ClusterLeave(int node) { this.Node = node; }
        }
    }
}

﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPlan.Optimization
{
    public interface IHierarchicalClusterOutput
    {
        IEnumerable<IEnumerable<Vector2d>> Clusters(int clusterCount = HierarchicalCluster.DefaultClusterCount, bool modifiedClusterSizes = true);
    }
}

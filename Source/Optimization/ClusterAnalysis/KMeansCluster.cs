﻿using OpenTK;
using QuickGraph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPlan.Optimization
{
    using StreetNetwork = UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>;
    using Clusters = IEnumerable<IEnumerable<Vector2d>>;
    using System.Threading;
    using System.Diagnostics.Contracts;

    /// <summary>
    /// This class implements the k-means cluster analysis algorithm.
    /// </summary>
    public class KMeansCluster : IClusterAlgorithm
    {
        public const int DefaultTries = 10;
        public const int DefaultIterations = 10;
        public const int DefaultClustersMin = 5;
        public const int DefaultClustersMax = 15;

        private Random random = new Random();

        public class Parameters
        {
            public int Tries { get; }
            public int Iterations { get; }
            public int ClustersMin { get; }
            public int ClustersMax { get; }
            public IProgress<int> ProgressBar { get; }
            public IProgress<Clusters> ProgressVisualization { get; }
            public bool ShortestPath { get; }

            public Parameters(int tries = DefaultTries, int iterations = DefaultIterations, int clustersMin = DefaultClustersMin, 
                int clustersMax = DefaultClustersMax, IProgress<int> progressBar = null, 
                IProgress<IEnumerable<IEnumerable<Vector2d>>> progressVisualization = null, bool shortestPath = false)
            {
                Contract.Requires(tries > 0);
                Contract.Requires(iterations > 0);
                Contract.Requires(clustersMin > 0);
                Contract.Requires(clustersMax >= clustersMin);

                Tries = tries;
                Iterations = iterations;
                ClustersMin = clustersMin;
                ClustersMax = clustersMax;
                ProgressBar = progressBar;
                ProgressVisualization = progressVisualization;
                ShortestPath = shortestPath;
            }
        }

        /// <summary>
        /// Tries to find optimal clusters in the given street network.
        /// </summary>
        /// <remarks>
        /// Setting the number of clusters to be equal to the number of vertices in the graph is a bad idea:
        /// The algorithm would just create one cluster per vertex.
        /// </remarks>
        /// <param name="graph"></param>
        /// <param name="parameters containing: tries, iterations, clustersMin, clustersMax"></param>
        /// <returns>IEnumerable of Cluster of points in cluster</returns>
        public Clusters FindClusters(StreetNetwork graph, Parameters parameters = null)
        {
            if (parameters == null)
            {
                parameters = new Parameters();
            }
            double percent = 100.0 / (parameters.Tries <= 1 ? 1 : parameters.Tries - 1);
            double iterationsPercent = percent / parameters.Iterations;
            var bestCost = double.PositiveInfinity;
            Clusters bestClusters = null;
            IEnumerable<Vector2d> centers = null;
            if (parameters.ProgressVisualization == null)
            {
                var syncProgress = 0;
                parameters.ProgressBar?.Report(syncProgress + 1);
                var innerSyncProgress = 0;
                var minResult = Enumerable.Range(0, parameters.Tries)
                    .AsParallel()
                    .Select(nr =>
                    {
                        var clusters = KMeans(graph, parameters.Iterations, random.Next(parameters.ClustersMin, parameters.ClustersMax + 1));
                        Interlocked.Increment(ref syncProgress);
                        parameters.ProgressBar?.Report((int)(percent * syncProgress));
                        Interlocked.Exchange(ref innerSyncProgress, 0);
                        return clusters;
                    }).AsSequential()
                    .MinBy(costAndCluster => costAndCluster.Item1);
                bestCost = minResult.Item1;
                bestClusters = minResult.Item2;
                centers = minResult.Item3;
            }
            else
            {
                for (int i = 0; i < parameters.Tries; ++i)
                {
                    var innerProgressBar = new Progress<int>(value =>
                    {
                        parameters.ProgressBar?.Report((int)(percent * i + iterationsPercent));
                    });
                    var innerProgressVisualization = new Progress<Clusters>(cluster =>
                    {
                        parameters.ProgressVisualization?.Report(cluster);
                    });
                    var clusters = KMeans(graph, parameters.Iterations, random.Next(parameters.ClustersMin, parameters.ClustersMax + 1),
                        progressBar: innerProgressBar, progressVisualization: innerProgressVisualization);
                    if (clusters.Item1 < bestCost)
                    {
                        bestCost = clusters.Item1;
                        bestClusters = clusters.Item2;
                        centers = clusters.Item3;
                    }
                    parameters.ProgressBar?.Report((int)(percent * i));
                    parameters.ProgressVisualization?.Report(bestClusters);
                }
            }
            if (!parameters.ShortestPath)
            {
                return bestClusters.ToList();
            }
            parameters.ProgressBar?.Report(1);

            var nodes = graph.Vertices.ToArray();
            var distances = new HierarchicalCluster().AllPairsDijkstra(graph, nodes);
            if(centers == null || nodes.Length < 1)
            {
                return bestClusters.ToList();
            }
            parameters.ProgressBar?.Report(10);
            //find nearest point to cluster center
            var nearestPoints = new Dictionary<Vector2d, Tuple<int, double>>();
            for (int i = 0; i < nodes.Count(); i++)
            {
                var nearest = centers.MinBy(center => (center - nodes[i]).LengthSquared);
                var distance = (nearest - nodes[i]).LengthSquared;
                if (!nearestPoints.ContainsKey(nearest))
                {
                    nearestPoints[nearest] = Tuple.Create(i, distance);
                }
                else if (distance < nearestPoints[nearest].Item2)
                {
                    nearestPoints[nearest] = Tuple.Create(i, distance);
                }
            }

            //map cluster to centers
            var assignment = new Dictionary<Vector2d, Vector2d>();
            var nodesPercent = 100.0 / nodes.Length;
            for (int i = 0; i < nodes.Length; i++)
            {
                assignment[nodes[i]] = nearestPoints.MinBy(center => distances[Math.Max(i, center.Value.Item1)][Math.Min(i, center.Value.Item1)]).Key;
                parameters.ProgressBar?.Report((int)(nodesPercent * i));
            }
            parameters.ProgressBar?.Report(100);
            return assignment.GroupBy(a => a.Value, a => a.Key).Select(cluster => (IEnumerable<Vector2d>)cluster);
        }

        /// <summary></summary>
        /// <param name="graph"></param>
        /// <param name="clusters"></param>
        /// <returns>Cost of this solution, the solution and the center points.</returns>
        private Tuple<double, Clusters, IEnumerable<Vector2d>> KMeans(StreetNetwork graph, int iterations, int clusters,
            IProgress<int> progressBar = null, IProgress<Clusters> progressVisualization = null)
        {
            var assignment = new Dictionary<Vector2d, Vector2d>();
            var centers = ClusterInit(graph, clusters);
            for (int i = 0; i < iterations; ++i)
            {
                AssignCluster(centers, graph, assignment);
                centers = MoveClusters(centers, assignment);
                progressBar?.Report(i);
                if(progressVisualization != null)
                {
                    var clusterTemp = assignment.GroupBy(a => a.Value, a => a.Key).Cast<IEnumerable<Vector2d>>().ToList();
                    progressVisualization.Report(clusterTemp);
                }
            }
            AssignCluster(centers, graph, assignment);
            return Tuple.Create(
                assignment.Aggregate(0.0, (aggregation, a) => aggregation + (a.Value - a.Key).LengthSquared) / assignment.Count,
                assignment.GroupBy(a => a.Value, a => a.Key).Cast<IEnumerable<Vector2d>>(),
                centers
            );
        }

        /// <summary>Returns a random initial cluster setup.</summary>
        /// <param name="graph"></param>
        /// <param name="clusters"></param>
        /// <returns>Cluster start positions. (The length of this collection is always equal to the given parameter clusters.)</returns>
        private IEnumerable<Vector2d> ClusterInit(StreetNetwork graph, int clusters)
        {
            var vertices = graph.Vertices.Count();
            Contract.Requires(vertices >= clusters, $"K-means can not work with {clusters} clusters, when there are only {vertices} vertices.");
            var centers = new List<Vector2d>();
            foreach (var vertex in graph.Vertices)
            {
                var p = random.NextDouble();
                if (p <= (clusters - centers.Count()) / (double)vertices)
                {
                    centers.Add(vertex);
                    if (centers.Count() >= clusters) { break; }
                }
                --vertices;
            }
            return centers;
        }

        /// <summary>Assigns each vertex in the graph to a center in the given assignment dictionary.</summary>
        /// <param name="centers"></param>
        /// <param name="graph"></param>
        /// <param name="assignment"></param>
        private void AssignCluster(IEnumerable<Vector2d> centers, StreetNetwork graph, IDictionary<Vector2d, Vector2d> assignment)
        {
            foreach(var vertex in graph.Vertices)
            {
                assignment[vertex] = centers.MinBy(center => (center - vertex).LengthSquared);
            }
        }

        /// <summary>Moves the clusters to the new position given by the mean of all vertices in this cluster.</summary>
        /// <param name="oldCenters"></param>
        /// <param name="assignment"></param>
        /// <returns>New cluster positions.</returns>
        private IEnumerable<Vector2d> MoveClusters(IEnumerable<Vector2d> oldCenters, IDictionary<Vector2d, Vector2d> assignment)
        {
            var clusters = assignment
                .GroupBy(a => a.Value, a => a.Key)
                .ToDictionary(a => a.Key);
            return oldCenters
                //Remove centers, that don't have any assignments
                .Where(clusters.ContainsKey)
                .Select(center => clusters[center]
                    .Aggregate((aggregation, vertex) => aggregation + vertex) / clusters[center].Count());
        }
    }
}

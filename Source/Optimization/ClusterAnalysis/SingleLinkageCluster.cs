﻿using OpenTK;
using QuickGraph;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPlan.Optimization
{
    public class SingleLinkageCluster
    {
        private const int DefaultClusterCount = 10;

        /// <summary>Finds the cluster using the single linkage reduction formula.</summary>
        public IHierarchicalClusterOutput FindClusters(UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> graph)
        {
            Contract.Requires(graph != null);

            //Create hierarchical clusters
            var clusters = new HashSet<ClusterBase>(graph.Vertices.Select(vector => new ClusterLeave(vector)));
            var vectorToCluster = clusters.ToDictionary(cluster => (cluster as ClusterLeave).Node);

            var chainStack = new List<ClusterBase>(clusters.Take(1));
            while (clusters.Count > 1)
            {
                var last = chainStack[chainStack.Count - 1];
                //Find nearest neighbour to last
                var nearestNeighbour = last
                    //Get an enumerable of all adjacent vertices
                    .SelectMany(lastVertex =>
                    {
                        var edges = graph.AdjacentEdges(lastVertex);
                        return edges.Select(edge => Tuple.Create(lastVertex, edge.Source == lastVertex ? edge.Target : edge.Source));
                    })
                    .Where(edge => vectorToCluster[edge.Item2] != last)
                    //Find closest one
                    .MinBy(edge => (edge.Item1 - edge.Item2).Length); //single linkage!
                var nearestCluster = vectorToCluster[nearestNeighbour.Item2];
                if (chainStack.Count > 1 && chainStack[chainStack.Count - 2] == nearestCluster)
                {
                    //The last two elements of the chain are reciprocal (or mutual) nearest neighbours
                    //Remove these two clusters
                    chainStack.RemoveRange(chainStack.Count - 2, 2);
                    clusters.Remove(last);
                    clusters.Remove(nearestCluster);
                    //Add new combined cluster
                    var newCluster = new Cluster(last, nearestCluster);
                    clusters.Add(newCluster);
                    foreach (var node in newCluster)
                    {
                        vectorToCluster[node] = newCluster;
                    }
                    //Start new chain if the old one was completed
                    if (chainStack.Count <= 0) { chainStack.Add(clusters.First()); }
                }
                else
                {
                    chainStack.Add(nearestCluster);
                }
            }

            return new Output(clusters.First());
        }

        private class Output : IHierarchicalClusterOutput
        {
            private readonly ClusterBase rootCluster;
            public Output(ClusterBase rootCluster)
            {
                this.rootCluster = rootCluster;
            }

            public IEnumerable<IEnumerable<Vector2d>> Clusters(int clusterCount, bool modifiedClusterSizes)
            {
                Contract.Requires(clusterCount <= rootCluster.Nodes.Count(), "Can't create more clusters than there are vertices in the given graph.");

                var queue = new Queue<ClusterBase>();
                queue.Enqueue(rootCluster);
                while (queue.Count < clusterCount && queue.Any(cluster => cluster is Cluster))
                {
                    var current = queue.Dequeue();
                    if (current is Cluster)
                    {
                        queue.Enqueue(((Cluster)current).Left);
                        queue.Enqueue(((Cluster)current).Right);
                    }
                    else
                    {
                        queue.Enqueue(current);
                    }
                }
                return queue;
            }
        }

        [DebuggerDisplay("{Nodes}")]
        private abstract class ClusterBase : IEnumerable<Vector2d>
        {
            public abstract IEnumerable<Vector2d> Nodes { get; }

            public IEnumerator<Vector2d> GetEnumerator() => Nodes.GetEnumerator();
            IEnumerator IEnumerable.GetEnumerator() => Nodes.GetEnumerator();
        }

        [DebuggerDisplay("{Nodes}")]
        private class Cluster : ClusterBase
        {
            public ClusterBase Left { get; }
            public ClusterBase Right { get; }

            public override IEnumerable<Vector2d> Nodes => Left.Nodes.Union(Right.Nodes);

            public Cluster(ClusterBase left, ClusterBase right)
            {
                this.Left = left;
                this.Right = right;
            }
        }

        [DebuggerDisplay("{Node}")]
        private class ClusterLeave : ClusterBase
        {
            public Vector2d Node { get; }

            public override IEnumerable<Vector2d> Nodes
            {
                get { yield return Node; }
            }

            public ClusterLeave(Vector2d node) { this.Node = node; }
        }
    }
}

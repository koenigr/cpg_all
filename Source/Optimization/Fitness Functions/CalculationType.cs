﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = CalculationType.cs 
//  Copyright (C) 2014/10/18  1:05 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using LuciCommunication;
using System;
using System.ComponentModel;

namespace CPlan.Optimization
{
    [Serializable]
    public static class LuciConnection
    {
        public static string IP = "localhost";//"192.168.0.1";//"137.189.153.150";//
        public static int Port = 7654;
        public static Communication2Luci LuciHub = null;
    }

        public enum VisualOptions
    {
        SolarResults = 0,
        IsovistResults = 1,            
    };

    //================================================================================================================================================
    /// <summary>
    /// Provide the enum to define, how the variants are displayed.
    /// </summary>
    [Serializable]
    public enum DrawLayoutOptions
        {
        Rows = 0,
        RowsAll = 1,
        SOM = 2,
        //Springs = 3,
        //XMeans = 4,
    };

    //================================================================================================================================================
    /// <summary>
    /// Provide the enum to define, which Isovis measure is shown.
    /// </summary>
    [Serializable]
    public enum DisplayIsovistMeasure
    {
        Area = 0,
        Perimeter = 1,
        Compactness = 2,
        Occlusivity = 3,
        MinRadial = 4, 
        MaxRadial = 5,
    };

    //================================================================================================================================================
    /// <summary>
    /// Provide the enum to select computation locally or via LUCI.
    /// </summary>
    [Serializable]
    /// <summary>
    /// Provide the enum to select computation locally of via LUCI.
    /// </summary>
    [Serializable]
    public struct CalculationType
    {
        public static bool LuciIsovistResultsAvailable;
        public static bool LuciSolarResultsAvailable;
        public static bool LuciNetworkResultsAvailable;

        public enum Method
        {
            Local = 0,
            Luci  = 1
        };
    }

    //================================================================================================================================================
    /// <summary>
    /// This class allow to slect for each analysis type, if it shall be computed locally of via LUCI.
    /// </summary>
    [Serializable]
    public class CalculateVia
    {
        [Category("Calculate via"), Description("IsovistAnalysis via LUCI or locally")]
        public CalculationType.Method Isovist { get; set; }

        [Category("Calculate via"), Description("SolarAnalysis via LUCI or locally")]
        public CalculationType.Method Solar { get; set; }

        [Category("Calculate via"), Description("NetworkAnalysis via LUCI or locally")]
        public CalculationType.Method Network { get; set; }

        public CalculateVia()
        {
            Isovist = CalculationType.Method.Local;
            Solar = CalculationType.Method.Local;
            Network = CalculationType.Method.Local;
        }

        public CalculateVia(CalculationType.Method isovist, CalculationType.Method solar, CalculationType.Method network)
        {
            Isovist = isovist;
            Solar = solar;
            Network = network;
        }
    }

    //================================================================================================================================================
    /// <summary>
    /// Struct with enumeration for the selection of the FitnessFunction via PropertyGrid.
    /// </summary>
    [Serializable]
    public struct FitnessFunctions
    {
        public static CalculateVia FfRunVia;
        public static String FfPathToHDRI;

        [Category("FitnessFunctions"), Description("Select fitness function")]
        public enum Functions
        {
            SolarFF,
            IsovistFF,
            NetworkFF,
            CombinedFF
        };
    }

    //================================================================================================================================================
    /// <summary>
    /// This class allow to slect for each analysis type, if it shall be computed locally of via LUCI.
    /// </summary>
    [Serializable]
    public class CalculateVia
    {
        [Category("Calculate via"), Description("IsovistAnalysis via LUCI or locally")]
        public CalculationType.Method Isovist { get; set; }

        [Category("Calculate via"), Description("SolarAnalysis via LUCI or locally")]
        public CalculationType.Method Solar { get; set; }

        [Category("Calculate via"), Description("NetworkAnalysis via LUCI or locally")]
        public CalculationType.Method Network { get; set; }

        [Category("Calculate via"), Description("Host of LUCI Server")]
        public String LuciHost { get; set; }

        [Category("Calculate via"), Description("Port of LUCI Server")]
        public int LuciPort { get; set; }

        public CalculateVia()
        {
            Isovist = CalculationType.Method.Local;
            Solar = CalculationType.Method.Local;
            Network = CalculationType.Method.Local;
            LuciHost = null;
            LuciPort = -1;
        }

        public CalculateVia(CalculationType.Method isovist, CalculationType.Method solar, CalculationType.Method network, String luciHost, int luciPort)
        {
            Isovist = isovist;
            Solar = solar;
            Network = network;
            LuciHost = luciHost;
            LuciPort = luciPort;
        }
    }

    //================================================================================================================================================
    /// <summary>
    /// Struct with enumeration for the selection of the FitnessFunction via PropertyGrid.
    /// </summary>
    [Serializable]
    public struct FitnessFunctions
    {
        public static CalculateVia FfRunVia;
        public static String FfPathToHDRI;

        [Category("FitnessFunctions"), Description("Select fitness function")]
        public enum Functions
        {
            SolarFF,
            IsovistFF,
            NetworkFF,
            CombinedFF
        };

        public FitnessFunctions(CalculateVia runVia, String pathToHDRI)
        {
            FfRunVia = runVia;
            FfPathToHDRI = pathToHDRI;
        }
    }

    //================================================================================================================================================
    /// <summary>
    /// FitnessFunctions extension to return the corresponding fitness function.
    /// </summary>
    [Serializable]
    public static class FitnessFunctionSelect
    {
        public static IMultiFitnessFunction GetFunction(this FitnessFunctions.Functions ff)
        {
            switch (ff)
            {
                case FitnessFunctions.Functions.IsovistFF:
                    return new FitnessFunctionIsovist(FitnessFunctions.FfRunVia);

                case FitnessFunctions.Functions.SolarFF:
                    return new FitnessFunctionSolar(FitnessFunctions.FfRunVia, FitnessFunctions.FfPathToHDRI);

                case FitnessFunctions.Functions.NetworkFF:
                    return new FitnessFunctionNetwork(FitnessFunctions.FfRunVia);

                case FitnessFunctions.Functions.CombinedFF:
                    return new FitnessFunctionLayoutCombined(FitnessFunctions.FfRunVia, FitnessFunctions.FfPathToHDRI);

                default:
                    return new FitnessFunctionIsovist(FitnessFunctions.FfRunVia);
            }
        }
    }

}

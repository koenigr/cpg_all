﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = CalculateIsovistAnalysis.cs 
//  Copyright (C) 2015/06/11  9:02 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CPlan.Evaluation;
using CPlan.Geometry;
using NetTopologySuite.Features;
using NetTopologySuite.IO;
using NetTopologySuite.Geometries;
using Newtonsoft.Json.Linq;
////using CPlan.CommunicateToLuci;
using LuciCommunication;
using System.Windows;
//using System.IO;
//using System.Text;
//using CPlan.VisualObjects;
//using GeoAPI.Geometries;
//using OpenTK;

namespace CPlan.Optimization
{
    [Serializable]
    public class CalculatorIsovistAnalysis
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Private Fields

        private Object thisLock = new Object();
        private SolarAnalysis _solarAnalysis;
        private string _mqttTopic;
        private IsovistAnalysis _isovistField;
        private Poly2D _isovistBorder;
        private List<GenBuildingLayout> _buildings;

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        public ChromosomeLayout ActiveLayout { private set; get; }
        public ChromosomeMultiLayout ActiveMultiLayout { private set; get; }
        //public GGrid SmartGrid { get; private set; }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        //public CalculatorIsovistAnalysis() { }

        //================================================================================================================================================
        public CalculatorIsovistAnalysis(ChromosomeLayout inActiveLayout)
        {
            ActiveLayout = inActiveLayout;
        }

        //================================================================================================================================================
        public CalculatorIsovistAnalysis(ChromosomeMultiLayout inActiveLayout)
        {
            ActiveMultiLayout = inActiveLayout; ;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        //================================================================================================================================================
        public async Task<CalculatorIsovistAnalysis> Compute(CalculateVia runVia, int scId)
        {
            CalculationType.LuciIsovistResultsAvailable = false;

            PrepareGeometry();

            await Calculate(runVia, scId);

            return this;
        }

        //================================================================================================================================================
        private void PrepareGeometry()
        {
            if (ActiveMultiLayout != null)
            {
                PrepareMultiLayoutGeometry();
            }
            else if (ActiveLayout != null)
            {
                PrepareSingleLayoutGeometry();
            }
        }

        //================================================================================================================================================
        // --- Analysis for multiple blocks -----------------------------------------------------------------------
        private void PrepareMultiLayoutGeometry()
        {
            List<Line2D> obstLines = new List<Line2D>();
            List<Poly2D> obstPolys = new List<Poly2D>();
            ChromosomeLayout combi = new ChromosomeLayout(ActiveMultiLayout.SubChromosomes[0]);
            combi.Border = ActiveMultiLayout.Border;
            combi.Gens.Clear();

            _isovistBorder = ActiveMultiLayout.Border;
            // test environmental polygons
            if (ActiveMultiLayout.Environment != null)
                foreach (Poly2D envPoly in ActiveMultiLayout.Environment)
                {
                    if (envPoly.Distance(_isovistBorder) <= 0)
                    {
                        obstLines.AddRange(envPoly.Edges.ToList());
                        obstPolys.Add(envPoly);
                    }
                }

            foreach (ChromosomeLayout subChromo in ActiveMultiLayout.SubChromosomes)
            {
                combi.Gens.AddRange(subChromo.Gens);
                List<GenBuildingLayout> buildings = subChromo.Gens;

                // add the buildings
                foreach (GenBuildingLayout curBuilding in buildings)
                {
                    if (!curBuilding.Open)
                    {
                        obstLines.AddRange(curBuilding.PolygonFromGen.Edges.ToList());
                        obstPolys.Add(curBuilding.PolygonFromGen);
                    }
                }

                obstLines.AddRange(_isovistBorder.Edges);
            }

            float cellSize = ControlParameters.IsovistCellSize;
            _isovistField = new IsovistAnalysis(_isovistBorder, obstPolys, cellSize);
        }

        //================================================================================================================================================
        // --- Analysis for one block only -----------------------------------------------------------------------
        private void PrepareSingleLayoutGeometry()
        {
            List<Line2D> obstLines = new List<Line2D>();
            List<Poly2D> obstPolys = new List<Poly2D>();

            if (ActiveLayout != null)
            {
                _isovistBorder = ActiveLayout.Border;
                //Rect2D borderRect = GeoAlgorithms2D.BoundingBox(isovistBorder.PointsFirstLoop);
                _buildings = ActiveLayout.Gens;

                // test environmental polygons
                if (ActiveLayout.Environment != null)
                    foreach (Poly2D envPoly in ActiveLayout.Environment)
                    {
                        if (envPoly.Distance(_isovistBorder) <= 0)
                        {
                            obstLines.AddRange(envPoly.Edges.ToList());
                            obstPolys.Add(envPoly);
                        }
                    }

                // add the buildings
                foreach (GenBuildingLayout curBuilding in _buildings)
                {
                    if (!curBuilding.Open)
                    {
                        obstLines.AddRange(curBuilding.PolygonFromGen.Edges.ToList());
                        obstPolys.Add(curBuilding.PolygonFromGen);
                    }
                }

                obstLines.AddRange(_isovistBorder.Edges);

                float cellSize = ControlParameters.IsovistCellSize;
                _isovistField = new IsovistAnalysis(_isovistBorder, obstPolys, cellSize);

            }
        }

        // ----------------------------------------------------------------------
        // ----------------Calculation-------------------------------------
        // ----------------------------------------------------------------------

        //================================================================================================================================================
        private async Task Calculate(CalculateVia runVia, int scId)
        {
         
            if (runVia.Isovist == CalculationType.Method.Local)
            {
                _isovistField.Calculate(0.05f);
                _isovistField.Finished = true;
            }

            else if (runVia.Isovist == CalculationType.Method.Luci)
            {
                lock (thisLock)
                {
                    if (LuciConnection.LuciHub == null)
                    {
                        LuciConnection.LuciHub = new LuciCommunication.Communication2Luci();
                        bool isConnected2Luci = LuciConnection.LuciHub.connect(runVia.LuciHost, runVia.LuciPort);
                        if (!isConnected2Luci)
                        {
                            MessageBox.Show("Could not connect to Luci. Please check, if Luci is running and if host / port are entered correctly");
                            return;
                        }

                        LuciConnection.LuciHub.authenticate("lukas", "1234");
                    }
                }

                CalculationType.LuciIsovistResultsAvailable = false;
                _isovistField.Finished = false;

                // -- create Scenario --
                // CM_L -> Implementation of creating new scenario/batch (scID = -1) or adding geometry to an existing one (scID > 0)
                // CM_R -> Done
                int scenarioID = CreateScenario(_buildings, _isovistBorder);
                    if (scenarioID >= 0)
                    {
                                            MultiPoint gridPointsJ = GeometryConverter.ToMultiPoint(_isovistField.AnalysisPoints);

                    //                        GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
                    //                        string geoJSONPoints = GeoJSONWriter.Write(gridPointsJ);

                                        var inputIS = new List<KeyValuePair<string, object>>
                                        {
                                            new KeyValuePair<string, object>("inputVals", gridPointsJ)
                                        };
/*
                    NetTopologySuite.Geometries.Point[] pts = new NetTopologySuite.Geometries.Point[4];
                    pts[0] = new NetTopologySuite.Geometries.Point(102, 0);
                    pts[1] = new NetTopologySuite.Geometries.Point(103, 13);
                    pts[2] = new NetTopologySuite.Geometries.Point(104, 0);
                    pts[3] = new NetTopologySuite.Geometries.Point(105, 1);
                    NetTopologySuite.Geometries.MultiPoint mp = new MultiPoint(pts);
                    string geoJSONPts = new GeoJsonWriter().Write(mp);
                    var inputIS = new List<KeyValuePair<string, object>>
                    {
                        new KeyValuePair<string, object>("inputVals", geoJSONPts)
                    };

                    var inputLS = new List<KeyValuePair<string, object>>
                    {
                        new KeyValuePair<string, object>("delay", 13)
                    };
                    */

                    // run Service asynchronously
                    LuciCommunication.Communication2Luci.LuciAnswer? asyncAnswer = await LuciConnection.LuciHub.runAsyncService("Isovist", inputIS, scenarioID);

 
                    await ReadLuciResults(asyncAnswer);

                    // create Service -> return mqtt-topic
                    /*                       string msg = "{'action':'create_service','inputs':{'inputVals':" + geoJSONPoints + "},'classname':'Isovist','ScID':" + scenarioID + "}";

                                           ConfigSettings configInfo = GlobalConnection.CL.sendCreateService2Lucy(msg, scenarioID);

                                           if (configInfo != null)
                                           {
                                               _mqttTopic = configInfo.mqtt_topic;

                                               ServiceManager.AddConfig(_mqttTopic, configInfo);
                                               if (ActiveLayout != null)
                                                   ServiceManager.AddID(ActiveLayout.Indentity, _mqttTopic);
                                               else if (ActiveMultiLayout != null)
                                                   ServiceManager.AddID(ActiveMultiLayout.Indentity, _mqttTopic);

                                               ServiceManager.AddConfig(_mqttTopic, configInfo);

                                               // -- start calculation service --

                                               if (_mqttTopic != null)
                                               {
                                                   ServiceManager.AddMqttTopic(_mqttTopic);
                                                   ServiceManager.mqttClient.MqttMsgPublishReceived += mqttClient_MqttMsgPublishReceived;
                                                   ServiceManager.mqttClient.Publish(_mqttTopic, System.Text.Encoding.UTF8.GetBytes("RUN"));
                                               }
                                           }
                                           _isovistField.Finished = false;*/
                }
                }
            //}

            if (ActiveLayout != null)
                ActiveLayout.IsovistField = _isovistField;
            else if (ActiveMultiLayout != null)
                ActiveMultiLayout.IsovistField = _isovistField;
        }


        //================================================================================================================================================
        //private static string GetMd5HashFromByteArr(byte[] bArr)
        //{
        //    using (var md5 = System.Security.Cryptography.MD5.Create())
        //    {
        //        return BitConverter.ToString(md5.ComputeHash(bArr)).Replace("-", string.Empty);
        //    }
        //}

        //================================================================================================================================================
        private int CreateScenario(List<GenBuildingLayout> buildings, Poly2D border)
        {
            int scenarioID = -1;

            FeatureCollection featurecollection;
            System.Collections.ObjectModel.Collection<IFeature> features = new System.Collections.ObjectModel.Collection<IFeature>();

            AttributesTable buildingAttr = new AttributesTable();
            buildingAttr.AddAttribute("layer", "building");
            AttributesTable boundaryAttr = new AttributesTable();
            boundaryAttr.AddAttribute("layer", "boundary");
            AttributesTable pointAttr = new AttributesTable();
            pointAttr.AddAttribute("layer", "buildingHeight");

            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();

            //NetTopologySuite.Geometries.Polygon[] polyArray = new NetTopologySuite.Geometries.Polygon[buildings.Count + 1];
            List<NetTopologySuite.Geometries.Point> pointArray = new List<NetTopologySuite.Geometries.Point>();
            int i = 0;
            foreach (GenBuildingLayout building in buildings)
            {
                LinearRing r1 = new NetTopologySuite.Geometries.LinearRing(GeometryConverter.ToCoordinates(building.PolygonFromGen));
                NetTopologySuite.Geometries.Polygon p = new NetTopologySuite.Geometries.Polygon(r1);
                features.Add(new Feature(p, buildingAttr));

                NetTopologySuite.Geometries.Point pt = new NetTopologySuite.Geometries.Point(i, building.Height);
                pointArray.Add(pt);
                i++;
            }

            LinearRing r2 = new NetTopologySuite.Geometries.LinearRing(GeometryConverter.ToCoordinates(border));
            NetTopologySuite.Geometries.Polygon p2 = new NetTopologySuite.Geometries.Polygon(r2);
            features.Add(new Feature(p2, boundaryAttr));


            NetTopologySuite.Geometries.MultiPoint multiPts = new MultiPoint(pointArray.ToArray());
            features.Add(new Feature(multiPts, pointAttr));


            featurecollection = new FeatureCollection(features);

            LuciCommunication.Communication2Luci.LuciAnswer luciAnswer = LuciConnection.LuciHub.createScenario("cplan", featurecollection, null);
            int.TryParse(LuciConnection.LuciHub.GetOutputValue(luciAnswer, "ScID").ToString(), out scenarioID);
    /*            if (luciAnswer.Outputs != null)
            {
                var scID = luciAnswer.Outputs.Where(q => q.Key == "ScID");
                if (scID != null && scID.Any())
                    scenarioID = int.Parse(scID.First().Value.ToString());
            }
        
            string geoJSONPoly = GeoJSONWriter.Write(featurecollection);
            geoJSONPoly = geoJSONPoly.Substring(0, geoJSONPoly.Length - 1);
            string createScenario4Geo = "{'action':'create_scenario','name':'test','geometry':{'GeoJSON':{'format':'GeoJSON','geometry':" + geoJSONPoly + "}}}}}";

            JObject scenarioObj = null;
            try
            {
                scenarioObj = GlobalConnection.CL.sendAction2Lucy(createScenario4Geo);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.StackTrace);
            }

            if (scenarioObj != null)
            {
                JProperty result = ((JProperty)(scenarioObj.First));
                if (result != null)
                {
                    if (result.Value["ScID"] != null)
                        scenarioID = (int)result.Value["ScID"];
                }
            }*/

            

            return scenarioID;
        }

        //================================================================================================================================================
        /*        void mqttClient_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
                {
                    if (e.Topic == _mqttTopic && System.Text.Encoding.UTF8.GetString(e.Message) == "DONE")
                    {
                        // service results available
                        ReadLucyResults(_mqttTopic);
                        CalculationType.LuciIsovistResultsAvailable = true;
                    }
                    else if (e.Topic == _mqttTopic && System.Text.Encoding.UTF8.GetString(e.Message) != "RUN")
                    {
                        Console.Out.WriteLine("haeh");
                    }
                }*/

        public async Task ReadLuciResults(LuciCommunication.Communication2Luci.LuciAnswer? luciAnswer)
        {
            if (luciAnswer == null)
                return;

            LuciCommunication.Communication2Luci.LuciAnswer input = (LuciCommunication.Communication2Luci.LuciAnswer)luciAnswer;
            if (input.State != LuciCommunication.Communication2Luci.LuciState.Result)
                return;

            string output = (string)LuciConnection.LuciHub.GetOutputValue(input, "outputVals");

            await _isovistField.AnalyseLucyResults(output);
            _isovistField.Finished = true;
            ActiveLayout.IsovistField = _isovistField;
        }

        //================================================================================================================================================
 /*       private void ReadLucyResults(string mqtt_topic)
        {
            string instanceID = "40";
            int index = mqtt_topic.LastIndexOf("/");
            if (index > 0)
                instanceID = mqtt_topic.Substring(index + 1);

            string msg = "{'action':'get_service_outputs','SObjID':" + instanceID + "}";
            JObject result = GlobalConnection.CL.sendAction2Lucy(msg);

            //float cellSize = ControlParameters.IsovistCellSize;

            if (result != null)
            {
                JToken outputs = result.Value<JToken>("result").Value<JToken>("outputs");
                if (outputs != null)
                {
                    string output = (string)(outputs.Value<JObject>("outputVals").Value<String>("value"));

                    _isovistField.AnalyseLucyResults(output);
                    _isovistField.Finished = true;
                    ActiveLayout.IsovistField = _isovistField;
                }
            }
        }
        */
        #endregion

        //================================================================================================================================================
        //private void IsovistAnalysis(ChromosomeMultiLayout curChromo)
        //{
        //    List<Line2D> obstLines = new List<Line2D>();
        //    List<Poly2D> obstPolys = new List<Poly2D>();
        //    ChromosomeLayout combi = new ChromosomeLayout(curChromo.SubChromosomes[0]);
        //    combi.Border = curChromo.Border;
        //    combi.Gens.Clear();

        //    if (curChromo != null)
        //    {
        //        Poly2D isovistBorder = curChromo.Border;
        //        Rect2D borderRect = GeoAlgorithms2D.BoundingBox(isovistBorder.PointsFirstLoop);

        //        // test environmental polygons
        //        if (curChromo.Environment != null)
        //            foreach (Poly2D envPoly in curChromo.Environment)
        //            {
        //                if (envPoly.Distance(isovistBorder) <= 0)
        //                {
        //                    obstLines.AddRange(envPoly.Edges.ToList());
        //                    obstPolys.Add(envPoly);
        //                }
        //            }

        //        foreach (ChromosomeLayout subChromo in curChromo.SubChromosomes)
        //        {
        //            combi.Gens.AddRange(subChromo.Gens);
        //            List<GenBuildingLayout> buildings = subChromo.Gens;

        //            // add the buildings
        //            foreach (GenBuildingLayout curBuilding in buildings)
        //            {
        //                if (!curBuilding.Open)
        //                {
        //                    obstLines.AddRange(curBuilding.PolygonFromGen.Edges.ToList());
        //                    obstPolys.Add(curBuilding.PolygonFromGen);
        //                }
        //            }

        //            obstLines.AddRange(isovistBorder.Edges);
        //        }

        //        float cellSize = ControlParameters.IsovistCellSize;
        //        _isovistField = new IsovistAnalysis(isovistBorder, obstPolys, cellSize);

        //        TimeSpan calcTime1, calcTime2;
        //        DateTime start = DateTime.Now;

        //        DateTime end = DateTime.Now;
        //        calcTime1 = end - start;

        //        // -- run via server --------------------------------------------------------------------------- 
        //        if (RunVia == CalculationType.Method.Local)
        //        {                  
        //            _isovistField.Calculate(0.05f);
        //            _isovistField.Finished = true;
        //        }
        //        else if (RunVia == CalculationType.Method.Luci)
        //        {
        //            // -- create Scenario --
        //            FitnessFunctionLayout ff = new FitnessFunctionLayout(RunVia);
        //            (ff as IMultiFitnessFunction).Evaluate(combi);
        //            _isovistField = combi.IsovistField;
        //            SolarAnalysisResults = combi.SolarAnalysisResults;

        //            CommunicateToLucy.ServiceManager.ReplaceID(combi.Indentity, curChromo.Indentity);

        //            _isovistField.Finished = false;
        //        }
        //        else
        //        {
        //            _isovistField.Calculate(0.05f);
        //            _isovistField.Finished = true;
        //        }

        //        curChromo.IsovistField = _isovistField;
        //        curChromo.SolarAnalysisResults = SolarAnalysisResults;

        //    }           
        //}

    }
}

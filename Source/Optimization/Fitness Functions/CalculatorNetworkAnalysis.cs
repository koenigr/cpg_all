﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = CalculateNetworkAnalysis.cs 
//  Copyright (C) 2015/06/11  9:02 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPlan.Evaluation;
using FW_GPU_NET;

namespace CPlan.Optimization
{
    [Serializable]
    public class CalculatorNetworkAnalysis
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Private Fields

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        //public InstructionTreePisa Network { private set; get; }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        //public CalculatorNetworkAnalysis() { }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        public async Task<CalculatorNetworkAnalysis> Compute(IMultiChromosome inNetwork, CalculationType.Method runVia)
        {
            //Network = inNetwork;

            if (runVia == CalculationType.Method.Local)
                ComputeLocal(inNetwork);
            else if (runVia == CalculationType.Method.Luci)
                ComputeViaLucy(inNetwork);//chromosome);
            else
                ComputeLocal(inNetwork);

            return this;
        }

        private void ComputeLocal(IMultiChromosome chromosome)
        {          
            List<double> fitnessValues = new List<double>();
            InstructionTreePisa curNetwork = (InstructionTreePisa)chromosome;
            curNetwork.Network = null;
            curNetwork.Network = curNetwork.CreateStreetNetwork();
            //------------------------------------------------------//

            //Subdivision InverseGraph;
            //Dictionary<Vector2d, Line2D> inverseEdgesMapping = new Dictionary<Vector2d, Line2D>();
            //Dictionary<Line2D, double> realDist = new Dictionary<Line2D, double>();
            //Dictionary<Line2D, double> realAngle = new Dictionary<Line2D, double>();

            //InverseGraph = RndGlobal.ConstructInverseGraph(StreetGenerator.Graph, ref RealDist, ref RealAngle, ref InverseEdgesMapping);
            //InverseGraph = RndGlobal.ConstructInverseGraph(curNetwork.Network, ref realDist, ref realAngle, ref inverseEdgesMapping);

            Graph fwGraph = GraphTools.GenerateFWGpuGraph(curNetwork.Network);
            Graph fwInverseGraph = GraphTools.GenerateInverseDoubleGraph(fwGraph);

            ShortestPath shortPahtesAngular = new ShortestPath(curNetwork.Network);
            shortPahtesAngular.Evaluate(new ToolStripProgressBar(), fwGraph, fwInverseGraph);

            curNetwork.ShortestPathes = shortPahtesAngular;

            //if (!ShortPahtesAngular.Connected) // --- something is wrong with the graph - probably unconnected ---
            //{
            //    for (int i = 0; i < _dimensions; i++) fitnessValues.Add(worstValue);
            //}
            //else // -- if graph is connected, calculate the fitness values ---

            fwGraph.ClearGraph();
            fwGraph.Dispose();
            fwInverseGraph.Dispose();
            
        }

        List<double> ComputeViaLucy(IMultiChromosome chromosome)
        {
            throw new NotImplementedException();
            //List<double> fitnessValues = new List<double>();
            //return fitnessValues;
        }

        # endregion
    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = CalculatorSolarAnalysis.cs 
//  Copyright (C) 2015/06/15  12:15 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CPlan.Evaluation;
using CPlan.Geometry;
using CPlan.VisualObjects;
using DirectSunLightNet;
using GeoAPI.Geometries;
using NetTopologySuite.Geometries;
using OpenTK;
using NetTopologySuite.Features;
using NetTopologySuite.IO;
using Newtonsoft.Json.Linq;
////using CPlan.CommunicateToLuci;
using LuciCommunication;
using System.IO;

namespace CPlan.Optimization
{
    [Serializable]
    public class CalculatorSolarAnalysis
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Private Fields

        private Object _thisLock = new Object();
        private SolarAnalysis _solarAnalysis;
        private GenBuildingLayout[] _buildings;
        private Poly2D _isovistBorder;
        private string _mqttTopicSolar;
        private string _pathToHdriFile;
        //private List<IPolygon> _polygons;

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        public ChromosomeLayout ActiveLayout { get; private set; }
        public ChromosomeMultiLayout ActiveMultiLayout { get; private set; }
        public GGrid SmartGrid { get; private set; }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        //public CalculatorSolarAnalysis() { }

        public CalculatorSolarAnalysis(ChromosomeLayout inActiveLayout, String pathToHdri)
        {
            ActiveLayout = inActiveLayout;
            _pathToHdriFile = pathToHdri;
        }

        public CalculatorSolarAnalysis(ChromosomeMultiLayout inActiveLayout, String pathToHdri)
        {
            ActiveMultiLayout = inActiveLayout;
            _pathToHdriFile = pathToHdri;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        public async Task<CalculatorSolarAnalysis> Compute(CalculateVia runVia, int scID)
        {
            _buildings = new GenBuildingLayout[0];
            CalculationType.LuciSolarResultsAvailable = false;

            PrepareGeometry();

            await Calculate(runVia, scID);

            return this;
        }

        private void PrepareGeometry()
        {
            if (ActiveMultiLayout != null)
            {
                PrepareMultiLayoutGeometry();
            }
            else if (ActiveLayout != null)
            {
                PrepareSingleLayoutGeometry();
            }
        }

        // --- Analysis for multiple blocks -----------------------------------------------------------------------
        private void PrepareMultiLayoutGeometry()
        {
            IEnumerable<Vector3d> gridNormales = null;
            IEnumerable<Vector3d> gridPoints = null;          
            float cellSize = ControlParameters.IsovistCellSize;
            _isovistBorder = ActiveMultiLayout.Border;
            //if (ActiveMultiLayout != null)
            

            //if ( !SmartGrid.SFDerived) 
            {
                var sf = new ScalarField(ActiveMultiLayout.Border.PointsFirstLoop
                                        .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
                                        cellSize, new Vector3d(0, 0, 1));
                SmartGrid = new GGrid(sf, "solarAnalysis");
            }
            //else if (SmartGrid.ScalarField == null)
            //{
            //    SmartGrid.ScalarField = new ScalarField(ActiveMultiLayout.Border.PointsFirstLoop
            //                            .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
            //                            cellSize, new Vector3d(0, 0, 1));
            //}

            gridNormales = Enumerable.Repeat(SmartGrid.ScalarField.Normal, SmartGrid.ScalarField.GridSize);
            gridPoints = SmartGrid.ScalarField.Grid.ToList();

            foreach (ChromosomeLayout curChromo in ActiveMultiLayout.SubChromosomes)
            {
                _buildings = curChromo.Gens.ToArray();

                foreach (GenBuildingLayout building in _buildings)
                {
                    var bpoints = building.PolygonFromGen.PointsFirstLoop;
                    var bcenter = new Vector3d(building.PolygonFromGen.PointsFirstLoop.Mean());
                    bcenter.Z = building.BuildingHeight / 2;
                    building.SFDerived = true;
                    building.FieldTypeName = "solarAnalysis";
                    building.SideGrids = new ScalarField[1 + bpoints.Length];
                    building.SideGrids[0] = new ScalarField(bpoints
                                            .Select(v => new Vector3d(v.X, v.Y, building.BuildingHeight)).ToArray(),
                                            cellSize, new Vector3d(0, 0, 1));
                    for (int i = 1; i <= bpoints.Length; i++)
                    {
                        var tpoints = new[] {
                        new Vector3d(bpoints[i-1].X, bpoints[i-1].Y, 0),
                        new Vector3d(bpoints[i-1].X, bpoints[i-1].Y, building.BuildingHeight),
                        new Vector3d(bpoints[i % bpoints.Length].X, bpoints[i % bpoints.Length].Y, building.BuildingHeight),
                        new Vector3d(bpoints[i % bpoints.Length].X, bpoints[i % bpoints.Length].Y, 0)
                    };
                        building.SideGrids[i] = new ScalarField(tpoints, cellSize, tpoints.Mean() - bcenter);


                    }
                    // next we have to put all the grids together into one...
                    foreach (var field in building.SideGrids)
                    {
                        gridPoints = gridPoints.Concat(field.Grid);
                        gridNormales = gridNormales.Concat(Enumerable.Repeat(field.Normal, field.GridSize));
                    }
                }
            }
            _solarAnalysis = new SolarAnalysis(gridPoints, gridNormales);
            _solarAnalysis.ClearGeometry();
            //_isovistBorder = _borderPoly.Poly.Clone();
            //_isovistBorder.Offset(1.2);

            foreach (ChromosomeLayout curChromo in ActiveMultiLayout.SubChromosomes)
            {
                _buildings = curChromo.Gens.ToArray();
                // --- add the buildings ---
                //var converter = new Pg3DNtsInterop.Pg3DNtsConverter();
                foreach (GenBuildingLayout curRect in _buildings)
                {
                    if (!curRect.Open)
                    {
                        Poly2D p2D = curRect.PolygonFromGen;
                        var pvv = new CGeom3d.Vec_3d[1][];
                        var pv = new CGeom3d.Vec_3d[p2D.PointsFirstLoop.Length];
                        pvv[0] = pv;
                        int z = 0;
                        foreach (Vector2d v2D in p2D.PointsFirstLoop)
                            pv[z++] = new CGeom3d.Vec_3d(v2D.X, v2D.Y, curRect.BuildingHeight);

                        CGeom3d.Vec_3d[][] pvvBottom = p2D.PointsVec3D;
                        _solarAnalysis.AddPolygon(pvvBottom, 1.0f);
                        //_polygons.Add(converter.PureGeomToPolygon(pvvBottom));
                        _solarAnalysis.AddPolygon(pvv, 1.0f);
                        //_polygons.Add(converter.PureGeomToPolygon(pvv));

                        for (int i = 0; i < pv.Length; ++i)
                        {
                            int j = i + 1;
                            if (j >= pv.Length) j -= pv.Length;

                            var pvvSide = new CGeom3d.Vec_3d[1][];
                            var pvSide = new CGeom3d.Vec_3d[4];
                            pvvSide[0] = pvSide;
                            pvSide[0] = pvvBottom[0][i];
                            pvSide[1] = pvvBottom[0][j];
                            pvSide[2] = pv[j];
                            pvSide[3] = pv[i];

                            _solarAnalysis.AddPolygon(pvvSide, 1.0f);
                            //_polygons.Add(converter.PureGeomToPolygon(pvvSide));
                        }
                    }
                }
            }     
        }


        // --- Analysis for one block only -----------------------------------------------------------------------
        private void PrepareSingleLayoutGeometry()
        {
            IEnumerable<Vector3d> gridNormales = null;
            IEnumerable<Vector3d> gridPoints = null;           
            float cellSize = ControlParameters.IsovistCellSize;
            _isovistBorder = ActiveLayout.Border;

            //if (SmartGrid == null || !SmartGrid.SFDerived) {
            //if ( !SmartGrid.SFDerived) 
            {
                var sf = new ScalarField(ActiveLayout.Border.PointsFirstLoop
                    .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
                    cellSize, new Vector3d(0, 0, 1));
                SmartGrid = new GGrid(sf, "solarAnalysis");
            }
            //else if (SmartGrid.ScalarField == null)
            //{
            //    SmartGrid.ScalarField = new ScalarField(ActiveLayout.Border.PointsFirstLoop
            //        .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
            //        cellSize, new Vector3d(0, 0, 1));
            //}

            gridNormales = Enumerable.Repeat(SmartGrid.ScalarField.Normal, SmartGrid.ScalarField.GridSize);
            gridPoints = SmartGrid.ScalarField.Grid.ToList();
            _buildings = ActiveLayout.Gens.ToArray();

            foreach (GenBuildingLayout building in _buildings)
            {
                var bpoints = building.PolygonFromGen.PointsFirstLoop;
                var bcenter = new Vector3d(building.PolygonFromGen.PointsFirstLoop.Mean());
                bcenter.Z = building.BuildingHeight / 2;
                building.SFDerived = true;
                building.FieldTypeName = "solarAnalysis";
                building.SideGrids = new ScalarField[1 + bpoints.Length];
                building.SideGrids[0] = new ScalarField(bpoints
                    .Select(v => new Vector3d(v.X, v.Y, building.BuildingHeight)).ToArray(),
                    cellSize, new Vector3d(0, 0, 1));
                for (int i = 1; i <= bpoints.Length; i++)
                {
                    var tpoints = new[]
                    {
                        new Vector3d(bpoints[i - 1].X, bpoints[i - 1].Y, 0),
                        new Vector3d(bpoints[i - 1].X, bpoints[i - 1].Y, building.BuildingHeight),
                        new Vector3d(bpoints[i%bpoints.Length].X, bpoints[i%bpoints.Length].Y, building.BuildingHeight),
                        new Vector3d(bpoints[i%bpoints.Length].X, bpoints[i%bpoints.Length].Y, 0)
                    };
                    building.SideGrids[i] = new ScalarField(tpoints, cellSize, tpoints.Mean() - bcenter);


                }
                // next we have to put all the grids together into one...
                foreach (var field in building.SideGrids)
                {
                    gridPoints = gridPoints.Concat(field.Grid);
                    gridNormales = gridNormales.Concat(Enumerable.Repeat(field.Normal, field.GridSize));
                }
            }

            _solarAnalysis = new SolarAnalysis(gridPoints, gridNormales);
            _solarAnalysis.ClearGeometry();
            //_isovistBorder = _borderPoly.Poly.Clone();
            //_isovistBorder.Offset(1.2);

            // --- add the buildings ---              
            //var converter = new Pg3DNtsInterop.Pg3DNtsConverter();
            foreach (GenBuildingLayout curRect in _buildings)
            {
                if (!curRect.Open)
                {
                    Poly2D p2D = curRect.PolygonFromGen;
                    var pvv = new CGeom3d.Vec_3d[1][];
                    var pv = new CGeom3d.Vec_3d[p2D.PointsFirstLoop.Length];
                    pvv[0] = pv;
                    int z = 0;
                    foreach (Vector2d v2D in p2D.PointsFirstLoop)
                        pv[z++] = new CGeom3d.Vec_3d(v2D.X, v2D.Y, curRect.BuildingHeight);

                    CGeom3d.Vec_3d[][] pvvBottom = p2D.PointsVec3D;
                    _solarAnalysis.AddPolygon(pvvBottom, 1.0f);
                    //_polygons.Add(converter.PureGeomToPolygon(pvvBottom));
                    _solarAnalysis.AddPolygon(pvv, 1.0f);
                    //_polygons.Add(converter.PureGeomToPolygon(pvv));

                    for (int i = 0; i < pv.Length; ++i)
                    {
                        int j = i + 1;
                        if (j >= pv.Length) j -= pv.Length;

                        var pvvSide = new CGeom3d.Vec_3d[1][];
                        var pvSide = new CGeom3d.Vec_3d[4];
                        pvvSide[0] = pvSide;
                        pvSide[0] = pvvBottom[0][i];
                        pvSide[1] = pvvBottom[0][j];
                        pvSide[2] = pv[j];
                        pvSide[3] = pv[i];

                        _solarAnalysis.AddPolygon(pvvSide, 1.0f);
                        //_polygons.Add(converter.PureGeomToPolygon(pvvSide));
                    }
                }
               
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // --- Run the calculation ---
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////           
        private async Task Calculate(CalculateVia runVia, int scID)
        {
            if ((ActiveLayout != null) || (ActiveMultiLayout != null))
            {
                //var mpol = new MultiPolygon(_polygons.ToArray());

                //############### run local ###############//
                if (runVia.Solar == CalculationType.Method.Local)
                {
                    CalculateLocal();
                }

                //############### run via luci ###############//
                else if (runVia.Solar == CalculationType.Method.Luci)
                {
                    await CalculateViaLuci(runVia, scID);
                }
            }
        }

        /// <summary>
        /// Calculate Solar Analysis via LUCI
        /// </summary>
        /// <param name="scID">Scenario ID for LUCI.</param>
        private async Task CalculateViaLuci(CalculateVia runVia, int scID)
        {
            lock (_thisLock)
            {
                if (LuciConnection.LuciHub == null)
                {
                    LuciConnection.LuciHub = new LuciCommunication.Communication2Luci();
                    bool isConnected2Luci = LuciConnection.LuciHub.connect(runVia.LuciHost, runVia.LuciPort);
                    if (!isConnected2Luci)
                    {
                        System.Windows.MessageBox.Show("Could not connect to Luci. Please check, if Luci is running and if host / port are entered correctly");
                        return;
                    }

                    LuciConnection.LuciHub.authenticate("lukas", "1234");
                }
            }

                // -- create Scenario --
                // CM_L -> Implementation of creating new scenario/batch (scID = -1) or adding geometry to an existing one (scID > 0)
                int scenarioID = CreateScenarioSolar(_buildings.ToList(), _isovistBorder);
                if (scenarioID >= 0)
                {
//                    string hdriFilename = "\\CA Cumulative Sky - Year 2012 23 Minute step.hdr";
                string hdriFilename = "\\HDRISky_2015_1Year_Singapore.hdr";
                //var pathToHdriFile = CPlan.UrbanPattern.Properties.Settings.Default.PathToHDRI + hdriFilename;
                var pathToHdriFile = _pathToHdriFile + hdriFilename;
                    if (!File.Exists(pathToHdriFile))
                    {
                        pathToHdriFile = Path.GetFullPath(Environment.CurrentDirectory + hdriFilename);
                        if (!File.Exists(pathToHdriFile))
                        {
                            //pathToHdriFile = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "\\CA Cumulative Sky - Year 2012 23 Minute step.hdr"); ;
                            pathToHdriFile = Path.GetFullPath(Environment.CurrentDirectory + hdriFilename);
                            if (!File.Exists(pathToHdriFile))
                            {
                                System.Windows.Forms.MessageBox.Show("There is no valid path to a HDRI file!");
                                return;
                            }
                        }
                    }



                var hdri = File.ReadAllBytes(pathToHdriFile);
                    float cellSize = ControlParameters.IsovistCellSize;
                    //                         System.Windows.Forms.MessageBox.Show("1");
/*cm                    ScalarField sf = null;
                    if (ActiveMultiLayout != null)
                    {
                        sf = new ScalarField(ActiveMultiLayout.Border.PointsFirstLoop
                            .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
                            cellSize, new Vector3d(0, 0, 1));
                    }
                    else if (ActiveLayout != null)
                    {
                        sf = new ScalarField(ActiveLayout.Border.PointsFirstLoop
                            .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
                            cellSize, new Vector3d(0, 0, 1));
                    }
*/
                    //GGrid _smartGrid = new GGrid(sf, "solarAnalysis");
                    //else if (_smartGrid.ScalarField == null)
                    //{
                    //    _smartGrid.ScalarField = new ScalarField(_activeMultiLayout.Border.PointsFirstLoop
                    //                            .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
                    //                            cellSize, new Vector3d(0, 0, 1));
                    //}

//cm                    if (sf != null)
                    {
/*cm                        IEnumerable<Vector3d> gridNormales = Enumerable.Repeat(sf.Normal, sf.GridSize);
                        IEnumerable<Vector3d> gridPoints = sf.Grid.ToList();

                        foreach (GenBuildingLayout building in _buildings)
                        {
                            // next we have to put all the grids together into one...
                            if (building.SideGrids != null)
                            {
                                foreach (var field in building.SideGrids)
                                {
                                    gridPoints = gridPoints.Concat(field.Grid);
                                    gridNormales = gridNormales.Concat(Enumerable.Repeat(field.Normal, field.GridSize));
                                }
                            }
                        }
*/
                        //System.Windows.Forms.MessageBox.Show("6");

                        // creating a service instance with input data
////                        var points = gridPoints.SelectMany(p => new[] { p.X, p.Y, p.Z }).ToArray();
////                        var normales = gridNormales.SelectMany(p => new[] { p.X, p.Y, p.Z }).ToArray();


                    var points = _solarAnalysis.GridPoints.SelectMany(p => new[] { p.X, p.Y, p.Z }).ToArray();
                    var normales = _solarAnalysis.GridNormales.SelectMany(p => new[] { p.X, p.Y, p.Z }).ToArray();


                    // here we create byte array and StreamInfo based on it (MD5 generated based on bytes)
                    byte[][] attachedBytes = new byte[3][];
                        attachedBytes[0] = new byte[points.Length * sizeof(double)];
                        attachedBytes[1] = new byte[normales.Length * sizeof(double)];
                        attachedBytes[2] = hdri;
                        Buffer.BlockCopy(points, 0, attachedBytes[0], 0, attachedBytes[0].Length);
                        Buffer.BlockCopy(normales, 0, attachedBytes[1], 0, attachedBytes[1].Length);
                        //System.Windows.Forms.MessageBox.Show("7");

                        //                              ConnectionManager lucyCM = new ConnectionManager("localhost", 7654, "lukas", "1234");
                        /*
                        var remoteServiceInstance = lucyCM.CreateServiceInstance("SolarAnalysis", scenarioID,
                            new Dictionary<string, object>()
                        {
                            {"Points", new StreamInfo(attachedBytes[0], 1, "doublesXYZ")}, // array of doubles: XYZ coordinates
                            {"Normales", new StreamInfo(attachedBytes[1], 2, "doublesXYZ")}, // array of doubles: XYZ coordinates
                            // Compulsory scenario parameter - HDRI (Information about file)
                            {"HdriImage", new Lucy.StreamInfo(hdri, 3, "hdrImage")},
                            // Optional scenario parameters. If they are not listed in a list of parameters. TODO: Now they are obligatory :(
                            {"calculationMode", "Klux" },
                            {"resolution", 64},
                            {"northOrientationAngle", 0f}
                        }, attachedBytes);
                        */
                        float angle = 0f;
                        int resolution = 64;
                        string calcMode = "Klux";


                        // neu
                        var inputSolar = new List<KeyValuePair<string, object>>
                                        {
                                            new KeyValuePair<string, object>("northOrientationAngle", angle),
                                            new KeyValuePair<string, object>("resolution", resolution),
                                            new KeyValuePair<string, object>("calculationMode", calcMode)
                                        };

                        LuciCommunication.Communication2Luci.LuciStreamData streamData1 = new Communication2Luci.LuciStreamData() { Data = attachedBytes[0], Format = "doublesXYZ", Name = "Points" };
                        LuciCommunication.Communication2Luci.LuciStreamData streamData2 = new Communication2Luci.LuciStreamData() { Data = attachedBytes[1], Format = "doublesXYZ", Name = "Normales" };
                        LuciCommunication.Communication2Luci.LuciStreamData streamData3 = new Communication2Luci.LuciStreamData() { Data = attachedBytes[2], Format = "doublesXYZ", Name = "HdriImage" };



//                    runSolarService(inputSolar, scenarioID, streamData1, streamData2, streamData3);



                    // run Service asynchronously
                      LuciCommunication.Communication2Luci.LuciAnswer? asyncAnswer = await LuciConnection.LuciHub.runAsyncService("SolarAnalysis", inputSolar, scenarioID, streamData1, streamData2, streamData3);
//                  LuciCommunication.Communication2Luci.LuciAnswer? asyncAnswer = LuciConnection.LuciHub.runService("SolarAnalysis", inputSolar, scenarioID, streamData1, streamData2, streamData3);


                    await ReadLuciResultsSolar(asyncAnswer);
                    await Task.Delay(1);
                        // neu

/*
                        string checksum1 = GetMd5HashFromByteArr(attachedBytes[0]);
                        string checksum2 = GetMd5HashFromByteArr(attachedBytes[1]);
                        string checksum3 = GetMd5HashFromByteArr(hdri);

                        string msg = "{'action':'create_service','inputs':{'northOrientationAngle':" + angle +
                                     ",'Points':{'format':'doublesXYZ','streaminfo':{'order':1, 'checksum':'" + checksum1 +
                                     "'}},'Normales':{'format':'doublesXYZ','streaminfo':{'order':2, 'checksum':'" + checksum2 +
                                     "'}},'HdriImage':{'format':'hdrImage','streaminfo':{'order':3, 'checksum':'" + checksum3 +
                                     "'}},'resolution':" + resolution + ", 'calculationMode':'" + calcMode + "'},'classname':'SolarAnalysis','ScID':" + scenarioID + "}";
                        //System.Windows.Forms.MessageBox.Show(msg);
                        JObject lucyAnswer = GlobalConnection.CL.sendAction2Lucy(msg, attachedBytes[0], attachedBytes[1], hdri);


                        int serviceID = lucyAnswer.Value<JObject>("result").Value<int>("SObjID");
                        _mqttTopicSolar = lucyAnswer.Value<JObject>("result").Value<String>("mqtt_topic");

                        if (_mqttTopicSolar.EndsWith("/"))
                            _mqttTopicSolar = _mqttTopicSolar.Substring(0, _mqttTopicSolar.Length - 1);

                        /*
                        ConfigSettings configInfo = new ConfigSettings() { scenarioID = scenarioID, mqtt_topic = _mqttTopicSolar, objID = serviceID };

                        if (configInfo != null)
                        {
                            _mqttTopicSolar = configInfo.mqtt_topic;

                            ServiceManager.AddConfig2(_mqttTopicSolar, configInfo);

                            if (ActiveLayout != null)
                                ServiceManager.AddID(ActiveLayout.Indentity, _mqttTopicSolar);
                            else if (ActiveMultiLayout != null)
                                ServiceManager.AddID(ActiveMultiLayout.Indentity, _mqttTopicSolar);

                            ServiceManager.AddConfig2(_mqttTopicSolar, configInfo);


                            // -- start calculation service --

                            if (_mqttTopicSolar != null)
                            {
//                                ServiceManager.AddMqttTopic2(_mqttTopicSolar);
//                                ServiceManager.mqttClient.MqttMsgPublishReceived += mqttClient_MqttMsgPublishReceived_Solar;
//                                ServiceManager.mqttClient.Publish(_mqttTopicSolar, System.Text.Encoding.UTF8.GetBytes("RUN"));
                            }
                        }*/
                    }
                }
//            }
        }


        private async Task runSolarService(List<KeyValuePair<string, object>> inputSolar, int scenarioID, LuciCommunication.Communication2Luci.LuciStreamData streamData1, 
            LuciCommunication.Communication2Luci.LuciStreamData streamData2, LuciCommunication.Communication2Luci.LuciStreamData streamData3)
        {
            // run Service asynchronously
            LuciCommunication.Communication2Luci.LuciAnswer? asyncAnswer = await LuciConnection.LuciHub.runAsyncService("SolarAnalysis", inputSolar, scenarioID, streamData1, streamData2, streamData3);

            await ReadLuciResultsSolar(asyncAnswer);
        }


        /// <summary>
        /// Calculate Solar Analysis on the local machine.
        /// </summary>
        private void CalculateLocal()
        {
            var saResults = Enumerable.Empty<double>();
            _solarAnalysis.Calculate(CalculationMode.illuminance_klux);
            saResults = _solarAnalysis.Results;

            SmartGrid.ScalarField.GenerateTexture(saResults.Take(SmartGrid.ScalarField.GridSize), "solarAnalysis");
            SmartGrid.GenerateTexture();

            // -- store the results for the plane of the chromosomes --
            if (ActiveMultiLayout != null)
            {
                ActiveMultiLayout.SolarAnalysisResults = saResults.Take(SmartGrid.ScalarField.GridSize);
                if (ActiveLayout != null)
                    ActiveLayout.SolarAnalysisResults = saResults.Take(SmartGrid.ScalarField.GridSize);
            }
            else if (ActiveLayout != null)
            {
                ActiveLayout.SolarAnalysisResults = saResults.Take(SmartGrid.ScalarField.GridSize);
            }

            saResults = saResults.Skip(SmartGrid.ScalarField.GridSize);


            if (ActiveMultiLayout != null)
            {
                foreach (ChromosomeLayout curChromo in ActiveMultiLayout.SubChromosomes)
                {
                    _buildings = curChromo.Gens.ToArray();
                    foreach (GenBuildingLayout building in _buildings)
                    {
                        foreach (var field in building.SideGrids)
                        {
                            field.GenerateTexture(saResults.Take(field.GridSize), "solarAnalysis");
                            saResults = saResults.Skip(field.GridSize);
                        }
                    }
                }
            }
            else
            {
                if (ActiveLayout != null) _buildings = ActiveLayout.Gens.ToArray();
                foreach (GenBuildingLayout building in _buildings)
                {
                    foreach (var field in building.SideGrids)
                    {
                        field.GenerateTexture(saResults.Take(field.GridSize), "solarAnalysis");
                        saResults = saResults.Skip(field.GridSize);
                    }
                }
            }
        }

        private void genTexture(double[] results)
        {
            var saResults = Enumerable.Empty<double>();
            saResults = results;

            SmartGrid.ScalarField.GenerateTexture(saResults.Take(SmartGrid.ScalarField.GridSize), "solarAnalysis");
            SmartGrid.GenerateTexture();

            // -- store the results for the plane of the chromosomes --
            if (ActiveMultiLayout != null)
            {
                ActiveMultiLayout.SolarAnalysisResults = saResults.Take(SmartGrid.ScalarField.GridSize);
                if (ActiveLayout != null)
                    ActiveLayout.SolarAnalysisResults = saResults.Take(SmartGrid.ScalarField.GridSize);
            }
            else if (ActiveLayout != null)
            {
                ActiveLayout.SolarAnalysisResults = saResults.Take(SmartGrid.ScalarField.GridSize);
            }

            saResults = saResults.Skip(SmartGrid.ScalarField.GridSize);


            if (ActiveMultiLayout != null)
            {
                foreach (ChromosomeLayout curChromo in ActiveMultiLayout.SubChromosomes)
                {
                    _buildings = curChromo.Gens.ToArray();
                    foreach (GenBuildingLayout building in _buildings)
                    {
                        foreach (var field in building.SideGrids)
                        {
                            field.GenerateTexture(saResults.Take(field.GridSize), "solarAnalysis");
                            saResults = saResults.Skip(field.GridSize);
                        }
                    }
                }
            }
            else
            {
                if (ActiveLayout != null) _buildings = ActiveLayout.Gens.ToArray();
                foreach (GenBuildingLayout building in _buildings)
                {
                    foreach (var field in building.SideGrids)
                    {
                        field.GenerateTexture(saResults.Take(field.GridSize), "solarAnalysis");
                        saResults = saResults.Skip(field.GridSize);
                    }
                }
            }
        }

        private string GetMd5HashFromByteArr(byte[] bArr)
        {
            using (var md5 = System.Security.Cryptography.MD5.Create())
            {
                return BitConverter.ToString(md5.ComputeHash(bArr)).Replace("-", string.Empty);
            }
        }

        private int CreateScenarioSolar(List<GenBuildingLayout> buildings, Poly2D border)
        {
            int scenarioID = -1;

            NetTopologySuite.Features.FeatureCollection featurecollection;
            System.Collections.ObjectModel.Collection<NetTopologySuite.Features.IFeature> features = new System.Collections.ObjectModel.Collection<IFeature>();

            //******
            IEnumerable<Vector3d> gridNormales = null;
            IEnumerable<Vector3d> gridPoints = null;

            /*
            var sf = new ScalarField(border.PointsFirstLoop
                        .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
                        ControlParameters.IsovistCellSize, new Vector3d(0, 0, 1));
            GGrid _smartGrid = new GGrid(sf, "solarAnalysis");
            gridNormales = Enumerable.Repeat(_smartGrid.ScalarField.Normal, _smartGrid.ScalarField.GridSize);
            gridPoints = _smartGrid.ScalarField.Grid.ToList();
            
            foreach (GenBuildingLayout building in buildings)
            {
                var bpoints = building.PolygonFromGen.PointsFirstLoop;
                var bcenter = new Vector3d(building.PolygonFromGen.PointsFirstLoop.Mean());
                bcenter.Z = building.BuildingHeight / 2;
                building.SFDerived = true;
                building.FieldTypeName = "solarAnalysis";
                building.SideGrids = new ScalarField[1 + bpoints.Length];
                building.SideGrids[0] = new ScalarField(bpoints
                    .Select(v => new Vector3d(v.X, v.Y, building.BuildingHeight)).ToArray(),
                    ControlParameters.IsovistCellSize, new Vector3d(0, 0, 1));
                for (int i = 1; i <= bpoints.Length; i++)
                {
                    var tpoints = new Vector3d[]
                        {
                            new Vector3d(bpoints[i - 1].X, bpoints[i - 1].Y, 0),
                            new Vector3d(bpoints[i - 1].X, bpoints[i - 1].Y, building.BuildingHeight),
                            new Vector3d(bpoints[i%bpoints.Length].X, bpoints[i%bpoints.Length].Y, building.BuildingHeight),
                            new Vector3d(bpoints[i%bpoints.Length].X, bpoints[i%bpoints.Length].Y, 0)
                        };
                    building.SideGrids[i] = new ScalarField(tpoints, ControlParameters.IsovistCellSize, tpoints.Mean() - bcenter);


                }
                // next we have to put all the grids together into one...
                foreach (var field in building.SideGrids)
                {
                    gridPoints = gridPoints.Concat(field.Grid);
                    gridNormales = gridNormales.Concat(Enumerable.Repeat(field.Normal, field.GridSize));
                }
            }

            */
            //_solarAnalysis = new SolarAnalysis(gridPoints, gridNormales);
            //_solarAnalysis.ClearGeometry();

            var polygons = new List<IPolygon>();
            var converter = new Pg3DNtsInterop.Pg3DNtsConverter();


            AttributesTable buildingAttr = new AttributesTable();
            buildingAttr.AddAttribute("layer", "building");
            /*     AttributesTable boundaryAttr = new AttributesTable();
                 boundaryAttr.AddAttribute("layer", "boundary");
                 AttributesTable pointAttr = new AttributesTable();
                 pointAttr.AddAttribute("layer", "buildingHeight");
                 */
            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();

            NetTopologySuite.Geometries.Polygon[] polyArray = new NetTopologySuite.Geometries.Polygon[buildings.Count + 1];
            List<NetTopologySuite.Geometries.Point> pointArray = new List<NetTopologySuite.Geometries.Point>();

            foreach (GenBuildingLayout curRect in buildings)
            {
                if (!curRect.Open)
                {
                    Poly2D p2D = curRect.PolygonFromGen;
                    var pvv = new CGeom3d.Vec_3d[1][];
                    var pv = new CGeom3d.Vec_3d[p2D.PointsFirstLoop.Length];
                    pvv[0] = pv;
                    int z = 0;
                    foreach (Vector2d v2D in p2D.PointsFirstLoop)
                        pv[z++] = new CGeom3d.Vec_3d(v2D.X, v2D.Y, curRect.BuildingHeight);

                    CGeom3d.Vec_3d[][] pvvBottom = p2D.PointsVec3D;
                    //_solarAnalysis.AddPolygon(pvvBottom, 1.0f);
                    polygons.Add(converter.PureGeomToPolygon(pvvBottom));
                    //_solarAnalysis.AddPolygon(pvv, 1.0f);
                    polygons.Add(converter.PureGeomToPolygon(pvv));

                    for (int i = 0; i < pv.Length; ++i)
                    {
                        int j = i + 1;
                        if (j >= pv.Length) j -= pv.Length;

                        var pvvSide = new CGeom3d.Vec_3d[1][];
                        var pvSide = new CGeom3d.Vec_3d[4];
                        pvvSide[0] = pvSide;
                        pvSide[0] = pvvBottom[0][i];
                        pvSide[1] = pvvBottom[0][j];
                        pvSide[2] = pv[j];
                        pvSide[3] = pv[i];

                        //_solarAnalysis.AddPolygon(pvvSide, 1.0f);
                        polygons.Add(converter.PureGeomToPolygon(pvvSide));
                    }
                }
            }

            var mpol = new MultiPolygon(polygons.ToArray());
            features.Add(new Feature(mpol, buildingAttr));

            featurecollection = new FeatureCollection(features);

            LuciCommunication.Communication2Luci.LuciAnswer luciAnswer = LuciConnection.LuciHub.createScenario("cplan_solar", featurecollection, null);
            int.TryParse(LuciConnection.LuciHub.GetOutputValue(luciAnswer, "ScID").ToString(), out scenarioID);

            /*
            string geoJSONPoly = GeoJSONWriter.Write(featurecollection);
            geoJSONPoly = geoJSONPoly.Substring(0, geoJSONPoly.Length - 1);
            string createScenario4Geo = "{'action':'create_scenario','name':'test','geometry':{'GeoJSON':{'format':'GeoJSON','geometry':" + geoJSONPoly + "}}}}}";

            JObject scenarioObj = null;
            try
            {
                scenarioObj = GlobalConnection.CL.sendAction2Lucy(createScenario4Geo);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.StackTrace);
            }

            if (scenarioObj != null)
            {
                JProperty result = ((JProperty)(scenarioObj.First));
                if (result != null)
                {
                    if (result.Value["ScID"] != null)
                        scenarioID = (int)result.Value["ScID"];
                }
            }*/

            return scenarioID;
        }

        /*
                private void mqttClient_MqttMsgPublishReceived_Solar(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
                {
                    if (e.Topic == _mqttTopicSolar && System.Text.Encoding.UTF8.GetString(e.Message) == "DONE")
                    {
                        // service results available
                        ReadLucyResultsSolar(_mqttTopicSolar);
                        CalculationType.LuciSolarResultsAvailable = true;
                    }
                }
                */

            /*
        private void ReadLucyResultsSolarOLD(string mqtt_topic)
        {
            string instanceID = "40";
            int index = mqtt_topic.LastIndexOf("/");
            if (index > 0)
                instanceID = mqtt_topic.Substring(index + 1);

            string msg = "{'action':'get_service_outputs','SObjID':" + instanceID + "}";
            JObject result = GlobalConnection.CL.sendAction2Lucy(msg);

            if (result != null)
            {
                //              byte[] barr = GlobalConnection.CL.GetBinaryAttachment(result);

                //                int l = barr.Count();

                JToken outputs = result.Value<JToken>("result").Value<JToken>("outputs");
                if (outputs != null)
                {
                    JArray radiationList = outputs.Value<JArray>("RadiationList");
                    double[] radArr = new double[radiationList.Count];
                    for (int r = 0; r < radiationList.Count; r++)
                    {
                        radArr[r] = (double)radiationList[r];
                    }

                    if (ActiveLayout != null)
                        ActiveLayout.SolarAnalysisResults = radArr;
                    else if (ActiveMultiLayout != null)
                        ActiveMultiLayout.SolarAnalysisResults = radArr;
                    //_curChromo.SolarAnalysisResults = radArr;

                    //   string output = (string)(outputs.Value<JObject>("RadiationList").Value<String>("value"));

                    //   _isovistField.AnalyseLucyResults(output);
                    // _isovistField.Finished = true;
                    // curChromo.IsovistField = _isovistField;
                }
            }
        }
        */

        public async Task ReadLuciResultsSolar(LuciCommunication.Communication2Luci.LuciAnswer? luciAnswer)
        {
            if (luciAnswer == null)
                return;

            LuciCommunication.Communication2Luci.LuciAnswer input = (LuciCommunication.Communication2Luci.LuciAnswer)luciAnswer;
            if (input.State != LuciCommunication.Communication2Luci.LuciState.Result)
                return;

//             var radiationList = (double[])LuciConnection.LuciHub.GetOutputValue(input, "RadiationList");

            var radiationList = ((JArray)LuciConnection.LuciHub.GetOutputValue(input, "RadiationList")).ToObject<double[]>();

            if (radiationList != null)
            {
                genTexture(radiationList);
//                SmartGrid.ScalarField.GenerateTexture(radiationList.Take(SmartGrid.ScalarField.GridSize), "solarAnalysis");
//                SmartGrid.GenerateTexture();
/*
                if (ActiveLayout != null)
                    ActiveLayout.SolarAnalysisResults = radiationList;
                else if (ActiveMultiLayout != null)
                    ActiveMultiLayout.SolarAnalysisResults = radiationList;*/
            }

//            _solarAnalysis.Calculate(CalculationMode.illuminance_klux);
//            genTexture(_solarAnalysis.Results);

           
        }

        # endregion

    }
}

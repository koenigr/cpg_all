﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = FitnessFunctionLayoutSingle.cs 
//  Copyright (C) 2014/10/18  7:52 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Linq;
using AForge.Genetic;
//using System.Collections.Generic;
//using CPlan.Evaluation;
//using CPlan.Geometry;

namespace CPlan.Optimization
{
    [Serializable]
    public class FitnessFunctionIsovistSingle : IFitnessFunction
    {

        //================================================================================================================================================
        /// <summary>
        /// Evaluates the fitness of a variant.
        /// For Single criteria optimization we use maximization!
        /// </summary>
        /// <param name="chromosome"> Variant to be evaluated.</param>
        /// <returns>The fitness value.</returns>
        double IFitnessFunction.Evaluate(IChromosome chromosome)
        {
          
            double fitnessValue = -123;// double.MinValue;
            ChromosomeLayout curChromo = (ChromosomeLayout)chromosome;

            //IsovistAnalysis
            var calculatorIsovistAnalysis = new CalculatorIsovistAnalysis(curChromo);
            
            // TODO :: the calculation type shall come from the function call! ::
            calculatorIsovistAnalysis.Compute(new CalculateVia() /*CalculationType.Method.Local*/, -1).Wait();

            // CM_L -> A smart method to wait until the IsovistAnalysis is finished for this chromosome (only one, which evaluate itself), 
            // before we continue with the assignement of the fitness values = labels

            //IsovistAnalysis(curChromo);
            fitnessValue = (curChromo.IsovistField.Area.Max());
            return fitnessValue;

        }

    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = FitnessFunctionMultiayout.cs 
//  Copyright (C) 2014/10/18  7:52 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
//using CPlan.Evaluation;
//using CPlan.Geometry;
//using OpenTK;

namespace CPlan.Optimization
{
    [Serializable]
    public class FitnessFunctionMultiLayout : IMultiFitnessFunction
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Private Fields
        
        //public CalculationType.Method RunVia_Solar { get; set; }
        //private IsovistAnalysis _isovistField = null;
        //private IEnumerable<double> _solarAnalysisResults = null;
        //private string _pathToHdriFile;
        private int _dimensions = 5;   

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        public int Dimensions { get { return _dimensions; } }
        public CalculateVia RunVia { get; set; }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Constructors

        //================================================================================================================================================
        public FitnessFunctionMultiLayout()
        {
            RunVia = new CalculateVia();
            //RunVia_Solar = CalculationType.Method.Local;
        }

        //================================================================================================================================================
        public FitnessFunctionMultiLayout(CalculateVia runVia)//, CalculationType.Method runVia_Solar)//, String pathToHdri)
        {
            RunVia = runVia;
            //RunVia_Solar = runVia_Solar;
        //    _pathToHdriFile = pathToHdri;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        //================================================================================================================================================
        async System.Threading.Tasks.Task<List<double>> IMultiFitnessFunction.Evaluate(IMultiChromosome chromosome)
        {
            List<double> fitnessValues = new List<double>();
            ChromosomeMultiLayout curChromo = (ChromosomeMultiLayout)chromosome;

            //CalculatorSolarAnalysis calculatorSolarAnalysis;
            CalculatorIsovistAnalysis calculatorIsovistAnalysis;

            //IsovistAnalysis
            int scId = -1;
            calculatorIsovistAnalysis = new CalculatorIsovistAnalysis(curChromo);
            // CM_L -> implement method PrepareScenario in the class CalculatorIsovistAnalysis --> scId = calculatorIsovistAnalysis.PrepareScenario (RunVia, scId) 
            calculatorIsovistAnalysis.Compute(RunVia.Isovist, scId).Wait();

            // CM_L -> A smart method to wait until the IsovistAnalysis is finished for this chromosome (only one, which evaluate itself), 
            // before we continue with the assignement of the fitness values = labels

            //------------------------------------------------------//
            // -- we use minimization !!! ---
            // -- add above number of _dimensions!

            double overlapSum = 0;
            foreach (ChromosomeLayout subChromo in curChromo.SubChromosomes)
            {
                overlapSum += Math.Abs(subChromo.Overlap());
            }
            fitnessValues.Add(Math.Abs(overlapSum));

            fitnessValues.Add((1 / curChromo.IsovistField.Area.Where(n => (!double.IsNaN(n) && n >= 0)).Max()));
            fitnessValues.Add((curChromo.IsovistField.Occlusivity.Where(n => (!double.IsNaN(n) && n >= 0)).Average()));
            fitnessValues.Add((1 / curChromo.IsovistField.Compactness.Where(n => (!double.IsNaN(n) && n >= 0)).Average()));
            fitnessValues.Add((1 / curChromo.IsovistField.MinRadial.Where(n => (!double.IsNaN(n) && n >= 0)).Average()));


            return fitnessValues;

        }

        #endregion

    }
}


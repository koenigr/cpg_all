﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = FitnessFunctionNetwork.cs 
//  Copyright (C) 2014/10/18  7:52 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using CPlan.Evaluation;
using QuickGraph;
using Tektosyne.Geometry;
using OpenTK;

namespace CPlan.Optimization
{
    [Serializable]
    public class FitnessFunctionNetwork : IMultiFitnessFunction
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Private Fields

        private int _dimensions = 2;

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        public int Dimensions { get { return _dimensions; } }
        public CalculateVia RunVia { get; set; }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        public FitnessFunctionNetwork()
        {
            RunVia = new CalculateVia();
        }

        public FitnessFunctionNetwork(CalculateVia runVia)
        {
            RunVia = runVia;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chromosome"></param>
        /// <returns></returns>
// *ac*        async System.Threading.Tasks.Task<IMultiChromosome> IMultiFitnessFunction.Evaluate(IMultiChromosome chromosome)
        async System.Threading.Tasks.Task<List<double>> IMultiFitnessFunction.Evaluate(IMultiChromosome chromosome)
        {
            CalculationType.LuciNetworkResultsAvailable = false;
            List<double> fitnessValues = new List<double>();
            InstructionTreePisa curNetwork = (InstructionTreePisa)chromosome;

            // CM_L -> For the NetworkAnalysis we don't have the service so far, but this would be the next to be implemented...
            // CM_L for later: implement method PrepareScenario in the class CalculatorNetworkAnalysis --> scId = calculatorNetworkAnalysis.PrepareScenario (RunVia, scId) 
            var calculatorNetworkAnalysis = new CalculatorNetworkAnalysis();
            calculatorNetworkAnalysis.Compute(curNetwork, RunVia.Network).Wait();


            // CM_L for later: A smart method to wait until the NetworkAnalysis is finished for this chromosome (only one, which evaluate itself), 
            // before we continue with the assignement of the fitness values = labels

            // -----------------------------------------------------------------------------------------
            // --- compute spatial relations -----------------------------------------------------------
            float worstValue = float.MaxValue;
            float maxValue = 0, sumQuiet = 0;
            int edgeIdx = -1;
            //float[] meanChoiceArr = ShortPahtesAngular.GetNormChoiceArray();
            float[] valueArr = null;
            if (ControlParameters.Choice) valueArr = curNetwork.ShortestPathes.GetChoice()[0];
            if (ControlParameters.Centrality) valueArr = curNetwork.ShortestPathes.GetCentralityMetric()[0];

            double sumValue = 0, meanValue = 0;
            //double dist;

            for (int i = 0; i < valueArr.Length; i++)
            {
                float value = valueArr[i];
                sumValue += value;
                if (maxValue < value)
                {
                    maxValue = value;
                    edgeIdx = i;
                }
            }

            if (valueArr.Length > 0)
                meanValue = sumValue / (valueArr.Length); // -1
            else meanValue = 0;

            List<LineD> qLines = new List<LineD>();
            foreach (UndirectedEdge<Vector2d> edge in curNetwork.Network.Edges) qLines.Add(new LineD(edge.Source.ToPointD(), edge.Target.ToPointD()));
            
            //double maxDist = Math.Sqrt(Math.Pow(curNetwork.Border.BoundingBox().Height, 2) + Math.Pow(curNetwork.Border.BoundingBox().Width, 2));
            //LineD bestEdge = qLines[edgeIdx];
            //// --- distance of the "max value segment" to a defined point:
            //Vector2d centralPoint = new Vector2d(curNetwork.Border.Left + 75, curNetwork.Border.Top - 50);
            //dist = GeoAlgorithms2D.DistancePointToLine(centralPoint, bestEdge.Start.ToVector2D(), bestEdge.End.ToVector2D(), new Vector2d());
            //float weight = Convert.ToSingle(Math.Pow(1 - (dist / maxDist), 2));
            //maxValue *= weight;

            // --- inside the rectangle? ---
            RectD quietArea = new RectD(150, 0, 150, 90); // the quiet area
            LineD[] streets = qLines.ToArray();

            int counter = 0;
            for (int i = 0; i < streets.Count(); i++)
            {
                LineD segment = streets[i];
                //                   if (quietArea.Contains(segment.Start) || quietArea.Contains(segment.End))
                {
                    //maxValue -= ValueArr[i];
                    sumQuiet += valueArr[i]; //;maxValue - ValueArr[i];
                    counter++;
                }
            }
            sumQuiet = sumQuiet / (counter * 10);
            //maxValue += sumQuiet;

            //return maxValue; //sumValue;// meanValue;// maxValue;//1 - (dist / maxDist);//  *ChoiceArr.Length;//1 - meanChoice;//meanChoice;//
            // prepare fitness values for minimization!
            if (maxValue > 0)
                fitnessValues.Add(1 / maxValue);
            else fitnessValues.Add(worstValue);
            //fitnessValues.Add(dist);
            if (sumQuiet > 0)
                fitnessValues.Add(sumQuiet);
            else fitnessValues.Add(worstValue);
           
            // nuber of elements
            //fitnessValues.Add(1/(float)counter);

            //if (RunVia == CalculationType.Method.Local)
            //    return EvaluateLocal(chromosome);
            //else if (RunVia == CalculationType.Method.Luci)
            //    return EvaluateLucy();//chromosome);
            //else
            //    return EvaluateLocal(chromosome);

            return fitnessValues;
        }

        # endregion

    }
}

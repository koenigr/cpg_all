﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = FitnessFunctionNetworkSingle.cs 
//  Copyright (C) 2014/10/18  7:52 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using AForge.Genetic;
using CPlan.Evaluation;
using CPlan.Geometry;
using QuickGraph;
using Tektosyne.Geometry;
using OpenTK;

namespace CPlan.Optimization
{
    [Serializable]
    public class FitnessFunctionNetworkSingle : IFitnessFunction
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Private Fields

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        public CalculationType.Method RunVia { get; set; }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        public FitnessFunctionNetworkSingle()
        {
            RunVia = CalculationType.Method.Local;
        }

        public FitnessFunctionNetworkSingle(CalculationType.Method runVia)
        {
            RunVia = runVia;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        double IFitnessFunction.Evaluate(IChromosome chromosome)
        {
            CalculationType.LuciNetworkResultsAvailable = false;
            CalculatorNetworkAnalysis calculatorNetworkAnalysis;
            InstructionTreeSingle curNetwork = (InstructionTreeSingle)chromosome;

            calculatorNetworkAnalysis = new CalculatorNetworkAnalysis();
            calculatorNetworkAnalysis.Compute(curNetwork, RunVia).Wait();

            // -----------------------------------------------------------------------------------------
            // --- compute spatial relations -----------------------------------------------------------
            double fitnessValue = -123;// double.MinValue;
            float maxValue = 0, sumQuiet = 0;
            int edgeIdx = -1;
            //float[] meanChoiceArr = ShortPahtesAngular.GetNormChoiceArray();
            float[] ValueArr = null;
            if (ControlParameters.Choice) ValueArr = curNetwork.ShortestPathes.GetChoice()[0];
            if (ControlParameters.Centrality) ValueArr = curNetwork.ShortestPathes.GetCentralityMetric()[0];

            double sumValue = 0, meanValue = 0;
            double dist;

            if (!curNetwork.ShortestPathes.Connected) // something is wrong with the graph - probably unconnected.
            {
                //return null;
                maxValue = 0.00001f;
                sumQuiet = Single.MaxValue / 2;
                dist = Single.MaxValue / 2;
            }
            else   // -- if graph is connected, calculate the fitness values ---
            {
                for (int i = 0; i < ValueArr.Count(); i++)
                {
                    float value = ValueArr[i];
                    sumValue += value;
                    if (maxValue < value)
                    {
                        maxValue = value;
                        edgeIdx = i;
                    }
                }

                if (ValueArr.Length > 0)
                    meanValue = sumValue / (ValueArr.Length); // -1
                else meanValue = 0;
                curNetwork.ShortestPathes = curNetwork.ShortestPathes;

                List<LineD> qLines = new List<LineD>();
                foreach (UndirectedEdge<Vector2d> edge in curNetwork.Network.Edges) qLines.Add(new LineD(edge.Source.ToPointD(), edge.Target.ToPointD()));
                // -- include spatial relations --
                double maxDist = Math.Sqrt(Math.Pow(curNetwork.Border.BoundingBox().Height, 2) + Math.Pow(curNetwork.Border.BoundingBox().Width, 2));
                LineD bestEdge = qLines[edgeIdx]; //origEdges[edgeIdx];
                //LineD bestEdge = curChromo.Network.ToLines().ToList()[edgeIdx];

                // distance of the "max value segment" to a defined point:
                Vector2d centralPoint = new Vector2d(curNetwork.Border.Left + 75, curNetwork.Border.Top - 50);
                dist = GeoAlgorithms2D.DistancePointToLine(centralPoint, bestEdge.Start.ToVector2D(), bestEdge.End.ToVector2D());
                float weight = Convert.ToSingle(Math.Pow(1 - (dist / maxDist), 2));
                maxValue *= weight;

                // --- inside the rectangle? ---
                RectD quietArea = new RectD(150, 0, 150, 90); // the quiet area
                LineD[] streets = qLines.ToArray();
                //LineD[] streets = curChromo.Network.ToLines();

                int counter = 0;
                for (int i = 0; i < streets.Count(); i++)
                {
                    LineD segment = streets[i];
                    if (quietArea.Contains(segment.Start) || quietArea.Contains(segment.End))
                    {
                        //maxValue -= ValueArr[i];
                        //sumQuiet += maxValue - ValueArr[i];
                        sumQuiet += ValueArr[i];
                        counter++;
                    }
                }
                sumQuiet = sumQuiet / (counter * 1);
                maxValue -= sumQuiet;
                //fitnessValue = (1 / maxValue);// + (1 - 1/sumQuiet));
                fitnessValue = maxValue;
            }

            //return maxValue; //sumValue;// meanValue;// maxValue;//1 - (dist / maxDist);//  *ChoiceArr.Length;//1 - meanChoice;//meanChoice;//

            //fitnessValues.Add(dist);
            //fitnessValues.Add(sumQuiet);

            return fitnessValue;

        }

        # endregion
    }
}

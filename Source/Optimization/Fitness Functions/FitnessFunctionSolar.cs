﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = FitnessFunctionSolar.cs 
//  Copyright (C) 2015/07/29  17:14 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DirectSunLightNet;

namespace CPlan.Optimization
{
    [Serializable]
    public class FitnessFunctionSolar : IMultiFitnessFunction
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Private Fields

        //private Object thisLock = new Object();
        //private string _mqttTopic;     
        private const int _dimensions = 2;
        private string _pathToHdriFile;

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        public int Dimensions { get { return _dimensions; } }
        public CalculateVia RunVia { get; set; }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        public FitnessFunctionSolar(String pathToHdri)
        {
            RunVia = new CalculateVia();
            _pathToHdriFile = pathToHdri;
        }

        public FitnessFunctionSolar(CalculateVia runVia, String pathToHdri)
        {
            RunVia = runVia;
            _pathToHdriFile = pathToHdri;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        async Task<List<double>> IMultiFitnessFunction.Evaluate(IMultiChromosome chromosome)
        {
            List<double> fitnessValues = new List<double>();
            ChromosomeLayout curChromo = (ChromosomeLayout)chromosome;

            int scId = -1;
            //SolarAnalysis
            var calculatorSolarAnalysis = new CalculatorSolarAnalysis(curChromo, _pathToHdriFile);
            // CM_L -> implement method PrepareScenario in the class CalculatorSolarAnalysis --> scId = calculatorSolarAnalysis.PrepareScenario (RunVia, scId) 
            await calculatorSolarAnalysis.Compute(RunVia, scId);

            //------------------------------------------------------//
            // --- minimization ------------------------------------//
            // --- add above number of _dimensions! ----------------//
            //------------------------------------------------------//

            //fitnessValues.Add(Math.Abs(curChromo.Overlap()));

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            double solarValueNorth = 0;
            double solarValueSouth = 0;
            foreach (GenBuildingLayout building in curChromo.Gens)
            {
                for (int s = 1; s < building.SideGrids.Count(); s++) // 0 = rooftop
                {
                    if (building.SideGrids[s] != null)
                    {
                        Vector3d normal = building.SideGrids[s].Normal;
                        if (normal.Y < 0)
                            solarValueNorth += building.SideGrids[s].SolarResults.Where(n => (!double.IsNaN(n) && n >= 0)).Max() * Math.Abs(normal.Y);
                        else
                            solarValueSouth += building.SideGrids[s].SolarResults.Where(n => (!double.IsNaN(n) && n >= 0)).Max() * Math.Abs(normal.Y);
                    }
                    else
                        Console.WriteLine("building sides error...");
                }
            }

            //if (solarValueNorth > 0)
            //    solarValueNorth = 1 / solarValueNorth; // more sun on the north
            //else solarValueNorth = 0;

            if (solarValueSouth > 0)
                solarValueSouth = 1 / solarValueSouth; // more sun on the south
            else solarValueSouth = 0;

            fitnessValues.Add(solarValueNorth); 
            fitnessValues.Add(solarValueSouth); 

            return fitnessValues;
        }

        # endregion
       
    }
}



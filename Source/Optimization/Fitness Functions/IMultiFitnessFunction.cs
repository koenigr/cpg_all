﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = IMultiFitnessFunction.cs 
//  Copyright (C) 2014/10/18  1:05 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System.Collections.Generic;
using System.Threading.Tasks;

namespace CPlan.Optimization
{
    /// <summary>
    /// Multi-Fitness function interface.
    /// </summary>
    /// 
    /// <remarks>The interface should be implemented by all fitness function
    /// classes, which are supposed to be used for calculation of chromosomes
    /// fitness values. All fitness functions should return positive (<b>greater
    /// then zero</b>) values, which indicates how good is the evaluated chromosome - 
    /// the greater the value, the better the chromosome.
    /// </remarks>
    public interface IMultiFitnessFunction
    {
        /// <summary>
        /// Evaluates chromosome.
        /// </summary>
        /// 
        /// <param name="chromosome">Chromosome to evaluate.</param>
        /// 
        /// <returns>Returns chromosome's fitness values.</returns>
        ///
        /// <remarks>The method calculates fitness values of the specified
        /// chromosome.</remarks>
        ///
        Task<List<double>> Evaluate(IMultiChromosome multi_Chromosome);
        /* *ac*
         Task<IMultiChromosome> Evaluate(IMultiChromosome multi_Chromosome);
         */

        int Dimensions { get; }

        CalculateVia RunVia { get; set; }
    }
}

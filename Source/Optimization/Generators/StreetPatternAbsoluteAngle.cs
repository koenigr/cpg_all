﻿using OpenTK;
using QuickGraph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPlan.Geometry;
using static CPlan.Geometry.GeoAlgorithms2D;

namespace CPlan.Optimization
{
    using StreetNetwork = UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>;

    public class StreetPatternAbsoluteAngle
    {
        private const double SnappingDistance = 0.1;

        public StreetNetwork GrowStreetPattern(InstructionTreePisaAbs instructionTree, bool addJunctions = true, bool snapping = true)
        {
            var network = Growing(instructionTree);
            if(addJunctions) { AddJunctions(network); }
            if(snapping) { Snapping(network); }
            return network;
        }

        /// <summary>
        /// Grows the graph based on a instruction Tree with absolut angles (from 0 to 360 degree, 180 degree is straight forward)
        /// </summary>
        /// <param name="instructionTree">The tree witch have to be painted</param>
        /// <returns>returns the grown network</returns>
        private StreetNetwork Growing(InstructionTreePisaAbs instructionTree)
        {
            var network = new StreetNetwork(false);
            InstructionNodeAbsolute firstNode = null;

            var nodeDictionary = new Dictionary<long, InstructionNodeAbsolute>(instructionTree.Nodes.Count());
            foreach(var node in instructionTree.Nodes)
            {
                node.Closed = false;
                if (node.Index == 0)
                {
                    firstNode = node;
                }
            }
            //InstructionTree need to have a firstNode with index 0
            if(firstNode == null)
            {
                throw new ArgumentException("instructionTree does not contain a node with index 0");
            }

            //dictionary with ParentID points to all his child Nodes
            var parentToChild = new Dictionary<long, List<InstructionNodeAbsolute>>(instructionTree.Nodes.Count());
            foreach (var node in instructionTree.Nodes)
            {
                if(!parentToChild.ContainsKey(node.Parent.Index))
                {
                    parentToChild[node.Parent.Index] = new List<InstructionNodeAbsolute>();
                }
                parentToChild[node.Parent.Index].Add(node);
            }

            var nodesToVisit = new Queue<InstructionNodeAbsolute>();

            //Dictionary with nodeID that points on absolut angle and Vector of posX and poxY
            var visitedNodes = new Dictionary<long, Tuple<double, Vector2d>>(instructionTree.Nodes.Count());
            visitedNodes[firstNode.Index] = new Tuple<double, Vector2d>(firstNode.Angle, instructionTree.InitalEdge.Source);

            //Special case first node (does start at pos 0,0)
            var first = firstNode.Angle % 360.0;
            var firstRAD = first / 180 * Math.PI;
            var rVector = new Vector2d(Math.Cos(firstRAD), Math.Sin(firstRAD));
            var childP = new Vector2d(0,0) + (rVector * firstNode.Length);

            network.AddVerticesAndEdge(new UndirectedEdge<Vector2d>(new Vector2d(0, 0), childP));
            visitedNodes[firstNode.Index] = new Tuple<double, Vector2d>(first, childP);
            firstNode.Closed = true;
            foreach (var child in parentToChild[firstNode.Index])
            {
                nodesToVisit.Enqueue(child);
            }

            //Iterate over all notes to visit and insert them with absolut angles and position into the network
            while (nodesToVisit.Count > 0)
            {
                var actualNode = nodesToVisit.Dequeue();
                if(!actualNode.Closed)
                {
                    if (visitedNodes.ContainsKey(actualNode.Parent.Index))
                    {
                        var parentNode = visitedNodes[actualNode.Parent.Index];
                        var relativeAngle = (360.0 + (parentNode.Item1 - 180.0) + actualNode.Angle) % 360;
                        var relativeAngleRad = (relativeAngle / 180) * Math.PI;
                        var rotationVector = new Vector2d(Math.Cos(relativeAngleRad), Math.Sin(relativeAngleRad));
                        var childPoint = parentNode.Item2 + (rotationVector * actualNode.Length);

                        network.AddVerticesAndEdge(new UndirectedEdge<Vector2d>(parentNode.Item2, childPoint));
                        visitedNodes[actualNode.Index] = new Tuple<double, Vector2d>(relativeAngle, childPoint);

                        if(parentToChild.ContainsKey(actualNode.Index))
                        {
                            foreach (var child in parentToChild[actualNode.Index])
                            {
                                nodesToVisit.Enqueue(child);
                            }
                        }
                    }
                    actualNode.Closed = true;
                }
            }
            return network;
        }

        /// <summary>Determines if the two given vertices should snap together (using <see cref="SnappingDistance"/>).</summary>
        private static bool DoSnap(Vector2d a, Vector2d b) => (a - b).LengthSquared <= SnappingDistance * SnappingDistance;

        /// <summary>Adds junctions at places, where streets intersect without a junction.</summary>
        /// <remarks>The performance could possibly be improved by implementing the Bentley–Ottmann sweep line algorithm.</remarks>
        /// <param name="graph"></param>
        public static void AddJunctions(StreetNetwork graph)
        {
            var stack = new Stack<UndirectedEdge<Vector2d>>(graph.Edges);
            var deleted = new HashSet<UndirectedEdge<Vector2d>>();
            while (stack.Any())
            {
                var current = stack.Pop();
                //Ignore edges, that were already removed from the graph
                if (deleted.Contains(current))
                {
                    deleted.Remove(current);
                    continue;
                }
                //Find an intersection between the current street and any street, that is not yet fully processed
                var intersection = stack
                    .Where(edge => !deleted.Contains(edge) && !DoSnap(current.Source, edge.Source) &&
                        !DoSnap(current.Source, edge.Target) && !DoSnap(current.Target, edge.Source) && !DoSnap(current.Target, edge.Target))
                    .Select(edge => Tuple.Create(edge, LineIntersection(current.Source, current.Target, edge.Source, edge.Target, type => type != IntersectionType.NoIntersection)))
                    .FirstOrDefault(i => i.Item2.HasValue);
                if (intersection != null)
                {
                    //Found intersection: Remove the intersecting edges and add 4 new edges to create the new junction
                    graph.RemoveEdge(current);
                    graph.RemoveEdge(intersection.Item1);
                    deleted.Add(intersection.Item1);
                    graph.AddVertex(intersection.Item2.Value);
                    var newEdges = new[]{
                        new UndirectedEdge<Vector2d>(current.Source, intersection.Item2.Value),
                        new UndirectedEdge<Vector2d>(intersection.Item2.Value, current.Target),
                        new UndirectedEdge<Vector2d>(intersection.Item1.Source, intersection.Item2.Value),
                        new UndirectedEdge<Vector2d>(intersection.Item2.Value, intersection.Item1.Target)
                    };
                    foreach(var edge in newEdges.Where(edge => (edge.Target - edge.Source).LengthSquared > SnappingDistance * SnappingDistance))
                    {
                        graph.AddEdge(edge);
                        stack.Push(edge);
                    }
                }
            }
        }

        /// <summary>Tries to combine junctions, that are very close to each other.</summary>
        /// <remarks>Complexity O(n choose 2), because every junction is compared with every other junction.</remarks>
        /// <param name="graph"></param>
        public static void Snapping(StreetNetwork graph)
        {
            var stack = new Stack<Vector2d>(graph.Vertices);
            var deleted = new HashSet<Vector2d>();
            while (stack.Any())
            {
                //Ignore nodes, that were already removed from the graph
                var current = stack.Pop();
                if (deleted.Contains(current))
                {
                    deleted.Remove(current);
                    continue;
                }
                foreach(var snap in stack.Where(node => DoSnap(current, node)))
                {
                    deleted.Add(snap);
                    foreach(var edge in graph.AdjacentEdges(snap))
                    {
                        var newEdge = (edge.Source == snap ?
                            new UndirectedEdge<Vector2d>(current, edge.Target) :
                            new UndirectedEdge<Vector2d>(edge.Source, current));
                        if (!DoSnap(newEdge.Source, newEdge.Target))
                        {
                            //Don't add edges, which did connect snap and current
                            graph.AddEdge(newEdge);
                        }
                    }
                    //This method call also removes every adjacent edge from the graph
                    graph.RemoveVertex(snap);
                }
            }
        }
    }
}

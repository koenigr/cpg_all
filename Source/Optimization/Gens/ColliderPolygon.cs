﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = ColliderPolygon.cs 
//  Copyright (C) 2015/03/24  17:41 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using CPlan.Geometry;
using OpenTK;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;
using FarseerPhysics.Common.Decomposition;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Dynamics;

namespace CPlan.Optimization
{

    public class ColliderPolygon : Poly2D
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Private Fields

        private Body _body;
        private Fixture fixture;

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        /// <summary>
        /// Rotaion in radians.
        /// </summary>
        public float Rotation
        {
            get;
            set;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        public ColliderPolygon(World world, Poly2D basicShape)
            : base(basicShape)
        {
            Microsoft.Xna.Framework.Vector2 position = new Microsoft.Xna.Framework.Vector2(Convert.ToSingle(basicShape.Center.X), Convert.ToSingle(basicShape.Center.Y));
            _body = new Body(world, position); //null;//world.CreateBody(basicShape);

            // --- define Box2d body ----------------
            _body.BodyType = BodyType.Dynamic;
            _body.SleepingAllowed = true;
            _body.IgnoreCCD = true;


            // Define another shape for our dynamic body.
            Poly2D tmpShape = basicShape.Clone();
            tmpShape.Position = new Vector2d(0, 0);
            Vertices polyVertis = new Vertices();
            foreach (Vector2d curVect in tmpShape.PointsFirstLoop)
                polyVertis.Add(new Microsoft.Xna.Framework.Vector2(Convert.ToSingle(curVect.X), Convert.ToSingle(curVect.Y)));

            // decompose to a concave polygon
            List<Vertices> polyVertisTriangulated = Triangulate.ConvexPartition(polyVertis, TriangulationAlgorithm.Earclip);
            PolyClipError err;
            Vertices unions = polyVertisTriangulated[0];
            for (int i = 1; i < polyVertisTriangulated.Count; ++i)
            {
                Vertices vertiSet = polyVertisTriangulated[i];
                YuPengClipper.Union(unions, vertiSet, out err);
            }

            float density = 1.0f;
            PolygonShape shapeDef = new PolygonShape(unions, density);
            //_body.FixedRotation = true;
            fixture = _body.CreateFixture(shapeDef);
            _body.Friction = 0.0f;
            _body.AngularDamping = 0.01f;
            _body.LinearDamping = 0.01f;

            // Now tell the dynamic body to compute it's mass properties based on its shape.
            //_body.SetMassFromShapes();
            // BodyCur.SetMass( new MassData());   

        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        //===============================================================================================================
        public void UpdatePos()
        {
            // --- rotation ----------------------------------------------------------------
            float diffRotate = _body.Rotation - Rotation;
            this.Rotate(diffRotate);
            Rotation = _body.Rotation;

            // --- damp rotation ---     
            //float damping = 0.5f;
            //float dampedRotate = diffRotate*damping;
            //base.Poly.Rotate(dampedRotate);
            //float newRotation = _body.Rotation - (diffRotate - dampedRotate);
            //_rotation = newRotation;
            //_body.Rotation = newRotation;

            // ---------------------------------------------------------------------------------------------------
            // --- distribute the movement vector to proportion and position change ---
            float dampMove = 1.0f; // large one increase the move vector, smaller decrease it.
            Vector2d moveVect = new Vector2d(_body.Position.X, _body.Position.Y) - this.Position;
            //Vector2 testMoveVect = _body.LinearVelocity;
            Vector2d damMoveVect = moveVect * dampMove;
            this.Position += damMoveVect;
            _body.Position = new Microsoft.Xna.Framework.Vector2(Convert.ToSingle(this.Position.X), Convert.ToSingle(this.Position.Y));

            // ---------------------------------------------------------------------------------------------------
            // --- test new shape --------------------------------------------------------------------------------
            // Define another box shape for our dynamic body.
            // if (rnd.NextDouble() < 0.1)
            //    Poly.PointsFirstLoop[0].X += (rnd.NextDouble() - 0.5)*0.2;

            Poly2D tmpShape = this.Clone();
            tmpShape.Position = new Vector2d(0, 0);
            tmpShape.Rotate(-Rotation);
            Poly2D tmpShapeBack = tmpShape.Clone();

            // --- apply scaling - use the rotated norm body ---
            // --- proportion change ---

            //Vector2d[] boundRect = GeoAlgorithms2D.MinimalBoundingRectangle(tmpShape.PointsFirstLoop.ToList());
            //Vector2d diagonal = boundRect[3] - boundRect[0];
            double area = GeoAlgorithms2D.Area(tmpShape.PointsFirstLoop.ToList());
            Vector2d rotatedFoce = GeoAlgorithms2D.RotateVector(-Rotation, GeoAlgorithms2D.Centroid(tmpShape.PointsFirstLoop.ToList()), moveVect);

            // --- find max values -----------
            Vector2d centre = tmpShape.Center;
            double maxDx = 0;// Math.Abs(diagonal.X / 2);
            double maxDy = 0;//Math.Abs(diagonal.Y / 2); 
            double minSide = 0.2;
            foreach (Vector2d curVect in tmpShape.PointsFirstLoop)
            {
                Vector2d curRelV = curVect - centre;
                if (Math.Abs(curRelV.X) > maxDx) maxDx = Math.Abs(curRelV.X);
                if (Math.Abs(curRelV.Y) > maxDy) maxDy = Math.Abs(curRelV.Y);
            }

            double forceScale = 0.0;//0.2;
            if (rotatedFoce.Y > rotatedFoce.X)
            {
                for (int i = 0; i < tmpShape.PointsFirstLoop.Length; ++i)
                {
                    Vector2d curVect = tmpShape.PointsFirstLoop[i];
                    Vector2d curRelV = curVect - centre;
                    Vector2d fact = new Vector2d(Math.Abs(curRelV.X / maxDx), Math.Abs(curRelV.Y / maxDy));

                    int dirX = Math.Sign(curRelV.X);
                    int dirY = Math.Sign(curRelV.Y);
                    Vector2d dir = new Vector2d(dirX, dirY * -1);
                    double remainingSide = forceScale * rotatedFoce.X * maxDx * 2;
                    if (minSide < remainingSide)
                    {
                        double newSideX = area / (maxDy * 2 + forceScale * rotatedFoce.Y * 2);
                        double deltaSideX = -1 * newSideX - maxDx * 2;
                        tmpShape.PointsFirstLoop[i].X += (rotatedFoce.Y * fact.X * dir.X * forceScale + deltaSideX * fact.X * dir.X);
                        tmpShape.PointsFirstLoop[i].Y += (rotatedFoce.Y * fact.Y * dir.Y * forceScale);
                    }
                    else tmpShape = tmpShapeBack.Clone();
                }
            }
            else
            {
                for (int i = 0; i < tmpShape.PointsFirstLoop.Length; ++i)
                {
                    Vector2d curVect = tmpShape.PointsFirstLoop[i];
                    Vector2d curRelV = curVect - centre;
                    Vector2d fact = new Vector2d(Math.Abs(curRelV.X / maxDx), Math.Abs(curRelV.Y / maxDy));

                    int dirX = Math.Sign(curRelV.X);
                    int dirY = Math.Sign(curRelV.Y);
                    Vector2d dir = new Vector2d(dirX * -1, dirY);
                    double remainingSide = forceScale * rotatedFoce.Y * maxDy * 2;
                    if (minSide < remainingSide)
                    {
                        double newSideY = area / (maxDx * 2 + forceScale * rotatedFoce.X * 2);
                        double deltaSideY = -1 * newSideY - maxDy * 2;
                        tmpShape.PointsFirstLoop[i].X += (rotatedFoce.X * fact.X * dir.X * forceScale);
                        tmpShape.PointsFirstLoop[i].Y += (rotatedFoce.X * fact.Y * dir.Y * forceScale + deltaSideY * fact.Y * dir.Y);
                    }
                    else tmpShape = tmpShapeBack.Clone();
                }
            }

            //UpdateFixture(tmpShape);
            
            // ---------------------------------------------------------------------------------------------------
            Vertices polyVertis = new Vertices();
            foreach (Vector2d curVect in tmpShape.PointsFirstLoop)
            {
                polyVertis.Add(new Microsoft.Xna.Framework.Vector2(Convert.ToSingle(curVect.X), Convert.ToSingle(curVect.Y)));
            }

            // ---------------------------------------------------------------------------------------------------
            float density = 1.0f;
            PolygonShape shapeDef = new PolygonShape(polyVertis, density);
            //_body.FixedRotation = true;
            _body.DestroyFixture(fixture);
            fixture = _body.CreateFixture(shapeDef);
            // ---------------------------------------------------------------------------------------------------

            // --- tempPoly back to original ---
            tmpShapeBack = tmpShape.Clone();
            tmpShapeBack.Position = this.Center;
            tmpShapeBack.Rotate(Rotation);
            this.Points = tmpShapeBack.Points;
        }

        public void UpdateFixture(World world, Poly2D newShape)//, float rotation)
        {
            //float diffRotate = _body.Rotation - rotation;
            //this.Rotate(diffRotate);
            Rotation = 0;
            _body.Rotation = 0;

            Microsoft.Xna.Framework.Vector2 position = new Microsoft.Xna.Framework.Vector2(Convert.ToSingle(newShape.Center.X), Convert.ToSingle(newShape.Center.Y));
            _body.Position = position;
            //_body = new Body(world, position); //null;//world.CreateBody(basicShape);

            //// Define another shape for our dynamic body.
            Poly2D tmpShape = newShape.Clone();
            tmpShape.Position = new Vector2d(0, 0);
            Vertices polyVertis = new Vertices();
            foreach (Vector2d curVect in tmpShape.PointsFirstLoop)
                polyVertis.Add(new Microsoft.Xna.Framework.Vector2(Convert.ToSingle(curVect.X), Convert.ToSingle(curVect.Y)));

            //// decompose to a concave polygon
            //List<Vertices> polyVertisTriangulated = Triangulate.ConvexPartition(polyVertis, TriangulationAlgorithm.Earclip);
            //PolyClipError err;
            //Vertices unions = polyVertisTriangulated[0];
            //for (int i = 1; i < polyVertisTriangulated.Count; ++i)
            //{
            //    Vertices vertiSet = polyVertisTriangulated[i];
            //    YuPengClipper.Union(unions, vertiSet, out err);
            //}

            float density = 1.0f;
            PolygonShape shapeDef = new PolygonShape(polyVertis, density); //(unions, density);
            //_body.FixedRotation = true;
            _body.DestroyFixture(fixture);
            fixture = _body.CreateFixture(shapeDef);
            _body.Friction = 0.0f;
            _body.AngularDamping = 0.01f;
            _body.LinearDamping = 0.01f;

            Points = newShape.Points;
        }

        # endregion

    }
}

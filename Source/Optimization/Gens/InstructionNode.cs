﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = InstructionNode.cs 
//  Copyright (C) 2014/10/18  7:52 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using OpenTK;
using System;
using Tektosyne.Geometry;

namespace CPlan.Optimization
{
    [Serializable]
    public class InstructionNode //: GPTreeNode
    {

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        /// <summary>
        /// Gets or sets the link to the parent node
        /// </summary>
        public Int64 ParentIndex
        {
            get;
            set;
        }

        ///// <summary>
        ///// Gets or sets a parent point.
        ///// Use this only, if there is no parent index available. This is usually only the case for the initial segments.
        ///// </summary>
        //public Vector2d ParentPoint
        //{
        //    get;
        //    set;
        //}

        /// <summary>
        /// Gets or sets the attribute index.
        /// </summary>
        public Int64 Index
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the position/coordinates of the node.
        /// </summary>
        public Vector2d Position
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the position/coordinates of the node.
        /// </summary>
        //public Vector2d Position2d
        //{
        //    get;
        //    set;
        //}

        /// <summary>
        /// Indicates if the further growth of a node is closed, if considered as parent.
        /// </summary>
        public bool Closed
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the attribute for angle.
        /// </summary>
        /// <value>(float) optimal area value.</value>
        public virtual float Angle
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the attribute for length.
        /// </summary>
        public float Length
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value for maximum connectivity - the maximum number of edges to or from a node.
        /// </summary>
        public float MaxConnectivity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value for minimum distance to other points or lines.
        /// </summary>
        public float MinDistance
        {
            get;
            set;
        }

        /// <summary>
        /// Stores a random decision during the street growth for reproduction.
        /// </summary>
        public int Divider
        {
            get;
            set;
        }


        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        # region Constructors

        public InstructionNode()
        {
            Index = -1;
            Closed = false;
            Divider = -1;
        }
        /// <summary>
        /// Initializes a new instance of the Node type with a specified index.
        /// </summary>
        /// <param name="idx">Index of the node.</param>
        public InstructionNode(Int64 idx)
        {
            //this.Children = new List<InstructionNode>();
            Index = idx;
            Closed = false;
            Divider = -1;
        }

        /// <summary>
        /// Initializes a new instance of the Node type with a specified index.
        /// </summary>
        /// <param name="idx">Index of the node.</param>
        /// <param name="angle">Angle deviation from regular division.</param>
        /// <param name="length">Length of the new street segment.</param>
        public InstructionNode(long idx, float angle, float length)
        {
            //this.Children = new List<InstructionNode>();
            Index = idx;
            Angle = angle;
            Length = length;
            Closed = false;
            Divider = -1;
        }

  
        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="idx">Index of the node.</param>
        public InstructionNode(InstructionNode source)
        {
            Index = source.Index;
            Position = new Vector2d(source.Position.X, source.Position.Y);
            Angle = source.Angle;
            Length = source.Length;
            Closed = source.Closed;
            MaxConnectivity = source.MaxConnectivity;
            ParentIndex = source.ParentIndex;
            Divider = source.Divider;
            //ParentPoint = source.ParentPoint;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        /// <summary>
        /// Clone the tree node.
        /// </summary>
        /// <returns>Returns exact clone of the node.</returns>
        public object Clone()
        {
            InstructionNode clone = new InstructionNode(this);
            return clone;
        }

        # endregion

    }
}

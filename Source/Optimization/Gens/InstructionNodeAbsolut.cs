﻿using OpenTK;
using System;

namespace CPlan.Optimization
{
    [Serializable]
    public class InstructionNodeAbsolute
    {
        /// <summary>
        /// Gets or sets the attribute index.
        /// </summary>
        public long Index { get; set; }

        /// <summary>
        /// Reference of the parent node
        /// </summary>
        public InstructionNodeAbsolute Parent { get; set; }

        /// <summary>
        /// Angle in degrees (0 - 360).
        /// </summary>
        public float Angle { get; set; }

        /// <summary>
        /// Gets or sets the attribute for length.
        /// </summary>
        public float Length { get; set; }

        /// <summary>
        /// Indicates if the further growth of a node is closed, if considered as parent.
        /// </summary>
        public bool Closed { get; set; }

        /// <summary>
        /// Initializes a new instance of the Node type with a specified index.
        /// </summary>
        /// <param name="index">the external index</param>
        public InstructionNodeAbsolute(long index)
        {
            Index = index;
            Closed = false;
            Length = 0;
            Angle = 0;
        }

        public InstructionNodeAbsolute(long index, InstructionNodeAbsolute parent, float angle, float length)
        {
            Index = index;
            Parent = parent;
            Angle = angle;
            Length = length;
            Closed = false;
        }

        /// <summary>
        /// Indicates if the further growth of a node is closed, if considered as parent.
        /// </summary>
        public InstructionNode AsInstructionNode()
        {
            var node = new InstructionNode(Index, Angle, Length);
            node.Closed = Closed;
            node.ParentIndex = Parent.Index;
            return node;
        }
    }
}

﻿using OpenTK;
using System;

namespace CPlan.Optimization
{
    [Serializable]
    public class InstructionNodeAbsolute: InstructionNode

    {
    /// <summary>
    /// Gets or sets the attribute index.
    /// </summary>

    /// <summary>
    /// Reference of the parent node
    /// </summary>
    public InstructionNodeAbsolute Parent { get; set; }

    /// <summary>
    /// Angle in degrees (0 - 360).
    /// </summary>
    public override float Angle { get; set; }

    /// <summary>
    /// Gets or sets the attribute for length.
    /// </summary>


    /// <summary>
    /// Indicates if the further growth of a node is closed, if considered as parent.
    /// </summary>


    /// <summary>
    /// extended tree structure for parcels on the left side of the street segment
    /// </summary>
    public Geometry.ReversedSTree.ExtendedBranch LeftParcels { set; get; }

    /// <summary>
    ///  extended tree structure for parcels on the right side of the street segment
    /// </summary>
    public Geometry.ReversedSTree.ExtendedBranch RightParcels { set; get; }

        /// <summary>
        /// Initializes a new instance of the Node type with a specified index.
        /// </summary>
        /// <param name="index">the external index</param>
      
    public InstructionNodeAbsolute(int index):base(index)
    {
        Index = index;
        Closed = false;
        Length = 0;
        Angle = 0;
    }

     public InstructionNodeAbsolute(int idx, float angle, float length)
        {
            //this.Children = new List<InstructionNode>();
            Index = idx;
            Angle = angle;
            Length = length;
            Closed = false;
            Divider = -1;
        }
        public InstructionNodeAbsolute(int index, InstructionNodeAbsolute parent, float angle, float length)
    {
        Index = index;
        Parent = parent;
        Angle = angle;
        Length = length;
        Closed = false;
        
    }

 
    /// <summary>
    /// Indicates if the further growth of a node is closed, if considered as parent.
    /// </summary>
    public InstructionNode AsInstructionNode()
    {
        var node = new InstructionNode(Index, Angle, Length);
        node.Closed = Closed;
        node.ParentIndex = Parent.Index;
        return node;
    }


      
   //todo: how to convert relative angles to absolute angles efficiently?

    }
}

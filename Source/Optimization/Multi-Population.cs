﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  This file is an extension of the 
//  AForge Genetic Library
//  AForge.NET framework
//  http://www.aforgenet.com/framework/
//
//  Copyright © AForge.NET, 2006-2011
//  contacts@aforgenet.com
// ----------------------------------------------------------------------------------------------------
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Multi-Population.cs 
//  Copyright (C) 2014/10/18  1:05 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Pavol Bielik
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AForge;
using AForge.Genetic;
//using CPlan.CommunicateToLuci;
////using CPlan.CommunicateToLuci;
using LuciCommunication;
using CPlan.Optimization.SelectionAlgorithms.PISA;

namespace CPlan.Optimization
{
    [Serializable]
    public class PopulationParameters
    {
        public IMultiFitnessFunction FitnessFunction;
        public IMultiSelectionMethod SelectionMethod;
        public IMultiChromosome Ancestor;
        public int Size;
        public int EpochCounter;
        public int MU;
        public int ALPHA;
        public int LAMBDA;
        public double RandomSelectionPortion;
        public bool AutoShuffling;
        public double CrossoverRate;
        public double MutationRate;
    }

    //============================================================================================================================================================================
    //============================================================================================================================================================================
    [Serializable]
    public class MultiPopulation
    {
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // PISA fields
        # region Fields 
        private IMultiFitnessFunction fitnessFunction;
        private List<IMultiChromosome> population = new List<IMultiChromosome>( );
        private List<IMultiChromosome> newPopulation = new List<IMultiChromosome>();
        protected int size;
        protected double randomSelectionPortion = 0.0;
        protected bool autoShuffling = false;

        // population parameters
        protected double crossoverRate = 0.3;
        protected double mutationRate = 0.8;

        // random number generator
        private static ThreadSafeRandom rand = new ThreadSafeRandom();

        //
        private double fitnessMax = 0;
        private IMultiChromosome bestChromosome = null;

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties
        //size of initial population -> size of archive!
        public IMultiChromosome Ancestor { get; protected set; }
        public int ALPHA { get; protected set; } //= 30;
        //number of individuals one receive from PISA in each subsequent epoch
        public int MU { get; protected set; }//= 20;
        // number of new individuals one have to send to PISA for evaluation in each subsequent epoch
        public int LAMBDA { get; protected set; }//= 40;
        public PisaWrapper Sel { get; protected set; }
        public int EpochCounter { get; protected set; }

        /// <summary>
        /// Number of members/solutions in the non-dominated set.
        /// </summary>
        public int SizeNonDominatedSet
        {
            get { return population.Count; }
        }

        /// <summary>
        /// Return the population.
        /// </summary>
        public List<IMultiChromosome> GetPopulation
        {
            get { return population; }
        }

        /// <summary>
        /// Crossover rate, [0.1, 1].
        /// </summary>
        /// 
        /// <remarks><para>The value determines the amount of chromosomes which participate
        /// in crossover.</para>
        /// 
        /// <para>Default value is set to <b>0.75</b>.</para>
        /// </remarks>
        /// 
        public double CrossoverRate
        {
            get { return crossoverRate; }
            set
            {
                //crossoverRate = Math.Max( 0.1, Math.Min( 1.0, value ) );
                crossoverRate = value;
            }
        }

        /// <summary>
        /// Mutation rate, [0.1, 1].
        /// </summary>
        /// 
        /// <remarks><para>The value determines the amount of chromosomes which participate
        /// in mutation.</para>
        /// 
        /// <para>Defaul value is set to <b>0.1</b>.</para></remarks>
        /// 
        public double MutationRate
        {
            get { return mutationRate; }
            set
            {
                mutationRate = Math.Max( 0.1, Math.Min( 1.0, value ) );
            }
        }

        /// <summary>
        /// Random selection portion, [0, 0.9].
        /// </summary>
        /// <remarks><para>The value determines the amount of chromosomes which will be
        /// randomly generated for the new population. The property controls the amount
        /// of chromosomes, which are selected to a new population using
        /// <see cref="SelectionMethod">selection operator</see>, and amount of random
        /// chromosomes added to the new population.</para>
        /// <para>Default value is set to <b>0</b>.</para></remarks>
        public double RandomSelectionPortion
        {
            get { return randomSelectionPortion; }
            set
            {
                randomSelectionPortion = Math.Max( 0, Math.Min( 0.9, value ) );
            }
        }

        /// <summary>
        /// Determines of auto shuffling is on or off.
        /// </summary>
        /// 
        /// <remarks><para>The property specifies if automatic shuffling needs to be done
        /// on each <see cref="RunEpoch">epoch</see> by calling <see cref="Shuffle"/>
        /// method.</para>
        /// 
        /// <para>Default value is set to <see langword="false"/>.</para></remarks>
        /// 
        public bool AutoShuffling
        {
            get { return autoShuffling; }
            set { autoShuffling = value; }
        }

        /// <summary>
        /// Fitness function to apply to the population.
        /// </summary>
        /// 
        /// <remarks><para>The property sets fitness function, which is used to evaluate
        /// usefulness of population's chromosomes. Setting new fitness function causes recalculation
        /// of fitness values for all population's members and new best member will be found.</para>
        /// </remarks>
        /// 
        public IMultiFitnessFunction FitnessFunction
        {
            get { return fitnessFunction; }
            set
            {   
                fitnessFunction = value;

                foreach (IMultiChromosome member in population)
                {
                    member.Evaluate(fitnessFunction);
                }

                /* *ac*
                var tasks = population.Select(
                    m =>
                    {
                        var t = new Task(async () =>
                        {
                            await m.Evaluate(fitnessFunction).ConfigureAwait(false);
                        });
                        t.Start();
                        return t;
                    });
                foreach (var t in tasks)
                    t.Wait();
                 */

                FindBestChromosome( );
            }
        }
        

        /// <summary>
        /// Maximum fitness of the population.
        /// </summary>
        /// 
        /// <remarks><para>The property keeps maximum fitness of chromosomes currently existing
        /// in the population.</para>
        /// 
        /// <para><note>The property is recalculate only after <see cref="Selection">selection</see>
        /// or <see cref="Migrate">migration</see> was done.</note></para>
        /// </remarks>
        /// 
        public double FitnessMax
        {
            get { return fitnessMax; }
        }

        /// <summary>
        /// Summary fitness of the population.
        /// </summary>
        ///
        /// <remarks><para>The property keeps summary fitness of all chromosome existing in the
        /// population.</para>
        /// 
        /// <para><note>The property is recalculate only after <see cref="Selection">selection</see>
        /// or <see cref="Migrate">migration</see> was done.</note></para>
        /// </remarks>
        ///
        //public double FitnessSum
        //{
        //    get { return fitnessSum; }
        //}

        /// <summary>
        /// Average fitness of the population.
        /// </summary>
        /// 
        /// <remarks><para>The property keeps average fitness of all chromosome existing in the
        /// population.</para>
        /// 
        /// <para><note>The property is recalculate only after <see cref="Selection">selection</see>
        /// or <see cref="Migrate">migration</see> was done.</note></para>
        /// </remarks>
        ///
        //public double FitnessAvg
        //{
        //    get { return fitnessAvg; }
        //}

        /// <summary>
        /// Best chromosome of the population.
        /// </summary>
        /// 
        /// <remarks><para>The property keeps the best chromosome existing in the population
        /// or <see langword="null"/> if all chromosomes have 0 fitness.</para>
        /// 
        /// <para><note>The property is recalculate only after <see cref="Selection">selection</see>
        /// or <see cref="Migrate">migration</see> was done.</note></para>
        /// </remarks>
        /// 
        public IMultiChromosome BestChromosome
        {
            get { return bestChromosome; }
        }

        /// <summary>
        /// Size of the population.
        /// </summary>
        /// 
        /// <remarks>The property keeps initial (minimal) size of population.
        /// Population always returns to this size after selection operator was applied,
        /// which happens after <see cref="Selection"/> or <see cref="RunEpoch"/> methods
        /// call.</remarks>
        /// 
        public int Size
        {
            get { return size; }
        }

        /// <summary>
        /// Get chromosome with specified index.
        /// </summary>
        /// 
        /// <param name="index">Chromosome's index to retrieve.</param>
        /// 
        /// <remarks>Allows to access individuals of the population.</remarks>
        /// 
        public IMultiChromosome this[int index]
        {
            get { return population[index]; }
        }

        /// <summary>
        /// If true, we make only variations from the initial chromosome!
        /// </summary>
        public bool InheritFromOneParent { get; set; }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Population"/> class.
        /// </summary>
        /// 
        /// <param name="size">Initial size of population.</param>
        /// <param name="alpha">Size of initial population for PISA. 
        /// Defines the size of the archive!</param>
        /// <param name="labda">Number of new individuals one have to send to PISA for evaluation in each subsequent epoch.</param>
        /// <param name="mu">Number of individuals one receive from PISA in each subsequent epoch.</param>
        /// <param name="ancestor">Ancestor chromosome to use for population creatioin.</param>
        /// <param name="fitnessFunction">Fitness function to use for calculating
        /// chromosome's fitness values.</param>
        /// 
        /// <remarks>Creates new population of specified size. The specified ancestor
        /// becomes first member of the population and is used to create other members
        /// with same parameters, which were used for ancestor's creation.</remarks>
        /// 
        /// <exception cref="ArgumentException">Too small population's size was specified. The
        /// exception is thrown in the case if <paramref name="size"/> is smaller than 2.</exception>
        ///
        public MultiPopulation (int size, int alpha, int lambda, int mu,
                                IMultiChromosome ancestor,
                                IMultiFitnessFunction fitnessFunction,
                                bool inheriteFromP = false
                                )
        {
            if ( size < 2 )
                throw new ArgumentException( "Too small population's size was specified." );

            InheritFromOneParent = inheriteFromP;
            Ancestor = ancestor;
            this.fitnessFunction = fitnessFunction;
            this.size = size;
            MU = mu;
            ALPHA = alpha;
            LAMBDA = lambda;

////            NotificationBroker.Instance.PostNotification("NUMALPHA", ALPHA);

            TerminateProcess("hype"); //("spea2");
            TerminateProcess("spea2");
            // --- PISA ---------------------------------------------------------       
            // --- Terminate running PISA processes in before you start a new one! ---
            // --- First, test if there is no running process - otherwise the new one crashes. 
            // --- Use the corresponding name for the selector used in the next line. For the process name look into MultiPopulation constructor.
            TerminateProcess("hype"); //("spea2");
            TerminateProcess("spea2");
            Sel = new HYPE(LAMBDA, MU, fitnessFunction.Dimensions); //new SPEA2(LAMBDA, MU, 2); // SPAM // SEMO2  // EPSMOEA
            //Sel = new SEMO2(LAMBDA, MU, fitnessFunction.Dimensions);

            //List<IMultiChromosome> population = new List<IMultiChromosome>();
            //for (int i = 0; i < ALPHA; i++)
            //    population.Add(new PisaTestChromosome());

            //printPopulation("initial population", population);
            // -------------------------------------------------------------------

            // add ancestor to the population
            /* *ac*
            var t = ancestor.Evaluate(fitnessFunction);
            t.Wait();
            */
            var t = ancestor.Evaluate(fitnessFunction);
            t.Wait();

            population.Add(ancestor.Clone());
////            NotificationBroker.Instance.PostNotification("NUMPOP", population.Count);
            // add more chromosomes to the population

            // *ac* population = population.Concat(EvaluateAll(ancestor, ALPHA - 1).Result).ToList();
            EvaluateAll(ancestor, ALPHA - 1).Wait();

            Sel.ApplySelection(population, MU);

            newPopulation = new List<IMultiChromosome>();
            EpochCounter = 1;
        }


        /// <summary>
        /// Copy constructor. Create an empty MultiPopulation! The coromosomes are not copied!!!
        /// </summary>
        /// <param name="originalPopulation">Original population</param>
        public MultiPopulation(MultiPopulation originalPopulation)
        {
            Ancestor = originalPopulation.Ancestor;
            fitnessFunction = originalPopulation.fitnessFunction;
            size = originalPopulation.size;
            MU = originalPopulation.MU;
            ALPHA = originalPopulation.ALPHA;
            LAMBDA = originalPopulation.LAMBDA;
            randomSelectionPortion = 0.0;
            autoShuffling = originalPopulation.autoShuffling;
            crossoverRate = originalPopulation.crossoverRate;
            mutationRate = originalPopulation.mutationRate;
            InheritFromOneParent = originalPopulation.InheritFromOneParent;
            TerminateProcess("hype"); //("spea2");
            TerminateProcess("spea2");
            // --- PISA ---------------------------------------------------------       
            // --- Terminate running PISA processes in before you start a new one! ---
            // --- First, test if there is no running process - otherwise the new one crashes. 
            // --- Use the corresponding name for the selector used in the next line. For the process name look into MultiPopulation constructor.
            TerminateProcess("hype"); //("spea2");
            TerminateProcess("spea2");
            Sel = originalPopulation.Sel;//new HYPE(LAMBDA, MU, fitnessFunction.Dimensions); //new SPEA2(LAMBDA, MU, 2); // SPAM // SEMO2  // EPSMOEA
            //Sel = new SEMO2 (LAMBDA, MU, 2);

            EpochCounter = originalPopulation.EpochCounter;
        }

        /// <summary>
        /// Initiates a new PisaWrapper. 
        /// Userful to continue optimization after loading a population
        /// </summary>
        /// <param name="alpha">Size of initial population</param>
        /// <param name="mu">Number of individuals one receive from PISA in each subsequent epoch</param>
        /// <param name="lambda">Number of new individuals one have to send to PISA for evaluation in each subsequent epoch</param>
        public MultiPopulation(PopulationParameters popParameters, List<IMultiChromosome> archive)
        {
            if (popParameters != null)
            {            
                EpochCounter = popParameters.EpochCounter;
                Ancestor = popParameters.Ancestor;
                RandomSelectionPortion = popParameters.RandomSelectionPortion;
                AutoShuffling = popParameters.AutoShuffling;
                CrossoverRate = popParameters.CrossoverRate;
                MutationRate = popParameters.MutationRate;
                size = popParameters.Size;
                fitnessFunction = popParameters.FitnessFunction;
                // use the archive as initial population
                population = archive;
                EvaluateAll(archive[0],ALPHA-1).Wait();

                // *ac* population = population.Concat(EvaluateAll(archive[0], ALPHA).Result).ToList();

            }
            // PISA fields
            //size of initial population
            ALPHA = popParameters.ALPHA;
            //number of individuals one receive from PISA in each subsequent epoch
            MU = popParameters.MU;
            // number of new individuals one have to send to PISA for evaluation in each subsequent epoch
            LAMBDA = popParameters.LAMBDA;

            // --- Terminate running PISA processes in before you start a new one! ---
            // --- First, test if there is no running process - otherwise the new one crashes. 
            // --- Use the corresponding name for the selector used in the next line. For the process name look into MultiPopulation constructor.
            TerminateProcess("hype"); //("spea2");
            TerminateProcess("spea2");

            //Sel = popParameters.Sel;
            Sel = new HYPE(LAMBDA, MU, fitnessFunction.Dimensions);
            
            Sel.ApplySelection(population, MU);
            newPopulation = new List<IMultiChromosome>();
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Methods

        //===================================================================================================================================================================
        /// <summary>
        /// Terminate a running process.
        /// </summary>
        /// <param name="processName">Name of the process.</param>
        private void TerminateProcess(string processName)
        {
            Process[] matchingProcesses = Process.GetProcessesByName(processName);

            if (matchingProcesses.Length > 0)
            {
                foreach (Process p in matchingProcesses)
                {
                    p.CloseMainWindow();
                }

                Thread.Sleep(500);

                matchingProcesses = Process.GetProcessesByName(processName);

                foreach (Process p in matchingProcesses)
                {
                    if (p.HasExited == false)
                        p.Kill();
                }
            }
        }


        // *ac* private async Task<List<IMultiChromosome>> EvaluateAll(IMultiChromosome ancestor, int amount, bool postNotification = true)
        private async Task EvaluateAll(IMultiChromosome ancestor, int amount, bool postNotification = true)
        {
            var tlist = new Task<IMultiChromosome>[amount];
            if (InheritFromOneParent)
            {
                for (int i = 0; i < amount; i++)
                {
                    Console.WriteLine(i + "-th chormosome: creating");
                    // create new chromosome
                    IMultiChromosome c = ancestor.Clone();
                    for (int m = 1; m < 5; m++)
                        c.Mutate();
                    c.Adapt();
                    // calculate it's fitness
                    // *ac* tlist[i] = c.Evaluate(fitnessFunction);
                    await c.Evaluate(fitnessFunction);
                    Console.WriteLine(i + "-th chormosome: created");

                    // add it to population
                    population.Add(c);
////                    NotificationBroker.Instance.PostNotification("NUMPOP", population.Count);
                }
            }
            else
            {
                for (int i = 0; i < amount; i++)
                {
                    Console.WriteLine(i + "-th chormosome: creating");
                    // create new chromosome
                    IMultiChromosome c = ancestor.CreateNew();
                    // calculate it's fitness
                    // *ac* tlist[i] = c.Evaluate(fitnessFunction);
                    await c.Evaluate(fitnessFunction);
                    Console.WriteLine(i + "-th chormosome: created");

                    // add it to population
                    population.Add(c);
////                    NotificationBroker.Instance.PostNotification("NUMPOP", population.Count);
                }
            }
            // wait for evaluation and add chromosomes to population
 /*  *ac*          var chromosomes = new List<IMultiChromosome>();
            for (int i = 0; i < amount; i++)
            {
                chromosomes.Add(await tlist[i].ConfigureAwait(false));
                Console.WriteLine(i + "-th chormosome: finished");
                if (postNotification)
                    NotificationBroker.Instance.PostNotification("NUMPOP", population.Count);
            }
            return chromosomes;*/
        }


        //============================================================================================================================================================================
        /// <summary>
        /// Clear population.
        /// </summary>
        public void Clear()
        {
            population = new List<IMultiChromosome>();
            newPopulation = new List<IMultiChromosome>();
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Regenerate population.
        /// </summary>
        /// 
        /// <remarks>The method regenerates population filling it with random chromosomes.</remarks>
        /// 
        public void Regenerate( )
        {
            IMultiChromosome ancestor = population[0];

            // clear population
            population.Clear( );
            // add chromosomes to the population

            // *ac*population = population.Concat(EvaluateAll(ancestor, ALPHA , false).Result).ToList();
            EvaluateAll(ancestor, ALPHA, false).Wait();
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Do crossover in the population.
        /// </summary>
        /// 
        /// <remarks>The method walks through the population and performs crossover operator
        /// taking each two chromosomes in the order of their presence in the population.
        /// The total amount of paired chromosomes is determined by
        /// <see cref="CrossoverRate">crossover rate</see>.</remarks>
        /// 
        public virtual void Crossover( )
        {
            // crossover            
            //float crossoverRate = 0.8f;
            int nrCross = (int)(crossoverRate * LAMBDA);
            for (int i = 1; i < nrCross; i += 2)
            {
                // generate next random number and check if we need to do crossover
                //if ( rand.NextDouble( ) <= crossoverRate )
                {
                    // clone both ancestors
                    int rndA = rand.Next(population.Count);
                    int rndB = rand.Next(population.Count);
                    IMultiChromosome c1 = population[rndA].Clone();
                    IMultiChromosome c2 = population[rndB].Clone();

                    // do crossover
                    c1.Crossover( c2 );

                    // mutate also the individuals produced by crossover
                    if (rand.NextDouble() <= mutationRate) c1.Mutate();
                    if (rand.NextDouble() <= mutationRate) c2.Mutate();
                    
                    // calculate fitness of these two offsprings
                    //c1.Evaluate( fitnessFunction );
                    //c2.Evaluate( fitnessFunction );

                    // add two new offsprings to the population
                    newPopulation.Add(c1);
                    newPopulation.Add(c2);
                }
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Do mutation in the population.
        /// </summary>
        /// 
        /// <remarks>The method walks through the population and performs mutation operator
        /// taking each chromosome one by one. The total amount of mutated chromosomes is
        /// determined by <see cref="MutationRate">mutation rate</see>.</remarks>
        /// 
        public virtual void Mutate( )
        {
            // use mutate operator to fill the next generation (size defined by LAMBDA).
            int nrMutate = LAMBDA - newPopulation.Count();
            for (int i = 0; i < nrMutate; i++)
            {
                // generate next random number and check if we need to do mutation
                //if ( rand.NextDouble( ) <= mutationRate )
                {
                    // clone the chromosome
                    int rndC = rand.Next(population.Count);
                    IMultiChromosome c = population[rndC].Clone();
                    // mutate it
                    c.Mutate( );
                    // calculate fitness of the mutant
                    //c.Evaluate( fitnessFunction );
                    // add mutant to the population
                    newPopulation.Add(c);
                }
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Do adaptation for each chromosome.
        /// </summary>
        /// 
        /// <remarks>The method walks through the population and performs adapt operator
        /// taking each chromosome one by one. 
        /// </remarks>
        /// 
        public virtual void Adapt()
        {
            //for (int i = 0; i < population.Count; i++)
            //{
            //    IMultiChromosome c = population[i];
            //    // run adaption operator
            //    c.Adapt();
            //}

            for (int i = 0; i < newPopulation.Count; i++)
            {
                IMultiChromosome c = newPopulation[i];
                // run adaption operator
                c.Adapt();
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Evaluate all new chromosomes.
        /// This separate method avoid repeated evaluations for a chromosome.
        /// </summary>
        public virtual async Task Evaluate()
        {
            //EpochCounter;
            //ResultManager.Instance.ResetResultList();
            //ResultManager.Instance.RegisterNotification(newPopulation.Count, "FitnessForMultiChromosome_Epoch_" + EpochCounter.ToString());
            //NotificationBroker.Instance.Register(NotificationAllResultsReadyHandler, "FitnessForMultiChromosome_Epoch_" + EpochCounter.ToString());

////            NotificationBroker.Instance.PostNotification("NUMALPHA", newPopulation.Count);
            for (int i = 0; i < newPopulation.Count; i++)
            {
                IMultiChromosome c = newPopulation[i];
                //c.FinisehdFitnessCalculation = false;
                //fitnessFunction.RunVia = RunVia;
                //fitnessFunction.RunVia_Solar = RunVia_Solar;

                // *ac* await c.Evaluate(fitnessFunction).ConfigureAwait(false);
                await c.Evaluate(fitnessFunction);

////                NotificationBroker.Instance.PostNotification("NUMPOP", i + 1);

                //bool tester = false;
                
                //while (tester == false)
                //{
                //    if (ResultManager.Instance.GetResult(i) != null) tester = true;
                //    Thread.Sleep(1);
                //}
                //Thread.Sleep(1);        
            }

            //while loop calls results...
            //while (ResultManager.Instance.Count < newPopulation.Count)
            //{
            //    Thread.Sleep(1);
            //}

            //for (int i = 0; i < newPopulation.Count; i++)
            //{
            //    IMultiChromosome c = newPopulation[i];
            //    //if (ResultManager.Instance.GetResult(i) != null) c.FinisehdFitnessCalculation = true;
            //    c.AssignResult(ResultManager.Instance.GetResult(i));
            //    c.Evaluate(fitnessFunction, false);
            //}
 
        }

        //void NotificationAllResultsReadyHandler(object sender, Dictionary<int, object> userInfo)
        //{
        //    if (userInfo != null)
        //    {
        //        // in dem Dictionary sind Deine Ergebnisse: key = index, value = Berechnungsergebnisse, wie Du Sie unter 4. AddResult() zugefügt hast.
        //    }
        //}


        //============================================================================================================================================================================
        /// <summary>
        /// Do selection.
        /// </summary>
        /// 
        /// <remarks>The method applies selection operator to the current population. Using
        /// specified selection algorithm it selects members to the new generation from current
        /// generates and adds certain amount of random members, if is required
        /// (see <see cref="RandomSelectionPortion"/>).</remarks>
        public virtual void Selection( )
        {
            // amount of random chromosomes in the new population
            int randomAmount = (int) ( randomSelectionPortion * size );

            // next generation
            population = newPopulation;
            foreach (IMultiChromosome chromo in population) chromo.Generation++;

            // do selection
            //selectionMethod.ApplySelection( population, size - randomAmount );
           
            Sel.ApplySelection(population, MU);
            Sel.Clean(); // clean up the lists in the PISA selector. Only necedssary to save memory.
            newPopulation = new List<IMultiChromosome>();

            // add random chromosomes
            if (randomAmount > 0)
            {
                IMultiChromosome ancestor = population[0];
                //List<IMultiChromosome> newPopulation = new List<IMultiChromosome>();
                // *ac* population = population.Concat(EvaluateAll(ancestor, randomAmount, false).Result).ToList();
                EvaluateAll(ancestor, randomAmount, false).Wait();
            }
            FindBestChromosome( );
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Run one epoch of the population.
        /// </summary>
        /// 
        /// <remarks>The method runs one epoch of the population, doing crossover, mutation
        /// and selection by calling <see cref="Crossover"/>, <see cref="Mutate"/> and
        /// <see cref="Selection"/>.</remarks>
        /// 
        public async Task RunEpoch( )
        {
////            NotificationBroker.Instance.PostNotification("NUMPOP", 0);

            // -- best solutions are preserved in the PISA archive.
            // -- therefore we don't need to copy the parents to the next generation!
            Crossover();
            Mutate();
            Adapt();
            await Evaluate().ConfigureAwait(false);


            // CM_L -> Here we need to wait until all chromosomes are evaluated!!!
/*
            while (ServiceManager.CheckListEmpty() != true)
            {
                // sleep
                Thread.Sleep(100);
            }
            */

            TerminateProcess("hype"); //("spea2");
            TerminateProcess("spea2");
            Sel.isPISAinitialized = false;
            Selection();

            if (autoShuffling)
                Shuffle();

            EpochCounter++;
            //for (int i = 0; i < 1000; i++)
            //{
            //    Sel.ApplySelection(population, MU);
            //    List<IMultiChromosome> mutatedPopulation = new List<IMultiChromosome>();
            //    foreach (IMultiChromosome chromosome in population)
            //    {
            //        chromosome.Mutate();
            //        mutatedPopulation.Add(chromosome);
            //    }
            //    population = mutatedPopulation;
            //}
       
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Shuffle randomly current population.
        /// </summary>
        /// 
        /// <remarks><para>Population shuffling may be useful in cases when selection
        /// operator results in not random order of chromosomes (for example, after elite
        /// selection population may be ordered in ascending/descending order).</para></remarks>
        /// 
        public void Shuffle( )
        {
            // current population size
            int size = population.Count;
            // create temporary copy of the population
            List<IMultiChromosome> tempPopulation = population.GetRange( 0, size );
            // clear current population and refill it randomly
            population.Clear( );

            while ( size > 0 )
            {
                int i = rand.Next( size );

                population.Add( tempPopulation[i] );
                tempPopulation.RemoveAt( i );

                size--;
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Add chromosome to the population.
        /// </summary>
        /// 
        /// <param name="chromosome">Chromosome to add to the population.</param>
        /// 
        /// <remarks><para>The method adds specified chromosome to the current population.
        /// Manual adding of chromosome maybe useful, when it is required to add some initialized
        /// chromosomes instead of random.</para>
        /// 
        /// <para><note>Adding chromosome manually should be done very carefully, since it
        /// may break the population. The manually added chromosome must have the same type
        /// and initialization parameters as the ancestor passed to constructor.</note></para>
        /// </remarks>
        /// 
        //public async Task AddChromosome( IMultiChromosome chromosome )
        //{
        //    await chromosome.Evaluate( fitnessFunction );
        //    population.Add( chromosome );
        //}

        //============================================================================================================================================================================
        /// <summary>
        /// Perform migration between two populations.
        /// </summary>
        /// 
        /// <param name="anotherPopulation">Population to do migration with.</param>
        /// <param name="numberOfMigrants">Number of chromosomes from each population to migrate.</param>
        /// <param name="migrantsSelector">Selection algorithm used to select chromosomes to migrate.</param>
        /// 
        /// <remarks><para>The method performs migration between two populations - current and the
        /// <paramref name="anotherPopulation">specified one</paramref>. During migration
        /// <paramref name="numberOfMigrants">specified number</paramref> of chromosomes is choosen from
        /// each population using <paramref name="migrantsSelector">specified selection algorithms</paramref>
        /// and put into another population replacing worst members there.</para></remarks>
        /// 
        public void Migrate(MultiPopulation anotherPopulation, int numberOfMigrants, IMultiSelectionMethod migrantsSelector)
        {
            int currentSize = size;
            int anotherSize = anotherPopulation.Size;

            // create copy of current population
            List<IMultiChromosome> currentCopy = new List<IMultiChromosome>( );

            for ( int i = 0; i < currentSize; i++ )
            {
                currentCopy.Add( population[i].Clone( ) );
            }

            // create copy of another population
            List<IMultiChromosome> anotherCopy = new List<IMultiChromosome>( );

            for ( int i = 0; i < anotherSize; i++ )
            {
                anotherCopy.Add( anotherPopulation.population[i].Clone( ) );
            }

            // apply selection to both populations' copies - select members to migrate
            migrantsSelector.ApplySelection( currentCopy, numberOfMigrants );
            migrantsSelector.ApplySelection( anotherCopy, numberOfMigrants );

            // sort original populations, so the best chromosomes are in the beginning
            population.Sort( );
            anotherPopulation.population.Sort( );

            // remove worst chromosomes from both populations to free space for new members
            population.RemoveRange( currentSize - numberOfMigrants, numberOfMigrants );
            anotherPopulation.population.RemoveRange( anotherSize - numberOfMigrants, numberOfMigrants );

            // put migrants to corresponding populations
            population.AddRange( anotherCopy );
            anotherPopulation.population.AddRange( currentCopy );

            // find best chromosomes in each population
            FindBestChromosome( );
            anotherPopulation.FindBestChromosome( );
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Resize population to the new specified size.
        /// </summary>
        /// 
        /// <param name="newPopulationSize">New size of population.</param>
        /// 
        /// <remarks><para>The method does resizing of population. In the case if population
        /// should grow, it just adds missing number of random members. In the case if
        /// population should get smaller, the <see cref="SelectionMethod">population's
        /// selection method</see> is used to reduce the population.</para></remarks>
        /// 
        /// <exception cref="ArgumentException">Too small population's size was specified. The
        /// exception is thrown in the case if <paramref name="newPopulationSize"/> is smaller than 2.</exception>
        /// 
        public void Resize( int newPopulationSize )
        {
            Resize( newPopulationSize);
        }

        /// <summary>
        /// Resize population to the new specified size.
        /// </summary>
        /// 
        /// <param name="newPopulationSize">New size of population.</param>
        /// <param name="membersSelector">Selection algorithm to use in the case
        /// if population should get smaller.</param>
        /// 
        /// <remarks><para>The method does resizing of population. In the case if population
        /// should grow, it just adds missing number of random members. In the case if
        /// population should get smaller, the specified selection method is used to
        /// reduce the population.</para></remarks>
        /// 
        /// <exception cref="ArgumentException">Too small population's size was specified. The
        /// exception is thrown in the case if <paramref name="newPopulationSize"/> is smaller than 2.</exception>
        ///
        public void Resize(int newPopulationSize, IMultiSelectionMethod membersSelector)
        {
            if ( newPopulationSize < 2 )
                throw new ArgumentException( "Too small new population's size was specified." );

            if (newPopulationSize > size)
            {
                // population is growing, so add new rundom members

                // Note: we use population.Count here instead of "size" because
                // population may be bigger already after crossover/mutation. So
                // we just keep those members instead of adding random member.
                int toAdd = newPopulationSize - population.Count;
                EvaluateAll(population[0],toAdd, false).Wait();

                // *ac* population = population.Concat(EvaluateAll(population[0], toAdd, false).Result).ToList();
            }
            else
            {
                // do selection
                membersSelector.ApplySelection(population, newPopulationSize);
            }

            size = newPopulationSize;
        }

        //============================================================================================================================================================================
        //public void SortChromosomesToSummedFitness()
        //{
        //    population.Sort(delegate(IMultiChromosome ind1, IMultiChromosome ind2) { return ind1.FitnessSummed.CompareTo(ind2.FitnessSummed); });
        //}

        //============================================================================================================================================================================
        /// <summary>
        /// Find best chromosome in the population so far
        /// </summary>
        private void FindBestChromosome( )
        {
            bestChromosome = population[0];
            /*fitnessMax = bestChromosome.Fitness;
            fitnessSum = fitnessMax;

            for ( int i = 1; i < size; i++ )
            {
                double fitness = population[i].Fitness;

                // accumulate summary value
                fitnessSum += fitness;

                // check for max
                if ( fitness > fitnessMax )
                {
                    fitnessMax = fitness;
                    bestChromosome = population[i];
                }
            }
            fitnessAvg = fitnessSum / size;*/
        }

        # endregion

    }
}

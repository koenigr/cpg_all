﻿using System;
using System.ComponentModel;

namespace CPlan.Optimization
{
    [Serializable]
    public class ParametersLayout
    {
        [Category("Layout parameters"), Description("Min buidling area")]
        public int MinBldArea { get; set; }

        [Category("Layout parameters"), Description("Max buidling area")]
        public int MaxBldArea { get; set; }

        [Category("Layout parameters"), Description("Min buidling heigth")]
        public int MinBldHeigth { get; set; }

        [Category("Layout parameters"), Description("Max buidling heigth")]
        public int MaxBldHeigth { get; set; }

        [Category("Layout parameters"), Description("Min number of buidlings")]
        public int MinNrBuildings { get; set; }

        [Category("Layout parameters"), Description("Max number of buidlings")]
        public int MaxNrBuildings { get; set; }

        [Category("Layout parameters"), Description("Denisity - coverage ration")]
        public double Density { get; set; }

        [Category("Layout parameters"), Description("Min length of a rectangle width respect to the other side")]
        public double MinSideRatio { get; set; }

        [Category("Layout parameters"), Description("Buildings align to the borders")]
        public bool AlignRotationToBorder { get; set; }

        [Category("Layout parameters"), Description("Value for the rotation of all buildings")]
        public float FixedRotationValue { get; set; }

        [Category("Layout parameters"), Description("Fix rotation to a specified value (FixedRotationValue)")]
        public bool FixRotation { get; set; }

    };
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = ParametersOptimization.cs 
//  Copyright (C) 2015/08/18  2:05 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.ComponentModel;
using System.Globalization;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;

namespace CPlan.Optimization
{
    [Serializable]
    public class ParametersOptimization
    {
        // --- Optimization parameters ---
        [Category("Optimization parameters"), Description("Number of generations/iterations")]
        public int Generations { get; set; }
        
        [Category("Optimization parameters"), Description("Size of the population")]
        public int PopulationSize { get; set; }

        [Category("Optimization parameters"), Description("Population preasure - multiplied with the population size is the number of children")]
        public float PopulationPreasure { get; set; }

        [Category("Optimization parameters"), Description("Archive size - number of solutions in the global archive")]
        public int ArchiveSize { get; set; }

        [ExpandableObject]
        [Category("Optimization parameters"), Description("Specifies if the evaluation is done locally or via luci")]
        public CalculateVia RunVia { get; set; }

        [ExpandableObject]
        [Category("Optimization parameters"), Description("The fitness function to be used for optimizatio.")]
        public FitnessFunctions.Functions SelectedFitnessFunction { get; set; }

        [ExpandableObject]
        [Category("Optimization parameters"), Description("Select, which anlysis reults are shown.")]
        public VisualOptions VisualizationOptions { get; set; }

        [ExpandableObject]
        [Category("Optimization parameters"), Description("Select how the layouts are drawn.")]
        public DrawLayoutOptions DrawLayoutStyle { get; set; }

        [ExpandableObject]
        [Category("Optimization parameters"), Description("Select how the layouts are drawn.")]
        public DisplayIsovistMeasure DisplayIsovistMeasure{ get; set; }
        

        private FitnessFunctions _fitnessFunction;// { get; set; }


        /// <summary>
        /// Constructor to inizilaize the optimization parameters.
        /// </summary>
        /// <param name="aGenerations"></param>
        /// <param name="aPopulationSize"></param>
        /// <param name="aPopulationPreasure"></param>
        /// <param name="aArchiveSize"></param>
        /// <param name="aRunVia"></param>
        public ParametersOptimization(int aGenerations, int aPopulationSize, float aPopulationPreasure, int aArchiveSize, CalculateVia aRunVia, String pathToHDRI)
        {
            Generations = aGenerations;
            PopulationSize = aPopulationSize;
            PopulationPreasure = aPopulationPreasure;
            ArchiveSize = aArchiveSize;
            RunVia = aRunVia;
            _fitnessFunction = new FitnessFunctions(aRunVia, pathToHDRI);
        }

    }


}

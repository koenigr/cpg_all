﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = PisaTest.cs 
//  Copyright (C) 2014/10/18  1:03 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Pavol Bielik, Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Threading;
using CPlan.Optimization.Chromosomes;
using CPlan.Optimization.SelectionAlgorithms.PISA;

namespace CPlan.Optimization
{
    
    /// <summary>
    /// In order to run the test program, we need to change output type of the project from "library" to "console"
    /// </summary>
    [Serializable]
    public class PisaTest
    {
        //size of initial population
        public static readonly int ALPHA = 100;
        //max number of new individuals as PISA input in each subsequent epoch
        public static readonly int MU = 40;

        static void printPopulation(String text, List<IMultiChromosome> population)
        {
            Console.WriteLine(text);
            foreach (IMultiChromosome chromosome in population)
                Console.WriteLine(chromosome.ToString());
        }

        public static void runPISA()
        {
            PisaWrapper sel = new SPEA2(MU, MU, 3);

            List<IMultiChromosome> population = new List<IMultiChromosome>();
            for (int i = 0; i < ALPHA; i++)
                population.Add(new PisaTestChromosome());

            printPopulation("initial population", population);

            for (int i = 0; i < 1000; i++)
            {
                sel.ApplySelection(population, MU);
                List<IMultiChromosome> mutatedPopulation = new List<IMultiChromosome>();
                foreach (IMultiChromosome chromosome in population)
                {
                    chromosome.Mutate();
                    mutatedPopulation.Add(chromosome);
                }
                population = mutatedPopulation;
            }

            sel.Terminate();
        }

        public static void Main(string[] args)
        {
            new Thread(runPISA).Start();
        }
    }
}

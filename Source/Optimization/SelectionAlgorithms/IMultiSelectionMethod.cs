﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = IMultiSelectionMethod.cs 
//  Copyright (C) 2014/10/18  1:03 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System.Collections.Generic;

namespace CPlan.Optimization
{
    /// <summary>
    /// Genetic selection method interface.
    /// </summary>
    /// 
    /// <remarks>The interface should be implemented by all classes, which
    /// implement genetic selection algorithm. These algorithms select members of
    /// current generation, which should be kept in the new generation. Basically,
    /// these algorithms filter provided population keeping only specified amount of
    /// members.</remarks>
    /// 
    public interface IMultiSelectionMethod
    {
        /// <summary>
        /// Apply selection to the specified population.
        /// </summary>
        /// 
        /// <param name="chromosomes">Population, which should be filtered.</param>
        /// <param name="size">The amount of chromosomes to keep.</param>
        /// 
        /// <remarks>Filters specified population according to the implemented
        /// selection algorithm.</remarks>
        /// 
        void ApplySelection(List<IMultiChromosome> chromosomes, int size);
    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = MultiEliteSelection.cs 
//  Copyright (C) 2014/10/18  1:03 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;

namespace CPlan.Optimization
{
    [Serializable]
    public class MultiEliteSelection : IMultiSelectionMethod
    {
        public void ApplySelection(List<IMultiChromosome> chromosomes, int size)
        {
            // calculate best-ellite chromosomes for each criteria
            List<IMultiChromosome> newPopulation = EliteSet(chromosomes, size);

            // empty current population
            chromosomes.Clear();

            // move elements from new to current population
            chromosomes.AddRange(newPopulation); //size limitation is here ingored
        }

        /// <summary>
        /// Calculate best-ellite chromosomes for each criteria.
        /// </summary>
        /// <param name="chromosomes">Population to select ellite chromosomes.</param>
        /// <param name="size">Population size.s</param>
        /// <returns>List of ellite chromosomes, one for each criteria.</returns>
        private List<IMultiChromosome> EliteSet(List<IMultiChromosome> chromosomes, int size)
        {
            // for each criteria create SortedDictionary to sort chromosomes by one criteria (fitness value) and save it in temp_set
            List<SortedDictionary<double, IMultiChromosome>> temp_set = new List<SortedDictionary<double, IMultiChromosome>>();
            for (int criterio = 0; criterio < chromosomes[0].FitnessValues.Count; criterio++)
            {
                // add and sort chromosome to the SortedDictionary
                SortedDictionary<double, IMultiChromosome> criteria_set = new SortedDictionary<double, IMultiChromosome>();
                foreach (IMultiChromosome chromosome in chromosomes)
                {
                    if (!criteria_set.ContainsKey(chromosome.FitnessValues[criterio]))
                        criteria_set.Add(chromosome.FitnessValues[criterio], chromosome);
                }
                // save SortedDictionary to the temp_set
                temp_set.Add(criteria_set);
            }

            int idx = 0;
            List<IMultiChromosome> elite_chromosomes = new List<IMultiChromosome>();
            while (elite_chromosomes.Count < size)
            { 
                foreach (SortedDictionary<double, IMultiChromosome> criterio in temp_set)
                {
                    //HACK here can be added more chromosomes per one criterio
                    List<IMultiChromosome> best = criterio.Values.ToList();
                    elite_chromosomes.Add(criterio.ElementAt(idx).Value); //(criterio.First().Value); //criterio.First - Last..
                    if (elite_chromosomes.Count >= size) break;
                }
                idx++;
            }
            return elite_chromosomes;
        }
    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = NonDominatedSet.cs 
//  Copyright (C) 2014/10/18  1:03 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using AForge;

namespace CPlan.Optimization
{
    //===============================================================================================================================================================================
    //===============================================================================================================================================================================
    [Serializable]
    public class NonDominatedSelection : IMultiSelectionMethod
    {
        /// <summary>
        /// Random number generator.
        /// </summary>
        private static ThreadSafeRandom _rand = new ThreadSafeRandom();
        
        /// <summary>
        /// Stores the density of soulutions per cell.
        /// </summary>
        private Dictionary<string, int> multiGrid;

        private Dictionary<string, List<IMultiChromosome>> chromosByAdress;

        //============================================================================================================================================================================
        public void ApplySelection(List<IMultiChromosome> chromosomes, int size)
        {
            //throw new NotImplementedException();

            List<IMultiChromosome> newPopulation = NonDominatedSet(chromosomes);

            // add old chromosomes if the newPopulation is too small
            while (newPopulation.Count < size)
            {
                int idx = _rand.Next(chromosomes.Count);
                IMultiChromosome newChromo = chromosomes[idx].Clone();
                newPopulation.Add(newChromo);
            }

            // delete chromosomes if the newPopulation is too large
            if (newPopulation.Count > size)
            {
                List<IMultiChromosome> deleteCandidates = new List<IMultiChromosome>();
                
                // -- find chromosomes with same fitness = same phenotypes
                for (int p = 0; p < newPopulation.Count(); p++)
                {
                    IMultiChromosome thisChromo = newPopulation[p];
                    bool same = true;
                    for (int r = p+1; r < newPopulation.Count(); r++)
                    {
                        IMultiChromosome otherChromo = newPopulation[r];
                        for (int condition = 0; condition < thisChromo.FitnessValues.Count; condition++)
                        {
                            if (thisChromo.FitnessValues[condition] != otherChromo.FitnessValues[condition])
                            {
                                same = false;
                                break;
                            }
                        }
                        if (same) 
                            deleteCandidates.Add(thisChromo);
                    }
                }
                // -- delete same chromosomes
                while (newPopulation.Count > size && deleteCandidates.Count > 0)
                {
                    int idx = _rand.Next(deleteCandidates.Count);
                    IMultiChromosome toBeDeleted = deleteCandidates[idx];
                    newPopulation.Remove(toBeDeleted);
                    deleteCandidates.Remove(toBeDeleted);
                }
            }

            // -- remove chromosomes from populated areas
            RemoveMostDenseSolutions(newPopulation, size);

            // empty current population
            chromosomes.Clear();

            // move elements from new to current population
            chromosomes.AddRange(newPopulation);//.GetRange(1,size));
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Find the non dominated set. It contains all solutions on the pareto-front that are not dominated (in all objectives) by any other solution.
        /// </summary>
        /// <param name="population"> List of population - parents and childs.</param>
        /// <returns>Non dominated set. </returns>
        private List<IMultiChromosome> NonDominatedSet(List<IMultiChromosome> population)
        {
            List<IMultiChromosome> non_dominated_individuals = new List<IMultiChromosome>();
            bool is_dominated = false;
            for (int actual = 0; actual < population.Count(); actual++)
            {
                //reset dominated status
                is_dominated = false;
                //test if is dominated by next individuals
                for (int next = 0; next < population.Count(); next++)
                {
                    if (actual != next) //skip comperation with it self
                    {
                        is_dominated = IsDominated(population[actual], population[next]);
                        if (is_dominated)
                            break;
                    }
                }
                //if the individual is not dominanted
                if (!is_dominated)
                    //add individual to result value - weak domination - pareto-optimal
                    non_dominated_individuals.Add(population[actual].Clone());
            }
            return non_dominated_individuals;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Compare actual chromosome with next chromosome
        /// </summary>
        /// <param name="actual">Actual chromosome to test.</param>
        /// <param name="next">Chromosome with will be actual tested.</param>
        /// <returns>Return if next chromosome is dominated by actual chromosome</returns>
        private bool IsDominated(IMultiChromosome actual, IMultiChromosome next)
        {
            if (actual.FitnessValues.Count == next.FitnessValues.Count)
            {
                int counter = 0;
                //for each condition
                for (int condition = 0; condition < actual.FitnessValues.Count; condition++)
                {
                    if (actual.FitnessValues[condition] > next.FitnessValues[condition])
                        counter++;
                    //return true; //next dominated by actual
                }
                if (counter == actual.FitnessValues.Count)
                    return true;
            }
            else
            {
                throw new IndexOutOfRangeException("Count of chromosomes' fintess values are not the same!");
            }
            return false;
        }

        //============================================================================================================================================================================
        //private void RemoveByRanking(int MaxSize)
        //{
        //}

        //============================================================================================================================================================================
        /// <summary>
        /// Remove solutions from the non-dominated set until the populations size is reached.
        /// </summary>
        /// <param name="nonDomSet"> The non-dominated set.</param>
        /// <param name="size">Population size.</param>
        private void RemoveMostDenseSolutions(List<IMultiChromosome> nonDomSet, int size)
        {
            
            CalculateSqueezeFactor(nonDomSet);// calculate squeeze factor
            while (nonDomSet.Count > size)
            {  
                // --- find the most dense grids cells
                int maxValue = 0;
                foreach (KeyValuePair<string, int> pair in multiGrid)
                {
                    if (pair.Value > maxValue)
                        maxValue = pair.Value;
                }
                
                if (maxValue <=1) // there is no cell with more that one solution
                    break;
                
                // Find all cells with the maximum density and get thei adress
                List<string> tempAdresses = new List<string>();
                foreach (KeyValuePair<string, int> pair in multiGrid)
                {
                    if (pair.Value == maxValue) 
                        tempAdresses.Add(pair.Key);
                }

                // Find all chromosomes of a max-dense cells and delete a random one of them.
                foreach (string curAdress in tempAdresses)
                {
                    //// --- only for debuggîng -----------------------
                    //if (bestAdresses.Contains(curAdress))
                    //    maxValue = maxValue;

                    // add all chromosomes of one adress to a list
                    List<IMultiChromosome> tempChromoList = chromosByAdress[curAdress]; // list from the chromosByAdress mapping

                    int rndIdx = _rand.Next(tempChromoList.Count);
                    IMultiChromosome toRemoveChromo = tempChromoList[rndIdx];

                    nonDomSet.Remove(toRemoveChromo);       // delete the chromosome from the non-dominated list
                    tempChromoList.Remove(toRemoveChromo);
                    multiGrid[curAdress]--;                 // if a solution is deleted, decrease the counter in the cell
                    if (nonDomSet.Count <= size) 
                        return;
                }
            }
        }

        //private string[] bestAdresses = new string[2]; // only for debugging!!!
        //============================================================================================================================================================================
        private void CalculateSqueezeFactor(List<IMultiChromosome> nonDomSet)
        {
            IMultiChromosome CurChrom;                                 
            int nrCellsRow = 10;                                        
            int nrObjectives = nonDomSet[0].FitnessValues.Count;    // number of objectives
            double[] maxValues = new double[nrObjectives];          // list for max values -> to calculate the normalized fitnes values
            for (int i = 0; i < nrObjectives; i++) maxValues[i] = double.MinValue;
            double[] normFitness = new double[nrObjectives];
            string adressKey;

            //// --- only for debugging START ---------------------------------------------------------------
            //IMulti_SelectionMethod selectionMethod = new Multi_EliteSelection();
            //List<IMulti_Chromosome> bestChromos = new List<IMulti_Chromosome>();
            //foreach (IMulti_Chromosome tempChromo in nonDomSet) bestChromos.Add(tempChromo.Clone());
            //selectionMethod.ApplySelection(bestChromos, 2);      
            //// --- only for debudding END -----------------------------------------------------------------

            //-- for each objective we use a list to represent the grid cells
            multiGrid = new Dictionary<string, int>(); // the string-key of the dictionary holds the decoded adress of the gridcell
            chromosByAdress = new Dictionary<string, List<IMultiChromosome>>();

            int[] adress = new int[nrObjectives];

            double GridCellSize = 1.0 / nrCellsRow;
            // --- Normalise ---
            // -- find the maximum values --
            for (int i = 0; i < nonDomSet.Count; i++)
            {
                CurChrom = nonDomSet[i]; 
                for (int k = 0; k < nrObjectives; k++)
                {
                    if (maxValues[k] < CurChrom.FitnessValues[k]) maxValues[k] = CurChrom.FitnessValues[k];
                }
            }

            for (int i = 0; i < nonDomSet.Count; i++)
            {
                CurChrom = nonDomSet[i];
                // -- calculate normalized values --
                for (int k = 0; k < nrObjectives; k++)
                {
                    // -- CurChrom.SetNormFitness(CurChrom.FitnessValues[k] / maxValues[k], k);
                    normFitness[k] = CurChrom.FitnessValues[k] / maxValues[k];
                    
                    // -- find the cell in the multidimensional grid, where the chromosome is lodated in.
                    adress[k] = (int)Math.Truncate(normFitness[k] / GridCellSize);
                }

                // -- count solutions per cell & add entries to the muti-dimensional grid --
                adressKey = CreateKey(adress);
                List<IMultiChromosome> tempChromoList; 
                if (multiGrid.ContainsKey(adressKey))
                {
                    multiGrid[adressKey]++;
                    tempChromoList = chromosByAdress[adressKey];
                    tempChromoList.Add(CurChrom);
                }
                else
                {
                    multiGrid.Add(adressKey, 1);
                    tempChromoList = new List<IMultiChromosome>();
                    tempChromoList.Add(CurChrom);
                    chromosByAdress.Add(adressKey, tempChromoList);
                }
            }

            //// --- only for debugging START ---------------------------------
            //for (int i = 0; i < 2; i++)
            //{
            //    CurChrom = bestChromos[i];
            //    // -- calculate normalized values --
            //    for (int k = 0; k < nrObjectives; k++)
            //    {
            //        // -- CurChrom.SetNormFitness(CurChrom.FitnessValues[k] / maxValues[k], k);
            //        normFitness[k] = CurChrom.FitnessValues[k] / maxValues[k];

            //        // -- find the cell in the multidimensional grid, where the chromosome is lodated in.
            //        adress[k] = (int)Math.Truncate(normFitness[k] / GridCellSize);
            //    }

            //    // -- count solutions per cell & add entries to the muti-dimensional grid --
            //    bestAdresses[i] = CreateKey(adress);
            //}
            //// --- only for debudding END ------------------------------------

        }


        /// <summary>
        /// Create a string out of an array.
        /// It is used to create a string as key for a dictionary.
        /// </summary>
        /// <param name="adress"> Array that points to a adress in a multi dimensional space.</param>
        /// <returns> The adress as string.</returns>
        private string CreateKey(int[] adress)
        {
            string specifier = "000";
            string key = "";
            for (int i = 0; i < adress.Length; i++) key += adress[i].ToString(specifier);
            return key;
        }

    }
}

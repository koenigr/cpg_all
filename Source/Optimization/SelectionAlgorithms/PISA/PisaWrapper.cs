﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = PisaWrapper.cs 
//  For more information about the PISA project, please visit: http://cplan-group.net/synthesis/tools/#pisa
//  Copyright (C) 2014/10/18  1:02 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Pavol Bielik, Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using CPlan.Optimization.Chromosomes;

namespace CPlan.Optimization
{
    [Serializable]
    /// <summary>
    /// PISA wrapper. Provides access to the PISA multi-criteria selectors.
    /// </summary>
    public abstract class PisaWrapper : IMultiSelectionMethod
    {
        public static int MAX_PISA_TIMEOUT_MS = 100000;

        //two dictionaries keeping all the chromosomes we have seen so far and their assigned ID
        private Dictionary<IMultiChromosome, int> chromosomeToIdentifier;
        private Dictionary<int, IMultiChromosome> identifierToChromosome;
        
        private int currentIdentifier = 0;

        public bool isPISAinitialized = false;
        private bool isPISAterminated = true;

        //number of chromosome we want PISA to select each epoch
        private readonly int LAMBDA;
        //number of chromosome we show to PISA at each epoch
        private readonly int MU;
        private readonly int DIMENSION;
        private readonly IMultiChromosome dummyChromosome;

        [NonSerialized]
        private Process ExternalProcess;

        public abstract Dictionary<String, String> GetParams();
        protected abstract String GetSelectorParamsPath();
        protected abstract String GetSelectorExecutablePath();

        //interval to check if the PISA computed the results
        public static double REFRESH_INTERVAL = 0.1; //1;//

        //PISA working files
        //since this is a library it can be included anywhere and therefore relative paths does not work very well

        protected static  String PISA_WORKING_DIR = PISA_DIR + @"workspace\";
        protected static  String STATUS_FILE = PISA_WORKING_DIR + @"PISA_sta";
        protected static  String ARCHIVE_FILE = PISA_WORKING_DIR + @"PISA_arc";
        protected static  String VARIATOR_FILE = PISA_WORKING_DIR + @"PISA_var";
        protected static  String SELECTOR_FILE = PISA_WORKING_DIR + @"PISA_sel";
        protected static  String INIT_FILE = PISA_WORKING_DIR + @"PISA_ini";
        protected static  String CONFIG_FILE = PISA_WORKING_DIR + @"PISA_cfg";

        /// <summary>
        /// PISA wrapper instance.
        /// Since we use only one shared workspace, only one PISA selector can be active at any given time
        /// </summary>
        private static PisaWrapper sActiveInstance = null;

        public static void TerminateAll()
        {
            if (sActiveInstance != null && !sActiveInstance.isTerminated())
                sActiveInstance.Terminate();  
        }

        public PisaWrapper(int lambda, int mu, int dimension)
        {
            if (sActiveInstance != null)
                throw new Exception("only one PISA selector can be active at any given time. Close the active selector and try again.");
            InitializePathes();
            sActiveInstance = this;

            chromosomeToIdentifier = new Dictionary<IMultiChromosome, int>();
            identifierToChromosome = new Dictionary<int, IMultiChromosome>();
            LAMBDA = lambda;
            MU = mu;
            DIMENSION = dimension;

            //initialize dummyChromosome with maximum values -> worst values
            dummyChromosome = new DummyChromosome(Enumerable.Repeat((double) float.MaxValue - 1, dimension).ToList<double>());               

            ExternalProcess = new Process();
            ExternalProcess.StartInfo.FileName = PISA_DIR + GetSelectorExecutablePath();
            ExternalProcess.StartInfo.Arguments = String.Format("{0} {1}PISA_ {2}", PISA_DIR + GetSelectorParamsPath(), PISA_WORKING_DIR, REFRESH_INTERVAL);
            ExternalProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden; //Normal;// change to NORMAL if we want to see the window    
        }

        private void InitializePathes()
        {
            PISA_WORKING_DIR = PISA_DIR + @"workspace\";
            STATUS_FILE = PISA_WORKING_DIR + @"PISA_sta";
            ARCHIVE_FILE = PISA_WORKING_DIR + @"PISA_arc";
            VARIATOR_FILE = PISA_WORKING_DIR + @"PISA_var";
            SELECTOR_FILE = PISA_WORKING_DIR + @"PISA_sel";
            INIT_FILE = PISA_WORKING_DIR + @"PISA_ini";
            CONFIG_FILE = PISA_WORKING_DIR + @"PISA_cfg";
    }

        public void Terminate()
        {
            File.WriteAllText(STATUS_FILE, "6");
            bool exitedSuccesfully = ExternalProcess.WaitForExit(MAX_PISA_TIMEOUT_MS);
            if (!exitedSuccesfully)
                ExternalProcess.Kill();
            isPISAterminated = true;

            sActiveInstance = null;
        }

        public bool isTerminated()
        {
            return isPISAterminated;
        }

        public void ApplySelection(List<IMultiChromosome> chromosomes, int size)
        {
            if (size != MU)
                throw new ArgumentException("PISA does not allow variable amount of selected individuals between epochs");

            if (isPISAinitialized && chromosomes.Count != LAMBDA)
                throw new ArgumentException("PISA does not allow variable amount input individual between epochs");

            if (chromosomes.Count == 0)
                return;

            if (chromosomes[0].FitnessValues.Count != DIMENSION)
                throw new ArgumentException("PISA does not allow variable dimension size between epochs");

            List<Tuple<int, IMultiChromosome>> newChromosomes = new List<Tuple<int, IMultiChromosome>>();

            foreach (IMultiChromosome chromosome in chromosomes)
            {
                if (!chromosomeToIdentifier.ContainsKey(chromosome))
                {
                    //Console.WriteLine("adding: " + chromosome.ToString());
                    //we clone the chromosome to prevent code outside of this method to change it's values,
                    //which is not allowed becouse we use it as a key in dictionary
                    newChromosomes.Add(Tuple.Create(currentIdentifier, chromosome.Clone()));                    
                    identifierToChromosome.Add(currentIdentifier, newChromosomes.Last().Item2);
                    //just incrementing indentifier with each new chromosome might become a problem in a long run.
                    //We might consider reusing some ids which are not in the identifierToChromosome dictionary.
                    chromosomeToIdentifier.Add(newChromosomes.Last().Item2, currentIdentifier++);

////                    CommunicateToLuci.ServiceManager.ReplaceID(((CPlan.Optimization.MultiPisaChromosomeBase)(chromosome)).Indentity, ((CPlan.Optimization.MultiPisaChromosomeBase)(newChromosomes.Last().Item2)).Indentity);
                }                
            }

            if (!isPISAinitialized)
            {
                isPISAinitialized = true;

                CreateFile(GetParams(), PISA_DIR + GetSelectorParamsPath());
                //PISA_cfg - configuration file
                CreateConfigFile(newChromosomes.Count, LAMBDA, MU, DIMENSION);
                //PISA_ini - initial population
                PopulateFile(newChromosomes, INIT_FILE, DIMENSION);
                //PISA_sel - individuals selected for variation (parents)
                File.WriteAllText(SELECTOR_FILE, "0");
                //PISA_arc - individuals in the archive
                File.WriteAllText(ARCHIVE_FILE, "0");
                //PISA_var - variated individuals (offspring)
                File.WriteAllText(VARIATOR_FILE, "0");
                //PISA_sta - current state of computation - 1 == initial state
                File.WriteAllText(STATUS_FILE, "1");
               // Console.ReadKey();
                
/*
                ExternalProcess.StartInfo.RedirectStandardOutput = true;
                ExternalProcess.StartInfo.RedirectStandardError = true;
                ExternalProcess.StartInfo.UseShellExecute = false;
*/
                ExternalProcess.Start();
                
                isPISAterminated = false;
            }
            else
            {
                //PISA expect MU input chromosomes, but also expect no duplicates
                //after we removed duplicates we fill up to MU with dummyChromosomes
                while (newChromosomes.Count != LAMBDA)
                {
                    newChromosomes.Add(Tuple.Create(currentIdentifier, dummyChromosome));
                    identifierToChromosome.Add(currentIdentifier++, dummyChromosome);
                }

                PopulateFile(newChromosomes, VARIATOR_FILE, DIMENSION);
                File.WriteAllText(SELECTOR_FILE, "0");
                File.WriteAllText(ARCHIVE_FILE, "0");                
                File.WriteAllText(STATUS_FILE, "3");
            }

            //wait until PISA computes the result
            int waited = 0;
            bool isReady = false;
            do
            {
                try
                {
                    isReady = Int32.Parse(File.ReadAllText(STATUS_FILE)) == 2;
                }
                catch (IOException e)
                {
                    //it can happen that both processes access the file at the same time
                    isReady = false;
                }
                Thread.Sleep(100);
                waited += 100;
            } while (!isReady && waited < MAX_PISA_TIMEOUT_MS);

            if (waited >= MAX_PISA_TIMEOUT_MS)
            {
                //StreamReader myStreamReader = null;
                //myStreamReader = ExternalProcess.StandardOutput;
                //String message = myStreamReader.ReadToEnd();
                //throw new Exception(String.Format("Maximum timeout {0} for PISA response reached.\nHype Message:\n{1}", MAX_PISA_TIMEOUT_MS, message));
                throw new Exception(String.Format("Maximum timeout {0} for PISA response reached.", MAX_PISA_TIMEOUT_MS));
            }

            List<IMultiChromosome> newPopulation = new List<IMultiChromosome>();
            //the result contains indexes of selected chromosones            
            string[] lines = File.ReadAllLines(SELECTOR_FILE);

            //first line contains count, last line "END" so we skip those
            for (int i = 1; i < lines.Count() - 1; i++)
            {
                int idx = Int32.Parse(lines[i]);
                if (identifierToChromosome.ContainsKey(idx) == false)
                {
                    throw new Exception("getting dummy chromosome from selector should not happen");
                }
                newPopulation.Add(identifierToChromosome[idx].Clone());
            }

            //printArchive();

            // empty current population
            chromosomes.Clear();

            // move elements from new to current population
            chromosomes.AddRange(newPopulation);           
        }

        private void printArchive()
        {
            Console.WriteLine("Archive: ");
            string[] lines = File.ReadAllLines(ARCHIVE_FILE);
            for (int i = 1; i < lines.Count() - 1; i++)
            {
                int idx = Int32.Parse(lines[i]);
                Console.WriteLine(identifierToChromosome[idx].ToString());
            }
        }

        /// <summary>
        /// Return cloned members ot the archive.
        /// They can be modified without effecting the archive.
        /// </summary>
        /// <returns>List of individuals from the archive.</returns>
        public List<IMultiChromosome> GetArchive()
        {
            List<IMultiChromosome> result = new List<IMultiChromosome>();
            string[] lines = File.ReadAllLines(ARCHIVE_FILE);
            for (int i = 1; i < lines.Count() - 1; i++)
            {
                int idx = Int32.Parse(lines[i]);
                //clone the chromosome to avoid modification
                result.Add(identifierToChromosome[idx].Clone());
            }
            return result;
        }

        /// <summary>
        /// Return the original members ot the archive.
        /// They shouldn't be modified. It's for saving purposes.
        /// </summary>
        /// <returns>List of individuals from the archive.</returns>
        public List<MultiPisaChromosomeBase> GetArchiveOrig()
        {
            List<MultiPisaChromosomeBase> result = new List<MultiPisaChromosomeBase>();
            string[] lines = File.ReadAllLines(ARCHIVE_FILE);
            for (int i = 1; i < lines.Count() - 1; i++)
            {
                int idx = Int32.Parse(lines[i]);
                //clone the chromosome to avoid modification
                result.Add((MultiPisaChromosomeBase)identifierToChromosome[idx]);
            }
            return result;
        }

        /// <summary>
        /// Delete all unnessesary chromosomes (=the ones that are not in the archive)
        /// from the dictionaries chromosomeToIdentifier and identifierToChromosome
        /// Use this, if you run out of memory.
        /// </summary>
        public void Clean()
        {
            List<MultiPisaChromosomeBase> archive = GetArchiveOrig();
            List<IMultiChromosome> toBeDeletedA = new List<IMultiChromosome>();
            foreach (var chromo in chromosomeToIdentifier.Keys)
            {
                bool isInArchive = false;
                for (int i = 0; i < archive.Count; i++)
                {
                    MultiPisaChromosomeBase archChromo = archive[i];
                    if (chromo.Equals(archChromo))
                            isInArchive = true;
                }
                if (!isInArchive) // collect chromosomes to be deleted
                    toBeDeletedA.Add(chromo);
            }
            // -- delete chromosomes from deictionary --
            foreach (var deadChromo in toBeDeletedA)
            {
                chromosomeToIdentifier.Remove(deadChromo);
            }

            List<int> toBeDeletedB = new List<int>();
            foreach (var chromo in identifierToChromosome.Values)
            {
                bool isInArchive = false;
                int key = identifierToChromosome.FirstOrDefault(x => x.Value.Equals(chromo)).Key;
                for (int i = 0; i < archive.Count; i++)
                {
                    MultiPisaChromosomeBase archChromo = archive[i];
                    if (chromo.Equals(archChromo))
                    {
                        isInArchive = true;   
                        break;             
                    }
                }
                if (!isInArchive)
                    if (key >= 0)
                        toBeDeletedB.Add(key);
            }
            // -- delete chromosomes from deictionary --
            foreach (var deadChromo in toBeDeletedB)
            {
                identifierToChromosome.Remove(deadChromo);
            }
        }


        public void SetArchive(List<MultiPisaChromosomeBase> chromosomes)
        {
            if (chromosomes != null)
            {
                string[] lines = new string[chromosomes.Count()+2];
                lines[0] = chromosomes.Count().ToString();
                for (int i = 1; i < lines.Length-1; i++)
                {
                    lines[i] = chromosomeToIdentifier[chromosomes[i-1]].ToString();
                }
                lines[lines.Length-1] = "END";

                File.WriteAllLines(ARCHIVE_FILE, lines);// StreamWriter file = new StreamWriter(Application.StartupPath +"\\" + fileName + ".lrn", false);//( fileName + ".txt", true);           
            }
        }

        public int GetChromosomeID(IMultiChromosome toTestChromo)
        {
            return chromosomeToIdentifier[toTestChromo];
        }

        //public IMultiChromosome GetChromosome(int id)
        //{
        //    return identifierToChromosome[id];
        //}

        private static void CreateFile(Dictionary<String, String> values, String filePath)
        {
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<String, String> entry in values)
            {
                sb.AppendFormat("{0} {1}\n", entry.Key, entry.Value);
            }

            File.WriteAllText(filePath, sb.ToString());
        }

        private static void CreateConfigFile(int initialSize, int populationSize, int selectionSize, int dim)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("alpha {0}\n", initialSize);
            sb.AppendFormat("mu {0}\n", selectionSize);
            sb.AppendFormat("lambda {0}\n", populationSize);
            sb.AppendFormat("dim {0}\n", dim);

            File.WriteAllText(CONFIG_FILE, sb.ToString());
        } 

        public static void PopulateFile(List<Tuple<int, IMultiChromosome>> chromosomes, String filePath, int dim)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(chromosomes.Count * (dim + 1)).AppendLine();
            foreach (Tuple<int, IMultiChromosome> t in chromosomes)
            {
                sb.Append(t.Item1);

                foreach (double value in t.Item2.FitnessValues)
                    sb.AppendFormat(" {0}", value);
                sb.AppendLine();
            }
            sb.AppendLine("END");

            File.WriteAllText(filePath, sb.ToString());
        }
    }
}

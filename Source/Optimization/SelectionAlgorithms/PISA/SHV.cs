﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = SHV.cs 
//  Copyright (C) 2014/10/18  1:02 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Pavol Bielik, Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;

namespace CPlan.Optimization.SelectionAlgorithms.PISA
{
    [Serializable]
    public class SHV : PisaWrapper
    {
        Dictionary<String, String> parameters = new Dictionary<String, String>();

        //provide default parameter which comes with PISA. No guarantees that these are actually any good.
        public SHV(int lambda, int mu, int dimension) : this(lambda, mu, dimension, 2, 2000, 100, 100) { }

        public SHV(int lambda, int mu, int dimension, int seed, int bound, int junks, int junksize)
            : base(lambda, mu, dimension)
        {
            parameters.Add("seed", seed.ToString());
            parameters.Add("bound", bound.ToString());
            parameters.Add("junks", junks.ToString());
            parameters.Add("junksize", junksize.ToString());
        }

        public override Dictionary<string, string> GetParams()
        {
            return parameters;
        }

        protected override string GetSelectorParamsPath()
        {
            return @"selectors\shv\shv_param.txt";
        }

        protected override string GetSelectorExecutablePath()
        {
            return @"selectors\shv\shv.exe";
        }
    }
}

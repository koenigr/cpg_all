﻿using OpenTK;
using QuickGraph;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Math;
using PriorityQueue;

public static class GraphAlgorithms
{

    /// <summary>
    /// Creates an empty triangular 2D array (~matrix).
    /// For any a, b where 0 &lt;= a,b &lt;= size the indexing array[Max(a,b), Min(a,b)] is a valid.
    /// </summary>
    /// <param name="size">Size of the array for "both" dimensions. (See the method description for more detail.)</param>
    /// <returns>Array with the described properties.</returns>
    public static float[][] CreateTriangular2DArray(int size)
    {
        Contract.Requires(size > 0);
        var array = new float[size][];
        for (int y = 0; y < size; ++y)
        {
            array[y] = new float[y + 1];
        }
        return array;
    }

    /// <summary>All pairs shortest path algorithm.</summary>
    public static float[][] FloydWarshall(UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> graph, Vector2d[] nodes, float[][] distance = null, IProgress<int> progress = null)
    {
        progress?.Report(0);
        var n = nodes.Length;
        var vectorToNode = nodes.Select((v, i) => Tuple.Create(i, v)).ToDictionary(i => i.Item2, i => i.Item1);
        if (distance == null) { distance = CreateTriangular2DArray(n); }
        //Initialise default distance (positive infinity)
        for (int i = 0; i < n; ++i)
        {
            for (int j = 0; j <= i; ++j) { distance[i][j] = float.PositiveInfinity; }
        }
        //Initialise node distances
        foreach (var edge in graph.Edges)
        {
            var length = (float)(edge.Target - edge.Source).Length;
            var item1 = vectorToNode[edge.Source];
            var item2 = vectorToNode[edge.Target];
            distance[Max(item1, item2)][Min(item1, item2)] = length;
        }
        //The distance to itself is 0 for each node
        for (int i = 0; i < n; ++i) { distance[i][i] = 0f; }
        //Floyd warshall algorithm for all pair shortest path
        var progressPercent = 100.0 / n;
        for (int a = 0; a < n; ++a)
        {
            for (int b = 0; b < n; ++b)
            {
                if (double.IsInfinity(distance[Max(a, b)][Min(a, b)])) { continue; }
                for (int c = 0; c < n; ++c)
                {
                    if (distance[Max(b, c)][Min(b, c)] > distance[Max(a, b)][Min(a, b)] + distance[Max(a, c)][Min(a, c)])
                    {
                        distance[Max(b, c)][Min(b, c)] = distance[Max(a, b)][Min(a, b)] + distance[Max(a, c)][Min(a, c)];
                    }
                }
            }
            progress?.Report((int)(a * progressPercent));
        }
        return distance;
    }

    public static float[][] AllPairsDijkstra(UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> graph, Vector2d[] nodes, float[][] distance = null, IProgress<int> progress = null)
    {
        if (distance == null) { distance = CreateTriangular2DArray(nodes.Length); }
        //Create simpler graph representation
        var myGraph = new List<Tuple<int, float>>[nodes.Length];
        var nodeLookup = nodes.Select((node, i) => Tuple.Create(node, i)).ToDictionary(node => node.Item1, node => node.Item2);
        for (int i = 0; i < nodes.Length; ++i)
        {
            myGraph[i] = new List<Tuple<int, float>>();
            foreach (var edge in graph.AdjacentEdges(nodes[i]))
            {
                var node = nodeLookup[nodes[i] == edge.Source ? edge.Target : edge.Source];
                myGraph[i].Add(Tuple.Create(node, (float)(edge.Target - edge.Source).Length));
            }
        }
        //Run dijkstra, starting from each node
        var progressPercent = 100.0 / nodes.Length;
        var progrssValue = 0;
        foreach (var result in Enumerable.Range(0, nodes.Length)
            .AsParallel().Select(node => Tuple.Create(node, FullDijkstra(myGraph, nodes, node)))
            .AsSequential())
        {
            for (var i = result.Item1; i >= 0; --i) { distance[result.Item1][i] = result.Item2[i]; }
            ++progrssValue;
            progress?.Report((int)(progrssValue * progressPercent));
        }
        return distance;
    }

    /// <summary>Dijkstra algorithm, which finds the shortest path from a target to all nodes.</summary>
    public static float[] FullDijkstra(List<Tuple<int, float>>[] graph, Vector2d[] nodes, int start)
    {
        var distance = new float[nodes.Length];
        var items = new Dictionary<int, PriorityQueue<Tuple<int, float>>.Item>();
        var finished = new bool[nodes.Length];
        var queue = new PriorityQueue<Tuple<int, float>>((a, b) => a.Item2.CompareTo(b.Item2));
        queue.Enqueue(Tuple.Create(start, 0f));
        while (queue.Count > 0)
        {
            var current = queue.Dequeue();
            if (items.ContainsKey(current.Item1)) { items.Remove(current.Item1); }
            distance[current.Item1] = current.Item2;
            finished[current.Item1] = true;
            foreach (var edge in graph[current.Item1])
            {
                if (finished[edge.Item1]) { continue; }
                var newEntry = Tuple.Create(edge.Item1, current.Item2 + edge.Item2);
                if (items.ContainsKey(edge.Item1))
                {
                    //Update only, if this edge is better than the previous one
                    if (newEntry.Item2 < items[edge.Item1].Data.Item2) { items[edge.Item1] = queue.Update(items[edge.Item1], newEntry); }
                }
                else { items[edge.Item1] = queue.Enqueue(newEntry); }
            }
        }
        return distance;
    }
}

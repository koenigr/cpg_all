﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPlan.Optimization
{
    public static class IEnumerableExtensions
    {
        private static T MinMaxBy<T>(this IEnumerable<T> enumerable, Func<T, IComparable> map, int factor = 1)
        {
            Contract.Requires(map != null);
            Contract.Requires(enumerable != null);

            var best = enumerable.FirstOrDefault();
            var mappedBest = map(best);
            foreach (var current in enumerable.Skip(1))
            {
                var mappedCurrent = map(current);
                if (mappedCurrent != null && factor * mappedCurrent.CompareTo(mappedBest) < 0)
                {
                    best = current;
                    mappedBest = mappedCurrent;
                }
            }
            return best;
        }

        /// <summary>Finds the minimal element with the given mapping function.</summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="map"></param>
        /// <returns>The minimal element.</returns>
        public static T MinBy<T>(this IEnumerable<T> enumerable, Func<T, IComparable> map) => enumerable.MinMaxBy(map, 1);

        /// <summary>Finds the maximal element with the given mapping function.</summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="map"></param>
        /// <returns>The maximal element.</returns>
        public static T MaxBy<T>(this IEnumerable<T> enumerable, Func<T, IComparable> map) => enumerable.MinMaxBy(map, -1);

        /// <summary>
        /// Finds min, max and calculate sum and count.
        /// </summary>
        /// <typeparam name="T">Number type (int, double)</typeparam>
        /// <param name="enumerable"></param>
        /// <param name="map"></param>
        /// <returns>Min, Max and Calculated Sum and Count.</returns>
        public static MinMaxSum<double> MinMaxSum<T>(this IEnumerable<T> enumerable, Func<T, double> map)
        {
            Contract.Requires(enumerable != null);
            Contract.Requires(map != null);

            if(!enumerable.Any())
            {
                return new MinMaxSum<double>(double.NaN, double.NaN, 0.0, 0);
            }
            var first = map(enumerable.First());
            var minMaxAreas = enumerable.Skip(1).Select(map).Aggregate(new MinMaxSum<double>(first, first, first, 1), (minMax, value) => {
                return new MinMaxSum<double>(Math.Min(minMax.Min, value), Math.Max(minMax.Max, value), minMax.Sum + value, minMax.Count + 1);
            });

            return minMaxAreas;
        }

        /// <summary>Finds min, max and calculate sum and count.</summary>
        /// <param name="enumerable"></param>
        /// <returns>Min, Max and Calculated Sum and Count.</returns>
        public static MinMaxSum<double> MinMaxSum(this IEnumerable<double> enumerable) => enumerable.MinMaxSum(d => d);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPlan.Optimization
{
    public class MinMaxSum<T>
    {
        public T Min { get; }
        public T Max { get; }
        public T Sum { get; }
        public int Count { get; }

        public MinMaxSum(T min, T max, T sum, int count)
        {
            Min = min;
            Max = max;
            Sum = sum;
            Count = count;
        }
    }
}

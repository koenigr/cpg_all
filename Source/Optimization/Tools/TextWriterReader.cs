﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = TextWriterReader.cs 
//  Copyright (C) 2014/10/18  12:57 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using CPlan.Geometry;

namespace CPlan.Optimization
{
    public class TextRW
    {
        public static List<int> LucyIDs = new List<int>();   

        //============================================================================================================================================================================
        /// <summary>
        /// Write the (best) fitness values in a text file.
        /// </summary>
        public static void WriteFitnessValues(List<List<double>> fitnessList)//, string fileName)
        {
            string fileName = Application.StartupPath + "\\FitnessValues.txt";
            StreamWriter file = new StreamWriter(fileName, false);
            
            if (fitnessList != null)
            {
                // -- write the lines into the text file --
                for (int i = 0; i < fitnessList.Count(); i++)
                {
                    List<double> fitValues = fitnessList[i];
                    string fitValLine = i.ToString(); // iteration counter

                    foreach (double value in fitValues)
                    {
                        fitValLine += ", " + value.ToString(); // add the fitness values as comma separated values                       
                    }
                    file.WriteLine(fitValLine); // add a new line to the text file
                }
            }
            file.Close();
        }



        //============================================================================================================================================================================
        /// <summary>
        /// Write a .lrn file as specified here:
        /// http://databionic-esom.sourceforge.net/user.html#File_formats
        /// This method writes uses the fitness values of a population
        /// </summary>
        /// <param name="chromosomes">List of chromosomes. 
        /// These are usually the ones from the archive of the multi-criteria-selection mechanism. </param>
        /// <param name="fileName">The name for the file to be saved. </param>
        public static void WriteLrn(MultiPopulation population, string fileName)
        {        
            List<string> lines = new List<string>();
            //fileName = Application.StartupPath + fileName;
            StreamWriter file = new StreamWriter(fileName, false);//( fileName + ".txt", true);

            if (population != null)
            {
                List<MultiPisaChromosomeBase> chromosomes = population.Sel.GetArchiveOrig();               
                // -- to handle fitness values for normalisation etc --
                double[][] fitnessValues = new double[chromosomes[0].FitnessValues.Count()][];
                for (int i = 0; i < chromosomes[0].FitnessValues.Count(); i++)
                {
                    fitnessValues[i] = new double[chromosomes.Count()];
                }
                for (int i = 0; i < chromosomes.Count(); i++)
                {
                    MultiPisaChromosomeBase curChromo = chromosomes[i];
                    for (int k = 0; k < curChromo.FitnessValues.Count; k++)
                    {
                        fitnessValues[k][i] = curChromo.FitnessValues[k];
                    }
                }

                // -- rescale and normalise --
                for (int i = 0; i < chromosomes[0].FitnessValues.Count(); i++)
                {
                    double avg = fitnessValues[i].Average();
                    double stdev = fitnessValues[i].StdDev();
                    for (int k = 0; k < fitnessValues[i].Count(); k++)
                    {
                        fitnessValues[i][k] -= avg;
                        fitnessValues[i][k] /= stdev;
                    }

                    double min = CPlan.Geometry.Tools.GetMin(fitnessValues[i]);
                    double max = CPlan.Geometry.Tools.GetMax(fitnessValues[i]);

                    CPlan.Geometry.Tools.CutMinValue(fitnessValues[i], CPlan.Geometry.Tools.GetMin(fitnessValues[i]));
                    CPlan.Geometry.Tools.Normalize(fitnessValues[i]);
                    CPlan.Geometry.Tools.IntervalExtension(fitnessValues[i], CPlan.Geometry.Tools.GetMax(fitnessValues[i]));

                    for (int k = 0; k < fitnessValues[i].Count(); k++)
                    {
                        fitnessValues[i][k] = Math.Round(Convert.ToSingle(fitnessValues[i][k]), 2);
                    }
                }

                // -- write file ---------------------------------------------------------
                // -----------------------------------------------------------------------
                // -- file header --------------------------------------------------------
                lines.Add("# Fitness Values from CPlan | Generation:");
                lines.Add("# " + population.EpochCounter.ToString());
                //lines.Add("# " + "Avg: ");
                //lines.Add("# " + "StdDev: ");
                //lines.Add("# " + "Min: ");
                //lines.Add("# " + "Max: ");
                lines.Add("% " + chromosomes.Count().ToString());
                lines.Add("% " + (chromosomes[0].FitnessValues.Count() + 1).ToString());

                // -- add the column-feature names and type of column --
                string type = "% 9";
                string name = "% ID";
                for (int k = 0; k < chromosomes[0].FitnessValues.Count; k++)
                {
                    type += "\t" + "1";
                    name += "\t" + "feature" + (k + 1).ToString();
                }
                lines.Add(type);
                lines.Add(name);

                // -- add the fitness values of each chromosome per line --         
                for (int i = 0; i < chromosomes.Count(); i++)
                {
                    MultiPisaChromosomeBase curChromo = chromosomes[i];

                    string line = curChromo.Indentity.ToString();// population.Sel.GetChromosomeID(curChromo).ToString(); //i.ToString();
                    for (int k = 0; k < curChromo.FitnessValues.Count; k++)
                    {
                        if (fitnessValues[k][i] == 0) 
                            line += "\t" + "0.0";
                        else if (fitnessValues[k][i] == 1) 
                            line += "\t" + "1.00";
                        else 
                            line += "\t" + fitnessValues[k][i].ToString();
                    }
                    lines.Add(line);
                }
            }
            else
            {
                lines.Add("# Error: No population found.");
            }

            // -- write the lines into the text file --
            foreach (string curline in lines)
            {
                file.WriteLine(curline);
            }
            file.Close();         
        }


        //============================================================================================================================================================================
        /// <summary>
        /// Write a .lrn file as specified here:
        /// http://databionic-esom.sourceforge.net/user.html#File_formats
        /// This method writes uses the fingerpring values of layout chromosomes
        /// </summary>
        /// <param name="chromosomes">List of chromosomes. 
        /// These are usually the ones from the archive of the multi-criteria-selection mechanism. </param>
        /// <param name="fileName">The name for the file to be saved. </param>
        public static void WriteLrnID(List<string> fingerprint, string fileName)
        {
            StreamWriter file = new StreamWriter(fileName, false);

            if (fingerprint == null)
            {
                fingerprint = new List<string>();
                fingerprint.Add("# Error: No population found.");
            }

            // -- write the lines into the text file --
            foreach (string curline in fingerprint)
            {
                file.WriteLine(curline);
            }
            file.Close();
        }


        //============================================================================================================================================================================
        /// <summary>
        /// Read a .bm file as specified here:
        /// http://databionic-esom.sourceforge.net/user.html#File_formats 
        /// The *.bm file contains the index of a data point and the coordinates of it's best match. The best match is the neuron, on which the data-point is projected by the training algorithm.
        /// % k l
        /// % n
        /// index1		k1	l1
        /// index2		k2	l2
        /// .		.	.
        /// .		.	.
        /// indexn		kn	ln
        /// k	Number of rows of the feature map.
        /// l	Number of columns of the feature map.
        /// indexi	Index of best match i. Corresponds to the index of the i-th data-point in the *.lrn file.
        /// ki	Row of the i-th best match.
        /// li	Column of the i-th best match.
        /// </summary>
        public static int[,] ReadBm(string fileName)
        {
            string[] lines = File.ReadAllLines(fileName);   //Application.StartupPath + "\\" + fileName + ".bm"); //(fileName);
            string lrnFileName = fileName.Remove(fileName.Length-2, 2);
            lrnFileName += "lrn";
            string[] lrnLines = File.ReadAllLines(lrnFileName);
            int [,] somMap;
            // -- read how many entries (lines) are filled with values.
            string[] values = lines[1].Split('%');
            int nrEntries = int.Parse(values[1]);
            somMap = new int[nrEntries, 3];

            for (int i = 2; i < lines.Count(); i++)
            {
                //values = lines[i].Split('\t');
                values = lines[i].Split(' ');
                //somMap[i - 2, 0] = int.Parse(values[0]); // take id from lrn - ids are changed by somoclu
                somMap[i - 2, 1] = int.Parse(values[1]);
                somMap[i - 2, 2] = int.Parse(values[2]);
            }
            for (int i = 6; i < lrnLines.Count(); i++)
            {
                values = lrnLines[i].Split('\t');
                //values = lines[i].Split(' ');
                somMap[i - 6, 0] = int.Parse(values[0]); 
            }
            return somMap;
        }


        public static float[,] ReadTabText(string fileName, out string[] titles)
        {
            string[] lines = File.ReadAllLines(fileName);   //Application.StartupPath + "\\" + fileName + ".bm"); //(fileName);        
            // -- read how many entries (lines) are filled with values.
            titles = lines[0].Split('\t');
            float[,] map = new float[lines.Length - 1, titles.Length];

            for (int i = 1; i < lines.Count(); i++)
            {
                string[] values = lines[i].Split('\t');
                for (int k = 0; k < titles.Count(); k++)
                {                  
                    //values = lines[i].Split(' ');
                    map[i, k] = float.Parse(values[k]);
                }
            }

            return map;
        }

    }
}

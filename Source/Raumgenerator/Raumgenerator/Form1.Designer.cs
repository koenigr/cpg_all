﻿namespace Raumgenerator
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.glCameraControl1 = new CPlan.VisualObjects.GLCameraControl();
            this.SuspendLayout();
            // 
            // glCameraControl1
            // 
            this.glCameraControl1.AllObjectsList = null;
            this.glCameraControl1.BackColor = System.Drawing.Color.Black;
            this.glCameraControl1.LoadedScene = false;
            this.glCameraControl1.Location = new System.Drawing.Point(170, 3);
            this.glCameraControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.glCameraControl1.Name = "glCameraControl1";
            this.glCameraControl1.ObjectInteraction = true;
            this.glCameraControl1.Size = new System.Drawing.Size(724, 628);
            this.glCameraControl1.SwapBuffersActivated = false;
            this.glCameraControl1.TabIndex = 0;
            this.glCameraControl1.VSync = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 635);
            this.Controls.Add(this.glCameraControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private CPlan.VisualObjects.GLCameraControl glCameraControl1;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPlan.VisualObjects;
using OpenTK.Graphics.OpenGL;


namespace Raumgenerator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            GRectangle myRect = new GRectangle(1, 1, 200, 100);
            myRect.Filled = true;
            //myRect.FillColor = ConvertColor.CreateColor4(Color.LightGray);
            this.glCameraControl1.AllObjectsList.Add(myRect, true, true);
            this.glCameraControl1.ZoomAll();
            this.glCameraControl1.Invalidate();
            this.glCameraControl1.AllObjectsList.Add(myRect, true, true);
        }

        private void glCameraControl1_Resize(object sender, EventArgs e)
        {
            var curCtrl = (GLCameraControl)sender;
            if (!curCtrl.Loaded) return;
            int w = curCtrl.Width;
            int h = curCtrl.Height;
            curCtrl.MakeCurrent();
            GL.Viewport(0, 0, w, h);
        }
    }
}

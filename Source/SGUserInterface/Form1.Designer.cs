﻿//#if CT_UNCOMMENT

namespace CPlan.UrbanPattern
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.glControl1 = new OpenTK.GLControl();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.glControl2 = new OpenTK.GLControl();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.propertyGrid3 = new System.Windows.Forms.PropertyGrid();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.propertyGrid2 = new System.Windows.Forms.PropertyGrid();
            this.comboBox_Isovist = new System.Windows.Forms.ComboBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cB_autoShot = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rB_fitCentral = new System.Windows.Forms.RadioButton();
            this.rB_fitChoice = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.uD_lineWidth = new System.Windows.Forms.NumericUpDown();
            this.cb_showBestGlobal = new System.Windows.Forms.RadioButton();
            this.cb_showBestGen = new System.Windows.Forms.RadioButton();
            this.cb_showAll = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.sB_multiOpti = new System.Windows.Forms.RadioButton();
            this.sB_singleOpti = new System.Windows.Forms.RadioButton();
            this.lbl_nDSet = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.uD_treeDepth = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.uD_populationSize = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.uD_generations = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.uD_minLength = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.uD_maxLength = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.uD_divAngle = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.uD_distance = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.uD_angle = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.uD_connectivity = new System.Windows.Forms.NumericUpDown();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.B_Analyse = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.ButtonStreets = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ButtonBuildings = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_LoadScene = new System.Windows.Forms.ToolStripButton();
            this.ButtonBuildingLayout = new System.Windows.Forms.ToolStripButton();
            this.Button_IsovistAnalysis = new System.Windows.Forms.ToolStripButton();
            this.Button_StartStop = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.Button_OptiLayout = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.Button_Move = new System.Windows.Forms.ToolStripButton();
            this.Button_Rotate = new System.Windows.Forms.ToolStripButton();
            this.Button_Scale = new System.Windows.Forms.ToolStripButton();
            this.Button_Size = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportDxfGraphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadDxfFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadGraphFromDxfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.dateiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.saveDxfDialog = new System.Windows.Forms.SaveFileDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.openDxfDialog = new System.Windows.Forms.OpenFileDialog();
            this.openDxfGraphDialog = new System.Windows.Forms.OpenFileDialog();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_lineWidth)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_treeDepth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_populationSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_generations)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_minLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_maxLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_divAngle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_distance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_angle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_connectivity)).BeginInit();
            this.toolStrip3.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // glControl1
            // 
            this.glControl1.BackColor = System.Drawing.Color.Black;
            this.glControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.glControl1.Location = new System.Drawing.Point(0, 0);
            this.glControl1.Name = "glControl1";
            this.glControl1.Size = new System.Drawing.Size(454, 681);
            this.glControl1.TabIndex = 0;
            this.glControl1.VSync = false;
            this.glControl1.Load += new System.EventHandler(this.glControl1_Load);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.toolStrip3);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.toolStrip1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1249, 718);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(1249, 764);
            this.toolStripContainer1.TabIndex = 1;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.menuStrip1);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1249, 22);
            this.statusStrip1.TabIndex = 0;
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(16, 17);
            this.statusLabel.Text = "...";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(92, 37);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Size = new System.Drawing.Size(1157, 681);
            this.splitContainer1.SplitterDistance = 851;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.glControl1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.glControl2);
            this.splitContainer2.Size = new System.Drawing.Size(851, 681);
            this.splitContainer2.SplitterDistance = 454;
            this.splitContainer2.TabIndex = 1;
            // 
            // glControl2
            // 
            this.glControl2.BackColor = System.Drawing.Color.Black;
            this.glControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.glControl2.Location = new System.Drawing.Point(0, 0);
            this.glControl2.Name = "glControl2";
            this.glControl2.Size = new System.Drawing.Size(393, 681);
            this.glControl2.TabIndex = 1;
            this.glControl2.VSync = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(302, 681);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.propertyGrid3);
            this.tabPage2.Controls.Add(this.splitter2);
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.splitter1);
            this.tabPage2.Controls.Add(this.propertyGrid1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(294, 655);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "BuildingLayout";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // propertyGrid3
            // 
            this.propertyGrid3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid3.Location = new System.Drawing.Point(3, 509);
            this.propertyGrid3.Name = "propertyGrid3";
            this.propertyGrid3.Size = new System.Drawing.Size(288, 143);
            this.propertyGrid3.TabIndex = 10;
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.splitter2.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter2.Location = new System.Drawing.Point(3, 505);
            this.splitter2.MinimumSize = new System.Drawing.Size(0, 4);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(288, 4);
            this.splitter2.TabIndex = 9;
            this.splitter2.TabStop = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.propertyGrid2);
            this.groupBox6.Controls.Add(this.comboBox_Isovist);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Location = new System.Drawing.Point(3, 251);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(288, 254);
            this.groupBox6.TabIndex = 7;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Isovist Field";
            // 
            // propertyGrid2
            // 
            this.propertyGrid2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid2.Location = new System.Drawing.Point(3, 37);
            this.propertyGrid2.Name = "propertyGrid2";
            this.propertyGrid2.Size = new System.Drawing.Size(282, 214);
            this.propertyGrid2.TabIndex = 4;
            this.propertyGrid2.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid2_PropertyValueChanged);
            // 
            // comboBox_Isovist
            // 
            this.comboBox_Isovist.Dock = System.Windows.Forms.DockStyle.Top;
            this.comboBox_Isovist.FormattingEnabled = true;
            this.comboBox_Isovist.Items.AddRange(new object[] {
            "Area",
            "Perimeter",
            "Compactness",
            "Min Radial",
            "Max Radial",
            "Occlusivity"});
            this.comboBox_Isovist.Location = new System.Drawing.Point(3, 16);
            this.comboBox_Isovist.Name = "comboBox_Isovist";
            this.comboBox_Isovist.Size = new System.Drawing.Size(282, 21);
            this.comboBox_Isovist.TabIndex = 3;
            this.comboBox_Isovist.Text = "Area";
            this.comboBox_Isovist.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.splitter1.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(3, 247);
            this.splitter1.MinimumSize = new System.Drawing.Size(0, 4);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(288, 4);
            this.splitter1.TabIndex = 6;
            this.splitter1.TabStop = false;
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Top;
            this.propertyGrid1.Location = new System.Drawing.Point(3, 3);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(288, 244);
            this.propertyGrid1.TabIndex = 0;
            this.propertyGrid1.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid1_PropertyValueChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.chart1);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(294, 655);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "StreetNetwork";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(3, 498);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastPoint;
            series1.IsVisibleInLegend = false;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastPoint;
            series2.IsVisibleInLegend = false;
            series2.Legend = "Legend1";
            series2.MarkerColor = System.Drawing.Color.Black;
            series2.Name = "Series2";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Size = new System.Drawing.Size(288, 154);
            this.chart1.TabIndex = 36;
            this.chart1.Text = "chart1";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.cB_autoShot);
            this.groupBox5.Controls.Add(this.button1);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Location = new System.Drawing.Point(3, 431);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(288, 67);
            this.groupBox5.TabIndex = 35;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Screenshot";
            // 
            // cB_autoShot
            // 
            this.cB_autoShot.AutoSize = true;
            this.cB_autoShot.Location = new System.Drawing.Point(18, 19);
            this.cB_autoShot.Name = "cB_autoShot";
            this.cB_autoShot.Size = new System.Drawing.Size(129, 17);
            this.cB_autoShot.TabIndex = 32;
            this.cB_autoShot.Text = "automatic Screenshot";
            this.cB_autoShot.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 37);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(152, 23);
            this.button1.TabIndex = 27;
            this.button1.Text = "Screenshot";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rB_fitCentral);
            this.groupBox4.Controls.Add(this.rB_fitChoice);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(3, 366);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(288, 65);
            this.groupBox4.TabIndex = 32;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Fitness";
            // 
            // rB_fitCentral
            // 
            this.rB_fitCentral.AutoSize = true;
            this.rB_fitCentral.Location = new System.Drawing.Point(9, 38);
            this.rB_fitCentral.Name = "rB_fitCentral";
            this.rB_fitCentral.Size = new System.Drawing.Size(87, 17);
            this.rB_fitCentral.TabIndex = 1;
            this.rB_fitCentral.Text = "use centrality";
            this.rB_fitCentral.UseVisualStyleBackColor = true;
            // 
            // rB_fitChoice
            // 
            this.rB_fitChoice.AutoSize = true;
            this.rB_fitChoice.Checked = true;
            this.rB_fitChoice.Location = new System.Drawing.Point(9, 19);
            this.rB_fitChoice.Name = "rB_fitChoice";
            this.rB_fitChoice.Size = new System.Drawing.Size(77, 17);
            this.rB_fitChoice.TabIndex = 0;
            this.rB_fitChoice.TabStop = true;
            this.rB_fitChoice.Text = "use choice";
            this.rB_fitChoice.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.uD_lineWidth);
            this.groupBox2.Controls.Add(this.cb_showBestGlobal);
            this.groupBox2.Controls.Add(this.cb_showBestGen);
            this.groupBox2.Controls.Add(this.cb_showAll);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(3, 258);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(288, 108);
            this.groupBox2.TabIndex = 31;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Visualization";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 83);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "Line Width:";
            // 
            // uD_lineWidth
            // 
            this.uD_lineWidth.Location = new System.Drawing.Point(71, 80);
            this.uD_lineWidth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_lineWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_lineWidth.Name = "uD_lineWidth";
            this.uD_lineWidth.Size = new System.Drawing.Size(42, 20);
            this.uD_lineWidth.TabIndex = 25;
            this.uD_lineWidth.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.uD_lineWidth.ValueChanged += new System.EventHandler(this.uD_treeDepth_ValueChanged_1);
            // 
            // cb_showBestGlobal
            // 
            this.cb_showBestGlobal.AutoSize = true;
            this.cb_showBestGlobal.Location = new System.Drawing.Point(9, 58);
            this.cb_showBestGlobal.Name = "cb_showBestGlobal";
            this.cb_showBestGlobal.Size = new System.Drawing.Size(104, 17);
            this.cb_showBestGlobal.TabIndex = 2;
            this.cb_showBestGlobal.Text = "show best global";
            this.cb_showBestGlobal.UseVisualStyleBackColor = true;
            // 
            // cb_showBestGen
            // 
            this.cb_showBestGen.AutoSize = true;
            this.cb_showBestGen.Location = new System.Drawing.Point(9, 38);
            this.cb_showBestGen.Name = "cb_showBestGen";
            this.cb_showBestGen.Size = new System.Drawing.Size(129, 17);
            this.cb_showBestGen.TabIndex = 1;
            this.cb_showBestGen.Text = "show best gerneration";
            this.cb_showBestGen.UseVisualStyleBackColor = true;
            // 
            // cb_showAll
            // 
            this.cb_showAll.AutoSize = true;
            this.cb_showAll.Checked = true;
            this.cb_showAll.Location = new System.Drawing.Point(9, 19);
            this.cb_showAll.Name = "cb_showAll";
            this.cb_showAll.Size = new System.Drawing.Size(120, 17);
            this.cb_showAll.TabIndex = 0;
            this.cb_showAll.TabStop = true;
            this.cb_showAll.Text = "show all populations";
            this.cb_showAll.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.sB_multiOpti);
            this.groupBox3.Controls.Add(this.sB_singleOpti);
            this.groupBox3.Controls.Add(this.lbl_nDSet);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.uD_treeDepth);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.uD_populationSize);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.uD_generations);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(3, 162);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(288, 96);
            this.groupBox3.TabIndex = 27;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "General";
            // 
            // sB_multiOpti
            // 
            this.sB_multiOpti.AutoSize = true;
            this.sB_multiOpti.Checked = true;
            this.sB_multiOpti.Location = new System.Drawing.Point(173, 50);
            this.sB_multiOpti.Name = "sB_multiOpti";
            this.sB_multiOpti.Size = new System.Drawing.Size(138, 17);
            this.sB_multiOpti.TabIndex = 28;
            this.sB_multiOpti.TabStop = true;
            this.sB_multiOpti.Text = "multi-criteria optimization";
            this.sB_multiOpti.UseVisualStyleBackColor = true;
            // 
            // sB_singleOpti
            // 
            this.sB_singleOpti.AutoSize = true;
            this.sB_singleOpti.Location = new System.Drawing.Point(173, 28);
            this.sB_singleOpti.Name = "sB_singleOpti";
            this.sB_singleOpti.Size = new System.Drawing.Size(144, 17);
            this.sB_singleOpti.TabIndex = 27;
            this.sB_singleOpti.Text = "single criteria optimization";
            this.sB_singleOpti.UseVisualStyleBackColor = true;
            // 
            // lbl_nDSet
            // 
            this.lbl_nDSet.AutoSize = true;
            this.lbl_nDSet.Location = new System.Drawing.Point(264, 69);
            this.lbl_nDSet.Name = "lbl_nDSet";
            this.lbl_nDSet.Size = new System.Drawing.Size(25, 13);
            this.lbl_nDSet.TabIndex = 26;
            this.lbl_nDSet.Text = "000";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(170, 69);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "non-dominated set:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(30, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Tree Depth:";
            // 
            // uD_treeDepth
            // 
            this.uD_treeDepth.Location = new System.Drawing.Point(98, 69);
            this.uD_treeDepth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_treeDepth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_treeDepth.Name = "uD_treeDepth";
            this.uD_treeDepth.Size = new System.Drawing.Size(55, 20);
            this.uD_treeDepth.TabIndex = 23;
            this.uD_treeDepth.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.uD_treeDepth.ValueChanged += new System.EventHandler(this.uD_treeDepth_ValueChanged_1);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 49);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Population Size:";
            // 
            // uD_populationSize
            // 
            this.uD_populationSize.Location = new System.Drawing.Point(98, 47);
            this.uD_populationSize.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_populationSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_populationSize.Name = "uD_populationSize";
            this.uD_populationSize.Size = new System.Drawing.Size(55, 20);
            this.uD_populationSize.TabIndex = 21;
            this.uD_populationSize.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.uD_populationSize.ValueChanged += new System.EventHandler(this.uD_treeDepth_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Generations:";
            // 
            // uD_generations
            // 
            this.uD_generations.Location = new System.Drawing.Point(98, 25);
            this.uD_generations.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_generations.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_generations.Name = "uD_generations";
            this.uD_generations.Size = new System.Drawing.Size(55, 20);
            this.uD_generations.TabIndex = 19;
            this.uD_generations.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.uD_generations.ValueChanged += new System.EventHandler(this.uD_treeDepth_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.uD_minLength);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.uD_maxLength);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.uD_divAngle);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.uD_distance);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.uD_angle);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.uD_connectivity);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(288, 159);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Morphological Parameter";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "min segment length:";
            // 
            // uD_minLength
            // 
            this.uD_minLength.Location = new System.Drawing.Point(116, 134);
            this.uD_minLength.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.uD_minLength.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_minLength.Name = "uD_minLength";
            this.uD_minLength.Size = new System.Drawing.Size(42, 20);
            this.uD_minLength.TabIndex = 27;
            this.uD_minLength.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.uD_minLength.ValueChanged += new System.EventHandler(this.uD_treeDepth_ValueChanged_1);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 114);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "max segment length:";
            // 
            // uD_maxLength
            // 
            this.uD_maxLength.Location = new System.Drawing.Point(116, 112);
            this.uD_maxLength.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.uD_maxLength.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_maxLength.Name = "uD_maxLength";
            this.uD_maxLength.Size = new System.Drawing.Size(42, 20);
            this.uD_maxLength.TabIndex = 25;
            this.uD_maxLength.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.uD_maxLength.ValueChanged += new System.EventHandler(this.uD_treeDepth_ValueChanged_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "min division angle:";
            // 
            // uD_divAngle
            // 
            this.uD_divAngle.Enabled = false;
            this.uD_divAngle.Location = new System.Drawing.Point(116, 90);
            this.uD_divAngle.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.uD_divAngle.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_divAngle.Name = "uD_divAngle";
            this.uD_divAngle.Size = new System.Drawing.Size(42, 20);
            this.uD_divAngle.TabIndex = 23;
            this.uD_divAngle.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.uD_divAngle.ValueChanged += new System.EventHandler(this.uD_treeDepth_ValueChanged_1);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "min distances:";
            // 
            // uD_distance
            // 
            this.uD_distance.Enabled = false;
            this.uD_distance.Location = new System.Drawing.Point(116, 68);
            this.uD_distance.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.uD_distance.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_distance.Name = "uD_distance";
            this.uD_distance.Size = new System.Drawing.Size(42, 20);
            this.uD_distance.TabIndex = 21;
            this.uD_distance.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.uD_distance.ValueChanged += new System.EventHandler(this.uD_treeDepth_ValueChanged_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "max random angle:";
            // 
            // uD_angle
            // 
            this.uD_angle.Location = new System.Drawing.Point(116, 46);
            this.uD_angle.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.uD_angle.Name = "uD_angle";
            this.uD_angle.Size = new System.Drawing.Size(42, 20);
            this.uD_angle.TabIndex = 19;
            this.uD_angle.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.uD_angle.ValueChanged += new System.EventHandler(this.uD_treeDepth_ValueChanged_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "max connectivity:";
            // 
            // uD_connectivity
            // 
            this.uD_connectivity.Location = new System.Drawing.Point(116, 24);
            this.uD_connectivity.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.uD_connectivity.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_connectivity.Name = "uD_connectivity";
            this.uD_connectivity.Size = new System.Drawing.Size(42, 20);
            this.uD_connectivity.TabIndex = 17;
            this.uD_connectivity.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.uD_connectivity.ValueChanged += new System.EventHandler(this.uD_treeDepth_ValueChanged_1);
            // 
            // toolStrip3
            // 
            this.toolStrip3.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton4,
            this.toolStripButton5,
            this.B_Analyse,
            this.toolStripSeparator5,
            this.ButtonStreets,
            this.toolStripButton6,
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripSeparator1,
            this.ButtonBuildings,
            this.toolStripSeparator2,
            this.toolStripButton_LoadScene,
            this.ButtonBuildingLayout,
            this.Button_IsovistAnalysis,
            this.Button_StartStop,
            this.toolStripSeparator4,
            this.Button_OptiLayout});
            this.toolStrip3.Location = new System.Drawing.Point(0, 37);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Size = new System.Drawing.Size(92, 681);
            this.toolStrip3.TabIndex = 1;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(89, 19);
            this.toolStripButton4.Text = "Ini.StreetNet";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(89, 19);
            this.toolStripButton5.Text = "GrowNetwork";
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click_1);
            // 
            // B_Analyse
            // 
            this.B_Analyse.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.B_Analyse.Image = ((System.Drawing.Image)(resources.GetObject("B_Analyse.Image")));
            this.B_Analyse.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.B_Analyse.Name = "B_Analyse";
            this.B_Analyse.Size = new System.Drawing.Size(89, 19);
            this.B_Analyse.Text = "Analyse";
            this.B_Analyse.Click += new System.EventHandler(this.B_Analyse_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(89, 6);
            // 
            // ButtonStreets
            // 
            this.ButtonStreets.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ButtonStreets.Image = ((System.Drawing.Image)(resources.GetObject("ButtonStreets.Image")));
            this.ButtonStreets.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButtonStreets.Name = "ButtonStreets";
            this.ButtonStreets.Size = new System.Drawing.Size(89, 19);
            this.ButtonStreets.Text = "TestTree";
            this.ButtonStreets.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(89, 19);
            this.toolStripButton6.Text = "OptiNetwork";
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(89, 19);
            this.toolStripButton7.Text = "StopOpti";
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton8.Image")));
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(89, 19);
            this.toolStripButton8.Text = "Reset";
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(89, 6);
            // 
            // ButtonBuildings
            // 
            this.ButtonBuildings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ButtonBuildings.Image = ((System.Drawing.Image)(resources.GetObject("ButtonBuildings.Image")));
            this.ButtonBuildings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButtonBuildings.Name = "ButtonBuildings";
            this.ButtonBuildings.Size = new System.Drawing.Size(89, 19);
            this.ButtonBuildings.Text = "TestBuildings";
            this.ButtonBuildings.Click += new System.EventHandler(this.ButtonBuildings_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(89, 6);
            // 
            // toolStripButton_LoadScene
            // 
            this.toolStripButton_LoadScene.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_LoadScene.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_LoadScene.Image")));
            this.toolStripButton_LoadScene.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_LoadScene.Name = "toolStripButton_LoadScene";
            this.toolStripButton_LoadScene.Size = new System.Drawing.Size(89, 19);
            this.toolStripButton_LoadScene.Text = "LoadScene";
            this.toolStripButton_LoadScene.Click += new System.EventHandler(this.toolStripButton_LoadScene_Click);
            // 
            // ButtonBuildingLayout
            // 
            this.ButtonBuildingLayout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ButtonBuildingLayout.Image = ((System.Drawing.Image)(resources.GetObject("ButtonBuildingLayout.Image")));
            this.ButtonBuildingLayout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButtonBuildingLayout.Name = "ButtonBuildingLayout";
            this.ButtonBuildingLayout.Size = new System.Drawing.Size(89, 19);
            this.ButtonBuildingLayout.Text = "BuildingLayout";
            this.ButtonBuildingLayout.Click += new System.EventHandler(this.ButtonBuildingLayout_Click);
            // 
            // Button_IsovistAnalysis
            // 
            this.Button_IsovistAnalysis.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.Button_IsovistAnalysis.Image = ((System.Drawing.Image)(resources.GetObject("Button_IsovistAnalysis.Image")));
            this.Button_IsovistAnalysis.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Button_IsovistAnalysis.Name = "Button_IsovistAnalysis";
            this.Button_IsovistAnalysis.Size = new System.Drawing.Size(89, 19);
            this.Button_IsovistAnalysis.Text = "IsovistAnalysis";
            this.Button_IsovistAnalysis.Click += new System.EventHandler(this.Button_IsovistAnalysis_Click);
            // 
            // Button_StartStop
            // 
            this.Button_StartStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.Button_StartStop.Image = ((System.Drawing.Image)(resources.GetObject("Button_StartStop.Image")));
            this.Button_StartStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Button_StartStop.Name = "Button_StartStop";
            this.Button_StartStop.Size = new System.Drawing.Size(89, 19);
            this.Button_StartStop.Text = "Start_Stop";
            this.Button_StartStop.Click += new System.EventHandler(this.Button_StartStop_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(89, 6);
            // 
            // Button_OptiLayout
            // 
            this.Button_OptiLayout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.Button_OptiLayout.Image = ((System.Drawing.Image)(resources.GetObject("Button_OptiLayout.Image")));
            this.Button_OptiLayout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Button_OptiLayout.Name = "Button_OptiLayout";
            this.Button_OptiLayout.Size = new System.Drawing.Size(89, 19);
            this.Button_OptiLayout.Text = "OptiLayout";
            this.Button_OptiLayout.Click += new System.EventHandler(this.Button_OptiLayout_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(30, 30);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton9,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripSeparator3,
            this.Button_Move,
            this.Button_Rotate,
            this.Button_Scale,
            this.Button_Size});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1249, 37);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton9.Image = global::CPlan.SGUserInterface.Properties.Resources.Mouse_Drag;
            this.toolStripButton9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.Size = new System.Drawing.Size(34, 34);
            this.toolStripButton9.Text = "ScreenMove";
            this.toolStripButton9.Click += new System.EventHandler(this.toolStripButton9_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = global::CPlan.SGUserInterface.Properties.Resources.ZoomAll2;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(34, 34);
            this.toolStripButton2.Text = "Zoom all";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::CPlan.SGUserInterface.Properties.Resources.V2D_3D;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(34, 34);
            this.toolStripButton3.Text = "Switch 2D - 3D ";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 37);
            // 
            // Button_Move
            // 
            this.Button_Move.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Button_Move.Image = ((System.Drawing.Image)(resources.GetObject("Button_Move.Image")));
            this.Button_Move.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Button_Move.Name = "Button_Move";
            this.Button_Move.Size = new System.Drawing.Size(34, 34);
            this.Button_Move.Text = "Move buildings";
            this.Button_Move.Click += new System.EventHandler(this.Button_Move_Click);
            // 
            // Button_Rotate
            // 
            this.Button_Rotate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Button_Rotate.Image = ((System.Drawing.Image)(resources.GetObject("Button_Rotate.Image")));
            this.Button_Rotate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Button_Rotate.Name = "Button_Rotate";
            this.Button_Rotate.Size = new System.Drawing.Size(34, 34);
            this.Button_Rotate.Text = "Rotate buildings";
            this.Button_Rotate.Click += new System.EventHandler(this.Button_Rotate_Click);
            // 
            // Button_Scale
            // 
            this.Button_Scale.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Button_Scale.Image = global::CPlan.SGUserInterface.Properties.Resources.Window_New_Open;
            this.Button_Scale.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Button_Scale.Name = "Button_Scale";
            this.Button_Scale.Size = new System.Drawing.Size(34, 34);
            this.Button_Scale.Text = "Change building proportion";
            this.Button_Scale.Click += new System.EventHandler(this.Button_Scale_Click_1);
            // 
            // Button_Size
            // 
            this.Button_Size.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Button_Size.Image = global::CPlan.SGUserInterface.Properties.Resources.Scale_To_Fit;
            this.Button_Size.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Button_Size.Name = "Button_Size";
            this.Button_Size.Size = new System.Drawing.Size(34, 34);
            this.Button_Size.Text = "Change building size";
            this.Button_Size.Click += new System.EventHandler(this.Button_Size_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1249, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportDxfGraphToolStripMenuItem,
            this.loadDxfFileToolStripMenuItem,
            this.loadGraphFromDxfToolStripMenuItem});
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.testToolStripMenuItem.Text = "Datei";
            // 
            // exportDxfGraphToolStripMenuItem
            // 
            this.exportDxfGraphToolStripMenuItem.Name = "exportDxfGraphToolStripMenuItem";
            this.exportDxfGraphToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.exportDxfGraphToolStripMenuItem.Text = "Export dxf file";
            this.exportDxfGraphToolStripMenuItem.Click += new System.EventHandler(this.exportDxfGraphToolStripMenuItem_Click);
            // 
            // loadDxfFileToolStripMenuItem
            // 
            this.loadDxfFileToolStripMenuItem.Name = "loadDxfFileToolStripMenuItem";
            this.loadDxfFileToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.loadDxfFileToolStripMenuItem.Text = "Load dxf file";
            this.loadDxfFileToolStripMenuItem.Click += new System.EventHandler(this.loadDxfFileToolStripMenuItem_Click);
            // 
            // loadGraphFromDxfToolStripMenuItem
            // 
            this.loadGraphFromDxfToolStripMenuItem.Name = "loadGraphFromDxfToolStripMenuItem";
            this.loadGraphFromDxfToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.loadGraphFromDxfToolStripMenuItem.Text = "Load graph from dxf";
            this.loadGraphFromDxfToolStripMenuItem.Click += new System.EventHandler(this.loadGraphFromDxfToolStripMenuItem_Click);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(16, 17);
            this.toolStripStatusLabel1.Text = "   ";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 16);
            this.toolStripProgressBar1.Visible = false;
            // 
            // dateiToolStripMenuItem
            // 
            this.dateiToolStripMenuItem.Name = "dateiToolStripMenuItem";
            this.dateiToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.dateiToolStripMenuItem.Text = "Datei";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 23);
            // 
            // saveDxfDialog
            // 
            this.saveDxfDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // timer1
            // 
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // openDxfDialog
            // 
            this.openDxfDialog.FileName = "openFileDialog1";
            this.openDxfDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.openDxfDialog_FileOk);
            // 
            // openDxfGraphDialog
            // 
            this.openDxfGraphDialog.FileName = "openFileDialog1";
            this.openDxfGraphDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.openDxfGraphDialog_FileOk);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1249, 764);
            this.Controls.Add(this.toolStripContainer1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Viewer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ContentPanel.PerformLayout();
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_lineWidth)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_treeDepth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_populationSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_generations)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_minLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_maxLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_divAngle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_distance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_angle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_connectivity)).EndInit();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private OpenTK.GLControl glControl1;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dateiToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton ButtonStreets;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripMenuItem exportDxfGraphToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveDxfDialog;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton ButtonBuildings;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton ButtonBuildingLayout;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripButton Button_IsovistAnalysis;
        private System.Windows.Forms.ToolStripButton Button_StartStop;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox cB_autoShot;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rB_fitCentral;
        private System.Windows.Forms.RadioButton rB_fitChoice;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label10;
        protected internal System.Windows.Forms.NumericUpDown uD_lineWidth;
        private System.Windows.Forms.RadioButton cb_showBestGlobal;
        private System.Windows.Forms.RadioButton cb_showBestGen;
        private System.Windows.Forms.RadioButton cb_showAll;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lbl_nDSet;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        protected internal System.Windows.Forms.NumericUpDown uD_treeDepth;
        private System.Windows.Forms.Label label8;
        protected internal System.Windows.Forms.NumericUpDown uD_populationSize;
        private System.Windows.Forms.Label label7;
        protected internal System.Windows.Forms.NumericUpDown uD_generations;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        protected internal System.Windows.Forms.NumericUpDown uD_minLength;
        private System.Windows.Forms.Label label5;
        protected internal System.Windows.Forms.NumericUpDown uD_maxLength;
        private System.Windows.Forms.Label label4;
        protected internal System.Windows.Forms.NumericUpDown uD_divAngle;
        private System.Windows.Forms.Label label3;
        protected internal System.Windows.Forms.NumericUpDown uD_distance;
        private System.Windows.Forms.Label label2;
        protected internal System.Windows.Forms.NumericUpDown uD_angle;
        private System.Windows.Forms.Label label1;
        protected internal System.Windows.Forms.NumericUpDown uD_connectivity;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton Button_Move;
        private System.Windows.Forms.ToolStripButton Button_Rotate;
        private System.Windows.Forms.ToolStripButton Button_Size;
        private System.Windows.Forms.ToolStripButton Button_Scale;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.PropertyGrid propertyGrid2;
        private System.Windows.Forms.ComboBox comboBox_Isovist;
        private System.Windows.Forms.PropertyGrid propertyGrid3;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.OpenFileDialog openDxfDialog;
        private System.Windows.Forms.ToolStripMenuItem loadDxfFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton_LoadScene;
        private System.Windows.Forms.ToolStripMenuItem loadGraphFromDxfToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openDxfGraphDialog;
        private System.Windows.Forms.RadioButton sB_multiOpti;
        private System.Windows.Forms.RadioButton sB_singleOpti;
        private System.Windows.Forms.ToolStripButton B_Analyse;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton Button_OptiLayout;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private OpenTK.GLControl glControl2;
        private System.Windows.Forms.ToolStripButton toolStripButton9;


    }
}


//#endif
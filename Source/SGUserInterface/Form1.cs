﻿//#if CT_UNCOMMENT
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using CPlan.Geometry;
using CPlan.Optimization;
using CPlan.Evaluation;
using Tektosyne;
using Tektosyne.Geometry;
using System.Threading;
using FW_GPU_NET;
using QuickGraph;
using OpenTK.Graphics;
using AForge.Genetic;
using System.Diagnostics;
using netDxf;
using netDxf.Header;
using System.IO;
using System.Reflection;
using CPlan.VisualObjects;
using CPlan.Optimization.SelectionAlgorithms.PISA;
using QuickGraph;

namespace CPlan.UrbanPattern
{

    public partial class Form1 : Form
    {
        private ViewControl ViewControl1;
        private ViewControl ViewControl2;

        StreetPattern StreetGenerator;
        UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> initailNet;
        TimeSpan span;

        private int _populationSize = 40;
        private int _iterations = 100;
        private int _selectionMethod = 0;

        private Thread _workerThread = null;
        private volatile bool _needToStop1 = false;
        private volatile bool _needToStop2 = false;
        private MultiPopulation _population;
        private Population _populationSingle;
        private Rect2D _border;
        private Poly2D _isovistBorder;

        // --- morphological parameter ---
        int maxEdges = 4;
        int rndAngle = 10;
        double minDist = 20;
        double minAngle = 80;
        int maxLength = 100;
        int minLength = 30;
        bool rotSymmetryL = false;
        bool rotSymmetryR = false;

        // --- value List --
        List<List<double>> _fitnesLists = new List<List<double>>();

        private ViewParameters _parameters = new ViewParameters();
        public static String MouseMode { get; set; }
        private ObjectsDrawList constantDrawObjects;

        GGrid _smartGrid = null;

        //===================================================================================================================================================================
        public Form1()
        {
            InitializeComponent();

            MouseMode = "ScreenMove";

            // --- this needs to be copied for a new glcontrol ----------------------------------------------------
            ViewControl1 = new ViewControl(glControl1); // --- new instance for glControl methods ---
            this.glControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseMove);
            this.glControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseDown);
            this.glControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseUp);
            this.glControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.glControl1_Paint);
            // ----------------------------------------------------------------------------------------------------
            ViewControl2 = new ViewControl(glControl2); // --- new instance for glControl methods ---
            //this.glControl2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.glControl2_MouseMove);
            //this.glControl2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.glControl2_MouseDown);
            //this.glControl2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.glControl2_MouseUp);
            this.glControl2.Paint += new System.Windows.Forms.PaintEventHandler(this.glControl2_Paint);
            // ----------------------------------------------------------------------------------------------------

            // --- zoom all ---
            ViewControl1.ZoomAll();
            ViewControl2.ZoomAll();
            SetControlParameters();

            // -- chart
            //  chart1.ChartAreas[0].AxisX.Maximum = 1.0;// 100000;// 1.0;// .3;
            //  chart1.ChartAreas[0].AxisY.Maximum = 1.0;// 100;// .3;
            //chart1.ChartAreas[0].AxisX.MinorGrid.Enabled = false;
            //chart1.ChartAreas[0].AxisX.MajorGrid.Interval = .1;// 1.0 / 32;
            //chart1.ChartAreas[0].AxisY.MinorGrid.Enabled = false;
            //chart1.ChartAreas[0].AxisY.MajorGrid.Interval = .1;//1.0 / 32;
            chart1.Series[1].MarkerSize = 3;
            chart1.Series[0].MarkerSize = 6;

            chart1.Series[0].Points.Clear();

            propertyGrid3.SelectedObject = _parameters;

            Poly2D poly1 = new Poly2D(0, 0, 100, 100);
            Poly2D poly2 = new Poly2D(60, 60, 150, 150);
            Poly2D isectRect = (poly1.Intersection(poly2))[0];


        }


        //===================================================================================================================================================================
        //========= Paint Event =========
        //===================================================================================================================================================================
        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            // -- enter pait istruction here --   
            ViewControl1.AllObjectsList.Draw();

            // -- end of paint instructions ---
            //GL.PopMatrix();
            glControl1.SwapBuffers();
        }

        //===================================================================================================================================================================
        private void glControl2_Paint(object sender, PaintEventArgs e)
        {
            // -- enter pait istruction here --   
            ViewControl2.AllObjectsList.Draw();

            // -- end of paint instructions ---
            //GL.PopMatrix();
            glControl2.SwapBuffers();
        }

        //===================================================================================================================================================================
        //========= Mouse Controls =========
        //===================================================================================================================================================================
        # region mouse controls

        private Poly2D _movingChild;
        private GenBuildingLayout _curGen;
        private int _movingIdx = -1;
        private int _lastSelectedIdx = -1;
        private Vector2D _deltaM = new Vector2D();
        private Vector2D _oldMousePos = new Vector2D();
        private bool _wasLocked = false;
        //===================================================================================================================================================================
        private void glControl1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            // -- interaction with building layout --
            if (_movingIdx == -1)
            {
                if (e.Button == MouseButtons.Left && Form1.MouseMode != "ScreenMove") // left button to move polygons
                {
                    if (_curLayout != null)
                    {
                        CGeom3d.Vec_3d point_xy = ViewControl1.Camera.GetPointXYPtn(e.X, e.Y, 0);
                        if (point_xy == null) return;
                        Vector2D p = new Vector2D(point_xy.x, point_xy.y);

                        //for (int i = 0; i < _curLayout.Gens.Count; i++)
                        //{
                        //    Poly2D go = _curLayout.Gens[i].Poly;

                        //    if (go.ContainsPoint(p))
                        //    {
                        //        _movingChild = go;
                        //        _movingIdx = i;
                        //        _lastSelectedIdx = i;
                        //        _wasLocked = _curLayout.Gens[_movingIdx].Locked;
                        //        //for (int k = 0; k < EA.Parents.Count; k++)
                        //        {
                        //            _curGen = _curLayout.Gens[_movingIdx];
                        //            _deltaM = new Vector2D(p - _curGen.Position);
                        //            _curGen.Locked = true;
                        //        }
                        //        break;
                        //    }
                        //}

                        int selectIndex = selectGenBuilding(e);
                        if (selectIndex != -1) {
                            _movingChild = _curLayout.Gens[selectIndex].Poly;
                            _movingIdx = selectIndex;
                            _lastSelectedIdx = selectIndex;
                            _wasLocked = _curLayout.Gens[_movingIdx].Locked;
                            {
                                _curGen = _curLayout.Gens[_movingIdx];
                                _deltaM = new Vector2D(p - _curGen.Position);
                                _curGen.Locked = true;
                            }
                        }


                        _oldMousePos = p;
                    }
                }
            }
        }

        private int selectGenBuilding(System.Windows.Forms.MouseEventArgs e)
        {
            int selectedIndex = -1;

            int minColor = 1, maxColor = 255 * 255 * 255;
            int curColor = minColor;
            int colorInc = (maxColor - minColor) / _curLayout.Gens.Count;

            List<GeoObject> drawObjects = new List<GeoObject>(); 
            for(int i = 0; i < _curLayout.Gens.Count; i++)
            {
                GenBuildingLayout building = _curLayout.Gens[i];
                GCube cube = new GCube(building.Poly.PointsFirstLoop, Convert.ToSingle(building.Height));
                cube.Identity = _curLayout.Indentity;
                cube.DeltaZ = 0.1f;
                cube.BorderWidth = 5;
                cube.FillColor = ConvertColor.CreateColor4(Color.Gray);
                cube.Filled = true;
                ViewControl1.AllObjectsList.ObjectsToDraw.Add(cube);

                cube.UniqueColor = curColor;
                curColor += colorInc;

                drawObjects.Add(cube);
            }

            //Console.WriteLine("1.selecting object from " + drawObjects.Count + " buildings at " + e.X + " " + e.Y);

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.DrawBuffer(DrawBufferMode.Back);
            for (int i = 0; i < drawObjects.Count; i++)
            {
                GeoObject go = drawObjects[i];
                go.DrawPickingMode();
            }

            GL.ReadBuffer(ReadBufferMode.Back);
            byte[] pixel = new byte[4];
            GL.ReadPixels(e.X, glControl1.Height - e.Y, 1, 1, PixelFormat.Rgba, PixelType.UnsignedByte, pixel);
            int color = pixel[0] + pixel[1] * 256 + pixel[2] * 256 * 256;
            //GL.Enable(EnableCap.Dither);

            //Console.WriteLine("pixel " + pixel[0] + " " + pixel[1] + " " + pixel[2] + " " + pixel[3]);
            //Console.WriteLine("selected color " + color);

            for (int i = 0; i < drawObjects.Count; i++)
            {
                GeoObject go = drawObjects[i];
                if (go.UniqueColor == color)
                {
                    selectedIndex = i;
                    break;
                }
            }

            //if (selectedIndex != -1)
            //{
            //    Console.WriteLine("selected object");
            //}

            return selectedIndex;
        }

        //===================================================================================================================================================================
        private void glControl1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            // -- interaction with building layout --
            Vector2D vel = new Vector2D();
            GenBuildingLayout curGen;
            Vector3D point_xy = ViewControl1.Camera.GetPointXYPtn(e.X, e.Y, 0);

            if (_movingChild != null)
            {
                // Wenn gerade ein Objekt verschoben werden soll, wird die Differenz zur letzten
                // Mausposition ausgerechnet und das Objekt um diese verschoben.              
                if (point_xy == null) return;
                Cursor tmpCursor = Cursors.Default;
                Vector2D p = new Vector2D(point_xy.x, point_xy.y);

                //for (int k = 0; k < EA.Parents.Count; k++)
                {
                    curGen = _curLayout.Gens[_movingIdx];

                    if (Control.ModifierKeys == Keys.Shift || MouseMode == "Rotate")
                    { // rotate
                        double deg = _oldMousePos.Y - p.Y;
                        curGen.Rotation += deg;
                    }
                    else if (MouseMode == "Scale")
                    {
                        double fact = 1;
                        Vector2D oldCenter = new Vector2D(curGen.Center);
                        double delta = (_oldMousePos.Y - p.Y) * fact;
                        double newWidth = curGen.Width + delta;
                        double newHeith = curGen.Area / newWidth;
                        if (newWidth > curGen.MinSide && newHeith > curGen.MinSide)
                        {
                            curGen.Width = newWidth;
                            curGen.Height = newHeith;
                            //Vector2D diffCenter = curGen.Position - oldCenter;
                            curGen.Center = oldCenter;// new Vector2D(delta / 2 / fact, delta / 2 / fact);
                        }
                    }
                    else if (MouseMode == "Size")
                    {
                        double fact = 20;
                        double delta = (_oldMousePos.Y - p.Y) * fact;
                        curGen.Area += delta;
                        //int dir = Math.Sign(delta);
                        //double retFact = Math.Sqrt(Math.Abs(delta));
                        //curGen.Position -= new Vector2D((retFact / 2) * (float)dir, (retFact / 2) * (float)dir);
                    }
                    else
                    { // move
                        curGen.Position = new Vector2D(p - _deltaM);
                    }
                }

                _oldMousePos = p;
            }

            if (point_xy != null) statusLabel.Text = "X: " + point_xy.x.ToString() + ", Y: " + point_xy.y.ToString();
            ViewControl1.Invalidate();
        }

        //===================================================================================================================================================================
        private void glControl1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            // -- interaction with building layout --
            Vector2D vel = new Vector2D();
            GenBuildingLayout curGen;
            vel.X = 0;
            vel.Y = 0;
            {
                if (_movingChild != null)
                {
                    _movingChild = null;

                    //for (int k = 0; k < EA.Parents.Count; k++)
                    {
                        curGen = _curLayout.Gens[_movingIdx];
                        curGen.Locked = _wasLocked;
                        propertyGrid1.SelectedObject = curGen;
                    }
                    if (RParameter.instantUpdate) IsovistAnalysis();
                }
            }
            _movingIdx = -1;
        }

        # endregion

        //===================================================================================================================================================================
        //========= Button Controls =========
        //===================================================================================================================================================================
        private void toolStripButton9_Click(object sender, EventArgs e)
        {
            MouseMode = "ScreenMove";
        }
        
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            MouseMode = "ScreenMove";
            ViewControl1.ZoomAll();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            MouseMode = "ScreenMove";
            ViewControl1.CameraChange();
        }

        //============================================================================================================================================================================
        private void SetGraphParameter()
        {
            // --- morphological parameter ---
            maxEdges = Convert.ToInt16(uD_connectivity.Value);
            rndAngle = Convert.ToInt16(uD_angle.Value);
            minDist = Convert.ToInt16(uD_distance.Value);
            minAngle = Convert.ToInt16(uD_divAngle.Value);
            maxLength = Convert.ToInt16(uD_maxLength.Value);
            minLength = Convert.ToInt16(uD_minLength.Value);
            _border = new Rect2D(0, 0, 2000, -2000);//glControl1.Left, glControl1.Top, glControl1.Width, -glControl1.Height);
            StreetGenerator = new StreetPattern(maxEdges, rndAngle, minDist, minAngle, maxLength, minLength, rotSymmetryL, rotSymmetryR, _border);

            // --- TestGraph ---
            PointD midPt = new PointD(_border.Width / 2, _border.Height / 2);
            PointD[] iniPts;
            iniPts = new PointD[2];
            iniPts[0] = new PointD(midPt.X + 100, midPt.Y + 100);
            iniPts[1] = new PointD(midPt.X + 150, midPt.Y + 100);
            StreetGenerator.AddEdge(iniPts[0], iniPts[1]);

            iniPts[0] = new PointD(midPt.X + 100, midPt.Y + 100);
            iniPts[1] = new PointD(midPt.X + 50, midPt.Y + 100);
            StreetGenerator.AddEdge(iniPts[0], iniPts[1]);

            iniPts[0] = new PointD(midPt.X + 100, midPt.Y + 100);
            iniPts[1] = new PointD(midPt.X + 70, midPt.Y + 50);
            StreetGenerator.AddEdge(iniPts[0], iniPts[1]);

            iniPts[0] = new PointD(midPt.X + 150, midPt.Y + 100);
            iniPts[1] = new PointD(midPt.X + 200, midPt.Y + 150);
            StreetGenerator.AddEdge(iniPts[0], iniPts[1]);

        }

        //============================================================================================================================================================================
        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            SetGraphParameter();
            int steps = 0;// 1000;
            for (int i = 0; i < steps; i++)
            {
                StreetGenerator.GrowStreetPatternBasic();
            }
            StreetGenerator.CleanUpSubdiv();
            DrawNetwork(StreetGenerator.Graph);
            ViewControl1.ZoomAll();

        }

        private void toolStripButton5_Click_1(object sender, EventArgs e)
        {
            int steps = 10;
            for (int i = 0; i < steps; i++)
            {
                StreetGenerator.GrowStreetPatternBasic();
            }
            StreetGenerator.CleanUpSubdiv();
            DrawNetwork(StreetGenerator.Graph);
            ViewControl1.ZoomAll();
        }

        //============================================================================================================================================================================
        private void B_Analyse_Click(object sender, EventArgs e)
        {
            UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> qGraph = new UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>>(false);

            if (StreetGenerator != null)
            {
                LineD[] subLines = StreetGenerator.Graph.ToLines();
                List<Line2D> netLines = new List<Line2D>();
                foreach (LineD line in subLines)
                {
                    Vector2D startPt = line.Start.ToVector2D();
                    Vector2D endPt = line.End.ToVector2D();
                    Line2D newLine = new Line2D(startPt, endPt);
                    netLines.Add(newLine);


                    UndirectedEdge<Vector2D> newEdge = new UndirectedEdge<Vector2D>(line.Start.ToVector2D(), line.End.ToVector2D());
                    UndirectedEdge<Vector2D> reverseNewEdge = qGraph.Edges.ToList().Find(x => x.Source == line.End.ToVector2D() && x.Target == line.Start.ToVector2D());
                    // --- a new edge (& vertex) is added ---                       
                    if (qGraph.ContainsVertex(line.End.ToVector2D()) && qGraph.ContainsVertex(line.Start.ToVector2D()))
                    {
                        if (reverseNewEdge == null)
                            qGraph.AddEdge(newEdge);
                    }
                    else
                    {
                        if (reverseNewEdge == null)
                            qGraph.AddVerticesAndEdge(newEdge);
                    }

                }

                ViewControl1.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
                //AnalyseNetwork(netLines);//StreetGenerator.Graph);
                AnalyseNetwork(qGraph);
            }
        }

        //============================================================================================================================================================================
        private void ReDrawGraph(Subdivision curGraph)
        {
            // --- draw the elements of the graph ---
            ViewControl1.AllObjectsList.ObjectsToDraw.Clear();
            ViewControl1.AllObjectsList.ObjectsToInteract.Clear();
            ViewControl1.AllObjectsList = new ObjectsAllList();
            LineD[] graphLines = curGraph.ToLines();
            foreach (LineD line in graphLines)
            {
                ViewControl1.AllObjectsList.Add(new GLine(line));
            }

            PointD[] graphPts = curGraph.Nodes.ToArray();
            foreach (PointD point in graphPts)
            {
                ViewControl1.AllObjectsList.Add(new GCircle(new Vector2D(point.X, point.Y), 1, 10));
            }
            glControl1.Invalidate();
        }

        //============================================================================================================================================================================
        private void ButtonBuildings_Click(object sender, EventArgs e)
        {
            //List<GPolygon> iniBuildings = new List<GPolygon>();
            //Vector2D[] pts = new Vector2D[4];// pt1, pt2, pt3, pt4;
            //pts[0] = new Vector2D(50, 50);
            //pts[1] = new Vector2D(52, 60);
            //pts[2] = new Vector2D(59, 60);
            //pts[3] = new Vector2D(60, 51);
            //GPolygon buildingA = new GPolygon(pts);
            //buildingA.FillColor = new Vector4(0, 0, 0, 1);
            //ViewControl1.AllObjectsList.ObjectsToDraw.Add(buildingA);
            //iniBuildings.Add(buildingA);

            //BuildingTree testBuildTree = new BuildingTree(8, iniBuildings, _border);
            //List<GPolygon> buildingLayout = BuildingLayout.GrowBuildingLayout(testBuildTree);

            //ViewControl1.AllObjectsList.ObjectsToDraw.AddRange(buildingLayout);

            //glControl1.Invalidate();
            //ViewControl1.ZoomAll();
        }

        //============================================================================================================================================================================
        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            Stopwatch timer = new Stopwatch();
            StringBuilder sb = new StringBuilder();
            bool testing = false;// true;

            if (testing)
                timer.Start();
            InitialGraph();
            if (testing)
            {
                sb.Append("Graph init.: " + timer.ElapsedMilliseconds + "ms \n");
                timer.Reset();
                timer.Start();
            }
            UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> networkQ = null;
            //Subdivision network = null;
            InstructionTreePisa testTree = new InstructionTreePisa(ControlParameters.TreeDepth, initailNet, _border);
            if (testing)
            {
                sb.Append("Instruction tree: " + timer.ElapsedMilliseconds + "ms \n");
                timer.Reset();
                timer.Start();
            }
            // ---
            networkQ = testTree.CreateStreetNetwork();
            if (testing)
            {
                sb.Append("Test tree: " + timer.ElapsedMilliseconds + "ms \n");
                timer.Reset();
                timer.Start();
            }
            AnalyseNetwork(networkQ);
            if (testing)
            {
                sb.Append("Analysis: " + timer.ElapsedMilliseconds + "ms \n");
                sb.Append("\nNetwork edges: " + networkQ.Edges.Count() + "\n");
                sb.Append("Network verticles: " + networkQ.Vertices.Count() + "\n");
                timer.Stop();
                MessageBox.Show(sb.ToString());
            }
        }

        //============================================================================================================================================================================
        private void AnalyseNetwork(List<Line2D> network)
        {

            GraphAnalysis analyzer = new GraphAnalysis(network);
            float[] radius = { float.MaxValue };//, 500, 150 }; //  
            analyzer.Calculate("angle", radius);

            Vector4[] ffColors = null;
            if (rB_fitChoice.Checked) ffColors = ConvertColor.CreateFFColor4(analyzer.ResultsAngular.GetNormCentralityStepsArray()[0]);
            //if (rB_fitChoice.Checked) ffColors = ConvertColor.CreateFFColor4(analyzer.ResultsAngular.GetNormChoiceArray()[0]);
            if (rB_fitCentral.Checked) ffColors = ConvertColor.CreateFFColor4(analyzer.ResultsAngular.GetNormCentralityMetricArray()[0]); // GetNormCentralityStepsArray()[0]); //
            Vector2D[] Nodes = analyzer.Network.Vertices.ToArray();
            UndirectedEdge<Vector2D>[] tempEdges = analyzer.Network.Edges.ToArray();
            List<Line2D> Edges = new List<Line2D>();
            foreach (UndirectedEdge<Vector2D> tempEdge in tempEdges)
            {
                Edges.Add(new Line2D(tempEdge.Source, tempEdge.Target));
            }

            DrawAnalysis(ffColors, Edges.ToArray(), Nodes, new Vector2D(0, 0));

            network = null;
            ViewControl1.ZoomAll();
        }

        //============================================================================================================================================================================
        private void AnalyseNetwork(UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> network)
        {

            FW_GPU_NET.Graph FWInverseGraph = null;
            ShortestPath ShortPahtesAngular = null;
            FW_GPU_NET.Graph fwGraph;

            //------------------------------------------------------------------------------//
            //Dictionary<Vector2D, Line2D> inverseEdgesMapping = new Dictionary<Vector2D, Line2D>();
            //Dictionary<Line2D, double> realDist = new Dictionary<Line2D, double>();
            //Dictionary<Line2D, double> realAngle = new Dictionary<Line2D, double>();

            //Subdivision InverseGraph = RndGlobal.ConstructInverseGraph(network, ref realDist, ref realAngle, ref inverseEdgesMapping);
            fwGraph = GraphTools.GenerateFWGpuGraph(network);
            FWInverseGraph = GraphTools.GenerateInverseDoubleGraph(fwGraph);
            ShortPahtesAngular = new ShortestPath(network);//, realAngle);
            ShortPahtesAngular.Evaluate(new System.Windows.Forms.ToolStripProgressBar(), fwGraph, FWInverseGraph);//  null);//FloydWarshall();//Dijikstra();//
            //ShortPahtesAngular.EvaluateOnServer (new System.Windows.Forms.ToolStripProgressBar(), fwGraph, FWInverseGraph);//  null);//FloydWarshall();//Dijikstra();//

            Vector2D[] Nodes = network.Vertices.ToArray();
            UndirectedEdge<Vector2D>[] tempEdges = network.Edges.ToArray();
            List<Line2D> Edges = new List<Line2D>();
            foreach (UndirectedEdge<Vector2D> tempEdge in tempEdges)
            {
                Edges.Add(new Line2D(tempEdge.Source, tempEdge.Target));
            }

            //Vector4[] ffColors = ConvertColor.CreateFFColor(ShortPahtesAngular.GetNormChoiceArray());
            Vector4[] ffColors = null;
            if (rB_fitChoice.Checked) ffColors = ConvertColor.CreateFFColor4(ShortPahtesAngular.GetNormChoiceArray()[0]);
            if (rB_fitCentral.Checked) ffColors = ConvertColor.CreateFFColor4(ShortPahtesAngular.GetNormCentralityMetricArray()[0]);

            DrawAnalysis(ffColors, Edges.ToArray(), Nodes, new Vector2D(0, 0));
            //DrawAnalysis(ffColors, ShortPahtesAngular.OrigEdges, ShortPahtesAngular.OrigPoints, new PointD(0, 0));

            fwGraph.ClearGraph();
            fwGraph.Dispose();
            fwGraph = null;
            FWInverseGraph.Dispose();
            FWInverseGraph = null;
            ShortPahtesAngular = null;

            network = null;
            ViewControl1.ZoomAll();
        }



        //============================================================================================================================================================================
        void DrawAnalysis(Vector4[] ffColors, Line2D[] origEdges, Vector2D[] graphPts, Vector2D Origin)
        {
            // -- draw the border --
            if (_border == null) _border = new Rect2D(glControl1.Left, glControl1.Top, glControl1.Width, -glControl1.Height);
            GRectangle bord = new GRectangle(Origin.X, _border.Height + Origin.Y, _border.Width, _border.Height);
            //bord.Move( new Vector2D (Origin.X, Origin.Y) );
            Color4 col = Color4.White;
            bord.BorderColor = new Vector4(col.R, col.G, col.B, col.A);
            ViewControl1.AllObjectsList.ObjectsToDraw.Add(bord);

            // --- draw the elements of the graph ---
            //List<LineD> origEdges = network.ToLines().ToList();
            //ViewControl1.AllObjectsList = new ObjectsAllList();
            Line2D curEdge;
            //if (ffColors.Length == origEdges.Count) 
            for (int i = 0; i < ffColors.Length; i++)
            {
                //PointD point = curVertices[i];
                curEdge = origEdges[i];// InverseEdgesMapping[point];
                curEdge = new Line2D(curEdge.Start.X + Origin.X, curEdge.Start.Y + Origin.Y, curEdge.End.X + Origin.X, curEdge.End.Y + Origin.Y);
                GLine curLine = new GLine(curEdge);
                curLine.Width = ControlParameters.LineWidth;
                curLine.Color = ffColors[i];
                ViewControl1.AllObjectsList.Add(curLine, true, false);
            }

            //PointD[] graphPts = network.Nodes.ToArray();
            foreach (Vector2D point in graphPts)
            {
                ViewControl1.AllObjectsList.Add(new GCircle(new Vector2D(point.X + Origin.X, point.Y + Origin.Y), .6, 10), true, false);
            }
            // GL.ClearColor(Color.Black);

        }


        //============================================================================================================================================================================
        void DrawAnalysis(Vector4[] ffColors, LineD[] origEdges, PointD[] graphPts, PointD Origin)
        {
            // -- draw the border --
            if (_border == null) _border = new Rect2D(glControl1.Left, glControl1.Top, glControl1.Width, -glControl1.Height);
            GRectangle bord = new GRectangle(Origin.X, _border.Height + Origin.Y, _border.Width, _border.Height);
            //bord.Move( new Vector2D (Origin.X, Origin.Y) );
            Color4 col = Color4.White;
            bord.BorderColor = new Vector4(col.R, col.G, col.B, col.A);
            ViewControl1.AllObjectsList.ObjectsToDraw.Add(bord);

            // --- draw the elements of the graph ---
            //List<LineD> origEdges = network.ToLines().ToList();
            //ViewControl1.AllObjectsList = new ObjectsAllList();
            LineD curEdge;
            //if (ffColors.Length == origEdges.Count) 
            for (int i = 0; i < ffColors.Length; i++)
            {
                //PointD point = curVertices[i];
                curEdge = origEdges[i];// InverseEdgesMapping[point];
                curEdge = new LineD(curEdge.Start.X + Origin.X, curEdge.Start.Y + Origin.Y, curEdge.End.X + Origin.X, curEdge.End.Y + Origin.Y);
                GLine curLine = new GLine(curEdge);
                curLine.Width = ControlParameters.LineWidth;
                curLine.Color = ffColors[i];
                ViewControl1.AllObjectsList.Add(curLine, true, false);
            }

            //PointD[] graphPts = network.Nodes.ToArray();
            foreach (PointD point in graphPts)
            {
                ViewControl1.AllObjectsList.Add(new GCircle(new Vector2D(point.X + Origin.X, point.Y + Origin.Y), .6, 10), true, false);
            }
            // GL.ClearColor(Color.Black);

        }

        //============================================================================================================================================================================
        private void DrawNetwork(Subdivision curGraph)
        {
            // --- draw the elements of the graph ---
            ViewControl1.AllObjectsList.ObjectsToDraw.Clear();
            ViewControl1.AllObjectsList.ObjectsToInteract.Clear();
            ViewControl1.AllObjectsList = new ObjectsAllList();
            LineD[] graphLines = curGraph.ToLines();
            foreach (LineD line in graphLines)
            {
                GLine curLine = new GLine(line);
                curLine.Color = new Vector4(.09f, .09f, .09f, 1f);
                ViewControl1.AllObjectsList.Add(curLine);
            }

            PointD[] graphPts = curGraph.Nodes.ToArray();
            foreach (PointD point in graphPts)
            {
                ViewControl1.AllObjectsList.Add(new GCircle(new Vector2D(point.X, point.Y), 1, 10));
            }
            glControl1.Invalidate();
        }

        //============================================================================================================================================================================
        //============================================================================================================================================================================
        //============================================================================================================================================================================
        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            // get population size
            try
            {
                _populationSize = Convert.ToInt16(uD_populationSize.Value);// 20; // Math.Max(10, Math.Min(100, int.Parse(populationSizeBox.Text)));
            }
            catch
            {
                _populationSize = 20;
            }
            // iterations
            try
            {
                _iterations = Convert.ToInt16(uD_generations.Value); ; //Math.Max(0, int.Parse(iterationsBox.Text));
            }
            catch
            {
                _iterations = 20;
            }

            InitialGraph();
            // update settings controls
            //UpdateSettings();
            ViewControl1.DeactivateInteraction();
            //selectionMethod = selectionBox.SelectedIndex;
            //greedyCrossover = greedyCrossoverBox.Checked;
            // disable all settings controls except "Stop" button
            //EnableControls(false);

            // run worker thread           
            _needToStop1 = false;
            if (sB_multiOpti.Checked)
            {
                _workerThread = new Thread(new ThreadStart(SearchNetworkMulti));
            }
            if (sB_singleOpti.Checked)
            {
                _workerThread = new Thread(new ThreadStart(SearchNetworkSingle));
            }
            _workerThread.Start();
        }

        //============================================================================================================================================================================
        void InitialGraph()
        {
            UndirectedEdge<Vector2D> iniEdge;
            _fitnesLists = new List<List<double>>();
            _border = new Rect2D(0, 200, 300, 200);
            // --- initial graph for optimization ---
            initailNet = new UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>>();
            Vector2D middle = new Vector2D(_border.Width / 2, _border.Height / 2);
            Vector2D[] iniPoints;
            iniPoints = new Vector2D[2];
            // segment in the middle
            //iniPoints[0] = new PointD(15 + middle.X, 10 + middle.Y);
            //iniPoints[1] = new PointD(30 + middle.X, 10 + middle.Y);
            //initailNet.AddEdge(iniPoints[0], iniPoints[1]);

            // segments at the borders
            iniPoints[0] = new Vector2D(-15, middle.Y + 20);
            iniPoints[1] = new Vector2D(15, middle.Y + 20);
            iniEdge = new UndirectedEdge<Vector2D>(iniPoints[0], iniPoints[1]);
            initailNet.AddVerticesAndEdge(iniEdge);

            iniPoints[0] = new Vector2D(_border.Width - 15, middle.Y - 20);
            iniPoints[1] = new Vector2D(_border.Width + 15, middle.Y - 20);
            iniEdge = new UndirectedEdge<Vector2D>(iniPoints[0], iniPoints[1]);
            initailNet.AddVerticesAndEdge(iniEdge);

            iniPoints[0] = new Vector2D(middle.X - 40, _border.Height - 15);
            iniPoints[1] = new Vector2D(middle.X - 40, _border.Height + 15);
            iniEdge = new UndirectedEdge<Vector2D>(iniPoints[0], iniPoints[1]);
            initailNet.AddVerticesAndEdge(iniEdge);

        }

        //============================================================================================================================================================================
        // Worker thread
        void SearchNetworkMulti()
        {
            // measure the needed time:
            DateTime start = DateTime.Now;// DateTime.MinValue;
            DateTime end = DateTime.MinValue;

            // create fitness function
            FitnessFunctionNetwork fitnessFunction = new FitnessFunctionNetwork();

            // create population
            _population = new MultiPopulation(_populationSize, 30, 40, 20,
                //(greedyCrossover) ? new InstructionTreePisa(treeDepth) : new PermutationChromosome(treeDepth),
                new InstructionTreePisa(ControlParameters.TreeDepth, initailNet, _border),
                fitnessFunction,
                //(_selectionMethod == 0) ? (ISelectionMethod)new EliteSelection() :
                //(_selectionMethod == 1) ? (ISelectionMethod)new RankSelection() :
                (IMultiSelectionMethod)new NonDominatedSelection(), //Multi_EliteSelection()//
                CalculationType.Method.Local
                );
            _population.MutationRate = 0.25;
            //population.CrossoverRate = 0.0;
            _population.RandomSelectionPortion = 0.1;
            // iterations
            DrawNetworkMulti(0);
            Debug.WriteLine("bestFit", Math.Round(_population.FitnessMax, 2).ToString());

            int i = 1;
            // CopyBestToDrawlist(i);
            // path
            //double[,] path = new double[roomsCount + 1, 2];

            // loop
            while (!_needToStop1)
            {
                // run one epoch of genetic algorithm
                _population.RunEpoch();
                //DrawBest(i);
                DrawNetworkMulti(i);

                // --- Achtung: Ading data to chart makes the program slow down over time!!! ------
                ReSetChart(chart1, 0);
                List<IMultiChromosome> archive = _population.Sel.GetArchive();
                for (int p = 0; p < archive.Count(); p++)
                {
                    InstructionTreePisa curTree = (InstructionTreePisa)(archive[p]);
                    if (curTree.FitnessValues.Count > 1)
                    {
                        SetChart(chart1, Math.Round(curTree.FitnessValues[0], 5), curTree.FitnessValues[1], 0);
                        SetChart(chart1, Math.Round(curTree.FitnessValues[0], 5), curTree.FitnessValues[1], 1);
                    }
                }

                //Debug.WriteLine(i.ToString() + ". iterat, bestFit", Math.Round(_population.FitnessMax, 2).ToString());
                Thread.Sleep(100);

                // set current iteration's info
                SetText(lbl_nDSet, _population.SizeNonDominatedSet.ToString());
                //SetText(currentIterationBox, i.ToString());
                //SetText(pathLengthBox, fitnessFunction.DistanceValue(population.BestChromosome).ToString());

                // increase current iteration
                i++;
                //
                if ((_iterations != 0) && (i > _iterations))
                    break;
            }
            // enable settings controls
            //EnableControls(true);
            end = DateTime.Now;
            span = end - start;

            _population.Sel.Terminate();

            ViewControl1.ActivateInteraction();
        }

        //============================================================================================================================================================================
        // Worker thread
        void SearchNetworkSingle()
        {
            // measure the needed time:
            DateTime start = DateTime.Now;// DateTime.MinValue;
            DateTime end = DateTime.MinValue;

            // create fitness function
            FitnessFunctionNetworkSingle fitnessFunction = new FitnessFunctionNetworkSingle();

            // create population
            _populationSingle = new Population(_populationSize,
                //(greedyCrossover) ? new InstructionTree(treeDepth) : new PermutationChromosome(treeDepth),
                new InstructionTreeSingle(ControlParameters.TreeDepth, initailNet, _border),
                fitnessFunction,
                //(_selectionMethod == 0) ? (ISelectionMethod)new EliteSelection() :
                //(_selectionMethod == 1) ? (ISelectionMethod)new RankSelection() :
                (ISelectionMethod)new EliteSelection() //Multi_EliteSelection()//
                );
            _populationSingle.MutationRate = 0.25;
            //population.CrossoverRate = 0.0;
            _populationSingle.RandomSelectionPortion = 0.2;
            // iterations
            DrawNetworkSingle(0);
            Debug.WriteLine("bestFit", Math.Round(_populationSingle.FitnessMax, 2).ToString());

            int i = 1;
            // CopyBestToDrawlist(i);
            // path
            //double[,] path = new double[roomsCount + 1, 2];

            // loop
            while (!_needToStop1)
            {
                // run one epoch of genetic algorithm
                _populationSingle.RunEpoch();
                //DrawBest(i);
                DrawNetworkSingle(i);

                // --- Achtung: Ading data to chart makes the program slow down over time!!! ------
                //ReSetChart(chart1, 0);
                //for (int p = 0; p < _population.SizeNonDominatedSet; p++)
                //{
                //    InstructionTree curTree = (InstructionTree)(_population[p]);
                //    if (curTree.FitnessValues.Count > 1)
                //    {
                //        SetChart(chart1, Math.Round(curTree.FitnessValues[0], 5), curTree.FitnessValues[1], 0);
                //        SetChart(chart1, Math.Round(curTree.FitnessValues[0], 5), curTree.FitnessValues[1], 1);
                //    }
                //}

                Debug.WriteLine(i.ToString() + ". iterat, bestFit", Math.Round(_populationSingle.FitnessMax, 2).ToString());
                Thread.Sleep(100);

                // set current iteration's info
                //SetText(lbl_nDSet, _populationSingle.SizeNonDominatedSet.ToString());
                //SetText(currentIterationBox, i.ToString());
                //SetText(pathLengthBox, fitnessFunction.DistanceValue(population.BestChromosome).ToString());

                // increase current iteration
                i++;
                //
                if ((_iterations != 0) && (i > _iterations))
                    break;
            }
            // enable settings controls
            //EnableControls(true);
            end = DateTime.Now;
            span = end - start;

            ViewControl1.ActivateInteraction();
        }

        //==============================================================================================
        // Delegates to enable async calls for setting controls properties
        private delegate void SetTextCallback(System.Windows.Forms.Control control, string text);

        // Thread safe updating of control's text property
        private void SetText(System.Windows.Forms.Control control, string text)
        {
            if (control.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                Invoke(d, new object[] { control, text });
            }
            else
            {
                control.Text = text;
            }
        }

        //==============================================================================================
        private delegate void SetChartCallback(System.Windows.Forms.DataVisualization.Charting.Chart chart, double valueX, double valueY, int series);

        private void SetChart(System.Windows.Forms.DataVisualization.Charting.Chart chart, double valueX, double valueY, int series)
        {
            if (chart.InvokeRequired)
            {
                SetChartCallback d = new SetChartCallback(SetChart);
                Invoke(d, new object[] { chart, valueX, valueY, series });
            }
            else
            {
                chart.Series[series].Points.AddXY(valueX, valueY);
            }
        }

        //==============================================================================================
        private delegate void ReSetChartCallback(System.Windows.Forms.DataVisualization.Charting.Chart chart, int series);

        private void ReSetChart(System.Windows.Forms.DataVisualization.Charting.Chart chart, int series)
        {
            if (chart.InvokeRequired)
            {
                ReSetChartCallback d = new ReSetChartCallback(ReSetChart);
                Invoke(d, new object[] { chart, series });
            }
            else
            {
                chart.Series[series].Points.Clear();
            }
        }

        //============================================================================================================================================================================
        private void DrawBest(int iterat)//Population pop); //InstructionTree fittest)
        {
            InstructionTreePisa fittest = (InstructionTreePisa)(_population.BestChromosome);
            Vector4[] ffColors = null;
            if (rB_fitChoice.Checked) ffColors = ConvertColor.CreateFFColor4(fittest.ShortestPathes.GetNormChoiceArray()[0]);
            if (rB_fitCentral.Checked) ffColors = ConvertColor.CreateFFColor4(fittest.ShortestPathes.GetNormCentralityMetricArray()[0]);
            DrawAnalysis(ffColors, fittest.ShortestPathes.OrigEdges, fittest.ShortestPathes.OrigPoints, new PointD(0, iterat * fittest.MaxSize));
            ViewControl1.ZoomAll();
        }

        //============================================================================================================================================================================
        private void DrawNetworkSingle(int iterat)//Population pop); //InstructionTreePisa fittest)
        {
            //ViewControl1.SetupViewport(populationSize * (int)_border.Width, populationSize * (int)_border.Height);
            // -- draw the network --
            float dist = 1.1f;
            List<double> fitnesValues = new List<double>();

            if (cb_showBestGlobal.Checked || cb_showBestGen.Checked)
            {
                ViewControl1.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            }

            if (cb_showAll.Checked || cb_showBestGen.Checked)
            {
                for (int i = 0; i < _populationSingle.Size; i++)
                {
                    InstructionTreeSingle curTree = (InstructionTreeSingle)_populationSingle[i];
                    Vector4[] ffColors = null;
                    if (curTree.ShortestPathes != null)
                    {
                        if (rB_fitChoice.Checked) ffColors = ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormChoiceArray()[0]);
                        if (rB_fitCentral.Checked) ffColors = ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormCentralityMetricArray()[0]);
                        DrawAnalysis(ffColors, curTree.ShortestPathes.OrigEdges, curTree.ShortestPathes.OrigPoints, new PointD(i * curTree.Border.Width * dist, iterat * curTree.Border.Height * dist));
                        if (!cb_showAll.Checked) fitnesValues.Add(curTree.Fitness);
                    }
                }
            }

            InstructionTreeSingle bestTree = (InstructionTreeSingle)_populationSingle.BestChromosome;
            if (bestTree != null)
            {
                //Vector4[] bestColors = ConvertColor.CreateFFColor(bestTree.ShortestPathes.GetNormChoiceArray());
                Vector4[] ffColors = null;
                if (bestTree.ShortestPathes != null)
                {
                    if (rB_fitChoice.Checked) ffColors = ConvertColor.CreateFFColor4(bestTree.ShortestPathes.GetNormChoiceArray()[0]);
                    if (rB_fitCentral.Checked) ffColors = ConvertColor.CreateFFColor4(bestTree.ShortestPathes.GetNormCentralityMetricArray()[0]);
                    DrawAnalysis(ffColors, bestTree.ShortestPathes.OrigEdges, bestTree.ShortestPathes.OrigPoints, new PointD(-1.3 * bestTree.Border.Width * dist, iterat * bestTree.Border.Height * dist));
                    if (!cb_showAll.Checked) fitnesValues.Add(bestTree.Fitness);
                }
            }

            for (int i = 0; i < _populationSingle.Size; i++)
            {
                InstructionTreeSingle curTree = (InstructionTreeSingle)(_populationSingle[i]);
                fitnesValues.Add(curTree.Fitness);
            }
            _fitnesLists.Add(fitnesValues);

            if (cB_autoShot.Checked) SetGraphicContext(glControl1); //Screenshot();

            ViewControl1.ZoomAll();
        }

        //============================================================================================================================================================================
        private void DrawNetworkMulti(int iterat)//Population pop); //InstructionTreePisa fittest)
        {
            // -- draw the network --
            float dist = 1.1f;
            List<double> fitnesValues = new List<double>();

            if (cb_showBestGlobal.Checked || cb_showBestGen.Checked)
            {
                ViewControl1.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            }

            //for (int i = 0; i < _population.SizeNonDominatedSet; i++)
            //{
            //    InstructionTreePisa curTree = (InstructionTreePisa)(_population[i]);

            List<IMultiChromosome> archive = _population.Sel.GetArchive();
            for (int p = 0; p < archive.Count(); p++)
            {
                InstructionTreePisa curTree = (InstructionTreePisa)(archive[p]);
                //if (curTree.FitnessValues==null) continue;
                if (curTree.ShortestPathes != null)
                {
                    Vector4[] ffColors = null;
                    if (rB_fitChoice.Checked) ffColors = ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormChoiceArray()[0]);
                    if (rB_fitCentral.Checked) ffColors = ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormCentralityMetricArray()[0]);
                    DrawAnalysis(ffColors, curTree.ShortestPathes.OrigEdges, curTree.ShortestPathes.OrigPoints, new PointD(p * curTree.Border.Width * dist, iterat * curTree.Border.Height * dist));
                }
            }
            if (cB_autoShot.Checked) SetGraphicContext(glControl1); //Screenshot();
            ViewControl1.ZoomAll();
        }

        //============================================================================================================================================================================
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // check if worker thread is running
            if ((_workerThread != null) && (_workerThread.IsAlive))
            {
                _needToStop1 = true;
                _needToStop2 = true;
                while (!_workerThread.Join(100))
                    Application.DoEvents();
            }
            if (_population != null)
                if (_population.Sel != null)
                    _population.Sel.Terminate();
            PisaWrapper.TerminateAll();
        }

        //============================================================================================================================================================================
        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            // stop worker thread
            if (_workerThread != null)
            {
                _needToStop1 = true;
                _needToStop2 = true;
                while (!_workerThread.Join(100))
                    Application.DoEvents();
                _workerThread = null;
            }
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            ViewControl1.AllObjectsList = new ObjectsAllList();
            constantDrawObjects = new ObjectsDrawList();
            _isovistField = null;
            glControl1.Invalidate();
            ReSetChart(chart1, 0);
            ReSetChart(chart1, 1);
        }

        //============================================================================================================================================================================
        private void button1_Click(object sender, EventArgs e)
        {
            Screenshot();
        }

        private void Screenshot()
        {
            ViewControl1.ZoomAll();
            glControl1.Invalidate();
            Bitmap img = ViewControl1.GrabScreenshot();

            string screenshotFilename = "screenShot_" +
                                                DateTime.Now.Day.ToString("00") + "-" +
                                                DateTime.Now.Month.ToString("00") + "_" +
                                                DateTime.Now.Hour.ToString("00") +
                                                DateTime.Now.Minute.ToString("00") +
                                                DateTime.Now.Second.ToString("00") + ".png";

            string path = System.IO.Path.Combine(Environment.CurrentDirectory, screenshotFilename);
            img.Save(path, System.Drawing.Imaging.ImageFormat.Png);
            MessageBox.Show(screenshotFilename);
        }

        // Delegates to enable async calls for setting controls properties
        private delegate void SetGLCallback(System.Windows.Forms.Control control);

        // Thread safe updating of control's text property
        private void SetGraphicContext(System.Windows.Forms.Control control)
        {
            if (control.InvokeRequired)
            {
                SetGLCallback d = new SetGLCallback(SetGraphicContext);
                Invoke(d, new object[] { control });
            }
            else
            {
                Screenshot();
            }
        }

        //============================================================================================================================================================================
        //============================================================================================================================================================================
        # region renderbuffer_test
        //============================================================================================================================================================================
        //============================================================================================================================================================================
        private void button2_Click(object sender, EventArgs e)
        {
            //TestRenderBuff2();
            RenderBufferTest();
            //StartFrameBuffer();
            ////Render things as if you were rendering them to screen, and they go to frame buffer()
            ////GL.ClearColor(Color4.White);
            ////GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            ////GL.MatrixMode(MatrixMode.Projection);                                       // Projektionsmatrix einrichten
            ////GL.LoadIdentity();

            ////GL.MatrixMode(MatrixMode.Projection);
            ////GL.PushMatrix();
            ////GL.LoadIdentity();
            ////GL.MatrixMode(MatrixMode.Modelview);
            ////GL.PushMatrix();
            ////GL.LoadIdentity();

            //GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
            //GL.Color4(Color4.Yellow);
            //GL.LineWidth(1);
            //GL.Begin(BeginMode.Quads);
            //GL.Vertex2(50, 50);
            //GL.Vertex2(5000, 50);
            //GL.Vertex2(5000, 5000);
            //GL.Vertex2(50, 5000);
            //GL.End();

            //GL.LineWidth(5);// * m_zoomFactor);
            //GL.Color4(Color4.Red);
            //GL.Begin(BeginMode.Lines);
            //GL.Vertex3(10, 47, 10);
            //GL.Vertex3(5000, 4777, 5000);
            //GL.End();

            ////++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            //Bitmap img = GrabBuffer();

            //string screenshotFilename = "test_" + DateTime.Now.Day.ToString("00") + "-" +
            //                                    DateTime.Now.Month.ToString("00") + "_" +
            //                                    DateTime.Now.Hour.ToString("00") +
            //                                    DateTime.Now.Minute.ToString("00") +
            //                                    DateTime.Now.Second.ToString("00") + ".png";

            //string path = System.IO.Path.Combine(Environment.CurrentDirectory, screenshotFilename);
            //img.Save(path, System.Drawing.Imaging.ImageFormat.Png);
            ////++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

            //EndFrameBuffer();

        }

        //============================================================================================================================================================================
        const int FboWidth = 1200;
        const int FboHeight = 1200;

        private void RenderBufferTest()
        {
            uint FboHandle;
            uint ColorTexture;
            uint DepthRenderbuffer;

            // Create Color Texture
            GL.GenTextures(1, out ColorTexture);
            GL.BindTexture(TextureTarget.Texture2D, ColorTexture);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Clamp);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Clamp);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba8, FboWidth, FboHeight, 0, PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero);

            // test for GL Error here (might be unsupported format)

            GL.BindTexture(TextureTarget.Texture2D, 0); // prevent feedback, reading and writing to the same image is a bad idea

            // Create Depth Renderbuffer
            GL.Ext.GenRenderbuffers(1, out DepthRenderbuffer);
            GL.Ext.BindRenderbuffer(RenderbufferTarget.RenderbufferExt, DepthRenderbuffer);
            GL.Ext.RenderbufferStorage(RenderbufferTarget.RenderbufferExt, (RenderbufferStorage)All.DepthComponent32, FboWidth, FboHeight);

            // test for GL Error here (might be unsupported format)

            // Create a FBO and attach the textures
            GL.Ext.GenFramebuffers(1, out FboHandle);
            GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, FboHandle);
            GL.Ext.FramebufferTexture2D(FramebufferTarget.FramebufferExt, FramebufferAttachment.ColorAttachment0Ext, TextureTarget.Texture2D, ColorTexture, 0);
            GL.Ext.FramebufferRenderbuffer(FramebufferTarget.FramebufferExt, FramebufferAttachment.DepthAttachmentExt, RenderbufferTarget.RenderbufferExt, DepthRenderbuffer);

            // now GL.Ext.CheckFramebufferStatus( FramebufferTarget.FramebufferExt ) can be called, check the end of this page for a snippet.
            CheckFboStatus();

            // since there's only 1 Color buffer attached this is not explicitly required
            GL.DrawBuffer((DrawBufferMode)FramebufferAttachment.ColorAttachment0Ext);

            GL.PushAttrib(AttribMask.ViewportBit); // stores GL.Viewport() parameters
            GL.Viewport(0, 0, FboWidth, FboHeight);


            // render whatever your heart desires, when done ...

            GL.ClearColor(0, 100, 125, 255);
            GL.MatrixMode(MatrixMode.Projection);
            GL.Ortho(0, FboWidth, 0, FboHeight, -1, 1); // Bottom-left corner pixel has coordinate (0, 0)
            GL.PushMatrix();
            GL.LoadIdentity();
            // GL.Ortho(0, Size.Width, Size.Height, 0, -1, 1);
            // GL.Ortho(0, Size.Width, 0, Size.Height, -1, 1);

            //GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();
            GL.Color3(Color.Yellow);
            GL.Begin(BeginMode.Triangles);
            GL.Vertex2(10, 20);
            GL.Vertex2(100, 20);
            GL.Vertex2(100, 50);
            GL.End();

            ViewControl1.AllObjectsList.Draw();

            //GL.MatrixMode(MatrixMode.Projection);
            //GL.PopMatrix();

            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  
            // -- grab the renderbuffer --
            Bitmap img = GrabBuffer();

            // -- save the renderbuffer --
            string screenshotFilename = "test_" + DateTime.Now.Day.ToString("00") + "-" +
                                                DateTime.Now.Month.ToString("00") + "_" +
                                                DateTime.Now.Hour.ToString("00") +
                                                DateTime.Now.Minute.ToString("00") +
                                                DateTime.Now.Second.ToString("00") + ".bmp";

            string path = System.IO.Path.Combine(Environment.CurrentDirectory, screenshotFilename);
            img.Save(path, System.Drawing.Imaging.ImageFormat.Bmp);
            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++   

            GL.PopAttrib(); // restores GL.Viewport() parameters
            GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0); // return to visible framebuffer
            GL.DrawBuffer(DrawBufferMode.Back);
        }

        public Bitmap GrabBuffer()
        {
            if (OpenTK.Graphics.GraphicsContext.CurrentContext == null)
                throw new GraphicsContextMissingException();

            Bitmap bmp = new Bitmap(FboWidth, FboHeight);//glControl.ClientSize.Width, glControl.Height);
            System.Drawing.Imaging.BitmapData data = bmp.LockBits(new System.Drawing.Rectangle(0, 0, FboWidth, FboHeight), System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            //GL.ReadBuffer(ReadBufferMode.ColorAttachment0);
            GL.ReadPixels(0, 0, FboWidth, FboHeight, PixelFormat.Bgr, PixelType.UnsignedByte, data.Scan0);

            bmp.UnlockBits(data);
            //bmp.SetResolution(300, 300);
            bmp.RotateFlip(RotateFlipType.RotateNoneFlipY);
            return bmp;
        }

        private bool CheckFboStatus()
        {
            switch (GL.Ext.CheckFramebufferStatus(FramebufferTarget.FramebufferExt))
            {
                case FramebufferErrorCode.FramebufferCompleteExt:
                    {
                        Trace.WriteLine("FBO: The framebuffer is complete and valid for rendering.");
                        return true;
                    }
                case FramebufferErrorCode.FramebufferIncompleteAttachmentExt:
                    {
                        Trace.WriteLine("FBO: One or more attachment points are not framebuffer attachment complete. This could mean there’s no texture attached or the format isn’t renderable. For color textures this means the base format must be RGB or RGBA and for depth textures it must be a DEPTH_COMPONENT format. Other causes of this error are that the width or height is zero or the z-offset is out of range in case of render to volume.");
                        break;
                    }
                case FramebufferErrorCode.FramebufferIncompleteMissingAttachmentExt:
                    {
                        Trace.WriteLine("FBO: There are no attachments.");
                        break;
                    }
                /* case  FramebufferErrorCode.GL_FRAMEBUFFER_INCOMPLETE_DUPLICATE_ATTACHMENT_EXT: 
                     {
                         Trace.WriteLine("FBO: An object has been attached to more than one attachment point.");
                         break;
                     }*/
                case FramebufferErrorCode.FramebufferIncompleteDimensionsExt:
                    {
                        Trace.WriteLine("FBO: Attachments are of different size. All attachments must have the same width and height.");
                        break;
                    }
                case FramebufferErrorCode.FramebufferIncompleteFormatsExt:
                    {
                        Trace.WriteLine("FBO: The color attachments have different format. All color attachments must have the same format.");
                        break;
                    }
                case FramebufferErrorCode.FramebufferIncompleteDrawBufferExt:
                    {
                        Trace.WriteLine("FBO: An attachment point referenced by GL.DrawBuffers() doesn’t have an attachment.");
                        break;
                    }
                case FramebufferErrorCode.FramebufferIncompleteReadBufferExt:
                    {
                        Trace.WriteLine("FBO: The attachment point referenced by GL.ReadBuffers() doesn’t have an attachment.");
                        break;
                    }
                case FramebufferErrorCode.FramebufferUnsupportedExt:
                    {
                        Trace.WriteLine("FBO: This particular FBO configuration is not supported by the implementation.");
                        break;
                    }
                default:
                    {
                        Trace.WriteLine("FBO: Status unknown. (yes, this is really bad.)");
                        break;
                    }
            }
            return false;
        }
        #endregion

        //============================================================================================================================================================================
        private void exportDxfGraphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // When user clicks button, show the dialog.
            string iniPath = "C:/Users/koenigr/Pictures/130424_StreetNetwork/dxf";//System.Reflection.Assembly.GetExecutingAssembly().Location;
            saveDxfDialog.InitialDirectory = Path.GetFullPath(iniPath);
            saveDxfDialog.Filter = "Dxf file|*.dxf";
            saveDxfDialog.Title = "Save an dxf file";
            saveDxfDialog.RestoreDirectory = true;
            saveDxfDialog.ShowDialog();
        }

        //============================================================================================================================================================================
        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            // Get file name.  
            string name = saveDxfDialog.FileName;
            // create the dxf file that shall be saved
            DxfDocument dxf = DxfConversions.ToDxfFile(ViewControl1.AllObjectsList.ObjectsToDraw, DxfVersion.AutoCad2000);
            // Write to the file name selected.
            dxf.Save(name);
            if (_population != null) SaveFitnessValues(name);
        }

        //============================================================================================================================================================================
        private void SaveFitnessValues(string name)
        {
            string curName = name.Remove(name.Length - 4);
            curName += ".txt";
            StreamWriter sw = new StreamWriter(curName);
            try
            {
                sw.WriteLine("Values for file:" + name);
                sw.WriteLine(_iterations.ToString() + " : Generations");
                sw.WriteLine(_populationSize.ToString() + " : Population Size");
                sw.WriteLine(ControlParameters.TreeDepth.ToString() + " : Tree Depth");
                InstructionTreePisa bestTree = (InstructionTreePisa)_population.BestChromosome;
                string mode = (ControlParameters.Choice) ? "Choice" : "Centrality";
                //if (bestTree != null) sw.WriteLine(bestTree.Fitness.ToString() + " : Best Fitness Value: " + mode);
                //DEBUG.WriteLine("Time of Travel: " + duration.ToString("dd\.hh\:mm\:ss"))
                sw.WriteLine(span.ToString(@"hh\:mm\:ss") + " : Calculation Time");
                //foreach (List<double> fitValues in _fitnesLists)
                for (int i = 0; i < _fitnesLists.Count; i++)
                {
                    List<double> fitValues = new List<double>();
                    fitValues = _fitnesLists[i];
                    string valueLine = i.ToString() + "; ";
                    for (int k = 0; k < fitValues.Count; k++)
                    {
                        double curValue = fitValues[k];
                        valueLine += curValue.ToString() + "; ";
                    }
                    sw.WriteLine(valueLine); // + "; Generation " + i.ToString() );
                }
                sw.Close();
            }
            catch (InvalidCastException e)
            {
                if (sw != null) sw.Close();
            }
        }

        //============================================================================================================================================================================
        private void loadDxfFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // When user clicks button, show the dialog.
            string iniPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            openDxfDialog.InitialDirectory = Path.GetFullPath(iniPath);
            openDxfDialog.Filter = "Dxf file|*.dxf";
            openDxfDialog.Title = "Load an dxf file";
            openDxfDialog.RestoreDirectory = true;
            openDxfDialog.ShowDialog();
        }

        //============================================================================================================================================================================
        private void openDxfDialog_FileOk(object sender, CancelEventArgs e)
        {
            // Get file name.  
            string name = openDxfDialog.FileName;
            // create the new objects from the selected file
            ObjectsDrawList newGObjects = DxfConversions.GObjectsFormDxf(name);
            // add the new objects to the current ObjectsToDraw-list
            ViewControl1.AllObjectsList.ObjectsToDraw.AddRange(newGObjects);
            ViewControl1.ZoomAll();
        }

        //============================================================================================================================================================================
        private void toolStripButton_LoadScene_Click(object sender, EventArgs e)
        {
            string iniPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            openDxfDialog.InitialDirectory = Path.GetFullPath(iniPath);
            constantDrawObjects = new ObjectsDrawList();

            iniPath = Path.Combine(Directory.GetCurrentDirectory(), "130823_areaBuildings12.dxf");
            ObjectsDrawList newGObjects = DxfConversions.GObjectsFormDxf(iniPath);
            foreach (object gObj in newGObjects)
            {
                if (gObj.GetType() == typeof(GPolygon))
                {
                    GPolygon gPoly = (GPolygon)gObj;
                    gPoly.BorderWidth = 1;
                    gPoly.Filled = true;
                    gPoly.FillColor = ConvertColor.CreateColor4(Color.Black);
                }
            }
            constantDrawObjects.AddRange(newGObjects);

            iniPath = Path.Combine(Directory.GetCurrentDirectory(), "130823_areaStreets7.dxf");
            ObjectsDrawList newGObjects2 = DxfConversions.GObjectsFormDxf(iniPath);
            foreach (object gObj in newGObjects2)
            {
                if (gObj.GetType() == typeof(GLine))
                {
                    GLine gLine = (GLine)gObj;
                    gLine.Width = 1;
                    gLine.Color = ConvertColor.CreateColor4(Color.Black);
                }
            }
            constantDrawObjects.AddRange(newGObjects2);

            ViewControl1.AllObjectsList.ObjectsToDraw.AddRange(constantDrawObjects);
            ViewControl1.ZoomAll();

        }

        //============================================================================================================================================================================
        private void loadGraphFromDxfToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openDxfGraphDialog.ShowDialog();
            /*if (ShortPahtesMetric == null) ShortestPathesMetricAnalysis();
            if (ShortPahtesAngular == null) ShortestPathesAngularAnalysis();
            LocalAnalysis();*/
        }

        //============================================================================================================================================================================
        private void openDxfGraphDialog_FileOk(object sender, CancelEventArgs e)
        {
            string name = openDxfGraphDialog.FileName;
            openDxfGraphDialog.InitialDirectory = System.Reflection.Assembly.GetExecutingAssembly().Location;

            DxfDocument dxf = DxfConversions.FromDxfFile(name);
            //netDxf.DxfObject
            List<netDxf.Entities.Line> lines = dxf.Lines.ToList();
            List<LineD> importedLines = new List<LineD>();
            foreach (netDxf.Entities.Line dLine in lines)
            {
                importedLines.Add(new LineD(dLine.StartPoint.X, dLine.StartPoint.Y, dLine.EndPoint.X, dLine.EndPoint.Y));
            }
            StreetGenerator = new StreetPattern(null);
            StreetGenerator.Graph = new Subdivision();
            LineD[] arrLines = importedLines.ToArray();
            StreetGenerator.Graph = Subdivision.FromLines(arrLines, .0);
            //foreach (LineD line in importedLines)
            //{
            //    ViewControl1.AllObjectsList.ObjectsToDraw.Add(new GLine(line));
            //}
            //AnalyseNetwork(StreetGenerator.Graph);
            DrawNetwork(StreetGenerator.Graph);
            ViewControl1.ZoomAll();
        }

        //============================================================================================================================================================================
        private void SetControlParameters()
        {
            ControlParameters.Centrality = rB_fitCentral.Checked;
            ControlParameters.Choice = rB_fitChoice.Checked;
            ControlParameters.AngleRange = Convert.ToInt16(uD_angle.Value);
            ControlParameters.LengthRange = Convert.ToInt16(uD_maxLength.Value - uD_minLength.Value);
            ControlParameters.MinLength = Convert.ToInt16(uD_minLength.Value);
            ControlParameters.MaxChilds = Convert.ToInt16(uD_connectivity.Value);
            ControlParameters.LineWidth = Convert.ToInt16(uD_lineWidth.Value);
            ControlParameters.TreeDepth = Convert.ToInt16(uD_treeDepth.Value);
        }

        //============================================================================================================================================================================
        private void uD_treeDepth_ValueChanged_1(object sender, EventArgs e)
        {
            SetControlParameters();
        }


        private void rB_fitChoice_CheckedChanged(object sender, EventArgs e)
        {
            SetControlParameters();
        }

        private void rB_fitCentral_CheckedChanged(object sender, EventArgs e)
        {
            SetControlParameters();
        }

        private void uD_generations_ValueChanged(object sender, EventArgs e)
        {
            SetControlParameters();
        }

        private void uD_minLength_ValueChanged(object sender, EventArgs e)
        {
            SetControlParameters();
        }

        private void uD_maxLength_ValueChanged(object sender, EventArgs e)
        {
            SetControlParameters();
        }

        private void uD_divAngle_ValueChanged(object sender, EventArgs e)
        {
            SetControlParameters();
        }

        private void uD_distance_ValueChanged(object sender, EventArgs e)
        {
            SetControlParameters();
        }

        private void uD_angle_ValueChanged(object sender, EventArgs e)
        {
            SetControlParameters();
        }

        private void uD_connectivity_ValueChanged(object sender, EventArgs e)
        {
            SetControlParameters();
        }

        private void uD_lineWidth_ValueChanged(object sender, EventArgs e)
        {
            SetControlParameters();
        }

        private void uD_treeDepth_ValueChanged(object sender, EventArgs e)
        {
            SetControlParameters();
        }

        //==============================================================================================
        //==============================================================================================
        //==============================================================================================
        # region BuildingLayout

        //private REvo EA;
        //private GRectangle _borderLayout;
        private GPolygon _borderPoly;
        private ChromosomeLayout _curLayout;

        //============================================================================================================================================================================
        private void ButtonBuildingLayout_Click(object sender, EventArgs e)
        {
            //_borderLayout = new GRectangle(0+400, 500+200, 500, 500);
            //ViewControl1.AllObjectsList.ObjectsToDraw.Add(_borderLayout);
            //ViewControl1.ZoomAll();

            // -- the points in the cape town area --
            Vector2D[] corners = new Vector2D[4];
            //corners[0] = new Vector2D(370, 195);
            //corners[1] = new Vector2D(780, 250);
            //corners[2] = new Vector2D(860, 420);
            //corners[3] = new Vector2D(560, 560);

            corners[0] = new Vector2D(0, 0);
            corners[1] = new Vector2D(410, 50);
            corners[2] = new Vector2D(490, 220);
            corners[3] = new Vector2D(190, 360);

            _borderPoly = new GPolygon(corners);
            ViewControl1.AllObjectsList.ObjectsToDraw.Add(_borderPoly);
            ViewControl1.ZoomAll();
            Poly2D borderPoly = new Poly2D(_borderPoly.PolyPoints);//_borderLayout.Rect.Points);


            //EA = new REvo(borderPoly, RParameter.popSize, RParameter.roomsNr);
            _curLayout = new ChromosomeLayout(borderPoly,
                ControlParameters.MinNrBuildings, ControlParameters.MaxNrBuildings,
                 ControlParameters.Density, ControlParameters.MinSideRatio);

            // run worker thread
            timer1.Enabled = true;
            //_needToStop2 = false;
            //_workerThread = new Thread(new ThreadStart(SearchBuildingLayout));
            //_workerThread.Start();

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            MainLoop();
        }
        //==============================================================================================
        private void MainLoop()
        //void SearchBuildingLayout()
        {
            //FPS set up
            double FPS = 30.0;
            long ticks1 = 0;
            long ticks2 = 0;
            double interval = (double)Stopwatch.Frequency / FPS;
            DateTime start = DateTime.Now;// DateTime.MinValue;
            DateTime end = DateTime.MinValue;
            TimeSpan diff;
            //
            int i = 1;
            //while (!_needToStop2)
            //for (int l = 0; l < 100; l++)
            {
                Application.DoEvents();
                end = DateTime.Now;
                diff = end - start;

                ticks2 = Stopwatch.GetTimestamp();
                if (ticks2 >= ticks1 + interval)
                {
                    ticks1 = Stopwatch.GetTimestamp();

                    //Actions
                    //if (EA != null)
                    //{
                    //    EA.Run();

                    //    DrawLayout();
                    //}
                    if (_curLayout != null)
                    {
                        _curLayout.Adapt();

                        DrawLayout();
                    }
                    // --- interface information ---
                    int test = Tools.CalculateFrameRate();
                    string toHead = "Layout with " + test.ToString() + " fps";
                    SetText(this, toHead);

                }
                i++;
                //      Debug.WriteLine(i.ToString() + ". iterat, bestFit", Math.Round(EA.Parents.FindFittestOverlap().FitRealOverl, 2).ToString());
                // Todo: Sleep funktion avoids "licker" problem while drag & drop...
                //Thread.Sleep(3); //frees up the cpu
            }
        }

        //============================================================================================================================================================================
        //private void DrawLayout()
        //{
        //    ViewControl1.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
        //    ViewControl1.AllObjectsList.ObjectsToInteract = new ObjectsInteractList();

        //    if (constantDrawObjects != null) ViewControl1.AllObjectsList.ObjectsToDraw.AddRange(constantDrawObjects);
        //    if (_isovistField != null) ViewControl1.AllObjectsList.ObjectsToDraw.Add(new GGrid(_isovistField));
        //    ViewControl1.AllObjectsList.ObjectsToDraw.Add(_borderPoly);


        //    if (EA != null)
        //    {
        //        ViewControl1.AllObjectsList.ObjectsToDraw.AddRange(EA.PhenotypeGl);
        //        ViewControl1.AllObjectsList.ObjectsToInteract.AddRange(EA.PhenotypeGl);
        //    }
        //    ViewControl1.Invalidate();
        //    // ViewControl1.ZoomAll();
        //}
        //============================================================================================================================================================================
        private void DrawLayout()
        {
            ViewControl1.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            ViewControl1.AllObjectsList.ObjectsToInteract = new ObjectsInteractList();
            if (constantDrawObjects != null) ViewControl1.AllObjectsList.ObjectsToDraw.AddRange(constantDrawObjects);
            if (_isovistField != null) ViewControl1.AllObjectsList.ObjectsToDraw.Add(new GGrid(_isovistField));
            ViewControl1.AllObjectsList.ObjectsToDraw.Add(_borderPoly);
            //if (EA != null) ViewControl1.AllObjectsList.ObjectsToDraw.AddRange(EA.PhenotypeGl);

            if (_curLayout.Gens != null)
               // foreach (GenBuildingLayout building in _curLayout.Gens)

                    for (int i = 0; i < _curLayout.Gens.Count; i++ )
                    {
                        GenBuildingLayout building = _curLayout.Gens[i];

                        //GPolygon bPoly = new GPolygon(building.Poly);                    
                        //bPoly.Identity = _curLayout.Indentity;
                        //bPoly.DeltaZ = 0.1f;
                        //bPoly.BorderWidth = 5;
                        //bPoly.FillColor = ConvertColor.CreateColor4(Color.Blue);
                        //bPoly.Filled = true;
                        ////bPoly.Move(deltaP);
                        //ViewControl1.AllObjectsList.ObjectsToDraw.Add(bPoly);


                        GCube cube = new GCube(building.Poly.PointsFirstLoop, Convert.ToSingle(building.Height));
                        cube.Identity = _curLayout.Indentity;
                        cube.DeltaZ = 0.1f;
                        cube.BorderWidth = 5;
                        cube.FillColor = ConvertColor.CreateColor4(Color.Gray);

                        if (i == _lastSelectedIdx)
                            cube.FillColor = ConvertColor.CreateColor4(Color.Red);

                        cube.Filled = true;
                        ViewControl1.AllObjectsList.ObjectsToDraw.Add(cube);
                    }

            GPolygon borderPoly = new GPolygon(_curLayout.Border);
            //borderPoly.Move(deltaP);
            ViewControl1.AllObjectsList.ObjectsToDraw.Add(borderPoly);

            ViewControl1.Invalidate();
            // ViewControl1.ZoomAll();
        }

        # endregion

        //==============================================================================================
        //==============================================================================================
        //==============================================================================================
        # region IsovistAnalysis
        IsovistAnalysis _isovistField;
        List<Poly2D> _obstRects;
        List<Line2D> _obstLines;
        //============================================================================================================================================================================
        private void Button_IsovistAnalysis_Click(object sender, EventArgs e)
        {
            IsovistAnalysis();
        }

        //============================================================================================================================================================================
        private void IsovistAnalysis()
        {
            _obstLines = new List<Line2D>();
            _obstRects = new List<Poly2D>();
            if (_curLayout != null)
            {
                _isovistBorder = _borderPoly.Poly.Clone();
                _isovistBorder.Offset(20);
                Rect2D borderRect = GeoAlgorithms2D.BoundingBox(_isovistBorder.PointsFirstLoop);
                List<GenBuildingLayout> rects = _curLayout.Gens;

                // test environmental polygons
                if (constantDrawObjects != null)
                    foreach (GeoObject testObj in constantDrawObjects)
                    {
                        if (testObj is GPolygon)
                        {
                            GPolygon envPoly = (GPolygon)testObj;
                            if (envPoly.Poly.Distance(_isovistBorder) <= 0)
                            {
                                _obstLines.AddRange(envPoly.Edges.ToList());
                                _obstRects.Add(envPoly.Poly);
                            }
                        }
                    }

                // add the buildings
                foreach (GenBuildingLayout curRect in rects)
                {
                    //if (curRect.Filled)
                    {
                        _obstLines.AddRange(curRect.Poly.Edges.ToList());
                        _obstRects.Add(curRect.Poly);
                    }
                }

                _obstLines.AddRange(_isovistBorder.Edges);
                float cellSize = 10;
                List<Vector2D> gridPoints = PointsForLucyIsovist(_curLayout.Border, _curLayout.Gens, cellSize);

                _isovistField = new IsovistAnalysis(gridPoints, _obstLines, cellSize);
                _isovistField.DisplayMeasure = comboBox_Isovist.Text;

                TimeSpan calcTime1, calcTime2;
                DateTime start = DateTime.Now;

                // -- run localr --
                _isovistField.Calculate(0.05f);

                DateTime end = DateTime.Now;
                calcTime1 = end - start;

                // -- run via server -- // approximately 3-4 times? slower on local maschine...
                //start = DateTime.Now;
                //_isovistField.CalculateViaService(borderRect, 5, _obstLines, _obstRects, _isovistBorder, 0.05f);

                //end = DateTime.Now; ;
                //calcTime2 = end - start;

                //Debug.WriteLine("grid with " + _isovistField.NumberOfActiveCells.ToString() + " active cells: ");
                //Debug.WriteLine("local calculation time 1: " + calcTime1.TotalMilliseconds.ToString() + " ms");
                //Debug.WriteLine("server calculation time 2: " + calcTime2.TotalMilliseconds.ToString() + " ms");

                string measure = "";
                if (comboBox_Isovist.SelectedValue != null)
                    measure = comboBox_Isovist.SelectedValue.ToString();
                _smartGrid = new GGrid(gridPoints, _isovistField.GetMeasure(measure), _isovistField.CellSize);

                propertyGrid2.SelectedObject = _isovistField;

            }
            DrawLayout();
        }

        //============================================================================================================================================================================
        private List<Vector2D> PointsForLucyIsovist(Poly2D field, List<GenBuildingLayout> buildings, float cellSize)
        {
            List<Vector2D> result = new List<Vector2D>();
            //_cellSize = cellSize;
            int countX = (int)(field.BoundingBox().Width / cellSize);
            int countY = (int)(field.BoundingBox().Height / cellSize);
            Vector2D startP = field.BoundingBox().BottomLeft;
            for (int i = 0; i < countX + 1; i++)
            {
                for (int j = 0; j < countY + 1; j++)
                {
                    Vector2D curPos = new Vector2D(startP.X + i * cellSize + cellSize / 2, startP.Y + j * cellSize + cellSize / 2);
                    //m_activeCells[j * m_countX + i] = true;
                    bool active = true;
                    foreach (GenBuildingLayout curGen in buildings)
                    {
                        if (field.ContainsPoint(curPos))
                        {
                            if (!curGen.Open)
                            {
                                if (curGen.Poly.ContainsPoint(curPos))
                                {
                                    active = false;
                                    break;
                                }
                            }
                        }
                        else active = false;
                    }

                    if (active) result.Add(curPos);
                }
            }
            return result;
        }

        //============================================================================================================================================================================
        private void Button_StartStop_Click(object sender, EventArgs e)
        {
            timer1.Enabled = !timer1.Enabled;
        }

        //============================================================================================================================================================================
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ControlParameters.DisplayMeasure = comboBox_Isovist.Text;
            if (_isovistField != null)
            {
                _isovistField.DisplayMeasure = comboBox_Isovist.Text;
                //_isovistField.WriteToGridValues();
                string measure = comboBox_Isovist.SelectedItem.ToString();
                _smartGrid = new GGrid(_isovistField.AnalysisPoints, _isovistField.GetMeasure(measure), _isovistField.CellSize);
                ViewControl1.Invalidate();
            }
        }

        //============================================================================================================================================================================
        private void propertyGrid2_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if (_isovistField != null)
            {
                Rect2D borderRect = GeoAlgorithms2D.BoundingBox(_isovistBorder.PointsFirstLoop);
                // _isovistField = new IsovistFieldOLD(borderRect, 5, _obstLines, _obstRects, _isovistBorder);
                _isovistField.Calculate(_isovistField.Precision);
                ViewControl1.Invalidate();
            }
        }

        private void propertyGrid1_Click(object sender, EventArgs e)
        {
            //timer1.Enabled = false;
        }

        private void propertyGrid1_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            //zunächt benötigen wir das aktuell im PropertyGrid angezeigt Objekt. Da nicht bekannt ist, um welches es sich handelt, 
            //erfolgt hier der Aufrug allgemeine als "object"
            object obj;
            if (e.ChangedItem.Parent.Value != null) obj = e.ChangedItem.Parent.Value;
            else obj = propertyGrid1.SelectedObject;

            //nun wird geprüft, ob es sich bei dem geänderten Wert um ein Property handelte!
            if (e.ChangedItem.GridItemType == GridItemType.Property)
            {
                PropertyInfo pInfo = obj.GetType().GetProperty(e.ChangedItem.PropertyDescriptor.Name);
                pInfo.SetValue(obj, e.ChangedItem.Value, null);
                if (_lastSelectedIdx != -1)
                {
                    //for (int k = 0; k < EA.Parents.Count; k++)
                    {
                        _curGen = _curLayout.Gens[_lastSelectedIdx];
                        pInfo.SetValue(_curGen, e.ChangedItem.Value, null);
                    }
                }
            }

            ViewControl1.Invalidate();
        }
        # endregion

        private void Button_Move_Click(object sender, EventArgs e)
        {
            MouseMode = "Move";
            statusLabel.Text = "Drag and drop building to MOVE it.";
            //this.Cursor = Cursors.SizeAll;
        }

        private void Button_Rotate_Click(object sender, EventArgs e)
        {
            MouseMode = "Rotate";
            statusLabel.Text = "Hold left button and move mouse up and down to ROTATE building.";
        }

        private void Button_Scale_Click_1(object sender, EventArgs e)
        {
            MouseMode = "Scale";
            statusLabel.Text = "Hold left button and move mouse up and down to SCALE building.";
        }

        private void Button_Size_Click(object sender, EventArgs e)
        {
            MouseMode = "Size";
            statusLabel.Text = "Hold left button and move mouse up and down to change building SIZE.";
        }

        //==============================================================================================
        //============================== Layout Optimization ===========================================
        //==============================================================================================
        private void Button_OptiLayout_Click(object sender, EventArgs e)
        {
            // -- the points in the cape town area --
            Vector2D[] corners = new Vector2D[4];
            corners[0] = new Vector2D(370, 195);
            corners[1] = new Vector2D(780, 250);
            corners[2] = new Vector2D(860, 420);
            corners[3] = new Vector2D(560, 560);

            _borderPoly = new GPolygon(corners);
            // get population size
            try
            {
                _populationSize = Convert.ToInt16(uD_populationSize.Value);// 20; // Math.Max(10, Math.Min(100, int.Parse(populationSizeBox.Text)));
            }
            catch
            {
                _populationSize = 20;
            }
            // iterations
            try
            {
                _iterations = Convert.ToInt16(uD_generations.Value); ; //Math.Max(0, int.Parse(iterationsBox.Text));
            }
            catch
            {
                _iterations = 20;
            }

            //InitialGraph();
            // update settings controls
            //UpdateSettings();
            ViewControl1.DeactivateInteraction();
            //selectionMethod = selectionBox.SelectedIndex;
            //greedyCrossover = greedyCrossoverBox.Checked;
            // disable all settings controls except "Stop" button
            //EnableControls(false);

            // run worker thread           
            _needToStop1 = false;
            if (sB_multiOpti.Checked)
            {
                _workerThread = new Thread(new ThreadStart(SearchLayoutMulti));
            }
            if (sB_singleOpti.Checked)
            {
                _workerThread = new Thread(new ThreadStart(SearchLayoutSingle));
            }
            _workerThread.Start();

        }

        //============================================================================================================================================================================
        // Worker thread
        void SearchLayoutSingle()
        {
            // measure the needed time:
            DateTime start = DateTime.Now;// DateTime.MinValue;
            DateTime end = DateTime.MinValue;

            // create fitness function
            FitnessFunctionLayoutSingle fitnessFunction = new FitnessFunctionLayoutSingle();

            // create population
            _populationSingle = new Population(_populationSize,
                //(greedyCrossover) ? new InstructionTree(treeDepth) : new PermutationChromosome(treeDepth),
                new ChromosomeLayoutSingle(_borderPoly.Poly.Clone(), 10, 20, 0.5, 0.5),
                fitnessFunction,
                //(_selectionMethod == 0) ? (ISelectionMethod)new EliteSelection() :
                //(_selectionMethod == 1) ? (ISelectionMethod)new RankSelection() :
                (ISelectionMethod)new EliteSelection() //Multi_EliteSelection()//
                );
            _populationSingle.MutationRate = 0.25;
            //population.CrossoverRate = 0.0;
            _populationSingle.RandomSelectionPortion = 0.2;
            // iterations
            DrawLayoutSingle(0);
            Debug.WriteLine("bestFit", Math.Round(_populationSingle.FitnessMax, 2).ToString());

            int i = 1;

            // loop
            while (!_needToStop1)
            {
                // run one epoch of genetic algorithm
                _populationSingle.RunEpoch();
                //DrawBest(i);
                DrawLayoutSingle(i);

                Debug.WriteLine(i.ToString() + ". iterat, bestFit", Math.Round(_populationSingle.FitnessMax, 2).ToString());
                Thread.Sleep(100);

                // increase current iteration
                i++;
                //
                if ((_iterations != 0) && (i > _iterations))
                    break;
            }
            end = DateTime.Now;
            span = end - start;

            ViewControl1.ActivateInteraction();
        }

        //============================================================================================================================================================================
        // Worker thread
        void SearchLayoutMulti()
        {
            // measure the needed time:
            DateTime start = DateTime.Now;// DateTime.MinValue;
            DateTime end = DateTime.MinValue;

            // create fitness function
            FitnessFunctionLayout fitnessFunction = new FitnessFunctionLayout();

            _isovistBorder = _borderPoly.Poly.Clone();
            _isovistBorder.Offset(20);
            Rect2D borderRect = GeoAlgorithms2D.BoundingBox(_isovistBorder.PointsFirstLoop);
            //List<GPolygon> rects = EA.PhenotypeGl;

            // create population
            _population = new MultiPopulation(_populationSize, 30, 40, 20,
                //(greedyCrossover) ? new InstructionTreePisa(treeDepth) : new PermutationChromosome(treeDepth),
                new ChromosomeLayout(_borderPoly.Poly.Clone(), 10, 20, 0.5, 0.5),
                fitnessFunction,
                //(_selectionMethod == 0) ? (ISelectionMethod)new EliteSelection() :
                //(_selectionMethod == 1) ? (ISelectionMethod)new RankSelection() :
                (IMultiSelectionMethod)new NonDominatedSelection(), //Multi_EliteSelection()//
                CalculationType.Method.Local
                );
            _population.MutationRate = 0.25;
            //population.CrossoverRate = 0.0;
            _population.RandomSelectionPortion = 0.1;
            // iterations
            DrawLayoutMulti(0);
            //Debug.WriteLine("bestFit", Math.Round(_population.FitnessMax, 2).ToString());

            int i = 1;

            // loop
            while (!_needToStop1)
            {
                // run one epoch of genetic algorithm
                _population.RunEpoch();
                //DrawBest(i);
                DrawLayoutMulti(i);

                // --- Achtung: Ading data to chart makes the program slow down over time!!! ------
                //ReSetChart(chart1, 0);
                //List<IMultiChromosome> archive = _population.Sel.GetArchive();
                //for (int p = 0; p < archive.Count(); p++)
                //{
                //    InstructionTreePisa curTree = (InstructionTreePisa)(archive[p]);
                //    if (curTree.FitnessValues.Count > 1)
                //    {
                //        SetChart(chart1, Math.Round(curTree.FitnessValues[0], 5), curTree.FitnessValues[1], 0);
                //        SetChart(chart1, Math.Round(curTree.FitnessValues[0], 5), curTree.FitnessValues[1], 1);
                //    }
                //}

                //Debug.WriteLine(i.ToString() + ". iterat, bestFit", Math.Round(_population.FitnessMax, 2).ToString());
                Thread.Sleep(100);

                // set current iteration's info
                SetText(lbl_nDSet, _population.SizeNonDominatedSet.ToString());
                //SetText(currentIterationBox, i.ToString());
                //SetText(pathLengthBox, fitnessFunction.DistanceValue(population.BestChromosome).ToString());

                // increase current iteration
                i++;
                //
                if ((_iterations != 0) && (i > _iterations))
                    break;
            }
            // enable settings controls
            //EnableControls(true);
            end = DateTime.Now;
            span = end - start;

            _population.Sel.Terminate();

            ViewControl1.ActivateInteraction();
        }

        //============================================================================================================================================================================
        private void DrawLayoutMulti(int iterat)//Population pop); //InstructionTreePisa fittest)
        {
            // -- draw the network --
            float dist = 1.1f;
            List<double> fitnesValues = new List<double>();

            if (cb_showBestGlobal.Checked || cb_showBestGen.Checked)
            {
                ViewControl2.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
                //if (constantDrawObjects != null) ViewControl1.AllObjectsList.ObjectsToDraw.AddRange(constantDrawObjects);
            }

            //for (int i = 0; i < _population.SizeNonDominatedSet; i++)
            //{
            //    InstructionTreePisa curTree = (InstructionTreePisa)(_population[i]);

            List<IMultiChromosome> archive = _population.Sel.GetArchive();
            for (int p = 0; p < archive.Count(); p++)
            {
                ChromosomeLayout curChromo = (ChromosomeLayout)(archive[p]);
                //if (curTree.FitnessValues==null) continue;
                if (curChromo.IsovistField != null)
                {
                    GGrid VisualIsovistField = new GGrid(curChromo.IsovistField);
                    Vector2D deltaP = new Vector2D(p * curChromo.BoundingRect.Width * dist, iterat * curChromo.BoundingRect.Height * dist);
                    VisualIsovistField.Move(deltaP);
                    ViewControl2.AllObjectsList.ObjectsToDraw.Add(VisualIsovistField);

                    if (curChromo.Environment != null)
                        foreach (Poly2D envObj in curChromo.Environment)
                        {
                            GPolygon ePoly = new GPolygon(envObj);
                            ePoly.Move(deltaP);
                            ViewControl2.AllObjectsList.ObjectsToDraw.Add(ePoly);
                        }

                    if (curChromo.Gens != null)
                        foreach (GenBuildingLayout building in curChromo.Gens)
                        {
                            GPolygon bPoly = new GPolygon(building.Poly);
                            bPoly.Move(deltaP);
                            ViewControl2.AllObjectsList.ObjectsToDraw.Add(bPoly);
                        }

                    GPolygon borderPoly = new GPolygon(curChromo.Border);
                    borderPoly.Move(deltaP);
                    ViewControl2.AllObjectsList.ObjectsToDraw.Add(borderPoly);

                }
            }
            //if (cB_autoShot.Checked) SetGraphicContext(glControl1); //Screenshot();
            ViewControl2.Invalidate();
            ViewControl2.ZoomAll();
        }

        //============================================================================================================================================================================
        private void DrawLayoutSingle(int iterat)//Population pop); //InstructionTreePisa fittest)
        {
            // -- draw the network --
            float dist = 1.1f;
            List<double> fitnesValues = new List<double>();

            if (cb_showBestGlobal.Checked || cb_showBestGen.Checked)
            {
                ViewControl2.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
                //if (constantDrawObjects != null) ViewControl1.AllObjectsList.ObjectsToDraw.AddRange(constantDrawObjects);
            }


            for (int p = 0; p < _populationSingle.Size; p++)
            {
                ChromosomeLayoutSingle curChromo = (ChromosomeLayoutSingle)(_populationSingle[p]);
                //if (curTree.FitnessValues==null) continue;
                if (curChromo.IsovistField != null)
                {
                    GGrid VisualIsovistField = new GGrid(curChromo.IsovistField);
                    Vector2D deltaP = new Vector2D(p * curChromo.BoundingRect.Width * dist, iterat * curChromo.BoundingRect.Height * dist);
                    VisualIsovistField.Move(deltaP);
                    ViewControl2.AllObjectsList.ObjectsToDraw.Add(VisualIsovistField);

                    if (curChromo.Environment != null)
                        foreach (Poly2D envObj in curChromo.Environment)
                        {
                            GPolygon ePoly = new GPolygon(envObj);
                            ePoly.Move(deltaP);
                            ViewControl2.AllObjectsList.ObjectsToDraw.Add(ePoly);
                        }

                    if (curChromo.Gens != null)
                        foreach (GenBuildingLayout building in curChromo.Gens)
                        {
                            GPolygon bPoly = new GPolygon(building.Poly);
                            bPoly.Move(deltaP);
                            ViewControl2.AllObjectsList.ObjectsToDraw.Add(bPoly);
                        }

                    GPolygon borderPoly = new GPolygon(curChromo.Border);
                    borderPoly.Move(deltaP);
                    ViewControl2.AllObjectsList.ObjectsToDraw.Add(borderPoly);

                }
            }
            //if (cB_autoShot.Checked) SetGraphicContext(glControl1); //Screenshot();
            ViewControl2.Invalidate();
            ViewControl2.ZoomAll();
        }

        private void glControl1_Load(object sender, EventArgs e)
        {

        }

    }
}

//#endif

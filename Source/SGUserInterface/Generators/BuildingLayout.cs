﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPlan.Optimization;
using CPlan.Geometry;
using CPlan.VisualObjects;

using OpenTK;

namespace CPlan.UrbanPattern
{
    public class BuildingLayout
    {
        //============================================================================================================================================================================
        /// <summary>
        /// Grow a Building Layout from the BuildingTree, resp. from the List of BuildingNodes
        /// </summary>
        /// <param name="InstructionNodes">List of InstructionNodes from a InstructionTree. </param>
        /// <returns></returns>
        public static List<GPolygon> GrowBuildingLayout(BuildingTree bulidingTree) //Subdivision GrowStreetPattern()
        {
            List<GPolygon> iniBuildings = bulidingTree.InitalBuildings;
            List<GPolygon> buildingLayout = new List<GPolygon>();
            foreach (GPolygon building in iniBuildings)
            {
                buildingLayout.Add(building);
            }

            foreach (BuildingNode node in bulidingTree.Nodes)
            {
                node.Closed = false;
            }

            for (int n = 0; n < bulidingTree.Nodes.Count; n++)
            {
                BuildingNode curBuildingNode = bulidingTree.Nodes[n];
                BuildingNode parentBuildingNode = bulidingTree.Nodes.Find(BuildingNode => BuildingNode.Index == curBuildingNode.ParentIndex);

                if (curBuildingNode.ParentIndex >= 0 && parentBuildingNode.Closed == true)
                {
                    curBuildingNode.Closed = true;
                }
                else if (curBuildingNode.ParentIndex >= 0 && parentBuildingNode.Closed == false)
                {
                    // -- select the reference edge
                    short idx = parentBuildingNode.Pointer;
                    idx = parentBuildingNode.Sequence[idx];
                    parentBuildingNode.Pointer++;
                    Line2D curRefLine = parentBuildingNode.GetBuildingEdge(idx);
                    // -- vector to indicate the direction for the new building related to the parent
                    Vector2D mid = curRefLine.Center;
                    Vector2D parCenter = parentBuildingNode.Building.GetCentroid();
                    Vector2D fp = new Vector2D();// = curRefLine.End - curRefLine.Start;//   curRefLine.Start.NormalVector();
                    GeoAlgorithms2D.DistancePointToLine(parCenter, curRefLine.Start, curRefLine.End, fp);
                    Vector2D ortho = fp - parCenter;

                    Vector2D direction;// = mid - parCenter;
                    //direction.Normalize();
                    ortho.Normalize();
                    //direction = ortho;
                    direction = ortho.Rotate(GeoAlgorithms2D.DegreeToRadian(curBuildingNode.Angle), new Vector2D(0,0) );
                    // -- test if the new center is located in an existing building or open space
                    double dist = GeoAlgorithms2D.DistancePoints(mid, parCenter);
                    Vector2D newCenter = parCenter + (direction * 2*dist);
                    bool isFree = true;
                    for (int j = 0; j < buildingLayout.Count; j++)
                    {
                        GPolygon testBuilding = buildingLayout[j];
                        int tester = GeoAlgorithms2D.PointInPolygon(newCenter, testBuilding.Poly.PointsFirstLoop.ToList());
                        if (tester >= 0)
                        {
                            isFree = false;
                            break;
                        }
                    }
                    if (!isFree) // -- if the location is not free, finalize this branch and continue with next node.
                    {
                        curBuildingNode.Closed = true;
                        // ** optimization possibility by closing/cutting the branch?
                        continue;
                    }
                    // -- if the location is free, construct a new building --
                    double length = curBuildingNode.Area / curRefLine.Length;
                    Vector2D[] newBuildingPts = new Vector2D[4];
                    newBuildingPts[0] = curRefLine.Start + (direction * curBuildingNode.Distance);
                    newBuildingPts[1] = curRefLine.End + (direction * curBuildingNode.Distance);
                    newBuildingPts[2] = curRefLine.End + (direction * length) + (direction* curBuildingNode.Distance);
                    newBuildingPts[3] = curRefLine.Start + (direction * length) + (direction * curBuildingNode.Distance);
                    GPolygon newBuilding = new GPolygon(newBuildingPts);
                    newBuilding.BorderWidth = 5;
                    // -- define the status of the new building --
                    curBuildingNode.Building = (GPolygon)newBuilding;
                    bulidingTree.RegisterNeighbour(curBuildingNode, parentBuildingNode, direction, length);
                    bool hasToBeFree = bulidingTree.GetRequiredSpaceStatus(curBuildingNode);
                    //short nrFree = bulidingTree.GetNrFreeParentNeigbours(parentBuildingNode);
                    //if (parentBuildingNode.MinFreeNeigbours < nrFree)
                    if (! hasToBeFree)
                    {
                        curBuildingNode.SpaceStatus = 1; // status: 1 = building
                        newBuilding.FillColor = new Vector4(0.04f, 0.04f, 0.04f, 1);
                    }
                    else
                    {
                        curBuildingNode.SpaceStatus = 0; // status: 0 = open space
                        newBuilding.FillColor = new Vector4(1, 1, 1, 1);
                    }
                    curBuildingNode.RemoveParentEdgeIndex(curRefLine);
                    bulidingTree.AdaptGeometry(curBuildingNode);

                    if (curBuildingNode.Building != null) buildingLayout.Add(newBuilding);
                }

            }
            return buildingLayout;
        }


    }
}

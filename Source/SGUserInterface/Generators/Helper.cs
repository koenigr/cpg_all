﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Troschuetz.Random.Generators;
using Troschuetz.Random.Distributions.Continuous;

namespace CPlan.UrbanPattern
{
    public static class Helper
    {
        public static NormalDistribution NormDist = new NormalDistribution();

        public static void NewNormDistGenerator()
        {
            ALFGenerator newGenerator = new ALFGenerator((int)DateTime.Now.Ticks);
            Helper.NormDist = new NormalDistribution(newGenerator);
        }
    }
}

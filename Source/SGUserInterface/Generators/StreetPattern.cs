﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tektosyne;
using Tektosyne.Geometry;
using Tektosyne.Collections;
using CPlan.Optimization;
using CPlan.Geometry;
using CPlan.VisualObjects;
using CPlan.Evaluation;
using QuickGraph;

//using System.Windows.Forms;

namespace CPlan.UrbanPattern
{

    [Serializable]
    public class StreetPattern
    {
        public Subdivision Graph
        {
            get;
            set;
        }

        public UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> QGraph
        {
            get;
            set;
        }

        private int maxEdges = 4;
      //  private int rndAngle = 10;
        private double minDist = 20;
        private double minAngle = 80;
      //  private int maxLength = 100;
      //  private int minLength = 30;
        private bool rotSymmetryL = false;
        private bool rotSymmetryR = false;
        private Rect2D border;
        private Dictionary<PointD, DataNode> RefNodes;

        //============================================================================================================================================================================
        public StreetPattern(int mE, int rA, double mD, double mA, int maxL, int minL, bool rSL, bool rSR, Rect2D bord) 
        {  
            maxEdges = mE;
         //   rndAngle = rA;
            minDist = mD;
            minAngle = mA;
         //   maxLength = maxL;
         //   minLength = minL;
            rotSymmetryL = rSL;
            rotSymmetryR = rSR;
            border = bord;
            //border = new RectD(bord.X-100, bord.Y - 100, bord.Width*2, bord.Height*2); 
            Graph = new Subdivision();
            RefNodes = new Dictionary<PointD, DataNode>();
        }

        //============================================================================================================================================================================
        public StreetPattern(Rect2D bord)
        {
            maxEdges = 4;
            minDist = 10;
            minAngle = 20;
            border = bord; 
            Graph = new Subdivision();
            RefNodes = new Dictionary<PointD, DataNode>();
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Grow a StreetNetwork from the InstructionTree, resp. from the List of InstructionNodes
        /// </summary>
        /// <param name="InstructionNodes">List of InstructionNodes from a InstructionTree. </param>
        /// <returns></returns>
        public void GrowStreetPatternNew(InstructionTreePisa instructTree, UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> iniStreets) //Subdivision GrowStreetPattern()
        {
            // the new graph qGraph...---------------------------------------
            UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> qGraph = new UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>>(false);

            // find all InstructionNodes whose parent indes = 0. This means they are the seed Nodes to create the seed Edges.
            foreach (InstructionNode node in instructTree.Nodes)
            {
                node.Closed = false;
            }

            foreach (UndirectedEdge<Vector2D> qIniEdge in iniStreets.Edges)
            {
                qGraph.AddVerticesAndEdge(qIniEdge);
            }
  
            // -- add all other edges from the instructionTree --
            for (int n = 0; n < instructTree.Nodes.Count; n++)
            {
                bool addEdge = true;
                bool splitEdge = false;
                Line2D toSplitEdge = null;
                InstructionNode curInstructNode = instructTree.Nodes[n];
                InstructionNode parentInstructNode = instructTree.Nodes.Find(InstructionNode => InstructionNode.Index == curInstructNode.ParentIndex); // curInstructNode.Parent;//
                if (curInstructNode.ParentIndex >= 0 && parentInstructNode == null)
                    continue; // if program arrives here, there is a problem with crossover...
                //----------------------------------------------------------------------------------------------------------------------------------
                //IF YOUR Parent WAS CLOSED IN THE INSTRUCTION TREE YOU ARE CLOSDED TOO
                if (curInstructNode.ParentIndex >= 0 && parentInstructNode.Closed == true)// if (curInstructNode.Parent != null && parentInstructNode.Closed == true)
                {
                    curInstructNode.Closed = true;
                    //ADDED DELETING PART OF THE CLOSED NODE
                    // --> possible to cut the remaining branch to make the process faster in the next generations <--

                    //List<InstructionNode> deleteNodes = new List<InstructionNode>();
                    //int rootIdx = instructTree.Nodes.IndexOf(curInstructNode);
                    //deleteNodes = instructTree.FindBranch(deleteNodes, rootIdx);
                    //if (deleteNodes.Count > 30)
                    //{
                    //    deleteNodes.RemoveRange(0, 30);
                    //    foreach (InstructionNode delNode in deleteNodes) instructTree.Nodes.Remove(delNode);
                    //}
                } // --- don't consider the global root node and the seed nodes ---------------

                //TESTING IF WE CAN ADD THE NEW NODE TO THE NETWORK
                else if (curInstructNode.ParentIndex >= 0 && parentInstructNode.Closed == false)
                {

                    PointD parentPt = parentInstructNode.Position; //LOOKING FOR PARRENT
                    // test qGraph---------------------------
                    int qNrNeighb = 0;
                    if (qGraph.ContainsVertex(parentPt.ToVector2D()))
                    {
                        qNrNeighb = qGraph.AdjacentDegree(parentPt.ToVector2D());
                    }
                    // ---------------------------------------

                    if (qNrNeighb >= parentInstructNode.MaxConnectivity) //TEST IF IS POSSIBLE TO ADD NEW ARM TO THE PARRENT
                    {
                        addEdge = false;
                        parentInstructNode.Closed = true;
                        curInstructNode.Closed = true;
                        continue; //LEAVE THIS NODE, CONTINUE ON NEXT INSTRUCTION NODE
                    }

                    // ---------------------------------------------------------------------------------------------------------------------
                    //CALCULATIONS OF NEW ANGLES DEPENDING ON HOW MANY CONNECTION PARRANT NODE HAS
                    double alpha = 0, rotAngle = 0;
                    double dist = 0;
                    List<Double> AngleList = new List<Double>();     
                    Vector2D parentsParentPt = null;
                    if (parentInstructNode.ParentIndex == -1) // only for the first inital nodes
                    {
                        List<Vector2D> parentNeighbours = qGraph.GetNeighbors(parentPt.ToVector2D());
                        parentsParentPt = parentNeighbours[0];
                    }
                    else
                    {
                        InstructionNode parentParentInstructNode = instructTree.Nodes.Find(InstructionNode => InstructionNode.Index == parentInstructNode.ParentIndex);
                        parentsParentPt = parentParentInstructNode.Position.ToVector2D();
                    }

                    List<Vector2D> parentNeighList = qGraph.GetNeighbors(parentPt.ToVector2D());
                    int nrParentNeighb = parentNeighList.Count;
                    if (nrParentNeighb == 1)
                    {
                        rotAngle = 180;
                    }
                    else
                    {
                        for (int i = 1; i < nrParentNeighb; i++)
                        {
                            Vector2D curNeighb = parentNeighList[i];
                            double temAngle = GeoAlgorithms2D.AngleBetween(parentsParentPt, curNeighb, parentPt.ToVector2D()); //selNode.Point.AngleBetween(refPt, curNeighb);
                            temAngle = Angle.RadiansToDegrees * temAngle;
                            if (temAngle < 0) 
                                temAngle = 360 + temAngle;
                            AngleList.Add(temAngle);
                            if (temAngle > alpha)
                            {
                                alpha = temAngle;
                            }
                        }
                        AngleList.Add(0);
                        AngleList.Add(360);
                        AngleList.Sort((y, x) => y.CompareTo(x));

                        double maxAngle = AngleList[1];
                        int inSection = 0;
                        for (int i = 2; i < AngleList.Count; i++)
                        {
                            double delta = AngleList[i] - AngleList[i - 1];
                            if (maxAngle < delta)
                            {
                                maxAngle = delta;
                                inSection = i - 1;
                            }
                        }
                        if (maxAngle < minAngle) addEdge = false; // only add a edge, if the angle to devide is large enough!
                        if (maxAngle >= 260) // allow also 90° steps for angles larger than 260°
                        {
                            int divNr = Convert.ToInt16(Math.Ceiling(maxAngle / 90) - 0);
                            if (curInstructNode.Divider == -1) curInstructNode.Divider = RndGlobal.Rnd.Next(divNr);
                            maxAngle = 90 + curInstructNode.Divider * 90; // 180;//
                        }
                        rotAngle = maxAngle / 2 + AngleList[inSection];

                    }

                    if (addEdge) // --- check if the requirements to create a new segment are fulfilled ----------------------
                    {
                        PointD newPt = GeoAlgorithms2D.RotatePoint(parentPt.ToVector2D(), parentsParentPt, GeoAlgorithms2D.DegreeToRadian(rotAngle + curInstructNode.Angle)).ToPointD();
                        newPt = parentPt.Move(newPt, curInstructNode.Length);
                        //
                        // ============================ candidata position defined - now test interaction with existing graph ======================================================
                        // ---------------------------------------------------------------------------------------------------------
                        // --- check if there is an intersection -------------------------------------------------------------------
                        // ---------------------------------------------------------------------------------------------------------
                        Line2D qNewLine = new Line2D(parentPt.ToVector2D(), newPt.ToVector2D());
                        Vector2D newPtff = IntersectionTest(qGraph, qNewLine, ref toSplitEdge, ref splitEdge);
                        if (newPtff != null)
                            newPt = newPtff.ToPointD();

                        // ---------------------------------------------------------------------------------------------------------
                        // --- check the distance of the newPt to an existing edge -------------------------------------------------
                        // ---------------------------------------------------------------------------------------------------------
                        UndirectedEdge<Vector2D>[] qEdges = qGraph.Edges.ToArray();
                        List<Line2D> qLines = new List<Line2D>();
                        foreach (UndirectedEdge<Vector2D> edge in qEdges) qLines.Add(new Line2D(edge.Source, edge.Target));
                        Line2D qNearEdge = GeoAlgorithms2D.NearestLineToPoint(qLines.ToArray(), newPt.ToVector2D(), out dist);
                        bool ptNew = true;
                        if (dist < minDist) // -- if the distance is small enough, connect the point to the existing edge
                        {
                            if (qNearEdge != null)
                            {
                                Vector2D qNewPt = qNearEdge.Intersect(newPt.ToVector2D());
                                newPt = qNewPt.ToPointD();
                                splitEdge = true; // the edge with the new point on it needs to be splitted
                                toSplitEdge = qNearEdge;
                                // --- test if the movement of the new point cause an intersection ---
                                qNewLine = new Line2D(parentPt.ToVector2D(), newPt.ToVector2D());
                                newPtff = IntersectionTest(qGraph, qNewLine, ref toSplitEdge, ref splitEdge);
                                if (newPtff != null)
                                    newPt = newPtff.ToPointD();
                            }
                        }
                        else // --- check the distande of the new line to existing points ------------------------------------------
                        {
                            Vector2D qNearPoint = GeoAlgorithms2D.NearestPointToLine(qGraph.Vertices.ToArray(), qNewLine, parentPt.ToVector2D(), out dist);
                            if (dist < minDist) // -- if the distance is small enough, end the line at the near point
                            {
                                if (qNearPoint != null)
                                {
                                    newPt = qNearPoint.ToPointD();
                                    splitEdge = false; // no splitting is nessesary anymore
                                    ptNew = false;
                                    // --- test if the movement of the new point cause an intersection ---
                                    qNewLine = new Line2D(parentPt.ToVector2D(), newPt.ToVector2D());
                                    newPtff = IntersectionTest(qGraph, qNewLine, ref toSplitEdge, ref splitEdge);
                                    if (newPtff != null)
                                        newPt = newPtff.ToPointD();
                                }
                            }
                        }

                        // ---------------------------------------------------------------------------------------------------------
                        // --- find the nearest vertex of the graph ----------------------------------------------------------------
                        // ---------------------------------------------------------------------------------------------------------
                        //if (ptNew)
                        {
                            Vector2D nearestNeighbour = GeoAlgorithms2D.NearestNeighbour(qGraph.Vertices.ToArray(), newPt.ToVector2D());
                            dist = GeoAlgorithms2D.DistancePoints(nearestNeighbour, newPt.ToVector2D());
                            if (dist < minDist)// if the distance is small enough, connect both points
                            {
                                //--- check connectivity of the considered node first
                                //int tester = qGraph.AdjacentDegree(nearestNeighbour);
                                if (nearestNeighbour != null)
                                if (qGraph.AdjacentDegree(nearestNeighbour) >= maxEdges || nearestNeighbour.ToPointD() == parentPt)
                                {
                                    addEdge = false;
                                }
                                else
                                {
                                    newPt = nearestNeighbour.ToPointD();
                                    splitEdge = false; // no splitting is nessesary anymore
                                    // --- test if the movement of the new point cause an intersection ---
                                    qNewLine = new Line2D(parentPt.ToVector2D(), newPt.ToVector2D());
                                    newPtff = IntersectionTest(qGraph, qNewLine, ref toSplitEdge, ref splitEdge);
                                    if (newPtff != null)
                                        newPt = newPtff.ToPointD();
                                }
                            }
                        }

                        // ============================ all tests are done - now add a new edge and/or split an existing edge ======================================================
                        // --- so far, the candidate point (newPt) is found ----------------------------------------------------
                        // --- add the new street segment to the graph and test, if the new point is added to the graph
                        if (addEdge) // --- check if the requirements to create a new segment are fulfilled
                        {
                            bool outOfBorder = false;
                            if      (newPt.X < border.Left)   outOfBorder = true;
                            else if (newPt.Y < (border.Bottom)) outOfBorder = true;
                            else if (newPt.X > border.Right ) outOfBorder = true;
                            else if (newPt.Y > border.Top)    outOfBorder = true;

                            if (!outOfBorder)
                            {
                                UndirectedEdge<Vector2D> newEdge = new UndirectedEdge<Vector2D>(parentPt.ToVector2D(), newPt.ToVector2D());
                                UndirectedEdge<Vector2D> reverseNewEdge = qGraph.Edges.ToList().Find(x => x.Source == newPt.ToVector2D() && x.Target == parentPt.ToVector2D());
                                // --- a new edge (& vertex) is added ---                       
                                if (qGraph.ContainsVertex(newPt.ToVector2D()) && qGraph.ContainsVertex(parentPt.ToVector2D()))
                                {                                                              
                                    if (reverseNewEdge == null)
                                        qGraph.AddEdge(newEdge);
                                }
                                else
                                {
                                    if (reverseNewEdge == null)
                                        qGraph.AddVerticesAndEdge(newEdge);
                                
                                    // --- test, if we need to split an existing edge ---
                                    if (splitEdge)
                                    {
                                        Vector2D existVertiA = toSplitEdge.Start;
                                        Vector2D existVertiB = toSplitEdge.End;
                                        UndirectedEdge<Vector2D> newEdgeA = new UndirectedEdge<Vector2D>(existVertiA, newPt.ToVector2D());
                                        UndirectedEdge<Vector2D> revNewEdgeA = qGraph.Edges.ToList().Find(x => x.Source == newPt.ToVector2D() && x.Target == existVertiA);
                                        UndirectedEdge<Vector2D> newEdgeB = new UndirectedEdge<Vector2D>(existVertiB, newPt.ToVector2D());
                                        UndirectedEdge<Vector2D> revNewEdgeB = qGraph.Edges.ToList().Find(x => x.Source == newPt.ToVector2D() && x.Target == existVertiB);
                                        if (revNewEdgeA == null)
                                            qGraph.AddEdge(newEdgeA);
                                        if (revNewEdgeB == null)
                                            qGraph.AddEdge(newEdgeB);

                                        if (qGraph.ContainsVertex(toSplitEdge.Start) && qGraph.ContainsVertex(toSplitEdge.End))
                                        {
                                            UndirectedEdge<Vector2D> removeEdge = qGraph.Edges.ToList().Find(x => x.Source == toSplitEdge.Start && x.Target == toSplitEdge.End);
                                            if (qGraph.ContainsEdge(removeEdge))
                                                qGraph.RemoveEdge(removeEdge);
                                            removeEdge = qGraph.Edges.ToList().Find(x => x.Source == toSplitEdge.End && x.Target == toSplitEdge.Start);
                                            if (removeEdge != null)
                                                if (qGraph.ContainsEdge(removeEdge))
                                                    qGraph.RemoveEdge(removeEdge);
                                        }
                                    }                                
                                }
                                curInstructNode.Position = newPt; // add the new vertex-position to the corresponding instruction node
                            }
                            else { curInstructNode.Closed = true; }
                        }                      
                        else { curInstructNode.Closed = true; } // no further growth at this node
                    }
                    else { curInstructNode.Closed = true; } // no further growth at this node
                }
            }
            // --- construct subdivision graph ---
            QGraph = qGraph;

            //List<UndirectedEdge<Vector2D>> edges = qGraph.Edges.ToList();
            //List<LineD> linesList = new List<LineD>();
            //List<Line2D> testList = new List<Line2D>();
            //int s = 0;
            //for (int i = 0; i < edges.Count; i++) //edges.Count
            //{
            //    UndirectedEdge<Vector2D> qEdge = edges[i];              
            //    // -- optional test -- needs to be changed above...
            //    Line2D testEdge = new Line2D(qEdge.Source, qEdge.Target);
            //    List<Vector2D> intersectPts = GeoAlgorithms2D.IntersectLines(testEdge, testList.ToArray());
            //    if (intersectPts.Count == 0)
            //    {
            //        if (qEdge.Source == qEdge.Target)
            //          s = 1;
            //        LineD testLD = new LineD(qEdge.Source.ToPointD(), qEdge.Target.ToPointD());

            //        UndirectedEdge<Vector2D> testIsEdge = qGraph.Edges.ToList().Find(x => x.Source == qEdge.Source && x.Target == qEdge.Target);
            //        //if (qGraph.ContainsEdge(removeEdge))
            //        if (testIsEdge != null)
            //        {
            //            linesList.Add(testLD);
            //            //testList.Add(testEdge);
            //        }
            //        else
            //          s = 1;
            //    }
            //    else
            //      s = 1;
            //}
            //LineD[] arrLines = linesList.ToArray();
            //Graph = new Subdivision();
            //Graph = Subdivision.FromLines(arrLines);

            //// *** TEST ***
            //int test;
            //test = Graph.Edges.Count / 2;
            //if (test != QGraph.EdgeCount)
            //    test = 0;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Test if there is an intersection of a new line with an existing graph.
        /// </summary>
        /// <param name="qGraph">Existing graph.</param>
        /// <param name="testLine">New line for testing.</param>
        /// <param name="toSplitEdge">Intersection edge of the graph if an intersection is found.</param>
        /// <param name="splitEdge">Indicates if an edge needs to be splitted.</param>
        /// <returns>The intersecting point of the new line with the graph.
        /// The function returns the nearest intersection point to the line origin it there are more intersection points.</returns>
        public Vector2D IntersectionTest(UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> qGraph, Line2D testLine, ref Line2D toSplitEdge, ref bool splitEdge)
        {
            // --- optimize the intersection test by using a grit to which the lines are registered...
            toSplitEdge = null;
            splitEdge = false;
            Vector2D intersectionPt = null;    
            UndirectedEdge<Vector2D>[] qEdges = qGraph.Edges.ToArray();
            List<Line2D> qLines = new List<Line2D>();
            foreach (UndirectedEdge<Vector2D> edge in qEdges) qLines.Add(new Line2D(edge.Source, edge.Target));
            //Line2D qNewLine = new Line2D(parentPt.ToVector2D(), newPt.ToVector2D());
            List<Line2D> intersectLines = new List<Line2D>();
            List<Vector2D> intersectPts = GeoAlgorithms2D.IntersectLines(testLine, qLines.ToArray(), intersectLines);
            // --- find the intersection point closes to the parent point ---
            double minDistc = double.MaxValue;
            Vector2D nearestInterPt = null;
            if (intersectPts.Count > 1)
            {
                //foreach (Vector2D nPt in intersectPts)
                for (int i = 0; i < intersectPts.Count; i++)
                {
                    Vector2D nPt = intersectPts[i];
                    double nDist = GeoAlgorithms2D.DistancePoints(testLine.Start, nPt);
                    if (minDistc > nDist)
                    {
                        minDistc = nDist;
                        nearestInterPt = nPt;
                        toSplitEdge = intersectLines[i];
                    }
                }
                splitEdge = true; // the intersecting edge needs to be splitted
                intersectionPt = nearestInterPt; // use the intersection point as new point 
            }
            else if (intersectPts.Count == 1)
            {
                nearestInterPt = intersectPts[0];
                toSplitEdge = intersectLines[0];
                splitEdge = true; // the intersecting edge needs to be splitted
                intersectionPt = nearestInterPt; // use the intersection point as new point 
            }

            return intersectionPt;
        }

        //============================================================================================================================================================================
        //======================================= old part with Tektosyne Subdivision class ==========================================================================================
        //============================================================================================================================================================================
        /// <summary>
        /// Grow a StreetNetwork from the InstructionTree, resp. from the List of InstructionNodes
        /// </summary>
        /// <param name="instructTree">The InstructionTree. </param>
        /// <param name="iniStreets">The initial setreet segments. </param>
        public void GrowStreetPatternSubdiv(InstructionTree instructTree, Subdivision iniStreets) //Subdivision GrowStreetPattern()
        {
           
            // find all InstructionNodes whose parent indes = 0. This means they are the seed Nodes to create the seed Edges.
            //List<InstructionNode> initialNodes = instructTree.Nodes.FindAll(InstructionNode => InstructionNode.SeedNode == true);
            foreach (InstructionNode node in instructTree.Nodes)
            {
                node.Closed = false;
            }
            
            List<LineD> iniEdges = iniStreets.ToLines().ToList();
            //Graph = (Subdivision)iniStreets.Clone();
            foreach (LineD edge in iniEdges)
            {
                AddEdge(edge.Start, edge.End);
            }

            // create the first seed edge(s)
            //for (int i = 0; i < initialNodes.Count; i += 2)
            //{
            //    PointD iniPt1 = initialNodes[i].Position; //GeoAlgorithms2D.PolarToCartesian(initialNodes[i].Angle, initialNodes[i].Length);
            //    PointD iniPt2 = initialNodes[i + 1].Position; //GeoAlgorithms2D.PolarToCartesian(initialNodes[i + 1].Angle, initialNodes[i + 1].Length);
            //    AddEdge(iniPt1, iniPt2);
            //}

            // add all other edges from the instructionTree
            for (int n = 0; n < instructTree.Nodes.Count; n++)
            {
                bool isAdded = false;
                bool addEdge = true;
                InstructionNode curInstructNode = instructTree.Nodes[n];
                //InstructionNode parentInstructNode = instructTree.Nodes.Find(InstructionNode => InstructionNode.Index == curInstructNode.ParentIndex);
                InstructionNode parentInstructNode = instructTree.Nodes.Find(InstructionNode => InstructionNode.Index == curInstructNode.ParentIndex); // curInstructNode.Parent;//
                if (curInstructNode.ParentIndex >= 0 && parentInstructNode == null)
                    continue; // if program arrives here, there is a problem with crossover...
                //----------------------------------------------------------------------------------------------------------------------------------
                //IF YOUR Parent WAS CLOSED IN THE INSTRUCTION TREE YOU ARE CLOSDED TOO
                if (curInstructNode.ParentIndex >= 0 && parentInstructNode.Closed == true)// if (curInstructNode.Parent != null && parentInstructNode.Closed == true)
                {
                    curInstructNode.Closed = true;
                    //ADDED DELETING PART OF THE CLOSED NODE
                    // --> possible to cut the remaining branch to make the process faster in the next generations <--
                    List<InstructionNode> deleteNodes = new List<InstructionNode>();
                    int rootIdx = instructTree.Nodes.IndexOf(curInstructNode);
                    deleteNodes = instructTree.FindBranch(deleteNodes, rootIdx);
                    if (deleteNodes.Count > 30)
                    {
                        deleteNodes.RemoveRange(0, 30);
                        foreach (InstructionNode delNode in deleteNodes) instructTree.Nodes.Remove(delNode);
                    }
                } // --- don't consider the global root node and the seed nodes ---------------

              //TESTING IF WE CAN ADD THE NEW NODE TO THE NETWORK
                //else if (curInstructNode.Parent != null && !curInstructNode.SeedNode && parentInstructNode.Closed == false) 
                else if (curInstructNode.ParentIndex >= 0 && parentInstructNode.Closed == false)
                {
                    
                    PointD parentPt = parentInstructNode.Position; //LOOKING FOR PARRENT
                    int nrNeighb = Graph.GetNeighbors(parentPt).Count; //HOW MANY ARMS PARRENT NODE HAVE
                    
                    if (nrNeighb >= parentInstructNode.MaxConnectivity) //TEST IF IS POSSIBLE TO ADD NEW ARM TO THE PARRENT
                    //if (nrNeighb >= maxEdges)
                    {
                        addEdge = false;
                        parentInstructNode.Closed = true;
                        curInstructNode.Closed = true;
                        continue; //LEAVE THIS NODE, CONTINUE ON NEXT INSTRUCTION NODE
                    }


                    // --- create a more comfortable temporary node list from the current network graph --------------------------------------TEMPORARY NODE LIST
                    IList<PointD> neighb;
                    List<GNode> Nodes = new List<GNode>();
                    List<PointD> vertices = Graph.Nodes.ToList();
                    for (int i = 0; i < vertices.Count; i++)
                    {
                        Nodes.Add(new GNode(vertices[i], true));
                        neighb = Graph.GetNeighbors(Nodes[i].Point);
                        Nodes[i].Connectivity = neighb.Count;
                        Nodes[i].Neighbours = neighb;
                    }

                    // --- X ---------------------------------------------------------------------------------------------------------------
                    // ---------------------------------------------------------------------------------------------------------------------
                    //CALCULATIONS OF NEW ANGLES DEPENDING ON HOW MANY CONNECTION PARRANT NODE HAS
                    double alpha = 0, rotAngle = 0;
                    double dist;
                    List<Double> AngleList = new List<Double>();
                    GNode selNode = Nodes.Find(Node => Node.Point == parentPt); // find the node of the network graph in the temp node list
                    if (selNode == null)
                        continue; // if the program arrive here, something is wrong...?
                    PointD refPt = selNode.Neighbours[0];

                    if (selNode.Connectivity == 1)
                    {
                        rotAngle = 180;
                    }
                    else
                    {
                        for (int i = 1; i < selNode.Neighbours.Count; i++)
                        {
                            PointD curNeighb = selNode.Neighbours[i];
                            double temAngle = selNode.Point.AngleBetween(refPt, curNeighb);
                            temAngle = Angle.RadiansToDegrees * temAngle;
                            if (temAngle < 0) temAngle = 360 + temAngle;
                            AngleList.Add(temAngle);
                            if (temAngle > alpha)
                            {
                                alpha = temAngle;
                            }
                        }
                        AngleList.Add(0);
                        AngleList.Add(360);
                        AngleList.Sort((y, x) => y.CompareTo(x));

                        double maxAngle = AngleList[1];
                        int inSection = 0;
                        for (int i = 2; i < AngleList.Count; i++)
                        {
                            double delta = AngleList[i] - AngleList[i - 1];
                            if (maxAngle < delta)
                            {
                                maxAngle = delta;
                                inSection = i - 1;
                            }
                        }
                        if (maxAngle < minAngle) addEdge = false; // only add a edge, if the angle to devide is large enough!
                        if (maxAngle >= 260) // allow also 90° steps for angles larger than 260°
                        {
                            int divNr = Convert.ToInt16(Math.Ceiling(maxAngle / 90) - 0);
                            if (curInstructNode.Divider == -1) curInstructNode.Divider = RndGlobal.Rnd.Next(divNr);
                            maxAngle = 90 + curInstructNode.Divider * 90; // 180;//
                        }
                        rotAngle = maxAngle / 2 + AngleList[inSection];

                    }

                    if (addEdge) // --- check if the requirements to create a new segment are fulfilled ----------------------
                    {
                        //CREATE MOVING VECTOR AND MOVE PARENT POINT TO CREATE NEW POINT
                        // --- second, rotate the reference point by half of the largest angle and create a new point
                        if (!RefNodes.ContainsKey(parentPt))
                            return;//  Graph; // --- Fehler!!! -> Abbruch... if the program arrive here, something is wrong.
                        // replaced by a fixed angle
                        //int rndAngle = RefNodes[parentPt].AngleToGrow;
                        //if (rotSymmetryR) alpha = Angle.DegreesToRadians * (rotAngle + RndGlobal.Rnd.Next(rndAngle));
                        //else if (rotSymmetryL) alpha = Angle.DegreesToRadians * (rndAngle - RndGlobal.Rnd.Next(rndAngle));
                        //else alpha = Angle.DegreesToRadians * (rotAngle + rndAngle);//rotAngle + RndGlobal.Rnd.NextDouble() * rndAngle); //);//
                        ////else alpha = Angle.DegreesToRadians * (rotAngle + RndGlobal.Rnd.Next(rndAngle) - rndAngle / 2); //);//
                        //PointD newPt = RndGlobal.RotatePoint(parentPt, refPt, alpha);

                        //PointD newPt = Tools.RotatePoint(parentPt, refPt, GeoAlgorithms2D.DegreeToRadian(rotAngle + curInstructNode.Angle));
                        PointD newPt = GeoAlgorithms2D.RotatePoint(parentPt.ToVector2D(), refPt.ToVector2D(), GeoAlgorithms2D.DegreeToRadian(rotAngle + curInstructNode.Angle)).ToPointD();
                        // --- move the new Point on the axis origPt - newPt
                        //DataNode refNode = RefNodes[parentPt];
                        //dist = RndGlobal.Rnd.Next(minLength, maxLength);
                        //dist = refNode.LengthToGrow;
                        //newPt = parentPt.Move(newPt, dist);
                        newPt = parentPt.Move(newPt, curInstructNode.Length);


                        //TEST POSITION OF NEW VECTOR - IF IT IS POSSIBLE TO ADD IT
                        // --- find the nearest vertex of the graph ----------------------------------------------------------------
                        PointD nearN = Graph.GetNearestNode(newPt);                       
                        List<PointD> interPts = new List<PointD>();
                        dist = Graph.GetDistance(newPt, nearN);

                        // First, check if there is an intersection!

                        // --- THE planar graph/street network TESTS ----------------------------------------------------------------------------------------------
                        if (dist < minDist)// if the distance is small enough, connect both points
                        {
                            //--- check connectivity of the considered node first
                            if (Graph.GetNeighbors(nearN).Count >= maxEdges) addEdge = false;
                            else newPt = nearN;                            
                        }
                        else // --- check the distance of the newPt to an existing edge
                        {                              
                            SubdivisionEdge curEdge = Graph.FindNearestEdge(newPt, out dist);
                            if (dist < minDist) // if the distance is small enough, connect the point to the existing edge
                            {
                                LineD curLine = new LineD(curEdge.Origin, curEdge.Destination);
                                newPt = curLine.Intersect(newPt);
                                interPts.Add(newPt);
                            }
                        }
                        // --- so far, the candidate point (newPt) is found ----------------------------------------------------
                        // --- add the new street segment to the graph and test, if the new point is added to the graph
                        if (addEdge) // --- check if the requirements to create a new segment are fulfilled
                        {                           
                            isAdded = AddGraphElements(parentPt, newPt);
                            if (isAdded) { curInstructNode.Position = (PointD)newPt; }
                            else //if (!isAdded) // if the new street segment is not added, test, if there is an intersection
                            {
                                LineD newLine = new LineD(parentPt, newPt);
                                LineD[] graphEdges = Graph.ToLines();
                                foreach (LineD line in graphEdges)
                                {
                                    LineIntersection inter = newLine.Intersect(line);
                                    if (inter.Exists)
                                    {
                                        LineLocation locTest = inter.First;
                                        if (locTest == LineLocation.Between) // only it the intersection is inside the line - also excludes start and end points
                                        {
                                            interPts.Add((PointD)inter.Shared);
                                        }
                                    }
                                }

                                PointD curPt = new PointD();
                                double curMinDist = double.MaxValue;
                                if (interPts.Count == 1)
                                {
                                    curPt = interPts[0];
                                }
                                else if (interPts.Count > 1) // find the closest intersection point...
                                {
                                    foreach (PointD point in interPts)
                                    {
                                        dist = Graph.GetDistance(parentPt, point);
                                        if (dist < curMinDist)
                                        {
                                            curMinDist = dist;
                                            curPt = point;
                                        }
                                    }
                                }

                                if (!(curPt.X == 0 & curPt.Y == 0))
                                {
                                    isAdded = ConnectNew(parentPt, ref curPt);
                                    if (isAdded) { curInstructNode.Position = curPt; }
                                    else { curInstructNode.Closed = true; } // no further growth at this node
                                }
                                else { curInstructNode.Closed = true; } // no further growth at this node
                            }
                        }
                        else { curInstructNode.Closed = true; } // no further growth at this node
                    }
                    else { curInstructNode.Closed = true; } // no further growth at this node
                }
            }
        }


        //============================================================================================================================================================================
        public bool GrowStreetPatternBasic() //Subdivision GrowStreetPattern()
        {
            bool isAdded = false;

            bool addEdge = true;
            // --- select a node to expand --------------------------------------------------------------
            IList<PointD> neighb;
            List<GNode> Nodes = new List<GNode>();
            List<PointD> vertices = Graph.Nodes.ToList();
            for (int i = 0; i < vertices.Count; i++)
            {
                Nodes.Add(new GNode(vertices[i], true));
                neighb = Graph.GetNeighbors(Nodes[i].Point);
                Nodes[i].Connectivity = neighb.Count;
                Nodes[i].Neighbours = neighb;
            }

            List<GNode> activeNodes = new List<GNode>();
            List<double> Connectivity = new List<double>();
            foreach (GNode node in Nodes)
            {
                if (node.Active)
                {
                    activeNodes.Add(node);
                    if (node.Connectivity == 0 | node.Connectivity >= maxEdges) 
                        Connectivity.Add(0);
                    //else Connectivity.Add( Math.Pow(node.Connectivity, 5));// * node.Connectivity));//(1.0 /
                    else Connectivity.Add(1.0 /Math.Pow(node.Connectivity, 5));// * node.Connectivity));//(
                }
            }

            double[] normValues = Connectivity.ToArray();
            Tektosyne.MathUtility.Normalize(normValues);

            // --- roulette wheel selection 
            double rndVal = RndGlobal.Rnd.NextDouble();
            GNode selNode = null;
            double tempSum = 0;
            for (int i = 0; i < activeNodes.Count; i++)
            {
                tempSum += normValues[i];
                if (tempSum >= rndVal)
                {
                    selNode = activeNodes[i];
                    break;
                }
            }

            // --- calculate the angle and length of of the new street segment --------------------------
            // --- first, find the largest free angle
            double alpha = 0, rotAngle = 0;
            double dist;
            PointD origPt = selNode.Point;
            if (Graph.GetNeighbors(origPt).Count >= maxEdges) addEdge = false;
            //if (selNode.Neighbours.Count >= maxEdges) addEdge = false;
            List<Double> AngleList = new List<Double>();
            PointD refPt = selNode.Neighbours[0];

            if (selNode.Connectivity == 1)
            {
                rotAngle = 180;
            }
            else
            {
                for (int i = 1; i < selNode.Neighbours.Count; i++)
                {
                    PointD curNeighb = selNode.Neighbours[i];
                    double temAngle = selNode.Point.AngleBetween(refPt, curNeighb);
                    temAngle = Angle.RadiansToDegrees * temAngle;
                    if (temAngle < 0) temAngle = 360 + temAngle;
                    AngleList.Add(temAngle);
                    if (temAngle > alpha)
                    {
                        alpha = temAngle;
                    }
                }
                AngleList.Add(0);
                AngleList.Add(360);
                AngleList.Sort((y, x) => y.CompareTo(x));

                double maxAngle = AngleList[1];
                int inSection = 0;
                for (int i = 2; i < AngleList.Count; i++)
                {
                    double delta = AngleList[i] - AngleList[i - 1];
                    if (maxAngle < delta)
                    {
                        maxAngle = delta;
                        inSection = i-1;
                    }
                }
                if (maxAngle < minAngle) addEdge = false; // only add a edge, if the angle to devide is large enough!
                if (maxAngle >= 260) // allow also 90° steps for angles larger than 260°
                {
                    int divNr = Convert.ToInt16(Math.Ceiling(maxAngle / 90));
                    maxAngle = 90 + RndGlobal.Rnd.Next(divNr)*90;
                }
                rotAngle = maxAngle / 2 + AngleList[inSection];
            }

            if (addEdge) // --- check if the requirements to create a new segment are fulfilled ----------------------
            {
                // --- second, rotate the reference point by half of the largest angle and create a new point
                if (!RefNodes.ContainsKey(origPt))
                    return false;//  Graph; // --- Fehler!!! -> Abbruch...
                int rndAngle = RefNodes[origPt].AngleToGrow;
                if (rotSymmetryR) alpha = Angle.DegreesToRadians * (rotAngle + RndGlobal.Rnd.Next(rndAngle));
                else if (rotSymmetryL) alpha = Angle.DegreesToRadians * (rndAngle - RndGlobal.Rnd.Next(rndAngle));
                else alpha = Angle.DegreesToRadians * (rotAngle + rndAngle);//rotAngle + RndGlobal.Rnd.NextDouble() * rndAngle); //);//
                //else alpha = Angle.DegreesToRadians * (rotAngle + RndGlobal.Rnd.Next(rndAngle) - rndAngle / 2); //);//
                PointD newPt = GeoAlgorithms2D.RotatePoint(origPt.ToVector2D(), refPt.ToVector2D(), alpha).ToPointD();
                // --- move the new Point on the axis origPt - newPt
                DataNode refNode = RefNodes[origPt];
                //dist = RndGlobal.Rnd.Next(minLength, maxLength);
                dist = refNode.LengthToGrow;
                newPt = origPt.Move(newPt, dist);

                // --- find the nearest vertex of the graph
                PointD nearN = Graph.GetNearestNode(newPt);
                List<PointD> interPts = new List<PointD>();
                dist = Graph.GetDistance(newPt, nearN);
                if (dist < minDist)// if the distance is small enough, connect both points
                { 
                    //--- check connectivity of the considered node first
                    if (Graph.GetNeighbors(nearN).Count >= maxEdges) addEdge = false;
                    else newPt = nearN;
                }
                else // --- check the distance of the newPt to an existing edge
                { 
                    SubdivisionEdge curEdge = Graph.FindNearestEdge(newPt, out dist);
                    if (dist < minDist) // if the distance is small enough, connect the point to the existing edge
                    {
                        LineD curLine = new LineD(curEdge.Origin, curEdge.Destination);
                        newPt = curLine.Intersect(newPt);
                        interPts.Add(newPt);
                    }
                } 
                // --- so far, the candidate point (newPt) is found ----------------------------------------------------
                
                // --- add the new street segment to the graph and test, if the new point is added to the graph
                if (addEdge) // --- check if the requirements to create a new segment are fulfilled
                {
                    isAdded = AddGraphElements(origPt, newPt);
                    if (!isAdded) // if the new street segment is not added, test, if there is an intersection
                    {
                        LineD newLine = new LineD(origPt, newPt);
                        LineD[] graphEdges = Graph.ToLines();
                        foreach (LineD line in graphEdges)
                        {
                            LineIntersection inter = newLine.Intersect(line);
                            if (inter.Exists)
                            {
                                LineLocation locTest = inter.First;
                                if (locTest == LineLocation.Between) // only it the intersection is inside the line - also excludes start and end points
                                {
                                    interPts.Add((PointD)inter.Shared);
                                }
                            }
                        }

                        PointD curPt = new PointD();
                        double curMinDist = double.MaxValue;
                        if (interPts.Count == 1)
                        {
                            curPt = interPts[0];
                        }
                        else if (interPts.Count > 1) // find the closest intersection point...
                        {
                            foreach (PointD point in interPts)
                            {
                                dist = Graph.GetDistance(origPt, point);
                                if (dist < curMinDist)
                                {
                                    curMinDist = dist;
                                    curPt = point;
                                }
                            }
                        }

                        if (!(curPt.X == 0 & curPt.Y == 0))
                            isAdded = ConnectNew(origPt, ref curPt);

                    }
                }
                //glControl1.Invalidate();
            }
            return isAdded;//Graph;
        }

        //============================================================================================================================================================================
        private bool ConnectNew(PointD origPt, ref PointD curPt)
        {
            bool isAdded = false;
            bool addEdge = true;
            double dist;
            SubdivisionEdge curEdge = Graph.FindNearestEdge(curPt, out dist);
            if (curEdge == null) return false;
            PointD origin = curEdge.Origin;
            PointD destination = curEdge.Destination;

            double distO = Graph.GetDistance(curPt, origin);
            double distD = Graph.GetDistance(curPt, destination);

            // --- check distance of the intersection point to existing points
            if (distO < minDist | distD < minDist) // if the distance is small enough, connect use existing points instead of the new intersection point
            {
                if (distO < distD) curPt = origin;
                else curPt = destination;
                if (Graph.GetNeighbors(curPt).Count >= maxEdges) addEdge = false;
                if (addEdge) // --- check if the requirements to create a new segment are fulfilled
                {
                    dist = Graph.GetDistance(curPt, origPt);
                    if (dist < minDist) // if the new point would be to close to the old one, move the old one to the position of the new one, instad of creating a new point
                    {
                        int vertIdx = Graph.FindNearestVertex(origPt);
                        if (Graph.MoveVertex(vertIdx, curPt)) // ? is connected to graph????????????????????
                        {
                            DataNode curNode = RefNodes[origPt];
                            RefNodes.Remove(origPt);
                            if (RefNodes.ContainsKey(curPt) == false) RefNodes.Add(curPt, curNode);
                        }
                    }
                    else isAdded = AddGraphElements(origPt, curPt);
                }
            }
            else // --- create a new intersection point
            {
                int key = curEdge.Key;
                //if (key < Graph.Edges.Count)
                //{
                    if (Graph.RemoveEdge(key))
                    {
                        bool newLineOC = true;
                        dist = Graph.GetDistance(curPt, origPt);
                        if (dist < minDist) // if the new point would be to close to the old one, move the old one to the position of the new one, instad of creating a new point
                        {
                            int vertIdx = Graph.FindNearestVertex(origPt);
                            if (Graph.MoveVertex(vertIdx, curPt)) // ? is connected to graph????????????????????
                            {
                                DataNode curNode = RefNodes[origPt];
                                RefNodes.Remove(origPt);
                                if (RefNodes.ContainsKey(curPt) == false) RefNodes.Add(curPt, curNode);
                            }
                            newLineOC = false;
                        }
                        LineD[] curLinesArr = Graph.ToLines();
                        List<LineD> curLinesList = curLinesArr.ToList();
                        if (newLineOC) curLinesList.Add(new LineD(origPt, curPt)); // create this line only, if the orig point is not to close
                        curLinesList.Add(new LineD(origin, curPt));
                        curLinesList.Add(new LineD(destination, curPt));
                        curLinesArr = curLinesList.ToArray();
                        Graph = new Subdivision();
                        if (RefNodes.ContainsKey(curPt) == false) RefNodes.Add(curPt, new DataNode());
                        //Graph.Epsilon = minDist - 1;
                        Graph = Subdivision.FromLines(curLinesArr);
                        isAdded = Graph.Contains(curPt);// true;
                    }
                //}
                    
            }
            return isAdded;
        }

        //============================================================================================================================================================================
        private void SetNextDataNode(PointD oldPtKey, PointD newPtKey)
        {
            DataNode oldNode = RefNodes[oldPtKey];
            DataNode newNode = new DataNode(oldNode);
            newNode.LengthToGrow += 10;
            if (RndGlobal.Rnd.NextDouble() < 0.1) newNode.AngleToGrow += RndGlobal.Rnd.Next(10)-5;
            //newNode.AngleToGrow += 1;
            if (RefNodes.ContainsKey(newPtKey) == false) RefNodes.Add(newPtKey, newNode);
        }

        //============================================================================================================================================================================
        private bool AddGraphElements(PointD oldPt, PointD newPt)
        {            
            bool isAdded = false;
            int testA, testB;
            bool outOfBorder = false;
            if      (newPt.X < border.Left)   outOfBorder = true;
            else if (newPt.Y < (border.Bottom)) outOfBorder = true;
            else if (newPt.X > border.Right ) outOfBorder = true;
            else if (newPt.Y > border.Top)    outOfBorder = true;

            if (!outOfBorder)
            {
                //MessageBox.Show("add element");
                Graph.AddEdge(oldPt, newPt, out testA, out testB);

                if (testA >= 0) // test, it the new point is added to the graph
                {
                    isAdded = true;
                    SetNextDataNode(oldPt, newPt);
                }
            }
            return isAdded;
        }

        //============================================================================================================================================================================
        public void AddEdge(PointD pt1, PointD pt2)
        {
            Graph.AddEdge(pt1, pt2);
            if (!RefNodes.ContainsKey(pt1))
            {
                DataNode curNode = new DataNode();
                RefNodes.Add(pt1, curNode);
            }
            if (!RefNodes.ContainsKey(pt2))
            {
                DataNode curNode = new DataNode();
                curNode.AngleToGrow *= -1;
                RefNodes.Add(pt2, curNode);
            }

            //AddGraphElements(pt1, pt2);
        }

        //============================================================================================================================================================================
        public void CleanUp()
        {
            bool changed = true;
            int counter = 0;
            while (changed == true & counter < 10)
            {
                counter++;
                bool test1 = false;//CleanNearPointsToPoints();
                bool test2 = false;// CleanNearPointsToLine();
                bool test3 = false;// CleanNearLinesToLine();
                if (test1 | test2 | test3) changed = true;
                else changed = false;
            }
        }

        //============================================================================================================================================================================
        // -- finds points that are too close to a line --> delete the point or integrate a corner to the line --
        private bool CleanNearPointsToLine()
        {         
            UndirectedEdge<Vector2D>[] qEdges = QGraph.Edges.ToArray();
            List<Line2D> graphLines = new List<Line2D>();
            foreach (UndirectedEdge<Vector2D> edge in qEdges) graphLines.Add(new Line2D(edge.Source, edge.Target));
            List<Vector2D> graphPoints = QGraph.Vertices.ToList();
            Line2D curLine;
            bool changed = false;
            
            for (int i = 0; i < graphLines.Count; i++)    
            {
                curLine = graphLines[i];
                foreach (Vector2D curPt in graphPoints)
                {
                    bool partOfLine = false;
                    if (curPt == curLine.Start | curPt == curLine.End) partOfLine = true;
                    if (!partOfLine)
                    {
                        Vector2D intersetPt;
                        double dist = GeoAlgorithms2D.DistancePointToSegment(curPt, curLine.Start, curLine.End, out intersetPt);
                        // -- test if the nearest pont on the line is between end and start point --
                        if ((intersetPt != curLine.Start) & (intersetPt != curLine.End))
                        {
                            if (dist < minDist-1)
                            {
                                if (QGraph.ContainsVertex(curPt))
                                {
                                    if (QGraph.AdjacentDegree(curPt) == 1)
                                    {
                                        Line2D curEdgeL = GeoAlgorithms2D.NearestLineToPoint(graphLines.ToArray(), curPt, out dist);
                                        UndirectedEdge<Vector2D> curEdge = QGraph.Edges.ToList().Find(x => x.Source == curEdgeL.Start && x.Target == curEdgeL.End);
                                        if (curEdge != null)
                                        {
                                            QGraph.RemoveEdge(curEdge);
                                        }
                                        else // test reverse edge
                                        {
                                            UndirectedEdge<Vector2D> revCurEdge = QGraph.Edges.ToList().Find(x => x.Source == curEdgeL.End && x.Target == curEdgeL.Start);
                                            if (revCurEdge != null)
                                                QGraph.RemoveEdge(revCurEdge);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // --- delete unconnected nodes ---
                graphPoints = QGraph.Vertices.ToList();
                foreach (Vector2D curPt in graphPoints)
                {
                    if (QGraph.AdjacentDegree(curPt) == 0)
                        QGraph.RemoveVertex(curPt);
                }
            }
            return changed;
        }

        //============================================================================================================================================================================
        private bool CleanNearPointsToPoints()
        {
            //SortedListEx<PointD, SubdivisionEdge> vertices = Graph.Vertices;
            //List<PointD> nodes = vertices.Keys.ToList();
            List<Vector2D> nodes = new List<Vector2D>();
            nodes = QGraph.Vertices.ToList();
            bool nearFound;
            bool changed = false;
            //DataNode oldNode;

            while (nodes.Count > 0)
            {
                List<Vector2D> toNearPts = new List<Vector2D>();
                nearFound = false;
                Vector2D curVert = nodes[0];
                //oldNode = new DataNode();
                // --- find points that are located to close to each other ---
                for (int k = 1; k < nodes.Count; k++)
                {
                    Vector2D compareVert = nodes[k];
                    Double dist = GeoAlgorithms2D.DistancePoints(curVert, compareVert);// Graph.GetDistance(curVert, compareVert);
                    if (dist < minDist)
                    {
                        if (toNearPts.Count == 0)
                        {
                            toNearPts.Add(curVert);
                            //if (RefNodes.ContainsKey(curVert)) oldNode = RefNodes[curVert];
                        }
                        toNearPts.Add(compareVert);
                        nearFound = true;
                    }
                }

                if (nearFound)
                {
                    // --- remove the old edges ---
                    List<Vector2D> allNeightb = new List<Vector2D>();
                    foreach (Vector2D firstVert in toNearPts)
                    {
                        List<Vector2D> curNeightb = QGraph.GetNeighbors(firstVert);
                        allNeightb.AddRange(curNeightb);
                        foreach (Vector2D secVert in curNeightb)
                        {
                            //SubdivisionEdge curEdge = Graph.FindEdge(firstVert, secVert);
                            UndirectedEdge<Vector2D> curEdge = QGraph.Edges.ToList().Find(x => x.Source == firstVert && x.Target == secVert);
                            if (curEdge != null)
                            {
                                QGraph.RemoveEdge(curEdge);
                                changed = true;
                            }
                        }
                        //if (RefNodes.ContainsKey(firstVert)) RefNodes.Remove(firstVert);
                    }

                    // --- calculate the new midPoint ---
                    Vector2D midPt = new Vector2D();
                    foreach (Vector2D firstVert in toNearPts)
                    {
                        midPt += firstVert;
                    }
                    double divider = Convert.ToDouble(1.0 / toNearPts.Count);
                    midPt = midPt.Multiply(divider);

                    //if (RefNodes.ContainsKey(midPt) == false) RefNodes.Add(midPt, oldNode);

                    // --- add the new edges ---
                    LineD[] curLinesArr = Graph.ToLines();
                    List<LineD> curLinesList = curLinesArr.ToList();

                    foreach (Vector2D conPt in allNeightb)
                    {
                        //curLinesList.Add(new LineD(midPt, conPt));
                        UndirectedEdge<Vector2D> curAddEdge = new UndirectedEdge<Vector2D>(midPt, conPt);
                        UndirectedEdge<Vector2D> checkRevAddEdge = QGraph.Edges.ToList().Find(x => x.Source == midPt && x.Target == conPt);
                        if (checkRevAddEdge == null)
                            QGraph.AddVerticesAndEdge(curAddEdge);                        
                    }

                    //curLinesArr = curLinesList.ToArray();
                    //Graph = new Subdivision();
                    //Graph = Subdivision.FromLines(curLinesArr);
                }
                nodes.Remove(curVert);
            }
            // --- delete unconnected nodes ---
            nodes = QGraph.Vertices.ToList();
            foreach (Vector2D curPt in nodes)
            {
                if (QGraph.AdjacentDegree(curPt) == 0)
                    QGraph.RemoveVertex(curPt);
            }

            return changed;
        }

        //============================================================================================================================================================================
        // -- there is still a bug somewhere... ---
        private bool CleanNearLinesToLine()
        {
            List<LineD> graphLines = Graph.ToLines().ToList();
            List<PointD> graphPoints = Graph.Nodes.ToList();// Vertices.Keys.ToList();
            bool restart = false;
            bool changed = false;
            LineD curLine;
            //DataNode oldNode;
            int cter = 0;

            while (graphLines.Count > 0)
            {
                cter++;
                restart = false;
                curLine = graphLines[0];
                //oldNode = new DataNode();
                foreach (PointD curPt in graphPoints)
                {
                    bool partOfLine = false;
                    if (curPt == curLine.Start | curPt == curLine.End) partOfLine = true;
                    if (!partOfLine)
                    {
                        Vector2D interPt;
                        SubdivisionEdge curEdge;
                        double dist = GeoAlgorithms2D.DistancePointToSegment(curPt.ToVector2D(), curLine.Start.ToVector2D(), curLine.End.ToVector2D(), out interPt);
                        PointD intersetPt = interPt.ToPointD();
                        // -- test if the nearest pont on the line is between end and start point --
                        if ((intersetPt != curLine.Start) & (intersetPt != curLine.End))
                        {
                            if (dist < minDist-1)
                            {
                                // -- if we have a individual point, delete it --
                                if (Graph.GetNeighbors(curPt).Count > 1)
                                {

                                    //if (RefNodes.ContainsKey(curPt)) oldNode = RefNodes[curPt];
                                    // --- remove the old edges ---
                                    List<PointD> allNeightb = Graph.GetNeighbors(curPt).ToList();                                  
                                    // --- remove the edges that belong to the point that is too close to the line
                                    foreach (PointD secVert in allNeightb)
                                    {
                                        curEdge = Graph.FindEdge(curPt, secVert);
                                        SubdivisionEdge otherEdge = Graph.FindEdge(secVert, curPt);
                                        if (curEdge != null & otherEdge != null)
                                        {
                                            Graph.RemoveEdge(curEdge.Key);
                                            changed = true;
                                        }
                                    }
                                    curEdge = Graph.FindEdge(curLine.Start, curLine.End);
                                    Graph.RemoveEdge(curEdge.Key);
                                    allNeightb.Add(curLine.Start);
                                    allNeightb.Add(curLine.End);
                                    if (RefNodes.ContainsKey(curPt)) RefNodes.Remove(curPt);
                                    //if (RefNodes.ContainsKey(intersetPt) == false) RefNodes.Add(intersetPt, oldNode);

                                    // --- add the new edges ---
                                    LineD[] curLinesArr = Graph.ToLines();
                                    List<LineD> curLinesList = curLinesArr.ToList();

                                    foreach (PointD conPt in allNeightb)
                                    {
                                        curLinesList.Add(new LineD(intersetPt, conPt));
                                    }

                                    curLinesArr = curLinesList.ToArray();
                                    Graph = new Subdivision();
                                    Graph = Subdivision.FromLines(curLinesArr);
                                    restart = true;
                                    //break;
                                    return changed;
                                }
                            }
                        }
                    }
                }
                if (restart)
                {
                    graphLines = Graph.ToLines().ToList();
                    graphPoints = Graph.Nodes.ToList();
                }
                else
                    graphLines.Remove(curLine);
            }
            return changed;
        }


        //============================================================================================================================================================================
        //=========================================== Subdivision cleaners ===========================================================================================================
        //============================================================================================================================================================================
        public void CleanUpSubdiv()
        {
            bool changed = true;
            int counter = 0;
            while (changed == true & counter < 10)
            {
                counter++;
                bool test1 = CleanNearPointsToPointsSubdiv();
                bool test2 = CleanNearPointsToLineSubdiv();
                bool test3 = CleanNearLinesToLineSubdiv();
                if (test1 | test2 | test3) changed = true;
                else changed = false;
            }
        }

        //============================================================================================================================================================================
        private bool CleanNearPointsToLineSubdiv()
        {
            // -- finds points that are too close to a line --> delete the point or integrate a corner to the line --
            List<LineD> graphLines = Graph.ToLines().ToList();
            List<PointD> graphPoints = Graph.Nodes.ToList();// Vertices.Keys.ToList();
            LineD curLine;
            DataNode oldNode;
            bool changed = false;

            for (int i = 0; i < graphLines.Count; i++)
            //while (graphLines.Count > 0)         
            {
                //restart = false;
                curLine = graphLines[i];
                oldNode = new DataNode();
                foreach (PointD curPt in graphPoints)
                {
                    bool partOfLine = false;
                    if (curPt == curLine.Start | curPt == curLine.End) partOfLine = true;
                    if (!partOfLine)
                    {
                        Vector2D interPt;
                        SubdivisionEdge curEdge;
                        double dist = GeoAlgorithms2D.DistancePointToSegment(curPt.ToVector2D(), curLine.Start.ToVector2D(), curLine.End.ToVector2D(), out interPt);
                        PointD intersetPt = interPt.ToPointD();
                        // -- test if the nearest pont on the line is between end and start point --
                        if ((intersetPt != curLine.Start) & (intersetPt != curLine.End))
                        {
                            if (dist < minDist - 1)
                            {
                                if (Graph.GetNeighbors(curPt).Count == 1)
                                {
                                    curEdge = Graph.FindNearestEdge(curPt, out dist);
                                    if (curEdge != null)
                                    {
                                        bool test = Graph.RemoveEdge(curEdge.Key);
                                        if (test)
                                        {
                                            if (RefNodes.ContainsKey(curPt)) RefNodes.Remove(curPt);
                                            changed = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return changed;
        }

        //============================================================================================================================================================================
        private bool CleanNearPointsToPointsSubdiv()
        {
            SortedListEx<PointD, SubdivisionEdge> vertices = Graph.Vertices;
            List<PointD> nodes = vertices.Keys.ToList();
            bool nearFound;
            bool changed = false;
            DataNode oldNode;

            while (nodes.Count > 0)
            {
                List<PointD> toNearPts = new List<PointD>();
                nearFound = false;
                PointD curVert = nodes[0];
                oldNode = new DataNode();
                // --- find points that are located to close to each other ---
                for (int k = 1; k < nodes.Count; k++)
                {
                    PointD compareVert = nodes[k];
                    Double dist = Graph.GetDistance(curVert, compareVert);
                    if (dist < minDist)
                    {
                        if (toNearPts.Count == 0)
                        {
                            toNearPts.Add(curVert);
                            if (RefNodes.ContainsKey(curVert)) oldNode = RefNodes[curVert];
                        }
                        toNearPts.Add(compareVert);
                        nearFound = true;
                    }
                }

                if (nearFound)
                {
                    // --- remove the old edges ---
                    List<PointD> allNeightb = new List<PointD>();
                    foreach (PointD firstVert in toNearPts)
                    {
                        List<PointD> curNeightb = Graph.GetNeighbors(firstVert).ToList();
                        allNeightb.AddRange(curNeightb);
                        foreach (PointD secVert in curNeightb)
                        {
                            SubdivisionEdge curEdge = Graph.FindEdge(firstVert, secVert);
                            if (curEdge != null)
                            {
                                Graph.RemoveEdge(curEdge.Key);
                                changed = true;
                            }
                        }
                        if (RefNodes.ContainsKey(firstVert)) RefNodes.Remove(firstVert);
                    }

                    // --- calculate the new midPoint ---
                    PointD midPt = new PointD();
                    foreach (PointD firstVert in toNearPts)
                    {
                        midPt += firstVert;
                    }
                    double divider = Convert.ToDouble(1.0 / toNearPts.Count);
                    midPt = midPt.MultiplyD(divider);

                    if (RefNodes.ContainsKey(midPt) == false) RefNodes.Add(midPt, oldNode);

                    // --- add the new edges ---
                    LineD[] curLinesArr = Graph.ToLines();
                    List<LineD> curLinesList = curLinesArr.ToList();

                    foreach (PointD conPt in allNeightb)
                    {
                        curLinesList.Add(new LineD(midPt, conPt));
                    }

                    curLinesArr = curLinesList.ToArray();
                    Graph = new Subdivision();
                    Graph = Subdivision.FromLines(curLinesArr);
                }
                nodes.Remove(curVert);
            }
            return changed;
        }

        //============================================================================================================================================================================
        // -- there is still a bug somewhere... ---
        private bool CleanNearLinesToLineSubdiv()
        {
            List<LineD> graphLines = Graph.ToLines().ToList();
            List<PointD> graphPoints = Graph.Nodes.ToList();// Vertices.Keys.ToList();
            bool restart = false;
            bool changed = false;
            LineD curLine;
            DataNode oldNode;
            int cter = 0;

            while (graphLines.Count > 0)
            {
                cter++;
                restart = false;
                curLine = graphLines[0];
                oldNode = new DataNode();
                foreach (PointD curPt in graphPoints)
                {
                    bool partOfLine = false;
                    if (curPt == curLine.Start | curPt == curLine.End) partOfLine = true;
                    if (!partOfLine)
                    {
                        Vector2D interPt;                     
                        SubdivisionEdge curEdge;
                        double dist = GeoAlgorithms2D.DistancePointToSegment(curPt.ToVector2D(), curLine.Start.ToVector2D(), curLine.End.ToVector2D(), out interPt);
                        PointD intersetPt = interPt.ToPointD();
                        // -- test if the nearest pont on the line is between end and start point --
                        if ((intersetPt != curLine.Start) & (intersetPt != curLine.End))
                        {
                            if (dist < minDist - 1)
                            {
                                // -- if we have a individual point, delete it --
                                if (Graph.GetNeighbors(curPt).Count > 1)
                                {

                                    if (RefNodes.ContainsKey(curPt)) oldNode = RefNodes[curPt];
                                    // --- remove the old edges ---
                                    List<PointD> allNeightb = Graph.GetNeighbors(curPt).ToList();
                                    // --- remove the edges that belong to the point that is too close to the line
                                    foreach (PointD secVert in allNeightb)
                                    {
                                        curEdge = Graph.FindEdge(curPt, secVert);
                                        SubdivisionEdge otherEdge = Graph.FindEdge(secVert, curPt);
                                        if (curEdge != null & otherEdge != null)
                                        {
                                            Graph.RemoveEdge(curEdge.Key);
                                            changed = true;
                                        }
                                    }
                                    curEdge = Graph.FindEdge(curLine.Start, curLine.End);
                                    Graph.RemoveEdge(curEdge.Key);
                                    allNeightb.Add(curLine.Start);
                                    allNeightb.Add(curLine.End);
                                    if (RefNodes.ContainsKey(curPt)) RefNodes.Remove(curPt);
                                    if (RefNodes.ContainsKey(intersetPt) == false) RefNodes.Add(intersetPt, oldNode);

                                    // --- add the new edges ---
                                    LineD[] curLinesArr = Graph.ToLines();
                                    List<LineD> curLinesList = curLinesArr.ToList();

                                    foreach (PointD conPt in allNeightb)
                                    {
                                        curLinesList.Add(new LineD(intersetPt, conPt));
                                    }

                                    curLinesArr = curLinesList.ToArray();
                                    Graph = new Subdivision();
                                    Graph = Subdivision.FromLines(curLinesArr);
                                    restart = true;
                                    //break;
                                    return changed;
                                }
                            }
                        }
                    }
                }
                if (restart)
                {
                    graphLines = Graph.ToLines().ToList();
                    graphPoints = Graph.Nodes.ToList();
                }
                else
                    graphLines.Remove(curLine);
            }
            return changed;
        }   

    }

}


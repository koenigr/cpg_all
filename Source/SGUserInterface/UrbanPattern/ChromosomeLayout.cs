﻿// CrossoverLayout.cs
//----------------------------------
// Last modified on 10/06/2014
// by Dr. Reinhard König
//
// Description:
//
// Comments:
// 
//
// To Dos:
// 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Drawing2D;
using System.Drawing;
using AForge;
using AForge.Genetic;
using CPlan.Geometry;
using CPlan.Evaluation;
using CPlan.Optimization;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public class ChromosomeLayout : MultiPisaChromosomeBase
    {

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Private Fields

        /// <summary>
        /// Random generator used for chromosoms' generation.
        /// </summary>
        protected static ThreadSafeRandom rand = new ThreadSafeRandom();
        //----------------------------------------------------------------------
        private Random Rnd = new Random((int)DateTime.Now.Ticks);
        private Collision m_Collision = new Collision();

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        //----------------------------------------------------------------------
        /// <summary>
        /// Gets the list of Gens of a ChromosomeLayout object.
        /// </summary>
        /// <value> List of Gens.</value>
        public List<GenBuildingLayout> Gens
        {
            get;
            private set;
        }

        //----------------------------------------------------------------------
        public double ProportionRatio
        {
            get;
            set;
        }

        //----------------------------------------------------------------------
        public double Overlapping
        {
            get;
            set;
        }

        //----------------------------------------------------------------------
        /// <summary>
        /// Defines the minimum number of buildings (gens) of a chromosome.
        /// </summary>
        public int MinNrBuildings
        {
            get;
            set;
        }

        //----------------------------------------------------------------------
        /// <summary>
        /// Defines the maximum number of buildings (gens) of a chromosome.
        /// </summary>
        public int MaxNrBuildings
        {
            get;
            set;
        }

        //----------------------------------------------------------------------
        public Poly2D Border
        {
            get;
            set;
        }

        //----------------------------------------------------------------------
        public Rect2D BoundingRect
        {
            get;
            set;
        }

        //----------------------------------------------------------------------
        public List<Poly2D> Environment
        {
            get;
            set;
        }

        ////----------------------------------------------------------------------
        //public IsovistFieldOLD IsovistField
        //{
        //    get;
        //    set;
        //}
        //----------------------------------------------------------------------
        public IsovistAnalysis IsovistField
        {
            get;
            set;
        }

        //----------------------------------------------------------------------
        /// <summary>
        /// Defines the density of an area that have to remain constant after crossover and mutation.
        /// Value between 0 (empty) and 1 (full).
        /// </summary>
        public double Density
        {
            get;
            set;
        }

        //----------------------------------------------------------------------
        /// <summary>
        /// Defines the minimal length of a buildings side.
        /// The square root of the buildings area is multiplied by this value. The range is [0; 1].
        /// E.g. 0.5 means that the minimal value for a side is half of the length of a square with the same area.
        /// </summary>
        public double MinSideRatio
        {
            get;
            set;
        }

        //----------------------------------------------------------------------
        /// <summary>
        /// Active layouts are considered in the adaption loop and you can interact with it's gens.
        /// </summary>
        public bool Active { get; set; }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        # region Constructors

        //----------------------------------------------------------------------------------------------
        public ChromosomeLayout(Poly2D border, int minPolys, int maxPolys, double density, double minSideRatio, int identity = -1)
        {
            Active = true;
            this.m_ID = (identity == -1) ? MultiPisaChromosomeBase.m_LastID++ : identity;
            this.Generation = 0;
            Rnd = new Random();
            Density = density;
            MinSideRatio = minSideRatio;
            MinNrBuildings = minPolys;
            MaxNrBuildings = maxPolys;
            Border = new Poly2D(border);
            BoundingRect = GeoAlgorithms2D.BoundingBox(border.PointsFirstLoop);
            // --- initialize the chromosoma ---
            Initialize();
        }

        //----------------------------------------------------------------------------------------------
        // Copy constructor.
        public ChromosomeLayout(ChromosomeLayout previousChromosome)
        {
            int nrPolys = previousChromosome.Gens.Count;
            //StrategyParam = new double[4] { previousChromosome.StrategyParam[0], previousChromosome.StrategyParam[1], previousChromosome.StrategyParam[2], previousChromosome.StrategyParam[3]};
            Generation = previousChromosome.Generation;
            ProportionRatio = previousChromosome.ProportionRatio;
            Overlapping = previousChromosome.Overlapping;
            MinNrBuildings = previousChromosome.MinNrBuildings;
            MaxNrBuildings = previousChromosome.MinNrBuildings;
            Border = previousChromosome.Border;
            BoundingRect = previousChromosome.BoundingRect;
            IsovistField = previousChromosome.IsovistField;
            Density = previousChromosome.Density;
            MinSideRatio = previousChromosome.MinSideRatio;
            Active = previousChromosome.Active;

            this.m_ID = MultiPisaChromosomeBase.m_LastID++;
            Gens = new List<GenBuildingLayout>();
            //Springs.Clear();
            for (int i = 0; i < nrPolys; i++)
            {
                GenBuildingLayout curGen = new GenBuildingLayout(previousChromosome.Gens[i]);
                if (previousChromosome.Gens[i].Locked)
                    previousChromosome.Gens[i].Locked = true;
                Gens.Add(curGen);
            }
            this.m_fitness_values = new List<double>(previousChromosome.FitnessValues.Count);
            foreach (double value in previousChromosome.FitnessValues)
            {
                m_fitness_values.Add(value);
            }
            //for (int i = 0; i < previousChromosome.Springs.Count; i++)
            //{
            //    RSpring curSpring = new RSpring(previousChromosome.Springs[i]);
            //    Springs.Add(curSpring);
            //}
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        //==================================================================
        public override void Generate()
        {
            Initialize();
        }

        //==================================================================
        public override IMultiChromosome CreateNew()
        {
            return new ChromosomeLayout(Border, MinNrBuildings, MaxNrBuildings, Density, MinSideRatio);  
        }

        //==================================================================
        public override IMultiChromosome Clone()
        {
            return new ChromosomeLayout(this);
        }

        //==================================================================
        /// <summary>
        /// Initializes the Chromosome for layouts.
        /// </summary>
        public void Initialize()
        {
            Gens = new List<GenBuildingLayout>();
            Rect2D bRect = this.BoundingRect;
            // --- create the Gens ---
            GenBuildingLayout curGen;
            int maxDiff = MaxNrBuildings - MinNrBuildings;
            int nrBuildings = MinNrBuildings + Rnd.Next(maxDiff);
            double area = (Border.Area * Density) / nrBuildings;
            for (int i = 0; i < nrBuildings; i++)
            {
                curGen = new GenBuildingLayout(Rnd.Next((int)bRect.Width) + Border.Left,
                                               Rnd.Next((int)bRect.Height) + Border.Bottom,
                                               bRect, area, MinSideRatio, i);
                Gens.Add(curGen);
            }
            //SetScenarioChromosome();
            RemainDensity(this);
            Adapt();
        }

        //============================================================================================================================================================================
        private void SetScenarioChromosome()
        {
            string scenario = ControlParameters.Scenario;
            if (scenario == "Cape Town Scenario")
            {

            }
            else
            {
                int idx = Gens.Count - 1;
                Gens[idx].Locked = true;
                Gens[idx].Open = true;
                Gens[idx].Attractor = true;
                Gens[idx].Position = new Vector2D(225, 400);
                Gens[idx].Rotation = 0;
                Gens[idx].Width = 50;
                Gens[idx].Height = 400;
            }
        }

        //==================================================================
        public void AttractPolys(double force, double radius)
        {
            Vector2D velocity = new Vector2D(0, 0);
            GenBuildingLayout actGen;
            Poly2D curAttractor = new Poly2D();
            List<Poly2D> attractors = new List<Poly2D>();
            foreach (GenBuildingLayout curGen in Gens)
            {
                if (curGen.Attractor) attractors.Add(curGen.Poly);
            }

            for (int i = 0; i < Gens.Count; i++)
            {
                actGen = (GenBuildingLayout)Gens[i];
                if (actGen.Attractor)
                    continue;
                double[] dists = new double[attractors.Count];

                // -- calculate the distances --
                for (int j = 0; j < attractors.Count; j++)
                {
                    curAttractor = attractors[j];
                    dists[j] = actGen.Poly.Distance(curAttractor);
                }

                // -- find shortest distance --
                int minIdx = -1;
                double minDist = double.MaxValue;
                for (int j = 0; j < dists.Length; j++)
                {
                    if (dists[j] < minDist)
                    {
                        minDist = dists[j];
                        minIdx = j;
                    }
                }

                if (minDist < radius)
                {
                    Vector2D intersection = new Vector2D();
                    if (!actGen.Locked)
                    {
                        double dist = attractors[minIdx].Distance(actGen.Poly, out intersection);
                        if (dist > 0 && minDist > 0)
                        {
                            intersection *= force;
                            actGen.Move(intersection);
                        }
                    }
                }
            }
        }

        //==================================================================
        public void RepelPolys(double force, double rate)
        {
            Vector2D velocity = new Vector2D(0, 0);
            GenBuildingLayout firstGen, secondGen;
            Vector2D translation = new Vector2D(velocity);

            for (int i = 0; i < Gens.Count; i++)
            {
                firstGen = (GenBuildingLayout)Gens[i];              
                for (int j = i + 1; j < Gens.Count; j++)
                {
                    if (Rnd.NextDouble() < rate)
                    {
                        secondGen = (GenBuildingLayout)Gens[j];
                        PolygonCollisionResult r = m_Collision.PolygonCollision(firstGen.ToPoly2D(), secondGen.ToPoly2D(), velocity);
                        if (r.WillIntersect)
                        {
                            translation = velocity + r.MinimumTranslationVector;
                            // -- add random values to the translation vector to avoid blockin situation --
                            if (translation.X == 0 & translation.Y > 0) translation.X = (Rnd.NextDouble() - 0.5) * translation.Y;//Rnd.NextDouble() * translation.Y;//
                            else if (translation.Y == 0 & translation.X > 0) translation.Y = (Rnd.NextDouble() - 0.5) * translation.X;//Rnd.NextDouble() * translation.X;//
                            translation.Multiply(force);
                            if (!firstGen.Locked)
                                firstGen.Move(translation);
                            translation.X = -translation.X;
                            translation.Y = -translation.Y;
                            if (!secondGen.Locked)
                                secondGen.Move(translation);
                        }
                    }
                }
            }
        }

        //==================================================================
        public void RepelJumpPolys(double force, double rate)
        {
                    
            Vector2D velocity = new Vector2D(0, 0);
            GenBuildingLayout firstGen, secondGen;
            Vector2D translation = new Vector2D(velocity);

            for (int i = 0; i < Gens.Count; i++)
            {
                firstGen = (GenBuildingLayout)Gens[i];
                double firstGArea = firstGen.Poly.Area;

                for (int j = i + 1; j < Gens.Count; j++)
                {
                    secondGen = (GenBuildingLayout)Gens[j];
                    double testArea = Overlap(firstGen, secondGen);
                    if (testArea / firstGArea > 0.8)
                    {
                        firstGen.Position = new Vector2D(
                            Rnd.Next(-(int)BoundingRect.Width / 2, (int)BoundingRect.Width / 2) + Border.Center.X,
                            Rnd.Next(-(int)BoundingRect.Height / 2, (int)BoundingRect.Height / 2) + Border.Center.Y);
                    }

                    if (Rnd.NextDouble() < rate)
                    {
                        PolygonCollisionResult r = m_Collision.PolygonCollision(firstGen.ToPoly2D(), secondGen.ToPoly2D(), velocity);
                        if (r.WillIntersect)
                        {
                            translation = velocity + r.MinimumTranslationVector;
                            // -- add random values to the translation vector to avoid blockin situation --
                            if (translation.X == 0 & translation.Y > 0) translation.X = (Rnd.NextDouble() - 0.5) * translation.Y;//Rnd.NextDouble() * translation.Y;//
                            else if (translation.Y == 0 & translation.X > 0) translation.Y = (Rnd.NextDouble() - 0.5) * translation.X;//Rnd.NextDouble() * translation.X;//
                            translation.Multiply(force);
                            if (!firstGen.Locked)
                                firstGen.Move(translation);
                            translation.X = -translation.X;
                            translation.Y = -translation.Y;
                            if (!secondGen.Locked)
                                secondGen.Move(translation);
                        }
                    }
                }
            }
        }

        //==================================================================
        public void ChangeRotation(double force, double rate)
        {
            Vector2D velocity = new Vector2D(0, 0);
            GenBuildingLayout firstGen, secondGen;
            Vector2D translation = new Vector2D(velocity);
            double divider;

            for (int i = 0; i < Gens.Count; i++)
            {
                firstGen = (GenBuildingLayout)Gens[i];
                for (int j = i + 1; j < Gens.Count; j++)
                {
                    if (Rnd.NextDouble() < rate)
                    {
                        secondGen = (GenBuildingLayout)Gens[j];
                        PolygonCollisionResult r = m_Collision.PolygonCollision(firstGen.ToPoly2D(), secondGen.ToPoly2D(), velocity);
                        if (r.WillIntersect && r.MinimumTranslationVector.Length > .1)
                        {
                            // -- virtual rotations -> to ensure minimal rotations for becomming parallel -- 
                            double firstVirtR = firstGen.Rotation;
                            divider = Math.Ceiling(firstVirtR / 90);
                            if (divider > 1)
                                firstVirtR -= (divider - 1) * 90;
                            if (firstVirtR > 45) firstVirtR -= 90;
                            if (firstVirtR < -45) firstVirtR += 90;

                            double secondVirtR = secondGen.Rotation;
                            divider = Math.Ceiling(secondVirtR / 90);
                            if (divider > 1)
                                secondVirtR -= (divider - 1) * 90;
                            if (secondVirtR > 45) secondVirtR -= 90;
                            if (secondVirtR < -45) secondVirtR += 90;

                            double diffRot = Math.Abs(firstVirtR - secondVirtR);
                            // -- adapt rotations --
                            diffRot *= force;
                            if (firstVirtR > secondVirtR)
                            {
                                if (!firstGen.Locked) firstGen.Rotation -= diffRot;
                                if (!secondGen.Locked) secondGen.Rotation += diffRot;
                            }
                            else
                            {
                                if (!firstGen.Locked) firstGen.Rotation += diffRot;
                                if (!secondGen.Locked) secondGen.Rotation -= diffRot;
                            }
                        }
                    }
                }
            }
        }

        //==================================================================
        public void BorderRotation(double force, double rate)
        {
            Vector2D velocity = new Vector2D(0, 0);
            GenBuildingLayout gen;
            Vector2D translation = new Vector2D(velocity);
            double divider, interArea;
            Poly2D boder = new Poly2D(Border.Points);
            List<Line2D> boderEdges = Border.Edges;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (Rnd.NextDouble() < rate)
                {
                    gen = (GenBuildingLayout)Gens[i];

                    Poly2D poly = gen.Poly;
                    double rectArea = poly.Area;
                    List<Poly2D> isect = boder.Intersection(poly);
                    if (isect == null) interArea = 0;
                    else
                    {
                        interArea = isect[0].Area;
                    }
                    double diffArea = rectArea - interArea;
                    if (diffArea > 10 & interArea > 0.1)
                    {
                        Line2D refLine = null;
                        foreach (Line2D iLine in boderEdges)
                        {
                            List<Vector2D> interPts = poly.Intersection(iLine);
                            if (interPts.Count > 0)
                            {
                                refLine = iLine;
                            }
                        }

                        Vector2D refVector = refLine.End - refLine.Start;
                        // test quadrant 
                        if ((refVector.X < 0 & refVector.Y < 0) || (refVector.X > 0 & refVector.Y < 0))
                            refVector *= -1;

                        double refRotation = GeoAlgorithms2D.AngleBetweenD(refVector, new Vector2D(1, 0));

                        // -- virtual rotations -> to ensure minimal rotations for becomming parallel -- 
                        double firstVirtR = gen.Rotation;
                        divider = Math.Ceiling(firstVirtR / 90);
                        if (divider > 1)
                            firstVirtR -= (divider - 1) * 90;
                        if (firstVirtR > 45) firstVirtR -= 90;
                        if (firstVirtR < -45) firstVirtR += 90;

                        divider = Math.Ceiling(refRotation / 90);
                        if (divider > 1)
                            refRotation -= (divider - 1) * 90;
                        if (refRotation > 45) refRotation -= 90;
                        if (refRotation < -45) refRotation += 90;

                        double secondVirtR = refRotation;// 0;                          
                        double diffRot = Math.Abs(firstVirtR - secondVirtR);

                        // -- adapt rotations --
                        diffRot *= force;
                        if (firstVirtR > secondVirtR)
                        {
                            if (!gen.Locked)
                                gen.Rotation -= diffRot;
                        }
                        else
                        {
                            if (!gen.Locked)
                                gen.Rotation += diffRot;
                        }
                    }
                }
            }
        }

        //==================================================================
        public void ChangeProportion(double force, double rate)
        {
            GenBuildingLayout firstGen, secondGen;
            Vector2D velocity = new Vector2D(0, 0);
            Vector2D translation = new Vector2D(velocity);

            for (int i = 0; i < Gens.Count; i++)
            {
                firstGen = (GenBuildingLayout)Gens[i];
                for (int j = i + 1; j < Gens.Count; j++)
                {
                    secondGen = (GenBuildingLayout)Gens[j];
                    PolygonCollisionResult r = m_Collision.PolygonCollision(firstGen.ToPoly2D(), secondGen.ToPoly2D(), velocity);
                    if (r.WillIntersect)
                    {
                        translation = velocity + r.MinimumTranslationVector;
                        translation.Multiply(force);

                        if (!firstGen.Locked) VaryOnesSize(rate, firstGen, translation);
                        if (!secondGen.Locked) VaryOnesSize(rate, secondGen, translation);
                    }
                }
            }
        }

        //==============================================================================================
        private void VaryOnesSize(double rate, GenBuildingLayout curGen, Vector2D changer)
        {
            double width;
            double mag;
            changer.X = Math.Abs(changer.X);
            changer.Y = Math.Abs(changer.Y);
            mag = changer.Magnitude;
            double multi = 5;

            if (!curGen.Locked)          
            {
                if (Rnd.NextDouble() < rate)
                {
                    if (curGen.CheckRot())
                    {
                        if (changer.X > changer.Y)
                        {
                            if (Rnd.NextDouble() < 0.9) width = curGen.Width - mag;
                            else width = curGen.Width + mag * multi;
                        }
                        else
                        {
                            if (Rnd.NextDouble() < 0.9) width = curGen.Width + mag;
                            else width = curGen.Width - mag * multi;
                        }
                    }
                    else
                    {
                        if (changer.X > changer.Y)
                        {
                            if (Rnd.NextDouble() < 0.9) width = curGen.Width + mag;
                            else width = curGen.Width - mag * multi;
                        }
                        else
                        {
                            if (Rnd.NextDouble() < 0.9) width = curGen.Width - mag;
                            else width = curGen.Width + mag * multi;
                        }
                    }
                    curGen.CheckWidtHeight(width);
                }
            }
        }

        //==================================================================
        public void CheckPolyBorder()
        {
            Vector2D translation = new Vector2D(0, 0);
            for (int i = 0; i < Gens.Count; i++)
            {
                GenBuildingLayout curGen = Gens[i];
                Poly2D curRect = curGen.Poly;
                Vector2D delta = new Vector2D();
                if (!curGen.Locked)
                {
                    double dist = Border.MaxDistance(curRect, out delta);
                    if (delta != null)
                    {
                        if (delta.Length > dist * 2)
                        {
                            //dist = Border.MaxDistance(curRect, out delta);
                            //break;
                        }
                        if (dist > 0)
                            curGen.Move(delta);
                    }
                }
            }
        }

        //==================================================================
        public void CheckBorder()
        {
            Vector2D translation = new Vector2D(0, 0);
            int faktor = 1;
            double damp = 1;

            for (int i = 0; i < Gens.Count; i++)
            {
                GenBuildingLayout curGen = Gens[i];
                Poly2D curPoly = curGen.Poly;
                if (!curGen.Locked)
                {
                    // ---------------------------------------------------------------------------
                    if (curPoly.Left < Border.Left)
                    {
                        translation.X = (Border.Left - curPoly.Left) / faktor;
                        translation.Y = 0;
                        translation.Multiply(damp);
                        curGen.Move(translation);
                    }
                    else if (curPoly.Right > Border.Right)
                    {
                        translation.X = (Border.Right - curPoly.Right) / faktor;
                        translation.Y = 0;
                        translation.Multiply(damp);
                        curGen.Move(translation);
                    }
                    if (curPoly.Top > Border.Top)
                    {
                        translation.X = 0;
                        translation.Y = (Border.Top - curPoly.Top) / faktor;
                        translation.Multiply(damp);
                        curGen.Move(translation);
                    }
                    else if (curPoly.Bottom < Border.Bottom)
                    {
                        translation.X = 0;
                        translation.Y = (Border.Bottom - curPoly.Bottom) / faktor;
                        translation.Multiply(damp);
                        curGen.Move(translation);
                    }
                }
            }
        }

        //==================================================================
        public void CheckBorderByPoints()
        {
            Vector2D translation = new Vector2D(0, 0);
            int faktor = 1;
            double damp = 1;
            List<Poly2D> isectPoly;
            double outArea;
            bool checkAgain = false;

            for (int i = 0; i < Gens.Count; i++)
            {
                GenBuildingLayout curGen = Gens[i];
                Poly2D curPoly = curGen.Poly;
                if (!curGen.Locked)
                {
                    // ---------------------------------------------------------------------------
                    translation = new Vector2D(0, 0);
                    for (int j = 0; j < curGen.Points.Count; j++)
                    {
                        Vector2D curVect = curGen.Points[j];
                        double deltaX, deltaY;
                        if (curVect.X < Border.Left)
                        {                           
                            deltaX = (Border.Left - curVect.X) / faktor;
                            if (deltaX > translation.X) translation.X = deltaX;
                        }
                        else if (curVect.X > Border.Right)
                        {
                            deltaX = (Border.Right - curVect.X) / faktor;
                            if (deltaX > translation.X) translation.X = deltaX;
                        }
                        if (curVect.Y > Border.Top)
                        {
                            deltaY = (Border.Top - curVect.Y) / faktor;
                            if (deltaY > translation.Y) translation.Y = deltaY;
                        }
                        else if (curVect.Y < Border.Bottom)
                        {
                            deltaY = (Border.Bottom - curVect.Y) / faktor;
                            if (deltaY > translation.Y) translation.Y = deltaY;
                        }
                    }
                    translation.Multiply(damp);
                    curGen.Move(translation);
                  
                    // -- random jumps --
                    isectPoly = curPoly.Intersection(Border);
                    if (isectPoly == null)
                    {
                        outArea = curPoly.Area;
                        //isectPoly = Border.Intersection(curPoly);
                    }
                    else outArea = curPoly.Area - isectPoly[0].Area;
                    double ratio = outArea / curPoly.Area;
                    if (ratio > 0.9 && rand.NextDouble() < 0.1)
                    {
                        if (Border.Distance(curPoly) > 0)
                        {
                            curGen.Position = new Vector2D(
                                Rnd.Next(-(int)BoundingRect.Width / 4, (int)BoundingRect.Width / 4) + Border.Center.X,
                                Rnd.Next(-(int)BoundingRect.Height / 2, (int)BoundingRect.Height / 2) + Border.Center.Y);
                            checkAgain = true;
                        }
                    }
                }
            }
            if (checkAgain)
                CheckBorder();
        }

        //==============================================================================================
        public bool CheckBorder(Poly2D testRect)
        {
            bool isInside = true;
            double tolerance = 5;

            if (testRect.Left + tolerance < Border.Left)
            {
                isInside = false;
            }
            else if (testRect.Right - tolerance > Border.Right)
            {
                isInside = false;
            }
            if (testRect.Top - tolerance > Border.Top)
            {
                isInside = false;
            }
            else if (testRect.Bottom + tolerance < Border.Bottom)
            {
                isInside = false;
            }

            return isInside;
        }

        //==============================================================================================
        public void Jump(double jumpRate)
        {
            double rndVal;
            GenBuildingLayout curGen;
            Vector2D newPos = new Vector2D(0, 0);
            Vector2D dPos = new Vector2D(0, 0);

            for (int i = 0; i < Gens.Count; i++)
            {
                curGen = Gens[i];
                if (!curGen.Locked)
                {
                    rndVal = Rnd.NextDouble();
                    if (rndVal < (jumpRate))
                    {
                        newPos.X = Rnd.NextDouble() * BoundingRect.Width  + Border.Center.X;
                        newPos.Y = Rnd.NextDouble() * BoundingRect.Height + Border.Center.Y;
                        curGen.Center = newPos;
                    }
                }
            }
        }

        //==============================================================================================
        public void RotateRandon(double rate, double sigmaValue)
        {
            GenBuildingLayout curGen;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (Rnd.NextDouble() < rate)
                {
                    curGen = Gens[i];
                    double rotation = 0;
                    Helper.NewNormDistGenerator(); //m_normDist = new NormalDistribution(newGenerator);
                    Helper.NormDist.Sigma = sigmaValue;// overlap / 1000;  

                    if (!curGen.Locked)
                    {
                        rotation = Helper.NormDist.NextDouble() - 1;
                        curGen.Poly.Rotate(rotation);
                    }

                }
            }
        }

        //==============================================================================================
        public void MoveRandon(double rate, double sigmaValue)
        {
            GenBuildingLayout curGen;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (Rnd.NextDouble() < rate)
                {
                    curGen = Gens[i];
                    Vector2D dPos = new Vector2D(0, 0);
                    Helper.NewNormDistGenerator(); 
                    Helper.NormDist.Sigma = sigmaValue;

                    if (!curGen.Locked)
                    {
                        dPos.X = Helper.NormDist.NextDouble() - 1;
                        dPos.Y = Helper.NormDist.NextDouble() - 1;

                        Poly2D newRect = curGen.Poly.Clone();
                        newRect.Move(dPos);
                        curGen.Move(dPos);
                    }
                }
            }
        }

        //==============================================================================================
        public void ReplaceGen(GenBuildingLayout oldGen, GenBuildingLayout newGen)
        {
            int idx = Gens.IndexOf(oldGen);
            Gens[idx] = new GenBuildingLayout(newGen);
        }

        //==============================================================================================
        public void VarySize(double rate)
        {
            GenBuildingLayout curGen;
            double min;
            double width, rVal;
            double height;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (Rnd.NextDouble() < rate)
                {
                    double divider;
                    if (Rnd.NextDouble() < 0.01)
                        divider = (int)Math.Round((decimal)(BoundingRect.Height * BoundingRect.Width) / 10);
                    else
                        divider = (int)Math.Round((decimal)(BoundingRect.Height * BoundingRect.Width) / 180);

                    Helper.NormDist.Sigma = (int)Math.Round((decimal)(BoundingRect.Height + BoundingRect.Width) / (Gens.Count), 0);// 
                    curGen = Gens[i];
                    min = curGen.MinSide;
                    if (!curGen.Locked)
                    {
                        rVal = Helper.NormDist.NextDouble() - 1;
                        width = curGen.Width + rVal;
                        if (width < min)
                            width = min;      //-- Minimale Scalierung X
                        else if (width > BoundingRect.Width) width = BoundingRect.Width; //-- Maximale Scalierung X
                        height = (curGen.Area / width);
                        curGen.Height = height;
                        curGen.Width = width;
                        if (curGen.Height < min)
                        {
                            curGen.Height = min;
                            curGen.Width = (curGen.Area / curGen.Height);
                        }
                        if (curGen.Height > BoundingRect.Height)
                        {
                            curGen.Height = BoundingRect.Height;
                            curGen.Width = (curGen.Area / curGen.Height);
                        }
                    }
                }
            }
        }

        //==============================================================================================
        public void VarySizeFull(double rate)
        {
            GenBuildingLayout curGen;
            double min = 50.0;
            double width, rVal;
            double height;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (Rnd.NextDouble() < rate)//
                {
                    Helper.NewNormDistGenerator(); 
                    Helper.NormDist.Sigma = (int)Math.Round((decimal)(BoundingRect.Height + BoundingRect.Width) / (Gens.Count), 0);
                    curGen = Gens[i];
                    if (!curGen.Locked)
                    {
                        rVal = Helper.NormDist.NextDouble() - 1;
                        width = curGen.Width + rVal;
                        if (width < min)
                            width = min;      //-- Minimale Scalierung X
                        else if (width > BoundingRect.Width) width = BoundingRect.Width; //-- Maximale Scalierung X
                        height = (curGen.Area / width);
                        curGen.Height = height;
                        curGen.Width = width;
                        if (curGen.Height < min)
                        {
                            curGen.Height = min;
                            curGen.Width = (curGen.Area / curGen.Height);
                        }
                        if (curGen.Height > BoundingRect.Height)
                        {
                            curGen.Height = BoundingRect.Height;
                            curGen.Width = (curGen.Area / curGen.Height);
                        }
                    }
                }
            }
        }

        //==============================================================================================
        public void VarySizeFull(double rate, double sigmaValue)
        {
            GenBuildingLayout curGen;
            double min = 50.0;
            double width, rVal;
            double height;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (Rnd.NextDouble() < rate)
                {
                    Helper.NewNormDistGenerator();
                    Helper.NormDist.Sigma = sigmaValue;
                    curGen = Gens[i];
                    if (!curGen.Locked)
                    {
                        rVal = Helper.NormDist.NextDouble() - 1;
                        width = curGen.Width + rVal;
                        if (width < min)
                            width = min;      //-- Minimale Scalierung X
                        else if (width > BoundingRect.Width) width = BoundingRect.Width; //-- Maximale Scalierung X
                        height = (curGen.Area / width);
                        curGen.Height = height;
                        curGen.Width = width;
                        if (curGen.Height < min)
                        {
                            curGen.Height = min;
                            curGen.Width = (curGen.Area / curGen.Height);
                        }
                        if (curGen.Height > BoundingRect.Height)
                        {
                            curGen.Height = BoundingRect.Height;
                            curGen.Width = (curGen.Area / curGen.Height);
                        }
                    }
                }
            }
        }

        //==============================================================================================
        public double Overlap()
        {
            double overlapAreaSum = 0;
            double shapeArea, diffArea = 0;
            double rectArea = 0;
            Poly2D poly1, poly2;
            Poly2D boder;
            List<Poly2D> isectRect;

            for (int i = 0; i < Gens.Count; i++)
            {
                poly1 = new Poly2D(Gens[i].Poly);
                rectArea = poly1.Area;
                for (int k = i + 1; k < Gens.Count; k++)
                {
                    poly2 = new Poly2D(Gens[k].Poly);
                    isectRect = poly1.Intersection(poly2);
                    if (isectRect == null) shapeArea = 0;
                    else shapeArea = isectRect[0].Area;
                    overlapAreaSum = overlapAreaSum + shapeArea;
                }
                boder = new Poly2D(Border.Points);
                isectRect = boder.Intersection(poly1);
                if (isectRect == null) shapeArea = 0;
                else shapeArea = isectRect[0].Area;
                diffArea = rectArea - shapeArea;
                overlapAreaSum += diffArea;
            } 
            this.Overlapping = overlapAreaSum;
            return overlapAreaSum;
        }

        //==============================================================================================
        public double Overlap(GenBuildingLayout firstGen, GenBuildingLayout secondGen)
        {
            double shapeArea;
            Poly2D poly1, poly2;
            List<Poly2D> isectPoly;

            poly1 = new Poly2D(firstGen.Poly); 
            poly2 = new Poly2D(secondGen.Poly);
            isectPoly = poly1.Intersection(poly2);
            if (isectPoly == null) shapeArea = 0;
            else shapeArea = isectPoly[0].Area;

            return shapeArea;
        }

        //==============================================================================================
        public double AdditionalDistFromOverlap(GenBuildingLayout fistGen, GenBuildingLayout secondGen)
        {
            double addDist = 0;
            RectangleF rect1, rect2;
            RectangleF isectRect;
            rect1 = new RectangleF((float)(fistGen.Points[0].X), (float)(fistGen.Points[0].Y), (float)(fistGen.Width), (float)(fistGen.Height));
            rect2 = new RectangleF((float)(secondGen.Points[0].X), (float)(secondGen.Points[0].Y), (float)(secondGen.Width), (float)(secondGen.Height));
            isectRect = RectangleF.Intersect(rect1, rect2);
            if (isectRect != null)
            {
                if (isectRect.Width < isectRect.Height) addDist = isectRect.Width;
                else addDist = isectRect.Height;
            }

            if (addDist <= RParameter.addDist) addDist = 0;
            else addDist = addDist - RParameter.addDist;
            return addDist;
        }

        //==============================================================================================
        public void EvaluationProportion()
        {
            GenBuildingLayout curGen;
            double ratio = 0, sum = 0;
            for (int k = 0; k < Gens.Count; k++)
            {
                curGen = Gens[k];
                if (curGen.Width <= curGen.Height)
                    ratio = curGen.Width / curGen.Height;
                else
                    ratio = curGen.Height / curGen.Width;
                sum += ratio;
            }
            ProportionRatio = sum / Gens.Count;
        }

        //==============================================================================================
        /// <summary>
        /// Mutation operator
        /// </summary>
        public override void Mutate()
        {
            double repForce = ControlParameters.repForce;  
            double repRate = ControlParameters.repRate;    
            double propForce = ControlParameters.propForce;   
            double propRate = ControlParameters.propRate; 

            // --- mutation operators ---
            int moveDist = (int)Math.Round((decimal)((BoundingRect.Height + BoundingRect.Width) / 8), 0); // NrBuildings), 0);

            double rate = 1.0 / MinNrBuildings;
            MoveRandon(rate/2, moveDist);
            VarySizeFull(rate / 4, moveDist);
            MoveRandon(rate, moveDist / 10);
            RotateRandon(rate / 2, moveDist / 10);
            VarySize(rate / 4);
            CheckBorder();
            CheckPolyBorder();
        }

        //==============================================================================================
        public override void Adapt()
        {
            double repForce = ControlParameters.repForce;
            double repRate = ControlParameters.repRate;
            double propForce = ControlParameters.propForce;
            double propRate = ControlParameters.propRate;
            double repAdapt = ControlParameters.RepeatAdapt;

            for (int i = 0; i < repAdapt; i++)
            {
                Adaption(propForce, propRate, repForce, repRate);
            }
            // the CheckPolyBorder method has a bug - therefore it is useful to check the result with CheckBorder...
            //this.CheckBorder();
            this.CheckPolyBorder();
            this.CheckBorder();
        }

        //==============================================================================================
        protected void Adaption(double propForce, double propRate, double repelForce, double repelRate)
        {
            bool propChange = ControlParameters.propChange;
            bool rotate = ControlParameters.rotate;
            double rotForce = ControlParameters.rotForce;
            double rotRate = ControlParameters.rotRate;
            bool borderRot = ControlParameters.borderRot;
            double borderRotForce = ControlParameters.borderRotForce;
            double borderRotRate = ControlParameters.borderRotRate;
            //------------------------------------------------------------

            // --- rotation ---
            if (rotate)
            {
                this.ChangeRotation(rotForce, rotRate);
                if (borderRot)
                    this.BorderRotation(borderRotForce, borderRotRate);
            }

            this.CheckBorder();
            this.CheckPolyBorder();
            // --- attractor ---
            this.AttractPolys(RParameter.attractForce, RParameter.attractRadius);

            // --- proportion ---
            if (propChange) this.ChangeProportion(propForce, propRate);

            // --- repel ---
            this.RepelPolys(repelForce, repelRate);
            //RepelJumpPolys(repelForce, repelRate);

        }

        //==============================================================================================
        /// <summary>
        /// Crossover operator
        /// </summary>
        public override void Crossover(IMultiChromosome pair)
        {
            CrossoverTopo(pair);
        }

        //==============================================================================================
        private void CrossoverTopo(IMultiChromosome pair)
        {
            ChromosomeLayout other = (ChromosomeLayout)pair;
            if (other != null)
            {
                //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                ChromosomeLayout tmpThis = new ChromosomeLayout(this); // temporary old parents
                ChromosomeLayout tmpOther = new ChromosomeLayout(other);
                int lengthParent0 = this.Gens.Count();
                int lengthParent1 = other.Gens.Count();

                //int nrPolys = MinNrBuildings; 
                int diffPolys = Math.Abs(lengthParent0 - lengthParent1);
                int diffPolysThis = RndGlobal.Rnd.Next(diffPolys + 1);
                int diffPolysOther = RndGlobal.Rnd.Next(diffPolys + 1);
                int maxNr = Math.Max(this.Gens.Count(), other.Gens.Count());
                int nrPolys = Math.Min(this.Gens.Count(), other.Gens.Count());
                this.Gens = new List<GenBuildingLayout>();
                other.Gens = new List<GenBuildingLayout>();
                //---------------------------------------------------------------
                // --- crossover for locked gens - they need to be maintained ---
                for (int i = 0; i < maxNr; i++)
                {

                    if (tmpThis.Gens.Count > tmpOther.Gens.Count)
                    {
                        if (tmpThis.Gens[i].Locked)
                        {
                            this.Gens.Add(new GenBuildingLayout(tmpThis.Gens[i]));
                            other.Gens.Add(new GenBuildingLayout(tmpThis.Gens[i]));
                        }
                    }
                    else
                    {
                        if (tmpOther.Gens[i].Locked)
                        {
                            this.Gens.Add(new GenBuildingLayout(tmpOther.Gens[i]));
                            other.Gens.Add(new GenBuildingLayout(tmpOther.Gens[i]));
                        }
                    }
                }

                // --- crossover --------------------------------------
                for (int i = 0; i < nrPolys; i++)
                {
                    // --- crossover for NOT locked gens ---
                    if (!tmpOther.Gens[i].Locked && !tmpThis.Gens[i].Locked)
                    {
                        // exchange gens randomly
                        if (RndGlobal.Rnd.NextDouble() < 0.5)
                        {
                            this.Gens.Add(new GenBuildingLayout(tmpOther.Gens[i]));
                            other.Gens.Add(new GenBuildingLayout(tmpThis.Gens[i]));
                        }
                        else
                        {
                            this.Gens.Add(new GenBuildingLayout(tmpThis.Gens[i]));
                            other.Gens.Add(new GenBuildingLayout(tmpOther.Gens[i]));
                        }
                    }
                }
                // --------------------------------
                // --- fill remaining positions ---
                // --- reverse direction randomly -> may be good to have a random selection for the gens of the longer chromosome
                bool forward = (RndGlobal.Rnd.NextDouble() < 0.5) ? false : true;
                int diff = 0;
                if (diffPolys > 0)
                {
                    ChromosomeLayout sourceChromo;
                    if (tmpThis.Gens.Count > tmpOther.Gens.Count)
                    {
                        sourceChromo = new ChromosomeLayout(tmpThis);
                    }
                    else  
                    {
                        sourceChromo = new ChromosomeLayout(tmpOther);
                    }

                    if (forward)
                    {
                        diff = nrPolys + diffPolysOther-1;
                        for (int i = nrPolys-1; i < diff; i++)
                        {
                            if (!sourceChromo.Gens[i].Locked)
                            {
                                other.Gens.Add(new GenBuildingLayout(sourceChromo.Gens[i]));
                            }
                        }
                        diff = nrPolys + diffPolysThis-1;
                        for (int i = nrPolys-1; i < diff; i++)
                        {
                            if (!sourceChromo.Gens[i].Locked)
                            {
                                this.Gens.Add(new GenBuildingLayout(sourceChromo.Gens[i]));
                            }
                        }
                    }
                    else
                    {
                        diff = sourceChromo.Gens.Count - 1 - diffPolysOther;
                        for (int i = sourceChromo.Gens.Count - 1; i > diff; i--)
                        {
                            if (!sourceChromo.Gens[i].Locked)
                            {
                                other.Gens.Add(new GenBuildingLayout(sourceChromo.Gens[i]));
                            }
                        }
                        diff = sourceChromo.Gens.Count - 1 - diffPolysThis;
                        for (int i = sourceChromo.Gens.Count - 1; i > diff; i--)
                        {
                            if (!sourceChromo.Gens[i].Locked)
                            {
                                this.Gens.Add(new GenBuildingLayout(sourceChromo.Gens[i]));
                            }
                        }
                    }

                }

                // -- assign new Gen-IDs --
                for (int i = 0; i < this.Gens.Count; i++)
                {
                    this.Gens[i].Id = i;
                }
                for (int i = 0; i < other.Gens.Count; i++)
                {
                    other.Gens[i].Id = i;
                }

                // --- keep cover ratio constant ---
                RemainDensity(this);
                RemainDensity(other);
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Remain the density of an area that is defined by the Density parameter.
        /// </summary>
        /// <param name="curLayout">The layout chromosome for which the density shall be checked.</param>
        public void RemainDensity(ChromosomeLayout curLayout)
        {
            double tolerance = (double)curLayout.Gens.Count();
            // -- measure actual density --
            double isDensity = 0;
            for (int i = 0; i < curLayout.Gens.Count(); i++)
            {
                GenBuildingLayout curGen = curLayout.Gens[i];
                isDensity += curGen.Poly.Area;
            }
            //double isRatio = isDensity / Border.Area;

            double shallArea = Border.Area * Density;
            double diffArea = shallArea - isDensity;
            ChromosomeLayout tempLayout = new ChromosomeLayout(curLayout);           
            // -- growth is needed -----
            int maxIterations = 20;
            int counter = 0;
            while (Math.Abs(diffArea) > tolerance && counter < maxIterations)
            {
                counter++;
                List<int> removeIdx = new List<int>();
                double perGen = diffArea / tempLayout.Gens.Count();
                // assign to temp gen
                for (int i = 0; i < tempLayout.Gens.Count(); i++)
                {
                    GenBuildingLayout tempGen = tempLayout.Gens[i];
                    GenBuildingLayout curGen = curLayout.Gens.Find(GenBuildingLayout => GenBuildingLayout.Id == tempGen.Id);
                    if (curGen.Locked)
                    {
                        removeIdx.Add(tempGen.Id);
                        continue;
                    }

                    bool setAreaFull = tempGen.SetArea(tempGen.Area += perGen); // test max/min side
                    double isDiff = tempGen.Area - curGen.Area;
                    curGen.Area = tempGen.Area;
                    diffArea -= isDiff;

                    if (! setAreaFull) // --- remove the Gen, which cannot be changed anymore.
                    {
                        removeIdx.Add(tempGen.Id);
                    }
                }
                foreach (int idx in removeIdx)
                {
                    GenBuildingLayout removeGen = tempLayout.Gens.Find(GenBuildingLayout => GenBuildingLayout.Id == idx);
                    tempLayout.Gens.Remove(removeGen);
                }
            }
        }

        # endregion

    }
}

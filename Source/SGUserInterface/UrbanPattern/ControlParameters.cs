﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPlan.Geometry;
using System.ComponentModel;
using CPlan.Optimization;
using CPlan.Evaluation;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public static class ControlParameters
    {
        # region Building Layout
        public static double repForce = 0.5;
        public static double repRate = 1;

        public static bool propChange = true;
        public static double propForce = 0.4;
        public static double propRate = 1;
        public static double rotForce = 0.25;
        public static double rotRate = 1;

        public static bool rotate = true;
        public static bool borderRot = true;
        public static double borderRotForce = 1.0;
        public static double borderRotRate = 1.0;

        public static double attractForce = 0.1;
        public static double attractRadius = 20;

       public static int MinNrBuildings = 10;
       public static int MaxNrBuildings = 20;
       public static double Density = 0.5;
       public static double MinSideRatio = 0.5;
       public static double MinArea = 200;
       public static double MaxArea = 0.2;
       public static double MinSide = 3;

       public static string Scenario = "";
       public static int RepeatAdapt = 30;
       public static int IsovistCellSize = 10;

        # endregion;

        # region StreetNetwork
        public static int popSize = 1;
        public static double pressVal = 2;
        public static double crossRate = 0.75;
        public static Boolean doCross = true;
        public static Boolean elite = false;
        public static Boolean plusSel = true;
        public static int TreeDepth = 7;

        public static bool Choice = true;
        public static bool Centrality = false;
        public static int AngleRange = 10;
        public static int LengthRange = 300;
        public static int MinLength = 110;
        public static int MaxChilds = 4;

        public static int LineWidth = 2;

        public static int density = 50;
        public static int roomsNr = 50;
        public static double thresH = roomsNr * 1.25;

        public static double sDamp = 0.5;
        public static int addDist = 50;

        public static int p = 2;

        public static bool jumps = true;
        public static bool instantUpdate = false;
        # endregion;

        public static string DisplayMeasure = "White";
        
    }

    [Serializable]
    public class ViewControlParameters
    {
        //==========================================================================================================
        [CategoryAttribute("Collission"), DescriptionAttribute("Force to repel the objects")]
        public double RepelForce
        {
            get { return ControlParameters.repForce; }
            set { ControlParameters.repForce = value; }
        }
        [CategoryAttribute("Collission"), DescriptionAttribute("Repel rate")]
        public double RepelRate
        {
            get { return ControlParameters.repRate; }
            set { ControlParameters.repRate = value; }
        }

        [CategoryAttribute("Collission"), DescriptionAttribute("Change proportion")]
        public bool PropChange
        {
            get { return ControlParameters.propChange; }
            set { ControlParameters.propChange = value; }
        }

        [CategoryAttribute("Collission"), DescriptionAttribute("Force to change the proportion")]
        public double PropForce
        {
            get { return ControlParameters.propForce; }
            set { ControlParameters.propForce = value; }
        }
        [CategoryAttribute("Collission"), DescriptionAttribute("Proportion change rate")]
        public double PropRate
        {
            get { return ControlParameters.propRate; }
            set { ControlParameters.propRate = value; }
        }

        [CategoryAttribute("Collission"), DescriptionAttribute("Rotation")]
        public bool Rotate
        {
            get { return ControlParameters.rotate; }
            set { ControlParameters.rotate = value; }
        }
        [CategoryAttribute("Collission"), DescriptionAttribute("Force for rotation")]
        public double RotForce
        {
            get { return ControlParameters.rotForce; }
            set { ControlParameters.rotForce = value; }
        }
        [CategoryAttribute("Collission"), DescriptionAttribute("Rotation rate")]
        public double RotRate
        {
            get { return ControlParameters.rotRate; }
            set { ControlParameters.rotRate = value; }
        }

        [CategoryAttribute("Collission"), DescriptionAttribute("Rotation by border collission")]
        public bool BorderRot
        {
            get { return ControlParameters.borderRot; }
            set { ControlParameters.borderRot = value; }
        }
        [CategoryAttribute("Collission"), DescriptionAttribute("Force for rotation by border collission")]
        public double BorderRotForce
        {
            get { return ControlParameters.borderRotForce; }
            set { ControlParameters.borderRotForce = value; }
        }
        [CategoryAttribute("Collission"), DescriptionAttribute("Rotation rate by border collission")]
        public double BorderRotRate
        {
            get { return ControlParameters.borderRotRate; }
            set { ControlParameters.borderRotRate = value; }
        }

        [CategoryAttribute("Collission"), DescriptionAttribute("Force for rotation")]
        public double AttractForce
        {
            get { return ControlParameters.attractForce; }
            set { ControlParameters.attractForce = value; }
        }
        [CategoryAttribute("Collission"), DescriptionAttribute("Rotation rate")]
        public double AttractRadius
        {
            get { return ControlParameters.attractRadius; }
            set { ControlParameters.attractRadius = value; }
        }

        //==========================================================================================================
        [CategoryAttribute("Layout"), DescriptionAttribute("Building desnity in %")]
        public int Density
        {
            get { return ControlParameters.density; }
            set { ControlParameters.density = value; }
        }
        [CategoryAttribute("Layout"), DescriptionAttribute("Number of houses")]
        public int Buildings_Nr
        {
            get { return ControlParameters.roomsNr; }
            set { ControlParameters.roomsNr = value; }
        }
        [CategoryAttribute("Layout"), DescriptionAttribute("Update Isovist field after moving a object")]
        public bool UpdateIsovist
        {
            get { return ControlParameters.instantUpdate; }
            set { ControlParameters.instantUpdate = value; }
        }

        //==========================================================================================================
        [CategoryAttribute("Optimization"), DescriptionAttribute("Allow jumping objects")]
        public bool Jumping
        {
            get { return ControlParameters.jumps; }
            set { ControlParameters.jumps = value; }
        }

        [CategoryAttribute("Optimization"), DescriptionAttribute("Population size")]
        public int PopSize
        {
            get { return ControlParameters.popSize; }
            set { ControlParameters.popSize = value; }
        }
        [CategoryAttribute("Optimization"), DescriptionAttribute("Population preasure")]
        public double Pressure
        {
            get { return ControlParameters.pressVal; }
            set { ControlParameters.pressVal = value; }
        }
        [CategoryAttribute("Optimization"), DescriptionAttribute("Crossover rate")]
        public double CrossoverRate
        {
            get { return ControlParameters.crossRate; }
            set { ControlParameters.crossRate = value; }
        }
        [CategoryAttribute("Optimization"), DescriptionAttribute("Activate crossover")]
        public Boolean Crossover
        {
            get { return ControlParameters.doCross; }
            set { ControlParameters.doCross = value; }
        }
        [CategoryAttribute("Optimization"), DescriptionAttribute("Number of parents for crossover")]
        public int pCross
        {
            get { return ControlParameters.p; }
            set { ControlParameters.p = value; }
        }

    }
}

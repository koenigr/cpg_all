﻿// FitnessFunctLayout.cs
//----------------------------------
// Last modified on 09/04/2014
// by Dr. Reinhard König
//
// Description:
//  
//
// Comments:
// 
//
// To Dos:
// 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AForge.Genetic;
using CPlan.Optimization;
using CPlan.Geometry;
using CPlan.Evaluation;
using System.Diagnostics;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public class FitnessFunctionLayout : IMultiFitnessFunction
    {

        private int _dimensions = 5;
        public int Dimensions { get{return _dimensions;} }
        public CalculationType.Method RunVia { get; set; }

        public FitnessFunctionLayout()
        {
            RunVia = CalculationType.Method.Local;
        }

        public FitnessFunctionLayout(CalculationType.Method runVia)
        {
            RunVia = runVia;
        }


        List<double> IMultiFitnessFunction.Evaluate(IMultiChromosome chromosome)
        {
            List<double> fitnessValues = new List<double>();
            ChromosomeLayout curChromo = (ChromosomeLayout)chromosome;
            
            //------------------------------------------------------//
            // --- minimization ??? ---
            // -- add above number of _dimensions!
            fitnessValues.Add(Math.Abs(curChromo.Overlap()));

            //if (RunVia == CalculationMethod.Local)
                IsovistAnalysis(curChromo);
          
            fitnessValues.Add((1 / curChromo.IsovistField.Area.Where(n => (!double.IsNaN( n) && n >= 0)).Max()));
            
            fitnessValues.Add((curChromo.IsovistField.Occlusivity.Where(n => (!double.IsNaN( n) && n >= 0)).Average()));
            fitnessValues.Add((1 / curChromo.IsovistField.Compactness.Where(n => (!double.IsNaN( n) && n >= 0)).Average()));
            fitnessValues.Add((1 / curChromo.IsovistField.MinRadial.Where(n => (!double.IsNaN( n) && n >= 0)).Average()));

            return fitnessValues;

            //fitnessValues.Add((1/ curChromo.IsovistField.AreaCleaned.Max())); //
            ////fitnessValues.Add(Math.Abs(1/ curChromo.IsovistField.Area.Mean)); //1 / 
            //fitnessValues.Add(( curChromo.IsovistField.Occlusivity.Average() ));
            //fitnessValues.Add((1 / curChromo.IsovistField.Compactness.Average()));
            //fitnessValues.Add((1 / curChromo.IsovistField.MinRadial.Average()));
        }


        //============================================================================================================================================================================
        private void IsovistAnalysis(ChromosomeLayout curChromo)
        {
            List<Line2D> obstLines = new List<Line2D>();
            List<Poly2D> obstPolys = new List<Poly2D>();
            if (curChromo != null)
            {
                IsovistAnalysis isovistField;
                Poly2D isovistBorder = curChromo.Border;
                Rect2D borderRect = GeoAlgorithms2D.BoundingBox(isovistBorder.PointsFirstLoop);
                List<GenBuildingLayout> buildings = curChromo.Gens;

                // test environmental polygons
                if (curChromo.Environment != null)
                    foreach (Poly2D envPoly in curChromo.Environment)
                    {
                        if (envPoly.Distance(isovistBorder) <= 0)
                        {
                            obstLines.AddRange(envPoly.Edges.ToList());
                            obstPolys.Add(envPoly);
                        }
                    }

                // add the buildings
                foreach (GenBuildingLayout curBuilding in buildings)
                {
                    if (!curBuilding.Open)
                    {
                        obstLines.AddRange(curBuilding.Poly.Edges.ToList());
                        obstPolys.Add(curBuilding.Poly);
                    }
                }

                obstLines.AddRange(isovistBorder.Edges);
                //List<Vector2D> gridPoints = IsovistAnalysis.PointsForLucyIsovist(_curLayout.Border, _curLayout.Gens, cellSize);
                float cellSize = ControlParameters.IsovistCellSize;
                isovistField = new IsovistAnalysis(isovistBorder, obstPolys, cellSize);

                //_isovistField.CutBorder(_isovistBorder); // consider only the area inside the border polygon
                TimeSpan calcTime1, calcTime2;
                DateTime start = DateTime.Now;

                // -- run local --------------------------------------------------------------------------------
                isovistField.Calculate(0.05f);

                DateTime end = DateTime.Now;
                calcTime1 = end - start;

                // -- run via server --------------------------------------------------------------------------- 
                // approximately 10 times! slower on local maschine...???
                //start = DateTime.Now;
                //isovistField.CalculateViaLucy();

                //end = DateTime.Now; ;
                //calcTime2 = end - start;

                //System.Diagnostics.Trace.WriteLine("grid with " + isovistField.AnalysisPoints.Count().ToString() + " analysis points: ");
                //System.Diagnostics.Trace.WriteLine("local calculation time 1: " + calcTime1.TotalMilliseconds.ToString() + " ms");
                //System.Diagnostics.Trace.WriteLine("server calculation time 2: " + calcTime2.TotalMilliseconds.ToString() + " ms");

                //isovistField.WriteToGridValues();
                curChromo.IsovistField = isovistField;
                //propertyGrid2.SelectedObject = isovistField;

            }           
        }
    }
}


﻿// FitnessFunctionLayoutSingle.cs
//----------------------------------
// Last modified on 22/04/2014
// by Dr. Reinhard König
//
// Description:
// 
//
// Comments:
// 
//


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AForge.Genetic;
using CPlan.Optimization;
using CPlan.Geometry;
using CPlan.Evaluation;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public class FitnessFunctionLayoutSingle : IFitnessFunction
    {
        /// <summary>
        /// Evaluates the fitness of a variant.
        /// For Single criteria optimization we use maximization!
        /// </summary>
        /// <param name="chromosome"> Variant to be evaluated.</param>
        /// <returns>The fitness value.</returns>
        double IFitnessFunction.Evaluate(IChromosome chromosome)
        {

            double fitnessValue = -123;// double.MinValue;
            ChromosomeLayout curChromo = (ChromosomeLayout)chromosome;
            IsovistAnalysis(curChromo);
            fitnessValue = (curChromo.IsovistField.Area.Max());
            return fitnessValue;
        }


        //============================================================================================================================================================================
        private void IsovistAnalysis(ChromosomeLayout curChromo)
        {
            List<Line2D> obstLines = new List<Line2D>();
            List<Poly2D> obstPolys = new List<Poly2D>();
            if (curChromo != null)
            {
                IsovistAnalysis isovistField;
                Poly2D isovistBorder = curChromo.Border;
                Rect2D borderRect = GeoAlgorithms2D.BoundingBox(isovistBorder.PointsFirstLoop);
                List<GenBuildingLayout> buildings = curChromo.Gens;

                // test environmental polygons
                if (curChromo.Environment != null)
                    foreach (Poly2D envPoly in curChromo.Environment)
                    {
                        if (envPoly.Distance(isovistBorder) <= 0)
                        {
                            obstLines.AddRange(envPoly.Edges.ToList());
                            obstPolys.Add(envPoly);
                        }
                    }

                // add the buildings
                foreach (GenBuildingLayout curBuilding in buildings)
                {
                    //if (curRect.Filled)
                    {
                        obstLines.AddRange(curBuilding.Poly.Edges.ToList());
                        obstPolys.Add(curBuilding.Poly);
                    }
                }

                obstLines.AddRange(isovistBorder.Edges);
                float cellSize = 10;

                isovistField = new IsovistAnalysis(isovistBorder, obstPolys, cellSize);

                TimeSpan calcTime1;
                DateTime start = DateTime.Now;

                // -- run local --
                isovistField.Calculate(0.05f);

                DateTime end = DateTime.Now;
                calcTime1 = end - start;

                //isovistField.WriteToGridValues();
                curChromo.IsovistField = isovistField;

            }

        }

    }
}

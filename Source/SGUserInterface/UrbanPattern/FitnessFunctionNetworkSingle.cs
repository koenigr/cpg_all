﻿// FitnessFunctionNetworkSingle.cs
//----------------------------------
// Last modified on 12/04/2013
// by Dr. Reinhard König
//
// Description:
// The Tree class describes a instruction tree. Its nodes contain all information to grow a street graph. 
// This class contains all necessary methods to build and alter such a tree.
//
// Comments:
// 
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AForge.Genetic;
using CPlan.Geometry;
using CPlan.Evaluation;
using Tektosyne.Geometry;
using FW_GPU_NET;
using OpenTK;
using System.Windows.Forms;
using System.Drawing;
using QuickGraph;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public class FitnessFunctionNetworkSingle : IFitnessFunction
    {

        double IFitnessFunction.Evaluate(IChromosome chromosome)
        {
            double fitnessValue = -123;// double.MinValue;
            InstructionTree curChromo = (InstructionTree)chromosome;
            curChromo.Network = null;
            curChromo.Network = curChromo.CreateStreetNetwork();
            //------------------------------------------------------//

            FW_GPU_NET.Graph FWInverseGraph;
            ShortestPath ShortPahtesAngular;
            //Subdivision InverseGraph;
            //Dictionary<Vector2D, Line2D> inverseEdgesMapping = new Dictionary<Vector2D, Line2D>();
            //Dictionary<Line2D, double> realDist = new Dictionary<Line2D, double>();
            //Dictionary<Line2D, double> realAngle = new Dictionary<Line2D, double>();

            //InverseGraph = RndGlobal.ConstructInverseGraph(StreetGenerator.Graph, ref RealDist, ref RealAngle, ref InverseEdgesMapping);
            //InverseGraph = GraphTools.ConstructInverseGraph(curChromo.Network, ref realDist, ref realAngle, ref inverseEdgesMapping);

            FW_GPU_NET.Graph fwGraph = GraphTools.GenerateFWGpuGraph(curChromo.Network);
            FWInverseGraph = GraphTools.GenerateInverseDoubleGraph(fwGraph);

            ShortPahtesAngular = new ShortestPath(curChromo.Network);//, realAngle);
            ShortPahtesAngular.Evaluate(new System.Windows.Forms.ToolStripProgressBar(), fwGraph, FWInverseGraph);

            float maxValue = 0, sumQuiet = 0;
            int edgeIdx = -1;
            //float[] meanChoiceArr = ShortPahtesAngular.GetNormChoiceArray();
            float[] ValueArr = null;
            if (ControlParameters.Choice) ValueArr = ShortPahtesAngular.GetChoice()[0];
            if (ControlParameters.Centrality) ValueArr = ShortPahtesAngular.GetCentralityMetric()[0];

            double sumValue = 0, meanValue = 0;
            double dist;

            if (!ShortPahtesAngular.Connected) // something is wrong with the graph - probably unconnected.
            {
                //return null;
                maxValue = 0.00001f;
                sumQuiet = Single.MaxValue / 2;
                dist = Single.MaxValue / 2;
            }
            else   // -- if graph is connected, calculate the fitness values ---
            {
                for (int i = 0; i < ValueArr.Count(); i++)
                {
                    float value = ValueArr[i];
                    sumValue += value;
                    if (maxValue < value)
                    {
                        maxValue = value;
                        edgeIdx = i;
                    }
                }

                if (ValueArr.Length > 0)
                    meanValue = sumValue / (ValueArr.Length); // -1
                else meanValue = 0;
                curChromo.ShortestPathes = ShortPahtesAngular;

                List<LineD> qLines = new List<LineD>();
                foreach (UndirectedEdge<Vector2D> edge in curChromo.Network.Edges) qLines.Add(new LineD(edge.Source.ToPointD(), edge.Target.ToPointD()));
                // -- include spatial relations --
                double maxDist = Math.Sqrt(Math.Pow(curChromo.Border.Height, 2) + Math.Pow(curChromo.Border.Width, 2));
                LineD bestEdge = qLines[edgeIdx]; //origEdges[edgeIdx];
                //LineD bestEdge = curChromo.Network.ToLines().ToList()[edgeIdx];

                // distance of the "max value segment" to a defined point:
                Vector2D centralPoint = new Vector2D(curChromo.Border.Left + 75, curChromo.Border.Top - 50);
                dist = GeoAlgorithms2D.DistancePointToLine(centralPoint, bestEdge.Start.ToVector2D(), bestEdge.End.ToVector2D(), new Vector2D());
                float weight = Convert.ToSingle(Math.Pow(1 - (dist / maxDist), 2));
                maxValue *= weight;

                // --- inside the rectangle? ---
                RectD quietArea = new RectD(150, 0, 150, 90); // the quiet area
                LineD[] streets = qLines.ToArray();
                //LineD[] streets = curChromo.Network.ToLines();

                int counter = 0;
                for (int i = 0; i < streets.Count(); i++)
                {
                    LineD segment = streets[i];
                    if (quietArea.Contains(segment.Start) || quietArea.Contains(segment.End))
                    {
                        //maxValue -= ValueArr[i];
                        //sumQuiet += maxValue - ValueArr[i];
                        sumQuiet += ValueArr[i];
                        counter++;
                    }
                }
                sumQuiet = sumQuiet / (counter * 1);
                maxValue -= sumQuiet;
                //fitnessValue = (1 / maxValue);// + (1 - 1/sumQuiet));
                fitnessValue = maxValue;
            }

            //return maxValue; //sumValue;// meanValue;// maxValue;//1 - (dist / maxDist);//  *ChoiceArr.Length;//1 - meanChoice;//meanChoice;//

            //fitnessValues.Add(dist);
            //fitnessValues.Add(sumQuiet);

            fwGraph.ClearGraph();
            fwGraph.Dispose();
            fwGraph = null;
            FWInverseGraph.Dispose();
            FWInverseGraph = null;
            ShortPahtesAngular = null;

            return fitnessValue;

        }

    }
}

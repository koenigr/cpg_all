﻿// GenBuildingLayout.cs
//----------------------------------
// Last modified on 08/04/2014
// by Dr. Reinhard König
//
// Description:
// The Node class describes a gene and its properties. 
// It is used for the ChromosomeLayout and represents an individual building.
//
// Comments:
// 
//
// To Dos:
// 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Drawing2D;
//using System.Drawing;
using CPlan.Geometry;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public class GenBuildingLayout
    {

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Private Fields

        private Rect2D _rect;
        private double _rotation;
        private double _ratio;
        public int Id;

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        //public bool Free { get; set; }
        public double MinSideRatio
        {
            get { return _ratio; }
            set
            {
                _ratio = value; 
                MinSide = Math.Sqrt(Area) * MinSideRatio;
                if (MinSide < 10) MinSide = 10; // ** only temporary - needs to be assigned to a property!!!
                MaxSide = Area / MinSide;
            }
        }// Math.Sqrt(fixArea) * 0.75;// 10.0;
        public double MinSide { get; protected set; }
        public double MaxSide { get; protected set; }
        //public bool Attractor { get; set; }
        public bool Locked { get; set; }

        //==============================================================================================
        public List<Vector2D> Points
        {
            get { return Poly.PointsFirstLoop.ToList(); }
        }

        //==============================================================================================
        public Poly2D Poly
        {
            get
            {
                Poly2D curPoly = new Poly2D(_rect.Points);
                curPoly.Rotate(GeoAlgorithms2D.DegreeToRadian(Rotation));
                return curPoly;
            }
            //set { _geometry = value; }
        }

        //==============================================================================================
        public Vector2D Position
        {
            get { return _rect.Position; }
            set { _rect.Position = value; }
        }

        //==============================================================================================
        public double Rotation
        {
            get { return _rotation; }
            set
            {
                double divider = Math.Ceiling(value / 180);
                if (divider > 1)
                    _rotation = value - ((divider - 1) * 180);
                else _rotation = value;
            }
        }

        //==============================================================================================
        public double Area
        {
            get
            {
                return _rect.Area;
            }
            set
            {
                double proport = 1;
                if (Height > 0) proport = Width / Height;

                double newWidth = Math.Sqrt(proport * value);
                double newHeith = Math.Sqrt(value / proport);
                //Vector2D deltaSize = new Vector2D(-(newWidth - Width), newHeith - Height);
                if (newWidth > MinSide && newHeith > MinSide && newWidth < MaxSide && newHeith < MaxSide)
                {
                    Width = newWidth;
                    Height = newHeith;
                    //Position += deltaSize / 2;
                }
                else
                    CheckWidtHeight(newWidth);
            }
        }
        //==============================================================================================
        public bool SetArea(double newArea)
        {
            bool setFull = true;
            double proport = 1;
            if (Height > 0) proport = Width / Height;

            double newWidth = Math.Sqrt(proport * newArea);
            double newHeith = Math.Sqrt(newArea / proport);
            //Vector2D deltaSize = new Vector2D(-(newWidth - Width), newHeith - Height);
            if (newWidth > MinSide && newHeith > MinSide && newWidth < MaxSide && newHeith < MaxSide)
            {
                Width = newWidth;
                Height = newHeith;
                //Position += deltaSize / 2;
            }
            else
                setFull = CheckWidtHeight(newWidth);

            return setFull;
        }

        //==============================================================================================
        public double Width
        {
            get
            {

                return _rect.Width;
            }
            set
            {
                _rect.Width = value;
            }
        }

        //==============================================================================================
        public double Height
        {
            get
            {
                return _rect.Height;
            }
            set
            {
                _rect.Height = value;
            }
        }

        //==============================================================================================
        public double Left
        {
            get
            {
                return Poly.BoundingBox().Left;
            }
        }

        //==============================================================================================
        public double Top
        {
            get
            {
                return Poly.BoundingBox().Top;
            }
        }

        //==============================================================================================
        public Vector2D Center
        {
            get
            {
                return _rect.Center;
            }
            set
            {
                Vector2D difference = (value - Center);
                _rect.Move(difference);
            }
        }

        private Rect2D BoundingRect
        {
            get;
            set;
        }

        //==============================================================================================
        public bool Open
        {
            get;
            set;
        }

        //==============================================================================================
        public bool Attractor 
        { 
            get; set; 
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
        # region Constructors

        //==============================================================================================
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="iniR"></param>
        /// <param name="vId"></param>
        public GenBuildingLayout(double x, double y, Rect2D boudRect, double area, double minSideRatio, int vId)
        {
            double width, height;
            Vector2D Velocity = new Vector2D(0, 0);
            Id = vId;
            Vector2D centre = new Vector2D(x, y);
            //bRect = GeoAlgorithms2D.BoundingBox(border.Points);
            width = height = Math.Sqrt(area); //40;// RndGlobal.Rnd.Next(40, 50);// 30;
            Rotation = RndGlobal.Rnd.Next(0, 360);
            _rect = new Rect2D(centre.X - width / 2, centre.Y - height / 2, width, height);
            MinSideRatio = minSideRatio;
            BoundingRect = boudRect;

        }

        //==============================================================================================
        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="sourceGen"></param>
        public GenBuildingLayout(GenBuildingLayout sourceGen)
        {
            Vector2D Velocity = new Vector2D(0, 0);
            _rect = sourceGen._rect.Clone();
            Rotation = sourceGen.Rotation;
            MinSideRatio = sourceGen.MinSideRatio;
            MaxSide = sourceGen.MaxSide;
            MinSide = sourceGen.MinSide;
            Attractor = sourceGen.Attractor;

            //_geometry = sourceGen._geometry.Clone();
            Id = sourceGen.Id;
            Locked = sourceGen.Locked;
            BoundingRect = sourceGen.BoundingRect;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        /// <summary>
        /// Clone the building Gene.
        /// </summary>
        /// <returns>Returns exact clone of the Gene.</returns>
        public object Clone()
        {
            GenBuildingLayout clone = new GenBuildingLayout(this);
            return clone;
        }

        //==============================================================================================
        /// <summary>
        /// Move the Gen.
        /// </summary>
        /// <param name="v"> Vector by which the Gen is moved. </param>
        public void Move(Vector2D v)
        {
            _rect.Move(v);
        }

        //==============================================================================================
        /// <summary>
        /// Creates a roted minimal bounding rectangle. 
        /// </summary>
        /// <returns>Bounding rectangle.</returns>
        private Rect2D ApproxRect()
        {
            List<Vector2D> minBR = GeoAlgorithms2D.MinimalBoundingRectangle(Poly.PointsFirstLoop.ToList()).ToList();
            Vector2D vecA = minBR[1] - minBR[0];
            Vector2D vecB = new Vector2D(1, 0);
            double rotation = -(GeoAlgorithms2D.AngleBetweenR(vecA, vecB));
            if (double.IsNaN(rotation))
            {
                Rect2D approxRect = new Rect2D(Points[0], Points[2]);
                return approxRect;
            }
            else
            {
                Poly2D rotatedGeom = new Poly2D(GeoAlgorithms2D.RotatePoly(rotation, minBR[0], Poly.Points));
                Rect2D approxRect = GeoAlgorithms2D.BoundingBox(rotatedGeom.PointsFirstLoop);
                return approxRect;
            }

        }

        //==============================================================================================
        //public void CheckHeightWidt(double m_height)
        //{
        //    double min = 10.0;
        //    double dH = 1;// canvas.Height / 4;
        //    double dW = 1;// canvas.Width / 4;
        //    double m_width;

        //    if (m_height < min) m_height = min;                                            //-- Minimale Scalierung X
        //    else if (m_height > REvo.Canvas.Height - dW) m_height = REvo.Canvas.Height - dW; //-- Maximale Scalierung X
        //    m_width = (this.Area / m_height);
        //    this.Width = m_width;
        //    this.Height = m_height;
        //    if (this.Width  < min)
        //    {
        //        this.Width  = min;
        //        this.Height = (this.Area / this.Width );
        //    }
        //    if (this.Width > REvo.Canvas.Width - dH)
        //    {
        //        this.Width = REvo.Canvas.Width - dH;
        //        this.Height = (this.Area / this.Width);
        //    }
        //}

        //==============================================================================================
        public bool CheckWidtHeight(double newSide)
        {
            bool changedFull = true; // indicates if the requrired size change was executed completely.
            double fixArea = this.Area;
            double dH = 0;// canvas.Height / 4;
            double dW = 0;// canvas.Width / 4;
            double m_height;
            double limboHeight = this.Height;
            double limboWidth = this.Width;

            if (newSide < MinSide)         //-- Minimale Scalierung X
            {
                newSide = MinSide;                                      
                changedFull = false;
            }
            //else if (newSide > BoundingRect.Width - dW) newSide = BoundingRect.Width - dW; //-- Maximale Scalierung X
            else if (newSide > MaxSide)    //-- Maximale Scalierung X
            {
                newSide = MaxSide;
                changedFull = false;
            }
            m_height = (fixArea / newSide);
            limboHeight = m_height;
            limboWidth = newSide;
            if (limboHeight < MinSide)
            {
                limboHeight = MinSide;
                limboWidth = (fixArea / limboHeight);
                changedFull = false;
            }
            if (limboHeight > MaxSide)
            {
                limboHeight = MaxSide;
                limboWidth = (fixArea / limboHeight);
                changedFull = false;
            }
            // execute new sizes
            if (limboHeight * limboWidth == fixArea)
            {
                double diffHeight = this.Height - limboHeight;
                double diffWidth = this.Width - limboWidth;
                this.Height = limboHeight;// -diffHeight / 2;
                this.Width = limboWidth;// -diffWidth / 2;
                this.Move(new Vector2D(diffWidth / 2, -diffHeight / 2));
            }
            return changedFull;
        }

        //==============================================================================================
        /// <summary>
        /// Check the rotation - width, height assignement
        /// </summary>
        /// <returns>Returns false if width and height needs to be considert inverse. </returns>
        public bool CheckRot()
        {
            double tester = Math.Round(Rotation / 90, 0);
            if (tester % 2 == 0) return true; // true, wenn Zahl gerade.
            else return false;
        }

        //==============================================================================================
        public Poly2D ToPoly2D()
        {
            return new Poly2D(Poly.PointsFirstLoop.ToArray());
        }

        # endregion


    }
}

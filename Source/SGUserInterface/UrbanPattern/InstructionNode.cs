﻿// GrowthNode.cs
//----------------------------------
// Last modified on 03/04/2013
// by Dr. Reinhard König
//
// Description:
// The Node class describes a node and its properties in a instruction tree, which describes the growth/development of a street network.
//
// Comments:
// 
//
// To Dos:
// 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tektosyne.Geometry;
//using AForge.Genetic;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public class InstructionNode //: GPTreeNode
    {

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        /// <summary>
        /// Gets or sets the link to the parent node
        /// </summary>
        public Int64 ParentIndex
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the attribute index.
        /// </summary>
        public Int64 Index
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the position/coordinates of the node.
        /// </summary>
        public PointD Position
        {
            get;
            set;
        }

        /// <summary>
        /// Indicates if the further growth of a node is closed, if considered as parent.
        /// </summary>
        public bool Closed
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the attribute for angle.
        /// </summary>
        /// <value>(float) optimal area value.</value>
        public float Angle
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the attribute for length.
        /// </summary>
        public float Length
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value for maximum connectivity - the maximum number of edges to or from a node.
        /// </summary>
        public float MaxConnectivity
        {
            get;
            set;
        }

        /// <summary>
        /// Stores a random decision during the street growth for reproduction.
        /// </summary>
        public int Divider
        {
            get;
            set;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        # region Constructors

        /// <summary>
        /// Initializes a new instance of the Node type with a specified index.
        /// </summary>
        /// <param name="idx">Index of the node.</param>
        public InstructionNode(int idx)
        {
            //this.Children = new List<InstructionNode>();
            this.Index = idx;
            this.Closed = false;
            this.Divider = -1;
        }

        /// <summary>
        /// Initializes a new instance of the Node type with a specified index.
        /// </summary>
        /// <param name="idx">Index of the node.</param>
        /// <param name="angle">Angle deviation from regular division.</param>
        /// <param name="length">Length of the new street segment.</param>
        public InstructionNode(int idx, float angle, float length)
        {
            //this.Children = new List<InstructionNode>();
            this.Index = idx;
            this.Angle = angle;
            this.Length = length;
            this.Closed = false;
            this.Divider = -1;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="idx">Index of the node.</param>
        public InstructionNode(InstructionNode source)
        {
            this.Index = source.Index;
            this.Position = new PointD(source.Position.X, source.Position.Y);
            this.Angle = source.Angle;
            this.Length = source.Length;
            this.Closed = source.Closed;
            this.MaxConnectivity = source.MaxConnectivity;
            this.ParentIndex = source.ParentIndex;
            this.Divider = source.Divider;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        /// <summary>
        /// Clone the tree node.
        /// </summary>
        /// <returns>Returns exact clone of the node.</returns>
        public object Clone()
        {
            InstructionNode clone = new InstructionNode(this);
            return clone;
        }

        # endregion

    }
}

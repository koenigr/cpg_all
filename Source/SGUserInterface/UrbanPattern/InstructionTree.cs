﻿// InstructionTree.cs
//----------------------------------
// Last modified on 03/04/2013
// by Dr. Reinhard König
//
// Description:
// The Tree class describes a instruction tree. Its nodes contain all information to grow a street graph. 
// This class contains all necessary methods to build and alter such a tree.
//
// Comments:
// 
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QuickGraph;
using AForge;
using AForge.Genetic;
using CPlan.Geometry;
using CPlan.Evaluation;
using CPlan.Optimization;
using Tektosyne.Geometry;
using System.Drawing;

//using System.Diagnostics;

using System.Windows.Forms;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public class InstructionTree : IMultiChromosome
    {

        protected List<double> m_fitness_values = new List<double>();

        //double IMultiChromosome.FitnessSummed
        //{
        //    get { throw new NotImplementedException(); }
        //}

        /// <summary>
        /// A list of fitness values for each criteria.
        /// </summary>
        public List<double> FitnessValues
        {
            get { return this.m_fitness_values; }
        }

        /// <summary>
        /// The generation or iteration in which the chromosome was created.
        /// </summary>
        public int Generation
        {
            get;
            set;
        }


        /// <summary>
        /// Evaluate chromosome with specified fitness function.
        /// </summary>
        /// <param name="function">Fitness function to use for evaluation of the chromosome.</param>
        /// <remarks><para>Calculates chromosome's fitness values using the specifed fitness function.</para></remarks>
        public void Evaluate(IMultiFitnessFunction function)
        {
            m_fitness_values = function.Evaluate(this);
        }


        public Vector2D Origin
        {
            get;
            set;
        }

        public ShortestPath ShortestPathes
        {
            get;
            set;
        }

        public UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> InitalNetwork
        {
            get;
            set;
        }

        public  UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> Network//Subdivision Network  
        {
            get;
            set;
        }

        public Rect2D Border
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the number of nodes of a InstructionTree object.
        /// </summary>
        /// <value>(int) number of rooms.</value>
        public int TreeDepth
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the list of nodes of a InstructionTree object.
        /// </summary>
        /// <value>(List Node) leaves.</value>
        public List<InstructionNode> Nodes
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the highest index from all InstructionNodes
        /// </summary>
        public Int64 HighestIdx
        {
            get
            {
                Int64 hIdx = 0;
                foreach (InstructionNode node in this.Nodes)
                {
                    if (node.Index > hIdx) hIdx = node.Index;
                }
                return hIdx;
            }
            //private set;
        }

        /// <summary>
        /// Random generator used for chromosoms' generation.
        /// </summary>
        protected static ThreadSafeRandom rand = new ThreadSafeRandom();

        //Random rnd = new Random();
        private int _angleRange = 10;
        private int _lengthRange = 30;
        private int _minLength = 11;
        private int _maxChilds = 4;

        public int MaxSize { get; private set; }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Initializes a new instance of the InstructionTree type using the specified number of rooms.
        /// </summary>
        /// <param name="_rooms">(int) number of rooms.</param>
        public InstructionTree(int treeDepth, UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> iniNet, Rect2D border)// : base(treeDepth)
        {
            this.Border = border;
            this.TreeDepth = treeDepth;
            this.InitalNetwork = iniNet;
            this.Initialize();
        }

        /// <summary>
		/// Copy Constructor.
		/// </summary>
        protected InstructionTree(InstructionTree source)//  : base(source)
		{
            this.Nodes = new List<InstructionNode>();
            foreach (InstructionNode oldNode in source.Nodes)
            {
                InstructionNode newNode = new InstructionNode(oldNode);
                this.Nodes.Add(newNode);
            }
            this.Border = new Rect2D(source.Border);
            this.Origin = new Vector2D(source.Origin);
            this.MaxSize = source.MaxSize;
            this.ShortestPathes = source.ShortestPathes;
            this.Network = (UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>>)source.Network;   //.Clone(); // (Subdivision)source.Network;//.Clone();
            this.InitalNetwork = (UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>>)source.InitalNetwork;//.Clone();
            this.TreeDepth = source.TreeDepth;
            this._angleRange = source._angleRange;
            this._lengthRange = source._lengthRange;
            this._minLength = source._minLength;
            this._maxChilds = source._maxChilds;

            //if (this.m_fitness_values.Count > 0)
            {
                this.m_fitness_values = new List<double>(source.FitnessValues.Count);
                foreach (double value in source.FitnessValues)
                {
                    m_fitness_values.Add(value);
                }
            }
		}

        # region Initialization

        /// <summary>
        /// Initializes the Tree.
        /// </summary>
        public void Initialize()
        {
            Nodes = new List<InstructionNode>();
            //MaxSize = Convert.ToInt32((TreeDepth*1.4) * (lengthRange + minLength));
            ////Border = new Rect2D(-1000.0, -1000.0, 2000.0, 2000.0); // border for the network
            //Border = new Rect2D(0.0, 0.0, MaxSize, MaxSize); // border for the network

            Origin = new Vector2D(0, 0); // global origin of coordinates for the network

            // --- add at least two seed nodes to create at least one seed edge
            InstructionNode seedNode;
            List<InstructionNode> parentNodes = new List<InstructionNode>();
            int idx = 0;
            foreach (Vector2D node in InitalNetwork.Vertices)
            {
                if (TestSeedNode(node))
                {
                    seedNode = new InstructionNode(idx);
                    idx++;
                    Vector2D pt = new Vector2D(node.X, node.Y);
                    seedNode.Position = pt.ToPointD();
                    float angle, length;
                    GeoAlgorithms2D.CartesianToPolar(pt, out angle, out length);
                    seedNode.Angle = angle;
                    seedNode.Length = length;
                    seedNode.MaxConnectivity = 4;
                    seedNode.ParentIndex = -1;
                    Nodes.Add(seedNode);
                    // -- start with three childs ---
                    for (int i = 0; i < 3; i++) parentNodes.Add(seedNode);
                } 
            }

            CreateTree(parentNodes, TreeDepth);
        }

        private bool TestSeedNode(Vector2D node)
        {
            bool validIstrNode = true;
            if (node.X < Border.Left) validIstrNode = false;
            else if (node.Y < (Border.Bottom)) validIstrNode = false;
            else if (node.X > Border.Right) validIstrNode = false;
            else if (node.Y > Border.Top) validIstrNode = false;
            return validIstrNode;
        }

        /// <summary>
        /// Generate random chromosome value.
        /// </summary>
        /// 
        /// <remarks><para>Regenerates chromosome's value using random number generator.</para>
        /// </remarks>
        ///
        public void Generate()
        {
            Initialize();
        }

        # endregion

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Create the instruction tree with a iterative function.  
        /// </summary>
        /// <param name="parentsIdx"> List of parent nodes to start from. </param>
        /// <param name="repetitions"> Number of iterations. </param>
        ////public void CreateTreeI(List<int> parentsIdx, int repetitions)
        ////{
        ////    List<int> newParentsIdx = new List<int>();
        ////    while (repetitions > 0)
        ////    {
        ////        foreach (int parIdx in parentsIdx)
        ////        {
        ////            //InstructionNode curNode = new InstructionNode(Nodes.Count, 5, 20);
        ////            InstructionNode curNode = new InstructionNode(Nodes.Count, rnd.Next(angleRange) - angleRange/2, rnd.Next(lengthRange) + minLength);
        ////            InstructionNode parentNode = Nodes.Find(InstructionNode => InstructionNode.Index == parIdx);
        ////            parentNode.Children.Add(curNode);
        ////            curNode.ParentIndex = parIdx;
        ////            curNode.MaxConnectivity = rnd.Next(maxChilds-1) + 2;
        ////            Nodes.Add(curNode);
                    
        ////            int nrArms = rnd.Next(maxChilds - 1) + 1;
        ////            for (int i = 0; i < nrArms; i++)
        ////            {
        ////                newParentsIdx.Add(curNode.Index);
        ////            }
        ////        }
        ////        parentsIdx = new List<int>();
        ////        parentsIdx.AddRange(newParentsIdx);
        ////        newParentsIdx = new List<int>();
        ////        repetitions--;
        ////    }  
        ////}

        public void CreateTree(List<InstructionNode> parents, int repetitions)
        {
            _angleRange = ControlParameters.AngleRange;
            _lengthRange = ControlParameters.LengthRange;
            _minLength = ControlParameters.MinLength;
            _maxChilds = ControlParameters.MaxChilds;

            List<InstructionNode> newParents = new List<InstructionNode>();
            while (repetitions > 0)
            {
                foreach (InstructionNode parNode in parents)
                {
                    InstructionNode curNode = new InstructionNode(Nodes.Count, rand.Next(_angleRange) - _angleRange / 2, rand.Next(_lengthRange) + _minLength);
                    //InstructionNode parentNode = Nodes.Find(InstructionNode => InstructionNode.Index == parNode.ParentIdx);
                    curNode.ParentIndex = parNode.Index;
                    curNode.MaxConnectivity = rand.Next(_maxChilds - 1) + 2;
                    double rndValue = rand.NextDouble();
                    // --- some streets doesn't go straight on...
                    //if (rndValue < 0.2)
                    //{
                    //    if (rndValue < 0.1) curNode.Angle += 90;
                    //    else curNode.Angle -= 90;
                    //}

                    Nodes.Add(curNode);

                    int nrArms = rand.Next(_maxChilds - 1) + 1;
                    for (int i = 0; i < nrArms; i++)
                    {
                        newParents.Add(curNode);
                    }
                }
                parents = new List<InstructionNode>();
                parents.AddRange(newParents);
                newParents = new List<InstructionNode>();

                repetitions--;
            }
        }

        /// <summary>
        /// Create the instruction tree with a recursive function.  
        /// </summary>
        /// <param name="parent"> The parent node to which the current node is connected to. </param>
        /// <param name="level"> The recursion level. </param>
        /// <remarks> The order of nodes produced by this recursive function isn't useful for the street network generating function.
        /// It's better to use the iterative CreateTreeI function.</remarks>
        //public void CreateTreeR(int parentIdx, int level)
        //{
        //    if (level < TreeDepth)
        //    {
        //        for (int i = 0; i < 3; i++)
        //        {
        //            InstructionNode curNode = new InstructionNode(Nodes.Count, 5, 20);
        //            curNode.ParentIndex = parentIdx;
        //            //if (Nodes.Count < NrNodes * 4)
        //            {
        //                Nodes.Add(curNode);
        //                int idx = curNode.Index;
        //                //if (Nodes.Count < NrNodes)
        //                CreateTreeR(idx, level + 1);
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// Collect the nodes of a branch.
        /// </summary>
        /// <param name="branchNodes">List of branch nodes. Initialise with an empty one.</param>
        /// <param name="parIdx">Index of node, from where the branch starts.</param>
        /// <returns>List of branch nodes.</returns>
        public List<InstructionNode> FindBranch(List<InstructionNode> branchNodes, Int64 parIdx)
        {
            List<InstructionNode> addNodes = Nodes.FindAll(InstructionNode => InstructionNode.ParentIndex == parIdx);
            if (addNodes.Count > 0)
            {
                branchNodes.AddRange(addNodes);
                foreach (InstructionNode node in addNodes)
                {
                    FindBranch(branchNodes, node.Index);
                }
            }
            return branchNodes;
        }

        /// <summary>
        /// Collect the nodes of a branch.
        /// </summary>
        /// <param name="branchNodes">List of branch nodes. Initialise with an empty one.</param>
        /// <param name="parIdx">Indes of node, from where the branch starts.</param>
        /// <returns>List of branch nodes.</returns>
        //public List<InstructionNode> FindBranch(List<InstructionNode> branchNodes, InstructionNode parentNode)
        //{
        //    List<InstructionNode> addNodes = parentNode.Children;
        //    if (addNodes.Count > 0)
        //    {
        //        branchNodes.AddRange(addNodes);
        //        foreach (InstructionNode node in addNodes)
        //        {
        //            FindBranch(branchNodes, node);
        //        }
        //    }
        //    return branchNodes;
        //}

        /// <summary>
        /// Create a street network from the instruction tree.
        /// </summary>
        public UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> CreateStreetNetwork() // Subdivision CreateStreetNetwork()//
        {
            //Stopwatch timer = new Stopwatch();
            bool testing = true;

            //if (testing)
            //    timer.Start();
            StreetPattern streetNetwork = new StreetPattern(this.Border);
            //if (testing)
            //{
            //    Debug.WriteLine("StreetNetwork new: " + timer.ElapsedMilliseconds + "ms \n");
            //    timer.Reset();
            //    timer.Start();
            //}
            //streetNetwork.GrowStreetPattern(this, InitalNetwork);
 //           streetNetwork.GrowStreetPatternNew(this, InitalNetwork);
            //if (testing)
            //{
            //    Debug.WriteLine("GrowStreetPattern: " + timer.ElapsedMilliseconds + "ms \n");
            //    timer.Reset();
            //    timer.Start();
            //}
            streetNetwork.CleanUp();
            //if (testing)
            //{
            //    Debug.WriteLine("CleanUp: " + timer.ElapsedMilliseconds + "ms \n");
            //    timer.Stop();
            //}
            return streetNetwork.QGraph;
            //WHY?
            //return null;
        }


        /// <summary>
        /// Create new random chromosome (factory method)
        /// </summary>
        public IMultiChromosome CreateNew()
        {
            return new InstructionTree(TreeDepth, InitalNetwork, Border);
        }

        /// <summary>
        /// Clone the chromosome.
        /// </summary>
        public IMultiChromosome Clone()
        {
            return new InstructionTree(this);
        }

        /// <summary>
        /// Mutation operator
        /// </summary>
        public void Mutate()
        {
            List<InstructionNode> mutationNodes = this.Nodes.FindAll(InstructionNode => InstructionNode.ParentIndex <= this.TreeDepth);
            int mutateRate = Convert.ToInt16 (Math.Ceiling(mutationNodes.Count * 0.05));
            double rndVal = rand.NextDouble();
            for (int i = 0; i < mutateRate; i++) // number of mutations per tree
            {
                InstructionNode mutationNode = mutationNodes[rand.Next(mutationNodes.Count-2)+2]; // Wahrscheinlichkeit nach Level der Nodes einstellen
                if (rndVal < 0.25)
                {
                    mutationNode.Angle = rand.Next(_angleRange) - _angleRange / 2;
                }
                else if (rndVal < 0.5)
                {
                    mutationNode.Length = rand.Next(_lengthRange) + _minLength;
                }
                else
                {
                    mutationNode.MaxConnectivity = rand.Next(_maxChilds - 1) + 2;
                }
            }
        }

        /// <summary>
        /// Crossover operator
        /// </summary>
        public void Crossover(IMultiChromosome pair)
        {
            MyCrossover(pair);
        }

        public void MyCrossover(IMultiChromosome pair)
        {
            InstructionTree p = (InstructionTree)pair;
            // check for correct pair
            if (p != null)
            {
                // --- choose random nodes
                //int r = rand.Next(this.Nodes.Count - 3) + 3;
                //InstructionNode childThis = (InstructionNode)this.Nodes[r];
                //Int64 parentThisIdx = childThis.ParentIndex;
                //List<InstructionNode> branchThis = new List<InstructionNode>();
                //branchThis = this.FindBranch(branchThis, childThis.Index);
                //branchThis.Add(childThis);

                //int m = rand.Next(p.Nodes.Count - 3) + 3;
                //InstructionNode childOther = (InstructionNode)p.Nodes[m];
                //Int64 parentOtherIdx = childOther.ParentIndex;
                //List<InstructionNode> branchOther = new List<InstructionNode>();
                //branchOther = p.FindBranch(branchOther, childOther.Index);
                //branchOther.Add(childOther);

                // --- choose random nodes
                List<InstructionNode> thisSelNodes = this.Nodes.FindAll(InstructionNode => InstructionNode.ParentIndex < this.TreeDepth);
                if (thisSelNodes.Count < 4) return;
                int r = rand.Next(thisSelNodes.Count - 3) + 3;
                InstructionNode childThis = (InstructionNode)thisSelNodes[r];
                Int64 parentThisIdx = childThis.ParentIndex;
                List<InstructionNode> branchThis = new List<InstructionNode>();
                branchThis = this.FindBranch(branchThis, childThis.Index);
                branchThis.Add(childThis);

                List<InstructionNode> otherSelNodes = p.Nodes.FindAll(InstructionNode => InstructionNode.ParentIndex < p.TreeDepth);
                if (otherSelNodes.Count < 4) return;
                int m = rand.Next(otherSelNodes.Count - 3) + 3;
                InstructionNode childOther = (InstructionNode)otherSelNodes[m];
                Int64 parentOtherIdx = childOther.ParentIndex;
                List<InstructionNode> branchOther = new List<InstructionNode>();
                branchOther = p.FindBranch(branchOther, childOther.Index);
                branchOther.Add(childOther);

                // --- delete nodes from onw InstructionTree 
                //parentThis.Children.Remove(childThis);
                foreach (InstructionNode tNode in branchThis)
                {
                    if (this.Nodes.Contains(tNode)) this.Nodes.Remove(tNode);
                }

                //parentOther.Children.Remove(childOther);
                foreach (InstructionNode oNode in branchOther)
                {
                    if (p.Nodes.Contains(oNode)) p.Nodes.Remove(oNode);
                }

                // --- copy nodes to the other InstructionTree
                // -- change indices of all branch nodes --
                Int64 highestThisIdx = this.HighestIdx;
                foreach (InstructionNode tNode in branchOther)
                {
                    Int64 newIdx = tNode.Index + highestThisIdx;
                    tNode.Index = newIdx;
                    tNode.ParentIndex = tNode.ParentIndex + highestThisIdx;
                }
                this.Nodes.AddRange(branchOther);
                childOther.ParentIndex = parentThisIdx;

                //parentOther.Children.Add(childThis);
                Int64 highestOtherIdx = p.HighestIdx;
                foreach (InstructionNode tNode in branchThis)
                {
                    Int64 newIdx = tNode.Index + highestOtherIdx;
                    tNode.Index = newIdx;
                    tNode.ParentIndex = tNode.ParentIndex + highestOtherIdx;
                }
                p.Nodes.AddRange(branchThis);
                childThis.ParentIndex = parentOtherIdx;
            }
        }

        public void Adapt() { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AForge.Genetic;
using CPlan.Geometry;
using Tektosyne.Geometry;
using System.Drawing;
using AForge;
using QuickGraph;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public class InstructionTreeSingle: InstructionTree, IChromosome 
    {
        double fitness;

        /// <summary>
        /// The fitness values for each criteria.
        /// </summary>
        public double Fitness
        {
            get { return this.fitness; }
        }


        public InstructionTreeSingle(int treeDepth, UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> iniNet, Rect2D border)// : base(treeDepth)
            : base(treeDepth, iniNet, border)
        {
        }

        /// <summary>
        /// Evaluate chromosome with specified fitness function.
        /// </summary>
        /// <param name="function">Fitness function to use for evaluation of the chromosome.</param>
        /// <remarks><para>Calculates chromosome's fitness values using the specifed fitness function.</para></remarks>
        public void Evaluate(IFitnessFunction function)
        {
            fitness = function.Evaluate(this);
        }

        /// <summary>
        /// Clone the chromosome.
        /// </summary>
        public new IChromosome Clone()
        {
            return new InstructionTreeSingle(this);
        }

        /// <summary>
		/// Copy Constructor.
		/// </summary>
        protected InstructionTreeSingle(InstructionTreeSingle source) : base(source)
		{            
            this.fitness = source.fitness;
		}

        /// <summary>
        /// Create new random chromosome (factory method)
        /// </summary>
        public new IChromosome CreateNew()
        {
            return new InstructionTreeSingle(TreeDepth, InitalNetwork, Border);
        }

        /// <summary>
        /// Crossover operator
        /// </summary>
        public void Crossover(IChromosome pair)
        {
            MyCrossoverSingle(pair);
        }
        public void MyCrossoverSingle(IChromosome pair)
        {
            InstructionTree p = (InstructionTree)pair;
            // check for correct pair
            if (p != null)
            {
                // --- choose random nodes
                //int r = rand.Next(this.Nodes.Count - 3) + 3;
                //InstructionNode childThis = (InstructionNode)this.Nodes[r];
                //Int64 parentThisIdx = childThis.ParentIndex;
                //List<InstructionNode> branchThis = new List<InstructionNode>();
                //branchThis = this.FindBranch(branchThis, childThis.Index);
                //branchThis.Add(childThis);

                //int m = rand.Next(p.Nodes.Count - 3) + 3;
                //InstructionNode childOther = (InstructionNode)p.Nodes[m];
                //Int64 parentOtherIdx = childOther.ParentIndex;
                //List<InstructionNode> branchOther = new List<InstructionNode>();
                //branchOther = p.FindBranch(branchOther, childOther.Index);
                //branchOther.Add(childOther);

                // --- choose random nodes
                List<InstructionNode> thisSelNodes = this.Nodes.FindAll(InstructionNode => InstructionNode.ParentIndex < this.TreeDepth);
                if (thisSelNodes.Count < 4) return;
                int r = rand.Next(thisSelNodes.Count - 3) + 3;
                InstructionNode childThis = (InstructionNode)thisSelNodes[r];
                Int64 parentThisIdx = childThis.ParentIndex;
                List<InstructionNode> branchThis = new List<InstructionNode>();
                branchThis = this.FindBranch(branchThis, childThis.Index);
                branchThis.Add(childThis);

                List<InstructionNode> otherSelNodes = p.Nodes.FindAll(InstructionNode => InstructionNode.ParentIndex < p.TreeDepth);
                if (otherSelNodes.Count < 4) return;
                int m = rand.Next(otherSelNodes.Count - 3) + 3;
                InstructionNode childOther = (InstructionNode)otherSelNodes[m];
                Int64 parentOtherIdx = childOther.ParentIndex;
                List<InstructionNode> branchOther = new List<InstructionNode>();
                branchOther = p.FindBranch(branchOther, childOther.Index);
                branchOther.Add(childOther);

                // --- delete nodes from onw InstructionTree 
                //parentThis.Children.Remove(childThis);
                foreach (InstructionNode tNode in branchThis)
                {
                    if (this.Nodes.Contains(tNode)) this.Nodes.Remove(tNode);
                }

                //parentOther.Children.Remove(childOther);
                foreach (InstructionNode oNode in branchOther)
                {
                    if (p.Nodes.Contains(oNode)) p.Nodes.Remove(oNode);
                }

                // --- copy nodes to the other InstructionTree
                // -- change indices of all branch nodes --
                Int64 highestThisIdx = this.HighestIdx;
                foreach (InstructionNode tNode in branchOther)
                {
                    Int64 newIdx = tNode.Index + highestThisIdx;
                    tNode.Index = newIdx;
                    tNode.ParentIndex = tNode.ParentIndex + highestThisIdx;
                }

                this.Nodes.AddRange(branchOther);
                childOther.ParentIndex = parentThisIdx;

                //parentOther.Children.Add(childThis);
                Int64 highestOtherIdx = p.HighestIdx;
                foreach (InstructionNode tNode in branchThis)
                {
                    Int64 newIdx = tNode.Index + highestOtherIdx;
                    tNode.Index = newIdx;
                    tNode.ParentIndex = tNode.ParentIndex + highestOtherIdx;
                }
                p.Nodes.AddRange(branchThis);
                childThis.ParentIndex = parentOtherIdx;
            }
        }

        /// <summary>
        /// Compare two chromosomes.
        /// </summary>
        /// 
        /// <param name="o">Binary chromosome to compare to.</param>
        /// 
        /// <returns>Returns comparison result, which equals to 0 if fitness values
        /// of both chromosomes are equal, 1 if fitness value of this chromosome
        /// is less than fitness value of the specified chromosome, -1 otherwise.</returns>
        /// 
        public int CompareTo(object o)
        {
            double f = ((InstructionTreeSingle)o).fitness;

            return (fitness == f) ? 0 : (fitness < f) ? 1 : -1;
        }
    }
}

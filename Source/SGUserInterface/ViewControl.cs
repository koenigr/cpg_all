﻿//#if CT_UNCOMMENT
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Windows.Forms;
using System.Drawing;
using CPlan.Geometry;
using CPlan.VisualObjects;

namespace CPlan.UrbanPattern
{

    /// <summary>
    /// Implements basic view controls.
    /// Zoom, Pan, (Rotate).
    /// </summary>
    public class ViewControl
    {
        //############################################################################################################################################################################
        # region Properties

        private GLControl glControl;
        //private Single scaleView = 1.0f;
        //private Single rotateView = 0.0f;
        private Vector3d posView = new Vector3d(0, 0, 0);
        private Vector3d transformCentre = new Vector3d(0, 0, 0);
        private Point lastMouseLocation;
        //private bool mouseDownR = false;
        //private bool mouseDownL = false;
        //private int w;
        //private int h;
        private bool pan;
        private Visualisation.Camera camera_2D;
        private Visualisation.Camera camera_3D;

        public Visualisation.Camera Camera;
        
        /// <summary> If GLControl control is loaded. </summary>
        public bool Loaded { get; protected set; }
        public Color BackgroundColor { get; protected set; }
        public bool Background { get; set; }
        public ObjectsAllList AllObjectsList = new ObjectsAllList();

        # endregion

        //############################################################################################################################################################################
        # region Constructors
        //===================================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of ViewControl.
        /// </summary>
        /// /// <param name="curControl">A OpenTK GLControl.</param>
        public ViewControl(GLControl curControl)
        {
            glControl = curControl; 

            glControl.MouseWheel += new System.Windows.Forms.MouseEventHandler(glControl_MouseWheel);
            glControl.MouseMove += new System.Windows.Forms.MouseEventHandler(glControl_MouseMove);
            glControl.MouseDown += new System.Windows.Forms.MouseEventHandler(glControl_MouseDown);
            glControl.MouseUp += new System.Windows.Forms.MouseEventHandler(glControl_MouseUp);
            glControl.Load += new System.EventHandler(glControl_Load);
            glControl.Resize += new System.EventHandler(glControl_Resize);
            glControl.Paint += new System.Windows.Forms.PaintEventHandler(this.glControl_Paint);

            Background = true;
            camera_2D = new Visualisation.Camera();
            camera_2D.m_beta  = -(float)Math.PI/2;
            camera_2D.m_scale =  20.0f;
            camera_2D.m_znear = -9999.9f;
            camera_2D.m_zfar  =  9999.9f;

            camera_3D = new Visualisation.Camera();
            camera_3D.m_alpha = -(float)Math.PI/4;
            camera_3D.m_beta  = -(float)Math.PI/12;
            camera_3D.m_zoom  =  60.0f;
            camera_3D.m_znear =  0.1f;
            camera_3D.m_zfar  =  9999.9f;
            camera_3D.m_ortho =  false;
            camera_3D.SetPosition(-2.0f, -2.0f, 1.8f);

            Camera = camera_2D;
        }

        # endregion

        //############################################################################################################################################################################
        # region Methods
        public void CameraChange()
        {
            if (Camera == camera_2D) Camera = camera_3D;
            else if (Camera == camera_3D) Camera = camera_2D;
            glControl.Invalidate();
        }

        //===================================================================================================================================================================
        private void glControl_Load(object sender, EventArgs e)
        {
            Loaded = true;
            BackgroundColor = Color.White;
            SetupViewport();
        }

        //===================================================================================================================================================================
        /// <summary>
        /// Initialize the GLControl viewport settings
        /// </summary>
        public void SetupViewport()
        {
            glControl.MakeCurrent();
			int w = glControl.Width;
			int h = glControl.Height;
			GL.Viewport(0, 0, w, h); // Use all of the glControl painting area
        }   

        //===================================================================================================================================================================
        private void glControl_Paint(object sender, PaintEventArgs e)
        {
            if (!Loaded) return;
            glControl.MakeCurrent();

            InitOpenGL();
            GL.ClearColor(BackgroundColor);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            Camera.SetOpenGLMatrixes();
            if (Background) Camera.DrawKoordinatenkreuz();
        }

        //===================================================================================================================================================================
        private void InitOpenGL()
        {
            GL.Enable(EnableCap.DepthTest);

            // set background color 
            GL.ClearColor(BackgroundColor);

            // set clear Z-Buffer value
            GL.ClearDepth(1.0f);

            GL.Enable(EnableCap.Blend);
            GL.Enable(EnableCap.PointSmooth);
            GL.Enable(EnableCap.LineSmooth);
            GL.Disable(EnableCap.PolygonSmooth);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.ShadeModel(ShadingModel.Smooth);
            GL.DepthFunc(DepthFunction.Lequal);
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
            GL.LineWidth(1);
        }
        
        //===================================================================================================================================================================
        private void glControl_Resize(object sender, EventArgs e)
        {
            if (!Loaded) return;
            SetupViewport();
            glControl.Invalidate();
        }

        //===================================================================================================================================================================
        public void DeactivateInteraction()
        {
            glControl.MouseMove -= glControl_MouseMove;
            glControl.MouseDown -= glControl_MouseDown;
            glControl.MouseUp -= glControl_MouseUp; 
        }

        public void ActivateInteraction()
        {
            glControl.MouseMove += glControl_MouseMove;
            glControl.MouseDown += glControl_MouseDown;
            glControl.MouseUp += glControl_MouseUp;
        }
        //===================================================================================================================================================================
        private void glControl_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!Loaded) return;
            //if (e.Button == System.Windows.Forms.MouseButtons.Right)
            //{
            //    if ((Control.ModifierKeys & Keys.Control) > 0)
            //    {
            //        transformCentre.X = e.Location.X;
            //        transformCentre.Y = e.Location.Y;
            //    }
            //    mouseDownR = true;
            //}
            //
            // -- object interaction --
            //
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                //mouseDownL = true;
                CGeom3d.Vec_3d point_xy = Camera.GetPointXYPtn(e.X, e.Y, glControl.Width, glControl.Height);
                if (point_xy == null) return;
                Vector2D p = new Vector2D(point_xy.x, point_xy.y);
                AllObjectsList.ObjectsToInteract.MouseDown(e, p, ref glControl);
            }
            //
            // -- view control --
            //
            else if (e.Button == System.Windows.Forms.MouseButtons.Right)
			{
                lastMouseLocation.X = Cursor.Position.X;
                lastMouseLocation.Y = Cursor.Position.Y;
				pan = true;
			}
        }

        //===================================================================================================================================================================
        private void glControl_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!Loaded) return;
            //
            // -- view control --
            //
			if (pan)
			{
                int delta_x = Cursor.Position.X - lastMouseLocation.X;
                int delta_y = Cursor.Position.Y - lastMouseLocation.Y;
               // Cursor.Position = new System.Drawing.Point(lastMouseLocation.X, lastMouseLocation.Y);
                lastMouseLocation.X = Cursor.Position.X;
                lastMouseLocation.Y = Cursor.Position.Y;

                if (Camera == camera_2D)
                {
                    double dAspectRatio = (double)glControl.Width / (double)glControl.Height;
                    double dAspectRatioX = dAspectRatio;
                    double dAspectRatioY = 1;

                    if (dAspectRatio < 1.0)
                    {
                        dAspectRatioX = 1;
                        dAspectRatioY = 1.0 / dAspectRatio;
                    }
                    CGeom3d.Vec_3d newPos = Camera.GetCameraPos();
                    newPos.x -= (float)(delta_x) / glControl.Width * Camera.m_scale * dAspectRatioX;
                    newPos.y += (float)(delta_y) / glControl.Height * Camera.m_scale * dAspectRatioY;
                    Camera.SetPosition(newPos.x, newPos.y, newPos.z);
                }
                else
                {
                    if (Control.ModifierKeys != Keys.Control)
                    {
                        // --- 3D Camera Pan ---
                        CGeom3d.Vec_3d vx = new Vector3D(), vy = new Vector3D();
                        Vector3D lot = new Vector3D(0,0,1);
                        CGeom3d.Vec_3d blick = Camera.GetBlickVectorPtn();
                        vx = blick.amul(lot);
                        vy = blick.amul(vx);
                        vx.Normalize();
                        vy.Normalize();

                        double faktor = Math.Abs(Camera.GetCameraPos().z) / 10.0;
                        if (faktor<1)
                            faktor = 1;
                        faktor *= 0.03;
                        CGeom3d.Vec_3d newPos = Camera.GetCameraPos() + vx * (delta_x * faktor) + vy * (delta_y * faktor);
                        Camera.SetPosition(newPos.x, newPos.y, newPos.z);
                    }
                    else
                    {
                        // --- 3D Camera Rotate ---
                        Camera.m_beta  -= delta_y * 0.01f;
                        Camera.m_alpha -= delta_x * 0.01f;

                        while (Camera.m_alpha < 0) Camera.m_alpha += Convert.ToSingle(2 * Math.PI);
                        while (Camera.m_alpha > 2 * Math.PI) Camera.m_alpha -= Convert.ToSingle(2 * Math.PI);
                        if (Camera.m_beta < -Math.PI / 2) Camera.m_beta = Convert.ToSingle(-Math.PI / 2);
                        if (Camera.m_beta > Math.PI / 2) Camera.m_beta = Convert.ToSingle(Math.PI / 2);
                    }
                }
                glControl.Invalidate(); //Invalidate();
                return;
			}
          
            //if (mouseDownR)
            //{
            //    if ((Control.ModifierKeys & Keys.Control) > 0) Rotate_MouseMove(e); // -- rotate view
            //    else Pan_MouseMove(e); // -- pan view 
            //}
            //
            // -- object interaction --
            //
            CGeom3d.Vec_3d point_xy = Camera.GetPointXYPtn(e.X, e.Y, glControl.Width, glControl.Height);
            if (point_xy == null) return;

            Cursor tmpCursor = Cursors.Default;
            Vector2D p = new Vector2D(point_xy.x, point_xy.y);
            AllObjectsList.ObjectsToInteract.MouseMove(e, p, ref glControl, ref tmpCursor);
            // Hier könnte man noch optimieren, indem man immer nur den Bereich neuzeichnet, in dem das Objekt bewegt wurde.
            lastMouseLocation = e.Location;
        }

        //===================================================================================================================================================================
        private void glControl_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!Loaded) return;
            ////
            //// -- view control --
            ////
            if (pan)
            {
                pan = false;
                return;
            }
            //mouseDownR = false;
            //transformCentre.X = 0;
            //transformCentre.Y = 0;
            ////
            //// -- object interaction --
            ////
            //mouseDownL = false;
            AllObjectsList.ObjectsToInteract.MouseUp(e);// put the selected element to the fornt
            if (AllObjectsList.ObjectsToInteract.FoundInteractObj())
            {
                CGeom3d.Vec_3d point_xy = Camera.GetPointXYPtn(e.X, e.Y, glControl.Width, glControl.Height);
                if (point_xy == null) return;
                Vector2D p = new Vector2D(point_xy.x, point_xy.y);
                AllObjectsList.ObjectsToDraw.MouseUp(e, p, ref glControl);
            }
        }

        //===================================================================================================================================================================
        private void glControl_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!Loaded) return;
            //Zoom_MouseWheel(e);
            //
            // -- view control --
            //
            if (Camera == camera_2D)
            {
                Camera.m_scale *= Convert.ToSingle(1.0 - e.Delta * 0.0005);
               // Invalidate();
            }
            else
            {
                //im 3D entlang der Sichtachse verschieben
                CGeom3d.Vec_3d blick = Camera.GetBlickVectorPtn();
                double faktor = Math.Abs(Camera.GetCameraPos().z);
                if (faktor < 1)
                    faktor = 1;
                if (Control.ModifierKeys == Keys.Control)
                    faktor *= 0.1;
                CGeom3d.Vec_3d newPos = Camera.GetCameraPos() + blick * (e.Delta * 0.002 * faktor);
                Camera.SetPosition(newPos.x, newPos.y, newPos.z);
               // Invalidate();
            }
            glControl.Invalidate();
        }

        /// <summary>
        /// Update the glControl.
        /// </summary>
        public void Invalidate()
        {
            glControl.Invalidate();
        }

        //===================================================================================================================================================================
        /// <summary>
        /// Zoom to show all objects.
        /// </summary>
        public void ZoomAll()
        {
            double zoomfak = 1.1;
            double zoomfak_perspective = 1.001;
            BoundingBox box = new BoundingBox();

            // -- find the bounding box for all objects --
            if (AllObjectsList.ObjectsToDraw.Count > 0)
            {
                foreach (GeoObject curObj in AllObjectsList.ObjectsToDraw)
                    curObj.GetBoundingBox(box);
            }
            Vector3D min = box.GetMin();
            Vector3D max = box.GetMax();

            if (box.IsValid()) //wurden überhaupt Punkte in das BoundingBox Objekt eingefügt?
            {
                if (Camera == camera_2D)
                {
                    //Grundriss
                    CGeom3d.Vec_3d midV = (min + max) / 2;
                    Camera.SetPosition(midV.x, midV.y, midV.z);
                    
                    int dx = glControl.Width;
                    int dy = glControl.Height;
                    double sx=(double)(max.x - min.x) * zoomfak;
                    double sy=(double)(max.y - min.y) * zoomfak;
                    if (sx/((double)dx)>sy/((double)dy))
                    {
                        if (dx>dy)
                            sx/=((double)dx/(double)dy);
                        if (Math.Abs(sx)>0.000001) Camera.m_scale = Convert.ToSingle(sx);
                    }
                    else
                    {
                        if (dy>dx)
                            sy/=((double)dy/(double)dx);
                        if (Math.Abs(sy)>0.000001) Camera.m_scale = Convert.ToSingle(sy);
                    }
                }
                else
                {
                    //Perspektive
                    CGeom3d.Vec_3d blick = Camera.GetBlickVectorPtn();
                    double angle = Camera.m_zoom / 180.0 * Math.PI;
                    CGeom3d.Vec_3d mitte = (min + max) / 2;
                    CGeom3d.Vec_3d erg = new Vector3D(), zw = new Vector3D(), point = new Vector3D();
                    double max_dist = 0;

                    for(int z = 0; z < 8; ++z)
                    {
                        switch (z)
                        {
                        case 0: point = new Vector3D(min.x, min.y, min.z); break;
                        case 1: point = new Vector3D(min.x, min.y, max.z); break;
                        case 2: point = new Vector3D(min.x, max.y, min.z); break;
                        case 3: point = new Vector3D(min.x, max.y, max.z); break;
                        case 4: point = new Vector3D(max.x, min.y, min.z); break;
                        case 5: point = new Vector3D(max.x, min.y, max.z); break;
                        case 6: point = new Vector3D(max.x, max.y, min.z); break;
                        case 7: point = new Vector3D(max.x, max.y, max.z); break;
                        default: break; //throw new NotImplementedException();
                        }

                        double dist = GetDistMitte(mitte, blick, point, angle, ref zw, zoomfak_perspective);

                        if ((z==0) || (dist > max_dist))
                        {
                            erg.x = zw.x;
                            erg.y = zw.y;
                            erg.z = zw.z;
                            max_dist = dist;
                        }
                    }
                    Camera.SetPosition(erg.x, erg.y, erg.z);
                }

                glControl.Invalidate();
            }
        }

        //===================================================================================================================================================================
        private double GetDistMitte(CGeom3d.Vec_3d mitte, CGeom3d.Vec_3d blick, CGeom3d.Vec_3d p, double angle, ref CGeom3d.Vec_3d erg, double zoomfak)
        {
            //Distanz Punkt Linie
            double sa,sb,sc,sd;
            double q,r;
            Vector3D bp = new Vector3D();
            double distPL,distNP,dist;

            sa=blick.x;
            sb=blick.y;
            sc=blick.z;
            sd=-p.x * sa - p.y * sb - p.z * sc;

            q= sa * blick.x + sb * blick.y + sc * blick.z;

            r= (sa * mitte.x + sb * mitte.y + sc * mitte.z + sd)/q;

            bp.x= mitte.x - blick.x * r;
            bp.y= mitte.y - blick.y * r;
            bp.z= mitte.z - blick.z * r;

            distPL = Math.Sqrt(Math.Pow(bp.x - p.x, 2) +  Math.Pow(bp.y - p.y, 2) + Math.Pow(bp.z - p.z, 2));

            distNP = distPL/ Math.Tan(angle/2.0f)*zoomfak;

            (erg).x=bp.x-(blick.x*distNP);
            (erg).y=bp.y-(blick.y*distNP);
            (erg).z=bp.z-(blick.z*distNP);

            dist = Math.Sqrt(Math.Pow((erg).x-mitte.x,2)+ Math.Pow((erg).y - mitte.y, 2) + Math.Pow((erg).z - mitte.z, 2));

            return dist;
        }

        //===================================================================================================================================================================
        // Returns a System.Drawing.Bitmap with the contents of the current framebuffer
        public Bitmap GrabScreenshot()
        {
            if (GraphicsContext.CurrentContext == null)
                throw new GraphicsContextMissingException();

            Bitmap bmp = new Bitmap(glControl.ClientSize.Width, glControl.Height);
            System.Drawing.Imaging.BitmapData data =
                bmp.LockBits(glControl.ClientRectangle, System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            //bmp.SetResolution(1200.0F, 1200.0F);

            GL.ReadPixels(0, 0, glControl.Width, glControl.Height, PixelFormat.Bgr, PixelType.UnsignedByte, data.Scan0);
            bmp.UnlockBits(data);

            //bmp.SetResolution(300, 300);
            bmp.RotateFlip(RotateFlipType.RotateNoneFlipY);
            return bmp;
        }

        # endregion

    }



}

        //===================================================================================================================================================================
        /// <summary>
        /// Rotate the viewport based on mouse move.
        /// </summary>
        /// <param name="e"></param>
        //public void Rotate_MouseMove(MouseEventArgs e)
        //{
        //    // -- view control --
        //    Vector2 vel;
        //    //vel.X = (e.Location.X - lastMouseLocation.X) * scaleView;// movingChild.centre.X;
        //    vel.Y = (e.Location.Y - lastMouseLocation.Y) * scaleView;
        //    rotateView += vel.Y;
        //    //posView.Y += -vel.Y;

        //    SetupViewport();
        //    glControl.Invalidate();
        //}

        ////===================================================================================================================================================================
        ///// <summary>
        ///// Pan the viewport based on mouse move.
        ///// </summary>
        ///// <param name="e"></param>
        //public void Pan_MouseMove(MouseEventArgs e)
        //{
        //  //  transformCentre.X = e.Location.X;
        //  //  transformCentre.Y = e.Location.Y;
        //    Vector2 vel;
        //    vel.X = (e.Location.X - lastMouseLocation.X);// *scaleView;// movingChild.centre.X;
        //    vel.Y = (e.Location.Y - lastMouseLocation.Y);// *scaleView;
        //    posView.X += vel.X;
        //    posView.Y += -vel.Y;

        //    SetupViewport();
        //    glControl.Invalidate();
        //}

        //===================================================================================================================================================================
        /// <summary>
        /// Draw the background grid.
        /// </summary>
        //public void DrawBackgroundGrid()
        //{
        //    glControl.MakeCurrent();
        //    float xtmp;
        //    float nrL = 60.0f;
        //    float max = 0.0f;
        //    if (h < w) max = w; else max = h;
        //    float min = -max;
        //    max *= 2;
        //    float multiV = 1;

        //    if (scaleView > 1) multiV = Convert.ToSingle(Math.Round(1/scaleView, 1 )+.1);
        //    else multiV = Convert.ToInt32(Math.Round((1-scaleView)*10 )) + 1;
        //    min *= multiV;
        //    max *= multiV;

        //    // -- Draw fine grid --
        //    GL.LineWidth(0.2f);
        //    GL.Color3(0.5F, 0.5F, 0.5F);
        //    Single delta = max / nrL;
        //    GL.Begin(BeginMode.Lines);
        //    for (xtmp = min; xtmp < max; xtmp += delta)
        //    {
        //        GL.Vertex2(xtmp, min);
        //        GL.Vertex2(xtmp, delta * nrL);
        //        GL.Vertex2(min, xtmp);
        //        GL.Vertex2(delta * nrL, xtmp);
        //    };
        //    GL.End();

        //    // -- Draw Grid --
        //    GL.LineWidth(0.3f);
        //    GL.Color3(1.0F, 1.0F, 1.0F);
        //    GL.Begin(BeginMode.Lines);
        //    for (xtmp = min; xtmp <= max; xtmp += delta * 5)
        //    {
        //        GL.Vertex2(xtmp, min);
        //        GL.Vertex2(xtmp, delta * nrL);
        //        GL.Vertex2(min, xtmp);
        //        GL.Vertex2(delta * nrL, xtmp);
        //    };
        //    GL.End();
        //}




//===================================================================================================================================================================
/// <summary>
/// Zoom for viewport based upon the mouse wheel scrolling.
/// </summary>
/// <param name="e"> MouseEventArgs </param>
//public void Zoom_MouseWheel(MouseEventArgs e)
//{
//   // transformCentre.X = e.Location.X;
//   // transformCentre.Y = e.Location.Y;

//    Single scaleValue = Convert.ToSingle(e.Delta / 1000.0);
//    scaleView += scaleValue;
//    Single minScale = 0.1f;
//    Single maxScale = 10f;
//    if (scaleView > maxScale) { scaleView = maxScale; }
//    else if (scaleView < minScale) { scaleView = minScale; }
//    else
//    {
//        posView.X += e.Location.X * scaleValue;
//        posView.Y += e.Location.Y * scaleValue;

//    }
//    SetupViewport();
//    glControl.Invalidate();

//    //transformCentre.X = 0;
//    //transformCentre.Y = 0;
//}

//===================================================================================================================================================================
/// <summary>
/// Zoom to show all objects.
/// </summary>
//public void ZoomAll()
//{           
//    BoundingBox box = new BoundingBox();
//    double zoomfak = 1.1;

//    // -- find the bounding box for all objects --
//    if (AllObjectsList.ObjectsToDraw.Count > 0)
//    {
//        foreach (GeoObject curObj in AllObjectsList.ObjectsToDraw)
//            curObj.GetBoundingBox(box);
//    }
//    Vector3D min = box.GetMin();
//    Vector3D max = box.GetMax();

//    // -- position for ortho view --> posView
//    Vector2D delta = new Vector2D(max.X - min.X, max.Y - min.Y);
//    posView.X = min.X + delta.X / 2;
//    posView.Y = min.Y + delta.Y / 2;

//    // -- zoom factor for ortho view --> scaleView
//    int dx = glControl.Width;
//    int dy = glControl.Height;
//    double sx = (double)(max.X - min.X) * zoomfak;
//    double sy = (double)(max.Y - min.Y) * zoomfak;
//    if (sx / ((double)dx) > sy / ((double)dy))
//    {
//        if (dx > dy)
//            sx /= ((double)dx / (double)dy);
//        if (Math.Abs(sx) > 0.000001) scaleView = Convert.ToSingle(sx/100);
//    }
//    else
//    {
//        if (dy > dx)
//            sy /= ((double)dy / (double)dx);
//        if (Math.Abs(sy) > 0.000001) scaleView = Convert.ToSingle(sy/100);
//    }

//    // -- reset rotation --
//    rotateView = 0;

//    SetupViewport();
//    glControl.Invalidate();
//}
//#endif
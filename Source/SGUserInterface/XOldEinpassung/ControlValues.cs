﻿/**
 * Kremlas/MOES
 * \file ControlValues.cs
 *
 * <A HREF="http://www.entwurfsforschung.de">
 * Copyright (C) 2011 by Dr. Reinhard Koenig. Alle Rechte vorbehalten.
 * Schutzvermerk nach DIN ISO 16016 beachten
 * </A>
 * koenig@entwurfsforschung.de
 * $Author: koenig $
 * $Rev: 390 $
 * $Date: 2011-02-11 11:12:42 +0100 (Fr, 11. Feb 2011) $
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CPlan.UrbanPattern
{
    public class RndGlobal
    {
        public static Random Rnd = new Random((int)DateTime.Now.Ticks);
    }

    public struct ControlValues
    {
        public Double SizeRate;
        public Double OldSizeRate;
        public Double MoveRate;
        public Double OldMoveRate;

        public ControlValues(double sizeRate, double oldSizeRate, double moveRate, double oldMoveRate)
        {
            this.SizeRate = sizeRate;
            this.OldSizeRate = oldSizeRate;
            this.MoveRate = moveRate;
            this.OldMoveRate = oldMoveRate;
        }
        public override string ToString()
        {
            return (String.Format("({0}, {1}, {2}, {3})", SizeRate, OldSizeRate, MoveRate, OldMoveRate));
        }

    }
}

﻿/**
 * Kremlas/MOES
 * \file REvo.cs
 *
 * <A HREF="http://www.entwurfsforschung.de">
 * Copyright (C) 2011 by Dr. Reinhard Koenig. Alle Rechte vorbehalten.
 * Schutzvermerk nach DIN ISO 16016 beachten
 * </A>
 * koenig@entwurfsforschung.de
 * $Author: koenig $
 * $Rev: 393 $
 * $Date: 2011-02-21 16:21:26 +0100 (Mo, 21. Feb 2011) $
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Threading.Tasks;
using Troschuetz.Random.Distributions.Continuous;
using CPlan.Geometry;
using CPlan.VisualObjects;

namespace CPlan.UrbanPattern
{
    public class REvo
    {
        public static Poly2D Canvas;
        public static Rect2D BoundingRect;
        //public static Rect2D Canvas;
        public RPopulation Parents = new RPopulation();
        public RPopulation Children = new RPopulation();
        public List<GPolygon> PhenotypeGdi = new List<GPolygon>();
        public List<GPolygon> PhenotypeGl = new List<GPolygon>();
        public RPopulation RndArchive = new RPopulation();

        //private Boolean m_topoOn = true;
        private NormalDistribution m_normDist = new NormalDistribution();
        //private Random Rnd = new Random((int)DateTime.Now.Ticks);
        private bool m_single = false;
        private int m_popSize;
        //private int m_successCounter;
        //private double m_strategyParam;

        public String Mode = "plus"; // plus selection is the more conservative method

        //==============================================================================================
        /// <summary>
        /// Creates a new genome for rectangles
        /// </summary>
        /// <param name="canvas">the border canvas</param>
        /// <param name="populationSize">nuber of parent individuals in this EA[0]</param>
        /// <param name="nrObjects">number of objects=gens=rectangles per individual</param>
        public REvo(Poly2D canvas, int populationSize, int nrObjects)
        {
            RChromosome curChrom;
            //m_strategyParam = 1;
            Canvas = canvas;
            BoundingRect = GeoAlgorithms2D.BoundingBox(Canvas.PointsFirstLoop);
            m_popSize = populationSize;
            //m_successCounter = 0;

            if (m_single)
            {
                curChrom = new RChromosome(canvas, nrObjects, RndGlobal.Rnd.Next(1000));
                Parents.Add(curChrom);
            }
            else
            {
                for (int i = 0; i < populationSize; i++)
                {
                    curChrom = new RChromosome(canvas, nrObjects, RndGlobal.Rnd.Next(1000));
                    Parents.Add(curChrom);
                }
            }

            for (int i = 0; i < 1000; i++)
            {
                curChrom = new RChromosome(canvas, nrObjects, RndGlobal.Rnd.Next(1000));
                RndArchive.Add(curChrom);
            }
          
            GrowPolys(Parents);
            //GrowPolys(RndArchive);
        }

        //==============================================================================================
        //==============================================================================================
        /// <summary>
        /// Run one iteration
        /// </summary>
        public void Run()
        {

            RunAdjust();
            
            NextGeneration();

            PhenotypeGl = Parents.BuildPhenotypeGl();
        }

        //==============================================================================================
        private void DevelopIndividual(RChromosome curIndi, double propForce, double propRate, double repelForce, double repelRate)
        {
            bool propChange = RParameter.propChange;
            bool rotate = RParameter.rotate;
            double rotForce = RParameter.rotForce;  
            double rotRate = RParameter.rotRate;
            bool borderRot = RParameter.borderRot;
            double borderRotForce = RParameter.borderRotForce;
            double borderRotRate = RParameter.borderRotRate;
            //------------------------------------------------------------
                       
            curIndi.CheckBorder();
            curIndi.CheckPolyBorder();
            // --- attractor ---
            curIndi.AttractPolys(RParameter.attractForce, RParameter.attractRadius);

            // --- proportion ---
            if (propChange) curIndi.ChangeProportion(propForce, propRate);

            // --- repel ---
            curIndi.RepelPolys(repelForce, repelRate);

            // --- rotation ---
            if (rotate)
            {
                curIndi.ChangeRotation(rotForce, rotRate);
                if (borderRot) curIndi.BorderRotation(borderRotForce, borderRotRate);
            }
            
        }

        //==============================================================================================
        private void RunAdjust()
        {
            double repForce = RParameter.repForce;  // (double)(((Form1)Application.OpenForms[0]).uD_repForce.Value / 100);
            double repRate = RParameter.repRate;    //(double)(((Form1)Application.OpenForms[0]).uD_repRate.Value / 100);
            double propForce = RParameter.propForce;   //(double)(((Form1)Application.OpenForms[0]).uD_propForce.Value / 100);
            double propRate = RParameter.propRate;   //(double)(((Form1)Application.OpenForms[0]).uD_propRate.Value / 100);
            
            RChromosome curChrom;
            RChromosome[] curChild = new RChromosome[2];
            double pressVal = RParameter.pressVal;   //((((double)((Form1)Application.OpenForms[0]).uD_pressure.Value)));
            double crossRate = RParameter.crossRate;   //(double)(((Form1)Application.OpenForms[0]).uD_crossRate.Value) / 100.0;
            Boolean doCross = RParameter.doCross;   //((Form1)Application.OpenForms[0]).cB_actiCross.Checked;
            Boolean plusSel = RParameter.plusSel;
            double roomsNr = RParameter.roomsNr;
            int moveDist = (int)Math.Round((decimal)((BoundingRect.Height + BoundingRect.Width) / (roomsNr)), 0);
            Boolean elite = RParameter.elite;

            int nrCopy =  (int)(Parents.Count * pressVal / 2);// 0;//
            int idx;

            //curChrom = Parents[0];
            //DevelopIndividual(curChrom, propForce, propRate, repForce, repRate);
            Children.Clear();
            Children = new RPopulation();

            //Parents.EvaluationOverlap();
            //Parents.Sort((x, y) => y.FitOverl.CompareTo(x.FitOverl));
            // --- copy Parents to childs ---
            if (elite)
            {
                curChrom = new RChromosome(Parents[0]);
                Children.Add(curChrom);
            }

            if (plusSel)
            {
                nrCopy -= Parents.Count / 2;
                for (int i = 0; i < Parents.Count; i++)
                {
                    curChrom = new RChromosome(Parents[i]);
                    DevelopIndividual(curChrom, propForce, propRate, repForce, repRate);
                    Children.Add(curChrom);
                }
            }
            // --- crossover & mutation --------------------------
            for (int i = 0; i < nrCopy; i++)
            {
                if (doCross & RndGlobal.Rnd.NextDouble() < crossRate) // --- crossover rate
                {
                    curChild = CrossoverTopo(); //CrossoverRandom();// CrossoverPos();//
                }
                else // --- just copy the Parents
                {
                    for (int j = 0; j < 2; j++)
                    {
                        idx = RndGlobal.Rnd.Next(Parents.Count);
                        curChild[j] = new RChromosome(Parents[idx]);
                    }
                }
                // --- copy mutated Parents to childs ------------
                for (int j = 0; j < 2; j++)
                {
                    // --- mutation operators ---
                    curChild[j].Parent = false;
                    if (RParameter.jumps)
                    {
                        curChild[j].MoveRandon(roomsNr / 1000, moveDist * roomsNr);
                        //  curChild[j].VarySizeFull(roomsNr / 500, moveDist);
                        curChild[j].MoveRandon(roomsNr / 1000, moveDist / 10);
                    
                        //curChild[j].VarySize(0.5);
                        //curChild[j].m_strategyParam[0] *= (Math.Exp(m_normDist.NextDouble() - 1));
                        //if (curChild[j].m_strategyParam[0] < 0.01) curChild[j].m_strategyParam[0] = 0.01;
                        //if (curChild[j].m_strategyParam[0] > 1) curChild[j].m_strategyParam[0] = 1;
                        //DevelopIndividual(curChild[j], curChild[j].m_strategyParam[0], propRate, repForce, repRate);
                    }
                    DevelopIndividual(curChild[j], propForce, propRate, repForce, repRate);
                    Children.Add(curChild[j]);
                }
            }
        }

        //==============================================================================================
        //private void RunTopo()
        //{
        //    double repForce = RParameter.repForce;// (double)(((Form1)Application.OpenForms[0]).uD_repForce.Value / 100);
        //    double repRate = RParameter.repRate;   //(double)(((Form1)Application.OpenForms[0]).uD_repRate.Value / 100);
        //    double propForce = RParameter.propForce;   //(double)(((Form1)Application.OpenForms[0]).uD_propForce.Value / 100);
        //    double propRate = RParameter.propRate;   //(double)(((Form1)Application.OpenForms[0]).uD_propRate.Value / 100);
        //    double sDamp = RParameter.sDamp;   //(double)(((Form1)Application.OpenForms[0]).uD_sDamp.Value / 100);
        //    RChromosome curChrom;
        //    RChromosome[] curChild = new RChromosome[2];
        //    double pressVal = RParameter.pressVal;   //((((double)((Form1)Application.OpenForms[0]).uD_pressure.Value)));
        //    double crossRate = RParameter.crossRate;   //(double)(((Form1)Application.OpenForms[0]).uD_crossRate.Value) / 100.0;
        //    Boolean doCross = RParameter.doCross;   //((Form1)Application.OpenForms[0]).cB_actiCross.Checked;
        //    Boolean elite = RParameter.elite;   //((Form1)Application.OpenForms[0]).cB_elite.Checked;
        //    Boolean plusSel = RParameter.plusSel;   //((Form1)Application.OpenForms[0]).rb_plus.Checked;
        //    int nrCopy = (int)((Parents.Count * (int)pressVal) / 2);
        //    nrCopy -= (int)(Parents.Count / 2);
        //    int idx;
        //    // --- temp flag - overrides the checkbox ---
        //    if (Mode == "plus") plusSel = true; else plusSel = false;

        //    for (int i = 0; i < Children.Count; i++)
        //    {
        //        Children[i] = null;
        //    }
        //    Children.Clear();

        //    double roomsNr = RParameter.roomsNr;   //(double)((Form1)Application.OpenForms[0]).uD_rooms.Value;
        //    int moveDist = (int)Math.Round((decimal)((Canvas.Height + Canvas.Width) / (roomsNr)), 0);
        //    int middle = (int)Math.Round((decimal)Parents.Count / 2, 0);
        //    int toFull = Parents.Count - middle;
        //    double thresH = RParameter.thresH;  // (double)((Form1)Application.OpenForms[0]).uD_rooms.Value *1.5;

        //    // EvaluationDist(Parents);
        //    // Parents.Sort((x, y) => y.FitDist.CompareTo(x.FitDist)); 
        //    // --- keep best parents - first half for distances ------------------
        //    // --- copy Parents to childs ---
        //    if (elite)
        //    {
        //        curChrom = new RChromosome(Parents[0]);
        //        Children.Add(curChrom);
        //        middle--;
        //    }

        //    //EvaluationOverlap(Parents);
        //    Parents.Sort((x, y) => y.FitOverl.CompareTo(x.FitOverl)); 
        //    // --- keep best parents - second half for overlapping -----------------
        //    // --- copy Parents to childs ---
        //    if (elite)
        //    {
        //        curChrom = new RChromosome(Parents[0]);
        //        Children.Add(curChrom);
        //        toFull--;
        //    }
        //    if (plusSel)
        //    {
        //        for (int i = 0; i < Parents.Count; i++)// toFull
        //        {
        //            Parents.EvaluationDist(Parents[i]);
        //            if (Parents[i].FitRealDist < thresH)
        //            {
        //                curChrom = new RChromosome(Parents[i]);
        //         //       curChrom.MoveRandon(roomsNr / 1000, moveDist*3);
        //         //       curChrom.MoveRandon(roomsNr / 1000, moveDist *0.001);
        //         //       curChrom.VarySizeFull(roomsNr / 10000, moveDist*.1);
        //           //     curChrom.VarySize(roomsNr / 1000);
        //           //   curChrom.RunSprings(sDamp);//.75);
        //          //      DevelopIndividual(curChrom, .5, 0.00, repForce, repRate);
        //      /*          if (i > Parents.Count/2)
        //                {
        //                    curChrom.VarySizeFull(roomsNr / 1000, moveDist * .01);
        //                    DevelopIndividual(curChrom, propForce, propRate, repForce, repRate);                          
        //                }*/
        //                Children.Add(curChrom);
        //            }
        //        }
        //    }

        //    // --- crossover & mutation -----------------------------------------
        //    for (int i = 0; i < nrCopy; i++)
        //    {
        //        if (doCross & RTools.Rnd.NextDouble() < crossRate) // --- crossover rate
        //        {
        //            curChild = CrossoverTopo(); //CrossoverPos();  // CrossoverRandom();//
        //        }
        //        else // --- just copy the Parents
        //        {
        //            for (int j = 0; j < 2; j++)
        //            {
        //                idx = RTools.Rnd.Next(Parents.Count);
        //                curChild[j] = new RChromosome(Parents[idx]);
        //            }
        //        }
        //        // --- copy mutated Parents to childs ------------
        //        for (int j = 0; j < 2; j++)
        //        {
        //            // --- mutation operators ---
        //            curChild[j].MoveRandon(roomsNr / 1000, moveDist*3);
        //            curChild[j].VarySize(0.25);
        //            curChild[j].VarySizeFull(roomsNr / 1000, moveDist*1);
        //           // curChild[j].VarySizeFull(roomsNr / 1000, moveDist * 0.01);
        //            curChild[j].MoveRandon(roomsNr / 1000, moveDist * 0.01);
        //            curChild[j].Inversion(0.05, 0);
        //            //if (RTools.Rnd.NextDouble() < 0.5)
        //            //{
        //                DevelopIndividual(curChild[j], 0.7, 0.1, 0.0, 0.0);
        //                curChild[j].RunSprings(sDamp);
        //            //}
        //            DevelopIndividual(curChild[j], propForce, propRate, repForce, repRate);
        //            curChild[j].RunSprings(sDamp);
        //            Children.Add(curChild[j]);
        //        }
        //    }
        //}

        //==============================================================================================

        private RChromosome[] CrossoverRandom()
        {
            int p = RParameter.p; //(int)((Form1)Application.OpenForms[0]).uD_p.Value;
            RChromosome[] curChild = new RChromosome[p];
            RChromosome[] tmpChild = new RChromosome[p];
            Double[] normFit = new Double[Parents.Count];
            int[] selIdx = new int[p]; // --- index of selected Parents
            int rndIdx;
            double sumS = 0;
            //----------------------------------------------------------
            for (int i = 0; i < p; i++)
            {
                selIdx[i] = RndGlobal.Rnd.Next(Parents.Count);
            }
            for (int i = 0; i < p; i++)
            {
                tmpChild[i] = new RChromosome(Parents[selIdx[i]]);
                curChild[i] = new RChromosome(Parents[selIdx[i]]);
                sumS += curChild[i].StrategyParam[0];
            }
            sumS = sumS / p;

            for (int i = 0; i < Parents[0].Gens.Count; i++)
            {
                rndIdx = RndGlobal.Rnd.Next(0, p);
                {
                    curChild[0].Gens[i] = tmpChild[rndIdx].Gens[i];
                    curChild[0].StrategyParam[0] = sumS;
                }
            }
            for (int i = 0; i < Parents[0].Gens.Count; i++)
            {
                rndIdx = RndGlobal.Rnd.Next(0, p);
                {
                    curChild[1].Gens[i] = tmpChild[rndIdx].Gens[i];
                    curChild[1].StrategyParam[0] = sumS;
                }
            }
            for (int i = 0; i < p; i++)
                tmpChild[i] = null;

            return curChild;
        }

        //==============================================================================================
        private RChromosome[] CrossoverTopo()
        {
            RChromosome[] curChild = new RChromosome[2];
            RChromosome[] tmpChild = new RChromosome[2];
            Double[] normFit = new Double[Parents.Count];
            int[] selIdx = new int[2]; // --- index of selected parentsForm
       //     Double sumFit = 0, testSum = 0;
            int rouletteCter, rndIdx1, rndIdx2;
       //     double rndVal;
            RGen child1, child2, parent1, parent2;
            int nrPolys = Parents[0].Gens.Count;
            //----------------------------------------------------------
            //------------------- prescale -----------------------------
      /*      Double a = 0, b = 0;
            Double maxFit, minFit, avgFit = 0;//, fitSum = 0;

            int fittestIdx = 0;// FindFittestParent(); // if polys are sorted we can take the first and last ploy for worst and best fittnes values
            maxFit = Parents[fittestIdx].FitDist;
            int worstIdx = Parents.Count - 1;// FindWorst();
            minFit = Parents[worstIdx].FitDist;

            for (int k = 0; k < Parents.Count; k++)
            {
                Parents[k].FitDist = Parents[k].FitDist - minFit;
                avgFit += Parents[k].FitDist;  // calculate the average fitness -> needed for fitness scaling
            }
            avgFit = avgFit / Parents.Count;

            Prescale(maxFit, avgFit, minFit, ref a, ref b);
            for (int k = 0; k < Parents.Count; k++)
            {
                Parents[k].FitDist = Scale(Parents[k].FitDist, a, b);
                if (Parents[k].FitDist < 0) Parents[k].FitDist = 0;
                // parentsForm[k].fitness = PowerLawScale(parentsForm[k].fitness, 1.005);
            }
            //----------------------------------------------------------

            for (int i = 0; i < Parents.Count; i++)
            {
                sumFit += Parents[i].FitDist;
            }

            // --- roulette wheel --------------------------------------
            for (int i = 0; i < 2; i++)
            {
                testSum = Parents[0].FitDist;
                if ((testSum == 0) || (double.IsNaN(testSum)))
                {
                    selIdx[i] = RTools.Rnd.Next(Parents.Count);
                    break;
                }
                selIdx[i] = 0;
                rouletteCter = 0;
                rndVal = RTools.Rnd.NextDouble() * sumFit;
                while (rndVal > testSum)
                { 
                    selIdx[i] = rouletteCter;
                    testSum += Parents[rouletteCter].FitDist;
                    rouletteCter++;
                    //break;
                }
            }*/


            for (int i = 0; i < 2; i++)
            {
                selIdx[i] = RndGlobal.Rnd.Next(Parents.Count);
            }
            //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
            tmpChild[0] = new RChromosome(Parents[selIdx[0]]); // temporary old parents
            tmpChild[1] = new RChromosome(Parents[selIdx[1]]);

            curChild[0] = new RChromosome(Parents[selIdx[0]]); // the new created chirldren
            curChild[1] = new RChromosome(Parents[selIdx[1]]);

            //-----------------------------------------------------
            rndIdx1 = RndGlobal.Rnd.Next(nrPolys - 1);
            int tmp = nrPolys - rndIdx1;
            rndIdx2 = rndIdx1 + RndGlobal.Rnd.Next(tmp);
            List<int> idxChild1 = new List<int>();
            List<int> idxChild2 = new List<int>();
            List<int> fehlerTest = new List<int>();

            // fill the index-List with parents current indexes 
            for (int i = 0; i < nrPolys; i++)
            {
                idxChild1.Add(Parents[selIdx[0]].Gens[i].Id);
                idxChild2.Add(Parents[selIdx[1]].Gens[i].Id);
                fehlerTest.Add(Parents[selIdx[0]].Gens[i].Id);
            }

            // crossover
            for (int i = 0; i < nrPolys; i++)
            {
                child1 = curChild[0].Gens[i];
                child2 = curChild[1].Gens[i];
                parent1 = tmpChild[0].Gens[i];
                parent2 = tmpChild[1].Gens[i];

                child1.Id = -1;
                child2.Id = -1;

                // exchange chromosome middle part 
                if ((rndIdx1 <= i) & (i <= rndIdx2))
                {
                    curChild[0].ReplaceGen(child1, parent2);
                    curChild[1].ReplaceGen(child2, parent1);
                    idxChild1.Remove(parent2.Id);
                    idxChild2.Remove(parent1.Id);
                }
            }

            //CheckDoubleEntries(curChild[0]);

            // keep the positions that are not used by exchange-procedure
            for (int i = 0; i < nrPolys; i++)
            {
                child1 = curChild[0].Gens[i];
                child2 = curChild[1].Gens[i];
                parent1 = tmpChild[0].Gens[i];
                parent2 = tmpChild[1].Gens[i];

                if (!((rndIdx1 <= i) & (i <= rndIdx2)))
                {
                    for (int j = 0; j < idxChild1.Count; j++)
                    {
                        if (idxChild1[j] == parent1.Id)
                        {
                            curChild[0].ReplaceGen(child1, parent1);
                            idxChild1.Remove(parent1.Id);
                            break;
                        }
                    }

                    for (int j = 0; j < idxChild2.Count; j++)
                    {
                        if (idxChild2[j] == parent2.Id)
                        {
                            curChild[1].ReplaceGen(child2, parent2);
                            idxChild2.Remove(parent2.Id);
                            break;
                        }
                    }
                }
            }

            //CheckDoubleEntries(curChild[0]);

            // fill remaining positions
            for (int i = 0; i < nrPolys; i++)
            {
                child1 = curChild[0].Gens[i];
                child2 = curChild[1].Gens[i];

                if (!((rndIdx1 <= i) & (i <= rndIdx2)))
                {
                    if (child1.Id == -1) // unbesetzte Position
                    {
                        for (int j = 0; j < nrPolys; j++) // check all parents
                        {
                            parent1 = tmpChild[0].Gens[j];
                            if (idxChild1[0] == parent1.Id) // find unused parents
                            {
                                curChild[0].ReplaceGen(child1, parent1);
                                idxChild1.Remove(parent1.Id);
                                break;
                            }
                        }
                    }

                    if (child2.Id == -1)
                    {
                        for (int j = 0; j < nrPolys; j++) // check all parents
                        {
                            parent2 = tmpChild[1].Gens[j];
                            if (idxChild2[0] == parent2.Id) // find unused parents
                            {
                                curChild[1].ReplaceGen(child2, parent2);
                                idxChild2.Remove(parent2.Id);
                                break;
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < 2; i++)
                tmpChild[i] = null;
            idxChild1.Clear();
            idxChild2.Clear();
            fehlerTest.Clear();
            idxChild1 = null;
            idxChild2 = null;
            fehlerTest = null;

            return curChild;
        }


        //==============================================================================================
        private RChromosome[] CrossoverPos()
        {
            RChromosome[] curChild = new RChromosome[2];
            RChromosome[] tmpChild = new RChromosome[2];
            Double[] normFit = new Double[Parents.Count];
            int[] selIdx = new int[2]; // --- index of selected Parents
            Double sumFit = 0, testSum = 0;
            int rouletteCter, rndIdx;
            double rndVal;

            //----------------------------------------------------------
            //------------------- prescale -----------------------------
            Double a = 0, b = 0;
            Double maxFit, minFit, avgFit = 0;//, fitSum = 0;

            int fittestIdx = 0;// FindFittestParent(); // if polys are sorted we can take the first and last ploy for worst and best fittnes values
            maxFit = Parents[fittestIdx].FitOverl;
            int worstIdx = Parents.Count - 1;// FindWorst();
            minFit = Parents[worstIdx].FitOverl;

            for (int k = 0; k < Parents.Count; k++)
            {
                Parents[k].FitOverl = Parents[k].FitOverl - minFit;
                avgFit += Parents[k].FitOverl;  // calculate the average fitness -> needed for fitness scaling
            }
            avgFit = avgFit / Parents.Count;

            Prescale(maxFit, avgFit, minFit, ref a, ref b);
            for (int k = 0; k < Parents.Count; k++)
            {
                Parents[k].FitOverl = Scale(Parents[k].FitOverl, a, b);
                //if (parentsForm[k].fitness < 0) parentsForm[k].fitness = 0;
                Parents[k].FitOverl = PowerLawScale(Parents[k].FitOverl, 1.005);
            }
            //----------------------------------------------------------

            for (int i = 0; i < Parents.Count; i++)
            {
                sumFit += Parents[i].FitOverl;
            }

            // --- roulette wheel --------------------------------------
            for (int i = 0; i < 2; i++)
            {
                testSum = Parents[0].FitOverl;
                selIdx[i] = 0;
                rouletteCter = 0;
                rndVal = RndGlobal.Rnd.NextDouble() * sumFit;
                while (rndVal > testSum)
                {
                    rouletteCter++;
                    selIdx[i] = rouletteCter;
                    testSum += Parents[rouletteCter].FitOverl;
                }
            }

            tmpChild[0] = new RChromosome(Parents[selIdx[0]]);
            tmpChild[1] = new RChromosome(Parents[selIdx[1]]);

            curChild[0] = new RChromosome(Parents[selIdx[0]]);
            curChild[1] = new RChromosome(Parents[selIdx[1]]);

            double sumS = (curChild[0].StrategyParam[0] + curChild[1].StrategyParam[0]) / 2; // intermediate recombinatíon
            curChild[0].StrategyParam[0] = sumS;
            curChild[1].StrategyParam[0] = sumS;

            rndIdx = RndGlobal.Rnd.Next(1, Parents[0].Gens.Count - 2);
            for (int i = 0; i < Parents[0].Gens.Count; i++)
            {
                if (i > rndIdx)
                {
                    curChild[0].Gens[i] = tmpChild[1].Gens[i];
                    curChild[1].Gens[i] = tmpChild[0].Gens[i];
                }
            }
            return curChild;
        }

        //==============================================================================================
        // ------- Linear Scaling - Goldberg 1989, S. 79 ----------------------------------------------
        private void Prescale(Double umax, Double uavg, Double umin, ref Double a, ref Double b)
        {
            Double fmultiple;
            Double delta;
            fmultiple = 1.2;//Convert.ToDouble(((Form1)Application.OpenForms[0]).tB_fitScale.Text);
            if (fmultiple == 1) fmultiple = 1.1;
            if (umin > (fmultiple * uavg - umax) / (fmultiple - 1.0))
            {

                delta = umax - uavg;
                a = (fmultiple - 1.0) * uavg / delta;
                b = uavg * (umax - fmultiple * uavg) / delta;
            }
            else
            {
                delta = uavg - umin;
                a = uavg / delta;
                b = -umin * uavg / delta;
            }
        }
        //==============================================================================================
        private Double Scale(Double u, Double a, Double b)
        {
            return (a * u + b);
        }

        //==============================================================================================
        private Double PowerLawScale(Double fitval, Double s_power)
        {
            return Math.Pow(fitval, s_power);
        }

        //==============================================================================================
        private void NextGeneration()
        {
            RChromosome curGenom;
            int nrPar = Parents.Count();
            Children.EvaluationOverlap();
            Children.Sort((x, y) => y.FitOverl.CompareTo(x.FitOverl));
            Parents.Clear();
            Parents = new RPopulation();

            FitnessNormalisation(Children);

            for (int i = 0; i < nrPar; i++)
            {
                curGenom = new RChromosome(Children[i]);
                Parents.Add(curGenom);
            }
        }

        //==============================================================================================
        //------------------- Roulette Wheel -----------------------------
        private List<RChromosome> RouletteWheel(List<RChromosome> inputList, int nrToSelect)
        {
            Double a = 0, b = 0;
            Double maxFit, minFit, avgFit = 0;//, fitSum = 0;
            int selIdx; // --- index of selected individuals
            List<RChromosome> roulettedList = new List<RChromosome>();
            Double sumFit = 0, testSum = 0;
            int rouletteCter;
            double rndVal;
            // List have to sorted before !!! 
            int fittestIdx = 0;// FindFittestParent(); // if polys are sorted we can take the first and last ploy for worst and best fittnes values
            maxFit = inputList[fittestIdx].TmpFit;
            int worstIdx = inputList.Count - 1;// FindWorst();
            minFit = inputList[worstIdx].TmpFit;

            for (int k = 0; k < inputList.Count; k++)
            {
                //inputList[k].TmpFit = inputList[k].TmpFit - minFit;
                avgFit += inputList[k].TmpFit;  // calculate the average fitness -> needed for fitness scaling
            }
            avgFit = avgFit / inputList.Count;
            /*for (int k = 0; k < inputList.Count; k++)
            {
                inputList[k].selProb = (inputList[k].TmpFit / avgFit) * (nrToSelect / inputList.Count);
                inputList[k].nrCopies = (int)(inputList[k].selProb * nrToSelect);
            }*/
            /*Prescale(maxFit, avgFit, minFit, ref a, ref b);
            for (int k = 0; k < inputList.Count; k++)
            {
                inputList[k].TmpFit = Scale(inputList[k].TmpFit, a, b);
                if (inputList[k].TmpFit < 0) inputList[k].TmpFit = 0;
                // parentsForm[k].fitness = PowerLawScale(parentsForm[k].fitness, 1.005);
            }*/


            //----------------------------------------------------------
            for (int i = 0; i < inputList.Count; i++)
            {
                sumFit += inputList[i].TmpFit;
            }

            // --- roulette wheel --------------------------------------
            for (int i = 0; i < nrToSelect; i++)
            {
                testSum = inputList[0].TmpFit;
                if ((testSum == 0) || (double.IsNaN(testSum)))
                {
                    selIdx = RndGlobal.Rnd.Next(inputList.Count);
                }
                else
                {
                    selIdx = 0;
                    rouletteCter = 0;
                    rndVal = RndGlobal.Rnd.NextDouble() * sumFit;
                    while (rndVal > testSum)
                    {
                        selIdx = rouletteCter;
                        testSum += inputList[rouletteCter].TmpFit;
                        rouletteCter++;
                    }
                }
                roulettedList.Add(inputList[selIdx]);
            }
            return roulettedList;
        }
        
        //==============================================================================================
        //private void NextGenerationProportionate(RPopulation parents, RPopulation children)
        //{
        //    RChromosome curGenom;
        //    int nrPar = RParameter.popSize;// parents.Count();
        //    int midChilds = (int)Math.Round(children.Count / 2.0, 0);  //  nrPar -1;//
        //    int midNrPar = (int)Math.Round(nrPar / 1.0, 0) - 1;  //  nrPar -1;//midNrPar = (int)Math.Round(nrPar * .4, 0); //
        //    double thresH = RParameter.roomsNr * 1.5;//(double)((Form1)Application.OpenForms[0]).uD_rooms.Value * 1.5;
        //    int nrRooms = (int)RParameter.roomsNr; // (int)((Form1)Application.OpenForms[0]).uD_rooms.Value;

        //    FitnessNormalisation(children);

        //    for (int i = 0; i < parents.Count; i++)
        //    {
        //        parents[i] = null;
        //    }
        //    parents.Clear();

        //    // --- Overlap-Evaluation ------------------------------
        //    children.EvaluationOverlap();
        //    //children.EvaluationDist();
        //    children.Sort((x, y) => y.FitOverl.CompareTo(x.FitOverl));
        //    if (children[0].FitRealDist < thresH) midNrPar = (int)Math.Round(nrPar * .4, 0); //
        //    int endNrPar = nrPar - midNrPar;
        //    for (int i = 0; i < endNrPar; i++)
        //    {
        //        // --- copy selected child to parents -------
        //        curGenom = new RChromosome(children[i]);
        //        parents.Add(curGenom);
        //    }

        //    // --- Distances-Evaluation ---------------------------
        //    //   children.EvaluationDist();
        //    children.Sort((x, y) => y.FitDist.CompareTo(x.FitDist));

        //    for (int i = 0; i < midNrPar; i++)
        //    {
        //        // --- copy selected child to parents -------
        //        curGenom = new RChromosome(children[i]);
        //        parents.Add(curGenom);
        //    }

        //    //  parents.EvaluationOverlap();
        //    //  parents.EvaluationDist();

        //    parents.Sort((x, y) => y.FitOverl.CompareTo(x.FitOverl));
        //}

        //==============================================================================================
        private void FitnessNormalisation(RPopulation children)
        {
            RChromosome curGenom;
            int nrRooms = (int)RParameter.roomsNr;// ((Form1)Application.OpenForms[0]).uD_rooms.Value;
            double MaxOverlap = (double)((BoundingRect.Width + BoundingRect.Height) * (nrRooms) * 10);// MyTool.Faculty(NrRooms) * (double)(Canvas.Width * Canvas.Height / NrRooms);
            double MaxDist = (double)((double)RChromosome.NrSprings * ((double)BoundingRect.Width / 100.0 + (double)BoundingRect.Height / 100.0)); // (Canvas.Width * Canvas.Height) / 1000;// Math.Sqrt(Math.Pow(REvo.Canvas.Width, 2) +

            for (int i = 0; i < children.Count; i++)
            {
                curGenom = children[i];
                curGenom.NormFitOverl = curGenom.FitRealOverl / MaxOverlap;
                //curGenom.NormFitDist =  curGenom.FitRealDist / MaxDist; // 1 - curGenom.FitDist; //
            }
        }

        //==============================================================================================
        //-------------grow the Polys-------------------------------------------------------------------
        private void GrowPolys(RPopulation growPop)
        {
            double allArea;
            double width, height;
            double oneArea;
            double[] areas;
            int rndSizeSpan = 10;
            int substract = 00;
            allArea = Canvas.Area;// (BoundingRect.Width - 1) * (BoundingRect.Height - 1);

            float density = (float)RParameter.density / 100;
            oneArea = ((allArea / growPop[0].Gens.Count) - substract) * density;

            for (int i = 0; i < growPop[0].Gens.Count; i++) //--- sizes for the polys ---
            {
                //sumArea = sumArea + Parents[0].Gens[i].Area;
                growPop[0].Gens[i].Area = oneArea *(RndGlobal.Rnd.NextDouble()  + 0.5);
                growPop[0].Gens[i].MinSide = Math.Sqrt(growPop[0].Gens[i].Area) * 0.75;// 10.0;
                //width = Math.Sqrt(growPop[0].Gens[i].Area) + RndGlobal.Rnd.Next(-rndSizeSpan, rndSizeSpan);
                //if (width > (Canvas.Width / 2)) width = Canvas.Width / 2;
                //height = growPop[0].Gens[i].Area / width;
                //growPop[0].Gens[i].Width = width;
                //growPop[0].Gens[i].Height = height;
            }
            growPop[0].Gens[0].FillColor = Color.Green;
            growPop[0].Gens[0].Attractor = true;
            growPop[0].Gens[0].Locked = true;
            growPop[0].Gens[0].MinSide = 10;

            // --- Position Individs ---
            for (int k = 1; k < growPop.Count; k++) //--- copy the sizes to the other individuals ---
            {
                for (int i = 0; i < growPop[0].Gens.Count; i++)
                {
                    growPop[k].Gens[i].Area = growPop[0].Gens[i].Area;
                    growPop[k].Gens[i].MinSide = growPop[0].Gens[i].MinSide;
                    //width = Math.Sqrt(growPop[0].Gens[i].Area) + RndGlobal.Rnd.Next(-rndSizeSpan, rndSizeSpan);
                    //if (width > (Canvas.Width / 2)) width = Canvas.Width / 2;
                    //height = growPop[k].Gens[i].Area / width;
                    //growPop[k].Gens[i].Width = width;
                    //growPop[k].Gens[i].Height = height;
                }
            }

        }
    }

    //==============================================================================================
    //==============================================================================================
    //==============================================================================================
     static class Ext  
     {  
         //==============================================================================================
         public static void QuickShuffle<T>(this IList<T> list)  
         {  
             var rng = new Random();  
             var n = list.Count;  
             while (n > 1)  
             {  
                 n--;  
                 var k = rng.Next(n + 1);  
                 var value = list[k];  
                 list[k] = list[n];  
                 list[n] = value;  
             }  
         }  
     }
}
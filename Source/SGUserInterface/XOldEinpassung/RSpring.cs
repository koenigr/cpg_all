﻿/**
 * Kremlas/MOES
 * \file RSpring.cs
 *
 * <A HREF="http://www.entwurfsforschung.de">
 * Copyright (C) 2011 by Dr. Reinhard Koenig. Alle Rechte vorbehalten.
 * Schutzvermerk nach DIN ISO 16016 beachten
 * </A>
 * koenig@entwurfsforschung.de
 * $Author: koenig $
 * $Rev: 390 $
 * $Date: 2011-02-11 11:12:42 +0100 (Fr, 11. Feb 2011) $
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Topology.Geometries;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Troschuetz.Random.Distributions.Continuous;
using CPlan.Geometry;

namespace CPlan.UrbanPattern
{
    public class RSpring
    {
        private int m_fromGenId;
        private int m_toGenId;
        private RGen m_fromGen, m_toGen;
        private double m_damping = 0.1;
        private double m_doorMover = 0;
        private NormalDistribution m_normDist = new NormalDistribution();

        //==============================================================================================
        public RSpring(RGen theFromGen, RGen theToGen)
        {
            m_fromGenId = theFromGen.Id;
            m_toGenId = theToGen.Id;
        }
        //==============================================================================================
        // --- copy constructor -----------------------------------------------------------
        public RSpring(RSpring prevGen)
        {
            m_fromGenId = prevGen.m_fromGenId;
            m_toGenId = prevGen.m_toGenId;
        }

        //==============================================================================================
        public void update(List<RGen> Gens, double damp)
        {
            m_damping = damp;
            Vector2D translation = new Vector2D(0, 0);
            for (int i = 0; i < Gens.Count; i++)
            {
                if (Gens[i].Id == m_fromGenId) m_fromGen = Gens[i];
                if (Gens[i].Id == m_toGenId)   m_toGen   = Gens[i];
            }

            if (m_toGen == null)
                m_toGen = null;

            translation = CalcSpringVector(m_fromGen, m_toGen);
           // translation = TranslationVectorFrauke(m_toGen, m_fromGen);

            // --- x or y ----------------------------------------
            if (Math.Abs(translation.X) > Math.Abs(translation.Y))
            {
                translation.Y = 0;
            } else {
                translation.X = 0;
            }

            translation.Multiply(0.5);
            translation.Multiply(1 - m_damping);
            //if (!m_fromGen.Locked) m_fromGen.Velocity += translation;
            translation.Invert();
            //if (!m_toGen.Locked) m_toGen.Velocity += translation;//
        }

        //==============================================================================================
        // Translation Calculation.
        public static Vector2D TranslationVectorFrauke(RGen fistPoly, RGen secondPoly)
        {
            Vector2D TransVector;
            double CenterAX = fistPoly.Center.X;
            double CenterAY = fistPoly.Center.Y;
            double CenterBX = secondPoly.Center.X;
            double CenterBY = secondPoly.Center.Y;

            double LengthAX = fistPoly.Width;// 
            double LengthAY = fistPoly.Height;//
            double LengthBX = secondPoly.Width;//
            double LengthBY = secondPoly.Height;//

            Vector2D RectA = new Vector2D(CenterAX, CenterAY);
            Vector2D RectB = new Vector2D(CenterBX, CenterBY);


            Vector2D a_translation = RectB - RectA;
            double dXCenterPts = Math.Abs(a_translation.X) - LengthAX * 0.5 - LengthBX * 0.5;
            double dYCenterPts = Math.Abs(a_translation.Y) - LengthAY * 0.5 - LengthBY * 0.5;

            double trans_sollX = 0.0;
            double trans_sollY = 0.0;

            //Translation zur näheren Seite und nicht mehr zwingend an die Ecke
            if (dXCenterPts < dYCenterPts)
            {
                if (dXCenterPts < 0.0)
                {
                    trans_sollX = Math.Abs(a_translation.X);
                    trans_sollY = LengthAY * 0.5 + LengthBY * 0.5;
                }
                else
                {
                    trans_sollX = LengthAX * 0.5 + LengthBX * 0.5 - LengthAX * 0.1;
                    trans_sollY = LengthAY * 0.5 + LengthBY * 0.5;
                }
            }
            else
            {
                if (dYCenterPts < 0.0)
                {
                    trans_sollX = LengthAX * 0.5 + LengthBX * 0.5;
                    trans_sollY = Math.Abs(a_translation.Y);
                }
                else
                {
                    trans_sollX = LengthAX * 0.5 + LengthBX * 0.5;
                    trans_sollY = LengthAY * 0.5 + LengthBY * 0.5 - LengthAY * 0.1;
                }
            }

            //Verschiebung des Centers
            if (a_translation.X < 0.0)
            {
                if (a_translation.Y < 0.0) // 3. Quadrant
                {
                    //Verschiebungsvektor
                    TransVector = new Vector2D(CenterAX - trans_sollX - CenterBX, CenterAY - trans_sollY - CenterBY);
                    CenterBX = CenterAX - trans_sollX;
                    CenterBY = CenterAY - trans_sollY;
                }
                else // 2. Quadrant
                {
                    //Verschiebungsvektor
                    TransVector = new Vector2D(CenterAX - trans_sollX - CenterBX, CenterAY + trans_sollY - CenterBY);
                    CenterBX = CenterAX - trans_sollX;
                    CenterBY = CenterAY + trans_sollY;
                }
            }
            else
            {
                if (a_translation.Y < 0.0) // 4. Quadrant
                {
                    //Verschiebungsvektor
                    TransVector = new Vector2D(CenterAX + trans_sollX - CenterBX, CenterAY - trans_sollY - CenterBY);
                    CenterBX = CenterAX + trans_sollX;
                    CenterBY = CenterAY - trans_sollY;
                }
                else // 1. Quadrant
                {
                    //Verschiebungsvektor
                    TransVector = new Vector2D(CenterAX + trans_sollX - CenterBX, CenterAY + trans_sollY - CenterBY);
                    CenterBX = CenterAX + trans_sollX;
                    CenterBY = CenterAY + trans_sollY;
                }
            }
            return new Vector2D(TransVector.X, TransVector.Y); ;
        }

        //==============================================================================================
        private Vector2D CalcSpringVector(RGen fistPoly, RGen secondPoly)
        {
            Vector2D distVect = new Vector2D();
            Vector2D overlVect = new Vector2D();
            double minAX, maxAX, minBX, maxBX;
            double minAY, maxAY, minBY, maxBY;
            minAX = fistPoly.Center.X - (fistPoly.Width / 2);
            maxAX = fistPoly.Center.X + (fistPoly.Width / 2);
            minBX = secondPoly.Center.X - (secondPoly.Width / 2);
            maxBX = secondPoly.Center.X + (secondPoly.Width / 2);

            minAY = fistPoly.Center.Y - (fistPoly.Height / 2) ;
            maxAY = fistPoly.Center.Y + (fistPoly.Height / 2);
            minBY = secondPoly.Center.Y - (secondPoly.Height / 2) ;
            maxBY = secondPoly.Center.Y + (secondPoly.Height / 2);

            // --- test X-direction 
            if (maxAX < minBX)
            {
                distVect.X = minBX - maxAX;
            }
            else if (maxBX < minAX)
            {
                distVect.X = -(minAX - maxBX);
            }
            else
            {
                distVect.X = 0;
            }

            // --- test Y-direction 
            if (maxAY < minBY)
            {
                distVect.Y = minBY - maxAY;
            }
            else if (maxBY < minAY)
            {
                distVect.Y = -(minAY - maxBY);
            }
            else
            {
                distVect.Y = 0;
            }

            // --- add the vector for overlappings to the spring vector
            overlVect = AdditionalVector(minAX, maxAX, minBX, maxBX, minAY, maxAY, minBY, maxBY, fistPoly, secondPoly);
            distVect.Add(overlVect);

            return distVect;
        }

        //============================================================================================================================================
        private Vector2D AdditionalVector(double minAX, double maxAX, double minBX, double maxBX, double minAY, double maxAY, double minBY, double maxBY, RGen fistPoly, RGen secondPoly)
        {
            Vector2D overlVect = new Vector2D(0, 0);
            double addDist = RParameter.addDist;// (double)((Form1)Application.OpenForms[0]).uD_RLap.Value; // minimum required distance

            // --- add a force to ensure the overlappings of the rectangles
            // --- overlVect is negative if the intervals overlap
            // --- consider X 
            if (minAX < minBX)
            {
                overlVect.X = minBX - maxAX;
            }
            else
            {
                overlVect.X = minAX - maxBX;
            }
            // --- consider Y
            if (minAY < minBY)
            {
                overlVect.Y = minBY - maxAY;
            }
            else
            {
                overlVect.Y = minAY - maxBY;
            }

            // --- test to which vector the additional distance has to be added ---
            if ((overlVect.X < 0) & (overlVect.Y > 0)) // overlap only in X-direction
            {
                if (Math.Abs(overlVect.X) < addDist) overlVect.X = addDist + overlVect.X;
                else overlVect.X = 0;
                overlVect.Y = 0;
            }
            else if ((overlVect.X > 0) & (overlVect.Y < 0)) // overlap only in Y-direction
            {
                if (Math.Abs(overlVect.Y) < addDist) overlVect.Y = addDist + overlVect.Y;
                else overlVect.Y = 0;
                overlVect.X = 0;
            }
            else if ((overlVect.X > 0) & (overlVect.Y > 0)) // no overlap at all
            {
                overlVect.X = addDist;
                overlVect.Y = addDist;
            }
            else
            {
                overlVect.X = 0;
                overlVect.Y = 0;
            }

            return overlVect;
        }

        //============================================================================================================================================================================================
        //============================================================================================================================================================================================
        //============================================================================================================================================================================================
        #region doors
        ////public void CreateDoors(List<RGen> Gens, ref RDoors curDoor)
        ////{
        ////    //RDoors curDoor = null;
        ////    for (int i = 0; i < Gens.Count; i++)
        ////    {
        ////        if (Gens[i].Id == m_fromGenId) m_fromGen = Gens[i];
        ////        if (Gens[i].Id == m_toGenId) m_toGen = Gens[i];
        ////    }
        ////    FindDoors(m_fromGen, m_toGen, ref curDoor);
        ////}

        ////==============================================================================================
        //private void FindDoors(RGen fistPoly, RGen secondPoly, ref RDoors curDoor)//, Vector overlVect)
        //{
        //    GraphicsPath doorPath = new GraphicsPath();
        //    PointF[] doorPoints = new PointF[4];
        //    Vector2D overlVect = new Vector2D(0, 0);
        //    Vector2D doorVectA = new Vector2D(0, 0);
        //    Vector2D doorVectB = new Vector2D(0, 0);
        //    Vector2D paP0, paP1, pbP0, pbP1;
        //    paP0 = fistPoly.Points[0];
        //    paP1 = fistPoly.Points[1];
        //    pbP0 = secondPoly.Points[0];
        //    pbP1 = secondPoly.Points[1];
        //    //float doorsDepth = 20;
        //    int doorWithPara = (RParameter.addDist-10) / 2;
        //    double minAX, maxAX, minBX, maxBX;
        //    double minAY, maxAY, minBY, maxBY;
        //    minAX = fistPoly.Center.X - (fistPoly.Width / 2);
        //    maxAX = fistPoly.Center.X + (fistPoly.Width / 2);
        //    minBX = secondPoly.Center.X - (secondPoly.Width / 2);
        //    maxBX = secondPoly.Center.X + (secondPoly.Width / 2);
        //    minAY = fistPoly.Center.Y - (fistPoly.Height / 2);
        //    maxAY = fistPoly.Center.Y + (fistPoly.Height / 2);
        //    minBY = secondPoly.Center.Y - (secondPoly.Height / 2);
        //    maxBY = secondPoly.Center.Y + (secondPoly.Height / 2);

        //    // --- overlVect is negative if the intervals overlap
        //    // --- consider X 
        //    if (minAX < minBX)
        //    {
        //        overlVect.X = minBX - maxAX;
        //    }
        //    else
        //    {
        //        overlVect.X = minAX - maxBX;
        //    }
        //    // --- consider Y
        //    if (minAY < minBY)
        //    {
        //        overlVect.Y = minBY - maxAY;
        //    }
        //    else
        //    {
        //        overlVect.Y = minAY - maxBY;
        //    }

        //    if (Math.Abs(overlVect.Y) > Math.Abs(overlVect.X)) // overlap in y-direction
        //    {
        //        if ((minAY <= minBY) & (maxAY >= maxBY))
        //        {
        //            doorVectA.Y = maxBY;
        //            doorVectB.Y = minBY;
        //        }
        //        else if ((minAY >= minBY) & (maxAY <= maxBY))
        //        {
        //            doorVectA.Y = maxAY;
        //            doorVectB.Y = minAY;
        //        }
        //        else if ((maxBY > minAY) & (maxBY < maxAY))
        //        {
        //            doorVectA.Y = maxBY;
        //            doorVectB.Y = minAY;
        //        }
        //        else if ((minBY > minAY) & (minBY < maxAY))
        //        {
        //            doorVectA.Y = maxAY;
        //            doorVectB.Y = minBY;
        //        }

        //        //          ScaleDoor(ref doorVectA, ref doorVectB, new Vector(0, doorWithPara), new Vector(0, -3));

        //        // --- decide to which edge the door is assigned ---
        //        if (Math.Abs(fistPoly.Points[1].X - secondPoly.Points[0].X) < Math.Abs(fistPoly.Points[0].X - secondPoly.Points[1].X))
        //        {
        //            curDoor = new RDoors("vertical", fistPoly.Points[1], doorVectA, doorVectB, new Vector2D(0, doorWithPara), new Vector2D(0, -3));
        //        }
        //        else
        //        {
        //            curDoor = new RDoors("vertical", fistPoly.Points[0], doorVectA, doorVectB, new Vector2D(0, doorWithPara), new Vector2D(0, -3));

        //        }

        //    }
        //    else // overlap in x-direction
        //    {
        //        if ((minAX <= minBX) & (maxAX >= maxBX))
        //        {
        //            doorVectA.X = maxBX;
        //            doorVectB.X = minBX;
        //        }
        //        else if ((minAX >= minBX) & (maxAX <= maxBX))
        //        {
        //            doorVectA.X = maxAX;
        //            doorVectB.X = minAX;
        //        }
        //        else if ((maxBX > minAX) & (maxBX < maxAX))
        //        {
        //            doorVectA.X = maxBX;
        //            doorVectB.X = minAX;
        //        }
        //        else if ((minBX > minAX) & (minBX < maxAX))
        //        {
        //            doorVectA.X = maxAX;
        //            doorVectB.X = minBX;
        //        }

        //        //   ScaleDoor(ref doorVectA, ref doorVectB, new Vector(doorWithPara, 0), new Vector(-3, 0));

        //        // --- decide to which edge the door is assigned ---
        //        if (Math.Abs(fistPoly.Points[2].Y - secondPoly.Points[0].Y) < Math.Abs(fistPoly.Points[1].Y - secondPoly.Points[3].Y))
        //        {
        //            curDoor = new RDoors("horizontal", fistPoly.Points[2], doorVectA, doorVectB, new Vector2D(doorWithPara, 0), new Vector2D(-3, 0));
        //        }
        //        else
        //        {
        //            curDoor = new RDoors("horizontal", fistPoly.Points[1], doorVectA, doorVectB, new Vector2D(doorWithPara, 0), new Vector2D(-3, 0));
        //        }
        //    }
        //}

        ////==============================================================================================
        //// --- scale the door ----------------------------
        //private void ScaleDoor(ref Vector2D doorVectA, ref Vector2D doorVectB, Vector2D maxDoor, Vector2D minDoor)
        //{
        //    double lengthDoor = doorVectA.DistanceTo(doorVectB);
        //    double lengthInsert = maxDoor.Magnitude;
        //    double lengthMin = minDoor.Magnitude;
        //    Vector2D doorVect = doorVectB - doorVectA;
        //    Vector2D possibleMoveV = new Vector2D(doorVect);
        //    possibleMoveV.Multiply(((lengthDoor - lengthMin) - 2 * lengthInsert) / (lengthDoor - lengthMin)); // find the possible movement vector
        //    possibleMoveV.Multiply(0.5);                                      // halt to the right or to the left

        //    if (lengthDoor > (lengthInsert * 2))
        //    {
        //        Vector2D doorRV = maxDoor;
        //        Vector2D midPt = doorVectA + doorVectB;
        //        midPt.Multiply(0.5); // get the middel point of the door
        //        possibleMoveV.Multiply(m_doorMover);//REvo.Rnd.NextDouble()*2 -1 ); // change the door position - randomly...
        //        midPt += possibleMoveV;
        //        doorVectA = midPt - doorRV;
        //        doorVectB = midPt + doorRV;
        //    }
        //    else if (lengthDoor > lengthInsert)
        //    {
        //        Vector2D doorRV = doorVectB - doorVectA - minDoor;// new Vector(0, -3);
        //        doorVectB = doorVectA + doorRV;
        //        doorRV = doorVectA - doorVectB - minDoor; // new Vector(0, -3);
        //        doorVectA = doorVectB + doorRV;
        //    }
        //    else // delete the door if it is too small
        //    {
        //        doorVectA = new Vector2D(0, 0);
        //        doorVectB = new Vector2D(0, 0);
        //    }
        //}

        ////==============================================================================================
        //public void UpdateDoorMover(double seed)
        //{
        //    ALFGenerator newGenerator = new ALFGenerator((int)DateTime.Now.Ticks);
        //    m_normDist = new NormalDistribution(newGenerator);
        //    m_normDist.Sigma = 0.1;
        //    m_doorMover += m_normDist.NextDouble() - 1;//REvo.Rnd.NextDouble()*2 -1;
        //    if (m_doorMover > 1) m_doorMover = 1;
        //    if (m_doorMover < -1) m_doorMover = -1;
        //}
        #endregion
    }
}

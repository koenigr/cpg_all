﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cloo;
using Cloo.Bindings;

namespace CPlan.CommonLib
{

    public class GPUIsovistField
    {
        IsovistServiceField2D isovistField;

        ComputeProgram program;
        ComputePlatform platform;
        IList<ComputeDevice> devices;
        ComputeContextPropertyList properties;
        ComputeContext context;

        ComputeBuffer<float> wallP1X_buf;
        ComputeBuffer<float> wallP1Y_buf;
        ComputeBuffer<float> wallP2X_buf;
        ComputeBuffer<float> wallP2Y_buf;
        ComputeBuffer<float> area_buf;
        ComputeBuffer<float> perimeter_buf;
        ComputeBuffer<float> minRadial_buf;
        ComputeBuffer<float> maxRadial_buf;
        ComputeBuffer<float> occlusivity_buf;
        ComputeBuffer<bool> activeCells_buf;

        ComputeKernel kernel;
        ComputeEventList eventList;
        ComputeCommandQueue commands;

        #region ComputationText
        string clIsovistFieldCalculation = @"

float2 LineIntersect(float L1P1X, float L1P1Y, float L1P2X, float L1P2Y,
float L2P1X, float L2P1Y,float L2P2X, float L2P2Y)
{
    //See http://local.wasp.uwa.edu.au/~pbourke/geometry/lineline2d/
    
    float2 ptIntersection;
    ptIntersection.x = MAXFLOAT;
    ptIntersection.y = MAXFLOAT;
    
    float d = (L2P2Y - L2P1Y) * (L1P2X - L1P1X) - (L2P2X - L2P1X) * (L1P2Y - L1P1Y);
    float n_a = (L2P2X - L2P1X) * (L1P1Y - L2P1Y) - (L2P2Y - L2P1Y) * (L1P1X - L2P1X);
    float n_b = (L1P2X - L1P1X) * (L1P1Y - L2P1Y) - (L1P2Y - L1P1Y) * (L1P1X - L2P1X);

    if (d == 0)
        return ptIntersection;

    float ua = n_a / d;
    float ub = n_b / d;

    if (ua >= 0.0 && ua <= 1.0 && ub >= 0.0 && ub <= 1.0)
    {
        ptIntersection.x = L1P1X + (ua * (L1P2X - L1P1X));
        ptIntersection.y = L1P1Y + (ua * (L1P2Y - L1P1Y));
        return ptIntersection;
    }
    return ptIntersection;
}


kernel void CalculateIsovistField(
    global  read_only float* wallP1X,
    global  read_only float* wallP1Y,
    global  read_only float* wallP2X,
    global  read_only float* wallP2Y,
    global write_only float* area,
    global write_only float* perimeter,    
    global write_only float* minRadial,
    global write_only float* maxRadial,
    global write_only float* occlusivity,
    global write_only bool* activeCells,

    float startPX,
    float startPY,
    float cellSize,
    int countX,
    int countY,
    float exactness,
    int wallCount)

{
    int gID0 = get_global_id(0);
    int gID1 = get_global_id(1);
    
    if ( ( gID0 < countX) && (gID1 < countY) && (activeCells[gID1 * countX + gID0]) )
    {
        int rayCount = (int) (2*M_PI / exactness);
        //float2 perimeterPoints[rayCount];
        float2 curPerimeterPoint;
        float2 oldPerimeterPoint;
        float2 firstPerimeterPoint;
        float2 rayP1 = (float2){ startPX+cellSize*gID0+cellSize/2, startPY+cellSize*gID1+cellSize/2 };
        float tmp_minRadial = MAXFLOAT;
        float tmp_maxRadial = -MAXFLOAT;
        float tmp_area = 0.0f;
        float tmp_perimeter = 0.0f;
        float tmp_occlusivity = 0.0f;
        
        int curWall = 0;
        int prevWall = 0; //für occlusivity
        
        //Isovist berechnen:
        for (int i = 0; i< rayCount; i++)
        {
            float angle = i*exactness;
            float l = 10000;
            float2 rayP2;        
            rayP2.x = rayP1.x + sin(angle) * l;
            rayP2.y = rayP1.y + cos(angle) * l;

            float2 cutPoint;
            float mindist = MAXFLOAT;
            bool cutted = false;
            
    
            for (int i = 0; i < wallCount; i++)
            {
                bool intersects = true;
                float2 iPoint = LineIntersect(rayP1.x, rayP1.y, rayP2.x, rayP2.y, wallP1X[i], wallP1Y[i], wallP2X[i], wallP2Y[i]);
                if ((iPoint.x == MAXFLOAT) && (iPoint.y == MAXFLOAT))
                    intersects = false;

                if (intersects)
                {
                    float delta_x = rayP1.x - iPoint.x;
                    float delta_y = rayP1.y - iPoint.y;
                    float dist = sqrt(delta_x * delta_x + delta_y * delta_y);

                    if (dist < mindist)
                    {
                        cutPoint = iPoint;
                        cutted = true;
                        mindist = dist;
                        curWall = i;
                    }
                }
            }
            if (!cutted)
                cutPoint = rayP2;
            
            if (i == 0)
                firstPerimeterPoint = cutPoint;                

            oldPerimeterPoint = curPerimeterPoint;
            curPerimeterPoint = cutPoint;
            
            if (i != 0)
            {
                tmp_area = tmp_area + (curPerimeterPoint.x*oldPerimeterPoint.y-curPerimeterPoint.y*oldPerimeterPoint.x);
                tmp_perimeter = tmp_perimeter + distance(curPerimeterPoint, oldPerimeterPoint);
                if (curWall != prevWall)
                {
                    tmp_occlusivity += distance(curPerimeterPoint, oldPerimeterPoint);
                    prevWall = curWall;
                }
            }
            
            float dist = distance(rayP1, cutPoint);
            if (dist < tmp_minRadial)
                tmp_minRadial = dist;
            if (dist > tmp_maxRadial)
                tmp_maxRadial = dist;

            

        }

        tmp_area = tmp_area + (firstPerimeterPoint.x*curPerimeterPoint.y-firstPerimeterPoint.y*curPerimeterPoint.x);
        tmp_area = tmp_area/2;
        tmp_perimeter = tmp_perimeter + distance(firstPerimeterPoint, curPerimeterPoint);

        minRadial[gID1 * countX + gID0] = tmp_minRadial;
        maxRadial[gID1 * countX + gID0] = tmp_maxRadial;
        area[gID1 * countX + gID0] = tmp_area;
        perimeter[gID1 * countX + gID0] = tmp_perimeter;
        occlusivity[gID1 * countX + gID0] = tmp_occlusivity;
    }
}
";

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructor

        public GPUIsovistField(IsovistServiceField2D _isovistField)
        {
            isovistField = _isovistField;
            Initialize();
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        public void UpdateComputeBufferWalls()
        {
            wallP1X_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, isovistField.wallP1X);
            wallP1Y_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, isovistField.wallP1Y);
            wallP2X_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, isovistField.wallP2X);
            wallP2Y_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, isovistField.wallP2Y);
        }

        public void UpdateWalls()
        {
            if (isovistField.wallP1X.Length <= 0)
                return;

            wallP1X_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, isovistField.wallP1X);
            wallP1Y_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, isovistField.wallP1Y);
            wallP2X_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, isovistField.wallP2X);
            wallP2Y_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, isovistField.wallP2Y);
            kernel.SetMemoryArgument(0, wallP1X_buf);
            kernel.SetMemoryArgument(1, wallP1Y_buf);
            kernel.SetMemoryArgument(2, wallP2X_buf);
            kernel.SetMemoryArgument(3, wallP2Y_buf);
        }

        public void UpdateComputeBufferMeasures()
        {
            //abhängig von der Anzahl aktiver GridCells:
            int cnt = isovistField.NumberOfActiveCells;
            activeCells_buf = new ComputeBuffer<bool>(context, ComputeMemoryFlags.ReadOnly | ComputeMemoryFlags.CopyHostPointer, isovistField.ActiveCells);
            area_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, isovistField.NumberOfCells);
            minRadial_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, isovistField.NumberOfCells);
            maxRadial_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, isovistField.NumberOfCells);
            perimeter_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, isovistField.NumberOfCells);
            occlusivity_buf = new ComputeBuffer<float>(context, ComputeMemoryFlags.WriteOnly, isovistField.NumberOfCells);
        }

        public void Initialize()
        {
            isovistField.UpdateWallCoordinates();

            try
            {
                platform = ComputePlatform.Platforms[0];
                devices = new List<ComputeDevice>();
                devices.Add(platform.Devices[0]);
                properties = new ComputeContextPropertyList(platform);
                context = new ComputeContext(devices, properties, null, IntPtr.Zero);
                eventList = new ComputeEventList();
                commands = new ComputeCommandQueue(context, context.Devices[0], ComputeCommandQueueFlags.None);

                if (isovistField.wallP1X.Length > 0)
                    UpdateComputeBufferWalls();

                UpdateComputeBufferMeasures();
                program = new ComputeProgram(context, clIsovistFieldCalculation);

                program.Build(null, null, null, IntPtr.Zero);
            }
            catch (System.Exception ex)
            {
                //MyTrace.Trace(String.Format(ex.ToString()));
                //MyTrace.Trace(program.GetBuildLog(context.Devices[0]));
            }

            kernel = program.CreateKernel("CalculateIsovistField");
            //Übergabe der Parameter:
            if (isovistField.wallP1X.Length > 0)
            {
                kernel.SetMemoryArgument(0, wallP1X_buf);
                kernel.SetMemoryArgument(1, wallP1Y_buf);
                kernel.SetMemoryArgument(2, wallP2X_buf);
                kernel.SetMemoryArgument(3, wallP2Y_buf);

                kernel.SetMemoryArgument(4, area_buf);
                kernel.SetMemoryArgument(5, perimeter_buf);
                kernel.SetMemoryArgument(6, minRadial_buf);
                kernel.SetMemoryArgument(7, maxRadial_buf);
                kernel.SetMemoryArgument(8, occlusivity_buf);
                kernel.SetMemoryArgument(9, activeCells_buf);
            }
            kernel.SetValueArgument(10, (float)isovistField.StartP.X);
            kernel.SetValueArgument(11, (float)isovistField.StartP.Y);
            kernel.SetValueArgument(12, (float)isovistField.CellSize);
            kernel.SetValueArgument(13, isovistField.CountX);
            kernel.SetValueArgument(14, isovistField.CountY);
            kernel.SetValueArgument(15, (float)isovistField.Precision);
            kernel.SetValueArgument(16, isovistField.wallP1X.Length);
        }

        public void Run()
        {
            try
            {
                if (isovistField.wallP1X.Length <= 0)
                    return;
                long[] globalSize = new long[] { (long)Math.Ceiling(isovistField.CountX / 16.0) * 16, (long)Math.Ceiling(isovistField.CountY / 16.0) * 16 };
                long[] localSize = new long[] { 16, 16 };
                commands.Execute(kernel, null, globalSize, localSize, eventList);
                commands.Finish();

                commands.ReadFromBuffer(area_buf, ref isovistField.Area.values, false, eventList);
                commands.ReadFromBuffer(perimeter_buf, ref isovistField.Perimeter.values, false, eventList);
                commands.ReadFromBuffer(minRadial_buf, ref isovistField.MinRadial.values, false, eventList);
                commands.ReadFromBuffer(maxRadial_buf, ref isovistField.MaxRadial.values, false, eventList);
                commands.ReadFromBuffer(occlusivity_buf, ref isovistField.Occlusivity.values, false, eventList);
                commands.Finish();


                for (int i = 0; i < isovistField.Area.values.Length; i++)
                {
                    if (isovistField.ActiveCells[i])
                        isovistField.Compactness.values[i] =
                            (float)(4.0 * Math.PI * isovistField.Area.values[i] /
                            (isovistField.Perimeter.Get(i) * isovistField.Perimeter.Get(i)));
                }


            }
            catch (System.Exception ex)
            {
                /*
                log.WriteLine(ex.ToString());
                log.WriteLine(" ");
                if (program != null)
                    log.WriteLine(program.GetBuildLog(context.Devices[0]));
                 * */
            }
        }

        #endregion
    }
}

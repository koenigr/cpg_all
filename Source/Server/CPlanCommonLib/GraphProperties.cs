﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tektosyne.Collections;
//using System.Text;
//using System.Threading.Tasks;

namespace CPlan.CommonLib
{
/// <summary>
    /// Represents the measures for a graph.
    /// </summary>
    [Serializable]
    public struct GraphProperties
    {
        public ArrayEx<Single> DistMetric { get; set; }
        public ArrayEx<Single> DistSteps { get; set; }
        public IList<Single> Choice { get; set; }
        public Dictionary<FW_GPU_NET.Node, Single> ChoiceDict { get; set; }
        public ArrayEx<Single> Connectivity { get; set; }
    }
}

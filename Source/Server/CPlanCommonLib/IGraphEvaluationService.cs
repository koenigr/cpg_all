﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using Hik.Communication.ScsServices.Service;
using Tektosyne.Geometry;
using Tektosyne;

namespace CPlan.CommonLib
{
    /// <summary>
    /// This interface defines methods of calculator service that can be called by clients.
    /// </summary>
    [ScsService]
    public interface IGraphEvaluationService
    {
        int Add(int number1, int number2);

        double Divide(double number1, double number2);

        GraphProperties EvaluateGraph(FW_GPU_NET.Graph originalGraph, LineD[] OrigEdges, FW_GPU_NET.Graph fwGraph);
    }

    [ScsService]
    public interface IIsoFieldEvaluationService
    {
        int Add(int number1, int number2);

        double Divide(double number1, double number2);

        GraphProperties EvaluateGraph(FW_GPU_NET.Graph originalGraph, LineD[] OrigEdges, FW_GPU_NET.Graph fwGraph);
    }
}

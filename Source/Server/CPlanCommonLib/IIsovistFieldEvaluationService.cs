﻿//using System;
using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using Hik.Communication.ScsServices.Service;
using CPlan.Geometry;
using CPlan.Isovists;

namespace CPlan.CommonLib
{
    /// <summary>
    /// This interface defines methods of calculator service that can be called by clients.
    /// </summary>
    [ScsService]
    public interface IIsovistFieldEvaluationService
    {

        //void EvaluateIsovistField(IsovistServiceField2D isovistField);
        List<MeasureGrid> EvaluateIsovistField(Rect2D field, double _cellSize, List<Line2D> _obstacleLines, List<Poly2D> _obstPolys, Poly2D _isovistBorder);

    }
}


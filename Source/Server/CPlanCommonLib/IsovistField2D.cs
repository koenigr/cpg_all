﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Diagnostics;
using CPlan.Geometry;

namespace CPlan.CommonLib
{

    [Serializable]
    public class IsovistServiceField2D: Grid2D
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Attributes

        List<Line2D> m_obstacleLines = new List<Line2D>();

        bool m_isGPUinitialized = false;
        bool m_useGPU = true;
        public float m_precision = 0.02f;

        //Vector2D m_startP, m_endP, m_size;
        //int m_countX, m_countY;
        //double m_cellSize;
        //String m_name;

        MeasureGrid m_area, m_perimeter, m_minRadial, m_maxRadial, m_occlusivity, m_compactness, m_skewness, m_variance;

        int m_nrOfActiveCells;
        bool[] m_activeCells;

        
        //for GPU-Calculation:
        public float[] wallP1X;
        public float[] wallP1Y;
        public float[] wallP2X;
        public float[] wallP2Y;
        public static GPUIsovistField gpu;

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        [CategoryAttribute("Measures"), DescriptionAttribute("Area")]
        public MeasureGrid Area { get { return m_area; } }
        [CategoryAttribute("Measures"), DescriptionAttribute("Compactness")]
        public MeasureGrid Compactness { get { return m_compactness; } }
        [CategoryAttribute("Measures"), DescriptionAttribute("Perimeter")]
        public MeasureGrid Perimeter { get { return m_perimeter; } }
        [CategoryAttribute("Measures"), DescriptionAttribute("Occlusivity")]
        public MeasureGrid Occlusivity { get { return m_occlusivity; } }
        [CategoryAttribute("Measures"), DescriptionAttribute("MinRadial")]
        public MeasureGrid MinRadial { get { return m_minRadial; } }
        [CategoryAttribute("Measures"), DescriptionAttribute("MaxRadial")]
        public MeasureGrid MaxRadial { get { return m_maxRadial; } }
        [CategoryAttribute("Settings"), DescriptionAttribute("Precision of Isovist Calculation")]
        public float Precision { get { return m_precision; } set { m_precision = value; } }
        public List<Line2D> ObstacleLines
        {
            get { return m_obstacleLines; }
            set { m_obstacleLines = value; }
        }
        public Vector2D StartP { get { return m_startP; } set { m_startP = value; } }
        public Vector2D EndP { get { return m_endP; } set { m_endP = value; } }
        //public int NumberOfCells { get { return m_countX*m_countY; } }
        ////public int CountX { get { return m_countX; } }
        ////public int CountY { get { return m_countY; } }
        //public int NumberOfActiveCells { get { int cnt = 0; for (int i = 0; i < m_activeCells.Length; i++) if (m_activeCells[i]) cnt++; return cnt; } }
        public bool[] ActiveCells { get { return m_activeCells; } }

        public double CellSize
        {
            get { return m_cellSize; }
            set
            {
                m_cellSize = value;
                m_countX = Convert.ToInt32(Math.Round(m_size.X / m_cellSize, 0));
                m_countY = Convert.ToInt32(Math.Round(m_size.Y / m_cellSize, 0));
                m_activeCells = new bool[m_countX * m_countY];
                gridCells = new GridCell[m_countX, m_countY];
                for (int i = 0; i < m_countX; i++)
                    for (int j = 0; j < m_countY; j++)
                        gridCells[i, j] = new GridCell(true);
                colorValues = new Vector3D[m_countX * m_countY];
                show = true;
                InitializeMeasureGrids();
            }
        }

        public String DisplayMeasure { get; set; }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors
        public IsovistServiceField2D(Vector2D _startP, Vector2D _endP, int _countX, int _countY, double _cellSize, List<Line2D> _obstacleLines)
            : base(_startP, _endP, _cellSize, "IsovistField Grid")
        {
            m_startP = _startP;
            m_endP = _endP;
            m_countX = _countX;
            m_countY = _countY;
            m_cellSize = _cellSize;
            m_size = new Vector2D((m_endP.X - m_startP.X), (m_endP.Y - m_startP.Y));
            m_obstacleLines = _obstacleLines;
            m_activeCells = new bool[m_countX * m_countY];
            InitializeMeasureGrids();
            
        }

        //============================================================================================================================================================================
        public IsovistServiceField2D(Rect2D field, double _cellSize, List<Line2D> _obstacleLines)
            : base(field.TopLeft, field.BottomRight, _cellSize, "IsovistField Grid")
        {
            m_startP = new Vector2D(field.Left, field.Bottom);
            m_endP = new Vector2D(field.Right, field.Top);
            m_size = field.Size;
            m_cellSize = _cellSize;
            m_countX = Convert.ToInt32( Math.Round( m_size.X / m_cellSize, 0));
            m_countY = Convert.ToInt32(Math.Round(m_size.Y / m_cellSize, 0));
            m_obstacleLines = _obstacleLines;
            m_activeCells = new bool[m_countX * m_countY];
            InitializeMeasureGrids();
        }

        //============================================================================================================================================================================
        public IsovistServiceField2D(Rect2D field, double _cellSize, List<Line2D> _obstacleLines, List<Poly2D> _obstPolys, Poly2D _isovistBorder)
            : base(field.TopLeft, field.BottomRight, _cellSize, "IsovistField Grid")
        {

            m_startP = new Vector2D(field.Left, field.Bottom);
            m_endP = new Vector2D(field.Right, field.Top);
            m_size = field.Size;
            m_cellSize = _cellSize;
            m_countX = Convert.ToInt32(Math.Round(m_size.X / m_cellSize, 0));
            m_countY = Convert.ToInt32(Math.Round(m_size.Y / m_cellSize, 0));
            m_obstacleLines = _obstacleLines;
            m_activeCells = new bool[m_countX * m_countY];
            InitializeMeasureGrids();

            DeactivateCells(_obstPolys);
            CutBorder(_isovistBorder); // consider only the area inside the border polygon
        }

        #endregion
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        //============================================================================================================================================================================
        public void FillGrid()
        {
            Bitmap bmp = new Bitmap(m_countX, m_countY);

            //schalte alle Gridpoints, die unter einer Linie liegen inaktiv
            Graphics graph = Graphics.FromImage(bmp);
            Pen pen = new Pen(Brushes.Red);
            foreach (Line2D line in m_obstacleLines)
            {
                Vector2D p1 = new Vector2D(line.Start.X - m_startP.X, line.Start.Y - m_startP.Y);
                Vector2D p2 = new Vector2D(line.End.X - m_startP.X, line.End.Y - m_startP.Y);

                double gridX = (p1.X / m_size.X) * m_countX;
                double gridY = (p1.Y / m_size.Y) * m_countY;
                System.Drawing.Point point1 = new System.Drawing.Point((int)gridX, (int)gridY);
                gridX = (p2.X / m_size.X) * m_countX;
                gridY = (p2.Y / m_size.Y) * m_countY;
                System.Drawing.Point point2 = new System.Drawing.Point((int)gridX, (int)gridY);
                graph.DrawLine(pen, point1, point2);
            }

            //Fill bmp
            AbstractFloodFiller floodFiller=null;
            QueueLinearFloodFiller queueFloodFiller=null;
            
            //at random Point until ratio of filled to all is > 0.3
            System.Drawing.Point fillPoint = new System.Drawing.Point(m_countX / 2, m_countY / 2);
            double ratioFloodAll = 0.0;
            int nrOfRuns = 0;
            Random rnd = new Random();

            while (ratioFloodAll<0.3 && nrOfRuns<5)
            {
                if (nrOfRuns > 0)
                {
                    //set fillPoint randomly
                    fillPoint.X = (int)(m_countX * rnd.NextDouble());
                    fillPoint.Y = (int)(m_countY * rnd.NextDouble());
                }
                floodFiller = new AbstractFloodFiller(bmp);
                queueFloodFiller = new QueueLinearFloodFiller(floodFiller);
                int count = queueFloodFiller.FloodFill(fillPoint);
                ratioFloodAll = (double)count / (double)(m_countX * m_countY);
                nrOfRuns++;
                
            }
            if (nrOfRuns>=5)
            {
                MessageBox.Show("Not able to fill the space automatically! Please select a point inside the spatial configuration. [not yet implemented]");
                // Todo: Select Point and fill again!

            }

            for (int i = 0; i < queueFloodFiller.Bitmap.Bitmap.Width; i++)
                for (int j = 0; j < queueFloodFiller.Bitmap.Bitmap.Height; j++)
                {
                    byte[] bitmapBits = queueFloodFiller.Bitmap.Bits;

                    int idx = (floodFiller.Bitmap.Stride * j) + (i * floodFiller.Bitmap.PixelFormatSize);

                    if ((bitmapBits[idx] == 255) && (bitmapBits[idx + 1] == 0) && (bitmapBits[idx + 2] == 255))
                        m_activeCells[j * m_countX + i] = true;
                    else
                        m_activeCells[j * m_countX + i] = false;                    
                }
        }

        //============================================================================================================================================================================
        public void DeactivateCells(List<Poly2D> rects)
        {
            for (int i = 0; i < CountX; i++)
            {
                for (int j = 0; j < CountY; j++)
                {
                    Vector2D curPos = new Vector2D(m_startP.X + i * CellSize + CellSize / 2, m_startP.Y + j * CellSize + CellSize / 2);
                    m_activeCells[j * m_countX + i] = true;
                    foreach (Poly2D curRect in rects)
                    {
                        if (curRect.ContainsPoint(curPos))
                        {
                            m_activeCells[j * m_countX + i] = false;
                            break;
                        }
                    }
                }
            }
        }

        //============================================================================================================================================================================
        public void CutBorder(Poly2D border)
        {
            for (int i = 0; i < CountX; i++)
            {
                for (int j = 0; j < CountY; j++)
                {
                    Vector2D curPos = new Vector2D(m_startP.X + i * CellSize + CellSize / 2, m_startP.Y + j * CellSize + CellSize / 2);
                    //m_activeCells[j * m_countX + i] = true;

                    if (! border.ContainsPoint(curPos))
                    {
                        m_activeCells[j * m_countX + i] = false;
                    }

                }
            }
        }

        //============================================================================================================================================================================
        public void Calculate(float precision, bool useGPU)
        {
            m_precision = precision;

            if (useGPU)
            {
                if ( (gpu == null) || (!m_isGPUinitialized) )
                {
                    gpu = new GPUIsovistField(this);
                }
                CalculateIsovistFieldGPU();
            }
            else
            {
                CalculateIsovistFieldCPU();
            }
        }

        //============================================================================================================================================================================
        public void InitializeGPU()
        {
            if ( (gpu == null) || (!m_isGPUinitialized) )
            {
                UpdateWallCoordinates();
                gpu = new GPUIsovistField(this);
            }

            gpu.Initialize();
        }

        //============================================================================================================================================================================
        public void UpdateWallCoordinates()
        {
            wallP1X = new float[m_obstacleLines.Count];
            wallP1Y = new float[m_obstacleLines.Count];
            wallP2X = new float[m_obstacleLines.Count];
            wallP2Y = new float[m_obstacleLines.Count];

            int k = 0;
            foreach (Line2D tmpLineSLink in m_obstacleLines)
            {
                Line2D tmpLine = tmpLineSLink;
                wallP1X[k] = (float)tmpLine.Start.X;
                wallP1Y[k] = (float)tmpLine.Start.Y;
                wallP2X[k] = (float)tmpLine.End.X;
                wallP2Y[k] = (float)tmpLine.End.Y;
                k++;
            }

            if ( (gpu != null) && (m_isGPUinitialized) )
                gpu.UpdateWalls();
        }

        //============================================================================================================================================================================
        public void UpdateComputeBufferMeasures()
        {
            gpu.UpdateComputeBufferMeasures();
        }

        //============================================================================================================================================================================
        public void InitializeMeasureGrids()
        {
            m_area = new MeasureGrid(m_countX, m_countY, "Area");
            m_perimeter = new MeasureGrid(m_countX, m_countY, "Perimeter");
            m_minRadial = new MeasureGrid(m_countX, m_countY, "MinRadial");
            m_maxRadial = new MeasureGrid(m_countX, m_countY, "MaxRadial");
            m_occlusivity = new MeasureGrid(m_countX, m_countY, "Occlusivity");
            m_compactness = new MeasureGrid(m_countX, m_countY, "Compactness");
        }

        //============================================================================================================================================================================
        private void CalculateIsovistFieldCPU()
        {
            //Isovist2D isovist = new Isovist2D(StartP.X, StartP.Y, model, vModel.Ref);
            
        }

        //============================================================================================================================================================================
        private void CalculateIsovistFieldGPU()
        {
            gpu.Run();
            UpdateMeasures();
        }

        //============================================================================================================================================================================
        public void UpdateMeasures()
        {
            m_area.UpdateSecondaryMeasures(m_activeCells);
            m_perimeter.UpdateSecondaryMeasures(m_activeCells);
            m_compactness.UpdateSecondaryMeasures(m_activeCells);
            m_minRadial.UpdateSecondaryMeasures(m_activeCells);
            m_maxRadial.UpdateSecondaryMeasures(m_activeCells);
            m_occlusivity.UpdateSecondaryMeasures(m_activeCells);
        }

        //============================================================================================================================================================================
        public void WriteToGridValues()
        {
            MeasureGrid isovistMeasure;
            //String m_displayMeasure = "Area";// "Min Radial";//
            switch (DisplayMeasure)
            {
                case "Area":
                    isovistMeasure = Area;
                    break;
                case "Perimeter":
                    isovistMeasure = Perimeter;
                    break;
                case "Compactness":
                    isovistMeasure = Compactness;
                    break;
                case "Min Radial":
                    isovistMeasure = MinRadial;
                    break;
                case "Max Radial":
                    isovistMeasure = MaxRadial;
                    break;
                case "Occlusivity":
                    isovistMeasure = Occlusivity;
                    break;
                default:
                    isovistMeasure = Area;
                    break;
            }

            for (int i = 0; i < CountX; i++)
                for (int j = 0; j < CountY; j++)
                    if (ActiveCells[j * CountX + i])
                    {
                        gridCells[i, j].Active = true;
                        gridCells[i, j].Value = isovistMeasure.values[j * CountX + i];
                    }
                    else
                    {
                        gridCells[i, j].Active = false;
                    }
        }

        # endregion

    }


}

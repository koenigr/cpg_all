﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using CPlan.Geometry;

namespace CPlan.CommonLib
{
    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class MeasureGrid
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Attributes and Properties

        String name;
        public float[] values;
        float min, max, mean, stdDev, sum;
        int countI, countJ;

        [Browsable(false)]
        public float[] Values { get { return values; } set { values = value; } }
        public float Min { get { return (float)Math.Round(min, 2); } }
        public float Max { get { return (float)Math.Round(max, 2); } }
        public float Mean { get { return (float)Math.Round(mean, 2); } }
        public float StdDev { get { return (float)Math.Round(stdDev, 2); } }
        [Browsable(false)]
        public String Name { get { return name; } }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        public MeasureGrid(int _i, int _j, String _name)
        {
            countI = _i;
            countJ = _j;
            values = new float[countI * countJ];
            name = _name;
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        public float Get(int i)
        {
            return values[i];
        }

        public void UpdateSecondaryMeasures(bool[] activeCells)
        {

            //Min Max Values
            Vector2D minMax = FindMinimaMaximaValues(activeCells);
            min = (float)minMax.X;
            max = (float)minMax.Y;

            //Mean Values
            sum = 0;
            int cnt = 0;
            for (int i = 0; i < countI; i++)
                for (int j = 0; j < countJ; j++)
                {
                    if (activeCells[j * countI + i])
                    {
                        sum += values[j * countI + i];
                        cnt++;
                    }
                }
            mean = sum / cnt;

            //Std Dev
            stdDev = 0;
            for (int i = 0; i < countI; i++)
                for (int j = 0; j < countJ; j++)
                {
                    if (activeCells[j * countI + i])
                    {
                        stdDev += (values[j * countI + i] - mean) * (values[j * countI + i] - mean);
                    }
                }
            stdDev = (float)Math.Sqrt(stdDev / (countI * countJ));
        }

        public Vector2D FindMinimaMaximaValues(bool[] activeCells)
        {
            float min, max;
            min = float.MaxValue;
            max = float.MinValue;

            for (int i = 0; i < values.Length; i++)
            {
                if (activeCells[i])
                {
                    if (values[i] > max)
                    {
                        max = values[i];
                    }

                    if (values[i] < min)
                    {
                        min = values[i];
                    }
                }

            }
            Vector2D minmax = new Vector2D(min, max);
            return minmax;
        }

        private double map(double val, double minOrigin, double maxOrigin, double minTarget, double maxTarget)
        {
            return minTarget + (maxTarget - minTarget) * (val - minOrigin) / (maxOrigin - minOrigin);
        }

        public Vector2D FindMinimaMaximaInGrid()
        {
            min = float.MaxValue;
            max = float.MinValue;

            for (int i = 0; i < countI; i++)
                for (int j = 0; j < countJ; j++)
                {
                    if (values[j * countI + i] > max)
                    {
                        max = values[j * countI + i];
                    }

                    if (values[j * countI + i] < min)
                    {
                        min = values[j * countI + i];
                    }
                }

            Vector2D minmax = new Vector2D(min, max);
            return minmax;
        }

        #endregion
    }
}

﻿using System;
using CPlan.CommonLib;
using Hik.Communication.Scs.Communication.EndPoints.Tcp;
using Hik.Communication.ScsServices.Service;
using System.Collections.Generic;
using FW_GPU_NET;
using Tektosyne;
using Tektosyne.Geometry;
using Tektosyne.Collections;
//using System.Collections.Generic;
using System.Linq;
using CPlan.Isovists;
using CPlan.Geometry;

namespace CPlan.Server
{
    class Program
    {
        static void Main()
        {
            //Create a service application that runs on 10083 TCP port
            var serviceApplication = ScsServiceBuilder.CreateService(new ScsTcpEndPoint(10083));

            //Create a CalculatorService and add it to service application
            serviceApplication.AddService<IGraphEvaluationService, EvaluationService>(new EvaluationService());
            serviceApplication.AddService<IIsovistFieldEvaluationService, EvaluationService>(new EvaluationService());
            
            
            //Start service application
            serviceApplication.Start();

            Console.WriteLine("Calculator service is started. Press enter to stop...");
            Console.ReadLine();

            //Stop service application
            serviceApplication.Stop();
        }
    }

    public class EvaluationService : ScsService, IGraphEvaluationService, IIsovistFieldEvaluationService
    {
        public int Add(int number1, int number2)
        {
            return number1 + number2;
        }

        public double Divide(double number1, double number2)
        {
            if (number2 == 0.0)
            {
                throw new DivideByZeroException("number2 can not be zero!");
            }

            return number1 / number2;
        }


        //============================================================================================================================================================================    
        public List<MeasureGrid> EvaluateIsovistField(Rect2D field, double _cellSize, List<Line2D> _obstacleLines, List<Poly2D> _obstPolys, Poly2D _isovistBorder)
        {
            IsovistField2D isovistField = new IsovistField2D(field, _cellSize, _obstacleLines, _obstPolys, _isovistBorder);
            List<MeasureGrid> results = isovistField.Calculate(isovistField.Precision);
            return results;
        }


        //============================================================================================================================================================================    
        /// <summary>
        /// Computes the shortes pathes for the graph.
        /// </summary>
        /// <remarks><para>
        /// The results of this computation are stored in the DistMetric, Choice and DistAngle <see cref="ArrayEx"/>.
        /// </para></remarks>
        /// <param name="progressBar">For the use of a ToolStripProgressBar.
        /// If there is no ProgressBar use "null" as parameter.</param>

        public GraphProperties EvaluateGraph(FW_GPU_NET.Graph originalGraph, LineD[] OrigEdges, FW_GPU_NET.Graph fwGraph)
        {
            //Connected = true;
            GraphProperties measures = new GraphProperties();
            measures.DistMetric = new ArrayEx<Single>(OrigEdges.Length / 2);
            measures.DistSteps = new ArrayEx<Single>(OrigEdges.Length / 2);
            measures.Choice = new List<Single>();
            measures.ChoiceDict = new Dictionary<FW_GPU_NET.Node, Single>();
            measures.Connectivity = new ArrayEx<Single>(OrigEdges.Length / 2);

            // --- reset the counting value -----------------------------
            for (int i = 0; i < measures.DistMetric.Count; i++)
            {
                measures.DistMetric.SetValue(0, i);
                measures.DistSteps.SetValue(0, i);
                measures.Connectivity.SetValue(0, i);
            }

            // --- use reference edges of the original graph ---
            //List<LineD> origEdges = OrigGraph.ToLines().ToList();

            // -- filter all parents ---
            System.Collections.Generic.List<FW_GPU_NET.Node> parentNodes = new System.Collections.Generic.List<FW_GPU_NET.Node>();
            foreach (FW_GPU_NET.Node testNode in fwGraph.Nodes)
                if (testNode.Parent == null)
                    parentNodes.Add(testNode);

            foreach (FW_GPU_NET.Node curPt in parentNodes) measures.ChoiceDict[curPt] = 0f;

            DateTime start = DateTime.Now;

            //FWGraph.Compute(FW_GPU_NET.Graph.ComputeType.GPU);
            //FWGraph.Compute(FW_GPU_NET.Graph.ComputeType.CPU_FLOYD_WARSHALL);
            fwGraph.Compute(FW_GPU_NET.Graph.ComputeType.CPU_DIJKSTRA_PARALLEL);
            //FWGraph.Compute(FW_GPU_NET.Graph.ComputeType.CPU);
            //graph.ComputeEdgeUsedCounts();

            //Calculate the node positions, for this we need to take the original graph where the nodes correspond to edges
            PointD sPoint;
            PointD ePoint;
            double[] positions = new double[2 * (int)parentNodes.Count];
            int pos = 0;
            for (int i = 0; i < originalGraph.EdgeCount; i += 2)
            {
                sPoint = (PointD)originalGraph.Edges[i].Start.UserData;
                ePoint = (PointD)originalGraph.Edges[i].End.UserData;
                positions[pos++] = (sPoint.X + ePoint.X) / 2;
                positions[pos++] = (sPoint.Y + ePoint.Y) / 2;
            }

            //define the radiuses we want to consider
            //if we want to calculate for the whole graph we use float.MaxValue
            float[] radiuses = new float[] { float.MaxValue, 700, 200 }; // 700, 

            //create the output where we store the node betweenness values for each radius
            List<NodeBetweenness[]> parentNodesPerRadius = new List<NodeBetweenness[]>();
            for (int i = 0; i < radiuses.Length; i++)
            {
                List<NodeBetweenness> betweennessNodes = new List<NodeBetweenness>();
                foreach (Node node in parentNodes)
                {
                    betweennessNodes.Add(new NodeBetweenness(node));
                }
                parentNodesPerRadius.Add(betweennessNodes.ToArray());
            }

            //FWGraph.ComputeEdgeUsedCountsWithChilds(parentNodesPerRadius[0].ToArray(), true);
            //FWGraph.ComputeEdgeUsedCountsWithChilds(parentNodesPerRadius[0].ToArray(), true, positions, 200);
            fwGraph.ComputeEdgeUsedCountsWithChildsRadius(parentNodesPerRadius.ToArray(), true, positions, radiuses);

            //now the parentNodesPerRadius contains two lists of nodes, where the first contains betweennes for radius 700 and second for 200
            NodeBetweenness[] betweennessNodesTmp = parentNodesPerRadius[1];
            for (int i = 0; i < parentNodes.Count; i++)
            {
                FW_GPU_NET.Edge refEdge = (FW_GPU_NET.Edge)parentNodes[i].UserData;
                LineD refLine = new LineD((PointD)refEdge.Start.UserData, (PointD)refEdge.End.UserData);
                int origIdx = Array.IndexOf(OrigEdges, refLine); //OrigEdges.IndexOf(refLine);
                if (origIdx == -1)
                {
                    LineD refLineB = refLine.Reverse();
                    origIdx = Array.IndexOf(OrigEdges, refLineB); //OrigEdges.IndexOf(refLineB);
                }

                measures.Connectivity[origIdx] = parentNodes[i].OutgoingEdges.Count();// = connectivity
                measures.Connectivity[origIdx] += parentNodes[i].IncomingEdges.Count();
                measures.DistSteps[origIdx] = betweennessNodesTmp[i].UsedStepSum;
                measures.DistMetric[origIdx] = fwGraph.GetAllPathLengthWithChilds(parentNodes[i]);
                //PointD curNode = nodes[i];
                measures.ChoiceDict[parentNodes[origIdx]] = betweennessNodesTmp[i].UsedCount;
            }

            measures.Choice = measures.ChoiceDict.Values.ToList();
            //CollectionsExtend.InvertValues(Choice);

            return measures;

            // --- handle unconnected graphs ---
            //if (DistMetric[0] == float.PositiveInfinity)
            //{
            //    Connected = false;
            //    for (int i = 0; i < DistMetric.Count; i++)
            //    {
            //        DistMetric[i] = -1;
            //        DistSteps[i] = -1;
            //    }
            //}
            //else
            //{
            //    Extensions.InvertValues(DistMetric);
            //    Extensions.InvertValues(DistSteps);
            //}

        }
        

    }
}


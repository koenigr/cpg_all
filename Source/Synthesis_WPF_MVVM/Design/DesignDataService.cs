﻿using System;
using Synthesis_WPF_MVVM.Model;
using CPlan.Optimization;
using System.Data;
using Synthesis_WPF_MVVM.Properties;

namespace Synthesis_WPF_MVVM.Design
{
    public class DesignDataService : IDataService
    {
        //============================================================================================================================================================================
        public void GetData(Action<DataItem, Exception> callback)
        {
            // Use this to create design time data

            var item = new DataItem("Welcome to MVVM Light [design]");
            callback(item, null);
        }

        //============================================================================================================================================================================
        public ParametersOptimization GetParametersOptimization()
        {
            var parameters = new ParametersOptimization(10, 10, 2, 15, new CalculateVia() { LuciHost = Settings.Default.LuciHost, LuciPort = Settings.Default.LuciPort }, Settings.Default.PathToHDRI);
            return parameters;
        }

        //============================================================================================================================================================================
        public DataView GetNeigbourhoodPreferences()
        { return null; }

        //============================================================================================================================================================================
        public DataView GetHouseSizes()
        { return null; }
    }
}
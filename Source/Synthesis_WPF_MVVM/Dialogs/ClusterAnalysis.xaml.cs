﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Synthesis_WPF_MVVM.Dialogs
{
    /// <summary>
    /// Interaction logic for ClusterAnalysis.xaml
    /// </summary>
    public partial class ClusterAnalysis : Window
    {
        public ClusterAnalysis()
        {
            InitializeComponent();
        }
        private void OkClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}

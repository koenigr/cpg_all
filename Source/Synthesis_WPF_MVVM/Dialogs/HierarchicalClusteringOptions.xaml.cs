﻿using System.Windows;

namespace Synthesis_WPF_MVVM.Dialogs
{
    /// <summary>
    /// Interaction logic for KMeansClusteringOptions.xaml
    /// </summary>
    public partial class KMeansClusteringOptions : Window
    {
        public KMeansClusteringOptions()
        {
            InitializeComponent();
        }

        private void RunClusteringClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}

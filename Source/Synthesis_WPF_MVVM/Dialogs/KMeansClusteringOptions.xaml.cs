﻿using System.Windows;

namespace Synthesis_WPF_MVVM.Dialogs
{
    /// <summary>
    /// Interaction logic for HierarchicalClusteringOptions.xaml
    /// </summary>
    public partial class HierarchicalClusteringOptions : Window
    {
        public HierarchicalClusteringOptions()
        {
            InitializeComponent();
        }

        private void RunClusteringClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}

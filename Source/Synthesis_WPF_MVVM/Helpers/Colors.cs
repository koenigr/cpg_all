﻿using OpenTK;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synthesis_WPF_MVVM.Helpers
{
    /// <summary>
    /// Distinctive color palettes from Kelly and Boynton.
    /// </summary>
    /// <remarks>
    /// This was taken from: http://stackoverflow.com/questions/470690/how-to-automatically-generate-n-distinct-colors
    /// </remarks>
    public static class Colors
    {
        private const float Factor = 1f / 255f;
        private static Color UIntToColor(uint color) => Color.FromArgb((byte)(color >> 16), (byte)(color >> 8), (byte)color);
        public static Vector4 ToVector4(this Color color) => new Vector4(color.R * Factor, color.G * Factor, color.B * Factor, color.A * Factor);

        public static ReadOnlyCollection<Color> KellysMaxContrastSet
        {
            get { return kellysMaxContrastSet.AsReadOnly(); }
        }

        private static readonly List<Color> kellysMaxContrastSet = new List<Color>
        {
            UIntToColor(0xFFB300), //Vivid Yellow
            UIntToColor(0x803E75), //Strong Purple
            UIntToColor(0xFF6800), //Vivid Orange
            UIntToColor(0xA6BDD7), //Very Light Blue
            UIntToColor(0xC10020), //Vivid Red
            UIntToColor(0xCEA262), //Grayish Yellow
            UIntToColor(0x817066), //Medium Gray

            //The following will not be good for people with defective color vision
            UIntToColor(0x007D34), //Vivid Green
            UIntToColor(0xF6768E), //Strong Purplish Pink
            UIntToColor(0x00538A), //Strong Blue
            UIntToColor(0xFF7A5C), //Strong Yellowish Pink
            UIntToColor(0x53377A), //Strong Violet
            UIntToColor(0xFF8E00), //Vivid Orange Yellow
            UIntToColor(0xB32851), //Strong Purplish Red
            UIntToColor(0xF4C800), //Vivid Greenish Yellow
            UIntToColor(0x7F180D), //Strong Reddish Brown
            UIntToColor(0x93AA00), //Vivid Yellowish Green
            UIntToColor(0x593315), //Deep Yellowish Brown
            UIntToColor(0xF13A13), //Vivid Reddish Orange
            UIntToColor(0x232C16), //Dark Olive Green
        };

        public static ReadOnlyCollection<Color> BoyntonOptimized
        {
            get { return boyntonOptimized.AsReadOnly(); }
        }

        private static readonly List<Color> boyntonOptimized = new List<Color>
        {
            Color.FromArgb(0, 0, 255),      //Blue
            Color.FromArgb(255, 0, 0),      //Red
            Color.FromArgb(0, 255, 0),      //Green
            Color.FromArgb(255, 255, 0),    //Yellow
            Color.FromArgb(255, 0, 255),    //Magenta
            Color.FromArgb(255, 128, 128),  //Pink
            Color.FromArgb(128, 128, 128),  //Gray
            Color.FromArgb(128, 0, 0),      //Brown
            Color.FromArgb(255, 128, 0),    //Orange
        };

        public static ColorContainer Get(int amount) => new ColorContainer(amount <= boyntonOptimized.Count ? boyntonOptimized : kellysMaxContrastSet, amount);

        public class ColorContainer : IEnumerable<Color>
        {
            private readonly List<Color> colors;
            public int Amount { get; }

            public ColorContainer(List<Color> colors, int amount)
            {
                Contract.Requires(colors.Count > 0);
                Contract.Requires(amount > 0);
                this.colors = new List<Color>(colors); //Clone input
                this.Amount = amount;
            }

            public Color this[int index] => colors[index % Math.Min(Amount, colors.Count)];

            public IEnumerator<Color> GetEnumerator()
            {
                for (int i = 0; i < Amount; ++i) { yield return this[i]; }
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }
    }
}

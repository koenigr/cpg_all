﻿using CPlan.Optimization;
using System;
using System.Collections.ObjectModel;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Reflection;
using Synthesis_WPF_MVVM.Properties;
using System.IO;
using System.Linq;

namespace Synthesis_WPF_MVVM.Model
{
    public class DataService : IDataService
    {
        //============================================================================================================================================================================
        public void GetData(Action<DataItem, Exception> callback)
        {
            // Use this to connect to the actual data service
            var item = new DataItem("Welcome to MVVM Light");
            callback(item, null);
        }

        //============================================================================================================================================================================
        public ParametersOptimization GetParametersOptimization() // optional receive a path to a file with custom parameters
        {
            var parameters = new ParametersOptimization(20, 10, 2, 20, new CalculateVia() { LuciHost = Settings.Default.LuciHost, LuciPort = Settings.Default.LuciPort }, Settings.Default.PathToHDRI);
            return parameters;
        }

        public DataView GetHouseSizes()
        {
            DataTable dt = new DataTable();
            var path = Path.GetFullPath(Settings.Default.PathToVarious + "\\Sizes_EmpowerShack.txt");
            var lines = File.ReadAllLines(path).ToList();

            // ---> first line contains the column titles ---
            string[] titles = lines[0].Split('\t');
            for (int column = 0; column < titles.Length; column++)
            {
                if (titles[column] != "")
                    dt.Columns.Add(titles[column]);
            }

            // ---> read the values ---
            for (int i = 1; i < lines.Count(); i++)
            {
                DataRow dr = dt.NewRow();
                string[] values = lines[i].Split('\t');
                for (int k = 0; k < values.Count(); k++)
                {
                    if (values[k] != "")
                        dr[k] = float.Parse(values[k]);
                }
                dt.Rows.Add(dr);
                dt.AcceptChanges();
            }

            return dt.DefaultView;
        }

        //============================================================================================================================================================================
        public DataView GetNeigbourhoodPreferences()
        {
            DataTable dt = new DataTable();
            var path = Path.GetFullPath(Settings.Default.PathToVarious + "\\NeighbourhoodPreferences_Clean2.txt");
            var lines = File.ReadAllLines(path).ToList();

            // ---> first line contains the column titles ---
            string[] titles = lines[0].Split('\t');
            for (int column = 0; column < titles.Length; column++) 
            {
                if (titles[column] != "")
                    dt.Columns.Add(titles[column]);
            }
  
            // ---> read the values ---
            for (int i = 1; i < lines.Count(); i++)
            {
                DataRow dr = dt.NewRow();
                string[] values = lines[i].Split('\t');
                for (int k = 0; k < values.Count(); k++)
                {
                    if (values[k] != "")
                        dr[k] = int.Parse(values[k]);
                }
                dt.Rows.Add(dr);
                dt.AcceptChanges();
            }

            return dt.DefaultView;
        }
    }
}
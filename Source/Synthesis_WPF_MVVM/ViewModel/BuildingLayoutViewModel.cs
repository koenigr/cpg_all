﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Windows.Input;
using System.Drawing;
using CPlan.VisualObjects;
using CPlan.Geometry;
using CPlan.Optimization;
using Synthesis_WPF_MVVM.Model;
using System.Threading;
using OpenTK;
using Synthesis_WPF_MVVM.Helpers;
using System.IO;
using Synthesis_WPF_MVVM.Properties;
using AForge.Genetic;
using Xceed.Wpf.Toolkit.PropertyGrid;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Windows;
using Smrf.NodeXL.Core;
using Smrf.NodeXL.Layouts;
using Smrf.NodeXL.Algorithms;
using Smrf.NodeXL.Visualization.Wpf;

namespace Synthesis_WPF_MVVM.ViewModel
{
    public class BuildingLayoutViewModel : ViewModelBase
    {
        private readonly IDataService _dataService;
        private MultiPopulation _population;
        private Population _populationSingle;
        private List<MultiPisaChromosomeBase> _SOMOrderedArchive;
        //private List<MultiPisaChromosomeBase> _globalArchive;
        private int _populationSize;
        private int _generations;
        private bool _needToStop;
        private Thread _workerThread;
        private TimeSpan _timeSpan;
        private Poly2D _borderPoly;
        private int[,] _somOrder;
        private DataView _neighboorhoodPreferences;

        public ParametersLayout LayoutParameters { get; set; }

        public ParametersOptimization OptimizationParameters { get; set; }

        public DataView NeighboorhoodPreferences
        {
            get { return _neighboorhoodPreferences; }
            set
            {
                _neighboorhoodPreferences = value;
                RaisePropertyChanged("NeighboorhoodPreferences");
            }
        }
        

        //============================================================================================================================================================================
        /// <summary>
        /// Initializes a new instance of the BuildingLayoutViewModel class.
        /// </summary>
        public BuildingLayoutViewModel(IDataService dataService)
        {
            _dataService = dataService;
            _dataService.GetData(
                (item, error) =>
                {
                    if (error != null)
                    {
                        // Report error here
                        return;
                    }
                });
            InitializeLParameter();

            NeighboorhoodPreferences = new DataView();
            OptimizationParameters = _dataService.GetParametersOptimization();
            
            ShowPopUp = new RelayCommand(() => ShowPopUpExecute(), () => true);
            CreateGeometry = new RelayCommand(() => CreateGeometryExecute(), () => true);
            OptiLayoutSolar = new RelayCommand(() => OptiLayoutSolarExecute(), () => true);
            LoadNodeXLData = new RelayCommand(() => LoadNodeXLDataExecute(), () => true);
            LoadSizes = new RelayCommand(() => LoadSizesExecute(), () => true);
        } 

        //============================================================================================================================================================================
        /// <summary>
        /// Initialize the layout parameters.
        /// todo: Later take the values from dataService!!!
        /// </summary>
        private void InitializeLParameter()
        {
            LayoutParameters = new ParametersLayout();
            LayoutParameters.MaxBldArea = 400;
            LayoutParameters.MinBldArea = 100;
            LayoutParameters.MaxBldHeigth = 20;
            LayoutParameters.MinBldHeigth = 10;
            LayoutParameters.MinNrBuildings = 10;
            LayoutParameters.MaxNrBuildings = 25;
            LayoutParameters.Density = 0.3;
            LayoutParameters.MinSideRatio = 0.5;

            LayoutParameters.AlignRotationToBorder = false;
            LayoutParameters.FixedRotationValue = 0.0f;
            LayoutParameters.FixRotation = true;
        }

        //============================================================================================================================================================================
        public ICommand ShowPopUp { get; private set; }
        //-------------------------------------------------------------
        private void ShowPopUpExecute()
        {
            System.Windows.MessageBox.Show("Hello!");
        }


        //============================================================================================================================================================================
        public ICommand LoadSizes { get; private set; }
        //-------------------------------------------------------------
        private void LoadSizesExecute()
        {
            NeighboorhoodPreferences = _dataService.GetHouseSizes();
            var vmNode = (new ViewModelLocator()).NodeXLViewModel;
            DataTable preferencesTable = NeighboorhoodPreferences.ToTable();
        }


        //============================================================================================================================================================================
        public ICommand LoadNodeXLData { get; private set; }
        //-------------------------------------------------------------
        private void LoadNodeXLDataExecute()
        {
            NeighboorhoodPreferences = _dataService.GetNeigbourhoodPreferences();
            var vmNode = (new ViewModelLocator()).NodeXLViewModel;
            DataTable preferencesTable = NeighboorhoodPreferences.ToTable();
            
            // --> read the values from the data table and create the graph ---
            int nrVertices = int.MinValue;

            int maxID = int.MinValue;
            for (int i = 1; i < preferencesTable.Rows.Count; i++)
            {
                DataRow dr = preferencesTable.Rows[i];
                int accountLevel = Convert.ToInt32(dr[0]);
                maxID = Math.Max(maxID, accountLevel);
            }

            vmNode.NodeControl = new NodeXLControl();
            // --> create the vertices of the graph --
            IVertexCollection vertices = vmNode.NodeControl.Graph.Vertices;
            for (int v = 0; v < maxID; v++)
            {
                Vertex vertex = new Vertex();
                vmNode.NodeControl.Graph.Vertices.Add(vertex);
                //ap.NodeXLVertexID = vertex.ID;
            }

            // --> create the edges of the graph --
            foreach (DataRowView rowView in NeighboorhoodPreferences)
            {
                DataRow row = rowView.Row;
                int fromNode = Convert.ToInt32(row[0]);
                int toNode = Convert.ToInt32(row[1]);
                IVertex vertex1, vertex2;
                vmNode.NodeControl.Graph.Vertices.Find(fromNode, out vertex1);
                vmNode.NodeControl.Graph.Vertices.Find(toNode, out vertex2);
                vmNode.NodeControl.Graph.Edges.Add(vertex1, vertex2, true);
            }      

            // -- Use a ClusterCalculator to partition the graph's vertices into clusters.
            ClusterCalculator myClusterer = new ClusterCalculator();
            myClusterer.Algorithm = ClusterAlgorithm.WakitaTsurumi;
            ICollection<Community> clusters = myClusterer.CalculateGraphMetrics(vmNode.NodeControl.Graph);

            VertexDegreeCalculator oVertexDegreeCalculator = new VertexDegreeCalculator();
            Dictionary<Int32, VertexDegrees> oVertexDegreeDictionary = oVertexDegreeCalculator.CalculateGraphMetrics(vmNode.NodeControl.Graph);

            // -- One group will be created for each cluster.
            int col = 0;
            //List<Color> clusterColors = Tools.ColorsGenerator(clusters.Count);
            //List<Color> clusterColors = Tools.ColorsKellysMaxContrast;
            List<Color> clusterColors = Tools.ColorsIndexList();
            //List<Color> clusterColors = Tools.ColorsBoyntonOptimized;
            List<GroupInfo> groups = new List<GroupInfo>();
            foreach (Community cluster in clusters)
            {
                // -- Create a group.
                GroupInfo group = new GroupInfo();
                groups.Add(group);

                // -- Populate the group with the cluster's vertices.           
                foreach (IVertex vertex in cluster.Vertices)
                {
                    vertex.SetValue(ReservedMetadataKeys.PerColor, clusterColors[col]);
                    float vertiSize = (float) (2*Math.Log(oVertexDegreeDictionary[vertex.ID].Degree));//  oVertexDegreeDictionary[oVertex1.ID].Degree);
                    if (vertiSize > 0)
                        vertex.SetValue(ReservedMetadataKeys.PerVertexRadius, 1+ vertiSize ); 
                    group.Vertices.AddLast(vertex);
                }
                col++;
                if (col >= clusterColors.Count) col = 0;
            }

            // -- Store the group information as metadata on the graph.
            vmNode.NodeControl.Graph.SetValue(ReservedMetadataKeys.GroupInfo, groups.ToArray());

            // -- Tell the layout class to use the groups.
            CircleLayout myLayout = new CircleLayout();
            myLayout.LayoutStyle = LayoutStyle.UseGroups;
            myLayout.BoxLayoutAlgorithm = BoxLayoutAlgorithm.ForceDirected;

            // -- bundle edges
            vmNode.NodeControl.GraphDrawer.EdgeDrawer.CurveStyle = EdgeCurveStyle.CurveThroughIntermediatePoints; //EdgeCurveStyle.Bezier; //
            vmNode.NodeControl.EdgeBundlerStraightening = 0.25f;

            vmNode.NodeControl.Layout = myLayout;

            (new ViewModelLocator()).Main.ExecuteNodeXLViewCommand();
        }

        

        //============================================================================================================================================================================
        /// <summary>
        /// Simple property to hold the 'CreateGeometryCommand' - when executed it will create a test geometry.
        /// </summary>
        public ICommand CreateGeometry { get; private set; }
        //-------------------------------------------------------------
        private void CreateGeometryExecute()
        {
            var vm = (new ViewModelLocator()).Viewer2ViewModel;
            // --- test geometry ---
            GCircle myCircle = new GCircle(new OpenTK.Vector2d(3, 4), 10);
            myCircle.Filled = true;
            myCircle.FillColor = ConvertColor.CreateColor4(System.Drawing.Color.Pink);
            vm.ViewControls[0].AllObjectsList.Add(myCircle, true, true);
            //vm.ViewControls[0].ZoomAll();
            vm.ViewControls[0].Invalidate();
        }

        //============================================================================================================================================================================
        //============================== Layout Optimization ===========================================
        public ICommand OptiLayoutSolar { get; private set; }
        private async Task OptiLayoutSolarExecute()
        {
            if (_population != null)
                if (_population.Sel != null)
                    _population.Sel.Terminate();
            PisaWrapper.TerminateAll();
            PisaWrapper.PISA_DIR = Settings.Default.PathToPISA;

            var corners = new Vector2d[5];
            corners[0] = new Vector2d(100, 101);
            corners[1] = new Vector2d(200, 100);
            corners[2] = new Vector2d(200, 201);
            corners[3] = new Vector2d(100, 200);
            corners[4] = new Vector2d(99, 150);
            _borderPoly = new Poly2D(corners);

            //NotificationBroker.Instance.Register(NotificationAlphaChangedHandler, "NUMALPHA");
            //NotificationBroker.Instance.Register(NotificationPopChangedHandler, "NUMPOP");
            //NotificationBroker.Instance.Register(NotificationNumLoopsChangedHandler, "NUMLOOPS");
            //NotificationBroker.Instance.Register(NotificationLoopChangedHandler, "CURLOOP");

            var vm = (new ViewModelLocator()).Viewer2ViewModel;
            vm.SearchCamera.AllObjectsList = new ObjectsAllList();
            vm.SearchCamera.Invalidate();
            //vm.SearchCamera.DeactivateInteraction();

            _SOMOrderedArchive = new List<MultiPisaChromosomeBase>();
            _population = null;
            //_activeLayout = null;
            //_selectedBlock = null;

            // ---> get population size
            _populationSize = OptimizationParameters.PopulationSize;
            
            // ---> generations = iterations
            _generations = OptimizationParameters.Generations;

            // ---> run worker thread           
            _needToStop = false;


            //if (sB_multiOpti.Checked)
            {
                //if (cB_layoutAll.Checked && cB_optiAll.Checked)
                //    _workerThread = new Thread(SearchLayoutMultiAllBlocks);
                //else
                    _workerThread = new Thread(SearchLayoutMulti);
            }

            //if (sB_singleOpti.Checked)
            //{
                //_workerThread = new Thread(SearchLayoutSingle);
            //}
            _workerThread.Start();
        }


        public ICommand OptiLayoutIsovist { get; private set; }
        private async Task OptiLayoutIsovistExecute()
        {
            if (_population != null)
                if (_population.Sel != null)
                    _population.Sel.Terminate();
            PisaWrapper.TerminateAll();
            PisaWrapper.PISA_DIR = Settings.Default.PathToPISA;

            var corners = new Vector2d[5];
            corners[0] = new Vector2d(100, 101);
            corners[1] = new Vector2d(200, 100);
            corners[2] = new Vector2d(200, 201);
            corners[3] = new Vector2d(100, 200);
            corners[4] = new Vector2d(99, 150);
            _borderPoly = new Poly2D(corners);

            //NotificationBroker.Instance.Register(NotificationAlphaChangedHandler, "NUMALPHA");
            //NotificationBroker.Instance.Register(NotificationPopChangedHandler, "NUMPOP");
            //NotificationBroker.Instance.Register(NotificationNumLoopsChangedHandler, "NUMLOOPS");
            //NotificationBroker.Instance.Register(NotificationLoopChangedHandler, "CURLOOP");

            var vm = (new ViewModelLocator()).Viewer2ViewModel;
            vm.SearchCamera.AllObjectsList = new ObjectsAllList();
            vm.SearchCamera.Invalidate();
            //vm.SearchCamera.DeactivateInteraction();

            _SOMOrderedArchive = new List<MultiPisaChromosomeBase>();
            _population = null;
            //_activeLayout = null;
            //_selectedBlock = null;

            // ---> get population size
            _populationSize = OptimizationParameters.PopulationSize;

            // ---> generations = iterations
            _generations = OptimizationParameters.Generations;

            // ---> run worker thread           
            _needToStop = false;


            //if (sB_multiOpti.Checked)
            {
                //if (cB_layoutAll.Checked && cB_optiAll.Checked)
                //    _workerThread = new Thread(SearchLayoutMultiAllBlocks);
                //else
                _workerThread = new Thread(SearchLayoutMulti);
            }

            //if (sB_singleOpti.Checked)
            //{
            //_workerThread = new Thread(SearchLayoutSingle);
            //}
            _workerThread.Start();
        }
        //============================================================================================================================================================================
        // Worker thread
        private void SearchLayoutSingle()
        {
            // measure the needed time:
            //DateTime start = DateTime.Now; // DateTime.MinValue;
            //DateTime end;
            // create fitness function
            var fitnessFunction = new FitnessFunctionSolarSinglecs(new CalculateVia().Solar, Settings.Default.PathToHDRI);
            // create population
            
            _populationSingle = new Population(_populationSize, new ChromosomeLayoutSingle(_borderPoly.Clone(), LayoutParameters),
                fitnessFunction,
                new EliteSelection()
                );
            _populationSingle.MutationRate = 0.25;
            _populationSingle.RandomSelectionPortion = 0.2;
            DrawLayoutSingle(0);
            //Debug.WriteLine("bestFit", Math.Round(_populationSingle.FitnessMax, 2).ToString());
            int i = 1;

            // loop
            while (!_needToStop)
            {
                // run one epoch of genetic algorithm
                _populationSingle.RunEpoch();
                DrawLayoutSingle(i);

                //Debug.WriteLine(i.ToString() + ". iterat, bestFit", Math.Round(_populationSingle.FitnessMax, 2).ToString());
                // increase current iteration
                i++;

                if ((_generations != 0) && (i > _generations))
                    break;

                // -- manual call of the garbage collector avoids 'out of memory' error on long optimization runs --
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Thread.Sleep(1);
            }
            //end = DateTime.Now;
            //_span = end - start;

            //glCameraMainWindow.ActivateInteraction();
        }

        //============================================================================================================================================================================
        private void SearchLayoutMulti()
        {
            //NotificationBroker.Instance.PostNotification("NUMLOOPS", _iterations);
            //NotificationBroker.Instance.PostNotification("CURLOOP", 0);

            // measure the needed time:
            DateTime start = DateTime.Now; // DateTime.MinValue;
            DateTime end;
            
            // create fitness function
            //var fitnessFunction = new FitnessFunctionSolar(OptimizationParameters.RunVia, Settings.Default.PathToHDRI);
            //var fitnessFunction = new FitnessFunctionIsovist(OptimizationParameters.RunVia);
            //var fitnessFunction = new FitnessFunctionLayoutCombined(OptimizationParameters.RunVia, Settings.Default.PathToHDRI); //(OptimizationParameters.RunVia);
            var fitnessFunction = OptimizationParameters.SelectedFitnessFunction.GetFunction();

            //if (_activeLayout != null) _borderPoly = new GPolygon(_activeLayout.Border);
            //_isovistBorder = _borderPoly.Poly.Clone();
            //_isovistBorder.Offset(2);
            //Rect2D borderRect = GeoAlgorithms2D.BoundingBox(_isovistBorder.PointsFirstLoop);

            var alpha = OptimizationParameters.ArchiveSize;// (int)uD_Alpha.Value;
            var lambda = (int)(OptimizationParameters.PopulationSize * OptimizationParameters.PopulationPreasure);// (int)uD_Lambda.Value;
            var mu = OptimizationParameters.PopulationSize;// (int)uD_Mu.Value;

            // create population
            if (_population == null)
            {
                ChromosomeLayout curChromosome;
                //if (_activeLayout != null) curChromosome = new ChromosomeLayout(_activeLayout);
                //else
                curChromosome = new ChromosomeLayout(_borderPoly, LayoutParameters);

                bool inheritOnly = false;
                //if (ControlParameters.Scenario == "Cape Town Scenario") inheritOnly = true;
                _population = new MultiPopulation(_populationSize, alpha, lambda, mu,
                    curChromosome,
                    fitnessFunction,
                    /* *ac* LucyManager.Manager.ViaLucy ? CalculationType.Method.Luci : CalculationType.Method.Local, //CalculationType.Method.Local,*/
                    inheritOnly
                    );

                _population.MutationRate = 0.1;
                _population.CrossoverRate = 0.7;
                _population.RandomSelectionPortion = 0.1;

                //SetPopulationParameters();
            }
            DrawLayoutMulti(0);
            //PlotFitnessValues(0);
            //VarianceAnalysis(0);
            // iterations
            int i = 1;

            // loop
            while (!_needToStop)
            {
                //NotificationBroker.Instance.PostNotification("CURLOOP", i);

                // run one epoch of genetic algorithm
                _population.RunEpoch().Wait();
                DrawLayoutMulti(i);
                //PlotFitnessValues(i);
                //VarianceAnalysis(i);

                // set current iteration's info
                //SetText(lbl_Generation, _population[0].Generation.ToString());

                // increase current iteration
                i++;

                if ((_generations != 0) && (i > _generations))
                {
                    break;
                }

                // -- manual call of the garbage collector avoids 'out of memory' error on long optimization runs --
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Thread.Sleep(1);
            }
            end = DateTime.Now;
            _timeSpan = end - start;
            _population.Sel.Terminate();
            //(new ViewModelLocator()).Viewer2ViewModel.SearchCamera.ActivateInteraction();
        }

        //============================================================================================================================================================================
        private void DrawLayoutMulti(int iterat) //Population pop); //InstructionTreePisa fittest)
        {
            var curArchive = new List<ChromosomeLayout>();
            var vm = (new ViewModelLocator()).Viewer2ViewModel;
            //vm.MainCamera.AllObjectsList.ObjectsToDraw.AddRange(_remainDrawingObjects);

            float distX = (float)1.2;// nUD_distX.Value / 10.0f;
            float distY = (float)1.2;// nUD_distY.Value / 10.0f;
            //var fitnesValues = new List<double>();
            if (_population == null) return;
            if (OptimizationParameters.DrawLayoutStyle != DrawLayoutOptions.RowsAll)
            {
                vm.SearchCamera.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
                //if (constantDrawObjects != null) glCameraMainWindow.AllObjectsList.ObjectsToDraw.AddRange(constantDrawObjects);
            }

            if (OptimizationParameters.DrawLayoutStyle == DrawLayoutOptions.SOM)
            {
                SOMorderProcess();
                // if (cB_showSomMap.Checked) ShowEsomMap();
                if (_SOMOrderedArchive != null)
                {
                    foreach (IMultiChromosome curImChromo in _SOMOrderedArchive)
                    {
                        if (curImChromo is ChromosomeLayout)
                            curArchive.Add((ChromosomeLayout)curImChromo);
                    }
                }
                if (_somOrder == null)
                    return;
            }
                    
            List<IMultiChromosome> archive = _population.Sel.GetArchive();
            for (int p = 0; p < archive.Count; p++)
            { 
                var curChromo = (ChromosomeLayout)(archive[p]);               
                if (curChromo != null)
                {
                    Vector2d deltaP = new Vector2d();
                    if (OptimizationParameters.DrawLayoutStyle == DrawLayoutOptions.SOM)
                    {
                        distX = (float)0.2;
                        distY = (float)0.2;         
                        deltaP = new Vector2d(_somOrder[p, 2]*curChromo.BoundingRect.Width*distX, _somOrder[p, 1]*-curChromo.BoundingRect.Height*distY);
                        curChromo = curArchive.Find(chromosomeLayout => chromosomeLayout.Indentity == _somOrder[p, 0]);
                    }
                    else if (OptimizationParameters.DrawLayoutStyle == DrawLayoutOptions.Rows)
                    { 
                        deltaP = new Vector2d(p*curChromo.BoundingRect.Width*distX, curChromo.BoundingRect.Height*distY);
                    }
                    else if (OptimizationParameters.DrawLayoutStyle == DrawLayoutOptions.RowsAll)
                    {
                        deltaP = new Vector2d(p * curChromo.BoundingRect.Width * distX, iterat * curChromo.BoundingRect.Height * distY);
                    }

                    // -------- draw Solar analysis ------------------------------------------------  
                    if (OptimizationParameters.VisualizationOptions == VisualOptions.SolarResults)
                    {
                        var sf = new ScalarField(curChromo.Border.PointsFirstLoop.Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(), ControlParameters.IsovistCellSize, new Vector3d(0, 0, 1));

                        GGrid solarGrid = new GGrid(sf, "solarAnalysis");
                        solarGrid.ScalarField.GenerateTexture(curChromo.SolarAnalysisResults, "solarAnalysis");
                        solarGrid.GenerateTexture();
                        solarGrid.Move(deltaP);
                        vm.SearchCamera.AllObjectsList.ObjectsToDraw.Add(solarGrid);

                        if (curChromo.Gens != null)
                        {
                            foreach (GenBuildingLayout building in curChromo.Gens)
                            {
                                var bPoly = new GPrism(building.PolygonFromGen);

                                bPoly.SFields = building.SideGrids;
                                bPoly.Height = building.BuildingHeight;
                                bPoly.LevelHeight = ControlParameters.FloorHeigth;
                                bPoly.Identity = curChromo.Indentity;
                                bPoly.DeltaZ = 0.1f;
                                bPoly.BorderWidth = 5;

                                var distPoly = new GPolygon(building.PolyForCollission.BodyPoly);
                                distPoly.Filled = false;
                                distPoly.BorderWidth = 1;
                                distPoly.BorderColor = ConvertColor.CreateColor4(Color.Chartreuse);
                                distPoly.Move(deltaP);
                                vm.SearchCamera.AllObjectsList.ObjectsToDraw.Add(distPoly);

                                if (!building.Open) bPoly.Filled = true;
                                else
                                {
                                    bPoly.Filled = false;
                                    bPoly.BorderWidth = 1;
                                    Vector4 col = ConvertColor.CreateColor4(Color.Gainsboro);
                                    col.W = 0.1f; // alpha value
                                    bPoly.BorderColor = col;
                                }
                                bPoly.Move(deltaP);
                                vm.SearchCamera.AllObjectsList.ObjectsToDraw.Add(bPoly);
                            }
                        }
                        var borderPoly = new GPolygon(curChromo.Border);
                        borderPoly.Identity = curChromo.Indentity;
                        borderPoly.Move(deltaP);
                        vm.SearchCamera.AllObjectsList.ObjectsToDraw.Add(borderPoly);
                    }

                    // -------- draw Isovist analysis ------------------------------------------------
                    if (OptimizationParameters.VisualizationOptions == VisualOptions.IsovistResults)
                    {
                        if (curChromo.IsovistField != null)
                        {
                            curChromo.IsovistField.DisplayMeasure = OptimizationParameters.DisplayIsovistMeasure.ToString();
                            var visualIsovistField = new GGrid(curChromo.IsovistField.AnalysisPoints, curChromo.IsovistField.GetMeasure(), curChromo.IsovistField.CellSize);
                            visualIsovistField.Move(deltaP);
                            vm.SearchCamera.AllObjectsList.ObjectsToDraw.Add(visualIsovistField);

                            if (curChromo.Environment != null)
                            {
                                foreach (Poly2D envObj in curChromo.Environment)
                                {
                                    var ePoly = new GPolygon(envObj);
                                    ePoly.Identity = curChromo.Indentity;
                                    ePoly.Move(deltaP);
                                    vm.SearchCamera.AllObjectsList.ObjectsToDraw.Add(ePoly);
                                }
                            }

                            if (curChromo.Gens != null)
                            {
                                foreach (GenBuildingLayout building in curChromo.Gens)
                                {
                                    var bPoly = new GPrism(building.PolygonFromGen);
                                    bPoly.Height = building.BuildingHeight;
                                    bPoly.LevelHeight = 4;
                                    bPoly.Identity = curChromo.Indentity;
                                    bPoly.DeltaZ = 0.1f;
                                    bPoly.BorderWidth = 2;
                                    bPoly.FillColor = ConvertColor.CreateColor4(Color.Blue);
                                    if (!building.Open) bPoly.Filled = true;
                                    else
                                    {
                                        bPoly.Filled = false;
                                        bPoly.BorderWidth = 1;
                                        Vector4 col = ConvertColor.CreateColor4(Color.Gainsboro);
                                        col.W = 0.1f; // alpha value
                                        bPoly.BorderColor = col;
                                    }
                                    bPoly.Move(deltaP);
                                    vm.SearchCamera.AllObjectsList.ObjectsToDraw.Add(bPoly);
                                }
                            }

                            var borderPoly = new GPolygon(curChromo.Border);
                            borderPoly.Identity = curChromo.Indentity;
                            borderPoly.Move(deltaP);
                            vm.SearchCamera.AllObjectsList.ObjectsToDraw.Add(borderPoly);
                        }
                    }
                }
            }
            //if (cB_autoShot.Checked) SetGraphicContext(glControl1); //Screenshot();
            vm.SearchCamera.Invalidate();
            //vm.SearchCamera.ZoomAll();
        }


        //============================================================================================================================================================================
        private void DrawLayoutSingle(int iterat) //Population pop); //InstructionTreePisa fittest)
        {
            var vm = (new ViewModelLocator()).Viewer2ViewModel;
            // -- draw the network --
            float dist = 1.1f;
            //var fitnesValues = new List<double>();

            //if (cb_showSOMorder.Checked || cb_showBestGen.Checked)
            {
                vm.SearchCamera.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            }

            for (int p = 0; p < _populationSingle.Size; p++)
            {
                var curChromo = (ChromosomeLayoutSingle)(_populationSingle[p]);
                //if (curChromo.IsovistField != null)
                //{
                //    var visualIsovistField = new GGrid(curChromo.IsovistField.AnalysisPoints,
                //        curChromo.IsovistField.GetMeasure(), curChromo.IsovistField.CellSize);
                //    // new GGrid(curChromo.IsovistField); 
                //    var deltaP = new Vector2d(p * curChromo.BoundingRect.Width * dist,
                //        iterat * curChromo.BoundingRect.Height * dist);
                //    visualIsovistField.Move(deltaP);
                //    vm.SearchCamera.AllObjectsList.ObjectsToDraw.Add(visualIsovistField);

                //    if (curChromo.Environment != null)
                //        foreach (Poly2D envObj in curChromo.Environment)
                //        {
                //            var ePoly = new GPolygon(envObj);
                //            ePoly.Move(deltaP);
                //            vm.SearchCamera.AllObjectsList.ObjectsToDraw.Add(ePoly);
                //        }

                //    if (curChromo.Gens != null)
                //        foreach (GenBuildingLayout building in curChromo.Gens)
                //        {
                //            var bPoly = new GPrism(building.PolygonFromGen);
                //            bPoly.Height = building.BuildingHeight;
                //            bPoly.DeltaZ = 0.1f;
                //            bPoly.BorderWidth = 2;
                //            if (!building.Open) bPoly.Filled = true;
                //            else
                //            {
                //                bPoly.Filled = false;
                //                bPoly.BorderWidth = 1;
                //                Vector4 col = ConvertColor.CreateColor4(Color.Gainsboro);
                //                col.W = 0.1f; // alpha value
                //                bPoly.BorderColor = col;
                //            }
                //            bPoly.FillColor = ConvertColor.CreateColor4(Color.White);
                //            bPoly.Filled = true;
                //            bPoly.Move(deltaP);
                //            vm.SearchCamera.AllObjectsList.ObjectsToDraw.Add(bPoly);
                //        }

                //    var borderPoly = new GPolygon(curChromo.Border);
                //    borderPoly.Move(deltaP);
                //    vm.SearchCamera.AllObjectsList.ObjectsToDraw.Add(borderPoly);
                //}


                if (curChromo != null)
                {
                    //if (curChromo.IsovistField.DisplayMeasure == "Solar Analysis")
                    //    curChromo.IsovistField.DisplayMeasure = "Solar Analysis";
                    //else
                    //    curChromo.IsovistField.DisplayMeasure = ControlParameters.DisplayMeasure; // comboBox_Isovist.Text;

                    //curChromo.IsovistField.WriteToGridValues();
                    //GGridOld VisualIsovistField = new GGridOld(curChromo.IsovistField);
                    //var visualIsovistField = new GGrid(curChromo.IsovistField.AnalysisPoints,
                    //    curChromo.IsovistField.GetMeasure(), curChromo.IsovistField.CellSize);
                    var deltaP = new Vector2d(p * curChromo.BoundingRect.Width * dist,
                       iterat * curChromo.BoundingRect.Height * dist);
                    //visualIsovistField.Move(deltaP);
                    //vm.SearchCamera.AllObjectsList.ObjectsToDraw.Add(visualIsovistField);

                    var sf = new ScalarField(curChromo.Border.PointsFirstLoop.Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(), ControlParameters.IsovistCellSize, new Vector3d(0, 0, 1));
                    GGrid solarGrid = new GGrid(sf, "solarAnalysis");
                    solarGrid.ScalarField.GenerateTexture(curChromo.SolarAnalysisResults, "solarAnalysis");
                    solarGrid.GenerateTexture();
                    solarGrid.Move(deltaP);
                    vm.SearchCamera.AllObjectsList.ObjectsToDraw.Add(solarGrid);


                    if (curChromo.Environment != null)
                        foreach (Poly2D envObj in curChromo.Environment)
                        {
                            var ePoly = new GPolygon(envObj);
                            ePoly.Identity = curChromo.Indentity;
                            ePoly.Move(deltaP);
                            vm.SearchCamera.AllObjectsList.ObjectsToDraw.Add(ePoly);
                        }

                    if (curChromo.Gens != null)
                    {
                        foreach (GenBuildingLayout building in curChromo.Gens)
                        {
                            var bPoly = new GPrism(building.PolygonFromGen);
                            bPoly.Height = building.BuildingHeight;
                            bPoly.LevelHeight = 4;
                            bPoly.Identity = curChromo.Indentity;
                            bPoly.DeltaZ = 0.1f;
                            bPoly.BorderWidth = 2;
                            bPoly.FillColor = ConvertColor.CreateColor4(Color.Blue);
                            if (!building.Open) bPoly.Filled = true;
                            else
                            {
                                bPoly.Filled = false;
                                bPoly.BorderWidth = 1;
                                Vector4 col = ConvertColor.CreateColor4(Color.Gainsboro);
                                col.W = 0.1f; // alpha value
                                bPoly.BorderColor = col;
                            }
                            bPoly.Move(deltaP);
                            vm.SearchCamera.AllObjectsList.ObjectsToDraw.Add(bPoly);
                        }
                    }
                    var borderPoly = new GPolygon(curChromo.Border);
                    borderPoly.Identity = curChromo.Indentity;
                    borderPoly.Move(deltaP);
                    vm.SearchCamera.AllObjectsList.ObjectsToDraw.Add(borderPoly);
                }





            }
            //if (cB_autoShot.Checked) SetGraphicContext(glControl1); //Screenshot();
            vm.SearchCamera.Invalidate();
            //vm.SearchCamera.ZoomAll();
        }
 
        //============================================================================================================================================================================
        private void SOMorderProcess()
        {
            //string iniPath = Application.StartupPath;
            string name = "SOM_" + DateTime.Now.Day.ToString("00") + "-" +
                          DateTime.Now.Month.ToString("00") + "_" +
                          DateTime.Now.Hour.ToString("00") +
                          DateTime.Now.Minute.ToString("00") +
                          DateTime.Now.Second.ToString("00");
            string globalLrn = name + ".lrn";

            //string path = Path.Combine(Environment.CurrentDirectory, _globalLrn);
            //TextRW.WriteLrn(_population, path);
            string OutputPath = Settings.Default.PathToOutput;
            string LRNpath = Path.Combine(OutputPath, globalLrn);
            string resultPath = Path.Combine(OutputPath, name);
            TextRW.WriteLrn(_population, LRNpath);

            //ExecuteCommandSync(OutputPath.Substring(0, 2));
            Helper.ExecuteCommandSync("cd " + OutputPath);
            /*
                        // --- Databionic ESOM ------------------------------------------------------------------------
                        string strCmdText;
                        string ESOMpath = Settings.Default.PathToESOM;
                        // -- esom training - lrn
                        strCmdText = ESOMpath + "esomtrn.bat"; //
                        // --------------
                        // -- parameters:
                        // -- path to the lrn file
                        //            strCmdText += " -l " + _globalLrn;
                        strCmdText += " -l " + LRNpath;
                        // -- number of training epochs
                        strCmdText += " -e 100";
                        // -- execute the command
                        ExecuteCommandSync(strCmdText);

                        //img.Save(path, System.Drawing.Imaging.ImageFormat.Png);
                        //MessageBox.Show(screenshotFilename);
                        */
            // --- Somoclu ------------------------------------------------------------------------
            string strCmdText;
            string SomocluPath = Settings.Default.PathToSomoclu;
            // -- esom training - lrn
            strCmdText = SomocluPath + "somoclu.exe "; //
            // --------------
            // -- parameters:
            // -- path to the lrn file
            //            strCmdText += " -l " + _globalLrn;
            strCmdText += LRNpath + " " + resultPath;//_globalLrn + " " + resultPath;
            // -- number of training epochs
            strCmdText += " -e 100";
            // -- execute the command
            Helper.ExecuteCommandSync(strCmdText);

            // --------------------------------------------------------------------------------------------
            _somOrder = TextRW.ReadBm(OutputPath + name + ".bm");

            _SOMOrderedArchive = _population.Sel.GetArchiveOrig();
        }


        //============================================================================================================================================================================
        public override void Cleanup()
        {
            // Clean up if needed
            if ((_workerThread != null) && (_workerThread.IsAlive))
            {
                _needToStop = true;
                //while (!_workerThread.Join(100))
                //    Application.DoEvents();
            }
            if (_population != null)
                if (_population.Sel != null)
                    _population.Sel.Terminate();
            PisaWrapper.TerminateAll();

            //base.Cleanup();
        }

    }
}

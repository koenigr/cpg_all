﻿using CPlan.Optimization;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace Synthesis_WPF_MVVM.ViewModel.Dialogs
{
    /// <summary>
    /// ViewModel for the <see cref="Synthesis_WPF_MVVM.Dialogs.ClusterAnalysis"/> Dialog.
    /// </summary>
    public class ClusterAnalysisViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event Action ShowPreview;

        private List<KeyValuePair<string, double>> analysisParameter0 = new List<KeyValuePair<string, double>>();
        public List<KeyValuePair<string, double>> AnalysisParameter0 {
            get
            {
                return analysisParameter0;
            }
            set
            {
                if(analysisParameter0 == null) { return; }
                analysisParameter0 = value;
                NotifyPropertyChanged(nameof(AnalysisParameter0));
            }
        }

        private List<KeyValuePair<string, double>> analysisParameter1 = new List<KeyValuePair<string, double>>();
        public List<KeyValuePair<string, double>> AnalysisParameter1
        {
            get
            {
                return analysisParameter1;
            }
            set
            {
                if (analysisParameter1 == null) { return; }
                analysisParameter1 = value;
                NotifyPropertyChanged(nameof(AnalysisParameter1));
            }
        }

        private int clusterCount = 0;
        public int ClusterCount
        {
            get { return clusterCount; }
            set
            {
                if (clusterCount != value && value >= 0)
                {
                    clusterCount = value;
                    NotifyPropertyChanged(nameof(ClusterCount));
                }
            }
        }

        private int clusterNumber0 = 0;
        public int ClusterNumber0
        {
            get { return clusterNumber0; }
            set
            {
                if (clusterNumber0 != value)
                {
                    if (value >= 0) { clusterNumber0 = value; }
                    NotifyPropertyChanged(nameof(ClusterNumber0));
                    var show = ShowPreview;
                    show?.Invoke();
                }
            }
        }

        private int clusterNumber1 = 0;
        public int ClusterNumber1
        {
            get { return clusterNumber1; }
            set
            {
                if (clusterNumber1 != value)
                {
                    if (value >= 0) { clusterNumber1 = value; }
                    NotifyPropertyChanged(nameof(ClusterNumber1));
                    var show = ShowPreview;
                    show?.Invoke();
                }
            }
        }

        private void NotifyPropertyChanged(string property)
        {
            var changed = PropertyChanged;
            changed?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}

﻿using CPlan.Optimization;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Synthesis_WPF_MVVM.ViewModel.Dialogs
{
    /// <summary>
    /// ViewModel for the <see cref="Synthesis_WPF_MVVM.Dialogs.HierarchicalClusteringOptions"/> Dialog.
    /// </summary>
    public class HCOptionsViewModel
    {
        private readonly Dictionary<string, HierarchicalCluster.Strategy> strategies;
        public IEnumerable<string> Items => strategies.Keys;

        public HierarchicalCluster.Strategy Strategy { get; private set; } = HierarchicalCluster.Strategy.FastUPGMA;
        public string ReductionFormula
        {
            set { Strategy = strategies[value]; }
            get { return Strategy.ToString(); }
        }

        public int NumberOfClusters { get; set; } = HierarchicalCluster.DefaultClusterCount;

        public bool ModifyOutput { get; set; } = true;

        public bool ShowSlider { get; set; } = true;

        public bool OptionsExpanded { get; set; } = false;

        public HCOptionsViewModel()
        {
            strategies = Enum.GetValues(typeof(HierarchicalCluster.Strategy)).Cast<HierarchicalCluster.Strategy>().ToDictionary(s => s.ToString());
        }
    }
}

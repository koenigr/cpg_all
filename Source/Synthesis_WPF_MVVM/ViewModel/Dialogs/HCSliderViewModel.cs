﻿using CPlan.Optimization;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Synthesis_WPF_MVVM.ViewModel.Dialogs
{
    /// <summary>
    /// ViewModel for the <see cref="Synthesis_WPF_MVVM.Dialogs.HierarchicalClusteringSlider"/> Dialog.
    /// </summary>
    public class HCSliderViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event Action ShowPreview;

        private bool modifyOutput = true;
        public bool ModifyOutput
        {
            get { return modifyOutput; }
            set
            {
                if(modifyOutput != value)
                {
                    modifyOutput = value;
                    NotifyPropertyChanged(nameof(ModifyOutput));
                    if (LivePreview) { PreviewExecute(); }
                }
            }
        }

        private bool livePreview = true;
        public bool LivePreview
        {
            get { return livePreview; }
            set
            {
                if(livePreview != value)
                {
                    livePreview = value;
                    NotifyPropertyChanged(nameof(LivePreview));
                    NotifyPropertyChanged(nameof(PreviewButtonVisibility));
                }
            }
        }

        private int numberOfClusters = HierarchicalCluster.DefaultClusterCount;
        public int NumberOfClusters
        {
            get { return numberOfClusters; }
            set
            {
                if(numberOfClusters != value)
                {
                    if (value > 0) { numberOfClusters = value; }
                    NotifyPropertyChanged(nameof(NumberOfClusters));
                    if (LivePreview) { PreviewExecute(); }
                }
            }
        }

        public ICommand Preview => new RelayCommand(PreviewExecute);
        private void PreviewExecute()
        {
            var show = ShowPreview;
            show?.Invoke();
        }

        public Visibility PreviewButtonVisibility => LivePreview ? Visibility.Hidden : Visibility.Visible;

        private void NotifyPropertyChanged(string property)
        {
            var changed = PropertyChanged;
            changed?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}

﻿using CPlan.Optimization;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Synthesis_WPF_MVVM.ViewModel.Dialogs
{
    /// <summary>
    /// ViewModel for the <see cref="Synthesis_WPF_MVVM.Dialogs.KMeansClusteringOptions"/> Dialog.
    /// </summary>
    public class KMeansOptionsViewModel
    {
        public bool OptionsExpanded { get; set; } = false;

        public bool ShowVisualisation { get; set; } = false;

        public int NumberOfTries { get; set; } = KMeansCluster.DefaultTries;

        public int NumberOfIterations { get; set; } = KMeansCluster.DefaultIterations;

        public int MinClusters { get; set; } = KMeansCluster.DefaultClustersMin;

        public int MaxClusters { get; set; } = KMeansCluster.DefaultClustersMax;

        public bool ShortestPath { get; set; } = true;
    }
}

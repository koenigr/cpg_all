using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Synthesis_WPF_MVVM.Model;
using System.Windows.Input;
using System.Windows.Controls;
using Microsoft.Practices.ServiceLocation;

namespace Synthesis_WPF_MVVM.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly IDataService _dataService;

        //============================================================================================================================================================================
        /// <summary>
        /// The current viewer view.
        /// </summary>
        private ViewModelBase _currentViewerViewModel;

        //============================================================================================================================================================================
        /// <summary>
        /// The current control view.
        /// </summary>
        private ViewModelBase _currentControlViewModel;

        //============================================================================================================================================================================
        /// <summary>
        /// Instance of one of the ViewModels.
        /// </summary>
        private static BuildingLayoutViewModel LayoutVM //= new BuildingLayoutViewModel(_dataService);
        {
            get
            {
                return ServiceLocator.Current.GetInstance<BuildingLayoutViewModel>();
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Instance of one of the ViewModels.
        /// </summary>
        private static NetworkViewModel NetworkVM //= new BuildingLayoutViewModel(_dataService);
        {
            get
            {
                return ServiceLocator.Current.GetInstance<NetworkViewModel>();
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Static instance of one of the ViewModels.
        /// </summary>
        private static Viewer1ViewModel Viewer1VM
        {
            get
            {
                return ServiceLocator.Current.GetInstance<Viewer1ViewModel>();
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Static instance of one of the ViewModels.
        /// </summary>
        private static Viewer2ViewModel Viewer2VM// = new Viewer2ViewModel();
        {
            get
            {
                return ServiceLocator.Current.GetInstance<Viewer2ViewModel>();
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Static instance of one of the ViewModels.
        /// </summary>
        private static NodeXLViewModel NodeXLVM //= new NodeXLViewModel();
        {
            get
            {
                return ServiceLocator.Current.GetInstance<NodeXLViewModel>();
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// The CurrentViewerView property.
        /// If the View is changed, we need to raise a property changed event (via INPC).
        /// </summary>
        public ViewModelBase CurrentViewerViewModel
        {
            get
            {
                return _currentViewerViewModel;
            }
            set
            {
                if (_currentViewerViewModel == value)
                    return;
                _currentViewerViewModel = value;
                RaisePropertyChanged("CurrentViewerViewModel");
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// The CurrentControlView property.
        /// If the View is changed, we need to raise a property changed event (via INPC).
        /// </summary>
        public ViewModelBase CurrentControlViewModel
        {
            get
            {
                return _currentControlViewModel;
            }
            set
            {
                if (_currentControlViewModel == value)
                    return;
                _currentControlViewModel = value;
                RaisePropertyChanged("CurrentControlViewModel");
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MainViewModel(IDataService dataService)
        {
            _dataService = dataService;
            _dataService.GetData(
                (item, error) =>
                {
                    if (error != null)
                    {
                        // Report error here
                        return;
                    }

                });

            CurrentControlViewModel = NetworkVM;
            CurrentViewerViewModel = Viewer1VM;

            //CurrentViewerViewModel = Viewer2VM;
            //CurrentControlViewModel =  LayoutVM;

            // --- change the views ---
            Viewer1ViewCommand = new RelayCommand(() => ExecuteViewer1ViewCommand());
            Viewer2ViewCommand = new RelayCommand(() => ExecuteViewer2ViewCommand());
            NodeXLViewCommand = new RelayCommand(() => ExecuteNodeXLViewCommand());

            // --- view control buttons ---
            Button2D3D_Click = new RelayCommand(() => ExecuteButton2D3D_Click(), () => true);
            ButtonZoomAll_Click = new RelayCommand(() => ExecuteButtonZoomAll_Click(), () => true);

            // --- object manipulation buttons ---
            ButtonMove_Click = new RelayCommand(() => ExecuteButtonMove_Click(), () => true);
            ButtonRotate_Click = new RelayCommand(() => ExecuteButtonRotate_Click(), () => true);
            ButtonScale_Click = new RelayCommand(() => ExecuteButtonScale_Click(), () => true);
            ButtonSize_Click = new RelayCommand(() => ExecuteButtonSize_Click(), () => true);
            FormClose = new RelayCommand(() => Cleanup(), () => true);
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Simple property to hold the 'NetworkViewCommand' - when executed it will change the control view to the 'BuildingLayoutView'
        /// </summary>
        public ICommand Viewer1ViewCommand { get; private set; }
        //-------------------------------------------------------------
        /// <summary>
        /// Set the CurrentControlViewModel to 'NetworkViewModel'
        /// </summary>
        private void ExecuteViewer1ViewCommand()
        {
            CurrentControlViewModel = NetworkVM;
            CurrentViewerViewModel = Viewer1VM;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Simple property to hold the 'ViewerViewCommand' - when executed it will change the viewer view to the 'Viewer2View'
        /// </summary>
        public ICommand Viewer2ViewCommand { get; private set; }
        //-------------------------------------------------------------
        /// <summary>
        /// Set the CurrentViewModel to 'Viewer2ViewModel'
        /// </summary>
        private void ExecuteViewer2ViewCommand()
        {
            CurrentControlViewModel = LayoutVM;
            CurrentViewerViewModel = Viewer2VM;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Simple property to hold the 'NodeXLViewCommand' - when executed it will change the control view to the 'BuildingLayoutView'
        /// </summary>
        public ICommand NodeXLViewCommand { get; private set; }
        //-------------------------------------------------------------
        /// <summary>
        /// Set the CurrentControlViewModel to 'BuildingLayoutViewModel'
        /// </summary>
        public void ExecuteNodeXLViewCommand()
        {
            CurrentControlViewModel = LayoutVM;
            CurrentViewerViewModel = NodeXLVM;
        }

        private void GetViewer()
        {

        }

        //============================================================================================================================================================================
        public ICommand Button2D3D_Click { get; private set; }
        //-------------------------------------------------------------
        /// <summary>
        /// Switch for the active camera from 2D to 3D and vice versa.
        /// </summary>
        private void ExecuteButton2D3D_Click()
        {
            //System.Type variableName = CurrentViewerViewModel.GetType();
            //var vmt = (new ViewModelLocator()).variableName;
            if (CurrentViewerViewModel.GetType() == typeof(Viewer2ViewModel))
            {
                var vm = (new ViewModelLocator()).Viewer2ViewModel;
                if (vm.ActiveCameraControl != null)
                {
                    vm.ActiveCameraControl.CameraChange();
                }
                else
                {
                    vm.ViewControls[0].CameraChange();
                }
            }
            else if (CurrentViewerViewModel.GetType() == typeof(Viewer1ViewModel))
            {
                var vm = (new ViewModelLocator()).Viewer1ViewModel;
                if (vm.ActiveCameraControl != null)
                {
                    vm.ActiveCameraControl.CameraChange();
                }
                else
                {
                    vm.ViewControls[0].CameraChange();
                }
            }
        }

        //============================================================================================================================================================================
        public ICommand ButtonZoomAll_Click { get; private set; }
        //-------------------------------------------------------------
        /// <summary>
        /// Zoom all for the active camera.
        /// </summary>
        private void ExecuteButtonZoomAll_Click()
        {
            var vm = (new ViewModelLocator()).Viewer2ViewModel;
            if (vm.ActiveCameraControl != null)
            {
                vm.ActiveCameraControl.ZoomAll();
            }
            else if(vm.ViewControls.Count > 0)
            {
                vm.ViewControls[0].ZoomAll();
            }
        }

        //============================================================================================================================================================================
        public ICommand ButtonMove_Click { get; private set; }
        private void ExecuteButtonMove_Click()
        {
            var vm = (new ViewModelLocator()).Viewer2ViewModel;
            vm.MouseMode = "Move";
            //statusLabel.Text = "Drag and drop building to MOVE it.";
        }

        //============================================================================================================================================================================
        public ICommand ButtonRotate_Click { get; private set; }
        private void ExecuteButtonRotate_Click()
        {
            var vm = (new ViewModelLocator()).Viewer2ViewModel;
            vm.MouseMode = "Rotate";
        }

        //============================================================================================================================================================================
        public ICommand ButtonScale_Click { get; private set; }
        private void ExecuteButtonScale_Click()
        {
            var vm = (new ViewModelLocator()).Viewer2ViewModel;
            vm.MouseMode = "Scale";
        }

        //============================================================================================================================================================================
        public ICommand ButtonSize_Click { get; private set; }
        private void ExecuteButtonSize_Click()
        {
            var vm = (new ViewModelLocator()).Viewer2ViewModel;
            vm.MouseMode = "Size";
        }

        //============================================================================================================================================================================
        public ICommand FormClose { get; private set; }
        public override void Cleanup()
        {
            // Clean up if needed
            CurrentControlViewModel.Cleanup();
            base.Cleanup();
        }
    }
}
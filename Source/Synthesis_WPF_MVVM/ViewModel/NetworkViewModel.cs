﻿using CPlan.Evaluation;
using CPlan.Geometry;
using CPlan.Geometry.PlotGeneration;
using CPlan.Optimization;
using CPlan.Optimization.Chromosomes;
using CPlan.VisualObjects;
using FW_GPU_NET;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GeoAPI.Geometries;
using Synthesis_WPF_MVVM.Helpers;
using netDxf;
using netDxf.Entities;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using NetTopologySuite.Operation.Polygonize;
using OpenTK;
using QuickGraph;
using QuickGraph.Algorithms.Observers;
using QuickGraph.Algorithms.ShortestPath;
using Synthesis_WPF_MVVM.Dialogs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Microsoft.Win32;
using CPlan.Optimization.ClusterAnalysis;

namespace Synthesis_WPF_MVVM.ViewModel
{
    using Cluster = IEnumerable<Vector2d>;
    using StreetNetwork = UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>;

    //struct used to sort the paths by their length
    public struct ShortestPathForSorting
    {
        public IEnumerable<UndirectedEdge<Vector2d>> path;
        public double length;
    }

    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class NetworkViewModel : ViewModelBase
    {
        private Poly2D _border;
        private Poly2D _offsetBorder;
        private UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> _initailNet;
        private InstructionTreePisa _activeNetwork;
        private Viewer1ViewModel viewModel = new ViewModelLocator().Viewer1ViewModel;
        private Dictionary<UndirectedEdge<Vector2d>, double> _biDirectEdgeCostsDist;
        private Dictionary<UndirectedEdge<Vector2d>, double> _biDirectEdgeCostsAngle;
        private HoffmanPavleyRankedShortestPathAlgorithmMaxDist<Vector2d, UndirectedEdge<Vector2d>> _hoffmanAlgorithm;
        private DijkstraShortestPathAlgorithm<Vector2d, UndirectedEdge<Vector2d>> _dijkstra;
        private List<UndirectedEdge<Vector2d>> _inverseQLines;
        private List<ShortestPathForSorting> _finalRedundantPaths;

        private StreetNetwork graph;
        private IEnumerable<Cluster> clusters;
        private int progressValue = 0;
        /// <summary>
        /// Paint elements with the uiFactory to avoid concurrency issues
        /// </summary>
        private readonly TaskFactory uiFactory;

        ObjectsAllList AllObjectsList = new ObjectsAllList();

        private CPlan.Geometry.PlotGeneration.Polygon[] Frontage;
        //private Subdivision LayoutCopy = new Subdivision();
        //private Subdivision subGraph = new Subdivision();

        public bool MetricPathsFirst { get; set; }
        public int MaxNrPathes { get; set; }
        public float RedundancyFactor { get; set; }
        public int PathTargetPt { get; set; }
        public int NrNodesInGraph { get; set; }
        public int ShowPathNr { get; set; }
        public int NrPathes { get; set; }
        public int ShowFinalPathNr { get; set; }
        public int NrFinalPathes { get; set; }
        public string CurMetricPathLength { get; set; }
        public string CurAngularPathLength { get; set; }
        public string ComputedPathes { get; set; }
        public int ProgressValue
        {
            get
            {
                return progressValue;
            }
            set
            {
                progressValue = value;
                RaisePropertyChanged("ProgressValue");
                RaisePropertyChanged("ShowProgressBar");
            }
        }
        public Visibility ShowProgressBar
        {
            get
            {
                return ProgressValue < 100 && ProgressValue > 0 ? Visibility.Visible : Visibility.Hidden;
            }
        }


        public double buildingWidth { get; set; }
        public double buildingLength { get; set; }

        IEnumerable<IEnumerable<UndirectedEdge<Vector2d>>> _rankedPaths;
        List<Line2D> _currentVisualizedGraph;
        Dictionary<Vector2d, UndirectedEdge<Vector2d>> _inverseEdgesMapping;



        /// <summary>
        /// Initializes a new instance of the NetworkViewModel class.
        /// </summary>
        public NetworkViewModel()
        {
            MetricPathsFirst = true;
            PathTargetPt = 12;
            RedundancyFactor = 1.2f;
            MaxNrPathes = 1000;
            NrPathes = MaxNrPathes;
            NrNodesInGraph = 2;
            CurMetricPathLength = "metrical dist: 000";
            CurAngularPathLength = "angular dist: 000";
            ComputedPathes = "computed pathes: 000";
            uiFactory = new TaskFactory(TaskScheduler.FromCurrentSynchronizationContext());
            // TestSubdivision = new RelayCommand(() => TestSubdivisionExecute(), () => true);
        }

        
        // Network modeling computations

        public ICommand GrowTestGraph => new RelayCommand(GrowTestGraphExecute);

        private void GrowTestGraphExecute()
        {
            viewModel.MainCamera.AllObjectsList.ObjectsToDraw.Clear();
            viewModel.MainCamera.AllObjectsList.ObjectsToInteract.Clear();

            InitialTestGraph();

            //_activeNetwork = new InstructionTreePisa(ControlParameters.TreeDepth, _initailNet, _border);

            _activeNetwork = new InstructionTreePisa(ControlParameters.TreeDepth, _initailNet, _border);
            var networkQ = _activeNetwork.CreateStreetNetwork();

            List<Line2D> edges = GraphTools.ToListLine2D(networkQ);
            AnalyseNetwork(networkQ, viewModel.MainCamera);

            //CreateBlocks(edges);
            //DrawNetwork(edges);

            viewModel.MainCamera.ZoomAll();

            AnalyseNetwork(networkQ, viewModel.MainCamera);

            // CreateBlocks(edges, true);  // good results using Tektosyne
            var blocks = CreateBlocks(edges);        // new implementation results
            DrawBlocks(blocks);
            //    List<Poly2D> part_blocks = new List<Poly2D>();

            //for (int i = 0; i < 3; ++i)
            //{
            //    part_blocks.Add(_blocks[i]);
            //}
            //GenerateParcelsBuildings(part_blocks);
            //GenerateParcelsBuildings(_blocks);

            viewModel.MainCamera.ZoomAll();
			viewModel.MainCamera.Invalidate();

        }

        public ICommand TestSubdivision => new RelayCommand(TestSubdivisionExecute);

        private void TestSubdivisionExecute()
        {
           // test.sw = new StreamWriter(@"C:\\Users\\Yufan Miao\\Desktop\\work\\python\\test.txt");
            //GenerateParcelsBuildings(_blocks);


            //   // GenerateParcelsReversedTree();
            //   ////////for (int i = 0; i < _border.Edges.Count; i++)
            //   ////////{
            //   ////////    test.sw.WriteLine(_border.Edges[i].Start.X + " " + _border.Edges[i].Start.Y + " " + _border.Edges[i].End.X + " " + _border.Edges[i].End.Y);
            //   ////////}

            //   test.sw.Close();
            //initialize view
            // var vm = (new ViewModelLocator()).Viewer1ViewModel;
            //initialize polygons convex/concave

            //ControlParameters.MinLength = 30;
            //ControlParameters.LengthRange = 100;
            //ControlParameters.MinDistance = 20;

            //ControlParameters.MinLength = 100;
            //ControlParameters.LengthRange = 500;
            //ControlParameters.MinDistance = 100;

           // float b0 = 10;
           // float by = 800;
           // float bx = 1000;
           // Vector2d[] polygonNodes = new Vector2d[4];
           // polygonNodes[0] = new Vector2d(b0, b0);
           // polygonNodes[1] = new Vector2d(b0 + bx, b0);
           // polygonNodes[2] = new Vector2d(b0 + bx , b0 + by);
           // polygonNodes[3] = new Vector2d(b0 , b0 + by);



           // _border = new Rect2D(b0, by, bx, by).ToPoly2D();

           // Line2D[] edges = new Line2D[4];

           // double rotatedAngle = Math.PI / 4.0;
           // //double rotatedAngle = 0;
           // for (int i = 0; i < polygonNodes.Length; ++i)
           // {
           //     Vector2d coords = polygonNodes[i];
           //     double new_x = coords.X * Math.Cos(rotatedAngle) - coords.Y * Math.Sin(rotatedAngle);
           //     double new_y = coords.X * Math.Sin(rotatedAngle) + coords.Y * Math.Cos(rotatedAngle);
           
           //     polygonNodes[i] = new Vector2d(new_x, new_y);
           // }
           // //for (int i = 0; i < polygonNodes.Length; ++i)
           // //{
           // //    Line2D line = new Line2D(polygonNodes[i], polygonNodes[(i + 1) % polygonNodes.Length]);
           // //    edges[i] = line;
           // //    test.sw.WriteLine(line.Start.X + " " + line.Start.Y + " " + line.End.X + " " + line.End.Y);
           // //}

           // Poly2D polygonBorder = new Poly2D(polygonNodes);
           // List<Poly2D> blocks = new List<Poly2D>();
           // blocks.Add(polygonBorder);

           //// _offsetBorder = polygonBorder;

           // GenerateParcelsBuildings(blocks);

           //// test.sw.Close();

            ////Subdivision subGraph = Subdivision.FromLines(edges);
            ////GenerateParcelsBuildings(subGraph);

            ////view controls
            ////vm.MainCamera.ZoomAll();
            ////vm.MainCamera.Invalidate();
        }

        public ICommand TestReversedSTree => new RelayCommand(TestReversedSTreeExecute);

        private void TestReversedSTreeExecute()
        {
            // CPlan.Geometry.ReversedSTree.RevSTree rSTree = new CPlan.Geometry.ReversedSTree.RevSTree(4, new List<double>() {0.25,0.25, 0.25, 0.25 });

        }

        private void GenerateParcelsBuildings(List<Poly2D> blocks) //Subdivision subGraph)
        {
            //var vm = (new ViewModelLocator()).Viewer1ViewModel;
            IList<Vector2d[]> Polys = GeometryConverter.ToListofVector2dArray(blocks);

            Subdivision subGraph = Subdivision.FromPolygons(Polys);
            //Subdivision subGraph = new Subdivision();
            //Subdivision subGraph = tmpSubGraph;

            CreateSubdivision(subGraph);

        }

        private void GenerateParcelsReversedTree()
        {
            
        }

        private void ReCreateGraph(Subdivision curGraph)
        {
            // --- draw the elements of the graph ---
            AllObjectsList.ObjectsToDraw.Clear();
            AllObjectsList.ObjectsToInteract.Clear();
            AllObjectsList = new ObjectsAllList();
            Line2D[] graphLines = curGraph.ToLines();
            foreach (Line2D line in graphLines)
            {
                AllObjectsList.Add(new GLine(line));
            }

            Vector2d[] graphPts = curGraph.Nodes.ToArray();
            foreach (Vector2d point in graphPts)
            {
                AllObjectsList.Add(new GCircle(point, 2, 10));
            }

        }

        public void CreateSubdivision(Subdivision subGraph)
        {
            //List<Poly2D> polygons = GeometryConverter.ToListPoly2D(polygonsT);
            Subdivision subdivForPolys = (Subdivision)subGraph.Clone();
            //List<Vector2d> curVertices = subdivForPolys.Vertices.Keys.ToList();
            List<Vector2d> polyAsList = new List<Vector2d>();
            List<List<Vector2d>> validPolys = new List<List<Vector2d>>();
            //List<List<Vector2d>> problemPolys = new List<List<Vector2d>>();
            List<List<Vector2d>> boundaryPolys = new List<List<Vector2d>>();

            double connectionDist = 5.0d;
            double streetOffset = 6.0d;
            double density = 0.3d;
            int buildingDepthMin = 20;
            int buildingDepthMax = 30;
            double buildingFrontLine = 0.0d;
            double buildingRestrictionLine = 0.0d;
            //double maxBDepth = 0.0d;
        
            double maxArea = 2000.0d;


           
            double buildingW = this.buildingWidth ;
            double buildingL = this.buildingLength;

            List<Line2D> border_edges = new List<Line2D>();
            //for (int i = 0; i < 1; ++i)
            //{
            //    //border_edges.Add(_offsetBorder.Edges[i]);
            //    // test.sw.WriteLine(border_edges[i-2].Start.X + " " + border_edges[i-2].Start.Y + " " + border_edges[i-2].End.X + " " + border_edges[i-2].End.Y);
            //}
            ////double maxArea = minParcelHeight*minParcelHeight;
            //this.ClipPolygonsAndAddTosubGraph(curPolys);
            // Parameter LayoutParameter = new Parameter(connectionDist, maxArea, streetOffset,buildingFrontLine, buildingRestrictionLine, density, buildingDepthMin, buildingDepthMax, minParcelHeight, minParcelWidth);
            //Line2D passedInLine = new Line2D(10.0d, 10.0d, 10.0d, 500.0d);
            // border_edges.Add(passedInLine);
            double scalingFactor = 1.0d;
            Parameter LayoutParameter = new Parameter(connectionDist, maxArea, streetOffset, buildingFrontLine, buildingRestrictionLine, density, buildingDepthMin, buildingDepthMax,  buildingW, buildingL, scalingFactor, border_edges);
            //Vector2d[][] curPolys2 = subGraph.ToPolygons();

            //System.Windows.MessageBox.Show(curPolys2.Length.ToString());

            //STree tmpTree = new STree(subGraph, maxArea, connectionDist, streetOffset, density);

           

            STree tmpTree = new STree(subGraph, LayoutParameter);
            List<CPlan.Geometry.PlotGeneration.Node> nodes = tmpTree.Leaves;

            var vm = (new ViewModelLocator()).Viewer1ViewModel;
            vm.MainCamera.AllObjectsList.ObjectsToDraw.Clear();
            vm.MainCamera.AllObjectsList.ObjectsToInteract.Clear();
            /*
            // tmpTree.DefineLeavesSiblings();
            // tmpTree.PrintLeaveClusters();
            //STree tmpTree = new STree(subGraph,LayoutParameter);

            //test.sw = new StreamWriter(@"C:\\Users\\yufan\\Desktop\\work\\test.txt");
            //for (int i = 0; i < tmpTree.Syntaxes.Length; i++)
            //{
            //    test.sw.WriteLine(tmpTree.Syntaxes[i]);
            //}
            //test.sw.Close();
            // CPlan.Geometry.PlotGeneration.Polygon tmpPoly;
            //String theSyntax = "";
            //foreach (String synt in tmpTree.Syntaxes)
            //{
            //    theSyntax += synt;
            //}
            //System.Windows.MessageBox.Show(theSyntax);
            //Vector2d[][]  tmpPolys = tmpTree.ToPolygons();

            //tmpTree.SlicingLayout.ToPolygons();

            ////for (int i = 0; i < tmpTree.Leaves.Count; i++)
            ////{
            ////    CPlan.Geometry.PlotGeneration.Node curNode = tmpTree.Leaves[i];
            ////    //test.sw.Write(curNode.Level + " " + curNode.Region.GetArea() + " ");
            ////    for (int j = 0; j < curNode.Region.Vertices.Length; j++)
            ////    {
            ////        CPlan.Geometry.PlotGeneration.Polygon curPolygon = curNode.Region;
            ////        test.sw.Write(curPolygon.Vertices[j].X + " " + curPolygon.Vertices[j].Y + " ");
            ////    }
            ////    test.sw.WriteLine();
            ////}
            //Frontage = tmpTree.BuildingBlocks;

            //for (int j = 0; j < tmpPolys.GetLength(0); j++)
            //{
            //    polyAsList = tmpPolys[j].ToList();
            //    validPolys.Add(polyAsList);
            //}

            //for (int i = 0; i < _blocks.Count; i++)
            //{
            //    for (int j = 0; j < _blocks[i].Points.GetLength(0); j++)
            //    {
            //        boundaryPolys.Add(_blocks[i].Points[j].ToList());
            //    }


            //}
            //for (int i = 0; i < curPolys2.GetLength(0); i++)
            //{
            //    tmpPoly = new CPlan.Geometry.PlotGeneration.Polygon(curPolys2[i]);

            //    if (tmpPoly.GetArea() < maxArea)
            //    {
            //        polyAsList = curPolys2[i].ToList();
            //        validPolys.Add(polyAsList);
            //    }
            //}

            //LayoutCopy = subGraph.Clone() as Subdivision;
            // subGraph = tmpTree.Graph.Clone() as Subdivision;
            //Console.WriteLine("No of Edges of subGraph: " + subGraph.Edges.Count);

            //this.ReCreateGraph(subGraph);
            //Console.WriteLine("No of Edges of subGraph after Recreation:


           
            //List<GPolygon> polys = new List<GPolygon>();
            //List<float> areas = new List<float>();
            // Vector4[] colors = GeoObj.Helper.ConvertColor.CreateFFColor(areas.ToArray());


            //for (int i = 0; i < validPolys.Count; i++)
            //{



            //    Vector2d[] arrPoly = validPolys[i].ToArray();
            //    GPolygon curPoly = new GPolygon(arrPoly);
            //   // curPoly.FillColor = colors[i];
            //    curPoly.BorderWidth = 2;
            //    curPoly.BorderColor = ConvertColor.CreateFFColor(Color.Red);

            //    vm.MainCamera.AllObjectsList.Add(curPoly);
            //   // AllObjectsList.Add(curPoly);
            //  //   polys.Add(curPoly);
            //    //Single curArea = Convert.ToSingle(Math.Pow(curPoly.GetArea(), 0.5));
            //    //areas.Add(curArea);
            //}


            //for (int i = 0; i < boundaryPolys.Count; i++)
            //{
            //    Vector2d[] arrPoly = boundaryPolys[i].ToArray();
            //    GPolygon curPoly = new GPolygon(arrPoly);
            //    // curPoly.FillColor = colors[i];
            //    curPoly.BorderWidth = 2;
            //    curPoly.BorderColor = ConvertColor.CreateFFColor(Color.Blue);

            //    vm.MainCamera.AllObjectsList.Add(curPoly);
            //}

            //GeoObj.Extensions.CollectionsExtend.CutMinValue(areas, Fortran.Min(areas.ToArray<Single>()));
            //GeoObj.Extensions.CollectionsExtend.Normalize(areas);
            //GeoObj.Extensions.CollectionsExtend.IntervalExtension(areas, Fortran.Max(areas.ToArray<Single>()));
            //Vector4[] colors = GeoObj.Helper.ConvertColor.CreateFFColor(areas.ToArray());
            //for (int i = 0; i < polys.Count; i++)
            //{
            //    GPolygon curPoly = polys[i];
            //   // curPoly.FillColor = colors[i];
            //    curPoly.BorderWidth = 5;
            //    curPoly.BorderColor = ConvertColor.CreateFFColor(Color.Red);
            //    vm.MainCamera.AllObjectsList.Add(curPoly);
            //}
            //  List<Vector2d[]> pol = Polygon.ToListVector2dArray(validPolys);

            //Line2D[] Lines = subGraph.ToLines();

            //for (int n = 0; n < Lines.Length; n++)
            //{
            //    var line2Draw = new GLine(Lines[n]);
            //    line2Draw.Color = ConvertColor.CreateColor4(Color.Blue);
            //    vm.MainCamera.AllObjectsList.Add(line2Draw, true, false);
            //}


            //foreach (var pnt in subGraph.Nodes)
            //{
            //    GCircle pnt2Draw = new GCircle(pnt, 0.2f);

            //    pnt2Draw.Filled = false;
            //    pnt2Draw.BorderColor = ConvertColor.CreateColor4(Color.DarkGray);
            //    vm.MainCamera.AllObjectsList.Add(pnt2Draw, true, false);
            //}
            */
            foreach (CPlan.Geometry.PlotGeneration.Node node in nodes)
            {


                Vector2d[] arrPoly = node.Region.Vertices;
                GPolygon curPoly = new GPolygon(arrPoly);
                // curPoly.FillColor = colors[i];
                curPoly.BorderWidth = 2;
                curPoly.BorderColor = ConvertColor.CreateFFColor(Color.Red);

                vm.MainCamera.AllObjectsList.Add(curPoly);
                //specify the building layout style in node.Building[0]: 0 is row; 1 is blocks; 2 is column; 3 is freestanding
                CPlan.Geometry.PlotGeneration.Polygon building_style = node.Building[0];

                if (node.Building != null && building_style != null && building_style.Vertices.Length > 0)
                {
                    Vector2d[] verts = building_style.Vertices;
                    var polyToDraw = new GPolygon(verts);

                    polyToDraw.Filled = true;
                    polyToDraw.FillColor = ConvertColor.CreateColor4(Color.Black);
                    polyToDraw.BorderColor = ConvertColor.CreateColor4(Color.Blue);
                    polyToDraw.BorderWidth = 2;
                    vm.MainCamera.AllObjectsList.Add(polyToDraw, true, false);
                    // AllObjectsList.Add(polyToDraw,true,false);
                    //if (node.FrontageLines != null)
                    //{
                    //    foreach (Line2D fline in node.FrontageLines)
                    //    {

                    //        var curLine = new GLine(fline);
                    //        curLine.Width = ControlParameters.LineWidth;
                    //        vm.MainCamera.AllObjectsList.Add(curLine, true, false);
                    //    }

                    //}
                    //if (node.RestrictionLines != null)
                    //{
                    //    foreach (Line2D rline in node.RestrictionLines)
                    //    {
                    //        var curLine = new GLine(rline);
                    //        curLine.Width = ControlParameters.LineWidth;
                    //        vm.MainCamera.AllObjectsList.Add(curLine, true, false);
                    //    }
                    //}
                }
            }

            //// AllObjectsList.ObjectsToDraw.Reverse();
            vm.MainCamera.ZoomAll();
           vm.MainCamera.Invalidate();
        }
        
       
        private List<Line2D> AnalyseNetwork(UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> network, GLCameraControl viewControl)
        {
            Graph fwInverseGraph;
            ShortestPath shortPahtesAngular;
            Graph fwGraph = GraphTools.GenerateFWGpuGraph(network);
            fwInverseGraph = GraphTools.GenerateInverseDoubleGraph(fwGraph);
            shortPahtesAngular = new ShortestPath(network); //, realAngle);
            shortPahtesAngular.Evaluate(new System.Windows.Forms.ToolStripProgressBar(), fwGraph, fwInverseGraph);
            //  null);//FloydWarshall();//Dijikstra();//

            Vector2d[] nodes = network.Vertices.ToArray();
            UndirectedEdge<Vector2d>[] tempEdges = network.Edges.ToArray();
            var edges = new List<Line2D>();
            foreach (var tempEdge in tempEdges)
            {
                edges.Add(new Line2D(tempEdge.Source, tempEdge.Target));
            }
            Vector4[] ffColors = null;

            ffColors = ConvertColor.CreateFFColor4(shortPahtesAngular.GetNormChoiceArray()[0]);

            DrawNetworkAnalysis(ffColors, edges.ToArray(), nodes, new Vector2d(0, 0), viewControl);

            fwGraph.ClearGraph();
            fwGraph.Dispose();
            fwInverseGraph.Dispose();

            //glCameraMainWindow.ZoomAll();
            return edges;
        }

        private void InitialTestGraph()
        {
            UndirectedEdge<Vector2d> iniEdge;
            //ControlParameters.MinLength = 30;
            //ControlParameters.LengthRange = 100;
            //ControlParameters.MinDistance = 20;

            //float b0 = 10;
            //float by = 400;
            //float bx = 500;
            ControlParameters.MinLength = 100;
            ControlParameters.LengthRange = 5000;
            ControlParameters.MinDistance = 100;
            ControlParameters.ParcelWidth = 20;
            ControlParameters.ParcelDepth = 30;

            float b0 = 10;
            float by = 8000;
            float bx = 10000;


            _border = new Rect2D(b0, by, bx, by).ToPoly2D();

            int max_num_parcels = (int)Math.Floor(_border.Area/(ControlParameters.ParcelDepth*ControlParameters.ParcelWidth));


            var middle = new Vector2d(_border.BoundingBox().Width / 2, _border.BoundingBox().Height / 2);

            //initial graph for optimization
            _initailNet = new UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>();

            // add seed streets
            double iniLgth = -10;
            Vector2d[] iniPoints;
            iniPoints = new Vector2d[2];
            // segment in the middle
            iniPoints[0] = new Vector2d(-20 + middle.X, 20 + middle.Y);
            iniPoints[1] = new Vector2d(30 + middle.X, 20 + middle.Y);
            iniEdge = new UndirectedEdge<Vector2d>(iniPoints[0], iniPoints[1]);
            _initailNet.AddVerticesAndEdge(iniEdge);

            // add boder segments
            Vector2d[] polyPts = new Vector2d[]
            {
                new Vector2d(b0, b0), new Vector2d(b0, middle.Y + 20), new Vector2d(b0, by),
                new Vector2d(middle.X - 40, by), new Vector2d(bx, by),
                new Vector2d(bx, middle.Y - 20), new Vector2d(bx, b0)
            };
            _border = new Poly2D(polyPts);
            _offsetBorder = _border.Clone();
            // segments at the borders
            iniPoints[0] = new Vector2d(b0, middle.Y + 20);
            iniPoints[1] = new Vector2d(b0 + iniLgth, middle.Y + 20);
            iniEdge = new UndirectedEdge<Vector2d>(iniPoints[0], iniPoints[1]);
            _initailNet.AddVerticesAndEdge(iniEdge);

            iniPoints[0] = new Vector2d(bx, middle.Y - 20);
            iniPoints[1] = new Vector2d(bx - iniLgth, middle.Y - 20);
            iniEdge = new UndirectedEdge<Vector2d>(iniPoints[0], iniPoints[1]);
            _initailNet.AddVerticesAndEdge(iniEdge);

            iniPoints[0] = new Vector2d(middle.X - 40, by + 0);
            iniPoints[1] = new Vector2d(middle.X - 40, by - iniLgth);
            iniEdge = new UndirectedEdge<Vector2d>(iniPoints[0], iniPoints[1]);
            _initailNet.AddVerticesAndEdge(iniEdge);
        }

         

        public ICommand ImportGraphCall => new RelayCommand(ImportGraphExecute);
        private void ImportGraphExecute()
        {
            graph = ImportGraph();
            if (graph == null) { return; }
            DrawNetwork(graph.Edges);
            viewModel.MainCamera.ZoomAll();
        }

        public ICommand ExportGraphCall => new RelayCommand(ExportGraphExecute);
        private void ExportGraphExecute()
        {
            if (graph == null)
            {
                MessageBox.Show("Didn't find a graph that can be saved. Create or open a graph first.", "Error - Saving Graph", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            ExportGraph(graph);
        }

        public ICommand NormaliseGraphCall => new RelayCommand(NormaliseGraphExecute);
        private void NormaliseGraphExecute()
        {
            if (graph == null)
            {
                MessageBox.Show("Didn't find a graph that can be normalised. Create or open a graph first.", "Error - Normalising Graph", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            //StreetPatternAbsoluteAngle.AddJunctions(graph);
            StreetPatternAbsoluteAngle.Snapping(graph);
        }

        // converting an imported graph to an InstructionTreeABS and back
        public ICommand CreateTreeAndBack => new RelayCommand(CreateTreeAndBackExecute);
        private async void CreateTreeAndBackExecute()
        {
            graph = graph ?? ImportGraph();
            if (graph == null) { return; }

            var streetGenerator = new StreetPattern(null);
            streetGenerator.QGraph = new UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>();
            streetGenerator.QGraph.AddVerticesAndEdgeRange(graph.Edges.ToList());

            // convert the network to InstructionTree chromosome
            var treeFromGraph =  await Task.Run(() => new TreeGenerator().TreeFromNetwork(streetGenerator.QGraph));
            var streetGeneratorAbsAngle = new StreetPatternAbsoluteAngle();
            graph = streetGeneratorAbsAngle.GrowStreetPattern(treeFromGraph);
            await uiFactory.StartNew(() => DrawNetwork(graph.Edges));

            viewModel.MainCamera.ZoomAll();
        }

        // converting an imported graph to an InstructionTree
        public ICommand MutateGraph => new RelayCommand(MutateGraphExecute);
        private async void MutateGraphExecute()
        {
            if(graph == null)
            {
                graph = ImportGraph();
            }
            if (graph == null)
            {
                return;
            }
            // convert the network to InstructionTree chromosome
            var treeFromGraph = await Task.Run(() => new TreeGenerator().TreeFromNetwork(graph));
            treeFromGraph.Mutate();
            var streetGeneratorAbsAngle = new StreetPatternAbsoluteAngle().GrowStreetPattern(treeFromGraph);
            await uiFactory.StartNew(() => DrawNetwork(streetGeneratorAbsAngle.Edges));

            viewModel.MainCamera.ZoomAll();
        }

        private InstructionTreePisa NetworkToChromosome(UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> baseGraph)
        {
            // Analyse the street network
            InstructionTreePisa treeFromGraph = new InstructionTreePisa();
            UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> iniNet = new UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>();
            if (baseGraph == null)
            {
                return treeFromGraph;
            }
            var lines2DSet = baseGraph.Edges.Select(line => new Line2D(line.Source, line.Target));

            var workinGraph = new UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>();
            workinGraph.AddVerticesAndEdgeRange(baseGraph.Edges);

            // Find the starting segment/nodes
            // use the nearest segment to the centre
            var sum = new Vector2d(0, 0);
            foreach (var line in lines2DSet)
            {
                sum += line.Center;
            }
            Vector2d globalCenter = sum / baseGraph.Edges.Count();

            // find nearest segment to centre
            double dist;
            Line2D centerLine = GeoAlgorithms2D.NearestLineToPoint(lines2DSet.ToArray(), globalCenter, out dist);

            // initialize the new IntructionTree
            UndirectedEdge<Vector2d> iniEdge = new UndirectedEdge<Vector2d>(centerLine.Start, centerLine.End);
            List<InstructionNodeAbsolute> openEnds = new List<InstructionNodeAbsolute>();
            List<InstructionNodeAbsolute> newOpenEnds;

            iniNet.AddVerticesAndEdge(iniEdge);
            treeFromGraph.InitalNetwork = iniNet;
            int idx = 0;

            // add the first seed edge(s)
            InstructionNodeAbsolute seedNodeA = new InstructionNodeAbsolute(idx++);
            InstructionNodeAbsolute seedNodeB = new InstructionNodeAbsolute(idx++);
            seedNodeA.Position = iniEdge.Source;
            seedNodeB.Position = iniEdge.Target;

            seedNodeA.Angle = 0;
            seedNodeA.Length = 1;
            seedNodeA.MaxConnectivity = 4;
            seedNodeA.ParentIndex = seedNodeB.Index;
            treeFromGraph.Nodes.Add(seedNodeA);
            openEnds.Add(seedNodeA);

            seedNodeB.Angle = 0;
            seedNodeB.Length = 1;
            seedNodeB.MaxConnectivity = 4;
            seedNodeB.ParentIndex = seedNodeA.Index;
            treeFromGraph.Nodes.Add(seedNodeB);
            openEnds.Add(seedNodeB);

            // remove the already added lines from the graph
            UndirectedEdge<Vector2d> removeCandA = new UndirectedEdge<Vector2d>(iniEdge.Source, iniEdge.Target);
            UndirectedEdge<Vector2d> removeCandB = new UndirectedEdge<Vector2d>(iniEdge.Target, iniEdge.Source);
            workinGraph.RemoveEdge(removeCandA);
            workinGraph.RemoveEdge(removeCandB);

            // walk from starting segment to neighbours (recursive for all openEnds, similar to FindBranch) 
            while (openEnds.Any())
            {
                newOpenEnds = new List<InstructionNodeAbsolute>();
                // look at all current openEnds
                foreach (InstructionNodeAbsolute curOpenNode in openEnds)
                {
                    var openEdges = workinGraph.AdjacentEdges(curOpenNode.Position);

                    // read the parameters angle and distance, add max connectivity
                    foreach (UndirectedEdge<Vector2d> testEdge in openEdges.ToList())
                    {
                        // which of the two end points of the edge is the new point?
                        Vector2d newPoint;
                        if (curOpenNode.Position == testEdge.Source)
                        {
                            newPoint = testEdge.Target;
                        }
                        else if (curOpenNode.Position == testEdge.Target)
                        {
                            newPoint = testEdge.Source;
                        }
                        else
                        {
                            break;
                        }
                        // test if there is already an instruction node for this position
                        InstructionNodeAbsolute testNode =
                            treeFromGraph.Nodes.Find(InstructionNodeAbsolute => InstructionNodeAbsolute.Position == newPoint);
                        if (testNode != null)
                            continue;

                        // find the parent node and calculate the parameters for the new node
                        InstructionNodeAbsolute nodeParent =
                            treeFromGraph.Nodes.Find(InstructionNodeAbsolute => InstructionNodeAbsolute.Index == curOpenNode.ParentIndex);
                        InstructionNodeAbsolute newNode = new InstructionNodeAbsolute(idx++);
                        newNode.Position = newPoint;
                        //GeoAlgorithms2D.CartesianToPolar(curNode.Position2d, out angle, out length);
                        newNode.Angle = Convert.ToSingle(GeoAlgorithms2D.AngleBetween(nodeParent.Position, newNode.Position, curOpenNode.Position));
                        newNode.Length = Convert.ToSingle(GeoAlgorithms2D.DistancePoints(testEdge.Source, testEdge.Target));
                        newNode.MaxConnectivity = 4;
                        newNode.ParentIndex = curOpenNode.Index;

                        // add new nodes to the InstructionTree
                        treeFromGraph.Nodes.Add(newNode);
                        workinGraph.RemoveEdge(testEdge);

                        newOpenEnds.Add(newNode);
                    }
                }
                openEnds = newOpenEnds;
            }
            // save chromosome
            return treeFromGraph;
        }
   
        // Path computations       
        //Yufan test=============================================================================
        //private class LineStringAdder : IGeometryComponentFilter
        //{
        //    PolygonizeGraph _graph;
        //    public LineStringAdder(PolygonizeGraph graph)
        //     {
        //        _graph = graph; 
        //     }

        //public void Filter(IGeometry g)
        //    {
        //        var lineString = g as ILineString;
        //        if (lineString != null)
        //            _graph.AddEdge(lineString);

        //    }
        //}
        
        //test==========================================================================
        //public PolygonizeGraph Yufan_Add_lines_to_graph(ICollection<IGeometry> geomList )
        // {
        //    IGeometry geom_init = geomList.First() as IGeometry;
        //    PolygonizeGraph _graph = new PolygonizeGraph(geom_init.Factory);

        //    foreach (var geometry in geomList)
        //    {

        //        LineStringAdder _lineStringAdder = new LineStringAdder(_graph);
        //        geometry.Apply(_lineStringAdder);

        //    }
        //   // System.Windows.Forms.MessageBox.Show("geom num " + _graph.Nodes.Count.ToString() + " " + geomList.Count.ToString());
        //    return _graph;   
        //}
        //public ICollection<IGeometry> Yufan_Polygonize( PolygonizeGraph _graph )
        //{
        //    ICollection<IGeometry> _polyList = new List<IGeometry>();
        //    ICollection<ILineString> _dangles = new List<ILineString>();
        //    ICollection<ILineString> _cutEdges = new List<ILineString>();
        //    IList<IGeometry> _invalidRingLines = new List<IGeometry>();
        //    List<EdgeRing> _holeList = new List<EdgeRing>(); 
        //    List<EdgeRing> _shellList  = new List<EdgeRing>(); 

        //    // if no geometries were supplied it's possible that graph is null
        //    _dangles = _graph.DeleteDangles();
        //    _cutEdges = _graph.DeleteCutEdges();
        //    var edgeRingList = _graph.GetEdgeRings();

        //    IList<EdgeRing> validEdgeRingList = new List<EdgeRing>();
        //    foreach (var er in edgeRingList)
        //    {
        //        if (er.IsValid)
        //            validEdgeRingList.Add(er);
        //        else _invalidRingLines.Add(er.LineString);
        //    }


        //    foreach (var er in validEdgeRingList)
        //    {
        //        er.ComputeHole();
        //        if (er.IsHole)
        //            _holeList.Add(er);
        //        else _shellList.Add(er);
        //    }

        //    foreach (EdgeRing holeEdgeRing in _shellList)
        //    {
        //        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!buggy part with null reference exception
        //        EdgeRing shell = null;
        //        try
        //        {
        //            shell = EdgeRing.FindEdgeRingContaining(holeEdgeRing, _shellList);
        //        }
        //        catch(System.NullReferenceException)
        //        {
        //            continue;
        //        }


        //        if (shell != null)
        //        {
        //            shell.AddHole(holeEdgeRing);
        //        }

        //        /*
        //        if (!holeER.hasShell()) {
        //            System.out.println("DEBUG: Outer hole: " + holeER);
        //        }
        //        */
        //    }
        //    // order the shells to make any subsequent processing deterministic
        //    _shellList.Sort(new EdgeRing.EnvelopeComparator());

        //    var includeAll = true;

        //    foreach (var er in _shellList)
        //    {
        //        if (includeAll || er.IsIncluded)
        //        {
        //            _polyList.Add(er.Polygon);
        //        }
        //    }
        //    return _polyList;
        //}
        ////Yufan test==========================================================================

        private void CreateBlocksAttachParcels(List<Line2D> network)
        {
            //Subdivision networkGraphS = new Subdivision();
            var polygonizer = new Polygonizer();
            IGeometryFactory geometryFactory = new GeometryFactory();
            var graphLines = new List<IGeometry>();
            var rdr = new WKTReader(geometryFactory);

            foreach (Line2D netLine in network)
            {

                if (netLine.Start == netLine.End)
                    continue;

                ILineString lineString = GeometryConverter.ToLineString(netLine);

                graphLines.Add(rdr.Read(lineString.ToString()));
            }

            //var nodedContours = new List<IGeometry>();
            //var geomNoder = new GeometryNoder(new PrecisionModel(0.01d));

            ICollection<IGeometry> polyline_nodes = graphLines as ICollection<IGeometry>;
            PolygonizeGraph graph = GeoAlgorithms2D.Add_lines_to_graph(polyline_nodes);
            ICollection<IGeometry> polys = GeoAlgorithms2D.Polygonize(graph);

            //foreach (ILineString c in geomNoder.Node(graphLines)) //(Contours))
            //{
            //    nodedContours.Add(c);
            //}
            //polygonizer.Add(nodedContours);
            //ICollection<IGeometry> polys = polygonizer.GetPolygons();

            // Console.WriteLine("Polygons formed (" + polys.Count + "):");
            //System.Windows.Forms.MessageBox.Show("Polygons formed (" + polys.Count + "):");
            var buildPolygons = new List<Poly2D>();
            foreach (IPolygon netPoly in polys)
            {
                Coordinate[] coords = netPoly.Coordinates;
                Poly2D foundPoly = GeometryConverter.ToPoly2D(coords);
                buildPolygons.Add(foundPoly);
            }

            //-- shrink the polygons---
            var toBeRemovedPolys = new List<Poly2D>();
            foreach (Poly2D poly in buildPolygons)
            {
                if (poly.Offset(-1) == false)
                    toBeRemovedPolys.Add(poly);
                if (poly.Area < ControlParameters.MinBlockSize)
                    toBeRemovedPolys.Add(poly);
            }
            foreach (Poly2D poly in toBeRemovedPolys)
            {
                buildPolygons.Remove(poly);
            }
        }

        private List<Poly2D> CreateBlocks(IEnumerable<Line2D> network)

        {
            //Subdivision networkGraphS = new Subdivision();
            var polygonizer = new Polygonizer();
            IGeometryFactory geometryFactory = new GeometryFactory();
            var graphLines = new List<IGeometry>();
            var rdr = new WKTReader(geometryFactory);

            foreach (Line2D netLine in network)
            {
                if (netLine.Start == netLine.End)
                    continue;

                ILineString lineString = GeometryConverter.ToLineString(netLine);

                graphLines.Add(rdr.Read(lineString.ToString()));
            }

            //var nodedContours = new List<IGeometry>();
            //var geomNoder = new GeometryNoder(new PrecisionModel(0.01d));

            ICollection<IGeometry> polyline_nodes = graphLines as ICollection<IGeometry>;
            PolygonizeGraph graph = GeoAlgorithms2D.Add_lines_to_graph(polyline_nodes);
            ICollection<IGeometry> polys = GeoAlgorithms2D.Polygonize(graph);

            //foreach (ILineString c in geomNoder.Node(graphLines)) //(Contours))
            //{
            //    nodedContours.Add(c);
            //}
            //polygonizer.Add(nodedContours);
            //ICollection<IGeometry> polys = polygonizer.GetPolygons();

            // Console.WriteLine("Polygons formed (" + polys.Count + "):");
            //System.Windows.Forms.MessageBox.Show("Polygons formed (" + polys.Count + "):");
            var buildPolygons = new List<Poly2D>();
            foreach (IPolygon netPoly in polys)
            {
                Coordinate[] coords = netPoly.Coordinates;
                Poly2D foundPoly = GeometryConverter.ToPoly2D(coords);
                buildPolygons.Add(foundPoly);
            }
            
            //-- shrink the polygons---
            var toBeRemovedPolys = new List<Poly2D>();
            foreach (Poly2D poly in buildPolygons)
            {
                if (poly.Offset(-1) == false)
                  toBeRemovedPolys.Add(poly);
                if (poly.Area < ControlParameters.MinBlockSize)
                    toBeRemovedPolys.Add(poly);
            }
            foreach (Poly2D poly in toBeRemovedPolys)
            {
                buildPolygons.Remove(poly);
            }

            return buildPolygons;


            //if (!_offsetBorder.Offset(-1))
            //{
            //   Console.WriteLine("the boundary is not offset correctly"); 
            //}


            //- get the areas of the blocks---    
        }
            //Vector4[] blockColors;
            //List<float> blockAreas = new List<float>();
            //for (int i = 0; i < buildPolygons.Count; i++)
            //{
            //    blockAreas.Add((float)buildPolygons[i].Area);
            //}
            //if (blockAreas.Any())
            //{
            //    blockAreas = Tools.Normalize(blockAreas, 0, 1).ToList();
            //    blockColors = ConvertColor.CreateFFColor4(blockAreas.ToArray());
            //}

            //- add to list with drawable objects-
        
        //--- need an Subdivision as input... -------
        //private void CreateBlocks(List<Line2D> network, bool old = true) //Subdivision networkGraphS)
        //{
        //    List<LineD> subdLines = new List<LineD>();
        //    foreach (Line2D netLine in network)
        //    {
        //        if (netLine.Start == netLine.End)
        //            continue;
        //        LineD lD = new LineD(netLine.Start.X, netLine.Start.Y, netLine.End.X, netLine.End.Y);
        //        subdLines.Add(lD);
        //    }
        //    Subdivision graph = Subdivision.FromLines(subdLines.ToArray());
            //foreach (var poly in buildPolygons)
            //for (int i = 0; i < buildPolygons.Count; i++)
            //{
            //    Poly2D poly = buildPolygons[i];
            //    var polyToDraw = new GPolygon(poly);
            //    polyToDraw.FillColor = ConvertColor.CreateColor4(Color.LightGray); //blockColors[i];//
            //    polyToDraw.Filled = false;
            //    polyToDraw.DeltaZ = -0.1f;
            //    polyToDraw.BorderWidth = 1;
            //    polyToDraw.BorderColor = ConvertColor.CreateColor4(Color.Black);
            //    glCameraMainWindow.AllObjectsList.Add(polyToDraw, true, false);
            //}

         //=== old working version of CreateBlocks ===       
        //--- need an Subdivision as input... -------
        //private void CreateBlocks(List<Line2D> network, bool old = true) //Subdivision networkGraphS)
        //{
        //    List<LineD> subdLines = new List<LineD>();
        //    foreach (Line2D netLine in network)
        //    {
        //        if (netLine.Start == netLine.End)
        //            continue;
        //        LineD lD = new LineD(netLine.Start.X, netLine.Start.Y, netLine.End.X, netLine.End.Y);
        //        subdLines.Add(lD);
        //    }
        //    Subdivision graph = Subdivision.FromLines(subdLines.ToArray());
        //    PointD[][] polygonsT = graph.ToPolygons();
        //    List<Poly2D> polygons = GeometryConverter.ToListPoly2D(polygonsT);
        //    // ---- shrink the polygons -----
        //    var toBeRemovedPolys = new List<Poly2D>();
        //    foreach (Poly2D poly in polygons)
        //    {
        //        if (poly.Offset(-5) == false)
        //            toBeRemovedPolys.Add(poly);
        //        if (poly.Area < ControlParameters.MinBlockSize)
        //            toBeRemovedPolys.Add(poly);
        //    }
        //    foreach (Poly2D poly in toBeRemovedPolys)
        //    {
        //        polygons.Remove(poly);
        //    }
        //    _blocks = polygons;
        //}

        //=== Path computations ======================================================================================================================================================
        public ICommand ShowPath => new RelayCommand(ShowPathExecute);
        private void ShowPathExecute()
        {
            ShowPathNr = 1;
            ShowFinalPathNr = 1;
            RaisePropertyChanged("ShowPathNr");
            RaisePropertyChanged("ShowFinalPathNr");
            if (_rankedPaths != null)
            {
                DrawNetwork(_currentVisualizedGraph);
                DrawPath(ShowPathNr - 1, Color.Red, 2);
            }
        }

        
        public ICommand ShowFinalPath => new RelayCommand(ShowFinalPathExecute);
        private void ShowFinalPathExecute()
        {
            if (_finalRedundantPaths == null)
            {
                return;
            }
            ShortestPathForSorting curPath = _finalRedundantPaths[ShowFinalPathNr - 1];
            DrawNetwork(_currentVisualizedGraph);
            DrawPath(curPath.path, Color.LightGreen, 10);
        }

        
        public ICommand LoadTestGraph => new RelayCommand(LoadTestGraphExecute);

        private void LoadTestGraphExecute()
        {
            var curGraph = ImportGraph();
            if (curGraph == null)
            {
                return;
            }
            //create inverse graph
            Dictionary<UndirectedEdge<Vector2d>, double> realDist = new Dictionary<UndirectedEdge<Vector2d>, double>();
            Dictionary<UndirectedEdge<Vector2d>, double> realAngle = new Dictionary<UndirectedEdge<Vector2d>, double>();
            _inverseEdgesMapping = new Dictionary<Vector2d, UndirectedEdge<Vector2d>>();
            UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> inverseGraph =
                GraphTools.ConstructInverseGraph(curGraph, ref realDist, ref realAngle, ref _inverseEdgesMapping);

            Dictionary<UndirectedEdge<Vector2d>, double> realSteps = new Dictionary<UndirectedEdge<Vector2d>, double>();
            foreach (KeyValuePair<UndirectedEdge<Vector2d>, double> pair in realDist)
            {
                realSteps.Add(pair.Key, 1);
            }

            //dictionary for edge costs
            //Dictionary<UndirectedEdge<Vector2d>, double> edgeCosts = realDist;//realAngle;//new Dictionary<UndirectedEdge<Vector2d>, double>(importedLines.Count * 2);
            _biDirectEdgeCostsDist = new Dictionary<UndirectedEdge<Vector2d>, double>(_currentVisualizedGraph.Count * 2);
            _biDirectEdgeCostsAngle = new Dictionary<UndirectedEdge<Vector2d>, double>(_currentVisualizedGraph.Count * 2);

            //create bi-directional graph
            _inverseQLines = new List<UndirectedEdge<Vector2d>>();
            List<UndirectedEdge<Vector2d>> existingEdges = inverseGraph.Edges.ToList();
            foreach (UndirectedEdge<Vector2d> curEdge in existingEdges)
            {
                double curCost = realDist[curEdge];

                UndirectedEdge<Vector2d> edgeDirA = curEdge;
                _inverseQLines.Add(curEdge);
                _biDirectEdgeCostsDist.Add(edgeDirA, curCost);
                _biDirectEdgeCostsAngle.Add(edgeDirA, realAngle[curEdge]);

                UndirectedEdge<Vector2d> edgeDirB =
                    new UndirectedEdge<Vector2d>(new Vector2d(curEdge.Target.X, curEdge.Target.Y),
                        new Vector2d(curEdge.Source.X, curEdge.Source.Y));
                _inverseQLines.Add(edgeDirB);
                _biDirectEdgeCostsDist.Add(edgeDirB, curCost);
                _biDirectEdgeCostsAngle.Add(edgeDirB, realAngle[curEdge]);
            }

            //build the graph
            var testGraph = _inverseQLines.ToBidirectionalGraph<Vector2d, UndirectedEdge<Vector2d>>(false);
            Func<UndirectedEdge<Vector2d>, double> getWeight;
            if (MetricPathsFirst)
                getWeight = edge => _biDirectEdgeCostsDist[edge];
            else
                getWeight = edge => _biDirectEdgeCostsAngle[edge];

            Func<UndirectedEdge<Vector2d>, double> getInvCost;
            if (MetricPathsFirst)
                getInvCost = edge => _biDirectEdgeCostsAngle[edge];
            else
                getInvCost = edge => _biDirectEdgeCostsDist[edge];

            //define the algorithms
            _hoffmanAlgorithm =
                new HoffmanPavleyRankedShortestPathAlgorithmMaxDist<Vector2d, UndirectedEdge<Vector2d>>(testGraph,
                    getWeight); // E => 2.0);
            _dijkstra = new DijkstraShortestPathAlgorithm<Vector2d, UndirectedEdge<Vector2d>>(testGraph, getInvCost);

            ComputePathsExecute();
        }

        
        private UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> ImportGraph()
        {
            //read the graph from a dxf file
            Microsoft.Win32.OpenFileDialog openDialog = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension
            openDialog.DefaultExt = "Drawing Interchange Format (*.dxf)|*.dxf";

            // Display OpenFileDialog by calling ShowDialog method
            var result = openDialog.ShowDialog();
            if (!result.HasValue || !result.Value) { return null; }

            // Open document 
            DxfDocument dxf = DxfConversions.FromDxfFile(openDialog.FileName);
            if(dxf == null)
            {
                MessageBox.Show("Could not import the file " + openDialog.FileName, "Error - Graph Import", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
            List<Line> lines = dxf.Lines.ToList();
            var importedLines = new List<Line2D>();
            var importedQLines = new List<UndirectedEdge<Vector2d>>();

            //create undirected graph
            foreach (Line dLine in lines)
            {
                importedLines.Add(new Line2D(dLine.StartPoint.X, dLine.StartPoint.Y, dLine.EndPoint.X, dLine.EndPoint.Y));

                UndirectedEdge<Vector2d> edgeDirA = new UndirectedEdge<Vector2d>(new Vector2d(dLine.StartPoint.X, dLine.StartPoint.Y),
                                                                                 new Vector2d(dLine.EndPoint.X, dLine.EndPoint.Y));
                importedQLines.Add(edgeDirA);
            }
            UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> curGraph = importedQLines.ToUndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>();
            _currentVisualizedGraph = importedLines;
            return curGraph;
        }
        
        private void ExportGraph(StreetNetwork graph)
        {
            var lines = graph.Edges.Select(edge => new Tektosyne.Geometry.LineD(edge.Source.X, edge.Source.Y, edge.Target.X, edge.Target.Y)).ToList();
            var document = DxfConversions.ToDxfFile(lines);

            //Show save file dialog
            var saveDialog = new Microsoft.Win32.SaveFileDialog();
            saveDialog.Filter = "Drawing Interchange Format (*.dxf)|*.dxf";
            var result = saveDialog.ShowDialog();

            //Abort process if it was canceled
            if(!result.HasValue || !result.Value) { return; }

            document.Save(saveDialog.FileName);
        }


        public ICommand ComputePaths => new RelayCommand(ComputePathsExecute);
        private void ComputePathsExecute()
        {
            if (_inverseQLines == null)
            {
                return;
            }
            NrNodesInGraph = _inverseQLines.Count;
            RaisePropertyChanged("NrNodesInGraph");
            Vector2d startPt = _inverseQLines[0].Source;
            Vector2d endPt = _inverseQLines[_inverseQLines.Count - (PathTargetPt - 1)].Target; //12 //importedLines[importedLines.Count-11].End;

            //compute the graph paths

            _hoffmanAlgorithm.MaximumPathLengthFactor = RedundancyFactor;
            _hoffmanAlgorithm.ShortestPathCount = MaxNrPathes;
            _hoffmanAlgorithm.SetRootVertex(startPt);
            _hoffmanAlgorithm.Compute(startPt, endPt);

            _rankedPaths = _hoffmanAlgorithm.ComputedShortestPaths;
            NrPathes = _hoffmanAlgorithm.ComputedShortestPathCount;
            RaisePropertyChanged("NrPathes");
            ComputedPathes = "computed pathes: " + NrPathes.ToString();
            RaisePropertyChanged("ComputedPathes");

            //draw the graph
            DrawNetwork(_currentVisualizedGraph);
            DrawPath(0, Color.Red, 2);

            //global shortest path to test angular weights
            // We use Dijkstra on this graph
            //Dictionary<UndirectedEdge<Vector2d>, double> edgeCostsAngular = realAngle;
            //Func<UndirectedEdge<Vector2d>, double> getInvCost = edge => edgeCostsAngular[edge];

            //UndirectedDijkstraShortestPathAlgorithm<Vector2d, UndirectedEdge<Vector2d>> dijkstraUD = new UndirectedDijkstraShortestPathAlgorithm<Vector2d, UndirectedEdge<Vector2d>>(inverseGraph, getInvCost);

            // attach a distance observer to give us the shortest path distances
            //UndirectedVertexDistanceRecorderObserver<Vector2d, UndirectedEdge<Vector2d>> distObserver = new UndirectedVertexDistanceRecorderObserver<Vector2d, UndirectedEdge<Vector2d>>(getInvCost);
            //distObserver.Attach(dijkstraUD);
            //VertexDistanceRecorderObserver<Vector2d, UndirectedEdge<Vector2d>> distObserver = new VertexDistanceRecorderObserver<Vector2d, UndirectedEdge<Vector2d>>(getInvCost);
            //distObserver.Attach(_dijkstra);

            // Attach a Vertex Predecessor Recorder Observer to give us the paths
            //UndirectedVertexPredecessorRecorderObserver<Vector2d, UndirectedEdge<Vector2d>> predecessorObserver = new UndirectedVertexPredecessorRecorderObserver<Vector2d, UndirectedEdge<Vector2d>>();
            //predecessorObserver.Attach(dijkstraUD);
            var predecessorObserver = new VertexPredecessorRecorderObserver<Vector2d, UndirectedEdge<Vector2d>>();
            predecessorObserver.Attach(_dijkstra);

            // Run the algorithm with A set to be the source
            //dijkstraUD.Compute(startPt);
            _dijkstra.Compute(startPt);

            IEnumerable<UndirectedEdge<Vector2d>> sPath;
            predecessorObserver.TryGetPath(endPt, out sPath); //predecessorObserver.VertexPredecessors[endPt];
            DrawPath(sPath, Color.Blue, 4);

            //find angular shortest path
            double minAngular = double.MaxValue;
            int minAngularPathID = -1;
            _finalRedundantPaths = new List<ShortestPathForSorting>();
            for (int i = 0; i < _rankedPaths.Count(); i++)
            {
                IEnumerable<UndirectedEdge<Vector2d>> curPath = _rankedPaths.ToList()[i];
                ShortestPathForSorting shortPath;
                shortPath.path = curPath;

                double pathLength = 0;
                foreach (UndirectedEdge<Vector2d> segment in curPath)
                {
                    if (MetricPathsFirst)
                        pathLength += _biDirectEdgeCostsAngle[segment]; // _biDirectEdgeCostsDist[segment]; //
                    else
                        pathLength += _biDirectEdgeCostsDist[segment]; //
                }
                shortPath.length = pathLength;
                if (pathLength < minAngular)
                {
                    minAngular = pathLength;
                    minAngularPathID = i;
                }
                _finalRedundantPaths.Add(shortPath);
            }

            _finalRedundantPaths = _finalRedundantPaths.OrderByDescending(x => 1 / x.length).ToList();
            NrFinalPathes = _finalRedundantPaths.Count;
            RaisePropertyChanged("NrFinalPathes");

            DrawPath(minAngularPathID, Color.LightGreen, 10);

            viewModel.MainCamera.ZoomAll();
        }

        
        private void DrawPath(int pathID, Color col, int lineWidth = 5)
        {
            IEnumerable<UndirectedEdge<Vector2d>> curPath = _rankedPaths.ToList()[pathID];
            DrawPath(curPath, col, lineWidth);
        }

        //draw the elements of the graph
        private void DrawPath(IEnumerable<UndirectedEdge<Vector2d>> path, Color col, int lineWidth = 5)
        {
            double pathMetricLength = 0;
            double pathAngularLength = 0;

            //map the original edges to the inverse graph points
            List<UndirectedEdge<Vector2d>> origEdges = new List<UndirectedEdge<Vector2d>>();
            origEdges.Add(_inverseEdgesMapping[path.ToList()[0].Source]);
            foreach (UndirectedEdge<Vector2d> segment in path)
            {
                //origEdges.Add(_inverseEdgesMapping[segment.Target]);
                UndirectedEdge<Vector2d> edge;
                _inverseEdgesMapping.TryGetValue(segment.Target, out edge);
                if (edge != null)
                    origEdges.Add(edge);
                //collect the lengths of the path-
                pathMetricLength += _biDirectEdgeCostsDist[segment];
                pathAngularLength += _biDirectEdgeCostsAngle[segment];
            }

            //drwas the original edges of the path
            foreach (UndirectedEdge<Vector2d> segment in origEdges)
            {
                var curLine = new GLine(segment.Source, segment.Target);
                curLine.Color = ConvertColor.CreateColor4(col);
                curLine.Width = lineWidth;
                viewModel.MainCamera.AllObjectsList.Add(curLine);
            }

            CurMetricPathLength = "metric path length: " + Math.Round(pathMetricLength).ToString();
            RaisePropertyChanged("CurMetricPathLength");
            CurAngularPathLength = "angular path length: " + Math.Round(pathAngularLength, 2).ToString();
            RaisePropertyChanged("CurAngularPathLength");

            viewModel.MainCamera.ZoomAll();
        }

        
        private void DrawNetwork(List<Line2D> graphAsLines)
        {
            viewModel.MainCamera.AllObjectsList.ObjectsToDraw.Clear();
            viewModel.MainCamera.AllObjectsList.ObjectsToInteract.Clear();

            foreach (Line2D line in graphAsLines)
            {
                var curLine = new GLine(line);
                curLine.Color = new Vector4(0.09f, 0.09f, 0.09f, 1f);
                viewModel.MainCamera.AllObjectsList.Add(curLine);

                var curPtA = new GCircle(line.Start, 0.1f, 6);
                curPtA.FillColor = ConvertColor.CreateColor4(Color.Black);
                var curPtB = new GCircle(line.End, 0.1f, 6);
                curPtB.FillColor = ConvertColor.CreateColor4(Color.Black);
                viewModel.MainCamera.AllObjectsList.Add(curPtA);
                viewModel.MainCamera.AllObjectsList.Add(curPtB);
            }

            viewModel.MainCamera.ZoomAll();
        }


        private void DrawNetwork(IEnumerable<UndirectedEdge<Vector2d>> graphAsLines, bool doReset = true, Vector4? lineColor = null)
        {
            if (doReset)
            {
                viewModel.MainCamera.AllObjectsList.ObjectsToDraw.Clear();
                viewModel.MainCamera.AllObjectsList.ObjectsToInteract.Clear();
            }
            foreach (UndirectedEdge<Vector2d> line in graphAsLines)
            {
                var curLine = new GLine(line.Source, line.Target);
                curLine.Color = lineColor.HasValue ? lineColor.Value : new Vector4(0.09f, 0.09f, 0.09f, 1f);
                viewModel.MainCamera.AllObjectsList.Add(curLine);

                var curPtA = new GCircle(line.Source, 0.1f, 6);
                curPtA.FillColor = lineColor.HasValue ? lineColor.Value : ConvertColor.CreateColor4(Color.Black);
                var curPtB = new GCircle(line.Target, 0.1f, 6);
                curPtB.FillColor = lineColor.HasValue ? lineColor.Value : ConvertColor.CreateColor4(Color.Black);
                viewModel.MainCamera.AllObjectsList.Add(curPtA);
                viewModel.MainCamera.AllObjectsList.Add(curPtB);
            }

            viewModel.MainCamera.ZoomAll();
        }

        private void DrawCluster(IEnumerable<UndirectedEdge<Vector2d>> edges, IEnumerable<IEnumerable<Vector2d>> clusters, Colors.ColorContainer colors = null, bool doReset = true)
        {
            if (clusters == null) { return; }

            if(doReset)
            {
                //Clear drawing canvas
                viewModel.MainCamera.AllObjectsList.ObjectsToDraw.Clear();
                viewModel.MainCamera.AllObjectsList.ObjectsToInteract.Clear();
            }

            //Assign a color to each cluster
            if(colors == null) { colors = Colors.Get(clusters.Count()); }
            var vectorCluster = clusters
                .SelectMany((c, index) => c.Select(node => new { Node = node, Cluster = index }))
                .ToDictionary(t => t.Node, t => t.Cluster);

            //Draw all edges and coler them per cluster
            var defaultColor = new Vector4(0.9f, 0.9f, 0.9f, 1f);
            foreach (UndirectedEdge<Vector2d> edge in edges)
            {
                var line = new GLine(edge.Source, edge.Target);
                line.Color = vectorCluster.ContainsKey(edge.Source) && vectorCluster.ContainsKey(edge.Target) &&
                    vectorCluster[edge.Source] == vectorCluster[edge.Target] ? colors[vectorCluster[edge.Source]].ToVector4() : defaultColor;
                viewModel.MainCamera.AllObjectsList.Add(line);
            }
            viewModel.MainCamera.ZoomAll();
        }

        
        private void DrawNetworkAnalysis(Vector4[] ffColors, Line2D[] origEdges, Vector2d[] graphPts, Vector2d origin, GLCameraControl viewControl)
        {
            viewModel.MainCamera.AllObjectsList.ObjectsToDraw.Clear();
            viewModel.MainCamera.AllObjectsList.ObjectsToInteract.Clear();

            //draw the elements of the graph-
            Line2D curEdge;
            for (int i = 0; i < ffColors.Length; i++)
            {
                curEdge = origEdges[i];
                curEdge = new Line2D(curEdge.Start.X + origin.X, curEdge.Start.Y + origin.Y, curEdge.End.X + origin.X,
                    curEdge.End.Y + origin.Y);
                var curLine = new GLine(curEdge);
                curLine.Width = ControlParameters.LineWidth;

                curLine.Color = ffColors[i];
                viewControl.AllObjectsList.Add(curLine, true, false);
            }
            foreach (Vector2d point in graphPts)
            {
                viewControl.AllObjectsList.Add(
                    new GCircle(new Vector2d(point.X + origin.X, point.Y + origin.Y), .6, 10), true, true);
            }
            viewModel.MainCamera.ZoomAll();
        }

        /// <summary>
        /// Command, which imports a graph, runs the k-means algorithm
        /// on the imported graph and displays the result graphically.
        /// </summary>
        public ICommand RunKMeans => new RelayCommand(RunKMeansExecute);
        private async void RunKMeansExecute()
        {
            graph = graph ?? ImportGraph();
            if(graph == null) { return; }

            //Show options dialog
            var result = new KMeansClusteringOptions().ShowDialog();
            if (!result.HasValue || !result.Value) { return; }
            var viewModel = ViewModelLocator.KMeansOptionsViewModel;

            //Run k-means
            var progressBar = new Progress<int>(value => uiFactory.StartNew(() => {
                ProgressValue = value;
            }));
            var progressVisualization = !viewModel.ShowVisualisation ? null : new Progress<IEnumerable<Cluster>>(clusters => uiFactory.StartNew(() => {
                uiFactory.StartNew(() => DrawCluster(graph.Edges, clusters, Colors.Get(clusters.Count())));
            }));
            clusters = await Task.Run(() => new KMeansCluster().FindClusters(graph, 
                new KMeansCluster.Parameters(tries: viewModel.NumberOfTries, iterations: viewModel.NumberOfIterations, 
                    clustersMin: viewModel.MinClusters, clustersMax: viewModel.MaxClusters, progressBar: progressBar, 
                    progressVisualization: progressVisualization, shortestPath: viewModel.ShortestPath)));

            //Visualise result
            await uiFactory.StartNew(() => DrawCluster(graph.Edges, clusters, Colors.Get(clusters.Count())));
        }

        /// <summary>
        /// Command, which imports a graph, runs a hierarchical clustering algorithm
        /// on the imported graph and displays the result graphically.
        /// </summary>
        public ICommand RunHierarchicalClustering => new RelayCommand(RunHierarchicalClusteringExecute);
        private async void RunHierarchicalClusteringExecute()
        {
            graph = graph ?? ImportGraph();
            if (graph == null) { return; }

            //Show options dialog
            var result = new HierarchicalClusteringOptions().ShowDialog();
            if(!result.HasValue || !result.Value) { return; }
            var viewModel = ViewModelLocator.HCOptionsViewModel;

            //Run Clustering
            var progress = new Progress<int>(value => uiFactory.StartNew(() => ProgressValue = value));
            var output = await Task.Run(() => new HierarchicalCluster().FindClusters(graph, viewModel.Strategy, progress));

            //Show Slider Dialog (if this option was chosen)
            Colors.ColorContainer colors = null;
            if (viewModel.ShowSlider)
            {
                var sliderViewModel = ViewModelLocator.HCSliderViewModel;
                sliderViewModel.NumberOfClusters = viewModel.NumberOfClusters;
                sliderViewModel.ModifyOutput = viewModel.ModifyOutput;
                colors = Colors.Get(Colors.KellysMaxContrastSet.Count);

                Action drawCluster = () => uiFactory.StartNew(() => DrawCluster(graph.Edges, output.Clusters(sliderViewModel.NumberOfClusters, sliderViewModel.ModifyOutput), colors));
                drawCluster();
                sliderViewModel.ShowPreview += drawCluster;

                var sliderResult = new HierarchicalClusteringSlider().ShowDialog();
                sliderViewModel.ShowPreview -= drawCluster;
                if (sliderResult.HasValue && sliderResult.Value) { clusters = output.Clusters(sliderViewModel.NumberOfClusters, sliderViewModel.ModifyOutput); }
            }
            else { clusters = output.Clusters(viewModel.NumberOfClusters, viewModel.ModifyOutput); }

            //Visualise result
            await uiFactory.StartNew(() => DrawCluster(graph.Edges, clusters, colors));
        }
        int selectedClusternr = -1;
        /// <summary>
        /// Command, which imports a graph, runs the k-means algorithm
        /// on the imported graph and displays the result graphically.
        /// </summary>
        public ICommand RunTreesFromClusters => new RelayCommand(RunTreesFromClustersExecute);
        private async void RunTreesFromClustersExecute()
        {
            if(clusters == null || clusters.Count() <= 0)
            {
                MessageBox.Show("Found no cluster to create tree from. Execute a clustering algorithm first", "Error - Tree from Cluster", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (selectedClusternr == -1)
            {
                int numberToSelect = (int)(new Random().NextDouble() * clusters.Count());
                var selectedCluster = clusters.ElementAt(numberToSelect);
                var tries = 10;
                while ((selectedCluster == null || selectedCluster.Count() == 0) && tries >= 0)
                {
                    numberToSelect = (int)(new Random().NextDouble() * clusters.Count());
                    selectedCluster = clusters.ElementAt(numberToSelect);
                    tries--;
                }
                if ((selectedCluster == null || selectedCluster.Count() == 0) && tries <= 0)
                {
                    return;
                }
                selectedClusternr = numberToSelect;

                await uiFactory.StartNew(() => DrawNetwork(graph.Edges.Where(e => selectedCluster.Contains(e.Source) && selectedCluster.Contains(e.Target))));
            }
            else
            {
                var selectedCluster2 = clusters.ElementAt(selectedClusternr);
                var tree = await Task.Run(() => new TreeGenerator().TreeFromCluster(graph, selectedCluster2));

                // convert the network to graph network
                var treeGraph = await Task.Run(() => new StreetPatternAbsoluteAngle().GrowStreetPattern(tree));

                await uiFactory.StartNew(() => DrawNetwork(treeGraph.Edges));
            }
        }

        /// <summary>
        /// Command, which creates an esport of the cluster analysis.
        /// </summary>
        public ICommand RunExportJSON => new RelayCommand(RunExportJSONExecute);
        private async void RunExportJSONExecute()
        {
            if (clusters == null || graph == null)
            {
                MessageBox.Show("Found no cluster to export. Execute a clustering algorithm first", "Error - Cluster Json Export", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Json Files (*.json)|*.json";
            saveFileDialog.Title = "Save Cluster Analysis to JSON";

            if (!saveFileDialog.ShowDialog().GetValueOrDefault()) { return; }
            await Task.Run(() => new ClusterAnalysisExporter(graph, clusters).Export(saveFileDialog.FileName, exportFields: ClusterAnalysisExporter.FieldsAll));
        }

        /// <summary>
        /// Command to show the cluster analysis view.
        /// </summary>
        public ICommand RunClusterAnalysis => new RelayCommand(RunClusterAnalysisExecute);
        private void RunClusterAnalysisExecute()
        {
            if(clusters == null || clusters.Count() == 0)
            {
                MessageBox.Show("Found no cluster to export. Execute a clustering algorithm first", "Error - Cluster Analysis", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            var clusterAnalysisSlider = new ClusterAnalysis();
            var viewModel = ViewModelLocator.ClusterAnalysisViewModel;

            viewModel.ClusterCount = clusters.Count() - 1;
            viewModel.ClusterNumber0 = 0;

            var clusterAnalysis = new ClusterAnalysisExporter(graph, clusters).GetAnalysisData(ClusterAnalysisExporter.FieldsWithoutCentrality);

            Action drawCluster = () => uiFactory.StartNew(() => {
                var colors = clusters.Select(c => Color.FromArgb(0, 0, 0)).ToList();
                colors[viewModel.ClusterNumber1] = Color.FromArgb(0, 0, 255);
                colors[viewModel.ClusterNumber0] = Color.FromArgb(255, 0, 0);
                var colorContainer = new Colors.ColorContainer(colors, colors.Count());
                DrawCluster(graph.Edges, clusters, colors: colorContainer);

                viewModel.AnalysisParameter0 = clusterAnalysis[viewModel.ClusterNumber0]
                    .Select(e => new KeyValuePair<string, double>(e.Key.ToString(), e.Value)).ToList();
                viewModel.AnalysisParameter1 = clusterAnalysis[viewModel.ClusterNumber1]
                    .Select(e => new KeyValuePair<string, double>(e.Key.ToString(), e.Value)).ToList();
            });
            drawCluster();
            viewModel.ShowPreview += drawCluster;

            var result = clusterAnalysisSlider.ShowDialog();
        }
        public ICommand RunBlockTest => new RelayCommand(RunBlockTestExecute);
        private void RunBlockTestExecute()
        {
            var skipEdges = new BlockAnalysis().GetIgnoreEdges(graph);
            
            var filteredClusterEdges = graph.Edges.Where(e => !skipEdges.Contains(e));
            var lines = filteredClusterEdges.Select(e => new Line2D(e.Source, e.Target));
            var poly = CreateBlocks(lines);
            DrawBlocks(poly);
        }

        public ICommand RunFeatureClustering => new RelayCommand(RunFeatureClusteringExecute);
        private async void RunFeatureClusteringExecute()
        {
            graph = graph ?? ImportGraph();
            if (graph == null) { return; }

            //Run Clustering
            var progress = new Progress<int>(value => uiFactory.StartNew(() => ProgressValue = value));
            clusters = await Task.Run(() => new FeatureClustering().FindClusters(graph));

            //Visualise result
            await uiFactory.StartNew(() => DrawCluster(graph.Edges, clusters));
        }

        private void DrawBlocks(IEnumerable<Poly2D> blocks)
        {
            var vm = (new ViewModelLocator()).Viewer1ViewModel;
            
            if (blocks == null) return;
            //get the areas of the blocks  
            Vector4[] blockColors;
            List<float> blockAreas = new List<float>();
            foreach (var block in blocks)
            {
                blockAreas.Add((float)block.Area);
            }

            if (blockAreas.Any())
            {
                blockAreas = Tools.Normalize(blockAreas, 0, 1).ToList();
                blockColors = ConvertColor.CreateFFColor4(blockAreas.ToArray());
            }

            //add to list with drawable objects
            foreach (var block in blocks)
            {
                var polyToDraw = new GPolygon(block);
                polyToDraw.FillColor = ConvertColor.CreateColor4(Color.LightGray); //blockColors[i];//
                polyToDraw.Filled = true;
                polyToDraw.DeltaZ = -0.1f;
                polyToDraw.BorderWidth = 1;
                polyToDraw.BorderColor = ConvertColor.CreateColor4(Color.Black);
                vm.MainCamera.AllObjectsList.Add(polyToDraw, true, false);
            }

            vm.MainCamera.ZoomAll();
        }

    }

}
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using Synthesis_WPF_MVVM.Model;

namespace Synthesis_WPF_MVVM.ViewModel
{
    //============================================================================================
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if (ViewModelBase.IsInDesignModeStatic)
            {
                SimpleIoc.Default.Register<IDataService, Design.DesignDataService>();
            }
            else
            {
                SimpleIoc.Default.Register<IDataService, DataService>();
            }

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<Viewer1ViewModel>();
            SimpleIoc.Default.Register<Viewer2ViewModel>();
            SimpleIoc.Default.Register<NetworkViewModel>();
            SimpleIoc.Default.Register<BuildingLayoutViewModel>();
            SimpleIoc.Default.Register<NodeXLViewModel>();
            SimpleIoc.Default.Register<Dialogs.HCOptionsViewModel>();
            SimpleIoc.Default.Register<Dialogs.HCSliderViewModel>();
            SimpleIoc.Default.Register<Dialogs.ClusterAnalysisViewModel>();
            SimpleIoc.Default.Register<Dialogs.KMeansOptionsViewModel>();
        }

        //============================================================================================
        /// <summary>
        /// Gets the Main property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        //============================================================================================
        /// <summary>
        /// Gets the Viewer1ViewModel property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public Viewer1ViewModel Viewer1ViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<Viewer1ViewModel>();
            }
        }

        //============================================================================================
        /// <summary>
        /// Gets the Viewer2ViewModel property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public Viewer2ViewModel Viewer2ViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<Viewer2ViewModel>();
            }
        }


        //============================================================================================
        /// <summary>
        /// Gets the NetworkViewModel property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public NetworkViewModel NetworkViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<NetworkViewModel>();
            }
        }

        //============================================================================================
        /// <summary>
        /// Gets the BuildingLayoutViewModel property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public BuildingLayoutViewModel BuildingLayoutViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<BuildingLayoutViewModel>();
            }
        }

        //============================================================================================
        /// <summary>
        /// Gets the NodeXLViewModel property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public NodeXLViewModel NodeXLViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<NodeXLViewModel>();
            }
        }

        public static Dialogs.HCOptionsViewModel HCOptionsViewModel => ServiceLocator.Current.GetInstance<Dialogs.HCOptionsViewModel>();

        public static Dialogs.HCSliderViewModel HCSliderViewModel => ServiceLocator.Current.GetInstance<Dialogs.HCSliderViewModel>();

        public static Dialogs.ClusterAnalysisViewModel ClusterAnalysisViewModel => ServiceLocator.Current.GetInstance<Dialogs.ClusterAnalysisViewModel>();

        public static Dialogs.KMeansOptionsViewModel KMeansOptionsViewModel => ServiceLocator.Current.GetInstance<Dialogs.KMeansOptionsViewModel>();


        //============================================================================================
        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public static void Cleanup()
        {
        }
    }
}
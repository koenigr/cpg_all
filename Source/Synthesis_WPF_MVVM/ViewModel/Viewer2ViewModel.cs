﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Windows;
using System.Windows.Input;
using CPlan.VisualObjects;
using System.Windows.Shapes;
using System.Windows.Forms;
using CPlan.Geometry;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Windows.Forms.Integration;
using System.Collections.ObjectModel;

namespace Synthesis_WPF_MVVM.ViewModel
{
    public class Viewer2ViewModel : ViewModelBase
    {

        //============================================================================================================================================================================
        /// <summary>
        /// Stores all GLCameraControls of this Viewer2ViewModel 
        /// </summary>
        public List<CPlan.VisualObjects.GLCameraControl> ViewControls { get; set; }

        //============================================================================================================================================================================
        /// <summary>
        /// Return the main camera usually used for interaction.
        /// </summary>
        public GLCameraControl MainCamera 
        {
            get { return ViewControls.Find(item => item.Name == "mainLayoutHost"); }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Retrun the search camera used for showing the archive of pareto-optimal solutions.
        /// </summary>
        public GLCameraControl SearchCamera
        {
            get { return ViewControls.Find(item => item.Name == "searchLayoutHost"); }
        }
            
        //============================================================================================================================================================================
        /// <summary>
        /// Holds the active GLCameraControl.
        /// </summary>
        public CPlan.VisualObjects.GLCameraControl ActiveCameraControl { get; protected set; }

        //============================================================================================================================================================================
        /// <summary>
        /// Defines the mode to interact with graphical objects.
        /// </summary>
        public String MouseMode { get; set; }


        //============================================================================================================================================================================
        //============================================================================================================================================================================
        //============================================================================================================================================================================
        /// <summary>
        /// Initializes a new instance of the ViewersViewModel class.
        /// </summary>
        public Viewer2ViewModel()
        {
            ViewControls = new List<CPlan.VisualObjects.GLCameraControl>();
            AssignViewer = new RelayCommand<WindowsFormsHost>(ExecuteAssignViewer);           
        }

        //============================================================================================================================================================================
        public RelayCommand<WindowsFormsHost> AssignViewer { get; private set; }
        //============================================================================================================================================================================
        /// <summary>
        /// Assign a GLCameraControl to a WindowsFormsHost.
        /// </summary>
        /// <param name="window">The WindowsFormsHost.</param>
        private void ExecuteAssignViewer(WindowsFormsHost window)
        {
            if ((window != null) && (window.Child == null)) {
                if (ViewControls.Count < 2)
                {
                    ViewControls.Add(new GLCameraControl());
                    int item = ViewControls.Count - 1;
                    ViewControls[item].Name = window.Name;
                    ViewControls[item].BackColor = System.Drawing.Color.Black;
                    ViewControls[item].BackgroundColor = System.Drawing.Color.White;
                    ViewControls[item].RasterColor = System.Drawing.Color.LightGray;
                    ViewControls[item].ShowRaster = true;
                    ViewControls[item].Dock = DockStyle.Fill;
                    ViewControls[item].Resize += GLCameraControl_Resize;
                    ViewControls[item].MouseUp += GlCameraMainWindow_MouseUp;
                    window.Child = ViewControls[item];
                    AddTestGeometry(item);
                }
                else 
                {
                    window.Child = ViewControls.Find(item => item.Name == window.Name); 
                }
            }
        }

        //============================================================================================================================================================================
        private void AddTestGeometry(int item)
        {
            // --- test geometry ---
            GRectangle myRect = new GRectangle(1, 1, 200, 100);
            myRect.Filled = true;
            myRect.FillColor = ConvertColor.CreateColor4(System.Drawing.Color.LightGreen);
            ViewControls[item].AllObjectsList.Add(myRect, true, true);

            GPrism myPrism = new GPrism(myRect.Poly, 70);
            ViewControls[item].AllObjectsList.Add(myPrism, true, true);

            ViewControls[item].ZoomAll();
            ViewControls[item].Invalidate();
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Resize the GLCameraControl.
        /// </summary>
        private void GLCameraControl_Resize(object sender, EventArgs e)
        {
            var curCtrl = (GLCameraControl)sender;
            if (!curCtrl.Loaded) return;
            int w = curCtrl.Width;
            int h = curCtrl.Height;
            curCtrl.MakeCurrent();
            GL.Viewport(0, 0, w, h);
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Set the active GLCameraControl.
        /// </summary>
        private void GlCameraMainWindow_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            ActiveCameraControl = (CPlan.VisualObjects.GLCameraControl)sender;
        }

    }
}

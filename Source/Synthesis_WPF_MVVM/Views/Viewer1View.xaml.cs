﻿using System.Windows.Controls;

namespace Synthesis_WPF_MVVM.Views
{
    /// <summary>
    /// Description for Viewer1View.
    /// </summary>
    public partial class Viewer1View : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the Viewer1View class.
        /// </summary>
        public Viewer1View()
        {
            InitializeComponent();
        }
    }
}
﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CPlan.Optimization;
using OpenTK;
using QuickGraph;
using CPlan.Geometry;
using CPlan.Optimization.Chromosomes;

namespace GraphTests
{
    [TestClass]
    public class AbsolutGraphTest
    {
        [TestMethod]
        public void CreateTestGraph()
        {
            InstructionTreePisaAbs treeFromGraph = new InstructionTreePisaAbs();
            treeFromGraph.InitalEdge = new UndirectedEdge<Vector2d>(new Vector2d(0, 0), new Vector2d(50, 50));

            int idx = 0;
            var seedPos = new Vector2d(0, 0);
            InstructionNodeAbsolute seed1 = new InstructionNodeAbsolute(idx++);
            seed1.Angle = 45.0f;
            seed1.Length = (float)Math.Sqrt(5000);
            seed1.Parent = seed1;

            treeFromGraph.AddNode(seed1);

            var node1Pos = new Vector2d(50, 50);
            InstructionNodeAbsolute node1 = new InstructionNodeAbsolute(idx++);
            node1.Angle = (float)(-GeoAlgorithms2D.AngleBetweenDegree(new Vector2d(100, 50), seedPos, node1Pos));
            node1.Length = 50;
            node1.Parent = seed1;
            treeFromGraph.AddNode(node1);

            var node2Pos = new Vector2d(100, 50);
            InstructionNodeAbsolute node2 = new InstructionNodeAbsolute(idx++);
            //newNode2.Angle = (float)(Math.PI - GeoAlgorithms2D.AngleBetweenDegree(new Vector2d(50, 100), newNode.Position, newNode2.Position));
            //newNode2.Angle = (float)(180.0 - GeoAlgorithms2D.AngleBetweenDegree(new Vector2d(50, 100) , newNode.Position, newNode2.Position));
            node2.Angle = (float)(360 - GeoAlgorithms2D.AngleBetweenDegree(new Vector2d(50, 100), node1Pos, node2Pos));
            node2.Length = (float)Math.Sqrt(5000);
            node2.Parent = node1;
            treeFromGraph.AddNode(node2);

            var node3Pos = new Vector2d(50, 50);
            InstructionNodeAbsolute node3 = new InstructionNodeAbsolute(idx++);
            //Wieso stimmt hier der Winkel nicht? 0 Grad ist nicht mehr 0 Grad (Relativ zu anderer Linie)?
            //newNode3.Angle = (float)(180.0 - GeoAlgorithms2D.AngleBetweenDegree(new Vector2d(50, 200), seed0.Position, newNode3.Position)); //-65.0f; //?
            node3.Angle = (float)(360 - GeoAlgorithms2D.AngleBetweenDegree(new Vector2d(50, 200), seedPos, node3Pos));
            node3.Length = 150;
            node3.Parent = seed1;
            treeFromGraph.AddNode(node3);

            var node4Pos = new Vector2d(50, 200);
            InstructionNodeAbsolute node4 = new InstructionNodeAbsolute(idx++);
            //newNode4.Angle = (float)(180.0 - GeoAlgorithms2D.AngleBetweenDegree(new Vector2d(100, 250), newNode3.Position, newNode4.Position));
            node4.Angle = (float)(360 - GeoAlgorithms2D.AngleBetweenDegree(new Vector2d(100, 250), node3Pos, node4Pos)) % 360;
            node4.Length = (float)Math.Sqrt(5000);
            node4.Parent = node3;
            treeFromGraph.AddNode(node4);


            var streetGeneratorAbsAngle = new StreetPatternAbsoluteAngle();
            var graph = streetGeneratorAbsAngle.GrowStreetPattern(treeFromGraph);
            //DrawNetwork(graph.Edges);
        }
    }
}

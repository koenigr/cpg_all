﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CPlan.Optimization.ClusterAnalysis;
using QuickGraph;
using OpenTK;

namespace GraphTests
{
    using CPlan.Geometry;
    using CPlan.Optimization;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using StreetNetwork = UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>;

    [TestClass]
    public class ExporterTest
    {
        [TestMethod]
        public void TestVariance0()
        {
            var network = new StreetNetwork();
            var edges = new List<Vector2d>() {
                new Vector2d(0, 0),
                new Vector2d(0, 10),
                new Vector2d(10, 10),
                new Vector2d(10, 0)
            };

            network.AddVertexRange(edges);

            network.AddEdge(new UndirectedEdge<Vector2d>(edges[0], edges[1]));
            network.AddEdge(new UndirectedEdge<Vector2d>(edges[1], edges[2]));
            network.AddEdge(new UndirectedEdge<Vector2d>(edges[2], edges[3]));
            network.AddEdge(new UndirectedEdge<Vector2d>(edges[3], edges[0]));

            var cluster = new List<List<Vector2d>>();
            cluster.Add(edges);

            var testResults = new ClusterAnalysisExporter(network, cluster);
            Assert.AreEqual(0.0, testResults[0, ClusterAnalysisExporter.Fields.StreetLengthSigma], 0.001);
            Assert.AreEqual(90.0, testResults[0, ClusterAnalysisExporter.Fields.StreetAngleSigma], 0.001);
        }

        [TestMethod]
        public void TestVarianceDiff()
        {
            var network = new StreetNetwork();
            var edges = new List<Vector2d>() {
                new Vector2d(0, 0),
                new Vector2d(0, 10),
                new Vector2d(10, 10),
                new Vector2d(10, 0),
                new Vector2d(0, 0),
                new Vector2d(0, 5),
                new Vector2d(5, 5),
                new Vector2d(5, 0)
            };

            network.AddVertexRange(edges);

            network.AddEdge(new UndirectedEdge<Vector2d>(edges[0], edges[1]));
            network.AddEdge(new UndirectedEdge<Vector2d>(edges[1], edges[2]));
            network.AddEdge(new UndirectedEdge<Vector2d>(edges[2], edges[3]));
            network.AddEdge(new UndirectedEdge<Vector2d>(edges[3], edges[0]));

            network.AddEdge(new UndirectedEdge<Vector2d>(edges[4], edges[5]));
            network.AddEdge(new UndirectedEdge<Vector2d>(edges[5], edges[6]));
            network.AddEdge(new UndirectedEdge<Vector2d>(edges[6], edges[7]));
            network.AddEdge(new UndirectedEdge<Vector2d>(edges[7], edges[4]));

            var cluster = new List<List<Vector2d>>();
            cluster.Add(edges);

            var testResults = new ClusterAnalysisExporter(network, cluster);
            Assert.AreEqual(2.5, testResults[0, ClusterAnalysisExporter.Fields.StreetLengthSigma], 0.001);
            Assert.AreEqual(106.8, testResults[0, ClusterAnalysisExporter.Fields.StreetAngleSigma], 0.001);
        }

        [TestMethod]
        public void TestBlockCircleError1()
        {
            var error1 = new List<UndirectedEdge<Vector2d>>() {
                new UndirectedEdge<Vector2d>(new Vector2d(65.65627, -85.32712), new Vector2d(64.20733, -80.04406)),
                new UndirectedEdge<Vector2d>(new Vector2d(64.20733, -80.04406), new Vector2d(67.83216, -79.63528)),
                new UndirectedEdge<Vector2d>(new Vector2d(67.83216, -79.63528), new Vector2d(67.91776, -78.86387)),
                new UndirectedEdge<Vector2d>(new Vector2d(67.91776, -78.86387), new Vector2d(68.76224, -78.30084)),
                new UndirectedEdge<Vector2d>(new Vector2d(68.76224, -78.30084), new Vector2d(66.81352, -72.73358)),
                new UndirectedEdge<Vector2d>(new Vector2d(66.81352, -72.73358), new Vector2d(66.20577, -72.63857)),
                new UndirectedEdge<Vector2d>(new Vector2d(66.20577, -72.63857), new Vector2d(64.61599, -73.43406)),
                new UndirectedEdge<Vector2d>(new Vector2d(64.61599, -73.43406), new Vector2d(60.27897, -73.89868)),
                new UndirectedEdge<Vector2d>(new Vector2d(60.27897, -73.89868), new Vector2d(59.14094, -71.29611)),
                new UndirectedEdge<Vector2d>(new Vector2d(59.14094, -71.29611), new Vector2d(58.68791, -70.41703)),
                new UndirectedEdge<Vector2d>(new Vector2d(58.68791, -70.41703), new Vector2d(57.82909, -68.41582)),
                new UndirectedEdge<Vector2d>(new Vector2d(57.82909, -68.41582), new Vector2d(62.56641, -67.45681)),
                new UndirectedEdge<Vector2d>(new Vector2d(62.56641, -67.45681), new Vector2d(63.64469, -67.00813)),
                new UndirectedEdge<Vector2d>(new Vector2d(63.64469, -67.00813), new Vector2d(66.93873, -67.03681)),
                new UndirectedEdge<Vector2d>(new Vector2d(66.93873, -67.03681), new Vector2d(66.16268, -65.74294)),
                new UndirectedEdge<Vector2d>(new Vector2d(66.16268, -65.74294), new Vector2d(64.22719, -62.41648)),
                new UndirectedEdge<Vector2d>(new Vector2d(64.22719, -62.41648), new Vector2d(63.43246, -61.33778)),
                new UndirectedEdge<Vector2d>(new Vector2d(63.43246, -61.33778), new Vector2d(60.76767, -58.39992)),
                new UndirectedEdge<Vector2d>(new Vector2d(60.76767, -58.39992), new Vector2d(50.0227, -61.30099)),
                new UndirectedEdge<Vector2d>(new Vector2d(50.0227, -61.30099), new Vector2d(36.79923, -64.97382)),
                new UndirectedEdge<Vector2d>(new Vector2d(36.79923, -64.97382), new Vector2d(28.37212, -67.24586)),
                new UndirectedEdge<Vector2d>(new Vector2d(28.37212, -67.24586), new Vector2d(26.60675, -67.8226)),
                new UndirectedEdge<Vector2d>(new Vector2d(26.60675, -67.8226), new Vector2d(23.91686, -68.99989)),
                new UndirectedEdge<Vector2d>(new Vector2d(23.91686, -68.99989), new Vector2d(22.61294, -70.48933)),
                new UndirectedEdge<Vector2d>(new Vector2d(22.61294, -70.48933), new Vector2d(22.67884, -72.07047)),
                new UndirectedEdge<Vector2d>(new Vector2d(22.67884, -72.07047), new Vector2d(22.70265, -72.07047)),
                new UndirectedEdge<Vector2d>(new Vector2d(22.70265, -72.07047), new Vector2d(25.11486, -72.6886)),
                new UndirectedEdge<Vector2d>(new Vector2d(25.11486, -72.6886), new Vector2d(33.37236, -73.09792)),
                new UndirectedEdge<Vector2d>(new Vector2d(33.37236, -73.09792), new Vector2d(33.37236, -76.08642)),
                new UndirectedEdge<Vector2d>(new Vector2d(33.37236, -76.08642), new Vector2d(32.10259, -75.87644)),
                new UndirectedEdge<Vector2d>(new Vector2d(32.10259, -75.87644), new Vector2d(31.64909, -79.65305)),
                new UndirectedEdge<Vector2d>(new Vector2d(31.64909, -79.65305), new Vector2d(35.10068, -80.02283)),
                new UndirectedEdge<Vector2d>(new Vector2d(35.10068, -80.02283), new Vector2d(34.98039, -80.86376)),
                new UndirectedEdge<Vector2d>(new Vector2d(34.98039, -80.86376), new Vector2d(35.66288, -80.92566)),
                new UndirectedEdge<Vector2d>(new Vector2d(35.66288, -80.92566), new Vector2d(35.91117, -78.93944)),
                new UndirectedEdge<Vector2d>(new Vector2d(35.91117, -78.93944), new Vector2d(36.50801, -74.61126)),
                new UndirectedEdge<Vector2d>(new Vector2d(36.50801, -74.61126), new Vector2d(38.4, -74.4013)),
                new UndirectedEdge<Vector2d>(new Vector2d(38.4, -74.4013), new Vector2d(49.32614, -73.54225)),
                new UndirectedEdge<Vector2d>(new Vector2d(49.32614, -73.54225), new Vector2d(54.22457, -73.28401)),
                new UndirectedEdge<Vector2d>(new Vector2d(54.22457, -73.28401), new Vector2d(55.84956, -73.98052)),
                new UndirectedEdge<Vector2d>(new Vector2d(55.84956, -73.98052), new Vector2d(56.26599, -79.1149)),
                new UndirectedEdge<Vector2d>(new Vector2d(56.26599, -79.1149), new Vector2d(56.93646, -87.94343)),
                new UndirectedEdge<Vector2d>(new Vector2d(56.93646, -87.94343), new Vector2d(58.90224, -88.11394)),
                new UndirectedEdge<Vector2d>(new Vector2d(58.90224, -88.11394), new Vector2d(65.65627, -88.64186)),
                new UndirectedEdge<Vector2d>(new Vector2d(65.65627, -88.64186), new Vector2d(65.65627, -85.32712)),
            };

            var vertexes = error1.SelectMany(v => {
                var e = new List<Vector2d>();
                e.Add(v.Source);
                e.Add(v.Target);
                return e;
            }).Distinct().ToList();

            var network = new StreetNetwork();
            network.AddVertexRange(vertexes);
            network.AddEdgeRange(error1);

            var blocks = new BlockAnalysis().GetBlocks(network.Edges);
            // tuple containing block with center and bounding circle radius
            var blocksWithCircle = blocks.Select(b =>
            {
                var edgesAsPointF = b.EdgesV.Select(e => new PointF((float)e.X, (float)e.Y));
                float boudingCircleRadius;
                PointF center;
                BoundingGeometry.FindMinimalBoundingCircle(edgesAsPointF.ToList(), out center, out boudingCircleRadius);
                return Tuple.Create(b, new Vector2d(center.X, center.Y), (double)boudingCircleRadius);
            });

            foreach(var bAndC in blocksWithCircle)
            {
                var blockArea = bAndC.Item1.Area;
                var circleArea = (Math.PI * bAndC.Item3 * bAndC.Item3);
                var resultingArea = blockArea / circleArea;

                Assert.IsTrue(resultingArea <= 1);
            }
        }

        [TestMethod]
        public void TestBlockCircleError2()
        {
            var error2 = new List<UndirectedEdge<Vector2d>>() {
                new UndirectedEdge<Vector2d>(new Vector2d(149.70707, -163.34856), new Vector2d(150.10687, -162.34993)),
                new UndirectedEdge<Vector2d>(new Vector2d(150.10687, -162.34993), new Vector2d(149.915, -161.29514)),
                new UndirectedEdge<Vector2d>(new Vector2d(149.915, -161.29514), new Vector2d(149.62323, -159.8351)),
                new UndirectedEdge<Vector2d>(new Vector2d(149.62323, -159.8351), new Vector2d(148.41296, -160.07703)),
                new UndirectedEdge<Vector2d>(new Vector2d(148.41296, -160.07703), new Vector2d(146.28247, -158.92571)),
                new UndirectedEdge<Vector2d>(new Vector2d(146.28247, -158.92571), new Vector2d(146.19309, -159.03421)),
                new UndirectedEdge<Vector2d>(new Vector2d(146.19309, -159.03421), new Vector2d(144.6305, -160.11623)),
                new UndirectedEdge<Vector2d>(new Vector2d(144.6305, -160.11623), new Vector2d(142.86138, -162.5935)),
                new UndirectedEdge<Vector2d>(new Vector2d(142.86138, -162.5935), new Vector2d(144.26221, -163.605)),
                new UndirectedEdge<Vector2d>(new Vector2d(144.26221, -163.605), new Vector2d(145.99662, -164.87792)),
                new UndirectedEdge<Vector2d>(new Vector2d(145.99662, -164.87792), new Vector2d(148.90513, -166.02272)),
                new UndirectedEdge<Vector2d>(new Vector2d(148.90513, -166.02272), new Vector2d(149.70707, -163.34856))
            };

            var vertexes = error2.SelectMany(v => {
                var e = new List<Vector2d>();
                e.Add(v.Source);
                e.Add(v.Target);
                return e;
            }).Distinct().ToList();

            var network = new StreetNetwork();
            network.AddVertexRange(vertexes);
            network.AddEdgeRange(error2);

            var blocks = new BlockAnalysis().GetBlocks(network.Edges);
            // tuple containing block with center and bounding circle radius
            var blocksWithCircle = blocks.Select(b =>
            {
                var edgesAsPointF = b.EdgesV.Select(e => new PointF((float)e.X, (float)e.Y));
                float boudingCircleRadius;
                PointF center;
                BoundingGeometry.FindMinimalBoundingCircle(edgesAsPointF.ToList(), out center, out boudingCircleRadius);
                return Tuple.Create(b, new Vector2d(center.X, center.Y), (double)boudingCircleRadius);
            });

            foreach (var bAndC in blocksWithCircle)
            {
                var blockArea = bAndC.Item1.Area;
                var circleArea = (Math.PI * bAndC.Item3 * bAndC.Item3);
                var resultingArea = blockArea / circleArea;

                Assert.IsTrue(resultingArea <= 1);
            }
        }

        [TestMethod]
        public void TestBlockCircleError3()
        {
            var error3 = new List<UndirectedEdge<Vector2d>>() {
                new UndirectedEdge<Vector2d>(new Vector2d(162.54412, -222.03577), new Vector2d(162.78575, -221.87334)),
                new UndirectedEdge<Vector2d>(new Vector2d(162.78575, -221.87334), new Vector2d(160.83752, -216.99616)),
                new UndirectedEdge<Vector2d>(new Vector2d(160.83752, -216.99616), new Vector2d(159.67602, -211.97398)),
                new UndirectedEdge<Vector2d>(new Vector2d(159.67602, -211.97398), new Vector2d(159.58014, -211.20395)),
                new UndirectedEdge<Vector2d>(new Vector2d(159.58014, -211.20395), new Vector2d(159.21116, -208.86822)),
                new UndirectedEdge<Vector2d>(new Vector2d(159.21116, -208.86822), new Vector2d(158.4799, -201.91988)),
                new UndirectedEdge<Vector2d>(new Vector2d(158.4799, -201.91988), new Vector2d(158.42146, -201.65657)),
                new UndirectedEdge<Vector2d>(new Vector2d(158.42146, -201.65657), new Vector2d(155.43126, -201.65657)),
                new UndirectedEdge<Vector2d>(new Vector2d(155.43126, -201.65657), new Vector2d(151.25033, -201.6874)),
                new UndirectedEdge<Vector2d>(new Vector2d(151.25033, -201.6874), new Vector2d(150.30251, -201.65344)),
                new UndirectedEdge<Vector2d>(new Vector2d(150.30251, -201.65344), new Vector2d(148.55948, -201.34567)),
                new UndirectedEdge<Vector2d>(new Vector2d(148.55948, -201.34567), new Vector2d(144.77301, -203.98396)),
                new UndirectedEdge<Vector2d>(new Vector2d(144.77301, -203.98396), new Vector2d(143.1257, -204.69)),
                new UndirectedEdge<Vector2d>(new Vector2d(143.1257, -204.69), new Vector2d(144.83171, -205.96873)),
                new UndirectedEdge<Vector2d>(new Vector2d(144.83171, -205.96873), new Vector2d(145.34914, -208.81624)),
                new UndirectedEdge<Vector2d>(new Vector2d(145.34914, -208.81624), new Vector2d(147.27847, -211.38827)),
                new UndirectedEdge<Vector2d>(new Vector2d(147.27847, -211.38827), new Vector2d(150.66132, -209.69698)),
                new UndirectedEdge<Vector2d>(new Vector2d(150.66132, -209.69698), new Vector2d(152.67072, -209.69698)),
                new UndirectedEdge<Vector2d>(new Vector2d(152.67072, -209.69698), new Vector2d(154.15639, -212.03163)),
                new UndirectedEdge<Vector2d>(new Vector2d(154.15639, -212.03163), new Vector2d(152.39581, -214.92414)),
                new UndirectedEdge<Vector2d>(new Vector2d(152.39581, -214.92414), new Vector2d(153.96025, -218.31384)),
                new UndirectedEdge<Vector2d>(new Vector2d(153.96025, -218.31384), new Vector2d(154.41174, -218.10883)),
                new UndirectedEdge<Vector2d>(new Vector2d(154.41174, -218.10883), new Vector2d(156.1645, -217.35767)),
                new UndirectedEdge<Vector2d>(new Vector2d(156.1645, -217.35767), new Vector2d(158.73159, -216.44032)),
                new UndirectedEdge<Vector2d>(new Vector2d(158.73159, -216.44032), new Vector2d(161.8368, -222.45965)),
                new UndirectedEdge<Vector2d>(new Vector2d(161.8368, -222.45965), new Vector2d(162.54412, -222.03577))
            };

            var vertexes = error3.SelectMany(v => {
                var e = new List<Vector2d>();
                e.Add(v.Source);
                e.Add(v.Target);
                return e;
            }).Distinct().ToList();

            var network = new StreetNetwork();
            network.AddVertexRange(vertexes);
            network.AddEdgeRange(error3);

            var blocks = new BlockAnalysis().GetBlocks(network.Edges);
            // tuple containing block with center and bounding circle radius
            var blocksWithCircle = blocks.Select(b =>
            {
                var edgesAsPointF = b.EdgesV.Select(e => new PointF((float)e.X, (float)e.Y));
                float boudingCircleRadius;
                PointF center;
                BoundingGeometry.FindMinimalBoundingCircle(edgesAsPointF.ToList(), out center, out boudingCircleRadius);
                return Tuple.Create(b, new Vector2d(center.X, center.Y), (double)boudingCircleRadius);
            });

            foreach (var bAndC in blocksWithCircle)
            {
                var blockArea = bAndC.Item1.Area;
                var circleArea = (Math.PI * bAndC.Item3 * bAndC.Item3);
                var resultingArea = blockArea / circleArea;

                Assert.IsTrue(resultingArea <= 1);
            }
        }

        [TestMethod]
        public void TestBlockCircleError4()
        {
            var error4 = new List<UndirectedEdge<Vector2d>>() {
                new UndirectedEdge<Vector2d>(new Vector2d(164.44141, -243.57801), new Vector2d(167.05783, -242.13715)),
                new UndirectedEdge<Vector2d>(new Vector2d(167.05783, -242.13715), new Vector2d(169.20401, -241.04717)),
                new UndirectedEdge<Vector2d>(new Vector2d(169.20401, -241.04717), new Vector2d(170.93654, -239.49162)),
                new UndirectedEdge<Vector2d>(new Vector2d(170.93654, -239.49162), new Vector2d(171.03601, -238.99155)),
                new UndirectedEdge<Vector2d>(new Vector2d(171.03601, -238.99155), new Vector2d(170.11684, -237.71798)),
                new UndirectedEdge<Vector2d>(new Vector2d(170.11684, -237.71798), new Vector2d(168.70166, -235.34013)),
                new UndirectedEdge<Vector2d>(new Vector2d(168.70166, -235.34013), new Vector2d(168.35101, -235.67464)),
                new UndirectedEdge<Vector2d>(new Vector2d(168.35101, -235.67464), new Vector2d(166.68608, -237.16804)),
                new UndirectedEdge<Vector2d>(new Vector2d(166.68608, -237.16804), new Vector2d(165.46436, -238.43932)),
                new UndirectedEdge<Vector2d>(new Vector2d(165.46436, -238.43932), new Vector2d(164.48274, -239.51982)),
                new UndirectedEdge<Vector2d>(new Vector2d(164.48274, -239.51982), new Vector2d(164.36304, -241.21842)),
                new UndirectedEdge<Vector2d>(new Vector2d(164.36304, -241.21842), new Vector2d(164.38657, -242.27587)),
                new UndirectedEdge<Vector2d>(new Vector2d(164.38657, -242.27587), new Vector2d(164.38693, -243.29505)),
                new UndirectedEdge<Vector2d>(new Vector2d(164.38693, -243.29505), new Vector2d(164.44067, -243.57828)),
                new UndirectedEdge<Vector2d>(new Vector2d(164.44067, -243.57828), new Vector2d(164.44141, -243.57801))
            };

            var vertexes = error4.SelectMany(v => {
                var e = new List<Vector2d>();
                e.Add(v.Source);
                e.Add(v.Target);
                return e;
            }).Distinct().ToList();

            var network = new StreetNetwork();
            network.AddVertexRange(vertexes);
            network.AddEdgeRange(error4);

            var blocks = new BlockAnalysis().GetBlocks(network.Edges);
            // tuple containing block with center and bounding circle radius
            var blocksWithCircle = blocks.Select(b =>
            {
                var edgesAsPointF = b.EdgesV.Select(e => new PointF((float)e.X, (float)e.Y));
                float boudingCircleRadius;
                PointF center;
                BoundingGeometry.FindMinimalBoundingCircle(edgesAsPointF.ToList(), out center, out boudingCircleRadius);
                return Tuple.Create(b, new Vector2d(center.X, center.Y), (double)boudingCircleRadius);
            });

            foreach (var bAndC in blocksWithCircle)
            {
                var blockArea = bAndC.Item1.Area;
                var circleArea = (Math.PI * bAndC.Item3 * bAndC.Item3);
                var resultingArea = blockArea / circleArea;

                Assert.IsTrue(resultingArea <= 1);
            }
        }

        [TestMethod]
        public void TestBlockCircleError5()
        {
            var error5 = new List<UndirectedEdge<Vector2d>>() {
                new UndirectedEdge<Vector2d>(new Vector2d(41.66536, -145.86707), new Vector2d(41.08557, -143.88031)),
                new UndirectedEdge<Vector2d>(new Vector2d(41.08557, -143.88031), new Vector2d(38.47375, -135.04921)),
                new UndirectedEdge<Vector2d>(new Vector2d(38.47375, -135.04921), new Vector2d(38.47378, -134.76479)),
                new UndirectedEdge<Vector2d>(new Vector2d(38.47378, -134.76479), new Vector2d(36.24191, -134.67905)),
                new UndirectedEdge<Vector2d>(new Vector2d(36.24191, -134.67905), new Vector2d(28.02942, -134.19613)),
                new UndirectedEdge<Vector2d>(new Vector2d(28.02942, -134.19613), new Vector2d(25.82726, -134.61548)),
                new UndirectedEdge<Vector2d>(new Vector2d(25.82726, -134.61548), new Vector2d(25.63323, -133.16133)),
                new UndirectedEdge<Vector2d>(new Vector2d(25.63323, -133.16133), new Vector2d(22.51738, -131.52422)),
                new UndirectedEdge<Vector2d>(new Vector2d(22.51738, -131.52422), new Vector2d(22.07702, -131.33959)),
                new UndirectedEdge<Vector2d>(new Vector2d(22.07702, -131.33959), new Vector2d(15.44305, -131.25123)),
                new UndirectedEdge<Vector2d>(new Vector2d(15.44305, -131.25123), new Vector2d(10.90172, -131.01892)),
                new UndirectedEdge<Vector2d>(new Vector2d(10.90172, -131.01892), new Vector2d(6.39521, -133.17006)),
                new UndirectedEdge<Vector2d>(new Vector2d(6.39521, -133.17006), new Vector2d(10.91308, -136.14184)),
                new UndirectedEdge<Vector2d>(new Vector2d(10.91308, -136.14184), new Vector2d(16.36176, -141.11585)),
                new UndirectedEdge<Vector2d>(new Vector2d(16.36176, -141.11585), new Vector2d(23.16881, -146.31392)),
                new UndirectedEdge<Vector2d>(new Vector2d(23.16881, -146.31392), new Vector2d(27.61545, -147.13761)),
                new UndirectedEdge<Vector2d>(new Vector2d(27.61545, -147.13761), new Vector2d(29.37245, -147.3137)),
                new UndirectedEdge<Vector2d>(new Vector2d(29.37245, -147.3137), new Vector2d(32.04644, -147.37345)),
                new UndirectedEdge<Vector2d>(new Vector2d(32.04644, -147.37345), new Vector2d(35.92711, -147.60903)),
                new UndirectedEdge<Vector2d>(new Vector2d(35.92711, -147.60903), new Vector2d(41.66536, -145.86707))
            };

            var vertexes = error5.SelectMany(v => {
                var e = new List<Vector2d>();
                e.Add(v.Source);
                e.Add(v.Target);
                return e;
            }).Distinct().ToList();

            var network = new StreetNetwork();
            network.AddVertexRange(vertexes);
            network.AddEdgeRange(error5);

            var blocks = new BlockAnalysis().GetBlocks(network.Edges);
            // tuple containing block with center and bounding circle radius
            var blocksWithCircle = blocks.Select(b =>
            {
                var edgesAsPointF = b.EdgesV.Select(e => new PointF((float)e.X, (float)e.Y));
                float boudingCircleRadius;
                PointF center;
                BoundingGeometry.FindMinimalBoundingCircle(edgesAsPointF.ToList(), out center, out boudingCircleRadius);
                return Tuple.Create(b, new Vector2d(center.X, center.Y), (double)boudingCircleRadius);
            });

            foreach (var bAndC in blocksWithCircle)
            {
                var blockArea = bAndC.Item1.Area;
                var circleArea = (Math.PI * bAndC.Item3 * bAndC.Item3);
                var resultingArea = blockArea / circleArea;

                Assert.IsTrue(resultingArea <= 1);
            }
        }
    }
}

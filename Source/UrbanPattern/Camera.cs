﻿using System;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using CPlan.Geometry;

namespace CPlan.UrbanPattern
{
    public class Camera
    {
        //private GLControl glControl;

        const double Eps6 = 0.000001;
        public CGeom3d.Vec_3d m_position;
        public float m_alpha, m_beta, m_gamma;
        public double fovy = Math.PI / 4;

        //public float rotate_x, rotate_y, rotate_z;
        public float m_znear, m_zfar, m_zoom;

        public bool m_ortho;
        private bool m_draw_koordinatenachsen;
        public Color4 Backgroundcolor;
        public bool DrawRaster;

        public float m_scale_init = 30;
        public float z_init = 50;

        Matrix4 modelviewMatrix, projectionMatrix;

        public Camera()//GLControl glControl)
        {
            //this.glControl = glControl;

            m_position = new Vector3D(0, 0, 1);
            m_alpha = 0;
            m_beta = 0;
            m_gamma = 0;
            // m_scale = 30;
            m_ortho = true;
            m_znear = 10;
            m_zfar = -10;
            m_zoom = 60;
            DrawRaster = true;
            m_draw_koordinatenachsen = true;
            Backgroundcolor = Color4.DarkGray; // Color4.Black;
        }

        private void SetPMatrix()
        {
            // Viewport auslesen
            Int32[] oldViewport = new Int32[4];
            GL.GetInteger(GetPName.Viewport, oldViewport);

            double dAspectRatio = Convert.ToDouble(oldViewport[2]) / Convert.ToDouble(oldViewport[3]);
            double dAspectRatioX = dAspectRatio;
            double dAspectRatioY = 1;

            if (!m_ortho)
            {
                Matrix4d m_perspective = Matrix4d.CreatePerspectiveFieldOfView(fovy, dAspectRatio, m_znear, m_zfar);
                GL.MatrixMode(MatrixMode.Projection);
                GL.LoadMatrix(ref m_perspective);

                GL.GetFloat(GetPName.ProjectionMatrix, out projectionMatrix);
            }
            else
            {
                if (dAspectRatioX < 1.0f)
                {
                    dAspectRatioX = 1;
                    dAspectRatioY = Convert.ToDouble(oldViewport[3]) / Convert.ToDouble(oldViewport[2]);
                }

                float ratio = 1.0f;
                double scale = m_position.z;
                GL.Ortho(dAspectRatioX * -ratio * scale, dAspectRatioX * ratio * scale,
                                  dAspectRatioY * -ratio * scale, dAspectRatioY * ratio * scale, m_znear, m_zfar);

                GL.GetFloat(GetPName.ProjectionMatrix, out projectionMatrix);
                //、Console.WriteLine("projection view " + projectionMatrix.ToString());
            }
            oldViewport = null;
        }

        void SetMMatrix()
        {
            if (!m_ortho)
            {
                Matrix4 lookat = Matrix4.LookAt(0, 0, (float)m_position.z, 0, -0.0f, 0, 0, 1, 0);
                GL.LoadMatrix(ref lookat);

                GL.Rotate(m_alpha * 180 / Math.PI, 1, 0, 0);
                GL.Rotate(m_beta * 180 / Math.PI, 0, 1, 0);
            }

            // allow to rotate along z-axis in both 2D and 3D
            GL.Rotate(m_gamma * 180 / Math.PI, 0, 0, 1);

            GL.Translate(m_position.x, m_position.y, 0);

            GL.GetFloat(GetPName.ModelviewMatrix, out modelviewMatrix);
            // Console.WriteLine("model view " + modelviewMatrix.ToString());
        }

        void draw_koordinaten_achsensymbol()
        {
            if (!DrawRaster)
                return;
            if (!m_draw_koordinatenachsen)
                return;

            float dx = 0, dy = 0, dz = 0;

            // ZW: GLU is officially deprecated, cannot draw cylinder using GLU anymore

            ////Pfeilspitze rot
            ////		IntPtr GLU = Glu.NewQuadric();
            //GL.PolygonMode(OpenTK.Graphics.OpenGL.MaterialFace.FrontAndBack, OpenTK.Graphics.OpenGL.PolygonMode.Fill);
            //GL.PushMatrix();
            //GL.LineWidth(1);
            //GL.Color3(1.0f, 0.0f, 0.0f);
            //GL.Translate(dx, dz, -dy);
            //GL.Rotate(90.0f, 0.0f, 1.0f, 0.0f);
            //GL.Translate(0.0f, 0.0f, 0.85f);
            ////		Glu.Cylinder(GLU,0.05,0,0.15,16,1);
            //GL.PopMatrix();

            ////Pfeilspitze grün
            //GL.PushMatrix();
            //GL.LineWidth(1);
            //GL.Color3(0.0f, 1.0f, 0.0f);
            //GL.Translate(dx, dz, -dy);
            //GL.Rotate(90.0f, -1.0f, 0.0f, 0.0f);
            //GL.Translate(0.0f, 0.0f, 0.85f);
            ////		Glu.Cylinder(GLU,0.05,0,0.15,16,1);
            //GL.PopMatrix();

            ////Pfeilspitze blau
            //GL.PushMatrix();
            //GL.LineWidth(1);
            //GL.Color3(0.0f, 0.0f, 1.0f);
            //GL.Translate(dx, dz, -dy);
            //GL.Rotate(180.0f, 1.0f, 0.0f, 0.0f);
            //GL.Translate(0.0f, 0.0f, 0.85f);
            ////Glu.Cylinder(GLU,0.05,0,0.15,16,1);
            //GL.PopMatrix();
            ////		Glu.DeleteQuadric(GLU);

            GL.LineWidth(3);
            GL.Begin(BeginMode.Lines);
            // red x axis
            GL.Color3(1.0f, 0.0f, 0.0f);
            GL.Vertex3(0.0f + dx, 0.0f + dy, 0.0f - dz);
            GL.Vertex3(100f + dx, 0.0f + dy, 0.0f - dz);
            // green z axis
            GL.Color3(0.0f, 1.0f, 0.0f);
            GL.Vertex3(0.0f + dx, 0.0f + dy, 0.0f - dz);
            GL.Vertex3(0.0f + dx, 0.0f + dy, 100f - dz);
            // blue y axis
            GL.Color3(0.0f, 0.0f, 1.0f);
            GL.Vertex3(0.0f + dx, 0.0f + dy, 0.0f - dz);
            GL.Vertex3(0.0f + dx, 100f + dy, 0.0f - dz);
            GL.End();
        }

        // draw coordinate lines
        void draw_koordinaten_lines(float raster_x, float raster_y, float raster_fak, float raster_size)
        {
            if (!DrawRaster)
                return;

            float dx = 0, dy = 0, dz = 0;

            GL.LineWidth(2);
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
            GL.Begin(BeginMode.Lines);
            for (float s1 = -raster_size; s1 <= raster_size; s1 += raster_fak)
            {
                int x = (int)System.Math.Floor((s1 + raster_x) / raster_fak + 0.5f);
                int y = (int)System.Math.Floor((s1 - raster_y) / raster_fak + 0.5f);

                if ((x % 10) == 0)
                    GL.Color4(1.0f, 1.0f, 1.0f, 1.0f);
                else
                    GL.Color4(1.0f, 1.0f, 1.0f, 0.3f);
                if (System.Math.Abs(s1 + raster_x) < 0.000001f) // (s1+raster_x == 0)
                {
                    GL.Vertex3(s1 + raster_x + dx, -raster_size - raster_y - dy, 0.0f + dz);
                    GL.Vertex3(s1 + raster_x + dx, -1.0f - dy, 0.0f + dz);
                    GL.Vertex3(s1 + raster_x + dx, 0.0f - dy, 0.0f + dz);
                    GL.Vertex3(s1 + raster_x + dx, raster_size - raster_y - dy, 0.0f + dz);
                }
                else
                {
                    GL.Vertex3(s1 + raster_x + dx, -raster_size - raster_y - dy, 0.0f + dz);
                    GL.Vertex3(s1 + raster_x + dx, raster_size - raster_y - dy, 0.0f + dz);
                }

                if ((y % 10) == 0)
                    GL.Color4(1.0f, 1.0f, 1.0f, 1.0f);
                else
                    GL.Color4(1.0f, 1.0f, 1.0f, 0.3f);
                if (System.Math.Abs(s1 - raster_y) < 0.000001f) // (s1-raster_y == 0)
                {
                    GL.Vertex3(-raster_size + raster_x + dx, s1 - raster_y - dy, 0.0f + dz);
                    GL.Vertex3(0.0f + dx, s1 - raster_y - dy, 0.0f + dz);
                    GL.Vertex3(1.0f + dx, s1 - raster_y - dy, 0.0f + dz);
                    GL.Vertex3(raster_size + raster_x + dx, s1 - raster_y - dy, 0.0f + dz);
                }
                else
                {
                    GL.Vertex3(-raster_size + raster_x + dx, s1 - raster_y - dy, 0.0f + dz);
                    GL.Vertex3(raster_size + raster_x + dx, s1 - raster_y - dy, 0.0f + dz);
                }
            }
            GL.End();
        }

        // draw background
        void draw_koordinaten_greybackground(float raster_x, float raster_y, float raster_size)
        {
            float dx = 0, dy = 0, dz = 0;
            GL.LineWidth(1);
            GL.Begin(BeginMode.Quads);
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
            GL.Color4(Color4.Blue);
            GL.Color4((Single)0, (Single)0, (Single)0, (Single)0.1f);
            GL.Color4(Backgroundcolor);
            GL.Vertex3(-raster_size + raster_x + dx, raster_size - raster_y - dy, 0.0f + dz);
            GL.Vertex3(-raster_size + raster_x + dx, -raster_size - raster_y - dy, 0.0f + dz);
            GL.Vertex3(raster_size + raster_x + dx, -raster_size - raster_y - dy, 0.0f + dz);
            GL.Vertex3(raster_size + raster_x + dx, raster_size - raster_y - dy, 0.0f + dz);
            GL.End();
        }

        public void DrawKoordinatenkreuz()
        {
            if (!DrawRaster)
                return;
            
            Int32[] oldViewport = new Int32[4];                                       // Viewport auslesen
            GL.GetInteger(GetPName.Viewport, oldViewport);

            float raster_size = 100, raster_fak = 1;
            float raster_x = 0, raster_y = 0;
            double mdx, mdy, md, mdmax;

            mdx = oldViewport[2];
            mdy = oldViewport[3];
            if (mdx < mdy)
            {
                md = mdx;
                mdmax = mdy;
            }
            else
            {
                md = mdy;
                mdmax = mdx;
            }

            //fak sollte auf 10er Potenzen gerundet werden

            raster_fak = (float)System.Math.Floor(System.Math.Log10(System.Math.Abs(m_position.z * 0.2) * 0.5));
            raster_fak = (float)System.Math.Pow(10.0, (double)raster_fak);
            if (raster_fak < 1.0f)
                raster_fak = 1;

            raster_size = raster_fak * 500;

            raster_x = ((float)((int)(m_position.x / raster_fak))) * raster_fak;
            raster_y = ((float)((int)(m_position.y / raster_fak))) * raster_fak;

            GL.Enable(EnableCap.Blend);
            GL.DepthMask(false);
            GL.DepthRange(-500, 500);
            GL.DepthFunc(DepthFunction.Always);

            //Perspektive
            //GL.DepthMask(true);
            //GL.DepthFunc(DepthFunction.Less);
            //draw_koordinaten_achsensymbol();
            //GL.DepthFunc(DepthFunction.Always);
            //GL.DepthMask(false);
            draw_koordinaten_greybackground(raster_x, raster_y, raster_size);
            draw_koordinaten_lines(raster_x, raster_y, raster_fak, raster_size);

            draw_koordinaten_achsensymbol();

            GL.DepthFunc(DepthFunction.Less);
            GL.DepthMask(true);
        }

        public void SetOpenGLMatrixes()
        {
            GL.MatrixMode(MatrixMode.Projection);                                       // Projektionsmatrix einrichten
            GL.PushMatrix();
            GL.LoadIdentity();
            SetPMatrix();

            GL.MatrixMode(MatrixMode.Modelview);                                        // Modellmatrix vom Zeitpunkt des Renderns laden
            GL.PushMatrix();
            GL.LoadIdentity();
            SetMMatrix();
        }

        private float GetAngleV()
        {
            return m_beta;
        }

        private float GetAngleH()
        {
            return m_alpha;
        }

        public Vector3D GetCameraPos()
        {
            return new Vector3D(m_position);
        }

        public Vector3D GetBlickVector()
        {
            Vector3D zw = new Vector3D(), blick = new Vector3D();
            float angle;

            zw.x = 0; zw.y = 0; zw.z = -1;
            blick = zw;

            angle = GetAngleV();
            blick.y = (zw.y * Math.Cos(angle) - zw.z * Math.Sin(angle));
            blick.z = (zw.z * Math.Cos(angle) + zw.y * Math.Sin(angle));

            zw = blick;

            angle = GetAngleH();
            blick.x = (zw.x * Math.Cos(angle) - zw.y * Math.Sin(angle));
            blick.y = (zw.y * Math.Cos(angle) + zw.x * Math.Sin(angle));

            return blick;
        }

        public Vector3D GetBlickVectorPtn()
        {
            return new Vector3D(GetBlickVector());
        }

        public void GetGLCoordinates(int x, int y, int width, int height, CGeom3d.Vec_3d blick_start, CGeom3d.Vec_3d blick_vec, CGeom3d.Vec_3d vecx, CGeom3d.Vec_3d vecy)
        {
            if (m_ortho)
                return;

            //Bug
            y = height - y;

            double rx, ry;
            CGeom3d.Vec_3d pos = new Vector3D(), blick = new Vector3D(), lot = new Vector3D(), zw = new Vector3D();
            double angle, zoom;

            pos = m_position;
            blick_start = m_position;
            blick = GetBlickVector();
            zoom = m_zoom / 180.0 * Math.PI;

            if ((Math.Abs(blick.x) > Eps6) || (Math.Abs(blick.y) > Eps6))
            {
                lot.x = 0.0;
                lot.y = 0.0;
                lot.z = 1.0;
            }
            else
            {
                angle = GetAngleV();
                if (angle < 0)
                {
                    zw.x = 0.0;
                    zw.y = 1.0;
                    zw.z = 0.0;
                }
                else
                {
                    zw.x = 0.0;
                    zw.y = -1.0;
                    zw.z = 0.0;
                }
                lot = zw;
                angle = GetAngleH();
                lot.x = (zw.x * Math.Cos(angle) - zw.y * Math.Sin(angle));
                lot.y = (zw.y * Math.Cos(angle) + zw.x * Math.Sin(angle));
            }
            vecx = lot.amul(blick);
            vecy = vecx.amul(blick);
            if (vecx.Value() < Eps6) return;
            if (vecy.Value() < Eps6) return;
            vecx /= vecx.Value();
            vecy /= vecy.Value();

            rx = x;
            ry = y;

            //relativ zum Mittelpunkt des Views(nr)
            rx = (rx - ((double)width / 2.0f));
            ry = (ry - ((double)height / 2.0f));

            double brennweite, brennweite_y;
            brennweite = (height / 2.0) / Math.Tan(zoom / 2.0);
            brennweite_y = Math.Sqrt(brennweite * brennweite + rx * rx);

            rx = Math.Atan(rx / brennweite);
            ry = Math.Atan(ry / brennweite_y);

            //Abweichungswinkel von der Blickrichtung
            blick_vec = blick;
            CGeom3d.Rot_Achse_3d(blick_vec, vecx, Math.Sin(-ry), Math.Cos(-ry));
            CGeom3d.Rot_Achse_3d(blick_vec, vecy, Math.Sin(rx), Math.Cos(rx));
        }

        public Vector3D GetClickRay(int x, int y, int width, int height)
        {
            Vector3D start = new Vector3D();
            Vector3D blick = new Vector3D();
            Vector3D vecx = new Vector3D();
            Vector3D vecy = new Vector3D();

            GetGLCoordinates(x, y, width, height, start, blick, vecx, vecy);

            return blick;
        }

        public Vector3D GetClickRayPtn(int x, int y, int width, int height)
        {
            return new Vector3D(GetClickRay(x, y, width, height));
        }

        public Vector3D GetPointXYPtn(Vector3D screen) {
            return GetPointXYPtn(screen.X, screen.Y, screen.Z);
        }

        //ZW: get the click point in the camera world
        //reference: http://www.opentk.com/node/1892
        public Vector3D GetPointXYPtn(double x, double y, double zPos)
        {
            int[] viewport = new int[4];

            // Get Matrix
            GL.GetInteger(GetPName.Viewport, viewport);

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadMatrix(ref projectionMatrix);

            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadMatrix(ref modelviewMatrix);

            if (IsZero(projectionMatrix) || IsZero(modelviewMatrix))
                return null;

            Vector3D pNear = new Vector3D(x, viewport[3] - y, 0);
            Vector3D pFar = new Vector3D(x, viewport[3] - y, 1);

            Vector3D worldPNear = UnProject(pNear, modelviewMatrix, projectionMatrix, viewport);
            Vector3D worldPFar = UnProject(pFar, modelviewMatrix, projectionMatrix, viewport);

            double ratio = (worldPNear.z - 0) / (worldPNear.Z - worldPFar.Z);
            double worldX = worldPNear.X + (worldPFar.X - worldPNear.X) * ratio;
            double worldY = worldPNear.Y + (worldPFar.Y - worldPNear.Y) * ratio;

            return new Vector3D(worldX, worldY, 0);
        }

        private Vector3D UnProject(Vector3D screen, Matrix4 view, Matrix4 projection, int[] view_port)
        {
            Vector4 pos = new Vector4();

            // Map x and y from window coordinates, map to range -1 to 1 
            pos.X = Convert.ToSingle(1.0f * (screen.X - view_port[0]) / view_port[2] * 2.0f - 1.0f);
            pos.Y =  Convert.ToSingle(1.0f * (screen.Y - view_port[1]) / view_port[3] * 2.0f - 1.0f);
            pos.Z = Convert.ToSingle(screen.Z * 2.0f - 1.0f);
            pos.W = 1.0f;

            Matrix4 mul = Matrix4.Mult(view, projection);
            
            Matrix4 inv = Matrix4.Invert(mul);
            Vector4 pos2 = Vector4.Transform(pos, inv);
            Vector3D pos_out = new Vector3D(pos2.X, pos2.Y, pos2.Z);

            return pos_out / pos2.W;
        }

        static bool IsZero(Matrix4 m) {
          return (m.M11 == 0)  && (m.M12 == 0) && (m.M13 == 0) && (m.M14 == 0)
              && (m.M21 == 0) && (m.M22 == 0) && (m.M23 == 0) && (m.M24 == 0)
              && (m.M31 == 0) && (m.M32 == 0) && (m.M33 == 0) && (m.M34 == 0)
              && (m.M41 == 0) && (m.M42 == 0) && (m.M43 == 0) && (m.M44 == 0);
        }

        public void SetPosition(double x, double y, double z)
        {
            if (m_ortho && z <= 0)
                z = 10;

            m_position = new CGeom3d.Vec_3d(x, y, z);
        }
    }

}

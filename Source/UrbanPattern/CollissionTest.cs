﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = CollissionTest.cs 
//  Copyright (C) 2014/11/21  11:54 AM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CPlan.Geometry;
using CPlan.VisualObjects;
using FarseerPhysics;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;
using FarseerPhysics.Common.Decomposition;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using System.Drawing;
using OpenTK;

namespace CPlan.UrbanPattern
{
    

    public partial class CollissionTest : Form
    {

        World _world;// = new World(worldAABB, gravity, doSleep);
        List<ColliderPoly> colObjects;

        //===============================================================================================================
        public CollissionTest()
        {
            InitializeComponent();
        }

        //===============================================================================================================
        private void button_collission1_Click(object sender, System.EventArgs e)
        {
            Random rnd = new Random();
            colObjects = new List<ColliderPoly>();
            glCameraControl1.AllObjectsList = new ObjectsAllList();

            _world = new World(new Microsoft.Xna.Framework.Vector2(0f, 0f));//9.82f));
            Settings.AllowSleep = true;
            Settings.VelocityIterations = Convert.ToInt32(numericUpDown_VelIterat.Value);
            Settings.PositionIterations = Convert.ToInt32(numericUpDown_PosIterat.Value);
            Settings.ContinuousPhysics = false;
            int nrObjects = Convert.ToInt32(numericUpDown_NrObj.Value);
            
            // create the test objects
            for (int i = 0; i < nrObjects; i++)
            {              
                Vector2d rndVect = new Vector2d(rnd.NextDouble()*2, rnd.NextDouble()*2);

                Rect2D curRect = new Rect2D(new Vector2d(0,0) , new Vector2d(1,-1) );
                List<Vector2d> vertices = curRect.Points.ToList();
                vertices[3] = new Vector2d(vertices[3].X - 0.5, vertices[3].Y);
                vertices.Add(new Vector2d(-0.5, -0.5));
                vertices.Add(new Vector2d(0, -0.5));

                Poly2D curPoly = new Poly2D(vertices.ToArray()); //curRect.ToPoly2D();

                curPoly.Move(rndVect);
                
                ColliderPoly curColPoly = new ColliderPoly(_world, curPoly);
                curColPoly.FillColor = ConvertColor.CreateColor4(System.Drawing.Color.LightGreen);
                curColPoly.Filled = true;

                glCameraControl1.AllObjectsList.Add(curColPoly);
                colObjects.Add(curColPoly);
            }

            timer1.Enabled = true;
        }

        //===============================================================================================================
        private void timer1_Tick(object sender, EventArgs e)
        {
            float timeStep = 1.0f;

            // This is our little game loop.
            int interat = Convert.ToInt32(numericUpDown_LoopIterat.Value);
            for (int i = 0; i < interat; ++i)
            {
                // Instruct the world to perform a single step of simulation. It is
                // generally best to keep the time step and iterations fixed.
                _world.Step(timeStep); 
            }

            foreach (ColliderPoly thisPoly in colObjects)
            {
                thisPoly.UpdatePos();
            }
            glCameraControl1.Invalidate();
            glCameraControl1.ZoomAll();
        }

        private List<PointF> _oldPoints;
        //===============================================================================================================
        private void button1_Click(object sender, EventArgs e)
        {
            glCameraControl1.AllObjectsList = new ObjectsAllList();

            Random rnd = new Random();
            // -- test bounding circle --
            PointF center;
            float radius;
            List<PointF> myPoints = new List<PointF>();

            if (checkBox_useOldPoints.Checked & _oldPoints != null)
                myPoints = _oldPoints;
            else
            {                
                int nrPts = 3 + rnd.Next(10);
                for (int i = 0; i < nrPts; ++i)
                {
                    myPoints.Add(new PointF((float) rnd.NextDouble()*100, (float) rnd.NextDouble()*100));
                }
                _oldPoints = myPoints;
            }
            List<Vector2d> vPts = new List<Vector2d>(); 
            foreach (PointF curP in myPoints) vPts.Add(new Vector2d(curP.X, curP.Y));
            GPolygon myPoly = new GPolygon(vPts);
            glCameraControl1.AllObjectsList.Add(myPoly);

            List<PointF> convexHull = BoundingGeometry.MakeConvexHull(myPoints);
            BoundingGeometry.FindMinimalBoundingCircle(myPoints, out center, out radius);
            GCircle myCircle = new GCircle(new Vector2d(center.X, center.Y), radius);
            myCircle.Filled = true;
            myCircle.FillColor = ConvertColor.CreateColor4(Color.GreenYellow);
            myCircle.Transparency = .9f;
            myCircle.DeltaZ = -0.2f;

            List<Vector2d> conPts = new List<Vector2d>();
            foreach (PointF curP in convexHull) conPts.Add(new Vector2d(curP.X, curP.Y));
            myPoly = new GPolygon(conPts);
            myPoly.DeltaZ = -0.1f;
            myPoly.BorderWidth = 2;
            myPoly.Filled = true;
            myPoly.FillColor = ConvertColor.CreateColor4(Color.PowderBlue);
            myPoly.BorderColor = ConvertColor.CreateColor4(Color.DeepSkyBlue);
            myPoly.Transparency = .5f;
            glCameraControl1.AllObjectsList.Add(myPoly);

            glCameraControl1.AllObjectsList.Add(myCircle);
            // -- draw the block points for testing --
            foreach (Vector2d point in vPts)
            {
                GCircle gPt = new GCircle(new Vector2d(point.X, point.Y), 1.0, 10);
                gPt.DeltaZ = 0.2f;
                gPt.Filled = true;
                gPt.FillColor = ConvertColor.CreateColor4(Color.LawnGreen);
                glCameraControl1.AllObjectsList.Add(gPt, true, true);
            }

            // --- calculate form factor ---
            double formFactor = GeoAlgorithms2D.Area(conPts) / myCircle.Poly.Area;
            label_FF.Text = formFactor.ToString();

            glCameraControl1.Invalidate();
            glCameraControl1.ZoomAll();
        }

        //===============================================================================================================
        private void button2_Click(object sender, EventArgs e)
        {
            glCameraControl1.AllObjectsList = new ObjectsAllList();

            Random rnd = new Random();
            // -- test bounding circle --
            PointF center;
            float radius;
            List<PointF> myPoints = new List<PointF>();

            if (checkBox_useOldPoints.Checked & _oldPoints != null)
                myPoints = _oldPoints;
            else
            {
                int nrPts = 3 + rnd.Next(10);
                for (int i = 0; i < nrPts; ++i)
                {
                    myPoints.Add(new PointF((float)rnd.NextDouble() * 100, (float)rnd.NextDouble() * 100));
                }
                _oldPoints = myPoints;
            }
            List<Vector2d> vPts = new List<Vector2d>();
            foreach (PointF curP in myPoints) vPts.Add(new Vector2d(curP.X, curP.Y));
            GPolygon myPoly = new GPolygon(vPts);
            glCameraControl1.AllObjectsList.Add(myPoly);

            Vector2d[] minBRpts = GeoAlgorithms2D.MinimalBoundingRectangle(vPts);
            if (minBRpts != null)
            {
                List<Vector2d> minBR = minBRpts.ToList();
                //Vector2d[] minBR = GeoAlgorithms2D.FindSmallestBoundingRectangle(vPts.ToArray());

                myPoly = new GPolygon(minBR.ToList());
                myPoly.DeltaZ = -0.1f;
                myPoly.BorderWidth = 2;
                myPoly.Filled = true;
                myPoly.FillColor = ConvertColor.CreateColor4(Color.PowderBlue);
                myPoly.BorderColor = ConvertColor.CreateColor4(Color.DeepSkyBlue);
                myPoly.Transparency = .5f;
                glCameraControl1.AllObjectsList.Add(myPoly);

                // -- draw the block points for testing --
                foreach (Vector2d point in vPts)
                {
                    GCircle gPt = new GCircle(new Vector2d(point.X, point.Y), 1.0, 10);
                    gPt.DeltaZ = 0.2f;
                    gPt.Filled = true;
                    gPt.FillColor = ConvertColor.CreateColor4(Color.LawnGreen);
                    glCameraControl1.AllObjectsList.Add(gPt, true, true);
                }

                glCameraControl1.Invalidate();
                glCameraControl1.ZoomAll();
            }
        }
    }

    //===============================================================================================================
    //===============================================================================================================
    //===============================================================================================================
    public class ColliderPoly : GPolygon
    {
        //class member variables
        private Body _body;
        private Fixture fixture;
        Random rnd = new Random();
        private float _rotation = 0;

        public ColliderPoly(World world, Poly2D basicShape)
            : base(basicShape)
        {
            Microsoft.Xna.Framework.Vector2 position = new Microsoft.Xna.Framework.Vector2(Convert.ToSingle(basicShape.Center.X), Convert.ToSingle(basicShape.Center.Y));
            _body = new Body(world, position); //null;//world.CreateBody(basicShape);

            // --- define Box2d body ----------------
            _body.BodyType = BodyType.Dynamic;
            _body.SleepingAllowed = true;
            _body.IgnoreCCD = true;
           

            // Define another box shape for our dynamic body.
            Poly2D tmpShape = basicShape.Clone();
            tmpShape.Position = new Vector2d(0,0);
            Vertices polyVertis = new Vertices();
            foreach (Vector2d curVect in tmpShape.PointsFirstLoop)
                polyVertis.Add(new Microsoft.Xna.Framework.Vector2(Convert.ToSingle(curVect.X), Convert.ToSingle(curVect.Y)));

            // decompose to a concave polygon
            List<Vertices> polyVertisTriangulated = Triangulate.ConvexPartition(polyVertis, TriangulationAlgorithm.Earclip);
            PolyClipError err;
            Vertices unions = polyVertisTriangulated[0];
            for (int i = 1; i < polyVertisTriangulated.Count; ++i)
            {
                Vertices vertiSet = polyVertisTriangulated[i];
                YuPengClipper.Union(unions, vertiSet, out err);
            }

            float density = 1.0f;
            PolygonShape shapeDef = new PolygonShape(unions, density);
            //_body.FixedRotation = true;
            fixture = _body.CreateFixture(shapeDef);
            _body.Friction = 0.0f; 
            _body.AngularDamping = 0.01f;
            _body.LinearDamping = 0.01f;

            // Now tell the dynamic body to compute it's mass properties based on its shape.
            //_body.SetMassFromShapes();
            // BodyCur.SetMass( new MassData());   

        }

        //===============================================================================================================
        public void UpdatePos()
        {
            // --- rotation ----------------------------------------------------------------
            float diffRotate = _body.Rotation - _rotation;
            Poly.Rotate(diffRotate);
            _rotation = _body.Rotation;

            // --- damp rotation ---     
            //float damping = 0.5f;
            //float dampedRotate = diffRotate*damping;
            //base.Poly.Rotate(dampedRotate);
            //float newRotation = _body.Rotation - (diffRotate - dampedRotate);
            //_rotation = newRotation;
            //_body.Rotation = newRotation;

            // ---------------------------------------------------------------------------------------------------
            // --- distribute the movement vector to proportion and position change ---
            float dampMove = 1.0f; // large one increase the move vector, smaller decrease it.
            Vector2d moveVect = new Vector2d(_body.Position.X, _body.Position.Y) - Poly.Position;
            //Vector2 testMoveVect = _body.LinearVelocity;
            Vector2d damMoveVect = moveVect * dampMove;
            Poly.Position += damMoveVect;
            _body.Position = new Microsoft.Xna.Framework.Vector2(Convert.ToSingle(Poly.Position.X), Convert.ToSingle(Poly.Position.Y));

            // ---------------------------------------------------------------------------------------------------
            // --- test new shape --------------------------------------------------------------------------------
            // Define another box shape for our dynamic body.
            // if (rnd.NextDouble() < 0.1)
            //    Poly.PointsFirstLoop[0].X += (rnd.NextDouble() - 0.5)*0.2;
            
            Poly2D tmpShape = Poly.Clone();           
            tmpShape.Position = new Vector2d(0, 0);
            tmpShape.Rotate(-_rotation);
            Poly2D tmpShapeBack = tmpShape.Clone();

            // --- apply scaling - use the rotated norm body ---
            // --- proportion change ---

            //Vector2d[] boundRect = GeoAlgorithms2D.MinimalBoundingRectangle(tmpShape.PointsFirstLoop.ToList());
            //Vector2d diagonal = boundRect[3] - boundRect[0];
            double area = GeoAlgorithms2D.Area(tmpShape.PointsFirstLoop.ToList());
            Vector2d rotatedFoce = GeoAlgorithms2D.RotateVector(-_rotation, GeoAlgorithms2D.Centroid(tmpShape.PointsFirstLoop.ToList()), moveVect);

            // --- find max values -----------
            Vector2d centre = tmpShape.Center;
            double maxDx = 0;// Math.Abs(diagonal.X / 2);
            double maxDy = 0;//Math.Abs(diagonal.Y / 2); 
            double minSide = 0.2;
            foreach (Vector2d curVect in tmpShape.PointsFirstLoop)
            {
                Vector2d curRelV = curVect - centre;
                if (Math.Abs(curRelV.X) > maxDx) maxDx = Math.Abs(curRelV.X);
                if (Math.Abs(curRelV.Y) > maxDy) maxDy = Math.Abs(curRelV.Y);
            }

            double forceScale = 0;//0.2;
            if (rotatedFoce.Y > rotatedFoce.X)
            {
                for (int i = 0; i < tmpShape.PointsFirstLoop.Length; ++i)
                {
                    Vector2d curVect = tmpShape.PointsFirstLoop[i];
                    Vector2d curRelV = curVect - centre;                    
                    Vector2d fact = new Vector2d(Math.Abs(curRelV.X/maxDx), Math.Abs(curRelV.Y/maxDy));

                    int dirX = Math.Sign(curRelV.X);
                    int dirY = Math.Sign(curRelV.Y);
                    Vector2d dir = new Vector2d(dirX, dirY*-1);
                    double remainingSide = forceScale*rotatedFoce.X* maxDx * 2;
                    if (minSide < remainingSide)
                    {
                        double newSideX = area / (maxDy*2 + forceScale * rotatedFoce.Y * 2);
                        double deltaSideX = -1* newSideX - maxDx*2;
                        tmpShape.PointsFirstLoop[i].X += (rotatedFoce.Y * fact.X * dir.X * forceScale + deltaSideX * fact.X * dir.X);
                        tmpShape.PointsFirstLoop[i].Y += (rotatedFoce.Y * fact.Y * dir.Y * forceScale);
                    }
                    else tmpShape = tmpShapeBack.Clone();
                }
            }
            else
            {
                for (int i = 0; i < tmpShape.PointsFirstLoop.Length; ++i)
                {
                    Vector2d curVect = tmpShape.PointsFirstLoop[i];
                    Vector2d curRelV = curVect - centre;
                    Vector2d fact = new Vector2d(Math.Abs(curRelV.X / maxDx), Math.Abs(curRelV.Y / maxDy));

                    int dirX = Math.Sign(curRelV.X);
                    int dirY = Math.Sign(curRelV.Y);
                    Vector2d dir = new Vector2d(dirX * -1, dirY);
                    double remainingSide = forceScale * rotatedFoce.Y* maxDy * 2;
                    if (minSide < remainingSide)
                    {
                        double newSideY = area / (maxDx * 2 + forceScale * rotatedFoce.X * 2);
                        double deltaSideY = -1 * newSideY - maxDy * 2;
                        tmpShape.PointsFirstLoop[i].X += (rotatedFoce.X * fact.X * dir.X * forceScale);
                        tmpShape.PointsFirstLoop[i].Y += (rotatedFoce.X * fact.Y * dir.Y * forceScale + deltaSideY * fact.Y * dir.Y);
                    }
                    else tmpShape = tmpShapeBack.Clone();
                }
            }          
            

            // ---------------------------------------------------------------------------------------------------
            Vertices polyVertis = new Vertices();
            foreach (Vector2d curVect in tmpShape.PointsFirstLoop)
            {
                polyVertis.Add(new Microsoft.Xna.Framework.Vector2(Convert.ToSingle(curVect.X), Convert.ToSingle(curVect.Y)));
            }

            // ---------------------------------------------------------------------------------------------------
            float density = 1.0f;
            PolygonShape shapeDef = new PolygonShape(polyVertis, density);
            //_body.FixedRotation = true;
            _body.DestroyFixture(fixture);
            fixture = _body.CreateFixture(shapeDef);
            // ---------------------------------------------------------------------------------------------------

            // --- tempPoly back to original ---
            tmpShapeBack = tmpShape.Clone();
            tmpShapeBack.Position = Poly.Center;
            tmpShapeBack.Rotate(_rotation);
            Poly = tmpShapeBack;
        }
  
    }

}

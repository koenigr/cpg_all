if %1!==! goto ende   --- falls kein Parameter 1 angegeben wurde, wird abgebrochen
if %2!==! goto ende   --- falls kein Parameter 2 angegeben wurde, wird abgebrochen
xcopy %1..\..\ExternalLibraries\OpenCV\bin\*.* %2 /Y
xcopy %1..\..\ExternalLibraries\Glew\bin\*.* %2 /Y
:ende

﻿//#if CT_UNCOMMENT

namespace CPlan.UrbanPattern
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripProgressBar2 = new System.Windows.Forms.ToolStripProgressBar();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.glCameraMainWindow = new CPlan.VisualObjects.GLCameraControl();
            this.glCameraNetwork = new CPlan.VisualObjects.GLCameraControl();
            this.splitter4 = new System.Windows.Forms.Splitter();
            this.glCameraLayouts = new CPlan.VisualObjects.GLCameraControl();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabBuildingLayout = new System.Windows.Forms.TabPage();
            this.propertyGrid3 = new System.Windows.Forms.PropertyGrid();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.propertyGrid2 = new System.Windows.Forms.PropertyGrid();
            this.comboBox_Isovist = new System.Windows.Forms.ComboBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.uD_floorHeigth = new System.Windows.Forms.NumericUpDown();
            this.label29 = new System.Windows.Forms.Label();
            this.uD_maxSideAbs = new System.Windows.Forms.NumericUpDown();
            this.cB_optiAll = new System.Windows.Forms.CheckBox();
            this.label28 = new System.Windows.Forms.Label();
            this.uD_maxHeigth = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.cB_layoutAll = new System.Windows.Forms.CheckBox();
            this.cB_realUpdate = new System.Windows.Forms.CheckBox();
            this.label27 = new System.Windows.Forms.Label();
            this.uD_maxAbsBldArea = new System.Windows.Forms.NumericUpDown();
            this.label26 = new System.Windows.Forms.Label();
            this.uD_minSideAbs = new System.Windows.Forms.NumericUpDown();
            this.label25 = new System.Windows.Forms.Label();
            this.uD_IsovistCellSize = new System.Windows.Forms.NumericUpDown();
            this.label24 = new System.Windows.Forms.Label();
            this.uD_Adapt = new System.Windows.Forms.NumericUpDown();
            this.comboBox_scenario = new System.Windows.Forms.ComboBox();
            this.uD_minHeigth = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.uD_minAbsBldArea = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            this.uD_minSideRatio = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.uD_density = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.uD_maxNrBuildings = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.uD_minNrBuildings = new System.Windows.Forms.NumericUpDown();
            this.tabStreetNetwork = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rB_fitCentral = new System.Windows.Forms.RadioButton();
            this.rB_fitChoice = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.uD_treeDepth = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.uD_minLength = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.uD_maxLength = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.uD_divAngle = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.uD_distance = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.uD_angle = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.uD_connectivity = new System.Windows.Forms.NumericUpDown();
            this.tabOptimization = new System.Windows.Forms.TabPage();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.pictureBox_ESOM = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cB_showSomMap = new System.Windows.Forms.CheckBox();
            this.cb_showSOMorder = new System.Windows.Forms.RadioButton();
            this.cb_showBestGen = new System.Windows.Forms.RadioButton();
            this.cb_showAll = new System.Windows.Forms.RadioButton();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.b_ESOMrender = new System.Windows.Forms.Button();
            this.b_ESOM = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.nUD_distY = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.nUD_distX = new System.Windows.Forms.NumericUpDown();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lbl_Generation = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.uD_Mu = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.uD_Lambda = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.uD_Alpha = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.uD_nrChildren = new System.Windows.Forms.NumericUpDown();
            this.sB_multiOpti = new System.Windows.Forms.RadioButton();
            this.sB_singleOpti = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.uD_populationSize = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.uD_generations = new System.Windows.Forms.NumericUpDown();
            this.tabLucy = new System.Windows.Forms.TabPage();
            this.cB_updateLucy = new System.Windows.Forms.CheckBox();
            this.messageBox = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.tabView = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.b_lockPanel = new System.Windows.Forms.Button();
            this.cB_autoShot = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.uD_lineWidth = new System.Windows.Forms.NumericUpDown();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.B_Analyse = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.ButtonStreets = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_LoadScene = new System.Windows.Forms.ToolStripButton();
            this.ButtonBuildingLayout = new System.Windows.Forms.ToolStripButton();
            this.Button_StartStop = new System.Windows.Forms.ToolStripButton();
            this.Button_IsovistAnalysis = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton10 = new System.Windows.Forms.ToolStripButton();
            this.progressOpti = new System.Windows.Forms.ProgressBar();
            this.progressLoops = new System.Windows.Forms.ProgressBar();
            this.Button_OptiLayout = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.Button_ScenarioLucy = new System.Windows.Forms.ToolStripButton();
            this.Button_IsovistLucy = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton11 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton12 = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.Button_Move = new System.Windows.Forms.ToolStripButton();
            this.Button_Rotate = new System.Windows.Forms.ToolStripButton();
            this.Button_Scale = new System.Windows.Forms.ToolStripButton();
            this.Button_Size = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportDxfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportLrnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importDxfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importGraphFromDxfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readBmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadScenarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.appSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.dateiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.saveDxfDialog = new System.Windows.Forms.SaveFileDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.openDxfDialog = new System.Windows.Forms.OpenFileDialog();
            this.openDxfGraphDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveAllDialog = new System.Windows.Forms.SaveFileDialog();
            this.openAllDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveExportLrn = new System.Windows.Forms.SaveFileDialog();
            this.openBmFile = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog_Scenarios = new System.Windows.Forms.FolderBrowserDialog();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabBuildingLayout.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_floorHeigth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_maxSideAbs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_maxHeigth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_maxAbsBldArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_minSideAbs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_IsovistCellSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_Adapt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_minHeigth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_minAbsBldArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_minSideRatio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_density)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_maxNrBuildings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_minNrBuildings)).BeginInit();
            this.tabStreetNetwork.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_treeDepth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_minLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_maxLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_divAngle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_distance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_angle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_connectivity)).BeginInit();
            this.tabOptimization.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ESOM)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nUD_distY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUD_distX)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_Mu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_Lambda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_Alpha)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_nrChildren)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_populationSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_generations)).BeginInit();
            this.tabLucy.SuspendLayout();
            this.tabView.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_lineWidth)).BeginInit();
            this.toolStrip3.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.toolStrip3);
            this.toolStripContainer1.ContentPanel.Controls.Add(this.toolStrip1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1471, 959);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(1471, 1005);
            this.toolStripContainer1.TabIndex = 1;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.menuStrip1);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBar2,
            this.statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1471, 22);
            this.statusStrip1.TabIndex = 0;
            // 
            // toolStripProgressBar2
            // 
            this.toolStripProgressBar2.Name = "toolStripProgressBar2";
            this.toolStripProgressBar2.Size = new System.Drawing.Size(100, 16);
            this.toolStripProgressBar2.Visible = false;
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(16, 17);
            this.statusLabel.Text = "...";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(102, 37);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.tabControl);
            this.splitContainer1.Size = new System.Drawing.Size(1369, 922);
            this.splitContainer1.SplitterDistance = 1067;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.glCameraMainWindow);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.glCameraNetwork);
            this.splitContainer2.Panel2.Controls.Add(this.splitter4);
            this.splitContainer2.Panel2.Controls.Add(this.glCameraLayouts);
            this.splitContainer2.Size = new System.Drawing.Size(1067, 922);
            this.splitContainer2.SplitterDistance = 565;
            this.splitContainer2.SplitterWidth = 5;
            this.splitContainer2.TabIndex = 1;
            // 
            // glCameraMainWindow
            // 
            this.glCameraMainWindow.AllObjectsList = ((CPlan.VisualObjects.ObjectsAllList)(resources.GetObject("glCameraMainWindow.AllObjectsList")));
            this.glCameraMainWindow.BackColor = System.Drawing.Color.Black;
            this.glCameraMainWindow.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.glCameraMainWindow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.glCameraMainWindow.LoadedScene = false;
            this.glCameraMainWindow.Location = new System.Drawing.Point(0, 0);
            this.glCameraMainWindow.Name = "glCameraMainWindow";
            this.glCameraMainWindow.ObjectInteraction = true;
            this.glCameraMainWindow.RasterColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.glCameraMainWindow.ShowRaster = true;
            this.glCameraMainWindow.Size = new System.Drawing.Size(565, 922);
            this.glCameraMainWindow.SwapBuffersActivated = true;
            this.glCameraMainWindow.TabIndex = 0;
            this.glCameraMainWindow.VSync = false;
            // 
            // glCameraNetwork
            // 
            this.glCameraNetwork.AllObjectsList = ((CPlan.VisualObjects.ObjectsAllList)(resources.GetObject("glCameraNetwork.AllObjectsList")));
            this.glCameraNetwork.BackColor = System.Drawing.Color.Black;
            this.glCameraNetwork.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.glCameraNetwork.Dock = System.Windows.Forms.DockStyle.Fill;
            this.glCameraNetwork.LoadedScene = false;
            this.glCameraNetwork.Location = new System.Drawing.Point(0, 405);
            this.glCameraNetwork.Name = "glCameraNetwork";
            this.glCameraNetwork.ObjectInteraction = true;
            this.glCameraNetwork.RasterColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.glCameraNetwork.ShowRaster = true;
            this.glCameraNetwork.Size = new System.Drawing.Size(497, 517);
            this.glCameraNetwork.SwapBuffersActivated = true;
            this.glCameraNetwork.TabIndex = 24;
            this.glCameraNetwork.VSync = false;
            // 
            // splitter4
            // 
            this.splitter4.BackColor = System.Drawing.SystemColors.Control;
            this.splitter4.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter4.Location = new System.Drawing.Point(0, 400);
            this.splitter4.MinimumSize = new System.Drawing.Size(0, 5);
            this.splitter4.Name = "splitter4";
            this.splitter4.Size = new System.Drawing.Size(497, 5);
            this.splitter4.TabIndex = 23;
            this.splitter4.TabStop = false;
            // 
            // glCameraLayouts
            // 
            this.glCameraLayouts.AllObjectsList = ((CPlan.VisualObjects.ObjectsAllList)(resources.GetObject("glCameraLayouts.AllObjectsList")));
            this.glCameraLayouts.BackColor = System.Drawing.Color.Black;
            this.glCameraLayouts.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.glCameraLayouts.Controls.Add(this.progressOpti);
            this.glCameraLayouts.Controls.Add(this.progressLoops);
            this.glCameraLayouts.Dock = System.Windows.Forms.DockStyle.Top;
            this.glCameraLayouts.LoadedScene = false;
            this.glCameraLayouts.Location = new System.Drawing.Point(0, 0);
            this.glCameraLayouts.Name = "glCameraLayouts";
            this.glCameraLayouts.ObjectInteraction = true;
            this.glCameraLayouts.RasterColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.glCameraLayouts.ShowRaster = true;
            this.glCameraLayouts.Size = new System.Drawing.Size(497, 400);
            this.glCameraLayouts.SwapBuffersActivated = true;
            this.glCameraLayouts.TabIndex = 22;
            this.glCameraLayouts.VSync = false;
            // 
            // progressOpti
            // 
            this.progressOpti.Location = new System.Drawing.Point(0, 0);
            this.progressOpti.Name = "progressOpti";
            this.progressOpti.Size = new System.Drawing.Size(100, 23);
            this.progressOpti.TabIndex = 0;
            // 
            // progressLoops
            // 
            this.progressLoops.Location = new System.Drawing.Point(0, 30);
            this.progressLoops.Name = "progressLoops";
            this.progressLoops.Size = new System.Drawing.Size(100, 23);
            this.progressLoops.TabIndex = 1;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabBuildingLayout);
            this.tabControl.Controls.Add(this.tabStreetNetwork);
            this.tabControl.Controls.Add(this.tabOptimization);
            this.tabControl.Controls.Add(this.tabLucy);
            this.tabControl.Controls.Add(this.tabView);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Multiline = true;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(298, 922);
            this.tabControl.TabIndex = 2;
            // 
            // tabBuildingLayout
            // 
            this.tabBuildingLayout.AutoScroll = true;
            this.tabBuildingLayout.Controls.Add(this.propertyGrid3);
            this.tabBuildingLayout.Controls.Add(this.splitter2);
            this.tabBuildingLayout.Controls.Add(this.groupBox6);
            this.tabBuildingLayout.Controls.Add(this.splitter1);
            this.tabBuildingLayout.Controls.Add(this.propertyGrid1);
            this.tabBuildingLayout.Controls.Add(this.groupBox9);
            this.tabBuildingLayout.Location = new System.Drawing.Point(4, 40);
            this.tabBuildingLayout.Name = "tabBuildingLayout";
            this.tabBuildingLayout.Padding = new System.Windows.Forms.Padding(3);
            this.tabBuildingLayout.Size = new System.Drawing.Size(290, 878);
            this.tabBuildingLayout.TabIndex = 1;
            this.tabBuildingLayout.Text = "BuildingLayout";
            this.tabBuildingLayout.UseVisualStyleBackColor = true;
            // 
            // propertyGrid3
            // 
            this.propertyGrid3.Dock = System.Windows.Forms.DockStyle.Top;
            this.propertyGrid3.Location = new System.Drawing.Point(3, 827);
            this.propertyGrid3.Name = "propertyGrid3";
            this.propertyGrid3.Size = new System.Drawing.Size(267, 226);
            this.propertyGrid3.TabIndex = 21;
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter2.Location = new System.Drawing.Point(3, 823);
            this.splitter2.MinimumSize = new System.Drawing.Size(0, 4);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(267, 4);
            this.splitter2.TabIndex = 20;
            this.splitter2.TabStop = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.propertyGrid2);
            this.groupBox6.Controls.Add(this.comboBox_Isovist);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Location = new System.Drawing.Point(3, 569);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(267, 254);
            this.groupBox6.TabIndex = 18;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Isovist Field";
            // 
            // propertyGrid2
            // 
            this.propertyGrid2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid2.Location = new System.Drawing.Point(3, 37);
            this.propertyGrid2.Name = "propertyGrid2";
            this.propertyGrid2.Size = new System.Drawing.Size(261, 214);
            this.propertyGrid2.TabIndex = 4;
            this.propertyGrid2.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid2_PropertyValueChanged);
            // 
            // comboBox_Isovist
            // 
            this.comboBox_Isovist.Dock = System.Windows.Forms.DockStyle.Top;
            this.comboBox_Isovist.FormattingEnabled = true;
            this.comboBox_Isovist.Items.AddRange(new object[] {
            "White",
            "Area",
            "Perimeter",
            "Compactness",
            "Min Radial",
            "Max Radial",
            "Occlusivity"});
            this.comboBox_Isovist.Location = new System.Drawing.Point(3, 16);
            this.comboBox_Isovist.Name = "comboBox_Isovist";
            this.comboBox_Isovist.Size = new System.Drawing.Size(261, 21);
            this.comboBox_Isovist.TabIndex = 3;
            this.comboBox_Isovist.Text = "Area";
            this.comboBox_Isovist.SelectedIndexChanged += new System.EventHandler(this.comboBox_IsovistIndexChanged);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(3, 565);
            this.splitter1.MinimumSize = new System.Drawing.Size(0, 4);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(267, 4);
            this.splitter1.TabIndex = 15;
            this.splitter1.TabStop = false;
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Top;
            this.propertyGrid1.Location = new System.Drawing.Point(3, 317);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(267, 248);
            this.propertyGrid1.TabIndex = 16;
            this.propertyGrid1.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid1_PropertyValueChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label30);
            this.groupBox9.Controls.Add(this.uD_floorHeigth);
            this.groupBox9.Controls.Add(this.label29);
            this.groupBox9.Controls.Add(this.uD_maxSideAbs);
            this.groupBox9.Controls.Add(this.cB_optiAll);
            this.groupBox9.Controls.Add(this.label28);
            this.groupBox9.Controls.Add(this.uD_maxHeigth);
            this.groupBox9.Controls.Add(this.label23);
            this.groupBox9.Controls.Add(this.cB_layoutAll);
            this.groupBox9.Controls.Add(this.cB_realUpdate);
            this.groupBox9.Controls.Add(this.label27);
            this.groupBox9.Controls.Add(this.uD_maxAbsBldArea);
            this.groupBox9.Controls.Add(this.label26);
            this.groupBox9.Controls.Add(this.uD_minSideAbs);
            this.groupBox9.Controls.Add(this.label25);
            this.groupBox9.Controls.Add(this.uD_IsovistCellSize);
            this.groupBox9.Controls.Add(this.label24);
            this.groupBox9.Controls.Add(this.uD_Adapt);
            this.groupBox9.Controls.Add(this.comboBox_scenario);
            this.groupBox9.Controls.Add(this.uD_minHeigth);
            this.groupBox9.Controls.Add(this.label22);
            this.groupBox9.Controls.Add(this.uD_minAbsBldArea);
            this.groupBox9.Controls.Add(this.label21);
            this.groupBox9.Controls.Add(this.uD_minSideRatio);
            this.groupBox9.Controls.Add(this.label20);
            this.groupBox9.Controls.Add(this.uD_density);
            this.groupBox9.Controls.Add(this.label18);
            this.groupBox9.Controls.Add(this.uD_maxNrBuildings);
            this.groupBox9.Controls.Add(this.label19);
            this.groupBox9.Controls.Add(this.uD_minNrBuildings);
            this.groupBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox9.Location = new System.Drawing.Point(3, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(267, 314);
            this.groupBox9.TabIndex = 1;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Layout Restrictions";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(41, 179);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(61, 13);
            this.label30.TabIndex = 51;
            this.label30.Text = "Floor Hight:";
            // 
            // uD_floorHeigth
            // 
            this.uD_floorHeigth.Location = new System.Drawing.Point(102, 177);
            this.uD_floorHeigth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_floorHeigth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_floorHeigth.Name = "uD_floorHeigth";
            this.uD_floorHeigth.Size = new System.Drawing.Size(55, 20);
            this.uD_floorHeigth.TabIndex = 50;
            this.uD_floorHeigth.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.uD_floorHeigth.ValueChanged += new System.EventHandler(this.uD_minNrBuildings_ValueChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(159, 112);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(12, 13);
            this.label29.TabIndex = 49;
            this.label29.Text = "–";
            // 
            // uD_maxSideAbs
            // 
            this.uD_maxSideAbs.Location = new System.Drawing.Point(173, 108);
            this.uD_maxSideAbs.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_maxSideAbs.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_maxSideAbs.Name = "uD_maxSideAbs";
            this.uD_maxSideAbs.Size = new System.Drawing.Size(50, 20);
            this.uD_maxSideAbs.TabIndex = 48;
            this.uD_maxSideAbs.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // cB_optiAll
            // 
            this.cB_optiAll.AutoSize = true;
            this.cB_optiAll.Location = new System.Drawing.Point(162, 268);
            this.cB_optiAll.Name = "cB_optiAll";
            this.cB_optiAll.Size = new System.Drawing.Size(79, 17);
            this.cB_optiAll.TabIndex = 47;
            this.cB_optiAll.Text = "Optimize all";
            this.cB_optiAll.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(159, 158);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(12, 13);
            this.label28.TabIndex = 46;
            this.label28.Text = "–";
            // 
            // uD_maxHeigth
            // 
            this.uD_maxHeigth.Location = new System.Drawing.Point(173, 156);
            this.uD_maxHeigth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_maxHeigth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_maxHeigth.Name = "uD_maxHeigth";
            this.uD_maxHeigth.Size = new System.Drawing.Size(50, 20);
            this.uD_maxHeigth.TabIndex = 45;
            this.uD_maxHeigth.Value = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this.uD_maxHeigth.ValueChanged += new System.EventHandler(this.uD_minNrBuildings_ValueChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(2, 158);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(100, 13);
            this.label23.TabIndex = 44;
            this.label23.Text = "Hight/Building abs.:";
            // 
            // cB_layoutAll
            // 
            this.cB_layoutAll.AutoSize = true;
            this.cB_layoutAll.Location = new System.Drawing.Point(37, 268);
            this.cB_layoutAll.Name = "cB_layoutAll";
            this.cB_layoutAll.Size = new System.Drawing.Size(121, 17);
            this.cB_layoutAll.TabIndex = 43;
            this.cB_layoutAll.Text = "Layout for all Blocks";
            this.cB_layoutAll.UseVisualStyleBackColor = true;
            // 
            // cB_realUpdate
            // 
            this.cB_realUpdate.AutoSize = true;
            this.cB_realUpdate.Location = new System.Drawing.Point(37, 251);
            this.cB_realUpdate.Name = "cB_realUpdate";
            this.cB_realUpdate.Size = new System.Drawing.Size(128, 17);
            this.cB_realUpdate.TabIndex = 42;
            this.cB_realUpdate.Text = "Live Updateing Fields";
            this.cB_realUpdate.UseVisualStyleBackColor = true;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(159, 136);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(12, 13);
            this.label27.TabIndex = 41;
            this.label27.Text = "–";
            // 
            // uD_maxAbsBldArea
            // 
            this.uD_maxAbsBldArea.Location = new System.Drawing.Point(173, 134);
            this.uD_maxAbsBldArea.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_maxAbsBldArea.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_maxAbsBldArea.Name = "uD_maxAbsBldArea";
            this.uD_maxAbsBldArea.Size = new System.Drawing.Size(50, 20);
            this.uD_maxAbsBldArea.TabIndex = 40;
            this.uD_maxAbsBldArea.Value = new decimal(new int[] {
            4000,
            0,
            0,
            0});
            this.uD_maxAbsBldArea.ValueChanged += new System.EventHandler(this.uD_minNrBuildings_ValueChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(24, 112);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(75, 13);
            this.label26.TabIndex = 39;
            this.label26.Text = "Side Absolute:";
            // 
            // uD_minSideAbs
            // 
            this.uD_minSideAbs.Location = new System.Drawing.Point(102, 110);
            this.uD_minSideAbs.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_minSideAbs.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_minSideAbs.Name = "uD_minSideAbs";
            this.uD_minSideAbs.Size = new System.Drawing.Size(55, 20);
            this.uD_minSideAbs.TabIndex = 38;
            this.uD_minSideAbs.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.uD_minSideAbs.ValueChanged += new System.EventHandler(this.uD_minNrBuildings_ValueChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(16, 227);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(83, 13);
            this.label25.TabIndex = 37;
            this.label25.Text = "Isovist Cell Size:";
            // 
            // uD_IsovistCellSize
            // 
            this.uD_IsovistCellSize.Location = new System.Drawing.Point(102, 225);
            this.uD_IsovistCellSize.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_IsovistCellSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_IsovistCellSize.Name = "uD_IsovistCellSize";
            this.uD_IsovistCellSize.Size = new System.Drawing.Size(55, 20);
            this.uD_IsovistCellSize.TabIndex = 36;
            this.uD_IsovistCellSize.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.uD_IsovistCellSize.ValueChanged += new System.EventHandler(this.uD_minNrBuildings_ValueChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(23, 203);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(76, 13);
            this.label24.TabIndex = 35;
            this.label24.Text = "Repeat Adapt:";
            // 
            // uD_Adapt
            // 
            this.uD_Adapt.Location = new System.Drawing.Point(102, 201);
            this.uD_Adapt.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_Adapt.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_Adapt.Name = "uD_Adapt";
            this.uD_Adapt.Size = new System.Drawing.Size(55, 20);
            this.uD_Adapt.TabIndex = 34;
            this.uD_Adapt.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.uD_Adapt.ValueChanged += new System.EventHandler(this.uD_minNrBuildings_ValueChanged);
            // 
            // comboBox_scenario
            // 
            this.comboBox_scenario.FormattingEnabled = true;
            this.comboBox_scenario.Items.AddRange(new object[] {
            "Basic Senario",
            "Cape Town Scenario",
            "Rochor Scenario"});
            this.comboBox_scenario.Location = new System.Drawing.Point(3, 287);
            this.comboBox_scenario.Name = "comboBox_scenario";
            this.comboBox_scenario.Size = new System.Drawing.Size(213, 21);
            this.comboBox_scenario.TabIndex = 33;
            this.comboBox_scenario.Text = "Select Scenario";
            this.comboBox_scenario.SelectedIndexChanged += new System.EventHandler(this.comboBox_scenario_SelectedIndexChanged);
            // 
            // uD_minHeigth
            // 
            this.uD_minHeigth.Location = new System.Drawing.Point(102, 156);
            this.uD_minHeigth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_minHeigth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_minHeigth.Name = "uD_minHeigth";
            this.uD_minHeigth.Size = new System.Drawing.Size(55, 20);
            this.uD_minHeigth.TabIndex = 31;
            this.uD_minHeigth.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.uD_minHeigth.ValueChanged += new System.EventHandler(this.uD_minNrBuildings_ValueChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(2, 136);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(97, 13);
            this.label22.TabIndex = 30;
            this.label22.Text = "Area/Building abs.:";
            // 
            // uD_minAbsBldArea
            // 
            this.uD_minAbsBldArea.Location = new System.Drawing.Point(102, 134);
            this.uD_minAbsBldArea.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_minAbsBldArea.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_minAbsBldArea.Name = "uD_minAbsBldArea";
            this.uD_minAbsBldArea.Size = new System.Drawing.Size(55, 20);
            this.uD_minAbsBldArea.TabIndex = 29;
            this.uD_minAbsBldArea.Value = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.uD_minAbsBldArea.ValueChanged += new System.EventHandler(this.uD_minNrBuildings_ValueChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 89);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(93, 13);
            this.label21.TabIndex = 28;
            this.label21.Text = "Min. Side Ratio %:";
            // 
            // uD_minSideRatio
            // 
            this.uD_minSideRatio.Enabled = false;
            this.uD_minSideRatio.Location = new System.Drawing.Point(102, 87);
            this.uD_minSideRatio.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_minSideRatio.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_minSideRatio.Name = "uD_minSideRatio";
            this.uD_minSideRatio.Size = new System.Drawing.Size(55, 20);
            this.uD_minSideRatio.TabIndex = 27;
            this.uD_minSideRatio.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.uD_minSideRatio.ValueChanged += new System.EventHandler(this.uD_minNrBuildings_ValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(32, 66);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(67, 13);
            this.label20.TabIndex = 26;
            this.label20.Text = "Density in %:";
            // 
            // uD_density
            // 
            this.uD_density.Location = new System.Drawing.Point(102, 64);
            this.uD_density.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_density.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_density.Name = "uD_density";
            this.uD_density.Size = new System.Drawing.Size(55, 20);
            this.uD_density.TabIndex = 25;
            this.uD_density.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.uD_density.ValueChanged += new System.EventHandler(this.uD_minNrBuildings_ValueChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(4, 43);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(95, 13);
            this.label18.TabIndex = 24;
            this.label18.Text = "Max. Nr. Buildings:";
            // 
            // uD_maxNrBuildings
            // 
            this.uD_maxNrBuildings.Location = new System.Drawing.Point(102, 41);
            this.uD_maxNrBuildings.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_maxNrBuildings.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_maxNrBuildings.Name = "uD_maxNrBuildings";
            this.uD_maxNrBuildings.Size = new System.Drawing.Size(55, 20);
            this.uD_maxNrBuildings.TabIndex = 23;
            this.uD_maxNrBuildings.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.uD_maxNrBuildings.ValueChanged += new System.EventHandler(this.uD_minNrBuildings_ValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(7, 20);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(92, 13);
            this.label19.TabIndex = 22;
            this.label19.Text = "Min. Nr. Buildings:";
            // 
            // uD_minNrBuildings
            // 
            this.uD_minNrBuildings.Location = new System.Drawing.Point(102, 18);
            this.uD_minNrBuildings.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_minNrBuildings.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_minNrBuildings.Name = "uD_minNrBuildings";
            this.uD_minNrBuildings.Size = new System.Drawing.Size(55, 20);
            this.uD_minNrBuildings.TabIndex = 21;
            this.uD_minNrBuildings.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.uD_minNrBuildings.ValueChanged += new System.EventHandler(this.uD_minNrBuildings_ValueChanged);
            // 
            // tabStreetNetwork
            // 
            this.tabStreetNetwork.Controls.Add(this.groupBox4);
            this.tabStreetNetwork.Controls.Add(this.groupBox1);
            this.tabStreetNetwork.Location = new System.Drawing.Point(4, 40);
            this.tabStreetNetwork.Name = "tabStreetNetwork";
            this.tabStreetNetwork.Padding = new System.Windows.Forms.Padding(3);
            this.tabStreetNetwork.Size = new System.Drawing.Size(290, 878);
            this.tabStreetNetwork.TabIndex = 0;
            this.tabStreetNetwork.Text = "StreetNetwork";
            this.tabStreetNetwork.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rB_fitCentral);
            this.groupBox4.Controls.Add(this.rB_fitChoice);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(3, 194);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(284, 65);
            this.groupBox4.TabIndex = 32;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Fitness";
            // 
            // rB_fitCentral
            // 
            this.rB_fitCentral.AutoSize = true;
            this.rB_fitCentral.Location = new System.Drawing.Point(54, 40);
            this.rB_fitCentral.Name = "rB_fitCentral";
            this.rB_fitCentral.Size = new System.Drawing.Size(87, 17);
            this.rB_fitCentral.TabIndex = 1;
            this.rB_fitCentral.Text = "use centrality";
            this.rB_fitCentral.UseVisualStyleBackColor = true;
            // 
            // rB_fitChoice
            // 
            this.rB_fitChoice.AutoSize = true;
            this.rB_fitChoice.Checked = true;
            this.rB_fitChoice.Location = new System.Drawing.Point(54, 21);
            this.rB_fitChoice.Name = "rB_fitChoice";
            this.rB_fitChoice.Size = new System.Drawing.Size(77, 17);
            this.rB_fitChoice.TabIndex = 0;
            this.rB_fitChoice.TabStop = true;
            this.rB_fitChoice.Text = "use choice";
            this.rB_fitChoice.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.uD_treeDepth);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.uD_minLength);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.uD_maxLength);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.uD_divAngle);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.uD_distance);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.uD_angle);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.uD_connectivity);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(284, 191);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Morphological Parameter";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(49, 164);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "Tree Depth:";
            // 
            // uD_treeDepth
            // 
            this.uD_treeDepth.Location = new System.Drawing.Point(116, 162);
            this.uD_treeDepth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_treeDepth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_treeDepth.Name = "uD_treeDepth";
            this.uD_treeDepth.Size = new System.Drawing.Size(42, 20);
            this.uD_treeDepth.TabIndex = 29;
            this.uD_treeDepth.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.uD_treeDepth.ValueChanged += new System.EventHandler(this.uD_treeDepth_ValueChanged_1);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "min segment length:";
            // 
            // uD_minLength
            // 
            this.uD_minLength.Location = new System.Drawing.Point(116, 134);
            this.uD_minLength.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.uD_minLength.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_minLength.Name = "uD_minLength";
            this.uD_minLength.Size = new System.Drawing.Size(42, 20);
            this.uD_minLength.TabIndex = 27;
            this.uD_minLength.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.uD_minLength.ValueChanged += new System.EventHandler(this.uD_treeDepth_ValueChanged_1);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 114);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "max segment length:";
            // 
            // uD_maxLength
            // 
            this.uD_maxLength.Location = new System.Drawing.Point(116, 112);
            this.uD_maxLength.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.uD_maxLength.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_maxLength.Name = "uD_maxLength";
            this.uD_maxLength.Size = new System.Drawing.Size(42, 20);
            this.uD_maxLength.TabIndex = 25;
            this.uD_maxLength.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.uD_maxLength.ValueChanged += new System.EventHandler(this.uD_treeDepth_ValueChanged_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "min division angle:";
            // 
            // uD_divAngle
            // 
            this.uD_divAngle.Enabled = false;
            this.uD_divAngle.Location = new System.Drawing.Point(116, 90);
            this.uD_divAngle.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.uD_divAngle.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_divAngle.Name = "uD_divAngle";
            this.uD_divAngle.Size = new System.Drawing.Size(42, 20);
            this.uD_divAngle.TabIndex = 23;
            this.uD_divAngle.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.uD_divAngle.ValueChanged += new System.EventHandler(this.uD_treeDepth_ValueChanged_1);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "min distances:";
            // 
            // uD_distance
            // 
            this.uD_distance.Location = new System.Drawing.Point(116, 68);
            this.uD_distance.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.uD_distance.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_distance.Name = "uD_distance";
            this.uD_distance.Size = new System.Drawing.Size(42, 20);
            this.uD_distance.TabIndex = 21;
            this.uD_distance.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.uD_distance.ValueChanged += new System.EventHandler(this.uD_treeDepth_ValueChanged_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "max random angle:";
            // 
            // uD_angle
            // 
            this.uD_angle.Location = new System.Drawing.Point(116, 46);
            this.uD_angle.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.uD_angle.Name = "uD_angle";
            this.uD_angle.Size = new System.Drawing.Size(42, 20);
            this.uD_angle.TabIndex = 19;
            this.uD_angle.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.uD_angle.ValueChanged += new System.EventHandler(this.uD_treeDepth_ValueChanged_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "max connectivity:";
            // 
            // uD_connectivity
            // 
            this.uD_connectivity.Location = new System.Drawing.Point(116, 24);
            this.uD_connectivity.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.uD_connectivity.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_connectivity.Name = "uD_connectivity";
            this.uD_connectivity.Size = new System.Drawing.Size(42, 20);
            this.uD_connectivity.TabIndex = 17;
            this.uD_connectivity.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.uD_connectivity.ValueChanged += new System.EventHandler(this.uD_treeDepth_ValueChanged_1);
            // 
            // tabOptimization
            // 
            this.tabOptimization.Controls.Add(this.chart1);
            this.tabOptimization.Controls.Add(this.splitter3);
            this.tabOptimization.Controls.Add(this.pictureBox_ESOM);
            this.tabOptimization.Controls.Add(this.groupBox2);
            this.tabOptimization.Controls.Add(this.groupBox8);
            this.tabOptimization.Controls.Add(this.groupBox7);
            this.tabOptimization.Controls.Add(this.groupBox3);
            this.tabOptimization.Location = new System.Drawing.Point(4, 40);
            this.tabOptimization.Name = "tabOptimization";
            this.tabOptimization.Padding = new System.Windows.Forms.Padding(3);
            this.tabOptimization.Size = new System.Drawing.Size(290, 878);
            this.tabOptimization.TabIndex = 2;
            this.tabOptimization.Text = "Optimization";
            this.tabOptimization.UseVisualStyleBackColor = true;
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(3, 605);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastPoint;
            series1.IsVisibleInLegend = false;
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastPoint;
            series2.IsVisibleInLegend = false;
            series2.Legend = "Legend1";
            series2.MarkerColor = System.Drawing.Color.Black;
            series2.Name = "Series2";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Size = new System.Drawing.Size(284, 270);
            this.chart1.TabIndex = 50;
            this.chart1.Text = "chart1";
            // 
            // splitter3
            // 
            this.splitter3.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Location = new System.Drawing.Point(3, 601);
            this.splitter3.MinimumSize = new System.Drawing.Size(0, 4);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(284, 4);
            this.splitter3.TabIndex = 49;
            this.splitter3.TabStop = false;
            // 
            // pictureBox_ESOM
            // 
            this.pictureBox_ESOM.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox_ESOM.Location = new System.Drawing.Point(3, 452);
            this.pictureBox_ESOM.Name = "pictureBox_ESOM";
            this.pictureBox_ESOM.Size = new System.Drawing.Size(284, 149);
            this.pictureBox_ESOM.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_ESOM.TabIndex = 47;
            this.pictureBox_ESOM.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cB_showSomMap);
            this.groupBox2.Controls.Add(this.cb_showSOMorder);
            this.groupBox2.Controls.Add(this.cb_showBestGen);
            this.groupBox2.Controls.Add(this.cb_showAll);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(3, 366);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(284, 86);
            this.groupBox2.TabIndex = 39;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Visualization";
            // 
            // cB_showSomMap
            // 
            this.cB_showSomMap.AutoSize = true;
            this.cB_showSomMap.Location = new System.Drawing.Point(132, 59);
            this.cB_showSomMap.Name = "cB_showSomMap";
            this.cB_showSomMap.Size = new System.Drawing.Size(101, 17);
            this.cB_showSomMap.TabIndex = 35;
            this.cB_showSomMap.Text = "show SOM map";
            this.cB_showSomMap.UseVisualStyleBackColor = true;
            // 
            // cb_showSOMorder
            // 
            this.cb_showSOMorder.AutoSize = true;
            this.cb_showSOMorder.Checked = true;
            this.cb_showSOMorder.Location = new System.Drawing.Point(9, 58);
            this.cb_showSOMorder.Name = "cb_showSOMorder";
            this.cb_showSOMorder.Size = new System.Drawing.Size(104, 17);
            this.cb_showSOMorder.TabIndex = 2;
            this.cb_showSOMorder.TabStop = true;
            this.cb_showSOMorder.Text = "show SOM order";
            this.cb_showSOMorder.UseVisualStyleBackColor = true;
            // 
            // cb_showBestGen
            // 
            this.cb_showBestGen.AutoSize = true;
            this.cb_showBestGen.Location = new System.Drawing.Point(9, 38);
            this.cb_showBestGen.Name = "cb_showBestGen";
            this.cb_showBestGen.Size = new System.Drawing.Size(129, 17);
            this.cb_showBestGen.TabIndex = 1;
            this.cb_showBestGen.Text = "show best gerneration";
            this.cb_showBestGen.UseVisualStyleBackColor = true;
            // 
            // cb_showAll
            // 
            this.cb_showAll.AutoSize = true;
            this.cb_showAll.Location = new System.Drawing.Point(9, 19);
            this.cb_showAll.Name = "cb_showAll";
            this.cb_showAll.Size = new System.Drawing.Size(120, 17);
            this.cb_showAll.TabIndex = 0;
            this.cb_showAll.Text = "show all populations";
            this.cb_showAll.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.b_ESOMrender);
            this.groupBox8.Controls.Add(this.b_ESOM);
            this.groupBox8.Controls.Add(this.label17);
            this.groupBox8.Controls.Add(this.nUD_distY);
            this.groupBox8.Controls.Add(this.label13);
            this.groupBox8.Controls.Add(this.nUD_distX);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox8.Location = new System.Drawing.Point(3, 261);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(284, 105);
            this.groupBox8.TabIndex = 38;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "SOM Layout";
            // 
            // b_ESOMrender
            // 
            this.b_ESOMrender.Location = new System.Drawing.Point(139, 23);
            this.b_ESOMrender.Name = "b_ESOMrender";
            this.b_ESOMrender.Size = new System.Drawing.Size(94, 23);
            this.b_ESOMrender.TabIndex = 26;
            this.b_ESOMrender.Text = "ESOM Render";
            this.b_ESOMrender.UseVisualStyleBackColor = true;
            this.b_ESOMrender.Click += new System.EventHandler(this.b_ESOMrender_Click);
            // 
            // b_ESOM
            // 
            this.b_ESOM.Location = new System.Drawing.Point(9, 23);
            this.b_ESOM.Name = "b_ESOM";
            this.b_ESOM.Size = new System.Drawing.Size(124, 23);
            this.b_ESOM.TabIndex = 25;
            this.b_ESOM.Text = "ESOM Analysis";
            this.b_ESOM.UseVisualStyleBackColor = true;
            this.b_ESOM.Click += new System.EventHandler(this.b_ESOM_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 77);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(62, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "Distance Y:";
            // 
            // nUD_distY
            // 
            this.nUD_distY.Location = new System.Drawing.Point(78, 75);
            this.nUD_distY.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nUD_distY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUD_distY.Name = "nUD_distY";
            this.nUD_distY.Size = new System.Drawing.Size(55, 20);
            this.nUD_distY.TabIndex = 23;
            this.nUD_distY.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nUD_distY.ValueChanged += new System.EventHandler(this.nUD_distX_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 54);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "Distance X:";
            // 
            // nUD_distX
            // 
            this.nUD_distX.Location = new System.Drawing.Point(78, 52);
            this.nUD_distX.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nUD_distX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUD_distX.Name = "nUD_distX";
            this.nUD_distX.Size = new System.Drawing.Size(55, 20);
            this.nUD_distX.TabIndex = 21;
            this.nUD_distX.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nUD_distX.ValueChanged += new System.EventHandler(this.nUD_distX_ValueChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.lbl_Generation);
            this.groupBox7.Controls.Add(this.label11);
            this.groupBox7.Controls.Add(this.label14);
            this.groupBox7.Controls.Add(this.uD_Mu);
            this.groupBox7.Controls.Add(this.label15);
            this.groupBox7.Controls.Add(this.uD_Lambda);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Controls.Add(this.uD_Alpha);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox7.Location = new System.Drawing.Point(3, 142);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(284, 119);
            this.groupBox7.TabIndex = 29;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Multi-Criteria-Selection";
            // 
            // lbl_Generation
            // 
            this.lbl_Generation.AutoSize = true;
            this.lbl_Generation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Generation.Location = new System.Drawing.Point(124, 96);
            this.lbl_Generation.Name = "lbl_Generation";
            this.lbl_Generation.Size = new System.Drawing.Size(28, 13);
            this.lbl_Generation.TabIndex = 30;
            this.lbl_Generation.Text = "000";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(60, 96);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "Generation:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 71);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(115, 13);
            this.label14.TabIndex = 24;
            this.label14.Text = "Receive from Selector:";
            // 
            // uD_Mu
            // 
            this.uD_Mu.Enabled = false;
            this.uD_Mu.Location = new System.Drawing.Point(127, 69);
            this.uD_Mu.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_Mu.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_Mu.Name = "uD_Mu";
            this.uD_Mu.Size = new System.Drawing.Size(55, 20);
            this.uD_Mu.TabIndex = 23;
            this.uD_Mu.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(33, 49);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(89, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "Send to Selector:";
            // 
            // uD_Lambda
            // 
            this.uD_Lambda.Enabled = false;
            this.uD_Lambda.Location = new System.Drawing.Point(127, 47);
            this.uD_Lambda.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_Lambda.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_Lambda.Name = "uD_Lambda";
            this.uD_Lambda.Size = new System.Drawing.Size(55, 20);
            this.uD_Lambda.TabIndex = 21;
            this.uD_Lambda.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(53, 27);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(69, 13);
            this.label16.TabIndex = 20;
            this.label16.Text = "Archive Size:";
            // 
            // uD_Alpha
            // 
            this.uD_Alpha.Location = new System.Drawing.Point(127, 25);
            this.uD_Alpha.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_Alpha.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_Alpha.Name = "uD_Alpha";
            this.uD_Alpha.Size = new System.Drawing.Size(55, 20);
            this.uD_Alpha.TabIndex = 19;
            this.uD_Alpha.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.uD_nrChildren);
            this.groupBox3.Controls.Add(this.sB_multiOpti);
            this.groupBox3.Controls.Add(this.sB_singleOpti);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.uD_populationSize);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.uD_generations);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(284, 139);
            this.groupBox3.TabIndex = 28;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Evolutionary Algorithm";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 114);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 13);
            this.label12.TabIndex = 30;
            this.label12.Text = "Number of Children:";
            // 
            // uD_nrChildren
            // 
            this.uD_nrChildren.Location = new System.Drawing.Point(126, 112);
            this.uD_nrChildren.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_nrChildren.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_nrChildren.Name = "uD_nrChildren";
            this.uD_nrChildren.Size = new System.Drawing.Size(55, 20);
            this.uD_nrChildren.TabIndex = 29;
            this.uD_nrChildren.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.uD_nrChildren.ValueChanged += new System.EventHandler(this.uD_nrChildren_ValueChanged);
            // 
            // sB_multiOpti
            // 
            this.sB_multiOpti.AutoSize = true;
            this.sB_multiOpti.Checked = true;
            this.sB_multiOpti.Location = new System.Drawing.Point(35, 41);
            this.sB_multiOpti.Name = "sB_multiOpti";
            this.sB_multiOpti.Size = new System.Drawing.Size(138, 17);
            this.sB_multiOpti.TabIndex = 28;
            this.sB_multiOpti.TabStop = true;
            this.sB_multiOpti.Text = "multi-criteria optimization";
            this.sB_multiOpti.UseVisualStyleBackColor = true;
            // 
            // sB_singleOpti
            // 
            this.sB_singleOpti.AutoSize = true;
            this.sB_singleOpti.Location = new System.Drawing.Point(35, 19);
            this.sB_singleOpti.Name = "sB_singleOpti";
            this.sB_singleOpti.Size = new System.Drawing.Size(144, 17);
            this.sB_singleOpti.TabIndex = 27;
            this.sB_singleOpti.Text = "single criteria optimization";
            this.sB_singleOpti.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(39, 92);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Population Size:";
            // 
            // uD_populationSize
            // 
            this.uD_populationSize.Location = new System.Drawing.Point(126, 90);
            this.uD_populationSize.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_populationSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_populationSize.Name = "uD_populationSize";
            this.uD_populationSize.Size = new System.Drawing.Size(55, 20);
            this.uD_populationSize.TabIndex = 21;
            this.uD_populationSize.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.uD_populationSize.ValueChanged += new System.EventHandler(this.uD_populationSize_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(55, 70);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Generations:";
            // 
            // uD_generations
            // 
            this.uD_generations.Location = new System.Drawing.Point(126, 68);
            this.uD_generations.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_generations.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_generations.Name = "uD_generations";
            this.uD_generations.Size = new System.Drawing.Size(55, 20);
            this.uD_generations.TabIndex = 19;
            this.uD_generations.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.uD_generations.ValueChanged += new System.EventHandler(this.uD_generations_ValueChanged);
            // 
            // tabLucy
            // 
            this.tabLucy.Controls.Add(this.cB_updateLucy);
            this.tabLucy.Controls.Add(this.messageBox);
            this.tabLucy.Controls.Add(this.button2);
            this.tabLucy.Location = new System.Drawing.Point(4, 40);
            this.tabLucy.Name = "tabLucy";
            this.tabLucy.Padding = new System.Windows.Forms.Padding(3);
            this.tabLucy.Size = new System.Drawing.Size(290, 878);
            this.tabLucy.TabIndex = 3;
            this.tabLucy.Text = "Lucy";
            this.tabLucy.UseVisualStyleBackColor = true;
            // 
            // cB_updateLucy
            // 
            this.cB_updateLucy.AutoSize = true;
            this.cB_updateLucy.Location = new System.Drawing.Point(9, 35);
            this.cB_updateLucy.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.cB_updateLucy.Name = "cB_updateLucy";
            this.cB_updateLucy.Size = new System.Drawing.Size(115, 17);
            this.cB_updateLucy.TabIndex = 43;
            this.cB_updateLucy.Text = "Use Lucy Services";
            this.cB_updateLucy.UseVisualStyleBackColor = true;
            this.cB_updateLucy.CheckedChanged += new System.EventHandler(this.cB_updateLucy_CheckedChanged);
            // 
            // messageBox
            // 
            this.messageBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.messageBox.Location = new System.Drawing.Point(3, 99);
            this.messageBox.Multiline = true;
            this.messageBox.Name = "messageBox";
            this.messageBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.messageBox.Size = new System.Drawing.Size(284, 776);
            this.messageBox.TabIndex = 1;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(6, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(139, 23);
            this.button2.TabIndex = 0;
            this.button2.Text = "ConnectLucyLocal";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // tabView
            // 
            this.tabView.Controls.Add(this.groupBox5);
            this.tabView.Controls.Add(this.groupBox10);
            this.tabView.Location = new System.Drawing.Point(4, 40);
            this.tabView.Name = "tabView";
            this.tabView.Padding = new System.Windows.Forms.Padding(3);
            this.tabView.Size = new System.Drawing.Size(290, 878);
            this.tabView.TabIndex = 4;
            this.tabView.Text = "View";
            this.tabView.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.checkBox1);
            this.groupBox5.Controls.Add(this.b_lockPanel);
            this.groupBox5.Controls.Add(this.cB_autoShot);
            this.groupBox5.Controls.Add(this.button1);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Location = new System.Drawing.Point(3, 73);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(284, 96);
            this.groupBox5.TabIndex = 36;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Screenshot";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(10, 19);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(115, 17);
            this.checkBox1.TabIndex = 34;
            this.checkBox1.Text = "White Background";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // b_lockPanel
            // 
            this.b_lockPanel.Location = new System.Drawing.Point(161, 39);
            this.b_lockPanel.Name = "b_lockPanel";
            this.b_lockPanel.Size = new System.Drawing.Size(49, 41);
            this.b_lockPanel.TabIndex = 33;
            this.b_lockPanel.Text = "Lock Panel";
            this.b_lockPanel.UseVisualStyleBackColor = true;
            this.b_lockPanel.Click += new System.EventHandler(this.b_lockPanel_Click);
            // 
            // cB_autoShot
            // 
            this.cB_autoShot.AutoSize = true;
            this.cB_autoShot.Location = new System.Drawing.Point(10, 39);
            this.cB_autoShot.Name = "cB_autoShot";
            this.cB_autoShot.Size = new System.Drawing.Size(129, 17);
            this.cB_autoShot.TabIndex = 32;
            this.cB_autoShot.Text = "automatic Screenshot";
            this.cB_autoShot.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 57);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(152, 23);
            this.button1.TabIndex = 27;
            this.button1.Text = "Screenshot";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label10);
            this.groupBox10.Controls.Add(this.uD_lineWidth);
            this.groupBox10.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox10.Location = new System.Drawing.Point(3, 3);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(284, 70);
            this.groupBox10.TabIndex = 35;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Visualization";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(51, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 13);
            this.label10.TabIndex = 36;
            this.label10.Text = "Line Width:";
            // 
            // uD_lineWidth
            // 
            this.uD_lineWidth.Location = new System.Drawing.Point(116, 28);
            this.uD_lineWidth.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.uD_lineWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.uD_lineWidth.Name = "uD_lineWidth";
            this.uD_lineWidth.Size = new System.Drawing.Size(42, 20);
            this.uD_lineWidth.TabIndex = 35;
            this.uD_lineWidth.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // toolStrip3
            // 
            this.toolStrip3.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton4,
            this.toolStripButton5,
            this.B_Analyse,
            this.toolStripSeparator5,
            this.ButtonStreets,
            this.toolStripButton6,
            this.toolStripButton8,
            this.toolStripSeparator1,
            this.toolStripButton_LoadScene,
            this.ButtonBuildingLayout,
            this.Button_StartStop,
            this.Button_IsovistAnalysis,
            this.toolStripButton10,
            this.Button_OptiLayout,
            this.toolStripSeparator4,
            this.toolStripButton7,
            this.toolStripSeparator2,
            this.Button_ScenarioLucy,
            this.Button_IsovistLucy,
            this.toolStripSeparator6,
            this.toolStripButton11,
            this.toolStripButton9,
            this.toolStripSeparator7,
            this.toolStripButton12});
            this.toolStrip3.Location = new System.Drawing.Point(0, 37);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Size = new System.Drawing.Size(102, 922);
            this.toolStrip3.TabIndex = 1;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(99, 19);
            this.toolStripButton4.Text = "Ini.StreetNet";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(99, 19);
            this.toolStripButton5.Text = "GrowNetwork";
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click_1);
            // 
            // B_Analyse
            // 
            this.B_Analyse.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.B_Analyse.Image = ((System.Drawing.Image)(resources.GetObject("B_Analyse.Image")));
            this.B_Analyse.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.B_Analyse.Name = "B_Analyse";
            this.B_Analyse.Size = new System.Drawing.Size(99, 19);
            this.B_Analyse.Text = "Analyse";
            this.B_Analyse.Click += new System.EventHandler(this.B_Analyse_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(99, 6);
            // 
            // ButtonStreets
            // 
            this.ButtonStreets.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ButtonStreets.Image = ((System.Drawing.Image)(resources.GetObject("ButtonStreets.Image")));
            this.ButtonStreets.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButtonStreets.Name = "ButtonStreets";
            this.ButtonStreets.Size = new System.Drawing.Size(99, 19);
            this.ButtonStreets.Text = "StreetNetwork";
            this.ButtonStreets.Click += new System.EventHandler(this.B_TestTree_Click);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(99, 19);
            this.toolStripButton6.Text = "OptiNetwork";
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton8.Image")));
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(99, 19);
            this.toolStripButton8.Text = "Reset";
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(99, 6);
            // 
            // toolStripButton_LoadScene
            // 
            this.toolStripButton_LoadScene.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_LoadScene.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_LoadScene.Image")));
            this.toolStripButton_LoadScene.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_LoadScene.Name = "toolStripButton_LoadScene";
            this.toolStripButton_LoadScene.Size = new System.Drawing.Size(99, 19);
            this.toolStripButton_LoadScene.Text = "LoadScene";
            this.toolStripButton_LoadScene.Click += new System.EventHandler(this.toolStripButton_LoadScene_Click);
            // 
            // ButtonBuildingLayout
            // 
            this.ButtonBuildingLayout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ButtonBuildingLayout.Image = ((System.Drawing.Image)(resources.GetObject("ButtonBuildingLayout.Image")));
            this.ButtonBuildingLayout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ButtonBuildingLayout.Name = "ButtonBuildingLayout";
            this.ButtonBuildingLayout.Size = new System.Drawing.Size(99, 19);
            this.ButtonBuildingLayout.Text = "BuildingLayout";
            this.ButtonBuildingLayout.Click += new System.EventHandler(this.ButtonBuildingLayout_Click);
            // 
            // Button_StartStop
            // 
            this.Button_StartStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.Button_StartStop.Image = ((System.Drawing.Image)(resources.GetObject("Button_StartStop.Image")));
            this.Button_StartStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Button_StartStop.Name = "Button_StartStop";
            this.Button_StartStop.Size = new System.Drawing.Size(99, 19);
            this.Button_StartStop.Text = "Start_Stop";
            this.Button_StartStop.Click += new System.EventHandler(this.Button_StartStop_Click);
            // 
            // Button_IsovistAnalysis
            // 
            this.Button_IsovistAnalysis.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.Button_IsovistAnalysis.Image = ((System.Drawing.Image)(resources.GetObject("Button_IsovistAnalysis.Image")));
            this.Button_IsovistAnalysis.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Button_IsovistAnalysis.Name = "Button_IsovistAnalysis";
            this.Button_IsovistAnalysis.Size = new System.Drawing.Size(99, 19);
            this.Button_IsovistAnalysis.Text = "IsovistAnalysis";
            this.Button_IsovistAnalysis.Click += new System.EventHandler(this.Button_IsovistAnalysis_Click);
            // 
            // toolStripButton10
            // 
            this.toolStripButton10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton10.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton10.Image")));
            this.toolStripButton10.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton10.Name = "toolStripButton10";
            this.toolStripButton10.Size = new System.Drawing.Size(99, 19);
            this.toolStripButton10.Text = "SolarAnalysis";
            this.toolStripButton10.ToolTipText = "SolarAnalysis";
            this.toolStripButton10.Click += new System.EventHandler(this.toolStripButton10_Click);
            // 
            // Button_OptiLayout
            // 
            this.Button_OptiLayout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.Button_OptiLayout.Image = ((System.Drawing.Image)(resources.GetObject("Button_OptiLayout.Image")));
            this.Button_OptiLayout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Button_OptiLayout.Name = "Button_OptiLayout";
            this.Button_OptiLayout.Size = new System.Drawing.Size(99, 19);
            this.Button_OptiLayout.Text = "OptiLayout";
            this.Button_OptiLayout.Click += new System.EventHandler(this.Button_OptiLayout_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(99, 6);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(99, 19);
            this.toolStripButton7.Text = "StopOpti";
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(99, 6);
            // 
            // Button_ScenarioLucy
            // 
            this.Button_ScenarioLucy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.Button_ScenarioLucy.Image = ((System.Drawing.Image)(resources.GetObject("Button_ScenarioLucy.Image")));
            this.Button_ScenarioLucy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Button_ScenarioLucy.Name = "Button_ScenarioLucy";
            this.Button_ScenarioLucy.Size = new System.Drawing.Size(99, 19);
            this.Button_ScenarioLucy.Text = "ScenarioLucy";
            this.Button_ScenarioLucy.Click += new System.EventHandler(this.Button_IsovistALucy_Click);
            // 
            // Button_IsovistLucy
            // 
            this.Button_IsovistLucy.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.Button_IsovistLucy.Image = ((System.Drawing.Image)(resources.GetObject("Button_IsovistLucy.Image")));
            this.Button_IsovistLucy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Button_IsovistLucy.Name = "Button_IsovistLucy";
            this.Button_IsovistLucy.Size = new System.Drawing.Size(99, 19);
            this.Button_IsovistLucy.Text = "IsovistLucy";
            this.Button_IsovistLucy.Click += new System.EventHandler(this.Button_IsovistLucy_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(99, 6);
            // 
            // toolStripButton11
            // 
            this.toolStripButton11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton11.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton11.Image")));
            this.toolStripButton11.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton11.Name = "toolStripButton11";
            this.toolStripButton11.Size = new System.Drawing.Size(99, 19);
            this.toolStripButton11.Text = "AnalyseNetwork";
            this.toolStripButton11.Click += new System.EventHandler(this.toolStripButton11_Click);
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton9.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton9.Image")));
            this.toolStripButton9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.Size = new System.Drawing.Size(99, 19);
            this.toolStripButton9.Text = "testSOMlayout";
            this.toolStripButton9.Click += new System.EventHandler(this.toolStripButton9_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(99, 6);
            // 
            // toolStripButton12
            // 
            this.toolStripButton12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton12.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton12.Image")));
            this.toolStripButton12.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton12.Name = "toolStripButton12";
            this.toolStripButton12.Size = new System.Drawing.Size(99, 19);
            this.toolStripButton12.Text = "newCollisionTest";
            this.toolStripButton12.Click += new System.EventHandler(this.toolStripButton12_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(30, 30);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripSeparator3,
            this.toolStripLabel1,
            this.Button_Move,
            this.Button_Rotate,
            this.Button_Scale,
            this.Button_Size});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1471, 37);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = global::CPlan.UrbanPattern.Properties.Resources.ZoomAll2;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(34, 34);
            this.toolStripButton2.Text = "Zoom all";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::CPlan.UrbanPattern.Properties.Resources.V2D_3D;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(34, 34);
            this.toolStripButton3.Text = "Switch 2D - 3D ";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 37);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(170, 34);
            this.toolStripLabel1.Text = "                 Object manipulation:";
            // 
            // Button_Move
            // 
            this.Button_Move.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Button_Move.Image = ((System.Drawing.Image)(resources.GetObject("Button_Move.Image")));
            this.Button_Move.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Button_Move.Name = "Button_Move";
            this.Button_Move.Size = new System.Drawing.Size(34, 34);
            this.Button_Move.Text = "Move buildings";
            this.Button_Move.Click += new System.EventHandler(this.Button_Move_Click);
            // 
            // Button_Rotate
            // 
            this.Button_Rotate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Button_Rotate.Image = ((System.Drawing.Image)(resources.GetObject("Button_Rotate.Image")));
            this.Button_Rotate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Button_Rotate.Name = "Button_Rotate";
            this.Button_Rotate.Size = new System.Drawing.Size(34, 34);
            this.Button_Rotate.Text = "Rotate buildings";
            this.Button_Rotate.Click += new System.EventHandler(this.Button_Rotate_Click);
            // 
            // Button_Scale
            // 
            this.Button_Scale.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Button_Scale.Image = global::CPlan.UrbanPattern.Properties.Resources.WindowNewOpen;
            this.Button_Scale.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Button_Scale.Name = "Button_Scale";
            this.Button_Scale.Size = new System.Drawing.Size(34, 34);
            this.Button_Scale.Text = "Change building proportion";
            this.Button_Scale.Click += new System.EventHandler(this.Button_Scale_Click_1);
            // 
            // Button_Size
            // 
            this.Button_Size.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Button_Size.Image = global::CPlan.UrbanPattern.Properties.Resources.ScaleToFit;
            this.Button_Size.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Button_Size.Name = "Button_Size";
            this.Button_Size.Size = new System.Drawing.Size(34, 34);
            this.Button_Size.Text = "Change building size";
            this.Button_Size.Click += new System.EventHandler(this.Button_Size_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testToolStripMenuItem,
            this.appSettingsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1471, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exportToolStripMenuItem,
            this.importToolStripMenuItem,
            this.loadScenarioToolStripMenuItem});
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.testToolStripMenuItem.Text = "Datei";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportDxfToolStripMenuItem,
            this.exportLrnToolStripMenuItem});
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.exportToolStripMenuItem.Text = "Export";
            // 
            // exportDxfToolStripMenuItem
            // 
            this.exportDxfToolStripMenuItem.Name = "exportDxfToolStripMenuItem";
            this.exportDxfToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.exportDxfToolStripMenuItem.Text = "Export dxf";
            this.exportDxfToolStripMenuItem.Click += new System.EventHandler(this.exportDxfToolStripMenuItem_Click);
            // 
            // exportLrnToolStripMenuItem
            // 
            this.exportLrnToolStripMenuItem.Name = "exportLrnToolStripMenuItem";
            this.exportLrnToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.exportLrnToolStripMenuItem.Text = "Export lrn";
            this.exportLrnToolStripMenuItem.Click += new System.EventHandler(this.exportLrnToolStripMenuItem_Click);
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importDxfToolStripMenuItem,
            this.importGraphFromDxfToolStripMenuItem,
            this.readBmToolStripMenuItem});
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.importToolStripMenuItem.Text = "Import";
            // 
            // importDxfToolStripMenuItem
            // 
            this.importDxfToolStripMenuItem.Name = "importDxfToolStripMenuItem";
            this.importDxfToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.importDxfToolStripMenuItem.Text = "Import dxf";
            this.importDxfToolStripMenuItem.Click += new System.EventHandler(this.importDxfToolStripMenuItem_Click);
            // 
            // importGraphFromDxfToolStripMenuItem
            // 
            this.importGraphFromDxfToolStripMenuItem.Name = "importGraphFromDxfToolStripMenuItem";
            this.importGraphFromDxfToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.importGraphFromDxfToolStripMenuItem.Text = "Import graph from dxf";
            this.importGraphFromDxfToolStripMenuItem.Click += new System.EventHandler(this.importGraphFromDxfToolStripMenuItem_Click);
            // 
            // readBmToolStripMenuItem
            // 
            this.readBmToolStripMenuItem.Name = "readBmToolStripMenuItem";
            this.readBmToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.readBmToolStripMenuItem.Text = "Read bm";
            this.readBmToolStripMenuItem.Click += new System.EventHandler(this.readBmToolStripMenuItem_Click);
            // 
            // loadScenarioToolStripMenuItem
            // 
            this.loadScenarioToolStripMenuItem.Name = "loadScenarioToolStripMenuItem";
            this.loadScenarioToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.loadScenarioToolStripMenuItem.Text = "LoadScenario";
            this.loadScenarioToolStripMenuItem.Click += new System.EventHandler(this.loadScenarioToolStripMenuItem_Click);
            // 
            // appSettingsToolStripMenuItem
            // 
            this.appSettingsToolStripMenuItem.Name = "appSettingsToolStripMenuItem";
            this.appSettingsToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.appSettingsToolStripMenuItem.Text = "AppSettings";
            this.appSettingsToolStripMenuItem.Click += new System.EventHandler(this.appSettingsToolStripMenuItem_Click);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(16, 17);
            this.toolStripStatusLabel1.Text = "   ";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 16);
            this.toolStripProgressBar1.Visible = false;
            // 
            // dateiToolStripMenuItem
            // 
            this.dateiToolStripMenuItem.Name = "dateiToolStripMenuItem";
            this.dateiToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.dateiToolStripMenuItem.Text = "Datei";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 23);
            // 
            // saveDxfDialog
            // 
            this.saveDxfDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // timer1
            // 
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // openDxfDialog
            // 
            this.openDxfDialog.FileName = "openFileDialog1";
            this.openDxfDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.openDxfDialog_FileOk);
            // 
            // openDxfGraphDialog
            // 
            this.openDxfGraphDialog.FileName = "openFileDialog1";
            this.openDxfGraphDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.openDxfGraphDialog_FileOk);
            // 
            // saveAllDialog
            // 
            this.saveAllDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.saveAllDialog_FileOk);
            // 
            // openAllDialog
            // 
            this.openAllDialog.FileName = "openAllDialog";
            this.openAllDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.openAllDialog_FileOk);
            // 
            // saveExportLrn
            // 
            this.saveExportLrn.FileOk += new System.ComponentModel.CancelEventHandler(this.saveExportLrn_FileOk);
            // 
            // openBmFile
            // 
            this.openBmFile.FileName = "openBmFile";
            this.openBmFile.FileOk += new System.ComponentModel.CancelEventHandler(this.openBmFile_FileOk);
            // 
            // folderBrowserDialog_Scenarios
            // 
            this.folderBrowserDialog_Scenarios.SelectedPath = "C:\\Users\\koenigr\\Programmierung\\ComputationalPlanningGroup\\Various\\InputDataRocho" +
    "r";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1471, 1005);
            this.Controls.Add(this.toolStripContainer1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Layout Synthesis";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ContentPanel.PerformLayout();
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabBuildingLayout.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_floorHeigth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_maxSideAbs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_maxHeigth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_maxAbsBldArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_minSideAbs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_IsovistCellSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_Adapt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_minHeigth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_minAbsBldArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_minSideRatio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_density)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_maxNrBuildings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_minNrBuildings)).EndInit();
            this.tabStreetNetwork.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_treeDepth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_minLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_maxLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_divAngle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_distance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_angle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_connectivity)).EndInit();
            this.tabOptimization.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ESOM)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nUD_distY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUD_distX)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_Mu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_Lambda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_Alpha)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_nrChildren)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_populationSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uD_generations)).EndInit();
            this.tabLucy.ResumeLayout(false);
            this.tabLucy.PerformLayout();
            this.tabView.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uD_lineWidth)).EndInit();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dateiToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton ButtonStreets;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.SaveFileDialog saveDxfDialog;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton ButtonBuildingLayout;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripButton Button_IsovistAnalysis;
        private System.Windows.Forms.ToolStripButton Button_StartStop;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton Button_Move;
        private System.Windows.Forms.ToolStripButton Button_Rotate;
        private System.Windows.Forms.ToolStripButton Button_Size;
        private System.Windows.Forms.ToolStripButton Button_Scale;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.OpenFileDialog openDxfDialog;
        private System.Windows.Forms.ToolStripButton toolStripButton_LoadScene;
        private System.Windows.Forms.OpenFileDialog openDxfGraphDialog;
        private System.Windows.Forms.ToolStripButton B_Analyse;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton Button_OptiLayout;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveAllDialog;
        private System.Windows.Forms.OpenFileDialog openAllDialog;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportDxfToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportLrnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importDxfToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importGraphFromDxfToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readBmToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton9;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar2;
        private System.Windows.Forms.SaveFileDialog saveExportLrn;
        private System.Windows.Forms.OpenFileDialog openBmFile;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabBuildingLayout;
        private System.Windows.Forms.PropertyGrid propertyGrid3;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.PropertyGrid propertyGrid2;
        private System.Windows.Forms.ComboBox comboBox_Isovist;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ComboBox comboBox_scenario;
        protected internal System.Windows.Forms.NumericUpDown uD_minHeigth;
        private System.Windows.Forms.Label label22;
        protected internal System.Windows.Forms.NumericUpDown uD_minAbsBldArea;
        private System.Windows.Forms.Label label21;
        protected internal System.Windows.Forms.NumericUpDown uD_minSideRatio;
        private System.Windows.Forms.Label label20;
        protected internal System.Windows.Forms.NumericUpDown uD_density;
        private System.Windows.Forms.Label label18;
        protected internal System.Windows.Forms.NumericUpDown uD_maxNrBuildings;
        private System.Windows.Forms.Label label19;
        protected internal System.Windows.Forms.NumericUpDown uD_minNrBuildings;
        private System.Windows.Forms.TabPage tabStreetNetwork;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rB_fitCentral;
        private System.Windows.Forms.RadioButton rB_fitChoice;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        protected internal System.Windows.Forms.NumericUpDown uD_treeDepth;
        private System.Windows.Forms.Label label6;
        protected internal System.Windows.Forms.NumericUpDown uD_minLength;
        private System.Windows.Forms.Label label5;
        protected internal System.Windows.Forms.NumericUpDown uD_maxLength;
        private System.Windows.Forms.Label label4;
        protected internal System.Windows.Forms.NumericUpDown uD_divAngle;
        private System.Windows.Forms.Label label3;
        protected internal System.Windows.Forms.NumericUpDown uD_distance;
        private System.Windows.Forms.Label label2;
        protected internal System.Windows.Forms.NumericUpDown uD_angle;
        private System.Windows.Forms.Label label1;
        protected internal System.Windows.Forms.NumericUpDown uD_connectivity;
        private System.Windows.Forms.TabPage tabOptimization;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label17;
        protected internal System.Windows.Forms.NumericUpDown nUD_distY;
        private System.Windows.Forms.Label label13;
        protected internal System.Windows.Forms.NumericUpDown nUD_distX;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button b_lockPanel;
        private System.Windows.Forms.CheckBox cB_autoShot;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label lbl_Generation;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        protected internal System.Windows.Forms.NumericUpDown uD_Mu;
        private System.Windows.Forms.Label label15;
        protected internal System.Windows.Forms.NumericUpDown uD_Lambda;
        private System.Windows.Forms.Label label16;
        protected internal System.Windows.Forms.NumericUpDown uD_Alpha;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label12;
        protected internal System.Windows.Forms.NumericUpDown uD_nrChildren;
        private System.Windows.Forms.RadioButton sB_multiOpti;
        private System.Windows.Forms.RadioButton sB_singleOpti;
        private System.Windows.Forms.Label label8;
        protected internal System.Windows.Forms.NumericUpDown uD_populationSize;
        private System.Windows.Forms.Label label7;
        protected internal System.Windows.Forms.NumericUpDown uD_generations;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label24;
        protected internal System.Windows.Forms.NumericUpDown uD_Adapt;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton cb_showSOMorder;
        private System.Windows.Forms.RadioButton cb_showBestGen;
        private System.Windows.Forms.RadioButton cb_showAll;
        private System.Windows.Forms.Button b_ESOM;
        private System.Windows.Forms.Button b_ESOMrender;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label10;
        protected internal System.Windows.Forms.NumericUpDown uD_lineWidth;
        private System.Windows.Forms.ToolStripMenuItem appSettingsToolStripMenuItem;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.PictureBox pictureBox_ESOM;
        private System.Windows.Forms.TabPage tabLucy;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox messageBox;
        private System.Windows.Forms.ToolStripButton Button_ScenarioLucy;
        private System.Windows.Forms.ToolStripButton Button_IsovistLucy;
        private System.Windows.Forms.Label label25;
        protected internal System.Windows.Forms.NumericUpDown uD_IsovistCellSize;
        private System.Windows.Forms.CheckBox cB_showSomMap;
        private System.Windows.Forms.Label label26;
        protected internal System.Windows.Forms.NumericUpDown uD_minSideAbs;
        private System.Windows.Forms.Label label27;
        protected internal System.Windows.Forms.NumericUpDown uD_maxAbsBldArea;
        private System.Windows.Forms.ToolStripButton toolStripButton10;
        private System.Windows.Forms.ToolStripMenuItem loadScenarioToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog_Scenarios;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton toolStripButton11;
        private System.Windows.Forms.CheckBox cB_realUpdate;
        private System.Windows.Forms.CheckBox cB_layoutAll;
        private System.Windows.Forms.Label label28;
        protected internal System.Windows.Forms.NumericUpDown uD_maxHeigth;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.CheckBox cB_updateLucy;
        private System.Windows.Forms.CheckBox cB_optiAll;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.Label label29;
        protected internal System.Windows.Forms.NumericUpDown uD_maxSideAbs;
        private System.Windows.Forms.Label label30;
        protected internal System.Windows.Forms.NumericUpDown uD_floorHeigth;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton toolStripButton12;
        private VisualObjects.GLCameraControl glCameraMainWindow;
        private VisualObjects.GLCameraControl glCameraNetwork;
        private System.Windows.Forms.Splitter splitter4;
        private VisualObjects.GLCameraControl glCameraLayouts;
        public System.Windows.Forms.ProgressBar progressOpti;
        public System.Windows.Forms.ProgressBar progressLoops;
        private System.Windows.Forms.TabPage tabView;
    }
}


//#endif
﻿#region CopyrightAndLicense

// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Form1.cs 
//  Copyright (C) 2014/10/18  7:51 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Zeng Wei, Christian Tonn
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using AForge.Genetic;
using CPlan.Evaluation;
using CPlan.Geometry;
using CPlan.Optimization;
using CPlan.UrbanPattern.Properties;
using CPlan.VisualObjects;
using FW_GPU_NET;
using GeoAPI.Geometries;
using netDxf;
using netDxf.Entities;
using netDxf.Header;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using NetTopologySuite.Noding.Snapround;
using NetTopologySuite.Operation.Polygonize;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using QuickGraph;
using Tektosyne.Geometry;
using Newtonsoft.Json.Linq;
using NetTopologySuite.Features;
using CPlan.CommunicateToLuci;
using TimeSpan = System.TimeSpan;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public enum AnalysisType
    {
        Isovist,
        Solar
    };

    public partial class Form1 : Form
    {
        //===================================================================================================================================================================
        #region Fields
        //
        // BuildingLayout
        //
        private ChromosomeLayout _activeLayout;
        private ChromosomeMultiLayout _activeMultiLayout;
        private List<ChromosomeLayout> _allLayouts = new List<ChromosomeLayout>();
        private List<Poly2D> _blocks;
        private GPolygon _borderPoly;

        private CalculatorSolarAnalysis _calculatorSolarAnalysis;

        private readonly Scenario _scenario = new Scenario();
        private List<MultiPisaChromosomeBase> _SOMOrderedArchive;
        private AnalysisType _analysisType = AnalysisType.Isovist;
        private BackgroundWorker _backWorker;
        private Poly2D _border;
        private ObjectsDrawList _constantDrawObjects;
        private List<List<double>> _fitnesLists = new List<List<double>>();
        private List<MultiPisaChromosomeBase> _globalArchive;

        private UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> _initailNet;
        private Poly2D _isovistBorder;
        private int _iterations = 100;
        private int _maxEdges = 4;
        private int _maxLength = 100;
        private double _minAngle = 80;
        private double _minDist = 20;
        private int _minLength = 30;

        private volatile bool _needToStop1;
        private ViewControlParameters _parameters = new ViewControlParameters();
        private MultiPopulation _population;
        private PopulationParameters _populationParameters;
        private Population _populationSingle;
        private int _populationSize = 40;
        private List<GeoObject> _remainDrawingObjects = new List<GeoObject>();
        private int _rndAngle = 10;
        private GGrid _smartGrid;
        //private ScalarField _terrainGrid;
        private int[,] _somOrder;
        private TimeSpan _span;
        private StreetPattern _streetGenerator;
        private Thread _workerThread;
        private bool rotSymmetryL = false;
        private bool rotSymmetryR = false;

        // --- value List --
        public static String MouseMode { get; set; }
        //private Scenario _scenarioCapeTown = new Scenario();
        //private MqttClient mqttClient;
        //private ConfigSettings _configInfo;

        private List<List<double>> _fitnessValues;
        private InstructionTreePisa _activeNetwork;
        private List<InstructionTreePisa> _allNetworks = new List<InstructionTreePisa>();
        private int _curScenarioID = -1;

        private string _globalLrn;
        private IsovistAnalysis _isovistField;

        private List<PoissonValues> _allVarianceValues = new List<PoissonValues>();

        public CalculateVia RunVia { get; set; }

        private VisualObjects.GLCameraControl _activeCameraControl;

        private struct Mesh
        {
            public Vector3d Normal;
            public Vector3d[] Vertice;
        }

        # endregion


        //===================================================================================================================================================================
        #region Constructor

        public Form1()
        {
            InitializeComponent();

            /* *ac*
            LucyManager.Create(this);
            LucyManager.Manager.ShowLucyDisconnected();
            */

            MouseMode = "ScreenMove";
            RunVia = new CalculateVia();

            // ----------------------------------------------------------------------------------------------------
            //glCameraMainWindow.SwapBuffersActivated = false;
            //glCameraMainWindow.Paint += glControl1_Paint;
            glCameraMainWindow.MouseMove += glControl1_MouseMove;
            glCameraMainWindow.MouseDown += glControl1_MouseDown;
            glCameraMainWindow.MouseUp += glControl1_MouseUp;
            glCameraMainWindow.Resize += glControls_Resize;

            // ----------------------------------------------------------------------------------------------------
            glCameraLayouts.MouseUp += glControl2_MouseUp;
            glCameraLayouts.Resize += glControls_Resize;
            //// ----------------------------------------------------------------------------------------------------
            glCameraNetwork.MouseUp += glControl3_MouseUp;
            glCameraNetwork.Resize += glControls_Resize;

            // --- zoom all ---
            glCameraMainWindow.ZoomAll();
            glCameraLayouts.ZoomAll();
            glCameraNetwork.ZoomAll();
            SetScenarioEnvironment();
            SetControlParameters();

            // -- chart
            //  chart1.ChartAreas[0].AxisX.Maximum = 1.0;// 100000;// 1.0;// .3;
            //  chart1.ChartAreas[0].AxisY.Maximum = 1.0;// 100;// .3;
            //chart1.ChartAreas[0].AxisX.MinorGrid.Enabled = false;
            //chart1.ChartAreas[0].AxisX.MajorGrid.Interval = .1;// 1.0 / 32;
            //chart1.ChartAreas[0].AxisY.MinorGrid.Enabled = false;
            //chart1.ChartAreas[0].AxisY.MajorGrid.Interval = .1;//1.0 / 32;
            chart1.Series[1].MarkerSize = 3;
            chart1.Series[0].MarkerSize = 6;

            chart1.Series[0].Points.Clear();

            propertyGrid3.SelectedObject = _parameters;
            _globalArchive = new List<MultiPisaChromosomeBase>();

            // --- terminate running PISA processes ---
            // first, test if there is no running process - otherwise the new one crashes. 
            // use the corresponding name for the selector used in the next line. 
            // for the process name look into MultiPopulation constructor.
            TerminateProcess("hype"); //("spea2");
            TerminateProcess("spea2");

            if (Settings.Default.Width > 0)
            {
                this.Width = Settings.Default.Width;
                this.splitContainer2.SplitterDistance = (int)(0.5 * Width - 110);
            }

            if (Settings.Default.Height > 0)
                this.Height = Settings.Default.Height;
        }
        
        #endregion


        //===================================================================================================================================================================
        #region FormEvents

        //===================================================================================================================================================================
        private void Form1_Load(object sender, EventArgs e)
        {
            _backWorker = new BackgroundWorker();
            _backWorker.WorkerReportsProgress = true;
            _backWorker.WorkerSupportsCancellation = true;

            _backWorker.DoWork += backWorker_DoWork;
            // backWorker.ProgressChanged += new ProgressChangedEventHandler(backWorker_ProgressChanged);
            _backWorker.RunWorkerCompleted += backWorker_RunWorkerCompleted;

            PisaWrapper.PISA_DIR = Settings.Default.PathToPISA;
        }

        //============================================================================================================================================================================
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // check if worker thread is running
            if ((_workerThread != null) && (_workerThread.IsAlive))
            {
                _needToStop1 = true;
                while (!_workerThread.Join(100))
                    Application.DoEvents();
            }
            if (_population != null)
                if (_population.Sel != null)
                    _population.Sel.Terminate();
            PisaWrapper.TerminateAll();
        }

        #endregion


        //===================================================================================================================================================================
        //========= Various Methods =========
        //===================================================================================================================================================================
        #region VariousMethods

        //===================================================================================================================================================================
        /// <summary>
        /// Terminate a running process.
        /// </summary>
        /// <param name="processName">Name of the process.</param>
        private void TerminateProcess(string processName)
        {
            Process[] matchingProcesses = Process.GetProcessesByName(processName);

            if (matchingProcesses.Length > 0)
            {
                foreach (Process p in matchingProcesses)
                {
                    p.CloseMainWindow();
                }

                Thread.Sleep(500);

                matchingProcesses = Process.GetProcessesByName(processName);

                foreach (Process p in matchingProcesses)
                {
                    if (p.HasExited == false)
                        p.Kill();
                }
            }
        }

        //============================================================================================================================================================================
        public List<string> LayoutFingerprintAnalysisPoints()
        {
            var lines = new List<string>();
            List<MultiPisaChromosomeBase> archive = _population.Sel.GetArchiveOrig();
            if (archive == null) return null;

            // -- write file ---------------------------------------------------------
            // -----------------------------------------------------------------------
            // -- file header --------------------------------------------------------
            lines.Add("# Layout-Fingerprints (IDs) from CPlan | Generation:");
            lines.Add("# " + _population.EpochCounter);
            //          lines.Add("# " + ((ChromosomeLayout)archive[0]).IsovistField.CountX.ToString() + " = length of one line");
            //lines.Add("# " + "Avg: ");
            //lines.Add("# " + "StdDev: ");
            //lines.Add("# " + "Min: ");
            //lines.Add("# " + "Max: ");
            lines.Add("% " + archive.Count());
            lines.Add("% " + "2");
            // -- add the column-feature names and type of column --
            string type = "% 9";
            string name = "% ID";
            int entires = ((ChromosomeLayout)archive[0]).IsovistField.FingerprintAnalysisPoints.Count;
            for (int k = 0; k < entires; k++)
            {
                type += "\t" + "1";
                name += "\t" + "feature" + (k + 1);
            }
            lines.Add(type);
            lines.Add(name);
            // -- add the finterprint features ---------------------
            for (int m = 0; m < archive.Count(); m++)
            {
                var curChromo = (ChromosomeLayout)archive[m];
                string lineID = curChromo.Indentity.ToString();
                for (int j = 0; j < curChromo.IsovistField.FingerprintAnalysisPoints.Count; j++)
                {
                    if (curChromo.IsovistField.FingerprintAnalysisPoints[j] == 0)
                    {
                        lineID += "\t0";
                    }
                    else
                    {
                        lineID += "\t1";
                    }
                }
                lines.Add(lineID);
            }
            return lines;
        }

        //============================================================================================================================================================================
        public void VarianceAnalysis(int iterat)
        {
            if (iterat == 0) _allVarianceValues = new List<PoissonValues>();

            var fingerPrintsArchive = new List<float[]>();
            List<MultiPisaChromosomeBase> archive = _population.Sel.GetArchiveOrig();
            ChromosomeLayout curChromo;
            for (int m = 0; m < archive.Count(); m++)
            {
                curChromo = (ChromosomeLayout)archive[m];
                fingerPrintsArchive.Add(curChromo.IsovistField.FingerprintGrid.ToArray());
            }

            curChromo = (ChromosomeLayout)archive[0];
            int nrCellsinLine = curChromo.IsovistField.FingerprintGridLineLgth;

            //TextRW.PoissonValues curVarianceList = PoissonAnalysis(FingerPrintsArchive, nrCellsinLine);
            var stat = new Statistics(fingerPrintsArchive, nrCellsinLine); // construct statistics
            stat.Calc();

            var curVarianceList = new PoissonValues();
            curVarianceList.Range = stat.DELTA_RANGE;
            curVarianceList.Sum = stat.DELTA_SUM;
            curVarianceList.AvgDeviation = stat.DELTA_AVG;
            curVarianceList.Lambda = stat.POISSON_LAMBDA;
            curVarianceList.Error = stat.POISSON_ERROR;

            _allVarianceValues.Add(curVarianceList);
            Geometry.Tools.WritePoissonValues(_allVarianceValues);
        }

        //============================================================================================================================================================================
        private GenBuildingLayout ConvertGPolygonToBuildingGen(GPolygon gPoly, int id = 0)
        {
            var buildPoly = new Poly2D(gPoly.Poly);
            double angle = GeoAlgorithms2D.AngleBetween(gPoly.PolyPoints[1],
                new Vector2d(gPoly.PolyPoints[0].X, gPoly.PolyPoints[0].Y + 100), gPoly.PolyPoints[0]);
            buildPoly.Rotate(angle);

            // -- find bottom left --
            double smallest = double.MaxValue;
            int smallestId = 1;
            for (int i = 0; i < buildPoly.PointsFirstLoop.Count(); i++)
            {
                double length = buildPoly.PointsFirstLoop[i].Length;
                if (length < smallest)
                {
                    smallest = length;
                    smallestId = i;
                }
            }

            // -- find top right --
            double lagest = double.MinValue;
            int lagestId = 3;
            for (int i = 0; i < buildPoly.PointsFirstLoop.Count(); i++)
            {
                double length = buildPoly.PointsFirstLoop[i].Length;
                if (length > lagest)
                {
                    lagest = length;
                    lagestId = i;
                }
            }

            double height = buildPoly.PointsFirstLoop[lagestId].Y - buildPoly.PointsFirstLoop[smallestId].Y;

            var topLeft = buildPoly.PointsFirstLoop[smallestId];
            topLeft.Y += height;
            var bottomRight = buildPoly.PointsFirstLoop[lagestId];
            bottomRight.Y -= height;

            var buildRect = new Rect2D(topLeft, bottomRight);
            var buildGen = new GenBuildingLayout(buildRect, GeoAlgorithms2D.RadianToDegree(-angle), 30, id, null);
            return buildGen;
        }

        //============================================================================================================================================================================
        void NetworkToChromosome(UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> baseGraph, out InstructionTreePisa treeFromGraph, out UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> iniNet)
        {
            // -- Analyse the street network
            treeFromGraph = new InstructionTreePisa();
            iniNet = new UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>();
            if (baseGraph == null) return;

            UndirectedEdge<Vector2d>[] linesSet = baseGraph.Edges.ToArray();
            UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> workinGraph = new UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>();
            workinGraph.AddVerticesAndEdgeRange(baseGraph.Edges);

            // -- Find the starting segment/nodes
            // -- use the nearest segment to the centre
            Vector2d sum = new Vector2d(0, 0);

            foreach (UndirectedEdge<Vector2d> line in linesSet)
            {
                var newLine = new Line2D(line.Source, line.Target);
                sum += newLine.Center;
            }
            Vector2d globalCenter = sum / linesSet.Count();

            // -- find nearest segment to ceter
            double dist;
            List<Line2D> lines2DSet = linesSet.Select(line => new Line2D(line.Source, line.Target)).ToList();
            Line2D centerLine = GeoAlgorithms2D.NearestLineToPoint(lines2DSet.ToArray(), globalCenter, out dist);

            // -- initialize the new IntructionTree --
            UndirectedEdge<Vector2d> iniEdge = new UndirectedEdge<Vector2d>(centerLine.Start, centerLine.End);
            List<InstructionNode> openEnds = new List<InstructionNode>();
            List<InstructionNode> newOpenEnds = new List<InstructionNode>();

            iniNet.AddVerticesAndEdge(iniEdge);
            treeFromGraph.InitalNetwork = iniNet;
            int idx = 0;
            // -- add the first seed edge(s) --
            foreach (UndirectedEdge<Vector2d> edge in iniNet.Edges)
            {
                InstructionNode seedNodeA = new InstructionNode(idx++);
                InstructionNode seedNodeB = new InstructionNode(idx++);
                seedNodeA.Position = edge.Source;
                seedNodeB.Position = edge.Target;

                seedNodeA.Angle = 0;//angle;
                seedNodeA.Length = 0;//length;
                seedNodeA.MaxConnectivity = 4;
                seedNodeA.ParentIndex = seedNodeB.Index;
                treeFromGraph.Nodes.Add(seedNodeA);
                openEnds.Add(seedNodeA);

                seedNodeB.Angle = 0;//angle;
                seedNodeB.Length = 0;//length;
                seedNodeB.MaxConnectivity = 4;
                seedNodeB.ParentIndex = seedNodeA.Index;
                treeFromGraph.Nodes.Add(seedNodeB);
                openEnds.Add(seedNodeB);
            }

            // -- remove the already added lines from the graph
            UndirectedEdge<Vector2d> removeCandA = new UndirectedEdge<Vector2d>(iniEdge.Source, iniEdge.Target);
            UndirectedEdge<Vector2d> removeCandB = new UndirectedEdge<Vector2d>(iniEdge.Target, iniEdge.Source);
            workinGraph.RemoveEdge(removeCandA);
            workinGraph.RemoveEdge(removeCandB);

            // |<--- walk from starting segment to neighbours (recursive for all openEnds, similar to FindBranch) --->|
            while (openEnds.Count() > 0)
            {
                // -- look at all current openEnds --
                foreach (InstructionNode curOpenNode in openEnds)
                {
                    UndirectedEdge<Vector2d>[] openEdges = workinGraph.AdjacentEdges(curOpenNode.Position).ToArray();

                    // -- read the parameters angle and distance, add max connectivity
                    foreach (UndirectedEdge<Vector2d> testEdge in openEdges)
                    {
                        // -- which of the two end points of the edge is the new point? --
                        Vector2d newPoint;
                        if (curOpenNode.Position == testEdge.Source) newPoint = testEdge.Target;
                        else if (curOpenNode.Position == testEdge.Target) newPoint = testEdge.Source;
                        else
                            break;

                        // -- test if there is already an istruction node for this position --
                        InstructionNode testNode =
                            treeFromGraph.Nodes.Find(instructionNode => instructionNode.Position == newPoint);
                        if (testNode != null)
                            continue;

                        // -- find the parent node and calculate the parameters for the new node --
                        InstructionNode nodeParent =
                            treeFromGraph.Nodes.Find(instructionNode => instructionNode.Index == curOpenNode.ParentIndex);
                        InstructionNode newNode = new InstructionNode(idx++);
                        newNode.Position = newPoint;
                        float angle = Convert.ToSingle(GeoAlgorithms2D.AngleBetween(nodeParent.Position, newNode.Position, curOpenNode.Position));
                        float length = Convert.ToSingle(GeoAlgorithms2D.DistancePoints(testEdge.Source, testEdge.Target));
                        //GeoAlgorithms2D.CartesianToPolar(curNode.Position2d, out angle, out length);
                        newNode.Angle = angle;
                        newNode.Length = length;
                        newNode.MaxConnectivity = 4;
                        newNode.ParentIndex = curOpenNode.Index;

                        // -- add new nodes to the InstructionTree
                        treeFromGraph.Nodes.Add(newNode);
                        workinGraph.RemoveEdgeDouble(testEdge);

                        newOpenEnds.Add(newNode);
                    }
                }
                openEnds.Clear();
                openEnds.AddRange(newOpenEnds);
                newOpenEnds.Clear();
            }
            // -- save chromosome
        }

        //============================================================================================================================================================================
        private void SOMorderProcess()
        {
            //string iniPath = Application.StartupPath;
            string name = "SOM_" + DateTime.Now.Day.ToString("00") + "-" +
                          DateTime.Now.Month.ToString("00") + "_" +
                          DateTime.Now.Hour.ToString("00") +
                          DateTime.Now.Minute.ToString("00") +
                          DateTime.Now.Second.ToString("00");
            _globalLrn = name + ".lrn";

            //string path = Path.Combine(Environment.CurrentDirectory, _globalLrn);
            //TextRW.WriteLrn(_population, path);
            string OutputPath = Settings.Default.PathToOutput;
            string LRNpath = Path.Combine(OutputPath, _globalLrn);
            string resultPath = Path.Combine(OutputPath, name);
            TextRW.WriteLrn(_population, LRNpath);

            //ExecuteCommandSync(OutputPath.Substring(0, 2));
            ExecuteCommandSync("cd " + OutputPath);
/*
            // --- Databionic ESOM ------------------------------------------------------------------------
            string strCmdText;
            string ESOMpath = Settings.Default.PathToESOM;
            // -- esom training - lrn
            strCmdText = ESOMpath + "esomtrn.bat"; //
            // --------------
            // -- parameters:
            // -- path to the lrn file
            //            strCmdText += " -l " + _globalLrn;
            strCmdText += " -l " + LRNpath;
            // -- number of training epochs
            strCmdText += " -e 100";
            // -- execute the command
            ExecuteCommandSync(strCmdText);

            //img.Save(path, System.Drawing.Imaging.ImageFormat.Png);
            //MessageBox.Show(screenshotFilename);
            */
            // --- Somoclu ------------------------------------------------------------------------
            string strCmdText;
            string SomocluPath = Settings.Default.PathToSomoclu;
            // -- esom training - lrn
            strCmdText = SomocluPath + "somoclu.exe "; //
            // --------------
            // -- parameters:
            // -- path to the lrn file
            //            strCmdText += " -l " + _globalLrn;
            strCmdText += LRNpath + " " + resultPath;//_globalLrn + " " + resultPath;
            // -- number of training epochs
            strCmdText += " -e 100";
            // -- execute the command
            ExecuteCommandSync(strCmdText);
            
            // --------------------------------------------------------------------------------------------
            _somOrder = TextRW.ReadBm(OutputPath + name + ".bm");

            _SOMOrderedArchive = _population.Sel.GetArchiveOrig();
        }

        //====================================================================================================
        /// Executes a shell command synchronously.
        /// Code from: http://www.codeproject.com/Articles/25983/How-to-Execute-a-Command-in-C
        public void ExecuteCommandSync(object command)
        {
            try
            {
                // create the ProcessStartInfo using "cmd" as the program to be run,
                // and "/c " as the parameters.
                // Incidentally, /c tells cmd that we want it to execute the command that follows,
                // and then exit.
                var procStartInfo =
                    new ProcessStartInfo("cmd", "/c " + command);

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo.CreateNoWindow = true;
                // Now we create a process, assign its ProcessStartInfo and start it
                var proc = new Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
                // Get the output into a string
                string result = proc.StandardOutput.ReadToEnd();
                // Display the command output.
                Console.WriteLine(result);
            }
            catch (Exception objException)
            {
                // Log the exception
            }
        }

        //============================================================================================================================================================================
        private void LucyConnectClick(object sender, EventArgs e)
        {
            /* *ac*
            if (!LucyManager.Manager.Connected)
                LucyManager.Manager.ConnectToLucy();
            else
                LucyManager.Manager.DisconnectFromLucy();
             */

            ////_cl = new Communication2Lucy();
            GlobalConnection.CL = new Communication2Lucy();

            string resultT = "Connection to Luci was ";

            if (!GlobalConnection.CL.connect(GlobalConnection.IP, GlobalConnection.Port))//("129.132.6.4", 7654)) // 192.168.2.100
                resultT += "not ";

            resultT += "successful! \r\n";
            Console.WriteLine(resultT);
            LucyConsoleLogBox.Text += resultT;

            //// --- connect --- 
            //// --- use lukas & 1234 ---
            if (GlobalConnection.CL == null)
            {
                InfoNoConnection();
            }
            else
            {
                //if (pwBox != null && pwBox is PasswordBox)
                {
                    //var passwordBox = (PasswordBox)param;
                    //JProperty result = GlobalConnection.CL.authenticate("lukas", "1234");
                    JProperty result = GlobalConnection.CL.authenticate(LucyUserNameTextBox.Text, (LucyUserPasswordTextBox).Text);
                    LucyConsoleLogBox.Text += (result.Name + ": " + result.Value + "\r\n");
                    ServiceManager.Init(GlobalConnection.IP);

                    LucyUseServicesCheckBox.Checked = true;
                }
            }

        }

        //============================================================================================================================================================================
        private void CreateIsovistLucyScenario(List<Line2D> edges, List<Poly2D> blocks, List<Poly2D> buildings, Poly2D boundary, List<Vector2d> analysisPoints, List<GPrism> buildings3D)
        {
            if (GlobalConnection.CL == null)
            {
                InfoNoConnection();
                return;
            }
            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
            // --- features collect all geometries + layer info ---
            System.Collections.ObjectModel.Collection<IFeature> features = new System.Collections.ObjectModel.Collection<IFeature>();
            // --- featurecollection is used to send the features via GeoJSON --- 
            NetTopologySuite.Features.FeatureCollection featurecollection;

            // --- streets ---
            if (edges != null && edges.Count > 0)
            {
                AttributesTable streetAttr = new AttributesTable();
                streetAttr.AddAttribute("layer", "street");
                foreach (Line2D edge in edges)
                {
                    LineString lNet = new NetTopologySuite.Geometries.LineString(GeometryConverter.ToCoordinates(edge));
                    features.Add(new Feature(lNet, streetAttr));
                }
            }

            // --- blocks ---
            if (blocks != null && blocks.Count > 0)
            {
                AttributesTable blockAttr = new AttributesTable();
                blockAttr.AddAttribute("layer", "parcel");
                foreach (Poly2D poly in blocks)
                {
                    LinearRing block = new NetTopologySuite.Geometries.LinearRing(GeometryConverter.ToCoordinates(poly));
                    NetTopologySuite.Geometries.Polygon blockNet = new NetTopologySuite.Geometries.Polygon(block);
                    features.Add(new Feature(blockNet, blockAttr));
                }
            }

            // --- buildings2D ---
            if (buildings != null && buildings.Count > 0)
            {
                AttributesTable buildingAttr = new AttributesTable();
                buildingAttr.AddAttribute("layer", "building");
                foreach (Poly2D build in buildings)
                {
                    if (!double.IsNaN(build.Area))
                    {
                        LinearRing bld = new NetTopologySuite.Geometries.LinearRing(GeometryConverter.ToCoordinates(build));
                        NetTopologySuite.Geometries.Polygon bldNet = new NetTopologySuite.Geometries.Polygon(bld);
                        features.Add(new Feature(bldNet, buildingAttr));
                    }
                }
            }

            // --- buildings3D ---
            if (buildings3D != null && buildings3D.Count > 0)
            {
                AttributesTable building3DAttr = new AttributesTable();
                building3DAttr.AddAttribute("layer", "building3D");
                foreach (GPrism build3D in buildings3D)
                {
                    if (!double.IsNaN(build3D.GetAreaBaseSurface()))
                    {
                        Vector2d[] vertices = build3D.Poly.PointsFirstLoop;
                        double dZ = 0;
                        Coordinate[] coord = new Coordinate[5];
                        NetTopologySuite.Geometries.Polygon[] bldNet = new NetTopologySuite.Geometries.Polygon[6];

                        for (int i = 0; i < vertices.Length; i++)
                        {
                            Vector2d next = vertices[i];// +offset;
                            int j = i + 1;
                            if (j >= vertices.Length) j -= vertices.Length;
                            Vector2d next2 = vertices[j];

                            coord = new Coordinate[5];
                            coord[0] = new Coordinate(next.X, next.Y, dZ);
                            coord[1] = new Coordinate(next.X, next.Y, build3D.Height);
                            coord[2] = new Coordinate(next2.X, next2.Y, build3D.Height);
                            coord[3] = new Coordinate(next2.X, next2.Y, dZ);
                            coord[4] = new Coordinate(next.X, next.Y, dZ);
                            LinearRing bld = new NetTopologySuite.Geometries.LinearRing(coord);
                            bldNet[i] = new NetTopologySuite.Geometries.Polygon(bld);
                        }

                        coord = new Coordinate[5];
                        coord[0] = new Coordinate(vertices[0].X, vertices[0].Y, dZ);
                        coord[1] = new Coordinate(vertices[1].X, vertices[1].Y, dZ);
                        coord[2] = new Coordinate(vertices[2].X, vertices[2].Y, dZ);
                        coord[3] = new Coordinate(vertices[3].X, vertices[3].Y, dZ);
                        coord[4] = new Coordinate(vertices[0].X, vertices[0].Y, dZ);
                        LinearRing bldb = new NetTopologySuite.Geometries.LinearRing(coord);
                        bldNet[4] = new NetTopologySuite.Geometries.Polygon(bldb);

                        coord = new Coordinate[5];
                        coord[0] = new Coordinate(vertices[0].X, vertices[0].Y, build3D.Height);
                        coord[1] = new Coordinate(vertices[1].X, vertices[1].Y, build3D.Height);
                        coord[2] = new Coordinate(vertices[2].X, vertices[2].Y, build3D.Height);
                        coord[3] = new Coordinate(vertices[3].X, vertices[3].Y, build3D.Height);
                        coord[4] = new Coordinate(vertices[0].X, vertices[0].Y, build3D.Height);
                        bldb = new NetTopologySuite.Geometries.LinearRing(coord);
                        bldNet[5] = new NetTopologySuite.Geometries.Polygon(bldb);

                        MultiPolygon buid3D = new MultiPolygon(bldNet);
                        features.Add(new Feature(buid3D, building3DAttr));
                    }
                }
            }

            // --- boundary ---
            if (boundary != null && boundary.PointsFirstLoop != null)
            {
                if (!double.IsNaN(boundary.Area))
                {
                    AttributesTable boundaryAttr = new AttributesTable();
                    boundaryAttr.AddAttribute("layer", "boundary");
                    LinearRing bound1 = new NetTopologySuite.Geometries.LinearRing(GeometryConverter.ToCoordinates(boundary));
                    NetTopologySuite.Geometries.Polygon boundNet = new NetTopologySuite.Geometries.Polygon(bound1);
                    features.Add(new Feature(boundNet, boundaryAttr));
                }
            }

            // --- analysisPoints ---
            if (analysisPoints != null)
            {
                if (analysisPoints.Count > 0)
                {
                    AttributesTable pointAttr = new AttributesTable();
                    pointAttr.AddAttribute("layer", "isovistPoints");
                    List<NetTopologySuite.Geometries.Point> pointArray = new List<NetTopologySuite.Geometries.Point>();
                    int cter = 0;
                    foreach (Vector2d point in analysisPoints)
                    {
                        NetTopologySuite.Geometries.Point pt = new NetTopologySuite.Geometries.Point(GeometryConverter.ToCoordinates(point));
                        if (pt != null)
                            pointArray.Add(pt);
                        cter++;
                    }
                    NetTopologySuite.Geometries.MultiPoint multiPts = new MultiPoint(pointArray.ToArray());
                    features.Add(new Feature(multiPts, pointAttr));
                }
            }

            // --- pack all together ---
            featurecollection = new FeatureCollection(features);
            string geoJSONPoly = GeoJSONWriter.Write(featurecollection);

            geoJSONPoly = geoJSONPoly.Substring(0, geoJSONPoly.Length - 1);
            //string createScenario4Geo = "{'action':'create', 'scenario':{'name':'test', 'boundary':{}, 'inputs':" + GeoJSONWriter.Write(mPoly) + "}}}}}";
            //string createScenario4Geo = "{'action':'create','scenario':{'boundary':{},'name':'test','inputs':{'geometry':{'GeoJSON':" + geoJSONPoly + ",'crs':{'type':'name','properties':{'name':'EPSG:21781'}}}}}}}}";
            string createScenario4Geo = "{'action':'create_scenario','name':'test','geometry':{'format':'GeoJSON','value':" + geoJSONPoly + "}}}}";
            //string createScenario4Geo = "{'action':'create','scenario':{'boundary':{},'name':'test','inputs':{'geometry':{'GeoJSON':" + geoJSONPoly + ",'crs':{'type':'name','properties':{'name':'EPSG:26918'}}}}}}}}";
            //LucyConsoleLogBox.Text = "____________  \r\n";
            //LucyConsoleLogBox.Text += "--- SEND --- : \r\n";
            //LucyConsoleLogBox.Text += "sent to lucy: " + " \r\n"; //+ createScenario4Geo

            // --- send to lucy ---
            JObject scenarioObj = GlobalConnection.CL.sendAction2Lucy(createScenario4Geo);
            //var jtw = new JTokenWriter();
            //new GeoJsonSerializer().Serialize(jtw, featurecollection);
            //var geometry = JObject.FromObject()
            //JObject scenarioObj = LucyManager.Manager.ConnectionManager.CreateScenario("UrbanPlanningTest",

            //        JObject.Parse("{'GeoJSON':{'format':'GeoJSON','geometry':" + geoJSONPoly + "}}"));
            /* *ac*
            var scenarioObj = LucyManager.Manager.ConnectionManager.CreateScenario("UrbanPlanningTest",
                        JObject.FromObject(new {geometry = (JObject)(new Lucy.Geometry()
                        {
                            Format = "GeoJSON",
                            Data = featurecollection
                        })
                        }));
            */

            if (scenarioObj != null)
            {
                JProperty result = ((JProperty)(scenarioObj.First));
                if (result != null)
                {
                    LucyConsoleLogBox.Text += "______________  \r\n";
                    LucyConsoleLogBox.Text += "--- Result --- : \r\n";
                    LucyConsoleLogBox.Text += (result.Name + ": " + result.Value + " \r\n");

                    if (result.Value["ScID"] != null)
                        TextRW.LucyIDs.Add(_curScenarioID = (int)result.Value["ScID"]);
                }
            }
        }

        //============================================================================================================================================================================
        private List<Vector2d> PointsForIsovist(Poly2D field, List<GenBuildingLayout> buildings, float cellSize)
        {
            if (field == null) throw new ArgumentNullException("field");
            var buildRects = new List<Poly2D>();
            foreach (GenBuildingLayout curGen in buildings)
            {
                if (!curGen.Open)
                {
                    buildRects.Add(curGen.PolygonFromGen);
                }
            }
            return PointsForIsovist(field, buildRects, cellSize);
        }

        //============================================================================================================================================================================
        private List<Vector2d> PointsForIsovist(Poly2D field, List<Poly2D> buildings, float cellSize)
        {
            var result = new List<Vector2d>();

            var countX = (int)(field.BoundingBox().Width / cellSize);
            var countY = (int)(field.BoundingBox().Height / cellSize);
            Vector2d startP = field.BoundingBox().BottomLeft;
            for (int i = 0; i < countX + 1; i++)
            {
                for (int j = 0; j < countY + 1; j++)
                {
                    var curPos = new Vector2d(startP.X + i * cellSize + cellSize / 2, startP.Y + j * cellSize + cellSize / 2);
                    //m_activeCells[j * m_countX + i] = true;
                    bool active = true;
                    foreach (Poly2D curGen in buildings)
                    {
                        if (field.ContainsPoint(curPos))
                        {
                            //if (!curGen.Open)
                            //{
                            if (curGen.ContainsPoint(curPos))
                            {
                                active = false;
                                break;
                            }
                            //}
                        }
                        else active = false;
                    }

                    if (active)
                    {
                        result.Add(curPos);
                    }
                }
            }
            return result;
        }

        //============================================================================================================================================================================
        private void InfoNoConnection()
        {
            LucyConsoleLogBox.Text += ("No Connection to Luci established yet! Please connect to Luci first of all! \r\n");
        }

        //============================================================================================================================================================================
        # region BuildingCollission

        //==============================================================================================
        private void MainLoop()
        {
            //FPS set up
            double FPS = 30.0;
            long ticks1 = 0;
            double interval = Stopwatch.Frequency / FPS;
            DateTime start = DateTime.Now; // DateTime.MinValue;
            //
            //int i = 1;

            Application.DoEvents();
            DateTime end = DateTime.Now;

            long ticks2 = Stopwatch.GetTimestamp();
            if (ticks2 >= ticks1 + interval)
            {
                ticks1 = Stopwatch.GetTimestamp();

                if (cB_layoutAll.Checked)
                {
                    if (_activeMultiLayout != null)
                    {
                        _activeMultiLayout.Adapt();
                        _allLayouts = _activeMultiLayout.SubChromosomes;
                        DrawMultiLayout();
                    }
                }
                //else
                {
                    if (_allLayouts.Count > 0)
                    {
                        //-- loop just for testing !!! ---
                        //for (int i = 0; i < _allLayouts.Count; i++)
                        //{
                        //    _allLayouts[i] = (ChromosomeLayout)_allLayouts[i].Clone();
                        //    _allLayouts[i].ResetCollissionWordAssignment();
                        //}

                        foreach (ChromosomeLayout layout in _allLayouts)
                        {
                            layout.Adapt();
                        }
                        DrawLayout();
                    }
                }
                // --- interface information ---
                int test = Tools.CalculateFrameRate();
                string toHead = "Layout with " + test + " fps";
                SetText(this, toHead);
            }
            //i++;
            // -- Sleep funktion avoids "licker" problem while drag & drop...
            Thread.Sleep(1); //frees up the cpu         
        }

        # endregion

        #endregion


        //===================================================================================================================================================================
        //========= Initialize and Set =========
        //===================================================================================================================================================================
        #region Initialize&Set

        //============================================================================================================================================================================
        private void InitialTestGraph()
        {
            UndirectedEdge<Vector2d> iniEdge;
            _fitnesLists = new List<List<double>>();
            float b0 = 10;
            float by = 400;
            float bx = 500;
            _border = new Rect2D(b0, by, bx, by).ToPoly2D();
            var middle = new Vector2d(_border.BoundingBox().Width / 2, _border.BoundingBox().Height / 2);
            //var middle = new Vector2d(200, 200);
            // --- initial graph for optimization ---
            _initailNet = new UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>();

            // -- add boder segments --
            /*Vector2d[] polyPts = new Vector2d[]
            {
                new Vector2d(b0, b0), new Vector2d(b0, middle.Y + 20), new Vector2d(b0, by),
                new Vector2d(middle.X - 40, by), new Vector2d(bx, by),
                new Vector2d(bx, middle.Y - 20), new Vector2d(bx, b0)
            };
            _border = new Poly2D(polyPts);*/

            // -- add seed streets --
            double iniLgth = -10;
            Vector2d[] iniPoints;
            iniPoints = new Vector2d[2];
            // segment in the middle
            iniPoints[0] = new Vector2d(15 + middle.X, 10 + middle.Y);
            iniPoints[1] = new Vector2d(30 + middle.X, 10 + middle.Y);
            iniEdge = new UndirectedEdge<Vector2d>(iniPoints[0], iniPoints[1]);
            _initailNet.AddVerticesAndEdge(iniEdge);

            /*
            // segments at the borders
            iniPoints[0] = new Vector2d(b0, middle.Y + 20);
            iniPoints[1] = new Vector2d(b0 + iniLgth, middle.Y + 20);
            iniEdge = new UndirectedEdge<Vector2d>(iniPoints[0], iniPoints[1]);
            _initailNet.AddVerticesAndEdge(iniEdge);

            iniPoints[0] = new Vector2d(bx, middle.Y - 20);
            iniPoints[1] = new Vector2d(bx - iniLgth, middle.Y - 20);           
            iniEdge = new UndirectedEdge<Vector2d>(iniPoints[0], iniPoints[1]);
            _initailNet.AddVerticesAndEdge(iniEdge);

            iniPoints[0] = new Vector2d(middle.X - 40, by + 0);
            iniPoints[1] = new Vector2d(middle.X - 40, by - iniLgth);         
            iniEdge = new UndirectedEdge<Vector2d>(iniPoints[0], iniPoints[1]);
            _initailNet.AddVerticesAndEdge(iniEdge);*/
        }

        //============================================================================================================================================================================
        private void InitialScenarioGraph()
        {
            UndirectedEdge<Vector2d> iniEdge;
            // _fitnesLists = new List<List<double>>();
            if (_scenario.Border != null) _border = _scenario.Border;
            else
                _border = new Rect2D(0, 200, 600, 200).ToPoly2D();

            // --- initial graph for optimization ---
            _initailNet = new UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>();

            if (_scenario.InitialLines != null)
            {
                foreach (Line2D line in _scenario.InitialLines)
                {
                    iniEdge = new UndirectedEdge<Vector2d>(line.Start, line.End);
                    _initailNet.AddVerticesAndEdge(iniEdge);
                }
            }
        }

        //============================================================================================================================================================================
        private void SetControlParameters()
        {
            ControlParameters.DisplayMeasure = comboBox_Isovist.Text;
            ControlParameters.Centrality = rB_fitCentral.Checked;
            ControlParameters.Choice = rB_fitChoice.Checked;
            ControlParameters.AngleRange = Convert.ToInt16(uD_angle.Value);
            ControlParameters.LengthRange = Convert.ToInt16(uD_maxLength.Value - uD_minLength.Value);
            ControlParameters.MinLength = Convert.ToInt16(uD_minLength.Value);
            ControlParameters.MaxChilds = Convert.ToInt16(uD_connectivity.Value);
            ControlParameters.LineWidth = Convert.ToInt16(uD_lineWidth.Value);
            ControlParameters.TreeDepth = Convert.ToInt16(uD_treeDepth.Value);
            ControlParameters.MinNrBuildings = Convert.ToInt16(uD_minNrBuildings.Value);
            ControlParameters.MaxNrBuildings = Convert.ToInt16(uD_maxNrBuildings.Value);
            ControlParameters.Density = Convert.ToDouble(uD_density.Value) / 100.0;
            ControlParameters.MinSideRatio = Convert.ToDouble(uD_minSideRatio.Value) / 100.0;
            ControlParameters.MinArea = Convert.ToDouble(uD_minAbsBldArea.Value);
            ControlParameters.MaxArea = Convert.ToDouble(uD_maxAbsBldArea.Value);
            ControlParameters.Scenario = comboBox_scenario.Text;
            ControlParameters.RepeatAdapt = Convert.ToInt16(uD_Adapt.Value);
            ControlParameters.IsovistCellSize = Convert.ToInt16(uD_IsovistCellSize.Value);
            ControlParameters.MinSide = Convert.ToInt16(uD_minSideAbs.Value);
            ControlParameters.MinBldArea = Convert.ToInt16(uD_minAbsBldArea.Value);
            ControlParameters.MaxBldArea = Convert.ToInt16(uD_maxAbsBldArea.Value);
            ControlParameters.MinBldSide = Convert.ToInt16(uD_minSideAbs.Value);
            ControlParameters.MaxBldSide = Convert.ToInt16(uD_maxSideAbs.Value);
            ControlParameters.MinBldHeigth = Convert.ToInt16(uD_minHeigth.Value);
            ControlParameters.MaxBldHeigth = Convert.ToInt16(uD_maxHeigth.Value);
            ControlParameters.FloorHeigth = Convert.ToInt16(uD_floorHeigth.Value);

            ControlParameters.MaxConnectivity = Convert.ToInt16(uD_connectivity.Value);
            ControlParameters.MaxRndAngle = Convert.ToInt16(uD_angle.Value);
            ControlParameters.MinDistance = Convert.ToInt16(uD_distance.Value);

            ControlParameters.AlignToBorder = radioButton2_AlignBorder.Checked;
            ControlParameters.FixedRotationValue = Convert.ToInt16(uD_Rotation.Value);
            ControlParameters.AlignTo45 = radioButton2_Align45.Checked;
            ControlParameters.MinBlockSize = Convert.ToInt16(uD_MinBlockSize2.Value);
        }

        //============================================================================================================================================================================
        private void SetScenarioEnvironment()
        {
            string scenario = ControlParameters.Scenario;

            if (scenario == "Cape Town Scenario")
            {
                if (_activeLayout != null)
                {
                    _isovistBorder = _borderPoly.Poly.Clone();
                    _isovistBorder.Offset(1.2);
                }
                else
                {
                    var corners = new Vector2d[4];
                    corners[0] = new Vector2d(370, 195);
                    corners[1] = new Vector2d(780, 250);
                    corners[2] = new Vector2d(860, 420);
                    corners[3] = new Vector2d(560, 560);
                    _borderPoly = new GPolygon(corners);
                }
            }

            else if (scenario == "Rochor Scenario")
            {
                if (_scenario.Border != null) _border = _scenario.Border;
            }
            else
            {
                var corners = new Vector2d[4];
                corners[0] = new Vector2d(1, 1);
                corners[1] = new Vector2d(500, 1);
                corners[2] = new Vector2d(500, 400);
                corners[3] = new Vector2d(1, 400);
                _borderPoly = new GPolygon(corners);
                // _border = new Rect2D(0, 200, 600, 200).ToPoly2D();
            }
        }

        //============================================================================================================================================================================
        private void SetPopulationParameters()
        {
            _populationParameters = new PopulationParameters();
            _populationParameters.FitnessFunction = _population.FitnessFunction;
            // -- check typeof...
            if (_population.Ancestor.GetType() == typeof(ChromosomeLayout))
                _populationParameters.Ancestor = new ChromosomeLayout((ChromosomeLayout)_population.Ancestor);
            if (_population.Ancestor.GetType() == typeof(InstructionTreePisa))
                _populationParameters.Ancestor = new InstructionTreePisa((InstructionTreePisa)_population.Ancestor);
            _populationParameters.Size = _population.Size;
            _populationParameters.EpochCounter = _population.EpochCounter;
            _populationParameters.MU = _population.MU;
            _populationParameters.ALPHA = _population.ALPHA;
            _populationParameters.LAMBDA = _population.LAMBDA;
            _populationParameters.RandomSelectionPortion = _population.RandomSelectionPortion;
            _populationParameters.AutoShuffling = _population.AutoShuffling;
            _populationParameters.CrossoverRate = _population.CrossoverRate;
            _populationParameters.MutationRate = _population.MutationRate;
        }

        //============================================================================================================================================================================
        private void SetGraphParameter()
        {
            // --- morphological parameter ---
            _maxEdges = Convert.ToInt16(uD_connectivity.Value);
            _rndAngle = Convert.ToInt16(uD_angle.Value);
            _minDist = Convert.ToInt16(uD_distance.Value);
            _minAngle = Convert.ToInt16(uD_divAngle.Value);
            _maxLength = Convert.ToInt16(uD_maxLength.Value);
            _minLength = Convert.ToInt16(uD_minLength.Value);
            _border = new Rect2D(0, 0, 2000, -2000).ToPoly2D();
            //glControl1.Left, glControl1.Top, glControl1.Width, -glControl1.Height);
            _streetGenerator = new StreetPattern(_maxEdges, _rndAngle, _minDist, _minAngle, _maxLength, _minLength,
                rotSymmetryL, rotSymmetryR, _border);

            // --- TestGraph ---
            var midPt = new PointD(_border.BoundingBox().Width / 2, _border.BoundingBox().Height / 2);
            PointD[] iniPts;
            iniPts = new PointD[2];
            iniPts[0] = new PointD(midPt.X + 100, midPt.Y + 100);
            iniPts[1] = new PointD(midPt.X + 150, midPt.Y + 100);
            _streetGenerator.AddEdge(iniPts[0], iniPts[1]);

            iniPts[0] = new PointD(midPt.X + 100, midPt.Y + 100);
            iniPts[1] = new PointD(midPt.X + 50, midPt.Y + 100);
            _streetGenerator.AddEdge(iniPts[0], iniPts[1]);

            iniPts[0] = new PointD(midPt.X + 100, midPt.Y + 100);
            iniPts[1] = new PointD(midPt.X + 70, midPt.Y + 50);
            _streetGenerator.AddEdge(iniPts[0], iniPts[1]);

            iniPts[0] = new PointD(midPt.X + 150, midPt.Y + 100);
            iniPts[1] = new PointD(midPt.X + 200, midPt.Y + 150);
            _streetGenerator.AddEdge(iniPts[0], iniPts[1]);
        }

        #endregion
        
       
        //===================================================================================================================================================================
        //========= Worker threads =========
        //===================================================================================================================================================================
        #region WorkerThreads

        /* *ac* private async Task SearchNetworkMulti()*/
        private void SearchNetworkMulti()
        {
            // measure the needed time:
            DateTime start = DateTime.Now; // DateTime.MinValue;
            DateTime end;

            // create fitness function
            /* *ac* var fitnessFunction = new FitnessFunctionNetwork(LucyManager.Manager.ViaLucy ? CalculationType.Method.Luci : CalculationType.Method.Local);*/
            var fitnessFunction = new FitnessFunctionNetwork();
            var alpha = (int) uD_Alpha.Value;
            var lambda = (int) uD_Lambda.Value;
            var mu = (int) uD_Mu.Value;

            // create population
            if (_population == null)
            {
                _population = new MultiPopulation(_populationSize, alpha, lambda, mu,
                    new InstructionTreePisa(ControlParameters.TreeDepth, _initailNet, _border),
                    fitnessFunction
                    /* *ac* LucyManager.Manager.ViaLucy ? CalculationType.Method.Luci : CalculationType.Method.Local // CalculationType.Method.Local*/
                    );
                _population.MutationRate = 0.1;
                _population.CrossoverRate = 0.7;
                _population.RandomSelectionPortion = 0.1;
                SetPopulationParameters();
            }
            DrawNetworkMulti(0);
            PlotFitnessValues(0);
            //Debug.WriteLine("bestFit", Math.Round(_population.FitnessMax, 2).ToString());
            int i = 1;
            Thread.Sleep(1);

            // loop
            while (!_needToStop1)
            {
                // run one epoch of genetic algorithm
                /* *ac* await _population.RunEpoch();*/
                _population.RunEpoch();
                DrawNetworkMulti(i);
                PlotFitnessValues(i);

                // --- Achtung: Adding data to chart makes the program slower over time!!! ------
                ReSetChart(chart1, 0);
                List<IMultiChromosome> archive = _population.Sel.GetArchive();
                for (int p = 0; p < archive.Count(); p++)
                {
                    var curTree = (InstructionTreePisa) (archive[p]);
                    if (curTree.FitnessValues.Count > 1)
                    {
                        SetChart(chart1, Math.Round(curTree.FitnessValues[0], 5), curTree.FitnessValues[1], 0);
                        SetChart(chart1, Math.Round(curTree.FitnessValues[0], 5), curTree.FitnessValues[1], 1);
                    }
                }

                // set current iteration's info
                SetText(lbl_Generation, _population.SizeNonDominatedSet.ToString());

                // increase current iteration
                i++;
                //
                if ((_iterations != 0) && (i > _iterations))
                    break;

                // -- manual call of the garbage collector avoids 'out of memory' error on long optimization runs --
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Thread.Sleep(1);
            }
            // enable settings controls
            end = DateTime.Now;
            _span = end - start;
            _population.Sel.Terminate();
            glCameraNetwork.ActivateInteraction();
        }

        //============================================================================================================================================================================
        // Worker thread
        private void SearchNetworkSingle()
        {
            // measure the needed time:
            DateTime start = DateTime.Now;
            DateTime end;

            // create fitness function
            var fitnessFunction = new FitnessFunctionNetworkSingle();

            // create population
            _populationSingle = new Population(_populationSize,
                new InstructionTreeSingle(ControlParameters.TreeDepth, _initailNet, _border),
                fitnessFunction,
                new EliteSelection()
                );
            _populationSingle.MutationRate = 0.25;
            //population.CrossoverRate = 0.0;
            _populationSingle.RandomSelectionPortion = 0.2;
            DrawNetworkSingle(0);
            Debug.WriteLine("bestFit", Math.Round(_populationSingle.FitnessMax, 2).ToString());
            int i = 1;

            // loop
            while (!_needToStop1)
            {
                // run one epoch of genetic algorithm
                _populationSingle.RunEpoch();
                DrawNetworkSingle(i);

                // --- Achtung: Ading data to chart makes the program slow down over time!!! ------
                //ReSetChart(chart1, 0);
                //for (int p = 0; p < _population.SizeNonDominatedSet; p++)
                //{
                //    InstructionTree curTree = (InstructionTree)(_population[p]);
                //    if (curTree.FitnessValues.Count > 1)
                //    {
                //        SetChart(chart1, Math.Round(curTree.FitnessValues[0], 5), curTree.FitnessValues[1], 0);
                //        SetChart(chart1, Math.Round(curTree.FitnessValues[0], 5), curTree.FitnessValues[1], 1);
                //    }
                //}

                Debug.WriteLine(i.ToString() + ". iterat, bestFit",
                    Math.Round(_populationSingle.FitnessMax, 2).ToString());

                // increase current iteration
                i++;
                //
                if ((_iterations != 0) && (i > _iterations))
                    break;

                // -- manual call of the garbage collector avoids 'out of memory' error on long optimization runs --
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Thread.Sleep(1);
            }
            end = DateTime.Now;
            _span = end - start;
            glCameraMainWindow.ActivateInteraction();
        }

        //============================================================================================================================================================================
        // Worker thread
        private void SearchLayoutSingle()
        {
            // measure the needed time:
            DateTime start = DateTime.Now; // DateTime.MinValue;
            DateTime end;
            // create fitness function
            var fitnessFunction = new FitnessFunctionIsovistSingle();
            // create population
            ParametersLayout lParameters = new ParametersLayout();
            lParameters.MaxBldArea = ControlParameters.MaxBldArea;
            lParameters.MinBldArea = ControlParameters.MinBldArea;
            lParameters.MaxBldHeigth = ControlParameters.MaxBldHeigth;
            lParameters.MinBldHeigth = ControlParameters.MinBldHeigth;
            lParameters.MinNrBuildings = ControlParameters.MinNrBuildings;
            lParameters.MaxNrBuildings = ControlParameters.MaxNrBuildings;
            lParameters.Density = ControlParameters.Density;
            lParameters.MinSideRatio = ControlParameters.MinSideRatio;
            lParameters.FixRotation = ControlParameters.AlignTo45;
            lParameters.FixedRotationValue = ControlParameters.FixedRotationValue;
            lParameters.AlignRotationToBorder = ControlParameters.AlignToBorder;

            _populationSingle = new Population(_populationSize,
                new ChromosomeLayoutSingle(_borderPoly.Poly.Clone(), lParameters),
                fitnessFunction,
                new EliteSelection()
                );
            _populationSingle.MutationRate = 0.25;
            _populationSingle.RandomSelectionPortion = 0.2;
            DrawLayoutSingle(0);
            //Debug.WriteLine("bestFit", Math.Round(_populationSingle.FitnessMax, 2).ToString());
            int i = 1;

            // loop
            while (!_needToStop1)
            {
                // run one epoch of genetic algorithm
                _populationSingle.RunEpoch();
                DrawLayoutSingle(i);

                Debug.WriteLine(i.ToString() + ". iterat, bestFit",
                    Math.Round(_populationSingle.FitnessMax, 2).ToString());
                // increase current iteration
                i++;

                if ((_iterations != 0) && (i > _iterations))
                    break;

                // -- manual call of the garbage collector avoids 'out of memory' error on long optimization runs --
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Thread.Sleep(1);
            }
            end = DateTime.Now;
            _span = end - start;

            glCameraMainWindow.ActivateInteraction();
        }

        //============================================================================================================================================================================
        // Worker thread
        /* *ac* private async Task SearchLayoutMultiAllBlocks()*/
        private void SearchLayoutMultiAllBlocks()
        {
            if (_blocks == null) return;
            // measure the needed time:
            DateTime start = DateTime.Now; // DateTime.MinValue;
            DateTime end;

            // create fitness function
            /* *ac* var fitnessFunction = new FitnessFunctionMultiLayout(LucyManager.Manager.ViaLucy ? CalculationType.Method.Luci : CalculationType.Method.Local);*/
            var fitnessFunction = new FitnessFunctionIsovist(RunVia);//, RunVia_Solar);//, Settings.Default.PathToHDRI);
            _borderPoly = new GPolygon(_border.Clone());

            _isovistBorder = _borderPoly.Poly.Clone();
            _isovistBorder.Offset(2);

            var alpha = (int)uD_Alpha.Value;
            var lambda = (int)uD_Lambda.Value;
            var mu = (int)uD_Mu.Value;

            // --- create population ---
            if (_population == null)
            {
                ParametersLayout lParameters = new ParametersLayout();
                lParameters.MaxBldArea = ControlParameters.MaxBldArea;
                lParameters.MinBldArea = ControlParameters.MinBldArea;
                lParameters.MaxBldHeigth = ControlParameters.MaxBldHeigth;
                lParameters.MinBldHeigth = ControlParameters.MinBldHeigth;
                lParameters.MinNrBuildings = ControlParameters.MinNrBuildings;
                lParameters.MaxNrBuildings = ControlParameters.MaxNrBuildings;
                lParameters.Density = ControlParameters.Density;
                lParameters.MinSideRatio = ControlParameters.MinSideRatio;
                lParameters.FixRotation = ControlParameters.AlignTo45;
                lParameters.FixedRotationValue = ControlParameters.FixedRotationValue;
                lParameters.AlignRotationToBorder = ControlParameters.AlignToBorder;

                ChromosomeMultiLayout curChromosome;
                if (_activeLayout != null) curChromosome = new ChromosomeMultiLayout(_activeMultiLayout);
                else
                    curChromosome = new ChromosomeMultiLayout(_borderPoly.Poly.Clone(), _blocks, lParameters);

                _population = new MultiPopulation(_populationSize, alpha, lambda, mu,
                    curChromosome,
                    fitnessFunction
                    /* *ac* LucyManager.Manager.ViaLucy ? CalculationType.Method.Luci : CalculationType.Method.Local //CalculationType.Method.Local*/
                    );
                _population.MutationRate = 0.1;
                _population.CrossoverRate = 0.7;
                _population.RandomSelectionPortion = 0.1;

                SetPopulationParameters();
            }
            DrawLayoutMulti(0);
            // iterations
            int i = 1;

            // loop
            while (!_needToStop1)
            {
                // run one epoch of genetic algorithm
                /* *ac* await _population.RunEpoch();*/
                _population.RunEpoch();
                DrawLayoutMulti(i);

                // set current iteration's info
                SetText(lbl_Generation, _population[0].Generation.ToString()); //SizeNonDominatedSet.ToString());

                //if (LucyUseServicesCheckBox.Checked)
                //    ArchiveToLucy();

                // increase current iteration
                i++;

                if ((_iterations != 0) && (i > _iterations))
                    break;

                // -- manual call of the garbage collector avoids 'out of memory' error on long optimization runs --
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Thread.Sleep(1);
            }
            end = DateTime.Now;
            _span = end - start;
            _population.Sel.Terminate();
            glCameraLayouts.ActivateInteraction();
        }

        //============================================================================================================================================================================
        // Worker thread
        /* *ac* private async Task SearchLayoutMulti()*/
        private void SearchLayoutMulti()
        {
            NotificationBroker.Instance.PostNotification("NUMLOOPS", _iterations);
            NotificationBroker.Instance.PostNotification("CURLOOP", 0);

            // measure the needed time:
            DateTime start = DateTime.Now; // DateTime.MinValue;
            DateTime end;

            // create fitness function
            //var fitnessFunction = new FitnessFunctionLayout(LucyManager.Manager.ViaLucy ? CalculationType.Method.Luci : CalculationType.Method.Local);
            /* *ac* var fitnessFunction = new FitnessFunctionLayoutCombined(LucyManager.Manager.ViaLucy ? CalculationType.Method.Luci : CalculationType.Method.Local);*/
            var fitnessFunction = new FitnessFunctionIsovist(RunVia);

            if (_activeLayout != null) _borderPoly = new GPolygon(_activeLayout.Border);
            _isovistBorder = _borderPoly.Poly.Clone();
            _isovistBorder.Offset(2);
            //Rect2D borderRect = GeoAlgorithms2D.BoundingBox(_isovistBorder.PointsFirstLoop);

            var alpha = (int)uD_Alpha.Value;
            var lambda = (int)uD_Lambda.Value;
            var mu = (int)uD_Mu.Value;

            // create population
            if (_population == null)
            {
                ParametersLayout lParameters = new ParametersLayout();
                lParameters.MaxBldArea = ControlParameters.MaxBldArea;
                lParameters.MinBldArea = ControlParameters.MinBldArea;
                lParameters.MaxBldHeigth = ControlParameters.MaxBldHeigth;
                lParameters.MinBldHeigth = ControlParameters.MinBldHeigth;
                lParameters.MinNrBuildings = ControlParameters.MinNrBuildings;
                lParameters.MaxNrBuildings = ControlParameters.MaxNrBuildings;
                lParameters.Density = ControlParameters.Density;
                lParameters.MinSideRatio = ControlParameters.MinSideRatio;
                lParameters.FixRotation = ControlParameters.AlignTo45;
                lParameters.FixedRotationValue = ControlParameters.FixedRotationValue;
                lParameters.AlignRotationToBorder = ControlParameters.AlignToBorder;

                ChromosomeLayout curChromosome;
                if (_activeLayout != null) curChromosome = new ChromosomeLayout(_activeLayout);
                else
                    curChromosome = new ChromosomeLayout(_borderPoly.Poly.Clone(), lParameters);

                bool inheritOnly = false;
                if (ControlParameters.Scenario == "Cape Town Scenario") inheritOnly = true;
                _population = new MultiPopulation(_populationSize, alpha, lambda, mu,
                    curChromosome,
                    fitnessFunction,
                    /* *ac* LucyManager.Manager.ViaLucy ? CalculationType.Method.Luci : CalculationType.Method.Local, //CalculationType.Method.Local,*/
                    inheritOnly
                    );

                _population.MutationRate = 0.1;
                _population.CrossoverRate = 0.7;
                _population.RandomSelectionPortion = 0.1;

                SetPopulationParameters();
            }
            DrawLayoutMulti(0);
            PlotFitnessValues(0);
            //VarianceAnalysis(0);
            // iterations
            int i = 1;

            // loop
            while (!_needToStop1)
            {
                NotificationBroker.Instance.PostNotification("CURLOOP", i);

                // run one epoch of genetic algorithm
                /* *ac* await _population.RunEpoch();*/
                _population.RunEpoch();
                DrawLayoutMulti(i);
                PlotFitnessValues(i);
                //VarianceAnalysis(i);

                // set current iteration's info
                SetText(lbl_Generation, _population[0].Generation.ToString()); //SizeNonDominatedSet.ToString());
                //if (LucyUseServicesCheckBox.Checked)
                //{
                //    ArchivePartialToLucy();
                //}

                // increase current iteration
                i++;

                if ((_iterations != 0) && (i > _iterations))
                {
                    break;
                }

                // -- manual call of the garbage collector avoids 'out of memory' error on long optimization runs --
                GC.Collect();
                GC.WaitForPendingFinalizers();
                Thread.Sleep(1);
            }
            end = DateTime.Now;
            _span = end - start;
            _population.Sel.Terminate();
            glCameraLayouts.ActivateInteraction();
        }

        #endregion


        //===================================================================================================================================================================
        //========= Draw =========
        //===================================================================================================================================================================
        # region Draw

        //============================================================================================================================================================================
        private void ReDrawGraph(Subdivision curGraph)
        {
            // --- draw the elements of the graph ---
            glCameraMainWindow.AllObjectsList.ObjectsToDraw.Clear();
            glCameraMainWindow.AllObjectsList.ObjectsToInteract.Clear();
            glCameraMainWindow.AllObjectsList = new ObjectsAllList();
            LineD[] graphLines = curGraph.ToLines();
            foreach (LineD line in graphLines)
            {
                glCameraMainWindow.AllObjectsList.Add(new GLine(line));
            }
            PointD[] graphPts = curGraph.Nodes.ToArray();
            foreach (PointD point in graphPts)
            {
                glCameraMainWindow.AllObjectsList.Add(new GCircle(new Vector2d(point.X, point.Y), 1, 10));
            }
            glCameraMainWindow.Invalidate();
        }

        //============================================================================================================================================================================
        private void DrawAnalysedNetwork(InstructionTreePisa curTree, GLCameraControl viewControl)
        {
            _allLayouts = new List<ChromosomeLayout>();
            _isovistField = null;
            _selectedBlock = null;
            _smartGrid = null;

            Vector2d[] nodes = curTree.Network.Vertices.ToArray();
            UndirectedEdge<Vector2d>[] tempEdges = curTree.Network.Edges.ToArray();
            var edges = new List<Line2D>();
            foreach (var tempEdge in tempEdges)
            {
                edges.Add(new Line2D(tempEdge.Source, tempEdge.Target));
            }
            Vector4[] ffColors = null;
            if (rB_fitChoice.Checked)
                ffColors = ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormChoiceArray()[0]);
            if (rB_fitCentral.Checked)
                ffColors = ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormCentralityMetricArray()[0]);

            DrawAnalysis(ffColors, edges.ToArray(), nodes, new Vector2d(0, 0), viewControl);

            CreateBlocks(edges);
        }

        //============================================================================================================================================================================
        private void DrawAnalysis(Vector4[] ffColors, Line2D[] origEdges, Vector2d[] graphPts, Vector2d origin, GLCameraControl viewControl)
        {
            _remainDrawingObjects = new List<GeoObject>();
            viewControl.AllObjectsList = new ObjectsAllList();

            // -- draw the border --
            if (_border == null)
                _border =
                    new Rect2D(glCameraMainWindow.Left, glCameraMainWindow.Top, glCameraMainWindow.Width,
                        -glCameraMainWindow.Height).ToPoly2D();
            //GRectangle bord = new GRectangle(Origin.X, _border.BoundingBox().Height + Origin.Y, _border.BoundingBox().Width, _border.BoundingBox().Height);
            //Color4 col = Color4.White;
            //bord.BorderColor = new Vector4(col.R, col.G, col.B, col.A);
            //glCameraMainWindow.AllObjectsList.ObjectsToDraw.Add(bord);
            // --- draw the elements of the graph ---
            Line2D curEdge;
            for (int i = 0; i < ffColors.Length; i++)
            {
                curEdge = origEdges[i];
                curEdge = new Line2D(curEdge.Start.X + origin.X, curEdge.Start.Y + origin.Y, curEdge.End.X + origin.X,
                    curEdge.End.Y + origin.Y);
                var curLine = new GLine(curEdge);
                curLine.Width = ControlParameters.LineWidth;

                curLine.Color = ffColors[i];
                viewControl.AllObjectsList.Add(curLine, true, false);
            }
            foreach (Vector2d point in graphPts)
            {
                viewControl.AllObjectsList.Add(
                    new GCircle(new Vector2d(point.X + origin.X, point.Y + origin.Y), .6, 10), true, true);
            }

            //_remainDrawingObjects = new List<GeoObject>();
            if (_scenario.EnvironmentCloseNetwork != null)
                foreach (Line2D line in _scenario.EnvironmentCloseNetwork)
                {
                    _remainDrawingObjects.Add(new GLine(line));
                }
            viewControl.AllObjectsList.ObjectsToDraw.AddRange(_remainDrawingObjects);
            _remainDrawingObjects = viewControl.AllObjectsList.ObjectsToDraw;
        }

        //============================================================================================================================================================================
        private void DrawAnalysis(Vector4[] ffColors, LineD[] origEdges, PointD[] graphPts, PointD origin, GLCameraControl viewControl, int netID)
        {
            // -- draw the border --
            if (_border == null)
                _border =
                    new Rect2D(glCameraMainWindow.Left, glCameraMainWindow.Top, glCameraMainWindow.Width,
                        -glCameraMainWindow.Height).ToPoly2D();
            // --- draw the elements of the graph ---
            LineD curEdge;
            for (int i = 0; i < ffColors.Length; i++)
            {
                curEdge = origEdges[i];
                curEdge = new LineD(curEdge.Start.X + origin.X, curEdge.Start.Y + origin.Y, curEdge.End.X + origin.X,
                    curEdge.End.Y + origin.Y);
                var curLine = new GLine(curEdge);
                curLine.Width = ControlParameters.LineWidth;
                curLine.PickingThreshold = 50;
                curLine.Identity = netID;
                curLine.Color = ffColors[i];
                viewControl.AllObjectsList.Add(curLine, true, false);
            }
            foreach (PointD point in graphPts)
            {
                viewControl.AllObjectsList.Add(
                    new GCircle(new Vector2d(point.X + origin.X, point.Y + origin.Y), .6, 10), true, false);
            }
        }

        //============================================================================================================================================================================
        private void DrawNetwork(Subdivision curGraph)
        {
            // --- draw the elements of the graph ---
            glCameraMainWindow.AllObjectsList.ObjectsToDraw.Clear();
            glCameraMainWindow.AllObjectsList.ObjectsToInteract.Clear();
            glCameraMainWindow.AllObjectsList = new ObjectsAllList();
            LineD[] graphLines = curGraph.ToLines();
            foreach (LineD line in graphLines)
            {
                var curLine = new GLine(line);
                curLine.Color = new Vector4(0.09f, 0.09f, 0.09f, 1f);
                glCameraMainWindow.AllObjectsList.Add(curLine);
            }

            PointD[] graphPts = curGraph.Nodes.ToArray();
            foreach (PointD point in graphPts)
            {
                glCameraMainWindow.AllObjectsList.Add(new GCircle(new Vector2d(point.X, point.Y), .1, 10));
            }
            glCameraMainWindow.Invalidate();
        }

        //============================================================================================================================================================================
        private void DrawNetwork(UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> curGraph)
        {
            // --- draw the elements of the graph ---
            glCameraMainWindow.AllObjectsList.ObjectsToDraw.Clear();
            glCameraMainWindow.AllObjectsList.ObjectsToInteract.Clear();
            glCameraMainWindow.AllObjectsList = new ObjectsAllList();
            UndirectedEdge<Vector2d>[] graphLines = curGraph.Edges.ToArray();
            foreach (UndirectedEdge<Vector2d> line in graphLines)
            {
                var curLine = new GLine(line.Source, line.Target);
                curLine.Color = new Vector4(0.09f, 0.09f, 0.09f, 1f);
                glCameraMainWindow.AllObjectsList.Add(curLine);
            }

            Vector2d[] graphPts = curGraph.Vertices.ToArray();
            foreach (Vector2d point in graphPts)
            {
                glCameraMainWindow.AllObjectsList.Add(new GCircle(new Vector2d(point.X, point.Y), .1, 10));
            }
            glCameraMainWindow.Invalidate();
        }

        //============================================================================================================================================================================
        private void DrawBest(int iterat)
        {
            var fittest = (InstructionTreePisa)(_population.BestChromosome);
            Vector4[] ffColors = null;
            if (rB_fitChoice.Checked)
                ffColors = ConvertColor.CreateFFColor4(fittest.ShortestPathes.GetNormChoiceArray()[0]);
            if (rB_fitCentral.Checked)
                ffColors = ConvertColor.CreateFFColor4(fittest.ShortestPathes.GetNormCentralityMetricArray()[0]);
            DrawAnalysis(ffColors, fittest.ShortestPathes.OrigEdges, fittest.ShortestPathes.OrigPoints,
                new PointD(0, iterat * fittest.MaxSize), glCameraNetwork, fittest.Indentity);
            //glCameraMainWindow.ZoomAll();
        }

        //============================================================================================================================================================================
        private void DrawNetworkSingle(int iterat)
        {
            // -- draw the network --
            float dist = 1.1f;
            var fitnesValues = new List<double>();

            if (cb_showSOMorder.Checked || cb_showBestGen.Checked)
            {
                glCameraMainWindow.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            }

            if (cb_showAll.Checked || cb_showBestGen.Checked)
            {
                for (int i = 0; i < _populationSingle.Size; i++)
                {
                    var curTree = (InstructionTreeSingle)_populationSingle[i];
                    Vector4[] ffColors = null;
                    if (curTree.ShortestPathes != null)
                    {
                        if (rB_fitChoice.Checked)
                            ffColors = ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormChoiceArray()[0]);
                        if (rB_fitCentral.Checked)
                            ffColors =
                                ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormCentralityMetricArray()[0]);
                        DrawAnalysis(ffColors, curTree.ShortestPathes.OrigEdges, curTree.ShortestPathes.OrigPoints,
                            new PointD(i * curTree.Border.BoundingBox().Width * dist,
                                iterat * curTree.Border.BoundingBox().Height * dist), glCameraNetwork, -1);
                        if (!cb_showAll.Checked) fitnesValues.Add(curTree.Fitness);
                    }
                }
            }

            var bestTree = (InstructionTreeSingle)_populationSingle.BestChromosome;
            if (bestTree != null)
            {
                Vector4[] ffColors = null;
                if (bestTree.ShortestPathes != null)
                {
                    if (rB_fitChoice.Checked)
                        ffColors = ConvertColor.CreateFFColor4(bestTree.ShortestPathes.GetNormChoiceArray()[0]);
                    if (rB_fitCentral.Checked)
                        ffColors = ConvertColor.CreateFFColor4(bestTree.ShortestPathes.GetNormCentralityMetricArray()[0]);
                    DrawAnalysis(ffColors, bestTree.ShortestPathes.OrigEdges, bestTree.ShortestPathes.OrigPoints,
                        new PointD(-1.3 * bestTree.Border.BoundingBox().Width * dist,
                            iterat * bestTree.Border.BoundingBox().Height * dist), glCameraNetwork, -1);
                    if (!cb_showAll.Checked) fitnesValues.Add(bestTree.Fitness);
                }
            }

            for (int i = 0; i < _populationSingle.Size; i++)
            {
                var curTree = (InstructionTreeSingle)(_populationSingle[i]);
                fitnesValues.Add(curTree.Fitness);
            }
            _fitnesLists.Add(fitnesValues);

            if (cB_autoShot.Checked) SetGraphicContext(glCameraMainWindow); //Screenshot();

            glCameraNetwork.ZoomAll();
        }

        //============================================================================================================================================================================
        private void DrawNetworkMulti(int iterat)
        {
            // -- draw the network --
            float dist = 1.1f;
            //var fitnesValues = new List<double>();

            if (cb_showSOMorder.Checked || cb_showBestGen.Checked)
            {
                glCameraNetwork.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            }

            if (cb_showSOMorder.Checked)
            {
                SOMorderProcess();
                if (cB_showSomMap.Checked) ShowEsomMap();
                DrawNetworkSOM();
                return;
            }

            List<IMultiChromosome> archive = _population.Sel.GetArchive();
            for (int p = 0; p < archive.Count(); p++)
            {
                var curTree = (InstructionTreePisa)(archive[p]);
                if (curTree.ShortestPathes != null)
                {
                    Vector4[] ffColors = null;
                    if (rB_fitChoice.Checked)
                        ffColors = ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormChoiceArray()[0]);
                    if (rB_fitCentral.Checked)
                        ffColors = ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormCentralityMetricArray()[0]);
                    DrawAnalysis(ffColors, curTree.ShortestPathes.OrigEdges, curTree.ShortestPathes.OrigPoints,
                        new PointD(p * curTree.Border.BoundingBox().Width * dist,
                            iterat * curTree.Border.BoundingBox().Height * dist), glCameraNetwork, curTree.Indentity);
                }
            }
            if (cB_autoShot.Checked) SetGraphicContext(glCameraMainWindow); //Screenshot();
            glCameraNetwork.Invalidate();
            glCameraNetwork.ZoomAll();
        }

        //============================================================================================================================================================================
        private void DrawLayoutMulti(int iterat) //Population pop); //InstructionTreePisa fittest)
        {
            // -- draw the network --
            glCameraMainWindow.AllObjectsList.ObjectsToDraw.AddRange(_remainDrawingObjects);

            float distX = (float) nUD_distX.Value/10.0f;
            float distY = (float) nUD_distY.Value/10.0f;
            //var fitnesValues = new List<double>();
            if (_population == null) return;
            if (cb_showSOMorder.Checked || cb_showBestGen.Checked)
            {
                glCameraLayouts.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
                //if (constantDrawObjects != null) glCameraMainWindow.AllObjectsList.ObjectsToDraw.AddRange(constantDrawObjects);
            }

            if (cb_showSOMorder.Checked)
            {
                SOMorderProcess();
                if (cB_showSomMap.Checked) ShowEsomMap();
                DrawLayoutSOM();
                return;
            }

            List<IMultiChromosome> archive = _population.Sel.GetArchive();
            for (int p = 0; p < archive.Count(); p++)
            {
                var curChromo = (ChromosomeLayout) (archive[p]);
                _globalArchive.Add(curChromo);
                if (curChromo.IsovistField != null)
                {
                    if (curChromo.IsovistField.DisplayMeasure == "Solar Analysis")
                        curChromo.IsovistField.DisplayMeasure = "Area";
                    else
                        curChromo.IsovistField.DisplayMeasure = ControlParameters.DisplayMeasure; // comboBox_Isovist.Text;

                    //curChromo.IsovistField.WriteToGridValues();
                    //GGridOld VisualIsovistField = new GGridOld(curChromo.IsovistField);
                    var visualIsovistField = new GGrid(curChromo.IsovistField.AnalysisPoints,
                        curChromo.IsovistField.GetMeasure(), curChromo.IsovistField.CellSize);
                    var deltaP = new Vector2d(p*curChromo.BoundingRect.Width*distX,
                        iterat*curChromo.BoundingRect.Height*distY);
                    visualIsovistField.Move(deltaP);
                    glCameraLayouts.AllObjectsList.ObjectsToDraw.Add(visualIsovistField);

                    if (curChromo.Environment != null)
                        foreach (Poly2D envObj in curChromo.Environment)
                        {
                            var ePoly = new GPolygon(envObj);
                            ePoly.Identity = curChromo.Indentity;
                            ePoly.Move(deltaP);
                            glCameraLayouts.AllObjectsList.ObjectsToDraw.Add(ePoly);
                        }

                    if (curChromo.Gens != null)
                    {
                        foreach (GenBuildingLayout building in curChromo.Gens)
                        {
                            var bPoly = new GPrism(building.PolygonFromGen);
                            bPoly.Height = building.BuildingHeight;
                            bPoly.LevelHeight = 4;
                            bPoly.Identity = curChromo.Indentity;
                            bPoly.DeltaZ = 0.1f;
                            bPoly.BorderWidth = 2;
                            bPoly.FillColor = ConvertColor.CreateColor4(Color.White);
                            if (!building.Open) bPoly.Filled = true;
                            else
                            {
                                bPoly.Filled = false;
                                bPoly.BorderWidth = 1;
                                Vector4 col = ConvertColor.CreateColor4(Color.Gainsboro);
                                col.W = 0.1f; // alpha value
                                bPoly.BorderColor = col;
                            }
                            bPoly.Move(deltaP);
                            glCameraLayouts.AllObjectsList.ObjectsToDraw.Add(bPoly);
                        }
                    }
                    var borderPoly = new GPolygon(curChromo.Border);
                    borderPoly.Identity = curChromo.Indentity;
                    borderPoly.Move(deltaP);
                    glCameraLayouts.AllObjectsList.ObjectsToDraw.Add(borderPoly);
                }
            }
            //if (cB_autoShot.Checked) SetGraphicContext(glControl1); //Screenshot();
            glCameraLayouts.Invalidate();
            glCameraLayouts.ZoomAll();
        }

        //============================================================================================================================================================================
        private void DrawLayoutSOM() //Population pop); //InstructionTreePisa fittest)
        {

            // -- draw the network --
            float distX = (float) nUD_distX.Value/100.0f;
            float distY = (float) nUD_distY.Value/100.0f;
            if (_somOrder == null)
            {
                DrawLayoutMulti(1);
                return;
            }
            glCameraLayouts.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            var curArchive = new List<ChromosomeLayout>();
            
            //if (_SOMOrderedArchive != null)
            {
                foreach (IMultiChromosome curImChromo in _SOMOrderedArchive)
                {
                    if (curImChromo is ChromosomeLayout)
                        curArchive.Add((ChromosomeLayout)curImChromo);
                }
            }
            //else
            //{
            //    foreach (IMultiChromosome curIMChromo in _population.Sel.GetArchive())
            //        curArchive.Add((ChromosomeLayout)curIMChromo);
            //}

            for (int p = 0; p < _somOrder.GetLength(0); p++)
            {
                ChromosomeLayout curChromo =
                    curArchive.Find(chromosomeLayout => chromosomeLayout.Indentity == _somOrder[p, 0]);

                if (curChromo != null)
                {
                    var deltaP = new Vector2d(_somOrder[p, 2] * curChromo.BoundingRect.Width * distX,
                            _somOrder[p, 1] * -curChromo.BoundingRect.Height * distY);

                    //if (ControlParameters.VisualiseAnalysis == FieldForVisual.IsovistAnalysis)
                    {
                        if (curChromo.IsovistField != null)
                        {
                            if (curChromo.IsovistField.DisplayMeasure == "Solar Analysis")
                                curChromo.IsovistField.DisplayMeasure = "Area";
                            else
                                curChromo.IsovistField.DisplayMeasure = ControlParameters.DisplayMeasure; // comboBox_Isovist.Text;

                            var visualIsovistField = new GGrid(curChromo.IsovistField.AnalysisPoints,
                                curChromo.IsovistField.GetMeasure(), curChromo.IsovistField.CellSize);

                            visualIsovistField.Move(deltaP);
                            glCameraLayouts.AllObjectsList.ObjectsToDraw.Add(visualIsovistField);

                            if (curChromo.Environment != null)
                                foreach (Poly2D envObj in curChromo.Environment)
                                {
                                    var ePoly = new GPolygon(envObj);
                                    ePoly.Identity = curChromo.Indentity;
                                    ePoly.Move(deltaP);
                                    glCameraLayouts.AllObjectsList.ObjectsToDraw.Add(ePoly);
                                }
                        }
                    }
                    //else if (ControlParameters.VisualiseAnalysis == FieldForVisual.SolarAnalysis)
                    //{

                    //    var sf = new ScalarField(curChromo.Border.PointsFirstLoop
                    //        .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
                    //        ControlParameters.IsovistCellSize, new Vector3d(0, 0, 1));
                    //    GGrid solarGrid = new GGrid(sf, "solarAnalysis");
                    //    solarGrid.ScalarField.GenerateTexture(curChromo.SolarAnalysisResults, "solarAnalysis");
                    //    solarGrid.GenerateTexture();
                    //    solarGrid.Move(deltaP);
                    //    glCameraLayouts.AllObjectsList.ObjectsToDraw.Add(solarGrid);
                    //}

                    if (curChromo.Gens != null)
                    {
                        foreach (GenBuildingLayout building in curChromo.Gens)
                        {
                            var bPoly = new GPrism(building.PolygonFromGen);
                            if (ControlParameters.VisualiseAnalysis == FieldForVisual.IsovistAnalysis)
                            {
                                bPoly.Height = building.BuildingHeight;
                                bPoly.Identity = curChromo.Indentity;
                                bPoly.DeltaZ = 0.1f;
                                bPoly.BorderWidth = 2;
                                bPoly.FillColor = ConvertColor.CreateColor4(Color.White);
                            }
                            else if (ControlParameters.VisualiseAnalysis == FieldForVisual.SolarAnalysis)
                            {
                                bPoly.SFields = building.SideGrids;
                                bPoly.Height = building.BuildingHeight;
                                bPoly.LevelHeight = ControlParameters.FloorHeigth;
                                bPoly.Identity = curChromo.Indentity;
                                bPoly.DeltaZ = 0.1f;
                                bPoly.BorderWidth = 5;
                            }
                            var distPoly = new GPolygon(building.PolyForCollission.BodyPoly);
                            distPoly.Filled = false;
                            distPoly.BorderWidth = 1;
                            distPoly.BorderColor = ConvertColor.CreateColor4(Color.Chartreuse);
                            distPoly.Move(deltaP);
                            glCameraLayouts.AllObjectsList.ObjectsToDraw.Add(distPoly);

                            if (!building.Open) bPoly.Filled = true;
                            else
                            {
                                bPoly.Filled = false;
                                bPoly.BorderWidth = 1;
                                Vector4 col = ConvertColor.CreateColor4(Color.Gainsboro);
                                col.W = 0.1f; // alpha value
                                bPoly.BorderColor = col;
                            }
                            bPoly.Move(deltaP);
                            glCameraLayouts.AllObjectsList.ObjectsToDraw.Add(bPoly);
                        }
                    }

                    var borderPoly = new GPolygon(curChromo.Border);
                    borderPoly.Identity = curChromo.Indentity;
                    borderPoly.Move(deltaP);
                    glCameraLayouts.AllObjectsList.ObjectsToDraw.Add(borderPoly);
                }
            }
            //if (cB_autoShot.Checked) SetGraphicContext(glControl1); //Screenshot();
            glCameraLayouts.Invalidate();
            glCameraLayouts.ZoomAll();
        }

        //============================================================================================================================================================================
        private void DrawNetworkSOM() //Population pop); //InstructionTreePisa fittest)
        {
            // -- draw the network --
            float distX = (float) nUD_distX.Value/100.0f;
            float distY = (float) nUD_distY.Value/100.0f;
            if (_somOrder == null)
            {
                DrawLayoutMulti(1);
                return;
            }
            glCameraNetwork.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            var curArchive = new List<InstructionTreePisa>();
            
            foreach (IMultiChromosome curImChromo in _SOMOrderedArchive)
                curArchive.Add((InstructionTreePisa) curImChromo);

            for (int p = 0; p < _somOrder.GetLength(0); p++)
            {
                InstructionTreePisa curTree =
                    curArchive.Find(instructionTreePisa => instructionTreePisa.Indentity == _somOrder[p, 0]);
                if (curTree.Network != null)
                {
                    var deltaP = new Vector2d(_somOrder[p, 2]*curTree.Border.BoundingBox().Width*distX,
                        _somOrder[p, 1]*-curTree.Border.BoundingBox().Height*distY);

                    // - Environment --
                    //if (curChromo.Environment != null)
                    //{
                    //    foreach (Poly2D envObj in curChromo.Environment)
                    //    {
                    //        GPolygon ePoly = new GPolygon(envObj);
                    //        ePoly.Identity = curChromo.Indentity;
                    //        ePoly.Move(deltaP);
                    //        glCameraNetwork.AllObjectsList.ObjectsToDraw.Add(ePoly);
                    //    }
                    //}

                    //if (curTree.Gens != null)
                    {
                        //foreach (GenBuildingLayout building in curChromo.Gens)
                        //{
                        //    GPolygon bPoly = new GPolygon(building.Poly);
                        //    bPoly.Identity = curChromo.Indentity;
                        //    bPoly.DeltaZ = 0.1f;
                        //    bPoly.BorderWidth = 2;
                        //    bPoly.FillColor = ConvertColor.CreateColor4(Color.DimGray);
                        //    if (!building.Open) bPoly.Filled = true;
                        //    else bPoly.Filled = false;
                        //    bPoly.Move(deltaP);
                        //    glCameraNetwork.AllObjectsList.ObjectsToDraw.Add(bPoly);
                        //}

                        //List<IMultiChromosome> archive = _population.Sel.GetArchive();
                        //for (int k = 0; k < archive.Count(); k++)
                        //{
                        //InstructionTreePisa curTree = (InstructionTreePisa)(archive[k]);
                        if (curTree.ShortestPathes != null)
                        {
                            Vector4[] ffColors = null;
                            if (rB_fitChoice.Checked)
                                ffColors = ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormChoiceArray()[0]);
                            if (rB_fitCentral.Checked)
                                ffColors =
                                    ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormCentralityMetricArray()[0]);
                            DrawAnalysis(ffColors, curTree.ShortestPathes.OrigEdges,
                                curTree.ShortestPathes.OrigPoints, deltaP.ToPointD(), glCameraNetwork, curTree.Indentity);
                        }
                        //}
                    }

                    //GPolygon borderPoly = new GPolygon(curChromo.Border);
                    //borderPoly.Identity = curChromo.Indentity;
                    //borderPoly.Move(deltaP);
                    //glCameraNetwork.AllObjectsList.ObjectsToDraw.Add(borderPoly);
                }
            }
            //if (cB_autoShot.Checked) SetGraphicContext(glControl1); //Screenshot();
            glCameraNetwork.Invalidate();
            glCameraNetwork.ZoomAll();
        }

        //============================================================================================================================================================================
        private void DrawLayoutSingle(int iterat) //Population pop); //InstructionTreePisa fittest)
        {
            // -- draw the network --
            float dist = 1.1f;
            //var fitnesValues = new List<double>();

            if (cb_showSOMorder.Checked || cb_showBestGen.Checked)
            {
                glCameraLayouts.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            }

            for (int p = 0; p < _populationSingle.Size; p++)
            {
                var curChromo = (ChromosomeLayoutSingle) (_populationSingle[p]);
                if (curChromo.IsovistField != null)
                {
                    var visualIsovistField = new GGrid(curChromo.IsovistField.AnalysisPoints,
                        curChromo.IsovistField.GetMeasure(), curChromo.IsovistField.CellSize);
                        // new GGrid(curChromo.IsovistField); 
                    var deltaP = new Vector2d(p*curChromo.BoundingRect.Width*dist,
                        iterat*curChromo.BoundingRect.Height*dist);
                    visualIsovistField.Move(deltaP);
                    glCameraLayouts.AllObjectsList.ObjectsToDraw.Add(visualIsovistField);

                    if (curChromo.Environment != null)
                        foreach (Poly2D envObj in curChromo.Environment)
                        {
                            var ePoly = new GPolygon(envObj);
                            ePoly.Move(deltaP);
                            glCameraLayouts.AllObjectsList.ObjectsToDraw.Add(ePoly);
                        }

                    if (curChromo.Gens != null)
                        foreach (GenBuildingLayout building in curChromo.Gens)
                        {
                            var bPoly = new GPrism(building.PolygonFromGen);
                            bPoly.Height = building.BuildingHeight;
                            bPoly.DeltaZ = 0.1f;
                            bPoly.BorderWidth = 2;
                            if (!building.Open) bPoly.Filled = true;
                            else
                            {
                                bPoly.Filled = false;
                                bPoly.BorderWidth = 1;
                                Vector4 col = ConvertColor.CreateColor4(Color.Gainsboro);
                                col.W = 0.1f; // alpha value
                                bPoly.BorderColor = col;
                            }
                            bPoly.FillColor = ConvertColor.CreateColor4(Color.White);
                            bPoly.Filled = true;
                            bPoly.Move(deltaP);
                            glCameraLayouts.AllObjectsList.ObjectsToDraw.Add(bPoly);
                        }

                    var borderPoly = new GPolygon(curChromo.Border);
                    borderPoly.Move(deltaP);
                    glCameraLayouts.AllObjectsList.ObjectsToDraw.Add(borderPoly);
                }
            }
            //if (cB_autoShot.Checked) SetGraphicContext(glControl1); //Screenshot();
            glCameraLayouts.Invalidate();
            glCameraLayouts.ZoomAll();
        }

        //============================================================================================================================================================================
        private void DrawLayout()
        {
            glCameraMainWindow.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            glCameraMainWindow.AllObjectsList.ObjectsToDraw.AddRange(_remainDrawingObjects);
            if (_constantDrawObjects != null)
                glCameraMainWindow.AllObjectsList.ObjectsToDraw.AddRange(_constantDrawObjects);

            //            if (_smartGrid != null)
            //              glCameraMainWindow.AllObjectsList.ObjectsToDraw.Remove(_smartGrid); 


            // -- assign analysis results to the smar grid --
            if (ControlParameters.VisualiseAnalysis == FieldForVisual.SolarAnalysis)
            {
                _smartGrid = _calculatorSolarAnalysis.SmartGrid;
                glCameraMainWindow.Invalidate();
            }
            else if (ControlParameters.VisualiseAnalysis == FieldForVisual.IsovistAnalysis)
            {
                if (_isovistField != null)
                {
                    _isovistField.DisplayMeasure = comboBox_Isovist.Text;
                    string measure = comboBox_Isovist.SelectedItem.ToString();
                    _smartGrid = new GGrid(_isovistField.AnalysisPoints, _isovistField.GetMeasure(measure),
                        _isovistField.CellSize);
                    glCameraMainWindow.Invalidate();
                }
            }
            if (_smartGrid != null)
                glCameraMainWindow.AllObjectsList.ObjectsToDraw.Add(_smartGrid);

            // -- draw the buildings --
            foreach (ChromosomeLayout layout in _allLayouts)
            {
                if (layout.Gens != null)
                    foreach (GenBuildingLayout building in layout.Gens)
                    {
                        var bPoly = new GPrism(building.PolygonFromGen);
                        if (ControlParameters.VisualiseAnalysis == FieldForVisual.IsovistAnalysis)
                        {
                            bPoly.Height = building.BuildingHeight;
                            bPoly.LevelHeight = ControlParameters.FloorHeigth;
                            bPoly.Identity = layout.Indentity;
                            bPoly.DeltaZ = 0.1f;
                            bPoly.BorderWidth = 5;
                        }
                        else if (ControlParameters.VisualiseAnalysis == FieldForVisual.SolarAnalysis)
                        {

                            bPoly.SFields = building.SideGrids;
                            bPoly.Height = building.BuildingHeight;
                            bPoly.LevelHeight = ControlParameters.FloorHeigth;
                            bPoly.Identity = layout.Indentity;
                            bPoly.DeltaZ = 0.1f;
                            bPoly.BorderWidth = 5;
                            //bPoly.FillColor = ConvertColor.CreateColor4(Color.Gray);
                        }
                        var distPoly = new GPolygon(building.PolyForCollission.BodyPoly);
                        distPoly.Filled = false;
                        distPoly.BorderWidth = 1;
                        distPoly.BorderColor = ConvertColor.CreateColor4(Color.Chartreuse);
                        glCameraMainWindow.AllObjectsList.ObjectsToDraw.Add(distPoly);

                        if (!building.Open) bPoly.Filled = true;
                        else
                        {
                            bPoly.Filled = false;
                            bPoly.BorderWidth = 1;
                            Vector4 col = ConvertColor.CreateColor4(Color.Gainsboro);
                            col.W = 0.1f; // alpha value
                            bPoly.BorderColor = col;
                        }

                        bPoly.FillColor = ConvertColor.CreateColor4(Color.White); //DimGray);
                        glCameraMainWindow.AllObjectsList.ObjectsToDraw.Add(bPoly);
                    }
                var borderPoly = new GPolygon(layout.Border);
                glCameraMainWindow.AllObjectsList.ObjectsToDraw.Add(borderPoly);
            }
            glCameraMainWindow.Invalidate();
            // glCameraMainWindow.ZoomAll();
        }

        //============================================================================================================================================================================
        private void DrawMultiLayout()
        {
            glCameraMainWindow.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            glCameraMainWindow.AllObjectsList.ObjectsToDraw.AddRange(_remainDrawingObjects);
            if (_constantDrawObjects != null)
                glCameraMainWindow.AllObjectsList.ObjectsToDraw.AddRange(_constantDrawObjects);
            if (_smartGrid != null)
                glCameraMainWindow.AllObjectsList.ObjectsToDraw.Add(_smartGrid); //new GGridOld(_isovistField));
            //if (_isovistField != null) glCameraMainWindow.AllObjectsList.ObjectsToDraw.Add(new GGrid(_isovistField));
            foreach (ChromosomeLayout layout in _activeMultiLayout.SubChromosomes)
            {
                if (layout.Gens != null)
                    foreach (GenBuildingLayout building in layout.Gens)
                    {
                        var bPoly = new GPrism(building.PolygonFromGen);
                        if (ControlParameters.VisualiseAnalysis == FieldForVisual.IsovistAnalysis)
                        {
                            bPoly.Height = building.BuildingHeight;
                            bPoly.LevelHeight = ControlParameters.FloorHeigth;
                            bPoly.Identity = layout.Indentity;
                            bPoly.DeltaZ = 0.1f;
                            bPoly.BorderWidth = 5;
                        }
                        else if (ControlParameters.VisualiseAnalysis == FieldForVisual.IsovistAnalysis)
                        {
                            bPoly.SFields = building.SideGrids;
                            bPoly.Height = building.BuildingHeight;
                            bPoly.LevelHeight = ControlParameters.FloorHeigth;
                            bPoly.Identity = layout.Indentity;
                            bPoly.DeltaZ = 0.1f;
                            bPoly.BorderWidth = 5;
                        }

                        var distPoly = new GPolygon(building.PolyForCollission.BodyPoly);
                        distPoly.Filled = false;
                        distPoly.BorderWidth = 1;
                        distPoly.BorderColor = ConvertColor.CreateColor4(Color.Chartreuse);
                        glCameraMainWindow.AllObjectsList.ObjectsToDraw.Add(distPoly);

                        if (!building.Open) bPoly.Filled = true;
                        else
                        {
                            bPoly.Filled = false;
                            bPoly.BorderWidth = 1;
                            Vector4 col = ConvertColor.CreateColor4(Color.Gainsboro);
                            col.W = 0.1f; // alpha value
                            bPoly.BorderColor = col;
                        }

                        bPoly.FillColor = ConvertColor.CreateColor4(Color.White);
                        glCameraMainWindow.AllObjectsList.ObjectsToDraw.Add(bPoly);
                    }
                var borderPoly = new GPolygon(layout.Border);
                glCameraMainWindow.AllObjectsList.ObjectsToDraw.Add(borderPoly);
            }
            glCameraMainWindow.Invalidate();
        }

        //============================================================================================================================================================================
        private void PlotFitnessValues(int iterat)
        {
            if (iterat == 0) _fitnessValues = new List<List<double>>();
            // -- archive of all solutions --
            List<IMultiChromosome> archive = _population.Sel.GetArchive();

            // -- how many fitness criteria are there --
            int nrFitVal = 0;
            if (archive[0] != null) nrFitVal = archive[0].FitnessValues.Count();
            var fitValues = new List<double>(nrFitVal);
            for (int f = 0; f < nrFitVal; f++) fitValues.Add(double.MaxValue);

            // -- find minimal values --
            for (int f = 0; f < nrFitVal; f++)
            {
                for (int p = 0; p < archive.Count(); p++)
                {
                    IMultiChromosome curChromo = archive[p];
                    if (fitValues[f] > curChromo.FitnessValues[f])
                    {
                        fitValues[f] = curChromo.FitnessValues[f];
                    }
                }
            }

            _fitnessValues.Add(fitValues);
            TextRW.WriteFitnessValues(_fitnessValues);
        }

        //====================================================================================================
        private void ShowEsomMap()
        {
            //
            //string myUserSetting = Properties.Settings.Default.MyUserSetting;
            //string myApplicationSetting = Properties.Settings.Default.MyApplicationSetting;

            string strCmdText;
            // -- The path to ESOM commands
            string path = Settings.Default.PathToESOM; //"C:\\Users\\koenigr\\Documents\\ESOM\\bin\\";
            // The path to the output files
            string outputPath = Settings.Default.PathToOutput;

            // -- esom render
            strCmdText = path + "esomrnd.bat";

            int len = _globalLrn.Length > 3 ? _globalLrn.Length - 4 : 0;
            string name = _globalLrn.Remove(len, 4);
            string pathToLrn = Path.Combine(outputPath, _globalLrn);
            string pathToBm = Path.Combine(outputPath, name + ".bm");
            string pathToWts = Path.Combine(outputPath, name + ".wts");
            string pathToPng = Path.Combine(outputPath, name + ".png");
            // --------------
            // -- parameters:
            // -- path to the lrn file
            strCmdText += " -l " + pathToLrn;
            // -- path to the bm file
            strCmdText += " -b " + pathToBm;
            // -- path to the wts file
            strCmdText += " -w " + pathToWts;
            // -- background renderer
            strCmdText += " -B pmx";
            // -- name of color table
            strCmdText += " -r jet";
            // -- png output file
            strCmdText += " -p " + pathToPng;
            // -- zoom factor
            strCmdText += " -z 20";

            // -- execute the command
            ExecuteCommandSync(strCmdText);

            // -- show the rendering
            pictureBox_ESOM.Image = new Bitmap(pathToPng);
            pictureBox_ESOM.Invalidate();
        }

        #endregion


        //===================================================================================================================================================================
        //========= Analysis =========
        //===================================================================================================================================================================
        # region Analysis
        //============================================================================================================================================================================
        private void AnalyseScenario()
        {
            _remainDrawingObjects = new List<GeoObject>();
            glCameraMainWindow.AllObjectsList = new ObjectsAllList();
            glCameraMainWindow.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();

            if (_scenario.EnvironmentCloseNetwork != null)
                AnalyseNetwork(_scenario.EnvironmentCloseNetwork, glCameraMainWindow);
        }

        //============================================================================================================================================================================
        private void AnalyseGraph()
        {
            var qGraph = new UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>(false);
            if (_streetGenerator != null)
            {
                LineD[] subLines = _streetGenerator.Graph.ToLines();
                var netLines = new List<Line2D>();
                foreach (LineD line in subLines)
                {
                    Vector2d startPt = line.Start.ToVector2D();
                    Vector2d endPt = line.End.ToVector2D();
                    var newLine = new Line2D(startPt, endPt);
                    netLines.Add(newLine);
                    var newEdge = new UndirectedEdge<Vector2d>(line.Start.ToVector2D(), line.End.ToVector2D());
                    UndirectedEdge<Vector2d> reverseNewEdge =
                        qGraph.Edges.ToList()
                            .Find(x => x.Source == line.End.ToVector2D() && x.Target == line.Start.ToVector2D());
                    // --- a new edge (& vertex) is added ---                       
                    if (qGraph.ContainsVertex(line.End.ToVector2D()) && qGraph.ContainsVertex(line.Start.ToVector2D()))
                    {
                        if (reverseNewEdge == null)
                            qGraph.AddEdge(newEdge);
                    }
                    else
                    {
                        if (reverseNewEdge == null)
                            qGraph.AddVerticesAndEdge(newEdge);
                    }
                }

                glCameraMainWindow.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
                AnalyseNetwork(qGraph, glCameraMainWindow);
                CreateBlocks(netLines);
            }
        }

        //============================================================================================================================================================================
        private void CreateBlocks(List<Line2D> network)
        {
            //Subdivision networkGraphS = new Subdivision();
            var polygonizer = new Polygonizer();
            IGeometryFactory geometryFactory = new GeometryFactory();
            var graphLines = new List<IGeometry>();
            var rdr = new WKTReader(geometryFactory);
            LineString lineString;

            foreach (Line2D netLine in network)
            {
                if (netLine.Start == netLine.End)
                    continue;
                lineString = GeometryConverter.ToLineString(netLine);
                graphLines.Add(rdr.Read(lineString.ToString()));
            }

            var nodedContours = new List<IGeometry>();
            var geomNoder = new GeometryNoder(new PrecisionModel(0.75d));

            foreach (ILineString c in geomNoder.Node(graphLines)) //(Contours))
                nodedContours.Add(c);

            //var polygonizer = new NetTopologySuite.Operation.Polygonize.Polygonizer();
            polygonizer.Add(nodedContours);

            ICollection<IGeometry> polys = polygonizer.GetPolygons();

            Console.WriteLine("Polygons formed (" + polys.Count + "):");

            var buildPolygons = new List<Poly2D>();
            foreach (IPolygon netPoly in polys)
            {
                Coordinate[] coords = netPoly.Coordinates;
                Poly2D foundPoly = GeometryConverter.ToPoly2D(coords);
                buildPolygons.Add(foundPoly);
            }

            // ---- shrink the polygons -----
            var toBeRemovedPolys = new List<Poly2D>();
            foreach (Poly2D poly in buildPolygons)
            {
                if (poly.Offset(-1) == false)
                    toBeRemovedPolys.Add(poly);
                if (poly.Area < ControlParameters.MinBlockSize)
                    toBeRemovedPolys.Add(poly);
            }
            foreach (Poly2D poly in toBeRemovedPolys)
            {
                buildPolygons.Remove(poly);
            }


            // --- get the areas of the blocks---    
            Vector4[] blockColors;
            List<float> blockAreas = new List<float>();
            for (int i = 0; i < buildPolygons.Count; i++)
            {
                blockAreas.Add((float)buildPolygons[i].Area);
            }
            if (blockAreas.Any())
            {
                blockAreas = Tools.Normalize(blockAreas, 0, 1).ToList();
                blockColors = ConvertColor.CreateFFColor4(blockAreas.ToArray());
            }

            // --- add to list with drawable objects ---
            //foreach (var poly in buildPolygons)
            for (int i = 0; i < buildPolygons.Count; i++)
            {
                Poly2D poly = buildPolygons[i];
                var polyToDraw = new GPolygon(poly);
                polyToDraw.FillColor = ConvertColor.CreateColor4(Color.LightGray); //blockColors[i];//
                polyToDraw.Filled = false;
                polyToDraw.DeltaZ = -0.1f;
                polyToDraw.BorderWidth = 1;
                polyToDraw.BorderColor = ConvertColor.CreateColor4(Color.Black);
                glCameraMainWindow.AllObjectsList.Add(polyToDraw, true, false);
            }

            _blocks = buildPolygons;
        }

        //============================================================================================================================================================================
        private void CreateBlocks(Subdivision networkGraphS)
        {
            PointD[][] polygonsT = networkGraphS.ToPolygons();

            List<Poly2D> polygons = GeometryConverter.ToListPoly2D(polygonsT);

            // ---- shrink the polygons -----
            foreach (Poly2D poly in polygons)
            {
                if (poly.Offset(-4) == false)
                    polygons.Remove(poly);
            }

            // --- add to list with drawable objects ---
            foreach (Poly2D poly in polygons)
            {
                var polyToDraw = new GPolygon(poly);
                polyToDraw.FillColor = ConvertColor.CreateColor4(Color.LightGray);
                polyToDraw.Filled = true;
                glCameraMainWindow.AllObjectsList.Add(polyToDraw, true, false);
            }
        }

        //============================================================================================================================================================================
        private List<Line2D> AnalyseNetwork(List<Line2D> network, GLCameraControl viewControl)
        {
            var analyzer = new GraphAnalysis(network);
            float[] radius = { float.MaxValue }; //, 500, 150 }; //  
            analyzer.Calculate("angle", radius);

            Vector4[] ffColors = null;
            if (rB_fitChoice.Checked)
                ffColors = ConvertColor.CreateFFColor4(analyzer.ResultsAngular.GetNormChoiceArray()[0]); //ResultsAngular.GetNormCentralityStepsArray()[0]);
            //if (rB_fitChoice.Checked) ffColors = ConvertColor.CreateFFColor4(analyzer.ResultsAngular.GetNormChoiceArray()[0]);
            if (rB_fitCentral.Checked)
                ffColors = ConvertColor.CreateFFColor4(analyzer.ResultsAngular.GetNormCentralityMetricArray()[0]);//ResultsAngular.GetNormCentralityMetricArray()[0]);
            // GetNormCentralityStepsArray()[0]); //

            Vector2d[] nodes = analyzer.Network.Vertices.ToArray();
            UndirectedEdge<Vector2d>[] tempEdges = analyzer.Network.Edges.ToArray();
            var edges = new List<Line2D>();
            foreach (var tempEdge in tempEdges)
            {
                edges.Add(new Line2D(tempEdge.Source, tempEdge.Target));
            }

            DrawAnalysis(ffColors, edges.ToArray(), nodes, new Vector2d(0, 0), viewControl);
            //network = null;
            //glCameraMainWindow.ZoomAll();
            return edges;
        }

        //============================================================================================================================================================================
        private List<Line2D> AnalyseNetwork(UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> network, GLCameraControl viewControl)
        {
            Graph fwInverseGraph;
            ShortestPath shortPahtesAngular;
            Graph fwGraph;
            //------------------------------------------------------------------------------//
            fwGraph = GraphTools.GenerateFWGpuGraph(network);
            fwInverseGraph = GraphTools.GenerateInverseDoubleGraph(fwGraph);
            shortPahtesAngular = new ShortestPath(network); //, realAngle);
            shortPahtesAngular.Evaluate(new ToolStripProgressBar(), fwGraph, fwInverseGraph);
            //  null);//FloydWarshall();//Dijikstra();//

            Vector2d[] nodes = network.Vertices.ToArray();
            UndirectedEdge<Vector2d>[] tempEdges = network.Edges.ToArray();
            var edges = new List<Line2D>();
            foreach (var tempEdge in tempEdges)
            {
                edges.Add(new Line2D(tempEdge.Source, tempEdge.Target));
            }
            Vector4[] ffColors = null;
            if (rB_fitChoice.Checked)
                ffColors = ConvertColor.CreateFFColor4(shortPahtesAngular.GetNormChoiceArray()[0]);
            if (rB_fitCentral.Checked)
                ffColors = ConvertColor.CreateFFColor4(shortPahtesAngular.GetNormCentralityMetricArray()[0]);

            DrawAnalysis(ffColors, edges.ToArray(), nodes, new Vector2d(0, 0), viewControl);

            fwGraph.ClearGraph();
            fwGraph.Dispose();
            fwInverseGraph.Dispose();

            //glCameraMainWindow.ZoomAll();
            return edges;
        }

        //==============================================================================================
        public void SolarAnalysis()
        {
            //CalculatorSolarAnalysis curSolarAnalysis = null;
            _calculatorSolarAnalysis = null;
            TaskCompletionSource<Exception> exHandler = new TaskCompletionSource<Exception>();
            var bworker = new BackgroundWorker();
            //bworker.DoWork += (sender, args) =>
                {
                    try
                    {                       
                        if (_activeMultiLayout != null)
                        {
                            _calculatorSolarAnalysis = new CalculatorSolarAnalysis(_activeMultiLayout, Settings.Default.PathToHDRI);
                        }
                        else if (_activeLayout != null)
                        {
                            _calculatorSolarAnalysis = new CalculatorSolarAnalysis(_activeLayout, Settings.Default.PathToHDRI);
                        }
                        if (_calculatorSolarAnalysis != null)
                            _calculatorSolarAnalysis.Compute(RunVia, -1).Wait();
                        exHandler.SetResult(null);
                    }
                    catch (Exception ex)
                    {
                        exHandler.SetResult(ex);
                    }
                };
            
            //bworker.RunWorkerCompleted += (sender, args) =>
            {
                //LucyManager.Manager.ShowLucyIdle();
                //var err = exHandler.Task.Result;
                //if (err != null)
                //{
                //    var er = err.InnerException == null ? err : err.InnerException;
                //    LucyManager.Manager.Log("Error occured while trying to compute: " + er.Message +
                //        "\n" + er.StackTrace);
                //}
//cm                _smartGrid = _solarAnalysisLayout.SmartGrid;
//cm                DrawLayout();
                
            }//;
            //LucyManager.Manager.ShowLucyInProgress();
            //bworker.RunWorkerAsync();
 
 
        }

        //==============================================================================================       
//        public void SolarAnalysis()
//        {
//            _analysisType = AnalysisType.Solar;
//            GenBuildingLayout[] buildings = new GenBuildingLayout[0];
//            IEnumerable<Vector3d> gridNormales = null;
//            //List<Vector3d> gridNormales = new List<OpenTK.Vector3d>();
//            IEnumerable<Vector3d> gridPoints = null;
//            var polygons = new List<IPolygon>();

//            float cellSize = ControlParameters.IsovistCellSize;

//            // --- Analysis for multiple blocks -----------------------------------------------------------------------
//            if (_activeMultiLayout != null)
//            {

//                //if ( !_smartGrid.SFDerived) 
//                {
//                    var sf = new ScalarField(_activeMultiLayout.Border.PointsFirstLoop
//                                           .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
//                                           cellSize, new Vector3d(0, 0, 1));
//                    _smartGrid = new GGrid(sf, "solarAnalysis");
//                }
//                //else if (_smartGrid.ScalarField == null)
//                //{
//                //    _smartGrid.ScalarField = new ScalarField(_activeMultiLayout.Border.PointsFirstLoop
//                //                            .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
//                //                            cellSize, new Vector3d(0, 0, 1));
//                //}

//                //for (int i = 0; i < _smartGrid.ScalarField.GridSize; i++) { gridNormales.Add(_smartGrid.ScalarField.Normal); }
//                gridNormales = Enumerable.Repeat(_smartGrid.ScalarField.Normal, _smartGrid.ScalarField.GridSize);
//                gridPoints = _smartGrid.ScalarField.Grid.ToList();

//                foreach (ChromosomeLayout curChromo in _activeMultiLayout.SubChromosomes)
//                {
//                    buildings = curChromo.Gens.ToArray();

//                    foreach (GenBuildingLayout building in buildings)
//                    {
//                        var bpoints = building.PolygonFromGen.PointsFirstLoop;
//                        var bcenter = new Vector3d(building.PolygonFromGen.PointsFirstLoop.Mean());
//                        bcenter.Z = building.BuildingHeight / 2;
//                        building.SFDerived = true;
//                        building.FieldTypeName = "solarAnalysis";
//                        building.SideGrids = new ScalarField[1 + bpoints.Length];
//                        building.SideGrids[0] = new ScalarField(bpoints
//                                                .Select(v => new Vector3d(v.X, v.Y, building.BuildingHeight)).ToArray(),
//                                                cellSize, new Vector3d(0, 0, 1));
//                        for (int i = 1; i <= bpoints.Length; i++)
//                        {
//                            var tpoints = new Vector3d[] {
//                            new Vector3d(bpoints[i-1].X, bpoints[i-1].Y, 0),
//                            new Vector3d(bpoints[i-1].X, bpoints[i-1].Y, building.BuildingHeight),
//                            new Vector3d(bpoints[i % bpoints.Length].X, bpoints[i % bpoints.Length].Y, building.BuildingHeight),
//                            new Vector3d(bpoints[i % bpoints.Length].X, bpoints[i % bpoints.Length].Y, 0)
//                        };
//                            building.SideGrids[i] = new ScalarField(tpoints, cellSize, tpoints.Mean() - bcenter);


//                        }
//                        // next we have to put all the grids together into one...
//                        foreach (var field in building.SideGrids)
//                        {
              
//                            //gridPoints.AddRange(field.Grid);
//                            gridPoints = gridPoints.Concat(field.Grid);
//                            gridNormales = gridNormales.Concat(Enumerable.Repeat(field.Normal, field.GridSize));
//                            //for (int i = 0; i < field.Grid.Count(); i++) { gridNormales.Add(_smartGrid.ScalarField.Normal); }
//                        }
//                    }
//                }
//                _solarAnalysis = new SolarAnalysis(gridPoints, gridNormales);
//                _solarAnalysis.ClearGeometry();
//                //_isovistBorder = _borderPoly.Poly.Clone();
//                //_isovistBorder.Offset(1.2);

//                foreach (ChromosomeLayout curChromo in _activeMultiLayout.SubChromosomes)
//                {
//                    buildings = curChromo.Gens.ToArray();
//                    // --- add the buildings ---
//                    var converter = new Pg3DNtsInterop.Pg3DNtsConverter();
//                    foreach (GenBuildingLayout curRect in buildings)
//                    {
//                        if (!curRect.Open)
//                        {
//                            Poly2D p2D = curRect.PolygonFromGen;
//                            var pvv = new CGeom3d.Vec_3d[1][];
//                            var pv = new CGeom3d.Vec_3d[p2D.PointsFirstLoop.Length];
//                            pvv[0] = pv;
//                            int z = 0;
//                            foreach (Vector2d v2D in p2D.PointsFirstLoop)
//                                pv[z++] = new CGeom3d.Vec_3d(v2D.X, v2D.Y, curRect.BuildingHeight);

//                            CGeom3d.Vec_3d[][] pvvBottom = p2D.PointsVec3D;
//                            _solarAnalysis.AddPolygon(pvvBottom, 1.0f);
//                            polygons.Add((IPolygon)converter.PureGeomToPolygon(pvvBottom));
//                            _solarAnalysis.AddPolygon(pvv, 1.0f);
//                            polygons.Add((IPolygon)converter.PureGeomToPolygon(pvv));

//                            for (int i = 0; i < pv.Length; ++i)
//                            {
//                                int j = i + 1;
//                                if (j >= pv.Length) j -= pv.Length;

//                                var pvvSide = new CGeom3d.Vec_3d[1][];
//                                var pvSide = new CGeom3d.Vec_3d[4];
//                                pvvSide[0] = pvSide;
//                                pvSide[0] = pvvBottom[0][i];
//                                pvSide[1] = pvvBottom[0][j];
//                                pvSide[2] = pv[j];
//                                pvSide[3] = pv[i];

//                                _solarAnalysis.AddPolygon(pvvSide, 1.0f);
//                                polygons.Add((IPolygon)converter.PureGeomToPolygon(pvvSide));
//                            }
//                        }
//                    }
//                }
//            }

//            // --- Analysis for one block only -----------------------------------------------------------------------
//            else if (_activeLayout != null)
//            {
//                if (_smartGrid == null || !_smartGrid.SFDerived) 
//                //if ( !_smartGrid.SFDerived) 
//                {
//                    var sf = new ScalarField(_activeLayout.Border.PointsFirstLoop
//                        .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
//                        cellSize, new Vector3d(0, 0, 1));
//                    _smartGrid = new GGrid(sf, "solarAnalysis");
//                }
//                else if (_smartGrid.ScalarField == null)
//                {
//                    _smartGrid.ScalarField = new ScalarField(_activeLayout.Border.PointsFirstLoop
//                        .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
//                        cellSize, new Vector3d(0, 0, 1));
//                }

//                //for (int i = 0; i < _smartGrid.ScalarField.GridSize; i++) { gridNormales.Add(_smartGrid.ScalarField.Normal); }
//                gridNormales = Enumerable.Repeat(_smartGrid.ScalarField.Normal, _smartGrid.ScalarField.GridSize);
//                //gridPoints = _smartGrid.ScalarField.Grid.ToList();
            
//                gridPoints = _smartGrid.ScalarField.Grid;
//                buildings = _activeLayout.Gens.ToArray();

//                foreach (GenBuildingLayout building in buildings)
//                {
//                    var bpoints = building.PolygonFromGen.PointsFirstLoop;
//                    var bcenter = new Vector3d(building.PolygonFromGen.PointsFirstLoop.Mean());
//                    bcenter.Z = building.BuildingHeight/2;
//                    building.SFDerived = true;
//                    building.FieldTypeName = "solarAnalysis";
//                    building.SideGrids = new ScalarField[1 + bpoints.Length];
//                    building.SideGrids[0] = new ScalarField(bpoints
//                        .Select(v => new Vector3d(v.X, v.Y, building.BuildingHeight)).ToArray(),
//                        cellSize, new Vector3d(0, 0, 1));
//                    for (int i = 1; i <= bpoints.Length; i++)
//                    {
//                        var tpoints = new Vector3d[]
//                        {
//                            new Vector3d(bpoints[i - 1].X, bpoints[i - 1].Y, 0),
//                            new Vector3d(bpoints[i - 1].X, bpoints[i - 1].Y, building.BuildingHeight),
//                            new Vector3d(bpoints[i%bpoints.Length].X, bpoints[i%bpoints.Length].Y, building.BuildingHeight),
//                            new Vector3d(bpoints[i%bpoints.Length].X, bpoints[i%bpoints.Length].Y, 0)
//                        };
//                        building.SideGrids[i] = new ScalarField(tpoints, cellSize, tpoints.Mean() - bcenter);


//                    }
//                    // next we have to put all the grids together into one...
//                    foreach (var field in building.SideGrids)
//                    {
//                        gridPoints = gridPoints.Concat(field.Grid);
//                        //gridPoints.AddRange(field.Grid);
//                        //for (int i = 0; i < field.Grid.Count(); i++) { gridNormales.Add(_smartGrid.ScalarField.Normal); }
//                        gridNormales = gridNormales.Concat(Enumerable.Repeat(field.Normal, field.GridSize));
//                    }
//                }

//                _solarAnalysis = new SolarAnalysis(gridPoints, gridNormales);
//                _solarAnalysis.ClearGeometry();
//                //_isovistBorder = _borderPoly.Poly.Clone();
//                //_isovistBorder.Offset(1.2);

//                // --- add the buildings ---              
//                var converter = new Pg3DNtsInterop.Pg3DNtsConverter();
//                foreach (GenBuildingLayout curRect in buildings)
//                {
//                    if (!curRect.Open)
//                    {
//                        Poly2D p2D = curRect.PolygonFromGen;
//                        var pvv = new CGeom3d.Vec_3d[1][];
//                        var pv = new CGeom3d.Vec_3d[p2D.PointsFirstLoop.Length];
//                        pvv[0] = pv;
//                        int z = 0;
//                        foreach (Vector2d v2D in p2D.PointsFirstLoop)
//                            pv[z++] = new CGeom3d.Vec_3d(v2D.X, v2D.Y, curRect.BuildingHeight);

//                        CGeom3d.Vec_3d[][] pvvBottom = p2D.PointsVec3D;
//                        _solarAnalysis.AddPolygon(pvvBottom, 1.0f);
//                        polygons.Add((IPolygon) converter.PureGeomToPolygon(pvvBottom));
//                        _solarAnalysis.AddPolygon(pvv, 1.0f);
//                        polygons.Add((IPolygon) converter.PureGeomToPolygon(pvv));

//                        for (int i = 0; i < pv.Length; ++i)
//                        {
//                            int j = i + 1;
//                            if (j >= pv.Length) j -= pv.Length;

//                            var pvvSide = new CGeom3d.Vec_3d[1][];
//                            var pvSide = new CGeom3d.Vec_3d[4];
//                            pvvSide[0] = pvSide;
//                            pvSide[0] = pvvBottom[0][i];
//                            pvSide[1] = pvvBottom[0][j];
//                            pvSide[2] = pv[j];
//                            pvSide[3] = pv[i];

//                            _solarAnalysis.AddPolygon(pvvSide, 1.0f);
//                            polygons.Add((IPolygon) converter.PureGeomToPolygon(pvvSide));
//                        }
//                    }
//                }              
//            }
//            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//            // --- Run the calculation ---
//            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//            if ((_activeLayout != null) || (_activeMultiLayout != null))
//            {
//                var saResults = Enumerable.Empty<double>();
/* later
//                var mpol = new MultiPolygon(polygons.ToArray());
//                var saResults = Enumerable.Empty<double>();
//                LucyManager.Manager.ShowLucyInProgress();
//                TaskCompletionSource<Exception> exHandler = new TaskCompletionSource<Exception>();
//                var bworker = new BackgroundWorker();
//                LucyManager.Manager.Log("Solar Analysis starting its work " + (LucyManager.Manager.ViaLucy ? "via Luci" : "locally"));
//                if (LucyManager.Manager.ViaLucy)
//                    bworker.DoWork += (sender, args) =>
//                    {
//                        try
//                        {
//                            saResults = LucyManager.Manager.DoSolarAnalysis(mpol, gridPoints, gridNormales).Result;
//                            exHandler.SetResult(null);
//                        }
//                        catch (Exception ex)
//                        {
//                            exHandler.SetResult(ex);
//                        }
//                    };
//                else
//                    bworker.DoWork += (sender, args) =>
//                    {*/
////                        exHandler.SetResult(null);
//                        _solarAnalysis.Calculate(CalculationMode.illuminance_klux);
//                        saResults = _solarAnalysis.Results;
//  /*                  };

//                bworker.RunWorkerCompleted += (sender, args) =>
//                {*/

////                    var err = exHandler.Task.Result;
////                    if (err != null)
////                    {
////                        LucyManager.Manager.ShowLucyIdle();
////                        var er = err.InnerException == null ? err : err.InnerException;
////                        LucyManager.Manager.Log("Error occured while trying to compute: " + er.Message +
////                            "\n" + er.StackTrace);
////                        return;
////                    }

//                    _smartGrid.ScalarField.GenerateTexture(saResults.Take(_smartGrid.ScalarField.GridSize), "solarAnalysis");
//                    _smartGrid.GenerateTexture();
//                    saResults = saResults.Skip(_smartGrid.ScalarField.GridSize);

//                    if (_activeMultiLayout != null)
//                    {
//                        foreach (ChromosomeLayout curChromo in _activeMultiLayout.SubChromosomes)
//                        {
//                            buildings = curChromo.Gens.ToArray();
//                            foreach (GenBuildingLayout building in buildings)
//                            {

//                                foreach (var field in building.SideGrids)
//                                {
//                                    field.GenerateTexture(saResults.Take(field.GridSize), "solarAnalysis");
//                                    saResults = saResults.Skip(field.GridSize);
//                                }
//                            }
//                        }
//                    }
//                    else
//                    {
//                        buildings = _activeLayout.Gens.ToArray();
//                        foreach (GenBuildingLayout building in buildings)
//                        {

//                            foreach (var field in building.SideGrids)
//                            {
//                                field.GenerateTexture(saResults.Take(field.GridSize), "solarAnalysis");
//                                saResults = saResults.Skip(field.GridSize);
//                            }
//                        }
//                    }

//                    propertyGrid2.SelectedObject = _solarAnalysis;
//                    DrawLayout();
///*                    LucyManager.Manager.Log("Solar Analysis complete ");
//                    LucyManager.Manager.ShowLucyIdle();
//                };
//                bworker.RunWorkerAsync();
//*/
//            }
//            //else
//            //    DrawLayout();
//        }

        //============================================================================================================================================================================
        private void IsovistAnalye()
        {
            _analysisType = AnalysisType.Isovist;
            //_obstLines = new List<Line2D>();
            //_obstRects = new List<Poly2D>();
            //var rects = new List<GenBuildingLayout>();

            /* *ac* Poly2D localIsovistField = null;*/
            float extField = 0.03f;
            int id = -1;

            // --- Multi Layout = many blocks -------------------------------------------------------------
            if (_activeMultiLayout != null)
            {
                Poly2D origBorder = _activeMultiLayout.Border.Clone();
                _isovistBorder = _activeMultiLayout.Border.Clone(); // _border.Clone(); 
                _isovistBorder.Offset(Math.Sqrt(_activeMultiLayout.Border.Area) * extField);
                _activeMultiLayout.Border = _isovistBorder;

                //SolarAnalysis2();

                var fitnessFunction = new FitnessFunctionIsovist(RunVia);//, RunVia_Solar);//, Settings.Default.PathToHDRI);
                _activeMultiLayout.Evaluate(fitnessFunction);
                _isovistField = _activeMultiLayout.IsovistField;
                _activeMultiLayout.Border = origBorder;

                id = _activeMultiLayout.Indentity;

            }
            // --- One Block only -------------------------------------------------------------------------
            else if (_activeLayout != null)
            {
                Poly2D origBorder = _borderPoly.Poly.Clone();
                _isovistBorder = _borderPoly.Poly.Clone();
                _isovistBorder.Offset(Math.Sqrt(_borderPoly.Poly.Area) * extField);
                _activeLayout.Border = _isovistBorder;

                //SolarAnalysis2();

                var fitnessFunction = new FitnessFunctionIsovist(RunVia);
                _activeLayout.Evaluate(fitnessFunction);
                _isovistField = _activeLayout.IsovistField;
                _activeLayout.Border = origBorder;

                id = _activeLayout.Indentity;
            }

            # region ToBeDeleted
            /* test environmental polygons
            else if (_constantDrawObjects != null)
            {
                foreach (GeoObject testObj in _constantDrawObjects)
                {
                    if (testObj is GPolygon)
                    {
                        var envPoly = (GPolygon)testObj;
                        if (envPoly.Poly.Distance(_isovistBorder) <= 0)
                        {
                            _obstLines.AddRange(envPoly.Edges.ToList());
                            _obstRects.Add(envPoly.Poly);
                        }
                    }
                }
            }*/

            //else
            //    return;

            //showResultsSolar(id);
            //showResultsIsovist(id);
            //DrawLayout();

            // --------------------------------------
            /* cm            _obstLines.AddRange(_isovistBorder.Edges);
                        float cellSize = ControlParameters.IsovistCellSize;
                        List<Vector2d> gridPoints = PointsForIsovist(_isovistBorder, _obstRects, cellSize);
                        // *ac* List<Vector2d> gridPoints = PointsForIsovist(localIsovistField, _obstRects, cellSize);

                        _isovistField = new IsovistAnalysis(gridPoints, _obstLines, cellSize);
                        _isovistField.DisplayMeasure = comboBox_Isovist.Text;
                        TimeSpan calcTime1, calcTime2;
                        DateTime start = DateTime.Now;
            */
            // -- run localr --
            //_isovistField.Calculate(0.05f);
            ////////

            /* cm          
                       if (_activeMultiLayout != null)
                       {
                           _activeMultiLayout.Border = _isovistBorder;
                           var fitnessFunction = new FitnessFunctionMultiLayout(RunVia);
                           _activeMultiLayout.Evaluate(fitnessFunction, true);
                       }
                       else if (_activeLayout != null)
                       {
                           _activeLayout.Border = _isovistBorder;
                           var fitnessFunction = new FitnessFunctionLayout(RunVia);
                           _activeLayout.Evaluate(fitnessFunction, true);
                           //IsovistAnalysis ia = _activeLayout.IsovistField;
                       }
           */

            //cm            DateTime end = DateTime.Now;
            //cm            calcTime1 = end - start;

            /*
            string measure = "";
            if (comboBox_Isovist.SelectedValue != null)
                measure = comboBox_Isovist.SelectedValue.ToString();
            //cm_smartGrid = new GGrid(gridPoints, _isovistField.GetMeasure(measure), _isovistField.CellSize);
            if (_isovistField != null)
            {
                _smartGrid = new GGrid(_isovistField.AnalysisPoints, _isovistField.GetMeasure(measure), _isovistField.CellSize);
                propertyGrid2.SelectedObject = _isovistField;
            }

            DrawLayout();
            
            if (id >= 0)
            {
                ConfigSettings config = ServiceManager.GetConfig4ID(id);
                if (config != null)
                {
                    int[] ids = new int[] { config.objID, config.scenarioID };
                    MemoryStream ms = new MemoryStream();
                    System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    bf.Serialize(ms, ids);
                    if (ServiceManager.mqttClient != null)
                    {
                        ServiceManager.mqttClient.Publish("SelectionChanged", ms.ToArray());
                    }
                }
            }*/
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // calculation
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /* later
                        var rez = "";
                        LucyManager.Manager.ShowLucyInProgress();
                        TaskCompletionSource<Exception> exHandler = new TaskCompletionSource<Exception>();
                        var bworker = new BackgroundWorker();
                        LucyManager.Manager.Log("Solar Analysis starting its work " + (LucyManager.Manager.ViaLucy ? "via Luci" : "locally"));
                        if (LucyManager.Manager.ViaLucy)
                            bworker.DoWork += (sender, args) =>
                            {
                                try
                                {
                                    rez = LucyManager.Manager.DoIsovistAnalysis(rects, _isovistBorder, gridPoints).Result;
                                    exHandler.SetResult(null);
                                }
                                catch (Exception ex)
                                {
                                    exHandler.SetResult(ex);
                                }
                                //curChromo.IsovistField = _isovistField;
                            };
                        else
                            bworker.DoWork += (sender, args) =>
                            {
                                exHandler.SetResult(null);
                                _isovistField.Calculate(0.05f);
                            };

                        bworker.RunWorkerCompleted += (sender, args) =>
                        {
                            var err = exHandler.Task.Result;
                            if (err != null)
                            {
                                LucyManager.Manager.ShowLucyIdle();
                                var er = err.InnerException == null ? err : err.InnerException;
                                LucyManager.Manager.Log("Error occured while trying to compute: " + er.Message +
                                    "\n" + er.StackTrace);
                                return;
                            }
                            if (LucyManager.Manager.ViaLucy)
                                _isovistField.AnalyseLucyResults(rez);
                            _isovistField.Finished = true;
                            string measure = "";
                            if (comboBox_Isovist.SelectedValue != null)
                                measure = comboBox_Isovist.SelectedValue.ToString();
                            _smartGrid = new GGrid(gridPoints, _isovistField.GetMeasure(measure), _isovistField.CellSize);

                            propertyGrid2.SelectedObject = _isovistField;

                            DrawLayout();
                            LucyManager.Manager.Log("Isovist Analysis complete ");
                            LucyManager.Manager.ShowLucyIdle();
                        };
                        bworker.RunWorkerAsync();
            
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        // end of calculation
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            */

            // -- run via server -- // approximately 3-4 times? slower on local maschine...
            //start = DateTime.Now;
            //_isovistField.CalculateViaService(borderRect, 5, _obstLines, _obstRects, _isovistBorder, 0.05f);

            //end = DateTime.Now; ;
            //calcTime2 = end - start;

            //Debug.WriteLine("grid with " + _isovistField.NumberOfActiveCells.ToString() + " active cells: ");
            //Debug.WriteLine("local calculation time 1: " + calcTime1.TotalMilliseconds.ToString() + " ms");
            //Debug.WriteLine("server calculation time 2: " + calcTime2.TotalMilliseconds.ToString() + " ms");

            //_isovistField.WriteToGridValues();

            //DateTime end = DateTime.Now;
            //calcTime1 = end - start;
            # endregion;
        }

        # region ToBeDeleted2
        //private void ShowResultsIsovist(int id=-1)
        //{
        //    if (_activeMultiLayout != null)
        //        _isovistField = _activeMultiLayout.IsovistField;
        //    else if (_activeLayout != null) 
        //        _isovistField = _activeLayout.IsovistField;

        //    string measure = "";
        //    if (comboBox_Isovist.SelectedValue != null)
        //        measure = comboBox_Isovist.SelectedValue.ToString();

        //    _smartGrid = new GGrid(_isovistField.AnalysisPoints, _isovistField.GetMeasure(measure), _isovistField.CellSize);

        //    propertyGrid2.SelectedObject = _isovistField;

        //    if (id >= 0)
        //    {
        //        ConfigSettings config = ServiceManager.GetConfig4ID(id);
        //        if (config != null)
        //        {
        //            int[] ids = new int[] { config.objID, config.scenarioID };
        //            MemoryStream ms = new MemoryStream();
        //            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        //            bf.Serialize(ms, ids);
        //            if (ServiceManager.mqttClient != null)
        //            {
        //                try
        //                {
        //                    ServiceManager.mqttClient.Publish("SelectionChanged", ms.ToArray());
        //                }
        //                catch (Exception ex) { }
        //            }
        //        }
        //    }
        //}

        //        private void ShowResultsSolar(int id = -1)
        //        {
        //            if (RunVia_Solar == CalculationType.Method.Local)
        //                return;

        //            IEnumerable<double> saResults = null;
        //            _smartGrid = _calculatorSolarAnalysis.SmartGrid;


        //                if (_activeMultiLayout != null)
        //                    saResults = _activeMultiLayout.SolarAnalysisResults;
        //                else if (_activeLayout != null)
        //                    saResults = _activeLayout.SolarAnalysisResults;

        //            if (saResults == null)
        //            {
        //               return;
        //            }


        //            GenBuildingLayout[] buildings = new GenBuildingLayout[0];
        //            float cellSize = ControlParameters.IsovistCellSize;


        //            // -- store the results for the plane of the chromosomes --
        //            if (_activeMultiLayout != null)
        //            {
        //                float extField = 0.03f;
        //                Poly2D origBorder = _activeMultiLayout.Border.Clone();
        //                Poly2D _isoBorder = _activeMultiLayout.Border.Clone(); // _border.Clone(); 
        //                _isoBorder.Offset(Math.Sqrt(_activeMultiLayout.Border.Area) * extField);
        //                var sf = new ScalarField(_isoBorder.PointsFirstLoop
        //                                           .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
        //                                           cellSize, new Vector3d(0, 0, 1));

        //               // _smartGrid = new GGrid(sf, "solarAnalysis");
        //                //_solarAnalysisLayout.SetSmartGrid(_smartGrid);
        //                _smartGrid = _calculatorSolarAnalysis.SmartGrid;
        //                try
        //                {
        //                    _smartGrid.ScalarField.GenerateTexture(saResults.Take(_smartGrid.ScalarField.GridSize), "solarAnalysis");
        //                    _smartGrid.GenerateTexture();
        //             //       glCameraMainWindow.AllObjectsList.ObjectsToDraw.Add(_smartGrid);
        //                }
        //                catch (Exception ex) { }

        //                _activeMultiLayout.SolarAnalysisResults = saResults.Take(_smartGrid.ScalarField.GridSize);
        //                if (_activeLayout != null)
        //                    _activeLayout.SolarAnalysisResults = saResults.Take(_smartGrid.ScalarField.GridSize);

        ///*
        //                _activeMultiLayout.SolarAnalysisResults = saResults;
        //                if (_activeLayout != null)
        //                    _activeLayout.SolarAnalysisResults = saResults; */
        //            }
        //            else
        //            {
        //                /*                _solarAnalysisLayout = new CalculatorSolarAnalysis(_activeLayout);

        //                                var sf = new ScalarField(_activeLayout.Border.PointsFirstLoop
        //                                       .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
        //                                       cellSize, new Vector3d(0, 0, 1));
        //                                _smartGrid = new GGrid(sf, "solarAnalysis");*/

        //                _smartGrid = _calculatorSolarAnalysis.SmartGrid;

        //                if (_smartGrid == null)
        //                {
        //                    var sf = new ScalarField(_activeLayout.Border.PointsFirstLoop
        //                       .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
        //                       cellSize, new Vector3d(0, 0, 1));
        //                    _smartGrid = new GGrid(sf, "solarAnalysis");
        //                }

        //                _smartGrid.ScalarField.GenerateTexture(saResults.Take(_smartGrid.ScalarField.GridSize), "solarAnalysis");
        //                _smartGrid.GenerateTexture();

        //                _activeLayout.SolarAnalysisResults = saResults.Take(_smartGrid.ScalarField.GridSize);
        //            }

        //            saResults = saResults.Skip(_smartGrid.ScalarField.GridSize);


        //            if (_activeMultiLayout != null)
        //            {
        //                foreach (ChromosomeLayout curChromo in _activeMultiLayout.SubChromosomes)
        //                {
        //                    buildings = curChromo.Gens.ToArray();
        //                    foreach (GenBuildingLayout building in buildings)
        //                    {
        //                        foreach (var field in building.SideGrids)
        //                        {
        //                            field.GenerateTexture(saResults.Take(field.GridSize), "solarAnalysis");
        //                            saResults = saResults.Skip(field.GridSize);
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {

        //                buildings = _activeLayout.Gens.ToArray();
        //     /*             foreach (GenBuildingLayout building in buildings)
        //                {
        //                    var bpoints = building.PolygonFromGen.PointsFirstLoop;
        //                    var bcenter = new Vector3d(building.PolygonFromGen.PointsFirstLoop.Mean());
        //                    bcenter.Z = building.BuildingHeight / 2;
        //                    building.SFDerived = true;
        //                    building.FieldTypeName = "solarAnalysis";
        //                    building.SideGrids = new ScalarField[1 + bpoints.Length];
        //                    building.SideGrids[0] = new ScalarField(bpoints
        //                                            .Select(v => new Vector3d(v.X, v.Y, building.BuildingHeight)).ToArray(),
        //                                            cellSize, new Vector3d(0, 0, 1));
        //                    for (int i = 1; i <= bpoints.Length; i++)
        //                    {
        //                        var tpoints = new Vector3d[] {
        //                            new Vector3d(bpoints[i-1].X, bpoints[i-1].Y, 0),
        //                            new Vector3d(bpoints[i-1].X, bpoints[i-1].Y, building.BuildingHeight),
        //                            new Vector3d(bpoints[i % bpoints.Length].X, bpoints[i % bpoints.Length].Y, building.BuildingHeight),
        //                            new Vector3d(bpoints[i % bpoints.Length].X, bpoints[i % bpoints.Length].Y, 0)
        //                        };
        //                        building.SideGrids[i] = new ScalarField(tpoints, cellSize, tpoints.Mean() - bcenter);
        //                    }
        //                }*/

        //                try
        //                {
        //                    foreach (GenBuildingLayout building in buildings)
        //                    {
        //                        foreach (var field in building.SideGrids)
        //                        {
        //                            field.GenerateTexture(saResults.Take(field.GridSize), "solarAnalysis");
        //                            saResults = saResults.Skip(field.GridSize);
        //                        }
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    Console.Out.WriteLine("rotz");
        //                }
        //            }

        //            MemoryStream ms = new MemoryStream();
        //            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        //            try
        //            {
        //                bf.Serialize(ms, _calculatorSolarAnalysis.SmartGrid.ScalarField.PointCoordinates);
        //            }
        //            catch (Exception ex) { }

        //            /*
        //            float _extField = 0.03f;
        //            Poly2D __isoBorder = _activeLayout.Border.Clone(); // _border.Clone(); 
        //            //__isoBorder.Offset(Math.Sqrt(_activeLayout.Border.Area) * _extField);
        //            Vector3d[] border =  __isoBorder.PointsFirstLoop.Select(v => new Vector3d(v.X, v.Y, 0)).ToArray();
        // //            bf.Serialize(ms, border);


        //            string measure = comboBox_Isovist.SelectedItem.ToString();
        //            GGrid ggg = new GGrid(_isovistField.AnalysisPoints, _isovistField.GetMeasure(measure),
        //                        _isovistField.CellSize);

        //            //bf.Serialize(ms, _isovistField.AnalysisPoints);

        ////           bf.Serialize(ms, _solarAnalysisLayout.SmartGrid);
        // //          bf.Serialize(ms, _smartGrid);
        //            */
        //            if (ServiceManager.mqttClient != null)
        //            {
        //                try
        //                {
        //                    ServiceManager.mqttClient.Publish("SmartGrid", ms.ToArray());
        //                }
        //                catch (Exception ex) { }
        //            }

        //            if (id >= 0)
        //            {
        //                ConfigSettings config = ServiceManager.GetConfig4ID2(id);
        //                if (config != null)
        //                {
        //                    int[] ids2 = new int[] { config.objID, config.scenarioID };
        //                    MemoryStream ms2 = new MemoryStream();
        //                    System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf2= new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        //                    bf2.Serialize(ms2, ids2);
        //                    if (ServiceManager.mqttClient != null)
        //                    {
        //                        try
        //                        {
        //                            ServiceManager.mqttClient.Publish("SelectionChangedSolar", ms2.ToArray());
        //                        }
        //                        catch (Exception ex) { }
        //                    }
        //                }
        //            }


        //        //    _smartGrid = _solarAnalysisLayout.SmartGrid;
        //        //    DrawLayout();
        //        }

        //============================================================================================================================================================================



        # endregion

        # endregion


        //===================================================================================================================================================================
        //========= Load/Save =========
        //===================================================================================================================================================================
        //============================================================================================================================================================================
        # region Load/Save

        private void SaveFitnessValues(string name)
        {
            string curName = name.Remove(name.Length - 4);
            curName += ".txt";
            var sw = new StreamWriter(curName);
            try
            {
                sw.WriteLine("Values for file:" + name);
                sw.WriteLine(_iterations + " : Generations");
                sw.WriteLine(_populationSize + " : Population Size");
                sw.WriteLine(ControlParameters.TreeDepth + " : Tree Depth");
                //var bestTree = (InstructionTreePisa) _population.BestChromosome;
                //string mode = (ControlParameters.Choice) ? "Choice" : "Centrality";
                sw.WriteLine(_span.ToString(@"hh\:mm\:ss") + " : Calculation Time");

                for (int i = 0; i < _fitnesLists.Count; i++)
                {
                    List<double> fitValues;
                    fitValues = _fitnesLists[i];
                    string valueLine = i + "; ";
                    for (int k = 0; k < fitValues.Count; k++)
                    {
                        double curValue = fitValues[k];
                        valueLine += curValue + "; ";
                    }
                    sw.WriteLine(valueLine); // + "; Generation " + i.ToString() );
                }
                sw.Close();
            }
            catch (InvalidCastException)
            {
                if (sw != null) sw.Close();
            }
        }

        //============================================================================================================================================================================
        private void LoadScenario(string pathToFolder)
        {
            _remainDrawingObjects = new ObjectsDrawList();
            var newLayout = new ObjectsDrawList();
            var openSpaces = new ObjectsDrawList();

            string pathToBorder = Path.Combine(pathToFolder, "Border.dxf");
            if (File.Exists(pathToBorder)) _scenario.Border = new Poly2D(DxfConversions.LinesFromDxf(pathToBorder));

            string pathToIniLines = Path.Combine(pathToFolder, "InitialLines.dxf");
            if (File.Exists(pathToIniLines)) _scenario.InitialLines = DxfConversions.LinesFromDxf(pathToIniLines);

            string pathToEnvironmentClose = Path.Combine(pathToFolder, "EnvironmentClose.dxf");
            if (File.Exists(pathToEnvironmentClose))
                _scenario.EnvironmentCloseNetwork = DxfConversions.LinesFromDxf(pathToEnvironmentClose);

            string pathToEnvironmentBuildings = Path.Combine(pathToFolder, "EnvironmentBuildings.dxf");
            if (File.Exists(pathToEnvironmentBuildings))
                _scenario.Buildings = DxfConversions.GObjectsFormDxf(pathToEnvironmentBuildings);

            string pathTo3DModel = Path.Combine(pathToFolder, "Environment3DBuildings.obj");
            if (File.Exists(pathTo3DModel)) glCameraMainWindow.OpenObjFile(pathTo3DModel);

            string pathToNewBuildings = Path.Combine(pathToFolder, "NewLayout.dxf");
            if (File.Exists(pathToEnvironmentBuildings)) newLayout = DxfConversions.GObjectsFormDxf(pathToNewBuildings);

            string pathToNewOpenSpaces = Path.Combine(pathToFolder, "NewLayoutOpenSpaces.dxf");
            if (File.Exists(pathToEnvironmentBuildings))
                openSpaces = DxfConversions.GObjectsFormDxf(pathToNewOpenSpaces);

            // send scene data via mqtt to visualizer
            if (ServiceManager.mqttClient != null)
            {
                ServiceManager.mqttClient.Publish("LoadScene_Border", File.ReadAllBytes(pathToBorder));
                ServiceManager.mqttClient.Publish("LoadScene_InitialLines", File.ReadAllBytes(pathToIniLines));
                ServiceManager.mqttClient.Publish("LoadScene_EnvironmentClose", File.ReadAllBytes(pathToEnvironmentClose));
                ServiceManager.mqttClient.Publish("LoadScene_Environment3DBuildings", File.ReadAllBytes(pathTo3DModel));

                publishCameraPos();
            }

            _scenario.NewLayout = new List<GenBuildingLayout>();
            int cter = 0;
            foreach (object gObj in newLayout)
            {
                if (gObj.GetType() == typeof(GPolygon))
                {
                    var gPoly = (GPolygon)gObj;
                    //gPoly.BorderWidth = 1;
                    //gPoly.Filled = true;
                    //_remainDrawingObjects.Add(gPoly);

                    GenBuildingLayout buildGen = ConvertGPolygonToBuildingGen(gPoly, cter);
                    //new GenBuildingLayout(buildRect, GeoAlgorithms2D.RadianToDegree(-angle), 30, cter);
                    buildGen.Locked = false;
                    buildGen.ProportionLock = true;
                    buildGen.RepelOffset = 2.0f;
                    _scenario.NewLayout.Add(buildGen);

                    cter++;
                }
            }

            foreach (object gObj in openSpaces)
            {
                if (gObj.GetType() == typeof(GPolygon))
                {
                    var gPoly = (GPolygon)gObj;

                    GenBuildingLayout openSpace = ConvertGPolygonToBuildingGen(gPoly, cter);
                    //new GenBuildingLayout(buildRect, GeoAlgorithms2D.RadianToDegree(-angle), 30, cter);
                    openSpace.Locked = false;
                    openSpace.Open = true;
                    openSpace.Attractor = false;
                    _scenario.NewLayout.Add(openSpace);

                    cter++;
                }
            }

            //AnalyseNetwork(_scenario.EnvironmentCloseNetwork);
            _remainDrawingObjects.Add(new GPolygon(_scenario.Border));
            if (_scenario.Buildings != null)
            {
                {
                    foreach (object gObj in _scenario.Buildings)
                    {
                        if (gObj.GetType() == typeof(GPolygon))
                        {
                            var gPoly = (GPolygon)gObj;
                            gPoly.BorderWidth = 1;
                            gPoly.Filled = true;
                            gPoly.FillColor = ConvertColor.CreateColor4(Color.Black);
                            _remainDrawingObjects.Add(gPoly);
                        }
                    }
                }
            }

            foreach (Line2D line in _scenario.EnvironmentCloseNetwork)
            {
                _remainDrawingObjects.Add(new GLine(line));
            }
            foreach (Line2D line in _scenario.InitialLines)
            {
                GLine iniLine = new GLine(line);
                iniLine.Width = 5;
                iniLine.Color = ConvertColor.CreateColor4(Color.Red); //RoyalBlue);
                _remainDrawingObjects.Add(iniLine);
            }
            DrawLayout();
            if (_scenario.Border != null) InitialScenarioGraph();
            else InitialTestGraph();

            glCameraMainWindow.ZoomAll();
        }

        //============================================================================================================================================================================
        private void backWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("File loaded."); // + e.Result.ToString());
            if (_populationParameters != null && _SOMOrderedArchive != null)
            {
                var convSomList = new List<IMultiChromosome>(_SOMOrderedArchive.Cast<IMultiChromosome>());
                _population = new MultiPopulation(_populationParameters, convSomList);
            }
        }

        //============================================================================================================================================================================
        private void backWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var percentFinished = (int) e.Argument;
            OpenAll();
        }

        //============================================================================================================================================================================
        private void OpenAll()
        {
            string name = openAllDialog.FileName;
            List<object> allObjects = BinarySerialization.DeserializeObjects(name);
            // -- cast types and assign to instances --
            foreach (object obj in allObjects)
            {
                if (obj != null)
                {
                    //if (obj.GetType() == typeof(MultiPopulation)) _population = (MultiPopulation)obj;
                    //if (obj.GetType() == typeof(MultiPopulation)) _initialPopulation = (MultiPopulation)obj;
                    //if (obj.GetType() == typeof(List<IMultiChromosome>)) _SOMOrderedArchive = (List<IMultiChromosome>)obj;
                    var list = obj as List<MultiPisaChromosomeBase>;
                    if (list != null)
                    {
                        _SOMOrderedArchive = list;
                        //_population.Sel.SetArchive(_SOMOrderedArchive); // copy the archive chromosomes into the PISA archive
                    }
                    if (obj.GetType() == typeof (ChromosomeLayout)) _activeLayout = (ChromosomeLayout) obj;
                    //if (obj.GetType() == typeof(List<MultiPisaChromosomeBase>)) _globalArchive = (List<MultiPisaChromosomeBase>)obj;
                    if (obj.GetType() == typeof (ViewControlParameters)) _parameters = (ViewControlParameters) obj;
                    if (obj.GetType() == typeof (PopulationParameters))
                        _populationParameters = (PopulationParameters) obj;
                }
            }
            // use the highest ID from the archive as starting point for new PISA-IDs.
            if (_SOMOrderedArchive != null)
            {
                int maxID = 0;
                foreach (MultiPisaChromosomeBase curChromo in _SOMOrderedArchive)
                {
                    if (curChromo.Indentity > maxID)
                    {
                        maxID = curChromo.Indentity;
                        curChromo.LastID = maxID + 1;
                    }
                }
            }
        }

        # endregion


        //===================================================================================================================================================================
        //========= Mouse Controls =========
        //===================================================================================================================================================================
        # region MouseControls

        private GenBuildingLayout _curGen;
        private Vector2d _deltaM = new Vector2d();
        private int _lastSelectedIdx = -1;
        private Poly2D _movingChild;
        private int _movingIdx = -1;
        private Vector2d _oldMousePos = new Vector2d();
        private int _selLayoutId;
        private Poly2D _selectedBlock;
        private ChromosomeLayout _selectedLayout;
        private bool _wasLocked;
        //===================================================================================================================================================================
        private void glControl1_MouseDown(object sender, MouseEventArgs e)
        {
            // -- interaction with building layout --
            if (_movingIdx == -1)
            {
                if (e.Button == MouseButtons.Left) // left button to move polygons
                {
                    CGeom3d.Vec_3d pointXy = glCameraMainWindow.ActiveCamera.GetPointXYPtn(e.X, e.Y).ToGeom3d();
                    if (pointXy == null) return;
                    var p = new Vector2d(pointXy.x, pointXy.y);

                    // -- interact with layout --
                    if (_allLayouts != null && _allLayouts.Count > 0)
                        for (int l = 0; l < _allLayouts.Count; l++)
                        {
                            ChromosomeLayout layout = _allLayouts[l];
                            if (layout.Border.ContainsPoint(p)) // select the layout
                            {
                                _activeLayout = _allLayouts[l];
                                _selLayoutId = l;
                                _selectedLayout = layout;
                                if (layout != null)
                                {
                                    for (int i = 0; i < layout.Gens.Count; i++)
                                    {
                                        Poly2D go = layout.Gens[i].PolygonFromGen;

                                        if (go.ContainsPoint(p))
                                        {
                                            _movingChild = go;
                                            _movingIdx = i;
                                            _lastSelectedIdx = i;
                                            _wasLocked = layout.Gens[_movingIdx].Locked;

                                            _curGen = layout.Gens[_movingIdx];
                                            _deltaM = p - _curGen.Position;
                                            _curGen.Locked = true;

                                            break;
                                        }
                                    }
                                    _oldMousePos = p;
                                }
                            }
                        }

                    // -- interact with layout --
                    if (_blocks != null)
                    {
                        for (int i = 0; i < _blocks.Count; i++)
                        {
                            Poly2D testPoly = _blocks[i];

                            if (testPoly.ContainsPoint(p))
                            {
                                _selectedBlock = testPoly;
                                _borderPoly = new GPolygon(testPoly);
                            }
                        }
                    }
                }
            }
        }

        //===================================================================================================================================================================
        private void glControl1_MouseMove(object sender, MouseEventArgs e)
        {
            // -- interaction with building layout --
            //var vel = new Vector2d();
            GenBuildingLayout curGen;
            CGeom3d.Vec_3d pointXy = glCameraMainWindow.ActiveCamera.GetPointXYPtn(e.X, e.Y).ToGeom3d();

            if (_movingChild != null)
            {
                // Wenn gerade ein Objekt verschoben werden soll, wird die Differenz zur letzten
                // Mausposition ausgerechnet und das Objekt um diese verschoben.              
                if (pointXy == null) return;
                //Cursor tmpCursor = Cursors.Default;
                var p = new Vector2d(pointXy.x, pointXy.y);
                if (_selectedLayout != null)
                {
                    curGen = _selectedLayout.Gens[_movingIdx];

                    if (ModifierKeys == Keys.Shift || MouseMode == "Rotate")
                    {
                        double deg = (_oldMousePos.Y - p.Y) * 2;
                        curGen.Rotation += deg;
                    }
                    else if (MouseMode == "Scale")
                    {
                        double fact = 1;
                        var oldCenter = curGen.Center;
                        double delta = (_oldMousePos.Y - p.Y)*fact;
                        double newWidth = curGen.Width + delta;
                        double newHeith = curGen.Area/newWidth;
                        if (newWidth > curGen.MinSide && newHeith > curGen.MinSide)
                        {
                            curGen.Width = newWidth;
                            curGen.Height = newHeith;
                            curGen.Center = oldCenter;
                        }
                    }
                    else if (MouseMode == "Size")
                    {
                        double fact = 20;
                        double delta = (_oldMousePos.Y - p.Y)*fact;
                        curGen.Area += delta;
                    }
                    else // move
                    {
                        var testNewVect = p - _deltaM;
                        
                        curGen.Position = testNewVect;
                    }
                    curGen.UpdateColliderPoly();
                }           
                _oldMousePos = p;
            }

            publishCameraPos();

            if (pointXy != null) statusLabel.Text = "X: " + pointXy.x + ", Y: " + pointXy.y;
            glCameraMainWindow.Invalidate();
        }

        private void publishCameraPos()
        {
            CGeom3d.Vec_3d camPos = glCameraMainWindow.ActiveCamera.Position;
            double[] dArr = new double[] { camPos.x, camPos.y, camPos.z, glCameraMainWindow.ActiveCamera.Alpha, glCameraMainWindow.ActiveCamera.Beta, glCameraMainWindow.ActiveCamera.Gamma };
            MemoryStream ms = new MemoryStream();
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            bf.Serialize(ms, dArr);
            if (ServiceManager.mqttClient != null)
            {
                try
                {
                    ServiceManager.mqttClient.Publish("CameraPos", ms.ToArray());
                }
                catch (Exception ex) { }
            }
        }


        //===================================================================================================================================================================
        private void glControl1_MouseUp(object sender, MouseEventArgs e)
        {
            // -- interaction with building layout --
            var vel = new Vector2d();
            GenBuildingLayout curGen;
            vel.X = 0;
            vel.Y = 0;
            {
                if (_selectedLayout != null)
                {
                    if (_movingChild != null)
                    {
                        _movingChild = null;
                        curGen = _selectedLayout.Gens[_movingIdx];
                        curGen.Locked = _wasLocked;
                        propertyGrid1.SelectedObject = curGen;
                        if (ControlParameters.instantUpdate) IsovistAnalye();
                    }
                }
            }
            if (cB_realUpdate.Checked)
            {
                _activeLayout = _selectedLayout;
                if (_analysisType == AnalysisType.Solar)
                {
                    SolarAnalysis();
                    IsovistAnalye();
                }
                if (_analysisType == AnalysisType.Isovist)
                    IsovistAnalye();
            }
            _selectedLayout = null;
            _movingIdx = -1;
        }

        //===================================================================================================================================================================
        //================glCameraLayouts======================================================================================================================================
        //===================================================================================================================================================================
        private void glControl2_MouseUp(object sender, MouseEventArgs e)
        {
            int id = -1;
            // --- find the ID of the chromosome and send it to the other window for interaction ---
            if (e.Button == MouseButtons.Left)
            {
                if (glCameraLayouts.AllObjectsList.ObjectsToDraw != null)
                {
                    CGeom3d.Vec_3d pointXy = glCameraLayouts.ActiveCamera.GetPointXYPtn(e.X, e.Y).ToGeom3d();
                    if (pointXy == null) return;
                    var p = new Vector2d(pointXy.x, pointXy.y);

                    for (int i = 0; i < glCameraLayouts.AllObjectsList.ObjectsToDraw.Count; i++)
                    {
                        GeoObject go = glCameraLayouts.AllObjectsList.ObjectsToDraw[i];
                        if (go.ContainsPoint(p))
                        {
                            id = go.Identity;
                            break;
                        }
                    }

                    if (id > -1)
                    {
                        /*
                        ConfigSettings config = ServiceManager.GetConfig4ID(id);
                        if (config != null)
                        {
                            int[] ids = new int[] { config.objID, config.scenarioID };
                            MemoryStream ms = new MemoryStream();
                            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                            bf.Serialize(ms, ids);
                            if (ServiceManager.mqttClient != null)
                            {
                                ServiceManager.mqttClient.Publish("SelectionChanged", ms.ToArray());
                            }
                        }*/

                        MultiPisaChromosomeBase curChromo;
                        if (_SOMOrderedArchive != null)
                        {
                            curChromo = _SOMOrderedArchive.Find(chromosomeLayout => chromosomeLayout.Indentity == id);
                        }
                        else
                        {
                            curChromo = _globalArchive.Find(ChromosomeLayout => ChromosomeLayout.Indentity == id);
                        }

                        if (curChromo == null)
                        {
                            curChromo = _globalArchive.Find(ChromosomeLayout => ChromosomeLayout.Indentity == id);
                        }
                        if (curChromo != null)
                        {
                            if (curChromo.GetType() == typeof (ChromosomeLayout))
                            {
                                if (_allLayouts.Count == 0)
                                    _allLayouts.Add((ChromosomeLayout)curChromo);

                                _allLayouts[_selLayoutId] = (ChromosomeLayout) curChromo;
                                _activeLayout = _allLayouts[_selLayoutId];
                                //_smartGrid = new GGrid(_allLayouts[_selLayoutId].IsovistField);
                                
//                                showResultsIsovist(id);
//                                showResultsSolar(true);
                                IsovistAnalye();
                                DrawLayout();
                            }
                        }
                    }
                }
            }
        }

        //===================================================================================================================================================================
        //================glCameraNetwork======================================================================================================================================
        //===================================================================================================================================================================
        private void glControl3_MouseUp(object sender, MouseEventArgs e)
        {
            int id = -1;
            // --- find the ID of the chromosome and send it to the other window for interaction ---
            if (e.Button == MouseButtons.Left)
            {
                if (glCameraNetwork.AllObjectsList.ObjectsToDraw != null)
                {
                    CGeom3d.Vec_3d pointXy = glCameraNetwork.ActiveCamera.GetPointXYPtn(e.X, e.Y).ToGeom3d();
                    if (pointXy == null) return;
                    var p = new Vector2d(pointXy.x, pointXy.y);

                    for (int i = 0; i < glCameraNetwork.AllObjectsList.ObjectsToDraw.Count; i++)
                    {
                        GeoObject go = glCameraNetwork.AllObjectsList.ObjectsToDraw[i];
                        if (go.ContainsPoint(p))
                        {
                            id = go.Identity;
                            break;
                        }
                    }

                    if (id > -1)
                    {
                        MultiPisaChromosomeBase curChromo;
                        if (_SOMOrderedArchive != null)
                        {
                            curChromo =
                                _SOMOrderedArchive.Find(instructionTreePisa => instructionTreePisa.Indentity == id);
                        }
                        else
                        {
                            curChromo = _globalArchive.Find(instructionTreePisa => instructionTreePisa.Indentity == id);
                        }

                        if (curChromo == null)
                        {
                            curChromo = _globalArchive.Find(instructionTreePisa => instructionTreePisa.Indentity == id);
                        }
                        if (curChromo != null)
                        {
                            if (curChromo.GetType() == typeof (InstructionTreePisa))
                            {
                                _activeNetwork = (InstructionTreePisa) curChromo;
                                DrawAnalysedNetwork(_activeNetwork, glCameraMainWindow);
                                glCameraMainWindow.Invalidate();
                            }
                        }
                    }
                }
            }
        }

        //===================================================================================================================================================================
        //todo: shall be in the custom cpmponent CustomGLControl! But this crashes if one call GL.Viewport there.
        private void glControls_Resize(object sender, EventArgs e)
        {
            var curCtrl = (GLCameraControl) sender;
            if (!curCtrl.Loaded) return;
            int w = curCtrl.Width;
            int h = curCtrl.Height;
            curCtrl.MakeCurrent();
            GL.Viewport(0, 0, w, h);
        }

        # endregion


        //===================================================================================================================================================================
        //========= Delegates =========
        //===================================================================================================================================================================
        #region Delegates

        //==============================================================================================
        // Delegates to enable async calls for setting controls properties

        private delegate void ReSetChartCallback(Chart chart, int series);

        private delegate void SetChartCallback(Chart chart, double valueX, double valueY, int series);

        private delegate void SetGLCallback(Control control);

        private delegate void SetTextCallback(Control control, string text);

        //==============================================================================================
        void NotificationPopChangedHandler(object sender, Dictionary<int, object> userInfo)
        {
            int numPop = (int)sender;
            //     _backWorker.ReportProgress(numPop);

            Console.Out.WriteLine(numPop);

            // progressOpti.Value = numPop;
            //this.BeginInvoke(delegate { progressOpti.Value = numPop; });

            numPop = Math.Min(progressOpti.Value, numPop);

            this.BeginInvoke((System.Windows.Forms.MethodInvoker)delegate() { progressOpti.Value = numPop; });

            if (_population != null && _population.GetPopulation != null)
            {
                int num = _population.GetPopulation.Count();
                Console.Out.WriteLine(num);
            }
        }

        //==============================================================================================
        void NotificationAlphaChangedHandler(object sender, Dictionary<int, object> userInfo)
        {
            int numAlpha = (int)sender;

            //progressOpti.Maximum = numAlpha;
            this.BeginInvoke((System.Windows.Forms.MethodInvoker)delegate() { progressOpti.Maximum = numAlpha; });
        }

        //==============================================================================================
        void NotificationNumLoopsChangedHandler(object sender, Dictionary<int, object> userInfo)
        {
            int numLoops = (int)sender;

            this.BeginInvoke((System.Windows.Forms.MethodInvoker)delegate() { progressLoops.Maximum = numLoops; });
        }

        //==============================================================================================
        void NotificationLoopChangedHandler(object sender, Dictionary<int, object> userInfo)
        {
            int curLoop = (int)sender;

            this.BeginInvoke((System.Windows.Forms.MethodInvoker)delegate() { progressLoops.Value = curLoop; });
        }

        //==============================================================================================
        // Thread safe updating of control's text property
        private void SetGraphicContext(Control control)
        {
            if (control.InvokeRequired)
            {
                SetGLCallback d = SetGraphicContext;
                Invoke(d, new object[] { control });
            }
            else
            {
                glCameraMainWindow.Screenshot(4);
            }
        }

        //==============================================================================================
        // Thread safe updating of control's text property
        private void SetText(Control control, string text)
        {
            if (control.InvokeRequired)
            {
                SetTextCallback d = SetText;
                Invoke(d, new object[] { control, text });
            }
            else
            {
                control.Text = text;
            }
        }

        //==============================================================================================
        private void SetChart(Chart chart, double valueX, double valueY, int series)
        {
            if (chart.InvokeRequired)
            {
                SetChartCallback d = SetChart;
                Invoke(d, new object[] { chart, valueX, valueY, series });
            }
            else
            {
                chart.Series[series].Points.AddXY(valueX, valueY);
            }
        }

        //==============================================================================================
        private void ReSetChart(Chart chart, int series)
        {
            if (chart.InvokeRequired)
            {
                ReSetChartCallback d = ReSetChart;
                Invoke(d, new object[] { chart, series });
            }
            else
            {
                chart.Series[series].Points.Clear();
            }
        }

        #endregion


        //===================================================================================================================================================================
        //========= UI Controls =========
        //===================================================================================================================================================================
        #region UI_Controls

        //============================================================================================================================================================================
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // If user clicks button, show the dialog.
            string iniPath = Application.StartupPath;
            // "C:/Users/koenigr/Pictures/130424_StreetNetwork/dxf";//System.Reflection.Assembly.GetExecutingAssembly().Location;
            openAllDialog.InitialDirectory = Path.GetFullPath(iniPath);
            openAllDialog.Filter = "cplan file|*.cplan";
            openAllDialog.Title = "Save a CPlan file";
            openAllDialog.RestoreDirectory = true;
            openAllDialog.ShowDialog();
        }

        //============================================================================================================================================================================
        private void openAllDialog_FileOk(object sender, CancelEventArgs e)
        {
            if (toolStripProgressBar2.Value == toolStripProgressBar2.Maximum)
            {
                toolStripProgressBar2.Value = toolStripProgressBar2.Minimum;
            }
            _backWorker.RunWorkerAsync(toolStripProgressBar2.Value);
        }


        //============================================================================================================================================================================
        private void importDxfToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // When user clicks button, show the dialog.
            string iniPath = Assembly.GetExecutingAssembly().Location;
            openDxfDialog.InitialDirectory = Path.GetFullPath(iniPath);
            openDxfDialog.Filter = "Dxf file|*.dxf";
            openDxfDialog.Title = "Load an dxf file";
            openDxfDialog.RestoreDirectory = true;
            openDxfDialog.ShowDialog();
        }

        //============================================================================================================================================================================
        private void openDxfDialog_FileOk(object sender, CancelEventArgs e)
        {
            // Get file name.  
            string name = openDxfDialog.FileName;
            // create the new objects from the selected file
            ObjectsDrawList newGObjects = DxfConversions.GObjectsFormDxf(name);
            // add the new objects to the current ObjectsToDraw-list
            glCameraMainWindow.AllObjectsList.ObjectsToDraw.AddRange(newGObjects);
            glCameraMainWindow.ZoomAll();
        }

        //============================================================================================================================================================================
        private void loadScenarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string pathToFolder;
            if (folderBrowserDialog_Scenarios.ShowDialog() == DialogResult.OK)
            {
                pathToFolder = folderBrowserDialog_Scenarios.SelectedPath;
            }
            else return;

            LoadScenario(pathToFolder);
        }

        
        //============================================================================================================================================================================
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // If user clicks button, show the dialog.
            string iniPath = Application.StartupPath;
            // "C:/Users/koenigr/Pictures/130424_StreetNetwork/dxf";//System.Reflection.Assembly.GetExecutingAssembly().Location;
            saveAllDialog.InitialDirectory = Path.GetFullPath(iniPath);
            saveAllDialog.Filter = "cplan file|*.cplan";
            saveAllDialog.Title = "Save a CPlan file";
            saveAllDialog.RestoreDirectory = true;
            saveAllDialog.ShowDialog();
        }

        //============================================================================================================================================================================
        private void saveAllDialog_FileOk(object sender, CancelEventArgs e)
        {
            // -- Get file name --  
            string name = saveAllDialog.FileName; //Application.StartupPath + "\\" +
            // -- save all --
            List<IMultiChromosome> archive;
            var allObjects = new List<object>();
            allObjects.Add(_population);
            if (_population != null)
            {
                archive = _population.Sel.GetArchive();
                allObjects.Add(archive);
            }
            allObjects.Add(_activeLayout);
            allObjects.Add(_parameters);
            BinarySerialization.SerializeObjects(allObjects, name);
        }

        //============================================================================================================================================================================
        private void exportDxfToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // When user clicks button, show the dialog.
            string iniPath = Application.StartupPath;
            // "C:/Users/koenigr/Pictures/130424_StreetNetwork/dxf";//System.Reflection.Assembly.GetExecutingAssembly().Location;
            saveDxfDialog.InitialDirectory = Path.GetFullPath(iniPath);
            saveDxfDialog.Filter = "Dxf file|*.dxf";
            saveDxfDialog.Title = "Save a dxf file";
            saveDxfDialog.RestoreDirectory = true;
            saveDxfDialog.ShowDialog();
        }

        //============================================================================================================================================================================
        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            // Get file name.  
            string name = saveDxfDialog.FileName;

            string nameA = name.Insert(name.Count() - 4, "_main");
            // create the dxf file that shall be saved
            DxfDocument dxf = DxfConversions.ToDxfFile(glCameraMainWindow.AllObjectsList.ObjectsToDraw,
                DxfVersion.AutoCad2000);
            // Write to the file name selected.
            dxf.Save(nameA);
            if (_population != null) SaveFitnessValues(name);

            string nameB = name.Insert(name.Count() - 4, "_layouts");
            // create the dxf file that shall be saved
            dxf = DxfConversions.ToDxfFile(glCameraLayouts.AllObjectsList.ObjectsToDraw, DxfVersion.AutoCad2000);
            // Write to the file name selected.
            dxf.Save(nameB);

            string nameC = name.Insert(name.Count() - 4, "_networks");
            // create the dxf file that shall be saved
            dxf = DxfConversions.ToDxfFile(glCameraNetwork.AllObjectsList.ObjectsToDraw, DxfVersion.AutoCad2000);
            // Write to the file name selected.
            dxf.Save(nameC);
        }


        //============================================================================================================================================================================
        private void toolStripButton_LoadScene_Click(object sender, EventArgs e)
        {
            LoadScenario("C:\\Users\\koenigr\\Programmierung\\ComputationalPlanningGroup\\Various\\InputDataCapeTown");
        }

        //============================================================================================================================================================================
        private void importGraphFromDxfToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openDxfGraphDialog.ShowDialog();
        }

        //============================================================================================================================================================================
        private void openDxfGraphDialog_FileOk(object sender, CancelEventArgs e)
        {
            string name = openDxfGraphDialog.FileName;
            openDxfGraphDialog.InitialDirectory = Assembly.GetExecutingAssembly().Location;

            DxfDocument dxf = DxfConversions.FromDxfFile(name);
            List<Line> lines = dxf.Lines.ToList();
            var importedLines = new List<LineD>();
            var importedQLines = new List<UndirectedEdge<Vector2d>>();
            foreach (Line dLine in lines)
            {
                importedLines.Add(new LineD(dLine.StartPoint.X, dLine.StartPoint.Y, dLine.EndPoint.X, dLine.EndPoint.Y));
                importedQLines.Add(new UndirectedEdge<Vector2d>(new Vector2d(dLine.StartPoint.X, dLine.StartPoint.Y), new Vector2d(dLine.EndPoint.X, dLine.EndPoint.Y)));
            }
            _streetGenerator = new StreetPattern(null);
            _streetGenerator.Graph = new Subdivision();
            LineD[] arrLines = importedLines.ToArray();
            _streetGenerator.Graph = Subdivision.FromLines(arrLines);

            _streetGenerator.QGraph = new UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>();
            _streetGenerator.QGraph.AddVerticesAndEdgeRange(importedQLines);

            //DrawNetwork(_streetGenerator.Graph);
            DrawNetwork(_streetGenerator.QGraph);
            glCameraMainWindow.ZoomAll();

            // -- Test -- ***************************************************************************************************
            InstructionTreePisa treeFromGraph;
            UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> iniNet;
            NetworkToChromosome(_streetGenerator.QGraph, out treeFromGraph, out iniNet);

            _streetGenerator = new StreetPattern(null);
            _streetGenerator.GrowStreetPatternNew(treeFromGraph, iniNet);
            DrawNetwork(_streetGenerator.QGraph);
            glCameraMainWindow.ZoomAll();
        }


        //============================== Layout Optimization ===========================================
        private void Button_OptiLayout_Click(object sender, EventArgs e)
        {
            NotificationBroker.Instance.Register(NotificationAlphaChangedHandler, "NUMALPHA");
            NotificationBroker.Instance.Register(NotificationPopChangedHandler, "NUMPOP");
            NotificationBroker.Instance.Register(NotificationNumLoopsChangedHandler, "NUMLOOPS");
            NotificationBroker.Instance.Register(NotificationLoopChangedHandler, "CURLOOP");

            glCameraLayouts.AllObjectsList = new ObjectsAllList();
            _SOMOrderedArchive = new List<MultiPisaChromosomeBase>();
            _population = null;
            //_activeLayout = null;
            _selectedBlock = null;
            glCameraLayouts.Invalidate();

            // get population size
            try
            {
                _populationSize = Convert.ToInt16(uD_populationSize.Value);
            }
            catch
            {
                _populationSize = 20;
            }
            // iterations
            try
            {
                _iterations = Convert.ToInt16(uD_generations.Value);
            }
            catch
            {
                _iterations = 20;
            }
            glCameraLayouts.DeactivateInteraction();

            // run worker thread           
            _needToStop1 = false;
            if (sB_multiOpti.Checked)
            {
                if (cB_layoutAll.Checked && cB_optiAll.Checked)
                    _workerThread = new Thread(SearchLayoutMultiAllBlocks);
                else
                    _workerThread = new Thread(SearchLayoutMulti);

                /* *ac*
                if (cB_layoutAll.Checked && cB_optiAll.Checked)
                    _workerThread = new Thread(() =>
                    {
                        SearchLayoutMultiAllBlocks().Wait();
                    });
                else
                    _workerThread = new Thread(() =>
                    {
                        SearchLayoutMulti().Wait();
                    });
                 * */
            }
            if (sB_singleOpti.Checked)
            {
                _workerThread = new Thread(SearchLayoutSingle);
            }
            _workerThread.Start();
        }


        //============================== Object manipulation ===========================================
        //==============================================================================================
        private void Button_Move_Click(object sender, EventArgs e)
        {
            MouseMode = "Move";
            statusLabel.Text = "Drag and drop building to MOVE it.";
        }

        //==============================================================================================
        private void Button_Rotate_Click(object sender, EventArgs e)
        {
            MouseMode = "Rotate";
            statusLabel.Text = "Hold left button and move mouse up and down to ROTATE building.";
        }

        //==============================================================================================
        private void Button_Scale_Click_1(object sender, EventArgs e)
        {
            MouseMode = "Scale";
            statusLabel.Text = "Hold left button and move mouse up and down to SCALE building.";
        }

        //==============================================================================================
        private void Button_Size_Click(object sender, EventArgs e)
        {
            MouseMode = "Size";
            statusLabel.Text = "Hold left button and move mouse up and down to change building SIZE.";
        }

        
        //====================================================================================================
        private void uD_populationSize_ValueChanged(object sender, EventArgs e)
        {
            if (uD_nrChildren.Value < uD_populationSize.Value) uD_nrChildren.Value = uD_populationSize.Value;
            uD_Mu.Value = uD_populationSize.Value;
        }

        //====================================================================================================
        private void uD_nrChildren_ValueChanged(object sender, EventArgs e)
        {
            if (uD_nrChildren.Value < uD_populationSize.Value) uD_nrChildren.Value = uD_populationSize.Value;
            uD_Lambda.Value = uD_nrChildren.Value;
        }

        //====================================================================================================
        private void b_lockPanel_Click(object sender, EventArgs e)
        {
            splitContainer1.IsSplitterFixed = !splitContainer1.IsSplitterFixed;
        }

        //====================================================================================================
        private void comboBox_scenario_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetControlParameters();
            SetScenarioEnvironment();
        }

        //====================================================================================================
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (!checkBox1.Checked)
            {
                glCameraMainWindow.ShowRaster = false;
                glCameraLayouts.ShowRaster = false;
                glCameraNetwork.ShowRaster = false;
                //glCameraMainWindow.BackgroundColor = Color.White;
                //glCameraLayouts.BackgroundColor = Color.White;
                //glCameraNetwork.BackgroundColor = Color.White;
            }
            else
            {
                glCameraMainWindow.ShowRaster = true;
                glCameraLayouts.ShowRaster = true;
                glCameraNetwork.ShowRaster = true;
                //glCameraMainWindow.BackgroundColor = Color.LightGray;
                //glCameraLayouts.BackgroundColor = Color.LightGray;
                //glCameraNetwork.BackgroundColor = Color.LightGray;
            }
            glCameraMainWindow.Invalidate();
            glCameraLayouts.Invalidate();
            glCameraNetwork.Invalidate();
        }

        //====================================================================================================
        private void b_ESOM_Click(object sender, EventArgs e)
        {
            string strCmdText;
            // -- The path to ESOM commands
            string path = Settings.Default.PathToESOM; // "C:\\Users\\koenigr\\Documents\\ESOM\\bin\\";
            // -- Commands that can be used: (execute them in the cmd shows you the parameters that can be specified)       
            //esomana	Starts the ESOM Analyzer GUI. You can specify filenames to be automatically loaded.
            //esomcls	Loads a class mask (*.cmx) and bestmatches (*.bm) and saves the result of the classification of the bestmatches (*.bm).
            //esomprj	Loads a trained ESOM (*.wts) and a dataset (*.lrn) and saves the bestmatches for the data on the grid (*.bm).
            //esommat	Loads a trained ESOM (*.wts) and optionally a dataset (*.lrn). Saves a matrix of height values (*.umx).
            //esomrnd	Loads a trained ESOM (*.wts) and optionally a dataset (*.lrn) and a classification (*.cls). Saves an image (*.png).
            //esomtrn	Loads a dataset (*.lrn) and trains an ESOM. The weights (*.wts) and final bestmatches (*.bm) are saved.

            // -- esom training
            strCmdText = path + "esomtrn.bat"; //
            // --------------
            // -- parameters:
            // -- path to the lrn file
            strCmdText +=
                " -l C:\\Users\\koenigr\\Programmierung\\ComputationalPlanningGroup\\Various\\140608_LayoutB_varNr_02.lrn";
            // -- number of training epochs
            strCmdText += " -e 200";

            // -- execute the command
            ExecuteCommandSync(strCmdText);
        }

        //====================================================================================================
        private void b_ESOMrender_Click(object sender, EventArgs e)
        {
            //
            //string myUserSetting = Properties.Settings.Default.MyUserSetting;
            //string myApplicationSetting = Properties.Settings.Default.MyApplicationSetting;

            string strCmdText;
            // -- The path to ESOM commands
            string path = Settings.Default.PathToESOM; //"C:\\Users\\koenigr\\Documents\\ESOM\\bin\\";

            // -- esom render
            strCmdText = path + "esomrnd.bat";

            // --------------
            // -- parameters:
            // -- path to the lrn file
            strCmdText +=
                " -l C:\\Users\\koenigr\\Programmierung\\ComputationalPlanningGroup\\Various\\140608_LayoutB_varNr_02.lrn";
            // -- path to the bm file
            strCmdText +=
                " -b C:\\Users\\koenigr\\Programmierung\\ComputationalPlanningGroup\\Various\\140608_LayoutB_varNr_02.bm";
            // -- path to the wts file
            strCmdText +=
                " -w C:\\Users\\koenigr\\Programmierung\\ComputationalPlanningGroup\\Various\\140608_LayoutB_varNr_02.wts";
            // -- background renderer
            strCmdText += " -B pmx";
            // -- name of color table
            strCmdText += " -r jet";
            // -- png output file
            strCmdText +=
                " -p C:\\Users\\koenigr\\Programmierung\\ComputationalPlanningGroup\\Various\\140608_LayoutB_varNr_02.png";
            // -- zoom factor
            strCmdText += " -z 20";

            // -- execute the command
            ExecuteCommandSync(strCmdText);

            // -- show the rendering
            pictureBox_ESOM.Image =
                new Bitmap(
                    "C:\\Users\\koenigr\\Programmierung\\ComputationalPlanningGroup\\Various\\140608_LayoutB_varNr_02.png");
            pictureBox_ESOM.Invalidate();
        }

        //==============================================================================================
        private void toolStripButton12_Click(object sender, EventArgs e)
        {
            var glForm = new CollissionTest();
            glForm.Show();
        }

        //============================================================================================================================================================================
        private void appSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var formSettings = new FormAppSettings();
            formSettings.ShowDialog();
            PisaWrapper.PISA_DIR = Settings.Default.PathToPISA;
        }

        //============================================================================================================================================================================
        private void toolStripButton10_Click(object sender, EventArgs e)
        {
            SolarAnalysis();
            comboBox_Isovist.SelectedIndex = 7;
            //IsovistAnalye(true);
            DrawLayout();
        }

        //============================================================================================================================================================================
        private void toolStripButton9_Click(object sender, EventArgs e)
        {
            DrawLayoutSOM();
        }

        //============================================================================================================================================================================
        private void readBmToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string iniPath = Application.StartupPath;
            // "C:/Users/koenigr/Pictures/130424_StreetNetwork/dxf";//System.Reflection.Assembly.GetExecutingAssembly().Location;
            openBmFile.InitialDirectory = Path.GetFullPath(iniPath);
            openBmFile.Filter = "databionic-esom bm file|*.bm";
            openBmFile.Title = "Load a bm file";
            openBmFile.RestoreDirectory = true;
            openBmFile.ShowDialog();
        }

        //============================================================================================================================================================================
        private void openBmFile_FileOk(object sender, CancelEventArgs e)
        {
            string name = openBmFile.FileName;
            _somOrder = TextRW.ReadBm(name);
        }

        //============================================================================================================================================================================
        private void exportLrnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // If user clicks button, show the dialog.
            string iniPath = Application.StartupPath;
            // "C:/Users/koenigr/Pictures/130424_StreetNetwork/dxf";//System.Reflection.Assembly.GetExecutingAssembly().Location;
            saveExportLrn.InitialDirectory = Path.GetFullPath(iniPath);
            saveExportLrn.Filter = "databionic-esom lrn file|*.lrn";
            saveExportLrn.Title = "Save a lrn file";
            saveExportLrn.RestoreDirectory = true;
            saveExportLrn.ShowDialog();
        }

        //============================================================================================================================================================================
        private void saveExportLrn_FileOk(object sender, CancelEventArgs e)
        {
            string name = saveExportLrn.FileName;
            TextRW.WriteLrn(_population, name);
            if (_population[0].GetType() == typeof(ChromosomeLayout))
            {
                string name2 = name.Replace(".lrn", "_ID.lrn");
                List<string> fingerprint = LayoutFingerprintAnalysisPoints();
                TextRW.WriteLrnID(fingerprint, name2);
            }

            List<MultiPisaChromosomeBase> archive;
            var allObjects = new List<object>();
            if (_population != null)
            {
                allObjects.Add(_populationParameters);
                archive = _population.Sel.GetArchiveOrig();
                allObjects.Add(archive);
                string name2 = name.Replace(".lrn", ".cplan");
                BinarySerialization.SerializeObjects(allObjects, name2);
            }
        }

        //============================================================================================================================================================================
        private void ButtonBuildingLayout_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            Poly2D borderPoly;
            //glCameraMainWindow.AllObjectsList.ObjectsToDraw.Add(_borderPoly);
            //glCameraMainWindow.ZoomAll();
            if (_selectedBlock != null)
            {
                borderPoly = _selectedBlock;
            }
            else if (_border != null)
            {
                _borderPoly = new GPolygon(_border);
                borderPoly = _border;
            }
            else
            {
                Vector2d[] pts = new Vector2d[6];
                pts[0] = new Vector2d(1, 1);
                pts[1] = new Vector2d(1, 110);
                pts[2] = new Vector2d(111, 150);
                pts[3] = new Vector2d(200, 90);
                pts[4] = new Vector2d(180, 10);
                pts[5] = new Vector2d(50, 0);
                _border = new Poly2D(pts);
                _borderPoly = new GPolygon(_border);
                borderPoly = new Poly2D(_borderPoly.PolyPoints);
            }

            bool makeNewLayout = true;

            // --- layouts for all blocks ---
            if (cB_layoutAll.Checked && _activeNetwork != null)
            {
                ParametersLayout lParameters = new ParametersLayout();
                lParameters.MaxBldArea = ControlParameters.MaxBldArea;
                lParameters.MinBldArea = ControlParameters.MinBldArea;
                lParameters.MaxBldHeigth = ControlParameters.MaxBldHeigth;
                lParameters.MinBldHeigth = ControlParameters.MinBldHeigth;
                lParameters.MinNrBuildings = ControlParameters.MinNrBuildings;
                lParameters.MaxNrBuildings = ControlParameters.MaxNrBuildings;
                lParameters.Density = ControlParameters.Density;
                lParameters.MinSideRatio = ControlParameters.MinSideRatio;
                lParameters.FixRotation = ControlParameters.AlignTo45;
                lParameters.FixedRotationValue = ControlParameters.FixedRotationValue;
                lParameters.AlignRotationToBorder = ControlParameters.AlignToBorder;
                _borderPoly = new GPolygon(_activeNetwork.Border);
                _activeMultiLayout = new ChromosomeMultiLayout(_borderPoly.Poly, _blocks, lParameters);
                makeNewLayout = false;
            }
            else
            {
                // --- layouts for individual blocks ---
                for (int i = 0; i < _allLayouts.Count; i++)
                {
                    ChromosomeLayout layout = _allLayouts[i];
                    double overlap = layout.Border.Overlap(borderPoly);
                    double ratio = overlap / borderPoly.Area;
                    if (ratio > 0.9)
                    {
                        ParametersLayout lParameters = new ParametersLayout();
                        lParameters.MaxBldArea = ControlParameters.MaxBldArea;
                        lParameters.MinBldArea = ControlParameters.MinBldArea;
                        lParameters.MaxBldHeigth = ControlParameters.MaxBldHeigth;
                        lParameters.MinBldHeigth = ControlParameters.MinBldHeigth;
                        lParameters.MinNrBuildings = ControlParameters.MinNrBuildings;
                        lParameters.MaxNrBuildings = ControlParameters.MaxNrBuildings;
                        lParameters.Density = ControlParameters.Density;
                        lParameters.MinSideRatio = ControlParameters.MinSideRatio;
                        lParameters.FixRotation = ControlParameters.AlignTo45;
                        lParameters.FixedRotationValue = ControlParameters.FixedRotationValue;
                        lParameters.AlignRotationToBorder = ControlParameters.AlignToBorder;
                        _allLayouts[i] = new ChromosomeLayout(borderPoly, lParameters);
                        makeNewLayout = false;
                        _activeLayout = _allLayouts[i];
                        break;
                    }
                }
            }

            //if (_scenario.NewLayout != null)
            //{
            //    var newLayout = new ChromosomeLayout(borderPoly,
            //                ControlParameters.MinNrBuildings, ControlParameters.MaxNrBuildings,
            //                ControlParameters.Density, ControlParameters.MinSideRatio);//new ChromosomeLayout(borderPoly, _scenario.NewLayout, ControlParameters.MinSideRatio);
            //    //ControlParameters.propChange = false;
            //    _activeLayout = newLayout;
            //    _allLayouts.Add(newLayout);
            //}
            //else 
            if (makeNewLayout)
            {
                ParametersLayout lParameters = new ParametersLayout();
                lParameters.MaxBldArea = ControlParameters.MaxBldArea;
                lParameters.MinBldArea = ControlParameters.MinBldArea;
                lParameters.MaxBldHeigth = ControlParameters.MaxBldHeigth;
                lParameters.MinBldHeigth = ControlParameters.MinBldHeigth;
                lParameters.MinNrBuildings = ControlParameters.MinNrBuildings;
                lParameters.MaxNrBuildings = ControlParameters.MaxNrBuildings;
                lParameters.Density = ControlParameters.Density;
                lParameters.MinSideRatio = ControlParameters.MinSideRatio;
                lParameters.FixRotation = ControlParameters.AlignTo45;
                lParameters.FixedRotationValue = ControlParameters.FixedRotationValue;
                lParameters.AlignRotationToBorder = ControlParameters.AlignToBorder;

                var newLayout = new ChromosomeLayout(borderPoly,lParameters);
                _activeLayout = newLayout;
                _allLayouts.Add(newLayout);
            }

            /*
                var fitnessFunction = new FitnessFunctionLayout(RunVia);
                _activeLayout.Evaluate(fitnessFunction, true);
                _isovistField = _activeLayout.IsovistField;
                string measure = "";
                if (comboBox_Isovist.SelectedValue != null)
                    measure = comboBox_Isovist.SelectedValue.ToString();
                _smartGrid = new GGrid(gridPoints, _isovistField.GetMeasure(measure), _isovistField.CellSize);
           
                propertyGrid2.SelectedObject = _isovistField;
 */

            //timer1.Enabled = true;
            //            IsovistAnalye();

            if (cB_layoutAll.Checked)
            {
                if (_activeMultiLayout != null)
                {
                    _activeMultiLayout.Adapt();
                    _allLayouts = _activeMultiLayout.SubChromosomes;
                    //DrawMultiLayout();
                }
            }
            else
            {
                if (_allLayouts.Count > 0)
                {
                    foreach (ChromosomeLayout layout in _allLayouts)
                        layout.Adapt();

                    //DrawLayout();
                }
            }
            if (rB_AnaMainIsovist.Checked)
            {
                IsovistAnalye();
                comboBox_Isovist.SelectedIndex = 1;
            }
            else if (rB_AnaMainSolar.Checked)
            {
                SolarAnalysis();
                comboBox_Isovist.SelectedIndex = 7;
                IsovistAnalye();
            }
            DrawLayout();
        }

        //============================================================================================================================================================================
        private void timer1_Tick(object sender, EventArgs e)
        {
            MainLoop();
        }

        //============================================================================================================================================================================
        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            // stop worker thread
            if (_workerThread != null)
            {
                _needToStop1 = true;
                while (!_workerThread.Join(100))
                    Application.DoEvents();
                _workerThread = null;
            }
        }

        //============================================================================================================================================================================
        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            glCameraMainWindow.AllObjectsList = new ObjectsAllList();
            glCameraLayouts.AllObjectsList = new ObjectsAllList();
            glCameraNetwork.AllObjectsList = new ObjectsAllList();
            _constantDrawObjects = new ObjectsDrawList();
            _globalArchive = new List<MultiPisaChromosomeBase>();
            _SOMOrderedArchive = new List<MultiPisaChromosomeBase>();
            _remainDrawingObjects = new List<GeoObject>();
            _allLayouts = new List<ChromosomeLayout>();
            _allNetworks = new List<InstructionTreePisa>();
            _blocks = new List<Poly2D>();
            _isovistField = null;
            _population = null;
            _activeLayout = null;
            _selectedBlock = null;
            _smartGrid = null;
            _activeMultiLayout = null;
            _border = null;
            glCameraMainWindow.Invalidate();
            glCameraLayouts.Invalidate();
            glCameraNetwork.Invalidate();
            ReSetChart(chart1, 0);
            ReSetChart(chart1, 1);
        }

        //============================================================================================================================================================================
        private void button1_Click(object sender, EventArgs e)
        {
            int pause = 2000;
            //Thread.Sleep(pause);
            glCameraNetwork.Screenshot(4);
            Thread.Sleep(pause);
            glCameraLayouts.Screenshot(4);
            Thread.Sleep(pause);
            glCameraMainWindow.Screenshot(4);
            Thread.Sleep(pause);
        }

        //============================================================================================================================================================================
        private void Button_IsovistAnalysis_Click(object sender, EventArgs e)
        {
            IsovistAnalye();
            comboBox_Isovist.SelectedIndex = 1;
            DrawLayout();
        }

        //============================================================================================================================================================================
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            glCameraMainWindow.ZoomAll();
            glCameraLayouts.ZoomAll();
            glCameraNetwork.ZoomAll();
        }

        //============================================================================================================================================================================
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (_activeCameraControl != null) _activeCameraControl.CameraChange();
            else glCameraMainWindow.CameraChange();

            if (ServiceManager.mqttClient != null)
            {
                if (glCameraMainWindow.ActiveCamera == glCameraMainWindow.Camera_2D)
                    ServiceManager.mqttClient.Publish("Camera2D", Encoding.UTF8.GetBytes("Camera2D"));

                if (glCameraMainWindow.ActiveCamera == glCameraMainWindow.Camera_3D)
                    ServiceManager.mqttClient.Publish("Camera3D", Encoding.UTF8.GetBytes("Camera3D"));
            }
        }

        //============================================================================================================================================================================
        private void uD_treeDepth_ValueChanged_1(object sender, EventArgs e)
        {
            SetControlParameters();
        }

        //==============================================================================================
        private void uD_generations_ValueChanged(object sender, EventArgs e)
        {
            SetControlParameters();
        }

        //==============================================================================================
        private void uD_minNrBuildings_ValueChanged(object sender, EventArgs e)
        {
            if (Math.Pow(Convert.ToInt16(uD_minSideAbs.Value), 2) < Convert.ToInt16(uD_minAbsBldArea.Value))
                uD_minAbsBldArea.Value = ((int)(Math.Pow(Convert.ToInt16(uD_minSideAbs.Value), 2)));
            SetControlParameters();
        }

        //============================================================================================================================================================================
        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            SetGraphParameter();
            int steps = 0; // 1000;
            for (int i = 0; i < steps; i++)
            {
                _streetGenerator.GrowStreetPatternBasic();

            }
            _streetGenerator.CleanUpSubdiv();
            DrawNetwork(_streetGenerator.Graph);

            glCameraMainWindow.ZoomAll();
        }

        //============================================================================================================================================================================
        private void toolStripButton5_Click_1(object sender, EventArgs e)
        {
            int steps = 100;
            for (int i = 0; i < steps; i++)
            {
                _streetGenerator.GrowStreetPatternBasic();
            }
            //_streetGenerator.CleanUpSubdiv();
            DrawNetwork(_streetGenerator.Graph);
            //AnalyseGraph();
            CreateBlocks(_streetGenerator.Graph);
            glCameraMainWindow.ZoomAll();
        }

        //============================================================================================================================================================================
        private void B_Analyse_Click(object sender, EventArgs e)
        {
            AnalyseGraph();
        }

        //============================================================================================================================================================================
        private void cB_updateLucy_CheckedChanged(object sender, EventArgs e)
        {

            if (LucyUseServicesCheckBox.Checked)
            {
                if (GlobalConnection.CL == null)
                {
                    LucyConnectClick(null, null);
                }
                RunVia = new CalculateVia(CalculationType.Method.Luci, CalculationType.Method.Luci, CalculationType.Method.Luci, LuciConnection.IP, LuciConnection.Port); // all via luci
            }
            else
                RunVia = new CalculateVia(0,0,0, LuciConnection.IP, LuciConnection.Port); // all local

        }

        //============================================================================================================================================================================
        private void cB_useSolarLucy_CheckedChanged(object sender, EventArgs e)
        {
            if (LucyUseSolarServiceCheckBox.Checked)
            {
                if (GlobalConnection.CL == null)
                {
                    LucyConnectClick(null, null);
                }
                RunVia = new CalculateVia(CalculationType.Method.Luci, CalculationType.Method.Luci, CalculationType.Method.Luci, LuciConnection.IP, LuciConnection.Port); // all via luci
            }
            else
                RunVia = new CalculateVia(0, 0, 0, LuciConnection.IP, LuciConnection.Port); // all local

        }

        //============================================================================================================================================================================
        private void glCameraMainWindow_MouseUp(object sender, MouseEventArgs e)
        {
            _activeCameraControl = glCameraMainWindow;
        }

        //============================================================================================================================================================================
        private void glCameraLayouts_MouseUp(object sender, MouseEventArgs e)
        {
            _activeCameraControl = glCameraLayouts;
        }

        //============================================================================================================================================================================
        private void glCameraNetwork_MouseUp(object sender, MouseEventArgs e)
        {
            _activeCameraControl = glCameraNetwork;
        }

        //============================================================================================================================================================================
        private void button2_Click(object sender, EventArgs e)
        {
            // Show the color dialog.
            DialogResult result = colorDialog1.ShowDialog();
            // See if user pressed ok.
            if (result == DialogResult.OK)
            {
                // Set glCamera background to the selected color.
                glCameraMainWindow.BackgroundColor = colorDialog1.Color;
                glCameraLayouts.BackgroundColor = colorDialog1.Color;
                glCameraNetwork.BackgroundColor = colorDialog1.Color;
                glCameraMainWindow.Invalidate();
                glCameraLayouts.Invalidate();
                glCameraNetwork.Invalidate();
            }
        }

        //============================================================================================================================================================================
        private void B_TestTree_Click(object sender, EventArgs e)
        {
            //_remainDrawingObjects = new ObjectsDrawList();
            //if(_scenario.EnvironmentCloseNetwork != null)
            //foreach (Line2D line in _scenario.EnvironmentCloseNetwork)
            //{
            //    _remainDrawingObjects.Add(new GLine(line));
            //}

            var timer = new Stopwatch();
            var sb = new StringBuilder();

            if (_scenario.Border != null) InitialScenarioGraph();
            else InitialTestGraph();

            UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> networkQ;
            _activeNetwork = new InstructionTreePisa(ControlParameters.TreeDepth, _initailNet, _border);

            // --------------------------------------------
            networkQ = _activeNetwork.CreateStreetNetwork();

            //bool analyseAll = false;//cB_analyseAll.Checked;
            List<Line2D> edges = GraphTools.ToListLine2D(networkQ);
            //if (analyseAll)
            //{
            //    var allEdges = new List<Line2D>(edges);
            //    if (_scenario.EnvironmentCloseNetwork != null)
            //        allEdges.AddRange(_scenario.EnvironmentCloseNetwork);
            //    AnalyseNetwork(allEdges, glCameraMainWindow);
            //}
            //else
            {
                AnalyseNetwork(networkQ, glCameraMainWindow);
            }

            CreateBlocks(edges);

            // -- test bounding circle --
            /*Vector2d center = new Vector2d();
            float radius = -1;
            Poly2D block = _blocks[0];
            List<Vector2d> blockPts = block.PointsFirstLoop.ToList();
            GeoAlgorithms2D.MinimalBoundingCircle(blockPts, out center, out radius);
            GCircle myCircle = new GCircle(center, radius);
            myCircle.DeltaZ = 1;
            glCameraMainWindow.AllObjectsList.Add(myCircle);
            // -- draw the block points for testing --
            List<Vector2d> points = blockPts;
            foreach (Vector2d point in points)
            {
                GCircle gPt = new GCircle(new Vector2d(point.X, point.Y), 1.0, 10);
                gPt.DeltaZ = 1.1f;
                gPt.Filled = true;
                gPt.FillColor = ConvertColor.CreateColor4(Color.LawnGreen);
                glCameraMainWindow.AllObjectsList.Add(gPt, true, true);
            }*/

            DrawLayout();
            //AnalyseNetwork(edges);
            //if (testing)
            //{
            //    sb.Append("Analysis: " + timer.ElapsedMilliseconds + "ms \n");
            //    sb.Append("\nNetwork edges: " + networkQ.Edges.Count() + "\n");
            //    sb.Append("Network verticles: " + networkQ.Vertices.Count() + "\n");
            //    timer.Stop();
            //    MessageBox.Show(sb.ToString());
            //}

            //_remainDrawingObjects = glCameraMainWindow.AllObjectsList.ObjectsToDraw;
            glCameraMainWindow.ZoomAll();
        }

        //============================================================================================================================================================================
        private void Button_StartStop_Click(object sender, EventArgs e)
        {
            timer1.Enabled = !timer1.Enabled;
        }

        //============================================================================================================================================================================
        private void comboBox_IsovistIndexChanged(object sender, EventArgs e)
        {
            if (comboBox_Isovist.Text == "Solar Analysis")
            {
                ControlParameters.VisualiseAnalysis = FieldForVisual.SolarAnalysis;
                if (_calculatorSolarAnalysis != null)
                    _smartGrid = _calculatorSolarAnalysis.SmartGrid;
            }
            else
            {
                ControlParameters.VisualiseAnalysis = FieldForVisual.IsovistAnalysis;
                ControlParameters.DisplayMeasure = comboBox_Isovist.Text;
                if (_isovistField != null)
                {
                    _isovistField.DisplayMeasure = comboBox_Isovist.Text;
                    //_isovistField.WriteToGridValues();
                    string measure = comboBox_Isovist.SelectedItem.ToString();
                    _smartGrid = new GGrid(_isovistField.AnalysisPoints, _isovistField.GetMeasure(measure),
                        _isovistField.CellSize);
                    glCameraMainWindow.Invalidate();
                }
            }
            DrawLayout();

            if (comboBox_Isovist.Text != "Solar Analysis")
                DrawLayoutSOM();
        }

        //============================================================================================================================================================================
        private void propertyGrid2_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if (_isovistField != null)
            {
                // Rect2D borderRect = GeoAlgorithms2D.BoundingBox(_isovistBorder.PointsFirstLoop);
                // _isovistField = new IsovistFieldOLD(borderRect, 5, _obstLines, _obstRects, _isovistBorder);
                _isovistField.Calculate(_isovistField.Precision);
                glCameraMainWindow.Invalidate();
            }
        }

        //============================================================================================================================================================================
        private void propertyGrid1_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            //zunächt benötigen wir das aktuell im PropertyGrid angezeigt Objekt. Da nicht bekannt ist, um welches es sich handelt, 
            //erfolgt hier der Aufrug allgemeine als "object"
            object obj;
            if (e.ChangedItem.Parent.Value != null) obj = e.ChangedItem.Parent.Value;
            else obj = propertyGrid1.SelectedObject;

            //nun wird geprüft, ob es sich bei dem geänderten Wert um ein Property handelte!
            if (e.ChangedItem.GridItemType == GridItemType.Property)
            {
                if (e.ChangedItem.PropertyDescriptor != null)
                {
                    PropertyInfo pInfo = obj.GetType().GetProperty(e.ChangedItem.PropertyDescriptor.Name);
                    pInfo.SetValue(obj, e.ChangedItem.Value, null);
                    if (_lastSelectedIdx != -1)
                    {
                        _curGen = _activeLayout.Gens[_lastSelectedIdx];
                        pInfo.SetValue(_curGen, e.ChangedItem.Value, null);
                    }
                }
            }
            glCameraMainWindow.Invalidate();
        }

        //============================================================================================================================================================================
        private void toolStripButton11_Click(object sender, EventArgs e)
        {
            AnalyseScenario();
        }

        //============================================================================================================================================================================
        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            glCameraNetwork.AllObjectsList = new ObjectsAllList();
            _allNetworks = new List<InstructionTreePisa>();
            _SOMOrderedArchive = new List<MultiPisaChromosomeBase>();
            _population = null;
            _activeLayout = null;
            _selectedBlock = null;
            glCameraNetwork.Invalidate();
            ReSetChart(chart1, 0);
            ReSetChart(chart1, 1);
            // get population size
            try
            {
                _populationSize = Convert.ToInt16(uD_populationSize.Value);
            }
            catch
            {
                _populationSize = 20;
            }
            // iterations
            try
            {
                _iterations = Convert.ToInt16(uD_generations.Value);
            }
            catch
            {
                _iterations = 20;
            }
            if (_scenario.Border != null) InitialScenarioGraph();
            else InitialTestGraph();

            glCameraNetwork.DeactivateInteraction();

            // run worker thread           
            _needToStop1 = false;
            if (sB_multiOpti.Checked)
            {
                _workerThread = new Thread(SearchNetworkMulti);

                /* *ac*
                _workerThread = new Thread(() =>
                {
                    SearchNetworkMulti().Wait();
                });*/
            }
            if (sB_singleOpti.Checked)
            {
                _workerThread = new Thread(SearchNetworkSingle);
            }
            _workerThread.Start();
        }

        # endregion

        private void toolStripButtonSolar_Click(object sender, EventArgs e)
        {

        }


        //===================================================================================================================================================================
        //========= Unused =========
        //===================================================================================================================================================================
        #region Unused
        //============================================================================================================================================================================
        /*private void ScenarioToLucy()
        {
            var buildings2D = new List<Poly2D>();
            var buildings3D = new List<GPrism>();
            var edges = new List<Line2D>();

            // --- streets ---
            if (_activeNetwork != null)
            {
                UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> network;
                network = _activeNetwork.CreateStreetNetwork();
                edges = GraphTools.ToListLine2D(network);
            }

            // --- buildings ---
            if (_activeMultiLayout != null)
            {
                foreach (ChromosomeLayout subChromo in _activeMultiLayout.SubChromosomes)
                {
                    foreach (GenBuildingLayout gen in subChromo.Gens)
                    {
                        if (!gen.Open)
                            buildings2D.Add(gen.PolygonFromGen);
                    }
                }
                foreach (ChromosomeLayout subChromo in _activeMultiLayout.SubChromosomes)
                {
                    foreach (GenBuildingLayout gen in subChromo.Gens)
                    {
                        if (!gen.Open)
                        {
                            buildings3D.Add(new GPrism(gen.PolygonFromGen, gen.BuildingHeight));
                        }
                    }
                }
            }
            else if (_activeLayout != null)
            {
                foreach (GenBuildingLayout gen in _activeLayout.Gens)
                {
                    if (!gen.Open)
                        buildings2D.Add(gen.PolygonFromGen);
                }
                foreach (GenBuildingLayout gen in _activeLayout.Gens)
                {
                    if (!gen.Open)
                    {
                        buildings3D.Add(new GPrism(gen.PolygonFromGen, gen.BuildingHeight));
                    }
                }
            }

            // --- send the following: streets / blocks / buildings  / boudary / isovistPoints / ---
            var anaPts = new List<Vector2d>();
            var border = new Poly2D();
            if (_activeLayout != null)
            {
                border = _activeLayout.Border.Clone();
                if (_activeLayout.IsovistField != null) anaPts = _activeLayout.IsovistField.AnalysisPoints;
            }
             CreateIsovistLucyScenario(edges, _blocks, buildings2D, border, anaPts, buildings3D);
        }*/

        //============================================================================================================================================================================
        /*private void ArchivePartialToLucy()
        {
            // --- streets ---
            if (_activeNetwork != null)
            {
                UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> network;
                network = _activeNetwork.CreateStreetNetwork();
                GraphTools.ToListLine2D(network);
            }

            List<IMultiChromosome> archive = _population.Sel.GetArchive();
            var myRefChromo = (ChromosomeLayout) archive[0];
            Poly2D myBlockRef = myRefChromo.Border;

            // --- make new scenario for each subChromo ---
            foreach (var multiChromosome in archive)
            {
                var archChromo = (ChromosomeLayout) multiChromosome;
                List<Poly2D> buildings2D = new List<Poly2D>();
                List<GPrism> buildings3D = new List<GPrism>();
                // --- buildings ---
                if (_activeMultiLayout != null)
                {
                    foreach (ChromosomeLayout subChromo in _activeMultiLayout.SubChromosomes)
                    {
                        double overlap = subChromo.Border.Overlap(myBlockRef);
                        double ratio = overlap/myBlockRef.Area;
                        if (ratio > 0.9)
                        {
                            foreach (GenBuildingLayout gen in archChromo.Gens)
                            {
                                if (!gen.Open)
                                {
                                    buildings2D.Add(gen.Poly);
                                    buildings3D.Add(new GPrism(gen.Poly, gen.BuildingHeight));
                                }
                            }
                        }
                        else
                        {
                            foreach (GenBuildingLayout gen in subChromo.Gens)
                            {
                                if (!gen.Open)
                                {
                                    buildings2D.Add(gen.Poly);
                                    buildings3D.Add(new GPrism(gen.Poly, gen.BuildingHeight));
                                }
                            }
                        }
                    }
                }

                // --- send the following: streets / blocks / buildings  / boudary / isovistPoints / ---
                var anaPts = new List<Vector2d>();
                var border = new Poly2D();
                if (_activeLayout != null)
                {
                    border = _activeLayout.Border.Clone();
                    //if (_activeLayout.IsovistField != null) anaPts = _activeLayout.IsovistField.AnalysisPoints;
                }
                //CreateIsovistLucyScenario(edges, _blocks, buildings2D, border, anaPts, buildings3D);
            }
        }*/

        //============================================================================================================================================================================
        /*private void ArchiveToLucy()
        {
            TextRW.LucyIDs = new List<int>();
            List<IMultiChromosome> archive = _population.Sel.GetArchive();
            for (int p = 0; p < archive.Count(); p++)
            {
                var curMultiChromo = (ChromosomeMultiLayout) (archive[p]);

                // --- make new scenario for each subChromo ---
                foreach (ChromosomeLayout subChromo in curMultiChromo.SubChromosomes)
                {
                    // --- streets --- always the same at the moment...
                    var edges = new List<Line2D>();
                    if (_activeNetwork != null)
                    {
                        UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> network;
                        network = _activeNetwork.CreateStreetNetwork();
                        edges = GraphTools.ToListLine2D(network);
                    }

                    var buildings2D = new List<Poly2D>();
                    var buildings3D = new List<GPrism>();

                    // --- buildings ---
                    if (subChromo.Gens.Count > 0)
                    {
                        foreach (GenBuildingLayout gen in subChromo.Gens)
                        {
                            if (!gen.Open)
                                buildings2D.Add(gen.Poly);
                        }
                        foreach (GenBuildingLayout gen in subChromo.Gens)
                        {
                            if (!gen.Open)
                            {
                                buildings3D.Add(new GPrism(gen.Poly, gen.BuildingHeight));
                            }
                        }
                    }

                    // --- isovist analysis points ---
                    var anaPts = new List<Vector2d>();
                    var border = new Poly2D();
                    if (subChromo.IsovistField != null)
                    {
                        border = subChromo.Border.Clone();
                        anaPts = subChromo.IsovistField.AnalysisPoints;
                    }
                    // --- send the following: streets / blocks / buildings  / boudary / isovistPoints / 3D buildings---
                    // CreateIsovistLucyScenario(edges, _blocks, buildings2D, border, anaPts, buildings3D);
                }
            }
        }*/

        /*=============GeoJasonTest==================================================================================================================================================
        //private void GeoJasonTest()
        //{
        //    GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
        //    GeoJsonReader GeoJSONReader = new GeoJsonReader();

        //    List<CPlan.Geometry.Vector2d> points = new List<CPlan.Geometry.Vector2d>();
        //    points.Add(new CPlan.Geometry.Vector2d(1, 1));
        //    points.Add(new CPlan.Geometry.Vector2d(3, 2));

        //    List<CPlan.Geometry.Line2D> lines = new List<CPlan.Geometry.Line2D>();
        //    lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2d(0, 0), new CPlan.Geometry.Vector2d(0, 10)));
        //    lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2d(0, 10), new CPlan.Geometry.Vector2d(10, 10)));
        //    lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2d(0, 0), new CPlan.Geometry.Vector2d(10, 0)));
        //    lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2d(10, 0), new CPlan.Geometry.Vector2d(10, 10)));
        //    lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2d(2, 5), new CPlan.Geometry.Vector2d(8, 5)));

        //    //create sample iso object on client
        //    IsovistField2D iso_client_orig = new IsovistField2D(points, lines);

        //    //pass it to the wrapper and serialize to JSON
        //    IsovistField2DWrapper wrapper = new IsovistField2DWrapper(iso_client_orig);
        //    string iso_client_send = GeoJSONWriter.Write(wrapper);
        //    LucyConsoleLogBox.Text += "--- SEND --- : \r\n";
        //    LucyConsoleLogBox.Text += iso_client_send + " \r\n";

        //    //read json on server and deserialize into wrapper
        //    IsovistField2DWrapper iso_wrapper_server = GeoJSONReader.Read<IsovistField2DWrapper>(iso_client_send);
        //    //conver wrapper to isovist object
        //    IsovistField2D iso_server = iso_wrapper_server.ToIsovistField2D();

        //    //perform actual calculations, the precision need to be set explicitly here since we can't do it using isovist constructor
        //    iso_server.Calculate(iso_wrapper_server.Precision, true);

        //    //wrap the results
        //    IsovistField2DWrapper iso_server_wrapper_result = new IsovistField2DWrapper(iso_server);
        //    string iso_server_send = GeoJSONWriter.Write(iso_server_wrapper_result);
        //    LucyConsoleLogBox.Text += "--- Receive ---: \r\n";
        //    LucyConsoleLogBox.Text += iso_server_send + " \r\n";

        //    //read results on client           
        //    IsovistField2DWrapper iso_client_wrapper_result = GeoJSONReader.Read<IsovistField2DWrapper>(iso_server_send);
        //    //here we are unable to convert back to isovist object since there are no public method allowing us to set the results.
        //    //we can change the isovist public API to allow this in the future.
        //}
        */

        /*=============ExecuteCommandAsync===================================================================================================================================================
        ///// <summary>
        ///// </summary>
        ///// <param name="edges"></param>
        ///// <param name="blocks"></param>
        ///// <param name="buildings"></param>
        ///// <param name="boundary"></param>
        ///// <param name="analysisPoints"></param>     
        ///// <span class="code-SummaryComment"><summary></span>
        ///// Execute the command Asynchronously.
        ///// Code from: http://www.codeproject.com/Articles/25983/How-to-Execute-a-Command-in-C
        ///// <span class="code-SummaryComment"></summary></span>
        ///// <span class="code-SummaryComment"><param name="command">string command.</param></span>
        //public void ExecuteCommandAsync(string command)
        //{
        //    try
        //    {
        //        //Asynchronously start the Thread to process the Execute command request.
        //        Thread objThread = new Thread(new ParameterizedThreadStart(ExecuteCommandSync));
        //        //Make the thread as background thread.
        //        objThread.IsBackground = true;
        //        //Set the Priority of the thread.
        //        objThread.Priority = ThreadPriority.AboveNormal;
        //        //Start the thread.
        //        objThread.Start(command);
        //    }
        //    catch (ThreadStartException objException)
        //    {
        //        // Log the exception
        //    }
        //    catch (ThreadAbortException objException)
        //    {
        //        // Log the exception
        //    }
        //    catch (Exception objException)
        //    {
        //        // Log the exception
        //    }
        //}
        */

        #endregion
    }
}
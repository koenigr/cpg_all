﻿//#if CT_UNCOMMENT
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using netDxf.Entities;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using CPlan.Geometry;
using CPlan.Optimization;
using CPlan.Evaluation;
using Tektosyne.Geometry;
using System.Threading;
using QuickGraph;
using AForge.Genetic;
using System.Diagnostics;
using netDxf;
using netDxf.Header;
using System.IO;
using System.Reflection;
using CPlan.VisualObjects;
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using CPlan.Isovist2D;
using GeoAPI.Geometries;
//using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ObjLoader.Loader.Loaders;
//using uPLibrary.Networking.M2Mqtt;
//using CPlan.CommunicateToLucy;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public enum AnalysisType { Isovist, Solar };

    public partial class Form1 : Form
    {
        #region Fields 

        private AnalysisType _analysisType = AnalysisType.Isovist;
        private ViewControl ViewControl1;
        private ViewControl ViewControl2;
        private ViewControl ViewControl3;

        StreetPattern StreetGenerator;
        UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> _initailNet;
        TimeSpan span;

        private int _populationSize = 40;
        private int _iterations = 100;

        private Thread _workerThread = null;
        private volatile bool _needToStop1 = false;
        private volatile bool _needToStop2 = false;
        private MultiPopulation _population;
        private PopulationParameters _populationParameters;
        private Population _populationSingle;
        private Poly2D _border;
        private Poly2D _isovistBorder;
        private BackgroundWorker backWorker;

        // --- morphological parameter ---
        int maxEdges = 4;
        int rndAngle = 10;
        double minDist = 20;
        double minAngle = 80;
        int maxLength = 100;
        int minLength = 30;
        bool rotSymmetryL = false;
        bool rotSymmetryR = false;

        // --- value List --
        List<List<double>> _fitnesLists = new List<List<double>>();

        private ViewControlParameters _parameters = new ViewControlParameters();
        public static String MouseMode { get; set; }
        private ObjectsDrawList constantDrawObjects;
        private List<MultiPisaChromosomeBase> _globalArchive;
        private List<MultiPisaChromosomeBase> _SOMOrderedArchive;
        private int[,] _somOrder;
        GGrid _smartGrid = null;

        List<GeoObject> _remainDrawingObjects = new List<GeoObject>();

        //private MqttClient mqttClient;
        //private ConfigSettings _configInfo;

        # endregion

        //===================================================================================================================================================================
        public Form1()
        {
            InitializeComponent();

            MouseMode = "ScreenMove";

            // --- this needs to be copied for a new glcontrol ----------------------------------------------------
            ViewControl1 = new ViewControl(glControl1); // --- new instance for glControl methods ---
            this.glControl1.MouseMove += glControl1_MouseMove;
            this.glControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseDown);
            this.glControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseUp);
            this.glControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.glControl1_Paint);
            // ----------------------------------------------------------------------------------------------------
            ViewControl2 = new ViewControl(glControl2); // --- new instance for glControl methods ---
            //this.glControl2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.glControl2_MouseMove);
            //this.glControl2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.glControl2_MouseDown);
            this.glControl2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.glControl2_MouseUp);
            this.glControl2.Paint += new System.Windows.Forms.PaintEventHandler(this.glControl2_Paint);
            // ----------------------------------------------------------------------------------------------------
            // ----------------------------------------------------------------------------------------------------
            ViewControl3 = new ViewControl(glControl3); // --- new instance for glControl methods ---
            //this.glControl2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.glControl2_MouseMove);
            //this.glControl2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.glControl2_MouseDown);
            this.glControl3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.glControl3_MouseUp);
            this.glControl3.Paint += new System.Windows.Forms.PaintEventHandler(this.glControl3_Paint);

            // --- zoom all ---
            ViewControl1.ZoomAll();
            ViewControl2.ZoomAll();
            ViewControl3.ZoomAll();
            SetControlParameters();
            SetScenarioEnvironment();
            SetControlParameters();

            // -- chart
            //  chart1.ChartAreas[0].AxisX.Maximum = 1.0;// 100000;// 1.0;// .3;
            //  chart1.ChartAreas[0].AxisY.Maximum = 1.0;// 100;// .3;
            //chart1.ChartAreas[0].AxisX.MinorGrid.Enabled = false;
            //chart1.ChartAreas[0].AxisX.MajorGrid.Interval = .1;// 1.0 / 32;
            //chart1.ChartAreas[0].AxisY.MinorGrid.Enabled = false;
            //chart1.ChartAreas[0].AxisY.MajorGrid.Interval = .1;//1.0 / 32;
            chart1.Series[1].MarkerSize = 3;
            chart1.Series[0].MarkerSize = 6;

            chart1.Series[0].Points.Clear();

            propertyGrid3.SelectedObject = _parameters;

            Poly2D poly1 = new Poly2D(0, 0, 100, 100);
            Poly2D poly2 = new Poly2D(60, 60, 150, 150);
            Poly2D isectRect = (poly1.Intersection(poly2))[0];

            _globalArchive = new List<MultiPisaChromosomeBase>();

            // --- terminate running PISA processes ---
            // first, test if there is no running process - otherwise the new one crashes. 
            // use the corresponding name for the selector used in the next line. 
            // for the process name look into MultiPopulation constructor.
            TerminateProcess("hype"); //("spea2");
            TerminateProcess("spea2");

            //string mqtt_host = "192.168.2.100";// "localhost";//"129.132.6.4"; //
            //mqttClient = new MqttClient(System.Net.IPAddress.Parse(mqtt_host));// MqttManager.getInstance().GetMQTTClient();
            //mqttClient.Connect("myClient");
            //mqttClient.MqttMsgPublishReceived += mqttClient_MqttMsgPublishReceived;
        }

        //void mqttClient_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        //{
        //    if (_configInfo != null && _configInfo.mqtt_topic == e.Topic)
        //    {
        //        string message = Encoding.UTF8.GetString(e.Message);
        //        if (message == "DONE")
        //        {
        //            int sobjID = _configInfo.objID;


        //            ReadIsovistResults(sobjID);
        //            /*                        System.Windows.Application.Current.Dispatcher.Invoke((Action)(delegate
        //                                    {
        //                                        readResultsOfService(index, sobjID);
        //                                    }));*/
        //        }
        //    }
        //}

        //===================================================================================================================================================================
        private void Form1_Load(object sender, EventArgs e)
        {
            backWorker = new BackgroundWorker();
            backWorker.WorkerReportsProgress = true;
            backWorker.WorkerSupportsCancellation = true;

            backWorker.DoWork += new DoWorkEventHandler(backWorker_DoWork);
            // backWorker.ProgressChanged += new ProgressChangedEventHandler(backWorker_ProgressChanged);
            backWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backWorker_RunWorkerCompleted);

            PisaWrapper.PISA_DIR = @Properties.Settings.Default.PathToPISA;
        }

        //===================================================================================================================================================================
        /// <summary>
        /// Terminate a running process.
        /// </summary>
        /// <param name="processName">Name of the process.</param>
        private void TerminateProcess(string processName)
        {
            Process[] MatchingProcesses = Process.GetProcessesByName(processName);

            if (MatchingProcesses.Length > 0)
            {
                foreach (Process p in MatchingProcesses)
                {
                    p.CloseMainWindow();
                }

                System.Threading.Thread.Sleep(500);

                MatchingProcesses = Process.GetProcessesByName(processName);

                foreach (Process p in MatchingProcesses)
                {
                    if (p.HasExited == false)
                        p.Kill();
                }
            }
        }

        //===================================================================================================================================================================
        //========= Paint Event =========
        //===================================================================================================================================================================
        # region paint ebvents
        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            // -- enter pait istruction here --   
            ViewControl1.AllObjectsList.Draw();
            if (_loaded_scene) DrawObjModels(_obj_load_result);

            // -- end of paint instructions ---
            //GL.PopMatrix();
            glControl1.SwapBuffers();
        }

        //===================================================================================================================================================================
        private void glControl2_Paint(object sender, PaintEventArgs e)
        {
            // -- enter pait istruction here --   
            ViewControl2.AllObjectsList.Draw();

            // -- end of paint instructions ---
            //GL.PopMatrix();
            glControl2.SwapBuffers();
        }

        //===================================================================================================================================================================
        private void glControl3_Paint(object sender, PaintEventArgs e)
        {
            // -- enter pait istruction here --   
            ViewControl3.AllObjectsList.Draw();

            // -- end of paint instructions ---
            //GL.PopMatrix();
            glControl3.SwapBuffers();
        }
        # endregion

        //===================================================================================================================================================================
        //========= Mouse Controls =========
        //===================================================================================================================================================================
        # region mouse controls

        private Poly2D _movingChild, _selectedBlock;
        private GenBuildingLayout _curGen;
        private int _movingIdx = -1;
        private ChromosomeLayout _selectedLayout = null;
        private int _selLayoutId = 0;
        private int _lastSelectedIdx = -1;
        private Vector2D _deltaM = new Vector2D();
        private Vector2D _oldMousePos = new Vector2D();
        private bool _wasLocked = false;
        //===================================================================================================================================================================
        private void glControl1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            // -- interaction with building layout --
            if (_movingIdx == -1)
            {
                if (e.Button == MouseButtons.Left) // left button to move polygons
                {
                    CGeom3d.Vec_3d point_xy = ViewControl1.Camera.GetPointXYPtn(e.X, e.Y, 0);
                    if (point_xy == null) return;
                    Vector2D p = new Vector2D(point_xy.x, point_xy.y);

                    // -- interact with layout --
                    if (_allLayouts != null && _allLayouts.Count > 0)
                    for (int l = 0; l < _allLayouts.Count; l++)
                    {
                        ChromosomeLayout layout = _allLayouts[l];
                        if (layout.Border.ContainsPoint(p)) // select the layout
                        {
                            _activeLayout =  _allLayouts[l];
                            _selLayoutId = l;
                            _selectedLayout = layout;
                            if (layout != null)
                            {
                                for (int i = 0; i < layout.Gens.Count; i++)
                                {
                                    Poly2D go = layout.Gens[i].Poly;

                                    if (go.ContainsPoint(p))
                                    {
                                        _movingChild = go;
                                        _movingIdx = i;
                                        _lastSelectedIdx = i;
                                        _wasLocked = layout.Gens[_movingIdx].Locked;

                                        _curGen = layout.Gens[_movingIdx];
                                        _deltaM = new Vector2D(p - _curGen.Position);
                                        _curGen.Locked = true;

                                        break;
                                    }
                                }
                                _oldMousePos = p;
                            }
                        }
                    }

                    // -- interact with layout --
                    if (_blocks != null)
                    {
                        for (int i = 0; i < _blocks.Count; i++)
                        {
                            Poly2D testPoly = _blocks[i];

                            if (testPoly.ContainsPoint(p))
                            {
                                _selectedBlock = testPoly;
                                _borderPoly = new GPolygon(testPoly);

                            }
                        }
                    }

                }
            }
        }

        //===================================================================================================================================================================
        private void glControl1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            // -- interaction with building layout --
            Vector2D vel = new Vector2D();
            GenBuildingLayout curGen;
            CGeom3d.Vec_3d point_xy = ViewControl1.Camera.GetPointXYPtn(e.X, e.Y, 0);

            if (_movingChild != null)
            {
                // Wenn gerade ein Objekt verschoben werden soll, wird die Differenz zur letzten
                // Mausposition ausgerechnet und das Objekt um diese verschoben.              
                if (point_xy == null) return;
                Cursor tmpCursor = Cursors.Default;
                Vector2D p = new Vector2D(point_xy.x, point_xy.y);
                if (_selectedLayout != null)
                {
                    curGen = _selectedLayout.Gens[_movingIdx];

                    if (Control.ModifierKeys == Keys.Shift || MouseMode == "Rotate")
                    {
                        double deg = _oldMousePos.Y - p.Y;
                        curGen.Rotation += deg;
                    }
                    else if (MouseMode == "Scale")
                    {
                        double fact = 1;
                        Vector2D oldCenter = new Vector2D(curGen.Center);
                        double delta = (_oldMousePos.Y - p.Y) * fact;
                        double newWidth = curGen.Width + delta;
                        double newHeith = curGen.Area / newWidth;
                        if (newWidth > curGen.MinSide && newHeith > curGen.MinSide)
                        {
                            curGen.Width = newWidth;
                            curGen.Height = newHeith;
                            curGen.Center = oldCenter;
                        }
                    }
                    else if (MouseMode == "Size")
                    {
                        double fact = 20;
                        double delta = (_oldMousePos.Y - p.Y) * fact;
                        curGen.Area += delta;
                    }
                    else  // move
                    {
                        Vector2D testNewVect = new Vector2D(p - _deltaM); ;
                        curGen.Position = testNewVect;
                    }
                }
                _oldMousePos = p;
            }

            if (point_xy != null) statusLabel.Text = "X: " + point_xy.x.ToString() + ", Y: " + point_xy.y.ToString();
            ViewControl1.Invalidate();
        }

        //===================================================================================================================================================================
        private void glControl1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            // -- interaction with building layout --
            Vector2D vel = new Vector2D();
            GenBuildingLayout curGen;
            vel.X = 0;
            vel.Y = 0;
            {
                if (_selectedLayout != null)
                {
                    if (_movingChild != null)
                    {
                        _movingChild = null;
                        curGen = _selectedLayout.Gens[_movingIdx];
                        curGen.Locked = _wasLocked;
                        propertyGrid1.SelectedObject = curGen;
                        if (ControlParameters.instantUpdate) IsovistAnalye();
                    }
                }
            }
            if (cB_realUpdate.Checked) { 
                _activeLayout = _selectedLayout;
                if (_analysisType == AnalysisType.Solar)
                    SolarAnalysis();
                if (_analysisType == AnalysisType.Isovist)
                    IsovistAnalye();          
            }
            _selectedLayout = null;
            _movingIdx = -1;
        }

        //===================================================================================================================================================================
        //================glControl2======================================================================================================================================
        //===================================================================================================================================================================
        private void glControl2_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            int id = -1;
            // --- find the ID of the chromosome and send it to the other window for interaction ---
            if (e.Button == MouseButtons.Left) 
            {
                if (ViewControl2.AllObjectsList.ObjectsToDraw != null)
                {
                    CGeom3d.Vec_3d point_xy = ViewControl2.Camera.GetPointXYPtn(e.X, e.Y, 0);
                    if (point_xy == null) return;
                    Vector2D p = new Vector2D(point_xy.x, point_xy.y);

                    for (int i = 0; i < ViewControl2.AllObjectsList.ObjectsToDraw.Count; i++)
                    {
                        GeoObject go = ViewControl2.AllObjectsList.ObjectsToDraw[i];
                        if (go.ContainsPoint(p))
                        {
                            id = go.Identity;
                            break;
                        }
                    }

                    if (id > -1)
                    {
                        MultiPisaChromosomeBase curChromo;
                        if (_SOMOrderedArchive != null)
                        {
                            curChromo = _SOMOrderedArchive.Find(ChromosomeLayout => ChromosomeLayout.Indentity == id);                          
                        }
                        else
                        {
                            curChromo = _globalArchive.Find(ChromosomeLayout => ChromosomeLayout.Indentity == id);
                        }

                        if (curChromo == null)
                        {
                            curChromo = _globalArchive.Find(ChromosomeLayout => ChromosomeLayout.Indentity == id);
                        }
                        if (curChromo != null)
                        {
                            if (curChromo.GetType() == typeof(ChromosomeLayout))
                            {
                                _allLayouts[_selLayoutId] = (ChromosomeLayout)curChromo;
                                _activeLayout = _allLayouts[_selLayoutId];
                                //_smartGrid = new GGrid(_allLayouts[_selLayoutId].IsovistField);
                                IsovistAnalye();
                                DrawLayout();
                            }
                        }
                    }
                }
            }

        }

        //===================================================================================================================================================================
        //================glControl3======================================================================================================================================
        //===================================================================================================================================================================
        private void glControl3_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            int id = -1;
            // --- find the ID of the chromosome and send it to the other window for interaction ---
            if (e.Button == MouseButtons.Left)
            {
                if (ViewControl3.AllObjectsList.ObjectsToDraw != null)
                {
                    CGeom3d.Vec_3d point_xy = ViewControl3.Camera.GetPointXYPtn(e.X, e.Y, 0);
                    if (point_xy == null) return;
                    Vector2D p = new Vector2D(point_xy.x, point_xy.y);

                    for (int i = 0; i < ViewControl3.AllObjectsList.ObjectsToDraw.Count; i++)
                    {
                        GeoObject go = ViewControl3.AllObjectsList.ObjectsToDraw[i];
                        if (go.ContainsPoint(p))
                        {
                            id = go.Identity;
                            break;
                        }
                    }

                    if (id > -1)
                    {
                        MultiPisaChromosomeBase curChromo;
                        if (_SOMOrderedArchive != null)
                        {
                            curChromo = _SOMOrderedArchive.Find(InstructionTreePisa => InstructionTreePisa.Indentity == id);
                        }
                        else
                        {
                            curChromo = _globalArchive.Find(InstructionTreePisa => InstructionTreePisa.Indentity == id);
                        }

                        if (curChromo == null)
                        {
                            curChromo = _globalArchive.Find(InstructionTreePisa => InstructionTreePisa.Indentity == id);
                        }
                        if (curChromo != null)
                        {
                            if (curChromo.GetType() == typeof(InstructionTreePisa))
                            {
                                _activeNetwork = (InstructionTreePisa)curChromo;
                                DrawAnalysedNetwork(_activeNetwork, ViewControl1);
                                ViewControl1.Invalidate();
                            }
                        }

                    }
                }
            }

        }

        # endregion

        //===================================================================================================================================================================
        //========= Button Controls =========
        //===================================================================================================================================================================
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            ViewControl1.ZoomAll();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            ViewControl1.CameraChange();
        }

        //============================================================================================================================================================================
        private void uD_treeDepth_ValueChanged_1(object sender, EventArgs e)
        {
            SetControlParameters();
        }

        //==============================================================================================
        private void rB_fitChoice_CheckedChanged(object sender, EventArgs e)
        {
            SetControlParameters();
        }

        //==============================================================================================
        private void rB_fitCentral_CheckedChanged(object sender, EventArgs e)
        {
            SetControlParameters();
        }

        //==============================================================================================
        private void uD_generations_ValueChanged(object sender, EventArgs e)
        {
            SetControlParameters();
        }

        //==============================================================================================
        private void uD_minNrBuildings_ValueChanged(object sender, EventArgs e)
        {
            if (Math.Pow(Convert.ToInt16(uD_minBldSide.Value), 2) < Convert.ToInt16(uD_minAbsBldArea.Value))
                uD_minAbsBldArea.Value = ((int)(Math.Pow(Convert.ToInt16(uD_minBldSide.Value), 2)));
            SetControlParameters();
        }

        //============================================================================================================================================================================
        private void SetGraphParameter()
        {
            // --- morphological parameter ---
            maxEdges = Convert.ToInt16(uD_connectivity.Value);
            rndAngle = Convert.ToInt16(uD_angle.Value);
            minDist = Convert.ToInt16(uD_distance.Value);
            minAngle = Convert.ToInt16(uD_divAngle.Value);
            maxLength = Convert.ToInt16(uD_maxLength.Value);
            minLength = Convert.ToInt16(uD_minLength.Value);
            _border = new Rect2D(0, 0, 2000, -2000).ToPoly2D();//glControl1.Left, glControl1.Top, glControl1.Width, -glControl1.Height);
            StreetGenerator = new StreetPattern(maxEdges, rndAngle, minDist, minAngle, maxLength, minLength, rotSymmetryL, rotSymmetryR, _border);

            // --- TestGraph ---
            PointD midPt = new PointD(_border.BoundingBox().Width / 2, _border.BoundingBox().Height / 2);
            PointD[] iniPts;
            iniPts = new PointD[2];
            iniPts[0] = new PointD(midPt.X + 100, midPt.Y + 100);
            iniPts[1] = new PointD(midPt.X + 150, midPt.Y + 100);
            StreetGenerator.AddEdge(iniPts[0], iniPts[1]);

            iniPts[0] = new PointD(midPt.X + 100, midPt.Y + 100);
            iniPts[1] = new PointD(midPt.X + 50, midPt.Y + 100);
            StreetGenerator.AddEdge(iniPts[0], iniPts[1]);

            iniPts[0] = new PointD(midPt.X + 100, midPt.Y + 100);
            iniPts[1] = new PointD(midPt.X + 70, midPt.Y + 50);
            StreetGenerator.AddEdge(iniPts[0], iniPts[1]);

            iniPts[0] = new PointD(midPt.X + 150, midPt.Y + 100);
            iniPts[1] = new PointD(midPt.X + 200, midPt.Y + 150);
            StreetGenerator.AddEdge(iniPts[0], iniPts[1]);

        }

        //============================================================================================================================================================================
        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            SetGraphParameter();
            int steps = 0;// 1000;
            for (int i = 0; i < steps; i++)
            {
                StreetGenerator.GrowStreetPatternBasic();
            }
            StreetGenerator.CleanUpSubdiv();
            DrawNetwork(StreetGenerator.Graph);

            ViewControl1.ZoomAll();

        }

        //============================================================================================================================================================================
        private void toolStripButton5_Click_1(object sender, EventArgs e)
        {
            int steps = 1000;
            for (int i = 0; i < steps; i++)
            {
                StreetGenerator.GrowStreetPatternBasic();
            }
            StreetGenerator.CleanUpSubdiv();
            //DrawNetwork(StreetGenerator.Graph);
            AnalyseGraph();
            //CreateBlocks(StreetGenerator.Graph);
            ViewControl1.ZoomAll();
        }

        //============================================================================================================================================================================
        private void B_Analyse_Click(object sender, EventArgs e)
        {
            AnalyseGraph();
        }

        private void AnalyseGraph()
        {
            UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> qGraph = new UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>>(false);
            if (StreetGenerator != null)
            {
                LineD[] subLines = StreetGenerator.Graph.ToLines();
                List<Line2D> netLines = new List<Line2D>();
                foreach (LineD line in subLines)
                {
                    Vector2D startPt = line.Start.ToVector2D();
                    Vector2D endPt = line.End.ToVector2D();
                    Line2D newLine = new Line2D(startPt, endPt);
                    netLines.Add(newLine);
                    UndirectedEdge<Vector2D> newEdge = new UndirectedEdge<Vector2D>(line.Start.ToVector2D(), line.End.ToVector2D());
                    UndirectedEdge<Vector2D> reverseNewEdge = qGraph.Edges.ToList().Find(x => x.Source == line.End.ToVector2D() && x.Target == line.Start.ToVector2D());
                    // --- a new edge (& vertex) is added ---                       
                    if (qGraph.ContainsVertex(line.End.ToVector2D()) && qGraph.ContainsVertex(line.Start.ToVector2D()))
                    {
                        if (reverseNewEdge == null)
                            qGraph.AddEdge(newEdge);
                    }
                    else
                    {
                        if (reverseNewEdge == null)
                            qGraph.AddVerticesAndEdge(newEdge);
                    }
                }
                
                ViewControl1.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
                AnalyseNetwork(qGraph, ViewControl1);
                CreateBlocks(netLines);
            }
        }

        //============================================================================================================================================================================
        private void ReDrawGraph(Subdivision curGraph)
        {
            // --- draw the elements of the graph ---
            ViewControl1.AllObjectsList.ObjectsToDraw.Clear();
            ViewControl1.AllObjectsList.ObjectsToInteract.Clear();
            ViewControl1.AllObjectsList = new ObjectsAllList();
            LineD[] graphLines = curGraph.ToLines();
            foreach (LineD line in graphLines)
            {
                ViewControl1.AllObjectsList.Add(new GLine(line));
            }
            PointD[] graphPts = curGraph.Nodes.ToArray();
            foreach (PointD point in graphPts)
            {
                ViewControl1.AllObjectsList.Add(new GCircle(new Vector2D(point.X, point.Y), 1, 10));
            }
            glControl1.Invalidate();
        }

        private InstructionTreePisa _activeNetwork;
        private List<InstructionTreePisa> _allNetworks = new List<InstructionTreePisa>();

        //============================================================================================================================================================================
        private void B_TestTree_Click(object sender, EventArgs e)
        {
            //_remainDrawingObjects = new ObjectsDrawList();
            //if(_scenarioRochor.EnvironmentCloseNetwork != null)
            //foreach (Line2D line in _scenarioRochor.EnvironmentCloseNetwork)
            //{
            //    _remainDrawingObjects.Add(new GLine(line));
            //}
      
            Stopwatch timer = new Stopwatch();
            StringBuilder sb = new StringBuilder();

            if (_scenarioRochor.Border != null) InitialScenarioGraph();
            else InitialTestGraph();

            UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> networkQ = null;
            _activeNetwork = new InstructionTreePisa(ControlParameters.TreeDepth, _initailNet, _border);
            
            // --------------------------------------------
            networkQ = _activeNetwork.CreateStreetNetwork();

            bool analyseAll =  false; //true;//
            List<Line2D> edges = GeometryConverter.ToListLine2D(networkQ);
            if (analyseAll)
            {
                List<Line2D> allEdges = new List<Line2D>(edges);
                if (_scenarioRochor.EnvironmentCloseNetwork != null)
                    allEdges.AddRange(_scenarioRochor.EnvironmentCloseNetwork);
                AnalyseNetwork(allEdges, ViewControl1);
            }
            else
            {
                AnalyseNetwork(networkQ, ViewControl1);
            }
            
            CreateBlocks(edges);

            DrawLayout();
            //AnalyseNetwork(edges);
            //if (testing)
            //{
            //    sb.Append("Analysis: " + timer.ElapsedMilliseconds + "ms \n");
            //    sb.Append("\nNetwork edges: " + networkQ.Edges.Count() + "\n");
            //    sb.Append("Network verticles: " + networkQ.Vertices.Count() + "\n");
            //    timer.Stop();
            //    MessageBox.Show(sb.ToString());
            //}

           //_remainDrawingObjects = ViewControl1.AllObjectsList.ObjectsToDraw;
           ViewControl1.ZoomAll();
        }

        //============================================================================================================================================================================
        private void toolStripButton11_Click(object sender, EventArgs e)
        {
            AnalyseScenario(); 
        }

        //============================================================================================================================================================================
        private void AnalyseScenario()
        {
            _remainDrawingObjects = new List<GeoObject>();
            ViewControl1.AllObjectsList = new ObjectsAllList();
            ViewControl1.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            if (_scenarioRochor.EnvironmentCloseNetwork != null)
                AnalyseNetwork(_scenarioRochor.EnvironmentCloseNetwork, ViewControl1);
        }

        //============================================================================================================================================================================
        private void CreateBlocks(List<Line2D> network)
        {
            //Subdivision networkGraphS = new Subdivision();
            NetTopologySuite.Operation.Polygonize.Polygonizer polygonizer = new NetTopologySuite.Operation.Polygonize.Polygonizer();
            IGeometryFactory geometryFactory = new GeometryFactory();
            List<IGeometry> graphLines = new List<IGeometry>();
            WKTReader rdr = new WKTReader(geometryFactory);
            LineString lineString;

            foreach (Line2D netLine in network)
            {
                if (netLine.Start == netLine.End) 
                    continue;
                lineString = GeometryConverter.ToLineString(netLine);
                graphLines.Add(rdr.Read(lineString.ToString()));            
            }
            
            var nodedContours = new System.Collections.Generic.List<IGeometry>();
            var geomNoder = new NetTopologySuite.Noding.Snapround.GeometryNoder(new PrecisionModel(0.75d));

            foreach (var c in geomNoder.Node(graphLines)) //(Contours))
                nodedContours.Add(c);

            //var polygonizer = new NetTopologySuite.Operation.Polygonize.Polygonizer();
            polygonizer.Add(nodedContours);

            foreach (var p in polygonizer.GetPolygons())
            {
                // do something
            }

            ICollection<IGeometry> polys = polygonizer.GetPolygons();

            Console.WriteLine("Polygons formed (" + polys.Count + "):");

            List<Poly2D> buildPolygons = new List<Poly2D>();
            foreach (GeoAPI.Geometries.IPolygon netPoly in polys)
            {
                Coordinate[] coords = netPoly.Coordinates;
                Poly2D foundPoly = GeometryConverter.ToPoly2D(coords);
                buildPolygons.Add(foundPoly);
            }

            // ---- shrink the polygons -----
            List<Poly2D> toBeRemovedPolys = new List<Poly2D>();
            foreach (Poly2D poly in buildPolygons)
            {
                if (poly.Offset(-2) == false)
                    toBeRemovedPolys.Add(poly);
                if (poly.Area < 100)
                    toBeRemovedPolys.Add(poly);
            }
            foreach (Poly2D poly in toBeRemovedPolys)
            {
                buildPolygons.Remove(poly);
            }

            // --- add to list with drawable objects ---
            foreach (Poly2D poly in buildPolygons)
            {
                GPolygon polyToDraw = new GPolygon(poly);
                //polyToDraw.FillColor = ConvertColor.CreateColor4(Color.LightGray);
                //polyToDraw.Filled = true;
                polyToDraw.BorderWidth = 5;
                polyToDraw.BorderColor = ConvertColor.CreateColor4(Color.LightGray);
                ViewControl1.AllObjectsList.Add(polyToDraw, true, true);
            }

            _blocks = buildPolygons;
        }

        //============================================================================================================================================================================
        private void CreateBlocks(Subdivision networkGraphS)
        {
            PointD[][] polygonsT = networkGraphS.ToPolygons();

            List<Poly2D> polygons = GeometryConverter.ToListPoly2D(polygonsT);

            // ---- shrink the polygons -----
            foreach (Poly2D poly in polygons)
            {
                if (poly.Offset(-4) == false)
                    polygons.Remove(poly);
            }

            // --- add to list with drawable objects ---
            foreach (Poly2D poly in polygons)
            {
                GPolygon polyToDraw = new GPolygon(poly);
                polyToDraw.FillColor = ConvertColor.CreateColor4(Color.LightGray);
                polyToDraw.Filled = true;
                ViewControl1.AllObjectsList.Add(polyToDraw, true, false);
            }
        }

        //============================================================================================================================================================================
        private List<Line2D> AnalyseNetwork(List<Line2D> network, ViewControl viewControl)
        {
            GraphAnalysis analyzer = new GraphAnalysis(network);
            float[] radius = { float.MaxValue };//, 500, 150 }; //  
            analyzer.Calculate("angle", radius);

            Vector4[] ffColors = null;
            if (rB_fitChoice.Checked) ffColors = ConvertColor.CreateFFColor4(analyzer.ResultsAngular.GetNormCentralityStepsArray()[0]);
            //if (rB_fitChoice.Checked) ffColors = ConvertColor.CreateFFColor4(analyzer.ResultsAngular.GetNormChoiceArray()[0]);
            if (rB_fitCentral.Checked) ffColors = ConvertColor.CreateFFColor4(analyzer.ResultsAngular.GetNormCentralityMetricArray()[0]); // GetNormCentralityStepsArray()[0]); //
            Vector2D[] Nodes = analyzer.Network.Vertices.ToArray();
            UndirectedEdge<Vector2D>[] tempEdges = analyzer.Network.Edges.ToArray();
            List<Line2D> Edges = new List<Line2D>();
            foreach (UndirectedEdge<Vector2D> tempEdge in tempEdges)
            {
                Edges.Add(new Line2D(tempEdge.Source, tempEdge.Target));
            }

            DrawAnalysis(ffColors, Edges.ToArray(), Nodes, new Vector2D(0, 0), viewControl);
            //network = null;
            //ViewControl1.ZoomAll();
            return Edges;
        }

        //============================================================================================================================================================================
        //private List<Line2D> AnalyseNetwork(List<Line2D> network)
        //{
        //    UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> qGraph = new UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>>(false);
        //    foreach (Line2D line in network)
        //    {

        //        UndirectedEdge<Vector2D> newEdge = new UndirectedEdge<Vector2D>(line.Start, line.End);
        //        UndirectedEdge<Vector2D> reverseNewEdge = qGraph.Edges.ToList().Find(x => x.Source == line.End && x.Target == line.Start);
        //        // --- a new edge (& vertex) is added ---                       
        //        if (qGraph.ContainsVertex(line.End) && qGraph.ContainsVertex(line.Start))
        //        {
        //            if (reverseNewEdge == null)
        //                qGraph.AddEdge(newEdge);
        //        }
        //        else
        //        {
        //            if (reverseNewEdge == null)
        //                qGraph.AddVerticesAndEdge(newEdge);
        //        }
        //    }

        //    ViewControl1.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
        //    return AnalyseNetwork(qGraph);       
        //}

        //============================================================================================================================================================================
        private List<Line2D> AnalyseNetwork(UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> network, ViewControl viewControl)
        {
            FW_GPU_NET.Graph FWInverseGraph = null;
            ShortestPath ShortPahtesAngular = null;
            FW_GPU_NET.Graph fwGraph;
            //------------------------------------------------------------------------------//
            fwGraph = GraphTools.GenerateFWGpuGraph(network);
            FWInverseGraph = GraphTools.GenerateInverseDoubleGraph(fwGraph);
            ShortPahtesAngular = new ShortestPath(network);//, realAngle);
            ShortPahtesAngular.Evaluate(new System.Windows.Forms.ToolStripProgressBar(), fwGraph, FWInverseGraph);//  null);//FloydWarshall();//Dijikstra();//

            Vector2D[] Nodes = network.Vertices.ToArray();
            UndirectedEdge<Vector2D>[] tempEdges = network.Edges.ToArray();
            List<Line2D> Edges = new List<Line2D>();
            foreach (UndirectedEdge<Vector2D> tempEdge in tempEdges)
            {
                Edges.Add(new Line2D(tempEdge.Source, tempEdge.Target));
            }
            Vector4[] ffColors = null;
            if (rB_fitChoice.Checked) ffColors = ConvertColor.CreateFFColor4(ShortPahtesAngular.GetNormChoiceArray()[0]);
            if (rB_fitCentral.Checked) ffColors = ConvertColor.CreateFFColor4(ShortPahtesAngular.GetNormCentralityMetricArray()[0]);

            DrawAnalysis(ffColors, Edges.ToArray(), Nodes, new Vector2D(0, 0), viewControl);

            fwGraph.ClearGraph();
            fwGraph.Dispose();
            fwGraph = null;
            FWInverseGraph.Dispose();
            FWInverseGraph = null;
            ShortPahtesAngular = null;

            //network = null;
            //ViewControl1.ZoomAll();
            return Edges;
        }

        //============================================================================================================================================================================
        void DrawAnalysedNetwork(InstructionTreePisa curTree, ViewControl viewControl)
        {
            _allLayouts = new List<ChromosomeLayout>();
            _isovistField = null;
            _selectedBlock = null;
            _smartGrid = null;

            Vector2D[] Nodes = curTree.Network.Vertices.ToArray();
            UndirectedEdge<Vector2D>[] tempEdges = curTree.Network.Edges.ToArray();
            List<Line2D> Edges = new List<Line2D>();
            foreach (UndirectedEdge<Vector2D> tempEdge in tempEdges)
            {
                Edges.Add(new Line2D(tempEdge.Source, tempEdge.Target));
            }
            Vector4[] ffColors = null;
            if (rB_fitChoice.Checked) ffColors = ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormChoiceArray()[0]);
            if (rB_fitCentral.Checked) ffColors = ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormCentralityMetricArray()[0]);

            DrawAnalysis(ffColors, Edges.ToArray(), Nodes, new Vector2D(0, 0), viewControl);

            CreateBlocks(Edges);

        }

        //============================================================================================================================================================================
        void DrawAnalysis(Vector4[] ffColors, Line2D[] origEdges, Vector2D[] graphPts, Vector2D Origin, ViewControl viewControl)
        {
            _remainDrawingObjects = new List<GeoObject>();
            viewControl.AllObjectsList = new ObjectsAllList();

            // -- draw the border --
            if (_border == null) 
                _border = new Rect2D(glControl1.Left, glControl1.Top, glControl1.Width, -glControl1.Height).ToPoly2D();
            //GRectangle bord = new GRectangle(Origin.X, _border.BoundingBox().Height + Origin.Y, _border.BoundingBox().Width, _border.BoundingBox().Height);
            //Color4 col = Color4.White;
            //bord.BorderColor = new Vector4(col.R, col.G, col.B, col.A);
            //ViewControl1.AllObjectsList.ObjectsToDraw.Add(bord);
            // --- draw the elements of the graph ---
            Line2D curEdge;
            for (int i = 0; i < ffColors.Length; i++)
            {
                curEdge = origEdges[i];
                curEdge = new Line2D(curEdge.Start.X + Origin.X, curEdge.Start.Y + Origin.Y, curEdge.End.X + Origin.X, curEdge.End.Y + Origin.Y);
                GLine curLine = new GLine(curEdge);
                curLine.Width = ControlParameters.LineWidth;

                curLine.Color = ffColors[i];
                viewControl.AllObjectsList.Add(curLine, true, false);
            }
            foreach (Vector2D point in graphPts)
            {
                viewControl.AllObjectsList.Add(new GCircle(new Vector2D(point.X + Origin.X, point.Y + Origin.Y), .6, 10), true, true);
            }

            //_remainDrawingObjects = new List<GeoObject>();
            if (_scenarioRochor.EnvironmentCloseNetwork != null)
                foreach (Line2D line in _scenarioRochor.EnvironmentCloseNetwork)
                {
                    _remainDrawingObjects.Add(new GLine(line));
                }
            viewControl.AllObjectsList.ObjectsToDraw.AddRange(_remainDrawingObjects);
            _remainDrawingObjects = viewControl.AllObjectsList.ObjectsToDraw;        
        }

        //============================================================================================================================================================================
        void DrawAnalysis(Vector4[] ffColors, LineD[] origEdges, PointD[] graphPts, PointD Origin, ViewControl viewControl, int netID)
        {
            // -- draw the border --
            if (_border == null) 
                _border = new Rect2D(glControl1.Left, glControl1.Top, glControl1.Width, -glControl1.Height).ToPoly2D();
            // --- draw the elements of the graph ---
            LineD curEdge;
            for (int i = 0; i < ffColors.Length; i++)
            {
                curEdge = origEdges[i];
                curEdge = new LineD(curEdge.Start.X + Origin.X, curEdge.Start.Y + Origin.Y, curEdge.End.X + Origin.X, curEdge.End.Y + Origin.Y);
                GLine curLine = new GLine(curEdge);
                curLine.Width = ControlParameters.LineWidth;
                curLine.PickingThreshold = 50;
                curLine.Identity = netID;
                curLine.Color = ffColors[i];
                viewControl.AllObjectsList.Add(curLine, true, false);
            }
            foreach (PointD point in graphPts)
            {
                viewControl.AllObjectsList.Add(new GCircle(new Vector2D(point.X + Origin.X, point.Y + Origin.Y), .6, 10), true, false);
            }
        }

        //============================================================================================================================================================================
        private void DrawNetwork(Subdivision curGraph)
        {
            // --- draw the elements of the graph ---
            ViewControl1.AllObjectsList.ObjectsToDraw.Clear();
            ViewControl1.AllObjectsList.ObjectsToInteract.Clear();
            ViewControl1.AllObjectsList = new ObjectsAllList();
            LineD[] graphLines = curGraph.ToLines();
            foreach (LineD line in graphLines)
            {
                GLine curLine = new GLine(line);
                curLine.Color = new Vector4(0.09f, 0.09f, 0.09f, 1f);
                ViewControl1.AllObjectsList.Add(curLine);
            }

            PointD[] graphPts = curGraph.Nodes.ToArray();
            foreach (PointD point in graphPts)
            {
                ViewControl1.AllObjectsList.Add(new GCircle(new Vector2D(point.X, point.Y), 1, 10));
            }
            glControl1.Invalidate();
        }

        //============================================================================================================================================================================
        //============================================================================================================================================================================
        //============================================================================================================================================================================
        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            ViewControl3.AllObjectsList = new ObjectsAllList();
            _allNetworks = new List<InstructionTreePisa>();
            _SOMOrderedArchive = new List<MultiPisaChromosomeBase>();
            _population = null;
            _activeLayout = null;
            _selectedBlock = null;
            glControl3.Invalidate();
            ReSetChart(chart1, 0);
            ReSetChart(chart1, 1);
            // get population size
            try
            {
                _populationSize = Convert.ToInt16(uD_populationSize.Value);
            }
            catch
            {
                _populationSize = 20;
            }
            // iterations
            try
            {
                _iterations = Convert.ToInt16(uD_generations.Value); ;
            }
            catch
            {
                _iterations = 20;
            }
            if (_scenarioRochor.Border != null) InitialScenarioGraph();
            else InitialTestGraph();

            ViewControl3.DeactivateInteraction();

            // run worker thread           
            _needToStop1 = false;
            if (sB_multiOpti.Checked)
            {
                _workerThread = new Thread(new ThreadStart(SearchNetworkMulti));
            }
            if (sB_singleOpti.Checked)
            {
                _workerThread = new Thread(new ThreadStart(SearchNetworkSingle));
            }
            _workerThread.Start();
        }

        //============================================================================================================================================================================
        void InitialTestGraph()
        {
            UndirectedEdge<Vector2D> iniEdge;
            _fitnesLists = new List<List<double>>();
            _border = new Rect2D(0, 200, 300, 200).ToPoly2D();
            // --- initial graph for optimization ---
            _initailNet = new UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>>();
            Vector2D middle = new Vector2D(_border.BoundingBox().Width / 2, _border.BoundingBox().Height / 2);
            Vector2D[] iniPoints;
            iniPoints = new Vector2D[2];
            // segment in the middle
            iniPoints[0] = new Vector2D(15 + middle.X, 10 + middle.Y);
            iniPoints[1] = new Vector2D(30 + middle.X, 10 + middle.Y);
            iniEdge = new UndirectedEdge<Vector2D>(iniPoints[0], iniPoints[1]);
            _initailNet.AddVerticesAndEdge(iniEdge);

            // segments at the borders
            iniPoints[0] = new Vector2D(-15, middle.Y + 20);
            iniPoints[1] = new Vector2D(15, middle.Y + 20);
            iniEdge = new UndirectedEdge<Vector2D>(iniPoints[0], iniPoints[1]);
            _initailNet.AddVerticesAndEdge(iniEdge);

            iniPoints[0] = new Vector2D(_border.BoundingBox().Width - 15, middle.Y - 20);
            iniPoints[1] = new Vector2D(_border.BoundingBox().Width + 15, middle.Y - 20);
            iniEdge = new UndirectedEdge<Vector2D>(iniPoints[0], iniPoints[1]);
            _initailNet.AddVerticesAndEdge(iniEdge);

            iniPoints[0] = new Vector2D(middle.X - 40, _border.BoundingBox().Height - 15);
            iniPoints[1] = new Vector2D(middle.X - 40, _border.BoundingBox().Height + 15);
            iniEdge = new UndirectedEdge<Vector2D>(iniPoints[0], iniPoints[1]);
            _initailNet.AddVerticesAndEdge(iniEdge);
        }


        //============================================================================================================================================================================
        void InitialScenarioGraph()
        {
            UndirectedEdge<Vector2D> iniEdge;
            // _fitnesLists = new List<List<double>>();
            if (_scenarioRochor.Border != null) _border = _scenarioRochor.Border;//.BoundingBox();
            else
                _border = new Rect2D(0, 200, 600, 200).ToPoly2D();

            // --- initial graph for optimization ---
            _initailNet = new UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>>();

            foreach (Line2D line in _scenarioRochor.InitialLines)
            {
                iniEdge = new UndirectedEdge<Vector2D>(line.Start, line.End);
                _initailNet.AddVerticesAndEdge(iniEdge);
            }
        }
        //============================================================================================================================================================================
        // Worker thread
        void SearchNetworkMulti()
        {
            // measure the needed time:
            DateTime start = DateTime.Now;// DateTime.MinValue;
            DateTime end = DateTime.MinValue;

            // create fitness function
            FitnessFunctionNetwork fitnessFunction = new FitnessFunctionNetwork();
            int alpha = (int)uD_Alpha.Value;
            int lambda = (int)uD_Lambda.Value;
            int mu = (int)uD_Mu.Value;

            // create population
            if (_population == null)
            {
                _population = new MultiPopulation(_populationSize, alpha, lambda, mu,
                    new InstructionTreePisa(ControlParameters.TreeDepth, _initailNet, _border),
                    fitnessFunction,
                    (IMultiSelectionMethod)new NonDominatedSelection(), //Multi_EliteSelection()//
                    CalculationType.Method.Local
                    );
                _population.MutationRate = 0.1;
                _population.CrossoverRate = 0.7;
                _population.RandomSelectionPortion = 0.1;
                SetPopulationParameters();


            }
            DrawNetworkMulti(0);
            PlotFitnessValues(0);
            //Debug.WriteLine("bestFit", Math.Round(_population.FitnessMax, 2).ToString());
            int i = 1;
            Thread.Sleep(100);

            // loop
            while (!_needToStop1)
            {
                // run one epoch of genetic algorithm
                _population.RunEpoch();
                DrawNetworkMulti(i);
                PlotFitnessValues(i);

                // --- Achtung: Adding data to chart makes the program slower over time!!! ------
                ReSetChart(chart1, 0);
                List<IMultiChromosome> archive = _population.Sel.GetArchive();
                for (int p = 0; p < archive.Count(); p++)
                {
                    InstructionTreePisa curTree = (InstructionTreePisa)(archive[p]);
                    if (curTree.FitnessValues.Count > 1)
                    {
                        SetChart(chart1, Math.Round(curTree.FitnessValues[0], 5), curTree.FitnessValues[1], 0);
                        SetChart(chart1, Math.Round(curTree.FitnessValues[0], 5), curTree.FitnessValues[1], 1);
                    }
                }
                //Debug.WriteLine(i.ToString() + ". iterat, bestFit", Math.Round(_population.FitnessMax, 2).ToString());
                Thread.Sleep(100);

                // set current iteration's info
                SetText(lbl_Generation, _population.SizeNonDominatedSet.ToString());

                // increase current iteration
                i++;
                //
                if ((_iterations != 0) && (i > _iterations))
                    break;
            }
            // enable settings controls
            end = DateTime.Now;
            span = end - start;
            _population.Sel.Terminate();
            ViewControl3.ActivateInteraction();
        }

        //============================================================================================================================================================================
        // Worker thread
        void SearchNetworkSingle()
        {
            // measure the needed time:
            DateTime start = DateTime.Now;
            DateTime end = DateTime.MinValue;

            // create fitness function
            FitnessFunctionNetworkSingle fitnessFunction = new FitnessFunctionNetworkSingle();

            // create population
            _populationSingle = new Population(_populationSize,
                new InstructionTreeSingle(ControlParameters.TreeDepth, _initailNet, _border),
                fitnessFunction,
                (ISelectionMethod)new EliteSelection()
                );
            _populationSingle.MutationRate = 0.25;
            //population.CrossoverRate = 0.0;
            _populationSingle.RandomSelectionPortion = 0.2;
            DrawNetworkSingle(0);
            Debug.WriteLine("bestFit", Math.Round(_populationSingle.FitnessMax, 2).ToString());
            int i = 1;

            // loop
            while (!_needToStop1)
            {
                // run one epoch of genetic algorithm
                _populationSingle.RunEpoch();
                DrawNetworkSingle(i);

                // --- Achtung: Ading data to chart makes the program slow down over time!!! ------
                //ReSetChart(chart1, 0);
                //for (int p = 0; p < _population.SizeNonDominatedSet; p++)
                //{
                //    InstructionTree curTree = (InstructionTree)(_population[p]);
                //    if (curTree.FitnessValues.Count > 1)
                //    {
                //        SetChart(chart1, Math.Round(curTree.FitnessValues[0], 5), curTree.FitnessValues[1], 0);
                //        SetChart(chart1, Math.Round(curTree.FitnessValues[0], 5), curTree.FitnessValues[1], 1);
                //    }
                //}

                Debug.WriteLine(i.ToString() + ". iterat, bestFit", Math.Round(_populationSingle.FitnessMax, 2).ToString());
                Thread.Sleep(100);

                // increase current iteration
                i++;
                //
                if ((_iterations != 0) && (i > _iterations))
                    break;
            }
            end = DateTime.Now;
            span = end - start;
            ViewControl1.ActivateInteraction();
        }

        private List<List<double>> FitnessValues;
        //============================================================================================================================================================================
        private void PlotFitnessValues(int iterat)
        {
            if (iterat == 0) FitnessValues = new List<List<double>>();
            // -- archive of all solutions --
            List<IMultiChromosome> archive = _population.Sel.GetArchive();

            // -- how many fitness criteria are there --
            int nrFitVal = 0;           
            if (archive[0] != null) nrFitVal = archive[0].FitnessValues.Count();
            List<double> fitValues = new List<double>(nrFitVal);
            for (int f = 0; f < nrFitVal; f++) fitValues.Add(double.MaxValue);

            // -- find minimal values --
            for (int f = 0; f < nrFitVal; f++)
            {
                for (int p = 0; p < archive.Count(); p++)
                {
                    IMultiChromosome curChromo = archive[p];
                    if (fitValues[f] > curChromo.FitnessValues[f])
                    {
                        fitValues[f] = curChromo.FitnessValues[f];
                    }
                }
            }
                      
            FitnessValues.Add(fitValues);
            TextRW.WriteFitnessValues(FitnessValues);
        }

        //==============================================================================================
        // Delegates to enable async calls for setting controls properties
        private delegate void SetTextCallback(System.Windows.Forms.Control control, string text);

        // Thread safe updating of control's text property
        private void SetText(System.Windows.Forms.Control control, string text)
        {
            if (control.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                Invoke(d, new object[] { control, text });
            }
            else
            {
                control.Text = text;
            }
        }

        //==============================================================================================
        private delegate void SetChartCallback(System.Windows.Forms.DataVisualization.Charting.Chart chart, double valueX, double valueY, int series);

        private void SetChart(System.Windows.Forms.DataVisualization.Charting.Chart chart, double valueX, double valueY, int series)
        {
            if (chart.InvokeRequired)
            {
                SetChartCallback d = new SetChartCallback(SetChart);
                Invoke(d, new object[] { chart, valueX, valueY, series });
            }
            else
            {
                chart.Series[series].Points.AddXY(valueX, valueY);
            }
        }

        //==============================================================================================
        private delegate void ReSetChartCallback(System.Windows.Forms.DataVisualization.Charting.Chart chart, int series);

        private void ReSetChart(System.Windows.Forms.DataVisualization.Charting.Chart chart, int series)
        {
            if (chart.InvokeRequired)
            {
                ReSetChartCallback d = new ReSetChartCallback(ReSetChart);
                Invoke(d, new object[] { chart, series });
            }
            else
            {
                chart.Series[series].Points.Clear();
            }
        }

        //============================================================================================================================================================================
        private void DrawBest(int iterat)
        {
            InstructionTreePisa fittest = (InstructionTreePisa)(_population.BestChromosome);
            Vector4[] ffColors = null;
            if (rB_fitChoice.Checked) ffColors = ConvertColor.CreateFFColor4(fittest.ShortestPathes.GetNormChoiceArray()[0]);
            if (rB_fitCentral.Checked) ffColors = ConvertColor.CreateFFColor4(fittest.ShortestPathes.GetNormCentralityMetricArray()[0]);
            DrawAnalysis(ffColors, fittest.ShortestPathes.OrigEdges, fittest.ShortestPathes.OrigPoints, new PointD(0, iterat * fittest.MaxSize), ViewControl3, fittest.Indentity);
            //ViewControl1.ZoomAll();
        }

        //============================================================================================================================================================================
        private void DrawNetworkSingle(int iterat)
        {
            // -- draw the network --
            float dist = 1.1f;
            List<double> fitnesValues = new List<double>();

            if (cb_showSOMorder.Checked || cb_showBestGen.Checked)
            {
                ViewControl1.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            }

            if (cb_showAll.Checked || cb_showBestGen.Checked)
            {
                for (int i = 0; i < _populationSingle.Size; i++)
                {
                    InstructionTreeSingle curTree = (InstructionTreeSingle)_populationSingle[i];
                    Vector4[] ffColors = null;
                    if (curTree.ShortestPathes != null)
                    {
                        if (rB_fitChoice.Checked) ffColors = ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormChoiceArray()[0]);
                        if (rB_fitCentral.Checked) ffColors = ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormCentralityMetricArray()[0]);
                        DrawAnalysis(ffColors, curTree.ShortestPathes.OrigEdges, curTree.ShortestPathes.OrigPoints, new PointD(i * curTree.Border.BoundingBox().Width * dist, iterat * curTree.Border.BoundingBox().Height * dist), ViewControl3, -1);
                        if (!cb_showAll.Checked) fitnesValues.Add(curTree.Fitness);
                    }
                }
            }

            InstructionTreeSingle bestTree = (InstructionTreeSingle)_populationSingle.BestChromosome;
            if (bestTree != null)
            {
                Vector4[] ffColors = null;
                if (bestTree.ShortestPathes != null)
                {
                    if (rB_fitChoice.Checked) ffColors = ConvertColor.CreateFFColor4(bestTree.ShortestPathes.GetNormChoiceArray()[0]);
                    if (rB_fitCentral.Checked) ffColors = ConvertColor.CreateFFColor4(bestTree.ShortestPathes.GetNormCentralityMetricArray()[0]);
                    DrawAnalysis(ffColors, bestTree.ShortestPathes.OrigEdges, bestTree.ShortestPathes.OrigPoints, new PointD(-1.3 * bestTree.Border.BoundingBox().Width * dist, iterat * bestTree.Border.BoundingBox().Height * dist), ViewControl3, -1);
                    if (!cb_showAll.Checked) fitnesValues.Add(bestTree.Fitness);
                }
            }

            for (int i = 0; i < _populationSingle.Size; i++)
            {
                InstructionTreeSingle curTree = (InstructionTreeSingle)(_populationSingle[i]);
                fitnesValues.Add(curTree.Fitness);
            }
            _fitnesLists.Add(fitnesValues);

            if (cB_autoShot.Checked) SetGraphicContext(glControl1); //Screenshot();

            ViewControl3.ZoomAll();
        }

        //============================================================================================================================================================================
        private void DrawNetworkMulti(int iterat)
        {
            // -- draw the network --
            float dist = 1.1f;
            List<double> fitnesValues = new List<double>();

            if (cb_showSOMorder.Checked || cb_showBestGen.Checked)
            {
                ViewControl3.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            }

            if (cb_showSOMorder.Checked)
            {
                SOMorderProcess();
                if (cB_showSomMap.Checked) ShowEsomMap(_globalLrn);
                DrawNetworkSOM();
                return;
            }

            List<IMultiChromosome> archive = _population.Sel.GetArchive();
            for (int p = 0; p < archive.Count(); p++)
            {
                InstructionTreePisa curTree = (InstructionTreePisa)(archive[p]);
                if (curTree.ShortestPathes != null)
                {
                    Vector4[] ffColors = null;
                    if (rB_fitChoice.Checked) ffColors = ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormChoiceArray()[0]);
                    if (rB_fitCentral.Checked) ffColors = ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormCentralityMetricArray()[0]);
                    DrawAnalysis(ffColors, curTree.ShortestPathes.OrigEdges, curTree.ShortestPathes.OrigPoints, new PointD(p * curTree.Border.BoundingBox().Width * dist, iterat * curTree.Border.BoundingBox().Height * dist), ViewControl3, curTree.Indentity);
                }
            }
            if (cB_autoShot.Checked) SetGraphicContext(glControl1); //Screenshot();
            ViewControl3.Invalidate();
            ViewControl3.ZoomAll();
        }

        //============================================================================================================================================================================
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // check if worker thread is running
            if ((_workerThread != null) && (_workerThread.IsAlive))
            {
                _needToStop1 = true;
                _needToStop2 = true;
                while (!_workerThread.Join(100))
                    Application.DoEvents();
            }
            if (_population != null)
                if (_population.Sel != null)
                    _population.Sel.Terminate();
            PisaWrapper.TerminateAll();
        }

        //============================================================================================================================================================================
        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            // stop worker thread
            if (_workerThread != null)
            {
                _needToStop1 = true;
                _needToStop2 = true;
                while (!_workerThread.Join(100))
                    Application.DoEvents();
                _workerThread = null;
            }
        }

        //============================================================================================================================================================================
        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            ViewControl1.AllObjectsList = new ObjectsAllList();
            ViewControl2.AllObjectsList = new ObjectsAllList();
            ViewControl3.AllObjectsList = new ObjectsAllList();
            constantDrawObjects = new ObjectsDrawList();
            _globalArchive = new List<MultiPisaChromosomeBase>();
            _SOMOrderedArchive = new List<MultiPisaChromosomeBase>();
            _remainDrawingObjects = new List<GeoObject>();
            _allLayouts = new List<ChromosomeLayout>();          
            _allNetworks = new List<InstructionTreePisa>();
            _blocks = new List<Poly2D>();
            _isovistField = null;
            _population = null;
            _activeLayout = null;
            _selectedBlock = null;
            _smartGrid = null;
            _activeMultiLayout = null;         
            glControl1.Invalidate();
            glControl2.Invalidate();
            glControl3.Invalidate();
            ReSetChart(chart1, 0);
            ReSetChart(chart1, 1);
        }

        //============================================================================================================================================================================
        private void button1_Click(object sender, EventArgs e)
        {
            Screenshot();
        }

        private void Screenshot()
        {
            //ViewControl1.ZoomAll();
            glControl1.Invalidate();
            Bitmap img = ViewControl1.GrabScreenshot();

            string screenshotFilename = "screenShot_" +
                                                DateTime.Now.Day.ToString("00") + "-" +
                                                DateTime.Now.Month.ToString("00") + "_" +
                                                DateTime.Now.Hour.ToString("00") +
                                                DateTime.Now.Minute.ToString("00") +
                                                DateTime.Now.Second.ToString("00") + ".png";

            string path = System.IO.Path.Combine(Environment.CurrentDirectory, screenshotFilename);
            img.Save(path, System.Drawing.Imaging.ImageFormat.Png);
            MessageBox.Show(screenshotFilename);
        }

        // Delegates to enable async calls for setting controls properties
        private delegate void SetGLCallback(System.Windows.Forms.Control control);

        // Thread safe updating of control's text property
        private void SetGraphicContext(System.Windows.Forms.Control control)
        {
            if (control.InvokeRequired)
            {
                SetGLCallback d = new SetGLCallback(SetGraphicContext);
                Invoke(d, new object[] { control });
            }
            else
            {
                Screenshot();
            }
        }

        //============================================================================================================================================================================
        private void toolStripButton9_Click(object sender, EventArgs e)
        {
            DrawLayoutSOM();
        }


        //============================================================================================================================================================================
        private void readBmToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string iniPath = Application.StartupPath;// "C:/Users/koenigr/Pictures/130424_StreetNetwork/dxf";//System.Reflection.Assembly.GetExecutingAssembly().Location;
            openBmFile.InitialDirectory = Path.GetFullPath(iniPath);
            openBmFile.Filter = "databionic-esom bm file|*.bm";
            openBmFile.Title = "Load a bm file";
            openBmFile.RestoreDirectory = true;
            openBmFile.ShowDialog();
        }

        //============================================================================================================================================================================
        private void openBmFile_FileOk(object sender, CancelEventArgs e)
        {
            string name = openBmFile.FileName;
            _somOrder = TextRW.ReadBm(name);
        }

        string _globalLrn;

        //============================================================================================================================================================================
        private void exportLrnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // If user clicks button, show the dialog.
            string iniPath = Application.StartupPath;// "C:/Users/koenigr/Pictures/130424_StreetNetwork/dxf";//System.Reflection.Assembly.GetExecutingAssembly().Location;
            saveExportLrn.InitialDirectory = Path.GetFullPath(iniPath);
            saveExportLrn.Filter = "databionic-esom lrn file|*.lrn";
            saveExportLrn.Title = "Save a lrn file";
            saveExportLrn.RestoreDirectory = true;
            saveExportLrn.ShowDialog();
        }

        //============================================================================================================================================================================
        private void saveExportLrn_FileOk(object sender, CancelEventArgs e)
        {
            string name = saveExportLrn.FileName;
            TextRW.WriteLrn(_population, name);
            if (_population[0].GetType() == typeof(ChromosomeLayout))
            {
                string name2 = name.Replace(".lrn", "_ID.lrn");
                List<string> fingerprint = LayoutFingerprintAnalysisPoints();
                TextRW.WriteLrnID(fingerprint, name2);
            }

            List<MultiPisaChromosomeBase> archive;
            List<object> allObjects = new List<object>();
            if (_population != null)
            {
                allObjects.Add(_populationParameters);
                archive = _population.Sel.GetArchiveOrig();
                allObjects.Add(archive);
                string name2 = name.Replace(".lrn", ".cplan");
                BinarySerialization.SerializeObjects(allObjects, name2);
            }
        }

        //============================================================================================================================================================================
        public List<string> LayoutFingerprintAnalysisPoints()
        {
            List<string> lines = new List<string>();
            List<MultiPisaChromosomeBase> archive = _population.Sel.GetArchiveOrig();
            if (archive == null) return null;

            // -- write file ---------------------------------------------------------
            // -----------------------------------------------------------------------
            // -- file header --------------------------------------------------------
            lines.Add("# Layout-Fingerprints (IDs) from CPlan | Generation:");
            lines.Add("# " + _population.EpochCounter.ToString());
//          lines.Add("# " + ((ChromosomeLayout)archive[0]).IsovistField.CountX.ToString() + " = length of one line");
            //lines.Add("# " + "Avg: ");
            //lines.Add("# " + "StdDev: ");
            //lines.Add("# " + "Min: ");
            //lines.Add("# " + "Max: ");
            lines.Add("% " + archive.Count().ToString());
            lines.Add("% " + "2");
            // -- add the column-feature names and type of column --
            string type = "% 9";
            string name = "% ID";
            int entires = ((ChromosomeLayout)archive[0]).IsovistField.FingerprintAnalysisPoints.Count;
            for (int k = 0; k < entires; k++)
            {
                type += "\t" + "1";
                name += "\t" + "feature" + (k + 1).ToString();
            }
            lines.Add(type);
            lines.Add(name);
            // -- add the finterprint features ---------------------
            for (int m = 0; m < archive.Count(); m++)
            {
                ChromosomeLayout curChromo = (ChromosomeLayout)archive[m];
                string lineID = curChromo.Indentity.ToString();
                for (int j = 0; j < curChromo.IsovistField.FingerprintAnalysisPoints.Count; j++)
                {                  
                    if (curChromo.IsovistField.FingerprintAnalysisPoints[j] == 0)
                    {
                        lineID += "\t0";
                    }
                    else
                    {
                        lineID += "\t1";
                    }
                }
                lines.Add(lineID);
            }
            return lines;
        }

        List<PoissonValues> allVarianceValues = new List<PoissonValues>(); 
        //============================================================================================================================================================================
        public void VarianceAnalysis(int iterat)
        {
            if (iterat == 0)  allVarianceValues = new List<PoissonValues>();               

            List<float[]> fingerPrintsArchive = new List<float[]>();
            List<MultiPisaChromosomeBase> archive = _population.Sel.GetArchiveOrig();
            ChromosomeLayout curChromo;
            for (int m = 0; m < archive.Count(); m++)
            {
                curChromo = (ChromosomeLayout) archive[m];
                fingerPrintsArchive.Add(curChromo.IsovistField.FingerprintGrid.ToArray());
            }

            curChromo = (ChromosomeLayout) archive[0];
            int nrCellsinLine = curChromo.IsovistField.FingerprintGridLineLgth;

            //TextRW.PoissonValues curVarianceList = PoissonAnalysis(FingerPrintsArchive, nrCellsinLine);
            Statistics STAT = new Statistics(fingerPrintsArchive, nrCellsinLine); // construct statistics
            STAT.Calc();

            PoissonValues curVarianceList = new PoissonValues();
            curVarianceList.Range = STAT.DELTA_RANGE;
            curVarianceList.Sum = STAT.DELTA_SUM;
            curVarianceList.AvgDeviation = STAT.DELTA_AVG;
            curVarianceList.Lambda = STAT.POISSON_LAMBDA;
            curVarianceList.Error = STAT.POISSON_ERROR;

            allVarianceValues.Add(curVarianceList);
            TextRW.WritePoissonValues(allVarianceValues);
        }

        //============================================================================================================================================================================
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // If user clicks button, show the dialog.
            string iniPath = Application.StartupPath;// "C:/Users/koenigr/Pictures/130424_StreetNetwork/dxf";//System.Reflection.Assembly.GetExecutingAssembly().Location;
            saveAllDialog.InitialDirectory = Path.GetFullPath(iniPath);
            saveAllDialog.Filter = "cplan file|*.cplan";
            saveAllDialog.Title = "Save a CPlan file";
            saveAllDialog.RestoreDirectory = true;
            saveAllDialog.ShowDialog();
        }

        //============================================================================================================================================================================
        private void saveAllDialog_FileOk(object sender, CancelEventArgs e)
        {
            // -- Get file name --  
            string name = saveAllDialog.FileName; //Application.StartupPath + "\\" +
            // -- save all --
            List<IMultiChromosome> archive;
            List<object> allObjects = new List<object>();
            allObjects.Add(_population);
            if (_population != null)
            {
                archive = _population.Sel.GetArchive();
                allObjects.Add(archive);
            }
            allObjects.Add(_activeLayout);
            allObjects.Add(_parameters);
            BinarySerialization.SerializeObjects(allObjects, name);
        }

        //============================================================================================================================================================================
        # region LoadAllByBackgroundWorker
        //============================================================================================================================================================================
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // If user clicks button, show the dialog.
            string iniPath = Application.StartupPath;// "C:/Users/koenigr/Pictures/130424_StreetNetwork/dxf";//System.Reflection.Assembly.GetExecutingAssembly().Location;
            openAllDialog.InitialDirectory = Path.GetFullPath(iniPath);
            openAllDialog.Filter = "cplan file|*.cplan";
            openAllDialog.Title = "Save a CPlan file";
            openAllDialog.RestoreDirectory = true;
            openAllDialog.ShowDialog();
        }

        //============================================================================================================================================================================
        private void openAllDialog_FileOk(object sender, CancelEventArgs e)
        {
            if (toolStripProgressBar2.Value == toolStripProgressBar2.Maximum)
            {
                toolStripProgressBar2.Value = toolStripProgressBar2.Minimum;
            }
            backWorker.RunWorkerAsync(toolStripProgressBar2.Value);
        }

        //============================================================================================================================================================================
        void backWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("File loaded.");// + e.Result.ToString());
            if (_populationParameters != null && _SOMOrderedArchive != null)
            {
                List<IMultiChromosome> convSomList = new List<IMultiChromosome>(_SOMOrderedArchive.Cast<IMultiChromosome>());
                _population = new MultiPopulation(_populationParameters, convSomList);
            }
        }

        //============================================================================================================================================================================
        void backWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            int percentFinished = (int)e.Argument;
            openAll();
        }

        //============================================================================================================================================================================
        private void openAll()
        {
            string name = openAllDialog.FileName;
            List<object> allObjects = BinarySerialization.DeserializeObjects(name);
            // -- cast types and assign to instances --
            foreach (object obj in allObjects)
            {
                if (obj != null)
                {
                    //if (obj.GetType() == typeof(MultiPopulation)) _population = (MultiPopulation)obj;
                    //if (obj.GetType() == typeof(MultiPopulation)) _initialPopulation = (MultiPopulation)obj;
                    //if (obj.GetType() == typeof(List<IMultiChromosome>)) _SOMOrderedArchive = (List<IMultiChromosome>)obj;
                    if (obj is List<MultiPisaChromosomeBase>)
                    {
                        _SOMOrderedArchive = (List<MultiPisaChromosomeBase>)obj;
                        //_population.Sel.SetArchive(_SOMOrderedArchive); // copy the archive chromosomes into the PISA archive
                    }
                    if (obj.GetType() == typeof(ChromosomeLayout)) _activeLayout = (ChromosomeLayout)obj;
                    //if (obj.GetType() == typeof(List<MultiPisaChromosomeBase>)) _globalArchive = (List<MultiPisaChromosomeBase>)obj;
                    if (obj.GetType() == typeof(ViewControlParameters)) _parameters = (ViewControlParameters)obj;
                    if (obj.GetType() == typeof(PopulationParameters)) _populationParameters = (PopulationParameters)obj;
                }
            }
            // use the highest ID from the archive as starting point for new PISA-IDs.
            if (_SOMOrderedArchive != null)
            {
                int maxID = 0;
                foreach (MultiPisaChromosomeBase curChromo in _SOMOrderedArchive)
                {
                    if (curChromo.Indentity > maxID)
                    {
                        maxID = curChromo.Indentity;
                        curChromo.LastID = maxID + 1;
                    }
                }
            }
        }
        # endregion

        //============================================================================================================================================================================
        private void exportDxfToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // When user clicks button, show the dialog.
            string iniPath = Application.StartupPath;// "C:/Users/koenigr/Pictures/130424_StreetNetwork/dxf";//System.Reflection.Assembly.GetExecutingAssembly().Location;
            saveDxfDialog.InitialDirectory = Path.GetFullPath(iniPath);
            saveDxfDialog.Filter = "Dxf file|*.dxf";
            saveDxfDialog.Title = "Save a dxf file";
            saveDxfDialog.RestoreDirectory = true;
            saveDxfDialog.ShowDialog();
        }

        //============================================================================================================================================================================
        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            // Get file name.  
            string name = saveDxfDialog.FileName;
            // create the dxf file that shall be saved
            DxfDocument dxf = DxfConversions.ToDxfFile(ViewControl1.AllObjectsList.ObjectsToDraw, DxfVersion.AutoCad2000);
            // Write to the file name selected.
            dxf.Save(name);
            if (_population != null) SaveFitnessValues(name);
        }

        //============================================================================================================================================================================
        private void SaveFitnessValues(string name)
        {
            string curName = name.Remove(name.Length - 4);
            curName += ".txt";
            StreamWriter sw = new StreamWriter(curName);
            try
            {
                sw.WriteLine("Values for file:" + name);
                sw.WriteLine(_iterations.ToString() + " : Generations");
                sw.WriteLine(_populationSize.ToString() + " : Population Size");
                sw.WriteLine(ControlParameters.TreeDepth.ToString() + " : Tree Depth");
                InstructionTreePisa bestTree = (InstructionTreePisa)_population.BestChromosome;
                string mode = (ControlParameters.Choice) ? "Choice" : "Centrality";
                sw.WriteLine(span.ToString(@"hh\:mm\:ss") + " : Calculation Time");

                for (int i = 0; i < _fitnesLists.Count; i++)
                {
                    List<double> fitValues = new List<double>();
                    fitValues = _fitnesLists[i];
                    string valueLine = i.ToString() + "; ";
                    for (int k = 0; k < fitValues.Count; k++)
                    {
                        double curValue = fitValues[k];
                        valueLine += curValue.ToString() + "; ";
                    }
                    sw.WriteLine(valueLine); // + "; Generation " + i.ToString() );
                }
                sw.Close();
            }
            catch (InvalidCastException e)
            {
                if (sw != null) sw.Close();
            }
        }

        //============================================================================================================================================================================
        private void importDxfToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // When user clicks button, show the dialog.
            string iniPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            openDxfDialog.InitialDirectory = Path.GetFullPath(iniPath);
            openDxfDialog.Filter = "Dxf file|*.dxf";
            openDxfDialog.Title = "Load an dxf file";
            openDxfDialog.RestoreDirectory = true;
            openDxfDialog.ShowDialog();
        }


        //============================================================================================================================================================================
        private void openDxfDialog_FileOk(object sender, CancelEventArgs e)
        {
            // Get file name.  
            string name = openDxfDialog.FileName;
            // create the new objects from the selected file
            ObjectsDrawList newGObjects = DxfConversions.GObjectsFormDxf(name);
            // add the new objects to the current ObjectsToDraw-list
            ViewControl1.AllObjectsList.ObjectsToDraw.AddRange(newGObjects);
            ViewControl1.ZoomAll();
        }

        Scenario _scenarioRochor = new Scenario();
        //============================================================================================================================================================================
        private void loadScenarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string pathToFolder;
            if (folderBrowserDialog_Scenarios.ShowDialog() == DialogResult.OK)
            {
                pathToFolder = folderBrowserDialog_Scenarios.SelectedPath;
            }
            else return;
            string pathToBorder = Path.Combine(pathToFolder, "Border.dxf");
            
            _scenarioRochor.Border = new Poly2D(DxfConversions.LinesFromDxf(pathToBorder));

            string pathToIniLines = Path.Combine(pathToFolder, "InitialLines.dxf");
            _scenarioRochor.InitialLines = DxfConversions.LinesFromDxf(pathToIniLines);

            string pathToEnvironmentClose = Path.Combine(pathToFolder, "EnvironmentClose.dxf");
            _scenarioRochor.EnvironmentCloseNetwork = DxfConversions.LinesFromDxf(pathToEnvironmentClose);
            string pathToEnvironmentBuildings = Path.Combine(pathToFolder, "EnvironmentClose.dxf");

            string pathTo3DModel = Path.Combine(pathToFolder, "Environment3DBuildings.obj");
            openObjFile(pathTo3DModel);   
            ////////////////
            //ObjectsDrawList newGObjects = DxfConversions.GObjectsFormDxf(iniPath);
            //foreach (object gObj in newGObjects)
            //{
            //    if (gObj.GetType() == typeof(GPolygon))
            //    {
            //        GPolygon gPoly = (GPolygon)gObj;
            //        gPoly.BorderWidth = 1;
            //        gPoly.Filled = true;
            //        gPoly.FillColor = ConvertColor.CreateColor4(Color.Black);
            //    }
            //}
            //constantDrawObjects.AddRange(newGObjects);

            //iniPath = Path.Combine(Directory.GetCurrentDirectory(), "130823_areaStreets7.dxf");
            //ObjectsDrawList newGObjects2 = DxfConversions.GObjectsFormDxf(iniPath);
            //foreach (object gObj in newGObjects2)
            //{
            //    if (gObj.GetType() == typeof(GLine))
            //    {
            //        GLine gLine = (GLine)gObj;
            //        gLine.Width = 1;
            //        gLine.Color = ConvertColor.CreateColor4(Color.Black);
            //    }
            //}
            //constantDrawObjects.AddRange(newGObjects2);

            //ViewControl1.AllObjectsList.ObjectsToDraw.AddRange(constantDrawObjects);
            ////////////////

            //AnalyseNetwork(_scenarioRochor.EnvironmentCloseNetwork);
            _remainDrawingObjects = new ObjectsDrawList();
            _remainDrawingObjects.Add(new GPolygon(_scenarioRochor.Border));
            foreach (Line2D line in _scenarioRochor.EnvironmentCloseNetwork)
            {
                _remainDrawingObjects.Add(new GLine(line));
            }
            DrawLayout();
            if (_scenarioRochor.Border != null) InitialScenarioGraph();
            else InitialTestGraph();

            ViewControl1.ZoomAll();
        }


        //============================================================================================================================================================================
        private void toolStripButton_LoadScene_Click(object sender, EventArgs e)
        {
            string iniPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            openDxfDialog.InitialDirectory = Path.GetFullPath(iniPath);
            constantDrawObjects = new ObjectsDrawList();

            iniPath = Path.Combine(Directory.GetCurrentDirectory(), "130823_areaBuildings12.dxf");
            ObjectsDrawList newGObjects = DxfConversions.GObjectsFormDxf(iniPath);
            foreach (object gObj in newGObjects)
            {
                if (gObj.GetType() == typeof(GPolygon))
                {
                    GPolygon gPoly = (GPolygon)gObj;
                    gPoly.BorderWidth = 1;
                    gPoly.Filled = true;
                    gPoly.FillColor = ConvertColor.CreateColor4(Color.Black);
                }
            }
            constantDrawObjects.AddRange(newGObjects);

            iniPath = Path.Combine(Directory.GetCurrentDirectory(), "130823_areaStreets7.dxf");
            ObjectsDrawList newGObjects2 = DxfConversions.GObjectsFormDxf(iniPath);
            foreach (object gObj in newGObjects2)
            {
                if (gObj.GetType() == typeof(GLine))
                {
                    GLine gLine = (GLine)gObj;
                    gLine.Width = 1;
                    gLine.Color = ConvertColor.CreateColor4(Color.Black);
                }
            }
            constantDrawObjects.AddRange(newGObjects2);

            ViewControl1.AllObjectsList.ObjectsToDraw.AddRange(constantDrawObjects);
            ViewControl1.ZoomAll();

        }

        //============================================================================================================================================================================
        private void importGraphFromDxfToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openDxfGraphDialog.ShowDialog();
        }


        //============================================================================================================================================================================
        private void openDxfGraphDialog_FileOk(object sender, CancelEventArgs e)
        {
            string name = openDxfGraphDialog.FileName;
            openDxfGraphDialog.InitialDirectory = System.Reflection.Assembly.GetExecutingAssembly().Location;

            DxfDocument dxf = DxfConversions.FromDxfFile(name);
            List<netDxf.Entities.Line> lines = dxf.Lines.ToList();
            List<LineD> importedLines = new List<LineD>();
            foreach (netDxf.Entities.Line dLine in lines)
            {
                importedLines.Add(new LineD(dLine.StartPoint.X, dLine.StartPoint.Y, dLine.EndPoint.X, dLine.EndPoint.Y));
            }
            StreetGenerator = new StreetPattern(null);
            StreetGenerator.Graph = new Subdivision();
            LineD[] arrLines = importedLines.ToArray();
            StreetGenerator.Graph = Subdivision.FromLines(arrLines, .0);

            DrawNetwork(StreetGenerator.Graph);
            ViewControl1.ZoomAll();
        }

        //============================================================================================================================================================================
        private void SetControlParameters()
        {
            ControlParameters.DisplayMeasure = comboBox_Isovist.Text;
            ControlParameters.Centrality = rB_fitCentral.Checked;
            ControlParameters.Choice = rB_fitChoice.Checked;
            ControlParameters.AngleRange = Convert.ToInt16(uD_angle.Value);
            ControlParameters.LengthRange = Convert.ToInt16(uD_maxLength.Value - uD_minLength.Value);
            ControlParameters.MinLength = Convert.ToInt16(uD_minLength.Value);
            ControlParameters.MaxChilds = Convert.ToInt16(uD_connectivity.Value);
            ControlParameters.LineWidth = Convert.ToInt16(uD_lineWidth.Value);
            ControlParameters.TreeDepth = Convert.ToInt16(uD_treeDepth.Value);
            ControlParameters.MinNrBuildings = Convert.ToInt16(uD_minNrBuildings.Value);
            ControlParameters.MaxNrBuildings = Convert.ToInt16(uD_maxNrBuildings.Value);
            ControlParameters.Density = Convert.ToDouble(uD_density.Value) / 100.0;
            ControlParameters.MinSideRatio = Convert.ToDouble(uD_minSideRatio.Value) / 100.0;
            ControlParameters.MinArea = Convert.ToDouble(uD_minAbsBldArea.Value);
            ControlParameters.MaxArea = Convert.ToDouble(uD_maxAbsBldArea.Value);
            ControlParameters.Scenario = comboBox_scenario.Text;
            ControlParameters.RepeatAdapt = Convert.ToInt16(uD_Adapt.Value);
            ControlParameters.IsovistCellSize = Convert.ToInt16(uD_IsovistCellSize.Value);
            ControlParameters.MinSide = Convert.ToInt16(uD_minSideAbs.Value);
            ControlParameters.MinBldArea = Convert.ToInt16(uD_minAbsBldArea.Value);
            ControlParameters.MaxBldArea = Convert.ToInt16(uD_maxAbsBldArea.Value);
            ControlParameters.MinBldSide = Convert.ToInt16(uD_minBldSide.Value);
            ControlParameters.MaxBldSide = Convert.ToInt16(uD_maxBldSide.Value);
        }

        //============================================================================================================================================================================
        private void SetScenarioEnvironment()
        {
            string scenario = ControlParameters.Scenario;

            if (scenario == "Cape Town Scenario")
            {
                Vector2D[] corners = new Vector2D[4];
                corners[0] = new Vector2D(370, 195);
                corners[1] = new Vector2D(780, 250);
                corners[2] = new Vector2D(860, 420);
                corners[3] = new Vector2D(560, 560);
                _borderPoly = new GPolygon(corners);
            }
            //else if (scenario == "Basic Senario")
            //{
            //    Vector2D[] corners = new Vector2D[4];
            //    corners[0] = new Vector2D(370, 195);
            //    corners[1] = new Vector2D(780, 250);
            //    corners[2] = new Vector2D(860, 420);
            //    corners[3] = new Vector2D(560, 560);
            //    _borderPoly = new GPolygon(corners);
            //}
            //else if (scenario == "Rochor Scenario")
            //{

            //}
            else
            {
                Vector2D[] corners = new Vector2D[4];
                corners[0] = new Vector2D(1, 1);
                corners[1] = new Vector2D(500, 1);
                corners[2] = new Vector2D(500, 400);
                corners[3] = new Vector2D(1, 400);
                _borderPoly = new GPolygon(corners);
            }
        }

        //============================================================================================================================================================================
        private void SetPopulationParameters()
        {
            _populationParameters = new PopulationParameters();
            _populationParameters.FitnessFunction = _population.FitnessFunction;
            _populationParameters.SelectionMethod = _population.SelectionMethod;
            // -- check typeof...
            if (_population.Ancestor.GetType() == typeof(ChromosomeLayout))
                _populationParameters.Ancestor = new ChromosomeLayout((ChromosomeLayout)_population.Ancestor);
            if (_population.Ancestor.GetType() == typeof(InstructionTreePisa))
                _populationParameters.Ancestor = new InstructionTreePisa((InstructionTreePisa)_population.Ancestor);
            _populationParameters.Size = _population.Size;
            _populationParameters.EpochCounter = _population.EpochCounter;
            _populationParameters.MU = _population.MU;
            _populationParameters.ALPHA = _population.ALPHA;
            _populationParameters.LAMBDA = _population.LAMBDA;
            _populationParameters.RandomSelectionPortion = _population.RandomSelectionPortion;
            _populationParameters.AutoShuffling = _population.AutoShuffling;
            _populationParameters.CrossoverRate = _population.CrossoverRate;
            _populationParameters.MutationRate = _population.MutationRate;
        }

        //==============================================================================================
        //==============================================================================================
        //==============================================================================================

        # region BuildingLayout

        private GPolygon _borderPoly;
        private List<ChromosomeLayout> _allLayouts = new List<ChromosomeLayout>();
        private ChromosomeLayout _activeLayout;
        private ChromosomeMultiLayout _activeMultiLayout;
        private List<Poly2D> _blocks;

        //============================================================================================================================================================================
        private void ButtonBuildingLayout_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            Poly2D borderPoly;
            ViewControl1.AllObjectsList.ObjectsToDraw.Add(_borderPoly);
            //ViewControl1.ZoomAll();
            if (_selectedBlock != null)
                borderPoly = _selectedBlock;
            else
            {                     
                borderPoly = new Poly2D(_borderPoly.PolyPoints); //_borderLayout.Rect.Points);  _border = new Rect2D(0, 0, 2000, -2000).ToPoly2D();
                //borderPoly = new Poly2D(new Rect2D(0, 0, 200, -200).ToPoly2D());
            }

            bool makeNewLayout = true;

            // --- layouts for all blocks ---
            if (cB_layoutAll.Checked)
            {
                _activeMultiLayout = new ChromosomeMultiLayout(_borderPoly.Poly,
                            ControlParameters.MinNrBuildings, ControlParameters.MaxNrBuildings,
                            ControlParameters.Density, ControlParameters.MinSideRatio, _blocks);
            }
            else 
            {
                // --- layouts for individual blocks ---
                for(int i=0; i<_allLayouts.Count; i++)         
                {
                    ChromosomeLayout layout = _allLayouts[i];
                    double overlap = layout.Border.Overlap(borderPoly);
                    double ratio = overlap / borderPoly.Area;
                    if (ratio > 0.9)
                    {
                        _allLayouts[i] = new ChromosomeLayout(borderPoly,
                            ControlParameters.MinNrBuildings, ControlParameters.MaxNrBuildings,
                            ControlParameters.Density, ControlParameters.MinSideRatio);
                        makeNewLayout = false;
                        _activeLayout = _allLayouts[i];
                        break;
                    }
                }
            }

            if (makeNewLayout)
            {
                ChromosomeLayout newLayout = new ChromosomeLayout(borderPoly,
                    ControlParameters.MinNrBuildings, ControlParameters.MaxNrBuildings,
                    ControlParameters.Density, ControlParameters.MinSideRatio);
                _activeLayout = newLayout;
                _allLayouts.Add(newLayout);
            }

            timer1.Enabled = true;
            IsovistAnalye();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            MainLoop();
        }
        //==============================================================================================
        private void MainLoop()
        {
            //FPS set up
            double FPS = 30.0;
            long ticks1 = 0;
            long ticks2 = 0;
            double interval = (double)Stopwatch.Frequency / FPS;
            DateTime start = DateTime.Now;// DateTime.MinValue;
            DateTime end = DateTime.MinValue;
            TimeSpan diff;
            //
            int i = 1;

            Application.DoEvents();
            end = DateTime.Now;
            diff = end - start;

            ticks2 = Stopwatch.GetTimestamp();
            if (ticks2 >= ticks1 + interval)
            {
                ticks1 = Stopwatch.GetTimestamp();

                if (cB_layoutAll.Checked)
                {
                    if (_activeMultiLayout != null)
                    {
                        _activeMultiLayout.Adapt();
                        _allLayouts = _activeMultiLayout.SubChromosomes;
                        DrawMultiLayout();
                    }
                }
                else
                {
                    if (_allLayouts.Count > 0)
                    {
                        foreach (ChromosomeLayout layout in _allLayouts)
                            layout.Adapt();

                        DrawLayout();
                    }
                }
                // --- interface information ---
                int test = Tools.CalculateFrameRate();
                string toHead = "Layout with " + test.ToString() + " fps";
                SetText(this, toHead);

            }
            i++;
            // Todo: Sleep funktion avoids "licker" problem while drag & drop...
            Thread.Sleep(1); //frees up the cpu         
        }

        //============================================================================================================================================================================
        private void DrawLayout()
        {
            ViewControl1.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            ViewControl1.AllObjectsList.ObjectsToDraw.AddRange(_remainDrawingObjects);
            if (constantDrawObjects != null) ViewControl1.AllObjectsList.ObjectsToDraw.AddRange(constantDrawObjects);
            if (_smartGrid != null) ViewControl1.AllObjectsList.ObjectsToDraw.Add(_smartGrid);//new GGridOld(_isovistField));
            //if (_isovistField != null) ViewControl1.AllObjectsList.ObjectsToDraw.Add(new GGrid(_isovistField));
            foreach (ChromosomeLayout layout in _allLayouts)
            {
                if (layout.Gens != null)
                    foreach (GenBuildingLayout building in layout.Gens)
                {
                    GPrism bPoly = new GPrism(building.Poly);
                    bPoly.Height = building.BuildingHeight;
                    bPoly.Identity = layout.Indentity;
                    bPoly.DeltaZ = 0.1f;
                    bPoly.BorderWidth = 5;
                    //bPoly.FillColor = ConvertColor.CreateColor4(Color.Gray);
                    if (!building.Open) bPoly.Filled = true;
                    else bPoly.Filled = false;

                    bPoly.FillColor = ConvertColor.CreateColor4(Color.DimGray);
                    ViewControl1.AllObjectsList.ObjectsToDraw.Add(bPoly);
                }
                GPolygon borderPoly = new GPolygon(layout.Border);
                ViewControl1.AllObjectsList.ObjectsToDraw.Add(borderPoly);
            }
            ViewControl1.Invalidate();
            // ViewControl1.ZoomAll();
        }

        //============================================================================================================================================================================
        private void DrawMultiLayout()
        {
            ViewControl1.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            ViewControl1.AllObjectsList.ObjectsToDraw.AddRange(_remainDrawingObjects);
            if (constantDrawObjects != null) ViewControl1.AllObjectsList.ObjectsToDraw.AddRange(constantDrawObjects);
            if (_smartGrid != null) ViewControl1.AllObjectsList.ObjectsToDraw.Add(_smartGrid);//new GGridOld(_isovistField));
            //if (_isovistField != null) ViewControl1.AllObjectsList.ObjectsToDraw.Add(new GGrid(_isovistField));
            foreach (ChromosomeLayout layout in _activeMultiLayout.SubChromosomes)
            {
                if (layout.Gens != null)
                    foreach (GenBuildingLayout building in layout.Gens)
                    {
                        GPrism bPoly = new GPrism(building.Poly);
                        bPoly.Height = building.BuildingHeight;
                        bPoly.Identity = layout.Indentity;
                        bPoly.DeltaZ = 0.1f;
                        bPoly.BorderWidth = 5;
                        //bPoly.FillColor = ConvertColor.CreateColor4(Color.Gray);
                        if (!building.Open) bPoly.Filled = true;
                        else bPoly.Filled = false;

                        bPoly.FillColor = ConvertColor.CreateColor4(Color.DimGray);
                        ViewControl1.AllObjectsList.ObjectsToDraw.Add(bPoly);
                    }
                GPolygon borderPoly = new GPolygon(layout.Border);
                ViewControl1.AllObjectsList.ObjectsToDraw.Add(borderPoly);
            }
            ViewControl1.Invalidate();
            // ViewControl1.ZoomAll();
        }

        # endregion

        //==============================================================================================
        //==============================================================================================
        //==============================================================================================
        # region IsovistAnalysis
        IsovistAnalysis _isovistField;
        List<Poly2D> _obstRects;
        List<Line2D> _obstLines;
        //============================================================================================================================================================================
        private void Button_IsovistAnalysis_Click(object sender, EventArgs e)
        {
            IsovistAnalye();
        }

        //============================================================================================================================================================================
        private void IsovistAnalye()
        {
            _analysisType = AnalysisType.Isovist;
            _obstLines = new List<Line2D>();
            _obstRects = new List<Poly2D>();
            List<GenBuildingLayout> rects;

            if (_activeMultiLayout != null)
            {
                _isovistBorder = _borderPoly.Poly.Clone();// _border.Clone(); 
                _isovistBorder.Offset(1.2);
                Rect2D borderRect = GeoAlgorithms2D.BoundingBox(_isovistBorder.PointsFirstLoop);
                foreach (ChromosomeLayout curChromo in _activeMultiLayout.SubChromosomes)
                {
                    foreach (GenBuildingLayout curRect in curChromo.Gens)
                    {
                        if (!curRect.Open)
                        {
                            _obstLines.AddRange(curRect.Poly.Edges.ToList());
                            _obstRects.Add(curRect.Poly);
                        }
                    }
                }

            }
            else if (_activeLayout != null)
            {
                _isovistBorder = _borderPoly.Poly.Clone();
                _isovistBorder.Offset(1.2);
                Rect2D borderRect = GeoAlgorithms2D.BoundingBox(_isovistBorder.PointsFirstLoop);
                rects = _activeLayout.Gens;

                // add the buildings
                foreach (GenBuildingLayout curRect in rects)
                {
                    if (!curRect.Open)
                    {
                        _obstLines.AddRange(curRect.Poly.Edges.ToList());
                        _obstRects.Add(curRect.Poly);
                    }
                }
            }
            // test environmental polygons
            else if (constantDrawObjects != null)
            {
                foreach (GeoObject testObj in constantDrawObjects)
                {
                    if (testObj is GPolygon)
                    {
                        GPolygon envPoly = (GPolygon)testObj;
                        if (envPoly.Poly.Distance(_isovistBorder) <= 0)
                        {
                            _obstLines.AddRange(envPoly.Edges.ToList());
                            _obstRects.Add(envPoly.Poly);
                        }
                    }
                }
            }
            else
                return;

            // --------------------------------------
            _obstLines.AddRange(_isovistBorder.Edges);
            float cellSize = ControlParameters.IsovistCellSize;
            List<Vector2D> gridPoints = PointsForIsovist(_isovistBorder, _obstRects, cellSize);

            _isovistField = new IsovistAnalysis(gridPoints, _obstLines, cellSize);
            _isovistField.DisplayMeasure = comboBox_Isovist.Text;
            TimeSpan calcTime1, calcTime2;
            DateTime start = DateTime.Now;

            // -- run localr --
            _isovistField.Calculate(0.05f);

            DateTime end = DateTime.Now;
            calcTime1 = end - start;

            // -- run via server -- // approximately 3-4 times? slower on local maschine...
            //start = DateTime.Now;
            //_isovistField.CalculateViaService(borderRect, 5, _obstLines, _obstRects, _isovistBorder, 0.05f);

            //end = DateTime.Now; ;
            //calcTime2 = end - start;

            //Debug.WriteLine("grid with " + _isovistField.NumberOfActiveCells.ToString() + " active cells: ");
            //Debug.WriteLine("local calculation time 1: " + calcTime1.TotalMilliseconds.ToString() + " ms");
            //Debug.WriteLine("server calculation time 2: " + calcTime2.TotalMilliseconds.ToString() + " ms");

            //_isovistField.WriteToGridValues();

            string measure = "";
            if (comboBox_Isovist.SelectedValue != null)
                measure = comboBox_Isovist.SelectedValue.ToString();
            _smartGrid = new GGrid(gridPoints, _isovistField.GetMeasure(measure), _isovistField.CellSize);

            propertyGrid2.SelectedObject = _isovistField;

            
            DrawLayout();
        }

        //============================================================================================================================================================================
        private void Button_StartStop_Click(object sender, EventArgs e)
        {
            timer1.Enabled = !timer1.Enabled;
        }

        //============================================================================================================================================================================
        private void comboBox_IsovistIndexChanged(object sender, EventArgs e)
        {
            ControlParameters.DisplayMeasure = comboBox_Isovist.Text;
            if (_isovistField != null)
            {
                _isovistField.DisplayMeasure = comboBox_Isovist.Text;
                //_isovistField.WriteToGridValues();
                string measure = comboBox_Isovist.SelectedItem.ToString();
                _smartGrid = new GGrid(_isovistField.AnalysisPoints, _isovistField.GetMeasure(measure), _isovistField.CellSize);
                ViewControl1.Invalidate();
            }
        }

        //============================================================================================================================================================================
        private void propertyGrid2_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if (_isovistField != null)
            {
                Rect2D borderRect = GeoAlgorithms2D.BoundingBox(_isovistBorder.PointsFirstLoop);
               // _isovistField = new IsovistFieldOLD(borderRect, 5, _obstLines, _obstRects, _isovistBorder);
                _isovistField.Calculate(_isovistField.Precision);
                ViewControl1.Invalidate();
            }
        }

        //============================================================================================================================================================================
        private void propertyGrid1_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            //zunächt benötigen wir das aktuell im PropertyGrid angezeigt Objekt. Da nicht bekannt ist, um welches es sich handelt, 
            //erfolgt hier der Aufrug allgemeine als "object"
            object obj;
            if (e.ChangedItem.Parent.Value != null) obj = e.ChangedItem.Parent.Value;
            else obj = propertyGrid1.SelectedObject;

            //nun wird geprüft, ob es sich bei dem geänderten Wert um ein Property handelte!
            if (e.ChangedItem.GridItemType == GridItemType.Property)
            {
                PropertyInfo pInfo = obj.GetType().GetProperty(e.ChangedItem.PropertyDescriptor.Name);
                pInfo.SetValue(obj, e.ChangedItem.Value, null);
                if (_lastSelectedIdx != -1)
                {
                    _curGen = _activeLayout.Gens[_lastSelectedIdx];
                    pInfo.SetValue(_curGen, e.ChangedItem.Value, null);
                }
            }
            ViewControl1.Invalidate();
        }
        # endregion

        //==============================================================================================
        private void Button_Move_Click(object sender, EventArgs e)
        {
            MouseMode = "Move";
            statusLabel.Text = "Drag and drop building to MOVE it.";
        }

        //==============================================================================================
        private void Button_Rotate_Click(object sender, EventArgs e)
        {
            MouseMode = "Rotate";
            statusLabel.Text = "Hold left button and move mouse up and down to ROTATE building.";
        }

        //==============================================================================================
        private void Button_Scale_Click_1(object sender, EventArgs e)
        {
            MouseMode = "Scale";
            statusLabel.Text = "Hold left button and move mouse up and down to SCALE building.";
        }

        //==============================================================================================
        private void Button_Size_Click(object sender, EventArgs e)
        {
            MouseMode = "Size";
            statusLabel.Text = "Hold left button and move mouse up and down to change building SIZE.";
        }

        //==============================================================================================
        //============================== Layout Optimization ===========================================
        //==============================================================================================
        private void Button_OptiLayout_Click(object sender, EventArgs e)
        {
            ViewControl2.AllObjectsList = new ObjectsAllList();          
            _SOMOrderedArchive = new List<MultiPisaChromosomeBase>();           
            _population = null;
            _activeLayout = null;
            _selectedBlock = null;
            glControl2.Invalidate();

            // get population size
            try
            {
                _populationSize = Convert.ToInt16(uD_populationSize.Value);
            }
            catch
            {
                _populationSize = 20;
            }
            // iterations
            try
            {
                _iterations = Convert.ToInt16(uD_generations.Value);
            }
            catch
            {
                _iterations = 20;
            }
            ViewControl2.DeactivateInteraction();

            // run worker thread           
            _needToStop1 = false;
            if (sB_multiOpti.Checked)
            {
                if (cB_layoutAll.Checked && cB_optiAll.Checked) 
                    _workerThread = new Thread(new ThreadStart(SearchLayoutMultiAllBlocks));
                else 
                    _workerThread = new Thread(new ThreadStart(SearchLayoutMulti));
            }
            if (sB_singleOpti.Checked)
            {
                _workerThread = new Thread(new ThreadStart(SearchLayoutSingle));
            }
            _workerThread.Start();

        }

        //============================================================================================================================================================================
        // Worker thread
        void SearchLayoutSingle()
        {
            // measure the needed time:
            DateTime start = DateTime.Now;// DateTime.MinValue;
            DateTime end = DateTime.MinValue;
            // create fitness function
            FitnessFunctionLayoutSingle fitnessFunction = new FitnessFunctionLayoutSingle();
            // create population
            _populationSingle = new Population(_populationSize,
                new ChromosomeLayoutSingle(_borderPoly.Poly.Clone(),
                    ControlParameters.MinNrBuildings, ControlParameters.MaxNrBuildings,
                    ControlParameters.Density, ControlParameters.MinSideRatio),
                    fitnessFunction,
                    (ISelectionMethod)new EliteSelection()
                );
            _populationSingle.MutationRate = 0.25;
            _populationSingle.RandomSelectionPortion = 0.2;
            DrawLayoutSingle(0);
            Debug.WriteLine("bestFit", Math.Round(_populationSingle.FitnessMax, 2).ToString());
            int i = 1;

            // loop
            while (!_needToStop1)
            {
                // run one epoch of genetic algorithm
                _populationSingle.RunEpoch();
                DrawLayoutSingle(i);

                Debug.WriteLine(i.ToString() + ". iterat, bestFit", Math.Round(_populationSingle.FitnessMax, 2).ToString());
                Thread.Sleep(100);
                // increase current iteration
                i++;

                //
                if ((_iterations != 0) && (i > _iterations))
                    break;
            }
            end = DateTime.Now;
            span = end - start;

            ViewControl1.ActivateInteraction();
        }

        //============================================================================================================================================================================
        // Worker thread
        void SearchLayoutMultiAllBlocks()
        {
            if (_blocks == null) return;
            // measure the needed time:
            DateTime start = DateTime.Now;// DateTime.MinValue;
            DateTime end = DateTime.MinValue;

            // create fitness function
            FitnessFunctionMultiLayout fitnessFunction = new FitnessFunctionMultiLayout();
            _borderPoly = new GPolygon(_border.Clone());

            _isovistBorder = _borderPoly.Poly.Clone();
            _isovistBorder.Offset(2);

            int alpha = (int)uD_Alpha.Value;
            int lambda = (int)uD_Lambda.Value;
            int mu = (int)uD_Mu.Value;

            // --- create population ---
            if (_population == null)
            {
                ChromosomeMultiLayout curChromosome;
                if (_activeLayout != null) curChromosome = new ChromosomeMultiLayout(_activeMultiLayout);      
                else curChromosome = new ChromosomeMultiLayout(_borderPoly.Poly.Clone(),
                        ControlParameters.MinNrBuildings, ControlParameters.MaxNrBuildings,
                        ControlParameters.Density, ControlParameters.MinSideRatio, _blocks);

                        _population = new MultiPopulation(_populationSize, alpha, lambda, mu,
                        curChromosome,
                        fitnessFunction,
                        (IMultiSelectionMethod)new NonDominatedSelection(), // net needed anymore!!!
                        CalculationType.Method.Local
                    );
                _population.MutationRate = 0.1;
                _population.CrossoverRate = 0.7;
                _population.RandomSelectionPortion = 0.1;

                SetPopulationParameters();
            }
            DrawLayoutMulti(0);
            // iterations
            int i = 1;

            // loop
            while (!_needToStop1)
            {
                // run one epoch of genetic algorithm
                _population.RunEpoch();
                DrawLayoutMulti(i);
                Thread.Sleep(1);
                // set current iteration's info
                SetText(lbl_Generation, _population[0].Generation.ToString()); //SizeNonDominatedSet.ToString());

                // increase current iteration
                i++;

                if (cB_updateLucy.Checked)
                    ArchiveToLucy();
                //
                if ((_iterations != 0) && (i > _iterations))
                    break;
            }
            end = DateTime.Now;
            span = end - start;
            _population.Sel.Terminate();
            ViewControl2.ActivateInteraction();
        }

        //============================================================================================================================================================================
        // Worker thread
        void SearchLayoutMulti()
        {
            // measure the needed time:
            DateTime start = DateTime.Now;// DateTime.MinValue;
            DateTime end = DateTime.MinValue;

            // create fitness function
            FitnessFunctionLayout fitnessFunction = new FitnessFunctionLayout();

            if (_activeLayout != null) _borderPoly = new GPolygon(_activeLayout.Border);
            _isovistBorder = _borderPoly.Poly.Clone();
            _isovistBorder.Offset(2);
            //Rect2D borderRect = GeoAlgorithms2D.BoundingBox(_isovistBorder.PointsFirstLoop);

            int alpha = (int)uD_Alpha.Value;
            int lambda = (int)uD_Lambda.Value;
            int mu = (int)uD_Mu.Value;

            // create population
            if (_population == null)
            {
                ChromosomeLayout curChromosome;
                if (_activeLayout != null) curChromosome = new ChromosomeLayout(_activeLayout);
                else curChromosome = new ChromosomeLayout(_borderPoly.Poly.Clone(),
                        ControlParameters.MinNrBuildings, ControlParameters.MaxNrBuildings,
                        ControlParameters.Density, ControlParameters.MinSideRatio);

                _population = new MultiPopulation(_populationSize, alpha, lambda, mu,
                    //(greedyCrossover) ? new InstructionTreePisa(treeDepth) : new PermutationChromosome(treeDepth),
                        curChromosome,
                    //new ChromosomeLayout(_borderPoly.Poly.Clone(),
                    //    ControlParameters.MinNrBuildings, ControlParameters.MaxNrBuildings,
                    //    ControlParameters.Density, ControlParameters.MinSideRatio),
                        fitnessFunction,
                        (IMultiSelectionMethod)new NonDominatedSelection(), // net needed anymore!!!
                        CalculationType.Method.Local             
                    );
                _population.MutationRate = 0.1;
                _population.CrossoverRate = 0.7;
                _population.RandomSelectionPortion = 0.1;

                SetPopulationParameters();

            }
            DrawLayoutMulti(0);
            PlotFitnessValues(0);
            VarianceAnalysis(0);
            // iterations
            int i = 1;

            // loop
            while (!_needToStop1)
            {
                // run one epoch of genetic algorithm
                _population.RunEpoch();
                DrawLayoutMulti(i);
                PlotFitnessValues(i);
                VarianceAnalysis(i);
                Thread.Sleep(1);
                // set current iteration's info
                SetText(lbl_Generation, _population[0].Generation.ToString()); //SizeNonDominatedSet.ToString());
                if (cB_updateLucy.Checked)
                {
                    ArchivePartialToLucy();
                }
                //
                if ((_iterations != 0) && (i > _iterations))
                {break;}
                
                // increase current iteration
                i++;

                // -- test if garbage collecor can avoid the out of memory exeption...?
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            end = DateTime.Now;
            span = end - start;
            _population.Sel.Terminate();
            ViewControl2.ActivateInteraction();
        }

        //============================================================================================================================================================================
        private void DrawLayoutMulti(int iterat)//Population pop); //InstructionTreePisa fittest)
        {
            // -- draw the network --
            ViewControl1.AllObjectsList.ObjectsToDraw.AddRange(_remainDrawingObjects);

            float distX = (float)nUD_distX.Value / 10.0f;
            float distY = (float)nUD_distY.Value / 10.0f;
            List<double> fitnesValues = new List<double>();
            if (_population == null) return;
            if (cb_showSOMorder.Checked || cb_showBestGen.Checked)
            {
                ViewControl2.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
                //if (constantDrawObjects != null) ViewControl1.AllObjectsList.ObjectsToDraw.AddRange(constantDrawObjects);
            }

            if (cb_showSOMorder.Checked)
            {
                SOMorderProcess();
                if (cB_showSomMap.Checked) ShowEsomMap(_globalLrn);
                DrawLayoutSOM();
                return;
            }

            List<IMultiChromosome> archive = _population.Sel.GetArchive();
            for (int p = 0; p < archive.Count(); p++)
            {
                ChromosomeLayout curChromo = (ChromosomeLayout)(archive[p]);
                _globalArchive.Add(curChromo);
                if (curChromo.IsovistField != null)
                {
                    curChromo.IsovistField.DisplayMeasure = ControlParameters.DisplayMeasure;
                    //curChromo.IsovistField.WriteToGridValues();
                    //GGridOld VisualIsovistField = new GGridOld(curChromo.IsovistField);
                    GGrid VisualIsovistField = new GGrid(curChromo.IsovistField.AnalysisPoints, curChromo.IsovistField.GetMeasure(), curChromo.IsovistField.CellSize);
                    Vector2D deltaP = new Vector2D(p * curChromo.BoundingRect.Width * distX, iterat * curChromo.BoundingRect.Height * distY);
                    VisualIsovistField.Move(deltaP);
                    ViewControl2.AllObjectsList.ObjectsToDraw.Add(VisualIsovistField);

                    if (curChromo.Environment != null)
                        foreach (Poly2D envObj in curChromo.Environment)
                        {
                            GPolygon ePoly = new GPolygon(envObj);
                            ePoly.Identity = curChromo.Indentity;
                            ePoly.Move(deltaP);
                            ViewControl2.AllObjectsList.ObjectsToDraw.Add(ePoly);
                        }

                    if (curChromo.Gens != null)
                    {
                        foreach (GenBuildingLayout building in curChromo.Gens)
                        {
                            GPrism bPoly = new GPrism(building.Poly);
                            bPoly.Height = building.BuildingHeight;
                            bPoly.Identity = curChromo.Indentity;
                            bPoly.DeltaZ = 0.1f;
                            bPoly.BorderWidth = 2;
                            bPoly.FillColor = ConvertColor.CreateColor4(Color.White);
                            if (!building.Open) bPoly.Filled = true;
                            else bPoly.Filled = false;
                            //bPoly.FillColor = ConvertColor.CreateColor4(Color.LightGray);                       
                            bPoly.Move(deltaP);
                            ViewControl2.AllObjectsList.ObjectsToDraw.Add(bPoly);
                        }
                    }
                    GPolygon borderPoly = new GPolygon(curChromo.Border);
                    borderPoly.Identity = curChromo.Indentity;
                    borderPoly.Move(deltaP);
                    ViewControl2.AllObjectsList.ObjectsToDraw.Add(borderPoly);
                }
            }
            //if (cB_autoShot.Checked) SetGraphicContext(glControl1); //Screenshot();
            ViewControl2.Invalidate();
            ViewControl2.ZoomAll();
        }

        //============================================================================================================================================================================
        private void DrawLayoutSOM()//Population pop); //InstructionTreePisa fittest)
        {
            // -- draw the network --
            float distX = (float)nUD_distX.Value / 100.0f;
            float distY = (float)nUD_distY.Value / 100.0f;
            if (_somOrder == null)
            {
                DrawLayoutMulti(1);
                return;
            }
            ViewControl2.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            List<ChromosomeLayout> curArchive = new List<ChromosomeLayout>(); ;
            //if (_SOMOrderedArchive != null)
            {
                foreach (IMultiChromosome curIMChromo in _SOMOrderedArchive)
                    curArchive.Add((ChromosomeLayout)curIMChromo);
            }
            //else
            //{
            //    foreach (IMultiChromosome curIMChromo in _population.Sel.GetArchive())
            //        curArchive.Add((ChromosomeLayout)curIMChromo);
            //}

            for (int p = 0; p < _somOrder.GetLength(0); p++)
            {
                ChromosomeLayout curChromo = curArchive.Find(ChromosomeLayout => ChromosomeLayout.Indentity == _somOrder[p, 0]);
                if (curChromo.IsovistField != null)
                {
                    curChromo.IsovistField.DisplayMeasure = ControlParameters.DisplayMeasure;// comboBox_Isovist.Text;
                    //curChromo.IsovistField.WriteToGridValues();
                    GGrid VisualIsovistField = new GGrid(curChromo.IsovistField);
                    Vector2D deltaP = new Vector2D(_somOrder[p, 2] * curChromo.BoundingRect.Width * distX, _somOrder[p, 1] * -curChromo.BoundingRect.Height * distY);
                    VisualIsovistField.Move(deltaP);
                    ViewControl2.AllObjectsList.ObjectsToDraw.Add(VisualIsovistField);

                    if (curChromo.Environment != null)
                        foreach (Poly2D envObj in curChromo.Environment)
                        {
                            GPolygon ePoly = new GPolygon(envObj);
                            ePoly.Identity = curChromo.Indentity;
                            ePoly.Move(deltaP);
                            ViewControl2.AllObjectsList.ObjectsToDraw.Add(ePoly);
                        }

                    if (curChromo.Gens != null)
                        foreach (GenBuildingLayout building in curChromo.Gens)
                        {
                            GPrism bPoly = new GPrism(building.Poly);
                            bPoly.Height = building.BuildingHeight;
                            bPoly.Identity = curChromo.Indentity;
                            bPoly.DeltaZ = 0.1f;
                            bPoly.BorderWidth = 2;
                            bPoly.FillColor = ConvertColor.CreateColor4(Color.White);
                            if (!building.Open) bPoly.Filled = true;
                            else bPoly.Filled = false;
                            bPoly.Move(deltaP);
                            ViewControl2.AllObjectsList.ObjectsToDraw.Add(bPoly);
                        }

                    GPolygon borderPoly = new GPolygon(curChromo.Border);
                    borderPoly.Identity = curChromo.Indentity;
                    borderPoly.Move(deltaP);
                    ViewControl2.AllObjectsList.ObjectsToDraw.Add(borderPoly);
                }
            }
            //if (cB_autoShot.Checked) SetGraphicContext(glControl1); //Screenshot();
            ViewControl2.Invalidate();
            ViewControl2.ZoomAll();
        }

        //============================================================================================================================================================================
        private void DrawNetworkSOM()//Population pop); //InstructionTreePisa fittest)
        {
            // -- draw the network --
            float distX = (float)nUD_distX.Value / 100.0f;
            float distY = (float)nUD_distY.Value / 100.0f;
            if (_somOrder == null)
            {
                DrawLayoutMulti(1);
                return;
            }
            ViewControl3.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            List<InstructionTreePisa> curArchive = new List<InstructionTreePisa>(); ;

            foreach (IMultiChromosome curIMChromo in _SOMOrderedArchive)
                curArchive.Add((InstructionTreePisa)curIMChromo);

            for (int p = 0; p < _somOrder.GetLength(0); p++)
            {
                InstructionTreePisa curTree = curArchive.Find(InstructionTreePisa => InstructionTreePisa.Indentity == _somOrder[p, 0]);
                if (curTree.Network != null)
                {
                    Vector2D deltaP = new Vector2D(_somOrder[p, 2] * curTree.Border.BoundingBox().Width * distX, _somOrder[p, 1] * -curTree.Border.BoundingBox().Height * distY);

                    // - Environment --
                    //if (curChromo.Environment != null)
                    //{
                    //    foreach (Poly2D envObj in curChromo.Environment)
                    //    {
                    //        GPolygon ePoly = new GPolygon(envObj);
                    //        ePoly.Identity = curChromo.Indentity;
                    //        ePoly.Move(deltaP);
                    //        ViewControl3.AllObjectsList.ObjectsToDraw.Add(ePoly);
                    //    }
                    //}

                    //if (curTree.Gens != null)
                    {
                        //foreach (GenBuildingLayout building in curChromo.Gens)
                        //{
                        //    GPolygon bPoly = new GPolygon(building.Poly);
                        //    bPoly.Identity = curChromo.Indentity;
                        //    bPoly.DeltaZ = 0.1f;
                        //    bPoly.BorderWidth = 2;
                        //    bPoly.FillColor = ConvertColor.CreateColor4(Color.DimGray);
                        //    if (!building.Open) bPoly.Filled = true;
                        //    else bPoly.Filled = false;
                        //    bPoly.Move(deltaP);
                        //    ViewControl3.AllObjectsList.ObjectsToDraw.Add(bPoly);
                        //}

                        //List<IMultiChromosome> archive = _population.Sel.GetArchive();
                        //for (int k = 0; k < archive.Count(); k++)
                        //{
                            //InstructionTreePisa curTree = (InstructionTreePisa)(archive[k]);
                            if (curTree.ShortestPathes != null)
                            {
                                Vector4[] ffColors = null;
                                if (rB_fitChoice.Checked) ffColors = ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormChoiceArray()[0]);
                                if (rB_fitCentral.Checked) ffColors = ConvertColor.CreateFFColor4(curTree.ShortestPathes.GetNormCentralityMetricArray()[0]);
                                DrawAnalysis(ffColors, curTree.ShortestPathes.OrigEdges,
                                    curTree.ShortestPathes.OrigPoints, deltaP.ToPointD(), ViewControl3, curTree.Indentity);
                            }
                        //}
                    }

                    //GPolygon borderPoly = new GPolygon(curChromo.Border);
                    //borderPoly.Identity = curChromo.Indentity;
                    //borderPoly.Move(deltaP);
                    //ViewControl3.AllObjectsList.ObjectsToDraw.Add(borderPoly);
                }
            }
            //if (cB_autoShot.Checked) SetGraphicContext(glControl1); //Screenshot();
            ViewControl3.Invalidate();
            ViewControl3.ZoomAll();
        }

        //============================================================================================================================================================================
        private void DrawLayoutSingle(int iterat)//Population pop); //InstructionTreePisa fittest)
        {
            // -- draw the network --
            float dist = 1.1f;
            List<double> fitnesValues = new List<double>();

            if (cb_showSOMorder.Checked || cb_showBestGen.Checked)
            {
                ViewControl2.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            }

            for (int p = 0; p < _populationSingle.Size; p++)
            {
                ChromosomeLayoutSingle curChromo = (ChromosomeLayoutSingle)(_populationSingle[p]);
                if (curChromo.IsovistField != null)
                {
                    GGrid VisualIsovistField = new GGrid(curChromo.IsovistField);
                    Vector2D deltaP = new Vector2D(p * curChromo.BoundingRect.Width * dist, iterat * curChromo.BoundingRect.Height * dist);
                    VisualIsovistField.Move(deltaP);
                    ViewControl2.AllObjectsList.ObjectsToDraw.Add(VisualIsovistField);

                    if (curChromo.Environment != null)
                        foreach (Poly2D envObj in curChromo.Environment)
                        {
                            GPolygon ePoly = new GPolygon(envObj);
                            ePoly.Move(deltaP);
                            ViewControl2.AllObjectsList.ObjectsToDraw.Add(ePoly);
                        }

                    if (curChromo.Gens != null)
                        foreach (GenBuildingLayout building in curChromo.Gens)
                        {
                            GPrism bPoly = new GPrism(building.Poly);
                            bPoly.Height = building.BuildingHeight;
                            bPoly.DeltaZ = 0.1f;
                            bPoly.BorderWidth = 2;
                            bPoly.FillColor = ConvertColor.CreateColor4(Color.White);
                            bPoly.Filled = true;
                            bPoly.Move(deltaP);
                            ViewControl2.AllObjectsList.ObjectsToDraw.Add(bPoly);
                        }

                    GPolygon borderPoly = new GPolygon(curChromo.Border);
                    borderPoly.Move(deltaP);
                    ViewControl2.AllObjectsList.ObjectsToDraw.Add(borderPoly);

                }
            }
            //if (cB_autoShot.Checked) SetGraphicContext(glControl1); //Screenshot();
            ViewControl2.Invalidate();
            ViewControl2.ZoomAll();
        }

        //====================================================================================================
        private void uD_populationSize_ValueChanged(object sender, EventArgs e)
        {
            if (uD_nrChildren.Value < uD_populationSize.Value) uD_nrChildren.Value = uD_populationSize.Value;
            uD_Mu.Value = uD_populationSize.Value;
        }

        //====================================================================================================
        private void uD_nrChildren_ValueChanged(object sender, EventArgs e)
        {
            if (uD_nrChildren.Value < uD_populationSize.Value) uD_nrChildren.Value = uD_populationSize.Value;
            uD_Lambda.Value = uD_nrChildren.Value;
        }

        //====================================================================================================
        private void b_lockPanel_Click(object sender, EventArgs e)
        {
            splitContainer1.IsSplitterFixed = !splitContainer1.IsSplitterFixed;
        }

        //====================================================================================================
        private void comboBox_scenario_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetScenarioEnvironment();
            SetControlParameters();
        }

        //====================================================================================================
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                ViewControl1.Background = false;
                ViewControl2.Background = false;
            }
            else
            {
                ViewControl1.Background = true;
                ViewControl2.Background = true;
            }
            ViewControl1.Invalidate();
            ViewControl2.Invalidate();
        }

        //============================================================================================================================================================================
        private void SOMorderProcess()
        {
            string iniPath = Application.StartupPath;
            string name = "SOM_" + DateTime.Now.Day.ToString("00") + "-" +
                                    DateTime.Now.Month.ToString("00") + "_" +
                                    DateTime.Now.Hour.ToString("00") +
                                    DateTime.Now.Minute.ToString("00") +
                                    DateTime.Now.Second.ToString("00");
            _globalLrn = name + ".lrn";

            string path = System.IO.Path.Combine(Environment.CurrentDirectory, _globalLrn);
            TextRW.WriteLrn(_population, path);

            // ---------------------------------------------------------------------------------
            string strCmdText;
            string ESOMpath = @Properties.Settings.Default.PathToESOM;
            // -- esom training - lrn
            strCmdText = ESOMpath + "esomtrn.bat"; //
            // --------------
            // -- parameters:
            // -- path to the lrn file
            strCmdText += " -l " + _globalLrn;
            // -- number of training epochs
            strCmdText += " -e 100";
            // -- execute the command
            ExecuteCommandSync(strCmdText);

            //img.Save(path, System.Drawing.Imaging.ImageFormat.Png);
            //MessageBox.Show(screenshotFilename);

            _somOrder = TextRW.ReadBm(name + ".bm");

            _SOMOrderedArchive = _population.Sel.GetArchiveOrig();

        }

        //====================================================================================================
        private void b_ESOM_Click(object sender, EventArgs e)
        {
            string strCmdText;
            // -- The path to ESOM commands
            string path = @Properties.Settings.Default.PathToESOM;  // "C:\\Users\\koenigr\\Documents\\ESOM\\bin\\";
            // -- Commands that can be used: (execute them in the cmd shows you the parameters that can be specified)       
            //esomana	Starts the ESOM Analyzer GUI. You can specify filenames to be automatically loaded.
            //esomcls	Loads a class mask (*.cmx) and bestmatches (*.bm) and saves the result of the classification of the bestmatches (*.bm).
            //esomprj	Loads a trained ESOM (*.wts) and a dataset (*.lrn) and saves the bestmatches for the data on the grid (*.bm).
            //esommat	Loads a trained ESOM (*.wts) and optionally a dataset (*.lrn). Saves a matrix of height values (*.umx).
            //esomrnd	Loads a trained ESOM (*.wts) and optionally a dataset (*.lrn) and a classification (*.cls). Saves an image (*.png).
            //esomtrn	Loads a dataset (*.lrn) and trains an ESOM. The weights (*.wts) and final bestmatches (*.bm) are saved.

            // -- esom training
            strCmdText = path + "esomtrn.bat"; //
            // --------------
            // -- parameters:
            // -- path to the lrn file
            strCmdText += " -l C:\\Users\\koenigr\\Programmierung\\ComputationalPlanningGroup\\Various\\140608_LayoutB_varNr_02.lrn";
            // -- number of training epochs
            strCmdText += " -e 200";

            // -- execute the command
            ExecuteCommandSync(strCmdText);
        }

        //====================================================================================================
        private void b_ESOMrender_Click(object sender, EventArgs e)
        {
            //
            //string myUserSetting = Properties.Settings.Default.MyUserSetting;
            //string myApplicationSetting = Properties.Settings.Default.MyApplicationSetting;

            string strCmdText;
            // -- The path to ESOM commands
            string path = @Properties.Settings.Default.PathToESOM; //"C:\\Users\\koenigr\\Documents\\ESOM\\bin\\";

            // -- esom render
            strCmdText = path + "esomrnd.bat";

            // --------------
            // -- parameters:
            // -- path to the lrn file
            strCmdText += " -l C:\\Users\\koenigr\\Programmierung\\ComputationalPlanningGroup\\Various\\140608_LayoutB_varNr_02.lrn";
            // -- path to the bm file
            strCmdText += " -b C:\\Users\\koenigr\\Programmierung\\ComputationalPlanningGroup\\Various\\140608_LayoutB_varNr_02.bm";
            // -- path to the wts file
            strCmdText += " -w C:\\Users\\koenigr\\Programmierung\\ComputationalPlanningGroup\\Various\\140608_LayoutB_varNr_02.wts";
            // -- background renderer
            strCmdText += " -B pmx";
            // -- name of color table
            strCmdText += " -r jet";
            // -- png output file
            strCmdText += " -p C:\\Users\\koenigr\\Programmierung\\ComputationalPlanningGroup\\Various\\140608_LayoutB_varNr_02.png";
            // -- zoom factor
            strCmdText += " -z 20";

            // -- execute the command
            ExecuteCommandSync(strCmdText);

            // -- show the rendering
            pictureBox_ESOM.Image = new Bitmap("C:\\Users\\koenigr\\Programmierung\\ComputationalPlanningGroup\\Various\\140608_LayoutB_varNr_02.png");
            pictureBox_ESOM.Invalidate();
        }

        //====================================================================================================
        private void ShowEsomMap(string fileName)
        {
            //
            //string myUserSetting = Properties.Settings.Default.MyUserSetting;
            //string myApplicationSetting = Properties.Settings.Default.MyApplicationSetting;

            string strCmdText;
            // -- The path to ESOM commands
            string path = @Properties.Settings.Default.PathToESOM; //"C:\\Users\\koenigr\\Documents\\ESOM\\bin\\";

            // -- esom render
            strCmdText = path + "esomrnd.bat";

            int len = _globalLrn.Length > 3 ? _globalLrn.Length - 4 : 0;
            string name = _globalLrn.Remove(len, 4);
            string pathToLrn = System.IO.Path.Combine(Environment.CurrentDirectory, _globalLrn);
            string pathToBm = System.IO.Path.Combine(Environment.CurrentDirectory, name + ".bm");
            string pathToWts = System.IO.Path.Combine(Environment.CurrentDirectory, name + ".wts");
            string pathToPng = System.IO.Path.Combine(Environment.CurrentDirectory, name + ".png");
            // --------------
            // -- parameters:
            // -- path to the lrn file
            strCmdText += " -l " + pathToLrn;
            // -- path to the bm file
            strCmdText += " -b " + pathToBm;
            // -- path to the wts file
            strCmdText += " -w " + pathToWts;
            // -- background renderer
            strCmdText += " -B pmx";
            // -- name of color table
            strCmdText += " -r jet";
            // -- png output file
            strCmdText += " -p " + pathToPng;
            // -- zoom factor
            strCmdText += " -z 20";

            // -- execute the command
            ExecuteCommandSync(strCmdText);

            // -- show the rendering
            pictureBox_ESOM.Image = new Bitmap(pathToPng);
            pictureBox_ESOM.Invalidate();
        }

        //====================================================================================================
        /// <span class="code-SummaryComment"><summary></span>
        /// Executes a shell command synchronously.
        /// Code from: http://www.codeproject.com/Articles/25983/How-to-Execute-a-Command-in-C
        /// <span class="code-SummaryComment"></summary></span>
        /// <span class="code-SummaryComment"><param name="command">string command</param></span>
        /// <span class="code-SummaryComment"><returns>string, as output of the command.</returns></span>
        public void ExecuteCommandSync(object command)
        {
            try
            {
                // create the ProcessStartInfo using "cmd" as the program to be run,
                // and "/c " as the parameters.
                // Incidentally, /c tells cmd that we want it to execute the command that follows,
                // and then exit.
                System.Diagnostics.ProcessStartInfo procStartInfo =
                    new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command);

                // The following commands are needed to redirect the standard output.
                // This means that it will be redirected to the Process.StandardOutput StreamReader.
                procStartInfo.RedirectStandardOutput = true;
                procStartInfo.UseShellExecute = false;
                // Do not create the black window.
                procStartInfo.CreateNoWindow = true;
                // Now we create a process, assign its ProcessStartInfo and start it
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = procStartInfo;
                proc.Start();
                // Get the output into a string
                string result = proc.StandardOutput.ReadToEnd();
                // Display the command output.
                Console.WriteLine(result);
            }
            catch (Exception objException)
            {
                // Log the exception
            }
        }

        //====================================================================================================
        /// <span class="code-SummaryComment"><summary></span>
        /// Execute the command Asynchronously.
        /// Code from: http://www.codeproject.com/Articles/25983/How-to-Execute-a-Command-in-C
        /// <span class="code-SummaryComment"></summary></span>
        /// <span class="code-SummaryComment"><param name="command">string command.</param></span>
        public void ExecuteCommandAsync(string command)
        {
            try
            {
                //Asynchronously start the Thread to process the Execute command request.
                Thread objThread = new Thread(new ParameterizedThreadStart(ExecuteCommandSync));
                //Make the thread as background thread.
                objThread.IsBackground = true;
                //Set the Priority of the thread.
                objThread.Priority = ThreadPriority.AboveNormal;
                //Start the thread.
                objThread.Start(command);
            }
            catch (ThreadStartException objException)
            {
                // Log the exception
            }
            catch (ThreadAbortException objException)
            {
                // Log the exception
            }
            catch (Exception objException)
            {
                // Log the exception
            }
        }









        //============================================================================================================================================================================
        //============================================================================================================================================================================
        //private Communication2Lucy _cl = null;
        private int _curScenarioID = -1;

        //====================================================================================================
        private void appSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAppSettings formSettings = new FormAppSettings();
            formSettings.ShowDialog();
            PisaWrapper.PISA_DIR = @Properties.Settings.Default.PathToPISA;
        }

        //============================================================================================================================================================================
        private void button2_Click(object sender, EventArgs e)
        {
        /*    //_cl = new Communication2Lucy();
            GlobalConnection.CL = new Communication2Lucy();

            string resultT = "Connection to Lucy was ";

            if (!GlobalConnection.CL.connect(GlobalConnection.IP, GlobalConnection.Port))//("129.132.6.4", 7654)) // 192.168.2.100
                resultT += "not ";

            resultT += "successful! \r\n";
            Console.WriteLine(resultT);
            messageBox.Text += resultT;

            // --- connect --- 
            // --- use lukas & 1234 ---
            if (GlobalConnection.CL == null)
            {
                InfoNoConnection();
            }
            else
            {
                //if (pwBox != null && pwBox is PasswordBox)
                {
                    //var passwordBox = (PasswordBox)param;
                    JProperty result = GlobalConnection.CL.authenticate("lukas", "1234");
                    //JProperty result = cl.authenticate(User, (pwBox as PasswordBox).Password);
                    messageBox.Text += (result.Name + ": " + result.Value + "\r\n");
                }
            }
          */
        }

        //============================================================================================================================================================================
        private void Button_IsovistALucy_Click(object sender, EventArgs e)
        {
            ScenarioToLucy();
        }

        //============================================================================================================================================================================
        private void ScenarioToLucy()
        {
            List<Poly2D> buildings2D = new List<Poly2D>();
            List<GPrism> buildings3D = new List<GPrism>();
            List<Line2D> edges = new List<Line2D>();

            // --- streets ---
            if (_activeNetwork != null)
            {
                UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> network;
                network = _activeNetwork.CreateStreetNetwork();
                edges = GeometryConverter.ToListLine2D(network);
            }

            // --- buildings ---
            if (_activeMultiLayout != null)
            {
                foreach (ChromosomeLayout subChromo in _activeMultiLayout.SubChromosomes)
                {
                    foreach (GenBuildingLayout gen in subChromo.Gens)
                    {
                        if (!gen.Open)
                            buildings2D.Add(gen.Poly);
                    }
                }
                foreach (ChromosomeLayout subChromo in _activeMultiLayout.SubChromosomes)
                {
                    foreach (GenBuildingLayout gen in subChromo.Gens)
                    {
                        if (!gen.Open)
                        {
                            buildings3D.Add(new GPrism(gen.Poly, gen.BuildingHeight));
                        }
                    }
                }
            }
            else if (_activeLayout != null)
            {
                foreach (GenBuildingLayout gen in _activeLayout.Gens)
                {
                    if (!gen.Open)
                        buildings2D.Add(gen.Poly);
                }
                foreach (GenBuildingLayout gen in _activeLayout.Gens)
                {
                    if (!gen.Open)
                    {
                        buildings3D.Add(new GPrism(gen.Poly, gen.BuildingHeight));
                    }
                }
            }

            // --- send the following: streets / blocks / buildings  / boudary / isovistPoints / ---
            List<Vector2D> anaPts = new List<Vector2D>();
            Poly2D border = new Poly2D();
            if (_activeLayout != null)
            {
                border = _activeLayout.Border.Clone();
                if (_activeLayout.IsovistField != null) anaPts = _activeLayout.IsovistField.AnalysisPoints;
            }
           // CreateIsovistLucyScenario(edges, _blocks, buildings2D, border, anaPts, buildings3D);
        }

        //============================================================================================================================================================================
        private void ArchivePartialToLucy()
        {
            List<Poly2D> buildings2D = new List<Poly2D>();
            List<GPrism> buildings3D = new List<GPrism>();
            List<Line2D> edges = new List<Line2D>();

            // --- streets ---
            if (_activeNetwork != null)
            {
                UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> network;
                network = _activeNetwork.CreateStreetNetwork();
                edges = GeometryConverter.ToListLine2D(network);
            }

            List<IMultiChromosome> archive = _population.Sel.GetArchive();
            ChromosomeLayout myRefChromo = (ChromosomeLayout)archive[0];
            Poly2D myBlockRef = myRefChromo.Border;

            // --- make new scenario for each subChromo ---
            foreach (ChromosomeLayout archChromo in archive)
            {
                buildings2D = new List<Poly2D>();
                buildings3D = new List<GPrism>();
                // --- buildings ---
                if (_activeMultiLayout != null)
                {
                    foreach (ChromosomeLayout subChromo in _activeMultiLayout.SubChromosomes)
                    {
                        double overlap = subChromo.Border.Overlap(myBlockRef);
                        double ratio = overlap / myBlockRef.Area;
                        if (ratio > 0.9)
                        {
                            foreach (GenBuildingLayout gen in archChromo.Gens)
                            {
                                if (!gen.Open)
                                {
                                    buildings2D.Add(gen.Poly);
                                    buildings3D.Add(new GPrism(gen.Poly, gen.BuildingHeight));
                                }
                            }
                        }
                        else
                        {
                            foreach (GenBuildingLayout gen in subChromo.Gens)
                            {
                                if (!gen.Open)
                                {
                                    buildings2D.Add(gen.Poly);
                                    buildings3D.Add(new GPrism(gen.Poly, gen.BuildingHeight));
                                }
                            }
                        }
                    }
                }

                // --- send the following: streets / blocks / buildings  / boudary / isovistPoints / ---
                List<Vector2D> anaPts = new List<Vector2D>();
                Poly2D border = new Poly2D();
                if (_activeLayout != null)
                {
                    border = _activeLayout.Border.Clone();
                    //if (_activeLayout.IsovistField != null) anaPts = _activeLayout.IsovistField.AnalysisPoints;
                }
                //CreateIsovistLucyScenario(edges, _blocks, buildings2D, border, anaPts, buildings3D);
            }
        }

        //============================================================================================================================================================================
        private void ArchiveToLucy()
        {
            TextRW.LucyIDs = new List<int>();
            List<IMultiChromosome> archive = _population.Sel.GetArchive();
            for (int p = 0; p < archive.Count(); p++)
            {
                ChromosomeMultiLayout curMultiChromo = (ChromosomeMultiLayout)(archive[p]);

                // --- make new scenario for each subChromo ---
                foreach (ChromosomeLayout subChromo in curMultiChromo.SubChromosomes)
                {
                    // --- streets --- always the same at the moment...
                    List<Line2D> edges = new List<Line2D>();
                    if (_activeNetwork != null)
                    {
                        UndirectedGraph<Vector2D, UndirectedEdge<Vector2D>> network;
                        network = _activeNetwork.CreateStreetNetwork();
                        edges = GeometryConverter.ToListLine2D(network);
                    }

                    List<Poly2D> buildings2D = new List<Poly2D>();
                    List<GPrism> buildings3D = new List<GPrism>();

                    // --- buildings ---
                    if (subChromo.Gens.Count > 0)
                    {
                        foreach (GenBuildingLayout gen in subChromo.Gens)
                        {
                            if (!gen.Open)
                                buildings2D.Add(gen.Poly);
                        }
                        foreach (GenBuildingLayout gen in subChromo.Gens)
                        {
                            if (!gen.Open)
                            {
                                buildings3D.Add(new GPrism(gen.Poly, gen.BuildingHeight));
                            }
                        }
                    }

                    // --- isovist analysis points ---
                    List<Vector2D> anaPts = new List<Vector2D>();
                    Poly2D border = new Poly2D();
                    if (subChromo.IsovistField != null)
                    {                   
                        border = subChromo.Border.Clone();
                        anaPts = subChromo.IsovistField.AnalysisPoints;
                    }
                    // --- send the following: streets / blocks / buildings  / boudary / isovistPoints / 3D buildings---
                   // CreateIsovistLucyScenario(edges, _blocks, buildings2D, border, anaPts, buildings3D);
                }
            }
        }

        //=============GeoJasonTest==================================================================================================================================================
        //private void GeoJasonTest()
        //{
        //    GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
        //    GeoJsonReader GeoJSONReader = new GeoJsonReader();

        //    List<CPlan.Geometry.Vector2D> points = new List<CPlan.Geometry.Vector2D>();
        //    points.Add(new CPlan.Geometry.Vector2D(1, 1));
        //    points.Add(new CPlan.Geometry.Vector2D(3, 2));

        //    List<CPlan.Geometry.Line2D> lines = new List<CPlan.Geometry.Line2D>();
        //    lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(0, 0), new CPlan.Geometry.Vector2D(0, 10)));
        //    lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(0, 10), new CPlan.Geometry.Vector2D(10, 10)));
        //    lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(0, 0), new CPlan.Geometry.Vector2D(10, 0)));
        //    lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(10, 0), new CPlan.Geometry.Vector2D(10, 10)));
        //    lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(2, 5), new CPlan.Geometry.Vector2D(8, 5)));

        //    //create sample iso object on client
        //    IsovistField2D iso_client_orig = new IsovistField2D(points, lines);

        //    //pass it to the wrapper and serialize to JSON
        //    IsovistField2DWrapper wrapper = new IsovistField2DWrapper(iso_client_orig);
        //    string iso_client_send = GeoJSONWriter.Write(wrapper);
        //    messageBox.Text += "--- SEND --- : \r\n";
        //    messageBox.Text += iso_client_send + " \r\n";

        //    //read json on server and deserialize into wrapper
        //    IsovistField2DWrapper iso_wrapper_server = GeoJSONReader.Read<IsovistField2DWrapper>(iso_client_send);
        //    //conver wrapper to isovist object
        //    IsovistField2D iso_server = iso_wrapper_server.ToIsovistField2D();

        //    //perform actual calculations, the precision need to be set explicitly here since we can't do it using isovist constructor
        //    iso_server.Calculate(iso_wrapper_server.Precision, true);

        //    //wrap the results
        //    IsovistField2DWrapper iso_server_wrapper_result = new IsovistField2DWrapper(iso_server);
        //    string iso_server_send = GeoJSONWriter.Write(iso_server_wrapper_result);
        //    messageBox.Text += "--- Receive ---: \r\n";
        //    messageBox.Text += iso_server_send + " \r\n";

        //    //read results on client           
        //    IsovistField2DWrapper iso_client_wrapper_result = GeoJSONReader.Read<IsovistField2DWrapper>(iso_server_send);
        //    //here we are unable to convert back to isovist object since there are no public method allowing us to set the results.
        //    //we can change the isovist public API to allow this in the future.
        //}

        //============================================================================================================================================================================
              
        /// <summary>
        /// 
        /// </summary>
        /// <param name="edges"></param>
        /// <param name="blocks"></param>
        /// <param name="buildings"></param>
        /// <param name="boundary"></param>
        /// <param name="analysisPoints"></param>
        /*
        private void CreateIsovistLucyScenario(List<Line2D> edges, List<Poly2D> blocks, List<Poly2D> buildings, Poly2D boundary, List<Vector2D> analysisPoints, List<GPrism> buildings3D )
        {
            if (GlobalConnection.CL == null)
            {
                InfoNoConnection();
                return;
            }
            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
            // --- features collect all geometries + layer info ---
            System.Collections.ObjectModel.Collection<NetTopologySuite.Features.Feature> features = new System.Collections.ObjectModel.Collection<Feature>();
            // --- featurecollection is used to send the features via GeoJSON --- 
            NetTopologySuite.Features.FeatureCollection featurecollection;
            
            // --- streets ---
            if (edges.Count > 0)
            {
                AttributesTable streetAttr = new AttributesTable();
                streetAttr.AddAttribute("layer", "street");
                foreach (Line2D edge in edges)
                {
                    LineString lNet = new NetTopologySuite.Geometries.LineString(GeometryConverter.ToCoordinates(edge));
                    features.Add(new Feature(lNet, streetAttr));
                }
            }

            // --- blocks ---
            if (blocks.Count > 0)
            {
                AttributesTable blockAttr = new AttributesTable();
                blockAttr.AddAttribute("layer", "parcel");
                foreach (Poly2D poly in blocks)
                {
                    LinearRing block = new NetTopologySuite.Geometries.LinearRing(GeometryConverter.ToCoordinates(poly));
                    NetTopologySuite.Geometries.Polygon blockNet = new NetTopologySuite.Geometries.Polygon(block);
                    features.Add(new Feature(blockNet, blockAttr));
                }
            }

            // --- buildings2D ---
            if (buildings.Count > 0)
            {
                AttributesTable buildingAttr = new AttributesTable();
                buildingAttr.AddAttribute("layer", "building");
                foreach (Poly2D build in buildings)
                {
                    if (!double.IsNaN(build.Area))
                    {
                        LinearRing bld = new NetTopologySuite.Geometries.LinearRing(GeometryConverter.ToCoordinates(build));
                        NetTopologySuite.Geometries.Polygon bldNet = new NetTopologySuite.Geometries.Polygon(bld);
                        features.Add(new Feature(bldNet, buildingAttr));
                    }
                }
            }

            // --- buildings3D ---
            if (buildings3D.Count > 0)
            {
                AttributesTable building3DAttr = new AttributesTable();
                building3DAttr.AddAttribute("layer", "building3D");
                foreach (GPrism build3D in buildings3D)
                {
                    if (!double.IsNaN(build3D.GetArea()))
                    {
                        Vector2D[] vertices = build3D.Poly.PointsFirstLoop;
                        double dZ = 0;
                        Coordinate[] coord = new Coordinate[5];
                        NetTopologySuite.Geometries.Polygon[] bldNet = new NetTopologySuite.Geometries.Polygon[6];

                        for (int i = 0; i < vertices.Length; i++)
                        {
                            Vector2D next = vertices[i];// +offset;
                            int j = i + 1;
                            if (j >= vertices.Length) j -= vertices.Length;
                            Vector2D next2 = vertices[j];

                            coord = new Coordinate[5];
                            coord[0] = new Coordinate(next.X, next.Y, dZ);
                            coord[1] = new Coordinate(next.X, next.Y, build3D.Height);
                            coord[2] = new Coordinate(next2.X, next2.Y, build3D.Height);
                            coord[3] = new Coordinate(next2.X, next2.Y, dZ);
                            coord[4] = new Coordinate(next.X, next.Y, dZ);
                            LinearRing bld = new NetTopologySuite.Geometries.LinearRing(coord);
                            bldNet[i] = new NetTopologySuite.Geometries.Polygon(bld);
                        }

                        coord = new Coordinate[5];
                        coord[0] = new Coordinate(vertices[0].X, vertices[0].Y, dZ);
                        coord[1] = new Coordinate(vertices[1].X, vertices[1].Y, dZ);
                        coord[2] = new Coordinate(vertices[2].X, vertices[2].Y, dZ);
                        coord[3] = new Coordinate(vertices[3].X, vertices[3].Y, dZ);
                        coord[4] = new Coordinate(vertices[0].X, vertices[0].Y, dZ);
                        LinearRing bldb = new NetTopologySuite.Geometries.LinearRing(coord);
                        bldNet[4] = new NetTopologySuite.Geometries.Polygon(bldb);

                        coord = new Coordinate[5];
                        coord[0] = new Coordinate(vertices[0].X, vertices[0].Y, build3D.Height);
                        coord[1] = new Coordinate(vertices[1].X, vertices[1].Y, build3D.Height);
                        coord[2] = new Coordinate(vertices[2].X, vertices[2].Y, build3D.Height);
                        coord[3] = new Coordinate(vertices[3].X, vertices[3].Y, build3D.Height);
                        coord[4] = new Coordinate(vertices[0].X, vertices[0].Y, build3D.Height);
                        bldb = new NetTopologySuite.Geometries.LinearRing(coord);
                        bldNet[5] = new NetTopologySuite.Geometries.Polygon(bldb);

                        MultiPolygon buid3D = new MultiPolygon(bldNet);
                        features.Add(new Feature(buid3D, building3DAttr));
                    }
                }
            }

            // --- boundary ---
            if (boundary.PointsFirstLoop != null)
            {
                if (!double.IsNaN(boundary.Area))
                {
                    AttributesTable boundaryAttr = new AttributesTable();
                    boundaryAttr.AddAttribute("layer", "boundary");
                    LinearRing bound1 = new NetTopologySuite.Geometries.LinearRing(GeometryConverter.ToCoordinates(boundary));
                    NetTopologySuite.Geometries.Polygon boundNet = new NetTopologySuite.Geometries.Polygon(bound1);
                    features.Add(new Feature(boundNet, boundaryAttr));
                }
            }

            // --- analysisPoints ---
            if (analysisPoints != null)
            {
                if (analysisPoints.Count > 0)
                {
                    AttributesTable pointAttr = new AttributesTable();
                    pointAttr.AddAttribute("layer", "isovistPoints");
                    List<NetTopologySuite.Geometries.Point> pointArray = new List<NetTopologySuite.Geometries.Point>();
                    int cter = 0;
                    foreach (Vector2D point in analysisPoints)
                    {
                        NetTopologySuite.Geometries.Point pt = new NetTopologySuite.Geometries.Point(GeometryConverter.ToCoordinates(point));
                        if (pt != null)
                            pointArray.Add(pt);
                        cter++;
                    }
                    NetTopologySuite.Geometries.MultiPoint multiPts = new MultiPoint(pointArray.ToArray());
                    features.Add(new Feature(multiPts, pointAttr));
                }
            }

            // --- pack all together ---
            featurecollection = new FeatureCollection(features);
            string geoJSONPoly = GeoJSONWriter.Write(featurecollection);

            geoJSONPoly = geoJSONPoly.Substring(0, geoJSONPoly.Length - 1);
            //string createScenario4Geo = "{'action':'create', 'scenario':{'name':'test', 'boundary':{}, 'inputs':" + GeoJSONWriter.Write(mPoly) + "}}}}}";
            //string createScenario4Geo = "{'action':'create','scenario':{'boundary':{},'name':'test','inputs':{'geometry':{'GeoJSON':" + geoJSONPoly + ",'crs':{'type':'name','properties':{'name':'EPSG:21781'}}}}}}}}";
            string createScenario4Geo = "{'action':'create_scenario','name':'test','geometry':{'format':'GeoJSON','value':" + geoJSONPoly + "}}}}";
            //string createScenario4Geo = "{'action':'create','scenario':{'boundary':{},'name':'test','inputs':{'geometry':{'GeoJSON':" + geoJSONPoly + ",'crs':{'type':'name','properties':{'name':'EPSG:26918'}}}}}}}}";
            //messageBox.Text = "____________  \r\n";
            //messageBox.Text += "--- SEND --- : \r\n";
            //messageBox.Text += "sent to lucy: " + " \r\n"; //+ createScenario4Geo

            // --- send to lucy ---
            JObject scenarioObj = GlobalConnection.CL.sendAction2Lucy(createScenario4Geo);

            if (scenarioObj != null)
            {
                JProperty result = ((JProperty)(scenarioObj.First));
                if (result != null)
                {
                    //messageBox.Text += "______________  \r\n";
                    //messageBox.Text += "--- Result --- : \r\n";
                    //messageBox.Text += (result.Name + ": " + result.Value + " \r\n");

                    if (result.Value["ScID"] != null)
                        TextRW.LucyIDs.Add( _curScenarioID = (int)result.Value["ScID"]);
                }
            }
        }
        
         * void NotificationAllResultsReadyHandler(object sender, Dictionary<int, object> userInfo)
        {
            if (userInfo != null)
            {
                // in dem Dictionary sind Deine Ergebnisse: key = index, value = Berechnungsergebnisse, wie Du Sie unter 4. AddResult() zugefügt hast.
            }
        }
         */

        //============================================================================================================================================================================
        private List<Vector2D> PointsForIsovist(Poly2D field, List<GenBuildingLayout> buildings, float cellSize)
        {
            List<Poly2D> buildRects = new List<Poly2D>();
            foreach (GenBuildingLayout curGen in buildings)
            {
                if (!curGen.Open)
                {
                    buildRects.Add(curGen.Poly);
                }
            }
            return PointsForIsovist(field, buildRects, cellSize);
        }

        private List<Vector2D> PointsForIsovist(Poly2D field, List<Poly2D> buildings, float cellSize)
        {
            List<Vector2D> result = new List<Vector2D>();

            int countX = (int)(field.BoundingBox().Width / cellSize);
            int countY = (int)(field.BoundingBox().Height / cellSize);
            Vector2D startP = field.BoundingBox().BottomLeft;
            for (int i = 0; i < countX+1; i++)
            {
                for (int j = 0; j < countY+1; j++)
                {
                    Vector2D curPos = new Vector2D(startP.X + i * cellSize + cellSize / 2, startP.Y + j * cellSize + cellSize / 2);
                    //m_activeCells[j * m_countX + i] = true;
                    bool active = true;
                    foreach (Poly2D curGen in buildings)
                    {
                        if (field.ContainsPoint(curPos))
                        {
                            //if (!curGen.Open)
                            //{
                                if (curGen.ContainsPoint(curPos))
                                {
                                    active = false;
                                    break;
                                }
                            //}
                        }
                        else active = false;
                    }

                    if (active)
                    {
                        result.Add(curPos);
                    }

                }
            }
            return result;
        }



        
        //============================================================================================================================================================================
        private void Button_IsovistLucy_Click(object sender, EventArgs e)
        {
            //ResultManager.Instance.RegisterNotification(1, "FitnessForMultiChromosome_Single");
            //NotificationBroker.Instance.Register(NotificationAllResultsReadyHandler, "FitnessForMultiChromosome_Single");
/*
            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
            float cellSize = ControlParameters.IsovistCellSize;
            List<Vector2D> gridPoints = PointsForIsovist(_activeLayout.Border, _activeLayout.Gens, cellSize);
            MultiPoint gridPointsJ = GeometryConverter.ToMultiPoint(gridPoints);

            string geoJSONPoints = GeoJSONWriter.Write(gridPointsJ);
            string msg = "{'action':'create_service','inputs':{'inputVals':{'format':'text','value':'" + geoJSONPoints + "'}},'classname':'Isovist','ScID':'" + _curScenarioID + "'}";

            if (GlobalConnection.CL == null)
            {
                InfoNoConnection();
            }
            else
            {
                _configInfo = GlobalConnection.CL.sendCreateService2Lucy(msg, _curScenarioID);
                //if (_configInfo.mqtt_topic != null)
                //{
                //    if (mqttClient.IsConnected)
                //    {
                //        mqttClient.Subscribe(new string[] { _configInfo.mqtt_topic }, new byte[] { 1 });
                //        mqttClient.Publish(_configInfo.mqtt_topic, Encoding.UTF8.GetBytes("RUN"));
                //    }
                //}
                NotificationBroker.Instance.Register(NotificationEvaluationReadyHandler, _configInfo.mqtt_topic);
                GlobalConnection.CL.startServiceViaMQTT(_configInfo.mqtt_topic, _configInfo.objID);

            }
            */
        }
        /*
        void NotificationEvaluationReadyHandler(object sender, Dictionary<int, object> userInfo)
        {
            //string msg = "{'action':'get_service_outputs','SObjID':" + instanceID + "}";
            float cellSize = ControlParameters.IsovistCellSize;
            List<Vector2D> gridPoints = PointsForIsovist(_activeLayout.Border, _activeLayout.Gens, cellSize);

            if (userInfo != null)
            {
                JObject result = (JObject)userInfo.First().Value;
                if (result != null)
                {
                    JToken outputs = result.Value<JToken>("result").Value<JToken>("outputs");
                    if (outputs != null)
                    {
                        string output = (string)(outputs.Value<JObject>("outputVals").Value<String>("value"));
                        //messageBox.Text += ("result: " + outputs + " - output rows: " + output + "\r\n");

                        GeoJsonReader GeoJSONReader = new GeoJsonReader();
                        //IsovistField2DWrapper iso_client_wrapper_result = GeoJSONReader.Read<IsovistField2DWrapper>(output);
                        IsovistResultsWrapper iso_results_wrapper = GeoJSONReader.Read<IsovistResultsWrapper>(output);

                        List<Dictionary<ResultsIsovist, double>> isovistFieldValues = iso_results_wrapper.Results;
                        double[] values = ResultsIsovistConverter.ToArray(isovistFieldValues, ResultsIsovist.Area);

                        _smartGrid = new GGrid(gridPoints, values, cellSize);
                        DrawLayout();
                        //messageBox.Text += ("New results are drawn! \r\n");
                    }
                    //else
                    //messageBox.Text += ("result: null \r\n");
                }
            }
        }

        private void InfoNoConnection()
        {
            messageBox.Text += ("No Connection to Lucy established yet! Please connect to Lucy first of all! \r\n");
        }
        */

        //==============================================================================================
        //==============================================================================================
        //==============================================================================================
        # region SolarAnalysis

        Evaluation.SolarAnalysis _solarAnalysis = null;

        public void SolarAnalysis()
        {
            _analysisType = AnalysisType.Solar;
            if (_activeLayout != null)
            {
                float cellSize = ControlParameters.IsovistCellSize;
                List<Vector2D> gridPoints = PointsForIsovist(_activeLayout.Border, _activeLayout.Gens, cellSize);

                _solarAnalysis = new Evaluation.SolarAnalysis(gridPoints);

                _solarAnalysis.ClearGeometry();
                _isovistBorder = _borderPoly.Poly.Clone();
                _isovistBorder.Offset(1.2);
                Rect2D borderRect = GeoAlgorithms2D.BoundingBox(_isovistBorder.PointsFirstLoop);
                List<GenBuildingLayout> rects = _activeLayout.Gens;

                // add the buildings
                foreach (GenBuildingLayout curRect in rects)
                {
                    if (!curRect.Open)
                    {
                        Poly2D p2d = curRect.Poly;
                        CGeom3d.Vec_3d[][] pvv = new CGeom3d.Vec_3d[1][];
                        CGeom3d.Vec_3d[] pv = new CGeom3d.Vec_3d[p2d.PointsFirstLoop.Length];
                        pvv[0] = pv;
                        int z = 0;
                        foreach (Vector2D v2d in p2d.PointsFirstLoop)
                            pv[z++] = new CGeom3d.Vec_3d(v2d.X, v2d.Y, curRect.BuildingHeight);

                        CGeom3d.Vec_3d[][] pvv_bottom = p2d.PointsVec3D;
                        _solarAnalysis.AddPolygon(pvv_bottom, 1.0f);
                        _solarAnalysis.AddPolygon(pvv, 1.0f);

                        for (int i = 0; i < pv.Length; ++i)
                        {
                            int j = i + 1;
                            if (j >= pv.Length) j -= pv.Length;

                            CGeom3d.Vec_3d[][] pvv_side = new CGeom3d.Vec_3d[1][];
                            CGeom3d.Vec_3d[] pv_side = new CGeom3d.Vec_3d[4];
                            pvv_side[0] = pv_side;
                            pv_side[0] = pvv_bottom[0][i];
                            pv_side[1] = pvv_bottom[0][j];
                            pv_side[2] = pv[j];
                            pv_side[3] = pv[i];

                            _solarAnalysis.AddPolygon(pvv_side, 1.0f);
                        }
                    }
                }

                _solarAnalysis.Calculate(DirectSunLightNet.CalculationMode.illuminance_klux);

                _smartGrid = new GGrid(_solarAnalysis.GridPoints, _solarAnalysis.Results, _isovistField.CellSize);

                propertyGrid2.SelectedObject = _solarAnalysis;
            }

            DrawLayout();
        }

        private void toolStripButton10_Click(object sender, EventArgs e)
        {
            SolarAnalysis();
        }

        # endregion

        private void nUD_distX_ValueChanged(object sender, EventArgs e)
        {
            //DrawLayoutSOM();
        }




        ///////////////////////////////////////////////////////////////////////
        //// OBJ Loader methods 
        ///////////////////////////////////////////////////////////////////////


        private bool _layout_update = true;
        private LoadResult _obj_load_result;

        private bool _loaded_scene = false;
        private List<Mesh> _mesh_list;
        //private List<Vector3d[]> _meshNormalList;

        private double _scene_min_x, _scene_min_y, _scene_min_z;
        private double _scene_max_x, _scene_max_y, _scene_max_z;

        //============================================================================================================================================================================
        private void openObjFile(string path)
        {
            ObjLoaderFactory objLoaderFactory = new ObjLoaderFactory();
            IObjLoader objLoader = objLoaderFactory.Create(new MaterialNullStreamProvider());
            FileStream fileStream = new FileStream(path, FileMode.Open);
            _obj_load_result = objLoader.Load(fileStream);

            PrintResult(_obj_load_result);

            _borderPoly = GetBounds(_obj_load_result);

            //ViewControl1.camera_3D.SetPosition((float)(_scene_max_x - _scene_min_x) / 2, (float)(_scene_max_z - _scene_min_z) / 2,
            //                             (float)(_scene_max_y - _scene_min_y));

            ViewControl1.AllObjectsList.ObjectsToDraw.Add(_borderPoly);
            ViewControl1.ZoomAll();

            ConvertObjModels(_obj_load_result);
            CalculateNormals();

            _layout_update = true;
            timer1.Enabled = true;
            _loaded_scene = true;
        }

        struct Mesh
        {
            public Vector3d[] vertice;
            public Vector3d normal;
        }

        // TODO: this is written only for visualization
        // to maintain the structure of the OBJ file (Group->Face), create new structure
        private void ConvertObjModels(LoadResult result)
        {
            _mesh_list = new List<Mesh>();

            List<ObjLoader.Loader.Data.VertexData.Vertex> vertices = result.Vertices.ToList();
            foreach (var group in result.Groups)
            {
                foreach (var face in group.Faces)
                {
                    // a point or a line, ignore
                    if (face.Count < 3)
                        continue;
                    else
                    {
                        int first_vertex_index = face[0].VertexIndex;
                        var first_vertex = vertices[first_vertex_index - 1];

                        for (int i = 1; i < face.Count - 1; i++)
                        {
                            int vertex_index = face[i].VertexIndex;
                            var vertex = vertices[vertex_index - 1];

                            int next_vertex_index = face[i + 1].VertexIndex;
                            var next_vertex = vertices[next_vertex_index - 1];

                            Mesh mesh = new Mesh();
                            mesh.vertice = new Vector3d[3];

                            mesh.vertice[0] = new Vector3d(first_vertex.X - _scene_min_x, first_vertex.Y - _scene_min_y, first_vertex.Z - _scene_min_z);
                            mesh.vertice[1] = new Vector3d(vertex.X - _scene_min_x, vertex.Y - _scene_min_y, vertex.Z - _scene_min_z);
                            mesh.vertice[2] = new Vector3d(next_vertex.X - _scene_min_x, next_vertex.Y - _scene_min_y, next_vertex.Z - _scene_min_z);

                            _mesh_list.Add(mesh);
                        }
                    }
                }
            }

            Console.WriteLine(_mesh_list.Count + " mesh faces converted");
        }

        private void CalculateNormals()
        {
            var temp_mesh_list = _mesh_list;
            _mesh_list = new List<Mesh>();

            for (int i = 0; i < temp_mesh_list.Count; i++)
            {
                var mesh = temp_mesh_list[i];

                var a = mesh.vertice[0];
                var b = mesh.vertice[1];
                var c = mesh.vertice[2];

                mesh.normal = Vector3d.Cross(b - a, c - a);
                mesh.normal.Normalize();

                _mesh_list.Add(mesh);
            }
        }


        //TODO: This method draws the models directly, which should be added into the objectstodraw list
        private void DrawObjModels(LoadResult result)
        {
            if (ViewControl1.Camera == ViewControl1.camera_3D)
            {
                // enable lighting for 3D visualization
                //GL.ShadeModel(ShadingModel.Smooth);

                //float[] light_ambient = { 0.7f, 0.7f, 0.7f, 1.0f };
                //float[] light_diffuse = { 0.8f, 0.8f, 0.8f, 1.0f };
                //float[] light_specular = { 1.0f, 1.0f, 1.0f, 1.0f };
                ////float[] light_position = { (float)(_scene_max_x - _scene_min_x), (float)(_scene_max_z - _scene_min_z) * 3, (float)(_scene_max_y - _scene_min_y), 1.0f };
                //float[] light_position = { (float)(_scene_max_x - _scene_min_x), (float)(_scene_max_y - _scene_min_y), (float)(_scene_max_z - _scene_min_z) * 3, 1.0f };


                //GL.Light(LightName.Light0, LightParameter.Position, light_position);

                //GL.Light(LightName.Light0, LightParameter.Ambient, light_ambient);
                //GL.Light(LightName.Light0, LightParameter.Diffuse, light_diffuse);
                //GL.Light(LightName.Light0, LightParameter.Specular, light_specular);
                //GL.LightModel(LightModelParameter.LightModelTwoSide, 0.0f);

                GL.Enable(EnableCap.Light0);
                GL.Enable(EnableCap.Lighting);

                // set up the colors for different properties
                //float[] mat_ambient = { 0.7f, 0.7f, 0.7f, 1.0f };
                //float[] mat_diffuse = { 0.8f, 0.8f, 0.8f, 1.0f };
                float[] mat_ambient = { 1.0f, 1.0f, 1.0f, 1.0f };
                float[] mat_diffuse = { 1.0f, 1.0f, 1.0f, 1.0f };
                GL.Material(MaterialFace.Front, MaterialParameter.Ambient, mat_ambient);
                GL.Material(MaterialFace.Front, MaterialParameter.Diffuse, mat_diffuse);
            }

            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
            GL.Color4(0, 0, 0, 1.0f);

            for (int i = 0; i < _mesh_list.Count; i++)
            {
                var mesh = _mesh_list[i];

                GL.Begin(BeginMode.Triangles);
                GL.Normal3(mesh.normal);
                GL.Vertex3(mesh.vertice[0].X, -mesh.vertice[0].Z, mesh.vertice[0].Y);

                GL.Normal3(mesh.normal);
                GL.Vertex3(mesh.vertice[1].X, -mesh.vertice[1].Z, mesh.vertice[1].Y);

                GL.Normal3(mesh.normal);
                GL.Vertex3(mesh.vertice[2].X, -mesh.vertice[2].Z, mesh.vertice[2].Y);

                GL.End();
            }

            if (ViewControl1.Camera == ViewControl1.camera_3D)
            {
                GL.Disable(EnableCap.Lighting);
                GL.Disable(EnableCap.Light0);
            }

            GL.End();
        }

        private GPolygon GetBounds(LoadResult result)
        {
            Vector2D[] corners = new Vector2D[4];

            corners[0] = new Vector2D(_scene_min_x - _scene_min_x, _scene_min_y - _scene_min_y);
            corners[1] = new Vector2D(_scene_min_x - _scene_min_x, _scene_max_y - _scene_min_y);
            corners[2] = new Vector2D(_scene_max_x - _scene_min_x, _scene_max_y - _scene_min_y);
            corners[3] = new Vector2D(_scene_max_x - _scene_min_x, _scene_min_y - _scene_min_y);

            //Console.WriteLine("min X " + _scene_min_x + " min Y " + _scene_min_y + " max X " + _scene_max_x + " max Y " + maxY + " min Z " + _scene_min_z + " max Z " + maxZ);

            return new GPolygon(corners);
        }

        private static void PrintResult(LoadResult result)
        {
            var sb = new StringBuilder();

            sb.AppendLine("Load result:");
            sb.Append("Vertices: ");
            sb.AppendLine(result.Vertices.Count.ToString(System.Globalization.CultureInfo.InvariantCulture));
            sb.Append("Textures: ");
            sb.AppendLine(result.Textures.Count.ToString(System.Globalization.CultureInfo.InvariantCulture));
            sb.Append("Normals: ");
            sb.AppendLine(result.Normals.Count.ToString(System.Globalization.CultureInfo.InvariantCulture));
            sb.AppendLine();
            sb.AppendLine("Groups: ");

            foreach (var loaderGroup in result.Groups)
            {
                sb.AppendLine(loaderGroup.Name);
                sb.Append("Faces: ");
                sb.AppendLine(loaderGroup.Faces.Count.ToString(System.Globalization.CultureInfo.InvariantCulture));
            }

            Console.WriteLine(sb);
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // --- Render test ---
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //private static void RenderToFramebuffer()
        //{

        //    const int FboWidth = 512;
        //    const int FboHeight = 512;

        //    uint FboHandle;
        //    uint ColorTexture;
        //    uint DepthRenderbuffer;

        //    // Create Color Texture
        //    GL.GenTextures(1, out ColorTexture);
        //    GL.BindTexture(TextureTarget.Texture2D, ColorTexture);
        //    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
        //    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
        //    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Clamp);
        //    GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Clamp);
        //    GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba8, FboWidth, FboHeight, 0, PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero);

        //    // test for GL Error here (might be unsupported format)

        //    GL.BindTexture(TextureTarget.Texture2D, 0); // prevent feedback, reading and writing to the same image is a bad idea

        //    // Create Depth Renderbuffer
        //    GL.Ext.GenRenderbuffers(1, out DepthRenderbuffer);
        //    GL.Ext.BindRenderbuffer(RenderbufferTarget.RenderbufferExt, DepthRenderbuffer);
        //    GL.Ext.RenderbufferStorage(RenderbufferTarget.RenderbufferExt, (RenderbufferStorage)All.DepthComponent32, FboWidth, FboHeight);

        //    // test for GL Error here (might be unsupported format)

        //    // Create a FBO and attach the textures
        //    GL.Ext.GenFramebuffers(1, out FboHandle);
        //    GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, FboHandle);
        //    GL.Ext.FramebufferTexture2D(FramebufferTarget.FramebufferExt, FramebufferAttachment.ColorAttachment0Ext, TextureTarget.Texture2D, ColorTexture, 0);
        //    GL.Ext.FramebufferRenderbuffer(FramebufferTarget.FramebufferExt, FramebufferAttachment.DepthAttachmentExt, RenderbufferTarget.RenderbufferExt, DepthRenderbuffer);

        //    // now GL.Ext.CheckFramebufferStatus( FramebufferTarget.FramebufferExt ) can be called, check the end of this page for a snippet.

        //    // since there's only 1 Color buffer attached this is not explicitly required
        //    GL.DrawBuffer((DrawBufferMode)FramebufferAttachment.ColorAttachment0Ext);

        //    GL.PushAttrib(AttribMask.ViewportBit); // stores GL.Viewport() parameters
        //    GL.Viewport(0, 0, FboWidth, FboHeight);

        //    // render whatever your heart desires, when done ...
        //    GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
        //    GL.Color4(FillColor);
        //    GL.Begin(BeginMode.Quads);
        //    // GL.Rect(RectD.TopLeft.X, RectD.TopLeft.Y, RectD.BottomRight.X, RectD.BottomRight.Y);
        //    GL.Vertex3(25, 225, 0);
        //    GL.Vertex3(175, 220, 0);
        //    GL.Vertex3(170, 55, 0);
        //    GL.Vertex3(25, 60, 0);
        //    GL.End();



        //    GL.PopAttrib(); // restores GL.Viewport() parameters
        //    GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0); // return to visible framebuffer
        //    GL.DrawBuffer(DrawBufferMode.Back);


        //    // ------------
        //    Bitmap bmp = new Bitmap(FboWidth, FboHeight);
        //    System.Drawing.Imaging.BitmapData data =
        //        bmp.LockBits(glControl.ClientRectangle, System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
        //    //bmp.SetResolution(1200.0F, 1200.0F);

        //    GL.ReadPixels(0, 0, glControl.Width, glControl.Height, PixelFormat.Bgr, PixelType.UnsignedByte, data.Scan0);
        //    bmp.UnlockBits(data);

        //    //bmp.SetResolution(300, 300);
        //    bmp.RotateFlip(RotateFlipType.RotateNoneFlipY);


        //}
    }
}


﻿namespace CPlan.UrbanPattern
{
    partial class FormAppSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.propertyGrid_AppSettings = new System.Windows.Forms.PropertyGrid();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // propertyGrid_AppSettings
            // 
            this.propertyGrid_AppSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid_AppSettings.Location = new System.Drawing.Point(0, 0);
            this.propertyGrid_AppSettings.Name = "propertyGrid_AppSettings";
            this.propertyGrid_AppSettings.Size = new System.Drawing.Size(454, 561);
            this.propertyGrid_AppSettings.TabIndex = 18;
            this.propertyGrid_AppSettings.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid_AppSettings_PropertyValueChanged);
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.button1.Location = new System.Drawing.Point(0, 538);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(454, 23);
            this.button1.TabIndex = 19;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormAppSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 561);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.propertyGrid_AppSettings);
            this.Name = "FormAppSettings";
            this.Text = "AppSettings";
            this.Load += new System.EventHandler(this.FormAppSettings_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PropertyGrid propertyGrid_AppSettings;
        private System.Windows.Forms.Button button1;
    }
}
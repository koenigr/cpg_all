﻿// ---------------------------------------------------------------------------------------------------- 
//  CPlan Synthesis Library
//  Computational Planning Group 
//  Copyright file = FormAppSettings.cs 
//  Copyright (C) 2014/10/18  12:07 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
//  The library is published under the LGPL v3 licence.
//  You should have received a copy of LGPL v3 license with this file. 
//  If not, please visit: http://www.gnu.org/licenses/lgpl.html
// ---------------------------------------------------------------------------------------------------- 
// 

using System;
using System.Reflection;
using System.Windows.Forms;
using CPlan.UrbanPattern.Properties;

namespace CPlan.UrbanPattern
{
    public partial class FormAppSettings : Form
    {
        public FormAppSettings()
        {
            InitializeComponent();
        }

        private void FormAppSettings_Load(object sender, EventArgs e)
        {
            propertyGrid_AppSettings.SelectedObject = Settings.Default;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void propertyGrid_AppSettings_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            object obj;
            if (e.ChangedItem.Parent.Value != null) obj = e.ChangedItem.Parent.Value;
            else obj = propertyGrid_AppSettings.SelectedObject;

            //nun wird geprüft, ob es sich bei dem geänderten Wert um ein Property handelte!
            if (e.ChangedItem.GridItemType == GridItemType.Property)
            {
                PropertyInfo pInfo = obj.GetType().GetProperty(e.ChangedItem.PropertyDescriptor.Name);
                pInfo.SetValue(obj, e.ChangedItem.Value, null);
                pInfo.SetValue(Settings.Default, e.ChangedItem.Value, null);
            }
        }


    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = StreetPattern.cs 
//  Copyright (C) 2014/10/18  7:52 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Forms;
using CPlan.Evaluation;
using CPlan.Geometry;
using CPlan.Optimization;
using QuickGraph;
using Tektosyne;
using Tektosyne.Collections;
using Tektosyne.Geometry;
using OpenTK;


namespace CPlan.UrbanPattern
{

    [Serializable]
    public class StreetPattern
    {
        # region Fields

        private int _maxEdges = ControlParameters.MaxConnectivity;
        private double _minDist = ControlParameters.MinDistance;
        private double _minAngle = 80;
        private bool _rotSymmetryL = false;
        private bool _rotSymmetryR = false;
        private Poly2D _border;
        private Poly2D _outerBorder;
        private Dictionary<PointD, DataNode> _refNodes;

        # endregion

        # region Properties

        public Subdivision Graph
        {
            get;
            set;
        }

        public UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> QGraph
        {
            get;
            set;
        }        

        # endregion

        //============================================================================================================================================================================
        public StreetPattern(int mE, int rA, double mD, double mA, int maxL, int minL, bool rSL, bool rSR, Poly2D bord) 
        {  
            _maxEdges = mE;
            _minDist = mD;
            _minAngle = mA;
            _rotSymmetryL = rSL;
            _rotSymmetryR = rSR;
            if (bord != null)
            {
                _border = bord;
                _outerBorder = _border.Clone();
                _outerBorder.Offset(1);
            }
            Graph = new Subdivision();
            _refNodes = new Dictionary<PointD, DataNode>();
        }

        //============================================================================================================================================================================
        public StreetPattern(Poly2D bord)
        {
            //_maxEdges = ControlParameters.MaxConnectivity;
            //_minDist = ControlParameters.MinDistance;
            //_minAngle = 20;
            if (bord != null)
            {
                _border = bord;
                _outerBorder = _border.Clone();
                _outerBorder.Offset(1);
            }
            Graph = new Subdivision();
            _refNodes = new Dictionary<PointD, DataNode>();
        }

        //============================================================================================================================================================================
        public void SubdivisionToQGraph()
        {
            QGraph = new UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>();
            LineD[] edges = Graph.ToLines();
            for (int n = 0; n < edges.Count(); n++){
                UndirectedEdge<Vector2d> newEdge = new UndirectedEdge<Vector2d>(edges[n].Start.ToVector2D(), edges[n].End.ToVector2D());
                QGraph.AddVerticesAndEdge(newEdge); 
            }
            //UndirectedEdge<Vector2d> newEdge = new UndirectedEdge<Vector2d>(parentPt.ToVector2D(), newPt.ToVector2D());
            //UndirectedEdge<Vector2d> reverseNewEdge = qGraph.Edges.ToList().Find(x => x.Source == newPt.ToVector2D() && x.Target == parentPt.ToVector2D());
            // --- a new edge (& vertex) is added ---                       
            //if (qGraph.ContainsVertex(newPt.ToVector2D()) && qGraph.ContainsVertex(parentPt.ToVector2D()))
            //{
            //    if (reverseNewEdge == null)
            //      qGraph.AddEdge(newEdge);   
            //}
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Grow a StreetNetwork from the InstructionTree, resp. from the List of InstructionNodes
        /// </summary>
        /// <param name="InstructionNodes">List of InstructionNodes from a InstructionTree. </param>
        /// <returns></returns>
        public void GrowStreetPatternNew(InstructionTreePisa instructTree,
            UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> iniStreets) //Subdivision GrowStreetPattern()
        {
            // the new graph qGraph...---------------------------------------
            UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> qGraph =
                new UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>>(false);

            // find all InstructionNodes whose parent indes = 0. This means they are the seed Nodes to create the seed Edges.
            foreach (InstructionNode node in instructTree.Nodes)
            {
                node.Closed = false;
            }

            double orgMinDist = _minDist;
            _minDist = 1;
            // --- ATTENTION! The border edges are not tested for intersection! --- need to be defined correctly by the user.
            if (_border != null)
            {
                foreach (Line2D borderEdge in _border.Edges)
                {
                    UndirectedEdge<Vector2d> qBordEdge = new UndirectedEdge<Vector2d>(borderEdge.Start, borderEdge.End);
                    //qGraph.AddVerticesAndEdge(qBordEdge);
                    AddGraphElement(borderEdge.Start, borderEdge.End, null, qGraph);
                }
            }

            foreach (UndirectedEdge<Vector2d> qIniEdge in iniStreets.Edges)
            {
                //qGraph.AddVerticesAndEdge(qIniEdge);
                int nrElements = qGraph.EdgeCount;
                AddGraphElement(qIniEdge.Source, qIniEdge.Target, null, qGraph);
                if (nrElements == qGraph.EdgeCount)
                    AddGraphElement(qIniEdge.Target, qIniEdge.Source, null, qGraph);
            }

            _minDist = orgMinDist;

            // -- add all other edges from the instructionTree --
            for (int n = 0; n < instructTree.Nodes.Count; n++)
            {
                bool addEdge = true;
                bool splitEdge = false;
                Line2D toSplitEdge = null;
                InstructionNode curInstructNode = instructTree.Nodes[n];
                InstructionNode parentInstructNode = instructTree.Nodes.Find(InstructionNode => InstructionNode.Index == curInstructNode.ParentIndex); // curInstructNode.Parent;//
                if (curInstructNode.ParentIndex >= 0 && parentInstructNode == null)
                    continue; // if program arrives here, there is a problem with crossover...
                //----------------------------------------------------------------------------------------------------------------------------------
                //IF YOUR Parent WAS CLOSED IN THE INSTRUCTION TREE YOU ARE CLOSDED TOO
                if (curInstructNode.ParentIndex >= 0 && parentInstructNode.Closed == true)// if (curInstructNode.Parent != null && parentInstructNode.Closed == true)
                {
                    curInstructNode.Closed = true;
                    //ADDED DELETING PART OF THE CLOSED NODE
                    // --> possible to cut the remaining branch to make the process faster in the next generations <--

                    //List<InstructionNode> deleteNodes = new List<InstructionNode>();
                    //int rootIdx = instructTree.Nodes.IndexOf(curInstructNode);
                    //deleteNodes = instructTree.FindBranch(deleteNodes, rootIdx);
                    //if (deleteNodes.Count > 30)
                    //{
                    //    deleteNodes.RemoveRange(0, 30);
                    //    foreach (InstructionNode delNode in deleteNodes) instructTree.Nodes.Remove(delNode);
                    //}
                } // --- don't consider the global root node and the seed nodes ---------------

                //TESTING IF WE CAN ADD THE NEW NODE TO THE NETWORK
                else if (curInstructNode.ParentIndex >= 0 && parentInstructNode.Closed == false)
                {

                    Vector2d parentPt = parentInstructNode.Position; //LOOKING FOR PARRENT
                    // test qGraph---------------------------
                    int qNrNeighb = 0;
                    if (qGraph.ContainsVertex(parentPt))
                    {
                        qNrNeighb = qGraph.AdjacentDegree(parentPt);
                    }
                    // ---------------------------------------

                    if (qNrNeighb >= parentInstructNode.MaxConnectivity) //TEST IF IS POSSIBLE TO ADD NEW ARM TO THE PARRENT
                    {
                        //addEdge = false;
                        parentInstructNode.Closed = true;
                        curInstructNode.Closed = true;
                        continue; //LEAVE THIS NODE, CONTINUE ON NEXT INSTRUCTION NODE
                    }

                    // ---------------------------------------------------------------------------------------------------------------------
                    //CALCULATIONS OF NEW ANGLES DEPENDING ON HOW MANY CONNECTION PARRANT NODE HAS
                    double alpha = 0, rotAngle = 0;
                    double dist = 0;
                    List<Double> AngleList = new List<Double>();     
                    Vector2d parentsParentPt = new Vector2d();
                    if (parentInstructNode.ParentIndex == -1) // only for the first inital nodes
                    {
                        List<Vector2d> parentNeighbours = qGraph.GetNeighbors(parentPt);
                        if (parentNeighbours.Count == 0)   // the program should never arrive here
                            continue;                   // open issue to be solved!
                        parentsParentPt = parentNeighbours[0];
                    }
                    else
                    {
                        InstructionNode parentParentInstructNode = instructTree.Nodes.Find(InstructionNode => InstructionNode.Index == parentInstructNode.ParentIndex);
                        parentsParentPt = parentParentInstructNode.Position;
                    }

                    List<Vector2d> parentNeighList = qGraph.GetNeighbors(parentPt);
                    int nrParentNeighb = parentNeighList.Count;
                    if (nrParentNeighb == 1)
                    {
                        rotAngle = 180;
                    }
                    else
                    {
                        for (int i = 1; i < nrParentNeighb; i++)
                        {
                            Vector2d curNeighb = parentNeighList[i];
                            double temAngle = GeoAlgorithms2D.AngleBetween(parentsParentPt, curNeighb, parentPt); //selNode.Point.AngleBetween(refPt, curNeighb);
                            temAngle = Angle.RadiansToDegrees * temAngle;
                            if (temAngle < 0) 
                                temAngle = 360 + temAngle;
                            AngleList.Add(temAngle);
                            if (temAngle > alpha)
                            {
                                alpha = temAngle;
                            }
                        }
                        AngleList.Add(0);
                        AngleList.Add(360);
                        AngleList.Sort((y, x) => y.CompareTo(x));

                        double maxAngle = AngleList[1];
                        int inSection = 0;
                        for (int i = 2; i < AngleList.Count; i++)
                        {
                            double delta = AngleList[i] - AngleList[i - 1];
                            if (maxAngle < delta)
                            {
                                maxAngle = delta;
                                inSection = i - 1;
                            }
                        }
                        if (maxAngle < _minAngle) addEdge = false; // only add a edge, if the angle to devide is large enough!
                        if (maxAngle >= 260) // allow also 90° steps for angles larger than 260°
                        {
                            int divNr = Convert.ToInt16(Math.Ceiling(maxAngle / 90) - 0);
                            if (curInstructNode.Divider == -1) curInstructNode.Divider = RndGlobal.Rnd.Next(divNr);
                            maxAngle = 90 + curInstructNode.Divider * 90; // 180;//
                        }
                        rotAngle = maxAngle / 2 + AngleList[inSection];

                    }

                    if (addEdge) // --- check if the requirements to create a new segment are fulfilled ----------------------
                    {
                        Vector2d newPt = GeoAlgorithms2D.RotatePoint(parentPt, parentsParentPt, GeoAlgorithms2D.DegreeToRadian(rotAngle + curInstructNode.Angle));
                        newPt = parentPt.Move(newPt, curInstructNode.Length);

                        // --- add new element to the graph --
                        AddGraphElement(newPt, parentPt, curInstructNode, qGraph);

                    }
                    else { curInstructNode.Closed = true; } // no further growth at this node
                        
                }
            }
            // --- construct subdivision graph ---
            QGraph = qGraph;


            // ------------------------------------------------------------------------------------------------------
            // """""""""""""" section only for testing """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
            // ------------------------------------------------------------------------------------------------------
            List<UndirectedEdge<Vector2d>> edges = qGraph.Edges.ToList();
            //List<LineD> linesList = new List<LineD>();
            List<Line2D> testList = new List<Line2D>();
            foreach (UndirectedEdge<Vector2d> curEdge in edges)
            {
                testList.Add(new Line2D(curEdge.Source, curEdge.Target));
            }
 
            for (int i = 0; i < edges.Count; i++) 
            {
                UndirectedEdge<Vector2d> testEdge = edges[i];
                // -- interxection test -- 
                bool somethingIsWrong = false;
                Line2D testLine = new Line2D(testEdge.Source, testEdge.Target);
                List<Vector2d> intersectPts = GeoAlgorithms2D.IntersectLines(testLine, testList.ToArray());
                if (intersectPts.Count != 0)
                {
                    // if we enter here, there are unsolved intersections - needs to be changed above.
                    somethingIsWrong = true;
                }

                if (!qGraph.ContainsVertex(testEdge.Source) | !qGraph.ContainsVertex(testEdge.Target))
                {
                    // if we enter here, the points of the edge are not added to the graph - needs to be changed above.
                    somethingIsWrong = true;
                }

                UndirectedEdge<Vector2d> reverseTestEdge = qGraph.Edges.ToList().Find(x => x.Source == testEdge.Target && x.Target == testEdge.Source);

                if (reverseTestEdge != null)
                {
                    // if we enter here, there is a double edge in the graph - needs to be changed above.
                    somethingIsWrong = true;
                }

                if (somethingIsWrong)
                    continue;
            }
            // """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        }


        //============================================================================================================================================================================
        /// <summary>
        /// Add new elements to an existing graph and test interaction with the existing elements.
        /// </summary>
        /// <param name="newPt"></param>
        /// <param name="parentPt"></param>
        /// <param name="curInstructNode"></param>
        /// <param name="curGraph"></param>
        private void AddGraphElement(Vector2d newPt, Vector2d parentPt, InstructionNode curInstructNode, UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> curGraph )
        {
            bool addEdge = true;
            bool splitEdge = false;
            Line2D toSplitEdge = null;
            double dist;
            Vector2d newPtff, newPtff1;
            bool intersect;

            // --- test, if the parentPt is contained in the graph (may not be the case for the initial lines and the border)
            if (!curGraph.ContainsVertex(parentPt))
            {
                // --- check the distance of the parentPt to an existing edge -------------------------------------------------
                UndirectedEdge<Vector2d>[] qEdges1 = curGraph.Edges.ToArray();
                List<Line2D> qLines1 = new List<Line2D>();
                foreach (UndirectedEdge<Vector2d> edge in qEdges1) qLines1.Add(new Line2D(edge.Source, edge.Target));
                Line2D qNearEdge1 = GeoAlgorithms2D.NearestLineToPoint(qLines1.ToArray(), parentPt, out dist);
                if (dist < _minDist) // -- if the distance is small enough, connect the point to the existing edge
                {
                    if (qNearEdge1 != null)
                    {
                        Vector2d qNewPt = qNearEdge1.Intersect(parentPt);
                        parentPt = qNewPt;
                        splitEdge = true; // the edge with the new point on it needs to be splitted
                        toSplitEdge = qNearEdge1;
                        // --- test if the movement of the new point cause an intersection ---
                        Line2D tmpToSplitEdge = null;
                        bool tmpSplitEdge = false;
                        Line2D tmpqNewLine = new Line2D(parentPt, newPt);                      
                        intersect = IntersectionTest(curGraph, tmpqNewLine, out tmpToSplitEdge, out tmpSplitEdge, out newPtff1);
                        if (intersect)
                            newPt = newPtff1;
                        if (tmpSplitEdge)
                            toSplitEdge = tmpToSplitEdge;
                    }
                }
                curGraph.AddVertex(parentPt);
            }

            // ============================ candidata position defined - now test interaction with existing graph ======================================================
            // ---------------------------------------------------------------------------------------------------------
            // --- check if there is an intersection -------------------------------------------------------------------
            // ---------------------------------------------------------------------------------------------------------
            Line2D qNewLine = new Line2D(parentPt, newPt);
            intersect = IntersectionTest(curGraph, qNewLine, out toSplitEdge, out splitEdge, out newPtff);
            if (intersect)
                newPt = newPtff;

            // ---------------------------------------------------------------------------------------------------------
            // --- check the distance of the newPt to an existing edge -------------------------------------------------
            // ---------------------------------------------------------------------------------------------------------
            UndirectedEdge<Vector2d>[] qEdges = curGraph.Edges.ToArray();
            List<Line2D> qLines = new List<Line2D>();
            foreach (UndirectedEdge<Vector2d> edge in qEdges) qLines.Add(new Line2D(edge.Source, edge.Target));

            bool done = false;
            int cter = 0;

            while (!done & cter <= 5)
            {
                Line2D qNearEdge = GeoAlgorithms2D.NearestLineToPoint(qLines.ToArray(), newPt, out dist);
                if (dist > 0 && dist < _minDist)
                    // -- if the distance is small enough, connect the point to the existing edge
                {
                    if (qNearEdge != null)
                    {
                        Vector2d qNewPt = qNearEdge.Intersect(newPt);
                        if (_outerBorder.ContainsPoint(qNewPt))
                            newPt = qNewPt;
                        else done = true;  
                        splitEdge = true; // the edge with the new point on it needs to be splitted
                        toSplitEdge = qNearEdge;
                        // --- test if the movement of the new point cause an intersection ---
                        Line2D tmpToSplitEdge = null;
                        bool tmpSplitEdge = false;
                        Line2D tmpqNewLine = new Line2D(parentPt, newPt);
                        intersect = IntersectionTest(curGraph, tmpqNewLine, out tmpToSplitEdge, out tmpSplitEdge, out newPtff);
                        if (intersect)
                            if (_outerBorder.ContainsPoint(newPtff))
                                newPt = newPtff;
                            else done = true;
                        if (tmpSplitEdge)
                            toSplitEdge = tmpToSplitEdge;
                    }
                }
                else 
                    done = true;

                cter ++;
            }

            //else
            // ----------------------------------------------------------------------------------------------------    
            // --- check the distance of the new line to existing points ------------------------------------------
            // ----------------------------------------------------------------------------------------------------
            {
                done = false;
                cter = 0;
                while (!done & cter <= 5)
                {
                    Vector2d qNearPoint = GeoAlgorithms2D.NearestPointToLine(curGraph.Vertices.ToArray(), qNewLine,
                        parentPt, out dist);

                    if (dist > 0 && dist < _minDist) // -- if the distance is small enough, end the line at the near point
                    {
                        if (qNearPoint != null)
                        {
                            if (_outerBorder.ContainsPoint(qNearPoint))
                                newPt = qNearPoint; // end the line at the point close to the line...
                            else done = true;                      
                            splitEdge = false; // therfore no splitting is nessesary 
                            // --- test if the movement of the new point cause an intersection ---
                            qNewLine = new Line2D(parentPt, newPt);
                            intersect = IntersectionTest(curGraph, qNewLine, out toSplitEdge, out splitEdge, out newPtff);
                            if (intersect)
                                if (_outerBorder.ContainsPoint(newPtff))
                                    newPt = newPtff;
                                else done = true;
                        }
                    }
                    else 
                        done = true;

                    cter ++;
                }

            }

            // ---------------------------------------------------------------------------------------------------------
            // --- find the nearest vertex of the graph ----------------------------------------------------------------
            // ---------------------------------------------------------------------------------------------------------
            done = false;
            cter = 0;
            while (!done & cter <= 5)
            {
                Vector2d nearestNeighbour = GeoAlgorithms2D.NearestNeighbour(curGraph.Vertices.ToArray(), newPt);
                if (nearestNeighbour != null) // if nearestNeighbour == null, there is a unknown error...
                {                      
                    dist = GeoAlgorithms2D.DistancePoints(nearestNeighbour, newPt);
                    if (dist > 0 && dist < _minDist) // if the distance is small enough, connect both points
                    {
                        //--- check connectivity of the considered node first
                        //int tester = qGraph.AdjacentDegree(nearestNeighbour);
                        if (nearestNeighbour != null)
                        {
                            if (curGraph.AdjacentDegree(nearestNeighbour) >= _maxEdges || nearestNeighbour == parentPt)
                            {
                                addEdge = false;
                            }
                            if (_outerBorder.ContainsPoint(nearestNeighbour))
                                newPt = nearestNeighbour;
                            else done = true;
                            splitEdge = false; // no splitting is nessesary anymore
                            // --- test if the movement of the new point cause an intersection ---
                            qNewLine = new Line2D(parentPt, newPt);
                            intersect = IntersectionTest(curGraph, qNewLine, out toSplitEdge, out splitEdge, out newPtff);
                            if (intersect)
                                if (_outerBorder.ContainsPoint(newPtff))
                                    newPt = newPtff;
                                else done = true;
                        }
                    }
                    else
                        done = true;

                    cter++;
                }
                
            }

            // ============================ all tests are done - now add a new edge and/or split an existing edge ======================================================
            // --- so far, the candidate point (newPt) is found ----------------------------------------------------
            // --- add the new street segment to the graph and test, if the new point is added to the graph
            if (addEdge | splitEdge) // --- check if the requirements to create a new segment are fulfilled
            {
                bool outOfBorder = false;
                if (_outerBorder != null)
                {
                    if (!_outerBorder.ContainsPoint(newPt))
                    {
                        outOfBorder = true;
                        // -- test intersection with border --
                        for (int i = 0; i < _border.Edges.Count; i++)
                        {
                            Line2D L1 = _border.Edges[i];
                            //bool intersect = false;
                            Vector2d interPt = new Vector2d();
                            int tester = GeoAlgorithms2D.LineIntersection(L1.Start, L1.End, parentPt, newPt, ref interPt);
                            if (tester == 2) // intersection 
                            {
                                outOfBorder = false;
                                newPt = interPt;
                            }
                        }
                        if (curInstructNode != null) curInstructNode.Closed = true;
                    }
                }

                if (!outOfBorder)
                {
                    UndirectedEdge<Vector2d> newEdge = new UndirectedEdge<Vector2d>(parentPt, newPt);
                    UndirectedEdge<Vector2d> reverseNewEdge = curGraph.Edges.ToList().Find(x => x.Source == newPt && x.Target == parentPt);
                    // --- a new edge (& vertex) is added ---                       
                    if (curGraph.ContainsVertex(newPt) && curGraph.ContainsVertex(parentPt))
                    {
                        if (reverseNewEdge == null)
                            curGraph.AddEdge(newEdge);
                    }
                    else
                    {
                        if (reverseNewEdge == null)
                            curGraph.AddVerticesAndEdge(newEdge);

                        // --- test, if we need to split an existing edge ---
                        if (splitEdge)
                        {
                            Vector2d existVertiA = toSplitEdge.Start;
                            Vector2d existVertiB = toSplitEdge.End;
                            UndirectedEdge<Vector2d> newEdgeA = new UndirectedEdge<Vector2d>(existVertiA, newPt);
                            UndirectedEdge<Vector2d> revNewEdgeA = curGraph.Edges.ToList().Find(x => x.Source == newPt && x.Target == existVertiA);
                            UndirectedEdge<Vector2d> newEdgeB = new UndirectedEdge<Vector2d>(existVertiB, newPt);
                            UndirectedEdge<Vector2d> revNewEdgeB = curGraph.Edges.ToList().Find(x => x.Source == newPt && x.Target == existVertiB);
                            if (revNewEdgeA == null)
                                curGraph.AddEdge(newEdgeA);
                            if (revNewEdgeB == null)
                                curGraph.AddEdge(newEdgeB);

                            if (curGraph.ContainsVertex(toSplitEdge.Start) &&
                                curGraph.ContainsVertex(toSplitEdge.End))
                            {
                                UndirectedEdge<Vector2d> removeEdge = curGraph.Edges.ToList().Find(x => x.Source == toSplitEdge.Start && x.Target == toSplitEdge.End);
                                if (curGraph.ContainsEdge(removeEdge))
                                    curGraph.RemoveEdge(removeEdge);
                                removeEdge = curGraph.Edges.ToList().Find(x => x.Source == toSplitEdge.End && x.Target == toSplitEdge.Start);
                                if (removeEdge != null)
                                    if (curGraph.ContainsEdge(removeEdge))
                                        curGraph.RemoveEdge(removeEdge);
                            }
                        }
                    }
                    if (curInstructNode != null)
                        curInstructNode.Position = newPt; // add the new vertex-position to the corresponding instruction node
                }
                else { if (curInstructNode != null) curInstructNode.Closed = true; }
            }
            else { if (curInstructNode != null) curInstructNode.Closed = true; } // no further growth at this node           
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Test if there is an intersection of a new line with an existing graph.
        /// </summary>
        /// <param name="qGraph">Existing graph.</param>
        /// <param name="testLine">New line for testing.</param>
        /// <param name="toSplitEdge">Intersection edge of the graph if an intersection is found.</param>
        /// <param name="splitEdge">Indicates if an edge needs to be splitted.</param>
        /// <param name="intersectionPt">The intersection point.</param>
        /// <returns>The intersecting point of the new line with the graph.
        /// The function returns the nearest intersection point to the line origin it there are more intersection points.</returns>
        public bool IntersectionTest(UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> qGraph, Line2D testLine, out Line2D toSplitEdge, out bool splitEdge, out Vector2d intersectionPt)
        {
            // --- optimize the intersection test by using a grit to which the lines are registered...
            bool intersectionFound = false;
            toSplitEdge = null;
            splitEdge = false;
            intersectionPt = new Vector2d();  
            UndirectedEdge<Vector2d>[] qEdges = qGraph.Edges.ToArray();
            List<Line2D> qLines = new List<Line2D>();
            foreach (UndirectedEdge<Vector2d> edge in qEdges) qLines.Add(new Line2D(edge.Source, edge.Target));
            //Line2D qNewLine = new Line2D(parentPt.ToVector2D(), newPt.ToVector2D());
            List<Line2D> intersectLines = new List<Line2D>();
            List<Vector2d> intersectPts = GeoAlgorithms2D.IntersectLines(testLine, qLines.ToArray(), intersectLines);
            // --- find the intersection point closes to the parent point ---
            double minDistc = double.MaxValue;
            Vector2d nearestInterPt = new Vector2d();
            if (intersectPts.Count > 1)
            {             
                //foreach (Vector2d nPt in intersectPts)
                for (int i = 0; i < intersectPts.Count; i++)
                {
                    Vector2d nPt = intersectPts[i];
                    double nDist = GeoAlgorithms2D.DistancePoints(testLine.Start, nPt);
                    if (minDistc > nDist)
                    {
                        minDistc = nDist;
                        nearestInterPt = nPt;
                        toSplitEdge = intersectLines[i];
                    }
                }
                intersectionFound = true;
                splitEdge = true; // the intersecting edge needs to be splitted
                intersectionPt = nearestInterPt; // use the intersection point as new point 
            }
            else if (intersectPts.Count == 1)
            {
                nearestInterPt = intersectPts[0];
                toSplitEdge = intersectLines[0];
                intersectionFound = true;
                splitEdge = true; // the intersecting edge needs to be splitted
                intersectionPt = nearestInterPt; // use the intersection point as new point 
            }

            return intersectionFound;
        }

        //============================================================================================================================================================================
        //======================================= old part with Tektosyne Subdivision class ==========================================================================================
        //============================================================================================================================================================================
        /// <summary>
        /// Grow a StreetNetwork from the InstructionTree, resp. from the List of InstructionNodes
        /// </summary>
        /// <param name="instructTree">The InstructionTree. </param>
        /// <param name="iniStreets">The initial setreet segments. </param>
        public void GrowStreetPatternSubdiv(InstructionTreePisa instructTree, Subdivision iniStreets) //Subdivision GrowStreetPattern()
        {
           
            // find all InstructionNodes whose parent indes = 0. This means they are the seed Nodes to create the seed Edges.
            //List<InstructionNode> initialNodes = instructTree.Nodes.FindAll(InstructionNode => InstructionNode.SeedNode == true);
            foreach (InstructionNode node in instructTree.Nodes)
            {
                node.Closed = false;
            }
            
            List<LineD> iniEdges = iniStreets.ToLines().ToList();
            //Graph = (Subdivision)iniStreets.Clone();
            foreach (LineD edge in iniEdges)
            {
                AddEdge(edge.Start, edge.End);
            }

            // create the first seed edge(s)
            //for (int i = 0; i < initialNodes.Count; i += 2)
            //{
            //    PointD iniPt1 = initialNodes[i].Position; //GeoAlgorithms2D.PolarToCartesian(initialNodes[i].Angle, initialNodes[i].Length);
            //    PointD iniPt2 = initialNodes[i + 1].Position; //GeoAlgorithms2D.PolarToCartesian(initialNodes[i + 1].Angle, initialNodes[i + 1].Length);
            //    AddEdge(iniPt1, iniPt2);
            //}

            // add all other edges from the instructionTree
            for (int n = 0; n < instructTree.Nodes.Count; n++)
            {
                bool isAdded = false;
                bool addEdge = true;
                InstructionNode curInstructNode = instructTree.Nodes[n];
                //InstructionNode parentInstructNode = instructTree.Nodes.Find(InstructionNode => InstructionNode.Index == curInstructNode.ParentIndex);
                InstructionNode parentInstructNode = instructTree.Nodes.Find(InstructionNode => InstructionNode.Index == curInstructNode.ParentIndex); // curInstructNode.Parent;//
                if (curInstructNode.ParentIndex >= 0 && parentInstructNode == null)
                    continue; // if program arrives here, there is a problem with crossover...
                //----------------------------------------------------------------------------------------------------------------------------------
                //IF YOUR Parent WAS CLOSED IN THE INSTRUCTION TREE YOU ARE CLOSDED TOO
                if (curInstructNode.ParentIndex >= 0 && parentInstructNode.Closed == true)// if (curInstructNode.Parent != null && parentInstructNode.Closed == true)
                {
                    curInstructNode.Closed = true;
                    //ADDED DELETING PART OF THE CLOSED NODE
                    // --> possible to cut the remaining branch to make the process faster in the next generations <--
                    List<InstructionNode> deleteNodes = new List<InstructionNode>();
                    int rootIdx = instructTree.Nodes.IndexOf(curInstructNode);
                    deleteNodes = instructTree.FindBranch(deleteNodes, rootIdx);
                    if (deleteNodes.Count > 30)
                    {
                        deleteNodes.RemoveRange(0, 30);
                        foreach (InstructionNode delNode in deleteNodes) instructTree.Nodes.Remove(delNode);
                    }
                } // --- don't consider the global root node and the seed nodes ---------------

              //TESTING IF WE CAN ADD THE NEW NODE TO THE NETWORK
                //else if (curInstructNode.Parent != null && !curInstructNode.SeedNode && parentInstructNode.Closed == false) 
                else if (curInstructNode.ParentIndex >= 0 && parentInstructNode.Closed == false)
                {
                    
                    PointD parentPt = parentInstructNode.Position.ToPointD(); //LOOKING FOR PARRENT
                    int nrNeighb = Graph.GetNeighbors(parentPt).Count; //HOW MANY ARMS PARRENT NODE HAVE
                    
                    if (nrNeighb >= parentInstructNode.MaxConnectivity) //TEST IF IS POSSIBLE TO ADD NEW ARM TO THE PARRENT
                    //if (nrNeighb >= _maxEdges)
                    {
                        addEdge = false;
                        parentInstructNode.Closed = true;
                        curInstructNode.Closed = true;
                        continue; //LEAVE THIS NODE, CONTINUE ON NEXT INSTRUCTION NODE
                    }


                    // --- create a more comfortable temporary node list from the current network graph --------------------------------------TEMPORARY NODE LIST
                    IList<PointD> neighb;
                    List<GNode> Nodes = new List<GNode>();
                    List<PointD> vertices = Graph.Nodes.ToList();
                    for (int i = 0; i < vertices.Count; i++)
                    {
                        Nodes.Add(new GNode(vertices[i], true));
                        neighb = Graph.GetNeighbors(Nodes[i].Point);
                        Nodes[i].Connectivity = neighb.Count;
                        Nodes[i].Neighbours = neighb;
                    }

                    // --- X ---------------------------------------------------------------------------------------------------------------
                    // ---------------------------------------------------------------------------------------------------------------------
                    //CALCULATIONS OF NEW ANGLES DEPENDING ON HOW MANY CONNECTION PARRANT NODE HAS
                    double alpha = 0, rotAngle = 0;
                    double dist;
                    List<Double> AngleList = new List<Double>();
                    GNode selNode = Nodes.Find(Node => Node.Point == parentPt); // find the node of the network graph in the temp node list
                    if (selNode == null)
                        continue; // if the program arrive here, something is wrong...?
                    PointD refPt = selNode.Neighbours[0];

                    if (selNode.Connectivity == 1)
                    {
                        rotAngle = 180;
                    }
                    else
                    {
                        for (int i = 1; i < selNode.Neighbours.Count; i++)
                        {
                            PointD curNeighb = selNode.Neighbours[i];
                            double temAngle = selNode.Point.AngleBetween(refPt, curNeighb);
                            temAngle = Angle.RadiansToDegrees * temAngle;
                            if (temAngle < 0) temAngle = 360 + temAngle;
                            AngleList.Add(temAngle);
                            if (temAngle > alpha)
                            {
                                alpha = temAngle;
                            }
                        }
                        AngleList.Add(0);
                        AngleList.Add(360);
                        AngleList.Sort((y, x) => y.CompareTo(x));

                        double maxAngle = AngleList[1];
                        int inSection = 0;
                        for (int i = 2; i < AngleList.Count; i++)
                        {
                            double delta = AngleList[i] - AngleList[i - 1];
                            if (maxAngle < delta)
                            {
                                maxAngle = delta;
                                inSection = i - 1;
                            }
                        }
                        if (maxAngle < _minAngle) addEdge = false; // only add a edge, if the angle to devide is large enough!
                        if (maxAngle >= 260) // allow also 90° steps for angles larger than 260°
                        {
                            int divNr = Convert.ToInt16(Math.Ceiling(maxAngle / 90) - 0);
                            if (curInstructNode.Divider == -1) curInstructNode.Divider = RndGlobal.Rnd.Next(divNr);
                            maxAngle = 90 + curInstructNode.Divider * 90; // 180;//
                        }
                        rotAngle = maxAngle / 2 + AngleList[inSection];

                    }

                    if (addEdge) // --- check if the requirements to create a new segment are fulfilled ----------------------
                    {
                        //CREATE MOVING VECTOR AND MOVE PARENT POINT TO CREATE NEW POINT
                        // --- second, rotate the reference point by half of the largest angle and create a new point
                        if (!_refNodes.ContainsKey(parentPt))
                            return;//  Graph; // --- Fehler!!! -> Abbruch... if the program arrive here, something is wrong.
                        // replaced by a fixed angle
                        //int rndAngle = _refNodes[parentPt].AngleToGrow;
                        //if (_rotSymmetryR) alpha = Angle.DegreesToRadians * (rotAngle + RndGlobal.Rnd.Next(rndAngle));
                        //else if (_rotSymmetryL) alpha = Angle.DegreesToRadians * (rndAngle - RndGlobal.Rnd.Next(rndAngle));
                        //else alpha = Angle.DegreesToRadians * (rotAngle + rndAngle);//rotAngle + RndGlobal.Rnd.NextDouble() * rndAngle); //);//
                        ////else alpha = Angle.DegreesToRadians * (rotAngle + RndGlobal.Rnd.Next(rndAngle) - rndAngle / 2); //);//
                        //PointD newPt = RndGlobal.RotatePoint(parentPt, refPt, alpha);

                        //PointD newPt = Tools.RotatePoint(parentPt, refPt, GeoAlgorithms2D.DegreeToRadian(rotAngle + curInstructNode.Angle));
                        PointD newPt = GeoAlgorithms2D.RotatePoint(parentPt.ToVector2D(), refPt.ToVector2D(), GeoAlgorithms2D.DegreeToRadian(rotAngle + curInstructNode.Angle)).ToPointD();
                        // --- move the new Point on the axis origPt - newPt
                        //DataNode refNode = _refNodes[parentPt];
                        //dist = RndGlobal.Rnd.Next(minLength, maxLength);
                        //dist = refNode.LengthToGrow;
                        //newPt = parentPt.Move(newPt, dist);
                        newPt = parentPt.Move(newPt, curInstructNode.Length);


                        //TEST POSITION OF NEW VECTOR - IF IT IS POSSIBLE TO ADD IT
                        // --- find the nearest vertex of the graph ----------------------------------------------------------------
                        PointD nearN = Graph.GetNearestNode(newPt);                       
                        List<PointD> interPts = new List<PointD>();
                        dist = Graph.GetDistance(newPt, nearN);

                        // First, check if there is an intersection!

                        // --- THE planar graph/street network TESTS ----------------------------------------------------------------------------------------------
                        if (dist < _minDist)// if the distance is small enough, connect both points
                        {
                            //--- check connectivity of the considered node first
                            if (Graph.GetNeighbors(nearN).Count >= _maxEdges) addEdge = false;
                            else newPt = nearN;                            
                        }
                        else // --- check the distance of the newPt to an existing edge
                        {                              
                            SubdivisionEdge curEdge = Graph.FindNearestEdge(newPt, out dist);
                            if (dist < _minDist) // if the distance is small enough, connect the point to the existing edge
                            {
                                LineD curLine = new LineD(curEdge.Origin, curEdge.Destination);
                                newPt = curLine.Intersect(newPt);
                                interPts.Add(newPt);
                            }
                        }
                        // --- so far, the candidate point (newPt) is found ----------------------------------------------------
                        // --- add the new street segment to the graph and test, if the new point is added to the graph
                        if (addEdge) // --- check if the requirements to create a new segment are fulfilled
                        {                           
                            isAdded = AddGraphElements(parentPt, newPt);
                            if (isAdded) { curInstructNode.Position = newPt.ToVector2D(); }
                            else //if (!isAdded) // if the new street segment is not added, test, if there is an intersection
                            {
                                LineD newLine = new LineD(parentPt, newPt);
                                LineD[] graphEdges = Graph.ToLines();
                                foreach (LineD line in graphEdges)
                                {
                                    LineIntersection inter = newLine.Intersect(line);
                                    if (inter.Exists)
                                    {
                                        LineLocation locTest = inter.First;
                                        if (locTest == LineLocation.Between) // only it the intersection is inside the line - also excludes start and end points
                                        {
                                            interPts.Add((PointD)inter.Shared);
                                        }
                                    }
                                }

                                PointD curPt = new PointD();
                                double curMinDist = double.MaxValue;
                                if (interPts.Count == 1)
                                {
                                    curPt = interPts[0];
                                }
                                else if (interPts.Count > 1) // find the closest intersection point...
                                {
                                    foreach (PointD point in interPts)
                                    {
                                        dist = Graph.GetDistance(parentPt, point);
                                        if (dist < curMinDist)
                                        {
                                            curMinDist = dist;
                                            curPt = point;
                                        }
                                    }
                                }

                                if (!(curPt.X == 0 & curPt.Y == 0))
                                {
                                    isAdded = ConnectNew(parentPt, ref curPt);
                                    if (isAdded) { curInstructNode.Position = curPt.ToVector2d(); }
                                    else { curInstructNode.Closed = true; } // no further growth at this node
                                }
                                else { curInstructNode.Closed = true; } // no further growth at this node
                            }
                        }
                        else { curInstructNode.Closed = true; } // no further growth at this node
                    }
                    else { curInstructNode.Closed = true; } // no further growth at this node
                }
            }
        }


        //============================================================================================================================================================================
        public bool GrowStreetPatternBasic() //Subdivision GrowStreetPattern()
        {
            bool isAdded = false;

            bool addEdge = true;
            // --- select a node to expand --------------------------------------------------------------
            IList<PointD> neighb;
            List<GNode> Nodes = new List<GNode>();
            List<PointD> vertices = Graph.Nodes.ToList();
            for (int i = 0; i < vertices.Count; i++)
            {
                Nodes.Add(new GNode(vertices[i], true));
                neighb = Graph.GetNeighbors(Nodes[i].Point);
                Nodes[i].Connectivity = neighb.Count;
                Nodes[i].Neighbours = neighb;
            }

            List<GNode> activeNodes = new List<GNode>();
            List<double> Connectivity = new List<double>();
            foreach (GNode node in Nodes)
            {
                if (node.Active)
                {
                    activeNodes.Add(node);
                    if (node.Connectivity == 0 | node.Connectivity >= _maxEdges) 
                        Connectivity.Add(0);
                    //else Connectivity.Add( Math.Pow(node.Connectivity, 5));// * node.Connectivity));//(1.0 /
                    else Connectivity.Add(1.0 /Math.Pow(node.Connectivity, 5));// * node.Connectivity));//(
                }
            }

            double[] normValues = Connectivity.ToArray();
            MathUtility.Normalize(normValues);

            // --- roulette wheel selection 
            double rndVal = RndGlobal.Rnd.NextDouble();
            GNode selNode = null;
            double tempSum = 0;
            for (int i = 0; i < activeNodes.Count; i++)
            {
                tempSum += normValues[i];
                if (tempSum >= rndVal)
                {
                    selNode = activeNodes[i];
                    break;
                }
            }

            // --- calculate the angle and length of of the new street segment --------------------------
            // --- first, find the largest free angle
            double alpha = 0, rotAngle = 0;
            double dist;
            PointD origPt = selNode.Point;
            if (Graph.GetNeighbors(origPt).Count >= _maxEdges) addEdge = false;
            //if (selNode.Neighbours.Count >= _maxEdges) addEdge = false;
            List<Double> AngleList = new List<Double>();
            PointD refPt = selNode.Neighbours[0];

            if (selNode.Connectivity == 1)
            {
                rotAngle = 180;
            }
            else
            {
                for (int i = 1; i < selNode.Neighbours.Count; i++)
                {
                    PointD curNeighb = selNode.Neighbours[i];
                    double temAngle = selNode.Point.AngleBetween(refPt, curNeighb);
                    temAngle = Angle.RadiansToDegrees * temAngle;
                    if (temAngle < 0) temAngle = 360 + temAngle;
                    AngleList.Add(temAngle);
                    if (temAngle > alpha)
                    {
                        alpha = temAngle;
                    }
                }
                AngleList.Add(0);
                AngleList.Add(360);
                AngleList.Sort((y, x) => y.CompareTo(x));

                double maxAngle = AngleList[1];
                int inSection = 0;
                for (int i = 2; i < AngleList.Count; i++)
                {
                    double delta = AngleList[i] - AngleList[i - 1];
                    if (maxAngle < delta)
                    {
                        maxAngle = delta;
                        inSection = i-1;
                    }
                }
                if (maxAngle < _minAngle) addEdge = false; // only add a edge, if the angle to devide is large enough!
                if (maxAngle >= 260) // allow also 90° steps for angles larger than 260°
                {
                    int divNr = Convert.ToInt16(Math.Ceiling(maxAngle / 90));
                    maxAngle = 90 + RndGlobal.Rnd.Next(divNr)*90;
                }
                rotAngle = maxAngle / 2 + AngleList[inSection];
            }

            if (addEdge) // --- check if the requirements to create a new segment are fulfilled ----------------------
            {
                // --- second, rotate the reference point by half of the largest angle and create a new point
                if (!_refNodes.ContainsKey(origPt))
                    return false;//  Graph; // --- Fehler!!! -> Abbruch...
                int rndAngle = _refNodes[origPt].AngleToGrow;
                if (_rotSymmetryR) alpha = Angle.DegreesToRadians * (rotAngle + RndGlobal.Rnd.Next(rndAngle));
                else if (_rotSymmetryL) alpha = Angle.DegreesToRadians * (rndAngle - RndGlobal.Rnd.Next(rndAngle));
                else alpha = Angle.DegreesToRadians * (rotAngle + rndAngle);//rotAngle + RndGlobal.Rnd.NextDouble() * rndAngle); //);//
                //else alpha = Angle.DegreesToRadians * (rotAngle + RndGlobal.Rnd.Next(rndAngle) - rndAngle / 2); //);//
                PointD newPt = GeoAlgorithms2D.RotatePoint(origPt.ToVector2D(), refPt.ToVector2D(), alpha).ToPointD();
                // --- move the new Point on the axis origPt - newPt
                DataNode refNode = _refNodes[origPt];
                //dist = RndGlobal.Rnd.Next(minLength, maxLength);
                dist = refNode.LengthToGrow;
                newPt = origPt.Move(newPt, dist);

                // --- find the nearest vertex of the graph
                PointD nearN = Graph.GetNearestNode(newPt);
                List<PointD> interPts = new List<PointD>();
                dist = Graph.GetDistance(newPt, nearN);
                if (dist < _minDist)// if the distance is small enough, connect both points
                { 
                    //--- check connectivity of the considered node first
                    if (Graph.GetNeighbors(nearN).Count >= _maxEdges) addEdge = false;
                    else newPt = nearN;
                }
                else // --- check the distance of the newPt to an existing edge
                { 
                    SubdivisionEdge curEdge = Graph.FindNearestEdge(newPt, out dist);
                    if (dist < _minDist) // if the distance is small enough, connect the point to the existing edge
                    {
                        LineD curLine = new LineD(curEdge.Origin, curEdge.Destination);
                        newPt = curLine.Intersect(newPt);
                        interPts.Add(newPt);
                    }
                } 
                // --- so far, the candidate point (newPt) is found ----------------------------------------------------
                
                // --- add the new street segment to the graph and test, if the new point is added to the graph
                if (addEdge) // --- check if the requirements to create a new segment are fulfilled
                {
                    isAdded = AddGraphElements(origPt, newPt);
                    if (!isAdded) // if the new street segment is not added, test, if there is an intersection
                    {
                        LineD newLine = new LineD(origPt, newPt);
                        LineD[] graphEdges = Graph.ToLines();
                        foreach (LineD line in graphEdges)
                        {
                            LineIntersection inter = newLine.Intersect(line);
                            if (inter.Exists)
                            {
                                LineLocation locTest = inter.First;
                                if (locTest == LineLocation.Between) // only it the intersection is inside the line - also excludes start and end points
                                {
                                    interPts.Add((PointD)inter.Shared);
                                }
                            }
                        }

                        PointD curPt = new PointD();
                        double curMinDist = double.MaxValue;
                        if (interPts.Count == 1)
                        {
                            curPt = interPts[0];
                        }
                        else if (interPts.Count > 1) // find the closest intersection point...
                        {
                            foreach (PointD point in interPts)
                            {
                                dist = Graph.GetDistance(origPt, point);
                                if (dist < curMinDist)
                                {
                                    curMinDist = dist;
                                    curPt = point;
                                }
                            }
                        }

                        if (!(curPt.X == 0 & curPt.Y == 0))
                            isAdded = ConnectNew(origPt, ref curPt);

                    }
                }
                //glControl1.Invalidate();
            }
            return isAdded;//Graph;
        }

        //============================================================================================================================================================================
        private bool ConnectNew(PointD origPt, ref PointD curPt)
        {
            bool isAdded = false;
            bool addEdge = true;
            double dist;
            SubdivisionEdge curEdge = Graph.FindNearestEdge(curPt, out dist);
            if (curEdge == null) return false;
            PointD origin = curEdge.Origin;
            PointD destination = curEdge.Destination;

            double distO = Graph.GetDistance(curPt, origin);
            double distD = Graph.GetDistance(curPt, destination);

            // --- check distance of the intersection point to existing points
            if (distO < _minDist | distD < _minDist) // if the distance is small enough, connect use existing points instead of the new intersection point
            {
                if (distO < distD) curPt = origin;
                else curPt = destination;
                if (Graph.GetNeighbors(curPt).Count >= _maxEdges) addEdge = false;
                if (addEdge) // --- check if the requirements to create a new segment are fulfilled
                {
                    dist = Graph.GetDistance(curPt, origPt);
                    if (dist < _minDist) // if the new point would be to close to the old one, move the old one to the position of the new one, instad of creating a new point
                    {
                        int vertIdx = Graph.FindNearestVertex(origPt);
                        if (Graph.MoveVertex(vertIdx, curPt)) // ? is connected to graph????????????????????
                        {
                            DataNode curNode = _refNodes[origPt];
                            _refNodes.Remove(origPt);
                            if (_refNodes.ContainsKey(curPt) == false) _refNodes.Add(curPt, curNode);
                        }
                    }
                    else isAdded = AddGraphElements(origPt, curPt);
                }
            }
            else // --- create a new intersection point
            {
                int key = curEdge.Key;
                //if (key < Graph.Edges.Count)
                //{
                    if (Graph.RemoveEdge(key))
                    {
                        bool newLineOC = true;
                        dist = Graph.GetDistance(curPt, origPt);
                        if (dist < _minDist) // if the new point would be to close to the old one, move the old one to the position of the new one, instad of creating a new point
                        {
                            int vertIdx = Graph.FindNearestVertex(origPt);
                            if (Graph.MoveVertex(vertIdx, curPt)) // ? is connected to graph????????????????????
                            {
                                DataNode curNode = _refNodes[origPt];
                                _refNodes.Remove(origPt);
                                if (_refNodes.ContainsKey(curPt) == false) _refNodes.Add(curPt, curNode);
                            }
                            newLineOC = false;
                        }
                        LineD[] curLinesArr = Graph.ToLines();
                        List<LineD> curLinesList = curLinesArr.ToList();
                        if (newLineOC)
                        {
                            if(!curLinesList.Contains(new LineD(origPt, curPt)) && !curLinesList.Contains(new LineD(curPt, origPt)))
                                curLinesList.Add(new LineD(origPt, curPt)); // create this line only, if the orig point is not to close
                        }
                        if (!curLinesList.Contains(new LineD(origin, curPt)) && !curLinesList.Contains(new LineD(curPt, origin)))
                            curLinesList.Add(new LineD(origin, curPt));
                        if (!curLinesList.Contains(new LineD(destination, curPt)) && !curLinesList.Contains(new LineD(curPt, destination)))
                            curLinesList.Add(new LineD(destination, curPt));
                        
                        curLinesArr = curLinesList.ToArray();
                        Graph = new Subdivision();
                        if (_refNodes.ContainsKey(curPt) == false) _refNodes.Add(curPt, new DataNode());
                        //Graph.Epsilon = _minDist - 1;
                        Graph = Subdivision.FromLines(curLinesArr);
                        isAdded = Graph.Contains(curPt);// true;
                    }
                //}
                    
            }
            return isAdded;
        }

        //============================================================================================================================================================================
        private void SetNextDataNode(PointD oldPtKey, PointD newPtKey)
        {
            DataNode oldNode = _refNodes[oldPtKey];
            DataNode newNode = new DataNode(oldNode);
            newNode.LengthToGrow += 10;
            if (RndGlobal.Rnd.NextDouble() < 0.1) newNode.AngleToGrow += RndGlobal.Rnd.Next(10)-5;
            //newNode.AngleToGrow += 1;
            if (_refNodes.ContainsKey(newPtKey) == false) _refNodes.Add(newPtKey, newNode);
        }

        //============================================================================================================================================================================
        private bool AddGraphElements(PointD oldPt, PointD newPt)
        {            
            bool isAdded = false;
            int testA, testB;
            bool outOfBorder = false;
            if (! _border.ContainsPoint(newPt.ToVector2D())) outOfBorder = true;

            if (!outOfBorder)
            {
                try
                {
                    Graph.AddEdge(oldPt, newPt, out testA, out testB);
                }
                catch (Exception)
                {
                    Console.WriteLine("Add element failed");
                    throw;
                }
                

                if (testA >= 0) // test, it the new point is added to the graph
                {
                    isAdded = true;
                    SetNextDataNode(oldPt, newPt);
                }
            }
            return isAdded;
        }

        //============================================================================================================================================================================
        public void AddEdge(PointD pt1, PointD pt2)
        {
            Graph.AddEdge(pt1, pt2);
            if (!_refNodes.ContainsKey(pt1))
            {
                DataNode curNode = new DataNode();
                _refNodes.Add(pt1, curNode);
            }
            if (!_refNodes.ContainsKey(pt2))
            {
                DataNode curNode = new DataNode();
                curNode.AngleToGrow *= -1;
                _refNodes.Add(pt2, curNode);
            }

            //AddGraphElements(pt1, pt2);
        }

        //============================================================================================================================================================================
        public void CleanUp()
        {
            bool changed = true;
            int counter = 0;
            while (changed == true & counter < 10)
            {
                counter++;
                bool test1 = false;//CleanNearPointsToPoints();
                bool test2 = false;// CleanNearPointsToLine();
                bool test3 = false;// CleanNearLinesToLine();
                if (test1 | test2 | test3) changed = true;
                else changed = false;
            }
        }

        //============================================================================================================================================================================
        // -- finds points that are too close to a line --> delete the point or integrate a corner to the line --
        private bool CleanNearPointsToLine()
        {         
            UndirectedEdge<Vector2d>[] qEdges = QGraph.Edges.ToArray();
            List<Line2D> graphLines = new List<Line2D>();
            foreach (UndirectedEdge<Vector2d> edge in qEdges) graphLines.Add(new Line2D(edge.Source, edge.Target));
            List<Vector2d> graphPoints = QGraph.Vertices.ToList();
            Line2D curLine;
            bool changed = false;
            
            for (int i = 0; i < graphLines.Count; i++)    
            {
                curLine = graphLines[i];
                foreach (Vector2d curPt in graphPoints)
                {
                    bool partOfLine = false;
                    if (curPt == curLine.Start | curPt == curLine.End) partOfLine = true;
                    if (!partOfLine)
                    {
                        Vector2d intersetPt;
                        double dist = GeoAlgorithms2D.DistancePointToSegment(curPt, curLine.Start, curLine.End, out intersetPt);
                        // -- test if the nearest pont on the line is between end and start point --
                        if ((intersetPt != curLine.Start) & (intersetPt != curLine.End))
                        {
                            if (dist < _minDist-1)
                            {
                                if (QGraph.ContainsVertex(curPt))
                                {
                                    if (QGraph.AdjacentDegree(curPt) == 1)
                                    {
                                        Line2D curEdgeL = GeoAlgorithms2D.NearestLineToPoint(graphLines.ToArray(), curPt, out dist);
                                        UndirectedEdge<Vector2d> curEdge = QGraph.Edges.ToList().Find(x => x.Source == curEdgeL.Start && x.Target == curEdgeL.End);
                                        if (curEdge != null)
                                        {
                                            QGraph.RemoveEdge(curEdge);
                                        }
                                        else // test reverse edge
                                        {
                                            UndirectedEdge<Vector2d> revCurEdge = QGraph.Edges.ToList().Find(x => x.Source == curEdgeL.End && x.Target == curEdgeL.Start);
                                            if (revCurEdge != null)
                                                QGraph.RemoveEdge(revCurEdge);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // --- delete unconnected nodes ---
                graphPoints = QGraph.Vertices.ToList();
                foreach (Vector2d curPt in graphPoints)
                {
                    if (QGraph.AdjacentDegree(curPt) == 0)
                        QGraph.RemoveVertex(curPt);
                }
            }
            return changed;
        }

        //============================================================================================================================================================================
        private bool CleanNearPointsToPoints()
        {
            //SortedListEx<PointD, SubdivisionEdge> vertices = Graph.Vertices;
            //List<PointD> nodes = vertices.Keys.ToList();
            List<Vector2d> nodes = new List<Vector2d>();
            nodes = QGraph.Vertices.ToList();
            bool nearFound;
            bool changed = false;
            //DataNode oldNode;

            while (nodes.Count > 0)
            {
                List<Vector2d> toNearPts = new List<Vector2d>();
                nearFound = false;
                Vector2d curVert = nodes[0];
                //oldNode = new DataNode();
                // --- find points that are located to close to each other ---
                for (int k = 1; k < nodes.Count; k++)
                {
                    Vector2d compareVert = nodes[k];
                    Double dist = GeoAlgorithms2D.DistancePoints(curVert, compareVert);// Graph.GetDistance(curVert, compareVert);
                    if (dist < _minDist)
                    {
                        if (toNearPts.Count == 0)
                        {
                            toNearPts.Add(curVert);
                            //if (_refNodes.ContainsKey(curVert)) oldNode = _refNodes[curVert];
                        }
                        toNearPts.Add(compareVert);
                        nearFound = true;
                    }
                }

                if (nearFound)
                {
                    // --- remove the old edges ---
                    List<Vector2d> allNeightb = new List<Vector2d>();
                    foreach (Vector2d firstVert in toNearPts)
                    {
                        List<Vector2d> curNeightb = QGraph.GetNeighbors(firstVert);
                        allNeightb.AddRange(curNeightb);
                        foreach (Vector2d secVert in curNeightb)
                        {
                            //SubdivisionEdge curEdge = Graph.FindEdge(firstVert, secVert);
                            UndirectedEdge<Vector2d> curEdge = QGraph.Edges.ToList().Find(x => x.Source == firstVert && x.Target == secVert);
                            if (curEdge != null)
                            {
                                QGraph.RemoveEdge(curEdge);
                                changed = true;
                            }
                        }
                        //if (_refNodes.ContainsKey(firstVert)) _refNodes.Remove(firstVert);
                    }

                    // --- calculate the new midPoint ---
                    Vector2d midPt = new Vector2d();
                    foreach (Vector2d firstVert in toNearPts)
                    {
                        midPt += firstVert;
                    }
                    midPt = midPt / toNearPts.Count;

                    //if (_refNodes.ContainsKey(midPt) == false) _refNodes.Add(midPt, oldNode);

                    // --- add the new edges ---
                    LineD[] curLinesArr = Graph.ToLines();
                    List<LineD> curLinesList = curLinesArr.ToList();

                    foreach (Vector2d conPt in allNeightb)
                    {
                        //curLinesList.Add(new LineD(midPt, conPt));
                        UndirectedEdge<Vector2d> curAddEdge = new UndirectedEdge<Vector2d>(midPt, conPt);
                        UndirectedEdge<Vector2d> checkRevAddEdge = QGraph.Edges.ToList().Find(x => x.Source == midPt && x.Target == conPt);
                        if (checkRevAddEdge == null)
                            QGraph.AddVerticesAndEdge(curAddEdge);                        
                    }

                    //curLinesArr = curLinesList.ToArray();
                    //Graph = new Subdivision();
                    //Graph = Subdivision.FromLines(curLinesArr);
                }
                nodes.Remove(curVert);
            }
            // --- delete unconnected nodes ---
            nodes = QGraph.Vertices.ToList();
            foreach (Vector2d curPt in nodes)
            {
                if (QGraph.AdjacentDegree(curPt) == 0)
                    QGraph.RemoveVertex(curPt);
            }

            return changed;
        }

        //============================================================================================================================================================================
        // -- there is still a bug somewhere... ---
        private bool CleanNearLinesToLine()
        {
            List<LineD> graphLines = Graph.ToLines().ToList();
            List<PointD> graphPoints = Graph.Nodes.ToList();// Vertices.Keys.ToList();
            bool restart = false;
            bool changed = false;
            LineD curLine;
            //DataNode oldNode;
            int cter = 0;

            while (graphLines.Count > 0)
            {
                cter++;
                restart = false;
                curLine = graphLines[0];
                //oldNode = new DataNode();
                foreach (PointD curPt in graphPoints)
                {
                    bool partOfLine = false;
                    if (curPt == curLine.Start | curPt == curLine.End) partOfLine = true;
                    if (!partOfLine)
                    {
                        Vector2d interPt;
                        SubdivisionEdge curEdge;
                        double dist = GeoAlgorithms2D.DistancePointToSegment(curPt.ToVector2D(), curLine.Start.ToVector2D(), curLine.End.ToVector2D(), out interPt);
                        PointD intersetPt = interPt.ToPointD();
                        // -- test if the nearest pont on the line is between end and start point --
                        if ((intersetPt != curLine.Start) & (intersetPt != curLine.End))
                        {
                            if (dist < _minDist-1)
                            {
                                // -- if we have a individual point, delete it --
                                if (Graph.GetNeighbors(curPt).Count > 1)
                                {

                                    //if (_refNodes.ContainsKey(curPt)) oldNode = _refNodes[curPt];
                                    // --- remove the old edges ---
                                    List<PointD> allNeightb = Graph.GetNeighbors(curPt).ToList();                                  
                                    // --- remove the edges that belong to the point that is too close to the line
                                    foreach (PointD secVert in allNeightb)
                                    {
                                        curEdge = Graph.FindEdge(curPt, secVert);
                                        SubdivisionEdge otherEdge = Graph.FindEdge(secVert, curPt);
                                        if (curEdge != null & otherEdge != null)
                                        {
                                            Graph.RemoveEdge(curEdge.Key);
                                            changed = true;
                                        }
                                    }
                                    curEdge = Graph.FindEdge(curLine.Start, curLine.End);
                                    Graph.RemoveEdge(curEdge.Key);
                                    allNeightb.Add(curLine.Start);
                                    allNeightb.Add(curLine.End);
                                    if (_refNodes.ContainsKey(curPt)) _refNodes.Remove(curPt);
                                    //if (_refNodes.ContainsKey(intersetPt) == false) _refNodes.Add(intersetPt, oldNode);

                                    // --- add the new edges ---
                                    LineD[] curLinesArr = Graph.ToLines();
                                    List<LineD> curLinesList = curLinesArr.ToList();

                                    foreach (PointD conPt in allNeightb)
                                    {
                                        curLinesList.Add(new LineD(intersetPt, conPt));
                                    }

                                    curLinesArr = curLinesList.ToArray();
                                    Graph = new Subdivision();
                                    Graph = Subdivision.FromLines(curLinesArr);
                                    restart = true;
                                    //break;
                                    return changed;
                                }
                            }
                        }
                    }
                }
                if (restart)
                {
                    graphLines = Graph.ToLines().ToList();
                    graphPoints = Graph.Nodes.ToList();
                }
                else
                    graphLines.Remove(curLine);
            }
            return changed;
        }


        //============================================================================================================================================================================
        //=========================================== Subdivision cleaners ===========================================================================================================
        //============================================================================================================================================================================
        public void CleanUpSubdiv()
        {
            bool changed = true;
            int counter = 0;
            while (changed == true & counter < 10)
            {
                counter++;
                bool test1 = CleanNearPointsToPointsSubdiv();
                bool test2 = CleanNearPointsToLineSubdiv();
                bool test3 = CleanNearLinesToLineSubdiv();
                if (test1 | test2 | test3) changed = true;
                else changed = false;
            }
        }

        //============================================================================================================================================================================
        private bool CleanNearPointsToLineSubdiv()
        {
            // -- finds points that are too close to a line --> delete the point or integrate a corner to the line --
            List<LineD> graphLines = Graph.ToLines().ToList();
            List<PointD> graphPoints = Graph.Nodes.ToList();// Vertices.Keys.ToList();
            LineD curLine;
            DataNode oldNode;
            bool changed = false;

            for (int i = 0; i < graphLines.Count; i++)
            //while (graphLines.Count > 0)         
            {
                //restart = false;
                curLine = graphLines[i];
                oldNode = new DataNode();
                foreach (PointD curPt in graphPoints)
                {
                    bool partOfLine = false;
                    if (curPt == curLine.Start | curPt == curLine.End) partOfLine = true;
                    if (!partOfLine)
                    {
                        Vector2d interPt;
                        SubdivisionEdge curEdge;
                        double dist = GeoAlgorithms2D.DistancePointToSegment(curPt.ToVector2D(), curLine.Start.ToVector2D(), curLine.End.ToVector2D(), out interPt);
                        PointD intersetPt = interPt.ToPointD();
                        // -- test if the nearest pont on the line is between end and start point --
                        if ((intersetPt != curLine.Start) & (intersetPt != curLine.End))
                        {
                            if (dist < _minDist - 1)
                            {
                                if (Graph.GetNeighbors(curPt).Count == 1)
                                {
                                    curEdge = Graph.FindNearestEdge(curPt, out dist);
                                    if (curEdge != null)
                                    {
                                        bool test = Graph.RemoveEdge(curEdge.Key);
                                        if (test)
                                        {
                                            if (_refNodes.ContainsKey(curPt)) _refNodes.Remove(curPt);
                                            changed = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return changed;
        }

        //============================================================================================================================================================================
        private bool CleanNearPointsToPointsSubdiv()
        {
            SortedListEx<PointD, SubdivisionEdge> vertices = Graph.Vertices;
            List<PointD> nodes = vertices.Keys.ToList();
            bool nearFound;
            bool changed = false;
            DataNode oldNode;

            while (nodes.Count > 0)
            {
                List<PointD> toNearPts = new List<PointD>();
                nearFound = false;
                PointD curVert = nodes[0];
                oldNode = new DataNode();
                // --- find points that are located to close to each other ---
                for (int k = 1; k < nodes.Count; k++)
                {
                    PointD compareVert = nodes[k];
                    Double dist = Graph.GetDistance(curVert, compareVert);
                    if (dist < _minDist)
                    {
                        if (toNearPts.Count == 0)
                        {
                            toNearPts.Add(curVert);
                            if (_refNodes.ContainsKey(curVert)) oldNode = _refNodes[curVert];
                        }
                        toNearPts.Add(compareVert);
                        nearFound = true;
                    }
                }

                if (nearFound)
                {
                    // --- remove the old edges ---
                    List<PointD> allNeightb = new List<PointD>();
                    foreach (PointD firstVert in toNearPts)
                    {
                        List<PointD> curNeightb = Graph.GetNeighbors(firstVert).ToList();
                        allNeightb.AddRange(curNeightb);
                        foreach (PointD secVert in curNeightb)
                        {
                            SubdivisionEdge curEdge = Graph.FindEdge(firstVert, secVert);
                            if (curEdge != null)
                            {
                                Graph.RemoveEdge(curEdge.Key);
                                changed = true;
                            }
                        }
                        if (_refNodes.ContainsKey(firstVert)) _refNodes.Remove(firstVert);
                    }

                    // --- calculate the new midPoint ---
                    PointD midPt = new PointD();
                    foreach (PointD firstVert in toNearPts)
                    {
                        midPt += firstVert;
                    }
                    double divider = Convert.ToDouble(1.0 / toNearPts.Count);
                    midPt = midPt.MultiplyD(divider);

                    if (_refNodes.ContainsKey(midPt) == false) _refNodes.Add(midPt, oldNode);

                    // --- add the new edges ---
                    LineD[] curLinesArr = Graph.ToLines();
                    List<LineD> curLinesList = curLinesArr.ToList();

                    foreach (PointD conPt in allNeightb)
                    {
                        curLinesList.Add(new LineD(midPt, conPt));
                    }

                    curLinesArr = curLinesList.ToArray();
                    Graph = new Subdivision();
                    Graph = Subdivision.FromLines(curLinesArr);
                }
                nodes.Remove(curVert);
            }
            return changed;
        }

        //============================================================================================================================================================================
        // -- there is still a bug somewhere... ---
        private bool CleanNearLinesToLineSubdiv()
        {
            List<LineD> graphLines = Graph.ToLines().ToList();
            List<PointD> graphPoints = Graph.Nodes.ToList();// Vertices.Keys.ToList();
            bool restart = false;
            bool changed = false;
            LineD curLine;
            DataNode oldNode;
            int cter = 0;

            while (graphLines.Count > 0)
            {
                cter++;
                restart = false;
                curLine = graphLines[0];
                oldNode = new DataNode();
                foreach (PointD curPt in graphPoints)
                {
                    bool partOfLine = false;
                    if (curPt == curLine.Start | curPt == curLine.End) partOfLine = true;
                    if (!partOfLine)
                    {
                        Vector2d interPt;                     
                        SubdivisionEdge curEdge;
                        double dist = GeoAlgorithms2D.DistancePointToSegment(curPt.ToVector2D(), curLine.Start.ToVector2D(), curLine.End.ToVector2D(), out interPt);
                        PointD intersetPt = interPt.ToPointD();
                        // -- test if the nearest pont on the line is between end and start point --
                        if ((intersetPt != curLine.Start) & (intersetPt != curLine.End))
                        {
                            if (dist < _minDist - 1)
                            {
                                // -- if we have a individual point, delete it --
                                if (Graph.GetNeighbors(curPt).Count > 1)
                                {

                                    if (_refNodes.ContainsKey(curPt)) oldNode = _refNodes[curPt];
                                    // --- remove the old edges ---
                                    List<PointD> allNeightb = Graph.GetNeighbors(curPt).ToList();
                                    // --- remove the edges that belong to the point that is too close to the line
                                    foreach (PointD secVert in allNeightb)
                                    {
                                        curEdge = Graph.FindEdge(curPt, secVert);
                                        SubdivisionEdge otherEdge = Graph.FindEdge(secVert, curPt);
                                        if (curEdge != null & otherEdge != null)
                                        {
                                            Graph.RemoveEdge(curEdge.Key);
                                            changed = true;
                                        }
                                    }
                                    curEdge = Graph.FindEdge(curLine.Start, curLine.End);
                                    Graph.RemoveEdge(curEdge.Key);
                                    allNeightb.Add(curLine.Start);
                                    allNeightb.Add(curLine.End);
                                    if (_refNodes.ContainsKey(curPt)) _refNodes.Remove(curPt);
                                    if (_refNodes.ContainsKey(intersetPt) == false) _refNodes.Add(intersetPt, oldNode);

                                    // --- add the new edges ---
                                    LineD[] curLinesArr = Graph.ToLines();
                                    List<LineD> curLinesList = curLinesArr.ToList();

                                    foreach (PointD conPt in allNeightb)
                                    {
                                        curLinesList.Add(new LineD(intersetPt, conPt));
                                    }

                                    curLinesArr = curLinesList.ToArray();
                                    Graph = new Subdivision();
                                    Graph = Subdivision.FromLines(curLinesArr);
                                    restart = true;
                                    //break;
                                    return changed;
                                }
                            }
                        }
                    }
                }
                if (restart)
                {
                    graphLines = Graph.ToLines().ToList();
                    graphPoints = Graph.Nodes.ToList();
                }
                else
                    graphLines.Remove(curLine);
            }
            return changed;
        }   

    }

}


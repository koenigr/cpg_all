﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPlan.Geometry;
using CPlan.Optimization;
using GeoAPI.Geometries;
using Lucy;
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
//using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenTK;
//using OpenTK.Graphics.OpenGL;

namespace CPlan.UrbanPattern
{
    public class LucyManager1
    {
        private Form1 _mainWindow;

        private static LucyManager1 _man = null;

        private LucyManager1(Form1 mainWindow)
        {
            _mainWindow = mainWindow;
            ViaLucy = false;
            Connected = false;
        }

        public static LucyManager1 Manager
        {
            get { return _man; }
        }

        public static LucyManager1 Create(Form1 mainWindow)
        {
            return _man = new LucyManager1(mainWindow);
        }

        /////////////////////////////////////////////////

        public void ShowLucyInProgress()
        {
            _mainWindow.LucyExecutionProgressBar.Visible = true;
            _mainWindow.LucyExecutionProgressBar.Style = ProgressBarStyle.Marquee;
            _mainWindow.LucyExecutionProgressBar.MarqueeAnimationSpeed = 10;

        }

        public void ShowLucyIdle()
        {
            _mainWindow.LucyExecutionProgressBar.Visible = false;
        }

        public void ShowLucyDisconnected()
        {
            _mainWindow.LucyConnectionButton.Text = "Connect to Luci";
            _mainWindow.LucyIpTextBox.Enabled = true;
            _mainWindow.LucyPortTextBox.Enabled = true;
            _mainWindow.LucyUserNameTextBox.Enabled = true;
            _mainWindow.LucyUserPasswordTextBox.Enabled = true;
            ViaLucy = false;
            _mainWindow.LucyUseServicesCheckBox.Checked = false;
            _mainWindow.LucyUseServicesCheckBox.Enabled = false;
        }

        public void ShowLucyConnected()
        {
            _mainWindow.LucyConnectionButton.Text = "Disconnect from Luci";
            _mainWindow.LucyIpTextBox.Enabled = false;
            _mainWindow.LucyPortTextBox.Enabled = false;
            _mainWindow.LucyUserNameTextBox.Enabled = false;
            _mainWindow.LucyUserPasswordTextBox.Enabled = false;
            _mainWindow.LucyUseServicesCheckBox.Enabled = true;
        }


        ////////////////////////////////////////////////////////////////////////////////////

        public bool ViaLucy { get; private set; }

        public bool Connected  { get; private set; }
        public ConnectionManager ConnectionManager { get { return _connectionManager; } }
        private ConnectionManager _connectionManager = null;

        public void ConnectToLucy()
        {
            CommunicateToLuci.ServiceManager.Init(_mainWindow.LucyIpTextBox.Text);
            var worker = new BackgroundWorker();
            worker.DoWork += (sender, args) =>
            {
                _connectionManager = new ConnectionManager(
                    _mainWindow.LucyIpTextBox.Text,
                    int.Parse(_mainWindow.LucyPortTextBox.Text),
                    _mainWindow.LucyUserNameTextBox.Text,
                    _mainWindow.LucyUserPasswordTextBox.Text);
            };
            worker.RunWorkerCompleted += (sender, args) =>
            {
                if (args.Error == null)
                {
                    _mainWindow.LucyConsoleLogBox.Text += "\r\nConnected to Luci.\r\n";
                    ShowLucyConnected();
                    Connected = true;

                   // CommunicateToLucy.ServiceManager.Init(_mainWindow.LucyIpTextBox.Text);
                }
                else
                {
                    _mainWindow.LucyConsoleLogBox.Text += "\r\nError occured while trying to connect to Luci\r\n" + args.Error.Message + "\r\n";
                }
                ShowLucyIdle();
            };
            ShowLucyInProgress();
            worker.RunWorkerAsync();

//            CommunicateToLucy.ServiceManager.Init(_mainWindow.LucyIpTextBox.Text);
        }

        public void DisconnectFromLucy()
        {
            try
            {
                _connectionManager.Dispose();
                _mainWindow.LucyConsoleLogBox.Text += "\r\nDisconnected.\r\n";
            }
            catch (Exception ex)
            {
                _mainWindow.LucyConsoleLogBox.Text += "\r\nError occured while trying to disconnect from Luci\r\n" + ex.Message + "\r\n";
            }
            finally
            {
                _connectionManager = null;
                Connected = false;
                ShowLucyDisconnected();
            }
        }

        public void WorkViaLucy()
        {
            if (Connected)
            {
                ViaLucy = true;
                if (!_mainWindow.LucyUseServicesCheckBox.Checked)
                    _mainWindow.LucyUseServicesCheckBox.Checked = true;
            }
                
        }

        public void WorkLocally()
        {
            ViaLucy = false;
            if (_mainWindow.LucyUseServicesCheckBox.Checked)
                _mainWindow.LucyUseServicesCheckBox.Checked = false;
        }

        public void Log(string s)
        {
            _mainWindow.LucyConsoleLogBox.Text += "\r\n" + s + "\r\n";
            _mainWindow.LucyConsoleLogBox.ScrollToCaret();
        }

        ////////////////////////////////////////////////////////////////////////////////////

        public async Task<double[]> DoSolarAnalysis(MultiPolygon geometry, IEnumerable<Vector3d> gridPoints, IEnumerable<Vector3d> gridNormales)
        {
            var pathToHdriFile = Path.GetFullPath(Environment.CurrentDirectory + "..\\..\\..\\..\\..\\Various\\HDRI\\CA Cumulative Sky - Year 2012 23 Minute step.hdr");
            var hdri = File.ReadAllBytes(pathToHdriFile);
            if (!File.Exists(pathToHdriFile))
            {
                //pathToHdriFile = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "\\CA Cumulative Sky - Year 2012 23 Minute step.hdr"); ;
                pathToHdriFile = Path.GetFullPath(Environment.CurrentDirectory + "\\CA Cumulative Sky - Year 2012 23 Minute step.hdr");
                if (!File.Exists(pathToHdriFile))
                {
                    pathToHdriFile = Path.GetFullPath(Environment.CurrentDirectory + "\\data\\HDRI\\CA Cumulative Sky - Year 2012 23 Minute step.hdr");
                    if (!File.Exists(pathToHdriFile))
                    {
                        MessageBox.Show("There is no valid path to a HDRI file!");
                    }
                }
            }
            // Creating a scenario is semi-automatic in current version of library 
            var scenarioObj = LucyManager1.Manager.ConnectionManager.CreateScenario("SolarAnalysisInUrbanPlanning",
                // put GeoJSON geometry into scenario
                JObject.FromObject(new
                {
                    SiGeometry =
                        (JObject)(new Lucy.Geometry()
                        {
                            Format = "GeoJSON",
                            Data = new FeatureCollection(new Collection<IFeature>() { new Feature(geometry, new AttributesTable()) })
                        })
                }));
            int? scId;
            try { scId = scenarioObj.SelectToken("result.ScID").Value<int>(); }
            catch (Exception ex) { throw new ConnectionManager.LucyCommunicationError("Could not create Scenario. " + ex.Message); }

            // creating a service instance with input data
            var points = gridPoints.SelectMany(p => new[] { p.X, p.Y, p.Z }).ToArray();
            var normales = gridNormales.SelectMany(p => new[] { p.X, p.Y, p.Z }).ToArray();
            // here we create byte array and StreamInfo based on it (MD5 generated based on bytes)
            byte[][] attachedBytes = new byte[3][];
            attachedBytes[0] = new byte[points.Length * sizeof(double)];
            attachedBytes[1] = new byte[normales.Length * sizeof(double)];
            attachedBytes[2] = hdri;
            Buffer.BlockCopy(points, 0, attachedBytes[0], 0, attachedBytes[0].Length);
            Buffer.BlockCopy(normales, 0, attachedBytes[1], 0, attachedBytes[1].Length);

            var remoteServiceInstance = LucyManager1.Manager.ConnectionManager.CreateServiceInstance("SolarAnalysis", scId,
                new Dictionary<string, object>()
                        {
                            {"Points", new StreamInfo(attachedBytes[0], 1, "doublesXYZ")}, // array of doubles: XYZ coordinates
                            {"Normales", new StreamInfo(attachedBytes[1], 2, "doublesXYZ")}, // array of doubles: XYZ coordinates
                            // Compulsory scenario parameter - HDRI (Information about file)
                            {"HdriImage", new Lucy.StreamInfo(hdri, 3, "hdrImage")},
                            // Optional scenario parameters. If they are not listed in a list of parameters. TODO: Now they are obligatory :(
                            {"calculationMode", "Klux" },
                            {"resolution", 64},
                            {"northOrientationAngle", 0f}
                        }, attachedBytes);

            // executing Service
            // asynchronious execution via mqtt (it is awaitable, so you could use "await remoteAsyncExecTask" to receive result in async context)
            var remoteAsyncExecResult = await remoteServiceInstance.ExecuteAsync().ConfigureAwait(false);
            var radSi = (StreamInfo)remoteAsyncExecResult.Item1["Radiation"];
            var remoteResults = new double[remoteAsyncExecResult.Item2[radSi.Order - 1].Length / sizeof(double)];
            Buffer.BlockCopy(remoteAsyncExecResult.Item2[radSi.Order - 1], 0, remoteResults, 0,
                remoteAsyncExecResult.Item2[radSi.Order - 1].Length);
            return remoteResults;
        }
        //async Task<string>
        public async Task<string> DoIsovistAnalysis(List<GenBuildingLayout> buildings, Poly2D border, IEnumerable<Vector2d> analysisPoints)
        {
            

            var scId = CreateScenarioForIsovist(buildings, border);
            if (scId == null) throw new ConnectionManager.LucyCommunicationError("Could not create Scenario");

            // creating a service instance with input data
            //var points = new JArray();
            //var pp = analysisPoints.Select(p => p.X));
            //foreach (var p in pp)
            //{
            //    points.Add(p);
            //}

            var points = new MultiPoint(analysisPoints.Select(p => new Point(p.X,p.Y,0) as IPoint).ToArray());
            var s = new GeoJsonWriter().Write(points);
            var jobj = JObject.Parse(s);
            //var points = new JArray(new [] {new JArray(analysisPoints.Select(p => p.X).ToArray()), new JArray(analysisPoints.Select(p => p.Y).ToArray())});
            //Console.WriteLine(points.ToString());

            var remoteServiceInstance = LucyManager1.Manager.ConnectionManager.CreateServiceInstance("Isovist", scId,
                new Dictionary<string, object>()
                {
                {"inputVals", jobj} //new FeatureCollection(new Collection<IFeature>(new [] {(IFeature) new Feature((IGeometry)points, new AttributesTable())}))}
                });

            // executing Service
            // asynchronious execution via mqtt (it is awaitable, so you could use "await remoteAsyncExecTask" to receive result in async context)
            //var remoteAsyncExecResult = remoteServiceInstance.Execute(new Dictionary<string, object>()
            //{
            //    {"inputVals", jobj}
            //    //new FeatureCollection(new Collection<IFeature>(new [] {(IFeature) new Feature((IGeometry)points, new AttributesTable())}))}
            //});

            var remoteAsyncExecResult = await remoteServiceInstance.ExecuteAsync().ConfigureAwait(false);

            var rez = JObject.Parse(remoteAsyncExecResult.Item1["outputVals"].ToString());
            return rez["value"].Value<string>();
        }


        public int? CreateScenarioForIsovist(List<GenBuildingLayout> buildings, Poly2D border)
        {
            int? scenarioId = null;

            NetTopologySuite.Features.FeatureCollection featurecollection;
            System.Collections.ObjectModel.Collection<IFeature> features = new System.Collections.ObjectModel.Collection<IFeature>();

            AttributesTable buildingAttr = new AttributesTable();
            buildingAttr.AddAttribute("layer", "building");
            AttributesTable boundaryAttr = new AttributesTable();
            boundaryAttr.AddAttribute("layer", "boundary");

            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();

            NetTopologySuite.Geometries.Polygon[] polyArray = new NetTopologySuite.Geometries.Polygon[buildings.Count + 1];
            foreach (GenBuildingLayout building in buildings)
            {
                LinearRing r1 = new NetTopologySuite.Geometries.LinearRing(GeometryConverter.ToCoordinates(building.PolygonFromGen));
                NetTopologySuite.Geometries.Polygon p = new NetTopologySuite.Geometries.Polygon(r1);
                features.Add(new Feature(p, buildingAttr));
            }

            LinearRing r2 = new NetTopologySuite.Geometries.LinearRing(GeometryConverter.ToCoordinates(border));

            NetTopologySuite.Geometries.Polygon p2 = new NetTopologySuite.Geometries.Polygon(r2);

            features.Add(new Feature(p2, boundaryAttr));

            featurecollection = new FeatureCollection(features);

            //string geoJSONPoly = GeoJSONWriter.Write(featurecollection);
            //geoJSONPoly = geoJSONPoly.Substring(0, geoJSONPoly.Length - 1);
            //string createScenario4Geo = "{'action':'create_scenario','name':'test','geometry':{'GeoJSON':{'format':'GeoJSON','geometry':" + geoJSONPoly + "}}}}}";
            var req = JObject.FromObject(new
            {
                geometry = (JObject) (new Lucy.Geometry()
                {
                    Format = "GeoJSON",
                    Data = featurecollection
                })
            });
            var scenarioObj = LucyManager1.Manager.ConnectionManager.CreateScenario("UrbanPlanningTest", req);
            //try
            //{
            //    scenarioObj = GlobalConnection.CL.sendAction2Lucy(createScenario4Geo);
            //}
            //catch (Exception ex)
            //{
            //    System.Windows.Forms.MessageBox.Show(ex.InnerException.ToString());
            //}

            if (scenarioObj != null)
            {
                JProperty result = ((JProperty) (scenarioObj.First));
                if (result != null && result.Value["ScID"] != null)
                    scenarioId = (int) result.Value["ScID"];
                else
                {
                    Console.WriteLine("Failed to create scenario");
                    Console.WriteLine("Request: " + req);
                    Console.WriteLine("Response: " + scenarioObj);

                }
            }
            else
            {
                Console.WriteLine("Failed to create scenario");
                Console.WriteLine("Request: " + req);
                Console.WriteLine("Response is null");

            }

            return scenarioId;
        }

    }
}
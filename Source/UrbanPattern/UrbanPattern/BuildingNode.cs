﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tektosyne.Geometry;
using AForge;
using CPlan.Geometry;
using CPlan.VisualObjects;
using Tektosyne;

namespace CPlan.UrbanPattern
{
    public class BuildingNode
    {

        /// <summary>
        /// Gets or sets the link to the parent node
        /// </summary>
        public Int64 ParentIndex
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the attribute index.
        /// </summary>
        public Int64 Index
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the building related to the node.
        /// </summary>
        public GPolygon Building
        {
            get;
            set; 
        }

        /// <summary>
        /// Indicates if the further growth of a node is closed, if considered as parent.
        /// </summary>
        public bool Closed
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the attribute for angle.
        /// </summary>
        /// <value>(float) optimal area value.</value>
        public float Angle
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the attribute for length.
        /// </summary>
        public float Proportion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value for maximum connectivity - the maximum number of edges to or from a node.
        /// </summary>
        public int MinFreeNeigbours
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the status of the node, which indicates if it represents a building or an open space.
        /// -1 = undefined
        /// 0 = open space
        /// 1 = building
        /// </summary>
        public short SpaceStatus 
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value for maximum connectivity - the maximum number of edges to or from a node.
        /// </summary>
        public float Distance
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value for the area of a building.
        /// </summary>
        public float Area
        {
            get;
            set;
        }

        private short[] _sequence = new short[4];
        /// <summary>
        /// Sequence to consider the edges where new nodes/buildings can be added.
        /// </summary>
        public short[] Sequence        
        {
            get { return _sequence; }        
        }
        /// <summary>
        /// Points to the current position in the Sequence.
        /// </summary>
        public short Pointer
        {
            get;
            set;
        }

        public List<BuildingNode> Neighbours
        {
            get;
            set;
        }

        protected static ThreadSafeRandom rand = new ThreadSafeRandom();

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Initializes a new instance of the Node type with a specified index.
        /// </summary>
        /// <param name="idx">Index of the node.</param>
        public BuildingNode(int idx)
        {
            //this.Children = new List<InstructionNode>();
            this.Index = idx;
            this.Closed = false;
            this.Pointer = 0;
            InitializeSequence();
            Neighbours = new List<BuildingNode>();
        }

        /// <summary>
        /// Initializes a new instance of the Node type with a specified index.
        /// </summary>
        /// <param name="idx">Index of the node.</param>
        /// <param name="angle">Angle deviation from regular division.</param>
        /// <param name="length">Length of the new street segment.</param>
        public BuildingNode(int idx, float angle, float proportion, int freeNeighb, float distance, float area, Int64 parIdx, short status)
        {
            this.Index = idx;
            this.Angle = angle;
            this.Proportion = proportion;
            this.Closed = false;
            this.MinFreeNeigbours = freeNeighb;
            this.ParentIndex = parIdx;
            this.Distance = distance;
            this.Area = area;
            this.Pointer = 0;
            this.SpaceStatus = status;
            InitializeSequence();
            Neighbours = new List<BuildingNode>();
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="idx">Index of the node.</param>
        public BuildingNode(BuildingNode source)
        {
            this.Index = source.Index;
            this.Building = (GPolygon)source.Building.Clone();
            this.Angle = source.Angle;
            this.Proportion = source.Proportion;
            this.Closed = source.Closed;
            this.MinFreeNeigbours = source.MinFreeNeigbours;
            this.ParentIndex = source.ParentIndex;
            this.Distance = source.Distance;
            this.Area = source.Area;
            this.SpaceStatus = source.SpaceStatus;
            this.Pointer = source.Pointer;
            this._sequence = (short[])source.Sequence.Clone();
        }

        /// <summary>
        /// Clone the tree node.
        /// </summary>
        /// 
        /// <returns>Returns exact clone of the node.</returns>
        /// 
        public object Clone()
        {
            BuildingNode clone = new BuildingNode(this);
            return clone;
        }

        private void InitializeSequence()
        {
            List<short> tempIdx = new List<short> { 0, 1, 2, 3 };
            for (Int16 i = 0; i < _sequence.Length; i++)
            {
                int sel = rand.Next(tempIdx.Count);
                _sequence[i] = tempIdx[sel];
                tempIdx.RemoveAt(sel);
            }
        }

        public Line2D GetBuildingEdge(short idx)
        {
            Vector2D pt1 = new Vector2D();
            Vector2D pt2 = new Vector2D();
            switch (idx)
            {
                case 0:
                    pt1 = Building.Poly.PointsFirstLoop[0];
                    pt2 = Building.Poly.PointsFirstLoop[1];
                    break;
                case 1:
                    pt1 = Building.Poly.PointsFirstLoop[1];
                    pt2 = Building.Poly.PointsFirstLoop[2];
                    break;
                case 2:
                    pt1 = Building.Poly.PointsFirstLoop[2];
                    pt2 = Building.Poly.PointsFirstLoop[3];
                    break;
                case 3:
                    pt1 = Building.Poly.PointsFirstLoop[3];
                    pt2 = Building.Poly.PointsFirstLoop[0];
                    break;

            }
            Line2D selLine = new Line2D(pt1, pt2);
            return selLine;
        }

        public short GetEdgeIndex(Vector2D pt1, Vector2D pt2)
        {
            short idx = -1;
            if ((pt1 == Building.Poly.PointsFirstLoop[0] && pt2 == Building.Poly.PointsFirstLoop[1]) || (pt2 == Building.Poly.PointsFirstLoop[0] && pt1 == Building.Poly.PointsFirstLoop[1]))
            { 
                idx = 0;
            } else if ((pt1 == Building.Poly.PointsFirstLoop[1] && pt2 == Building.Poly.PointsFirstLoop[2]) || (pt2 == Building.Poly.PointsFirstLoop[1] && pt1 == Building.Poly.PointsFirstLoop[2]))
            {
                idx = 1;
            }
            else if ((pt1 == Building.Poly.PointsFirstLoop[2] && pt2 == Building.Poly.PointsFirstLoop[3]) || (pt2 == Building.Poly.PointsFirstLoop[2] && pt1 == Building.Poly.PointsFirstLoop[3]))
            {
                idx = 2;
            }
            else if ((pt1 == Building.Poly.PointsFirstLoop[3] && pt2 == Building.Poly.PointsFirstLoop[0]) || (pt2 == Building.Poly.PointsFirstLoop[3] && pt1 == Building.Poly.PointsFirstLoop[0]))
            {
                idx = 3;
            }               
            return idx;
        }

        public short Intersect(Vector2D pt1, Vector2D pt2)
        {
            short idx = -1;
            if ((pt1 == Building.Poly.PointsFirstLoop[0] && pt2 == Building.Poly.PointsFirstLoop[1]) || (pt2 == Building.Poly.PointsFirstLoop[0] && pt1 == Building.Poly.PointsFirstLoop[1]))
            {
                idx = 0;
            }
            else if ((pt1 == Building.Poly.PointsFirstLoop[1] && pt2 == Building.Poly.PointsFirstLoop[2]) || (pt2 == Building.Poly.PointsFirstLoop[1] && pt1 == Building.Poly.PointsFirstLoop[2]))
            {
                idx = 1;
            }
            else if ((pt1 == Building.Poly.PointsFirstLoop[2] && pt2 == Building.Poly.PointsFirstLoop[3]) || (pt2 == Building.Poly.PointsFirstLoop[2] && pt1 == Building.Poly.PointsFirstLoop[3]))
            {
                idx = 2;
            }
            else if ((pt1 == Building.Poly.PointsFirstLoop[3] && pt2 == Building.Poly.PointsFirstLoop[0]) || (pt2 == Building.Poly.PointsFirstLoop[3] && pt1 == Building.Poly.PointsFirstLoop[0]))
            {
                idx = 3;
            }
            return idx;
        }

        public void RemoveParentEdgeIndex(Line2D curRefLine)
        {
            short idx = GetEdgeIndex(curRefLine.Start, curRefLine.End); // it is alway the edge with index = 0 since this is the first one which is added...
            List<short> tempIdx = Sequence.ToList();
            _sequence = new short[_sequence.Length - 1];
            tempIdx.Remove(idx);
            _sequence = tempIdx.ToArray();    
        }

        public bool ContainsEdge(Line2D testLine)
        {
            bool contains = false;
            short testIdx = GetEdgeIndex(testLine.Start, testLine.End);
            if (testIdx != -1) contains = true;
            return contains;
        }

        public bool ContainsPoint(Vector2D refPoint)
        {
            bool contains = false;
            if (refPoint == Building.Poly.PointsFirstLoop[0] || refPoint == Building.Poly.PointsFirstLoop[1] || refPoint == Building.Poly.PointsFirstLoop[2] || refPoint == Building.Poly.PointsFirstLoop[3])
            {
                contains = true;
            }
            return contains;
        }

    }
}

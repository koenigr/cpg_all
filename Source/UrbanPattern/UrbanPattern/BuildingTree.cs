﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AForge;
using AForge.Genetic;
using CPlan.Geometry;
using CPlan.VisualObjects;
//using CPlan.Optimization;
using OpenTK;

namespace CPlan.UrbanPattern
{
    public class BuildingTree : ChromosomeBase 
    {
        public Vector2D Origin
        {
            get;
            set;
        }

        public List<GPolygon> BuildingLayout
        {
            get;
            set;
        }

        public List<GPolygon> InitalBuildings
        {
            get;
            set;
        }

        
        public Rect2D Border
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the number of nodes of a InstructionTree object.
        /// </summary>
        /// <value>(int) number of rooms.</value>
        public int TreeDepth
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the list of nodes of a InstructionTree object.
        /// </summary>
        /// <value>(List Node) leaves.</value>
        public List<BuildingNode> Nodes
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the highest index from all InstructionNodes
        /// </summary>
        public Int64 HighestIdx
        {
            get
            {
                Int64 hIdx = 0;
                foreach (BuildingNode node in this.Nodes)
                {
                    if (node.Index > hIdx) hIdx = node.Index;
                }
                return hIdx;
            }
            //private set;
        }

        /// <summary>
        /// Random generator used for chromosoms' generation.
        /// </summary>
        protected static ThreadSafeRandom rand = new ThreadSafeRandom();
        public int MaxSize { get; private set; }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Initializes a new instance of the InstructionTree type using the specified number of rooms.
        /// </summary>
        /// <param name="_rooms">(int) number of rooms.</param>
        public BuildingTree(int treeDepth, List<GPolygon> iniBuildings, Rect2D border)// : base(treeDepth)
        {
            this.Border = border;
            this.TreeDepth = treeDepth;
            this.InitalBuildings = iniBuildings;
            this.Initialize();        
        }

        /// <summary>
		/// Copy Constructor.
		/// </summary>
        protected BuildingTree(BuildingTree source)//  : base(source)
		{
            this.Nodes = new List<BuildingNode>();
            foreach (BuildingNode oldNode in source.Nodes)
            {
                BuildingNode newNode = new BuildingNode(oldNode);
                this.Nodes.Add(newNode);
            }
            this.Border = new Rect2D(source.Border);
            this.Origin = new Vector2D(source.Origin);
            this.MaxSize = source.MaxSize;
            //this.ShortestPathes = source.ShortestPathes;
            //this.Network = (Subdivision)source.Network.Clone();
            //this.InitalNetwork = (Subdivision)source.InitalNetwork.Clone();
            this.TreeDepth = source.TreeDepth;
		}

        /// <summary>
        /// Generate random chromosome value.
        /// </summary>
        /// 
        /// <remarks><para>Regenerates chromosome's value using random number generator.</para>
        /// </remarks>
        ///
        public override void Generate()
        {
            Initialize();
        }

        public void Initialize()
        {
            Nodes = new List<BuildingNode>();
            //MaxSize = Convert.ToInt32((TreeDepth*1.4) * (lengthRange + minLength));
            ////Border = new Rect2D(-1000.0, -1000.0, 2000.0, 2000.0); // border for the network
            //Border = new Rect2D(0.0, 0.0, MaxSize, MaxSize); // border for the network

            Origin = new Vector2D(0, 0); // global origin of coordinates for the network

            // --- add at least two seed nodes to create at least one seed edge
            BuildingNode seedNode;
            List<BuildingNode> parentNodes = new List<BuildingNode>();
            int idx = 0;
            foreach (GPolygon building in InitalBuildings)
            {
                //if (TestSeedNode(node))
                {
                    seedNode = new BuildingNode(idx);
                    idx++;
                    //Vector2D pt = building.GetCentroid();
                    //seedNode.Position = pt.ToPointD();
                    seedNode.Building = building;
                    seedNode.SpaceStatus = 1;
                    seedNode.Area = Convert.ToSingle(building.GetArea());
                    seedNode.MinFreeNeigbours = 1;
                    //float angle, length;
                    //GeoAlgorithms2D.CartesianToPolar(pt, out angle, out length);
                    //seedNode.Angle = angle;
                    //seedNode.Length = length;
                    //seedNode.MaxConnectivity = 4;
                    seedNode.ParentIndex = -1;
                    Nodes.Add(seedNode);
                    // -- start with x childs ---
                    for (int i = 0; i < 4; i++) parentNodes.Add(seedNode);
                }
            }

            CreateTree(parentNodes, TreeDepth);
        }

        public void CreateTree(List<BuildingNode> parents, int repetitions)
        {
            int angleRange = 0;
            int freeNeighb = 1;
            float distance = 0.5f;
            float proportion = 1;
            float area = Convert.ToSingle( InitalBuildings[0].GetArea() );
            int rndFraction = Convert.ToInt32 (area * 0.1f);
            List<BuildingNode> newParents = new List<BuildingNode>();
            while (repetitions > 0)
            {
                foreach (BuildingNode parNode in parents)
                {
                    BuildingNode curNode = new BuildingNode(Nodes.Count, rand.Next(angleRange) - angleRange / 2, proportion, 
                        freeNeighb, distance, area + rand.Next(rndFraction) - rndFraction / 2, parNode.Index, -1);
                    //InstructionNode parentNode = Nodes.Find(InstructionNode => InstructionNode.Index == parNode.ParentIdx);
                    //curNode.ParentIndex = parNode.Index;
                    //curNode.MinFreeNeigbours = 1;// rand.Next(_maxChilds - 1) + 2;
                    //double rndValue = rand.NextDouble();
                    // --- some streets doesn't go straight on...
                    //if (rndValue < 0.2)
                    //{
                    //    if (rndValue < 0.1) curNode.Angle += 90;
                    //    else curNode.Angle -= 90;
                    //}

                    Nodes.Add(curNode);

                    int nrArms = 3;// rand.Next(_maxChilds - 1) + 1;
                    for (int i = 0; i < nrArms; i++)
                    {
                        newParents.Add(curNode);
                    }
                }
                parents = new List<BuildingNode>();
                parents.AddRange(newParents);
                newParents = new List<BuildingNode>();

                repetitions--;
            }
        }

        /// <summary>
        /// Get the number of open spaces / free neigbours of a node
        /// </summary>
        /// <param name="refNode">The reference node for which the neighbours shall be found.</param>
        /// <param name="refEdge">The reference edge, which connects the refNode to its parent.</param>
        /// <returns>The number of free neighbours.</returns>
        public short GetNrFreeParentNeigbours(BuildingNode refNode)
        {
            short nrFreeNeighb = 0, counter = 4;
            List<BuildingNode> childNodes = Nodes.FindAll(BuildingNode => BuildingNode.ParentIndex == refNode.Index);
            foreach (BuildingNode childNode in childNodes)
            {
                if (childNode.SpaceStatus < 1) nrFreeNeighb++;
                else counter--;
            }
            // -- consider the parent's status --
            if (refNode.ParentIndex != -1)
            {
                BuildingNode parentNode = Nodes.Find(BuildingNode => BuildingNode.Index == refNode.ParentIndex);
                if (parentNode.SpaceStatus < 1) nrFreeNeighb++;
                else counter--;
            }
            // -- consider the not connected neighbouring buildings --
            //List<BuildingNode> additionalNodes = GetNotConnectedNeigbours(refNode, refEdge);
            //foreach (BuildingNode curNode in additionalNodes)
            //{
            //    if (curNode == Nodes.Find(BuildingNode => BuildingNode.Index == refNode.ParentIndex))
            //        continue;
            //    if (curNode.SpaceStatus < 1) nrFreeNeighb++;
            //    else counter--;
            //}

            return counter;// nrFreeNeighb;
        }

        public short GetNrFreeNeigbours(BuildingNode refNode)
        {
            short nrFreeNeighb = 0, counter = 4;
            List<BuildingNode> neigbours = refNode.Neighbours;
            foreach (BuildingNode neiNode in neigbours)
            {
                if (neiNode.SpaceStatus < 1) nrFreeNeighb++;
                else counter--;
            }
            return  counter;//nrFreeNeighb;//
        }

        /// <summary>
        /// Returns the required status for a new node/building.
        /// </summary>
        /// <param name="candidateNode"></param>
        /// <returns> True = needs to be an open space.
        ///           False = can be a building.  </returns>
        public bool GetRequiredSpaceStatus(BuildingNode candidateNode)
        {
            bool hasToBeFree = false;
            List<BuildingNode> neigbours = candidateNode.Neighbours;
            short buildNeighb;
            foreach (BuildingNode curNeigbour in neigbours)
            {
                buildNeighb = GetNrFreeNeigbours(curNeigbour);
                if (curNeigbour.MinFreeNeigbours >= buildNeighb)
                {
                    hasToBeFree = true;
                    return hasToBeFree;
                }
            }
            return hasToBeFree;
        }

        public void AdaptGeometry(BuildingNode curNode)
        {
            // -- check if the new edge intersects with an existing building
            Vector2D ptA = curNode.Building.Poly.PointsFirstLoop[2];
            Vector2D ptB = curNode.Building.Poly.PointsFirstLoop[3];
            Line2D aLine = new Line2D(ptA, ptB);
            foreach (BuildingNode curNeigbour in curNode.Neighbours)
            {
                Vector2D intersection = curNeigbour.Building.Intersect(aLine);
                if (intersection != null)
                {
                    Vector2D moveA = new Vector2D(0, 0);
                    Vector2D moveB = new Vector2D(0, 0);
                    Vector2D move = new Vector2D(0, 0);
                    Vector2D direction;
                    short cter = 0;
                    // -- which point is inside the polygon? --> move the point/line to the outside.
                    if (curNeigbour.Building.ContainsPoint(ptA))
                    {
                        moveA = intersection - ptA;
                        move = moveA;
                        cter++;
                    }
                    if (curNeigbour.Building.ContainsPoint(ptB))
                    {
                        moveB = intersection - ptB;
                        move = moveB;
                        cter++;
                    }
                    if (cter == 2) 
                    {
                        cter++;  // nixxxxxxxxxxxxxxxxxxxxxx ever happens????????????????
                    }
                    else
                    {
                        direction = (Vector2D)move.Clone();
                        direction.Normalize();
                        curNode.Building.Poly.PointsFirstLoop[2] = ptA + move + (direction * curNode.Distance);
                        curNode.Building.Poly.PointsFirstLoop[3] = ptB + move + (direction * curNode.Distance);
                        curNode.Building.FillColor = new Vector4(0.1f, 0.1f, 0.1f, 1);
                    }
                }
            }

            foreach (BuildingNode node in Nodes)
            {
                if (node.Building != null)
                {
                    if (node == curNode) continue;
                    List<Vector2D> pts = curNode.Building.Intersection(node.Building);
                    if (pts != null)
                    //if (pts.Count > 0)
                    {
                        curNode.Building = null;
                        curNode.Closed = true;
                        break;
                    }
                }

            }
            //foreach (BuildingNode curNeigbour in curNode.Neighbours)
            //{
            //    if (curNode.Building == null) continue;
            //    Vector2D ptNA = curNeigbour.Building.Poly.Points[2];
            //    Vector2D ptNB = curNeigbour.Building.Poly.Points[3];
            //    Line2D bLine = new Line2D(ptNA, ptNB);
            //    Vector2D intersectionB = curNode.Building.Intersect(bLine);
            //    if (intersectionB != null)
            //    {
            //        Vector2D moveB = new Vector2D(0, 0);
            //        if (curNode.Building.IsPointInside(ptNA))
            //        {
            //            //moveB = intersectionB - ptNA;
            //            //curNode.Building.Poly.Points[2] -= moveB;
            //            //curNode.Building.Poly.Points[3] -= moveB;
            //            //curNode.Building.FillColor = new Vector4(0.4f, 0.4f, 0.6f, 1);
            //            curNode.Building = null;
            //            curNode.Closed = true;
            //        }
            //        else if (curNode.Building.IsPointInside(ptNB))
            //        {
            //            //moveB = intersectionB - ptNB;
            //            //curNode.Building.Poly.Points[2] -= moveB;
            //            //curNode.Building.Poly.Points[3] -= moveB;
            //            //curNode.Building.FillColor = new Vector4(0.4f, 0.4f, 0.6f, 1);
            //            curNode.Building = null;
            //            curNode.Closed = true;
            //        }
            //    }
            //}

        }

        /// <summary>
        /// Register the new node to all neigbours and register the new node at all neigbours.
        /// </summary>
        /// <param name="nodeToRegister"></param>
        /// <param name="parentNode"></param>
        /// <param name="direction"></param>
        /// <param name="length"></param>
        public void RegisterNeighbour(BuildingNode nodeToRegister, BuildingNode parentNode, Vector2D direction, double length)
        {
            //length *= 0.75;
            if (nodeToRegister.Building == null) return;
            // -- reister to and from parent --
            parentNode.Neighbours.Add(nodeToRegister);
            nodeToRegister.Neighbours.Add(parentNode);
            List<BuildingNode> neigbours = new List<BuildingNode>();
            Vector2D center = nodeToRegister.Building.GetCentroid();            
            // -- not connected neighbours --
            for (int i = 1; i < 4; i++)
            {
                double rotateAngle = GeoAlgorithms2D.DegreeToRadian(i * 90);
                Vector2D tentDir = direction.Rotate(rotateAngle, new Vector2D(0,0));
                Line2D tentacle = new Line2D(center, center + (tentDir * length));
                foreach (BuildingNode curNode in Nodes)
                {
                    if (curNode.Building == null) continue;
                    if (curNode == parentNode) continue;
                    if (curNode == nodeToRegister) continue;
                    if (curNode.Building.IntersectLine(tentacle))
                    {
                        neigbours.Add(curNode);
                        break;
                    }
                }
            }
            // -- reister to and from all found not connected neighbours --
            foreach (BuildingNode newNeighb in neigbours)
            {
                newNeighb.Neighbours.Add(nodeToRegister);
                nodeToRegister.Neighbours.Add(newNeighb);
            }
        }


        private List<BuildingNode> GetNotConnectedNeigboursByPoints(BuildingNode refNode, Line2D refEdge)
        {
            double refDist, radius;
            List<BuildingNode> neigbours = new List<BuildingNode>();
            Vector2D center = refNode.Building.GetCentroid();
            foreach (BuildingNode curNode in Nodes)
            {
                if (curNode.Building != null)
                {
                    if ((curNode != refNode) && (!curNode.ContainsEdge(refEdge)))
                    {
                        if (curNode.ContainsPoint(refEdge.Start))
                        {
                            refDist = center.Distance(refEdge.Start);
                            radius = refDist + refDist * 0.1;
                            foreach (Vector2D pt in curNode.Building.Poly.PointsFirstLoop)
                            {
                                if (pt != refEdge.Start)
                                {
                                    double curDist = pt.Distance(center);
                                    if (curDist < radius) neigbours.Add(curNode);
                                }
                            }
                        }
                        else if (curNode.ContainsPoint(refEdge.End))
                        {
                            refDist = center.Distance(refEdge.End);
                            radius = refDist + refDist * 0.1;
                            foreach (Vector2D pt in curNode.Building.Poly.PointsFirstLoop)
                            {
                                if (pt != refEdge.End)
                                {
                                    double curDist = pt.Distance(center);
                                    if (curDist < radius) neigbours.Add(curNode);
                                }
                            }
                        }

                    }
                }
            }
            return neigbours;
        }

        /// <summary>
        /// Create a building layout from the building tree.
        /// </summary>
        public List<GPolygon> CreateBuildingLayout()
        {
            List<GPolygon> BildingLayout = new List<GPolygon>();
            
            //StreetPattern streetNetwork = new StreetPattern(this.Border);
            //streetNetwork.GrowStreetPattern(this, InitalNetwork);
            //streetNetwork.CleanUp();

            return BildingLayout;
        }

        /// <summary>
        /// Create new random chromosome (factory method)
        /// </summary>
        public override IChromosome CreateNew()
        {
            return new BuildingTree(TreeDepth, InitalBuildings, Border);
        }

        /// <summary>
        /// Clone the chromosome.
        /// </summary>
        public override IChromosome Clone()
        {
            return new BuildingTree(this);
        }


        /// <summary>
        /// Mutation operator
        /// </summary>
        public override void Mutate()
        {
            //List<InstructionNode> mutationNodes = this.Nodes.FindAll(InstructionNode => InstructionNode.ParentIndex <= this.TreeDepth);
            //int mutateRate = Convert.ToInt16(Math.Ceiling(mutationNodes.Count * 0.05));
            //double rndVal = rand.NextDouble();
            //for (int i = 0; i < mutateRate; i++) // number of mutations per tree
            //{
            //    InstructionNode mutationNode = mutationNodes[rnd.Next(mutationNodes.Count - 2) + 2]; // Wahrscheinlichkeit nach Level der Nodes einstellen
            //    if (rndVal < 0.25)
            //    {
            //        mutationNode.Angle = rnd.Next(_angleRange) - _angleRange / 2;
            //    }
            //    else if (rndVal < 0.5)
            //    {
            //        mutationNode.Length = rnd.Next(_lengthRange) + _minLength;
            //    }
            //    else
            //    {
            //        mutationNode.MaxConnectivity = rnd.Next(_maxChilds - 1) + 2;
            //    }
            //}
        }

        /// <summary>
        /// Crossover operator
        /// </summary>
        public override void Crossover(IChromosome pair)
        {
            MyCrossover(pair);
        }

        public void MyCrossover(IChromosome pair)
        {
            BuildingTree p = (BuildingTree)pair;
            // check for correct pair
            //if (p != null)
            //{
            //    // --- choose random nodes
            //    //int r = rand.Next(this.Nodes.Count - 3) + 3;
            //    //InstructionNode childThis = (InstructionNode)this.Nodes[r];
            //    //Int64 parentThisIdx = childThis.ParentIndex;
            //    //List<InstructionNode> branchThis = new List<InstructionNode>();
            //    //branchThis = this.FindBranch(branchThis, childThis.Index);
            //    //branchThis.Add(childThis);

            //    //int m = rand.Next(p.Nodes.Count - 3) + 3;
            //    //InstructionNode childOther = (InstructionNode)p.Nodes[m];
            //    //Int64 parentOtherIdx = childOther.ParentIndex;
            //    //List<InstructionNode> branchOther = new List<InstructionNode>();
            //    //branchOther = p.FindBranch(branchOther, childOther.Index);
            //    //branchOther.Add(childOther);

            //    // --- choose random nodes
            //    List<InstructionNode> thisSelNodes = this.Nodes.FindAll(InstructionNode => InstructionNode.ParentIndex < this.TreeDepth);
            //    if (thisSelNodes.Count < 4) return;
            //    int r = rand.Next(thisSelNodes.Count - 3) + 3;
            //    InstructionNode childThis = (InstructionNode)thisSelNodes[r];
            //    Int64 parentThisIdx = childThis.ParentIndex;
            //    List<InstructionNode> branchThis = new List<InstructionNode>();
            //    branchThis = this.FindBranch(branchThis, childThis.Index);
            //    branchThis.Add(childThis);

            //    List<InstructionNode> otherSelNodes = p.Nodes.FindAll(InstructionNode => InstructionNode.ParentIndex < p.TreeDepth);
            //    if (otherSelNodes.Count < 4) return;
            //    int m = rand.Next(otherSelNodes.Count - 3) + 3;
            //    InstructionNode childOther = (InstructionNode)otherSelNodes[m];
            //    Int64 parentOtherIdx = childOther.ParentIndex;
            //    List<InstructionNode> branchOther = new List<InstructionNode>();
            //    branchOther = p.FindBranch(branchOther, childOther.Index);
            //    branchOther.Add(childOther);

            //    // --- delete nodes from onw InstructionTree 
            //    //parentThis.Children.Remove(childThis);
            //    foreach (InstructionNode tNode in branchThis)
            //    {
            //        if (this.Nodes.Contains(tNode)) this.Nodes.Remove(tNode);
            //    }

            //    //parentOther.Children.Remove(childOther);
            //    foreach (InstructionNode oNode in branchOther)
            //    {
            //        if (p.Nodes.Contains(oNode)) p.Nodes.Remove(oNode);
            //    }

            //    // --- copy nodes to the other InstructionTree
            //    // -- change indices of all branch nodes --
            //    Int64 highestThisIdx = this.HighestIdx;
            //    foreach (InstructionNode tNode in branchOther)
            //    {
            //        Int64 newIdx = tNode.Index + highestThisIdx;
            //        tNode.Index = newIdx;
            //        tNode.ParentIndex = tNode.ParentIndex + highestThisIdx;
            //    }

            //    this.Nodes.AddRange(branchOther);
            //    childOther.ParentIndex = parentThisIdx;

            //    //parentOther.Children.Add(childThis);
            //    Int64 highestOtherIdx = p.HighestIdx;
            //    foreach (InstructionNode tNode in branchThis)
            //    {
            //        Int64 newIdx = tNode.Index + highestOtherIdx;
            //        tNode.Index = newIdx;
            //        tNode.ParentIndex = tNode.ParentIndex + highestOtherIdx;
            //    }
            //    p.Nodes.AddRange(branchThis);
            //    childThis.ParentIndex = parentOtherIdx;
            //}
        }
    }
}

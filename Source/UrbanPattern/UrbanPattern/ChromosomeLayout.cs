﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = ChromosomeLayout.cs 
//  Copyright (C) 2014/10/18  7:52 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using AForge;
using CPlan.Evaluation;
using CPlan.Geometry;
using CPlan.Optimization;
using OpenTK;

using FarseerPhysics;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;
using FarseerPhysics.Common.Decomposition;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Vector2 = netDxf.Vector2;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public class ChromosomeLayout : MultiPisaChromosomeBase
    {

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Private Fields

        /// <summary>
        /// Random generator used for chromosoms' generation.
        /// </summary>
        protected static ThreadSafeRandom rand = new ThreadSafeRandom();
        //----------------------------------------------------------------------
        private Random Rnd = new Random();
        private Geometry.Collision m_Collision = new Geometry.Collision();

        private World _world;// = new World(worldAABB, gravity, doSleep);
        //private List<GenBuildingLayout> _colObjects;
        //public bool FinisehdFitnessCalculation { get; set; }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        //----------------------------------------------------------------------
        /// <summary>
        /// Gets the list of Gens of a ChromosomeLayout object.
        /// </summary>
        /// <value> List of Gens.</value>
        public List<GenBuildingLayout> Gens
        {
            get;
            private set;
        }

        //----------------------------------------------------------------------
        public double ProportionRatio
        {
            get;
            set;
        }

        //----------------------------------------------------------------------
        public double Overlapping
        {
            get;
            set;
        }

        //----------------------------------------------------------------------
        /// <summary>
        /// Defines the minimum number of buildings (gens) of a chromosome.
        /// </summary>
        public int MinNrBuildings
        {
            get;
            set;
        }

        //----------------------------------------------------------------------
        /// <summary>
        /// Defines the maximum number of buildings (gens) of a chromosome.
        /// </summary>
        public int MaxNrBuildings
        {
            get;
            set;
        }

        //----------------------------------------------------------------------
        public Poly2D Border
        {
            get;
            set;
        }

        //----------------------------------------------------------------------
        public Rect2D BoundingRect
        {
            get;
            set;
        }

        //----------------------------------------------------------------------
        public List<Poly2D> Environment
        {
            get;
            set;
        }

        //----------------------------------------------------------------------
        public IEnumerable<double> SolarAnalysisResults 
        {
            get;
            set;
        }

        //----------------------------------------------------------------------
        public IsovistAnalysis IsovistField
        {
            get;
            set;
        }


        //----------------------------------------------------------------------
        public SolarAnalysis SolarField
        {
            get;
            set;
        }


        //----------------------------------------------------------------------
        /// <summary>
        /// Assigns values to the IsovistField.
        /// </summary>
        /// <param name="result">Values to be assigned.</param>
        public override void AssignResult(object result)
        {
            if (result.GetType() == typeof(IsovistAnalysis))
            {
                IsovistField = (IsovistAnalysis)result;
            }
        }

        //----------------------------------------------------------------------
        /// <summary>
        /// Defines the density of an area that have to remain constant after crossover and mutation.
        /// Value between 0 (empty) and 1 (full).
        /// </summary>
        public double Density
        {
            get;
            set;
        }

        //----------------------------------------------------------------------
        /// <summary>
        /// Defines the minimal length of a buildings side.
        /// The square root of the buildings area is multiplied by this value. The range is [0; 1].
        /// E.g. 0.5 means that the minimal value for a side is half of the length of a square with the same area.
        /// </summary>
        public double MinSideRatio
        {
            get;
            set;
        }

        //----------------------------------------------------------------------
        /// <summary>
        /// Active layouts are considered in the adaption loop and you can interact with it's gens.
        /// </summary>
        public bool Active { get; set; }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        //----------------------------------------------------------------------------------------------
        public ChromosomeLayout(Poly2D border, int minPolys, int maxPolys, double density, double minSideRatio, int identity = -1)
        {
            Rnd = new Random((int)DateTime.Now.Ticks);
            Active = true;
            m_ID = (identity == -1) ? m_LastID++ : identity;
            Generation = 0;
            Rnd = new Random();
            Density = density;
            MinSideRatio = minSideRatio;
            MinNrBuildings = minPolys;
            MaxNrBuildings = maxPolys;
            Border = new Poly2D(border);
            BoundingRect = GeoAlgorithms2D.BoundingBox(border.PointsFirstLoop);
            // --- initialize the chromosoma ---
            Initialize();
        }

        public ChromosomeLayout(Poly2D border, List<GenBuildingLayout> gens, double minSideRatio, int identity = -1)
        {
            Gens = gens;
            Active = true;
            m_ID = (identity == -1) ? m_LastID++ : identity;
            Generation = 0;
            Rnd = new Random();

            double areaCoverd = 0;
            foreach (GenBuildingLayout gen in Gens) areaCoverd += gen.Area;
            double density = areaCoverd / border.Area;
            Density = density;
            MinSideRatio = minSideRatio;
            MinNrBuildings = Gens.Count;
            MaxNrBuildings = Gens.Count;
            Border = new Poly2D(border);
            BoundingRect = GeoAlgorithms2D.BoundingBox(border.PointsFirstLoop);
            // --- initialize the chromosoma ---
            Initialize();
        }

        //----------------------------------------------------------------------------------------------
        // Copy constructor.
        public ChromosomeLayout(ChromosomeLayout previousChromosome)
        {
            int nrPolys = previousChromosome.Gens.Count;
            //StrategyParam = new double[4] { previousChromosome.StrategyParam[0], previousChromosome.StrategyParam[1], previousChromosome.StrategyParam[2], previousChromosome.StrategyParam[3]};
            Generation = previousChromosome.Generation;
            ProportionRatio = previousChromosome.ProportionRatio;
            Overlapping = previousChromosome.Overlapping;
            MinNrBuildings = previousChromosome.MinNrBuildings;
            MaxNrBuildings = previousChromosome.MaxNrBuildings;
            Border = previousChromosome.Border;
            BoundingRect = previousChromosome.BoundingRect;
            IsovistField = previousChromosome.IsovistField;
            Density = previousChromosome.Density;
            MinSideRatio = previousChromosome.MinSideRatio;
            Active = previousChromosome.Active;
            SolarAnalysisResults = previousChromosome.SolarAnalysisResults;

            m_ID = m_LastID++;
            Gens = new List<GenBuildingLayout>();
            //Springs.Clear();
            for (int i = 0; i < nrPolys; i++)
            {
                GenBuildingLayout curGen = new GenBuildingLayout(previousChromosome.Gens[i]);
                if (previousChromosome.Gens[i].Locked)
                    previousChromosome.Gens[i].Locked = true;
                Gens.Add(curGen);
            }
            m_fitness_values = new List<double>(previousChromosome.FitnessValues.Count);
            foreach (double value in previousChromosome.FitnessValues)
            {
                m_fitness_values.Add(value);
            }
            //for (int i = 0; i < previousChromosome.Springs.Count; i++)
            //{
            //    RSpring curSpring = new RSpring(previousChromosome.Springs[i]);
            //    Springs.Add(curSpring);
            //}
            _world = previousChromosome._world;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        //==================================================================
        public override void Generate()
        {
            Initialize();
        }

        //==================================================================
        public override IMultiChromosome CreateNew()
        {
            return new ChromosomeLayout(Border, MinNrBuildings, MaxNrBuildings, Density, MinSideRatio);  
        }

        //==================================================================
        public override IMultiChromosome Clone()
        {
            return new ChromosomeLayout(this);
        }

        //==================================================================
        /// <summary>
        /// Initializes the Chromosome for Layouts.
        /// </summary>
        public void Initialize()
        {           
            InitializeCollissionWorld();
   
            Gens = new List<GenBuildingLayout>();
            Rect2D bRect = BoundingRect;
            // --- create the Gens ---
            GenBuildingLayout curGen;
            double allArea = (Border.Area * Density);

            // --- Buidling area calculation ---
            List<double> bldAreas = new List<double>();
            double sum = 0;
            while (sum < allArea)
            {
                double newArea = Rnd.Next(ControlParameters.MinBldArea, ControlParameters.MaxBldArea);
                bldAreas.Add(newArea);
                sum += newArea;
                if (bldAreas.Count >= MaxNrBuildings)
                    break;
            }     

            // --- Building hights ---
            List<double> bldHight = new List<double>();
            for (int i = 0; i < bldAreas.Count; i++)
            {
                bldHight.Add(Rnd.Next(ControlParameters.MinBldHeigth, ControlParameters.MaxBldHeigth));
            }

            // -------------------------------------------

            // --- Create new buildings ---
            for (int i = 0; i < bldAreas.Count; i++)
            {
                curGen = new GenBuildingLayout(Rnd.Next((int)bRect.Width) + Border.Left,
                                               Rnd.Next((int)bRect.Height) + Border.Bottom,
                                               bRect, bldAreas[i], bldHight[i], i,
                                               _world);
                Gens.Add(curGen);
            }
            //SetScenarioChromosome();
            RemainDensity();

            Adapt();
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Initialize the collission detection world (Farseer Physics)
        /// </summary>
        public void InitializeCollissionWorld()
        {           
            _world = new World(new Microsoft.Xna.Framework.Vector2(0f, 0f));//9.82f));
            Settings.AllowSleep = true;
            Settings.VelocityIterations = 6;//Convert.ToInt32(numericUpDown_VelIterat.Value);
            Settings.PositionIterations = 10;//Convert.ToInt32(numericUpDown_PosIterat.Value);
            Settings.ContinuousPhysics = false;
            //Settings.UseFPECollisionCategories = true;

            Body border = new Body(_world);

            Vertices aBorders = new Vertices(Border.PointsFirstLoop.Length);
            for (int i = 0; i < Border.PointsFirstLoop.Length; i++)
            {
                Vector2d curPt = Border.PointsFirstLoop[i];
                aBorders.Add(new Microsoft.Xna.Framework.Vector2(Convert.ToSingle(curPt.X), Convert.ToSingle(curPt.Y)));
            }

            FixtureFactory.AttachLoopShape(aBorders, border);
            border.CollisionCategories = Category.All;
            border.CollidesWith = Category.All;
            border.Restitution = 1f;
            border.Friction = 0f;
        }  

        //============================================================================================================================================================================
        private void SetScenarioChromosome()
        {
            string scenario = ControlParameters.Scenario;
            if (scenario == "Cape Town Scenario")
            {

            }
            else
            {
                int idx = Gens.Count - 1;
                Gens[idx].Locked = true;
                Gens[idx].Open = true;
                Gens[idx].Attractor = true;
                Gens[idx].Position = new Vector2d(225, 400);
                Gens[idx].Rotation = 0;
                Gens[idx].Width = 50;
                Gens[idx].Height = 400;
            }
        }

        //==================================================================
        public void AdaptToNeighbours()
        {
            double forceMove = .1;
            double forceRotate = .05;
            // --- reset closest neighbours ---
            foreach (GenBuildingLayout curGen in Gens)
            {
                curGen.ClosestNeighbourDist = double.MaxValue;
                curGen.ClosestNeighbourGen = null;
            }

            // --- find closest neighbours ---
            for (int i = 0; i < Gens.Count; i++)
            {
                GenBuildingLayout curGenA = Gens[i];
                for (int j = i + 1; j < Gens.Count; j++)
                {
                    GenBuildingLayout curGenB = Gens[j];
                    double curDist = GeoAlgorithms2D.DistancePolygons(curGenA.PolyForCollission.BodyPoly, curGenB.PolyForCollission.BodyPoly);
                    
                    if (curDist < curGenA.ClosestNeighbourDist)
                    {
                        curGenA.ClosestNeighbourDist = curDist;
                        curGenA.ClosestNeighbourGen = curGenB;
                    }

                    if (curDist < curGenB.ClosestNeighbourDist)
                    {
                        curGenB.ClosestNeighbourDist = curDist;
                        curGenB.ClosestNeighbourGen = curGenA;
                    }
                }
            }

            // --- Attract each other ---
            foreach (GenBuildingLayout curGen in Gens)
            {
                if (curGen.ClosestNeighbourDist > 0.1)
                {
                    if (curGen.ClosestNeighbourGen != null)
                    {
                        Vector2d moveVect = curGen.ClosestNeighbourGen.PolyForCollission.Center - curGen.PolyForCollission.Center;
                        moveVect.Normalize();
                        moveVect = moveVect * curGen.ClosestNeighbourDist * forceMove;
                        curGen.Move(moveVect);
                    }
                }
            }

            // --- Adapt Rotation ---
            //foreach (GenBuildingLayout curGen in Gens)
            //{
            //    if (curGen.ClosestNeighbourGen != null)
            //    {
            //        double diffRotate = curGen.ClosestNeighbourGen.Rotation - curGen.Rotation;
            //        curGen.Rotation += diffRotate * forceRotate;
            //    }
            //}
     
        }


        //==================================================================
        public void AlignToBorder()
        {
            double forceMove = .05;
            double forceRotate = 1;//.5;//
            double maxDistToBorder = 10;// ControlParameters.            
            Line2D clostestBorder = null;

            foreach (GenBuildingLayout curGen in Gens)
            {
                double minDist = double.MaxValue;
                Poly2D curPoly = curGen.PolyForCollission.BodyPoly;
                for (int j = 0; j < Border.Edges.Count; j++)
                {
                    Line2D curEdge = Border.Edges[j];
                    double curDist = GeoAlgorithms2D.DistancePolygonToLine(curPoly, curEdge);
                    if (curDist < minDist)
                    {
                        minDist = curDist;
                        clostestBorder = curEdge;
                    }                   
                }
              
                Vector2d moveVect = new Vector2d();

                if (minDist < maxDistToBorder)
                {
                    if (minDist > 1.2)
                    {
                        if (clostestBorder != null)
                        {
                            // --- move towards the border ---
                            Vector2d toPoint;
                            double distToBorder = GeoAlgorithms2D.DistancePointToLine(curGen.PolyForCollission.BodyPoly.Center, clostestBorder.Start, clostestBorder.End, out toPoint);
                            moveVect = toPoint - curGen.PolyForCollission.BodyPoly.Center;
                            moveVect.Normalize();
                            curGen.Move(moveVect*distToBorder*forceMove);
                        }
                    }
                }
                if (ControlParameters.AlignToBorder)
                {
                    if (clostestBorder != null)
                    {
                        // --- adapt rotation to be parallel to the border ---
                        Vector2d refVect = curGen.PolyForCollission.BodyPoly.PointsFirstLoop[1] - curGen.PolyForCollission.BodyPoly.PointsFirstLoop[0];
                        Vector2d edgeVect = clostestBorder.End - clostestBorder.Start;
                        double angle = GeoAlgorithms2D.AngleBetweenD(edgeVect, refVect);
                        //double angle = GeoAlgorithms2D.AngleBetweenR(edgeVect, refVect);
                        if (!double.IsNaN(angle))
                        {
                            if (angle > 180) angle = 360 - angle;
                            //if (angle > 135) angle = 270 - angle;
                            if (angle > 90) angle = 90 - angle;
                            //if (angle > 45) angle = -angle;
                            //if (angle > 5)
                            //    curGen.Rotation += (angle * forceRotate);

                            double diff = angle*forceRotate; //- (curGen.Rotation);// += (angle * forceRotate);
                            curGen.Rotation -= diff;
                        }
                        else
                            curGen.Rotation = 45;
                    }
                }
                else if (ControlParameters.AlignTo45)
                {
                    curGen.Rotation = ControlParameters.FixedRotationValue;
                }
                // }
            }
    
        }

        //==================================================================
        public void AttractPolys(double force, double radius)
        {
            //Vector2d velocity = new Vector2d(0, 0);
            GenBuildingLayout actGen;
            Poly2D curAttractor;
            List<Poly2D> attractors = new List<Poly2D>();
            foreach (GenBuildingLayout curGen in Gens)
            {
                if (curGen.Attractor) attractors.Add(curGen.PolygonFromGen);
            }

            for (int i = 0; i < Gens.Count; i++)
            {
                actGen = Gens[i];
                if (actGen.Attractor)
                    continue;
                double[] dists = new double[attractors.Count];

                // -- calculate the distances --
                for (int j = 0; j < attractors.Count; j++)
                {
                    curAttractor = attractors[j];
                    dists[j] = actGen.PolygonFromGen.Distance(curAttractor);
                }

                // -- find shortest distance --
                int minIdx = -1;
                double minDist = double.MaxValue;
                for (int j = 0; j < dists.Length; j++)
                {
                    if (dists[j] < minDist)
                    {
                        minDist = dists[j];
                        minIdx = j;
                    }
                }

                if (minDist < radius)
                {
                    if (!actGen.Locked)
                    {
                        Vector2d intersection;
                        double dist = attractors[minIdx].Distance(actGen.PolygonFromGen, out intersection);
                        if (dist > 0 && minDist > 0)
                        {
                            intersection *= force;
                            actGen.Move(intersection);
                        }
                    }
                }
            }
        }

        //==================================================================
        public void RepelPolys(double force, double rate)
        {
            Vector2d velocity = new Vector2d(0, 0);
            GenBuildingLayout firstGen, secondGen;
            Vector2d translation;

            for (int i = 0; i < Gens.Count; i++)
            {
                firstGen = Gens[i];              
                for (int j = i + 1; j < Gens.Count; j++)
                {
                    if (Rnd.NextDouble() < rate)
                    {
                        secondGen = Gens[j];
                        Poly2D firstPoly = new Poly2D(firstGen.ToPoly2D());
                        Poly2D secondPoly = new Poly2D(secondGen.ToPoly2D());
                        firstPoly.Offset(firstGen.RepelOffset);
                        secondPoly.Offset(secondGen.RepelOffset);

                        PolygonCollisionResult r = m_Collision.PolygonCollision(firstPoly, secondPoly, velocity);
                        if (r.WillIntersect)
                        {
                            translation = velocity + r.MinimumTranslationVector;
                            // -- add random values to the translation vector to avoid blockin situation --
                            if (translation.X == 0 & translation.Y > 0) translation.X = (Rnd.NextDouble() - 0.5) * translation.Y;//Rnd.NextDouble() * translation.Y;//
                            else if (translation.Y == 0 & translation.X > 0) translation.Y = (Rnd.NextDouble() - 0.5) * translation.X;//Rnd.NextDouble() * translation.X;//
                            translation = translation * force;
                            if (!firstGen.Locked)
                                firstGen.Move(translation);
                            translation.X = -translation.X;
                            translation.Y = -translation.Y;
                            if (!secondGen.Locked)
                                secondGen.Move(translation);
                        }
                    }
                }
            }
        }

        //==================================================================
        public void RepelJumpPolys(double force, double rate)
        {
                    
            Vector2d velocity = new Vector2d(0, 0);
            GenBuildingLayout firstGen, secondGen;
            Vector2d translation;

            for (int i = 0; i < Gens.Count; i++)
            {
                firstGen = Gens[i];
                double firstGArea = firstGen.PolygonFromGen.Area;

                for (int j = i + 1; j < Gens.Count; j++)
                {
                    secondGen = Gens[j];
                    double testArea = Overlap(firstGen, secondGen);
                    if (testArea / firstGArea > 0.8)
                    {
                        firstGen.Position = new Vector2d(
                            Rnd.Next(-(int)BoundingRect.Width / 2, (int)BoundingRect.Width / 2) + Border.Center.X,
                            Rnd.Next(-(int)BoundingRect.Height / 2, (int)BoundingRect.Height / 2) + Border.Center.Y);
                    }

                    if (Rnd.NextDouble() < rate)
                    {
                        PolygonCollisionResult r = m_Collision.PolygonCollision(firstGen.ToPoly2D(), secondGen.ToPoly2D(), velocity);
                        if (r.WillIntersect)
                        {
                            translation = velocity + r.MinimumTranslationVector;
                            // -- add random values to the translation vector to avoid blockin situation --
                            if (translation.X == 0 & translation.Y > 0) translation.X = (Rnd.NextDouble() - 0.5) * translation.Y;//Rnd.NextDouble() * translation.Y;//
                            else if (translation.Y == 0 & translation.X > 0) translation.Y = (Rnd.NextDouble() - 0.5) * translation.X;//Rnd.NextDouble() * translation.X;//
                            translation = translation * force;
                            if (!firstGen.Locked)
                                firstGen.Move(translation);
                            translation.X = -translation.X;
                            translation.Y = -translation.Y;
                            if (!secondGen.Locked)
                                secondGen.Move(translation);
                        }
                    }
                }
            }
        }

        //==================================================================
        public void ChangeRotation(double force, double rate)
        {
            Vector2d velocity = new Vector2d(0, 0);
            GenBuildingLayout firstGen, secondGen;
            double divider;

            for (int i = 0; i < Gens.Count; i++)
            {
                firstGen = Gens[i];
                for (int j = i + 1; j < Gens.Count; j++)
                {
                    if (Rnd.NextDouble() < rate)
                    {
                        secondGen = Gens[j];
                        PolygonCollisionResult r = m_Collision.PolygonCollision(firstGen.ToPoly2D(), secondGen.ToPoly2D(), velocity);
                        if (r.WillIntersect && r.MinimumTranslationVector.Length > .1)
                        {
                            // -- virtual rotations -> to ensure minimal rotations for becomming parallel -- 
                            double firstVirtR = firstGen.Rotation;
                            divider = System.Math.Ceiling(firstVirtR / 90);
                            if (divider > 1)
                                firstVirtR -= (divider - 1) * 90;
                            if (firstVirtR > 45) firstVirtR -= 90;
                            if (firstVirtR < -45) firstVirtR += 90;

                            double secondVirtR = secondGen.Rotation;
                            divider = System.Math.Ceiling(secondVirtR / 90);
                            if (divider > 1)
                                secondVirtR -= (divider - 1) * 90;
                            if (secondVirtR > 45) secondVirtR -= 90;
                            if (secondVirtR < -45) secondVirtR += 90;

                            double diffRot = System.Math.Abs(firstVirtR - secondVirtR);
                            // -- adapt rotations --
                            diffRot *= force;
                            if (firstVirtR > secondVirtR)
                            {
                                if (!firstGen.Locked) firstGen.Rotation -= diffRot;
                                if (!secondGen.Locked) secondGen.Rotation += diffRot;
                            }
                            else
                            {
                                if (!firstGen.Locked) firstGen.Rotation += diffRot;
                                if (!secondGen.Locked) secondGen.Rotation -= diffRot;
                            }
                        }
                    }
                }
            }
        }

        //==================================================================
        public void BorderRotation(double force, double rate)
        {
            GenBuildingLayout gen;
            double divider, interArea;
            Poly2D boder = new Poly2D(Border.Points);
            List<Line2D> boderEdges = Border.Edges;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (Rnd.NextDouble() < rate)
                {
                    gen = Gens[i];

                    Poly2D poly = gen.PolygonFromGen;
                    double rectArea = poly.Area;
                    List<Poly2D> isect = boder.Intersection(poly);
                    if (isect == null) interArea = 0;
                    else
                    {
                        interArea = isect[0].Area;
                    }
                    double diffArea = rectArea - interArea;
                    if (diffArea > 10 & interArea > 0.1)
                    {
                        Line2D refLine = null;
                        foreach (Line2D iLine in boderEdges)
                        {
                            List<Vector2d> interPts = poly.Intersection(iLine);
                            if (interPts.Count > 0)
                            {
                                refLine = iLine;
                            }
                        }

                        if (refLine != null)
                        {
                            Vector2d refVector = refLine.End - refLine.Start;
                            // test quadrant 
                            if ((refVector.X < 0 & refVector.Y < 0) || (refVector.X > 0 & refVector.Y < 0))
                                refVector *= -1;

                            double refRotation = GeoAlgorithms2D.AngleBetweenD(refVector, new Vector2d(1, 0));

                            // -- virtual rotations -> to ensure minimal rotations for becomming parallel -- 
                            double firstVirtR = gen.Rotation;
                            divider = System.Math.Ceiling(firstVirtR / 90);
                            if (divider > 1)
                                firstVirtR -= (divider - 1) * 90;
                            if (firstVirtR > 45) firstVirtR -= 90;
                            if (firstVirtR < -45) firstVirtR += 90;

                            divider = System.Math.Ceiling(refRotation / 90);
                            if (divider > 1)
                                refRotation -= (divider - 1) * 90;
                            if (refRotation > 45) refRotation -= 90;
                            if (refRotation < -45) refRotation += 90;

                            double secondVirtR = refRotation;// 0;                          
                            double diffRot = System.Math.Abs(firstVirtR - secondVirtR);

                            // -- adapt rotations --
                            diffRot *= force;
                            if (firstVirtR > secondVirtR)
                            {
                                if (!gen.Locked)
                                    gen.Rotation -= diffRot;
                            }
                            else
                            {
                                if (!gen.Locked)
                                    gen.Rotation += diffRot;
                            }
                        }
                    }
                }
            }
        }

        //==================================================================
        public void ChangeProportion(double force, double rate)
        {
            GenBuildingLayout firstGen, secondGen;
            Vector2d velocity = new Vector2d(0, 0);
            Vector2d translation;

            for (int i = 0; i < Gens.Count; i++)
            {
                firstGen = Gens[i];
                for (int j = i + 1; j < Gens.Count; j++)
                {
                    secondGen = Gens[j];
                    PolygonCollisionResult r = m_Collision.PolygonCollision(firstGen.ToPoly2D(), secondGen.ToPoly2D(), velocity);
                    if (r.WillIntersect)
                    {
                        translation = (velocity + r.MinimumTranslationVector) * force;

                        if (!firstGen.Locked) VaryOnesSize(rate, firstGen, translation);
                        if (!secondGen.Locked) VaryOnesSize(rate, secondGen, translation);
                    }
                }
            }
        }

        //==============================================================================================
        private void VaryOnesSize(double rate, GenBuildingLayout curGen, Vector2d changer)
        {
            double width;
            double mag;
            changer.X = Math.Abs(changer.X);
            changer.Y = Math.Abs(changer.Y);
            mag = changer.Length;
            double multi = 5;

            if (!curGen.Locked)          
            {
                if (Rnd.NextDouble() < rate)
                {
                    if (curGen.CheckRot())
                    {
                        if (changer.X > changer.Y)
                        {
                            if (Rnd.NextDouble() < 0.9) width = curGen.Width - mag * multi;
                            else width = curGen.Width + mag * multi;
                        }
                        else
                        {
                            if (Rnd.NextDouble() < 0.9) width = curGen.Width + mag * multi;
                            else width = curGen.Width - mag * multi;
                        }
                    }
                    else
                    {
                        if (changer.X > changer.Y)
                        {
                            if (Rnd.NextDouble() < 0.9) width = curGen.Width + mag * multi;
                            else width = curGen.Width - mag * multi;
                        }
                        else
                        {
                            if (Rnd.NextDouble() < 0.9) width = curGen.Width - mag * multi;
                            else width = curGen.Width + mag * multi;
                        }
                    }
                    curGen.CheckWidtHeight(width);
                }
            }
        }

        //==================================================================
        public void CheckPolyBorder()
        {
            for (int i = 0; i < Gens.Count; i++)
            {
                GenBuildingLayout curGen = Gens[i];
                Poly2D curRect = curGen.PolygonFromGen;
                Vector2d delta;
                if (!curGen.Locked)
                {
                    double dist = Border.MaxDistance(curRect, out delta);
                    if (delta != null)
                    {
                        if (delta.Length > dist * 2)
                        {
                            //dist = Border.MaxDistance(curRect, out delta);
                            //break;
                        }
                        if (dist > 0)
                            curGen.Move(delta);
                    }
                }
            }
        }

        //==================================================================
        public void CheckBorder()
        {
            Vector2d translation = new Vector2d(0, 0);
            int faktor = 1;
            double damp = 1;

            for (int i = 0; i < Gens.Count; i++)
            {
                GenBuildingLayout curGen = Gens[i];
                Poly2D curPoly = curGen.PolygonFromGen;
                if (!curGen.Locked)
                {
                    // ---------------------------------------------------------------------------
                    if (curPoly.Left < Border.Left)
                    {
                        translation.X = (Border.Left - curPoly.Left) * damp / faktor;
                        translation.Y = 0;
                        curGen.Move(translation);
                    }
                    else if (curPoly.Right > Border.Right)
                    {
                        translation.X = (Border.Right - curPoly.Right) * damp / faktor;
                        translation.Y = 0;
                        curGen.Move(translation);
                    }
                    if (curPoly.Top > Border.Top)
                    {
                        translation.X = 0;
                        translation.Y = (Border.Top - curPoly.Top) * damp / faktor;
                        curGen.Move(translation);
                    }
                    else if (curPoly.Bottom < Border.Bottom)
                    {
                        translation.X = 0;
                        translation.Y = (Border.Bottom - curPoly.Bottom) * damp / faktor;
                        curGen.Move(translation);
                    }
                }
            }
        }

        //==================================================================
        public void CheckBorderByPoints()
        {
            const int faktor = 1;
            const double damp = 1;
            bool checkAgain = false;

            for (int i = 0; i < Gens.Count; i++)
            {
                GenBuildingLayout curGen = Gens[i];
                Poly2D curPoly = curGen.PolygonFromGen;
                if (!curGen.Locked)
                {
                    // ---------------------------------------------------------------------------
                    Vector2d translation = new Vector2d(0, 0);
                    for (int j = 0; j < curGen.Points.Count; j++)
                    {
                        Vector2d curVect = curGen.Points[j];
                        double deltaX, deltaY;
                        if (curVect.X < Border.Left)
                        {                           
                            deltaX = (Border.Left - curVect.X) / faktor;
                            if (deltaX > translation.X) translation.X = deltaX;
                        }
                        else if (curVect.X > Border.Right)
                        {
                            deltaX = (Border.Right - curVect.X) / faktor;
                            if (deltaX > translation.X) translation.X = deltaX;
                        }
                        if (curVect.Y > Border.Top)
                        {
                            deltaY = (Border.Top - curVect.Y) / faktor;
                            if (deltaY > translation.Y) translation.Y = deltaY;
                        }
                        else if (curVect.Y < Border.Bottom)
                        {
                            deltaY = (Border.Bottom - curVect.Y) / faktor;
                            if (deltaY > translation.Y) translation.Y = deltaY;
                        }
                    }
                    translation = translation * damp;
                    curGen.Move(translation);
                  
                    // -- random jumps --
                    List<Poly2D> isectPoly = curPoly.Intersection(Border);
                    double outArea;
                    if (isectPoly == null)
                    {
                        outArea = curPoly.Area;
                    }
                    else outArea = curPoly.Area - isectPoly[0].Area;
                    double ratio = outArea / curPoly.Area;
                    if (ratio > 0.9 && rand.NextDouble() < 0.1)
                    {
                        if (Border.Distance(curPoly) > 0)
                        {
                            curGen.Position = new Vector2d(
                                Rnd.Next(-(int)BoundingRect.Width / 4, (int)BoundingRect.Width / 4) + Border.Center.X,
                                Rnd.Next(-(int)BoundingRect.Height / 2, (int)BoundingRect.Height / 2) + Border.Center.Y);
                            checkAgain = true;
                        }
                    }
                }
            }
            if (checkAgain)
                CheckBorder();
        }

        //==============================================================================================
        public bool CheckBorder(Poly2D testRect)
        {
            bool isInside = true;
            double tolerance = 5;

            if (testRect.Left + tolerance < Border.Left)
            {
                isInside = false;
            }
            else if (testRect.Right - tolerance > Border.Right)
            {
                isInside = false;
            }
            if (testRect.Top - tolerance > Border.Top)
            {
                isInside = false;
            }
            else if (testRect.Bottom + tolerance < Border.Bottom)
            {
                isInside = false;
            }

            return isInside;
        }

        //==============================================================================================
        public void Jump(double jumpRate)
        {
            double rndVal;
            GenBuildingLayout curGen;
            Vector2d newPos = new Vector2d(0, 0);

            for (int i = 0; i < Gens.Count; i++)
            {
                curGen = Gens[i];
                if (!curGen.Locked)
                {
                    rndVal = Rnd.NextDouble();
                    if (rndVal < (jumpRate))
                    {
                        newPos.X = Rnd.NextDouble() * BoundingRect.Width  + Border.Center.X;
                        newPos.Y = Rnd.NextDouble() * BoundingRect.Height + Border.Center.Y;
                        curGen.Center = newPos;
                    }
                }
            }
        }

        //==============================================================================================
        public void RotateRandon(double rate, double sigmaValue)
        {
            GenBuildingLayout curGen;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (Rnd.NextDouble() < rate)
                {
                    curGen = Gens[i];
                    Helper.NewNormDistGenerator(); //m_normDist = new NormalDistribution(newGenerator);
                    Helper.NormDist.Sigma = sigmaValue;// overlap / 1000;  

                    if (!curGen.Locked)
                    {
                        double rotation = Helper.NormDist.NextDouble() - 1;
                        curGen.PolygonFromGen.Rotate(rotation);
                    }
                }
            }
        }

        //==============================================================================================
        public void MoveRandon(double rate, double sigmaValue)
        {
            GenBuildingLayout curGen;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (Rnd.NextDouble() < rate)
                {
                    curGen = Gens[i];
                    Vector2d dPos = new Vector2d(0, 0);
                    Helper.NewNormDistGenerator(); 
                    Helper.NormDist.Sigma = sigmaValue;

                    if (!curGen.Locked)
                    {
                        dPos.X = Helper.NormDist.NextDouble() - 1;
                        dPos.Y = Helper.NormDist.NextDouble() - 1;

                        Poly2D newRect = curGen.PolygonFromGen.Clone();
                        newRect.Move(dPos);
                        curGen.Move(dPos);
                    }
                }
            }
        }

        //==============================================================================================
        public void ReplaceGen(GenBuildingLayout oldGen, GenBuildingLayout newGen)
        {
            int idx = Gens.IndexOf(oldGen);
            Gens[idx] = new GenBuildingLayout(newGen);
        }

        //==============================================================================================
        public void VarySize(double rate)
        {
            GenBuildingLayout curGen;
            double min, width, rVal;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (Rnd.NextDouble() < rate)
                {
                    Helper.NormDist.Sigma = (int)System.Math.Round((decimal)(BoundingRect.Height + BoundingRect.Width) / (Gens.Count), 0);// 
                    curGen = Gens[i];
                    min = curGen.MinSide;
                    if (!curGen.Locked)
                    {
                        rVal = Helper.NormDist.NextDouble() - 1;
                        width = curGen.Width + rVal;
                        if (width < min)
                            width = min;      //-- Minimale Scalierung X
                        else if (width > BoundingRect.Width) width = BoundingRect.Width; //-- Maximale Scalierung X

                        curGen.CheckWidtHeight(width);

                    }
                }
            }
        }

        //==============================================================================================
        public void VarySizeFull(double rate)
        {
            GenBuildingLayout curGen;
            double min = 50.0;
            double width, rVal;
            //double height;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (Rnd.NextDouble() < rate)//
                {
                    Helper.NewNormDistGenerator();
                    Helper.NormDist.Sigma = (int)System.Math.Round((decimal)(BoundingRect.Height + BoundingRect.Width) / (Gens.Count), 0);
                    curGen = Gens[i];
                    if (!curGen.Locked)
                    {
                        rVal = Helper.NormDist.NextDouble() - 1;
                        width = curGen.Width + rVal;
                        if (width < min)
                            width = min;      //-- Minimale Scalierung X
                        else if (width > BoundingRect.Width) width = BoundingRect.Width; //-- Maximale Scalierung X

                        curGen.CheckWidtHeight(width);
                    }
                }
            }
        }

        //==============================================================================================
        public void VarySizeFull(double rate, double sigmaValue)
        {
            GenBuildingLayout curGen;
            double min = 50.0;
            double width, rVal;
            //double height;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (Rnd.NextDouble() < rate)
                {
                    Helper.NewNormDistGenerator();
                    Helper.NormDist.Sigma = sigmaValue;
                    curGen = Gens[i];
                    if (!curGen.Locked)
                    {
                        rVal = Helper.NormDist.NextDouble() - 1;
                        width = curGen.Width + rVal;
                        if (width < min)
                            width = min;      //-- Minimale Scalierung X
                        else if (width > BoundingRect.Width) width = BoundingRect.Width; //-- Maximale Scalierung X

                        curGen.CheckWidtHeight(width);

                    }
                }
            }
        }

        //==============================================================================================
        public double Overlap()
        {
            double overlapAreaSum = 0;

            for (int i = 0; i < Gens.Count; i++)
            {
                Poly2D poly1 = new Poly2D(Gens[i].PolygonFromGen);
                double rectArea = poly1.Area;
                double shapeArea;
                List<Poly2D> isectRect;
                for (int k = i + 1; k < Gens.Count; k++)
                {
                    Poly2D poly2 = new Poly2D(Gens[k].PolygonFromGen);
                    isectRect = poly1.Intersection(poly2);
                    if (isectRect == null) shapeArea = 0;
                    else shapeArea = isectRect[0].Area;
                    overlapAreaSum = overlapAreaSum + shapeArea;
                }
                Poly2D boder = new Poly2D(Border.Points);
                isectRect = boder.Intersection(poly1);
                if (isectRect == null) shapeArea = 0;
                else shapeArea = isectRect[0].Area;
                double diffArea = rectArea - shapeArea;
                overlapAreaSum += diffArea;
            } 
            Overlapping = overlapAreaSum;
            return overlapAreaSum;
        }

        //==============================================================================================
        public double Overlap(GenBuildingLayout firstGen, GenBuildingLayout secondGen)
        {
            double shapeArea;

            Poly2D poly1 = new Poly2D(firstGen.PolygonFromGen);
            Poly2D poly2 = new Poly2D(secondGen.PolygonFromGen);
            List<Poly2D> isectPoly = poly1.Intersection(poly2);
            if (isectPoly == null) shapeArea = 0;
            else shapeArea = isectPoly[0].Area;

            return shapeArea;
        }

        //==============================================================================================
        public double AdditionalDistFromOverlap(GenBuildingLayout fistGen, GenBuildingLayout secondGen)
        {
            double addDist = 0;
            RectangleF rect1, rect2;
            RectangleF isectRect;
            rect1 = new RectangleF((float)(fistGen.Points[0].X), (float)(fistGen.Points[0].Y), (float)(fistGen.Width), (float)(fistGen.Height));
            rect2 = new RectangleF((float)(secondGen.Points[0].X), (float)(secondGen.Points[0].Y), (float)(secondGen.Width), (float)(secondGen.Height));
            isectRect = RectangleF.Intersect(rect1, rect2);
            if (isectRect != null)
            {
                if (isectRect.Width < isectRect.Height) addDist = isectRect.Width;
                else addDist = isectRect.Height;
            }

            if (addDist <= ControlParameters.addDist) addDist = 0;
            else addDist = addDist - ControlParameters.addDist;
            return addDist;
        }

        //==============================================================================================
        public void EvaluationProportion()
        {
            GenBuildingLayout curGen;
            double sum = 0;
            for (int k = 0; k < Gens.Count; k++)
            {
                curGen = Gens[k];
                double ratio;
                if (curGen.Width <= curGen.Height)
                    ratio = curGen.Width / curGen.Height;
                else
                    ratio = curGen.Height / curGen.Width;
                sum += ratio;
            }
            ProportionRatio = sum / Gens.Count;
        }

        //==============================================================================================
        /// <summary>
        /// Mutation operator
        /// </summary>
        public override void Mutate()
        {
            // --- mutation operators ---
            int moveDist = (int)System.Math.Round((decimal)((BoundingRect.Height + BoundingRect.Width) / 8), 0); // NrBuildings), 0);

            double rate = 1.0 / MinNrBuildings;
            MoveRandon(rate/2, moveDist);
            Jump(rate / 2);
            VarySizeFull(rate / 4, moveDist);
            MoveRandon(rate, moveDist / 10);
            RotateRandon(rate / 2, moveDist / 10);
            VarySize(rate / 4);
            CheckBorder();
            CheckPolyBorder();
            CheckSizes(); 
        }

        //==============================================================================================
        public override void Adapt()
        {
            double repForce = ControlParameters.repForce;
            double repRate = ControlParameters.repRate;
            double propForce = ControlParameters.propForce;
            double propRate = ControlParameters.propRate;
            double repAdapt = ControlParameters.RepeatAdapt;

            for (int i = 0; i < repAdapt; i++)
            {   
                Adaption(propForce, propRate, repForce, repRate);       
            }
  
            //CheckBorder();
            //CheckPolyBorder();
            //CheckSizes();
        }

        //==============================================================================================
        /// <summary>
        /// Update the Gens after changes. Needed for Farseer Physics Collision detection.
        /// </summary>
        private void UpdateGensForCollission()
        {
            foreach (GenBuildingLayout curGen in Gens)
            {
                curGen.UpdateColliderPoly();
            }
        }

        //==============================================================================================
        protected void Adaption(double propForce, double propRate, double repelForce, double repelRate)
        {
            bool propChange = ControlParameters.propChange;
            bool rotate = ControlParameters.rotate;
            double rotForce = ControlParameters.rotForce;
            double rotRate = ControlParameters.rotRate;
            bool borderRot = ControlParameters.borderRot;
            double borderRotForce = ControlParameters.borderRotForce;
            double borderRotRate = ControlParameters.borderRotRate;
            //------------------------------------------------------------
            # region oldAdaption
            // --- rotation ---
            /*if (rotate)
            {
                ChangeRotation(rotForce, rotRate);
                if (borderRot)
                    BorderRotation(borderRotForce, borderRotRate);
            }

            CheckBorder();
            CheckPolyBorder();

            // --- attractor ---
            AttractPolys(ControlParameters.attractForce, ControlParameters.attractRadius);

            // --- proportion ---
            if (propChange) ChangeProportion(propForce, propRate);

            // --- repel ---
            RepelPolys(repelForce, repelRate);
            //RepelJumpPolys(repelForce, repelRate);
            */

            // --- new collission detection ---
            //CheckBorder();
            //CheckPolyBorder();
            //CheckSizes();
            //UpdateGensForCollission();
            # endregion           


            AlignToBorder();
            AdaptToNeighbours();
           
            CheckBorder();
            CheckPolyBorder();
            CheckSizes();
            UpdateGensForCollission();

            Collission();        
        }

        //===============================================================================================================
        public void Collission()
        {
            float timeStep = 1.0f;

            // This is our little game loop.
            int interat = 3; //Convert.ToInt32(numericUpDown_LoopIterat.Value);
            for (int i = 0; i < interat; ++i)
            {
                // Instruct the world to perform a single step of simulation. It is
                // generally best to keep the time step and iterations fixed.
                _world.Step(timeStep);
            }

            foreach (GenBuildingLayout curGen in Gens)
            {
                curGen.UpdateGenFromCollission();
            }

        }

        //==============================================================================================
     /*   protected void Adaption(double propForce, double propRate, double repelForce, double repelRate)
        {
            float timeStep = 1.0f / 1.0f;
            int velocityIterations = 4;
            int positionIterations = 1;

            bool propChange = ControlParameters.propChange;
            bool rotate = ControlParameters.rotate;
            double rotForce = ControlParameters.rotForce;
            double rotRate = ControlParameters.rotRate;
            bool borderRot = ControlParameters.borderRot;
            double borderRotForce = ControlParameters.borderRotForce;
            double borderRotRate = ControlParameters.borderRotRate;
            //------------------------------------------------------------

            // --- rotation ---
            //if (rotate)
            //{
            //    ChangeRotation(rotForce, rotRate);
            //    if (borderRot)
            //        BorderRotation(borderRotForce, borderRotRate);
            //}

            //CheckBorder();
            //CheckPolyBorder();
            // --- attractor ---
            AttractPolys(ControlParameters.attractForce, ControlParameters.attractRadius);

            // --- proportion ---
            //if (propChange) ChangeProportion(propForce, propRate);

            // --- repel ---
            //RepelPolys(repelForce, repelRate);
            //RepelJumpPolys(repelForce, repelRate);
            // This is our little game loop.
            for (int i = 0; i < 1; ++i)
            {
                // Instruct the world to perform a single step of simulation. It is
                // generally best to keep the time step and iterations fixed.
                WorldCur.Step(timeStep, velocityIterations, positionIterations);

                // Now print the position and angle of the body.
                //Vec2 position = body.GetPosition();
                //float angle = body.GetAngle();

                //Console.WriteLine("Step: {3} - X: {0}, Y: {1}, Angle: {2}", new object[] { position.X.ToString(), position.Y.ToString(), angle.ToString(), i.ToString() });
            }

            for (int i = 0; i < Gens.Count; i++)
            {
                GenBuildingLayout curGen = Gens[i];
                curGen.Rotation = 0;
                curGen.BodyCur.SetAngle(0);
                if (!curGen.Locked)
                    curGen.PositionFromBody( new Vector2d(curGen.BodyCur.GetPosition().X, curGen.BodyCur.GetPosition().Y) );
            }

        }
        */


        //==============================================================================================
        /// <summary>
        /// Reset the assignement of the Collission body and fisxture to the wold object of the chromosome.
        /// Needed after crossover operation.
        /// </summary> 
        public void ResetCollissionWordAssignment(ChromosomeLayout other)
        {
            foreach (GenBuildingLayout curGen in Gens)
            {
                curGen.ResetCollissionWoldAssignement(_world);
            }
            foreach (GenBuildingLayout curGen in other.Gens)
            {
                curGen.ResetCollissionWoldAssignement(other._world);
            }
        }

        public void RemoveBodysFromWorld(ChromosomeLayout other)
        {
            InitializeCollissionWorld();
            other.InitializeCollissionWorld();
        }

        //==============================================================================================
        /// <summary>
        /// Crossover operator
        /// </summary>
        public override void Crossover(IMultiChromosome pair)
        {
            CrossoverTopo(pair);
        }

        //==============================================================================================
        private void CrossoverTopo(IMultiChromosome pair)
        {
            ChromosomeLayout other = (ChromosomeLayout)pair;
            if (other != null)
            {
                //;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                ChromosomeLayout tmpThis = new ChromosomeLayout(this); // temporary old parents
                ChromosomeLayout tmpOther = new ChromosomeLayout(other);
                int lengthParent0 = Gens.Count();
                int lengthParent1 = other.Gens.Count();

                //int nrPolys = MinNrBuildings; 
                int diffPolys = System.Math.Abs(lengthParent0 - lengthParent1);
                int diffPolysThis = RndGlobal.Rnd.Next(diffPolys + 1);
                int diffPolysOther = RndGlobal.Rnd.Next(diffPolys + 1);
                int maxNr = System.Math.Max(Gens.Count(), other.Gens.Count());
                int nrPolys = System.Math.Min(Gens.Count(), other.Gens.Count());
                Gens = new List<GenBuildingLayout>();
                other.Gens = new List<GenBuildingLayout>();
                //---------------------------------------------------------------
                // --- crossover for locked gens - they need to be maintained ---
                for (int i = 0; i < maxNr; i++)
                {

                    if (tmpThis.Gens.Count > tmpOther.Gens.Count)
                    {
                        if (tmpThis.Gens[i].Locked)
                        {
                            Gens.Add(new GenBuildingLayout(tmpThis.Gens[i]));
                            other.Gens.Add(new GenBuildingLayout(tmpThis.Gens[i]));
                        }
                    }
                    else
                    {
                        if (tmpOther.Gens[i].Locked)
                        {
                            Gens.Add(new GenBuildingLayout(tmpOther.Gens[i]));
                            other.Gens.Add(new GenBuildingLayout(tmpOther.Gens[i]));
                        }
                    }
                }

                // --- crossover --------------------------------------
                for (int i = 0; i < nrPolys; i++)
                {
                    // --- crossover for NOT locked gens ---
                    if (!tmpOther.Gens[i].Locked && !tmpThis.Gens[i].Locked)
                    {
                        // exchange gens randomly
                        if (RndGlobal.Rnd.NextDouble() < 0.5)
                        {
                            Gens.Add(new GenBuildingLayout(tmpOther.Gens[i]));
                            other.Gens.Add(new GenBuildingLayout(tmpThis.Gens[i]));
                        }
                        else
                        {
                            Gens.Add(new GenBuildingLayout(tmpThis.Gens[i]));
                            other.Gens.Add(new GenBuildingLayout(tmpOther.Gens[i]));
                        }
                    }
                }
                // --------------------------------
                // --- fill remaining positions ---
                // --- reverse direction randomly -> may be good to have a random selection for the gens of the longer chromosome
                bool forward = (RndGlobal.Rnd.NextDouble() < 0.5) ? false : true;
                if (diffPolys > 0)
                {
                    ChromosomeLayout sourceChromo;
                    if (tmpThis.Gens.Count > tmpOther.Gens.Count)
                    {
                        sourceChromo = new ChromosomeLayout(tmpThis);
                    }
                    else  
                    {
                        sourceChromo = new ChromosomeLayout(tmpOther);
                    }

                    int diff;
                    if (forward)
                    {
                        diff = nrPolys + diffPolysOther-1;
                        for (int i = nrPolys-1; i < diff; i++)
                        {
                            if (!sourceChromo.Gens[i].Locked)
                            {
                                other.Gens.Add(new GenBuildingLayout(sourceChromo.Gens[i]));
                            }
                        }
                        diff = nrPolys + diffPolysThis-1;
                        for (int i = nrPolys-1; i < diff; i++)
                        {
                            if (!sourceChromo.Gens[i].Locked)
                            {
                                Gens.Add(new GenBuildingLayout(sourceChromo.Gens[i]));
                            }
                        }
                    }
                    else
                    {
                        diff = sourceChromo.Gens.Count - 1 - diffPolysOther;
                        for (int i = sourceChromo.Gens.Count - 1; i > diff; i--)
                        {
                            if (!sourceChromo.Gens[i].Locked)
                            {
                                other.Gens.Add(new GenBuildingLayout(sourceChromo.Gens[i]));
                            }
                        }
                        diff = sourceChromo.Gens.Count - 1 - diffPolysThis;
                        for (int i = sourceChromo.Gens.Count - 1; i > diff; i--)
                        {
                            if (!sourceChromo.Gens[i].Locked)
                            {
                                Gens.Add(new GenBuildingLayout(sourceChromo.Gens[i]));
                            }
                        }
                    }

                }

                // -- assign new Gen-IDs --
                for (int i = 0; i < Gens.Count; i++)
                {
                    Gens[i].Id = i;
                }
                for (int i = 0; i < other.Gens.Count; i++)
                {
                    other.Gens[i].Id = i;
                }

                // --- keep cover ratio constant ---
                RemainDensity();
                other.RemainDensity();

                // --- Reset the assignement of the Collission body and fisxture to a wold object of the chromosome ---
                RemoveBodysFromWorld(other);
                ResetCollissionWordAssignment(other);
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Remain the density of an area that is defined by the Density parameter.
        /// </summary>
        public void RemainDensity()
        {
            double tolerance = Gens.Count();
            // -- measure actual density --
            double isDensity = 0;
            for (int i = 0; i < Gens.Count(); i++)
            {
                GenBuildingLayout curGen = Gens[i];
                isDensity += curGen.PolygonFromGen.Area;
            }

            double shallArea = Border.Area * Density;
            double diffArea = shallArea - isDensity;
            ChromosomeLayout tempLayout = new ChromosomeLayout(this);           
            // -- growth is needed -----
            int maxIterations = 20;
            int counter = 0;
            while (System.Math.Abs(diffArea) > tolerance && counter < maxIterations)
            {
                counter++;
                List<int> removeIdx = new List<int>();
                double perGen = diffArea / tempLayout.Gens.Count();
                // assign to temp gen
                for (int i = 0; i < tempLayout.Gens.Count(); i++)
                {
                    GenBuildingLayout tempGen = tempLayout.Gens[i];
                    GenBuildingLayout curGen = Gens.Find(genBuildingLayout => genBuildingLayout.Id == tempGen.Id);
                    if (curGen.Locked)
                    {
                        removeIdx.Add(tempGen.Id);
                        continue;
                    }

                    bool setAreaFull = tempGen.SetArea(tempGen.Area + perGen); // test max/min side

                    double isDiff = tempGen.Area - curGen.Area;        
                    curGen.Area = tempGen.Area;

                    diffArea -= isDiff;

                    if (! setAreaFull) // --- remove the Gen, which cannot be changed anymore.
                    {
                        removeIdx.Add(tempGen.Id);
                    }
                   
                }
                foreach (int idx in removeIdx)
                {
                    GenBuildingLayout removeGen = tempLayout.Gens.Find(genBuildingLayout => genBuildingLayout.Id == idx);
                    tempLayout.Gens.Remove(removeGen);
                }

                if (counter >= maxIterations)
                    break;
            }

            // --- final check since there are some problems ---
            CheckSizes();
        }

        public void CheckSizes()
        {
            double tolerance = Gens.Count();
            // --- final check since there are some problems ---
            for (int i = 0; i < Gens.Count(); i++)
            {
                GenBuildingLayout curGen = Gens[i];
                if (curGen.Area > ControlParameters.MaxBldArea + tolerance) 
                    curGen.Area = ControlParameters.MaxBldArea;
                else if (curGen.Area < ControlParameters.MinBldArea - tolerance) 
                    curGen.Area = ControlParameters.MinBldArea;
            }
        }

        # endregion

    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = ColliderRect.cs 
//  Copyright (C) 2015/03/24  17:41 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Diagnostics;
using System.Linq;
using CPlan.Geometry;
using OpenTK;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;

namespace CPlan.UrbanPattern
{

    public class ColliderRect : Rect2D
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Private Fields

        private Body _body;
        //private Body _bodyExt;
        private Fixture fixture;
        //private Fixture fixtureExt;
        private double _offsetFactor = 0.2;
        private bool _allowOffsetShift = true;
        private float _shiftMode = 0f; // [-2; 2]; 0 = no scale; + scales to width, - to heigth.
        private short _counter;
        private bool _locked;

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        public bool Locked
        {
            get { return _locked; }
            set
            {
                _locked = value;
                if (_locked)
                {
                    _body.BodyType = BodyType.Static;
                }
                else
                {
                    _body.BodyType = BodyType.Dynamic;
                    _body.SleepingAllowed = true;
                    _body.IgnoreCCD = true;
                    _body.Friction = 0.0f;
                    _body.AngularDamping = 0.01f;
                    _body.LinearDamping = 0.01f;
                    _body.FixedRotation = true;
                }
            }
        }

        /// <summary>
        /// Rotaion in radians.
        /// </summary>
        public float Rotation
        {
            get; set;
        }

        public Body ColliderBody
        {
            get;
            protected set;
        }

        public Poly2D BodyPoly
        {
            get
            {
                double offsetDist = Math.Sqrt(this.Area) * _offsetFactor;
                Rect2D tmpRect = this.Clone();
                tmpRect.Width += offsetDist * 2;
                tmpRect.Height += offsetDist * 2;

                tmpRect.Width += offsetDist * _shiftMode;
                tmpRect.Height += offsetDist * -_shiftMode;
                tmpRect.Center = this.Center;

                Poly2D tmpShape = new Poly2D(tmpRect.Points);
                tmpShape.Rotate(Rotation);
                return tmpShape;
            }
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        public ColliderRect(World world, Rect2D basicShape, float rotate)
            : base(basicShape)
        {
            Rotation = rotate;
            Microsoft.Xna.Framework.Vector2 position = new Microsoft.Xna.Framework.Vector2(Convert.ToSingle(basicShape.Center.X), Convert.ToSingle(basicShape.Center.Y));
            _body = new Body(world, position, Rotation);
            
            // --- define Box2d body ----------------
            if (Locked)
            {
                _body.BodyType = BodyType.Static;
            }
            else
            {
                _body.BodyType = BodyType.Dynamic;
                _body.SleepingAllowed = true;
                _body.IgnoreCCD = true;
                _body.Friction = 0.0f;
                _body.AngularDamping = 0.01f;
                _body.LinearDamping = 0.01f;
                _body.FixedRotation = true;
            }

            // -- we change the distance polygon by shiftMode --
            double offsetDist = Math.Sqrt(this.Area) * _offsetFactor;
            Rect2D tmpRect = this.Clone();
            tmpRect.Width += offsetDist*2;
            tmpRect.Height += offsetDist*2;

            tmpRect.Width += offsetDist * _shiftMode;
            tmpRect.Height += offsetDist * -_shiftMode;
            tmpRect.Center = this.Center;

            Poly2D tmpShape = new Poly2D(tmpRect.Points);
            // -- the fixture is larger than the rectangle to ensure distances between buildings --> offset.
            //double offsetDist = Math.Sqrt(tmpShape.Area) * _offsetFactor;
            //tmpShape.Offset(offsetDist); 
            tmpShape.Position = new Vector2d(0, 0);
            Vertices polyVertis = new Vertices();
            foreach (Vector2d curVect in tmpShape.PointsFirstLoop)
                polyVertis.Add(new Microsoft.Xna.Framework.Vector2(Convert.ToSingle(curVect.X), Convert.ToSingle(curVect.Y)));

            float density = 1.0f;
            PolygonShape shapeDef = new PolygonShape(polyVertis, density);
            fixture = _body.CreateFixture(shapeDef);
            //fixture.CollisionCategories = Category.Cat1;
            //fixture.CollidesWith = Category.Cat1;


            // Now tell the dynamic body to compute it's mass properties based on its shape.
            //_body.Mass = Convert.ToSingle(this.Area);// .SetMassFromShapes();
             //BodyCur.SetMass( new MassData());   
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        //===============================================================================================================
        /// <summary>
        /// Update the rectangle and rotation after Farseer Physics Collission.
        /// </summary>
        public void UpdatePos()
        {
            // --- rotation ----------------------------------------------------------------
            Rotation = _body.Rotation;

            // ---------------------------------------------------------------------------------------------------
            // --- distribute the movement vector to proportion and position change ---
            double dampMove = ControlParameters.repForce; //1.0f; // large one increase the move vector, smaller decrease it.
            Vector2d moveVect = new Vector2d(_body.Position.X, _body.Position.Y) - this.Center;
            Vector2d damMoveVect = moveVect * dampMove;
            this.Center += damMoveVect;
            //_body.Position = new Microsoft.Xna.Framework.Vector2(Convert.ToSingle(this.Center.X), Convert.ToSingle(this.Center.Y));

            // ---------------------------------------------------------------------------------------------------
            // --- apply scaling - use the rotated norm body ---
            Rect2D scaledRect = this.Clone();
            scaledRect.Center = new Vector2d(0, 0);
         
            // --- proportion change ---
            Vector2d rotatedFoce = GeoAlgorithms2D.RotateVector(-Rotation, GeoAlgorithms2D.Centroid(scaledRect.Points.ToList()), moveVect);
            double forceScale = ControlParameters.propForce;// 0.5;
            Vector2d scaleVect =  rotatedFoce * forceScale;
            VarySize(scaleVect, ref scaledRect);
            //shiftMode = 2;
            // --- various offset modes for spacings ---


            double offsetDist = Math.Sqrt(scaledRect.Area) * _offsetFactor;
            Rect2D tmpRect = scaledRect.Clone();
            tmpRect.Width += offsetDist * 2;
            tmpRect.Height += offsetDist * 2;
            
            tmpRect.Width += offsetDist * _shiftMode;
            tmpRect.Height += offsetDist * -_shiftMode;
            tmpRect.Center = scaledRect.Center;

            Poly2D tmpShape = new Poly2D(tmpRect.Points);
            tmpShape.Offset(offsetDist);

            // ---------------------------------------------------------------------------------------------------
            Vertices polyVertis = new Vertices();
            foreach (Vector2d curVect in tmpShape.PointsFirstLoop)
            {
                polyVertis.Add(new Microsoft.Xna.Framework.Vector2(Convert.ToSingle(curVect.X), Convert.ToSingle(curVect.Y)));
            }

            // ---------------------------------------------------------------------------------------------------
            float density = 1.0f;
            PolygonShape shapeDef = new PolygonShape(polyVertis, density);
            _body.DestroyFixture(fixture);
            
            fixture = _body.CreateFixture(shapeDef);
            // ---------------------------------------------------------------------------------------------------
            // -- create the new rectangle --
            Rect2D newRect = new Rect2D(scaledRect);
            newRect.Center = new Vector2d(_body.Position.X, _body.Position.Y);
            this.CreateNew(newRect);

        }

        //==============================================================================================
        /// <summary>
        /// Reset the assignement to a world. Needed after crossover.
        /// </summary>
        /// <param name="world">The world to which the body and fixture shall be assigned to.</param>
        public void ResetWoldAssignement(World world)
        {
            Microsoft.Xna.Framework.Vector2 position = new Microsoft.Xna.Framework.Vector2(Convert.ToSingle(this.Center.X), Convert.ToSingle(this.Center.Y));      
            _body = new Body(world, position, Rotation);

            // --- define Box2d body ----------------
            if (Locked)
            {
                _body.BodyType = BodyType.Static;
            }
            else
            {
                _body.BodyType = BodyType.Dynamic;
                _body.SleepingAllowed = true;
                _body.IgnoreCCD = true;
                _body.Friction = 0.0f;
                _body.AngularDamping = 0.01f;
                _body.LinearDamping = 0.01f;
                _body.FixedRotation = true;
            }
            //Body newBody = new Body(world, position, Rotation);
            //// --- define Box2d body ----------------
            //newBody.BodyType = _body.BodyType;
            //newBody.SleepingAllowed = _body.SleepingAllowed;
            //newBody.IgnoreCCD = _body.IgnoreCCD;
            //newBody.Friction = _body.Friction;
            //newBody.AngularDamping = _body.AngularDamping;
            //newBody.LinearDamping = _body.LinearDamping;
            //newBody.FixedRotation = _body.FixedRotation;
            //newBody.Mass = _body.Mass;

            //_body.DestroyFixture(fixture);
            //_body = newBody.Clone();

            // -- we change the distance polygon by shiftMode --
            double offsetDist = Math.Sqrt(this.Area) * _offsetFactor;
            Rect2D tmpRect = this.Clone();
            tmpRect.Width += offsetDist * 2;
            tmpRect.Height += offsetDist * 2;

            tmpRect.Width += offsetDist * _shiftMode;
            tmpRect.Height += offsetDist * -_shiftMode;
            tmpRect.Center = this.Center;

            Poly2D tmpShape = new Poly2D(tmpRect.Points);
            // -- the fixture is larger than the rectangle to ensure distances between buildings --> offset.
            //double offsetDist = Math.Sqrt(tmpShape.Area) * _offsetFactor;
            //tmpShape.Offset(offsetDist); 
            tmpShape.Position = new Vector2d(0, 0);
            Vertices polyVertis = new Vertices();
            foreach (Vector2d curVect in tmpShape.PointsFirstLoop)
                polyVertis.Add(new Microsoft.Xna.Framework.Vector2(Convert.ToSingle(curVect.X), Convert.ToSingle(curVect.Y)));

            float density = 1.0f;
            PolygonShape shapeDef = new PolygonShape(polyVertis, density);

            // --- reset fixture ---
            _body.DestroyFixture(fixture);
            fixture = _body.CreateFixture(shapeDef);           
        }

        //==============================================================================================
        /// <summary>
        /// Receive an update for rectangle and rotation for the Farseer Physics Collission.
        /// </summary>
        public void UpdateFixture(Rect2D newShape, float rotation)
        {
            if (Locked)
            {
                _body.BodyType = BodyType.Static;
            }
            Rotation = rotation;
            _body.Rotation = Rotation;
            Microsoft.Xna.Framework.Vector2 position = new Microsoft.Xna.Framework.Vector2(Convert.ToSingle(newShape.Center.X), Convert.ToSingle(newShape.Center.Y));
            _body.Position = position;

            //-- Define shape for the fixture --
            double offsetDist = Math.Sqrt(newShape.Area) * _offsetFactor;
            Rect2D tmpRect = newShape.Clone();
            tmpRect.Width += offsetDist * 2;
            tmpRect.Height += offsetDist * 2;

            tmpRect.Width += offsetDist * _shiftMode;
            tmpRect.Height += offsetDist * -_shiftMode;
            tmpRect.Center = newShape.Center;

            Poly2D tmpShape = new Poly2D(tmpRect.Points);
            //double offsetDist = Math.Sqrt(tmpShape.Area) * _offsetFactor;
            //tmpShape.Offset(offsetDist);
            tmpShape.Position = new Vector2d(0, 0);
            Vertices polyVertis = new Vertices();
            foreach (Vector2d curVect in tmpShape.PointsFirstLoop)
                polyVertis.Add(new Microsoft.Xna.Framework.Vector2(Convert.ToSingle(curVect.X), Convert.ToSingle(curVect.Y)));

            float density = 1.0f;
            PolygonShape shapeDef = new PolygonShape(polyVertis, density); 
            _body.DestroyFixture(fixture);
            fixture = _body.CreateFixture(shapeDef);
            _body.Friction = 0.0f;
            _body.AngularDamping = 0.01f;
            _body.LinearDamping = 0.01f;

        }

        //==============================================================================================
        /// <summary>
        /// Change the size of a rectangle.
        /// </summary>
        /// <param name="changer">Vector containing the force.</param>
        /// <param name="toChangeRect">Rectangle to be changed.</param>
        private void VarySize(Vector2d changer, ref Rect2D toChangeRect)
        {
            if (Locked)
                return;

            double width;
            double mag;
            float thresh = 0.0001f;
            short threshChange = 2;
            changer.X = Math.Abs(changer.X);
            changer.Y = Math.Abs(changer.Y);
            mag = changer.Length;

            if (changer.X > changer.Y)
            {
                width = toChangeRect.Width - mag;
                if (changer.Length > thresh)
                {
                    _counter--;
                    if (_counter < -threshChange)
                    {                  
                        _shiftMode -= 2f; // -- smaller steps are also possible - results in more differentiated spacings... --
                        if (_shiftMode < -2) _shiftMode = -2;
                    }
                }
            }
            else
            {
                width = toChangeRect.Width + mag;
                if (changer.Length > thresh)
                {
                    _counter++;
                    if (_counter > threshChange)
                    {
                        _shiftMode += 2f;
                        if (_shiftMode > +2) _shiftMode = +2;
                    }
                }
            }

            double fixArea = toChangeRect.Area;
            double height;
            double limboHeight;
            double limboWidth;

            if (width < ControlParameters.MinBldSide)         //-- Minimale Scalierung X
            {
                width = ControlParameters.MinBldSide;
            }
            else if (width > ControlParameters.MaxBldSide)    //-- Maximale Scalierung X
            {
                width = ControlParameters.MaxBldSide;
            }
            height = (fixArea / width);
            limboHeight = height;
            limboWidth = width;
            if (limboHeight < ControlParameters.MinBldSide)
            {
                limboHeight = ControlParameters.MinBldSide;
                limboWidth = (fixArea / limboHeight);
            }
            if (limboHeight > ControlParameters.MaxBldSide)
            {
                limboHeight = ControlParameters.MaxBldSide;
                limboWidth = (fixArea / limboHeight);
            }

            // execute new sizes
            if ((limboHeight * limboWidth) == fixArea)
            {
                if (!(limboWidth > ControlParameters.MinBldSide && limboHeight > ControlParameters.MinBldSide &&
                      limboWidth < ControlParameters.MaxBldSide && limboHeight < ControlParameters.MaxBldSide))
                {
                    // -- cut or extend the sides to fit into min/max values --
                    if (limboWidth < ControlParameters.MinBldSide)
                    {
                        limboWidth = ControlParameters.MinBldSide;
                    }
                    if (limboHeight < ControlParameters.MinBldSide)
                    {
                        limboHeight = ControlParameters.MinBldSide;
                    }
                    if (limboWidth > ControlParameters.MaxBldSide)
                    {
                        limboWidth = ControlParameters.MaxBldSide;
                    }
                    if (limboHeight > ControlParameters.MaxBldSide)
                    {
                        limboHeight = ControlParameters.MaxBldSide;
                    }
                }

                double diffHeight = toChangeRect.Height - limboHeight;
                double diffWidth = toChangeRect.Width - limboWidth;
                toChangeRect.Height = limboHeight; // -diffHeight / 2;
                toChangeRect.Width = limboWidth; // -diffWidth / 2;

                toChangeRect.Move(new Vector2d(diffWidth / 2, -diffHeight / 2));

            }
        }

        # endregion

    }
}

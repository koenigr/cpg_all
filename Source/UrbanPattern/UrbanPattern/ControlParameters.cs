﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = ControlParameters.cs 
//  Copyright (C) 2014/10/18  7:52 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.ComponentModel;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public class RndGlobal
    {
        public static Random Rnd = new Random((int)DateTime.Now.Ticks);
    }

    [Serializable]
    public enum FieldForVisual { SolarAnalysis, IsovistAnalysis };

    [Serializable]
    public static class ControlParameters
    {
        //============================================================================
        # region Building Layout
        //----------------------
        public static double repForce = 1;
        public static double repRate = 1;

        public static bool propChange = true;
        public static double propForce = 0.2;
        public static double propRate = 1;
        public static double rotForce = 0.25;
        public static double rotRate = 1;

        public static bool rotate = true;
        public static bool borderRot = true;
        public static double borderRotForce = 1.0;
        public static double borderRotRate = 1.0;

        public static double attractForce = 0.1;
        public static double attractRadius = 20;

        public static int MinNrBuildings = 10;
        public static int MaxNrBuildings = 20;
        public static double Density = 0.5;
        public static double MinSideRatio = 0.5;
        public static double MinArea = 200;
        public static double MaxArea = 1000;
        public static double MinSide = 3;
        public static int MinBldArea = 50;
        public static int MaxBldArea = 150;
        public static int MinBldSide = 5;
        public static int MaxBldSide = 10;
        public static int MinBldHeigth = 10;
        public static int MaxBldHeigth = 50;
        public static int FloorHeigth = 3;
       
        public static string Scenario = "";
        public static int RepeatAdapt = 30;
        public static int IsovistCellSize = 10;

        public static int density = 50;
        public static int roomsNr = 50;
        public static double thresH = roomsNr * 1.25;

        public static double sDamp = 0.5;
        public static int addDist = 50;

        public static bool jumps = true;
        public static bool instantUpdate = false;

        public static bool AlignToBorder;
        public static int FixedRotationValue;
        public static bool AlignTo45;
        public static int MinBlockSize;
        public static FieldForVisual VisualiseAnalysis = FieldForVisual.IsovistAnalysis;

        # endregion;

        public static int popSize = 1;
        public static double pressVal = 2;
        public static double crossRate = 0.75;
        public static Boolean doCross = true;
        public static Boolean elite = false;
        public static Boolean plusSel = true;
        public static int p = 2;

        //============================================================================
        # region StreetNetwork
        //----------------------

        public static int MaxConnectivity;
        public static int MaxRndAngle;
        public static int MinDistance;
        public static int TreeDepth = 7;
        public static bool Choice = true;
        public static bool Centrality = false;
        public static int AngleRange = 10;
        public static int LengthRange = 300;
        public static int MinLength = 110;
        public static int MaxChilds = 4;
        public static int LineWidth = 2;
        
        # endregion;

        public static string DisplayMeasure = "White";
        
    }

    [Serializable]
    public class ViewControlParameters
    {
        //==========================================================================================================
        [Category("Collission"), Description("Force to repel the objects")]
        public double RepelForce
        {
            get { return ControlParameters.repForce; }
            set { ControlParameters.repForce = value; }
        }
        [Category("Collission"), Description("Repel rate")]
        public double RepelRate
        {
            get { return ControlParameters.repRate; }
            set { ControlParameters.repRate = value; }
        }

        [Category("Collission"), Description("Change proportion")]
        public bool PropChange
        {
            get { return ControlParameters.propChange; }
            set { ControlParameters.propChange = value; }
        }

        [Category("Collission"), Description("Force to change the proportion")]
        public double PropForce
        {
            get { return ControlParameters.propForce; }
            set { ControlParameters.propForce = value; }
        }
        [Category("Collission"), Description("Proportion change rate")]
        public double PropRate
        {
            get { return ControlParameters.propRate; }
            set { ControlParameters.propRate = value; }
        }

        [Category("Collission"), Description("Rotation")]
        public bool Rotate
        {
            get { return ControlParameters.rotate; }
            set { ControlParameters.rotate = value; }
        }
        [Category("Collission"), Description("Force for rotation")]
        public double RotForce
        {
            get { return ControlParameters.rotForce; }
            set { ControlParameters.rotForce = value; }
        }
        [Category("Collission"), Description("Rotation rate")]
        public double RotRate
        {
            get { return ControlParameters.rotRate; }
            set { ControlParameters.rotRate = value; }
        }

        [Category("Collission"), Description("Rotation by border collission")]
        public bool BorderRot
        {
            get { return ControlParameters.borderRot; }
            set { ControlParameters.borderRot = value; }
        }
        [Category("Collission"), Description("Force for rotation by border collission")]
        public double BorderRotForce
        {
            get { return ControlParameters.borderRotForce; }
            set { ControlParameters.borderRotForce = value; }
        }
        [Category("Collission"), Description("Rotation rate by border collission")]
        public double BorderRotRate
        {
            get { return ControlParameters.borderRotRate; }
            set { ControlParameters.borderRotRate = value; }
        }

        [Category("Collission"), Description("Force for rotation")]
        public double AttractForce
        {
            get { return ControlParameters.attractForce; }
            set { ControlParameters.attractForce = value; }
        }
        [Category("Collission"), Description("Rotation rate")]
        public double AttractRadius
        {
            get { return ControlParameters.attractRadius; }
            set { ControlParameters.attractRadius = value; }
        }

        //==========================================================================================================
        [Category("Layout"), Description("Building desnity in %")]
        public int Density
        {
            get { return ControlParameters.density; }
            set { ControlParameters.density = value; }
        }
        [Category("Layout"), Description("Number of houses")]
        public int Buildings_Nr
        {
            get { return ControlParameters.roomsNr; }
            set { ControlParameters.roomsNr = value; }
        }
        [Category("Layout"), Description("Update Isovist field after moving a object")]
        public bool UpdateIsovist
        {
            get { return ControlParameters.instantUpdate; }
            set { ControlParameters.instantUpdate = value; }
        }

        //==========================================================================================================
        [Category("Optimization"), Description("Allow jumping objects")]
        public bool Jumping
        {
            get { return ControlParameters.jumps; }
            set { ControlParameters.jumps = value; }
        }

        [Category("Optimization"), Description("Population size")]
        public int PopSize
        {
            get { return ControlParameters.popSize; }
            set { ControlParameters.popSize = value; }
        }
        [Category("Optimization"), Description("Population preasure")]
        public double Pressure
        {
            get { return ControlParameters.pressVal; }
            set { ControlParameters.pressVal = value; }
        }
        [Category("Optimization"), Description("Crossover rate")]
        public double CrossoverRate
        {
            get { return ControlParameters.crossRate; }
            set { ControlParameters.crossRate = value; }
        }
        [Category("Optimization"), Description("Activate crossover")]
        public Boolean Crossover
        {
            get { return ControlParameters.doCross; }
            set { ControlParameters.doCross = value; }
        }
        [Category("Optimization"), Description("Number of parents for crossover")]
        public int pCross
        {
            get { return ControlParameters.p; }
            set { ControlParameters.p = value; }
        }

    }
}

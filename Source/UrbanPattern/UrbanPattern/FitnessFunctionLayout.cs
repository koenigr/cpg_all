﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = FitnessFunctionLayout.cs 
//  Copyright (C) 2014/10/18  7:52 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using CPlan.Evaluation;
using CPlan.Geometry;
using CPlan.Optimization;
using System.Threading.Tasks;
//using CommunicateToLucy;
using GeoAPI.Geometries;
using Lucy;
using OpenTK;
using NetTopologySuite.Features;
using NetTopologySuite.IO;
using NetTopologySuite.Geometries;
using Newtonsoft.Json.Linq;
using TimeSpan = System.TimeSpan;
using CPlan.CommunicateToLucy;
using System.IO;
using CPlan.VisualObjects;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public class FitnessFunctionLayout : IMultiFitnessFunction
    {
        private Object thisLock = new Object();

        private int _dimensions = 5;
        public int Dimensions { get{return _dimensions;} }
        public CalculationType.Method RunVia { get; set; }
        public CalculationType.Method RunVia_Solar { get; set; }

        private IsovistAnalysis _isovistField = null;
        private SolarAnalysis _solarAnalysis = null;
        private string mqtt_topic;
        private string mqtt_topic_solar;
        private ChromosomeLayout curChromo;
        private bool lucyResultsAvailable = false;
        private bool lucyResultsAvailable_Solar = false;

        private bool calculateSolar = false;

        public FitnessFunctionLayout()
        {
            RunVia = CalculationType.Method.Local;
            RunVia_Solar = CalculationType.Method.Local;
        }

        public FitnessFunctionLayout(CalculationType.Method runVia, CalculationType.Method runVia_Solar, bool calcSolar = false)
        {
            RunVia = runVia;
            RunVia_Solar = runVia_Solar;
            calculateSolar = calcSolar;
        }

        async Task<List<double>> IMultiFitnessFunction.Evaluate(IMultiChromosome chromosome, bool calcSolar = false)
        {
           // calculateSolar = calcSolar;

            List<double> fitnessValues = new List<double>();
            //ChromosomeLayout 
            curChromo = (ChromosomeLayout)chromosome;

            IsovistAnalysis(curChromo);
            SolarAnalysis(curChromo);

            if (RunVia == CalculationType.Method.Lucy)
            {
                while (!lucyResultsAvailable)
                    System.Threading.Thread.Sleep(100);
            }
            if (RunVia_Solar == CalculationType.Method.Lucy)
            {
                while (!lucyResultsAvailable_Solar)
                    System.Threading.Thread.Sleep(100);
            }

            // ---intermediate check - the error is somewhere else ---
            if (curChromo == null || curChromo.IsovistField == null || curChromo.IsovistField.Compactness == null)
            {
                Console.WriteLine("ups!");
            }
            if (!double.IsNaN(curChromo.IsovistField.Compactness[0]))
            {
            //------------------------------------------------------//
            // --- minimization ??? ---
            // -- add above number of _dimensions!
                if (curChromo.IsovistField.Area.Sum() == 0) fitnessValues.Add(double.MaxValue);
                else 
                    fitnessValues.Add(Math.Abs(curChromo.Overlap()));

                if (curChromo.IsovistField.Area.Sum() == 0) fitnessValues.Add(double.MaxValue);
                else 
                    fitnessValues.Add((1 / curChromo.IsovistField.Area.Where(n => (!double.IsNaN(n) && n >= 0)).Max()));

                if (curChromo.IsovistField.Occlusivity.Sum() == 0) fitnessValues.Add(double.MaxValue);
                else 
                    fitnessValues.Add((curChromo.IsovistField.Occlusivity.Where(n => (!double.IsNaN(n) && n >= 0)).Average()));

                if (curChromo.IsovistField.Compactness.Sum() == 0) fitnessValues.Add(double.MaxValue);
                else 
                    fitnessValues.Add((1 / curChromo.IsovistField.Compactness.Where(n => (!double.IsNaN(n) && n >= 0)).Average()));

                if (curChromo.IsovistField.MinRadial.Sum() == 0) fitnessValues.Add(double.MaxValue);
                else 
                    fitnessValues.Add((1 / curChromo.IsovistField.MinRadial.Where(n => (!double.IsNaN(n) && n >= 0)).Average()));
                 
            } 
            else
            {
                for (int f = 0; f < _dimensions; f++) fitnessValues.Add(double.MaxValue);
            }

            ServiceManager.RemoveMqttTopic(mqtt_topic);
            ServiceManager.RemoveMqttTopic2(mqtt_topic_solar);

            return fitnessValues;
        }


        public void IsovistTest(ChromosomeLayout cc)
        {
            IsovistAnalysis(cc);
        }

        //============================================================================================================================================================================
/* *ac*        private async Task<ChromosomeLayout> IsovistAnalysis(ChromosomeLayout curChromo)*/
        private void IsovistAnalysis(ChromosomeLayout curChromo)
        {
            lucyResultsAvailable = false;

            List<Line2D> obstLines = new List<Line2D>();
            List<Poly2D> obstPolys = new List<Poly2D>();
            if (curChromo != null)
            {
                Poly2D isovistBorder = curChromo.Border;
                Rect2D borderRect = GeoAlgorithms2D.BoundingBox(isovistBorder.PointsFirstLoop);
                List<GenBuildingLayout> buildings = curChromo.Gens;

                // test environmental polygons
                if (curChromo.Environment != null)
                    foreach (Poly2D envPoly in curChromo.Environment)
                    {
                        if (envPoly.Distance(isovistBorder) <= 0)
                        {
                            obstLines.AddRange(envPoly.Edges.ToList());
                            obstPolys.Add(envPoly);
                        }
                    }

                // add the buildings
                foreach (GenBuildingLayout curBuilding in buildings)
                {
                    if (!curBuilding.Open)
                    {
                        obstLines.AddRange(curBuilding.PolygonFromGen.Edges.ToList());
                        obstPolys.Add(curBuilding.PolygonFromGen);
                    }
                }

                obstLines.AddRange(isovistBorder.Edges);

                float cellSize = ControlParameters.IsovistCellSize;
                _isovistField = new IsovistAnalysis(isovistBorder, obstPolys, cellSize);
                
                TimeSpan calcTime1; // calcTime2;
                DateTime start = DateTime.Now;


                if (RunVia == CalculationType.Method.Local)
                {
                    _isovistField.Calculate(0.05f);
                    _isovistField.Finished = true;
                }
                else if (RunVia == CalculationType.Method.Lucy)
                {
                    lock (thisLock)
                    {
                        lucyResultsAvailable = false;

                        // -- create Scenario --
                        int scenarioID = createScenario(buildings, isovistBorder);
                        if (scenarioID >= 0)
                        {
                            MultiPoint gridPointsJ = GeometryConverter.ToMultiPoint(_isovistField.AnalysisPoints);

                            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
                            string geoJSONPoints = GeoJSONWriter.Write(gridPointsJ);

                            // create Service -> return mqtt-topic
                            string msg = "{'action':'create_service','inputs':{'inputVals':" + geoJSONPoints + "},'classname':'Isovist','ScID':" + scenarioID + "}";

                            ConfigSettings configInfo = CPlan.CommunicateToLucy.GlobalConnection.CL.sendCreateService2Lucy(msg, scenarioID);

                            if (configInfo != null)
                            {
                                mqtt_topic = configInfo.mqtt_topic;

                                ServiceManager.AddConfig(mqtt_topic, configInfo);
                                ServiceManager.AddID(curChromo.Indentity, mqtt_topic);
                                ServiceManager.AddConfig(mqtt_topic, configInfo);


                                // -- start calculation service --

                                if (mqtt_topic != null)
                                {
                                    ServiceManager.AddMqttTopic(mqtt_topic);
                                    ServiceManager.mqttClient.MqttMsgPublishReceived += mqttClient_MqttMsgPublishReceived;
                                    ServiceManager.mqttClient.Publish(mqtt_topic, System.Text.Encoding.UTF8.GetBytes("RUN"));
                                }
                            }
                            _isovistField.Finished = false;
                        }

                    }

                }
                else
                {
                    _isovistField.Calculate(0.05f);
                    _isovistField.Finished = true;
                }

                curChromo.IsovistField = _isovistField;
            }
        }


        private void SolarAnalysis(ChromosomeLayout curChromo)
        {
            if (!calculateSolar)
            {
                lucyResultsAvailable_Solar = true;
                return;
            }

            lucyResultsAvailable_Solar = false;

            List<Line2D> obstLines = new List<Line2D>();
            List<Poly2D> obstPolys = new List<Poly2D>();
            if (curChromo != null)
            {
                Poly2D isovistBorder = curChromo.Border;
                Rect2D borderRect = GeoAlgorithms2D.BoundingBox(isovistBorder.PointsFirstLoop);
                List<GenBuildingLayout> buildings = curChromo.Gens;

                // test environmental polygons
                if (curChromo.Environment != null)
                    foreach (Poly2D envPoly in curChromo.Environment)
                    {
                        if (envPoly.Distance(isovistBorder) <= 0)
                        {
                            obstLines.AddRange(envPoly.Edges.ToList());
                            obstPolys.Add(envPoly);
                        }
                    }

                // add the buildings
                foreach (GenBuildingLayout curBuilding in buildings)
                {
                    if (!curBuilding.Open)
                    {
                        obstLines.AddRange(curBuilding.PolygonFromGen.Edges.ToList());
                        obstPolys.Add(curBuilding.PolygonFromGen);
                    }
                }



                obstLines.AddRange(isovistBorder.Edges);

                float cellSize = ControlParameters.IsovistCellSize;
                _isovistField = new IsovistAnalysis(isovistBorder, obstPolys, cellSize);
                
                TimeSpan calcTime1; // calcTime2;
                DateTime start = DateTime.Now;

                if (RunVia_Solar == CalculationType.Method.Local)
                {
                    _isovistField.Calculate(0.05f);
                    _isovistField.Finished = true;
                }
                else if (RunVia_Solar == CalculationType.Method.Lucy)
                {
                    lock (thisLock)
                    {
                        lucyResultsAvailable_Solar = false;

                        // -- create Scenario --
                        int scenarioID = createScenarioSolar(buildings, isovistBorder);
                        if (scenarioID >= 0)
                        {
                            string hdriFilename = "CA Cumulative Sky - Year 2012 23 Minute step.hdr";
                            var pathToHdriFile = CPlan.UrbanPattern.Properties.Settings.Default.PathToHDRI + hdriFilename;
                            if (!File.Exists(pathToHdriFile))
                            {
                                pathToHdriFile = Path.GetFullPath(Environment.CurrentDirectory + "\\CA Cumulative Sky - Year 2012 23 Minute step.hdr");
                                if (!File.Exists(pathToHdriFile))
                                {
                                    //pathToHdriFile = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "\\CA Cumulative Sky - Year 2012 23 Minute step.hdr"); ;
                                    pathToHdriFile = Path.GetFullPath(Environment.CurrentDirectory + "\\data\\HDRI\\CA Cumulative Sky - Year 2012 23 Minute step.hdr");
                                    if (!File.Exists(pathToHdriFile))
                                    {
                                        System.Windows.Forms.MessageBox.Show("There is no valid path to a HDRI file!");
                                        return;
                                    }
                                }
                            }

                            var hdri = File.ReadAllBytes(pathToHdriFile);

   //                         System.Windows.Forms.MessageBox.Show("1");
                                                var sf = new CPlan.VisualObjects.ScalarField(curChromo.Border.PointsFirstLoop
                                           .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
                                           cellSize, new Vector3d(0, 0, 1));
                    GGrid _smartGrid = new GGrid(sf, "solarAnalysis");
                
                //else if (_smartGrid.ScalarField == null)
                //{
                //    _smartGrid.ScalarField = new ScalarField(_activeMultiLayout.Border.PointsFirstLoop
                //                            .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
                //                            cellSize, new Vector3d(0, 0, 1));
                //}

                IEnumerable<Vector3d> gridNormales = Enumerable.Repeat(_smartGrid.ScalarField.Normal, _smartGrid.ScalarField.GridSize);
                IEnumerable<Vector3d> gridPoints = _smartGrid.ScalarField.Grid.ToList();
     //           System.Windows.Forms.MessageBox.Show("2");
                foreach (GenBuildingLayout building in buildings)
                {
       //             System.Windows.Forms.MessageBox.Show("3");
/*                    var bpoints = building.PolygonFromGen.PointsFirstLoop;
                    var bcenter = new Vector3d(building.PolygonFromGen.PointsFirstLoop.Mean());
                    bcenter.Z = building.BuildingHeight / 2;
                    building.SFDerived = true;
                    building.FieldTypeName = "solarAnalysis";
                    building.SideGrids = new ScalarField[1 + bpoints.Length];
                    building.SideGrids[0] = new ScalarField(bpoints
                        .Select(v => new Vector3d(v.X, v.Y, building.BuildingHeight)).ToArray(),
                        cellSize, new Vector3d(0, 0, 1));

         //           System.Windows.Forms.MessageBox.Show("4");
                    for (int i = 1; i <= bpoints.Length; i++)
                    {
                        var tpoints = new Vector3d[]
                        {
                            new Vector3d(bpoints[i - 1].X, bpoints[i - 1].Y, 0),
                            new Vector3d(bpoints[i - 1].X, bpoints[i - 1].Y, building.BuildingHeight),
                            new Vector3d(bpoints[i%bpoints.Length].X, bpoints[i%bpoints.Length].Y, building.BuildingHeight),
                            new Vector3d(bpoints[i%bpoints.Length].X, bpoints[i%bpoints.Length].Y, 0)
                        };
                        building.SideGrids[i] = new ScalarField(tpoints, cellSize, tpoints.Mean() - bcenter);


                    }*/
                    //System.Windows.Forms.MessageBox.Show("5");
                    // next we have to put all the grids together into one...
                    if (building.SideGrids != null)
                    {
                        foreach (var field in building.SideGrids)
                        {
                            gridPoints = gridPoints.Concat(field.Grid);
                            gridNormales = gridNormales.Concat(Enumerable.Repeat(field.Normal, field.GridSize));
                        }
                    }
                }

                //System.Windows.Forms.MessageBox.Show("6");

                            // creating a service instance with input data
                            var points = gridPoints.SelectMany(p => new[] { p.X, p.Y, p.Z }).ToArray();
                            var normales = gridNormales.SelectMany(p => new[] { p.X, p.Y, p.Z }).ToArray();
                            // here we create byte array and StreamInfo based on it (MD5 generated based on bytes)
                            byte[][] attachedBytes = new byte[3][];
                            attachedBytes[0] = new byte[points.Length * sizeof(double)];
                            attachedBytes[1] = new byte[normales.Length * sizeof(double)];
                            attachedBytes[2] = hdri;
                            Buffer.BlockCopy(points, 0, attachedBytes[0], 0, attachedBytes[0].Length);
                            Buffer.BlockCopy(normales, 0, attachedBytes[1], 0, attachedBytes[1].Length);
                            //System.Windows.Forms.MessageBox.Show("7");

//                            ConnectionManager lucyCM = new ConnectionManager("localhost", 7654, "lukas", "1234");
                            /*
                            var remoteServiceInstance = lucyCM.CreateServiceInstance("SolarAnalysis", scenarioID,
                                new Dictionary<string, object>()
                        {
                            {"Points", new StreamInfo(attachedBytes[0], 1, "doublesXYZ")}, // array of doubles: XYZ coordinates
                            {"Normales", new StreamInfo(attachedBytes[1], 2, "doublesXYZ")}, // array of doubles: XYZ coordinates
                            // Compulsory scenario parameter - HDRI (Information about file)
                            {"HdriImage", new Lucy.StreamInfo(hdri, 3, "hdrImage")},
                            // Optional scenario parameters. If they are not listed in a list of parameters. TODO: Now they are obligatory :(
                            {"calculationMode", "Klux" },
                            {"resolution", 64},
                            {"northOrientationAngle", 0f}
                        }, attachedBytes);
                            */
                            float angle = 0f;
                            int resolution = 64;
                            string calcMode = "Klux";

                            string checksum1 = GetMD5HashFromByteArr(attachedBytes[0]);
                            string checksum2 = GetMD5HashFromByteArr(attachedBytes[1]);
                            string checksum3 = GetMD5HashFromByteArr(hdri);

                            string msg = "{'action':'create_service','inputs':{'northOrientationAngle':" + angle +
                                            ",'Points':{'format':'doublesXYZ','streaminfo':{'order':1, 'checksum':'" + checksum1 +
                                            "'}},'Normales':{'format':'doublesXYZ','streaminfo':{'order':2, 'checksum':'" + checksum2 +
                                            "'}},'HdriImage':{'format':'hdrImage','streaminfo':{'order':3, 'checksum':'" + checksum3 +
                                            "'}},'resolution':" + resolution + ", 'calculationMode':'" + calcMode + "'},'classname':'SolarAnalysis','ScID':" + scenarioID + "}";
                            //System.Windows.Forms.MessageBox.Show(msg);
                            JObject lucyAnswer = CPlan.CommunicateToLucy.GlobalConnection.CL.sendAction2Lucy(msg, attachedBytes[0], attachedBytes[1], hdri);


                            int serviceID = lucyAnswer.Value<JObject>("result").Value<int>("SObjID");
                            mqtt_topic_solar = lucyAnswer.Value<JObject>("result").Value<String>("mqtt_topic");

                            if (mqtt_topic_solar.EndsWith("/"))
                                mqtt_topic_solar = mqtt_topic_solar.Substring(0, mqtt_topic_solar.Length - 1);

                            ConfigSettings configInfo = new ConfigSettings() { scenarioID = scenarioID, mqtt_topic = mqtt_topic_solar, objID = serviceID };

                            if (configInfo != null)
                            {
                                mqtt_topic_solar = configInfo.mqtt_topic;

                                ServiceManager.AddConfig2(mqtt_topic_solar, configInfo);
                                ServiceManager.AddID2(curChromo.Indentity, mqtt_topic_solar);
                                ServiceManager.AddConfig2(mqtt_topic_solar, configInfo);


                                // -- start calculation service --

                                if (mqtt_topic_solar != null)
                                {
                                    ServiceManager.AddMqttTopic2(mqtt_topic_solar);
                                    ServiceManager.mqttClient.MqttMsgPublishReceived += mqttClient_MqttMsgPublishReceived_Solar;
                                    ServiceManager.mqttClient.Publish(mqtt_topic_solar, System.Text.Encoding.UTF8.GetBytes("RUN"));
                                }
                            }
                            /*_isovistField.Finished = false;*/
                        }

                    }

                }
/*                else
                {
                    _isovistField.Calculate(0.05f);
                    _isovistField.Finished = true;
                }

                curChromo.IsovistField = _isovistField;
            }*/
        }
                             


            //curChromo.SolarAnalysisResults = blabla;             }
        }


        public static string GetMD5HashFromByteArr(byte[] bArr)
        {
            using (var md5 = System.Security.Cryptography.MD5.Create())
            {
                return BitConverter.ToString(md5.ComputeHash(bArr)).Replace("-", string.Empty);
            }
        }

        private int createScenario(List<GenBuildingLayout> buildings, Poly2D border)
        {
            int scenarioID = -1;

            NetTopologySuite.Features.FeatureCollection featurecollection;
            System.Collections.ObjectModel.Collection<NetTopologySuite.Features.IFeature> features = new System.Collections.ObjectModel.Collection<IFeature>();

            AttributesTable buildingAttr = new AttributesTable();
            buildingAttr.AddAttribute("layer", "building");
            AttributesTable boundaryAttr = new AttributesTable();
            boundaryAttr.AddAttribute("layer", "boundary");
            AttributesTable pointAttr = new AttributesTable();
            pointAttr.AddAttribute("layer", "buildingHeight");

            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();

            NetTopologySuite.Geometries.Polygon[] polyArray = new NetTopologySuite.Geometries.Polygon[buildings.Count + 1];
            List<NetTopologySuite.Geometries.Point> pointArray = new List<NetTopologySuite.Geometries.Point>();
            int i = 0;
            foreach (GenBuildingLayout building in buildings)
            {
                LinearRing r1 = new NetTopologySuite.Geometries.LinearRing(GeometryConverter.ToCoordinates(building.PolygonFromGen));
                NetTopologySuite.Geometries.Polygon p = new NetTopologySuite.Geometries.Polygon(r1);
                features.Add(new Feature(p, buildingAttr));

                NetTopologySuite.Geometries.Point pt = new NetTopologySuite.Geometries.Point(i, building.Height);
                pointArray.Add(pt);
                i++;
            }

            LinearRing r2 = new NetTopologySuite.Geometries.LinearRing(GeometryConverter.ToCoordinates(border));
            NetTopologySuite.Geometries.Polygon p2 = new NetTopologySuite.Geometries.Polygon(r2);
            features.Add(new Feature(p2, boundaryAttr));


            NetTopologySuite.Geometries.MultiPoint multiPts = new MultiPoint(pointArray.ToArray());
            features.Add(new Feature(multiPts, pointAttr));


            featurecollection = new FeatureCollection(features);

            string geoJSONPoly = GeoJSONWriter.Write(featurecollection);
            geoJSONPoly = geoJSONPoly.Substring(0, geoJSONPoly.Length - 1);
            string createScenario4Geo = "{'action':'create_scenario','name':'test','geometry':{'GeoJSON':{'format':'GeoJSON','geometry':" + geoJSONPoly + "}}}}}";

            JObject scenarioObj = null;
            try
            {
                scenarioObj = CPlan.CommunicateToLucy.GlobalConnection.CL.sendAction2Lucy(createScenario4Geo);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.StackTrace);
            }

            if (scenarioObj != null)
            {
                JProperty result = ((JProperty)(scenarioObj.First));
                if (result != null)
                {
                    if (result.Value["ScID"] != null)
                        scenarioID = (int)result.Value["ScID"];
                }
            }

            return scenarioID;
        }


        private int createScenarioSolar(List<GenBuildingLayout> buildings, Poly2D border)
        {
            int scenarioID = -1;

            NetTopologySuite.Features.FeatureCollection featurecollection;
            System.Collections.ObjectModel.Collection<NetTopologySuite.Features.IFeature> features = new System.Collections.ObjectModel.Collection<IFeature>();

            //******
            IEnumerable<Vector3d> gridNormales = null;
            IEnumerable<Vector3d> gridPoints = null;

            /*
            var sf = new ScalarField(border.PointsFirstLoop
                        .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
                        ControlParameters.IsovistCellSize, new Vector3d(0, 0, 1));
            GGrid _smartGrid = new GGrid(sf, "solarAnalysis");
            gridNormales = Enumerable.Repeat(_smartGrid.ScalarField.Normal, _smartGrid.ScalarField.GridSize);
            gridPoints = _smartGrid.ScalarField.Grid.ToList();
            
            foreach (GenBuildingLayout building in buildings)
            {
                var bpoints = building.PolygonFromGen.PointsFirstLoop;
                var bcenter = new Vector3d(building.PolygonFromGen.PointsFirstLoop.Mean());
                bcenter.Z = building.BuildingHeight / 2;
                building.SFDerived = true;
                building.FieldTypeName = "solarAnalysis";
                building.SideGrids = new ScalarField[1 + bpoints.Length];
                building.SideGrids[0] = new ScalarField(bpoints
                    .Select(v => new Vector3d(v.X, v.Y, building.BuildingHeight)).ToArray(),
                    ControlParameters.IsovistCellSize, new Vector3d(0, 0, 1));
                for (int i = 1; i <= bpoints.Length; i++)
                {
                    var tpoints = new Vector3d[]
                        {
                            new Vector3d(bpoints[i - 1].X, bpoints[i - 1].Y, 0),
                            new Vector3d(bpoints[i - 1].X, bpoints[i - 1].Y, building.BuildingHeight),
                            new Vector3d(bpoints[i%bpoints.Length].X, bpoints[i%bpoints.Length].Y, building.BuildingHeight),
                            new Vector3d(bpoints[i%bpoints.Length].X, bpoints[i%bpoints.Length].Y, 0)
                        };
                    building.SideGrids[i] = new ScalarField(tpoints, ControlParameters.IsovistCellSize, tpoints.Mean() - bcenter);


                }
                // next we have to put all the grids together into one...
                foreach (var field in building.SideGrids)
                {
                    gridPoints = gridPoints.Concat(field.Grid);
                    gridNormales = gridNormales.Concat(Enumerable.Repeat(field.Normal, field.GridSize));
                }
            }

            */
            //_solarAnalysis = new SolarAnalysis(gridPoints, gridNormales);
            //_solarAnalysis.ClearGeometry();

            var polygons = new List<IPolygon>();
            var converter = new Pg3DNtsInterop.Pg3DNtsConverter();


            AttributesTable buildingAttr = new AttributesTable();
            buildingAttr.AddAttribute("layer", "building");
            /*     AttributesTable boundaryAttr = new AttributesTable();
                 boundaryAttr.AddAttribute("layer", "boundary");
                 AttributesTable pointAttr = new AttributesTable();
                 pointAttr.AddAttribute("layer", "buildingHeight");
                 */
            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();

            NetTopologySuite.Geometries.Polygon[] polyArray = new NetTopologySuite.Geometries.Polygon[buildings.Count + 1];
            List<NetTopologySuite.Geometries.Point> pointArray = new List<NetTopologySuite.Geometries.Point>();

            foreach (GenBuildingLayout curRect in buildings)
            {
                if (!curRect.Open)
                {
                    Poly2D p2D = curRect.PolygonFromGen;
                    var pvv = new CGeom3d.Vec_3d[1][];
                    var pv = new CGeom3d.Vec_3d[p2D.PointsFirstLoop.Length];
                    pvv[0] = pv;
                    int z = 0;
                    foreach (Vector2d v2D in p2D.PointsFirstLoop)
                        pv[z++] = new CGeom3d.Vec_3d(v2D.X, v2D.Y, curRect.BuildingHeight);

                    CGeom3d.Vec_3d[][] pvvBottom = p2D.PointsVec3D;
                    //_solarAnalysis.AddPolygon(pvvBottom, 1.0f);
                    polygons.Add((IPolygon)converter.PureGeomToPolygon(pvvBottom));
                    //_solarAnalysis.AddPolygon(pvv, 1.0f);
                    polygons.Add((IPolygon)converter.PureGeomToPolygon(pvv));

                    for (int i = 0; i < pv.Length; ++i)
                    {
                        int j = i + 1;
                        if (j >= pv.Length) j -= pv.Length;

                        var pvvSide = new CGeom3d.Vec_3d[1][];
                        var pvSide = new CGeom3d.Vec_3d[4];
                        pvvSide[0] = pvSide;
                        pvSide[0] = pvvBottom[0][i];
                        pvSide[1] = pvvBottom[0][j];
                        pvSide[2] = pv[j];
                        pvSide[3] = pv[i];

                        //_solarAnalysis.AddPolygon(pvvSide, 1.0f);
                        polygons.Add((IPolygon)converter.PureGeomToPolygon(pvvSide));
                    }
                }
            }

            var mpol = new MultiPolygon(polygons.ToArray());
            features.Add(new Feature(mpol, buildingAttr));

            featurecollection = new FeatureCollection(features);

            string geoJSONPoly = GeoJSONWriter.Write(featurecollection);
            geoJSONPoly = geoJSONPoly.Substring(0, geoJSONPoly.Length - 1);
            string createScenario4Geo = "{'action':'create_scenario','name':'test','geometry':{'GeoJSON':{'format':'GeoJSON','geometry':" + geoJSONPoly + "}}}}}";

            JObject scenarioObj = null;
            try
            {
                scenarioObj = CPlan.CommunicateToLucy.GlobalConnection.CL.sendAction2Lucy(createScenario4Geo);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.StackTrace);
            }

            if (scenarioObj != null)
            {
                JProperty result = ((JProperty)(scenarioObj.First));
                if (result != null)
                {
                    if (result.Value["ScID"] != null)
                        scenarioID = (int)result.Value["ScID"];
                }
            }

            return scenarioID;
        }

        void mqttClient_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            if (e.Topic == mqtt_topic && System.Text.Encoding.UTF8.GetString(e.Message) == "DONE")
            {
                // service results available
                ReadLucyResults(mqtt_topic);
                lucyResultsAvailable = true;
            }
            else if (e.Topic == mqtt_topic && System.Text.Encoding.UTF8.GetString(e.Message) != "RUN")
            {
                Console.Out.WriteLine("haeh");
            }
        }

        private void ReadLucyResults(string mqtt_topic)
        {
            string instanceID = "40";
            int index = mqtt_topic.LastIndexOf("/");
            if (index > 0)
                instanceID = mqtt_topic.Substring(index + 1);

            string msg = "{'action':'get_service_outputs','SObjID':" + instanceID + "}";
            JObject result = GlobalConnection.CL.sendAction2Lucy(msg);

            float cellSize = ControlParameters.IsovistCellSize;

            if (result != null)
            {
                JToken outputs = result.Value<JToken>("result").Value<JToken>("outputs");
                if (outputs != null)
                {
                    string output = (string)(outputs.Value<JObject>("outputVals").Value<String>("value"));

                    _isovistField.AnalyseLucyResults(output);
                    _isovistField.Finished = true;
                    curChromo.IsovistField = _isovistField;
                }
            }
        }

        void mqttClient_MqttMsgPublishReceived_Solar(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            if (e.Topic == mqtt_topic_solar && System.Text.Encoding.UTF8.GetString(e.Message) == "DONE")
            {
                // service results available
                ReadLucyResultsSolar(mqtt_topic_solar);
                lucyResultsAvailable_Solar = true;
            }
        }

        private void ReadLucyResultsSolar(string mqtt_topic)
        {
            if (!calculateSolar)
                return;

            string instanceID = "40";
            int index = mqtt_topic.LastIndexOf("/");
            if (index > 0)
                instanceID = mqtt_topic.Substring(index + 1);

            string msg = "{'action':'get_service_outputs','SObjID':" + instanceID + "}";
            JObject result = GlobalConnection.CL.sendAction2Lucy(msg);

            float cellSize = ControlParameters.IsovistCellSize;

            if (result != null)
            {
  //              byte[] barr = GlobalConnection.CL.GetBinaryAttachment(result);

//                int l = barr.Count();
                
                JToken outputs = result.Value<JToken>("result").Value<JToken>("outputs");
                if (outputs != null)
                {
                    JArray radiationList = outputs.Value<JArray>("RadiationList");
                    double[] radArr = new double[radiationList.Count];
                    for (int r = 0; r<radiationList.Count; r++) {
                        radArr[r] = (double)radiationList[r];
                    }

                    //curChromo.SolarField = _solarAnalysis;
                    curChromo.SolarAnalysisResults = radArr;

                 //   string output = (string)(outputs.Value<JObject>("RadiationList").Value<String>("value"));

                 //   _isovistField.AnalyseLucyResults(output);
                   // _isovistField.Finished = true;
                   // curChromo.IsovistField = _isovistField;
                }
            }
        }

    }
}


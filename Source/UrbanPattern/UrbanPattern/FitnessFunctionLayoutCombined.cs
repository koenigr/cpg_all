﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = FitnessFunctionLayoutCombined.cs 
//  Copyright (C) 2015/04/01  14:54 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using CPlan.Evaluation;
using CPlan.Geometry;
using CPlan.Optimization;
using System.Threading.Tasks;
//using CommunicateToLucy;
using CPlan.VisualObjects;
using GeoAPI.Geometries;
using Lucy;
using OpenTK;
using NetTopologySuite.Features;
using NetTopologySuite.IO;
using NetTopologySuite.Geometries;
using Newtonsoft.Json.Linq;
using TimeSpan = System.TimeSpan;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public class FitnessFunctionLayoutCombined : IMultiFitnessFunction
    {
        private Object thisLock = new Object();

        private int _dimensions = 3;
        public int Dimensions { get{return _dimensions;} }
        public CalculationType.Method RunVia { get; set; }
        public CalculationType.Method RunVia_Solar { get; set; }
        private string mqtt_topic;
        private ChromosomeLayout curChromo;
        private bool lucyResultsAvailable = false;

        public FitnessFunctionLayoutCombined()
        {
            RunVia = CalculationType.Method.Local;
            RunVia_Solar = CalculationType.Method.Local;
        }

        public FitnessFunctionLayoutCombined(CalculationType.Method runVia, CalculationType.Method runVia_Solar)
        {
            RunVia = runVia;
            RunVia_Solar = runVia_Solar;
        }


//        async Task<IMultiChromosome> IMultiFitnessFunction.Evaluate(IMultiChromosome chromosome)//, bool calculate = true)
        async System.Threading.Tasks.Task<List<double>> IMultiFitnessFunction.Evaluate(IMultiChromosome chromosome, bool calculate = true)
        {
            List<double> fitnessValues = new List<double>();
            curChromo = (ChromosomeLayout)chromosome;

            //if (calculate)
            {
                //IsovistAnalysis(curChromo);
                curChromo = await IsovistAnalysis(curChromo)
                    .ConfigureAwait(false);
                //await SolarAnalysis(curChromo);//.ConfigureAwait(false);
                curChromo = await SolarAnalysis(curChromo)
                    .ConfigureAwait(false);
            }


            //------------------------------------------------------//
            // --- minimization ---
            // -- add above number of _dimensions!
            //if (curChromo.IsovistField.Area.Sum() == 0) fitnessValues.Add(double.MaxValue);
            //else 
            fitnessValues.Add(Math.Abs(curChromo.Overlap()));

            if (curChromo.IsovistField.Area.Sum() == 0) fitnessValues.Add(double.MaxValue);
            else fitnessValues.Add((1 / curChromo.IsovistField.Area.Where(n => (!double.IsNaN(n) && n >= 0)).Max()));

            //if (curChromo.IsovistField.Occlusivity.Sum() == 0) fitnessValues.Add(double.MaxValue);
            //else fitnessValues.Add((curChromo.IsovistField.Occlusivity.Where(n => (!double.IsNaN(n) && n >= 0)).Average()));

            //if (curChromo.IsovistField.Compactness.Sum() == 0) fitnessValues.Add(double.MaxValue);
            //else fitnessValues.Add((1 / curChromo.IsovistField.Compactness.Where(n => (!double.IsNaN(n) && n >= 0)).Average()));

            //if (curChromo.IsovistField.MinRadial.Sum() == 0) fitnessValues.Add(double.MaxValue);
            //else fitnessValues.Add((1 / curChromo.IsovistField.MinRadial.Where(n => (!double.IsNaN(n) && n >= 0)).Average()));

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            double solarValue = 0;
            foreach (GenBuildingLayout building in curChromo.Gens)
            {
                for (int s = 1; s < building.SideGrids.Count(); s++)
                {
                    if (building.SideGrids[s] != null)
                        solarValue += building.SideGrids[s].SolarResults.Where(n => (!double.IsNaN(n) && n >= 0)).Max();
                    else
                        Console.WriteLine("building sides error...");
                }
            }
            fitnessValues.Add(solarValue);
            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

            /* *ac*
            curChromo.m_fitness_values = fitnessValues;
            return curChromo;//fitnessValues;
            */

            return fitnessValues;
        }

        async Task<ChromosomeLayout> SolarAnalysis(ChromosomeLayout curChromo)
        {
            SolarAnalysisLayout curSolarAnalysis = null;

            if (curChromo != null)
            {
                curSolarAnalysis = new SolarAnalysisLayout(curChromo);
            }
            if (curSolarAnalysis != null)
                curSolarAnalysis = await curSolarAnalysis.Compute((RunVia_Solar == CalculationType.Method.Local) ? true : false).ConfigureAwait(false);

            return curSolarAnalysis.ActiveLayout;
        }


        //============================================================================================================================================================================
        private async Task<ChromosomeLayout> IsovistAnalysis(ChromosomeLayout curChromo)
        {
            //lucyResultsAvailable = false;

            List<Line2D> obstLines = new List<Line2D>();
            List<Poly2D> obstPolys = new List<Poly2D>();
            if (curChromo != null)
            {
                Poly2D isovistBorder = curChromo.Border;
                Rect2D borderRect = GeoAlgorithms2D.BoundingBox(isovistBorder.PointsFirstLoop);
                List<GenBuildingLayout> buildings = curChromo.Gens;

                // test environmental polygons
                if (curChromo.Environment != null)
                    foreach (Poly2D envPoly in curChromo.Environment)
                    {
                        if (envPoly.Distance(isovistBorder) <= 0)
                        {
                            obstLines.AddRange(envPoly.Edges.ToList());
                            obstPolys.Add(envPoly);
                        }
                    }

                // add the buildings
                foreach (GenBuildingLayout curBuilding in buildings)
                {
                    if (!curBuilding.Open)
                    {
                        obstLines.AddRange(curBuilding.PolygonFromGen.Edges.ToList());
                        obstPolys.Add(curBuilding.PolygonFromGen);
                    }
                }

                obstLines.AddRange(isovistBorder.Edges);
                //List<Vector2d> gridPoints = IsovistAnalysis.PointsForLucyIsovist(_curLayout.Border, _curLayout.Gens, cellSize);
                float cellSize = ControlParameters.IsovistCellSize;
                var isovistField = new IsovistAnalysis(isovistBorder, obstPolys, cellSize);
                isovistField.Finished = false;

                //_isovistField.CutBorder(_isovistBorder); // consider only the area inside the border polygon
                TimeSpan calcTime1; // calcTime2;
                DateTime start = DateTime.Now;

                // -- run local --------------------------------------------------------------------------------
                //isovistField.Calculate(0.05f);

                //DateTime end = DateTime.Now;
                //calcTime1 = end - start;

                // -- run via server --------------------------------------------------------------------------- 
                // approximately 10 times! slower on local maschine...???
                //start = DateTime.Now;
                //isovistField.CalculateViaLucy();


                //if (RunVia == CalculationType.Method.Local)
                //{                  
                //    _isovistField.Calculate(0.05f);
                //    _isovistField.Finished = true;
                //}
                //else 
// *ac*                if (LucyManager.Manager.ViaLucy) //
                if (RunVia == CalculationType.Method.Lucy)
                {
                    //lock (thisLock)
                    //{
                    //lucyResultsAvailable = false;

                    // -- create Scenario --
// *ac*                    int? scenarioID = LucyManager.Manager.CreateScenarioForIsovist(buildings, isovistBorder);
                    int? scenarioID = null;

                    if (scenarioID != null)
                    {
                        var points =
                            new MultiPoint(
                                isovistField.AnalysisPoints.Select(p => new Point(p.X, p.Y, 0) as IPoint).ToArray());
                        var s = new GeoJsonWriter().Write(points);
                        var jobj = JObject.Parse(s);

                        //GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
                        //string geoJSONPoints = GeoJSONWriter.Write(gridPointsJ);

                        //// create Service -> return mqtt-topic
                        //string msg = "{'action':'create_service','inputs':{'inputVals':" + geoJSONPoints + "},'classname':'Isovist','ScID':" + scenarioID + "}";

/* *ac*
                        LucyServiceInstance serviceInstance = LucyManager.Manager.ConnectionManager
                            .CreateServiceInstance(
                                "Isovist",
                                scenarioID,
                                new Dictionary<string, object>()
                                {
                                    {"inputVals", jobj}
                                });
*/

/*
                        CPlan.CommunicateToLucy.ConfigSettings cs = new CPlan.CommunicateToLucy.ConfigSettings() { objID = (int)serviceInstance.ServiceId, scenarioID = (int)serviceInstance.ScenarioId };
                        cs.mqtt_topic = "/Lucy/" + cs.scenarioID + "/" + cs.objID;


                        CPlan.CommunicateToLucy.ServiceManager.AddConfig(cs.mqtt_topic, cs);
                        CPlan.CommunicateToLucy.ServiceManager.AddID(curChromo.Indentity, cs.mqtt_topic);
                        CPlan.CommunicateToLucy.ServiceManager.AddMqttTopic(cs.mqtt_topic);
                        */
                        //ConfigSettings configInfo = GlobalConnection.CL.sendCreateService2Lucy(msg, scenarioID);

                        //mqtt_topic = configInfo.mqtt_topic;

                        // -- start calculation service --
/* *ac*                        var t = await serviceInstance.ExecuteAsync().ConfigureAwait(false);
                        //var bworker = new BackgroundWorker();
                        //bworker.DoWork += (sender, args) =>
                        //{
                        //t.Result

                        CPlan.CommunicateToLucy.ConfigSettings cs = new CPlan.CommunicateToLucy.ConfigSettings() { objID = (int)serviceInstance.ServiceId, scenarioID = (int)serviceInstance.ScenarioId };
                        cs.mqtt_topic = "/Lucy/" + cs.scenarioID + "/" + cs.objID;

                        CPlan.CommunicateToLucy.ServiceManager.AddConfig(cs.mqtt_topic, cs);
                        CPlan.CommunicateToLucy.ServiceManager.AddID(curChromo.Indentity, cs.mqtt_topic);
                        CPlan.CommunicateToLucy.ServiceManager.AddMqttTopic(cs.mqtt_topic);
                        CPlan.CommunicateToLucy.ServiceManager.RemoveMqttTopic(cs.mqtt_topic);

                        var rez = JObject.Parse(t.Item1["outputVals"].ToString());
                        var val = rez["value"].Value<string>();
                        try
                        {
                            isovistField.AnalyseLucyResults(val);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("!!!!!!!!!!!!ERROR: " + ex.Message);
                            Console.WriteLine(ex.StackTrace);
                            Console.WriteLine(val);
                        }
                        //};
                        //bworker.RunWorkerAsync();
                        //if (mqtt_topic != null)
                        //{
                        //    ServiceManager.AddMqttTopic(mqtt_topic);
                        //    ServiceManager.mqttClient.MqttMsgPublishReceived += mqttClient_MqttMsgPublishReceived;
                        //    ServiceManager.mqttClient.Publish(mqtt_topic, System.Text.Encoding.UTF8.GetBytes("RUN"));
                        //}
 */
                    }
                    else
                    {
                        Console.WriteLine("!!!!!!!!!!!!ERROR: Scenario is not created!");
                    }
                    //}
                }
                else
                {
                    isovistField.Calculate(0.05f);
                }

                //end = DateTime.Now; ;
                //calcTime2 = end - start;

                //System.Diagnostics.Trace.WriteLine("grid with " + isovistField.AnalysisPoints.Count().ToString() + " analysis points: ");
                //System.Diagnostics.Trace.WriteLine("local calculation time 1: " + calcTime1.TotalMilliseconds.ToString() + " ms");
                //System.Diagnostics.Trace.WriteLine("server calculation time 2: " + calcTime2.TotalMilliseconds.ToString() + " ms");

                //isovistField.WriteToGridValues();

                isovistField.Finished = true;
                //propertyGrid2.SelectedObject = isovistField;
                //curChromo.FinisehdFitnessCalculation = true;
                curChromo.IsovistField = isovistField;
                return curChromo;
            }
            else
            {
                Console.WriteLine("!!!!!!!!!!!!ERROR: no current chromosome!");
                return null;
            }
        }

        //============================================================================================================================================================================
        private int createScenario(List<GenBuildingLayout> buildings, Poly2D border)
        {
            int scenarioID = -1;

            NetTopologySuite.Features.FeatureCollection featurecollection;
            System.Collections.ObjectModel.Collection<IFeature> features = new System.Collections.ObjectModel.Collection<IFeature>();

            AttributesTable buildingAttr = new AttributesTable();
            buildingAttr.AddAttribute("layer", "building");
            AttributesTable boundaryAttr = new AttributesTable();
            boundaryAttr.AddAttribute("layer", "boundary");

            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();

            NetTopologySuite.Geometries.Polygon[] polyArray = new NetTopologySuite.Geometries.Polygon[buildings.Count + 1];
            foreach (GenBuildingLayout building in buildings)
            {
                LinearRing r1 = new NetTopologySuite.Geometries.LinearRing(GeometryConverter.ToCoordinates(building.PolygonFromGen));
                NetTopologySuite.Geometries.Polygon p = new NetTopologySuite.Geometries.Polygon(r1);
                features.Add(new Feature(p, buildingAttr));
            }

            LinearRing r2 = new NetTopologySuite.Geometries.LinearRing(GeometryConverter.ToCoordinates(border));

            NetTopologySuite.Geometries.Polygon p2 = new NetTopologySuite.Geometries.Polygon(r2);

            features.Add(new Feature(p2, boundaryAttr));

            featurecollection = new FeatureCollection(features);

/* *ac*            var scenarioObj = LucyManager.Manager.ConnectionManager.CreateScenario("UrbanPlanningTest",
                        JObject.FromObject(new
                        {
                            geometry = (JObject)(new Lucy.Geometry()
                            {
                                Format = "GeoJSON",
                                Data = featurecollection
                            })
                        }));

            if (scenarioObj != null)
            {
                JProperty result = ((JProperty)(scenarioObj.First));
                if (result != null)
                {
                    if (result.Value["ScID"] != null)
                        scenarioID = (int)result.Value["ScID"];
                }
            }

 */
            return scenarioID;

        }
       
    }
}


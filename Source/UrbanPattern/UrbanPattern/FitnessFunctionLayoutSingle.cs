﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = FitnessFunctionLayoutSingle.cs 
//  Copyright (C) 2014/10/18  7:52 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using AForge.Genetic;
using CPlan.Evaluation;
using CPlan.Geometry;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public class FitnessFunctionLayoutSingle : IFitnessFunction
    {
        /// <summary>
        /// Evaluates the fitness of a variant.
        /// For Single criteria optimization we use maximization!
        /// </summary>
        /// <param name="chromosome"> Variant to be evaluated.</param>
        /// <returns>The fitness value.</returns>
        double IFitnessFunction.Evaluate(IChromosome chromosome)
        {
            
            double fitnessValue = -123;// double.MinValue;
            ChromosomeLayout curChromo = (ChromosomeLayout)chromosome;
            IsovistAnalysis(curChromo);
            fitnessValue = (curChromo.IsovistField.Area.Max());
            return fitnessValue;
        }


        //============================================================================================================================================================================
        private void IsovistAnalysis(ChromosomeLayout curChromo)
        {
            List<Line2D> obstLines = new List<Line2D>();
            List<Poly2D> obstPolys = new List<Poly2D>();
            if (curChromo != null)
            {
                IsovistAnalysis isovistField;
                Poly2D isovistBorder = curChromo.Border;
                Rect2D borderRect = GeoAlgorithms2D.BoundingBox(isovistBorder.PointsFirstLoop);
                List<GenBuildingLayout> buildings = curChromo.Gens;

                // test environmental polygons
                if (curChromo.Environment != null)
                    foreach (Poly2D envPoly in curChromo.Environment)
                    {
                        if (envPoly.Distance(isovistBorder) <= 0)
                        {
                            obstLines.AddRange(envPoly.Edges.ToList());
                            obstPolys.Add(envPoly);
                        }
                    }

                // add the buildings
                foreach (GenBuildingLayout curBuilding in buildings)
                {
                    //if (curRect.Filled)
                    {
                        obstLines.AddRange(curBuilding.PolygonFromGen.Edges.ToList());
                        obstPolys.Add(curBuilding.PolygonFromGen);
                    }
                }

                obstLines.AddRange(isovistBorder.Edges);
                float cellSize = 10;

                isovistField = new IsovistAnalysis(isovistBorder, obstPolys, cellSize);
                
                TimeSpan calcTime1;
                DateTime start = DateTime.Now;

                // -- run local --
                isovistField.Calculate(0.05f);

                DateTime end = DateTime.Now;
                calcTime1 = end - start;

                //isovistField.WriteToGridValues();
                curChromo.IsovistField = isovistField;

            }

        }

    }
}

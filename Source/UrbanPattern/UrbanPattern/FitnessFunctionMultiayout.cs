﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = FitnessFunctionMultiayout.cs 
//  Copyright (C) 2014/10/18  7:52 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using CPlan.Evaluation;
using CPlan.Geometry;
using CPlan.Optimization;
using OpenTK;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public class FitnessFunctionMultiLayout : IMultiFitnessFunction
    {

        private int _dimensions = 5;
        public int Dimensions { get{return _dimensions;} }
        public CalculationType.Method RunVia { get; set; }
        public CalculationType.Method RunVia_Solar { get; set; }
        private IsovistAnalysis _isovistField = null;
        private IEnumerable<double> SolarAnalysisResults = null;
        private bool calculateSolar = false;

        public FitnessFunctionMultiLayout()
        {
            RunVia = CalculationType.Method.Local;
            RunVia_Solar = CalculationType.Method.Local;
        }

        public FitnessFunctionMultiLayout(CalculationType.Method runVia, CalculationType.Method runVia_Solar, bool calcSolar)
        {
            RunVia = runVia;
            RunVia_Solar = runVia_Solar;
            calculateSolar = calcSolar;
        }


        async System.Threading.Tasks.Task<List<double>> IMultiFitnessFunction.Evaluate(IMultiChromosome chromosome, bool calculate = true)
        {
            List<double> fitnessValues = new List<double>();
            ChromosomeMultiLayout curChromo = (ChromosomeMultiLayout)chromosome;

            IsovistAnalysis(curChromo);
            

            //------------------------------------------------------//
            // --- minimization ??? ---
            // -- add above number of _dimensions!

            double overlapSum = 0;
            foreach (ChromosomeLayout subChromo in curChromo.SubChromosomes)
            {
                overlapSum += Math.Abs(subChromo.Overlap());
            }
            fitnessValues.Add(Math.Abs(overlapSum));

            fitnessValues.Add((1 / curChromo.IsovistField.Area.Where(n => (!double.IsNaN(n) && n >= 0)).Max()));
            fitnessValues.Add((curChromo.IsovistField.Occlusivity.Where(n => (!double.IsNaN(n) && n >= 0)).Average()));
            fitnessValues.Add((1 / curChromo.IsovistField.Compactness.Where(n => (!double.IsNaN(n) && n >= 0)).Average()));
            fitnessValues.Add((1 / curChromo.IsovistField.MinRadial.Where(n => (!double.IsNaN(n) && n >= 0)).Average()));


            return fitnessValues;

        }


        //============================================================================================================================================================================
        private void IsovistAnalysis(ChromosomeMultiLayout curChromo)
        {
            List<Line2D> obstLines = new List<Line2D>();
            List<Poly2D> obstPolys = new List<Poly2D>();
            ChromosomeLayout combi = new ChromosomeLayout(curChromo.SubChromosomes[0]);
            combi.Border = curChromo.Border;
            combi.Gens.Clear();

            if (curChromo != null)
            {
                Poly2D isovistBorder = curChromo.Border;
                Rect2D borderRect = GeoAlgorithms2D.BoundingBox(isovistBorder.PointsFirstLoop);

                // test environmental polygons
                if (curChromo.Environment != null)
                    foreach (Poly2D envPoly in curChromo.Environment)
                    {
                        if (envPoly.Distance(isovistBorder) <= 0)
                        {
                            obstLines.AddRange(envPoly.Edges.ToList());
                            obstPolys.Add(envPoly);
                        }
                    }

                foreach (ChromosomeLayout subChromo in curChromo.SubChromosomes)
                {
                    combi.Gens.AddRange(subChromo.Gens);
                    List<GenBuildingLayout> buildings = subChromo.Gens;

                    // add the buildings
                    foreach (GenBuildingLayout curBuilding in buildings)
                    {
                        if (!curBuilding.Open)
                        {
                            obstLines.AddRange(curBuilding.PolygonFromGen.Edges.ToList());
                            obstPolys.Add(curBuilding.PolygonFromGen);
                        }
                    }

                    obstLines.AddRange(isovistBorder.Edges);
                }

                float cellSize = ControlParameters.IsovistCellSize;
                _isovistField = new IsovistAnalysis(isovistBorder, obstPolys, cellSize);

                TimeSpan calcTime1, calcTime2;
                DateTime start = DateTime.Now;

                DateTime end = DateTime.Now;
                calcTime1 = end - start;

                // -- run via server --------------------------------------------------------------------------- 
                if (RunVia == CalculationType.Method.Local)
                {                  
                    _isovistField.Calculate(0.05f);
                    _isovistField.Finished = true;
                }
                else if (RunVia == CalculationType.Method.Lucy)
                {
                    // -- create Scenario --
                    FitnessFunctionLayout ff = new FitnessFunctionLayout(RunVia, RunVia_Solar, calculateSolar);
                    (ff as IMultiFitnessFunction).Evaluate(combi);
                    _isovistField = combi.IsovistField;
                    SolarAnalysisResults = combi.SolarAnalysisResults;
                    
                    CPlan.CommunicateToLucy.ServiceManager.ReplaceID(combi.Indentity, curChromo.Indentity);
                    
                    _isovistField.Finished = false;
                }
                else
                {
                    _isovistField.Calculate(0.05f);
                    _isovistField.Finished = true;
                }

                curChromo.IsovistField = _isovistField;
                curChromo.SolarAnalysisResults = SolarAnalysisResults;

            }           
        }
    }
}


﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GenBuildingLayout.cs 
//  Copyright (C) 2014/10/18  7:52 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using CPlan.Geometry;
using Math = System.Math;
using OpenTK;
using CPlan.VisualObjects;
using FarseerPhysics.Dynamics;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public class GenBuildingLayout
    {
       
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Private Fields

        private Rect2D _rect;

        /// <summary>
        /// Rotation in degree!
        /// </summary>
        private double _rotation;

        private bool _locked;

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Textures

        // top = 0
        // 1..4 - sides
        public ScalarField[] SideGrids;
        public bool SFDerived = false;
        public string FieldTypeName = null;

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        public double MinSide { get { return ControlParameters.MinBldSide; }  }
        //public double MaxSide {get { return ControlParameters.MaxBldSide; } } 
        public bool Locked {
            get { return _locked; }
            set
            {
                _locked = value;
                if (PolyForCollission != null)
                    PolyForCollission.Locked = _locked;
            }
        }

        public int Id { get; set; }
        public GenBuildingLayout ClosestNeighbourGen { get; set; }
        public double ClosestNeighbourDist { get; set; }

        //==============================================================================================
        public List<Vector2d> Points
        {
            get { return PolygonFromGen.PointsFirstLoop.ToList(); }
        }

        //==============================================================================================
        public Poly2D PolygonFromGen
        {
            get
            {
                Poly2D curPoly = new Poly2D(_rect.Points);
                curPoly.Rotate(GeoAlgorithms2D.DegreeToRadian(Rotation));
                return curPoly;
            }
            //set { _geometry = value; }
        }

        //==============================================================================================
        //public ColliderPolygon PolyForCollission { get; set; }
        public ColliderRect PolyForCollission { get; set; }

        //==============================================================================================
        public Vector2d Position
        {
            get { return _rect.Position; }
            set
            {
                _rect.Position = value;
            }
        }

        //==============================================================================================
        public double Rotation
        {
            get { return _rotation; }
            set
            {
                double tmpRotate = value;
                if (PerpendicularLock && !Locked)
                {
                    double diffRotate = Math.Abs(_rotation - tmpRotate);
                    if (diffRotate < 45) diffRotate = 0;
                    else diffRotate = 90;
                    tmpRotate = diffRotate;
                }

                double divider = Math.Ceiling(tmpRotate / 180);
                if (divider > 1)
                    _rotation = tmpRotate - ((divider - 1) * 180);
                else _rotation = tmpRotate;
            }
        }

        //==============================================================================================
        public double Area
        {
            get
            {
                return _rect.Area;
            }
            set
            {
                //if (!Locked)
                {
                    if (value < ControlParameters.MinArea)
                        value = ControlParameters.MinArea;
                    if (value > ControlParameters.MaxBldArea)
                        value = ControlParameters.MaxBldArea;

                    double proport = 1;
                    if (Height > 0) proport = Width/Height;

                    double newWidth = Math.Sqrt(proport*value);
                    double newHeith = Math.Sqrt(value/proport);

                    if (newWidth < ControlParameters.MinBldSide)
                    {
                        newWidth = ControlParameters.MinBldSide;
                        newHeith = Area/newWidth;
                    }
                    else if (newHeith < ControlParameters.MinBldSide)
                    {
                        newHeith = ControlParameters.MinBldSide;
                        newWidth = Area/newHeith;
                    }
                    if (newWidth > ControlParameters.MaxBldSide)
                    {
                        newWidth = ControlParameters.MaxBldSide;
                        newHeith = Area/newWidth;
                    }
                    else if (newHeith > ControlParameters.MaxBldSide)
                    {
                        newHeith = ControlParameters.MaxBldSide;
                        newWidth = Area/newHeith;
                    }

                    if (newWidth > ControlParameters.MinBldSide && newHeith > ControlParameters.MinBldSide &&
                        newWidth < ControlParameters.MaxBldSide && newHeith < ControlParameters.MaxBldSide)
                    {
                        Width = newWidth;
                        Height = newHeith;
                        //Position += deltaSize / 2;
                    }
                    else
                        CheckWidtHeight(newWidth);
                }
            }
        }
        //==============================================================================================
        public bool SetArea(double newArea)
        {           
            bool setFull = true;

            if (!Locked)
            {

                if (newArea < ControlParameters.MinArea)
                {
                    newArea = ControlParameters.MinArea;
                    setFull = false;
                }
                if (newArea > ControlParameters.MaxBldArea)
                {
                    newArea = ControlParameters.MaxBldArea;
                    setFull = false;
                }

                double proport = 1;
                if (Height > 0) proport = Width/Height;

                double newWidth = Math.Sqrt(proport*newArea);
                double newHeith = Math.Sqrt(newArea/proport);

                if (newWidth < ControlParameters.MinBldSide)
                {
                    newWidth = ControlParameters.MinBldSide;
                    newHeith = Area/newWidth;
                }
                else if (newHeith < ControlParameters.MinBldSide)
                {
                    newHeith = ControlParameters.MinBldSide;
                    newWidth = Area/newHeith;
                }
                if (newWidth > ControlParameters.MaxBldSide)
                {
                    newWidth = ControlParameters.MaxBldSide;
                    newHeith = Area/newWidth;
                }
                else if (newHeith > ControlParameters.MaxBldSide)
                {
                    newHeith = ControlParameters.MaxBldSide;
                    newWidth = Area/newHeith;
                }

                if (newWidth > ControlParameters.MinBldSide && newHeith > ControlParameters.MinBldSide &&
                    newWidth < ControlParameters.MaxBldSide && newHeith < ControlParameters.MaxBldSide)
                {
                    Width = newWidth;
                    Height = newHeith;
                }
                else
                {
                    setFull = CheckWidtHeight(newWidth);
                }  
            }

            return setFull;
        }

        //==============================================================================================
        public double Width
        {
            get
            {

                return _rect.Width;
            }
            set
            {
                _rect.Width = value;
            }
        }

        //==============================================================================================
        public double Height
        {
            get
            {
                return _rect.Height;
            }
            set
            {
                _rect.Height = value;
            }
        }

        //==============================================================================================
        public double Left
        {
            get
            {
                return PolygonFromGen.BoundingBox().Left;
            }
        }

        //==============================================================================================
        public double Top
        {
            get
            {
                return PolygonFromGen.BoundingBox().Top;
            }
        }

        //==============================================================================================
        public Vector2d Center
        {
            get
            {
                return _rect.Center;
            }
            set
            {
                Vector2d difference = (value - Center);
                Move(difference);
            }
        }

        //==============================================================================================
        public Rect2D BoundingRect { get; set; }

        //==============================================================================================
        /// <summary>
        /// The hight of the corresponding building.
        /// </summary>
        public double BuildingHeight { get; set; }

        //==============================================================================================
        /// <summary>
        /// True indicates that the object attracts others.
        /// </summary>
        public bool Attractor { get; set; }

        //==============================================================================================
        /// <summary>
        /// True indicates that it is an open space.
        /// </summary>
        public bool Open { get; set; }

        //==============================================================================================
        /// <summary>
        /// True allows only rotations by a multiple of 90°.
        /// </summary>
        public bool PerpendicularLock { get; set; }

        //==============================================================================================
        /// <summary>
        /// True prevent proportion changes.
        /// </summary>
        public bool ProportionLock { get; set; }

        //==============================================================================================
        /// <summary>
        /// Distance to other objects.
        /// </summary>
        public float RepelOffset { get; set; }

        //==============================================================================================

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        //==============================================================================================
        /// <summary>
        /// Initailize a new GenBuildingLayout instance.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="boudRect"></param>
        /// <param name="area"></param>
        /// <param name="hight"></param>
        /// <param name="vId"></param>
        /// <param name="world"></param>
        public GenBuildingLayout(double x, double y, Rect2D boudRect, double area, double hight, int vId,
            World world)
        {
            double width, height;
            Id = vId;
            Vector2d centre = new Vector2d(x, y);
            width = height = Math.Sqrt(area); 
            Rotation = RndGlobal.Rnd.Next(0, 360);
            Attractor = false;
            Open = false;
            _rect = new Rect2D(centre.X, centre.Y , width, height);
            _rect.Move(new Vector2d(-width / 2, height / 2));
            BoundingRect = boudRect;
            BuildingHeight = hight;

            //PolyForCollission = new ColliderPolygon(world, PolygonFromGen);
            PolyForCollission = new ColliderRect(world, _rect, Convert.ToSingle(GeoAlgorithms2D.DegreeToRadian(Rotation)));
        }

        //==============================================================================================
        /// <summary>
        /// Initailize a new GenBuildingLayout instance.
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="rotation"></param>
        /// <param name="hight"></param>
        /// <param name="vId"></param>
        /// <param name="world"></param>
        public GenBuildingLayout(Rect2D rect, double rotation, double hight, int vId,
            World world)
        {
            Id = vId;
            Attractor = false;
            Open = false;
            _rect = new Rect2D(rect);
            Rotation = rotation;
            BuildingHeight = hight;

            //PolyForCollission = new ColliderPolygon(world, PolygonFromGen);
            PolyForCollission = new ColliderRect(world, _rect, Convert.ToSingle(GeoAlgorithms2D.DegreeToRadian(Rotation)));
        }

        //==============================================================================================
        /// <summary>
        /// Copy Constructor
        /// </summary>
        /// <param name="sourceGen"></param>
        public GenBuildingLayout(GenBuildingLayout sourceGen)
        {
            _rect = sourceGen._rect.Clone();
            Rotation = sourceGen.Rotation;
            Attractor = sourceGen.Attractor;
            Open = sourceGen.Open;
            Id = sourceGen.Id;
            Locked = sourceGen.Locked;
            BoundingRect = sourceGen.BoundingRect;
            BuildingHeight = sourceGen.BuildingHeight;
            PerpendicularLock = sourceGen.PerpendicularLock;
            ProportionLock = sourceGen.ProportionLock;
            RepelOffset = sourceGen.RepelOffset;
            PolyForCollission = sourceGen.PolyForCollission;
            SideGrids = sourceGen.SideGrids;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        /// <summary>
        /// Clone the building Gene.
        /// </summary>
        /// <returns>Returns exact clone of the Gene.</returns>
        public GenBuildingLayout Clone()
        {
            GenBuildingLayout clone = new GenBuildingLayout(this);
            return clone;
        }

        //==============================================================================================
        /// <summary>
        /// Move the Gen.
        /// </summary>
        /// <param name="v"> Vector by which the Gen is moved. </param>
        public void Move(Vector2d v)
        {
            _rect.Move(v);          
        }

        //==============================================================================================
        /// <summary>
        /// Reset the assignement of the Collission body and fisxture to a new wold object.
        /// Needed e.g. after crossover operation.
        /// </summary>
        /// <param name="world"> The world to which the body and fixture shall be assigned to. </param>
        public void ResetCollissionWoldAssignement(World world)
        {
            PolyForCollission = new ColliderRect(world, _rect, Convert.ToSingle(GeoAlgorithms2D.DegreeToRadian(Rotation)));
        
            //PolyForCollission.ResetWoldAssignement(world);
        }

        //==============================================================================================
        /// <summary>
        /// Run Farseer Physics Collission and update the rectangle & rotation.
        /// </summary>
        public void UpdateGenFromCollission()
        {
            PolyForCollission.UpdatePos();
            _rect = PolyForCollission;//ApproxRect(PolyForCollission);
            _rotation = GeoAlgorithms2D.RadianToDegree(PolyForCollission.Rotation);
        }

        //==============================================================================================
        /// <summary>
        /// Send the current rectangle and rotation to the Farseer Physics Collission.
        /// </summary>
        public void UpdateColliderPoly()
        {
            PolyForCollission.UpdateFixture(_rect, Convert.ToSingle(GeoAlgorithms2D.DegreeToRadian(Rotation)));
        }

        //==============================================================================================
        /// <summary>
        /// Creates a roted minimal bounding rectangle. 
        /// </summary>
        /// <returns>Bounding rectangle.</returns>
        /*internal Rect2D ApproxRect(Poly2D polyToConvert)
        {
            List<Vector2d> minBR = polyToConvert.PointsFirstLoop.ToList();//GeoAlgorithms2D.MinimalBoundingRectangle(polyToConvert.PointsFirstLoop.ToList()).ToList();
            Vector2d vecA = minBR[1] - minBR[0];
            Vector2d vecB = new Vector2d(1, 0);
            double rotation = GeoAlgorithms2D.AngleBetweenR(vecA, vecB);
            if (double.IsNaN(rotation))
            {
                Rect2D approxRect = new Rect2D(Points[0], Points[2]);
                _rotation = 0;
                return approxRect;
            }
            else
            {
                Poly2D rotatedGeom = new Poly2D(GeoAlgorithms2D.RotatePoly(-rotation, GeoAlgorithms2D.Centroid(minBR), polyToConvert.Points)); // // PolygonFromGen.Points
                Rect2D approxRect = GeoAlgorithms2D.BoundingBox(rotatedGeom.PointsFirstLoop);
                _rotation = GeoAlgorithms2D.RadianToDegree(rotation);
                return approxRect;
            }
        }*/

        //==============================================================================================
        /// <summary>
        /// Check if the newSide (width) can be assigned for the rectangle.
        /// </summary>
        /// <param name="newSide">New width to be tested.</param>
        /// <returns></returns>
        public bool CheckWidtHeight(double newSide)
        {
            bool changedFull = true; // indicates if the requrired size change was executed completely.
            double fixArea = Area;
            double height;
            double limboHeight;
            double limboWidth;

            if (ProportionLock)
            { return false;}

            if (newSide < ControlParameters.MinBldSide)         //-- Minimale Scalierung X
            {
                newSide = ControlParameters.MinBldSide;                                      
                changedFull = false;
            }
            else if (newSide > ControlParameters.MaxBldSide)    //-- Maximale Scalierung X
            {
                newSide = ControlParameters.MaxBldSide;
                changedFull = false;
            }
            height = (fixArea / newSide);
            limboHeight = height;
            limboWidth = newSide;
            if (limboHeight < ControlParameters.MinBldSide)
            {
                limboHeight = ControlParameters.MinBldSide;
                limboWidth = (fixArea / limboHeight);
                changedFull = false;
            }
            if (limboHeight > ControlParameters.MaxBldSide)
            {
                limboHeight = ControlParameters.MaxBldSide;
                limboWidth = (fixArea / limboHeight);
                changedFull = false;
            }

            // execute new sizes
            if ((limboHeight*limboWidth) == fixArea)
            {
                if (!(limboWidth > ControlParameters.MinBldSide && limboHeight > ControlParameters.MinBldSide &&
                      limboWidth < ControlParameters.MaxBldSide && limboHeight < ControlParameters.MaxBldSide))
                {
                    // -- cut or extend the sides to fit into min/max values --
                    changedFull = false;
                    if (limboWidth < ControlParameters.MinBldSide)
                    {
                        limboWidth = ControlParameters.MinBldSide;
                    }
                    if (limboHeight < ControlParameters.MinBldSide)
                    {
                        limboHeight = ControlParameters.MinBldSide;
                    }
                    if (limboWidth > ControlParameters.MaxBldSide)
                    {
                        limboWidth = ControlParameters.MaxBldSide;
                    }
                    if (limboHeight > ControlParameters.MaxBldSide)
                    {
                        limboHeight = ControlParameters.MaxBldSide;
                    }
                }

                double diffHeight = Height - limboHeight;
                double diffWidth = Width - limboWidth;
                Height = limboHeight; // -diffHeight / 2;
                Width = limboWidth; // -diffWidth / 2;
               
                Move(new Vector2d(diffWidth / 2, -diffHeight / 2));

            }
            return changedFull;
        }

        //==============================================================================================
        /// <summary>
        /// Check the rotation - width, height assignement
        /// </summary>
        /// <returns>Returns false if width and height needs to be considert inverse. </returns>
        public bool CheckRot()
        {
            double tester = Math.Round(Rotation / 90, 0);
            if (tester % 2 == 0) return true; // true, wenn Zahl gerade.
            else return false;
        }

        //==============================================================================================
        /// <summary>
        /// Return a Poly2D.
        /// </summary>
        /// <returns></returns>
        public Poly2D ToPoly2D()
        {
            return new Poly2D(PolygonFromGen.PointsFirstLoop.ToArray());
        }

        # endregion

    }
}

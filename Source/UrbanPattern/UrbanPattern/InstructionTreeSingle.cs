﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = InstructionTreeSingle.cs 
//  Copyright (C) 2014/10/18  7:52 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using AForge.Genetic;
using CPlan.Geometry;
using QuickGraph;
using OpenTK;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public class InstructionTreeSingle: InstructionTree, IChromosome 
    {
        double fitness;

        /// <summary>
        /// The fitness values for each criteria.
        /// </summary>
        public double Fitness
        {
            get { return fitness; }
        }


        public InstructionTreeSingle(int treeDepth, UndirectedGraph<Vector2d, UndirectedEdge<Vector2d>> iniNet, Poly2D border)// : base(treeDepth)
            : base(treeDepth, iniNet, border)
        {
        }

        /// <summary>
        /// Evaluate chromosome with specified fitness function.
        /// </summary>
        /// <param name="function">Fitness function to use for evaluation of the chromosome.</param>
        /// <remarks><para>Calculates chromosome's fitness values using the specifed fitness function.</para></remarks>
        public void Evaluate(IFitnessFunction function)
        {
            fitness = function.Evaluate(this);
        }

        /// <summary>
        /// Clone the chromosome.
        /// </summary>
        public new IChromosome Clone()
        {
            return new InstructionTreeSingle(this);
        }

        /// <summary>
		/// Copy Constructor.
		/// </summary>
        protected InstructionTreeSingle(InstructionTreeSingle source) : base(source)
		{            
            fitness = source.fitness;
		}

        /// <summary>
        /// Create new random chromosome (factory method)
        /// </summary>
        public new IChromosome CreateNew()
        {
            return new InstructionTreeSingle(TreeDepth, InitalNetwork, Border);
        }

        /// <summary>
        /// Crossover operator
        /// </summary>
        public void Crossover(IChromosome pair)
        {
            MyCrossoverSingle(pair);
        }
        public void MyCrossoverSingle(IChromosome pair)
        {
            InstructionTree p = (InstructionTree)pair;
            // check for correct pair
            if (p != null)
            {
                // --- choose random nodes
                //int r = rand.Next(this.Nodes.Count - 3) + 3;
                //InstructionNode childThis = (InstructionNode)this.Nodes[r];
                //Int64 parentThisIdx = childThis.ParentIndex;
                //List<InstructionNode> branchThis = new List<InstructionNode>();
                //branchThis = this.FindBranch(branchThis, childThis.Index);
                //branchThis.Add(childThis);

                //int m = rand.Next(p.Nodes.Count - 3) + 3;
                //InstructionNode childOther = (InstructionNode)p.Nodes[m];
                //Int64 parentOtherIdx = childOther.ParentIndex;
                //List<InstructionNode> branchOther = new List<InstructionNode>();
                //branchOther = p.FindBranch(branchOther, childOther.Index);
                //branchOther.Add(childOther);

                // --- choose random nodes
                List<InstructionNode> thisSelNodes = Nodes.FindAll(InstructionNode => InstructionNode.ParentIndex < TreeDepth);
                if (thisSelNodes.Count < 4) return;
                int r = rand.Next(thisSelNodes.Count - 3) + 3;
                InstructionNode childThis = (InstructionNode)thisSelNodes[r];
                Int64 parentThisIdx = childThis.ParentIndex;
                List<InstructionNode> branchThis = new List<InstructionNode>();
                branchThis = FindBranch(branchThis, childThis.Index);
                branchThis.Add(childThis);

                List<InstructionNode> otherSelNodes = p.Nodes.FindAll(InstructionNode => InstructionNode.ParentIndex < p.TreeDepth);
                if (otherSelNodes.Count < 4) return;
                int m = rand.Next(otherSelNodes.Count - 3) + 3;
                InstructionNode childOther = (InstructionNode)otherSelNodes[m];
                Int64 parentOtherIdx = childOther.ParentIndex;
                List<InstructionNode> branchOther = new List<InstructionNode>();
                branchOther = p.FindBranch(branchOther, childOther.Index);
                branchOther.Add(childOther);

                // --- delete nodes from onw InstructionTree 
                //parentThis.Children.Remove(childThis);
                foreach (InstructionNode tNode in branchThis)
                {
                    if (Nodes.Contains(tNode)) Nodes.Remove(tNode);
                }

                //parentOther.Children.Remove(childOther);
                foreach (InstructionNode oNode in branchOther)
                {
                    if (p.Nodes.Contains(oNode)) p.Nodes.Remove(oNode);
                }

                // --- copy nodes to the other InstructionTree
                // -- change indices of all branch nodes --
                Int64 highestThisIdx = HighestIdx;
                foreach (InstructionNode tNode in branchOther)
                {
                    Int64 newIdx = tNode.Index + highestThisIdx;
                    tNode.Index = newIdx;
                    tNode.ParentIndex = tNode.ParentIndex + highestThisIdx;
                }

                Nodes.AddRange(branchOther);
                childOther.ParentIndex = parentThisIdx;

                //parentOther.Children.Add(childThis);
                Int64 highestOtherIdx = p.HighestIdx;
                foreach (InstructionNode tNode in branchThis)
                {
                    Int64 newIdx = tNode.Index + highestOtherIdx;
                    tNode.Index = newIdx;
                    tNode.ParentIndex = tNode.ParentIndex + highestOtherIdx;
                }
                p.Nodes.AddRange(branchThis);
                childThis.ParentIndex = parentOtherIdx;
            }
        }

        /// <summary>
        /// Compare two chromosomes.
        /// </summary>
        /// 
        /// <param name="o">Binary chromosome to compare to.</param>
        /// 
        /// <returns>Returns comparison result, which equals to 0 if fitness values
        /// of both chromosomes are equal, 1 if fitness value of this chromosome
        /// is less than fitness value of the specified chromosome, -1 otherwise.</returns>
        /// 
        public int CompareTo(object o)
        {
            double f = ((InstructionTreeSingle)o).fitness;

            return (fitness == f) ? 0 : (fitness < f) ? 1 : -1;
        }
    }
}

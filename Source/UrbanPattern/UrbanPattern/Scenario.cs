﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Scenario.cs 
//  Copyright (C) 2014/10/18  7:52 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using CPlan.Geometry;
using CPlan.VisualObjects;
using CPlan.Optimization;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public class Scenario
    {
        /// <summary>
        /// The border of the planning area.
        /// </summary>
        public Poly2D Border
        {
            get; set;
        }

        /// <summary>
        /// The seed lines for the strett network growth.
        /// </summary>
        public List<Line2D> InitialLines
        {
            get;
            set;
        }

        /// <summary>
        /// The lines representing street segments of a network. 
        /// Holds the direct neighbouring street network.
        /// </summary>
        public List<Line2D> EnvironmentCloseNetwork
        {
            get;
            set;
        }

        /// <summary>
        /// The lines representing street segments of a network. 
        /// Holds the second level network, which ist farther away...
        /// </summary>
        public List<Line2D> EnvironmentFarNetwork
        {
            get;
            set;
        }
        
        /// <summary>
        /// The sorrounding buildings.
        /// </summary>
        public ObjectsDrawList Buildings
        {
            get;
            set;
        }

        /// <summary>
        /// An imported new building layout
        /// </summary>
        public List<GenBuildingLayout> NewLayout
        {
            get;
            set;
        }

    }
}

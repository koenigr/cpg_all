﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using CPlan.Evaluation;
using CPlan.Geometry;
using CPlan.Optimization;
using CPlan.VisualObjects;
using DirectSunLightNet;
using GeoAPI.Geometries;
using NetTopologySuite.Geometries;
using OpenTK;


namespace CPlan.UrbanPattern
{
    public class SolarAnalysisLayout
    {
        private SolarAnalysis _solarAnalysis;
        private GGrid _smartGrid;
        private ChromosomeLayout _activeLayout;
        private ChromosomeMultiLayout _activeMultiLayout;
        private Object thisLock = new Object();

        public ChromosomeLayout ActiveLayout { get { return _activeLayout; }}
        public GGrid SmartGrid { get { return _smartGrid; } }

        public SolarAnalysisLayout(ChromosomeLayout inActiveLayout)
        {
            _activeLayout = inActiveLayout;
        }

        public SolarAnalysisLayout(ChromosomeMultiLayout inActiveLayout)
        {
            _activeMultiLayout = inActiveLayout; ;
        }

        public SolarAnalysisLayout() {}


        public void SetSmartGrid(GGrid grid)
        {
            _smartGrid = grid;
        }

        public async Task<SolarAnalysisLayout> Compute(bool calc = false)
        {
            //_analysisType = AnalysisType.Solar;
            GenBuildingLayout[] buildings = new GenBuildingLayout[0];
            IEnumerable<Vector3d> gridNormales = null;
            IEnumerable<Vector3d> gridPoints = null;
            var polygons = new List<IPolygon>();

            float cellSize = ControlParameters.IsovistCellSize;

            // --- Analysis for multiple blocks -----------------------------------------------------------------------
            if (_activeMultiLayout != null)
            {

                //if ( !_smartGrid.SFDerived) 
                {
                    var sf = new ScalarField(_activeMultiLayout.Border.PointsFirstLoop
                                           .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
                                           cellSize, new Vector3d(0, 0, 1));
                    _smartGrid = new GGrid(sf, "solarAnalysis");
                }
                //else if (_smartGrid.ScalarField == null)
                //{
                //    _smartGrid.ScalarField = new ScalarField(_activeMultiLayout.Border.PointsFirstLoop
                //                            .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
                //                            cellSize, new Vector3d(0, 0, 1));
                //}

                gridNormales = Enumerable.Repeat(_smartGrid.ScalarField.Normal, _smartGrid.ScalarField.GridSize);
                gridPoints = _smartGrid.ScalarField.Grid.ToList();

                foreach (ChromosomeLayout curChromo in _activeMultiLayout.SubChromosomes)
                {
                    buildings = curChromo.Gens.ToArray();

                    foreach (GenBuildingLayout building in buildings)
                    {
                        var bpoints = building.PolygonFromGen.PointsFirstLoop;
                        var bcenter = new Vector3d(building.PolygonFromGen.PointsFirstLoop.Mean());
                        bcenter.Z = building.BuildingHeight / 2;
                        building.SFDerived = true;
                        building.FieldTypeName = "solarAnalysis";
                        building.SideGrids = new ScalarField[1 + bpoints.Length];
                        building.SideGrids[0] = new ScalarField(bpoints
                                                .Select(v => new Vector3d(v.X, v.Y, building.BuildingHeight)).ToArray(),
                                                cellSize, new Vector3d(0, 0, 1));
                        for (int i = 1; i <= bpoints.Length; i++)
                        {
                            var tpoints = new Vector3d[] {
                            new Vector3d(bpoints[i-1].X, bpoints[i-1].Y, 0),
                            new Vector3d(bpoints[i-1].X, bpoints[i-1].Y, building.BuildingHeight),
                            new Vector3d(bpoints[i % bpoints.Length].X, bpoints[i % bpoints.Length].Y, building.BuildingHeight),
                            new Vector3d(bpoints[i % bpoints.Length].X, bpoints[i % bpoints.Length].Y, 0)
                        };
                            building.SideGrids[i] = new ScalarField(tpoints, cellSize, tpoints.Mean() - bcenter);


                        }
                        // next we have to put all the grids together into one...
                        foreach (var field in building.SideGrids)
                        {
                            gridPoints = gridPoints.Concat(field.Grid);
                            gridNormales = gridNormales.Concat(Enumerable.Repeat(field.Normal, field.GridSize));
                        }
                    }
                }
                _solarAnalysis = new SolarAnalysis(gridPoints, gridNormales);
                _solarAnalysis.ClearGeometry();
                //_isovistBorder = _borderPoly.Poly.Clone();
                //_isovistBorder.Offset(1.2);

                foreach (ChromosomeLayout curChromo in _activeMultiLayout.SubChromosomes)
                {
                    buildings = curChromo.Gens.ToArray();
                    // --- add the buildings ---
                    var converter = new Pg3DNtsInterop.Pg3DNtsConverter();
                    foreach (GenBuildingLayout curRect in buildings)
                    {
                        if (!curRect.Open)
                        {
                            Poly2D p2D = curRect.PolygonFromGen;
                            var pvv = new CGeom3d.Vec_3d[1][];
                            var pv = new CGeom3d.Vec_3d[p2D.PointsFirstLoop.Length];
                            pvv[0] = pv;
                            int z = 0;
                            foreach (Vector2d v2D in p2D.PointsFirstLoop)
                                pv[z++] = new CGeom3d.Vec_3d(v2D.X, v2D.Y, curRect.BuildingHeight);

                            CGeom3d.Vec_3d[][] pvvBottom = p2D.PointsVec3D;
                            _solarAnalysis.AddPolygon(pvvBottom, 1.0f);
                            polygons.Add((IPolygon)converter.PureGeomToPolygon(pvvBottom));
                            _solarAnalysis.AddPolygon(pvv, 1.0f);
                            polygons.Add((IPolygon)converter.PureGeomToPolygon(pvv));

                            for (int i = 0; i < pv.Length; ++i)
                            {
                                int j = i + 1;
                                if (j >= pv.Length) j -= pv.Length;

                                var pvvSide = new CGeom3d.Vec_3d[1][];
                                var pvSide = new CGeom3d.Vec_3d[4];
                                pvvSide[0] = pvSide;
                                pvSide[0] = pvvBottom[0][i];
                                pvSide[1] = pvvBottom[0][j];
                                pvSide[2] = pv[j];
                                pvSide[3] = pv[i];

                                _solarAnalysis.AddPolygon(pvvSide, 1.0f);
                                polygons.Add((IPolygon)converter.PureGeomToPolygon(pvvSide));
                            }
                        }
                    }
                }
            }

            // --- Analysis for one block only -----------------------------------------------------------------------
            else if (_activeLayout != null)
            {
                //if (_smartGrid == null || !_smartGrid.SFDerived) {
                //if ( !_smartGrid.SFDerived) 
                {
                    var sf = new ScalarField(_activeLayout.Border.PointsFirstLoop
                        .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
                        cellSize, new Vector3d(0, 0, 1));
                    _smartGrid = new GGrid(sf, "solarAnalysis");
                }
                //else if (_smartGrid.ScalarField == null)
                //{
                //    _smartGrid.ScalarField = new ScalarField(_activeLayout.Border.PointsFirstLoop
                //        .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
                //        cellSize, new Vector3d(0, 0, 1));
                //}

                gridNormales = Enumerable.Repeat(_smartGrid.ScalarField.Normal, _smartGrid.ScalarField.GridSize);
                gridPoints = _smartGrid.ScalarField.Grid.ToList();
                buildings = _activeLayout.Gens.ToArray();

                foreach (GenBuildingLayout building in buildings)
                {
                    var bpoints = building.PolygonFromGen.PointsFirstLoop;
                    var bcenter = new Vector3d(building.PolygonFromGen.PointsFirstLoop.Mean());
                    bcenter.Z = building.BuildingHeight / 2;
                    building.SFDerived = true;
                    building.FieldTypeName = "solarAnalysis";
                    building.SideGrids = new ScalarField[1 + bpoints.Length];
                    building.SideGrids[0] = new ScalarField(bpoints
                        .Select(v => new Vector3d(v.X, v.Y, building.BuildingHeight)).ToArray(),
                        cellSize, new Vector3d(0, 0, 1));
                    for (int i = 1; i <= bpoints.Length; i++)
                    {
                        var tpoints = new Vector3d[]
                        {
                            new Vector3d(bpoints[i - 1].X, bpoints[i - 1].Y, 0),
                            new Vector3d(bpoints[i - 1].X, bpoints[i - 1].Y, building.BuildingHeight),
                            new Vector3d(bpoints[i%bpoints.Length].X, bpoints[i%bpoints.Length].Y, building.BuildingHeight),
                            new Vector3d(bpoints[i%bpoints.Length].X, bpoints[i%bpoints.Length].Y, 0)
                        };
                        building.SideGrids[i] = new ScalarField(tpoints, cellSize, tpoints.Mean() - bcenter);


                    }
                    // next we have to put all the grids together into one...
                    foreach (var field in building.SideGrids)
                    {
                        gridPoints = gridPoints.Concat(field.Grid);
                        gridNormales = gridNormales.Concat(Enumerable.Repeat(field.Normal, field.GridSize));
                    }
                }

                _solarAnalysis = new SolarAnalysis(gridPoints, gridNormales);
                _solarAnalysis.ClearGeometry();
                //_isovistBorder = _borderPoly.Poly.Clone();
                //_isovistBorder.Offset(1.2);

                // --- add the buildings ---              
                var converter = new Pg3DNtsInterop.Pg3DNtsConverter();
                foreach (GenBuildingLayout curRect in buildings)
                {
                    if (!curRect.Open)
                    {
                        Poly2D p2D = curRect.PolygonFromGen;
                        var pvv = new CGeom3d.Vec_3d[1][];
                        var pv = new CGeom3d.Vec_3d[p2D.PointsFirstLoop.Length];
                        pvv[0] = pv;
                        int z = 0;
                        foreach (Vector2d v2D in p2D.PointsFirstLoop)
                            pv[z++] = new CGeom3d.Vec_3d(v2D.X, v2D.Y, curRect.BuildingHeight);

                        CGeom3d.Vec_3d[][] pvvBottom = p2D.PointsVec3D;
                        _solarAnalysis.AddPolygon(pvvBottom, 1.0f);
                        polygons.Add((IPolygon)converter.PureGeomToPolygon(pvvBottom));
                        _solarAnalysis.AddPolygon(pvv, 1.0f);
                        polygons.Add((IPolygon)converter.PureGeomToPolygon(pvv));

                        for (int i = 0; i < pv.Length; ++i)
                        {
                            int j = i + 1;
                            if (j >= pv.Length) j -= pv.Length;

                            var pvvSide = new CGeom3d.Vec_3d[1][];
                            var pvSide = new CGeom3d.Vec_3d[4];
                            pvvSide[0] = pvSide;
                            pvSide[0] = pvvBottom[0][i];
                            pvSide[1] = pvvBottom[0][j];
                            pvSide[2] = pv[j];
                            pvSide[3] = pv[i];

                            _solarAnalysis.AddPolygon(pvvSide, 1.0f);
                            polygons.Add((IPolygon)converter.PureGeomToPolygon(pvvSide));
                        }
                    }
                }
            }

            
            if (!calc)    
                return this;
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // --- Run the calculation ---
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if ((_activeLayout != null) || (_activeMultiLayout != null))
            {
                var mpol = new MultiPolygon(polygons.ToArray());
                var saResults = Enumerable.Empty<double>();
                //LucyManager.Manager.ShowLucyInProgress();
                //var bworker = new BackgroundWorker();
                //LucyManager.Manager.Log("Solar Analysis starting its work " + (LucyManager.Manager.ViaLucy ? "via Lucy" : "locally"));
/* later                if (LucyManager.Manager.ViaLucy)
                    //bworker.DoWork += (sender, args) =>
                    {
                        //lock (thisLock)
                        {
                             //int t = await Task.Run(() => 
                            //int x = await task;
                             //LucyManager.Manager.DoSolarAnalysis(mpol, gridPoints, gridNormales).;
                             saResults = await LucyManager.Manager.DoSolarAnalysis(mpol, gridPoints, gridNormales).ConfigureAwait(false);
                        }
                    }//;
                else*/
                    //bworker.DoWork += (sender, args) =>
                    {
                        _solarAnalysis.Calculate(CalculationMode.illuminance_klux);
                        saResults = _solarAnalysis.Results;
                        //    };
                    }//;

                //bworker.RunWorkerCompleted += (sender, args) =>
                {
                    _smartGrid.ScalarField.GenerateTexture(saResults.Take(_smartGrid.ScalarField.GridSize), "solarAnalysis");
                    _smartGrid.GenerateTexture();

                    // -- store the results for the plane of the chromosomes --
                    if (_activeMultiLayout != null)
                    {
                        _activeMultiLayout.SolarAnalysisResults = saResults.Take(_smartGrid.ScalarField.GridSize);
                        if (_activeLayout != null)
                            _activeLayout.SolarAnalysisResults = saResults.Take(_smartGrid.ScalarField.GridSize);
                    }
                    else
                    {
                        _activeLayout.SolarAnalysisResults = saResults.Take(_smartGrid.ScalarField.GridSize);
                    }

                    saResults = saResults.Skip(_smartGrid.ScalarField.GridSize);


                    if (_activeMultiLayout != null)
                    {
                        foreach (ChromosomeLayout curChromo in _activeMultiLayout.SubChromosomes)
                        {                           
                            buildings = curChromo.Gens.ToArray();
                            foreach (GenBuildingLayout building in buildings)
                            {
                                foreach (var field in building.SideGrids)
                                {
                                    field.GenerateTexture(saResults.Take(field.GridSize), "solarAnalysis");
                                    saResults = saResults.Skip(field.GridSize);
                                }
                            }
                        }
                    }
                    else
                    {
                        buildings = _activeLayout.Gens.ToArray();
                        foreach (GenBuildingLayout building in buildings)
                        {

                            foreach (var field in building.SideGrids)
                            {
                                field.GenerateTexture(saResults.Take(field.GridSize), "solarAnalysis");
                                saResults = saResults.Skip(field.GridSize);
                            }
                        }
                    }
                }//;
                //bworker.RunWorkerAsync();

            }
            //else
                //DrawLayout();
            return this;
        }

    }
}

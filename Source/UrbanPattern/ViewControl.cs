﻿//#if CT_UNCOMMENT
using System;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Windows.Forms;
using System.Drawing;
using CPlan.Geometry;
using CPlan.VisualObjects;


namespace CPlan.UrbanPattern
{

    /// <summary>
    /// Implements basic view controls.
    /// Zoom, Pan, (Rotate).
    /// </summary>
    public class ViewControl
    {
        //############################################################################################################################################################################
        # region Private Fields

        private GLControl glControl;
        //private Single scaleView = 1.0f;
        //private Single rotateView = 0.0f;
        private Vector3d posView = new Vector3d(0, 0, 0);
        private Vector3d transformCentre = new Vector3d(0, 0, 0);
        private Vector3D lastMouseLocation;
        //private bool mouseDownR = false;
        //private bool mouseDownL = false;
        //private int w;
        //private int h;
        private bool pan_rotate;
        private bool move;

        # endregion

        //############################################################################################################################################################################
        # region Properties

        public Camera camera_2D;
        public Camera camera_3D;

        public Camera Camera;

        /// <summary> If GLControl control is loaded. </summary>
        public bool Loaded { get; protected set; }
        public Color BackgroundColor { get; protected set; }
        public bool Background { get; set; }
        public ObjectsAllList AllObjectsList = new ObjectsAllList();

        static int count = 0;

        # endregion

        //############################################################################################################################################################################
        # region Constructors
        //===================================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of ViewControl.
        /// </summary>
        /// /// <param name="curControl">A OpenTK GLControl.</param>
        public ViewControl(GLControl curControl)
        {
            glControl = curControl;

            glControl.MouseWheel += new System.Windows.Forms.MouseEventHandler(glControl_MouseWheel);
            glControl.MouseMove += new System.Windows.Forms.MouseEventHandler(glControl_MouseMove);
            glControl.MouseDown += new System.Windows.Forms.MouseEventHandler(glControl_MouseDown);
            glControl.MouseUp += new System.Windows.Forms.MouseEventHandler(glControl_MouseUp);
            glControl.Load += new System.EventHandler(glControl_Load);
            glControl.Resize += new System.EventHandler(glControl_Resize);
            glControl.Paint += new System.Windows.Forms.PaintEventHandler(this.glControl_Paint);

            camera_2D = new Camera();//curControl);
            camera_2D.m_beta = -(float)Math.PI / 2;
            camera_2D.m_znear = -9999.9f;
            camera_2D.m_zfar = 9999.9f;
            camera_2D.m_ortho = true;
            camera_2D.SetPosition(0, 0, 50);

            camera_3D = new Camera();//curControl);
            camera_3D.m_alpha = -(float)Math.PI / 4;
            camera_3D.m_beta = -(float)Math.PI / 12;
            camera_3D.m_zoom = 60.0f;
            camera_3D.m_znear = 0.1f;
            camera_3D.m_zfar = 9999.9f;
            camera_3D.m_ortho = false;
            camera_3D.SetPosition(0.0f, 0.0f, 50f);

            Camera = camera_2D;
        }

        # endregion

        //############################################################################################################################################################################
        # region Methods
        public void CameraChange()
        {
            if (Camera == camera_2D) Camera = camera_3D;
            else if (Camera == camera_3D) Camera = camera_2D;
            glControl.Invalidate();
        }

        //===================================================================================================================================================================
        private void glControl_Load(object sender, EventArgs e)
        {
            Loaded = true;
            BackgroundColor = Color.White;
            SetupViewport();
        }

        //===================================================================================================================================================================
        /// <summary>
        /// Initialize the GLControl viewport settings
        /// </summary>
        public void SetupViewport()
        {
            glControl.MakeCurrent();
            int w = glControl.Width;
            int h = glControl.Height;

            //GL.Viewport(0, 0, w, h); // Use all of the glControl painting area
        }


        DateTime start;
        //===================================================================================================================================================================
        private void glControl_Paint(object sender, PaintEventArgs e)
        {
            if (!Loaded) return;
            glControl.MakeCurrent();

            InitOpenGL();
            GL.ClearColor(BackgroundColor);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            //Console.WriteLine("Camera: X "+Camera.m_position.x+" Y "+Camera.m_position.y+" Z "+Camera.m_position.z);
            Camera.SetOpenGLMatrixes();
            Camera.DrawKoordinatenkreuz();

            AllObjectsList.Draw();

            //glControl.SwapBuffers();


            // XXX: framerate drops when users interact with the scene
            //TimeSpan timeDiff = DateTime.Now - start;
            //Console.WriteLine("frame rate " + 1000 / timeDiff.TotalMilliseconds);
            //start = DateTime.Now;

            //GL.MatrixMode(MatrixMode.Projection);
            //GL.PopMatrix();
            //GL.MatrixMode(MatrixMode.Modelview);
            //GL.PopMatrix();
        }

        //===================================================================================================================================================================
        private void InitOpenGL()
        {
            GL.Enable(EnableCap.DepthTest);

            // set background color 
            GL.ClearColor(BackgroundColor);

            // set clear Z-Buffer value
            GL.ClearDepth(1.0f);

            GL.Enable(EnableCap.Blend);
            GL.Enable(EnableCap.PointSmooth);
            GL.Enable(EnableCap.LineSmooth);
            GL.Disable(EnableCap.PolygonSmooth);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.ShadeModel(ShadingModel.Smooth);
            GL.DepthFunc(DepthFunction.Lequal);
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
        }

        //===================================================================================================================================================================
        private void glControl_Resize(object sender, EventArgs e)
        {
            if (!Loaded) return;
            SetupViewport();
            glControl.Invalidate();
        }

        //===================================================================================================================================================================
        public void DeactivateInteraction()
        {
            glControl.MouseMove -= glControl_MouseMove;
            glControl.MouseDown -= glControl_MouseDown;
            glControl.MouseUp -= glControl_MouseUp;
        }

        public void ActivateInteraction()
        {
            glControl.MouseMove += glControl_MouseMove;
            glControl.MouseDown += glControl_MouseDown;
            glControl.MouseUp += glControl_MouseUp;
        }
        //===================================================================================================================================================================
        private void glControl_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!Loaded) return;

            // ZW: if select move screen button
            if (Form1.MouseMode == "ScreenMove")
            {
                //lastMouseLocation = Camera.GetPointXYPtn(Cursor.Position.X, Cursor.Position.Y, 0);

                lastMouseLocation = new Vector3D(Cursor.Position.X, Cursor.Position.Y, 0);
                //
                // -- view control --
                //

                //ZW: left mouse for move
                if (e.Button == System.Windows.Forms.MouseButtons.Right)
                {
                    pan_rotate = false;
                    move = true;
                }

                //ZW: right mouse for pan & rotate
                if (e.Button == System.Windows.Forms.MouseButtons.Right && Camera == camera_3D)
                {
                    pan_rotate = true;
                    move = false;
                }
            }
            //ZW: building interactions
            else
            {
                // -- object interaction --
                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    //mouseDownL = true;
                    // CGeom3d.Vec_3d point_xy = Camera.GetPointXYPtn(e.X, e.Y, glControl.Width, glControl.Height);
                    Vector3D point_xy = Camera.GetPointXYPtn(e.X, e.Y, 0);

                    // this.AllObjectsList.ObjectsToDraw.Add(new GPoint(point_xy.x, point_xy.y));

                    if (point_xy == null) return;
                    Vector2D p = new Vector2D(point_xy.x, point_xy.y);
                    AllObjectsList.ObjectsToInteract.MouseDown(e, p);//, ref glControl);
                }

                pan_rotate = false;
                move = false;
            }
            glControl.Invalidate();

        }

        //===================================================================================================================================================================
        private void glControl_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!Loaded) return;
            //
            // -- view control --
            //

            if (lastMouseLocation == null)
                return;

            double delta_x = Cursor.Position.X - lastMouseLocation.X;
            double delta_y = Cursor.Position.Y - lastMouseLocation.Y;

            //right mouse move
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                Vector3D preWorldPos = Camera.GetPointXYPtn(lastMouseLocation);
                Vector3D curWorldPos = Camera.GetPointXYPtn(Cursor.Position.X, Cursor.Position.Y, 0);
                double delta_x_world = curWorldPos.X - preWorldPos.X;
                double delta_y_world = curWorldPos.Y - preWorldPos.Y;

                //Control key down,  zoom
                if (Control.ModifierKeys == Keys.Control)
                {
                    float scale_ratio = 0.01f;
                    if (Camera.m_position.z <= 10)
                        scale_ratio = 0.1f;

                    Camera.m_position.z *= Convert.ToSingle(1.0 - delta_y_world * scale_ratio);
                    glControl.Invalidate();
                }
                else if (Control.ModifierKeys == Keys.Shift && Camera == camera_3D)
                {
                    // --- 3D Camera Rotate ---
                    Camera.m_alpha += Convert.ToSingle(delta_y * 0.01f);
                    Camera.m_beta += Convert.ToSingle(delta_x * 0.01f);

                    while (Camera.m_alpha < 0) Camera.m_alpha += Convert.ToSingle(2 * Math.PI);
                    while (Camera.m_alpha > 2 * Math.PI) Camera.m_alpha -= Convert.ToSingle(2 * Math.PI);

                    while (Camera.m_beta < 0) Camera.m_beta += Convert.ToSingle(2 * Math.PI);
                    while (Camera.m_beta > 2 * Math.PI) Camera.m_beta -= Convert.ToSingle(2 * Math.PI);

                    //if (Camera.m_beta < -Math.PI / 2) Camera.m_beta = Convert.ToSingle(-Math.PI / 2);
                    //if (Camera.m_beta > Math.PI / 2) Camera.m_beta = Convert.ToSingle(Math.PI / 2);
                }
                else if (Control.ModifierKeys == Keys.Alt || (Control.ModifierKeys == Keys.Shift && Camera == camera_2D))
                {
                    // change the origin point here
                    Vector3D originPoint = new Vector3D(0, 0, 0);

                    Vector3D preDir = new Vector3D(preWorldPos.X - originPoint.X, preWorldPos.Y - originPoint.Y, 0);
                    Vector3D curDir = new Vector3D(curWorldPos.X - originPoint.X, curWorldPos.Y - originPoint.Y, 0);

                    double angle1 = Math.Atan2(curDir.Y, curDir.X);
                    double angle2 = Math.Atan2(preDir.Y, preDir.X);
                    double rotateAngle = angle2 - angle1;

                    Camera.m_gamma -= (float)rotateAngle;
                }
                else
                {
                    CGeom3d.Vec_3d newPos = Camera.GetCameraPos();
                    newPos.x += delta_x_world;
                    newPos.y += delta_y_world;
                    Camera.SetPosition(newPos.x, newPos.y, newPos.z);
                }

                lastMouseLocation.X = Cursor.Position.X;
                lastMouseLocation.Y = Cursor.Position.Y;
                lastMouseLocation.Z = 0;
                glControl.Invalidate();
                return;
            }

            //TODO: left button for object manipulation, not test yet
            else if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Vector3D point_xy = Camera.GetPointXYPtn(e.X, e.Y, 0);
                if (point_xy == null) return;

                Cursor tmpCursor = Cursors.Default;
                Vector2D p = new Vector2D(point_xy.x, point_xy.y);
                //AllObjectsList.ObjectsToInteract.MouseMove(e, p, ref glControl, ref tmpCursor);
                // Hier könnte man noch optimieren, indem man immer nur den Bereich neuzeichnet, in dem das Objekt bewegt wurde.
            }

            //else if (pan_rotate)
            //{
            //    if (Control.ModifierKeys != Keys.Control)
            //    {
            //        // --- 3D Camera Pan ---
            //        CGeom3d.Vec_3d vx = new Vector3D(), vy = new Vector3D();
            //        Vector3D lot = new Vector3D(0, 0, 1);
            //        CGeom3d.Vec_3d blick = Camera.GetBlickVectorPtn();
            //        vx = blick.amul(lot);
            //        vy = blick.amul(vx);
            //        vx.Normalize();
            //        vy.Normalize();

            //        double faktor = Math.Abs(Camera.GetCameraPos().z) / 10.0;
            //        if (faktor < 1)
            //            faktor = 1;
            //        faktor *= 0.03;

            //        CGeom3d.Vec_3d newPos = Camera.GetCameraPos() + vx * (delta_x * faktor) + vy * (delta_y * faktor);

            //        Camera.SetPosition(newPos.x, newPos.y, newPos.z);
            //    }
            //    else
            //    {
            //        // --- 3D Camera Rotate ---
            //        Camera.m_alpha += Convert.ToSingle(delta_y * 0.01f);
            //        Camera.m_beta += Convert.ToSingle(delta_x * 0.01f);

            //        while (Camera.m_alpha < 0) Camera.m_alpha += Convert.ToSingle(2 * Math.PI);
            //        while (Camera.m_alpha > 2 * Math.PI) Camera.m_alpha -= Convert.ToSingle(2 * Math.PI);

            //        while (Camera.m_beta < 0) Camera.m_beta += Convert.ToSingle(2 * Math.PI);
            //        while (Camera.m_beta > 2 * Math.PI) Camera.m_beta -= Convert.ToSingle(2 * Math.PI);

            //        //if (Camera.m_beta < -Math.PI / 2) Camera.m_beta = Convert.ToSingle(-Math.PI / 2);
            //        //if (Camera.m_beta > Math.PI / 2) Camera.m_beta = Convert.ToSingle(Math.PI / 2);
            //    }

            //    lastMouseLocation.X = Cursor.Position.X;
            //    lastMouseLocation.Y = Cursor.Position.Y;

            //    glControl.Invalidate(); //Invalidate();
            //    return;
            //}

            //if (mouseDownR)
            //{
            //    if ((Control.ModifierKeys & Keys.Control) > 0) Rotate_MouseMove(e); // -- rotate view
            //    else Pan_MouseMove(e); // -- pan view 
            //}
            //
            // -- object interaction --
            //
        }

        //===================================================================================================================================================================
        private void glControl_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!Loaded) return;
            ////
            //// -- view control --
            ////
            if (pan_rotate || move)
            {
                pan_rotate = false;
                move = false;
                return;
            }

            //mouseDownR = false;
            //transformCentre.X = 0;
            //transformCentre.Y = 0;
            ////
            //// -- object interaction --
            ////
            //mouseDownL = false;
            AllObjectsList.ObjectsToInteract.MouseUp(e);// put the selected element to the fornt
            if (AllObjectsList.ObjectsToInteract.FoundInteractObj())
            {
                Vector3D point_xy = Camera.GetPointXYPtn(e.X, e.Y, 0);
                if (point_xy == null) return;
                Vector2D p = new Vector2D(point_xy.x, point_xy.y);
                AllObjectsList.ObjectsToDraw.MouseUp(e, p);//, ref glControl);
            }
            glControl.Invalidate();
        }

        //===================================================================================================================================================================
        private void glControl_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!Loaded) return;
            //Zoom_MouseWheel(e);
            //
            // -- view control --
            //
            //if (Camera == camera_2D)
            //{
            float scale_ratio = 0.0005f;
            if (Camera.m_position.z <= 10)
                scale_ratio = 0.001f;

            Camera.m_position.z *= Convert.ToSingle(1.0 - e.Delta * scale_ratio);
            glControl.Invalidate();
            //}
            //else
            //{
            //    //im 3D entlang der Sichtachse verschieben
            //    CGeom3d.Vec_3d blick = Camera.GetBlickVectorPtn();
            //    double faktor = Math.Abs(Camera.GetCameraPos().z);
            //    if (faktor < 1)
            //        faktor = 1;
            //    if (Control.ModifierKeys == Keys.Control)
            //        faktor *= 0.1;

            //    CGeom3d.Vec_3d newPos = Camera.GetCameraPos() + blick * (e.Delta * 0.002 * faktor);
            //    Camera.SetPosition(newPos.x, newPos.y, newPos.z);
            //    glControl.Invalidate();
            //}
            glControl.Invalidate();
        }

        /// <summary>
        /// Update the glControl.
        /// </summary>
        public void Invalidate()
        {
            glControl.Invalidate();
        }

        //===================================================================================================================================================================
        /// <summary>
        /// Zoom to show all objects.
        /// </summary>
        public void ZoomAll()
        {
            double zoomfak = 10.1;
            double zoomfak_perspective = 10.001;
            BoundingBox box = new BoundingBox();

            // -- find the bounding box for all objects --
            if (AllObjectsList.ObjectsToDraw.Count > 0)
            {
                foreach (GeoObject curObj in AllObjectsList.ObjectsToDraw)
                    curObj.GetBoundingBox(box);
            }
            Vector3D min = box.GetMin();
            Vector3D max = box.GetMax();

            //Console.WriteLine("In ZoomAll");

            if (box.IsValid()) //wurden überhaupt Punkte in das BoundingBox Objekt eingefügt?
            {
                if (Camera == camera_2D)
                {
                    //Console.WriteLine("box is valid and camera is 2d");
                    //Grundriss
                    CGeom3d.Vec_3d midV = (min + max) / 2;

                    double camera_z = Camera.m_position.z;
                    if (camera_z <= 0)
                        camera_z = 50;
                    else
                    {
                        camera_z = Math.Max(max.X - min.X, max.Y - min.Y) / 2;
                    }
                    Camera.SetPosition(-midV.x, -midV.y, camera_z);

                    //int dx = glControl.Width;
                    //int dy = glControl.Height;
                    //double sx = (double)(max.x - min.x) * zoomfak;
                    //double sy = (double)(max.y - min.y) * zoomfak;
                    //if (sx / ((double)dx) > sy / ((double)dy))
                    //{
                    //    if (dx > dy)
                    //        sx /= ((double)dx / (double)dy);
                    //    // if (Math.Abs(sx) > 0.000001) Camera.m_scale = Convert.ToSingle(sx);
                    //}
                    //else
                    //{
                    //    if (dy > dx)
                    //        sy /= ((double)dy / (double)dx);
                    //    // if (Math.Abs(sy) > 0.000001) Camera.m_scale = Convert.ToSingle(sy);
                    //}
                }
                else
                {
                    //Perspektive
                    CGeom3d.Vec_3d midV = (min + max) / 2;

                    double camera_z = Camera.m_position.z;
                    if (camera_z <= 0)
                        camera_z = 50;
                    else
                    {
                        // distance of the camera for covering objects in Y-dimension 
                        double z_for_y = (max.Y - min.Y) / 2 / Math.Tan(Camera.fovy / 2);

                        Int32[] oldViewport = new Int32[4];
                        GL.GetInteger(GetPName.Viewport, oldViewport);

                        double dAspectRatio = Convert.ToDouble(oldViewport[2]) / Convert.ToDouble(oldViewport[3]);

                        // distance of the camera for covering objects in Y-dimension 
                        double z_for_x = (max.X - min.X) / 2 / Math.Tan(Camera.fovy / 2) / dAspectRatio;

                        camera_z = Math.Max(z_for_x, z_for_y);
                    }
                    Camera.SetPosition(-midV.x, -midV.y, camera_z);

                    Camera.m_alpha = 0;
                    Camera.m_beta = 0;
                    Camera.m_gamma = 0;

                    //CGeom3d.Vec_3d blick = Camera.GetBlickVectorPtn();
                    //double angle = Camera.m_zoom / 180.0 * Math.PI;
                    //CGeom3d.Vec_3d mitte = (min + max) / 2;
                    //CGeom3d.Vec_3d erg = new Vector3D(), zw = new Vector3D(), point = new Vector3D();
                    //double max_dist = 0;

                    //for (int z = 0; z < 8; ++z)
                    //{
                    //    switch (z)
                    //    {
                    //        case 0: point = new Vector3D(min.x, min.y, min.z); break;
                    //        case 1: point = new Vector3D(min.x, min.y, max.z); break;
                    //        case 2: point = new Vector3D(min.x, max.y, min.z); break;
                    //        case 3: point = new Vector3D(min.x, max.y, max.z); break;
                    //        case 4: point = new Vector3D(max.x, min.y, min.z); break;
                    //        case 5: point = new Vector3D(max.x, min.y, max.z); break;
                    //        case 6: point = new Vector3D(max.x, max.y, min.z); break;
                    //        case 7: point = new Vector3D(max.x, max.y, max.z); break;
                    //        default: break; //throw new NotImplementedException();
                    //    }

                    //    double dist = GetDistMitte(mitte, blick, point, angle, ref zw, zoomfak_perspective);

                    //    if ((z == 0) || (dist > max_dist))
                    //    {
                    //        erg.x = zw.x;
                    //        erg.y = zw.y;
                    //        erg.z = zw.z;
                    //        max_dist = dist;
                    //    }
                    //}
                    //Camera.SetPosition(erg.x, erg.y, erg.z);
                }

                glControl.Invalidate();
            }
        }

        //===================================================================================================================================================================
        private double GetDistMitte(CGeom3d.Vec_3d mitte, CGeom3d.Vec_3d blick, CGeom3d.Vec_3d p, double angle, ref CGeom3d.Vec_3d erg, double zoomfak)
        {
            //Distanz Punkt Linie
            double sa, sb, sc, sd;
            double q, r;
            Vector3D bp = new Vector3D();
            double distPL, distNP, dist;

            sa = blick.x;
            sb = blick.y;
            sc = blick.z;
            sd = -p.x * sa - p.y * sb - p.z * sc;

            q = sa * blick.x + sb * blick.y + sc * blick.z;

            r = (sa * mitte.x + sb * mitte.y + sc * mitte.z + sd) / q;

            bp.x = mitte.x - blick.x * r;
            bp.y = mitte.y - blick.y * r;
            bp.z = mitte.z - blick.z * r;

            distPL = Math.Sqrt(Math.Pow(bp.x - p.x, 2) + Math.Pow(bp.y - p.y, 2) + Math.Pow(bp.z - p.z, 2));

            distNP = distPL / Math.Tan(angle / 2.0f) * zoomfak;

            (erg).x = bp.x - (blick.x * distNP);
            (erg).y = bp.y - (blick.y * distNP);
            (erg).z = bp.z - (blick.z * distNP);

            dist = Math.Sqrt(Math.Pow((erg).x - mitte.x, 2) + Math.Pow((erg).y - mitte.y, 2) + Math.Pow((erg).z - mitte.z, 2));

            return dist;
        }

        public static Vector3D rotate(Vector3D v, Vector3D axis, double angle)
        {
            // Rotate the point (x,y,z) around the vector (u,v,w)
            // Function RotatePointAroundVector(x#,y#,z#,u#,v#,w#,a#)
            double ux = axis.X * v.X;
            double uy = axis.X * v.Y;
            double uz = axis.X * v.Z;
            double vx = axis.Y * v.X;
            double vy = axis.Y * v.Y;
            double vz = axis.Y * v.Z;
            double wx = axis.Z * v.X;
            double wy = axis.Z * v.Y;
            double wz = axis.Z * v.Z;
            double sa = Math.Sin(angle);
            double ca = Math.Cos(angle);
            double x = axis.X
                    * (ux + vy + wz)
                    + (v.X * (axis.Y * axis.Y + axis.Z * axis.Z) - axis.X
                            * (vy + wz)) * ca + (-wy + vz) * sa;
            double y = axis.Y
                    * (ux + vy + wz)
                    + (v.Y * (axis.X * axis.X + axis.Z * axis.Z) - axis.Y
                            * (ux + wz)) * ca + (wx - uz) * sa;
            double z = axis.Z
                    * (ux + vy + wz)
                    + (v.Z * (axis.X * axis.X + axis.Y * axis.Y) - axis.Z
                            * (ux + vy)) * ca + (-vx + uy) * sa;

            return new Vector3D(x, y, z);

        }

        # endregion
    }
}



//===================================================================================================================================================================
/// <summary>
/// Rotate the viewport based on mouse move.
/// </summary>
/// <param name="e"></param>
//public void Rotate_MouseMove(MouseEventArgs e)
//{
//    // -- view control --
//    Vector2 vel;
//    //vel.X = (e.Location.X - lastMouseLocation.X) * scaleView;// movingChild.centre.X;
//    vel.Y = (e.Location.Y - lastMouseLocation.Y) * scaleView;
//    rotateView += vel.Y;
//    //posView.Y += -vel.Y;

//    SetupViewport();
//    glControl.Invalidate();
//}

////===================================================================================================================================================================
///// <summary>
///// Pan the viewport based on mouse move.
///// </summary>
///// <param name="e"></param>
//public void Pan_MouseMove(MouseEventArgs e)
//{
//  //  transformCentre.X = e.Location.X;
//  //  transformCentre.Y = e.Location.Y;
//    Vector2 vel;
//    vel.X = (e.Location.X - lastMouseLocation.X);// *scaleView;// movingChild.centre.X;
//    vel.Y = (e.Location.Y - lastMouseLocation.Y);// *scaleView;
//    posView.X += vel.X;
//    posView.Y += -vel.Y;

//    SetupViewport();
//    glControl.Invalidate();
//}

//===================================================================================================================================================================
/// <summary>
/// Draw the background grid.
/// </summary>
//public void DrawBackgroundGrid()
//{
//    glControl.MakeCurrent();
//    float xtmp;
//    float nrL = 60.0f;
//    float max = 0.0f;
//    if (h < w) max = w; else max = h;
//    float min = -max;
//    max *= 2;
//    float multiV = 1;

//    if (scaleView > 1) multiV = Convert.ToSingle(Math.Round(1/scaleView, 1 )+.1);
//    else multiV = Convert.ToInt32(Math.Round((1-scaleView)*10 )) + 1;
//    min *= multiV;
//    max *= multiV;

//    // -- Draw fine grid --
//    GL.LineWidth(0.2f);
//    GL.Color3(0.5F, 0.5F, 0.5F);
//    Single delta = max / nrL;
//    GL.Begin(BeginMode.Lines);
//    for (xtmp = min; xtmp < max; xtmp += delta)
//    {
//        GL.Vertex2(xtmp, min);
//        GL.Vertex2(xtmp, delta * nrL);
//        GL.Vertex2(min, xtmp);
//        GL.Vertex2(delta * nrL, xtmp);
//    };
//    GL.End();

//    // -- Draw Grid --
//    GL.LineWidth(0.3f);
//    GL.Color3(1.0F, 1.0F, 1.0F);
//    GL.Begin(BeginMode.Lines);
//    for (xtmp = min; xtmp <= max; xtmp += delta * 5)
//    {
//        GL.Vertex2(xtmp, min);
//        GL.Vertex2(xtmp, delta * nrL);
//        GL.Vertex2(min, xtmp);
//        GL.Vertex2(delta * nrL, xtmp);
//    };
//    GL.End();
//}




//===================================================================================================================================================================
/// <summary>
/// Zoom for viewport based upon the mouse wheel scrolling.
/// </summary>
/// <param name="e"> MouseEventArgs </param>
//public void Zoom_MouseWheel(MouseEventArgs e)
//{
//   // transformCentre.X = e.Location.X;
//   // transformCentre.Y = e.Location.Y;

//    Single scaleValue = Convert.ToSingle(e.Delta / 1000.0);
//    scaleView += scaleValue;
//    Single minScale = 0.1f;
//    Single maxScale = 10f;
//    if (scaleView > maxScale) { scaleView = maxScale; }
//    else if (scaleView < minScale) { scaleView = minScale; }
//    else
//    {
//        posView.X += e.Location.X * scaleValue;
//        posView.Y += e.Location.Y * scaleValue;

//    }
//    SetupViewport();
//    glControl.Invalidate();

//    //transformCentre.X = 0;
//    //transformCentre.Y = 0;
//}

//===================================================================================================================================================================
/// <summary>
/// Zoom to show all objects.
/// </summary>
//public void ZoomAll()
//{           
//    BoundingBox box = new BoundingBox();
//    double zoomfak = 1.1;

//    // -- find the bounding box for all objects --
//    if (AllObjectsList.ObjectsToDraw.Count > 0)
//    {
//        foreach (GeoObject curObj in AllObjectsList.ObjectsToDraw)
//            curObj.GetBoundingBox(box);
//    }
//    Vector3D min = box.GetMin();
//    Vector3D max = box.GetMax();

//    // -- position for ortho view --> posView
//    Vector2D delta = new Vector2D(max.X - min.X, max.Y - min.Y);
//    posView.X = min.X + delta.X / 2;
//    posView.Y = min.Y + delta.Y / 2;

//    // -- zoom factor for ortho view --> scaleView
//    int dx = glControl.Width;
//    int dy = glControl.Height;
//    double sx = (double)(max.X - min.X) * zoomfak;
//    double sy = (double)(max.Y - min.Y) * zoomfak;
//    if (sx / ((double)dx) > sy / ((double)dy))
//    {
//        if (dx > dy)
//            sx /= ((double)dx / (double)dy);
//        if (Math.Abs(sx) > 0.000001) scaleView = Convert.ToSingle(sx/100);
//    }
//    else
//    {
//        if (dy > dx)
//            sy /= ((double)dy / (double)dx);
//        if (Math.Abs(sy) > 0.000001) scaleView = Convert.ToSingle(sy/100);
//    }

//    // -- reset rotation --
//    rotateView = 0;

//    SetupViewport();
//    glControl.Invalidate();
//}
//#endif
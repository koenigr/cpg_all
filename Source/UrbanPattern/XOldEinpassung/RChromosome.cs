﻿/**
 * Kremlas/MOES
 * \file RChromosome.cs
 *
 * <A HREF="http://www.entwurfsforschung.de">
 * Copyright (C) 2011 by Dr. Reinhard Koenig. Alle Rechte vorbehalten.
 * Schutzvermerk nach DIN ISO 16016 beachten
 * </A>
 * koenig@entwurfsforschung.de
 * $Author: koenig $
 * $Rev: 393 $
 * $Date: 2011-02-21 16:21:26 +0100 (Mo, 21. Feb 2011) $
 */

using System.Windows.Forms;
using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Linq;
using System.Text;
using Troschuetz.Random.Distributions.Continuous;
using Troschuetz.Random.Generators;
using Topology.Geometries;
//using System.Threading;
using System.Threading.Tasks;
using CPlan.Geometry;

namespace CPlan.UrbanPattern
{
    public class RChromosome// : IComparable
    {
        public double NormFitOverl = 1;
        //public double NormFitDist = 1;
        public double TmpFit = 0;
        public double FitOverl = 1000000;// 0.0001;
        public double FitRealOverl = 1000000;//1;
        //public double FitDist = 0.0001;
        //public double FitRealDist = 1;
        public double OldFitness;
        public double ProportionRatio;
        public int SqueezeFit;
        public int[] GridAdress = new int[2];
        public double[] StrategyParam;
        public bool Parent = true;
        public List<RGen> Gens = new List<RGen>();
        public List<RSpring> Springs = new List<RSpring>();
        //public List<RDoors> Doors = new List<RDoors>();
        public ControlValues CValues;
        public static int NrSprings = 0;

        //private Random m_rnd = new Random((int)DateTime.Now.Ticks);
        private NormalDistribution m_normDist = new NormalDistribution();
        private Random Rnd = new Random((int)DateTime.Now.Ticks);
        private Collision m_Collision = new Collision();
        //private Rect2D bRect;

        //----------------------------------------------------------------------------------------------
        public RChromosome(Poly2D border, int nrPolys, int rndIdx)
        {
            Rnd = new Random(rndIdx);
            StrategyParam = new double [4] {0.1, 0.25, 0.35, 1.0};
            RGen curGen;
            OldFitness = FitOverl + 1;
            CValues.SizeRate = Rnd.NextDouble() / 2;
            CValues.OldSizeRate = CValues.SizeRate;
            CValues.MoveRate = Rnd.NextDouble() / 2;
            CValues.OldMoveRate = CValues.MoveRate;
            Rect2D bRect = GeoAlgorithms2D.BoundingBox(border.PointsFirstLoop);
            // --- create the Gens ---
            for (int i = 0; i < nrPolys; i++) 
            {             
                curGen = new RGen(Rnd.Next((int)bRect.Width)+border.Left, Rnd.Next((int)bRect.Height)+border.Bottom, Rnd.Next(10000), i);
                Gens.Add(curGen);
            }
            //if (Application.OpenForms.Count > 0)
            //{
            //    if (((Form1)Application.OpenForms[0]).cB_testScenario.Checked)
            //    {
            //        SetSzenario();
            //    } else SetNeighbours();
            //} else SetNeighbours();
            //CreateSprings();
            //NrSprings = Springs.Count();
        }
        //----------------------------------------------------------------------------------------------
        // Copy constructor.
        public RChromosome(RChromosome previousChromosome)
        {
            int nrPolys = previousChromosome.Gens.Count;
            StrategyParam = new double[4] { previousChromosome.StrategyParam[0], previousChromosome.StrategyParam[1], previousChromosome.StrategyParam[2], previousChromosome.StrategyParam[3]};
            FitOverl = previousChromosome.FitOverl;
            FitRealOverl = previousChromosome.FitRealOverl;
            NormFitOverl = previousChromosome.NormFitOverl;
            //FitDist = previousChromosome.FitDist;
            //FitRealDist = previousChromosome.FitRealDist;
            OldFitness = previousChromosome.OldFitness;
            Parent = previousChromosome.Parent;
            CValues = previousChromosome.CValues;
            //bRect = previousChromosome.bRect();
            //NormFitDist = previousChromosome.NormFitDist;
            ProportionRatio = previousChromosome.ProportionRatio;
            Gens.Clear();
            Gens = new List<RGen>();
            Springs.Clear();
            for (int i = 0; i < nrPolys; i++)
            {
                RGen curGen = new RGen(previousChromosome.Gens[i]);
                if (previousChromosome.Gens[i].Locked)
                    previousChromosome.Gens[i].Locked = true;
                Gens.Add(curGen);
            }
            for (int i = 0; i < previousChromosome.Springs.Count; i++)
            {
                RSpring curSpring = new RSpring(previousChromosome.Springs[i]);
                Springs.Add(curSpring);
            }
        }

        ////==============================================================================================
        //private void SetNeighbours()
        //{
        //    RGen p = Gens[0];
        //    for (int i = 1; i < Gens.Count; i++)
        //    {
        //        p.Neighbours.Add(i);
        //    }
        //}

        ////==============================================================================================
        //private void SetSzenario()
        //{
        //    // 3 flats a 5 rooms connected to a corridor
        //    // for this szenario 19 rooms are necessary!
        //    int rPF = 4;
        //    RGen p = Gens[0];
        //    for (int i = 1; i < 4; i++)
        //    {
        //        if (Gens.Count < i) break;
        //        p.Neighbours.Add(i);
        //    }
        //    int cter = 4;
        //    for (int i = 1; i < 4; i++)
        //    {
        //        if (Gens.Count < i) break;
        //        p = Gens[i];
        //        for (int j = cter; j < cter + rPF; j++)
        //        {
        //            if (Gens.Count < j) break;
        //            p.Neighbours.Add(j);
        //        }
        //        cter += rPF;
        //    }
        //}

        ////==============================================================================================
        //private void CreateSprings()
        //{
        //    RGen firstGen, secondGen;
        //    RSpring curSpring;
        //    for (int i = 0; i < Gens.Count; i++)
        //    {
        //        firstGen = Gens[i];
        //        for (int m = 0; m < firstGen.Neighbours.Count; m++)
        //        {
        //            for (int k = 0; k < Gens.Count; k++)
        //            {
        //                secondGen = Gens[k];
        //                if (firstGen.Neighbours[m] == secondGen.Id)
        //                {
        //                    curSpring = new RSpring(firstGen, secondGen);
        //                    Springs.Add(curSpring);
        //                }
        //            }
        //        }
        //    }
        //}

        //==================================================================
        //public void RunSprings(double damp)
        //{
        //    for (int i = 0; i < Springs.Count; i++)
        //    {
        //        Springs[i].update(Gens, damp); // run the Springs
        //    }
        //    for (int i = 0; i < Gens.Count; i++)
        //    {
        //        Gens[i].UpdatePos(); // update all positions
        //    }
        //}

        //==================================================================
        //public void CreateDoors()
        //{
        //    foreach (RGen myGen in Gens)
        //    {
        //        myGen.Doors.Clear();
        //    }
        //    Doors.Clear();
        //    for (int i = 0; i < Springs.Count; i++)
        //    {
        //        RDoors curDoor = null;
        //        Springs[i].CreateDoors(Gens, ref curDoor); // run the Springs
        //        Doors.Add(curDoor);
        //    }
        //}

        //==================================================================

        //==================================================================
        public void AttractPolys(double force, double radius)
        {
            Vector2D velocity = new Vector2D(0, 0);
            RGen actGen;
            //Vector2D translation = new Vector2D(velocity);
            Poly2D curAttractor = new Poly2D();

            List<Poly2D> attractors = new List<Poly2D>();
            foreach(RGen curGen in Gens)
            {
                if (curGen.Attractor) attractors.Add(curGen.Poly);
            }
            
            for (int i = 0; i < Gens.Count; i++)
            {
                actGen = (RGen)Gens[i];
                if ( actGen.Attractor ) 
                    continue;
                double[] dists = new double[attractors.Count];

                // -- calculate the distances --
                for (int j = 0; j < attractors.Count; j++)
                {
                    curAttractor = attractors[j];
                    dists[j] = actGen.Poly.Distance(curAttractor);    
                }
            
                // -- find shortest distance --
                int minIdx = -1;
                double minDist = double.MaxValue;
                for (int j = 0; j < dists.Length; j++)
                {
                    if (dists[j] < minDist) {
                        minDist = dists[j];
                        minIdx = j;
                    }  
                }

                if (minDist < radius)
                {
                    //Vector2D direction = attractors[minIdx].Center - actGen.Center;
                    
                    Vector2D intersection = new Vector2D();
                    if (!actGen.Locked)
                    {
                        double dist = attractors[minIdx].Distance(actGen.Poly, out intersection);
                        if (dist > 0 && minDist > 0)
                        {
                            //if (intersection.Length > radius * 2)
                            //{
                            //    force = 0;
                            //    dist = curAttractor.Distance(actGen.Poly, out intersection);
                            //}
                            intersection *= force;
                            actGen.Move(intersection);
                        }
                    }                               
                    
                    //direction.Normalize();
                    //direction *= ((minDist -1) * force);

                    //if (!actGen.Locked)
                    //    actGen.Move(direction);
                }
            }
        }

        //==================================================================
        public void RepelPolys(double force, double rate)
        {
            Vector2D velocity = new Vector2D(0, 0);
            RGen firstGen, secondGen;
            Vector2D translation = new Vector2D(velocity);

            for (int i = 0; i < Gens.Count; i++)
            {

                firstGen = (RGen)Gens[i];
                for (int j = i + 1; j < Gens.Count; j++)
                {
                    if (Rnd.NextDouble() < rate)
                    {
                        secondGen = (RGen)Gens[j];
                        PolygonCollisionResult r = m_Collision.PolygonCollision(firstGen.ToPoly2D(), secondGen.ToPoly2D(), velocity);
                        if (r.WillIntersect)
                        {
                            translation = velocity + r.MinimumTranslationVector;
                            // -- add random values to the translation vector to avoid blockin situation --
                            if (translation.X == 0 & translation.Y > 0) translation.X = (Rnd.NextDouble() - 0.5) * translation.Y;//Rnd.NextDouble() * translation.Y;//
                            else if (translation.Y == 0 & translation.X > 0) translation.Y = (Rnd.NextDouble() - 0.5) * translation.X;//Rnd.NextDouble() * translation.X;//
                            translation.Multiply(force);
                            if (!firstGen.Locked) 
                                firstGen.Move(translation);
                            translation.X = -translation.X;
                            translation.Y = -translation.Y;
                            if (!secondGen.Locked) 
                                secondGen.Move(translation);
                        }
                    }
                }
            }
        }

        //==================================================================
        public void ChangeRotation(double force, double rate)
        {
            Vector2D velocity = new Vector2D(0, 0);
            RGen firstGen, secondGen;
            Vector2D translation = new Vector2D(velocity);
            double divider;

            for (int i = 0; i < Gens.Count; i++)
            {  
                firstGen = (RGen)Gens[i];
                for (int j = i + 1; j < Gens.Count; j++)
                {
                    if (Rnd.NextDouble() < rate)
                    {
                        secondGen = (RGen)Gens[j];
                        PolygonCollisionResult r = m_Collision.PolygonCollision(firstGen.ToPoly2D(), secondGen.ToPoly2D(), velocity);
                        if (r.WillIntersect && r.MinimumTranslationVector.Length > .1)
                        {
                            // -- virtual rotations -> to ensure minimal rotations for becomming parallel -- 
                            double firstVirtR = firstGen.Rotation;
                            divider = Math.Ceiling(firstVirtR / 90);
                            if (divider > 1)
                                firstVirtR -= (divider - 1) * 90;
                            if (firstVirtR > 45) firstVirtR -= 90;
                            if (firstVirtR < -45) firstVirtR += 90;
                            
                            double secondVirtR = secondGen.Rotation;
                            divider = Math.Ceiling(secondVirtR / 90);
                            if (divider > 1)
                                secondVirtR -= (divider - 1) * 90;
                            if (secondVirtR > 45) secondVirtR -= 90;
                            if (secondVirtR < -45) secondVirtR += 90;

                            double diffRot = Math.Abs(firstVirtR - secondVirtR);

                            // -- adapt rotations --
                            
                            diffRot *= force;
                            if (firstVirtR > secondVirtR)
                            {
                                if (!firstGen.Locked) firstGen.Rotation -= diffRot;
                                if (!secondGen.Locked) secondGen.Rotation += diffRot;
                            }
                            else
                            {
                                if (!firstGen.Locked) firstGen.Rotation += diffRot;
                                if (!secondGen.Locked) secondGen.Rotation -= diffRot;
                            }
                        }
                    }
                }
            }
        }

        //==================================================================
        public void BorderRotation(double force, double rate)
        {
            Vector2D velocity = new Vector2D(0, 0);
            RGen gen, secondGen;
            Vector2D translation = new Vector2D(velocity);
            double divider, interArea;
            Poly2D boder = new Poly2D(REvo.Canvas.Points);
            List<Line2D> boderEdges = REvo.Canvas.Edges;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (Rnd.NextDouble() < rate)
                {
                    gen = (RGen)Gens[i];
                 
                    Poly2D poly = gen.Poly;
                    double rectArea = poly.Area;                   
                    List<Poly2D> isect = boder.Intersection(poly);
                    if (isect == null) interArea = 0;
                    else
                    {
                        interArea = isect[0].Area;              
                    }
                    double diffArea = rectArea - interArea;
                    if (diffArea > 10 & interArea > 0)
                    {
                        Line2D refLine = null;
                        foreach (Line2D iLine in boderEdges)
                        {
                            List<Vector2D> interPts = poly.Intersection(iLine);
                            if (interPts.Count > 0)
                            {
                                refLine = iLine;
                            }
                        }

                        Vector2D refVector =  refLine.End  -refLine.Start;
                        // test quadrant 
                        if ((refVector.X < 0 & refVector.Y < 0) || (refVector.X > 0 & refVector.Y < 0))
                            refVector *= -1;

                        double refRotation = GeoAlgorithms2D.AngleBetweenD(refVector, new Vector2D(1, 0));

                        // -- virtual rotations -> to ensure minimal rotations for becomming parallel -- 
                        double firstVirtR = gen.Rotation;
                        divider = Math.Ceiling(firstVirtR / 90);
                        if (divider > 1)
                            firstVirtR -= (divider - 1) * 90;
                        if (firstVirtR > 45) firstVirtR -= 90;
                        if (firstVirtR < -45) firstVirtR += 90;

                        divider = Math.Ceiling(refRotation / 90);
                        if (divider > 1)
                            refRotation -= (divider - 1) * 90;
                        if (refRotation > 45) refRotation -= 90;
                        if (refRotation < -45) refRotation += 90;
                        
                        double secondVirtR = refRotation;// 0;                          
                        double diffRot = Math.Abs(firstVirtR - secondVirtR);

                        // -- adapt rotations --
                        diffRot *= force;
                        if (firstVirtR > secondVirtR)
                        {
                            if (!gen.Free) 
                            gen.Rotation -= diffRot;
                        }
                        else
                        {
                            if (!gen.Free) 
                            gen.Rotation += diffRot;
                        }
                        //gen.Rotation = refRotation;
                    }
                }
            }
        }

        //==================================================================
        public void ChangeProportion(double force, double rate)
        {
            RGen firstGen, secondGen;
            Vector2D velocity = new Vector2D(0, 0);
            Vector2D translation = new Vector2D(velocity);
            Vector2D midP = new Vector2D();

            for (int i = 0; i < Gens.Count; i++)
            {
                firstGen = (RGen)Gens[i];
                for (int j = i + 1; j < Gens.Count; j++)
                {
                    secondGen = (RGen)Gens[j];
                    PolygonCollisionResult r = m_Collision.PolygonCollision(firstGen.ToPoly2D(), secondGen.ToPoly2D(), velocity);
                    if (r.WillIntersect)
                    {
                        translation = velocity + r.MinimumTranslationVector;
                        translation.Multiply(force);
                        midP = secondGen.Center - firstGen.Center;

                        if (!firstGen.Locked) VaryOnesSize(rate, firstGen, translation, midP);
                        if (!secondGen.Locked) VaryOnesSize(rate, secondGen, translation, midP);
                    }
                }
            }
        }

        //==============================================================================================
        private void VaryOnesSize(double rate, RGen curGen, Vector2D changer, Vector2D midP)
        {
            double width, height;
            double mag;
            changer.X = Math.Abs(changer.X);
            changer.Y = Math.Abs(changer.Y);
            midP.X = Math.Abs(midP.X);
            midP.Y = Math.Abs(midP.Y);
            mag = changer.Magnitude;

            if (Rnd.NextDouble() < rate)
            {
                if (!curGen.Locked)
                {
                    if (curGen.CheckRot())
                    {
                        if (midP.X > midP.Y)
                        {
                            width = curGen.Width - mag;
                        }
                        else
                        {
                            width = curGen.Width + mag;
                        }
                        //curGen.CheckWidtHeight(width);
                    }
                    else
                    {
                        if (midP.X > midP.Y)
                        {
                            width = curGen.Width + mag;
                        }
                        else
                        {
                            width = curGen.Width - mag;
                        }
                        //curGen.CheckWidtHeight(width);
                    }
                    //if (curGen.Rotation > 90) width *= -1;
                    curGen.CheckWidtHeight(width);
                    //{
                    //    if (midP.Y > midP.X)
                    //    {
                    //        height = curGen.Height - mag;
                    //    }
                    //    else
                    //    {
                    //        height = curGen.Height + mag;
                    //    }
                    //    curGen.CheckHeightWidt(height);
                    //}
                }
            }
        }

        //==================================================================
        public void CheckPolyBorder()
        {
            Vector2D translation = new Vector2D(0, 0);
            for (int i = 0; i < Gens.Count; i++)
            {
                RGen curGen = Gens[i];
                Poly2D curRect = curGen.Poly;
                Vector2D delta = new Vector2D();
                if (!curGen.Locked)
                {
                    double dist = REvo.Canvas.MaxDistance(curRect, out delta);
                    if (delta != null)
                    {
                        if (delta.Length > dist * 2)
                        {
                            //dist = REvo.Canvas.MaxDistance(curRect, out delta);
                            //break;
                        }
                        if (dist > 0) curGen.Move(delta);
                    }
                }
            }
        }

        //==================================================================
        public void CheckBorder()
        {
            Vector2D translation = new Vector2D(0, 0);
            int faktor = 1;
            double damp = 1;

            for (int i = 0; i < Gens.Count; i++)
            {
                RGen curGen = Gens[i];
                Poly2D curRect = curGen.Poly;

                if (!curGen.Locked)
                {
                    //curGen.Velocity = new Vector2D(0, 0);
                    //for (int j = 0; j < curGen.Points.Count; j++)
                    //{
                        //Vector2D curVect = curGen.Points[j];
                    if (curRect.Left < REvo.Canvas.Left)
                    {
                        translation.X = (REvo.Canvas.Left - curRect.Left) / faktor;
                        translation.Y = 0;
                        translation.Multiply(damp);
                        //curGen.velocity = translation;
                        curGen.Move(translation);
                        //break;
                    }
                    else if (curRect.Right > REvo.Canvas.Right)
                    {
                        translation.X = (REvo.Canvas.Right - curRect.Right) / faktor;
                        translation.Y = 0;
                        translation.Multiply(damp);
                        //curGen.velocity = translation;
                        curGen.Move(translation);
                        //break;
                    }
                    if (curRect.Top > REvo.Canvas.Top)
                    {
                        translation.X = 0;
                        translation.Y = (REvo.Canvas.Top - curRect.Top) / faktor;
                        translation.Multiply(damp);
                        //curGen.velocity = translation;
                        curGen.Move(translation);
                        //break;
                    }
                    else if (curRect.Bottom < REvo.Canvas.Bottom)
                    {
                        translation.X = 0;
                        translation.Y = (REvo.Canvas.Bottom - curRect.Bottom) / faktor;
                        translation.Multiply(damp);
                        //curGen.velocity = translation;
                        curGen.Move(translation);
                        //break;
                    }
                    //}
                }
            }
        }

        //==================================================================
        public bool CheckBorder(Poly2D testRect)
        {
            bool isInside = true;
            double tolerance = 5;

            if (testRect.Left + tolerance < REvo.Canvas.Left)
            {
                isInside = false;
            }
            else if (testRect.Right - tolerance > REvo.Canvas.Right)
            {
                isInside = false;
            }
            if (testRect.Top - tolerance > REvo.Canvas.Top)
            {
                isInside = false;
            }
            else if (testRect.Bottom + tolerance < REvo.Canvas.Bottom)
            {
                isInside = false;
            }

            return isInside;
        }

        //==============================================================================================
        public void Inversion(double inverseRate, int rIdx)
        {
            Rnd = new Random((int)DateTime.Now.Ticks + rIdx);
            int rndIdx1, rndIdx2, counter = 0;
            RGen tempGen;
            int nrPolys = Gens.Count;
            //Form1 MainForm = ((Form1)Application.OpenForms["Form1"]);// Application.OpenForms[0];// ["Form1"];
            //int generat = MainForm.Generations;

            if ((Rnd.NextDouble() < inverseRate))
            {
                //-----------------------------------------------
                rndIdx1 = Rnd.Next(nrPolys - 2);
                int tmp = nrPolys - rndIdx1 - 1;
                rndIdx2 = rndIdx1 + Rnd.Next(tmp) + 1;
                RChromosome tmpChrom = new RChromosome(this);
               
                for (int i = 0; i < nrPolys; i++)
                {
                    if (counter > 0) break;
                    counter = 0;
                    if ((rndIdx1 <= i) & (i <= rndIdx2))
                    {
                        for (int j = rndIdx1; j <= rndIdx2; j++)
                        {
                            tempGen = tmpChrom.Gens[j];
                            Gens[rndIdx2 - counter] = new RGen(tempGen);
                            counter++;
                        }
                    }
                }
            }
        }

        //==============================================================================================
        public void Jump(double jumpRate)
        {
            double rndVal;
            RGen curGen;
            Vector2D newPos = new Vector2D(0, 0);
            Vector2D dPos = new Vector2D(0, 0);
            ALFGenerator newGenerator;

            for (int i = 0; i < Gens.Count; i++)
            {
                curGen = Gens[i];
                if (!curGen.Locked)
                {
                    rndVal = Rnd.NextDouble();
                    if (rndVal < (jumpRate))
                    {

                        newGenerator = new ALFGenerator((int)DateTime.Now.Ticks);
                        m_normDist = new NormalDistribution(newGenerator);
                        m_normDist.Sigma = 100;
                        newPos.X = Rnd.NextDouble() * REvo.BoundingRect.Width + REvo.Canvas.Left;
                        newPos.Y = Rnd.NextDouble() * REvo.BoundingRect.Height + REvo.Canvas.Top;
                        curGen.Center = newPos;
                    }
                    newGenerator = null;
                }
            }

        }

        //==============================================================================================
        public void MoveRandon(double rate, double sigmaValue)
        {
            RGen curGen;       
            //REvo.Rnd = new Random((int)DateTime.Now.Ticks);
            //if (_normDist.Sigma > 10) _normDist.Sigma = 10;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (Rnd.NextDouble() < rate)
                {
                    curGen = Gens[i];
                    Vector2D dPos = new Vector2D(0, 0);
                    ALFGenerator newGenerator = new ALFGenerator((int)DateTime.Now.Ticks);
                    m_normDist = new NormalDistribution(newGenerator);
                    m_normDist.Sigma = sigmaValue;// overlap / 1000;  

                    if (!curGen.Locked)
                    {
                        dPos.X = m_normDist.NextDouble() - 1;
                        dPos.Y = m_normDist.NextDouble() - 1;

                        Poly2D newRect = curGen.Poly.Clone();
                        newRect.Move(dPos);
                        if (CheckBorder(newRect) )
                            curGen.Move(dPos);
                    }
                    newGenerator = null;
                }
            }
        }

        //==============================================================================================
        public void ReplaceGen(RGen oldGen, RGen newGen)
        {
            int idx = Gens.IndexOf(oldGen);
            Gens[idx] = new RGen (newGen);
        }

        //==============================================================================================
        public void VarySize(double rate)
        {
            RGen curGen;
            double min = 50.0;
            double width, rVal;
            double height;
            //if (m_normDist.Sigma > 10) m_normDist.Sigma =10;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (Rnd.NextDouble() < rate)//_parentPolys[k].sizeRate)//
                {
                    double divider;
                    if (Rnd.NextDouble() < 0.01)
                        divider = (int)Math.Round((decimal)(REvo.BoundingRect.Height * REvo.BoundingRect.Width) / 10);
                    else
                        divider = (int)Math.Round((decimal)(REvo.BoundingRect.Height * REvo.BoundingRect.Width) / 180);

                    m_normDist.Sigma = this.FitRealOverl / divider;//// overlap / (1000.0) + 1;//2;//
                    curGen = Gens[i];
                    if (!curGen.Locked)
                    {
                        //rVal = (REvo.Rnd.Next(3) - 1) * 2;
                        rVal = m_normDist.NextDouble() - 1;
                        width = curGen.Width + rVal;
                        if (width < min) 
                            width = min;      //-- Minimale Scalierung X
                        else if (width > REvo.BoundingRect.Width) width = REvo.BoundingRect.Width; //-- Maximale Scalierung X
                        height = (curGen.Area / width);
                        curGen.Height = height;
                        curGen.Width = width;
                        if (curGen.Height < min)
                        {
                            curGen.Height = min;
                            curGen.Width = (curGen.Area / curGen.Height);
                        }
                        if (curGen.Height > REvo.BoundingRect.Height)
                        {
                            curGen.Height = REvo.BoundingRect.Height;
                            curGen.Width = (curGen.Area / curGen.Height);
                        }
                    }
                }
            }
        }

        public void VarySizeFull(double rate)
        {
            RGen curGen;
            double min = 50.0;
            double width, rVal;
            double height;
            //if (m_normDist.Sigma > 10) m_normDist.Sigma =10;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (Rnd.NextDouble() < rate)//_parentPolys[k].sizeRate)//
                {
                    ALFGenerator newGenerator = new ALFGenerator((int)DateTime.Now.Ticks);
                    m_normDist = new NormalDistribution(newGenerator);
                    m_normDist.Sigma = (int)Math.Round((decimal)(REvo.BoundingRect.Height + REvo.BoundingRect.Width) / (Gens.Count), 0);// this.fitReal / 800 + 0;//// overlap / (1000.0) + 1;//2;//
            
                    curGen = Gens[i];
                    if (!curGen.Locked)
                    {
                        //rVal = (REvo.Rnd.Next(3) - 1) * 2;
                        rVal = m_normDist.NextDouble() - 1;
                        width = curGen.Width + rVal;
                        if (width < min) 
                            width = min;      //-- Minimale Scalierung X
                        else if (width > REvo.BoundingRect.Width) width = REvo.BoundingRect.Width; //-- Maximale Scalierung X
                        height = (curGen.Area / width);
                        curGen.Height = height;
                        curGen.Width = width;
                        if (curGen.Height < min)
                        {
                            curGen.Height = min;
                            curGen.Width = (curGen.Area / curGen.Height);
                        }
                        if (curGen.Height > REvo.BoundingRect.Height)
                        {
                            curGen.Height = REvo.BoundingRect.Height;
                            curGen.Width = (curGen.Area / curGen.Height);
                        }
                    }
                    newGenerator = null;
                }
            }
        }

        public void VarySizeFull(double rate, double sigmaValue)
        {
            RGen curGen;
            double min = 50.0;
            double width, rVal;
            double height;          
            //if (m_normDist.Sigma > 10) m_normDist.Sigma =10;

            for (int i = 0; i < Gens.Count; i++)
            {
                if (Rnd.NextDouble() < rate)//_parentPolys[k].sizeRate)//
                {
                    ALFGenerator newGenerator = new ALFGenerator((int)DateTime.Now.Ticks);
                    m_normDist = new NormalDistribution(newGenerator);
                    m_normDist.Sigma = sigmaValue;
                    curGen = Gens[i];
                    if (!curGen.Locked)
                    {
                        //rVal = (REvo.Rnd.Next(3) - 1) * 2;
                        rVal = m_normDist.NextDouble() - 1;
                        width = curGen.Width + rVal;
                        if (width < min) 
                            width = min;      //-- Minimale Scalierung X
                        else if (width > REvo.BoundingRect.Width) width = REvo.BoundingRect.Width; //-- Maximale Scalierung X
                        height = (curGen.Area / width);
                        curGen.Height = height;
                        curGen.Width = width;
                        if (curGen.Height < min)
                        {
                            curGen.Height = min;
                            curGen.Width = (curGen.Area / curGen.Height);
                        }
                        if (curGen.Height > REvo.BoundingRect.Height)
                        {
                            curGen.Height = REvo.BoundingRect.Height;
                            curGen.Width = (curGen.Area / curGen.Height);
                        }
                    }
                    newGenerator = null;
                }
            }
        }

        //==============================================================================================
        //public double Overlap()
        //{
        //    double overlapAreaSum = 0;
        //    double shapeArea, diffArea;
        //    Rect2D rect1, rect2;
        //    Rect2D isectRect, boder;

        //    //Parallel.For(0, Gens.Count, delegate(int i)
        //    for (int i = 0; i < Gens.Count; i++)
        //    {
        //        rect1 = new Rect2D(Gens[i].RectGeometry);
        //        for (int k = i + 1; k < Gens.Count; k++)
        //        {
        //            rect2 = new Rect2D(Gens[k].RectGeometry);
        //            isectRect = rect1.IntersectionRect(rect2);
        //            if (isectRect != null)
        //            {
        //                shapeArea = isectRect.Area;
        //                overlapAreaSum = overlapAreaSum + shapeArea;
        //            }
        //        }
        //        boder = new Rect2D(REvo.Canvas);
        //        isectRect = rect1.IntersectionRect(boder);
        //        if (isectRect != null)
        //        {
        //            shapeArea = isectRect.Area;
        //            diffArea = rect1.Area - shapeArea;
        //        }
        //        else diffArea = rect1.Area;

        //        overlapAreaSum += diffArea;
                
        //    } //);
        //    return overlapAreaSum;
        //}

        public double Overlap()
        {
            double overlapAreaSum = 0;
            double shapeArea, diffArea =0;
            double rectArea = 0;
            Poly2D poly1, poly2;
            Poly2D boder;
            List<Poly2D> isectRect;

            //Parallel.For(0, Gens.Count, delegate(int i)
            for (int i = 0; i < Gens.Count; i++)
            {
                poly1 = new Poly2D(Gens[i].Poly);
                rectArea = poly1.Area;
                for (int k = i + 1; k < Gens.Count; k++)
                {
                    poly2 = new Poly2D(Gens[k].Poly);
                    isectRect = poly1.Intersection(poly2);
                    if (isectRect == null) shapeArea = 0;
                    else shapeArea = isectRect[0].Area;
                  //  if (shapeArea > 0)
                        overlapAreaSum = overlapAreaSum + shapeArea;
                }
                boder = new Poly2D(REvo.Canvas.Points);
                isectRect = boder.Intersection(poly1);
                if (isectRect == null) shapeArea = 0;
                else shapeArea = isectRect[0].Area;
                diffArea = rectArea - shapeArea;
                //if (diffArea > 0)
                    overlapAreaSum += diffArea;
            } //);
            return overlapAreaSum;
        }


        //==============================================================================================
        public double Overlap(RGen firstGen, RGen secondGen)
        {
            double overlapAreaSum = 0;
            double shapeArea;
            RectangleF rect1, rect2;
            RectangleF isectRect;

            rect1 = new RectangleF((float)(firstGen.Points[0].X), (float)(firstGen.Points[0].Y), (float)(firstGen.Width), (float)(firstGen.Height));
            rect2 = new RectangleF((float)(secondGen.Points[0].X), (float)(secondGen.Points[0].Y), (float)(secondGen.Width), (float)(secondGen.Height));
            isectRect = RectangleF.Intersect(rect1, rect2);
            shapeArea = isectRect.Width * isectRect.Height;
            overlapAreaSum = overlapAreaSum + shapeArea;

            return overlapAreaSum;
        }

        //==============================================================================================
        // calculate the distances 
        //public double Distances()
        //{
        //    Poly2D fistPoly, secondPoly, testPoly;
        //    double sum = 0, dist = 0;

        //    for (int i = 0; i < Gens.Count; i++)
        //    {
        //        fistPoly = Gens[i].ToPoly2D(); //new RPolygon(Pens.Black, Brushes.LightGray, Gens[i]); //(RPolygon)objects[i];
        //        for (int m = 0; m < fistPoly.neighbours.Count; m++)
        //        {
        //            for (int k = i+1; k < Gens.Count; k++)
        //            {
        //                testPoly = Gens[k].ToPoly2D();// new RPolygon(Pens.Black, Brushes.LightGray, Gens[k]); //(RPolygon)objects[k];
        //                if (fistPoly.neighbours[m] == testPoly.Id)
        //                {
        //                    secondPoly = testPoly;
        //                    dist = fistPoly.Distance(secondPoly);
        //                    if (dist < 0) dist = 0;
        //                    //dist *= 10;
        //                    dist += AdditionalDist(fistPoly, secondPoly, dist);
        //                    if (dist < 0) dist = 0;
        //                    dist += AdditionalDistFromOverlap(Gens[i], Gens[k]);
        //                    sum += dist;
        //                    secondPoly = null;
        //                    break;
        //                }
        //                testPoly = null;
        //                secondPoly = null;
        //            }
        //        }
        //        fistPoly = null;
        //    }
        //    return (int)Math.Round(sum);
        //}

        //==============================================================================================
        public double AdditionalDistFromOverlap(RGen fistGen, RGen secondGen)
        {
            double addDist = 0;

            RectangleF rect1, rect2;
            RectangleF isectRect;

            rect1 = new RectangleF((float)(fistGen.Points[0].X), (float)(fistGen.Points[0].Y), (float)(fistGen.Width), (float)(fistGen.Height));
            rect2 = new RectangleF((float)(secondGen.Points[0].X), (float)(secondGen.Points[0].Y), (float)(secondGen.Width), (float)(secondGen.Height));
            isectRect = RectangleF.Intersect(rect1, rect2);
            if (isectRect != null)
            {
                if (isectRect.Width < isectRect.Height) addDist = isectRect.Width;
                else addDist = isectRect.Height;
            }

            if (addDist <= RParameter.addDist) addDist = 0;
            else addDist = addDist - RParameter.addDist;

            return addDist;
        }

        //==============================================================================================
        //private int AdditionalDist(Poly2D fistPoly, Poly2D secondPoly, double dist)
        //{
        //    // the concept for this method is the same as for the Collision detection from http://www.codeproject.com/KB/GDI-plus/PolygonCollision.aspx
        //    fistPoly.FindMinMaxXY();
        //    secondPoly.FindMinMaxXY();
        //    int addDist = RParameter.addDist; //(int)((Form1)Application.OpenForms[0]).uD_RLap.Value; // minimum required distance
        //    int innerDist = 0;
        //    int tmpDist = 0;

        //    // --- test in x-direction ---
        //    if (fistPoly.minX < secondPoly.minX)
        //    {
        //        tmpDist = secondPoly.minX - fistPoly.maxX;
        //    } else
        //    {
        //        tmpDist = fistPoly.minX - secondPoly.maxX;
        //    }
        //    if (tmpDist < 0) innerDist += -1 * tmpDist;

        //    // --- test in y-direction ---
        //    if (fistPoly.minY < secondPoly.minY)
        //    {
        //        tmpDist = secondPoly.minY - fistPoly.maxY;
        //    }
        //    else
        //    {
        //        tmpDist = fistPoly.minY - secondPoly.maxY;
        //    }
        //    if (tmpDist < 0) innerDist += -1 * tmpDist;

        //    int returnDist = (addDist - innerDist);
        //    if (returnDist < 0) returnDist = 0;
        // //   int test = (returnDist - (int)Math.Round(dist, 0));

        // //   if (test < 0) test = 0;
        // //   return test;// returnDist;
        //    return returnDist;
        //}

        //==============================================================================================
        public void EvaluationProportion()
        {
            RGen curGen;
            double ratio = 0, sum = 0;
            for (int k = 0; k < Gens.Count; k++)
            {
                curGen = Gens[k];
                if (curGen.Width <= curGen.Height)
                    ratio = curGen.Width / curGen.Height;
                else
                    ratio = curGen.Height / curGen.Width;
                sum += ratio;
            }
            ProportionRatio = sum / Gens.Count;
        }
    }
}
       
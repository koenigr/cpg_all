﻿/**
 * Kremlas/MOES
 * \file RGen.cs
 *
 * <A HREF="http://www.entwurfsforschung.de">
 * Copyright (C) 2011 by Dr. Reinhard Koenig. Alle Rechte vorbehalten.
 * Schutzvermerk nach DIN ISO 16016 beachten
 * </A>
 * koenig@entwurfsforschung.de
 * $Author: koenig $
 * $Rev: 393 $
 * $Date: 2011-02-21 16:21:26 +0100 (Mo, 21. Feb 2011) $
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//using Topology.Geometries;
using System.Drawing.Drawing2D;
using CPlan.Geometry;

namespace CPlan.UrbanPattern
{
    public class RGen
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Private Fields
        //public Vector2D Velocity;
        //public double LimboWidth, LimboHeight;
        private Rect2D _rect;
        private double _rotation;
        public int Id;
        public List<int> Neighbours = new List<int>(); // list with neighbouring indizes
            
        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        public bool Free { get; set; }
        public Color FillColor { get; set; }
        public double MinSide { get; set; }// Math.Sqrt(fixArea) * 0.75;// 10.0;
        public bool Attractor { get; set; }
        public bool Locked { get; set; }

        //==============================================================================================
        public List<Vector2D> Points
        {
            get { return Poly.PointsFirstLoop.ToList(); }
        }

        //--------------------------------------------------------------------------------
        public Poly2D Poly
        {
            get
            {
                Poly2D curPoly = new Poly2D(_rect.Points);
                curPoly.Rotate(GeoAlgorithms2D.DegreeToRadian(Rotation));
                return curPoly;
            }
            //set { _geometry = value; }
        }

        public Vector2D Position
        {
            get { return _rect.Position; }
            set { _rect.Position = value; }
        }

        //==============================================================================================
        public double Rotation
        {
            get{return _rotation;}
            set
            {
                double divider = Math.Ceiling(value / 180);
                if (divider > 1)
                    _rotation = value - ((divider - 1) * 180);
                else _rotation = value;
            }
        }

        //==============================================================================================
        public double Area
        {
            get
            {
                return _rect.Area;
            }
            set
            {
                double proport = 1;
                if (Height > 0) proport =  Width / Height;

                double newWidth = Math.Sqrt(proport * value);
                double newHeith = Math.Sqrt(value / proport);
                Vector2D deltaSize = new Vector2D(-(newWidth - Width), newHeith- Height);
                if (newWidth > MinSide && newHeith > MinSide)
                {
                    Width = newWidth;
                    Height = newHeith;
                    Position += deltaSize/2;
                }
            }
        }

        //==============================================================================================
        public double Width
        {
            get
            {

                return _rect.Width;
            }
            set
            {
                _rect.Width = value;
            }
        }

        //==============================================================================================
        public double Height
        {
            get
            {
                return _rect.Height;
            }
            set
            {
                _rect.Height = value;
            }
        }

        //---------------------------------------------------------------------------------
        public double Left
        {
            get
            {
                return Poly.BoundingBox().Left;
            }
        }

        //---------------------------------------------------------------------------------
        public double Top
        {
            get
            {
                return Poly.BoundingBox().Top;
            }
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        //---------------------------------------------------------------------------------
        public RGen(double x, double y, int iniR, int vId)
        {
            double width, height;
            Vector2D Velocity = new Vector2D(0, 0);
            Id = vId;
            Vector2D centre = new Vector2D(x, y);
            //bRect = GeoAlgorithms2D.BoundingBox(border.Points);
            width = height = 50;// RndGlobal.Rnd.Next(10, 50);// 30;
            Rotation = RndGlobal.Rnd.Next(0, 360);
            _rect = new Rect2D(centre.X - width / 2, centre.Y - height / 2, width, height);
            Free = false;
            FillColor = Color.Gray;
            Attractor = false;
        }

        // --- copy constructor -----------------------------------------------------------
        public RGen(RGen sourceGen)
        {
            Vector2D Velocity = new Vector2D(0, 0);
            _rect = sourceGen._rect.Clone();
            Rotation = sourceGen.Rotation;
            MinSide = sourceGen.MinSide;
            //_geometry = sourceGen._geometry.Clone();
            Id = sourceGen.Id;
            Neighbours.Clear();
            for (int i = 0; i < sourceGen.Neighbours.Count; i++)
            {
                Neighbours.Add(sourceGen.Neighbours[i]);
            }
            Locked = sourceGen.Locked;
            Free = sourceGen.Free;
            FillColor = sourceGen.FillColor;
            Attractor = sourceGen.Attractor;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        // ==============================================================================================
        // ==============================================================================================
        // ==============================================================================================
        //public void Rotate90()
        //{
        //    double tmpWidth;
        //    tmpWidth = Width;
        //    Width = Height;
        //    Height = tmpWidth;
        //}

        //==============================================================================================
        public Vector2D Center
        {
            get
            {
                return _rect.Center ;
            }
            set
            {
                Vector2D difference = (value - Center);
                _rect.Move(difference);
            }
        }

        

        //==============================================================================================
        public void Move(Vector2D v)
        {
            _rect.Move(v);
        }

        //==============================================================================================
        /// <summary>
        /// Creates a roted minimal bounding rectangle. 
        /// </summary>
        /// <returns>Bounding rectangle.</returns>
        private Rect2D ApproxRect()
        {
            List<Vector2D> minBR = GeoAlgorithms2D.MinimalBoundingRectangle(Poly.PointsFirstLoop.ToList()).ToList();
            Vector2D vecA = minBR[1] - minBR[0];
            Vector2D vecB = new Vector2D(1, 0);
            double rotation = -(GeoAlgorithms2D.AngleBetweenR(vecA, vecB));
            if (double.IsNaN(rotation))
            {
                Rect2D approxRect = new Rect2D(Points[0], Points[2]);
                return approxRect;
            }
            else
            {
                Poly2D rotatedGeom = new Poly2D(GeoAlgorithms2D.RotatePoly(rotation, minBR[0], Poly.Points));
                Rect2D approxRect = GeoAlgorithms2D.BoundingBox(rotatedGeom.PointsFirstLoop);
                return approxRect;
            }
            
        }

         //==============================================================================================
        //public void CheckHeightWidt(double m_height)
        //{
        //    double min = 10.0;
        //    double dH = 1;// canvas.Height / 4;
        //    double dW = 1;// canvas.Width / 4;
        //    double m_width;

        //    if (m_height < min) m_height = min;                                            //-- Minimale Scalierung X
        //    else if (m_height > REvo.Canvas.Height - dW) m_height = REvo.Canvas.Height - dW; //-- Maximale Scalierung X
        //    m_width = (this.Area / m_height);
        //    this.Width = m_width;
        //    this.Height = m_height;
        //    if (this.Width  < min)
        //    {
        //        this.Width  = min;
        //        this.Height = (this.Area / this.Width );
        //    }
        //    if (this.Width > REvo.Canvas.Width - dH)
        //    {
        //        this.Width = REvo.Canvas.Width - dH;
        //        this.Height = (this.Area / this.Width);
        //    }
        //}

        //==============================================================================================
        public void CheckWidtHeight(double m_width)
        {
            double fixArea = this.Area;
            double min = MinSide;// Math.Sqrt(fixArea) * 0.75;// 10.0;
            double dH = 0;// canvas.Height / 4;
            double dW = 0;// canvas.Width / 4;
            double m_height;
            double limboHeight = this.Height;
            double limboWidth = this.Width;

            if (m_width < min) m_width = min;                                            //-- Minimale Scalierung X
            else if (m_width > REvo.BoundingRect.Width - dW) m_width = REvo.BoundingRect.Width - dW; //-- Maximale Scalierung X
            m_height = (fixArea / m_width);
            limboHeight = m_height;
            limboWidth = m_width;
            if (limboHeight < min)
            {
                limboHeight = min;
                limboWidth = (fixArea / limboHeight);
            }
            if (limboHeight > REvo.BoundingRect.Height - dH)
            {
                limboHeight = REvo.BoundingRect.Height - dH;
                limboWidth = (fixArea / limboHeight);
            }
            // execute new sizes
            if (limboHeight * limboWidth == fixArea)
            {
                double diffHeight = this.Height - limboHeight;
                double diffWidth = this.Width - limboWidth;
                this.Height = limboHeight;// -diffHeight / 2;
                this.Width = limboWidth;// -diffWidth / 2;
                this.Move(new Vector2D(diffWidth / 2, -diffHeight / 2));
            }
        }

        /// <summary>
        /// Check the rotation - width, height assignement
        /// </summary>
        /// <returns>Returns false if width and height needs to be considert inverse. </returns>
        public bool CheckRot()
        {
            double tester = Math.Round(Rotation / 90, 0);
            if (tester % 2 == 0) return true; // true, wenn Zahl gerade.
            else return false;
        }

        //==============================================================================================
        public Poly2D ToPoly2D()
        {
            return new Poly2D(Poly.PointsFirstLoop.ToArray());
        }

        # endregion

    }
}

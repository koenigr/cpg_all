/**
 * Kremlas/MOES
 * \file RObject.cs
 *
 * <A HREF="http://www.entwurfsforschung.de">
 * Copyright (C) 2011 by Dr. Reinhard Koenig. Alle Rechte vorbehalten.
 * Schutzvermerk nach DIN ISO 16016 beachten
 * </A>
 * koenig@entwurfsforschung.de
 * $Author: koenig $
 * $Rev: 390 $
 * $Date: 2011-02-11 11:12:42 +0100 (Fr, 11. Feb 2011) $
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using Topology.Geometries;
using Topology.CoordinateSystems.Transformations;
using System.Windows.Forms;
using CPlan.Geometry;

namespace CPlan.UrbanPattern
{

    public abstract class RGraphicObject
    {
        /// <summary>
        /// Dieses Pen-Objekt wird verwendet um zu �berpr�fen, ob ein
        /// Punkt �ber der Linie des Objekts liegt. Die Farbe ist
        /// hierf�r irrelevant. Als Breite hat sich 4 als sinnvoll erwiesen.
        /// </summary>
        static private Pen m_hitTestPen = new Pen(Brushes.Black, 4);
        private Pen m_pen;
        private Brush m_brush;
        private bool m_bVisible = true;
        private GraphicsPath m_path = new GraphicsPath();
        private LinearRing m_ring = null;
        private List<Vector2D> m_points = new List<Vector2D>();
        private List<Vector2D> m_edges = new List<Vector2D>();

        //----------------------------------------------------------------------------------------------
        public RGraphicObject(Pen pen, Brush brush)
        {
            m_pen = new Pen (pen.Color, 10);
            m_brush = brush;
        }

        //==============================================================================================
        //==============================================================================================
        protected GraphicsPath Path
        {
            get { return m_path; }
        }
        //==============================================================================================
        protected LinearRing Ring
        {
            get { return m_ring; }
            set { m_ring = value; }
        }

        //==============================================================================================
        /*public Vector Velocity
        {
            get { return _velocity; }
            set { _velocity = value; }
        }*/

        public List<Vector2D> Points
        {
            get { return m_points; }
        }

        //==============================================================================================
        public Pen Pen
        {
            get { return m_pen; }
            set { m_pen = value; }
        }

        //==============================================================================================
        public Brush Brush
        {
            get { return m_brush; }
            set { m_brush = value;}
        }

        //==============================================================================================
        public bool Visible
        {
            get { return m_bVisible; }
            set { m_bVisible = value; }
        }

        //==============================================================================================
        /// <summary>
        /// Befindet sich der angegebene Punkt �ber der Linie des Objekts?
        /// </summary>
        public virtual bool Hit(System.Drawing.Point pt)
        {
            return m_path.IsOutlineVisible(pt, m_hitTestPen);
        }

        //==============================================================================================
        /// <summary>
        /// Befindet sich der angegebene Punkt innerhalb des Objekts?
        /// </summary>
        public virtual bool Contains(System.Drawing.Point pt)
        {
            return m_path.IsVisible(pt);
        }

        //==============================================================================================
        public virtual void Draw(Graphics g)
        {
            g.FillPath(m_brush, m_path);
            g.DrawPath(m_pen, m_path);
        }

        //==============================================================================================
        /// <summary>
        /// Bewegt das Objekt um deltaX in x-Richtung und deltaY in y-Richtung.
        /// </summary>
        public virtual void Move(float deltaX, float deltaY)
        {
            Matrix mat = new Matrix();
            mat.Translate(deltaX, deltaY);
            m_path.Transform(mat);
        }

        //==============================================================================================
        public void BuildEdges()
        {
            Vector2D p1;
            Vector2D p2;
            m_edges.Clear();
            for (int i = 0; i < m_points.Count; i++)
            {
                p1 = m_points[i];
                if (i + 1 >= m_points.Count)
                {
                    p2 = m_points[0];
                }
                else
                {
                    p2 = m_points[i + 1];
                }
                m_edges.Add(p2 - p1);
            }
            CreatePath();

        }

        //==============================================================================================
        // --- Pfad erstellen ---
        public void CreatePath()
        {
            Vector2D curVect;
            Path.Reset();
            System.Drawing.PointF[] pathPoints = new System.Drawing.PointF[m_points.Count];
            for (int i = 0; i < m_points.Count; i++)
            {
                curVect = m_points[i];
                pathPoints[i].X = (float)curVect.X;
                pathPoints[i].Y = (float)curVect.Y;
            }
            Path.AddPolygon(pathPoints);
            //CreateRing();
        }

        //==============================================================================================
        public virtual void CreateRing()
        {
            PointF[] pts = m_path.PathPoints;
            Coordinate[] curCoords = new Coordinate[pts.Length + 1];

            for (int i = 0; i < pts.Length; i++)
            {
                curCoords[i] = new Coordinate(pts[i].X, pts[i].Y);
            }
            curCoords[pts.Length] = new Coordinate(pts[0].X, pts[0].Y);
            if (m_ring != null) m_ring = null;
            m_ring = new LinearRing(curCoords);
        }
    }

}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;

namespace CPlan.UrbanPattern
{
    [Serializable]
    public class ViewParameters
    {
        //==========================================================================================================
        [CategoryAttribute("Collission"), DescriptionAttribute("Force to repel the objects")]
        public double RepelForce { 
            get { return RParameter.repForce; }
            set { RParameter.repForce = value; }
        }
        [CategoryAttribute("Collission"), DescriptionAttribute("Repel rate")]
        public double RepelRate { 
            get { return RParameter.repRate; }
            set { RParameter.repRate = value; }
        }

        [CategoryAttribute("Collission"), DescriptionAttribute("Change proportion")]
        public bool PropChange
        {
            get { return RParameter.propChange; }
            set { RParameter.propChange = value; }
        }

        [CategoryAttribute("Collission"), DescriptionAttribute("Force to change the proportion")]
        public double PropForce
        {
            get { return RParameter.propForce; }
            set { RParameter.propForce = value; }
        }
        [CategoryAttribute("Collission"), DescriptionAttribute("Proportion change rate")]
        public double PropRate
        {
            get { return RParameter.propRate; }
            set { RParameter.propRate = value; }
        }

        [CategoryAttribute("Collission"), DescriptionAttribute("Rotation")]
        public bool Rotate
        {
            get { return RParameter.rotate; }
            set { RParameter.rotate = value; }
        }
        [CategoryAttribute("Collission"), DescriptionAttribute("Force for rotation")]
        public double RotForce
        {
            get { return RParameter.rotForce; }
            set { RParameter.rotForce = value; }
        }
        [CategoryAttribute("Collission"), DescriptionAttribute("Rotation rate")]
        public double RotRate
        {
            get { return RParameter.rotRate; }
            set { RParameter.rotRate = value; }
        }

        [CategoryAttribute("Collission"), DescriptionAttribute("Rotateion by border collission")]
        public bool BorderRot
        {
            get { return RParameter.borderRot; }
            set { RParameter.borderRot = value; }
        }
        [CategoryAttribute("Collission"), DescriptionAttribute("Force for rotation by border collission")]
        public double BorderRotForce
        {
            get { return RParameter.borderRotForce; }
            set { RParameter.borderRotForce = value; }
        }
        [CategoryAttribute("Collission"), DescriptionAttribute("Rotation rate by border collission")]
        public double BorderRotRate
        {
            get { return RParameter.borderRotRate; }
            set { RParameter.borderRotRate = value; }
        }

        [CategoryAttribute("Collission"), DescriptionAttribute("Force for rotation")]
        public double AttractForce
        {
            get { return RParameter.attractForce; }
            set { RParameter.attractForce = value; }
        }
        [CategoryAttribute("Collission"), DescriptionAttribute("Rotation rate")]
        public double AttractRadius
        {
            get { return RParameter.attractRadius; }
            set { RParameter.attractRadius = value; }
        }

        //==========================================================================================================
        [CategoryAttribute("Layout"), DescriptionAttribute("Building desnity in %")]
        public int Density
        {
            get { return RParameter.density; }
            set { RParameter.density = value; }
        }
        [CategoryAttribute("Layout"), DescriptionAttribute("Number of houses")]
        public int Buildings_Nr
        {
            get { return RParameter.roomsNr; }
            set { RParameter.roomsNr = value; }
        }
        [CategoryAttribute("Layout"), DescriptionAttribute("Update Isovist field after moving a object")]
        public bool UpdateIsovist
        {
            get { return RParameter.instantUpdate; }
            set { RParameter.instantUpdate = value; }
        }

        //==========================================================================================================
        [CategoryAttribute("Optimization"), DescriptionAttribute("Allow jumping objects")]
        public bool Jumping
        {
            get { return RParameter.jumps; }
            set { RParameter.jumps = value; }
        }
        
        [CategoryAttribute("Optimization"), DescriptionAttribute("Population size")]
        public int PopSize
        {
            get { return RParameter.popSize; }
            set { RParameter.popSize = value; }
        }
        [CategoryAttribute("Optimization"), DescriptionAttribute("Population preasure")]
        public double Pressure
        {
            get { return RParameter.pressVal; }
            set { RParameter.pressVal = value; }
        }
        [CategoryAttribute("Optimization"), DescriptionAttribute("Crossover rate")]
        public double CrossoverRate
        {
            get { return RParameter.crossRate; }
            set { RParameter.crossRate = value; }
        }
        [CategoryAttribute("Optimization"), DescriptionAttribute("Activate crossover")]
        public Boolean Crossover
        {
            get { return RParameter.doCross; }
            set { RParameter.doCross = value; }
        }
        [CategoryAttribute("Optimization"), DescriptionAttribute("Number of parents for crossover")]
        public int pCross
        {
            get { return RParameter.p; }
            set { RParameter.p = value; }
        }

    }

    //==========================================================================================================
    [Serializable]
    public class RParameter
    {
        # region StreetNetwork
        public static double repForce = 0.6;
        public static double repRate = 1;

        public static bool propChange = true;
        public static double propForce = 0.4;
        public static double propRate = 1;
        public static double rotForce = 0.25;
        public static double rotRate = 1;

        public static bool rotate = true; 
        public static bool borderRot = true; 
        public static double borderRotForce = 1.0;
        public static double borderRotRate = 1.0;

        public static double attractForce = 0.1;
        public static double attractRadius = 20;
        
        public static int popSize = 1;
        public static double pressVal = 2;
        public static double crossRate = 0.75;
        public static Boolean doCross = true;
        public static Boolean elite = false;
        public static Boolean plusSel = true;

        public static int density = 50;
        public static int roomsNr = 50;
        public static double thresH = roomsNr * 1.25;

        public static double sDamp = 0.5;
        public static int addDist = 50;

        public static int p = 2;

        public static bool jumps = true;
        public static bool instantUpdate = false;
        # endregion;

    }
}

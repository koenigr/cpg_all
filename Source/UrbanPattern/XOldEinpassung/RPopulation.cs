﻿/**
 * Kremlas/MOES
 * \file RPopulation.cs
 *
 * <A HREF="http://www.entwurfsforschung.de">
 * Copyright (C) 2011 by Dr. Reinhard Koenig. Alle Rechte vorbehalten.
 * Schutzvermerk nach DIN ISO 16016 beachten
 * </A>
 * koenig@entwurfsforschung.de
 * $Author: koenig $
 * $Rev: 393 $
 * $Date: 2011-02-21 16:21:26 +0100 (Mo, 21. Feb 2011) $
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using CPlan.Geometry;
using CPlan.VisualObjects;

namespace CPlan.UrbanPattern
{
    public class RPopulation : List<RChromosome>
    {
        //private Random Rnd = new Random((int)DateTime.Now.Ticks);

        public RPopulation() {}
        //==============================================================================================
        //-------------find solution with smallest overlapping------------------------------------------
        public RChromosome FindFittestOverlap()
        {
            RChromosome curChrom, bestSol = null;
            double testValue = double.MaxValue;
            //int minIdx = 0;
            for (int i = 0; i < this.Count; i++) //--- random sizes for the polys ---
            {
                curChrom = this[i];
                if (testValue > curChrom.FitRealOverl)
                {
                    testValue = curChrom.FitRealOverl;
                    bestSol = curChrom;
                }
            }
            return bestSol;
        }

        //==============================================================================================
        //public RChromosome FindFittestDistance()
        //{
        //    RChromosome curChrom, bestSol = null;
        //    double testValue = double.MaxValue;
        //    //int minIdx = 0;
        //    for (int i = 0; i < this.Count; i++) //--- random sizes for the polys ---
        //    {
        //        curChrom = this[i];
        //        if (testValue > curChrom.FitRealDist)
        //        {
        //            testValue = curChrom.FitRealDist;
        //            bestSol = curChrom;
        //        }
        //    }
        //    return bestSol;
        //}
        //==============================================================================================
        //-------------find solution with largest overlapping------------------------------------------
        public RChromosome FindWorstOverlap()
        {
            RChromosome curChrom, worstSol = null;
            double testValue = -1;
            //int minIdx = 0;
            for (int i = 0; i < this.Count; i++) //--- random sizes for the polys ---
            {
                curChrom = this[i];
                if (testValue < curChrom.FitRealOverl)
                {
                    testValue = curChrom.FitRealOverl;
                    worstSol = curChrom;
                }
            }
            return worstSol;
        }

        //==============================================================================================
        //public RChromosome FindWorstDistance()
        //{
        //    RChromosome curChrom, worstSol = null;
        //    double testValue = -1;
        //    //int minIdx = 0;
        //    for (int i = 0; i < this.Count; i++) //--- random sizes for the polys ---
        //    {
        //        curChrom = this[i];
        //        if (testValue < curChrom.FitRealDist)
        //        {
        //            testValue = curChrom.FitRealDist;
        //            worstSol = curChrom;
        //        }
        //    }
        //    return worstSol;
        //}

        //==============================================================================================
        //public List<RChromosome> NonDominatedSet()
        //{
        //    List<RChromosome> NonDomSet = new List<RChromosome>();
        //    bool dominated = false;
        //    RChromosome curIndi, otherIndi;
        //    for (int i = 0; i < this.Count(); i++)
        //    {
        //        curIndi = this[i];
        //        dominated = false;
        //        for (int k = 0; k < this.Count(); k++)
        //        {
        //            if (!(i == k))
        //            {
        //                otherIndi = this[k];
        //                if (!dominated) // first objective
        //                {
        //                    if (curIndi.FitRealOverl > otherIndi.FitRealOverl)
        //                    {
        //                        if (curIndi.FitRealDist > otherIndi.FitRealDist)
        //                        {
        //                            dominated = true;
        //                            break;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        if (!dominated) // if the solution dominates in one criteria = weak domination = pareto-optimal
        //        {
        //            NonDomSet.Add(curIndi);
        //        }
        //    }
        //    return NonDomSet;
        //}

        //==============================================================================================
        //public void RemoveSolutionInNonDominatedSet()
        //{
        //    int MaxSize = 50;
        //    this.EvaluationOverlap();
        //    //this.EvaluationDist();
        //    List<RChromosome> NonDominatedList = this.NonDominatedSet();
        //    this.Clear(); // delete actual NonDominatedList
        //    this.AddRange(NonDominatedList); // save new NonDominatedList
        //    /*for (int i = 0; i < NonDominatedList.Count; i++)
        //    {
        //        this.Add(NonDominatedList[i]);
        //    }*/
        //    //RemoveByRanking(MaxSize);
        //    RemoveMostDense(MaxSize);
        //    //((Form1)Application.OpenForms[0]).lbl_succsess.Text = this.Count().ToString(); // NonDominatedList.Count().ToString();
        //}

        //==============================================================================================
        private void RemoveByRanking(int MaxSize)
        {

        }

        //==============================================================================================
        //private void RemoveMostDense(int MaxSize)
        //{
        //    RChromosome CurChrom;
        //    List<RChromosome> TempList = new List<RChromosome>();
        //    int MaxValue = 0;
        //    int RndIdx;
            
        //    CalculateSqueezeFactor();// calculate squeeze factor
        //    while (this.Count > MaxSize)
        //    {
        //        // --- find solutions in the most dense grids
        //        MaxValue = 0;
        //        for (int i = 0; i < this.Count; i++)
        //        {
        //            CurChrom = this[i];
        //            if (CurChrom.SqueezeFit > MaxValue) MaxValue = CurChrom.SqueezeFit;
        //        }
        //        TempList.Clear();
        //        for (int i = 0; i < this.Count; i++)
        //        {
        //            CurChrom = this[i];
        //            if (CurChrom.SqueezeFit == MaxValue) TempList.Add(CurChrom);
        //        }

        //        // --- delete randomly one of the found solutions
        //        RndIdx = RndGlobal.Rnd.Next(TempList.Count);
        //        CurChrom = TempList[RndIdx];
        //        this.Remove(CurChrom);
        //    }
        //}

        //==============================================================================================
        //private void CalculateSqueezeFactor()
        //{
        //    RChromosome CurChrom;
        //    int NrRooms = 10;
        //    int NrGrid = 64; // Versch dichten Sinnvoll? Höhere bei Overlappings?
        //    int[,] Grid = new int[NrGrid, NrGrid];
        //    int XG, YG;
        //    double GridCellSize = 1.0 / NrGrid;
        //    // --- Normalise ---
        //    double test = RndGlobal.Faculty(NrRooms);
        //    double MaxOverlap = (double)REvo.Canvas.Width * REvo.Canvas.Height;// MyTool.Faculty(NrRooms) * (double)(canvas.Width * canvas.Height / NrRooms);
        //    double MaxDist = (REvo.Canvas.Width * REvo.Canvas.Height)/10;// Math.Sqrt(Math.Pow(REvo.Canvas.Width, 2) + Math.Pow(REvo.Canvas.Height, 2));
        //    for (int i = 0; i < this.Count; i++)
        //    {
        //        CurChrom = this[i];
        //        CurChrom.NormFitOverl = CurChrom.FitRealOverl / MaxOverlap;
        //        CurChrom.NormFitDist = CurChrom.FitRealDist / MaxDist;
        //        // --- count solutions per cell ---
        //        XG = (int)Math.Truncate(CurChrom.NormFitOverl / GridCellSize);
        //        YG = (int)Math.Truncate(CurChrom.NormFitDist / GridCellSize);
        //        if (XG >= NrGrid) XG = NrGrid - 1;
        //        if (YG >= NrGrid) YG = NrGrid - 1;
        //        Grid[XG, YG]++;
        //        CurChrom.GridAdress[0] = XG;
        //        CurChrom.GridAdress[1] = YG;
        //    }
        //    // --- set Squeez-Fitness
        //    for (int i = 0; i < this.Count; i++)
        //    {
        //        CurChrom = this[i];
        //        CurChrom.SqueezeFit = Grid[CurChrom.GridAdress[0], CurChrom.GridAdress[1]];
        //    }
        //}

        //==============================================================================================
        public void EvaluationOverlap()
        {
            RChromosome curGenom;
            double overL;
            for (int i = 0; i < this.Count; i++)
            {
                curGenom = this[i];
                curGenom.OldFitness = curGenom.FitOverl;
                overL = curGenom.Overlap();// calculte the fitness from the overlap-values
                curGenom.FitRealOverl = overL;
                if (overL >= 1)
                {
                    curGenom.FitOverl = 1 / overL;
                }
                else curGenom.FitOverl = 1;
            }
        }

        //==============================================================================================
        public void EvaluationOverlap(RChromosome curGenom)
        {
            double overL;
            //for (int i = 0; i < this.Count; i++)
            //{
                curGenom.OldFitness = curGenom.FitOverl;
                overL = curGenom.Overlap();// calculte the fitness from the overlap-values
                curGenom.FitRealOverl = overL;
                if (overL >= 1)
                {
                    curGenom.FitOverl = 1 / overL;
                }
                else curGenom.FitOverl = 1;
            //}
        }

        //==============================================================================================
        //public void EvaluationDist()
        //{
        //    RChromosome curGenom;
        //    double dist;
        //    for (int i = 0; i < this.Count; i++)
        //    {
        //        curGenom = this[i];
        //        curGenom.OldFitness = curGenom.FitDist;
        //        dist = curGenom.Distances();// calculte the fitness from the overlap-values
        //        curGenom.FitRealDist = dist;
        //        if (dist >= 1) curGenom.FitDist = 1 / dist;
        //        else curGenom.FitDist = 1;
        //    }
        //}

        //==============================================================================================
        //public void EvaluationDist(RChromosome curGenom)
        //{
        //    double dist;
        //    curGenom.OldFitness = curGenom.FitDist;
        //    dist = curGenom.Distances();// calculte the fitness from the overlap-values
        //    curGenom.FitRealDist = dist;
        //    if (dist >= 1) curGenom.FitDist = 1 / dist;
        //    else curGenom.FitDist = 1;
        //}

        //==============================================================================================
        public List<GPolygon> BuildPhenotypeGl()
        {
            List<GPolygon> TempList = new List<GPolygon>();
            GPolygon curRect;
            RChromosome curChrom;

            if ((Application.OpenForms.Count > 0))
            {
                curChrom = this.FindFittestOverlap();
                if (curChrom != null)
                {
                    for (int i = 0; i < curChrom.Gens.Count(); i++)
                    {                  
                        RGen curGen = curChrom.Gens[i];
                        //if (!curGen.Free)
                        {
                            curRect = new GPolygon(curGen.Poly);
                            curRect.BorderWidth = 5;
                            curRect.FillColor = ConvertColor.CreateColor4(curGen.FillColor);
                            if (curGen.Free) curRect.BorderWidth = 1;
                            curRect.Filled = !curGen.Free;
                            TempList.Add(curRect);
                        }
                    }
                }
            }
            return TempList;
        }
    }
}

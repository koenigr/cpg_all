﻿/**
 * Kremlas/MOES
 * \file RRepel.cs
 *
 * <A HREF="http://www.entwurfsforschung.de">
 * Copyright (C) 2011 by Dr. Reinhard Koenig. Alle Rechte vorbehalten.
 * Schutzvermerk nach DIN ISO 16016 beachten
 * </A>
 * koenig@entwurfsforschung.de
 * $Author: koenig $
 * $Rev: 390 $
 * $Date: 2011-02-11 11:12:42 +0100 (Fr, 11. Feb 2011) $
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CPlan.Geometry;

namespace CPlan.UrbanPattern
{
    public static class RRepel
    {
        static private Collision m_collision = new Collision();

        //==============================================================================================
        public static void Repel(this RGen curGen, RGen otherGen, double force)
        {
            RepelMe(curGen, otherGen, force);
        }

        //==============================================================================================
        public static void Repel(this RGen curGen, List<RGen> Gens, double force)
        {
            for (int i = 0; i < Gens.Count; i++)
            {
                if (curGen != Gens[i])
                {
                   // if (rnd.NextDouble() < 0.15)
                    RepelMe(curGen, Gens[i], force);
                }
            }
        }

        //==============================================================================================
        public static void RepelMe(RGen thisGen, RGen otherGen, double force)
        {
            Vector2D velocity = new Vector2D(0, 0);
            Vector2D translation = velocity;
            //double factor = 0.5;
            PolygonCollisionResult r = m_collision.PolygonCollision(thisGen.ToPoly2D(), otherGen.ToPoly2D(), velocity);
            if (r.WillIntersect)
            {
                translation = velocity + r.MinimumTranslationVector;
                translation.Multiply(force);

                if (!thisGen.Locked) 
                    thisGen.Move(translation);

                translation.Invert();
                if (!otherGen.Locked)
                    otherGen.Move(translation);
            }
        }
    }
}

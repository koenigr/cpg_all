﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Program.cs 
//  Copyright (C) 2014/10/18  12:57 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Matthias Standfest, Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using CPlan.Geometry;

namespace VarietyTest
{
    class Program
    {
        static void Main(string[] args)
        {
            // create sample data
            List<float[]> bitmapsDiff = new List<float[]>();
            List<float[]> bitmapsSame = new List<float[]>();
            List<float[]> bitmapsSimilar = new List<float[]>();
            
            int size = 9;
            bitmapsDiff.Add(new float[]{    1,1,0, 0,0,0, 1,1,0,
                                            1,1,0, 0,1,1, 0,0,0,
                                            0,0,0, 0,1,1, 1,1,0});

            bitmapsDiff.Add(new float[] {   0,0,0, 0,1,1, 1,1,0,
                                            1,1,0, 0,0,0, 1,1,0,
                                            1,1,0, 0,1,1, 0,0,0});

            bitmapsDiff.Add(new float[] {   1,1,0, 0,1,1, 0,0,0,
                                            0,0,0, 0,1,1, 1,1,0,
                                            1,1,0, 0,0,0, 1,1,0});

            bitmapsDiff.Add(new float[] {   0,1,1, 0,0,0, 0,1,1,
                                            0,1,1, 1,0,1, 0,0,0,
                                            0,0,0, 1,0,1, 0,1,1});

            bitmapsDiff.Add(new float[] {   0,0,0, 1,0,1, 0,1,1,
                                            0,1,1, 0,0,0, 0,1,1,
                                            0,1,1, 1,0,1, 0,0,0});

            bitmapsDiff.Add(new float[] {   0,1,1, 1,0,1, 0,0,0,
                                            0,0,0, 1,0,1, 0,1,1,
                                            0,1,1, 0,0,0, 0,1,1});

            bitmapsDiff.Add(new float[] {   1,0,1, 0,0,0, 1,0,1,
                                            1,0,1, 1,1,0, 0,0,0,
                                            0,0,0, 1,1,0, 1,0,1});

            bitmapsDiff.Add(new float[] {   0,0,0, 1,1,0, 1,0,1,
                                            1,0,1, 0,0,0, 1,0,1,
                                            1,0,1, 1,1,0, 0,0,0});

            bitmapsDiff.Add(new float[] {   1,0,1, 1,1,0, 0,0,0,
                                            0,0,0, 1,1,0, 1,0,1,
                                            1,0,1, 0,0,0, 1,0,1});

            //------------------------------------------------------
            //------------ all the same ----------------------------
            for (int j = 0; j < 9; j++)
            {
                bitmapsSame.Add(new float[]
                {
                    1, 1, 0,  0, 0, 0,  0, 1, 1,
                    1, 1, 0,  0, 1, 1,  0, 1, 1,
                    0, 0, 0,  0, 1, 1,  0, 0, 0
                });
            }

            //------------------------------------------------------
            //------------ very small change -----------------------
            for (int j = 0; j < 8; j++)
            {
                bitmapsSimilar.Add(new float[]
                {
                    1, 1, 0,  0, 0, 0,  0, 1, 1,
                    1, 1, 0,  0, 1, 1,  0, 1, 1,
                    0, 0, 0,  0, 1, 1,  0, 0, 0
                });
            }
            bitmapsSimilar.Add(new float[]
                {
                    1, 1, 0,  0, 0, 0,  0, 1, 1,
                    1, 1, 0,  0, 1, 1,  0, 1, 1,
                    0, 0, 0,  0, 1, 1,  0, 0, 1
                });

            //eo create sample data
            Statistics STAT = new Statistics(bitmapsDiff, size); // construct statistics
            STAT.Calc();
            Console.WriteLine("=============================================");
            Console.WriteLine("============== Different ====================");
            Console.WriteLine("RANGE: " + STAT.DELTA_RANGE);
            Console.WriteLine("avg distance: " + STAT.DELTA_AVG);
            Console.WriteLine("poisson lambda (bin index @ 10% steps): " + STAT.POISSON_LAMBDA);
            Console.WriteLine("poisson error: " + STAT.POISSON_ERROR);
            //STAT.PrintSmoothBMPs();
            Console.Write("\n");
            Console.Write("Deltas_HGRAM: \n");
            STAT.PrintDELTAS_HGRAM();
            Console.Write("\n");
            Console.WriteLine("=============================================");
            Console.Write("\n");

            STAT = new Statistics(bitmapsSame, size); // construct statistics
            STAT.Calc();
            Console.WriteLine("=============================================");
            Console.WriteLine("================ Same =======================");
            Console.WriteLine("RANGE: " + STAT.DELTA_RANGE);
            Console.WriteLine("avg distance: " + STAT.DELTA_AVG);
            Console.WriteLine("poisson lambda (bin index @ 10% steps): " + STAT.POISSON_LAMBDA);
            Console.WriteLine("poisson error: " + STAT.POISSON_ERROR);
            //STAT.PrintSmoothBMPs();
            Console.Write("\n");
            Console.Write("Deltas_HGRAM: \n");
            STAT.PrintDELTAS_HGRAM();
            Console.Write("\n");
            Console.WriteLine("=============================================");
            Console.Write("\n");

            STAT = new Statistics(bitmapsSimilar, size); // construct statistics
            STAT.Calc();
            Console.WriteLine("=============================================");
            Console.WriteLine("================ Similar =======================");
            Console.WriteLine("RANGE: " + STAT.DELTA_RANGE);
            Console.WriteLine("avg distance: " + STAT.DELTA_AVG);
            Console.WriteLine("poisson lambda (bin index @ 10% steps): " + STAT.POISSON_LAMBDA);
            Console.WriteLine("poisson error: " + STAT.POISSON_ERROR);
            //STAT.PrintSmoothBMPs();
            Console.Write("\n");
            Console.Write("Deltas_HGRAM: \n");
            STAT.PrintDELTAS_HGRAM();
            Console.Write("\n");
            Console.WriteLine("=============================================");

            Console.WriteLine("\n\nPress [enter] to close...");
            Console.ReadLine();
        }
    }
}

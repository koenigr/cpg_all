﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Bodies.cs 
//  Copyright (C) 2014/10/18  12:53 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using OpenTK;
using System.ComponentModel;

namespace CPlan.VisualObjects
{
    /// <summary>
    /// Represents the basic class for all body objects. 
    /// Inherit from <see cref="GeoObject"/> class.
    /// </summary>
    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public abstract class Bodies : GeoObject
    {
        //############################################################################################################################################################################
        # region Properties

        /// <summary> The border color (as vector) of the volume object. </summary>
        public Vector4 BorderColor { get; set; }

        /// <summary> The with of the border lines of the volume object. </summary>
        public int BorderWidth { get; set; }

        /// <summary> The fill color (as vector) of the volume object. </summary>
        public Vector4 FillColor { get; set; }

        /// <summary> Indicates if the volume is filled. </summary>
        public bool Filled { get; set; }

        /// <summary> The z-koordinate of the volume object.
        /// It's only useful in case of surfaces converted to bodies, see e.g. GPrism class.
        ///  </summary>
        public float DeltaZ { get; set; }

        # endregion

         //############################################################################################################################################################################
        # region Constructors

        //============================================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>Bodies</b> class.
        /// </summary>
        /// <remarks>
        /// The preset values are for: 
        /// FillColor=new Vector4(1,1,1,1) (white and opaque);
        /// BorderColor=new Vector4(0,0,0,1) (black and opaque);
        /// BorderWidth=1.
        /// </remarks>
        public Bodies()
        {
            FillColor = new Vector4(1,1,1, 1);
            BorderColor = new Vector4(0,0,0,1);
            BorderWidth = 1;
        }

        #endregion

        //############################################################################################################################################################################
        # region Methods

        //============================================================================================================================================================================
        /// <summary>
        /// Computes the area of the <b>Bodies</b>
        /// </summary>
        /// <returns>
        /// The volume of the <b>Bodies</b>, as <see cref="double"/> value.
        /// </returns>
        /// <remarks><para>
        /// The GetVolume Method needs to be implemented/overridden by all objects that inherit from <b>Surfaces</b>.
        /// </para></remarks>
        public abstract double GetVolume();

        #endregion
       
    }

}

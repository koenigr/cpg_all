﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GeoObject.cs 
//  Copyright (C) 2014/10/18  12:53 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Drawing;
using CPlan.Geometry;
using OpenTK;
using System.ComponentModel;

namespace CPlan.VisualObjects
{
    /// <summary>
    /// Represents the basic class for all GeoObjects.
    /// </summary>
    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public abstract class GeoObject
    {
        //############################################################################################################################################################################
        # region Properties

        /// <summary> Color for selecting stuff  </summary>
        //public Vector4 SelectColor { get; set; }
        public Color SelectColor { get; set; }

        /// <summary> Z-Buffer for display </summary>
        public uint Buffer { get; set; }

        /// <summary> Flag for showing or hiding the object </summary>
        public bool Display { get; set; }

        //protected int m_ID;
        /// <summary> Get the ID of a object. </summary>
        public int Identity { get; set; }


        public float HandleSize = 0.4f;
        public float PickingThreshold = 0.1f;
        public static float  m_zoomFactor = 1.0f;

        // added by ZW
        /// <summary> The unique color of the object for object picking </summary>
        public int UniqueColor { get; set; }
        
        # endregion

        //############################################################################################################################################################################
        # region Constructors

        //============================================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>GeoObject</b> class.
        /// </summary>
        public GeoObject()
        {
            //Path = new GraphicsPath();
            SelectColor = Color.Yellow;// new Vector4(0.4f, 1, 0.0f, 1);
            Buffer = 1000;
            Display = true;
        }

        # endregion

        //############################################################################################################################################################################
        # region Methods

        //============================================================================================================================================================================
        /// <summary>
        /// Draw the object. 
        /// Needs to be impemented by all derived classes.
        /// </summary>
        public abstract void Draw();

        //============================================================================================================================================================================
        /// <summary>
        /// Move the object. 
        /// Needs to be impemented by all derived classes.
        /// </summary>
        public abstract void Move(Vector2d delta);

        //============================================================================================================================================================================
        /// <summary>
        /// Highlights the object, part of an object, that is touched by the mousecursor. 
        /// Needs to be impemented by all derived classes.
        /// </summary>
        public abstract bool Selector(Vector2d delta);


        //============================================================================================================================================================================
        /// <summary>
        /// The bounding box of a geometry.
        /// </summary>
        /// <param name="box">Bounding box/rectangle.</param>
        public abstract void GetBoundingBox(BoundingBox box);

        //============================================================================================================================================================================
        /// <summary>
        /// Test, if a point is located on or in a geometry.
        /// </summary>
        /// <returns>If a geometry contains a point.</returns>
        public abstract bool ContainsPoint(Vector2d p);

        # endregion
    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Lines.cs 
//  Copyright (C) 2014/10/18  12:53 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Drawing;
using OpenTK;

namespace CPlan.VisualObjects
{
    /// <summary>
    /// Represents the basic class for all line objects. 
    /// Inherit from <see cref="GeoObject"/> class.
    /// </summary>
    [Serializable]
    public abstract class Lines : GeoObject
    {
        //############################################################################################################################################################################
        # region Properties

        /// <summary> The color (as vector) of the line object. </summary>
        public Vector4 Color{ get; set; }

        /// <summary> The width ot the line object. </summary>
        public int Width { get; set; }

        /// <summary> The z-koordinate of the line object. </summary>
        public float DeltaZ { get; set; }


        # endregion

        //############################################################################################################################################################################
        # region Constructors

        //============================================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>Lines</b> class.
        /// </summary>
        /// <remarks>
        /// The preset values are for Color=new Vector4(0,0,0,1) (black and opaque) and for Width=1.
        /// </remarks>
        public Lines()
        {
            Color = new Vector4(0,0,0,1);
            
            Width = 1;
        }

        #endregion

        //############################################################################################################################################################################
        # region Methods

        //============================================================================================================================================================================
        /// <summary>
        /// Set a new color vector.
        /// </summary>
        /// <param name="col">A System.Drawing.Color can be used.</param>
        /// <param name="alpha">The trancparency value [0,1]. 
        /// The line is completely transparent with alpha=0.</param>
        public void SetColor(Color col, float alpha)
        {
            Color = new Vector4(col.R / 255, col.G / 255, col.B / 255, alpha);
        }

        #endregion
    }
}

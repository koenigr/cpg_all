﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Surfaces.cs 
//  Copyright (C) 2014/10/18  12:53 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using OpenTK;
using System.ComponentModel;

namespace CPlan.VisualObjects
{
    /// <summary>
    /// Represents the basic class for all survace objects. 
    /// Inherit from <see cref="GeoObject"/> class.
    /// </summary>
    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public abstract class Surfaces : GeoObject
    {
        //############################################################################################################################################################################
        # region Properties

        /// <summary> The border color (as vector) of the survace object. </summary>
        public Vector4 BorderColor { get; set; }

        /// <summary> The with of the border lines of the survace object. </summary>
        public int BorderWidth { get; set; }

        /// <summary> The fill color (as vector) of the survace object. </summary>
        public Vector4 FillColor { get; set; }

        /// <summary> 
        /// The transparency of the fill color - need to be set after the FillColor! 
        /// 1 is opaque. 
        /// </summary>
        public float Transparency { get; set; }

        /// <summary> Indicates if the survace is filled. </summary>
        public bool Filled { get; set; }

        /// <summary> The z-koordinate of the survace object. </summary>
        public float DeltaZ { get; set; }

        # endregion

        //############################################################################################################################################################################
        # region Constructors

        //============================================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>Surfaces</b> class.
        /// </summary>
        /// <remarks>
        /// The preset values are for: 
        /// FillColor=new Vector4(1,1,1,1) (white and opaque);
        /// BorderColor=new Vector4(0,0,0,1) (black and opaque);
        /// BorderWidth=1.
        /// </remarks>
        public Surfaces()
        {
            FillColor = new Vector4(1,1,1, 1);
            BorderColor = new Vector4(0,0,0,1);
            BorderWidth = 1;
        }

        #endregion

        //############################################################################################################################################################################
        # region Methods

        //============================================================================================================================================================================
        /// <summary>
        /// Computes the area of the <b>Surfaces</b>
        /// </summary>
        /// <returns>
        /// The area of the <b>Surfaces</b>, as <see cref="double"/> value.
        /// </returns>
        /// <remarks><para>
        /// The GetArea Method needs to be implemented/overridden by all objects that inherit from <b>Surfaces</b>.
        /// </para></remarks>
        public abstract double GetArea();

        #endregion
    }
}

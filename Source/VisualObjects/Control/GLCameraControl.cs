﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GLCameraControl.cs 
//  Copyright (C) 2014/10/18  1:01 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Zeng Wei, Christian Tonn, Sven Schneider 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Drawing;
using System.Windows.Forms;
using CPlan.Geometry;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.IO;
using System.Drawing.Imaging;
using System.Collections.Generic;
using ObjLoader.Loader.Data.Elements;
using ObjLoader.Loader.Data.VertexData;
using ObjLoader.Loader.Loaders;
using System.Text;
using System.Globalization;
using System.Linq;
using System.ComponentModel;

namespace CPlan.VisualObjects
{
    public partial class GLCameraControl : GLControl
    {
        //############################################################################################################################################################################
        # region Private Fields

        private Vector3d _lastMouseLocation;
        private bool _panRotate;
        private bool _move;
        private bool _objInteract = true;

        private static int _textureId;
        private static int _framebufferId;
        private static int _depthbufferId;
        

        private List<Mesh> _meshList;
        private LoadResult _objLoadResult;
        private double _scene_max_x;
        private double _scene_max_y;
        private double _scene_max_z;
        private double _scene_min_x;
        private double _scene_min_y;
        private double _scene_min_z;

        # endregion

        //############################################################################################################################################################################
        # region Properties

        public Camera Camera_2D;
        public Camera Camera_3D;

        /// <summary>
        /// Used camera setting.
        /// </summary>
        public Camera ActiveCamera;

        /// <summary> 
        /// Indicates if GLControl control is loaded. 
        /// </summary>
        public bool Loaded { get; protected set; }

        /// <summary> 
        /// If you want to to extend to paint method in your form, SwapBuffers shall be turned off. Call SwapBuffers() after all objects are drawn!
        /// </summary>
        public bool SwapBuffersActivated { get; set; }
        
        /// <summary>
        /// The BackgroundColor of the view;
        /// </summary>
        public Color BackgroundColor {
            get { return ConvertColor.CreateColor(ActiveCamera.BackgroundColor); }
            set
            {
                Camera_2D.BackgroundColor = ConvertColor.CreateOpenTKColor4(value);
                Camera_3D.BackgroundColor = ConvertColor.CreateOpenTKColor4(value);
            } 
        }

        /// <summary>
        /// The RasterColor of the view;
        /// </summary>
        public Color RasterColor
        {
            get { return ConvertColor.CreateColor(ActiveCamera.RasterColor); }
            set
            {
                Camera_2D.RasterColor = ConvertColor.CreateOpenTKColor4(value);
                Camera_3D.RasterColor = ConvertColor.CreateOpenTKColor4(value);
            }
        }

        /// <summary>
        /// Background raster on or off.
        /// </summary>
        public bool ShowRaster
        {
            get { return ActiveCamera.DrawRaster; }
            set
            {
                Camera_2D.DrawRaster = value;
                Camera_3D.DrawRaster = value;
            }
        }

        /// <summary>
        /// Cellection of all objects that shall be drawn in this view.
        /// </summary>
        public ObjectsAllList AllObjectsList { get; set; }

        public bool ObjectInteraction {
            get { return _objInteract; }
            set { _objInteract = value; }
        }

        /// <summary>
        /// Used for Obj model. True if a obj file is loaded.
        /// </summary>
        public bool LoadedScene { get; set; }

        # endregion

        //############################################################################################################################################################################
        # region Constructors

        //===================================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of GLCameraControl.
        /// Antialiasing is set via GraphicsMode: http://www.opentk.com/node/3613
        /// </summary>
        public GLCameraControl()
            : base(new GraphicsMode(32, 24, 8, 4))
        {
            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime) return;

            InitializeComponent();

            AllObjectsList = new ObjectsAllList();
            
            //MouseMode = "ScreenMove";
            SwapBuffersActivated = true;

            MouseWheel += glControl_MouseWheel;
            MouseMove += glControl_MouseMove;
            MouseDown += glControl_MouseDown;
            MouseUp += glControl_MouseUp;
            Load += glControl_Load;
            Resize += glControl_Resize;
            Paint += glControl_Paint;

            Camera_2D = new Camera
            {
                Beta = -(float) Math.PI/2, 
                Znear = -9999.9f, 
                Zfar = 9999.9f, 
                Ortho = true
            };
            Camera_2D.SetPosition(0, 0, 50);

            Camera_3D = new Camera
            {
                Alpha = -(float) Math.PI/4,
                Beta = -(float) Math.PI/12,
                Zoom = 60.0f,
                Znear = 0.1f,
                Zfar = 9999.9f,
                Ortho = false
            };
            Camera_3D.SetPosition(0.0f, 0.0f, 50f);

            ActiveCamera = Camera_2D;
        }

        # endregion

        //############################################################################################################################################################################
        # region Methods

        # region ControlPanel

        public void CameraChange()
        {
            if (ActiveCamera == Camera_2D) ActiveCamera = Camera_3D;
            else if (ActiveCamera == Camera_3D) ActiveCamera = Camera_2D;
            Invalidate();
        }

        //===================================================================================================================================================================
        private void glControl_Load(object sender, EventArgs e)
        {
            Loaded = true;
            //BackgroundColor = Color.LightGray;
            //RasterColor = Color.White;
            SetupViewport();
        }

        //===================================================================================================================================================================
        /// <summary>
        /// Initialize the GLControl viewport settings
        /// </summary>
        public void SetupViewport()
        {
            MakeCurrent();

            // Bug: for some reason visual studio 2012 crashes when using GL.Viewport from the custom control!
            //int w = Width;
            //int h = Height;
            //GL.Viewport(0, 0, w, h); 
        }

        //===================================================================================================================================================================
        /// <summary>
        /// Draw method of the control.
        /// Draw all objects in AllObjectsList and the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <comment> If you want to add a custom paint part in your form, use SwapBuffers() after your drawing part and deactivate SwapBuffers() here!</comment>
        private void glControl_Paint(object sender, PaintEventArgs e)      
        {
            if (!Loaded) return;

            try
            {
                MakeCurrent();

                InitOpenGL();
                GL.ClearColor(BackgroundColor);
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

                //Console.WriteLine("Camera: X "+Camera.m_position.x+" Y "+Camera.m_position.y+" Z "+Camera.m_position.z);
                ActiveCamera.SetOpenGLMatrixes();
                ActiveCamera.DrawKoordinatenkreuz();

                AllObjectsList.Draw();

                //if there is a obj model, render it...
                if (LoadedScene) DrawObjModels();

                if (SwapBuffersActivated) SwapBuffers();
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
            }
        }

        //===================================================================================================================================================================
        /// <summary>
        /// Initialize the OpenGL settings.
        /// </summary>
        private void InitOpenGL()
        {
            GL.Enable(EnableCap.DepthTest);

            // -- set background color 
            GL.ClearColor(BackgroundColor);

            // -- set clear Z-Buffer value
            GL.ClearDepth(1.0);

            GL.Enable(EnableCap.Blend);
            GL.Enable(EnableCap.PointSmooth);
            GL.Enable(EnableCap.LineSmooth);
            GL.Enable(EnableCap.PolygonSmooth);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.ShadeModel(ShadingModel.Smooth);
            GL.DepthFunc(DepthFunction.Lequal);
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
        }

        //===================================================================================================================================================================
        private void glControl_Resize(object sender, EventArgs e)
        {
            if (!Loaded) return;
            SetupViewport();
            Invalidate();
        }

        # endregion

        # region MouseInteraction

        //===================================================================================================================================================================
        /// <summary>
        /// Can be used to deactivate MouseMove, MouseDown, and MouseUp.
        /// Maybe useful for multi threading.
        /// </summary>
        public void DeactivateInteraction()
        {
            MouseMove -= glControl_MouseMove;
            MouseDown -= glControl_MouseDown;
            MouseUp -= glControl_MouseUp;
        }

        /// <summary>
        /// Activate MouseMove, MouseDown, and MouseUp.
        /// </summary>
        public void ActivateInteraction()
        {
            MouseMove += glControl_MouseMove;
            MouseDown += glControl_MouseDown;
            MouseUp += glControl_MouseUp;
        }


        //===================================================================================================================================================================
        private void glControl_MouseDown(object sender, MouseEventArgs e)
        {
            if (!Loaded) return;

            // -- if select move screen button
            //if (MouseMode == "ScreenMove")
            //{
            System.Drawing.Point mousePoint = new Point(e.X, e.Y);// System.Windows.Forms.Control.MousePosition;
                _lastMouseLocation = new Vector3d(mousePoint.X, mousePoint.Y, 0);
                //_lastMouseLocation = new Vector3d(Cursor.Position.X, Cursor.Position.Y, 0);

                // -- view control --------------------------------------------------
                // -- left mouse for move
                if (e.Button == MouseButtons.Right)
                {
                    _panRotate = false;
                    _move = true;
                }

                // -- right mouse for pan & rotate
                if (e.Button == MouseButtons.Right && ActiveCamera == Camera_3D)
                {
                    _panRotate = true;
                    _move = false;
                }
            //}

            // -- objects interactions -----------------------------------------------
            if (ObjectInteraction)
            {
                if (e.Button == MouseButtons.Left)
                {
                    //System.Drawing.Point pointE = System.Windows.Forms.Control.MousePosition;
                    Vector3d pointXy = ActiveCamera.GetPointXYPtn(mousePoint.X, mousePoint.Y);

                    if (pointXy == null) return;
                    Vector2d p = new Vector2d(pointXy.X, pointXy.Y);
                    AllObjectsList.ObjectsToInteract.MouseDown(e, p);
                }

                _panRotate = false;
                _move = false;
            }
            Invalidate();

        }

        //===================================================================================================================================================================
        private void glControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (!Loaded) return;
            
            // -- view control --
            if (_lastMouseLocation == null)
                return;

            System.Drawing.Point mousePoint = new Point(e.X, e.Y);// System.Windows.Forms.Control.MousePosition;
            double deltaX = mousePoint.X - _lastMouseLocation.X;
            double deltaY = mousePoint.Y - _lastMouseLocation.Y;

            // -- right mouse move
            if (e.Button == MouseButtons.Right)
            {
                Vector3d preWorldPos = ActiveCamera.GetPointXYPtn(_lastMouseLocation);
                Vector3d curWorldPos = ActiveCamera.GetPointXYPtn(mousePoint.X, mousePoint.Y);
                double deltaXWorld = curWorldPos.X - preWorldPos.X;
                double deltaYWorld = curWorldPos.Y - preWorldPos.Y;

                // -- control key down,  zoom
                if (ModifierKeys == Keys.Control)
                {
                    float scaleRatio = 0.01f;
                    if (ActiveCamera.Position.z <= 10)
                        scaleRatio = 0.1f;

                    ActiveCamera.Position.z *= Convert.ToSingle(1.0 - deltaYWorld * scaleRatio);
                }
                else if (ModifierKeys == Keys.Shift && ActiveCamera == Camera_3D)
                {
                    // --- 3D Camera Rotate ---
                    ActiveCamera.Alpha += Convert.ToSingle(deltaY * 0.01f);
                    ActiveCamera.Beta += Convert.ToSingle(deltaX * 0.01f);

                    while (ActiveCamera.Alpha < 0) ActiveCamera.Alpha += Convert.ToSingle(2 * Math.PI);
                    while (ActiveCamera.Alpha > 2 * Math.PI) ActiveCamera.Alpha -= Convert.ToSingle(2 * Math.PI);

                    while (ActiveCamera.Beta < 0) ActiveCamera.Beta += Convert.ToSingle(2 * Math.PI);
                    while (ActiveCamera.Beta > 2 * Math.PI) ActiveCamera.Beta -= Convert.ToSingle(2 * Math.PI);
                }

                else if (ModifierKeys == Keys.Alt || (ModifierKeys == Keys.Shift && ActiveCamera == Camera_2D))
                {
                    // -- change the origin point here
                    Vector3d originPoint = new Vector3d(0, 0, 0);

                    Vector3d preDir = new Vector3d(preWorldPos.X - originPoint.X, preWorldPos.Y - originPoint.Y, 0);
                    Vector3d curDir = new Vector3d(curWorldPos.X - originPoint.X, curWorldPos.Y - originPoint.Y, 0);

                    double angle1 = Math.Atan2(curDir.Y, curDir.X);
                    double angle2 = Math.Atan2(preDir.Y, preDir.X);
                    double rotateAngle = angle2 - angle1;

                    ActiveCamera.Gamma -= (float)rotateAngle;
                }
                else
                {
                    Vector3d newPos = ActiveCamera.GetCameraPos;
                    newPos.X += deltaXWorld;
                    newPos.Y += deltaYWorld;
                    ActiveCamera.SetPosition(newPos.X, newPos.Y, newPos.Z);
                }

                _lastMouseLocation.X = mousePoint.X;
                _lastMouseLocation.Y = mousePoint.Y;
                _lastMouseLocation.Z = 0;
                Invalidate();
            }

            // -- objects interactions -----------------------------------------------
            if (ObjectInteraction)
            {       
                if (e.Button == MouseButtons.Left)
                {
                    Vector3d pointXy = ActiveCamera.GetPointXYPtn(mousePoint.X, mousePoint.Y);
                    if (pointXy == null) return;

                    //Cursor tmpCursor = Cursors.Default;
                    Vector2d p = new Vector2d(pointXy.X, pointXy.Y);
                    AllObjectsList.ObjectsToInteract.MouseMove(e, p); //, ref glControl, ref tmpCursor);

                    // Hier könnte man noch optimieren, indem man immer nur den Bereich neuzeichnet, in dem das Objekt bewegt wurde.
                }
            }

        }

        //===================================================================================================================================================================
        private void glControl_MouseUp(object sender, MouseEventArgs e)
        {
            if (!Loaded) return;

            // -- view control --
            if (_panRotate || _move)
            {
                _panRotate = false;
                _move = false;
                return;
            }

            // -- objects interactions -----------------------------------------------
            if (ObjectInteraction)
            {
                System.Drawing.Point mousePoint = new Point(e.X, e.Y);// System.Windows.Forms.Control.MousePosition;
                AllObjectsList.ObjectsToInteract.MouseUp(e); // put the selected element to the fornt
                if (AllObjectsList.ObjectsToInteract.FoundInteractObj())
                {
                    // TODO: pointXy is not nullable, this logic won't work
                    Vector3d pointXy = ActiveCamera.GetPointXYPtn(mousePoint.X, mousePoint.Y);
                    if (pointXy == null) return;
                    Vector2d p = new Vector2d(pointXy.X, pointXy.Y);
                    AllObjectsList.ObjectsToDraw.MouseUp(e, p);
                }
            }
            Invalidate();
        }

        //===================================================================================================================================================================
        private void glControl_MouseWheel(object sender, MouseEventArgs e)
        {
            if (!Loaded) return;

            // -- view control --{
            float scaleRatio = 0.0005f;
            if (ActiveCamera.Position.z <= 10)
                scaleRatio = 0.001f;

            ActiveCamera.Position.z *= Convert.ToSingle(1.0 - e.Delta * scaleRatio);
            Invalidate();
        }

        //===================================================================================================================================================================
        /// <summary>
        /// Zoom to show all objects.
        /// </summary>
        public void ZoomAll()
        {
            //double zoomfak = 10.1;
            //double zoomfakPerspective = 10.001;
            BoundingBox box = new BoundingBox();

            if (AllObjectsList == null)
                AllObjectsList = new ObjectsAllList();
            // -- find the bounding box for all objects --
            if (AllObjectsList.ObjectsToDraw.Count > 0)
            {
                foreach (GeoObject curObj in AllObjectsList.ObjectsToDraw)
                {
                    if(curObj != null)
                    {
                        curObj.GetBoundingBox(box);
                    }
                }  
            }
            Vector3d min = box.GetMin();
            Vector3d max = box.GetMax();

            //Console.WriteLine("In ZoomAll");

            if (!box.IsValid()) return;
            if (ActiveCamera == Camera_2D)
            {
                //Console.WriteLine("box is valid and camera is 2d");
                //Grundriss
                Vector3d midV = (min + max) / 2;

                double cameraZ = ActiveCamera.Position.z;
                if (cameraZ <= 0)
                {
                    cameraZ = 50;
                }
                else
                {
                    cameraZ = Math.Max(max.X - min.X, max.Y - min.Y) / 2;
                }
                ActiveCamera.SetPosition(-midV.X, -midV.Y, cameraZ);
            }
            else
            {
                //Perspektive
                Vector3d midV = (min + max) / 2;

                double cameraZ = ActiveCamera.Position.z;
                if (cameraZ <= 0)
                    cameraZ = 50;
                else
                {
                    // distance of the camera for covering objects in Y-dimension 
                    double zForY = (max.Y - min.Y) / 2 / Math.Tan(Camera.Fovy / 2);

                    Int32[] oldViewport = new Int32[4];
                    GL.GetInteger(GetPName.Viewport, oldViewport);

                    double dAspectRatio = Convert.ToDouble(oldViewport[2]) / Convert.ToDouble(oldViewport[3]);

                    // distance of the camera for covering objects in Y-dimension 
                    double zForX = (max.X - min.X) / 2 / Math.Tan(Camera.Fovy / 2) / dAspectRatio;

                    cameraZ = Math.Max(zForX, zForY);
                }
                ActiveCamera.SetPosition(-midV.X, -midV.Y, cameraZ);

                ActiveCamera.Alpha = 0;
                ActiveCamera.Beta = 0;
                ActiveCamera.Gamma = 0;
            }
            Invalidate();
        }
        # endregion

        # region Screenshot

        /// <summary>
        /// Makes a screenshot and saves it with the current timestamp
        /// into the same folder from which the application is running from.
        /// </summary>
        /// <param name="factor">Multiplies the width and length = resolution of the view.</param>
        public void Screenshot(float factor)
        {
            //glCameraMainWindow.ZoomAll();
            //glControl1.Invalidate();
            //Bitmap img = glCameraMainWindow.GrabScreenshot();

            //make glControl1 as current rendering context
            MakeCurrent();
            CheckFboStatus();

            //int width = 1500, height = 1500;
            int width = Convert.ToInt32(Width*factor);
            int height = Convert.ToInt32(Height * factor);

            EnableFboRender(width, height);
            //screen_shoot_paint();

            AllObjectsList.Draw();
            if (LoadedScene) DrawObjModels();

            Bitmap bmp = GrabScreenshot(width, height);

            string screenshotFilename = "screenShot_" +
                                        DateTime.Now.Day.ToString("00") + "_" +
                                        DateTime.Now.Month.ToString("00") + "_" +
                                        DateTime.Now.Hour.ToString("00") + "_" +
                                        DateTime.Now.Minute.ToString("00") + "_" +
                                        DateTime.Now.Second.ToString("00") + ".png";

            string path = Path.Combine(Environment.CurrentDirectory, screenshotFilename);
            bmp.Save(path, ImageFormat.Png);
            //MessageBox.Show(path + screenshotFilename);

            // dispose the image to prevent running out of memory 
            bmp.Dispose();
            ReleaseFboRender();
            Invalidate();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // --- FBO Render by Zeng Wei ---
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //===================================================================================================================================================================
        // Returns a System.Drawing.Bitmap with the contents of the current framebuffer
        public Bitmap GrabScreenshot(int width, int height)
        {
            //if (GraphicsContext.CurrentContext == null)
            //    throw new GraphicsContextMissingException();

            GL.BindTexture(TextureTarget.Texture2D, _textureId);

            var bmp = new Bitmap(width, height);
            BitmapData data =
                bmp.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            //bmp.SetResolution(1200.0F, 1200.0F);

            //GL.ReadPixels(0, 0, width, height, PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
            GL.GetTexImage(TextureTarget.Texture2D, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte,
                data.Scan0);
            bmp.UnlockBits(data);

            //bmp.SetResolution(300, 300);
            try
            {
                bmp.RotateFlip(RotateFlipType.RotateNoneFlipY);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return bmp;
        }

        public bool CheckFboStatus()
        {
            switch (GL.Ext.CheckFramebufferStatus(FramebufferTarget.FramebufferExt))
            {
                case FramebufferErrorCode.FramebufferCompleteExt:
                    {
                        Console.WriteLine("FBO: The framebuffer is complete and valid for rendering.");
                        return true;
                    }
                case FramebufferErrorCode.FramebufferIncompleteAttachmentExt:
                    {
                        Console.WriteLine(
                            "FBO: One or more attachment points are not framebuffer attachment complete. This could mean there’s no texture attached or the format isn’t renderable. For color textures this means the base format must be RGB or RGBA and for depth textures it must be a DEPTH_COMPONENT format. Other causes of this error are that the width or height is zero or the z-offset is out of range in case of render to volume.");
                        break;
                    }
                case FramebufferErrorCode.FramebufferIncompleteMissingAttachmentExt:
                    {
                        Console.WriteLine("FBO: There are no attachments.");
                        break;
                    }
                /* case  FramebufferErrorCode.GL_FRAMEBUFFER_INCOMPLETE_DUPLICATE_ATTACHMENT_EXT: 
                 {
                     Trace.WriteLine("FBO: An object has been attached to more than one attachment point.");
                     break;
                 }*/
                case FramebufferErrorCode.FramebufferIncompleteDimensionsExt:
                    {
                        Console.WriteLine(
                            "FBO: Attachments are of different size. All attachments must have the same width and height.");
                        break;
                    }
                case FramebufferErrorCode.FramebufferIncompleteFormatsExt:
                    {
                        Console.WriteLine(
                            "FBO: The color attachments have different format. All color attachments must have the same format.");
                        break;
                    }
                case FramebufferErrorCode.FramebufferIncompleteDrawBufferExt:
                    {
                        Console.WriteLine(
                            "FBO: An attachment point referenced by GL.DrawBuffers() doesn’t have an attachment.");
                        break;
                    }
                case FramebufferErrorCode.FramebufferIncompleteReadBufferExt:
                    {
                        Console.WriteLine(
                            "FBO: The attachment point referenced by GL.ReadBuffers() doesn’t have an attachment.");
                        break;
                    }
                case FramebufferErrorCode.FramebufferUnsupportedExt:
                    {
                        Console.WriteLine("FBO: This particular FBO configuration is not supported by the implementation.");
                        break;
                    }
                default:
                    {
                        Console.WriteLine("FBO: Status unknown. (yes, this is really bad.)");
                        break;
                    }
            }
            return false;
        }
  
        /// <summary>
        /// Set to render on Frame Buffer Object
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public void EnableFboRender(int width, int height)
        {
            // init texture buffer
            GL.GenTextures(1, out _textureId);
            GL.Enable(EnableCap.Texture2D);
            GL.BindTexture(TextureTarget.Texture2D, _textureId);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter,
                (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter,
                (int)TextureMagFilter.Linear);
            //GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Clamp);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba8, width, height, 0,
                OpenTK.Graphics.OpenGL.PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero);
            //GL.TexImage2DMultisample(TextureTarget.Texture2DMultisample, 2, PixelInternalFormat.Rgba8, width, height, false);

            GL.BindTexture(TextureTarget.Texture2D, 0);

            // init depth buffer
            GL.Ext.GenRenderbuffers(1, out _depthbufferId);
            GL.Ext.BindRenderbuffer(RenderbufferTarget.RenderbufferExt, _depthbufferId);
            GL.Ext.RenderbufferStorage(RenderbufferTarget.RenderbufferExt, (RenderbufferStorage)All.DepthComponent32,
                width, height);

            // init frame buffer
            GL.Ext.GenFramebuffers(1, out _framebufferId);
            GL.Ext.BindFramebuffer(FramebufferTarget.DrawFramebuffer, _framebufferId);

            GL.Ext.FramebufferTexture2D(FramebufferTarget.FramebufferExt, FramebufferAttachment.ColorAttachment0Ext,
                TextureTarget.Texture2D, _textureId, 0);
            GL.Ext.FramebufferRenderbuffer(FramebufferTarget.FramebufferExt, FramebufferAttachment.DepthAttachmentExt,
                RenderbufferTarget.RenderbufferExt, _depthbufferId);

            // since there's only 1 Color buffer attached this is not explicitly required
            GL.DrawBuffer((DrawBufferMode)FramebufferAttachment.ColorAttachment0Ext);

            //GL.DrawBuffer(DrawBufferMode.Back);
            GL.PushAttrib(AttribMask.ViewportBit);

            GL.Viewport(0, 0, width, height);

            //GL.ClearColor(0.0f, 0.0f, 0.0f, 1.0f);
            GL.ClearColor(ConvertColor.CreateOpenTKColor4(BackgroundColor));

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        }

        public void ReleaseFboRender()
        {
            GL.PopAttrib();

            GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0); // disable rendering into the FBO
            GL.BindTexture(TextureTarget.Texture2D, 0);
            GL.DrawBuffer(DrawBufferMode.Back);
        }

        # endregion

        # region ObjRenderer

        private struct Mesh
        {
            public Vector3d Normal;
            public Vector3d[] Vertice;
        }

        ///////////////////////////////////////////////////////////////////////
        //// OBJ Loader methods 
        ///////////////////////////////////////////////////////////////////////
        //============================================================================================================================================================================
        public void OpenObjFile(string path)
        {
            var fileStream = new FileStream(path, FileMode.Open);
            OpenObjFile(fileStream);
            fileStream.Close();
        }

        public void OpenObjFile(Stream stream)
        {
            var objLoaderFactory = new ObjLoaderFactory();
            IObjLoader objLoader = objLoaderFactory.Create(new MaterialNullStreamProvider());
            _objLoadResult = objLoader.Load(stream);

            // used for testing
            PrintResult(_objLoadResult);

            //_borderPoly = GetBounds();
            //AllObjectsList.ObjectsToDraw.Add(_borderPoly);
            ZoomAll();

            ConvertObjModels(_objLoadResult);
            CalculateNormals();

            //timer1.Enabled = true;
            LoadedScene = true;
        }

        private static void PrintResult(LoadResult result)
        {
            var sb = new StringBuilder();

            sb.AppendLine("Load result:");
            sb.Append("Vertices: ");
            sb.AppendLine(result.Vertices.Count.ToString(CultureInfo.InvariantCulture));
            sb.Append("Textures: ");
            sb.AppendLine(result.Textures.Count.ToString(CultureInfo.InvariantCulture));
            sb.Append("Normals: ");
            sb.AppendLine(result.Normals.Count.ToString(CultureInfo.InvariantCulture));
            sb.AppendLine();
            sb.AppendLine("Groups: ");

            foreach (Group loaderGroup in result.Groups)
            {
                sb.AppendLine(loaderGroup.Name);
                sb.Append("Faces: ");
                sb.AppendLine(loaderGroup.Faces.Count.ToString(CultureInfo.InvariantCulture));
            }

            Console.WriteLine(sb);
        }

        // TODO: this is written only for visualization, better convert it to CPlan VisualObjects!
        // to maintain the structure of the OBJ file (Group->Face), create new structure
        private void ConvertObjModels(LoadResult result)
        {
            _meshList = new List<Mesh>();

            List<Vertex> vertices = result.Vertices.ToList();
            foreach (Group group in result.Groups)
            {
                foreach (Face face in group.Faces)
                {
                    // a point or a line, ignore
                    if (face.Count < 3)
                        continue;
                    int firstVertexIndex = face[0].VertexIndex;
                    Vertex firstVertex = vertices[firstVertexIndex - 1];

                    for (int i = 1; i < face.Count - 1; i++)
                    {
                        int vertexIndex = face[i].VertexIndex;
                        Vertex vertex = vertices[vertexIndex - 1];

                        int nextVertexIndex = face[i + 1].VertexIndex;
                        Vertex nextVertex = vertices[nextVertexIndex - 1];

                        var mesh = new Mesh();
                        mesh.Vertice = new Vector3d[3];

                        mesh.Vertice[0] = new Vector3d(firstVertex.X - _scene_min_x, firstVertex.Y - _scene_min_y,
                            firstVertex.Z - _scene_min_z);
                        mesh.Vertice[1] = new Vector3d(vertex.X - _scene_min_x, vertex.Y - _scene_min_y,
                            vertex.Z - _scene_min_z);
                        mesh.Vertice[2] = new Vector3d(nextVertex.X - _scene_min_x, nextVertex.Y - _scene_min_y,
                            nextVertex.Z - _scene_min_z);

                        _meshList.Add(mesh);
                    }
                }
            }

            Console.WriteLine(_meshList.Count + " mesh faces converted");
        }

        private void CalculateNormals()
        {
            List<Mesh> temp_mesh_list = _meshList;
            _meshList = new List<Mesh>();

            for (int i = 0; i < temp_mesh_list.Count; i++)
            {
                Mesh mesh = temp_mesh_list[i];

                Vector3d a = mesh.Vertice[0];
                Vector3d b = mesh.Vertice[1];
                Vector3d c = mesh.Vertice[2];

                mesh.Normal = Vector3d.Cross(b - a, c - a);
                mesh.Normal.Normalize();

                _meshList.Add(mesh);
            }
        }

        public void DrawObjModels()
        {
            if (ActiveCamera == Camera_3D)
            {
                // enable lighting for 3D visualization
                //GL.ShadeModel(ShadingModel.Smooth);

                //float[] light_ambient = { 0.7f, 0.7f, 0.7f, 1.0f };
                //float[] light_diffuse = { 0.8f, 0.8f, 0.8f, 1.0f };
                //float[] light_specular = { 1.0f, 1.0f, 1.0f, 1.0f };
                ////float[] light_position = { (float)(_scene_max_x - _scene_min_x), (float)(_scene_max_z - _scene_min_z) * 3, (float)(_scene_max_y - _scene_min_y), 1.0f };
                //float[] light_position = { (float)(_scene_max_x - _scene_min_x), (float)(_scene_max_y - _scene_min_y), (float)(_scene_max_z - _scene_min_z) * 3, 1.0f };


                //GL.Light(LightName.Light0, LightParameter.Position, light_position);

                //GL.Light(LightName.Light0, LightParameter.Ambient, light_ambient);
                //GL.Light(LightName.Light0, LightParameter.Diffuse, light_diffuse);
                //GL.Light(LightName.Light0, LightParameter.Specular, light_specular);
                //GL.LightModel(LightModelParameter.LightModelTwoSide, 0.0f);

                GL.Enable(EnableCap.Light0);
                GL.Enable(EnableCap.Lighting);

                // set up the colors for different properties
                //float[] mat_ambient = { 0.7f, 0.7f, 0.7f, 1.0f };
                //float[] mat_diffuse = { 0.8f, 0.8f, 0.8f, 1.0f };
                float[] matAmbient = { 1.0f, 1.0f, 1.0f, 1.0f };
                float[] matDiffuse = { 1.0f, 1.0f, 1.0f, 1.0f };
                GL.Material(MaterialFace.Front, MaterialParameter.Ambient, matAmbient);
                GL.Material(MaterialFace.Front, MaterialParameter.Diffuse, matDiffuse);
            }

            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
            GL.Color4(0, 0, 0, 1.0f);

            for (int i = 0; i < _meshList.Count; i++)
            {
                Mesh mesh = _meshList[i];

                GL.Begin(PrimitiveType.Triangles);
                GL.Normal3(mesh.Normal);
                GL.Vertex3(mesh.Vertice[0].X, -mesh.Vertice[0].Z, mesh.Vertice[0].Y);

                GL.Normal3(mesh.Normal);
                GL.Vertex3(mesh.Vertice[1].X, -mesh.Vertice[1].Z, mesh.Vertice[1].Y);

                GL.Normal3(mesh.Normal);
                GL.Vertex3(mesh.Vertice[2].X, -mesh.Vertice[2].Z, mesh.Vertice[2].Y);

                GL.End();
            }

            if (ActiveCamera == Camera_3D)
            {
                GL.Disable(EnableCap.Lighting);
                GL.Disable(EnableCap.Light0);
            }

            GL.End();
        }

        # endregion

        # endregion


    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GCircle.cs 
//  Copyright (C) 2014/10/18  12:56 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Linq;
using CPlan.Geometry;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace CPlan.VisualObjects
{
    using GdiPointF = System.Drawing.PointF;

    /// <summary>
    /// Represents a circle in two-dimensional space, using <see cref="double"/> coordinates.
    /// </summary>
  
    [Serializable]
    public class GCircle : Surfaces 
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties
        
        private int _selected;

        public double Radius { get; protected set; }
        public int Sides { get; set; }
        public Vector2d Center { get; set; }

        public Poly2D Poly {
            get
            {
                Poly2D curPoly = new Poly2D();
                curPoly.PointsFirstLoop = new Vector2d[Sides];
                GdiPointF[] pathPts = new GdiPointF[Sides];
                for (int i = 0; i < Sides; i++)
                {
                    double degInRad = i * 3.1416 / 180;
                    curPoly.PointsFirstLoop[i] = new Vector2d(Math.Cos(degInRad) * Radius + Center.X, Math.Sin(degInRad) * Radius + Center.Y);
                    pathPts[i] = curPoly.PointsFirstLoop[i].ToPointF();
                }
                return curPoly;
            }
        }

        /// <summary> The z-koordinate of the survace object. </summary>
        public new float DeltaZ { get; set; }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        //================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>GCircle</b> class in two-dimensional space.
        /// </summary>
        /// <param name="midPt">The middle point of the circle.</param>
        /// <param name="radius">The radius of the circle.</param>
        public GCircle(Vector2d midPt, float radius)
        {
            Center = midPt;
            Radius = radius;
            Sides = 360;
            Transparency = 1;
        }

        ///================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>GCircle</b> class in two-dimensional space.
        /// The circle is created from a regular polygon. This means, its roundness is defined by the number of sides.
        /// </summary>
        /// <param name="midPt">The middle point of the circle.</param>
        /// <param name="radius">The radius of the circle.</param>
        /// <param name="sides">The number of sides of the regular polygon.</param>
        public GCircle(Vector2d midPt, Double radius, int sides)
        {
            Center = midPt;
            Radius = radius;
            Sides = sides;
            Transparency = 1;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        /// <summary>
        /// 
        /// </summary>
        public override bool Selector(Vector2d mPoint)
        {
            _selected = 0;

            if (mPoint.Distance(Center) < Radius)
                _selected = 1; //Kreisinneres selektiert
            if ((mPoint.Distance(Center) >= Radius) && (mPoint.Distance(Center) <= Radius + 4))
                _selected = 2;
            if (_selected == 0)
                if (ContainsPoint(mPoint)) _selected = 3;

            if (_selected != 0)
                return true;
            else
                return false;
        }

        //================================================================================================================================================
        /// <summary>
        /// Moves the <b>GCircle</b>
        /// </summary>
        /// <param name="delta">The 2d vector to move the polygon.</param>
        public override void Move(Vector2d delta)
        {
            Center = Center + delta;
            //if (_selected_ == 1)
            //{
            //    Center = Center + delta;
            //}
            //if (_selected_ == 2)
            //{
            //    //this = new GCircle(Center, delta.Length);
            //}
        }


        //================================================================================================================================================
        /// <summary>
        /// Draw the <b>GCircle</b> to a <see cref="OpenTK.Graphics.OpenGL"/> component
        /// </summary>
        public override void Draw()
        {
            double dZ = DeltaZ;

            // Draw the selected filled circle 
            if (_selected == 1)
            {
                GL.LineWidth(BorderWidth);
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
                GL.Color4(SelectColor);
                GL.Begin(PrimitiveType.LineLoop);
                for (int i = 0; i < Sides; i++)
                {
                    float theta = (2.0f * (float)Math.PI * i) / Sides;
                    float xx = (float)Radius * (float)Math.Cos(theta);
                    float yy = (float)Radius * (float)Math.Sin(theta);
                    GL.Vertex3(Center.X + xx, Center.Y + yy, dZ);
                }
            }

            // Draw the filled circle 
            if (Filled)
            {
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(FillColor.X, FillColor.Y, FillColor.Z, Transparency);
                GL.Begin(PrimitiveType.Polygon);
                for (int i = 0; i < Sides; i++)
                {
                    float theta = (2.0f*(float) Math.PI* i)/ Sides;
                    float xx = (float) Radius*(float) Math.Cos(theta);
                    float yy = (float) Radius*(float) Math.Sin(theta);
                    GL.Vertex3(Center.X + xx, Center.Y + yy, dZ);
                }
                GL.End();
            }

            // Draw the outline of the circle 
            GL.LineWidth(BorderWidth);
            GL.Color4(BorderColor);
            GL.Begin(PrimitiveType.LineLoop);           
            for (int i = 0; i < Sides; i++)
            {
                float theta = (2.0f * (float)Math.PI * i) / Sides;
                float xx = (float)Radius * (float)Math.Cos(theta);
                float yy = (float)Radius * (float)Math.Sin(theta);
                GL.Vertex3(Center.X + xx, Center.Y + yy, dZ);
            }
            GL.End();
        }

        # endregion

        //============================================================================================================================================================================
        /// <summary>
        /// Computes the area of the <b>GPolygon</b>
        /// </summary>
        /// <returns>
        /// The area of the <b>GPolygon</b>, as <see cref="double"/> value.
        /// </returns>
        public override double GetArea()
        {
            return Poly.Area;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Computes the bounding box.
        /// </summary>
        /// <param name="box"></param>
        public override void GetBoundingBox(BoundingBox box)
        {
            for (int i = 0; i < Poly.PointsFirstLoop.Count(); i++)
            {
                box.AddPoint(Poly.PointsFirstLoop[i]);
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Test, if a point is located on or in a geometry.
        /// </summary>
        /// <returns>If a geometry contains a point.</returns>
        public override bool ContainsPoint(Vector2d point)
        {
            return Poly.ContainsPoint(point);
        }
    }
}

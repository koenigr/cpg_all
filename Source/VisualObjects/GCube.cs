﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GCube.cs 
//  Copyright (C) 2014/10/18  12:56 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using CPlan.Geometry;
using OpenTK.Graphics.OpenGL;
using Tektosyne;

namespace CPlan.VisualObjects
{
    [Serializable]
    public class GCube : Surfaces
    {
        //############################################################################################################################################################################
        #region Properties

        /// <summary>
        /// Poly3D class.
        /// </summary>
        public Poly3D Poly { get; protected set; }

        public float Height { get; protected set; }

        /// <summary> List with the edges of the polygon. </summary>
        public Line3D[] Edges
        {
            get
            {
                Line3D[] edges = new Line3D[Poly.PointsFirstLoop.Count() * 3];

                // bottom Polygon
                for (int i = 0; i < Poly.PointsFirstLoop.Count(); i++)
                {
                    int k = i + 1;
                    if (k >= Poly.PointsFirstLoop.Count()) k = 0;
                    Vector3D p1 = Poly.PointsFirstLoop[i];
                    Vector3D p2 = Poly.PointsFirstLoop[k];

                    Vector3D p1Up = new Vector3D(p1.x, p1.y, p1.z + Height);
                    Vector3D p2Up = new Vector3D(p2.x, p2.y, p2.z + Height);

                    edges[i * 3] = new Line3D(p1, p2);
                    edges[i * 3 + 1] = new Line3D(p1Up, p2Up);
                    edges[i * 3 + 2] = new Line3D(p1, p1Up);
                }

                return edges;
            }
        }

        #endregion

        //############################################################################################################################################################################
        # region Constructors

        public GCube() { }

        //================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>GCube</b> class in three-dimensional space.
        /// </summary>
        public GCube(Vector3D[] polygon, float height)
        {
            if (polygon == null)
                ThrowHelper.ThrowArgumentNullException("GCuble polygon null");
            if (polygon.Length < 3)
                ThrowHelper.ThrowArgumentExceptionWithFormat(
                    "polygon.Length", Strings.ArgumentLessValue, 3);

            Poly = new Poly3D(polygon);
            Height = height;
        }

        //================================================================================================================================================
        public GCube(Vector2D[] polygon, float height)
        {
            if (polygon == null)
                ThrowHelper.ThrowArgumentNullException("GCuble polygon null");
            if (polygon.Length < 3)
                ThrowHelper.ThrowArgumentExceptionWithFormat(
                    "polygon.Length", Strings.ArgumentLessValue, 3);

            Poly = new Poly3D(polygon, 0);
            Height = height;
        }

        //================================================================================================================================================
        public GCube(List<Vector3D> polygon, float height)
        {
            if (polygon.Count == 0)
                ThrowHelper.ThrowArgumentNullException("polygon");
            if (polygon.Count < 3)
                ThrowHelper.ThrowArgumentExceptionWithFormat(
                    "polygon.Length", Strings.ArgumentLessValue, 3);

            Poly = new Poly3D(polygon.ToArray());
            Height = height;
        }

        # endregion

        //############################################################################################################################################################################
        #region Methods

        //================================================================================================================================================
        public override void Move(Vector2D delta)
        {
            Vector3D delta3D = new Vector3D(delta.X, delta.Y, 0);

            Vector3D[] vertices = Poly.PointsFirstLoop;
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i] += delta3D;
            }
 
        }

        //================================================================================================================================================
        /// <summary>
        /// Is not implemented for <b>GCube</b>.
        /// </summary>
        public override bool Selector(Vector2D delta)
        {
            throw new NotImplementedException();
        }

        //================================================================================================================================================
        public override void GetBoundingBox(BoundingBox box)
        {
            for (int i = 0; i < Poly.PointsFirstLoop.Count(); i++)
            {
                Vector3D curPt = Poly.PointsFirstLoop[i];
                Vector3D upPt = new Vector3D(curPt.x, curPt.y, curPt.z + Height);
                box.AddPoint(curPt);
                box.AddPoint(upPt);
            }
        }

        //================================================================================================================================================
        public override void Draw()
        {
            Vector3D[] vertices = Poly.PointsFirstLoop;

            if (Filled)
            {
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(FillColor);

                // draw bottom polygon
                GL.Begin(PrimitiveType.Polygon);
                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector3D next = vertices[i];// +offset;
                    GL.Vertex3(next.X, next.Y, next.z);
                }
                GL.End();

                // draw top polygon
                GL.Begin(PrimitiveType.Polygon);
                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector3D next = vertices[i];// +offset;
                    GL.Vertex3(next.X, next.Y, next.z + Height);
                }
                GL.End();

                // draw slide 
                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector3D cur = vertices[i];
                    Vector3D next = vertices[(i + 1) % vertices.Length];
                    GL.Begin(PrimitiveType.Polygon);
                    GL.Vertex3(cur.X, cur.Y, cur.z);
                    GL.Vertex3(next.X, next.Y, next.z);
                    GL.Vertex3(next.X, next.Y, next.z + Height);
                    GL.Vertex3(cur.X, cur.Y, cur.z + Height);
                    GL.End();
                }
            }

            GL.LineWidth(BorderWidth);
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
            GL.Color4(BorderColor);

            // draw bottom polygon
            GL.Begin(PrimitiveType.Polygon);
            for (int i = 0; i < vertices.Length; i++)
            {
                Vector3D next = vertices[i];// +offset;
                GL.Vertex3(next.X, next.Y, 0);
            }
            GL.End();

            // draw top polygon
            GL.Begin(PrimitiveType.Polygon);
            for (int i = 0; i < vertices.Length; i++)
            {
                Vector3D next = vertices[i];// +offset;
                GL.Vertex3(next.X, next.Y, next.z + Height);
            }
            GL.End();

            // draw slide lines
            GL.Begin(PrimitiveType.Lines);
            for (int i = 0; i < vertices.Length; i++)
            {
                Vector3D cur = vertices[i];
                GL.Vertex3(cur.X, cur.Y, cur.z);
                GL.Vertex3(cur.X, cur.Y, cur.z + Height);
            }
            GL.End();
        }

        //================================================================================================================================================
        /*public override void DrawPickingMode() {
            Vector3D[] vertices = Poly.PointsFirstLoop;

           // UniqueColor = 244 * 256 * 256 + 189 * 256 + 19;

            // Convert "i", the integer mesh ID, into an RGB color
            int r = (UniqueColor & 0x000000FF) >> 0;
            int g = (UniqueColor & 0x0000FF00) >> 8;
            int b = (UniqueColor & 0x00FF0000) >> 16;

            if (Filled)
            {
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(r / 255.0f, g / 255.0f, b / 255.0f, 1);

                // draw bottom polygon
                GL.Begin(PrimitiveType.Polygon);
                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector3D next = vertices[i];// +offset;
                    GL.Vertex3(next.X, next.Y, next.z);
                }
                GL.End();

                // draw top polygon
                GL.Begin(PrimitiveType.Polygon);
                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector3D next = vertices[i];// +offset;
                    GL.Vertex3(next.X, next.Y, next.z + Height);
                }
                GL.End();

                // draw slide 
                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector3D cur = vertices[i];
                    Vector3D next = vertices[(i + 1) % vertices.Length];
                    GL.Begin(PrimitiveType.Polygon);
                    GL.Vertex3(cur.X, cur.Y, cur.z);
                    GL.Vertex3(next.X, next.Y, next.z);
                    GL.Vertex3(next.X, next.Y, next.z + Height);
                    GL.Vertex3(cur.X, cur.Y, cur.z + Height);
                    GL.End();
                }
            }

            GL.LineWidth(BorderWidth);
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);

            GL.Color4(r / 255.0f, g / 255.0f, b / 255.0f, 1);

            // draw bottom polygon
            GL.Begin(PrimitiveType.Polygon);
            for (int i = 0; i < vertices.Length; i++)
            {
                Vector3D next = vertices[i];// +offset;
                GL.Vertex3(next.X, next.Y, 0);
            }
            GL.End();

            // draw top polygon
            GL.Begin(PrimitiveType.Polygon);
            for (int i = 0; i < vertices.Length; i++)
            {
                Vector3D next = vertices[i];// +offset;
                GL.Vertex3(next.X, next.Y, next.z + Height);
            }
            GL.End();

            // draw slide lines
            GL.Begin(PrimitiveType.Lines);
            for (int i = 0; i < vertices.Length; i++)
            {
                Vector3D cur = vertices[i];
                GL.Vertex3(cur.X, cur.Y, cur.z);
                GL.Vertex3(cur.X, cur.Y, cur.z + Height);
            }
            GL.End();
        }*/

        //================================================================================================================================================
        public override double GetArea()
        {
            return Poly.Area;
        }

        //================================================================================================================================================
        public override bool ContainsPoint(Vector2D p)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}

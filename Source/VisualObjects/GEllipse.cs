﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GEllipse.cs 
//  Copyright (C) 2014/10/18  12:56 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using CPlan.Geometry;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace CPlan.VisualObjects
{
    using GdiPointF = System.Drawing.PointF;

    [Serializable]
    public class GEllipse : GPolygon
    {

        /// <summary>
        /// Define the number of segments for the ellipse.
        /// Use values in the interval [0.3 , 0.01]
        /// </summary>
        public float Precision { get; set; }

        //================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>GEllipse</b> class in two-dimensional space.
        /// </summary>
        /// <param name="p">The center point</param>
        /// <param name="radius1">The first radius</param>
        /// <param name="radius2">The second radius</param>
        /// <param name="precision">Define the number of segments for the ellipse. Use values in the interval [0.3 , 0.01]</param>
        public GEllipse(Vector2d p, int radius1, int radius2, float precision)
        {
            Precision = precision;
            int length = Convert.ToInt32(2.0f*3.14159/Precision);// + 1;
            Poly = new Poly2D {PointsFirstLoop = new Vector2d[length]};
            double vectorX;
            double vectorY;
            int i = 0;

            for (float angle = 0.0f; angle <= (2.0f * 3.14159); angle += Precision)
            {
                vectorX=p.X+(radius1*Math.Sin(angle));
                vectorY=p.Y+(radius2*Math.Cos(angle));
                Poly.PointsFirstLoop[i] = new Vector2d(vectorX, vectorY);
                i++;      
            }
            Transparency = 1;
        }

        //================================================================================================================================================
        //public override void Draw()
        //{
        //    double dZ = 0;
        //    GL.LineWidth(BorderWidth);
        //    GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
        //    GL.Color4(BorderColor);
        //    GL.Begin(PrimitiveType.Polygon);
        //    for (int i = 0; i < Poly.PointsFirstLoop.Length; i++)
        //    {
        //        Vector2d next = Poly.PointsFirstLoop[i];// +offset;
        //        GL.Vertex3(next.X, next.Y, dZ);
        //    }
        //    GL.LineWidth(1);
        //    GL.End();
        //}

    }
}

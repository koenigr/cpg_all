﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GGrid.cs 
//  Copyright (C) 2014/10/18  12:56 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using CPlan.Geometry;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace CPlan.VisualObjects
{
    [Serializable]
    public class GGrid : Surfaces
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Fields

        private double[] _values;
        private List<Vector2d> _points;
        protected Vector3d[] _colorValues;
        protected double _cellSize;
        protected String _name;
        protected bool _show = true;
        protected float _transparency = 1;

        private int _colorMode = 2; //0 = greyScale, 1 = Farbe
        private int _interpolation = 0; //0 = linear, 1 = abhängig von der Verteilung der Werte

        private int _texDx;
        private int _texDy;
        private Vector2d _texCornerPosition;
        private byte[] _texBuffer;  // BGRA
        private int _texId;
        private bool _texIdObtained = false;

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        [Category("Settings"), Description("Size of the Grid Cells")]
        public double CellSize { get { return _cellSize; } set { _cellSize = value; } }
        [Category("Settings"), Description("Origin Point of the Grid")]
        public Vector2d OriginPoint { get; set; }
        [Category("Visualisation"), Description("Show Grid on Screen")]
        public bool ShowGrid { get { return _show; } set { _show = value; } }
        [Category("Visualisation"), Description("Set Transparency of the grid (0 = invisble, 1 = opaque)")]
        public float Transparency { get { return _transparency; } set { _transparency = value; } }
        //[CategoryAttribute("Information"), DescriptionAttribute("Total Number of Grid Cells")]
        //public int NumberOfCells { get { return _values.Count(); } }
 
        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        //================================================================================================================================================
        /// <summary>
        /// Initiate a new GGrid.
        /// </summary>
        /// <param name="points"></param>
        /// <param name="isovistField"></param>
        /// <param name="cellSize"></param>
        public GGrid(List<Vector2d> points,  double[] values, double cellSize)
        {
            _points = points;
            _values = values;
            _cellSize = cellSize;
            OriginPoint = new Vector2d(0, 0);
            GenerateTexture();
        }


        public ScalarField ScalarField { get { return _scalarField; } set { _scalarField = value; } }
        private ScalarField _scalarField = null;
        public bool SFDerived = false;
        private string _fieldName;
        public GGrid(ScalarField scalarField, string fieldName)
        {
            //OriginPoint = new Vector2d(0, 0);
            _scalarField = scalarField;
            _cellSize = CellSize;
            SFDerived = true;
            _fieldName = fieldName;
        }

        //================================================================================================================================================
        //public GGrid(IsovistAnalysis isovistField)
        //{
        //    _points = isovistField.AnalysisPoints;
        //    _values = isovistField.GetMeasure();
        //    _cellSize = isovistField.CellSize;
        //    OriginPoint = new Vector2d(0, 0);
        //    GenerateTexture();
        //}

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        //================================================================================================================================================
        public void CalculateSize(out double minX, out double minY, out double maxX, out double maxY)
        {
            minX = minY = double.MaxValue;
            maxX = maxY = double.MinValue;

            foreach (Vector2d p in _points)
            {
                if (p.X < minX) minX = p.X;
                if (p.Y < minY) minY = p.Y;
                if (p.X > maxX) maxX = p.X;
                if (p.Y > maxY) maxY = p.Y;
            }
        }

        //================================================================================================================================================
        public void GenerateTexture()
        {
            if (SFDerived)
            {
                _texIdObtained = false;
                //_scalarField.GenerateTexture(_values, _fieldName);
                return;
            }
            double minX, maxX, minY, maxY;
            CalculateSize(out minX, out minY, out maxX, out maxY);
            double halfSize = _cellSize * 0.5;

            _texDx = (int)Math.Floor((maxX - minX) / _cellSize + 0.5) + 1;
            _texDy = (int)Math.Floor((maxY - minY) / _cellSize + 0.5) + 1;
            _texCornerPosition = new Vector2d(minX - halfSize, minY - halfSize);
            _texBuffer = new byte[_texDx * _texDy * 4];
            Tools.Populate<byte>(_texBuffer, 0);

            switch (_colorMode)
            {
                case 0:
                    GetNormArray();
                    break;
                case 1:
                    GetNormArray();
                    _colorValues = ConvertColor.CreateFFColor1D(_values);
                    break;
                case 2:
                    GetNormArray();
                    _colorValues = ConvertColor.CreateFFColor1D(_values);
                    break;
                default:
                    GetNormArray();
                    break;
            }
            
            System.Collections.IEnumerator e = _values.GetEnumerator();
            System.Collections.IEnumerator cv = _colorValues.GetEnumerator();
            foreach (Vector2d p in _points)
            {
                if (e.MoveNext() && cv.MoveNext())
                {
                    int x = (int)Math.Floor((p.X - minX) / _cellSize + 0.5);
                    int y = (int)Math.Floor((p.Y - minY) / _cellSize + 0.5);
                    Vector3d col = (Vector3d)cv.Current;
                    int pos = (y * _texDx + x) * 4;
                    _texBuffer[pos++] = (byte)Math.Floor((float)col.Z * 255.0f + 0.5f);
                    _texBuffer[pos++] = (byte)Math.Floor((float)col.Y * 255.0f + 0.5f);
                    _texBuffer[pos++] = (byte)Math.Floor((float)col.X * 255.0f + 0.5f);
                    _texBuffer[pos] = (byte)Math.Floor((float)_transparency * 255.0f + 0.5f);
                }
            }
            
        }

        //================================================================================================================================================
        /// <summary>
        /// Test, if a point is located on or in a geometry.
        /// </summary>
        /// <returns>If a geometry contains a point.</returns>
        public override bool ContainsPoint(Vector2d point)
        {
            return false;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Get the normalised Distances value of all grid elements.
        /// </summary>
        /// <remarks>
        /// It is useful for the definition of the color values of the regular polygons
        /// of the corresponding <see cref="GGridOld"/>.
        /// </remarks>
        /// <returns>
        /// The normalise Distances values as <see cref="float[,]"/> array.
        /// </returns>
        public void GetNormArray()
        {
            if (SFDerived)
                return;
            Tools.CutMinValue(_values, Tools.GetMin(_values));
            Tools.Normalize(_values);
            Tools.IntervalExtension(_values, Tools.GetMax(_values));
        }

        //================================================================================================================================================
        /// <summary>
        /// Sets the color mode of the grid.
        /// </summary>
        /// <param m_name="col">(int) color mode. 0 - grey scale, 1 - color.</param>
        public void SetColorMode(int col)
        {
            if (col < 0 || col > 2) _colorMode = 2;
            else _colorMode = col;
        }

        //================================================================================================================================================
        public override void Draw()
        {
            if (!_show)
                return;

            if (SFDerived)
            {

                if (!_texIdObtained)
                {
                    _texId = GL.GenTexture();

                    GL.ActiveTexture(TextureUnit.Texture0);
                    GL.BindTexture(TextureTarget.Texture2D, _texId);
                    GL.TexImage2D<byte>(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba,
                        _scalarField.TexWidth, _scalarField.TexHeight, 0,
                        PixelFormat.Rgba, PixelType.UnsignedByte, _scalarField.Texture);
                    _texIdObtained = true;
                }

                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, _texId);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToBorder);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToBorder);
                GL.Enable(EnableCap.Texture2D);

                GL.Enable(EnableCap.Blend);
                GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
                GL.DepthMask(false);
                GL.Enable(EnableCap.DepthTest);

                
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                
                GL.Begin(PrimitiveType.Triangles);

                GL.Color4(1.0f, 1.0f, 1.0f, 1.0f);
                GL.Normal3(_scalarField.Normal);


                for(int i = 0; i < _scalarField.PointCoordinates.Length; i++)
                {
                    GL.TexCoord2(_scalarField.TextureCoordinates[i]);
                    GL.Vertex3(_scalarField.PointCoordinates[i]);
                }

                GL.End();

                GL.ActiveTexture(TextureUnit.Texture0);
                GL.Disable(EnableCap.Texture2D);
                GL.DepthMask(true);

                return;
            }

            switch (_colorMode)
            {
                case 0:
                    GetNormArray();
                    break;
                case 1:
                    GetNormArray();
                    _colorValues = ConvertColor.CreateFFColor1D(_values);
                    break;
                case 2:
                    GetNormArray();
                    _colorValues = ConvertColor.CreateFFColor1D(_values);
                    break;
                default:
                    GetNormArray();
                    break;
            }

            // --- Offset ---
            float offset = 0.0f;// -2;// -640;
            if (offset != 0.0f)
            {
                GL.Enable(EnableCap.PolygonOffsetPoint);
                GL.Enable(EnableCap.PolygonOffsetLine);
                GL.Enable(EnableCap.PolygonOffsetFill);
                GL.PolygonOffset(0.0f, -offset);
            }

            if (!_texIdObtained)
            {
                _texId = GL.GenTexture();

                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.Texture2D, _texId);
                GL.TexImage2D<byte>(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, _texDx, _texDy, 0, PixelFormat.Bgra, PixelType.UnsignedByte, _texBuffer);
                _texIdObtained = true;
            }

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, _texId);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToBorder);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToBorder);
            GL.Enable(EnableCap.Texture2D);

            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.DepthMask( false );
		    GL.Enable(EnableCap.DepthTest);

            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
            GL.Color4(1.0f, 1.0f, 1.0f, 1.0f);
            GL.Normal3(0.0f, 1.0f, 0.0f);

            GL.Begin(PrimitiveType.Quads);

            double sizeX = _texDx * _cellSize;
            double sizeY = _texDy * _cellSize;
            GL.TexCoord2(0.0f, 0.0f);
            GL.Vertex3(_texCornerPosition.X + OriginPoint.X, _texCornerPosition.Y + OriginPoint.Y, 0);
            GL.TexCoord2(1.0f, 0.0f);
            GL.Vertex3(_texCornerPosition.X + OriginPoint.X + sizeX, _texCornerPosition.Y + OriginPoint.Y, 0);
            GL.TexCoord2(1.0f, 1.0f);
            GL.Vertex3(_texCornerPosition.X + OriginPoint.X + sizeX, (_texCornerPosition.Y + sizeY) + OriginPoint.Y, 0);
            GL.TexCoord2(0.0f, 1.0f);
            GL.Vertex3(_texCornerPosition.X + OriginPoint.X, (_texCornerPosition.Y + sizeY) + OriginPoint.Y, 0);

            GL.End();

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.Disable(EnableCap.Texture2D);
            GL.DepthMask( true );
            
            if (offset != 0.0f)
            {
                GL.Disable(EnableCap.PolygonOffsetPoint);
                GL.Disable(EnableCap.PolygonOffsetLine);
                GL.Disable(EnableCap.PolygonOffsetFill);
            }
        }

        //================================================================================================================================================
        public override double GetArea()
        {
            throw new NotImplementedException();
        }

        //================================================================================================================================================
        public override void Move(Vector2d delta)
        {
            OriginPoint = OriginPoint + delta;

            if (_scalarField != null)
            {
                _scalarField.MoveTo(delta);
            }        
        }

        //================================================================================================================================================
        public override bool Selector(Vector2d delta)
        {
            throw new NotImplementedException();
        }

        //================================================================================================================================================
        public override void GetBoundingBox(BoundingBox box)
        {
            if (_points != null)
            for (int i = 0; i < _points.Count; i++)
            {
                box.AddPoint(_points[i]);
            }
        }

        # endregion
    }

}


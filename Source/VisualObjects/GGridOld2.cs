﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using System.ComponentModel;
using CPlan.Geometry;
using CPlan.Evaluation;
using CPlan.Isovist2D;

namespace CPlan.VisualObjects
{
    [Serializable]
    public class GGridOld2 : Surfaces
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Fields

        private double[] _values;
        private List<Vector2D> _points;
        protected Vector3D[] _colorValues;
        protected double _cellSize;
        protected String _name;
        protected bool _show = true;
        protected float _transparency = 1;

        int _colorMode = 2; //0 = greyScale, 1 = Farbe
        int _interpolation = 0; //0 = linear, 1 = abhängig von der Verteilung der Werte

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties

        [CategoryAttribute("Settings"), DescriptionAttribute("Size of the Grid Cells")]
        public double CellSize { get { return _cellSize; } set { _cellSize = value; } }
        [CategoryAttribute("Settings"), DescriptionAttribute("Size of the Grid Cells")]
        public Vector2D OriginPoint { get; set; }
        [CategoryAttribute("Visualisation"), DescriptionAttribute("Show Grid on Screen")]
        public bool ShowGrid { get { return _show; } set { _show = value; } }
        [CategoryAttribute("Visualisation"), DescriptionAttribute("Set Transparency of the grid (0 = invisble, 1 = opaque)")]
        public float Transparency { get { return _transparency; } set { _transparency = value; } }

        //[CategoryAttribute("Information"), DescriptionAttribute("Total Number of Grid Cells")]
        //public int NumberOfCells { get { return _values.Count(); } }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors

        //================================================================================================================================================
        /// <summary>
        /// Initiate a new GGridOld2.
        /// </summary>
        /// <param name="points"></param>
        /// <param name="isovistField"></param>
        /// <param name="cellSize"></param>
        public GGridOld2(List<Vector2D> points, double[] values, double cellSize)
        {
            _points = points;
            _values = values;
            _cellSize = cellSize;
            OriginPoint = new Vector2D(0, 0);
        }

        public GGridOld2(IsovistAnalysis isovistField)
        {
            _points = isovistField.AnalysisPoints;
            _values = isovistField.GetMeasure();
            _cellSize = isovistField.CellSize;
            OriginPoint = new Vector2D(0, 0);
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        /// <summary>
        /// Test, if a point is located on or in a geometry.
        /// </summary>
        /// <returns>If a geometry contains a point.</returns>
        public override bool ContainsPoint(Vector2D point)
        {
            //Rect2D tempRect = new Rect2D(StartP.X, -StartP.Y, EndP.X, -EndP.Y);
            //return tempRect.ContainsPoint(point);
            return false;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Get the normalised Distances value of all grid elements.
        /// </summary>
        /// <remarks>
        /// It is useful for the definition of the color values of the regular polygons
        /// of the corresponding <see cref="GGridOld2Old"/>.
        /// </remarks>
        /// <returns>
        /// The normalise Distances values as <see cref="float[,]"/> array.
        /// </returns>
        public void GetNormArray()
        {
            Tools.CutMinValue(_values, Tools.GetMin(_values));
            Tools.Normalize(_values);
            Tools.IntervalExtension(_values, Tools.GetMax(_values));
        }

        /// <summary>
        /// Sets the color mode of the grid.
        /// </summary>
        /// <param m_name="col">(int) color mode. 0 - grey scale, 1 - color.</param>
        public void SetColorMode(int col)
        {
            if (col < 0 || col > 2) _colorMode = 2;
            else _colorMode = col;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Interfaces

        public void Dialog()
        {
        }

        public override void Draw()
        {
            if (!_show)
                return;

            switch (_colorMode)
            {
                case 0:
                    GetNormArray();
                    break;
                case 1:
                    GetNormArray();
                    _colorValues = ConvertColor.CreateFFColor1D(_values);
                    break;
                case 2:
                    GetNormArray();
                    _colorValues = ConvertColor.CreateFFColor1D(_values);
                    break;
                default:
                    GetNormArray();
                    break;
            }

            // --- Offset ---
            float offset = 0.0f;// -2;// -640;
            if (offset != 0.0f)
            {
                GL.Enable(EnableCap.PolygonOffsetPoint);
                GL.Enable(EnableCap.PolygonOffsetLine);
                GL.Enable(EnableCap.PolygonOffsetFill);
                GL.PolygonOffset(0.0f, -offset);
            }

            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);

            for (int i = 0; i < _points.Count; i++)
            {
                Vector4 FillColor = new Vector4();
                FillColor = new Vector4((float)_colorValues[i].X, (float)_colorValues[i].Y, (float)_colorValues[i].Z, _transparency);

                double halfSize = _cellSize / 2;
                GL.Begin(BeginMode.Polygon);
                GL.Color4(FillColor);
                GL.Vertex3(OriginPoint.X + _points[i].X - halfSize, 0, -(OriginPoint.Y + _points[i].Y - halfSize));
                GL.Vertex3(OriginPoint.X + _points[i].X - halfSize, 0, -(OriginPoint.Y + _points[i].Y + halfSize));
                GL.Vertex3(OriginPoint.X + _points[i].X + halfSize, 0, -(OriginPoint.Y + _points[i].Y + halfSize));
                GL.Vertex3(OriginPoint.X + _points[i].X + halfSize, 0, -(OriginPoint.Y + _points[i].Y - halfSize));
                //GL.Vertex3(m_startP.X + x * m_size.X / m_countX, 0, -(m_startP.Y + y * m_size.Y / m_countY));
                //GL.Vertex3(m_startP.X + x * m_size.X / m_countX, 0, -(m_startP.Y + y * m_size.Y / m_countY + m_cellSize));
                //GL.Vertex3(m_startP.X + x * m_size.X / m_countX + m_cellSize, 0, -(m_startP.Y + y * m_size.Y / m_countY + m_cellSize));
                //GL.Vertex3(m_startP.X + x * m_size.X / m_countX + m_cellSize, 0, -(m_startP.Y + y * m_size.Y / m_countY));
                GL.End();
            }

            if (offset != 0.0f)
            {
                GL.Disable(EnableCap.PolygonOffsetPoint);
                GL.Disable(EnableCap.PolygonOffsetLine);
                GL.Disable(EnableCap.PolygonOffsetFill);
            }
        }

        //public string GetIdentificationName()
        //{
        //    return m_name;
        //}

        #endregion


        public override double GetArea()
        {
            throw new NotImplementedException();
        }

        public override void Move(Vector2D delta)
        {
            OriginPoint = OriginPoint + delta;
        }

        public override bool Selector(Vector2D delta)
        {
            throw new NotImplementedException();
        }

        public override void GetBoundingBox(BoundingBox box)
        {
            //Rect2D tempRect = new Rect2D(StartP.X, -StartP.Y, EndP.X, -EndP.Y);
            for (int i = 0; i < _points.Count; i++)
            {
                box.AddPoint(_points[i]);
            }
        }

        public override void DrawPickingMode()
        {
            throw new NotImplementedException();
        }
    }

}


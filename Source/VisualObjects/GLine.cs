﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GLine.cs 
//  Copyright (C) 2014/10/18  12:56 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Drawing.Drawing2D;
using CPlan.Geometry;
using OpenTK.Graphics.OpenGL;
using Tektosyne.Geometry;
using OpenTK;

namespace CPlan.VisualObjects
{
    /// <summary>
    /// Represents a directed line segment in two-dimensional space, using <see cref="double"/>
    /// coordinates.</summary>
    [Serializable]
    public class GLine : Lines
    {
        //############################################################################################################################################################################
        # region Properties
        /// <summary>
        /// Basic Line2D object. </summary>
        public Line2D Line
        {
            get
            {
                return new Line2D(Start.Pos, End.Pos);
            }
            protected set
            {
                Start.Pos = value.Start;
                End.Pos = value.End;
            }
        }
        public GPoint Start { get; protected set; }
        public GPoint End { get; protected set; }
        public bool ActiveHandles { get; protected set; } // if the line is constructed by reference points, handles needs to be deactivated
        
        private int _selected; // 0 = not selected, 1 = line selected, 2 = start-point selected, 3 = end-point selected

        # endregion

        //############################################################################################################################################################################
        # region Constructors

        public GLine() { }

        //============================================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>GLine</b> class in two-dimensional space, 
        /// using <see cref="double"/> coordinates.
        /// using WORLD coordinates
        /// do not use for mouse interaction
        /// </summary>
        /// <param name="x1">The x-coordinate of the start point.</param>
        /// <param name="y1">The y-coordinate of the start point.</param>
        /// <param name="x2">The x-coordinate of the end point.</param>
        /// <param name="y2">The y-coordinate of the end point.</param>
        public GLine(double x1, double y1, double x2, double y2)
        {
            Start = new GPoint(x1, y1);
            End = new GPoint(x2, y2);
            _selected = 0;
            ActiveHandles = true;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>GLine</b> class in two-dimensional space, 
        /// using the <see cref="System.Drawing.PointF"/> point structure.
        /// using WORLD coordinates
        /// do not use for mouse interaction
        /// </summary>
        /// <param name="pt1">The start point of the line.</param>
        /// <param name="pt2">The end point of the line.</param>
        public GLine(System.Drawing.PointF pt1, System.Drawing.PointF pt2)
        {
            Start = new GPoint(pt1.X, pt1.Y);
            End = new GPoint(pt2.X, pt2.Y);
            _selected = 0;
            ActiveHandles = true;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>GLine</b> class in two-dimensional space.
        /// </summary>
        /// <param name="pt1">The start point of the line.</param>
        /// <param name="pt2">The end point of the line.</param>
        public GLine(Vector2d pt1, Vector2d pt2)
        {
            Start = new GPoint(pt1.X, pt1.Y);
            End = new GPoint(pt2.X, pt2.Y);
            _selected = 0;
            ActiveHandles = true;
        }

        //============================================================================================================================================================================
        public GLine(ref GPoint pt1, ref GPoint pt2)
        {
            Start = pt1;
            End = pt2;
            _selected = 0;
            ActiveHandles = false;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Initializes a new instance of the GLine.
        /// </summary>>
        public GLine(LineD line)
        {
            Start = new GPoint(line.Start.X, line.Start.Y);
            End = new GPoint(line.End.X, line.End.Y);
            _selected = 0;
            ActiveHandles = true;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Initializes a new instance of the <b>GLine</b> from a <see cref="Line2D"/>.
        /// </summary>
        public GLine(Line2D line)
        {
            Start = new GPoint(line.Start.X, line.Start.Y);
            End = new GPoint(line.End.X, line.End.Y);
            _selected = 0;
            ActiveHandles = true;
        }

        # endregion

        //############################################################################################################################################################################
        # region Methods
        /// <summary>
        /// Moves the <b>GLine</b>
        /// </summary>
        /// <param name="delta">The 2d vector to move the line segment.</param>
        public override void Move(Vector2d delta)
        {
            Matrix mat = new Matrix();
            switch (_selected)
            {
                case 1:
                    Start.Pos += delta;
                    End.Pos += delta;
                    mat.Translate((int)delta.X, (int)delta.Y);
                    break;
                case 2:
                    Start.Pos += delta;
                    mat.Translate((int)delta.X, (int)delta.Y);
                    break;
                case 3:
                    End.Pos += delta;
                    mat.Translate((int)delta.X, (int)delta.Y);
                    break;
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Highlights the Line or the End-Points of the <b>GLine</b>
        /// </summary>
        /// <param name="mPoint">The mouse point.</param>
        public override bool Selector(Vector2d mPoint)
        {
            _selected = 0;
            double lineDist = Line.DistanceToPoint(mPoint);
            
            if (ActiveHandles)
            {
                if (lineDist < PickingThreshold)
                    _selected = 1; //Linie selektiert

                double distance = Math.Sqrt((Math.Pow(mPoint.X - Start.Pos.X, 2.0) - Math.Pow(mPoint.Y - Start.Pos.Y, 2.0)));
                if ((distance <= PickingThreshold))//&& (lineDist < 10))
                    _selected = 2; //Startpunkt = selected

                distance = Math.Sqrt((Math.Pow(mPoint.X - End.Pos.X, 2.0) - Math.Pow(mPoint.Y - End.Pos.Y, 2.0)));
                if ((distance <= PickingThreshold))//&& (lineDist < 10))
                    _selected = 3; //Endpunkt = selected
            }
            if (_selected != 0)
                return true;
            else
                return false;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Test, if a point is located on or in a geometry.
        /// </summary>
        /// <returns>If a geometry contains a point.</returns>
        public override bool ContainsPoint(Vector2d point)
        {
            bool contains = false;
            double lineDist = Line.DistanceToPoint(point);
            if (lineDist <= PickingThreshold) contains = true;
            return contains;
        }
        
        //============================================================================================================================================================================
        public void SetCoordinates(Vector2d pt1, Vector2d pt2)
        {
            Start = new GPoint(pt1.X, pt1.Y);
            End = new GPoint(pt2.X, pt2.Y);
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Draw the GLine object.
        /// </summary>
        public override void Draw()
        {
            double dZ = 0;

            if (_selected == 0)
            {
                GL.LineWidth(Width);// * m_zoomFactor);
                GL.Color4(Color);
                GL.Begin(PrimitiveType.Lines);
                GL.Vertex3(Start.Pos.X, Start.Pos.Y, dZ);
                GL.Vertex3(End.Pos.X, End.Pos.Y, dZ);
                GL.End();
            }
            else if (_selected >= 1)
            {
                GL.LineWidth(Width);
                GL.Color4(SelectColor);
                GL.Begin(PrimitiveType.Lines);
                GL.Vertex3(Start.Pos.X, Start.Pos.Y, dZ);
                GL.Vertex3(End.Pos.X, End.Pos.Y, dZ);
                GL.End();
            }

            if (_selected == 2) //zeichne Startpunkt-Anfasser (Handle)
            {
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(SelectColor);
                GL.LineWidth(1);
                GL.Begin(PrimitiveType.Quads);
                GL.Vertex3(Start.Pos.X - HandleSize, Start.Pos.Y - HandleSize, dZ);
                GL.Vertex3(Start.Pos.X - HandleSize, Start.Pos.Y + HandleSize, dZ);
                GL.Vertex3(Start.Pos.X + HandleSize, Start.Pos.Y + HandleSize, dZ);
                GL.Vertex3(Start.Pos.X + HandleSize, Start.Pos.Y - HandleSize, dZ);
                GL.End();
            }
            else if (_selected == 3) //zeichne Endpunkt-Anfasser (Handle)
            {
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(SelectColor);
                GL.LineWidth(1);
                GL.Begin(PrimitiveType.Quads);
                GL.Vertex3(End.Pos.X - HandleSize, End.Pos.Y - HandleSize, dZ);
                GL.Vertex3(End.Pos.X - HandleSize, End.Pos.Y + HandleSize, dZ);
                GL.Vertex3(End.Pos.X + HandleSize, End.Pos.Y + HandleSize, dZ);
                GL.Vertex3(End.Pos.X + HandleSize, End.Pos.Y - HandleSize, dZ);
                GL.End();
            }
        }

        //============================================================================================================================================================================
        public override void GetBoundingBox(BoundingBox box)
        {
            box.AddPoint(Start.Pos);
            box.AddPoint(End.Pos);
        }

        #endregion

    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GPoint.cs 
//  Copyright (C) 2014/10/18  12:56 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using CPlan.Geometry;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CPlan.VisualObjects
{
    [Serializable]
    public class GPoint : GeoObject
    {
        //############################################################################################################################################################################
        # region Properties

        private int _selected;
        public Vector2d Pos { get; set; }
        public Vector2d[] PolyPoints { get; set; }
        public double Radius { get; protected set; }
        
        /// <summary> The fill color (as vector) of the survace object. </summary>
        public Vector4 Color { get; set; }

        #endregion

        //############################################################################################################################################################################
        #region Constructor

        //==============================================================================================
        public GPoint(double x, double y)
        {
            Pos = new Vector2d(x, y);

            Radius = 0.2;
            const int sides = 12;
            PolyPoints = new Vector2d[sides];
            for (int i = 0; i < PolyPoints.Length; i++)
            {
                double degInRad = i * 3.1416 / (sides/2);
                PolyPoints[i] = new Vector2d(Math.Cos(degInRad) * Radius + Pos.X, Math.Sin(degInRad) * Radius + Pos.Y);
            }
        }
        #endregion

        //############################################################################################################################################################################
        # region Methods

        //==============================================================================================
        public override bool Selector(Vector2d mPoint)
        {
            _selected = 0;

            if (mPoint.Distance(Pos) < Radius)
                _selected = 1; //Kreisinneres selektiert

            if (_selected != 0)
                return true;

            return false;
        }

        //==============================================================================================
        /// <summary>
        /// Test, if a point is located on or in a geometry.
        /// </summary>
        /// <returns>If a geometry contains a point.</returns>
        public override bool ContainsPoint(Vector2d point)
        {
            bool contains = point.Distance(Pos) < Radius;
            return contains;
        }

        //==============================================================================================
        /// <summary>
        /// Moves the Point
        /// </summary>
        /// <param name="delta">The 2d vector to move the polygon.</param>
        public override void Move(Vector2d delta)
        {
            if (_selected == 1)
            {
                Pos = Pos + delta;
                for (int i = 0; i < PolyPoints.Length; i++)
                {
                    PolyPoints[i] = PolyPoints[i] + delta;
                }
            }
            if (_selected == 2)
            {
                //this = new GCircle(midPoint, delta.Length);
            }

        }

        //==============================================================================================
        /// <summary>
        /// Draw the <b>GCircle</b> to a <see cref="OpenTK.OpenGL"/> component
        /// </summary>
        public override void Draw()
        {
            double dZ = 0;
            Vector2d[] vertices = PolyPoints;
            int width = 1;

            if (_selected == 0)
            {
                GL.LineWidth(width);
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(Color);
                GL.PointSize(10);
                GL.Begin(PrimitiveType.Points);
                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector2d next = vertices[i];
                    GL.Vertex3(next.X, next.Y, dZ);
                }
                GL.End();
            }
            else if (_selected >= 1)
            {
                GL.LineWidth(width );
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(SelectColor);
                GL.Begin(PrimitiveType.Polygon);
                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector2d next = vertices[i];
                    GL.Vertex3(next.X, next.Y, dZ);
                }
                GL.LineWidth(1 * m_zoomFactor);
                GL.End();
            }
        }

        //==============================================================================================
        public override void GetBoundingBox(BoundingBox box)
        {
            box.AddPoint(Pos);
        }

        //==============================================================================================
        public Tektosyne.Geometry.PointD ToPointD()
        {
            return new Tektosyne.Geometry.PointD(Pos.X, Pos.Y);
        }

        # endregion

    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GPolygon.cs 
//  Copyright (C) 2014/10/18  12:56 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CPlan.Geometry;
using OpenTK.Graphics.OpenGL;
using Tektosyne;
using OpenTK;

namespace CPlan.VisualObjects
{
    using GdiPointF = System.Drawing.PointF;
    using GdiPoint = System.Drawing.Point;
    using System.ComponentModel;

    /// <summary>
    /// Represents a polygon in two-dimensional space, using <see cref="double"/> coordinates.
    /// </summary>
    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class GPolygon : Surfaces
    {
        //############################################################################################################################################################################
        # region Properties

        /// <summary>
        /// Poly2D class.
        /// </summary>
        public Poly2D Poly { get; protected set; }

        private int _selected; // 0 = not selected, 1 = point selected, 2 = line selected, 3 = polygon selected 
        private int _selectedPoint;
        private int _selectedLineP1, _selectedLineP2;

        //==========================================================================================================================================
        /// <summary>
        /// The outer border (without wholes) of the polygon.
        /// </summary>
        public Vector2d[] PolyPoints { get { return Poly.PointsFirstLoop; } }

        //==========================================================================================================================================
        /// <summary> List with the edges of the polygon. </summary>
        public Line2D[] Edges
        {
            get
            {
                Line2D[] edges = new Line2D[Poly.PointsFirstLoop.Count()];
                for (int i = 0; i < Poly.PointsFirstLoop.Count(); i++)
                {
                    int k = i + 1;
                    if (k >= Poly.PointsFirstLoop.Count()) k = 0;
                    edges[i] = new Line2D(Poly.PointsFirstLoop[i], Poly.PointsFirstLoop[k]);
                }
                return edges;
            }
        }

        //==========================================================================================================================================
        /// <summary>
        /// Centroid of the polygon.
        /// </summary>
        public Vector2d Centroid
        {
            get {return GeoAlgorithms2D.Centroid(Poly.PointsFirstLoop.ToList());}
        }

        # endregion

        //############################################################################################################################################################################
        # region Constructors
        
        public GPolygon() { }

        //==========================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>GPolygon</b> class in two-dimensional space.
        /// </summary>
        public GPolygon(params Vector2d[] polygon)
        {
            if (polygon == null)
                ThrowHelper.ThrowArgumentNullException("polygon");
            if (polygon.Length < 3)
                ThrowHelper.ThrowArgumentExceptionWithFormat(
                    "polygon.Length", Strings.ArgumentLessValue, 3);

            Poly = new Poly2D(polygon);
            Transparency = 1;
        }

        //==========================================================================================================================================
        public GPolygon(List<Vector2d> polygon)
        {
            if (polygon.Count == 0)
                ThrowHelper.ThrowArgumentNullException("polygon");
            if (polygon.Count < 3)
                ThrowHelper.ThrowArgumentExceptionWithFormat(
                    "polygon.Length", Strings.ArgumentLessValue, 3);
            
            Poly = new Poly2D(polygon.ToArray());
            Transparency = 1;
        }

        //==========================================================================================================================================
        public GPolygon(Poly2D polygon)
        {
            if (polygon.PointsFirstLoop.Count() == 0)
                ThrowHelper.ThrowArgumentNullException("polygon");
            if (polygon.PointsFirstLoop.Count() < 3)
                ThrowHelper.ThrowArgumentExceptionWithFormat(
                    "polygon.Length", Strings.ArgumentLessValue, 3);

            Poly = new Poly2D(polygon);
            Transparency = 1;
        }

        //==========================================================================================================================================
        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="source">GPolygon to copy</param>
        public GPolygon(GPolygon source)
        {
            Poly = source.Poly.Clone();     
            _selected = source._selected; // 0 = not selected, 1 = point selected, 2 = line selected, 3 = polygon selected 
            _selectedPoint = source._selectedPoint;
            _selectedLineP1 = source._selectedLineP1;
            _selectedLineP2 = source._selectedLineP2;
            Transparency = 1;
        }

        # endregion

        //############################################################################################################################################################################
        # region Methods

        //==========================================================================================================================================
        /// <summary>
        /// Clone the GPolygon.
        /// </summary>
        /// <returns>Returns exact clone of the polygon.</returns>
        public object Clone()
        {
            GPolygon clone = new GPolygon(this);
            return clone;
        }

        //==========================================================================================================================================
        /// <summary>
        /// Moves the <b>GPolygon</b>
        /// </summary>
        /// <param name="delta">The 2d vector to move the polygon.</param>
        public override void Move(Vector2d delta)    
        {
            Vector2d[] vertices;
            switch (_selected)
            {
                case 1:
                    Poly.PointsFirstLoop[_selectedPoint] += delta;
                    break;

                case 2:
                    Poly.PointsFirstLoop[_selectedLineP1] += delta;
                    Poly.PointsFirstLoop[_selectedLineP2] += delta;
                    break;

                case 3:
                    goto default;

                default:
                    vertices = Poly.PointsFirstLoop;
                    for (int i = 0; i < vertices.Length; i++)
                    {
                        vertices[i] = vertices[i] + delta;
                    }
                    break;
            }
        }

        //==========================================================================================================================================
        /// <summary>
        /// Defines the select mode.
        /// </summary>
        public override bool Selector(Vector2d mPoint)
        {       
            _selected = 0;

            //alle Linien durchschauen
            Line2D curEdge;
            for (int i = 0; i < Poly.PointsFirstLoop.Length-1; i++)
            {
                curEdge = new Line2D(Poly.PointsFirstLoop[i], Poly.PointsFirstLoop[i + 1]);
                if (curEdge.DistanceToPoint(mPoint) <= PickingThreshold)
                {
                    _selected = 2; //Linie = selected
                    _selectedLineP1 = i;
                    _selectedLineP2 = i + 1;
                }
            }
            curEdge = new Line2D(Poly.PointsFirstLoop[0], Poly.PointsFirstLoop[Poly.PointsFirstLoop.Length - 1]);
            if (curEdge.DistanceToPoint(mPoint) <= PickingThreshold)
            {
                _selected = 2; //Linie = selected
                _selectedLineP1 = 0;
                _selectedLineP2 = Poly.PointsFirstLoop.Length - 1;
            }

            //alle Punkte durchschauen
            for (int i = 0; i < Poly.PointsFirstLoop.Length; i++ )
            {
                double distance = Math.Sqrt((Math.Pow(mPoint.X - Poly.PointsFirstLoop[i].X, 2.0) - Math.Pow(mPoint.Y - Poly.PointsFirstLoop[i].Y, 2.0)));
                if ( distance <= PickingThreshold ) 
                {
                    _selected = 1; //Punkt = selected
                    _selectedPoint = i;
                }
            }

            if (_selected == 0)
                if (ContainsPoint(mPoint)) 
                    _selected = 3;

            if (_selected != 0)
                return true;
           
                return false;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Test, if a point is located on or in a geometry.
        /// </summary>
        /// <returns>If a geometry contains a point.</returns>
        public override bool ContainsPoint(Vector2d point)
        {
            return Poly.ContainsPoint(point);
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Computes the area of the <b>GPolygon</b>
        /// </summary>
        /// <returns>
        /// The area of the <b>GPolygon</b>, as <see cref="double"/> value.
        /// </returns>
        public override Double GetArea()
        {
            return Poly.Area;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Draw the <b>GPolygon</b> to a <see cref="OpenTK.Graphics.OpenGL"/> component
        /// </summary>
        public override void Draw()
        {
            double dZ = DeltaZ;
            Vector2d[] vertices = Poly.PointsFirstLoop;

            
            if (_selected == 1) //draw handles 
            {
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(SelectColor);
                GL.LineWidth(1 * m_zoomFactor);
                GL.Begin(PrimitiveType.Quads);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedPoint].X - HandleSize, Poly.PointsFirstLoop[_selectedPoint].Y - HandleSize, dZ);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedPoint].X - HandleSize, Poly.PointsFirstLoop[_selectedPoint].Y + HandleSize, dZ);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedPoint].X + HandleSize, Poly.PointsFirstLoop[_selectedPoint].Y + HandleSize, dZ);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedPoint].X + HandleSize, Poly.PointsFirstLoop[_selectedPoint].Y - HandleSize, dZ);
                GL.End();

                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
                GL.Color4(0, 0, 0, .5);
                GL.LineWidth(1 * m_zoomFactor);
                GL.Begin(PrimitiveType.Polygon);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedPoint].X - HandleSize, Poly.PointsFirstLoop[_selectedPoint].Y - HandleSize, dZ);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedPoint].X - HandleSize, Poly.PointsFirstLoop[_selectedPoint].Y + HandleSize, dZ);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedPoint].X + HandleSize, Poly.PointsFirstLoop[_selectedPoint].Y + HandleSize, dZ);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedPoint].X + HandleSize, Poly.PointsFirstLoop[_selectedPoint].Y - HandleSize, dZ);
                GL.End();
            }

            if (_selected == 2) //draw line
            {
                GL.LineWidth(BorderWidth);
                GL.Color4(SelectColor);
                GL.Begin(PrimitiveType.Lines);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedLineP1].X, Poly.PointsFirstLoop[_selectedLineP1].Y, dZ);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedLineP2].X, Poly.PointsFirstLoop[_selectedLineP2].Y, dZ);
                GL.End();
            }

            if (_selected == 3)
            {
                GL.LineWidth(BorderWidth);
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
                GL.Color4(SelectColor);
                GL.Begin(PrimitiveType.Polygon);
                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector2d next = vertices[i];// +offset;
                    GL.Vertex3(next.X, next.Y, dZ);
                }
                GL.End();
            }
            
            // --------------- Draw the filled polygon -------------------------------------------------------------
            if (Filled)
            {
                //ToDo: Tesselierung .... Crashes at intersecting polygons... maybe use Faraser Physics Decomposer? -> Delauny : http://farseerphysics.codeplex.com/documentation
                List<Vector2d[]> triangles = Triangulation2D.Triangulate(vertices);
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(FillColor.X, FillColor.Y, FillColor.Z, Transparency);
                for (int i = 0; i < triangles.Count; i++)
                {
                    GL.Begin(PrimitiveType.Triangles);
                    GL.Vertex3(triangles[i][0].X, triangles[i][0].Y, dZ);
                    GL.Vertex3(triangles[i][1].X, triangles[i][1].Y, dZ);
                    GL.Vertex3(triangles[i][2].X, triangles[i][2].Y, dZ);
                    GL.End();
                }               
            }

            GL.LineWidth(BorderWidth);
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
            GL.Color4(BorderColor);
            GL.Begin(PrimitiveType.Polygon);
            for (int i = 0; i < vertices.Length; i++)
            {
                Vector2d next = vertices[i];// +offset;
                GL.Vertex3(next.X, next.Y, dZ);
            }
            GL.LineWidth(1);
            GL.End();
        }

        //==========================================================================================================================================
        public override void GetBoundingBox(BoundingBox box)
        {
            for (int i = 0; i < Poly.PointsFirstLoop.Count(); i++)
            {
                box.AddPoint(Poly.PointsFirstLoop[i]);
            }
        }

        # endregion
    }
}

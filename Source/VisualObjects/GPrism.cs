﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GPrism.cs 
//  Copyright (C) 2014/10/18  12:56 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using CPlan.Geometry;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace CPlan.VisualObjects
{
    using GdiPointF = System.Drawing.PointF;
    using GdiPoint = System.Drawing.Point;
    using System.ComponentModel;

    /// <summary>
    /// Represents a prism in three-dimensional space, using <see cref="double"/> coordinates.
    /// </summary>
    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class GPrism : Bodies
    {
        //############################################################################################################################################################################
        # region Properties

        public ScalarField[] SFields = null;
        private int[] _texIds = null;

        /// <summary>
        /// Poly2D class.
        /// </summary>
        public Poly2D Poly { get; protected set; }

        private int _selected; // 0 = not selected, 1 = point selected, 2 = line selected, 3 = polygon selected 
        private int _selectedPoint;
        private int _selectedLineP1, _selectedLineP2;

        public double Transparency {get; set;}
        public double Height { get; set; }
        public double LevelHeight { get; set; }

        public IEnumerable<Vector2d> PolyPoints { get { return Poly.PointsFirstLoop; } }

        /// <summary> List with the edges of the polygon. </summary>
        public Line2D[] Edges
        {
            get
            {
                Line2D[] edges = new Line2D[Poly.PointsFirstLoop.Count()];
                for (int i = 0; i < Poly.PointsFirstLoop.Count(); i++)
                {
                    int k = i + 1;
                    if (k >= Poly.PointsFirstLoop.Count()) k = 0;
                    edges[i] = new Line2D(Poly.PointsFirstLoop[i], Poly.PointsFirstLoop[k]);
                }
                return edges;
            }
        }

        //==========================================================================================================================================
        /// <summary>
        /// Centroid of the base polygon.
        /// </summary>
        public Vector2d Centroid
        {
            get { return GeoAlgorithms2D.Centroid(Poly.PointsFirstLoop.ToList()); }
            set { throw new NotImplementedException(); }
        }

        # endregion

        //############################################################################################################################################################################
        # region Constructors

        //==========================================================================================================================================
        public GPrism() 
        {
            Height = 0.0;
            LevelHeight = 0.0;
            Transparency = 1.0;
        }

        //==========================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>GPolygon</b> class in two-dimensional space.
        /// </summary>
        public GPrism(params Vector2d[] polygon)
        {
            if (polygon == null)
                throw new System.ArgumentException("Parameter cannot be null", "polygon");
            if (polygon.Length < 3)
                throw new System.InvalidOperationException("polygon length needs to be larger than 2");

            GdiPointF[] konvVert = new GdiPointF[polygon.Length];
            Poly = new Poly2D(polygon);

            Filled = true;
            Height = 0.0;
            LevelHeight = 0.0;
            Transparency = 1.0;
        }

        //==========================================================================================================================================
        public GPrism(List<Vector2d> polygon)
        {
            if (polygon.Count == 0)
                throw new System.ArgumentException("Parameter cannot be null", "polygon");
            if (polygon.Count < 3)
                throw new System.InvalidOperationException("polygon length needs to be larger than 2");

            Poly = new Poly2D(polygon.ToArray());

            Filled = true;
            Height = 0.0;
            LevelHeight = 0.0;
            Transparency = 1.0;
        }

        //==========================================================================================================================================
        public GPrism(Poly2D polygon, double height)
        {
            if (polygon.PointsFirstLoop.Count() == 0)
                throw new System.ArgumentException("Parameter cannot be null", "polygon");
            if (polygon.PointsFirstLoop.Count() < 3)
                throw new System.InvalidOperationException("polygon length needs to be larger than 2");

            Poly = new Poly2D(polygon);
            //Poly.PointsFirstLoop = new Vector2d[polygon.Count];

            Filled = true;
            Height = height;
            LevelHeight = 0.0;
            Transparency = 1.0;
        }

        //==========================================================================================================================================
        public GPrism(Poly2D polygon)
        {
            if (polygon.PointsFirstLoop.Count() == 0)
                throw new System.ArgumentException("Parameter cannot be null", "polygon");
            if (polygon.PointsFirstLoop.Count() < 3)
                throw new System.InvalidOperationException("polygon length needs to be larger than 2");

            Poly = new Poly2D(polygon);

            Filled = true;
            Height = 0.0;
            LevelHeight = 0.0;
            Transparency = 1.0;
        }

        //==========================================================================================================================================
        /// <summary>
        /// Copy constructor.
        /// </summary>
        /// <param name="source">GPrism to copy</param>
        public GPrism(GPrism source)
        {
            Poly = source.Poly.Clone();     
            _selected = source._selected; // 0 = not selected, 1 = point selected, 2 = line selected, 3 = polygon selected 
            _selectedPoint = source._selectedPoint;
            _selectedLineP1 = source._selectedLineP1;
            _selectedLineP2 = source._selectedLineP2;
            Filled = true;
            Height = source.Height;
            LevelHeight = source.LevelHeight;
            Transparency = source.Transparency;
        }

        # endregion

        //############################################################################################################################################################################
        # region Methods

        //==========================================================================================================================================
        /// <summary>
        /// Clone the GPrism.
        /// </summary>
        /// <returns>Returns exact clone of the polygon.</returns>
        public object Clone()
        {
            GPrism clone = new GPrism(this);
            return clone;
        }

        //==========================================================================================================================================
        /// <summary>
        /// Moves the <b>GPrism</b>
        /// </summary>
        /// <param name="delta">The 2d vector to move the polygon.</param>
        public override void Move(Vector2d delta)
        {
            Vector2d[] vertices;
            switch (_selected)
            {
                case 1:
                    Poly.PointsFirstLoop[_selectedPoint] += delta;
                    break;

                case 2:
                    Poly.PointsFirstLoop[_selectedLineP1] += delta;
                    Poly.PointsFirstLoop[_selectedLineP2] += delta;
                    break;

                case 3:
                    goto default;

                default:
                    vertices = Poly.PointsFirstLoop;
                    for (int i = 0; i < vertices.Length; i++)
                    {
                        vertices[i] = vertices[i] + delta;
                    }
                    break;
            }

            // -- more the scalar field --
            if (SFields != null){
                for (int i = 0; i < SFields.Length; i++)
                {
                    ScalarField curField = SFields[i];
                    curField.MoveTo(delta);
                }
            }        
        }

        //==========================================================================================================================================
        public override bool Selector(Vector2d mPoint)
        {
            _selected = 0;

            //alle Linien durchschauen
            Line2D curEdge;
            for (int i = 0; i < Poly.PointsFirstLoop.Length - 1; i++)
            {
                curEdge = new Line2D(Poly.PointsFirstLoop[i], Poly.PointsFirstLoop[i + 1]);
                if (curEdge.DistanceToPoint(mPoint) <= PickingThreshold)
                {
                    _selected = 2; //Linie = selected
                    _selectedLineP1 = i;
                    _selectedLineP2 = i + 1;
                }
            }
            curEdge = new Line2D(Poly.PointsFirstLoop[0], Poly.PointsFirstLoop[Poly.PointsFirstLoop.Length - 1]);
            if (curEdge.DistanceToPoint(mPoint) <= PickingThreshold)
            {
                _selected = 2; //Linie = selected
                _selectedLineP1 = 0;
                _selectedLineP2 = Poly.PointsFirstLoop.Length - 1;
            }

            //alle Punkte durchschauen
            for (int i = 0; i < Poly.PointsFirstLoop.Length; i++)
            {
                double distance = Math.Sqrt((Math.Pow(mPoint.X - Poly.PointsFirstLoop[i].X, 2.0) - Math.Pow(mPoint.Y - Poly.PointsFirstLoop[i].Y, 2.0)));
                if (distance <= PickingThreshold)
                {
                    _selected = 1; //Punkt = selected
                    _selectedPoint = i;
                }
            }

            if (_selected == 0)
                if (ContainsPoint(mPoint))
                    _selected = 3;

            if (_selected != 0)
                return true;
            else
                return false;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Test, if a point is located on or in a geometry.
        /// </summary>
        /// <returns>If a geometry contains a point.</returns>
        public override bool ContainsPoint(Vector2d point)
        {
            return Poly.ContainsPoint(point);
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Computes the area of the base <b>GPolygon</b>
        /// </summary>
        /// <returns>
        /// The area of the base <b>GPolygon</b>.
        /// </returns>
        public Double GetAreaBaseSurface()
        {
            return Poly.Area;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Draw the <b>GPolygon</b> to a <see cref="OpenTK.OpenGL"/> component
        /// </summary>
        public override void Draw()
        {
            double dZ = DeltaZ;
            Vector2d[] vertices = Poly.PointsFirstLoop;

            GL.Enable(EnableCap.Blend);

            if (_selected == 1) //zeichne Anfasser (Handle)
            {
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(SelectColor);
                GL.LineWidth(1 * m_zoomFactor);
                GL.Begin(PrimitiveType.Quads);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedPoint].X - HandleSize, Poly.PointsFirstLoop[_selectedPoint].Y - HandleSize, dZ);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedPoint].X - HandleSize, Poly.PointsFirstLoop[_selectedPoint].Y + HandleSize, dZ);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedPoint].X + HandleSize, Poly.PointsFirstLoop[_selectedPoint].Y + HandleSize, dZ);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedPoint].X + HandleSize, Poly.PointsFirstLoop[_selectedPoint].Y - HandleSize, dZ);
                GL.End();

                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
                GL.Color4(0, 0, 0, .5);
                GL.LineWidth(1 * m_zoomFactor);
                GL.Begin(PrimitiveType.Polygon);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedPoint].X - HandleSize, Poly.PointsFirstLoop[_selectedPoint].Y - HandleSize, dZ);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedPoint].X - HandleSize, Poly.PointsFirstLoop[_selectedPoint].Y + HandleSize, dZ);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedPoint].X + HandleSize, Poly.PointsFirstLoop[_selectedPoint].Y + HandleSize, dZ);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedPoint].X + HandleSize, Poly.PointsFirstLoop[_selectedPoint].Y - HandleSize, dZ);
                GL.End();
            }

            if (_selected == 2) //zeichne Linie
            {
                GL.LineWidth(BorderWidth);
                GL.Color4(SelectColor);
                GL.Begin(PrimitiveType.Lines);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedLineP1].X, Poly.PointsFirstLoop[_selectedLineP1].Y, dZ);
                GL.Vertex3(Poly.PointsFirstLoop[_selectedLineP2].X, Poly.PointsFirstLoop[_selectedLineP2].Y, dZ);
                GL.End();
            }

            if (_selected == 3)
            {
                GL.LineWidth(BorderWidth);
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
                GL.Color4(SelectColor);
                GL.Begin(PrimitiveType.Polygon);
                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector2d next = vertices[i];// +offset;
                    GL.Vertex3(next.X, next.Y, dZ);
                }
                GL.End();
            }


            GL.DepthMask(true);
            GL.Color4(BorderColor);
            GL.LineWidth(BorderWidth);
            GL.Begin(PrimitiveType.LineLoop);
            for (int i = 0; i < vertices.Length; i++)
                GL.Vertex3(vertices[i].X, vertices[i].Y, dZ);
            GL.End();

            if (Height == 0.0)
            {
                if (Filled)
                {

                    GL.DepthMask(true);
                    GL.Color4(FillColor.X, FillColor.Y, FillColor.Z, Transparency);
                    GL.LineWidth(BorderWidth);
                    GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                    GL.Begin(PrimitiveType.Polygon);
                    for (int i = 0; i < vertices.Length; i++)
                    {
                        Vector2d next = vertices[i];// +offset;
                        GL.Vertex3(next.X, next.Y, dZ);
                    }
                    GL.End();
                }
                return; // we end drawing procedure if the height is zero (no need to redraw everything below)
            }

            //============================ Next part only for Height != 0 ====================================================


            // --- draw planes -----------------------------------------
            if (Filled)
            {
                GL.DepthMask(true);
                GL.Color4(FillColor.X, FillColor.Y, FillColor.Z, Transparency);
                GL.LineWidth(BorderWidth);
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);

                if (SFields != null)
                {
                    GL.Enable(EnableCap.Texture2D);
                    if (_texIds == null)
                    {
                        _texIds = new int[SFields.Length];
                        for (int i = 0; i < _texIds.Length; i++)
                        {
                            _texIds[i] = GL.GenTexture();
                            GL.ActiveTexture(TextureUnit.Texture0);
                            GL.BindTexture(TextureTarget.Texture2D, _texIds[i]);
                            GL.TexImage2D<byte>(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba,
                                SFields[i].TexWidth, SFields[i].TexHeight, 0,
                                PixelFormat.Rgba, PixelType.UnsignedByte, SFields[i].Texture);
                            
                        }
                    }

                    for (int i = 0; i < SFields.Length; i++)
                    {
                        GL.ActiveTexture(TextureUnit.Texture0);
                        GL.BindTexture(TextureTarget.Texture2D, _texIds[i]);
                        GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
                        GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
                        GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToBorder);
                        GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToBorder);

                        //GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                        if (SFields[i].PointCoordinates.Length != 4)
                            GL.Begin(PrimitiveType.Triangles);
                        else
                            GL.Begin(PrimitiveType.Quads);
                        for (int j = 0; j < SFields[i].PointCoordinates.Length; j++)
                        {
                            GL.Normal3(SFields[i].Normal);
                            GL.TexCoord2(SFields[i].TextureCoordinates[j]);

                            GL.Vertex3(SFields[i].PointCoordinates[j]);
                        }
                        GL.End();
                    }
                    GL.Disable(EnableCap.Texture2D);
                }
                else
                {
                    GL.Begin(PrimitiveType.Polygon);
                    for (int i = 0; i < vertices.Length; i++)
                    {
                        Vector2d next = vertices[i];// +offset;
                        GL.Vertex3(next.X, next.Y, dZ + Height);
                    }
                    GL.End();

                    GL.Begin(PrimitiveType.Quads);
                    for (int i = 0; i < vertices.Length; i++)
                    {
                        Vector2d next = vertices[i];// +offset;
                        int j = i + 1;
                        if (j >= vertices.Length) j -= vertices.Length;
                        Vector2d next2 = vertices[j];

                        GL.Vertex3(next.X, next.Y, dZ);
                        GL.Vertex3(next.X, next.Y, dZ + Height);
                        GL.Vertex3(next2.X, next2.Y, dZ + Height);
                        GL.Vertex3(next2.X, next2.Y, dZ);
                    }
                    GL.End();
                }
            }

            // --- draw edges ------------------------------------------
            {
                GL.DepthMask(true);
                GL.Color4(BorderColor);
                    GL.LineWidth(1);
                    GL.Color4(BorderColor);
                GL.Begin(PrimitiveType.LineLoop);
                for (int i = 0; i < vertices.Length; i++)
                    GL.Vertex3(vertices[i].X, vertices[i].Y, dZ + Height);
                GL.End();

                GL.Begin(PrimitiveType.Lines);
                for (int i = 0; i < vertices.Length; i++)
                {
                    GL.Vertex3(vertices[i].X, vertices[i].Y, dZ);
                    GL.Vertex3(vertices[i].X, vertices[i].Y, dZ + Height);
                }
                GL.End();
            }

            // --- draw the levels -------------------------------------
            if (LevelHeight > 0)
            {
                GL.LineWidth(1);
                int nrLevels = Convert.ToInt16(Height / LevelHeight);
                for (int l = 1; l < nrLevels; l++)
                {
                    GL.Begin(PrimitiveType.LineLoop);
                    for (int i = 0; i < vertices.Length; i++)
                        GL.Vertex3(vertices[i].X, vertices[i].Y, dZ + LevelHeight * l);
                    GL.End();
                }
            }

        }

        //==========================================================================================================================================
        public override void GetBoundingBox(BoundingBox box)
        {
            for (int i = 0; i < Poly.PointsFirstLoop.Count(); i++)
            {
                box.AddPoint(Poly.PointsFirstLoop[i]);
            }
        }

        //==========================================================================================================================================
        public override double GetVolume()
        {
            return Poly.Area*Height;
        }


        # endregion

    }
}

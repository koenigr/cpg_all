﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GRectangle.cs 
//  Copyright (C) 2014/10/18  12:56 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using CPlan.Geometry;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CPlan.VisualObjects
{
    /// <summary>
    /// Represents a rectangular region in two-dimensional space, using <see cref="double"/>
    /// coordinates.</summary>
    [Serializable]
    public class GRectangle : Surfaces
    {
        //############################################################################################################################################################################
        # region Properties

        /// <summary> 0 if nothing is selected, 1 if Point is selected, 2 if Line is selected </summary>
        private int _selected;

        /// <summary> 0 = bottomleft, 1 = topleft, 2 = topright, 3 = bottomright </summary>
        private int _selectedPoint;
        
        /// <summary>
        /// Rect2D class.
        /// </summary>
        public Rect2D Rect { get; protected set; }

        /// <summary>
        /// Poly2D class.
        /// </summary>
        public Poly2D Poly
        {
            get { return new Poly2D(PolyPoints); }
        }

        /// <summary> Array containing the 4 points of the rectangle. Clockwise, starting with BottomLeft </summary>
        public Vector2d[] PolyPoints { get { return Rect.Points; } }

        /// <summary> List with the 4 Lines of the rectangle. </summary>
        public Line2D[] Edges { get {return Rect.Edges; } }

        /// <summary> Height of the rectangle. </summary>
        public double Height
        {
            get { return Rect.Height; }
            set { Rect.Height = value;}
        }

        /// <summary> Width of the rectangle. </summary>
        public double Width
        {
            get { return Rect.Width; }
            set { Rect.Width = value;}
        }

        /// <summary>
        /// Gets the center of the rectangle as <see cref="Vector2d"/>.
        /// </summary>
        public Vector2d Center
        {
            get
            {
                Vector2d diag = Rect.BottomRight - Rect.TopLeft;
                Vector2d diagonal = new Vector2d(diag.X, diag.Y);
                Vector2d temp = new Vector2d(Rect.TopLeft.X, Rect.TopLeft.Y);
                return (temp + 0.5 * diagonal);
            }
        }

        # endregion

        //############################################################################################################################################################################
        # region Constructors
        public GRectangle() { }
        /// <summary>
        /// Initialise a new instance of the <b>GRectangle</b> class in two-dimensional space, using <see cref="double"/> coordinates.
        /// Initializes a new GRectangle
        /// </summary>
        /// <param name="x1">The x-coordinate of the top left corner.</param>
        /// <param name="y1">The y-coordinate of the top left corner.</param>
        /// <param name="width">The x-coordinate of the bottom right corner.</param>
        /// <param name="height">The y-coordinate of the bottom right corner.</param>
        public GRectangle(double x1, double y1, double width, double height)
        {
            Rect = new Rect2D(x1, y1, width, height);
            Transparency = 1;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Initializes a new instance of the <b>GRectangle</b> object.
        /// </summary>
        /// <param name="rect">A Rect2D.</param>
        public GRectangle(Rect2D rect)
        {
            Rect = rect;
            Transparency = 1;
        }

        # endregion

        //############################################################################################################################################################################
        # region Methods

        //============================================================================================================================================================================
        /// <summary>
        /// Moves the <b>GRectangle</b>
        /// </summary>
        /// <param name="delta">The 2d vector to move the rectangle.</param>
        public override void Move(Vector2d delta)
        {
            Rect.Position += delta;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Test, if a point is located on or in a geometry.
        /// </summary>
        /// <returns>If a geometry contains a point.</returns>
        public override bool ContainsPoint(Vector2d point)
        {
            return Rect.ContainsPoint(point);
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Is not implemented for <b>GRectangle</b>.
        /// </summary>
        public override bool Selector(Vector2d mPoint)
        {
            _selected = 0;

            //alle Linien durchschauen
            Line2D curEdge;
            for (int i = 0; i < 3; i++)
            {
                curEdge = Edges[i];
                if (curEdge.DistanceToPoint(mPoint) <= PickingThreshold)
                {
                    _selected = 2; //Linie = selected
                }
            }

            // alle Punkte durchsuchen
            double distance = Math.Sqrt((Math.Pow(mPoint.X - Rect.BottomLeft.X, 2.0) - Math.Pow(mPoint.Y - Rect.BottomLeft.Y, 2.0)));
                if ( distance <= PickingThreshold ) 
                {
                    _selected = 1; //Punkt = selected
                    _selectedPoint = 0;
                }
            distance = Math.Sqrt((Math.Pow(mPoint.X - Rect.TopLeft.X, 2.0) - Math.Pow(mPoint.Y - Rect.TopLeft.Y, 2.0)));
                if ( distance <= PickingThreshold ) 
                {
                    _selected = 1; //Punkt = selected
                    _selectedPoint = 1;
                }
            distance = Math.Sqrt((Math.Pow(mPoint.X - Rect.TopRight.X, 2.0) - Math.Pow(mPoint.Y - Rect.TopRight.Y, 2.0)));
                if ( distance <= PickingThreshold ) 
                {
                    _selected = 1; //Punkt = selected
                    _selectedPoint = 2;
                }
            distance = Math.Sqrt((Math.Pow(mPoint.X - Rect.BottomRight.X, 2.0) - Math.Pow(mPoint.Y - Rect.BottomRight.Y, 2.0)));
                if ( distance <= PickingThreshold ) 
                {
                    _selected = 1; //Punkt = selected
                    _selectedPoint = 3;
                }

            if (_selected == 0)
                if (ContainsPoint(mPoint)) _selected = 3;

            if (_selected != 0)
                return true;
            
            return false;
        }
        
        //============================================================================================================================================================================
        /// <summary>
        /// Computes the area of the <b>GRectangle</b>
        /// </summary>
        /// <returns>
        /// The area of the <b>GRectangle</b>, as <see cref="double"/> value.
        /// </returns>
        public override Double GetArea()
        {
            double area;
            area = Rect.Width * Rect.Height;
            return area;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Draw the <b>GRectangle</b> object.
        /// </summary>
        public override void Draw()
        {

            if (_selected == 1)
            {
                GLine tempLine = new GLine(Edges[_selectedPoint]);

                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(SelectColor);
                GL.LineWidth(BorderWidth);
                GL.Begin(PrimitiveType.Quads);
                GL.Vertex3(tempLine.Start.Pos.X - HandleSize, tempLine.Start.Pos.Y - HandleSize, DeltaZ);
                GL.Vertex3(tempLine.Start.Pos.X - HandleSize, tempLine.Start.Pos.Y + HandleSize, DeltaZ);
                GL.Vertex3(tempLine.Start.Pos.X + HandleSize, tempLine.Start.Pos.Y + HandleSize, DeltaZ);
                GL.Vertex3(tempLine.Start.Pos.X + HandleSize, tempLine.Start.Pos.Y - HandleSize, DeltaZ);
                GL.End();
            }
            
            // Draw the filled rectangles 
            if (Filled)
            {
                GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                GL.Color4(FillColor.X, FillColor.Y, FillColor.Z, Transparency);
                GL.Begin(PrimitiveType.Quads);
                GL.Vertex3(Rect.TopLeft.X, Rect.TopLeft.Y, DeltaZ);
                GL.Vertex3(Rect.BottomRight.X, Rect.TopLeft.Y, DeltaZ);
                GL.Vertex3(Rect.BottomRight.X, Rect.BottomRight.Y, DeltaZ);
                GL.Vertex3(Rect.TopLeft.X, Rect.BottomRight.Y, DeltaZ);
                GL.End();
            }

            // Draw the outline of the rectangles   
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
            GL.LineWidth(BorderWidth);
            GL.Color4(BorderColor);
            GL.Begin(PrimitiveType.Quads);
            GL.Vertex3(Rect.TopLeft.X, Rect.TopLeft.Y, DeltaZ);
            GL.Vertex3(Rect.BottomRight.X, Rect.TopLeft.Y, DeltaZ);
            GL.Vertex3(Rect.BottomRight.X, Rect.BottomRight.Y, DeltaZ);
            GL.Vertex3(Rect.TopLeft.X, Rect.BottomRight.Y, DeltaZ);
            GL.End();     
        }

        //============================================================================================================================================================================
        public override void GetBoundingBox(BoundingBox box)
        {
            for (int i = 0; i < PolyPoints.Length; i++)
            {
                box.AddPoint(PolyPoints[i]);
            }
        }

        # endregion
    }
}

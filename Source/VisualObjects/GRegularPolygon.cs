﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = GRegularPolygon.cs 
//  Copyright (C) 2014/10/18  12:56 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Linq;
using CPlan.Geometry;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace CPlan.VisualObjects
{
    /// <summary>
    /// Represents a regular polygon in two-dimensional space, using <see cref="double"/>
    /// coordinates.</summary>
    [Serializable]
    public class GRegularPolygon : GPolygon
    {
        //############################################################################################################################################################################
        # region Properties

        /// <summary>
        /// Poly2D class.
        /// </summary>
        public Poly2D Poly { get; protected set; }

        public Vector2d[] PolyPoints { get { return Poly.PointsFirstLoop; } }

        # endregion

        //############################################################################################################################################################################
        # region Constructors

        //============================================================================================================================================================================
        /// <summary>
        /// Initialise an empty  instance of the <b>GRegularPolygon</b> class in two-dimensional space. 
        /// </summary>
        public GRegularPolygon() { }

        //============================================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>GRegularPolygon</b> class in two-dimensional space. 
        /// </summary>
        /// <param name="vertices">Vertices of the polygon.</param>
        public GRegularPolygon(Vector2d[] vertices)
        {
            Poly= new Poly2D(vertices);
            Transparency = 1;
        }

        //============================================================================================================================================================================
        //============================================================================================================================================================================
        /// <summary>
        /// Initialise a new instance of the <b>GRegularPolygon</b> class in two-dimensional space.
        /// </summary>
        /// <param name="vertices">Vertices of the polygon.</param>
        /// <param name="startPt">The starting point (offset) for the <b>GRegularPolygon</b>.</param>
        public GRegularPolygon(Vector2d[] vertices, Vector2d startPt)
        {
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i] += startPt;
            }
            Poly = new Poly2D(vertices);
            Transparency = 1;
        }

        # endregion

        //############################################################################################################################################################################
        # region Methods

        ////============================================================================================================================================================================
        ///// <summary>
        ///// Moves the <b>GRegularPolygon</b>
        ///// </summary>
        ///// <param name="delta">The 2d vector to move the regular polygon.</param>
        //public override void Move(Vector2d delta)
        //{
        //    Poly.Move(delta);
        //}

        ////============================================================================================================================================================================
        ///// <summary>
        ///// Is not implemented for <b>GRegularPolygon</b>.
        ///// </summary>
        //public override bool Selector(Vector2d mPoint)
        //{
        //    return false;
        //}

        ////============================================================================================================================================================================
        ///// <summary>
        ///// Computes the area of the <b>GRegularPolygon</b>
        ///// </summary>
        ///// <returns>
        ///// The area of the <b>GRegularPolygon</b>, as <see cref="double"/> value.
        ///// </returns>
        //public override Double GetArea()
        //{
        //    return Poly.Area;
        //}

        ////============================================================================================================================================================================
        ///// <summary>
        ///// Test, if a point is located on or in a geometry.
        ///// </summary>
        ///// <returns>If a geometry contains a point.</returns>
        //public override bool ContainsPoint(Vector2d point)
        //{
        //    return Poly.ContainsPoint(point);
        //}

        ////============================================================================================================================================================================
        ///// <summary>
        ///// Draw the object.
        ///// </summary>
        //public override void Draw()
        //{
        //    double dZ = 0;
        //    Vector2d[] vertices = Poly.PointsFirstLoop;

        //    // Draw the filled polygon 
        //    if (Filled)
        //    {
        //        GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
        //        GL.Color4(FillColor);
        //        GL.Begin(PrimitiveType.Polygon);
        //        for (int i = 0; i < vertices.Length; i++)
        //        {
        //            Vector2d next = vertices[i];// +offset;
        //            GL.Vertex3(next.X, next.Y, dZ);
        //        }
        //        GL.End();
        //    }

        //    GL.LineWidth(BorderWidth*m_zoomFactor);
        //    GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
        //    GL.Color4(BorderColor);
        //    GL.Begin(PrimitiveType.Polygon);
        //    for (int i = 0; i < vertices.Length; i++)
        //    {
        //        Vector2d next = vertices[i];// +offset;
        //        GL.Vertex3(next.X, next.Y, dZ);
        //    }
        //    GL.LineWidth(1);
        //    GL.End();
        //}

        ////============================================================================================================================================================================
        //public override void GetBoundingBox(BoundingBox box)
        //{
        //    for (int i = 0; i < Poly.PointsFirstLoop.Count(); i++)
        //    {
        //        box.AddPoint(Poly.PointsFirstLoop[i]);
        //    }

        //}

        # endregion
    }
}

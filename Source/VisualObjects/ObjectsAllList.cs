﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = ObjectsAllList.cs 
//  Copyright (C) 2014/10/18  12:56 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;

namespace CPlan.VisualObjects
{
    [Serializable]
    public class ObjectsAllList 
    {
        //############################################################################################################################################################################
        # region Properties
        /// <summary> The objects in this list are drawn.</summary>
        public ObjectsDrawList ObjectsToDraw; // 

        /// <summary> With the objects in this list an user can interact with.</summary>
        public ObjectsInteractList ObjectsToInteract;

        # endregion

        //############################################################################################################################################################################
        # region Constructors

        //============================================================================================================================================================================
        /// <summary>
        /// Initializes a new instance of the <b>ObjectsAllList</b>. 
        /// The new list initialises one instance of a ObjectsDrawList and one of a ObjectsInteractList. 
        /// </summary>
        public ObjectsAllList() 
        {
            ObjectsToDraw = new ObjectsDrawList(); // the objects in this list are drawn 
            ObjectsToInteract = new ObjectsInteractList(); // with the objects in this list an user can interact with
        }
        # endregion

        //############################################################################################################################################################################
        # region Methods

        //============================================================================================================================================================================
        /// <summary>
        /// Adds a new GeoObject to the ObjectsToDraw and/or to the ObjectsToInteract list.
        /// </summary>
        /// <param name="curObj">The GeoObject to add to the lists.</param>
        /// <param name="toDraw">A boolean variable to decide, whether the curObj hat to be added to the ObjectsToDraw list.</param>
        /// <param name="toInteract">A boolean variable to decide, whether the curObj hat to be added to the ObjectsToInteract list.</param>
        public void Add(GeoObject curObj, bool toDraw, bool toInteract)
        {
            if (toDraw) ObjectsToDraw.Add(curObj);
            if (toInteract) ObjectsToInteract.Add(curObj);
        }

        //============================================================================================================================================================================
        public void Add(GeoObject curObj)
        {
            ObjectsToDraw.Add(curObj);
            ObjectsToInteract.Add(curObj);
        }

        //============================================================================================================================================================================
        public void Remove(GeoObject curObj, bool toDraw, bool toInteract)
        {
            if (toDraw) ObjectsToDraw.Remove(curObj);
            if (toInteract) ObjectsToInteract.Remove(curObj);
        }

        //============================================================================================================================================================================
        public void Remove(GeoObject curObj)
        {
            ObjectsToDraw.Remove(curObj);
            ObjectsToInteract.Remove(curObj);
        }

        //============================================================================================================================================================================
        public void Draw()
        {
            ObjectsToDraw.Draw();
        }

        # endregion

    }
}

﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = ObjectsDrawList.cs 
//  Copyright (C) 2014/10/18  12:56 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using CPlan.Geometry;
using OpenTK;

namespace CPlan.VisualObjects
{
    /// <summary>
    /// Represents a list with elements which shall be drawn drawn to a <see cref="OpenTK.OpenGL"/> component.
    /// </summary>
    /// <remarks><para>
    /// To change the order to draw the elements, change the sequence of the elements in the list. 
    /// The last element is drawn at last. This means that this element is drawn at the top of all other elements.
    /// </para></remarks>
    [Serializable]
    public class ObjectsDrawList : List<GeoObject>
    {
        //============================================================================================================================================================================
        //############################################################################################################################################################################
        # region Constructors

        //============================================================================================================================================================================
        /// <summary>
        /// Initializes a new instance of the <b>GeoObjectList</b>. 
        /// The new list is empty. 
        /// </summary>
        public ObjectsDrawList() { }
        # endregion

        //############################################################################################################################################################################
        # region Methods 

        //============================================================================================================================================================================
        /// <summary>
        /// All objects in the <b>GeoObjectList</b> are drawn to a <see cref="OpenTK.OpenGL"/> component.
        /// </summary>
        public void Draw()
        {
            foreach (GeoObject geoObj in this)
            {
                if (geoObj.Display)
                    geoObj.Draw();
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// By releasing the right mouse button, the selected element is put to the front of all other elements 
        /// by moving it to the last position of the <b>GeoObjectList</b>.
        /// </summary>
        /// <remarks><para>
        /// The list is scanned starting with the last element. This ensures, that the element at the top is selected.
        /// </para></remarks>
        public void MouseUp(MouseEventArgs e, Vector2d P) //, ref OpenTK.GLControl glCtrl)
        {
            GeoObject selectedObject = null;
            //Vector2d P = new Vector2d();
            //P = Coordinates.GetWorldCoordinatesFromFormsCoordinates(e, ref glCtrl);
            for (int i = Count - 1; i >= 0; i--)
            {
                GeoObject go = this[i];
                Point tmp = new Point((int)Math.Round(P.X), (int)Math.Round(P.Y));
            }

            if (e.Button == MouseButtons.Right)
            {
                if (selectedObject != null)
                {
                    this.Remove(selectedObject);
                    this.Insert(this.Count, selectedObject);
                }
            }
        }

        # endregion
    }
}

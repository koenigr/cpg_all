﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = ObjectsInteractList.cs 
//  Copyright (C) 2014/10/18  12:56 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig, Sven Schneider, Christian Tonn 
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using CPlan.Geometry;
using OpenTK;

namespace CPlan.VisualObjects
{
    /// <summary>
    /// Represents a list of <see cref="GeoObject"/> with which one can interact with.
    /// </summary>
    [Serializable]
    public class ObjectsInteractList : List<GeoObject>
    {
        //############################################################################################################################################################################
        # region Private Fields

        /// <summary> Holds the last position of the selected element. </summary>
        private Vector2d m_lastMouseLocation;
        /// <summary> Holds the selected element to interact with. </summary>
        private GeoObject m_movingObject, m_highlightedObject;

        # endregion

        //############################################################################################################################################################################
        # region Constructors

        //============================================================================================================================================================================
        /// <summary>
        /// Initializes a new <b>ObjectsInteractList</b> class.
        /// </summary>
        public ObjectsInteractList() { }

        # endregion

        //############################################################################################################################################################################
        # region Methods

        //============================================================================================================================================================================
        /// <summary>
        /// By pressing the mouse button, a element at the mouse position is selected 
        /// from the <b>GeoObjectList</b>.
        /// </summary>
        /// <remarks><para>
        /// The list is scanned starting with the last element. 
        /// This ensures, that the element at the top is selected.
        /// </para></remarks>
        public void MouseDown(MouseEventArgs e, Vector2d P)//, ref GLControl glCtrl)
        {
            //Vector2d P = new Vector2d();
            //P = Coordinates.GetWorldCoordinatesFromFormsCoordinates(e, ref glCtrl);

            if (m_highlightedObject == null)
            {
                //Console.WriteLine("selecting object from " + this.Count + " buildings at " + P.X + " " + P.Y);
                for (int i = Count - 1; i >= 0; i--)
                {
                    GeoObject gO = this[i];
                    //tmp = new Point(e.X,  e.Y);
                    //if (gO.Contains(tmp) | (gO.Hit(tmp)))
                    {
                        if (gO.Selector(P))
                            m_highlightedObject = gO;
                    }
                }

                if (m_highlightedObject != null)
                {
                    Console.WriteLine("selected object");
                }
            }
            m_movingObject = m_highlightedObject;
            m_lastMouseLocation = P;

        }

        //============================================================================================================================================================================
        /// <summary>
        /// The selected element is moved with the mouse
        /// when moving the mouse with pressed left mouse button.
        /// </summary>
        /// <remarks><para>
        /// The offset vector for the selected element is calculated from 
        /// the difference of the new and the old position of the mouse
        /// </para></remarks>
        public void MouseMove(MouseEventArgs e, Vector2d P)//, ref GLControl glCtrl, ref Cursor cursor)
        {
            //Vector2d P = new Vector2d();
            //P = Coordinates.GetWorldCoordinatesFromFormsCoordinates(e, ref glCtrl);
            //int refHeight = glCtrl.Height;

            if ((m_movingObject != null) && (m_highlightedObject != null))
            {
                if (e.Button == MouseButtons.Left)
                {
                    Console.WriteLine("move objects");
                    m_movingObject.Move(new Vector2d(P.X - m_lastMouseLocation.X, P.Y - m_lastMouseLocation.Y));
                    m_lastMouseLocation = P;
                    //m_lastMouseLocation.Y = refHeight - m_lastMouseLocation.Y;
                    //cursor = Cursors.Hand;
                }
            }
            else
            {
                m_highlightedObject = null;

                for (int i = Count - 1; i >= 0; i--)
                {
                    GeoObject gO = this[i];
                    //tmp = new Point(e.X,  e.Y);
                    //if (gO.Contains(tmp) | (gO.Hit(tmp)))
                    {
                        if (gO.Selector(new Vector2d(P.X, P.Y)))
                            m_highlightedObject = gO;
                    }

                }
            }
            //glCtrl.Invalidate();
        }

        //============================================================================================================================================================================
        /// <summary>
        /// By releasing the right mouse button, the selected element is put to the front of all other elements 
        /// by moving it to the last position of the <b>GeoObjectList</b>.
        /// </summary>
        public void MouseUp(MouseEventArgs e)
        {
            
            if (e.Button == MouseButtons.Right)
            {
                if (m_movingObject != null)
                {
                    Remove(m_movingObject);
                    Insert(Count, m_movingObject);
                }
            }
            else
            {
                m_movingObject = null;
                m_highlightedObject = null;
            }
        }

        //============================================================================================================================================================================
        /// <summary>
        /// By releasing the left mouse button, the selected element is returned as <b>GeoObject</b>.
        /// </summary>
        public GeoObject FindObject(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                return m_movingObject;
            }
            else return null;
        }
        
        //============================================================================================================================================================================
        /// <summary>
        /// Test if there is a selected element.
        /// </summary>
        /// <returns> The boolean value if there is a selected element at the moment. </returns>
        public bool FoundInteractObj()
        {
            if (m_movingObject == null) return false;
            else return true;
        }

        # endregion
    }
}

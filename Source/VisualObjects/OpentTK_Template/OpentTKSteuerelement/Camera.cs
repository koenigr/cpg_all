﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using CPlan.Geometry;

namespace OpentTKView
{
    //public class Camera : Visualisation.Camera
    //{ }

    public class Camera
    {
        const double Eps6 = 0.000001;
        public CGeom3d.Vec_3d m_position;
        public float m_alpha, m_beta, m_gamma, m_scale, m_znear, m_zfar, m_zoom;
        public bool m_ortho;
        private bool m_draw_raster, m_draw_koordinatenachsen;

        public Camera()
        {
            m_position = new Vector3D(0, 0, 0);
            m_alpha = 0;
            m_beta = 0;
            m_gamma = 0;
            m_scale = 30;
            m_ortho = true;
            m_znear = 10;
            m_zfar = -10;
            m_zoom = 60;
            m_draw_raster = true;
            m_draw_koordinatenachsen = true;
        }

        private void SetPMatrix()
        {
            //array<Int32> ^ oldViewport = gcnew array<Int32>(4);                                         // Viewport auslesen
            Int32[] oldViewport = new Int32[4];
            GL.GetInteger(GetPName.Viewport, oldViewport);

            double dAspectRatio = Convert.ToDouble(oldViewport[2]) / Convert.ToDouble(oldViewport[3]);
            double dAspectRatioX = dAspectRatio;
            double dAspectRatioY = 1;

            if (!m_ortho)
            {
                // Glu.Perspective(m_zoom, dAspectRatio, m_znear, m_zfar);
                Matrix4d m_perspective = Matrix4d.CreatePerspectiveFieldOfView(Math.PI / 4.0, dAspectRatio, m_znear, m_zfar);
                GL.LoadMatrix(ref m_perspective);
            }
            else
            {
                if (dAspectRatioX < 1.0f)
                {
                    dAspectRatioX = 1;
                    dAspectRatioY = Convert.ToDouble(oldViewport[3]) / Convert.ToDouble(oldViewport[2]);
                }

                GL.Ortho(dAspectRatioX * -0.5f * m_scale, dAspectRatioX * 0.5f * m_scale,
                                  dAspectRatioY * -0.5f * m_scale, dAspectRatioY * 0.5f * m_scale, m_znear, m_zfar);
            }
            oldViewport = null;
        }

        void SetMMatrix()
        {
            GL.Rotate(m_gamma * 180 / Math.PI, 0, 0, -1);
            GL.Rotate(m_beta * 180 / Math.PI, -1, 0, 0);
            GL.Rotate(m_alpha * 180 / Math.PI, 0, -1, 0);

            GL.Translate(-m_position.x, -m_position.z, m_position.y);
        }

        void draw_koordinaten_achsensymbol()
        {
            if (!m_draw_raster)
                return;
            if (!m_draw_koordinatenachsen)
                return;

            float dx = 0, dy = 0, dz = 0;

            //Pfeilspitze rot
            //		IntPtr GLU = Glu.NewQuadric();
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
            GL.PushMatrix();
            GL.LineWidth(1);
            GL.Color3(1.0f, 0.0f, 0.0f);
            GL.Translate(dx, dz, -dy);
            GL.Rotate(90.0f, 0.0f, 1.0f, 0.0f);
            GL.Translate(0.0f, 0.0f, 0.85f);
            //		Glu.Cylinder(GLU,0.05,0,0.15,16,1);
            GL.PopMatrix();

            //Pfeilspitze grün
            GL.PushMatrix();
            GL.LineWidth(1);
            GL.Color3(0.0f, 1.0f, 0.0f);
            GL.Translate(dx, dz, -dy);
            GL.Rotate(90.0f, -1.0f, 0.0f, 0.0f);
            GL.Translate(0.0f, 0.0f, 0.85f);
            //		Glu.Cylinder(GLU,0.05,0,0.15,16,1);
            GL.PopMatrix();

            //Pfeilspitze blau
            GL.PushMatrix();
            GL.LineWidth(1);
            GL.Color3(0.0f, 0.0f, 1.0f);
            GL.Translate(dx, dz, -dy);
            GL.Rotate(180.0f, 1.0f, 0.0f, 0.0f);
            GL.Translate(0.0f, 0.0f, 0.85f);
            //		Glu.Cylinder(GLU,0.05,0,0.15,16,1);
            GL.PopMatrix();
            //		Glu.DeleteQuadric(GLU);

            GL.Begin(BeginMode.Lines);
            // red x axis
            GL.LineWidth(1);
            GL.Color3(1.0f, 0.0f, 0.0f);
            GL.Vertex3(0.0f + dx, 0.0f + dz, 0.0f - dy);
            GL.Vertex3(1.0f + dx, 0.0f + dz, 0.0f - dy);
            // green z axis
            GL.Color3(0.0f, 1.0f, 0.0f);
            GL.Vertex3(0.0f + dx, 0.0f + dz, 0.0f - dy);
            GL.Vertex3(0.0f + dx, 1.0f + dz, 0.0f - dy);
            // blue y axis
            GL.Color3(0.0f, 0.0f, 1.0f);
            GL.Vertex3(0.0f + dx, 0.0f + dz, 0.0f - dy);
            GL.Vertex3(0.0f + dx, 0.0f + dz, -1.0f - dy);
            GL.End();
        }

        void draw_koordinaten_lines(float raster_x, float raster_y, float raster_fak, float raster_size)
        {
            if (!m_draw_raster)
                return;

            float dx = 0, dy = 0, dz = 0;

            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
            GL.Begin(BeginMode.Lines);
            for (float s1 = -raster_size; s1 <= raster_size; s1 += raster_fak)
            {
                int x = (int)System.Math.Floor((s1 + raster_x) / raster_fak + 0.5f);
                int y = (int)System.Math.Floor((s1 - raster_y) / raster_fak + 0.5f);

                GL.LineWidth(1);
                if ((x % 10) == 0)
                    GL.Color4(1.0f, 1.0f, 1.0f, 1.0f);
                else
                    GL.Color4(1.0f, 1.0f, 1.0f, 0.3f);
                if (System.Math.Abs(s1 + raster_x) < 0.000001f) // (s1+raster_x == 0)
                {
                    GL.Vertex3(s1 + raster_x + dx, 0.0f + dz, -raster_size - raster_y - dy);
                    GL.Vertex3(s1 + raster_x + dx, 0.0f + dz, -1.0f - dy);
                    GL.Vertex3(s1 + raster_x + dx, 0.0f + dz, 0.0f - dy);
                    GL.Vertex3(s1 + raster_x + dx, 0.0f + dz, raster_size - raster_y - dy);
                }
                else
                {
                    GL.Vertex3(s1 + raster_x + dx, 0.0f + dz, -raster_size - raster_y - dy);
                    GL.Vertex3(s1 + raster_x + dx, 0.0f + dz, raster_size - raster_y - dy);
                }

                if ((y % 10) == 0)
                    GL.Color4(1.0f, 1.0f, 1.0f, 1.0f);
                else
                    GL.Color4(1.0f, 1.0f, 1.0f, 0.3f);
                if (System.Math.Abs(s1 - raster_y) < 0.000001f) // (s1-raster_y == 0)
                {
                    GL.Vertex3(-raster_size + raster_x + dx, 0.0f + dz, s1 - raster_y - dy);
                    GL.Vertex3(0.0f + dx, 0.0f + dz, s1 - raster_y - dy);
                    GL.Vertex3(1.0f + dx, 0.0f + dz, s1 - raster_y - dy);
                    GL.Vertex3(raster_size + raster_x + dx, 0.0f + dz, s1 - raster_y - dy);
                }
                else
                {
                    GL.Vertex3(-raster_size + raster_x + dx, 0.0f + dz, s1 - raster_y - dy);
                    GL.Vertex3(raster_size + raster_x + dx, 0.0f + dz, s1 - raster_y - dy);
                }
            }
            GL.End();
        }

        void draw_koordinaten_greybackground(float raster_x, float raster_y, float raster_size)
        {
            float dx = 0, dy = 0, dz = 0;
            GL.Begin(BeginMode.Quads);
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
            //GL.Color4(Color4.LightGray);
            GL.LineWidth(1);
            GL.Color4((Single)0, (Single)0, (Single)0, (Single)0.1f);
            GL.Vertex3(-raster_size + raster_x + dx, 0.0f + dz, raster_size - raster_y - dy);
            GL.Vertex3(-raster_size + raster_x + dx, 0.0f + dz, -raster_size - raster_y - dy);
            GL.Vertex3(raster_size + raster_x + dx, 0.0f + dz, -raster_size - raster_y - dy);
            GL.Vertex3(raster_size + raster_x + dx, 0.0f + dz, raster_size - raster_y - dy);
            GL.End();
        }

        public void DrawKoordinatenkreuz()
        {
            if (!m_draw_raster)
                return;

            Int32[] oldViewport = new Int32[4];                                       // Viewport auslesen
            GL.GetInteger(GetPName.Viewport, oldViewport);

            float raster_size = 100, raster_fak = 1;
            float raster_x = 0, raster_y = 0;
            double mdx, mdy, md, mdmax;

            mdx = oldViewport[2];
            mdy = oldViewport[3];
            if (mdx < mdy)
            {
                md = mdx;
                mdmax = mdy;
            }
            else
            {
                md = mdy;
                mdmax = mdx;
            }

            //fak sollte auf 10er Potenzen gerundet werden

            raster_fak = (float)System.Math.Floor(System.Math.Log10(System.Math.Abs(m_scale * 0.2) * 0.5));
            raster_fak = (float)System.Math.Pow(10.0, (double)raster_fak);
            if ((!m_ortho) && (raster_fak < 1.0f))
                raster_fak = 1;
            raster_size = raster_fak * 500;

            raster_x = ((float)((int)(m_position.x / raster_fak))) * raster_fak;
            raster_y = ((float)((int)(m_position.y / raster_fak))) * raster_fak;

            GL.Enable(EnableCap.Blend);
            GL.DepthMask(false);
            GL.DepthRange(-500, 500);
            GL.DepthFunc(DepthFunction.Always);

            //Perspektive
            GL.DepthMask(true);
            GL.DepthFunc(DepthFunction.Less);
            draw_koordinaten_achsensymbol();
            //GL.DepthFunc(OpenGL.DepthFunction.Always);
            GL.DepthMask(false);
            draw_koordinaten_greybackground(raster_x, raster_y, raster_size);
            draw_koordinaten_lines(raster_x, raster_y, raster_fak, raster_size);

            GL.DepthFunc(DepthFunction.Less);
            GL.DepthMask(true);
        }

        public void SetOpenGLMatrixes()
        {
            GL.MatrixMode(MatrixMode.Projection);                                       // Projektionsmatrix einrichten

            GL.LoadIdentity();

            SetPMatrix();

            GL.MatrixMode(MatrixMode.Modelview);                                        // Modellmatrix vom Zeitpunkt des Renderns

            GL.LoadIdentity();                                                  //    laden

            SetMMatrix();
        }

        private float GetAngleV()
        {
            return m_beta;
        }

        private float GetAngleH()
        {
            return m_alpha;
        }

        private Vector3D GetCameraPos()
        {
            return new Vector3D(m_position);
        }

        public Vector3D GetBlickVector()
        {
            Vector3D zw = new Vector3D(), blick = new Vector3D();
            float angle;

            zw.x = 0; zw.y = 1; zw.z = 0;
            blick.x = zw.x;
            blick.y = zw.y;
            blick.z = zw.z;

            angle = GetAngleV();
            blick.y = (zw.y * Math.Cos(angle) - zw.z * Math.Sin(angle));
            blick.z = (zw.z * Math.Cos(angle) + zw.y * Math.Sin(angle));

            zw.x = blick.x;
            zw.y = blick.y;
            zw.z = blick.z;

            angle = GetAngleH();
            blick.x = (zw.x * Math.Cos(angle) - zw.y * Math.Sin(angle));
            blick.y = (zw.y * Math.Cos(angle) + zw.x * Math.Sin(angle));

            return blick;
        }

        public Vector3D GetBlickVectorPtn()
        {
            return new Vector3D(GetBlickVector());
        }

        //public void GetGLCoordinates(int x, int y, int width, int height, ref CGeom3d.Vec_3d blick_start, ref CGeom3d.Vec_3d blick_vec, ref CGeom3d.Vec_3d vecx, ref CGeom3d.Vec_3d vecy)
        public void GetGLCoordinates(int x, int y, int width, int height,  CGeom3d.Vec_3d blick_start,  CGeom3d.Vec_3d blick_vec,  CGeom3d.Vec_3d vecx,  CGeom3d.Vec_3d vecy)
        {
            if (m_ortho)
                return;

            //Bug
            y = height - y;

            double rx, ry;
            CGeom3d.Vec_3d pos = new Vector3D(), blick = new Vector3D(), lot = new Vector3D(), zw = new Vector3D();
            double angle, zoom;

            pos = new Vector3D(m_position);
            blick_start.x = m_position.x;
            blick_start.y = m_position.y;
            blick_start.z = m_position.z;
            blick = GetBlickVector();
            zoom = m_zoom / 180.0 * Math.PI;

            if ((Math.Abs(blick.x) > Eps6) || (Math.Abs(blick.y) > Eps6))
            {
                lot.x = 0.0;
                lot.y = 0.0;
                lot.z = 1.0;
            }
            else
            {
                angle = GetAngleV();
                if (angle < 0)
                {
                    zw.x = 0.0;
                    zw.y = 1.0;
                    zw.z = 0.0;
                }
                else
                {
                    zw.x = 0.0;
                    zw.y = -1.0;
                    zw.z = 0.0;
                }
                lot = zw;
                angle = GetAngleH();
                lot.x = (zw.x * Math.Cos(angle) - zw.y * Math.Sin(angle));
                lot.y = (zw.y * Math.Cos(angle) + zw.x * Math.Sin(angle));
            }
            vecx = lot.amul(blick);
            vecy = vecx.amul(blick);
            if (vecx.Value() < Eps6) return;
            if (vecy.Value() < Eps6) return;
            vecx /= vecx.Value();
            vecy /= vecy.Value();

            rx = x;
            ry = y;

            //relativ zum Mittelpunkt des Views(nr)
            rx = (rx - ((double)width / 2.0f));
            ry = (ry - ((double)height / 2.0f));

            double brennweite, brennweite_y;
            brennweite = (height / 2.0) / Math.Tan(zoom / 2.0);
            brennweite_y = Math.Sqrt(brennweite * brennweite + rx * rx);

            rx = Math.Atan(rx / brennweite);
            ry = Math.Atan(ry / brennweite_y);

            //Abweichungswinkel von der Blickrichtung
            blick_vec.x = blick.x;
            blick_vec.y = blick.y;
            blick_vec.z = blick.z;
            CGeom3d.Rot_Achse_3d( blick_vec,  vecx, Math.Sin(-ry), Math.Cos(-ry));
            CGeom3d.Rot_Achse_3d( blick_vec,  vecy, Math.Sin(rx), Math.Cos(rx));
        }

        public CGeom3d.Vec_3d GetClickRay(int x, int y, int width, int height)
        {
            CGeom3d.Vec_3d start = new CGeom3d.Vec_3d();
            CGeom3d.Vec_3d blick = new CGeom3d.Vec_3d();
            CGeom3d.Vec_3d vecx = new CGeom3d.Vec_3d();
            CGeom3d.Vec_3d vecy = new CGeom3d.Vec_3d();
            GetGLCoordinates(x, y, width, height,  start,  blick,  vecx,  vecy);
            //GetGLCoordinates(x, y, width, height, ref start, ref blick, ref vecx, ref vecy);

            return blick;
        }



        public CGeom3d.Vec_3d GetClickRayPtn(int x, int y, int width, int height)
        {
            return new Vector3D(GetClickRay(x, y, width, height));
        }

        //public Vector3D GetPointXYPtn(int x, int y, int width, int height)
        //{
        //    Vector3D erg = new Vector3D(0, 0, 0);

        //    if (m_ortho)
        //    {
        //        //Parallel Projektion
        //        double dAspectRatio = (double)width / (double)height;
        //        double dAspectRatioX = dAspectRatio;
        //        double dAspectRatioY = 1;

        //        if (dAspectRatio < 1.0)
        //        {
        //            dAspectRatioX = 1;
        //            dAspectRatioY = 1.0 / dAspectRatio;
        //        }

        //        erg.x = (x - width / 2.0) / width * m_scale * dAspectRatioX + m_position.x;
        //        erg.y = ((height - y) - height / 2.0) / height * m_scale * dAspectRatioY + m_position.y;
        //        erg.z = 0;
        //    }
        //    else
        //    {
        //        //Perspektive
        //        CGeom3d.Vec_3d ray = GetClickRay(x, y, width, height);
        //        Vector3D cut_point = new Vector3D(0, 0, 0);
        //        CGeom3d.Line_3d line = new CGeom3d.Line_3d();
        //        CGeom3d.Plane_3d plane_xy = new CGeom3d.Plane_3d();

        //        CGeom3d.Plane_PN_3d(new Vector3D(0, 0, 0), new Vector3D(0, 0, 1), plane_xy);
        //        CGeom3d.Line_VP_3d(ray, m_position, line);
        //        CGeom3d.Point_LS_3d(line, plane_xy, cut_point);

        //        if (CGeom3d.Err_3d != 1)
        //        {
        //            return null;
        //        }

        //        Vector3D blick = GetBlickVector();
        //        CGeom3d.Plane_3d plane_camera = new CGeom3d.Plane_3d();
        //        CGeom3d.Plane_PN_3d(m_position, blick, plane_camera);

        //        bool valid = (CGeom3d.Dist_PS_3d(cut_point, plane_camera) > 0);

        //        if (valid == true)
        //            erg = cut_point;
        //        else
        //            erg = null;
        //    }

        //    return erg;
        //}

        public CGeom3d.Vec_3d GetPointXY(int x, int y, int width, int height, ref bool valid)
        {
            CGeom3d.Vec_3d erg = new CGeom3d.Vec_3d(0, 0, 0);
            valid = false;

            if (m_ortho)
            {
                //Parallel Projektion
                double dAspectRatio = (double)width / (double)height;
                double dAspectRatioX = dAspectRatio;
                double dAspectRatioY = 1;

                if (dAspectRatio < 1.0)
                {
                    dAspectRatioX = 1;
                    dAspectRatioY = 1.0 / dAspectRatio;
                }

                erg.x = (x - width / 2.0) / width * m_scale * dAspectRatioX + m_position.x;
                erg.y = ((height - y) - height / 2.0) / height * m_scale * dAspectRatioY + m_position.y;
                erg.z = 0;
                valid = true;
            }
            else
            {
                //Perspektive
                CGeom3d.Vec_3d ray = GetClickRay(x, y, width, height);
                CGeom3d.Vec_3d cut_point = new CGeom3d.Vec_3d();
                CGeom3d.Line_3d line = new CGeom3d.Line_3d();
                CGeom3d.Plane_3d plane_xy = new CGeom3d.Plane_3d();

                //CGeom3d::Plane_PN_3d(%CGeom3d::Vec_3d(0, 0, 0), %CGeom3d::Vec_3d(0, 0, 1), %plane_xy);
                //GeoAlgorithms3D.Plane_PN_3d( new Vector3D(0, 0, 0),  new Vector3D(0, 0, 1), ref plane_xy);
                CGeom3d.Plane_PN_3d(new Vector3D(0, 0, 0), new Vector3D(0, 0, 1), plane_xy);
                CGeom3d.Line_VP_3d(ray, m_position, line);
                //GeoAlgorithms3D.Line_VP_3d(ref ray, ref m_position, ref line);
                CGeom3d.Point_LS_3d(line, plane_xy, cut_point);
                //GeoAlgorithms3D.Point_LS_3d(ref line, ref plane_xy, ref cut_point);

                if (CGeom3d.Err_3d != 1)
                {
                    valid = false;
                    return erg;
                }

                Vector3D blick = GetBlickVector();
                CGeom3d.Plane_3d plane_camera = new CGeom3d.Plane_3d();
                CGeom3d.Plane_PN_3d(m_position, blick, plane_camera);
                //GeoAlgorithms3D.Plane_PN_3d(m_position, blick, ref plane_camera);

                valid = (CGeom3d.Dist_PS_3d( cut_point,  plane_camera) > 0);
                erg = cut_point;
            }

            return erg;
        }
    }
}

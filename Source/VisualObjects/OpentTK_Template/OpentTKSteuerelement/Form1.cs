﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using CPlan.Geometry;

namespace OpentTKView
{

    public partial class Form1 : Form
    {
        private ViewControl ViewControl1;
        GPoint testPt;
        //===================================================================================================================================================================
        public Form1()
        {
            InitializeComponent();

            // --- this needs to be copied for a new glcontrol ----------------------------------------------------
            ViewControl1 = new ViewControl(glControl1); // --- new instance for glControl methods ---
            this.glControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseMove);
            this.glControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseDown);
            this.glControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseUp);
            this.glControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.glControl1_Paint);
            // ----------------------------------------------------------------------------------------------------

            // --- test objects ---
            // --- punkt ---
             testPt = new GPoint(2, 2);
            ViewControl1.AllObjectsList.Add(testPt, true, true);

            GPoint testPt2 = new GPoint(12, 8);
            ViewControl1.AllObjectsList.Add(testPt2, true, true);

            // --- linie ---
            GLine testLine = new GLine(0.5, 1, 25, 10);
            testLine.Width = 2;
            testLine.SetColor(Color.Black, 1f);
            ViewControl1.AllObjectsList.Add(testLine, true, true);

            GLine testLine2 = new GLine( ref testPt, ref testPt2);
            testLine2.Width = 2;
            testLine2.SetColor(Color.Black, 1f);
            ViewControl1.AllObjectsList.Add(testLine2, true, true);

            // --- rechteck ---
            GRectangle testRect = new GRectangle(5, 1, 3, 5);
            testRect.Filled = true;
            testRect.BorderWidth = 2;
            ViewControl1.AllObjectsList.Add(testRect, true, false);

            // --- polygon ---
            Vector2D[] polyPts = new Vector2D[3];
            polyPts[0] = new Vector2D(15, 35);
            polyPts[1] = new Vector2D(18, 25);
            polyPts[2] = new Vector2D(10, 30);
            GPolygon testPolygonA = new GPolygon(polyPts);
            testPolygonA.BorderWidth = 2;
            testPolygonA.Filled = true;
            testPolygonA.FillColor = new Vector4(0.5f, 0.1f, 0.2f, 0.5f);
            ViewControl1.AllObjectsList.Add(testPolygonA, true, true);

            // --- zoom all ---
            ViewControl1.ZoomAll();
        }


        //===================================================================================================================================================================
        //========= Paint Event =========
        //===================================================================================================================================================================
        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            // -- enter pait istruction here --
            ViewControl1.AllObjectsList.Draw();

            // -- end of paint instructions ---
            //GL.PopMatrix();
            glControl1.SwapBuffers();
        }

        //===================================================================================================================================================================
        //========= Mouse Controls =========
        //===================================================================================================================================================================
        private void glControl1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {

        }

        //===================================================================================================================================================================
        private void glControl1_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            //CGeom3d.Vec_3d point_xy = ViewControl1.Camera.GetPointXYPtn(e.X, e.Y, glControl1.Width, glControl1.Height);
            //if (point_xy == null) return;
            //testPt.Pos.X = point_xy.x;
            //testPt.Pos.Y = point_xy.y; 
        }

        //===================================================================================================================================================================
        private void glControl1_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {

        }

   
        //===================================================================================================================================================================
        //========= Button Controls =========
        //===================================================================================================================================================================
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            ViewControl1.ZoomAll();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            ViewControl1.CameraChange();
        }


        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

    }
}

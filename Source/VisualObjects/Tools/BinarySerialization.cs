﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = BinarySerialization.cs 
//  Copyright (C) 2014/10/18  12:57 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace CPlan.VisualObjects
{
    public class BinarySerialization
    {
        //============================================================================================================================================================================
        public static void SerializeVisualObjects(ObjectsDrawList allObjects, string fileName)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = new FileStream(Application.StartupPath + "\\" + fileName + ".cplan", FileMode.Create);
            bf.Serialize(fs, allObjects);
            fs.Close();
        }

        //============================================================================================================================================================================
        public static ObjectsDrawList DeserializeVisualObjects(string fileName)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = new FileStream(Application.StartupPath + "\\" + fileName + ".cplan", FileMode.Open);
            ObjectsDrawList Result = (ObjectsDrawList)bf.Deserialize(fs);
            fs.Close();
            return Result;
        }

        //============================================================================================================================================================================
        public static void SerializeObjects(List<object> objects, string fileName)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = new FileStream(fileName, FileMode.Create); 
            bf.Serialize(fs, objects);
            fs.Close();
        }

        //============================================================================================================================================================================
        public static List<object> DeserializeObjects(string fileName)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = new FileStream(fileName, FileMode.Open); 
            List<object> Result = (List<object>)bf.Deserialize(fs);
            fs.Close();
            return Result;
        }

    }
}

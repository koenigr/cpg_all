﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = ConvertColor.cs 
//  Copyright (C) 2014/10/18  12:57 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using CPlan.Geometry;
using OpenTK;
using Tektosyne.Geometry;

namespace CPlan.VisualObjects
{
    /// <summary>
    /// Provides conversions between color types and calculates color values.
    /// </summary>
    public static class ConvertColor
    {
        //============================================================================================================================================================================
        /// <summary>
        /// Create a OpenTK.Color4 from a Color value.
        /// </summary>
        /// <param name="inVal">The input value to be transformed.</param>
        public static OpenTK.Graphics.Color4  CreateOpenTKColor4 (Color inVal)
        {
            // --- new color ---
            double red = inVal.R/255f;
            double green = inVal.G/255f;
            double blue = inVal.B/255f;
            OpenTK.Graphics.Color4 newColor = new OpenTK.Graphics.Color4((float)red, (float)green, (float)blue, 1);
            return newColor;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Create a  Color value from a OpenTK.Color4. 
        /// </summary>
        /// <param name="inVal">The input value to be transformed.</param>
        public static Color CreateColor (OpenTK.Graphics.Color4 inVal)
        {
            Color newColor = new Color();// ((float)red, (float)green, (float)blue);
            newColor = Color.FromArgb(inVal.ToArgb());
            return newColor;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Create a vector with false-colors
        /// </summary>
        /// <param name="inVal">The input value to transform</param>
        public static Vector4 CreateFFColor4(int inVal)
        {
            // --- old color ---
            Color oldColor = Color.FromArgb(inVal, inVal, inVal);

            // --- new color [1,1] ---
            double red = Math.Sin(inVal * 2 * Math.PI / 255d - Math.PI);
            double green = Math.Sin(inVal * 2 * Math.PI / 255d - Math.PI / 2);
            double blue = Math.Sin(inVal * 2 * Math.PI / 255d);

            // --- new color [0,1] ---
            red = (red + 1) * 0.5;
            green = (green + 1) * 0.5;
            blue = (blue + 1) * 0.5;

            Vector4 newColor = new Vector4((float)red, (float)green, (float)blue, 1);

            return newColor;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Create a array of vectors with false-colors
        /// </summary>
        /// <param name="values">The input value to transform</param>
        public static Vector4[,] CreateFFColor4(float[,] values)
        {
            int width = values.GetLength(0), height = values.GetLength(1);
            Vector4[,] ColorGrid = new Vector4[width, height];
            Vector4 curVect;
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    float val = values[x, y];
                    int cutToBlue = 30; // lower values resut in purple colors comming from blue
                    int cutToRed = 195; // higher values result in purple colors comming form red
                    int transformedValue = cutToBlue + (int)((val) * cutToRed);
                    curVect = CreateFFColor4(transformedValue);
                    ColorGrid[x, y] = curVect;
                }
            }

            return ColorGrid;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Create a array of vectors with false-colors
        /// </summary>
        /// <param name="values">The input value to transform</param>
        public static Vector4[] CreateFFColor4(Single[] values)
        {          
            int size = values.Length;
            Vector4[] ColorGrid = new Vector4[size];
            Vector4 curVect = new Vector4(0.2f, 0.2f, 0.2f, 1);
            for (int x = 0; x < size; x++)
            {
                float val = 0;
                if (Single.IsNaN(values[x]))
                {
                    ColorGrid[x] = curVect;
                    continue;
                }
                val = values[x];
                int cutToBlue = 30; // lower values resut in purple colors comming from blue
                int cutToRed = 195; // higher values result in purple colors comming form red
                int transformedValue = cutToBlue + (int)((val) * cutToRed);
                curVect = CreateFFColor4(transformedValue);
                ColorGrid[x] = curVect;
            }
            return ColorGrid;
        }

        //==============================================================================================
        /// <summary>
        /// Create vectors with false-colors and return them as dictionary
        /// </summary>
        /// <param name="values">The input value to transform</param>
        public static Dictionary<PointD, Vector4> CreateFFColor4(Dictionary<PointD, Single> values)
        {
            Dictionary<PointD, Vector4> newDict = new Dictionary<PointD, Vector4>();
            Vector4 curVect;
            foreach (KeyValuePair<PointD, Single> kvp in values)
            {
                float val = kvp.Value;
                int cutToBlue = 30; // lower values resut in purple colors comming from blue
                int cutToRed = 195; // higher values result in purple colors comming form red
                int transformedValue = cutToBlue + (int)((val) * cutToRed);
                curVect = CreateFFColor4(transformedValue);
                newDict.Add(kvp.Key, curVect);
            }
            return newDict;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Create a vector from a color value
        /// </summary>
        /// <param name="inVal">The input value to transform</param>
        public static Vector4 CreateColor4(Color inVal)
        {
            // --- new color ---
            double red = inVal.R/255f;
            double green = inVal.G/255f;
            double blue = inVal.B/255f;

            Vector4 newColor = new Vector4((float)red, (float)green, (float)blue, 1);

            return newColor;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Create a  color value from a vector 
        /// </summary>
        /// <param name="inVal">The input value to transform</param>
        public static Color CreateColor4(Vector4 inVal)
        {
            Color newColor = new Color();// ((float)red, (float)green, (float)blue);
            newColor = Color.FromArgb(Convert.ToInt16(inVal.W * 255), Convert.ToInt16(inVal.X * 255), Convert.ToInt16(inVal.Y * 255), Convert.ToInt16(inVal.Z * 255));

            return newColor;
        }


        //============================================================================================================================================================================
        //============================================================================================================================================================================
        // Vector3d
        //============================================================================================================================================================================
        /// <summary>
        /// Create a vector with false-colors
        /// </summary>
        /// <param name="inVal">The input value to transform</param>
        public static Vector3d CreateFFColor(int inVal)
        {
            // --- old color ---
            if (inVal > 255)
                inVal = 255;
            if (inVal < 0)
                inVal = 0;
            Color oldColor = Color.FromArgb(inVal, inVal, inVal);

            // --- new color [1,1] ---
            double red = Math.Sin(inVal * 2 * Math.PI / 255d - Math.PI);
            double green = Math.Sin(inVal * 2 * Math.PI / 255d - Math.PI / 2);
            double blue = Math.Sin(inVal * 2 * Math.PI / 255d);

            // --- new color [0,1] ---
            red = (red + 1) * 0.5;
            green = (green + 1) * 0.5;
            blue = (blue + 1) * 0.5;

            Vector3d newColor = new Vector3d((float)red, (float)green, (float)blue);

            return newColor;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Create a array of vectors with false-colors
        /// </summary>
        /// <param name="values">The input value to transform</param>
        public static Vector3d[,] CreateFFColor(double[,] values)
        {
            int width = values.GetLength(0), height = values.GetLength(1);
            Vector3d[,] ColorGrid = new Vector3d[width, height];
            Vector3d curVect;
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    double val = values[x, y];
                    int cutToBlue = 30; // lower values resut in purple colors comming from blue
                    int cutToRed = 195; // higher values result in purple colors comming form red
                    int transformedValue = cutToBlue + (int)((val) * cutToRed);
                    curVect = CreateFFColor(transformedValue);
                    ColorGrid[x, y] = curVect;
                }
            }

            return ColorGrid;
        }

        //============================================================================================================================================================================
        public static Vector3d[] CreateFFColor1D(double[] values)
        {
            Vector3d[] ColorGrid = new Vector3d[values.Length];
            Vector3d curVect;
            for (int x = 0; x < values.Length; x++)
            {
                if (Double.IsNaN(values[x]))
                    ColorGrid[x] = new Vector3d(1.0, 1.0, 1.0);
                else
                {
                    double val = values[x];
                    int cutToBlue = 30; // lower values resut in purple colors comming from blue
                    int cutToRed = 195; // higher values result in purple colors comming form red
                    int transformedValue = cutToBlue + (int)((val) * cutToRed);
                    curVect = CreateFFColor(transformedValue);
                    ColorGrid[x] = curVect;
                }
            }

            return ColorGrid;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Create a array of vectors with false-colors
        /// </summary>
        /// <param name="values">The input value to transform</param>
        public static Vector3d[] CreateFFColor(Single[] values)
        {
            int size = values.Length;
            Vector3d[] ColorGrid = new Vector3d[size];
            Vector3d curVect;
            for (int x = 0; x < size; x++)
            {
                float val = values[x];
                int cutToBlue = 30; // lower values resut in purple colors comming from blue
                int cutToRed = 195; // higher values result in purple colors comming form red
                int transformedValue = cutToBlue + (int)((val) * cutToRed);
                curVect = CreateFFColor(transformedValue);
                ColorGrid[x] = curVect;
            }
            return ColorGrid;
        }

        //==============================================================================================
        /// <summary>
        /// Create vectors with false-colors and return them as dictionary
        /// </summary>
        /// <param name="values">The input value to transform</param>
        public static Dictionary<PointD, Vector3d> CreateFFColor(Dictionary<PointD, Single> values)
        {
            Dictionary<PointD, Vector3d> newDict = new Dictionary<PointD, Vector3d>();
            Vector3d curVect;
            foreach (KeyValuePair<PointD, Single> kvp in values)
            {
                float val = kvp.Value;
                int cutToBlue = 30; // lower values resut in purple colors comming from blue
                int cutToRed = 195; // higher values result in purple colors comming form red
                int transformedValue = cutToBlue + (int)((val) * cutToRed);
                curVect = CreateFFColor(transformedValue);
                newDict.Add(kvp.Key, curVect);
            }
            return newDict;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Create a vector from a color value
        /// </summary>
        /// <param name="inVal">The input value to transform</param>
        public static Vector4 CreateFFColor(Color inVal)
        {
            // --- new color ---
            double red = inVal.R / 255f;
            double green = inVal.G / 255f;
            double blue = inVal.B / 255f;

            Vector4 newColor = new Vector4((float)red, (float)green, (float)blue, 1);

            return newColor;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// Create a  color value from a vector 
        /// </summary>
        /// <param name="inVal">The input value to transform</param>
        public static Color CreateColor(Vector4 inVal)
        {
            Color newColor = new Color();// ((float)red, (float)green, (float)blue);
            newColor = Color.FromArgb(Convert.ToInt16(inVal.W * 255), Convert.ToInt16(inVal.X * 255), Convert.ToInt16(inVal.Y * 255), Convert.ToInt16(inVal.Z * 255));

            return newColor;
        }

    }
}

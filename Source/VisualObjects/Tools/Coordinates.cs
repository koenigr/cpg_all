﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = Coordinates.cs 
//  Copyright (C) 2014/10/18  12:57 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Drawing;
using System.Windows.Forms;
using CPlan.Geometry;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CPlan.VisualObjects
{
    [Serializable]
    static public class Coordinates
    {
        //============================================================================================================================================================================
        //delivers the <b>world coordinate</b> that is below the cursor position <b>e</b>
        static public Vector2d GetWorldCoordinatesFromFormsCoordinates(MouseEventArgs e, ref GLControl glCtrl)
        {
            //to let OpenTK work with the given glCtrl:
            glCtrl.MakeCurrent();
            //setting MousePosition:
            Vector2 n = new Vector2(e.X, e.Y);
            Matrix4 proMatr = new Matrix4();
            //reading ProjectionMatrix values:
            GL.GetFloat(GetPName.ProjectionMatrix, out proMatr);
            Matrix4 viewMatr = new Matrix4();
            //reading ModelViewMatrix:
            GL.GetFloat(GetPName.ModelviewMatrix, out viewMatr);
            //calculate the coordinates under the courser
            Vector4 Pos = new Vector4(UnProject(ref viewMatr, ref proMatr, glCtrl.Size, ref n));
            Vector2d P = new Vector2d(Pos.X, Pos.Y);
            return P;
            
        }

        //============================================================================================================================================================================
        static public Vector2d GetWorldCoordinatesFromFormsCoordinates(Vector2d _p, ref GLControl glCtrl)
        {
            //to let OpenTK work with the given glCtrl:
            glCtrl.MakeCurrent();
            //setting MousePosition:
            Vector2 n = new Vector2((float)(Math.Round(_p.X)), (float)Math.Round( _p.Y));
            Matrix4 proMatr = new Matrix4();
            //reading ProjectionMatrix values:
            GL.GetFloat(GetPName.ProjectionMatrix, out proMatr);
            Matrix4 viewMatr = new Matrix4();
            //reading ModelViewMatrix:
            GL.GetFloat(GetPName.ModelviewMatrix, out viewMatr);
            //calculate the coordinates under the courser
            Vector4 Pos = new Vector4(UnProject(ref viewMatr, ref proMatr, glCtrl.Size, ref n));
            Vector2d P = new Vector2d(Pos.X, Pos.Y);
            return P;

        }

        //============================================================================================================================================================================
        static public System.Drawing.PointF GetWorldCoordinatesFromFormsCoordinates(System.Drawing.PointF _p, ref GLControl glCtrl)
        {
            //to let OpenTK work with the given glCtrl:
            glCtrl.MakeCurrent();
            //setting MousePosition:
            Vector2 n = new Vector2((float)(Math.Round(_p.X)), (float)Math.Round(_p.Y));
            Matrix4 proMatr = new Matrix4();
            //reading ProjectionMatrix values:
            GL.GetFloat(GetPName.ProjectionMatrix, out proMatr);
            Matrix4 viewMatr = new Matrix4();
            //reading ModelViewMatrix:
            GL.GetFloat(GetPName.ModelviewMatrix, out viewMatr);
            //calculate the coordinates under the courser
            Vector4 Pos = new Vector4(UnProject(ref viewMatr, ref proMatr, glCtrl.Size, ref n));
            System.Drawing.PointF P = new System.Drawing.PointF(Pos.X, Pos.Y);
            return P;

        }

        //============================================================================================================================================================================
        //Methods taken from http://www.opentk.com/node/1276
        private static Vector4 UnProject(ref Matrix4 projection, ref Matrix4 view, Size viewport, ref Vector2 mouse)
        {
            Vector4 vec;

            vec.X = 2.0f * mouse.X / (float)viewport.Width - 1;
            vec.Y = -(2.0f * mouse.Y / (float)viewport.Height - 1);
            vec.Z = 0;
            vec.W = 1.0f;

            Matrix4 viewInv = Matrix4.Invert(view);
            Matrix4 projInv = Matrix4.Invert(projection);

            Vector4.Transform(ref vec, ref projInv, out vec);
            Vector4.Transform(ref vec, ref viewInv, out vec);

            if (vec.W > float.Epsilon || vec.W < float.Epsilon)
            {
                vec.X /= vec.W;
                vec.Y /= vec.W;
                vec.Z /= vec.W;
            }

            return vec;
        }
    }
}

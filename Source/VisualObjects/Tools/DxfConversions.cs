﻿#region CopyrightAndLicense
// ---------------------------------------------------------------------------------------------------- 
//  CPlan Library
//  Computational Planning Group 
//  Copyright file = DxfConversions.cs 
//  Copyright (C) 2014/10/18  12:57 PM
//  All Rights Reserved
//  Computational Planning Group: http://cplan-group.net/
//  Author(s): Dr. Reinhard Koenig
//  contact@cplan-group.net
// ---------------------------------------------------------------------------------------------------- 
//  This file is part of  CPlan Library
// 
//  CPlan Library is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  CPlan Library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with CPlan Library. If not, see <http://www.gnu.org/licenses/>.
// ---------------------------------------------------------------------------------------------------- 
// 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using CPlan.Geometry;
using netDxf;
using netDxf.Entities;
using netDxf.Header;
using netDxf.Tables;
using Tektosyne.Geometry;

namespace CPlan.VisualObjects
{
    public static class DxfConversions
    {

        //============================================================================================================================================================================
        // line converters
        public static Line ToDxfLine(this LineD lineD)
        {
            Line line = new Line(new Vector3(Convert.ToSingle(lineD.Start.X), Convert.ToSingle(lineD.Start.Y), 0),
                new Vector3(Convert.ToSingle(lineD.End.X), Convert.ToSingle(lineD.End.Y), 0));
            return line;
        }

        public static Line ToDxfLine(this GLine lineG)
        {
            Line line = new Line(new Vector3(Convert.ToSingle(lineG.Start.Pos.X), Convert.ToSingle(lineG.Start.Pos.Y), 0),
                new Vector3(Convert.ToSingle(lineG.End.Pos.X), Convert.ToSingle(lineG.End.Pos.Y), 0));
            return line;
        }

        public static Line ToDxfLine(this Line2D line2)
        {
            Line line = new Line(new Vector3(Convert.ToSingle(line2.Start.X), Convert.ToSingle(line2.Start.Y), 0),
                new Vector3(Convert.ToSingle(line2.End.X), Convert.ToSingle(line2.End.Y), 0));
            return line;
        }

        //============================================================================================================================================================================
        // point converters
        public static PolylineVertex ToPolylineVertex(this OpenTK.Vector2d vector)
        {
            PolylineVertex vertex = new PolylineVertex(Convert.ToSingle(vector.X), Convert.ToSingle(vector.Y), 0);
            return vertex;
        }

        public static PolylineVertex ToPolylineVertex(this GPoint vectorG)
        {
            PolylineVertex vertex = new PolylineVertex(Convert.ToSingle(vectorG.Pos.X), Convert.ToSingle(vectorG.Pos.Y), 0);
            return vertex;
        }

        public static OpenTK.Vector2d ToVector2D(this PolylineVertex vertex)
        {
            OpenTK.Vector2d vector = new OpenTK.Vector2d(vertex.Location.X, vertex.Location.Y);
            return vector;
        }

        public static OpenTK.Vector2d ToVector2D(this LwPolylineVertex vertex)
        {
            OpenTK.Vector2d vector = new OpenTK.Vector2d(vertex.Location.X, vertex.Location.Y);
            return vector;
        }

        //============================================================================================================================================================================
        /// <summary>
        /// line converters
        /// </summary>
        /// <param name="linesD">lins to be converted</param>
        /// <returns>the dxf document</returns>
        public static DxfDocument ToDxfFile(List<LineD> linesD)
        {
            Layer curLayer = new Layer("line");
            DxfDocument dxf = new DxfDocument();
            foreach (LineD curLine in linesD)
            {
                Line line = curLine.ToDxfLine();
                //line.Color = AciColor.Blue;
                line.Layer = curLayer;
                line.Layer.Color.Index = 6;
                dxf.AddEntity(line);
            }
            return dxf;
            //dxf.Save("test2000.dxf", DxfVersion.AutoCad2000);
        }

        //============================================================================================================================================================================
        /// <summary>
        /// convert objects to dxf file
        /// </summary>
        /// <param name="allObjects"></param>
        /// <param name="dxfVersion"></param>
        /// <returns>DXF Document</returns>
        public static DxfDocument ToDxfFile(ObjectsDrawList allObjects, DxfVersion dxfVersion)
        {
            Layer lineLayer = new Layer("Lines");
            Layer rectLayer = new Layer("Rectangles");
            Layer polygonLayer = new Layer("Polygons");
            Layer polygonBuildingLayer = new Layer("Buildings");
            //Layer cubeLayer = new Layer("Cubes");
            DxfDocument dxf = new DxfDocument(dxfVersion);
            foreach (GeoObject curObj in allObjects)
            {
                if (curObj.GetType() == typeof(GLine))
                {
                    GLine gLine = (GLine)curObj;
                    Line line = gLine.ToDxfLine();
                    line.Color = new AciColor(ConvertColor.CreateColor(gLine.Color));
                    //line.Thickness = 2;
                    line.Layer = lineLayer;
                    line.Layer.Color.Index = 6;
                    dxf.AddEntity(line);
                }

                if (curObj.GetType() == typeof(GRectangle))
                {
                    GRectangle curRect = (GRectangle)curObj;
                    List<PolylineVertex> ptList = new List<PolylineVertex>();
                    foreach (OpenTK.Vector2d pt in curRect.PolyPoints)
                    {
                        ptList.Add(pt.ToPolylineVertex());
                    }
                    Polyline polyline = new Polyline(ptList, true);
                    if (curRect.Filled) polyline.Color = new AciColor(ConvertColor.CreateColor(curRect.FillColor));
                    else polyline.Color = new AciColor(ConvertColor.CreateColor(curRect.BorderColor));
                    polyline.Lineweight.Value = Convert.ToInt16(curRect.BorderWidth);
                    polyline.Layer = rectLayer;
                    polyline.Layer.Color.Index = 5;
                    dxf.AddEntity(polyline);
                }

                if (curObj.GetType() == typeof(GPolygon))
                {
                    GPolygon curPoly = (GPolygon)curObj;
                    List<PolylineVertex> ptList = new List<PolylineVertex>();
                    foreach (OpenTK.Vector2d pt in curPoly.PolyPoints)
                    {
                        ptList.Add(pt.ToPolylineVertex());
                    }
                    Polyline polyline = new Polyline(ptList, true);
                    if (curPoly.Filled) polyline.Color = new AciColor(ConvertColor.CreateColor(curPoly.FillColor));
                    else polyline.Color = new AciColor(ConvertColor.CreateColor(curPoly.BorderColor));
                    polyline.Lineweight.Value = Convert.ToInt16(curPoly.BorderWidth);
                    polyline.Layer = polygonLayer;
                    polyline.Layer.Color.Index = 4;
                    dxf.AddEntity(polyline);
                }

                if (curObj.GetType() == typeof(GPrism))
                {
                    GPrism curCube = (GPrism)curObj;
                    List<PolylineVertex> ptList = new List<PolylineVertex>();
                    foreach (OpenTK.Vector2d pt in curCube.PolyPoints)
                    {
                        ptList.Add(pt.ToPolylineVertex());
                    }
                    Polyline polyline = new Polyline(ptList, true);
                    if (curCube.Filled)
                    {
                        polyline.Color = new AciColor(ConvertColor.CreateColor(curCube.FillColor));
                    }
                    else polyline.Color = new AciColor(ConvertColor.CreateColor(curCube.BorderColor));
                    polyline.Lineweight.Value = Convert.ToInt16(curCube.BorderWidth);
                    polyline.Layer = polygonBuildingLayer;
                    polyline.Layer.Color.Index = 4;
                    dxf.AddEntity(polyline);
                }

                if (curObj.GetType() == typeof(GRegularPolygon))
                {
                    GRegularPolygon curPoly = (GRegularPolygon)curObj;
                    List<PolylineVertex> ptList = new List<PolylineVertex>();
                    foreach (OpenTK.Vector2d pt in curPoly.PolyPoints)
                    {
                        ptList.Add(pt.ToPolylineVertex());
                    }
                    Polyline polyline = new Polyline(ptList, true);
                    if (curPoly.Filled) polyline.Color = new AciColor(ConvertColor.CreateColor(curPoly.FillColor));
                    else polyline.Color = new AciColor(ConvertColor.CreateColor(curPoly.BorderColor));
                    polyline.Lineweight.Value = Convert.ToInt16(curPoly.BorderWidth);
                    polyline.Layer = polygonLayer;
                    polyline.Layer.Color.Index = 4;
                    dxf.AddEntity(polyline);
                }

            }
            return dxf;
        }

        /// <summary>
        /// creates objects from dxf file
        /// </summary>
        /// <param name="filename">name of the file to be loaded</param>
        /// <returns>dxf document or null if error occures</returns>
        public static DxfDocument FromDxfFile(String filename)
        {
            //DxfVersion.AutoCad2013); //DxfVersion.AutoCad14
            return DxfDocument.Load(filename);
        }

        //============================================================================================================================================================================
        public static ObjectsDrawList GObjectsFormDxf(String filename)
        {
            DxfDocument dxf = FromDxfFile(filename);
            ObjectsDrawList newGObjects = new ObjectsDrawList();

            //DxfObject test = dxf.GetGroup("LightWeightPolyline");

            // --- the polygons ---
            List<Polyline> polyLines = dxf.Polylines.ToList();
            //dxf.DrawingVariables.AcadVer = DxfVersion.AutoCad2010;
            foreach (Polyline pLine in polyLines)
            {
                List<OpenTK.Vector2d> ptList = new List<OpenTK.Vector2d>();
                foreach (PolylineVertex vertex in pLine.Vertexes)
                {
                    ptList.Add(vertex.ToVector2D());
                }
                GPolygon gPoly = new GPolygon(ptList);
                gPoly.FillColor = ConvertColor.CreateColor4(pLine.Color.ToColor());
                gPoly.BorderWidth = pLine.Lineweight.Value;
                newGObjects.Add(gPoly);
            }

            List<LwPolyline> lwPolyLines = dxf.LwPolylines.ToList();
            foreach (LwPolyline lwLine in lwPolyLines)
            {
                List<OpenTK.Vector2d> ptList = new List<OpenTK.Vector2d>();
                foreach (LwPolylineVertex vertex in lwLine.Vertexes)
                {
                    ptList.Add(vertex.ToVector2D());
                }
                if (ptList.Count == 2)
                {
                    GLine gLine = new GLine(ptList[0].ToPointF(), ptList[1].ToPointF());
                    gLine.Color = ConvertColor.CreateColor4(lwLine.Color.ToColor());
                    gLine.Width = 1;// lwLine.Lineweight.Value;
                    newGObjects.Add(gLine);
                }
                else if (ptList.Count > 2)
                {
                    GPolygon gPoly = new GPolygon(ptList);
                    gPoly.FillColor = ConvertColor.CreateColor4(lwLine.Color.ToColor());
                    gPoly.BorderWidth = lwLine.Lineweight.Value;
                    newGObjects.Add(gPoly);
                }
            }

            // --- the lines ---
            List<Line> dLines = dxf.Lines.ToList();
            foreach (Line dLine in dLines)
            {
                GLine gLine = new GLine(dLine.StartPoint.X, dLine.StartPoint.Y, dLine.EndPoint.X, dLine.EndPoint.Y);
                gLine.Color = ConvertColor.CreateColor4(dLine.Color.ToColor());
                gLine.Width = 1;// dLine.Lineweight.Value;
                newGObjects.Add(gLine);
            }

            return newGObjects;
        }

        //============================================================================================================================================================================
        public static List<Line2D> LinesFromDxf(String filename)
        {
            DxfDocument dxf = FromDxfFile(filename);
            List<Line2D> lines = new List<Line2D>();
            List<Line> dLines = dxf.Lines.ToList();
            foreach (Line dLine in dLines)
            {
                Line2D line = new Line2D(dLine.StartPoint.X, dLine.StartPoint.Y, dLine.EndPoint.X, dLine.EndPoint.Y);
                lines.Add(line);
            }

            List<LwPolyline> lwPolyLines = dxf.LwPolylines.ToList();
            foreach (LwPolyline lwLine in lwPolyLines)
            {
                List<OpenTK.Vector2d> ptList = new List<OpenTK.Vector2d>();
                foreach (LwPolylineVertex vertex in lwLine.Vertexes)
                {
                    ptList.Add(vertex.ToVector2D());
                }
                if (ptList.Count == 2)
                {
                    lines.Add(new Line2D(ptList[0].X, ptList[0].Y, ptList[1].X, ptList[1].Y));
                }
                else if (ptList.Count > 2)
                {
                    for (int i = 0; i < ptList.Count - 1; i++)
                    {
                        lines.Add(new Line2D(ptList[i].X, ptList[i].Y, ptList[i + 1].X, ptList[i + 1].Y));
                    }
                }
            }

            return lines;
        }


    }
}

﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace CollabSample.Communication
{
    
    public class Communication2Lucy 
    {
        public StreamReader OutputStream { get; private set; }
        private StreamWriter InputStream;

        public Communication2Lucy() {
            //connect ("localhost", 7654);
        }
    

    public bool connect(String host, int port) {
        TcpClient socketForServer;
        try
        {
            socketForServer = new TcpClient(host, port);
        }
        catch
        {
            Console.WriteLine(
            "Failed to connect to server at {0}:{1}", host, port);
            return false;
        }
       
        NetworkStream networkStream = socketForServer.GetStream();
        System.IO.StreamReader streamReader =
        new System.IO.StreamReader(networkStream);
        System.IO.StreamWriter streamWriter =
        new System.IO.StreamWriter(networkStream);
        Console.WriteLine("*******This is client program who is connected to localhost on port No:10*****");

      //  streamWriter.WriteLine("{'action':'authenticate','username':'lukas','userpasswd':'1234'}");
      //  streamWriter.Flush();

        OutputStream = new StreamReader(networkStream);
        InputStream = new StreamWriter(networkStream);


        return true;
        try
        {
            string outputString;
            // read the data from the host and display it
            {
                //outputString = streamReader.ReadLine();
                //Console.WriteLine("Message Recieved by server:" + outputString);

                //Console.WriteLine("Type your message to be recieved by server:");

                streamWriter.WriteLine("{'action':'authenticate','username':'lukas','userpasswd':'1234'}");
                streamWriter.Flush();

                //outputString = streamReader.ReadLine();


                Console.WriteLine("type:");
                string str = Console.ReadLine();
                str = "exit";
                while (str != "exit")
                {
                    streamWriter.WriteLine(str);
                    streamWriter.Flush();
                    Console.WriteLine("type:");
                    str = Console.ReadLine();
                }
                if (str == "exit")
                {
                    streamWriter.WriteLine(str);
                    streamWriter.Flush();
                   
                }
                
            }
        }
        catch
        {
            Console.WriteLine("Exception reading from Server");
        }
        // tidy up
        networkStream.Close();
        //Console.WriteLine("Press any key to exit from client program");
        //Console.ReadKey();
    }

    public JProperty sendAction2Lucy(string action)
    {
        InputStream.WriteLine(action);
        InputStream.Flush();

        string json = OutputStream.ReadLine();
        var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

        return ((Newtonsoft.Json.Linq.JProperty)(jObj2.First));
    }

    public JProperty authenticate(string user, string password)
    {
        return sendAction2Lucy("{'action':'authenticate','username':'" + user + "','userpasswd':'" + password + "'}");

        InputStream.WriteLine("{'action':'authenticate','username':'" + user+ "','userpasswd':'" + password + "'}");
        InputStream.Flush();

        string json = OutputStream.ReadLine();
        var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

        return ((Newtonsoft.Json.Linq.JProperty)(jObj2.First));
    }

    public JProperty getActionsList()
    {
        return sendAction2Lucy("{'action':'get_list','of':['actions']}");

        InputStream.WriteLine("{'action':'get_list','of':['actions']}");
        InputStream.Flush();

        string json = OutputStream.ReadLine();
        var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

        return ((Newtonsoft.Json.Linq.JProperty)(jObj2.First));
    }

    public JProperty getActionParameters(string action)
    {
        return sendAction2Lucy("{'action':'get_infos_about','actionname':'" + action + "'}");
    }

    public JProperty getServiceParameters(string service)
    {
        return sendAction2Lucy("{'action':'get_infos_about','servicename':'" + service + "'}");
    }

    public JProperty getServicesList()
    {
        return sendAction2Lucy("{'action':'get_list','of':['services']}");

        InputStream.WriteLine("{'action':'get_list','of':['services']}");
        InputStream.Flush();

        string json = OutputStream.ReadLine();
        var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

        return ((Newtonsoft.Json.Linq.JProperty)(jObj2.First));
    }

    private static string GetData()
    {
        //Ack from sql server
        return "ack";
    }

        /*
        public class Connection extends Thread{	
	Socket socket;
	ClientConsole console;
	BufferedReader input;
	DataOutputStream output;
	PrintStream print;
	boolean stopTalking = false;
		
	public Connection() throws IOException, ConnectException{		
		this.connect();
	}	   	
	
	private void connect() throws IOException{		

		System.out.println("connect");
		InetAddress host = InetAddress.getLocalHost();
        this.socket = new Socket(host.getHostName(), 7654);
		this.console = new ClientConsole(this);
        this.input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.output = new DataOutputStream(socket.getOutputStream());
		this.print = new PrintStream(socket.getOutputStream()); 
	}		*/
    }
}

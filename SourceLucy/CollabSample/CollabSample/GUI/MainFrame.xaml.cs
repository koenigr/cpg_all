﻿using CollabSample.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CollabSample.GUI
{
    /// <summary>
    /// Interaction logic for MainFrame.xaml
    /// </summary>
    public partial class MainFrame : UserControl
    {
        #region ViewModel dependency property

        public static DependencyProperty ViewModelProperty =
            DependencyProperty.Register(
                "ViewModel",
                typeof(GeoVM),
                typeof(MainFrame),
                null
            );

        public GeoVM ViewModel
        {
            get { return (GeoVM)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }
        #endregion

        public MainFrame()
        {
            InitializeComponent();

            Loaded += MainFrame_Loaded;
        }

        void MainFrame_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel = new GeoVM();
            DataContext = ViewModel;
        }
    }
}

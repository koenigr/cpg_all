﻿using CollabSample.Communication;
using GeoAPI.Geometries;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.ViewModel;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace CollabSample.ViewModels
{
    public class GeoVM : NotificationObject
    {
        public ObservableCollection<Rect> Buildings { get; set; }

        public String InfoMW { get; set; }

        public String User { get; set; }
        public String PW { get; set; }

        public IEnumerable<JToken> ActionsListContent { get; set; }
        public IEnumerable<JToken> ServicesListContent { get; set; }

        public String ActionDetailsContent { get; set; }
        public String ServiceDetailsContent { get; set; }

        private  StreamReader OutputFromLucy;
        private DispatcherTimer timer;

        private Communication2Lucy cl = null;


        public String SelectedAction
        {
            get { return _selectedAction; }
            set
            {
                if (value == _selectedAction)
                    return;

                _selectedAction = value;

                RaisePropertyChanged("SelectedAction");

                // selection changed - do something special
                ShowDetails4SelectedAction(_selectedAction);
            }
        }
        private String _selectedAction;


        public String SelectedService
        {
            get { return _selectedService; }
            set
            {
                if (value == _selectedService)
                    return;

                _selectedService = value;

                RaisePropertyChanged("SelectedService");

                // selection changed - do something special
                ShowDetails4SelectedService(_selectedService);
            }
        }
        private String _selectedService;

        // commands
        public ICommand CreateNewGeoCommand { get; private set; }
        public ICommand CreateScenarioCommand { get; private set; }
        public ICommand Connect2LucyCommand { get; private set; }
        public ICommand Login2LucyCommand { get; private set; }
        public ICommand GetActionsListCommand { get; private set; }
        public ICommand GetServicesListCommand { get; private set; }
        public DelegateCommand<String> SendMessageToMWCommand { get; private set; }


        public GeoVM() {

            Buildings = new ObservableCollection<Rect>();
            initBuildings();

            InfoMW = "Started\n";

           
            CreateNewGeoCommand = new DelegateCommand(OnCreateNewGeoExecuted);
            CreateScenarioCommand = new DelegateCommand(OnCreateScenarioExecuted);
            Connect2LucyCommand = new DelegateCommand(OnConnect2LucyExecuted);
            Login2LucyCommand = new DelegateCommand<object>(OnLogin2LucyExecuted);
            GetActionsListCommand = new DelegateCommand(OnGetActionsListExecuted);
            GetServicesListCommand = new DelegateCommand(OnGetServicesListExecuted);
            SendMessageToMWCommand = new DelegateCommand<String>(OnSendMessageToMWCommandExecuted);
        }

        private void initBuildings()
        {
            
            Rect r1 = new Rect(10, 10, 25, 80);
            Rect r2 = new Rect(50, 50, 100, 20);
          
            
            //Buildings.Add(r1);
            //Buildings.Add(r2);

            Random num = new Random();

            for (int c = 0; c < 8; c++)
            {
                for (int r = 0; r < 7; r++)
                {
                    int valX = 10 * (num.Next(0, 80)/10);
                    int valY = 10 * (num.Next(0, 50)/10);
                    int width = 10*(num.Next(20, 100 - valX)/10);
                    int height = 10*(num.Next(20, 70 - valY)/10);

                    Rect rect = new Rect(100*c + valX, 70*r + valY, width, height);

                    Buildings.Add(rect);
                }
            }
        }

        private void OnCreateNewGeoExecuted()
        {
            Buildings.Clear();
            initBuildings();

            InfoMW += "new Geo created\n";
            RaisePropertyChanged("InfoMW");
        }

        private void OnCreateScenarioExecuted() {
            //write point
            NetTopologySuite.Geometries.Point point = new NetTopologySuite.Geometries.Point(12, 34);
            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
            string geoJSON = GeoJSONWriter.Write(point);
            AddText2InfoMW(geoJSON + "\n");

            //write poly
            LinearRing ring1 = new NetTopologySuite.Geometries.LinearRing(new Coordinate[] 
            { 
                new Coordinate(100.0, 0.0), 
                new Coordinate(101.0, 0.0), 
                new Coordinate(101.0, 1.0), 
                new Coordinate(100.0, 0.0) 
            });
            NetTopologySuite.Geometries.Polygon poly = new NetTopologySuite.Geometries.Polygon(ring1);
            string geoJSONPoly = GeoJSONWriter.Write(poly);
            AddText2InfoMW(geoJSONPoly + "\n");
            
            // MultiPoint
            // Geometry collection


            /* sample code
            foreach (Coordinate coord in poly.Coordinates)
            {
                pX = coord.X;
                pY = coord.Y;
                pZ = coord.Z;
            }

            GeoJsonReader reader = new GeoJsonReader();
            MultiPolygon multiPoly = reader.Read<NetTopologySuite.Geometries.MultiPolygon>(jsonString);
            */

            /*
            { "type": "Polygon",
    "coordinates": [
      [ [100.0, 0.0], [101.0, 0.0], [101.0, 1.0], [100.0, 1.0], [100.0, 0.0] ]
      ]
   }*/
        }

        private void OnConnect2LucyExecuted()
        {
            cl = new Communication2Lucy();

            string result = "Connection to Lucy was ";

            if (!cl.connect("localhost", 7654))
                result += "not ";

            result += "successful! \n";

            AddText2InfoMW(result);


            return;
            if (cl.OutputStream != null )
            {
                OutputFromLucy = cl.OutputStream;
                // This code creates a new DispatcherTimer with an interval of 15 seconds.
                timer = new DispatcherTimer();
                timer.Interval = new TimeSpan(0, 0, 0, 0, 20);

                timer.Tick += new EventHandler(ReadOutputFromServer);
                // Start the timer.  
                //Note that this call can be made from any thread.
                timer.Start();
                /*
                string output = cl.OutputStream.ReadLine();
                while (output != null)
                {
                    InfoMW += output;
                    output = cl.OutputStream.ReadLine();
                }*/
            }
        }

        private void OnGetActionsListExecuted()
        {
            if (cl == null)
            {
                InfoNoConnection();
            }
            else
            {
                JProperty result = cl.getActionsList();
                AddText2InfoMW(result.Name + ": " + result.Value + "\n");

                IEnumerable<JToken> arrActions =  result.Value["actions"];
                ActionsListContent = arrActions;
                SelectedAction = null;

                RaisePropertyChanged("ActionsListContent");
            }
        }

        private void OnGetServicesListExecuted()
        {
            if (cl == null)
            {
                InfoNoConnection();
            }
            else
            {
                JProperty result = cl.getServicesList();
                AddText2InfoMW(result.Name + ": " + result.Value + "\n");

                ServicesListContent = result.Value["services"]; 
                SelectedService = null;

                RaisePropertyChanged("ServicesListContent");
            }
        }

        private void OnSendMessageToMWCommandExecuted(string msg) 
        {
            if (cl == null)
            {
                InfoNoConnection();
            }
            else
            {
                JProperty result = cl.sendAction2Lucy(msg);
                AddText2InfoMW(result.Name + ": " + result.Value + "\n");
            }
        }


        private void ShowDetails4SelectedAction(string action)
        {
            if (cl == null)
            {
                InfoNoConnection();
            }
            else
            {
                if (action == null || action.Length == 0)
                {
                    ActionDetailsContent = "no action selected";
                }
                else
                {
                    JProperty result = cl.getActionParameters(action);
                    //AddText2InfoMW(result.Name + ": " + result.Value + "\n");

                    ActionDetailsContent = result.Name + ": " + result.Value;
                }
                RaisePropertyChanged("ActionDetailsContent");
            }
        }

        private void ShowDetails4SelectedService(string service)
        {
            if (cl == null)
            {
                InfoNoConnection();
            }
            else
            {
                if (service == null || service.Length == 0)
                {
                    ActionDetailsContent = "no service selected";
                }
                else
                {
                    JProperty result = cl.getServiceParameters(service);

                    ServiceDetailsContent = result.Name + ": " + result.Value;
                }
                RaisePropertyChanged("ServiceDetailsContent");
            }
        }


        private void ReadOutputFromServer(object sender, EventArgs e)
        {
            // Timer callback code here...
           // while (!OutputFromLucy.EndOfStream) 
            {
                string json = OutputFromLucy.ReadLine();
                dynamic jObj = JsonConvert.DeserializeObject(json);

                var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

                string name = ((Newtonsoft.Json.Linq.JProperty)(jObj2.First)).Name;
                JToken value = ((Newtonsoft.Json.Linq.JProperty)(jObj2.First)).Value;
                object obj = value.ToObject(value.GetType());

                var val2 = ((Newtonsoft.Json.Linq.JProperty)(jObj2.First)).Value;

                InfoMW += name + ": " + obj.ToString() + "\n";
                RaisePropertyChanged("InfoMW");
            }

            timer.Stop();
        }

        private void OnLogin2LucyExecuted(object pwBox)
        {
            if (cl == null)
            {
                InfoNoConnection();
            }
            else
            {
                if (pwBox != null && pwBox is PasswordBox)
                {
                    //var passwordBox = (PasswordBox)param;
                    // string result = cl.authenticate("lukas", "1234");
                    JProperty result = cl.authenticate(User, (pwBox as PasswordBox).Password);
                    AddText2InfoMW(result.Name + ": " + result.Value + "\n");
                }
            }
        }

        private void InfoNoConnection()
        {
            AddText2InfoMW("No Connection to Lucy established yet! Please connect to Lucy first of all!");
        }

        private void AddText2InfoMW(string text)
        {
            InfoMW += text;
            RaisePropertyChanged("InfoMW");
        }
    }
}

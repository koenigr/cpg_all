﻿
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CPlan.Isovist2D;
using GeoAPI.Geometries;

namespace IsovistLucyService
{

    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
            GeoJsonReader GeoJSONReader = new GeoJsonReader();

            List<CPlan.Geometry.Vector2D> points = new List<CPlan.Geometry.Vector2D>();
            points.Add(new CPlan.Geometry.Vector2D(1, 1));
            points.Add(new CPlan.Geometry.Vector2D(3, 2));

            List<CPlan.Geometry.Line2D> lines = new List<CPlan.Geometry.Line2D>();
            lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(0, 0), new CPlan.Geometry.Vector2D(0, 10)));
            lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(0, 10), new CPlan.Geometry.Vector2D(10, 10)));
            lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(0, 0), new CPlan.Geometry.Vector2D(10, 0)));
            lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(10, 0), new CPlan.Geometry.Vector2D(10, 10)));
            lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(2, 5), new CPlan.Geometry.Vector2D(8, 5)));

            //create sample iso object on client
            IsovistField2D iso_client_orig = new IsovistField2D(points, lines);

            //pass it to the wrapper and serialize to JSON
            IsovistField2DWrapper wrapper = new IsovistField2DWrapper(iso_client_orig);
            string iso_client_send = GeoJSONWriter.Write(wrapper);
            textBox1.Text = iso_client_send;

            //read json on server and deserialize into wrapper
            IsovistField2DWrapper iso_wrapper_server = GeoJSONReader.Read<IsovistField2DWrapper>(iso_client_send);
            //conver wrapper to isovist object
            IsovistField2D iso_server = iso_wrapper_server.ToIsovistField2D();

            //perform actual calculations, the precision need to be set explicitly here since we can't do it using isovist constructor
            iso_server.Calculate(iso_wrapper_server.Precision, true);

            //wrap the results
            IsovistField2DWrapper iso_server_wrapper_result = new IsovistField2DWrapper(iso_server);
            string iso_server_send = GeoJSONWriter.Write(iso_server_wrapper_result);
            textBox2.Text = iso_server_send;

            //read results on client           
            IsovistField2DWrapper iso_client_wrapper_result = GeoJSONReader.Read<IsovistField2DWrapper>(iso_server_send);
            //here we are unable to convert back to isovist object since there are no public method allowing us to set the results.
            //we can change the isovist public API to allow this in the future.
        }

    }


    public class GeometryConverter
    {
        public static Point ToPoint(CPlan.Geometry.Vector2D point)
        {
            return new Point(point.X, point.Y);
        }

        public static LineString ToLineString(CPlan.Geometry.Line2D line)
        {            
            return new LineString(new Coordinate[] { new Coordinate(line.Start.X, line.Start.Y), new Coordinate(line.End.X, line.End.Y) });
        }

        public static CPlan.Geometry.Vector2D ToVector2D(IPoint point){
            return new CPlan.Geometry.Vector2D(point.X, point.Y);
        }

        public static CPlan.Geometry.Line2D ToLine2D(ILineString line)
        {
            return new CPlan.Geometry.Line2D(line.StartPoint.X, line.StartPoint.Y, line.EndPoint.X, line.EndPoint.Y);
        }

        public static MultiPoint ToMultiPoint(List<CPlan.Geometry.Vector2D> points)
        {
            Point[] tmp = new Point[points.Count];
                        
            for (int i = 0; i < points.Count; i++) {
                tmp[i] = ToPoint(points[i]);
            }

            return new MultiPoint(tmp);
        }

        public static List<CPlan.Geometry.Vector2D> ToVector2DList(MultiPoint points)
        {
            List<CPlan.Geometry.Vector2D> result = new List<CPlan.Geometry.Vector2D>();

            foreach (IPoint point in points.Geometries)
            {
              result.Add(ToVector2D(point));                
            }

            return result;
        }

        public static MultiLineString ToMultiLineString(List<CPlan.Geometry.Line2D> lines)
        {
            LineString[] tmp = new LineString[lines.Count];
                        
            for (int i = 0; i < lines.Count; i++) {
                tmp[i] = ToLineString(lines[i]);
            }

            return new MultiLineString(tmp);
        }

        public static List<CPlan.Geometry.Line2D> ToLine2DList(MultiLineString lines)
        {
            List<CPlan.Geometry.Line2D> result = new List<CPlan.Geometry.Line2D>();

            foreach (ILineString line in lines.Geometries)
            {
                result.Add(ToLine2D(line));
            }

            return result;
        }
    }

    public class IsovistField2DWrapper
    {
        private MultiPoint m_points;
        private MultiLineString m_obstacles;
        private float m_precision;
        private List<Dictionary<ResultsIsovist, double>> m_results;

        public List<Dictionary<ResultsIsovist, double>> Results
        {
            get { return m_results; }
            set { m_results = value; }
        }

        public MultiLineString Obstacles
        {
            get { return m_obstacles; }
            set { m_obstacles = value; }
        }        

        public float Precision
        {
            get { return m_precision; }
            set { m_precision = value; }
        }

        //by default all Properties are serialized but can be exluded using following annotation        
        //[Newtonsoft.Json.JsonIgnore]
        public MultiPoint Points
        {
            get { return m_points; }
            set { m_points = value; }
        }

        [Newtonsoft.Json.JsonConstructor]
        public IsovistField2DWrapper(MultiPoint points, MultiLineString obstacles, List<Dictionary<ResultsIsovist, double>> results,  float precision)
        {
            m_points = points;
            m_precision = precision;
            m_obstacles = obstacles;
            m_results = results;
        }

        public IsovistField2DWrapper(IsovistField2D isovist) : this(GeometryConverter.ToMultiPoint(isovist.Points), GeometryConverter.ToMultiLineString(isovist.ObstacleLines), isovist.Results, isovist.Precision) { }

        public IsovistField2D ToIsovistField2D()
        {
            return new IsovistField2D(GeometryConverter.ToVector2DList(Points), GeometryConverter.ToLine2DList(Obstacles));           
        }
    }

   
}

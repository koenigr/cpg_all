﻿using System;
using System.Collections.Generic;
using System.Text;
using Grasshopper.Kernel;
using Rhino.Geometry;
using System.Drawing;
using Grasshopper.Kernel.Components;
using Rhino.Display;
using Grasshopper.Kernel.Types;

using Tektosyne.Geometry;
using Tektosyne.Collections;
using Tektosyne;
using System.Windows.Forms;
using System.Linq;
using System.Runtime.InteropServices;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using DecodingSpaceComponents;
using DecodingSpaceComponents.Properties;
using CPlan.Evaluation;
using QuickGraph;


namespace DecodingSpaceComponents
{

    [StructLayout(LayoutKind.Sequential)]
    internal struct GH_CustomPreviewItem
    {
        internal Color m_col;
        internal DisplayMaterial m_mat;
        internal IGH_PreviewData m_obj;
    }

    public class GH_TextTag
    {
        // Fields
        public Point3d m_P;
        public string m_T;

        // Methods
        public GH_TextTag(Point3d nPoint, string nText)
        {
            this.m_P = nPoint;
            this.m_T = nText;
        }
    }

    public class Choice : GH_Component
    {
        //Global Variables
        private BoundingBox m_clipbox;
        private List<GH_CustomPreviewItem> m_items;
        private int m_height;
        private List<GH_TextTag> m_tags;
        bool drawText = true;

        //Constructor
        public Choice()

            //Call the base constructor
            : base("Betweenness", "Betweenness", "Provide Betweenness(Choice) Analysis", "DeCodingSpaces", "Graph & Analyse")
        {
            //this.m_tags = new List<GH_TextTag>();
            this.m_height = 12;

        }
        # region preview + input
        public override GH_Exposure Exposure
        {
            get
            {
                return GH_Exposure.secondary;
            }
        }

        public override bool IsPreviewCapable
        {
            get
            {
                return true;
            }
        }

        public override void ClearData()
        {
            base.ClearData();

        }

        public override BoundingBox ClippingBox
        {
            get
            {
                return this.m_clipbox;
            }
        }

        private float map(float value, float low1, float high1, float low2, float high2)
        {
            float newValue = ((value - low1) * (high2 - low2) / (high1 - low1)) + low2;
            return (newValue);
        }

        public override void DrawViewportMeshes(IGH_PreviewArgs args)
        {
            if (this.m_items != null)
            {
                if (this.Attributes.Selected)
                {
                    GH_PreviewMeshArgs e = new GH_PreviewMeshArgs(args.Viewport, args.Display, args.ShadeMaterial_Selected, args.MeshingParameters);
                    foreach (GH_CustomPreviewItem item in this.m_items)
                    {
                        item.m_obj.DrawViewportMeshes(e);
                    }
                }
                else
                {
                    foreach (GH_CustomPreviewItem item in this.m_items)
                    {
                        GH_PreviewMeshArgs e = new GH_PreviewMeshArgs(args.Viewport, args.Display, item.m_mat, args.MeshingParameters);
                        item.m_obj.DrawViewportMeshes(e);
                    }
                }
            }
        }

        public override void DrawViewportWires(IGH_PreviewArgs args)
        {
            if (this.m_items != null)
            {
                if (this.Attributes.Selected)
                {
                    GH_PreviewWireArgs e = new GH_PreviewWireArgs(args.Viewport, args.Display, args.WireColour_Selected, args.DefaultCurveThickness);
                    foreach (GH_CustomPreviewItem item in this.m_items)
                    {
                        if (!(item.m_obj is GH_Mesh))
                        {
                            item.m_obj.DrawViewportWires(e);
                        }
                    }
                }
                else
                {
                    foreach (GH_CustomPreviewItem item in this.m_items)
                    {
                        if (!(item.m_obj is GH_Mesh))
                        {
                            GH_PreviewWireArgs e = new GH_PreviewWireArgs(args.Viewport, args.Display, item.m_col, args.DefaultCurveThickness);
                            item.m_obj.DrawViewportWires(e);
                        }
                    }
                }
            }



            //draw numbers

            if (drawText)
            {
                if (!this.Locked && (this.m_tags != null))
                {
                    Color col = args.WireColour;
                    int hgt = this.m_height;
                    if (this.Attributes.Selected)
                    {
                        col = args.WireColour_Selected;
                    }
                    foreach (GH_TextTag tag in this.m_tags)
                    {
                        args.Display.Draw2dText(tag.m_T, col, tag.m_P, true, hgt);
                    }
                }
            }
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {

            //pManager.RegisterParam(new GraphParameter(), "Graph", "IGh", "Deliver Inverse Graph", GH_ParamAccess.item);
            pManager.AddLineParameter("Line segments", "L", "Line segments to analyze (you can use filter geometry to get the right representation)", GH_ParamAccess.list);
            //pManager.Register_IntegerParam("Analysis Type", "T", "Set '0' for Angular, Set '1' for Metric Analysis", 0);
            pManager.Register_DoubleParam("Radius", "R", "Radius values for Graph Analysis", GH_ParamAccess.list);
            //pManager.Register_BooleanParam("Text Preview", "B", "Preview results of Analysis", false);
            pManager[1].Optional = true;
            //pManager[3].Optional = true;


        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.Register_DoubleParam("Choice", "Ch", "Analysis results", GH_ParamAccess.list);
            pManager.Register_DoubleParam("Centrality StepDepth", "CSd", "Analysis results", GH_ParamAccess.list);
            pManager.Register_DoubleParam("Centrality Metric", "CM", "Analysis results", GH_ParamAccess.list);
            pManager.Register_DoubleParam("Choice2", "Ch2", "Analysis results");
            pManager.Register_LineParam("L", "l", "lines", GH_ParamAccess.list);
            pManager.Register_LineParam("Lorigin", "lorigin", "lines", GH_ParamAccess.list);
        }
        # endregion

        #region solveInstance
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            string typeChoiceStr = "angle";
            //get line segments and convert them from Rhino.Line to Cplan.Geometry.Line2D
            List<Line> lineInput = new List<Line>();
            DA.GetDataList(0, lineInput);
            List<CPlan.Geometry.Line2D> lineIn = new List<CPlan.Geometry.Line2D>();
            foreach (Line l in lineInput)
            {
                lineIn.Add(new CPlan.Geometry.Line2D(l.FromX, l.FromY, l.ToX, l.ToY));
            }

            //get radiuses
            List<double> radInput = new List<double>();
            DA.GetDataList(1, radInput);
            List<float> radIn = new List<float>();

            foreach (double d in radInput)
            {
                radIn.Add((float)d);
            }
            //if no radius defined by user global analysis is set up
            if (radIn.Count == 0)
                radIn.Add(float.MaxValue);

            //create gpraph and analyse
            GraphAnalysis analyzer = new GraphAnalysis(lineIn);
            analyzer.Calculate(typeChoiceStr, radIn.ToArray());

            //get analysis results
            float[] resultsChoice2 = analyzer.ResultsAngular.GetChoice()[0];
            float[] resultsChoice = analyzer.ResultsAngular.GetNormChoiceArray()[0];
            float[] resultsCentralityMetric = analyzer.ResultsAngular.GetNormCentralityMetricArray()[0];
            float[] resultsCentralitySteps = analyzer.ResultsAngular.GetNormCentralityStepsArray()[0];

            //get the line segments in the same order as the analysis results
            List<Line> EdgesOrigin = new List<Line>();
            LineD[] edgeOrigin = analyzer.ResultsAngular.OrigEdges;
            foreach (LineD l in edgeOrigin)
            {
                EdgesOrigin.Add(new Line(l.Start.X, l.Start.Y, 0, l.End.X, l.End.Y, 0));
            }

            //UndirectedEdge<CPlan.Geometry.Vector2D>[] tempEdges = analyzer.Network.Edges.ToArray();
            //List<Line> Edges = new List<Line>();
            //foreach (UndirectedEdge<CPlan.Geometry.Vector2D> tempEdge in tempEdges)
            //{
            //    Edges.Add(new Line(new Point3d(tempEdge.Source.X, tempEdge.Source.Y, 0), new Point3d(tempEdge.Target.X, tempEdge.Target.Y, 0)));
            //}

            /* GraphAnalysis analyzer = new GraphAnalysis(network);
             float[] radius = { 700 };//{float.MaxValue, 700};
             analyzer.Calculate("angle", radius);

             Vector4[] ffColors = null;
             if (rB_fitChoice.Checked) ffColors = ConvertColor.CreateFFColor4(analyzer.ResultsAngular.GetNormChoiceArray()[0]);
             if (rB_fitCentral.Checked) ffColors = ConvertColor.CreateFFColor4(analyzer.ResultsAngular.GetNormCentralityMetricArray()[0]);
             Vector2D[] Nodes = analyzer.Network.Vertices.ToArray();
             UndirectedEdge<Vector2D>[] tempEdges = analyzer.Network.Edges.ToArray();
             List<Line2D> Edges = new List<Line2D>();
             foreach (UndirectedEdge<Vector2D> tempEdge in tempEdges)
             {
                 Edges.Add(new Line2D(tempEdge.Source, tempEdge.Target));
             }

             DrawAnalysis(ffColors, Edges.ToArray(), Nodes, new Vector2D(0, 0));

             network = null;
             ViewControl1.ZoomAll();*/


            ////set Output
            DA.SetDataList(0, new List<float>(resultsChoice));
            DA.SetDataList(1, new List<float>(resultsCentralitySteps));
            DA.SetDataList(2, new List<float>(resultsCentralityMetric));
            DA.SetDataList(3, new List<float>(resultsChoice2));
            //DA.SetDataList(4, Edges);
            DA.SetDataList(5, EdgesOrigin);
        }
        #endregion
        protected override Bitmap Internal_Icon_24x24
        {
            get
            {
                return Resources.BE1_01;
            }
        }

        public override Guid ComponentGuid
        {
            get
            {
                //Do not ever copy a Guid!
                return new Guid("{06740816-3708-4281-ABF5-F99D0ABCE3D7}");

            }
        }
    }
}
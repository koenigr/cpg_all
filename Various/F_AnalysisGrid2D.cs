﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Visualisation;
using DOMMC;
using GeometryModel;
using CPlan.Geometry;
using System.Windows.Forms;
using System.Drawing;
using System.Collections;
using OpenTK.Graphics.OpenGL;
using Helper;
using OpenTK;
using Cloo;
using Cloo.Bindings;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Diagnostics;
using OptimizationFramework;
using Helper;
using CPlan.Isovist2D;
//using FW_GPU_NET;
using Smrf.NodeXL.Core;
using Smrf.NodeXL.Algorithms;

namespace VisibilityAnalysisModel
{
    [Serializable]
    public class F_AnalysisGrid : VisibilityAnalysisObject, IVisualisation, IPreprocessDrawingWithoutOpenGLContext, IDisposable, ICustomTypeDescriptor, IObserver//, IEvaluationObject
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Attributes

        ObserverLink<Visualisation.Model> m_VModel;
        private static bool m_bResetToken;

        [NonSerialized]
        static bool m_constructorRunning = false;
        [NonSerialized]
        bool m_UpdateCalculationNotNecessary = false;

        bool m_realTimeUpdate = true;
        Vector2D m_fillPoint = null;
        bool m_calcVisbilityGraph = true;
        bool m_showGraph = false;
        bool m_show = true;
        Vector2D m_startP, m_endP;

        List<ObserverLink<Line>> m_listLines = new List<ObserverLink<Line>>();
        //List<ObserverLink<GeometryModel.Polygon>> listPolys = new List<ObserverLink<GeometryModel.Polygon>>();

        //Settings
        bool m_useGPU = true;
        String m_name;
        float m_precision = 0.02f;
        public CPlan.Isovist2D.AnalysisMeasure m_displayMeasure = CPlan.Isovist2D.AnalysisMeasure.ClusterVGA;

        public List<AnalysisPoint> m_analysisPoints;
        public List<Edge> m_visibilityEdges;
        public List<Edge> m_neumannEdges;
        public List<Edge> m_mooreEdges;
        int m_colorMode = 0;
        double m_cellSize;

        

        /*
        public TargetValues m_targetArea = new TargetValues();
        public TargetValues m_targetAreaDistanceWeighted = new TargetValues();
        public TargetValues m_targetCompactness = new TargetValues();
        public TargetValues m_targetPerimeter = new TargetValues();
        public TargetValues m_targetOcclusivity = new TargetValues();
        public TargetValues m_targetMinRadial = new TargetValues();
        public TargetValues m_targetMaxRadial = new TargetValues();
        */
        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Properties
        /*
        [CategoryAttribute("Measures"), DescriptionAttribute("Area")]
        public MeasureGrid Area { get { return m_isovistField.Area; } }
        [CategoryAttribute("Measures"), DescriptionAttribute("Area Distance Weighted")]
        public MeasureGrid AreaDistanceWeighted { get { return m_isovistField.AreaDistanceWeighted; } }
        [CategoryAttribute("Measures"), DescriptionAttribute("Compactness")]
        public MeasureGrid Compactness { get { return m_isovistField.Compactness; } }
        [CategoryAttribute("Measures"), DescriptionAttribute("Perimeter")]
        public MeasureGrid Perimeter { get { return m_isovistField.Perimeter; } }
        [CategoryAttribute("Measures"), DescriptionAttribute("Occlusivity")]
        public MeasureGrid Occlusivity { get { return m_isovistField.Occlusivity; } }
        [CategoryAttribute("Measures"), DescriptionAttribute("MinRadial")]
        public MeasureGrid MinRadial { get { return m_isovistField.MinRadial; } }
        [CategoryAttribute("Measures"), DescriptionAttribute("MaxRadial")]
        public MeasureGrid MaxRadial { get { return m_isovistField.MaxRadial; } }

        
        [CategoryAttribute("Optimization"), DescriptionAttribute("Target Values for Isovist Area")]
        public TargetValues TargetArea { get { return m_targetArea; } set { Begin(); m_targetArea = value; End(); } }
        [CategoryAttribute("Optimization"), DescriptionAttribute("Target Values for Isovist Area (Distance Weighted)")]
        public TargetValues TargetAreaDistanceWeighted { get { return m_targetAreaDistanceWeighted; } set { Begin(); m_targetAreaDistanceWeighted = value; End(); } }
        [CategoryAttribute("Optimization"), DescriptionAttribute("Target Values for Isovist Compactness")]
        public TargetValues TargetCompactness { get { return m_targetCompactness; } set { Begin(); m_targetCompactness = value; End(); } }
        [CategoryAttribute("Optimization"), DescriptionAttribute("Target Values for Isovist Perimeter")]
        public TargetValues TargetPerimeter { get { return m_targetPerimeter; } set { Begin(); m_targetPerimeter = value; End(); } }
        [CategoryAttribute("Optimization"), DescriptionAttribute("Target Values for Isovist Occlusivity")]
        public TargetValues TargetOcclusivity { get { return m_targetOcclusivity; } set { Begin(); m_targetOcclusivity = value; End(); } }
        [CategoryAttribute("Optimization"), DescriptionAttribute("Target Values for Isovist MinRadial")]
        public TargetValues TargetMinRadial { get { return m_targetMinRadial; } set { Begin(); m_targetMinRadial = value; End(); } }
        [CategoryAttribute("Optimization"), DescriptionAttribute("Target Values for Isovist MaxRadial")]
        public TargetValues TargetMaxRadial { get { return m_targetMaxRadial; } set { Begin(); m_targetMaxRadial = value; End(); } }
*/

        [CategoryAttribute("Settings"), DescriptionAttribute("Activates / Deactivates Real Time Update")]
        public bool RealTimeUpdate { get { return m_realTimeUpdate; } set { Begin(); m_realTimeUpdate = value; End(); } }
        [CategoryAttribute("Settings"), DescriptionAttribute("Precision of Isovist Calculation")]
        public float Precision { get { return m_precision; } set { Begin(); m_precision = value; End(); } }
        [CategoryAttribute("Settings"), DescriptionAttribute("Name of the Isovist Field")]
        public String Name { get { return m_name; } set { Begin(); m_name = value; End(); } }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Constructors
        public F_AnalysisGrid(Vector2D _startP, Vector2D _endP, Vector2D _fillPoint, double _cellSize, Visualisation.Model _vModel)
        {
            m_constructorRunning = true;
            m_UpdateCalculationNotNecessary = true;
            m_VModel = new ObserverLink<Visualisation.Model>(_vModel, this);
            m_VModel.Ref.Add(this);
            m_name = "AnalysisGrid " + m_VModel.Ref.GetNewId().ToString();

            GetObjectLists();

            m_fillPoint = _fillPoint;
            m_cellSize = _cellSize;
            m_analysisPoints = new List<AnalysisPoint>();
            m_startP = _startP;
            m_endP = _endP;

            Calculate();

            m_constructorRunning = false;
        }

        #endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Methods

        private void Begin()
        {
            m_bResetToken = false;
            DOMMC.ModelInterface pModelInterface = DOMMC.ModelInterface.ActualModelInterface;
            if (!pModelInterface.TransactionIsRunning)
            {
                pModelInterface.BeginTransaction();
                m_bResetToken = true;
            }
        }

        private void End()
        {
            ModifyObject();
            DOMMC.ModelInterface pModelInterface = DOMMC.ModelInterface.ActualModelInterface;
            if (m_bResetToken)
                pModelInterface.EndTransaction();
        }


        public List<Line2D> GetObstacleLines2D()
        {
            List<Line2D> obstacleLines = new List<Line2D>();

            foreach (ObserverLink<Line> lineS in m_listLines)
            {
                obstacleLines.Add(new Line2D(lineS.Ref.P1.Ref.XV, lineS.Ref.P1.Ref.YV, lineS.Ref.P2.Ref.XV, lineS.Ref.P2.Ref.YV));
            }
            /*
            foreach (ObserverLink<GeometryModel.Polygon> polyLink in listPolys)
            {
                GeometryModel.Polygon polyS = polyLink.Ref;
                GeometryModel.Point[] points = polyS.Loops[0].Ref.GetPoints();
                for (int i = 0; i < points.Length; i++)
                {
                    if (i == points.Length - 1)
                        obstacleLines.Add(new Line2D(points[i].XV, points[i].YV, points[0].XV, points[0].YV));
                    else
                        obstacleLines.Add(new Line2D(points[i].XV, points[i].YV, points[i + 1].XV, points[i + 1].YV));
                }
            }
            */
            return obstacleLines;
        }

        public void UpdateObstacleLines()
        {
            foreach (ObserverLink<Line> lineS in m_listLines)
            {
                Line2D line = new Line2D(lineS.Ref.P1.Ref.XV, lineS.Ref.P1.Ref.YV, lineS.Ref.P2.Ref.XV, lineS.Ref.P2.Ref.YV);
            }



            /*
            foreach (UMBuilding building in m_listUMBuildings)
            {
                List<Vector3D> footprint = building.FootprintPolygon.PointsSimpleMode;

                for (int i = 0; i < footprint.Count; i++)
                {
                    if (i == footprint.Count - 1)
                        m_isovist.ObstacleLines.Add(new Line2D(footprint[i].X, footprint[i].Y, footprint[0].X, footprint[0].Y));
                    else
                        m_isovist.ObstacleLines.Add(new Line2D(footprint[i].X, footprint[i].Y, footprint[i + 1].X, footprint[i + 1].Y));
                }
            }
             * */

        }

        public void GetObjectLists()
        {
            //listPolys.Clear();
            m_listLines.Clear();

            foreach (var obj in m_VModel.Ref.Objects)
            {
                if (obj.IsValid)
                {
                    // Get Lines
                    if ((obj.Ref.GetType().BaseType == typeof(Line)) || (obj.Ref.GetType() == typeof(Line)))
                    {
                        Line line = (Line)(obj.Ref);
                        if ((line.P1.Ref.ZV == 0) && (line.P2.Ref.ZV == 0))
                            m_listLines.Add(new ObserverLink<Line>(line, this));
                    }
                    /*
                    // Get Polygons
                    if ( (obj.Ref.GetType().BaseType == typeof(GeometryModel.Polygon)) || (obj.Ref.GetType() == typeof(GeometryModel.Polygon)))
                    {
                        GeometryModel.Polygon polyS = (GeometryModel.Polygon)(obj.Ref);

                        listPolys.Add(new ObserverLink<GeometryModel.Polygon>(polyS, this));
                    }
                     * */
                }
            }
        }

        public void Calculate()
        {
            Rect2D boundingRect = new Rect2D(m_startP, m_endP);
            List<Line2D> lines = GetObstacleLines2D();
            List<Vector2D> points = GeoAlgorithms2D.GetGridPoints(m_fillPoint, boundingRect, lines, m_cellSize);
            IsovistField2D isovistField = new IsovistField2D(points, lines);
            isovistField.Calculate(m_precision, m_useGPU);
            m_analysisPoints.Clear();
            foreach (CPlan.Isovist2D.AnalysisPoint ap in isovistField.AnalysisPoints)
                m_analysisPoints.Add(new AnalysisPoint(ap.X, ap.Y, ap.Results));

            //Create Visibility Graph
            if (m_calcVisbilityGraph)
                CreateGraph(lines);

            CalculateGraphMeasures();
        }

        public void CalculateGraphMeasures()
        {
            Smrf.NodeXL.Core.Graph graph = new Smrf.NodeXL.Core.Graph();

            foreach(AnalysisPoint ap in m_analysisPoints)
            {
                Vertex vertex = new Vertex();
                graph.Vertices.Add(vertex);
                ap.NodeXLVertexID = vertex.ID;
            }

            //VISIBILITY GRAPH..............................................
            foreach (Edge edge in m_visibilityEdges)
            {
                try
                {
                    IVertex vertex1, vertex2;
                    graph.Vertices.Find(edge.Node1.NodeXLVertexID, out vertex1);
                    graph.Vertices.Find(edge.Node2.NodeXLVertexID, out vertex2);
                    graph.Edges.Add(vertex1, vertex2, false);
                }
                catch (Exception e)
                { }
            }
            /*
            BrandesFastCentralityCalculator centralityCalculator = new BrandesFastCentralityCalculator();
            BackgroundWorker bgWorker = new BackgroundWorker();
            Dictionary<Int32, BrandesVertexCentralities> metricsCentralityVisibility = new Dictionary<int, BrandesVertexCentralities>();
            try 
            { 
                centralityCalculator.TryCalculateGraphMetrics(graph, null, out metricsCentralityVisibility);
            }
            catch (Exception ex) { }

            foreach (AnalysisPoint ap in m_analysisPoints)
            {
                ap.Properties.Add(AnalysisMeasure.ClosenessVGA, metricsCentralityVisibility[ap.NodeXLVertexID].ClosenessCentrality);
                ap.Properties.Add(AnalysisMeasure.BetweennessVGA, metricsCentralityVisibility[ap.NodeXLVertexID].BetweennessCentrality);
            }
            */
            ClusteringCoefficientCalculator clusteringCoefficientCalculator = new ClusteringCoefficientCalculator();
            Dictionary<int, double> metricClusteringCoeff = new Dictionary<int, double>();
            clusteringCoefficientCalculator.TryCalculateGraphMetrics(graph, null, out metricClusteringCoeff);

            foreach (AnalysisPoint ap in m_analysisPoints)
                ap.Properties.Add(AnalysisMeasure.ClusteringCoefficient, metricClusteringCoeff[ap.NodeXLVertexID]);
            
            ClusterCalculator clusterCalculator = new ClusterCalculator();
            clusterCalculator.Algorithm = ClusterAlgorithm.ClausetNewmanMoore;
            
            ICollection<Community> communities = null;
            try { clusterCalculator.TryCalculateGraphMetrics(graph, null, out communities); }
            catch (Exception ex) { }

            List<AnalysisPoint> tmpNodes = new List<AnalysisPoint>();
            foreach (AnalysisPoint node in m_analysisPoints)
                tmpNodes.Add(node);

            while (tmpNodes.Count != 0)
            //foreach (AnalysisPoint curNode in m_analysisPoints)
            {
                AnalysisPoint curNode = tmpNodes[0];
                
                int k = 0;
                foreach (Community community in communities)
                {                    
                    foreach (Vertex vertex in community.Vertices)
                    {
                        if (curNode.NodeXLVertexID == vertex.ID)
                        {
                            curNode.Properties.Add(AnalysisMeasure.ClusterVGA, k);
                            break;
                        }                        
                    }
                    k++;
                }
                tmpNodes.Remove(curNode);
            }
            /*
            //NEUMANN GRAPH..............................................
            graph.Edges.Clear();
            foreach (Edge edge in m_neumannEdges)
            {

                IVertex vertex1, vertex2;
                graph.Vertices.Find(edge.Node1.NodeXLVertexID, out vertex1);
                graph.Vertices.Find(edge.Node2.NodeXLVertexID, out vertex2);
                graph.Edges.Add(vertex1, vertex2, false);
            }
            Dictionary<Int32, BrandesVertexCentralities> metricsCentralityNeumann = new Dictionary<int, BrandesVertexCentralities>();
            centralityCalculator.TryCalculateGraphMetrics(graph, null, out metricsCentralityNeumann);


            foreach (AnalysisPoint ap in m_analysisPoints)
            {
                ap.Properties.Add(AnalysisMeasure.ClosenessNeumann, metricsCentralityNeumann[ap.NodeXLVertexID].ClosenessCentrality);
                ap.Properties.Add(AnalysisMeasure.BetweennessNeumann, metricsCentralityNeumann[ap.NodeXLVertexID].BetweennessCentrality);
            }

            communities = null;
            try { clusterCalculator.TryCalculateGraphMetrics(graph, null, out communities); }
            catch (Exception ex) { }

            tmpNodes = new List<AnalysisPoint>();
            foreach (AnalysisPoint node in m_analysisPoints)
                tmpNodes.Add(node);

            while (tmpNodes.Count != 0)
            //foreach (AnalysisPoint curNode in m_analysisPoints)
            {
                AnalysisPoint curNode = tmpNodes[0];

                int k = 0;
                foreach (Community community in communities)
                {
                    foreach (Vertex vertex in community.Vertices)
                    {
                        if (curNode.NodeXLVertexID == vertex.ID)
                        {
                            curNode.Properties.Add(AnalysisMeasure.ClusterNeumann, k);
                            break;
                        }
                    }
                    k++;
                }
                tmpNodes.Remove(curNode);
            }


            //MOORE GRAPH..............................................
            foreach (Edge edge in m_mooreEdges)
            {

                IVertex vertex1, vertex2;
                graph.Vertices.Find(edge.Node1.NodeXLVertexID, out vertex1);
                graph.Vertices.Find(edge.Node2.NodeXLVertexID, out vertex2);
                graph.Edges.Add(vertex1, vertex2, false);
            }
            Dictionary<Int32, BrandesVertexCentralities> metricsCentralityMoore = new Dictionary<int, BrandesVertexCentralities>();
            centralityCalculator.TryCalculateGraphMetrics(graph, null, out metricsCentralityMoore);

            foreach (AnalysisPoint ap in m_analysisPoints)
            {
                ap.Properties.Add(AnalysisMeasure.ClosenessMoore, metricsCentralityMoore[ap.NodeXLVertexID].ClosenessCentrality);
                ap.Properties.Add(AnalysisMeasure.BetweennessMoore, metricsCentralityMoore[ap.NodeXLVertexID].BetweennessCentrality);
            }

            communities = null;
            try { clusterCalculator.TryCalculateGraphMetrics(graph, null, out communities); }
            catch (Exception ex) { }

            tmpNodes = new List<AnalysisPoint>();
            foreach (AnalysisPoint node in m_analysisPoints)
                tmpNodes.Add(node);

            while (tmpNodes.Count != 0)
            //foreach (AnalysisPoint curNode in m_analysisPoints)
            {
                AnalysisPoint curNode = tmpNodes[0];

                int k = 0;
                foreach (Community community in communities)
                {
                    foreach (Vertex vertex in community.Vertices)
                    {
                        if (curNode.NodeXLVertexID == vertex.ID)
                        {
                            curNode.Properties.Add(AnalysisMeasure.ClusterMoore, k);
                            break;
                        }
                    }
                    k++;
                }
                tmpNodes.Remove(curNode);
            }
            */
            //FW_GPU_NET.Graph fwInverseGraphAngular = null;
            //FW_GPU_NET.Graph fwGraph;
            //float[] radi = new float[] { float.MaxValue };

            ////------------------------------------------------------------------------------//
            //fwGraph = GraphTools.GenerateFWGpuGraph(_network);
            //fwInverseGraphAngular = GraphTools.GenerateInverseDoubleGraph(fwGraph); // the costs/wightings are calculated here...

            //inverseGraph.Compute(FW_GPU_NET.Graph.ComputeType.CPU_DIJKSTRA_BASIC);
            //fwGraph.ComputeEdgeUsedCountsWithChilds(parentNodesPerRadius[0].ToArray(), true);


            //_shortPahtesAngular = new ShortestPath(_network);
            //_shortPahtesAngular.Evaluate(new System.Windows.Forms.ToolStripProgressBar(), fwGraph, fwInverseGraphAngular, radi);
            //fwInverseGraphAngular.Dispose();
            //fwInverseGraphAngular = null;

        }

        private void CreateGraph(List<Line2D> lines)
        {
            List<AnalysisPoint> tmpNodes = new List<AnalysisPoint>();
            foreach (AnalysisPoint node in m_analysisPoints)
            {
                node.VisualNeighbors = new HashSet<AnalysisPoint>();
                tmpNodes.Add(node);
            }

            m_visibilityEdges = new List<Edge>();
            m_neumannEdges = new List<Edge>();
            m_mooreEdges = new List<Edge>();

            while (tmpNodes.Count != 0)
            {
                AnalysisPoint curNode = tmpNodes[0];

                for (int i = 1; i < tmpNodes.Count; i++)
                {
                    bool isVisible = true;
                    foreach (Line2D line in lines)
                        if (GeoAlgorithms2D.LineIntersection(line.Start, line.End, curNode.Position, tmpNodes[i].Position) != null)
                        {
                            isVisible = false;
                            break;
                        }
                    if (isVisible)
                    {
                        curNode.VisualNeighbors.Add(tmpNodes[i]);
                        tmpNodes[i].VisualNeighbors.Add(curNode);
                        m_visibilityEdges.Add(new Edge(curNode, tmpNodes[i]));

                        double eps = 0.0001;
                        if (tmpNodes[i].Position.DistanceTo(curNode.Position) <= Math.Sqrt(2*m_cellSize*m_cellSize)+eps)
                        {
                            curNode.MooreNeighbors.Add(tmpNodes[i]);
                            tmpNodes[i].MooreNeighbors.Add(curNode);
                            m_mooreEdges.Add(new Edge(curNode, tmpNodes[i]));

                            if (tmpNodes[i].Position.DistanceTo(curNode.Position) <= m_cellSize + eps)
                            {
                                curNode.NeumannNeighbors.Add(tmpNodes[i]);
                                tmpNodes[i].NeumannNeighbors.Add(curNode);
                                m_neumannEdges.Add(new Edge(curNode, tmpNodes[i]));
                            }
                        }

                    }
                }
                tmpNodes.Remove(curNode);
            }
        
        }

        private double Map(double val, double minOrigin, double maxOrigin, double minTarget, double maxTarget)
        {
            if ((maxOrigin - minOrigin) != 0)
                return minTarget + (maxTarget - minTarget) * (val - minOrigin) / (maxOrigin - minOrigin);
            else if (minOrigin == maxOrigin)
                return minOrigin;
            else
                return maxOrigin;
        }

        # endregion

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Interfaces

        # region Visualisation
        public void Dialog(GUI gui)
        {
            Form_DialogAnalysisGrid DialogIF = new Form_DialogAnalysisGrid(this);
        }

        public void Draw2D(GUI Gui)
        {
            Draw3D(Gui);
        }

        public void Draw3D(GUI Gui)
        {
            if (!m_show)
                return;
            

            //Offset
            float offset = -640;
            if (offset != 0.0f)
            {
                GL.Enable(EnableCap.PolygonOffsetPoint);
                GL.Enable(EnableCap.PolygonOffsetLine);
                GL.Enable(EnableCap.PolygonOffsetFill);
                GL.PolygonOffset(0.0f, -offset);
            }

            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
            //GL.DepthMask(false);


            //find min & max
            double min = double.MaxValue;
            double max = double.MinValue;
            foreach (AnalysisPoint ap in m_analysisPoints)
            {
                if (ap.Properties[m_displayMeasure] > max)
                    max = ap.Properties[m_displayMeasure];
                if (ap.Properties[m_displayMeasure] < min)
                    min = ap.Properties[m_displayMeasure];
            }

            foreach (AnalysisPoint ap in m_analysisPoints)
            {
                //hier noch unterschiedliche Farbskalen/Interpolationen einbauen
                Vector4 FillColor = new Vector4();
                float cl = (float)Map(ap.Properties[m_displayMeasure], min, max, 0, 1);
                switch (m_colorMode)
                {
                    case 0:                        
                        FillColor = new Vector4(cl, cl, cl, 1f);
                        break;
                    case 1:
                        FillColor = new Vector4(cl, cl, cl, 1f);
                        break;
                    default:
                        break;
                }

                GL.Begin(BeginMode.Polygon);
                GL.Color4(FillColor);
                GL.Vertex3(ap.Position.X + m_cellSize / 2, 0, -(ap.Position.Y + m_cellSize / 2));
                GL.Vertex3(ap.Position.X - m_cellSize / 2, 0, -(ap.Position.Y + m_cellSize / 2));
                GL.Vertex3(ap.Position.X - m_cellSize / 2, 0, -(ap.Position.Y - m_cellSize / 2));
                GL.Vertex3(ap.Position.X + m_cellSize / 2, 0, -(ap.Position.Y - m_cellSize / 2));
                GL.End();
            }
                    
        


            //GL.DepthMask(true);
            if (offset != 0.0f)
            {
                GL.Disable(EnableCap.PolygonOffsetPoint);
                GL.Disable(EnableCap.PolygonOffsetLine);
                GL.Disable(EnableCap.PolygonOffsetFill);
            }


            if (m_showGraph && m_mooreEdges != null)
            {
                foreach (Edge edge in m_mooreEdges)
                    UserDraw.DrawLine(Gui, edge.Node1.Position.Vec3D, edge.Node2.Position.Vec3D, 0.5f, 0.5f, 0.5f, 0.8f);
                foreach (AnalysisPoint ap in m_analysisPoints)
                    UserDraw.DrawPoint(Gui, ap.Position.Vec3D, 0.1f, 0,0,0,1);
            }

        }

        public void GetBoundingBox(GUI Gui, Visualisation.BoundingBox box)
        {
        }

        public string GetIdentificationName()
        {
            return m_name;
        }

        public void SetVisualisationModel(Visualisation.Model model)
        {

        }
        #endregion

        void IDisposable.Dispose()
        {

        }

        # region ICustomTypeDescriptor
        private string[] CategoriesToShow = { "Measures", "Settings", "Information", "Visualisation", "Optimization" };

        //Does the property filtering...
        private PropertyDescriptorCollection FilterProperties(PropertyDescriptorCollection pdc)
        {
            List<String> toShow = new List<string>();
            foreach (string s in CategoriesToShow)
                toShow.Add(s);

            PropertyDescriptorCollection adjustedProps =
            new PropertyDescriptorCollection(new PropertyDescriptor[] { });
            foreach (PropertyDescriptor pd in pdc)
                if (toShow.Contains(pd.Category))
                    adjustedProps.Add(pd);

            return adjustedProps;
        }
        public AttributeCollection GetAttributes()
        {
            return TypeDescriptor.GetAttributes(this, true);
        }

        public string GetClassName()
        {
            return TypeDescriptor.GetClassName(this, true);
        }

        public string GetComponentName()
        {
            return TypeDescriptor.GetComponentName(this, true);
        }

        public TypeConverter GetConverter()
        {
            return TypeDescriptor.GetConverter(this, true);
        }

        public EventDescriptor GetDefaultEvent()
        {
            return TypeDescriptor.GetDefaultEvent(this, true);
        }

        public PropertyDescriptor GetDefaultProperty()
        {
            return TypeDescriptor.GetDefaultProperty(this, true);
        }

        public object GetEditor(Type editorBaseType)
        {
            return TypeDescriptor.GetEditor(this, editorBaseType, true);
        }

        public EventDescriptorCollection GetEvents(Attribute[] attributes)
        {
            return TypeDescriptor.GetEvents(this, attributes, true);
        }

        public EventDescriptorCollection GetEvents()
        {
            return TypeDescriptor.GetEvents(this, true);
        }

        public PropertyDescriptorCollection GetProperties(Attribute[] attributes)
        {
            PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(this, attributes, true);
            return FilterProperties(pdc);
        }

        public PropertyDescriptorCollection GetProperties()
        {
            PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(this, true);
            return FilterProperties(pdc);
        }

        public object GetPropertyOwner(PropertyDescriptor pd)
        {
            return this;
        }
        # endregion

        public void PreprocessDrawing(GUI Gui)
        {
            if (m_realTimeUpdate)
                if (!m_UpdateCalculationNotNecessary)
                {
                    Gui.UseHoverToolTip = false;
                    GetObstacleLines2D();
                    UpdateObstacleLines();
                    Calculate();

                    //if (m_calcVisbilityGraph)
                    //{
                    //    m_visibilityGraph.BuildGraph();
                    //    m_visibilityGraph.CalculateTotalDepth();
                    //}
                    m_UpdateCalculationNotNecessary = true;
                    Gui.UseHoverToolTip = true;
                }
        }

        public override void RecvObserverMessage(IObservable sender, ObserverMessage message)
        {
            if (m_constructorRunning || !m_realTimeUpdate)
                return;

            if (message.MessageType == DOMMC.ObserverMessageType.OBSERVED_DELETE)
            {

                if ((sender.GetType() == typeof(Line)) || (sender.GetType().BaseType == typeof(Line)))
                {
                    Line lineS = (Line)sender;
                    ObserverLink<Line> ObserverLinkLine = null;
                    foreach (ObserverLink<Line> ObserverLink in m_listLines)
                    {
                        if (ObserverLink.Ref == lineS)
                            ObserverLinkLine = ObserverLink;
                    }
                    if (ObserverLinkLine != null)
                        m_listLines.Remove(ObserverLinkLine);

                    m_UpdateCalculationNotNecessary = false;
                }
            }
            if (message.MessageType == DOMMC.ObserverMessageType.OBSERVED_CHANGED)
            {
                if ((sender.GetType() == typeof(Line)) || (sender.GetType().BaseType == typeof(Line)))
                {
                    m_UpdateCalculationNotNecessary = false;
                }

                if (sender.GetType() == typeof(Visualisation.Model))
                {
                    GetObjectLists();
                    m_UpdateCalculationNotNecessary = false;
                }
            }
        }

        #endregion
        /*
        public List<double> GetPeformanceValues()
        {
            List<double> perfValues = new List<double>();

            //Area:
            if (TargetArea.Min >= 0)
                if (Area.Min < TargetArea.Min)
                    perfValues.Add((double)Math.Abs(TargetArea.Min - Area.Min));
                else
                    perfValues.Add(0);

            if (TargetArea.Max >= 0)
                if (Area.Max > TargetArea.Max)
                    perfValues.Add(Math.Abs(TargetArea.Max - Area.Max));
                else
                    perfValues.Add(0);

            if (TargetArea.Avg >= 0)
                perfValues.Add(Math.Abs(TargetArea.Avg - Area.Mean));

            if (TargetArea.StdDev >= 0)
                perfValues.Add(Math.Abs(TargetArea.StdDev - Area.StdDev));


            //AreaDistanceWeighted:
            if (TargetAreaDistanceWeighted.Min >= 0)
                if (AreaDistanceWeighted.Min < TargetAreaDistanceWeighted.Min)
                    perfValues.Add(Math.Abs(TargetAreaDistanceWeighted.Min - AreaDistanceWeighted.Min));
                else
                    perfValues.Add(0);

            if (TargetAreaDistanceWeighted.Max >= 0)
                if (AreaDistanceWeighted.Max > TargetAreaDistanceWeighted.Max)
                    perfValues.Add(Math.Abs(TargetAreaDistanceWeighted.Max - AreaDistanceWeighted.Max));
                else
                    perfValues.Add(0);

            if (TargetAreaDistanceWeighted.Avg >= 0)
                perfValues.Add(Math.Abs(TargetAreaDistanceWeighted.Avg - AreaDistanceWeighted.Mean));

            if (TargetAreaDistanceWeighted.StdDev >= 0)
                perfValues.Add(Math.Abs(TargetAreaDistanceWeighted.StdDev - AreaDistanceWeighted.StdDev));


            //Perimeter:
            if (TargetPerimeter.Min >= 0)
                if (Perimeter.Min < TargetPerimeter.Min)
                    perfValues.Add(Math.Abs(TargetPerimeter.Min - Perimeter.Min));
                else
                    perfValues.Add(0);

            if (TargetPerimeter.Max >= 0)
                if (Perimeter.Max > TargetPerimeter.Max)
                    perfValues.Add(Math.Abs(TargetPerimeter.Max - Perimeter.Max));
                else
                    perfValues.Add(0);

            if (TargetPerimeter.Avg >= 0)
                perfValues.Add(Math.Abs(TargetPerimeter.Avg - Perimeter.Mean));

            if (TargetPerimeter.StdDev >= 0)
                perfValues.Add(Math.Abs(TargetPerimeter.StdDev - Perimeter.StdDev));

            //Compactness:
            if (TargetCompactness.Min >= 0)
                if (Compactness.Min < TargetCompactness.Min)
                    perfValues.Add(Math.Abs(TargetCompactness.Min - Compactness.Min));
                else
                    perfValues.Add(0);

            if (TargetCompactness.Max >= 0)
                if (Compactness.Max > TargetCompactness.Max)
                    perfValues.Add(Math.Abs(TargetCompactness.Max - Compactness.Max));
                else
                    perfValues.Add(0);

            if (TargetCompactness.Avg >= 0)
                perfValues.Add(Math.Abs(TargetCompactness.Avg - Compactness.Mean));

            if (TargetCompactness.StdDev >= 0)
                perfValues.Add(Math.Abs(TargetCompactness.StdDev - Compactness.StdDev));


            //Occlusivity:
            if (TargetOcclusivity.Min >= 0)
                if (Occlusivity.Min < TargetOcclusivity.Min)
                    perfValues.Add(Math.Abs(TargetOcclusivity.Min - Occlusivity.Min));
                else
                    perfValues.Add(0);

            if (TargetOcclusivity.Max >= 0)
                if (Occlusivity.Max > TargetOcclusivity.Max)
                    perfValues.Add(Math.Abs(TargetOcclusivity.Max - Occlusivity.Max));
                else
                    perfValues.Add(0);

            if (TargetOcclusivity.Avg >= 0)
                perfValues.Add(Math.Abs(TargetOcclusivity.Avg - Occlusivity.Mean));

            if (TargetOcclusivity.StdDev >= 0)
                perfValues.Add(Math.Abs(TargetOcclusivity.StdDev - Occlusivity.StdDev));


            //MinRadial:
            if (TargetMinRadial.Min >= 0)
                if (MinRadial.Min < TargetMinRadial.Min)
                    perfValues.Add(Math.Abs(TargetMinRadial.Min - MinRadial.Min));
                else
                    perfValues.Add(0);

            if (TargetMinRadial.Max >= 0)
                if (MinRadial.Max > TargetMinRadial.Max)
                    perfValues.Add(Math.Abs(TargetMinRadial.Max - MinRadial.Max));
                else
                    perfValues.Add(0);

            if (TargetMinRadial.Avg >= 0)
                perfValues.Add(Math.Abs(TargetMinRadial.Avg - MinRadial.Mean));

            if (TargetMinRadial.StdDev >= 0)
                perfValues.Add(Math.Abs(TargetMinRadial.StdDev - MinRadial.StdDev));


            //MaxRadial:
            if (TargetMaxRadial.Min >= 0)
                if (MaxRadial.Min < TargetMaxRadial.Min)
                    perfValues.Add(Math.Abs(TargetMaxRadial.Min - MaxRadial.Min));
                else
                    perfValues.Add(0);

            if (TargetMaxRadial.Max >= 0)
                if (MaxRadial.Max > TargetMaxRadial.Max)
                    perfValues.Add(Math.Abs(TargetMaxRadial.Max - MaxRadial.Max));
                else
                    perfValues.Add(0);

            if (TargetMaxRadial.Avg >= 0)
                perfValues.Add(Math.Abs(TargetMaxRadial.Avg - MaxRadial.Mean));

            if (TargetMaxRadial.StdDev >= 0)
                perfValues.Add(Math.Abs(TargetMaxRadial.StdDev - MaxRadial.StdDev));




            return perfValues;
        }*/
    }
            
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region AdditionalClasses

        [Serializable]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public class AnalysisPoint
        {
            Vector2D m_position;
            HashSet<AnalysisPoint> m_mooreNeighbors;
            HashSet<AnalysisPoint> m_neumannNeigbors;
            HashSet<AnalysisPoint> m_visualNeighbors;
            Dictionary<CPlan.Isovist2D.AnalysisMeasure, double> m_properties;
            int m_vertexID;

            public AnalysisPoint(double pX, double pY, Dictionary<CPlan.Isovist2D.AnalysisMeasure, double> properties)
            {
                m_position = new Vector2D(pX, pY);
                m_properties = new Dictionary<CPlan.Isovist2D.AnalysisMeasure, double>();
                m_properties = properties;
                m_visualNeighbors = new HashSet<AnalysisPoint>();
                m_mooreNeighbors = new HashSet<AnalysisPoint>();
                m_neumannNeigbors = new HashSet<AnalysisPoint>();
            }

            public Dictionary<CPlan.Isovist2D.AnalysisMeasure, double> Properties
            {
                get { return m_properties; }
                set { m_properties = value; }
            }

            public Vector2D Position
            {
                get { return m_position; }
                set { m_position = value; }
            }

            public int NodeXLVertexID
            {
                get { return m_vertexID; }
                set { m_vertexID = value; }
            }

            public HashSet<AnalysisPoint> VisualNeighbors
            {
                get { return m_visualNeighbors; }
                set { m_visualNeighbors = value; }
            }

            public HashSet<AnalysisPoint> NeumannNeighbors
            {
                get { return m_neumannNeigbors; }
                set { m_neumannNeigbors = value; }
            }

            public HashSet<AnalysisPoint> MooreNeighbors
            {
                get { return m_mooreNeighbors; }
                set { m_mooreNeighbors = value; }
            }
        }
        
        [Serializable]
        public class Edge
        {
            AnalysisPoint m_node1;
            AnalysisPoint m_node2;

            public AnalysisPoint Node1
            {
                get { return m_node1; }
                set { m_node1 = value; }
            }
            public AnalysisPoint Node2
            {
                get { return m_node2; }
                set { m_node2 = value; }
            }

            public Edge(AnalysisPoint node1, AnalysisPoint node2)
            {
                m_node1 = node1;
                m_node2 = node2;                       
            }
        }

        [Serializable]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public class TargetValues
        {
            float min = -1, max = -1, avg = -1, stdDev = -1;

            public float Min { get { return min; } set { min = value; } }
            public float Max { get { return max; } set { max = value; } }
            public float Avg { get { return avg; } set { avg = value; } }
            public float StdDev { get { return stdDev; } set { stdDev = value; } }

        }

        #endregion;

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        # region Handler


        [Serializable]
        [CreateObjectAttribute]
        public class CreateIsovistField
        {
            public static String GetCreationName()
            {
                return "VisbilityAnalysisModel.Create IsovistField";
            }

            public static InputHandler GetCreationHandler(GUI gui)
            {
                return new Handler(gui);
            }

            class Handler : InputHandler
            {
                Vector2D firstP, secondP;
                CGeom3d.Vec_3d mouse_point;
                public Handler(GUI gui)
                    : base(gui)
                {
                    HandlerMessage = "Set first point of the grid";
                }

                public override bool MouseDown(object sender, MouseEventArgs e)
                {
                    Link<VisibilityAnalysisModel.VisibilityModel> vAModel = new Link<VisibilityAnalysisModel.VisibilityModel>();

                    if (e.Button == MouseButtons.Left)
                    {
                        if (this.MyMasterModelTable == null) return true;

                        ObjectId modelId = this.MyMasterModelTable.FindModel(typeof(VisibilityAnalysisModel.VisibilityModel));
                        if (modelId.IsSet == true)
                            vAModel.ID = modelId;
                        else
                        {
                            this.MyModelInterface.BeginTransaction();
                            vAModel = new Link<VisibilityAnalysisModel.VisibilityModel>(new VisibilityAnalysisModel.VisibilityModel());
                            this.MyMasterModelTable.AddModel(vAModel);
                            this.MyModelInterface.EndTransaction();
                        }


                        CGeom3d.Vec_3d pointXY = MyCamera.GetPointXYPtn(e.X, e.Y, MyControl.Width, MyControl.Height);
                        if (pointXY == null)
                            return true;

                        if (firstP == null)
                        {
                            firstP = new Vector2D(pointXY.x, pointXY.y);
                            HandlerMessage = "Set second point of the grid";
                        }
                        else
                        {
                            secondP = new Vector2D(pointXY.x, pointXY.y);
                            this.MyModelInterface.BeginTransaction();
                            double cellSize = 0.3;
                            double dX = Math.Abs(secondP.X - firstP.X);
                            double dY = secondP.Y - firstP.Y;

                            if (dX > dY)
                                cellSize = dX / 80;
                            else
                                cellSize = dY / 80;

                            cellSize = 1;

                            Vector2D fillPoint = new Vector2D(firstP.X + dX/2, firstP.Y + dY/2);

                            Link<F_AnalysisGrid> curIsovistField = new Link<F_AnalysisGrid>(new F_AnalysisGrid(firstP, secondP, fillPoint, cellSize, MyVisualisationModel));//, MyGUI));
                            //this.MyVisualisationModel.Add(curIsovistField.Ref);
                            this.MyModelInterface.EndTransaction();
                            this.MyControl.Invalidate();
                            this.MyGUI.EraseInputHandler(this);
                        }



                        return false;
                    }
                    else
                    {
                        this.MyGUI.EraseInputHandler(this);
                    }

                    return true;
                }

                public override bool MouseMove(Object sender, MouseEventArgs e)
                {
                    if (MyModelInterface == null)
                        return true;

                    bool point_valid = true;
                    CGeom3d.Vec_3d point_xy;

                    point_xy = MyCamera.GetPointXYPtn(e.X, e.Y, MyControl.Width, MyControl.Height);
                    point_xy.z = 0;

                    mouse_point = point_xy;
                    MyForm.Invalidate();
                    MyForm.Update();

                    return true;
                }

                public override void DrawTemporary()
                {
                    if (firstP != null)
                    {
                        GL.Color3(System.Drawing.Color.DarkGray);
                        GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
                        GL.Begin(BeginMode.Polygon);
                        GL.Vertex3(firstP.X, mouse_point.z, -firstP.Y);
                        GL.Vertex3(firstP.X, mouse_point.z, -mouse_point.y);
                        GL.Vertex3(mouse_point.x, mouse_point.z, -mouse_point.y);
                        GL.Vertex3(mouse_point.x, mouse_point.z, -firstP.Y);
                        GL.End();
                        GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                    }
                }
            }
        }

    /*
        [Serializable]
        [CreateObjectAttribute]
        public class SearchForSimilarities
        {
            public static String GetCreationName()
            {
                return "VisbilityAnalysisModel.Find similar Vantage Points";
            }

            public static InputHandler GetCreationHandler(GUI gui)
            {
                return new Handler(gui);
            }

            class Handler : InputHandler
            {
                List<Vector2D> similarPoints;
                CGeom3d.Vec_3d mouse_point;
                PickHint[] hits;
                float cellSize;

                public Handler(GUI gui)
                    : base(gui)
                {
                    similarPoints = new List<Vector2D>();
                    HandlerMessage = "Set Original Point";
                }

                public override bool MouseDown(object sender, MouseEventArgs e)
                {
                    Link<VisibilityAnalysisModel.VisibilityModel> vAModel = new Link<VisibilityAnalysisModel.VisibilityModel>();

                    if (e.Button == MouseButtons.Left)
                    {
                        if (this.MyMasterModelTable == null) return true;

                        ObjectId modelId = this.MyMasterModelTable.FindModel(typeof(VisibilityAnalysisModel.VisibilityModel));
                        if (modelId.IsSet == true)
                            vAModel.ID = modelId;
                        else
                        {
                            this.MyModelInterface.BeginTransaction();
                            vAModel = new Link<VisibilityAnalysisModel.VisibilityModel>(new VisibilityAnalysisModel.VisibilityModel());
                            this.MyMasterModelTable.AddModel(vAModel);
                            this.MyModelInterface.EndTransaction();
                        }


                        CGeom3d.Vec_3d pointXY = MyCamera.GetPointXYPtn(e.X, e.Y, MyControl.Width, MyControl.Height);
                        if (pointXY == null)
                            return true;
                        Vector2D originalP = new Vector2D(pointXY.x, pointXY.y);


                        hits = this.SelectAt(e.X, e.Y);
                        F_AnalysisGrid isoField = null;
                        foreach (PickHint hit in hits)
                        {
                            if (hit.@object.GetType() == typeof(F_AnalysisGrid))
                                isoField = (F_AnalysisGrid)hit.@object;

                        }


                        if (isoField != null)
                            FindSimilarPoints(originalP, isoField);

                        this.MyControl.Invalidate();
                        //this.MyGUI.EraseInputHandler(this);




                        return false;
                    }
                    else
                    {
                        this.MyGUI.EraseInputHandler(this);
                    }

                    return true;
                }

                public void FindSimilarPoints(Vector2D originalP, F_AnalysisGrid isoField)
                {
                    similarPoints.Clear();
                    cellSize = (float)isoField.CellSize;

                    float range = 0.1f;
                    float thresholdArea = (isoField.Area.Max - isoField.Area.Min) * range;
                    float thresholdCompactness = (isoField.Compactness.Max - isoField.Compactness.Min) * range;
                    float thresholdPerimeter = (isoField.Perimeter.Max - isoField.Perimeter.Min) * range;
                    float thresholdMinRadial = (isoField.MinRadial.Max - isoField.MinRadial.Min) * range;
                    float thresholdMaxRadial = (isoField.MaxRadial.Max - isoField.MaxRadial.Min) * range;

                    double width = isoField.EndP.X - isoField.StartP.X;
                    int originalI = (int)(((originalP.X - isoField.StartP.X) / width) * isoField.CountX);
                    double height = isoField.EndP.Y - isoField.StartP.Y;
                    int originalJ = (int)(((originalP.Y - isoField.StartP.Y) / height) * isoField.CountY);


                    for (int i = 0; i < isoField.CountX; i++)
                        for (int j = 0; j < isoField.CountY; j++)
                        {
                            int cntSimilarFeatures = 0;
                            if (isoField.m_isovistField.ActiveCells[j * isoField.CountX + i])
                            {
                                if ((isoField.Area.Values[originalJ * isoField.CountX + originalI] >= isoField.Area.Values[j * isoField.CountX + i] - thresholdArea) &&
                                    (isoField.Area.Values[originalJ * isoField.CountX + originalI] <= isoField.Area.Values[j * isoField.CountX + i] + thresholdArea))
                                    cntSimilarFeatures++;
                                if ((isoField.Compactness.Values[originalJ * isoField.CountX + originalI] >= isoField.Compactness.Values[j * isoField.CountX + i] - thresholdCompactness) &&
                                    (isoField.Compactness.Values[originalJ * isoField.CountX + originalI] <= isoField.Compactness.Values[j * isoField.CountX + i] + thresholdCompactness))
                                    cntSimilarFeatures++;
                                if ((isoField.Perimeter.Values[originalJ * isoField.CountX + originalI] >= isoField.Perimeter.Values[j * isoField.CountX + i] - thresholdPerimeter) &&
                                    (isoField.Perimeter.Values[originalJ * isoField.CountX + originalI] <= isoField.Perimeter.Values[j * isoField.CountX + i] + thresholdPerimeter))
                                    cntSimilarFeatures++;
                                if ((isoField.MinRadial.Values[originalJ * isoField.CountX + originalI] >= isoField.MinRadial.Values[j * isoField.CountX + i] - thresholdMinRadial) &&
                                   (isoField.MinRadial.Values[originalJ * isoField.CountX + originalI] <= isoField.MinRadial.Values[j * isoField.CountX + i] + thresholdMinRadial))
                                    cntSimilarFeatures++;
                                if ((isoField.MaxRadial.Values[originalJ * isoField.CountX + originalI] >= isoField.MaxRadial.Values[j * isoField.CountX + i] - thresholdMaxRadial) &&
                                    (isoField.MaxRadial.Values[originalJ * isoField.CountX + originalI] <= isoField.MaxRadial.Values[j * isoField.CountX + i] + thresholdMaxRadial))
                                    cntSimilarFeatures++;

                                if (cntSimilarFeatures >= 4)
                                    similarPoints.Add(new Vector2D(isoField.StartP.X + i * isoField.CellSize, isoField.StartP.Y + j * isoField.CellSize));
                            }


                        }

                    this.MyControl.Invalidate();
                    MyForm.Invalidate();
                    MyForm.Update();
                }

                public override bool MouseUp(object sender, MouseEventArgs e)
                {
                    similarPoints.Clear();
                    //this.MyGUI.EraseInputHandler(this);
                    MyForm.Invalidate();
                    MyForm.Update();
                    return true;
                }

                public override void DrawTemporary()
                {
                    // draw Circles around similar points...
                    float size = cellSize;
                    Vector4 FillColor = new Vector4(1, 0, 0, 1);
                    foreach (Vector2D vec in similarPoints)
                    {
                        GL.Begin(BeginMode.Polygon);
                        GL.Color4(FillColor);
                        GL.Vertex3(vec.X, 0.1, -vec.Y);
                        GL.Vertex3(vec.X + size, 0.1, -vec.Y);
                        GL.Vertex3(vec.X + size, 0.1, -(vec.Y + size));
                        GL.Vertex3(vec.X, 0.1, -(vec.Y + size));
                        GL.End();
                    }

                }

            }
        }
    */
        #endregion

    }

